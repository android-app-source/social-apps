.class public final LX/1mA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04o;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Z

.field private final c:LX/04m;

.field private final d:I

.field private final e:J

.field private final f:J

.field private final g:J

.field private final h:F

.field private final i:F

.field private final j:Z

.field private final k:LX/1m6;

.field private final l:LX/0wp;

.field private final m:LX/0ka;

.field private final n:LX/0oz;

.field private final o:LX/0YR;

.field private final p:LX/1AA;

.field private q:LX/04G;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 313239
    const-class v0, LX/1mA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1mA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/04m;ZLX/1m6;LX/0wp;LX/0ka;LX/0oz;LX/0YR;LX/1AA;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 313221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313222
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1mA;->b:Z

    .line 313223
    iput-object p1, p0, LX/1mA;->c:LX/04m;

    .line 313224
    iget v0, p4, LX/0wp;->s:I

    iput v0, p0, LX/1mA;->d:I

    .line 313225
    iget v0, p4, LX/0wp;->t:I

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/1mA;->e:J

    .line 313226
    iget v0, p4, LX/0wp;->u:I

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/1mA;->f:J

    .line 313227
    iget v0, p4, LX/0wp;->v:I

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/1mA;->g:J

    .line 313228
    iget v0, p4, LX/0wp;->w:F

    iput v0, p0, LX/1mA;->h:F

    .line 313229
    iget v0, p4, LX/0wp;->x:F

    iput v0, p0, LX/1mA;->i:F

    .line 313230
    iput-boolean p2, p0, LX/1mA;->j:Z

    .line 313231
    iput-object p3, p0, LX/1mA;->k:LX/1m6;

    .line 313232
    iput-object p4, p0, LX/1mA;->l:LX/0wp;

    .line 313233
    iput-object p5, p0, LX/1mA;->m:LX/0ka;

    .line 313234
    iput-object p6, p0, LX/1mA;->n:LX/0oz;

    .line 313235
    invoke-virtual {p3}, LX/1m6;->b()LX/04G;

    move-result-object v0

    iput-object v0, p0, LX/1mA;->q:LX/04G;

    .line 313236
    iput-object p7, p0, LX/1mA;->o:LX/0YR;

    .line 313237
    iput-object p8, p0, LX/1mA;->p:LX/1AA;

    .line 313238
    return-void
.end method

.method private static a(LX/1mA;LX/0AR;)I
    .locals 2

    .prologue
    .line 313220
    iget-object v0, p0, LX/1mA;->l:LX/0wp;

    iget-boolean v0, v0, LX/0wp;->G:Z

    if-eqz v0, :cond_0

    iget v0, p1, LX/0AR;->f:I

    iget v1, p1, LX/0AR;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, LX/0AR;->f:I

    goto :goto_0
.end method

.method private static a(LX/1mA;JF)J
    .locals 3

    .prologue
    .line 313219
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/1mA;->d:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    long-to-float v0, p1

    mul-float/2addr v0, p3

    float-to-long v0, v0

    goto :goto_0
.end method

.method private static a(LX/1mA;LX/0xt;)J
    .locals 6

    .prologue
    .line 313208
    const-wide/16 v0, -0x1

    .line 313209
    sget-object v2, LX/7Q1;->a:[I

    invoke-virtual {p1}, LX/0xt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 313210
    :cond_0
    :goto_0
    return-wide v0

    .line 313211
    :pswitch_0
    iget-object v2, p0, LX/1mA;->n:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->f()D

    move-result-wide v2

    .line 313212
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 313213
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    goto :goto_0

    .line 313214
    :pswitch_1
    iget-object v0, p0, LX/1mA;->c:LX/04m;

    invoke-interface {v0}, LX/04m;->a()J

    move-result-wide v0

    goto :goto_0

    .line 313215
    :pswitch_2
    iget-object v2, p0, LX/1mA;->o:LX/0YR;

    invoke-interface {v2}, LX/0YR;->a()LX/1hM;

    move-result-object v2

    .line 313216
    if-eqz v2, :cond_0

    .line 313217
    iget-object v0, v2, LX/1hM;->I:Ljava/lang/Long;

    move-object v0, v0

    .line 313218
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a([LX/0AR;LX/0AR;JZJ)LX/0AR;
    .locals 11

    .prologue
    .line 313125
    iget v2, p0, LX/1mA;->h:F

    invoke-static {p0, p3, p4, v2}, LX/1mA;->a(LX/1mA;JF)J

    move-result-wide v6

    .line 313126
    iget-object v2, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v2}, LX/1m6;->c()LX/0AR;

    move-result-object v3

    .line 313127
    if-nez p5, :cond_3

    iget-object v2, p0, LX/1mA;->l:LX/0wp;

    iget-boolean v2, v2, LX/0wp;->E:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/1mA;->q:LX/04G;

    iget-object v4, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v4}, LX/1m6;->b()LX/04G;

    move-result-object v4

    if-ne v2, v4, :cond_1

    :cond_0
    if-nez p2, :cond_3

    :cond_1
    if-eqz v3, :cond_3

    .line 313128
    const/4 v2, 0x0

    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_3

    .line 313129
    aget-object v4, p1, v2

    iget-object v4, v4, LX/0AR;->a:Ljava/lang/String;

    iget-object v5, v3, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v4, p1, v2

    iget v4, v4, LX/0AR;->c:I

    iget v5, v3, LX/0AR;->c:I

    if-ne v4, v5, :cond_2

    .line 313130
    aget-object v2, p1, v2

    .line 313131
    :goto_1
    return-object v2

    .line 313132
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 313133
    :cond_3
    if-nez p5, :cond_4

    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-nez v2, :cond_a

    .line 313134
    :cond_4
    iget-object v2, p0, LX/1mA;->l:LX/0wp;

    iget-object v3, p0, LX/1mA;->m:LX/0ka;

    iget-object v4, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v4}, LX/1m6;->f()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wp;->a(LX/0ka;Z)I

    move-result v5

    .line 313135
    const/4 v2, 0x0

    .line 313136
    iget-object v3, p0, LX/1mA;->l:LX/0wp;

    iget-boolean v3, v3, LX/0wp;->P:Z

    if-eqz v3, :cond_5

    .line 313137
    iget-object v2, p0, LX/1mA;->p:LX/1AA;

    iget-object v3, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v3}, LX/1m6;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1AA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 313138
    :cond_5
    const/4 v3, 0x0

    :goto_2
    array-length v4, p1

    if-ge v3, v4, :cond_a

    .line 313139
    aget-object v4, p1, v3

    .line 313140
    if-eqz v2, :cond_6

    .line 313141
    iget-object v8, v4, LX/0AR;->a:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    move-object v2, v4

    .line 313142
    goto :goto_1

    .line 313143
    :cond_6
    if-lez v5, :cond_7

    .line 313144
    invoke-static {p0, v4}, LX/1mA;->a(LX/1mA;LX/0AR;)I

    move-result v8

    if-gt v8, v5, :cond_9

    move-object v2, v4

    .line 313145
    goto :goto_1

    .line 313146
    :cond_7
    iget-object v8, v4, LX/0AR;->a:Ljava/lang/String;

    const-string v9, "vd"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    iget-object v8, v4, LX/0AR;->a:Ljava/lang/String;

    const-string v9, "ad"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    :cond_8
    move-object v2, v4

    .line 313147
    goto :goto_1

    .line 313148
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 313149
    :cond_a
    const/4 v2, 0x0

    :goto_3
    array-length v3, p1

    if-ge v2, v3, :cond_c

    .line 313150
    aget-object v3, p1, v2

    .line 313151
    iget v4, v3, LX/0AR;->c:I

    int-to-long v4, v4

    cmp-long v4, v4, v6

    if-gtz v4, :cond_b

    move-wide/from16 v0, p6

    invoke-static {p0, v3, p2, v0, v1}, LX/1mA;->a(LX/1mA;LX/0AR;LX/0AR;J)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object v2, v3

    .line 313152
    goto :goto_1

    .line 313153
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 313154
    :cond_c
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, p1, v2

    goto/16 :goto_1
.end method

.method private static a(LX/1mA;LX/0AR;LX/0AR;J)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 313240
    const v0, 0x7fffffff

    .line 313241
    iget-object v2, p0, LX/1mA;->m:LX/0ka;

    invoke-virtual {v2}, LX/0ka;->b()Z

    move-result v2

    .line 313242
    if-nez v2, :cond_3

    .line 313243
    iget-object v0, p0, LX/1mA;->l:LX/0wp;

    iget v0, v0, LX/0wp;->r:I

    .line 313244
    :cond_0
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-nez v3, :cond_1

    iget-object v3, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v3}, LX/1m6;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-ne v3, v4, :cond_1

    .line 313245
    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1mA;->l:LX/0wp;

    iget v2, v2, LX/0wp;->C:I

    .line 313246
    :goto_1
    if-lez v2, :cond_1

    iget-object v3, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v3}, LX/1m6;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 313247
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 313248
    :cond_1
    invoke-static {p0, p1}, LX/1mA;->a(LX/1mA;LX/0AR;)I

    move-result v2

    if-gt v2, v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    .line 313249
    :cond_3
    iget-object v3, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v3}, LX/1m6;->b()LX/04G;

    move-result-object v3

    sget-object v4, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    if-eq v3, v4, :cond_0

    .line 313250
    if-nez p2, :cond_4

    move v0, v1

    .line 313251
    :goto_2
    iget-object v3, p0, LX/1mA;->l:LX/0wp;

    iget v3, v3, LX/0wp;->q:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 313252
    :cond_4
    invoke-static {p0, p2}, LX/1mA;->a(LX/1mA;LX/0AR;)I

    move-result v0

    goto :goto_2

    .line 313253
    :cond_5
    iget-object v2, p0, LX/1mA;->l:LX/0wp;

    iget v2, v2, LX/0wp;->D:I

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ln;ZLX/0Lq;)LX/0AR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ln;",
            "Z",
            "LX/0Lq;",
            ")",
            "LX/0AR;"
        }
    .end annotation

    .prologue
    .line 313207
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;I)LX/0Ah;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0Ah;",
            ">;I)",
            "LX/0Ah;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 313194
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 313195
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 313196
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 313197
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 313198
    goto :goto_0

    .line 313199
    :cond_1
    iget-object v0, p0, LX/1mA;->l:LX/0wp;

    iget-object v0, v0, LX/0wp;->O:LX/0xt;

    invoke-static {p0, v0}, LX/1mA;->a(LX/1mA;LX/0xt;)J

    move-result-wide v0

    iget v3, p0, LX/1mA;->i:F

    invoke-static {p0, v0, v1, v3}, LX/1mA;->a(LX/1mA;JF)J

    move-result-wide v6

    move-object v3, v4

    move-object v1, v4

    .line 313200
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 313201
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ah;

    .line 313202
    if-eqz v3, :cond_2

    iget-object v5, v3, LX/0Ah;->c:LX/0AR;

    iget v5, v5, LX/0AR;->c:I

    iget-object v8, v0, LX/0Ah;->c:LX/0AR;

    iget v8, v8, LX/0AR;->c:I

    if-le v5, v8, :cond_3

    :cond_2
    move-object v3, v0

    .line 313203
    :cond_3
    iget-object v5, v0, LX/0Ah;->c:LX/0AR;

    iget v5, v5, LX/0AR;->c:I

    int-to-long v8, v5

    cmp-long v5, v8, v6

    if-gtz v5, :cond_8

    if-lez p2, :cond_4

    iget-object v5, v0, LX/0Ah;->c:LX/0AR;

    invoke-static {p0, v5}, LX/1mA;->a(LX/1mA;LX/0AR;)I

    move-result v5

    if-gt v5, p2, :cond_8

    :cond_4
    iget-object v5, v0, LX/0Ah;->c:LX/0AR;

    const-wide/16 v8, 0x0

    invoke-static {p0, v5, v4, v8, v9}, LX/1mA;->a(LX/1mA;LX/0AR;LX/0AR;J)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 313204
    if-eqz v1, :cond_5

    iget-object v5, v1, LX/0Ah;->c:LX/0AR;

    iget v5, v5, LX/0AR;->c:I

    iget-object v8, v0, LX/0Ah;->c:LX/0AR;

    iget v8, v8, LX/0AR;->c:I

    if-ge v5, v8, :cond_8

    .line 313205
    :cond_5
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move-object v1, v0

    goto :goto_2

    .line 313206
    :cond_6
    if-eqz v1, :cond_7

    move-object v0, v1

    goto :goto_1

    :cond_7
    move-object v0, v3

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_3
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 313193
    return-void
.end method

.method public final a(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/0LQ;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    .line 313188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 313189
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0LQ;

    .line 313190
    new-instance v3, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    invoke-direct {v3, v0}, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;-><init>(LX/0LQ;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 313191
    invoke-virtual/range {v0 .. v5}, LX/1mA;->b(Ljava/util/List;J[LX/0AR;LX/0Ld;)V

    .line 313192
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 313187
    return-void
.end method

.method public final b(Ljava/util/List;J[LX/0AR;LX/0Ld;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "LX/0AR;",
            "LX/0Ld;",
            ")V"
        }
    .end annotation

    .prologue
    .line 313155
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v8, 0x0

    .line 313156
    :goto_0
    iget-boolean v2, p0, LX/1mA;->j:Z

    if-eqz v2, :cond_2

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-nez v2, :cond_2

    const/4 v7, 0x1

    .line 313157
    :goto_1
    move-object/from16 v0, p5

    iget-object v4, v0, LX/0Ld;->c:LX/0AR;

    .line 313158
    iget-object v2, p0, LX/1mA;->l:LX/0wp;

    iget-object v2, v2, LX/0wp;->N:LX/0xt;

    invoke-static {p0, v2}, LX/1mA;->a(LX/1mA;LX/0xt;)J

    move-result-wide v5

    move-object v2, p0

    move-object/from16 v3, p4

    invoke-direct/range {v2 .. v9}, LX/1mA;->a([LX/0AR;LX/0AR;JZJ)LX/0AR;

    move-result-object v5

    .line 313159
    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    iget v2, v5, LX/0AR;->c:I

    iget v3, v4, LX/0AR;->c:I

    if-le v2, v3, :cond_3

    const/4 v2, 0x1

    move v6, v2

    .line 313160
    :goto_2
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    iget v2, v5, LX/0AR;->c:I

    iget v3, v4, LX/0AR;->c:I

    if-ge v2, v3, :cond_4

    const/4 v2, 0x1

    .line 313161
    :goto_3
    const/16 v3, 0xa

    new-array v7, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v7, v3

    const/4 v3, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v7, v3

    const/4 v10, 0x2

    if-nez v5, :cond_5

    const/4 v3, -0x1

    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v10

    const/4 v10, 0x3

    if-nez v5, :cond_6

    const/4 v3, -0x1

    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v10

    const/4 v10, 0x4

    if-nez v4, :cond_7

    const/4 v3, -0x1

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v10

    const/4 v3, 0x5

    const-wide/16 v10, 0x3e8

    div-long v10, v8, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v3

    const/4 v3, 0x6

    iget-object v10, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v10}, LX/1m6;->b()LX/04G;

    move-result-object v10

    iget-object v10, v10, LX/04G;->value:Ljava/lang/String;

    aput-object v10, v7, v3

    const/4 v3, 0x7

    const-wide/16 v10, 0x3e8

    div-long v10, p2, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v3

    const/16 v3, 0x8

    sget-object v10, LX/0xt;->BANDWIDTH_METER:LX/0xt;

    invoke-static {p0, v10}, LX/1mA;->a(LX/1mA;LX/0xt;)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v3

    const/16 v3, 0x9

    sget-object v10, LX/0xt;->DATA_CONNECTION_MANAGER:LX/0xt;

    invoke-static {p0, v10}, LX/1mA;->a(LX/1mA;LX/0xt;)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v7, v3

    .line 313162
    if-eqz v6, :cond_b

    .line 313163
    iget-wide v2, p0, LX/1mA;->e:J

    cmp-long v2, v8, v2

    if-gez v2, :cond_8

    move-object v2, v4

    .line 313164
    :goto_7
    if-eqz v4, :cond_0

    if-eq v2, v4, :cond_0

    .line 313165
    const/4 v3, 0x3

    move-object/from16 v0, p5

    iput v3, v0, LX/0Ld;->b:I

    .line 313166
    :cond_0
    move-object/from16 v0, p5

    iput-object v2, v0, LX/0Ld;->c:LX/0AR;

    .line 313167
    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    move-object/from16 v0, p5

    iget v4, v0, LX/0Ld;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    if-nez v2, :cond_c

    const-string v2, "null"

    :goto_8
    aput-object v2, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    if-nez v2, :cond_d

    const/4 v2, -0x1

    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    .line 313168
    iget-object v2, p0, LX/1mA;->k:LX/1m6;

    invoke-virtual {v2}, LX/1m6;->b()LX/04G;

    move-result-object v2

    iput-object v2, p0, LX/1mA;->q:LX/04G;

    .line 313169
    return-void

    .line 313170
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    iget-wide v2, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->d:J

    sub-long v8, v2, p2

    goto/16 :goto_0

    .line 313171
    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 313172
    :cond_3
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_2

    .line 313173
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 313174
    :cond_5
    iget v3, v5, LX/0AR;->c:I

    div-int/lit16 v3, v3, 0x3e8

    goto/16 :goto_4

    :cond_6
    iget v3, v5, LX/0AR;->f:I

    goto/16 :goto_5

    :cond_7
    iget v3, v4, LX/0AR;->c:I

    div-int/lit16 v3, v3, 0x3e8

    goto/16 :goto_6

    .line 313175
    :cond_8
    iget-wide v2, p0, LX/1mA;->g:J

    cmp-long v2, v8, v2

    if-ltz v2, :cond_e

    .line 313176
    const/4 v2, 0x1

    move v3, v2

    :goto_a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_a

    .line 313177
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;

    .line 313178
    iget-wide v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->c:J

    sub-long v6, v6, p2

    .line 313179
    iget-wide v8, p0, LX/1mA;->g:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_9

    iget-object v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v6, v6, LX/0AR;->c:I

    iget v7, v5, LX/0AR;->c:I

    if-ge v6, v7, :cond_9

    iget-object v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v6, v6, LX/0AR;->g:I

    iget v7, v5, LX/0AR;->g:I

    if-ge v6, v7, :cond_9

    iget-object v6, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v6, v6, LX/0AR;->g:I

    const/16 v7, 0x2d0

    if-ge v6, v7, :cond_9

    iget-object v2, v2, Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;->b:Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;

    iget v2, v2, LX/0AR;->f:I

    const/16 v6, 0x500

    if-ge v2, v6, :cond_9

    .line 313180
    move-object/from16 v0, p5

    iput v3, v0, LX/0Ld;->a:I

    move-object v2, v5

    .line 313181
    goto/16 :goto_7

    .line 313182
    :cond_9
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_a

    :cond_a
    move-object v2, v5

    .line 313183
    goto/16 :goto_7

    .line 313184
    :cond_b
    if-eqz v2, :cond_e

    if-eqz v4, :cond_e

    iget-wide v2, p0, LX/1mA;->f:J

    cmp-long v2, v8, v2

    if-ltz v2, :cond_e

    move-object v2, v4

    .line 313185
    goto/16 :goto_7

    .line 313186
    :cond_c
    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    iget-object v2, v2, LX/0AR;->a:Ljava/lang/String;

    goto/16 :goto_8

    :cond_d
    move-object/from16 v0, p5

    iget-object v2, v0, LX/0Ld;->c:LX/0AR;

    iget v2, v2, LX/0AR;->c:I

    div-int/lit16 v2, v2, 0x3e8

    goto/16 :goto_9

    :cond_e
    move-object v2, v5

    goto/16 :goto_7
.end method
