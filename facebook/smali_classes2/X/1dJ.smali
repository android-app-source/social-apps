.class public LX/1dJ;
.super LX/1cv;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field private e:Landroid/graphics/Rect;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 284990
    invoke-direct {p0}, LX/1cv;-><init>()V

    .line 284991
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final D()Z
    .locals 1

    .prologue
    .line 284992
    iget-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dL;Landroid/graphics/Rect;)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "LX/1dL;",
            "Landroid/graphics/Rect;",
            ")V"
        }
    .end annotation

    .prologue
    .line 284993
    invoke-virtual/range {p4 .. p4}, LX/1dK;->i()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, LX/1dK;->j()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual/range {p4 .. p4}, LX/1dK;->k()Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual/range {p4 .. p4}, LX/1dK;->f()LX/1dQ;

    move-result-object v7

    invoke-virtual/range {p4 .. p4}, LX/1dK;->g()LX/1dQ;

    move-result-object v8

    invoke-virtual/range {p4 .. p4}, LX/1dK;->h()LX/1dQ;

    move-result-object v9

    invoke-virtual/range {p4 .. p4}, LX/1dK;->n()LX/1dQ;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, LX/1dK;->o()LX/1dQ;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, LX/1dK;->p()LX/1dQ;

    move-result-object v12

    invoke-virtual/range {p4 .. p4}, LX/1dK;->q()LX/1dQ;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, LX/1dK;->r()LX/1dQ;

    move-result-object v14

    invoke-virtual/range {p4 .. p4}, LX/1dK;->s()LX/1dQ;

    move-result-object v15

    invoke-virtual/range {p4 .. p4}, LX/1dK;->t()LX/1dQ;

    move-result-object v16

    invoke-virtual/range {p4 .. p4}, LX/1dK;->u()LX/1dQ;

    move-result-object v17

    invoke-virtual/range {p4 .. p4}, LX/1dK;->c()I

    move-result v18

    invoke-virtual/range {p4 .. p4}, LX/1dK;->m()I

    move-result v19

    invoke-virtual/range {p4 .. p4}, LX/1dL;->A()I

    move-result v20

    invoke-virtual/range {p4 .. p4}, LX/1dL;->z()Landroid/graphics/Rect;

    move-result-object v21

    invoke-virtual/range {p4 .. p4}, LX/1dL;->x()LX/1dc;

    move-result-object v22

    invoke-virtual/range {p4 .. p4}, LX/1dL;->y()LX/1dc;

    move-result-object v23

    invoke-virtual/range {p4 .. p4}, LX/1dL;->B()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v24, p5

    invoke-virtual/range {v0 .. v25}, LX/1dJ;->a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;IIILandroid/graphics/Rect;LX/1dc;LX/1dc;Landroid/graphics/Rect;Ljava/lang/String;)V

    .line 284994
    return-void
.end method

.method public final a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;IIILandroid/graphics/Rect;LX/1dc;LX/1dc;Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Object;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;III",
            "Landroid/graphics/Rect;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/graphics/Rect;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 284995
    const/16 v20, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    move/from16 v21, p18

    move/from16 v22, p19

    invoke-super/range {v2 .. v22}, LX/1cv;->a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1cz;II)V

    .line 284996
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1dJ;->a:Landroid/graphics/Rect;

    move-object/from16 v0, p21

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 284997
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dJ;->b:LX/1dc;

    .line 284998
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dJ;->c:LX/1dc;

    .line 284999
    move/from16 v0, p20

    move-object/from16 v1, p0

    iput v0, v1, LX/1dJ;->d:I

    .line 285000
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1dJ;->f:Ljava/lang/String;

    .line 285001
    if-eqz p24, :cond_0

    invoke-virtual/range {p24 .. p24}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 285002
    invoke-static {}, LX/1cy;->n()Landroid/graphics/Rect;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/1dJ;->e:Landroid/graphics/Rect;

    .line 285003
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1dJ;->e:Landroid/graphics/Rect;

    move-object/from16 v0, p24

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 285004
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 285005
    invoke-super {p0, p1}, LX/1cv;->a(Landroid/content/Context;)V

    .line 285006
    iget-object v0, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 285007
    iput-object v1, p0, LX/1dJ;->b:LX/1dc;

    .line 285008
    iput-object v1, p0, LX/1dJ;->c:LX/1dc;

    .line 285009
    const/4 v0, 0x0

    iput v0, p0, LX/1dJ;->d:I

    .line 285010
    iput-object v1, p0, LX/1dJ;->f:Ljava/lang/String;

    .line 285011
    iget-object v0, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 285012
    iget-object v0, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    invoke-static {v0}, LX/1cy;->a(Landroid/graphics/Rect;)V

    .line 285013
    iput-object v1, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    .line 285014
    :cond_0
    return-void
.end method

.method public final t()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 285015
    iget-object v0, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285016
    :cond_0
    const/4 v0, 0x0

    .line 285017
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/1dJ;->e:Landroid/graphics/Rect;

    goto :goto_0
.end method
