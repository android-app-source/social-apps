.class public abstract LX/1a1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/view/View;

.field public b:I

.field public c:I

.field public d:J

.field public e:I

.field public f:I

.field public g:LX/1a1;

.field public h:LX/1a1;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public k:Landroid/support/v7/widget/RecyclerView;

.field public l:I

.field private n:I

.field public o:LX/1Od;

.field private p:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275862
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    sput-object v0, LX/1a1;->m:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 275794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275795
    iput v3, p0, LX/1a1;->b:I

    .line 275796
    iput v3, p0, LX/1a1;->c:I

    .line 275797
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1a1;->d:J

    .line 275798
    iput v3, p0, LX/1a1;->e:I

    .line 275799
    iput v3, p0, LX/1a1;->f:I

    .line 275800
    iput-object v2, p0, LX/1a1;->g:LX/1a1;

    .line 275801
    iput-object v2, p0, LX/1a1;->h:LX/1a1;

    .line 275802
    iput-object v2, p0, LX/1a1;->i:Ljava/util/List;

    .line 275803
    iput-object v2, p0, LX/1a1;->j:Ljava/util/List;

    .line 275804
    iput v4, p0, LX/1a1;->n:I

    .line 275805
    iput-object v2, p0, LX/1a1;->o:LX/1Od;

    .line 275806
    iput v4, p0, LX/1a1;->p:I

    .line 275807
    if-nez p1, :cond_0

    .line 275808
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "itemView may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275809
    :cond_0
    iput-object p1, p0, LX/1a1;->a:Landroid/view/View;

    .line 275810
    return-void
.end method

.method public static A(LX/1a1;)Z
    .locals 1

    .prologue
    .line 275811
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static B(LX/1a1;)Z
    .locals 1

    .prologue
    .line 275812
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()Z
    .locals 1

    .prologue
    .line 275813
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, 0x200

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1a1;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()V
    .locals 1

    .prologue
    .line 275814
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    if-nez v0, :cond_0

    .line 275815
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1a1;->i:Ljava/util/List;

    .line 275816
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1a1;->j:Ljava/util/List;

    .line 275817
    :cond_0
    return-void
.end method

.method public static y(LX/1a1;)V
    .locals 2

    .prologue
    .line 275818
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/1a1;->p:I

    .line 275819
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 275820
    return-void
.end method

.method public static z(LX/1a1;)V
    .locals 2

    .prologue
    .line 275821
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget v1, p0, LX/1a1;->p:I

    invoke-static {v0, v1}, LX/0vv;->d(Landroid/view/View;I)V

    .line 275822
    const/4 v0, 0x0

    iput v0, p0, LX/1a1;->p:I

    .line 275823
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 275824
    iput v0, p0, LX/1a1;->c:I

    .line 275825
    iput v0, p0, LX/1a1;->f:I

    .line 275826
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 275827
    iget v0, p0, LX/1a1;->l:I

    xor-int/lit8 v1, p2, -0x1

    and-int/2addr v0, v1

    and-int v1, p1, p2

    or-int/2addr v0, v1

    iput v0, p0, LX/1a1;->l:I

    .line 275828
    return-void
.end method

.method public final a(IIZ)V
    .locals 1

    .prologue
    .line 275829
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/1a1;->b(I)V

    .line 275830
    invoke-virtual {p0, p2, p3}, LX/1a1;->a(IZ)V

    .line 275831
    iput p1, p0, LX/1a1;->b:I

    .line 275832
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 275833
    iget v0, p0, LX/1a1;->c:I

    if-ne v0, v1, :cond_0

    .line 275834
    iget v0, p0, LX/1a1;->b:I

    iput v0, p0, LX/1a1;->c:I

    .line 275835
    :cond_0
    iget v0, p0, LX/1a1;->f:I

    if-ne v0, v1, :cond_1

    .line 275836
    iget v0, p0, LX/1a1;->b:I

    iput v0, p0, LX/1a1;->f:I

    .line 275837
    :cond_1
    if-eqz p2, :cond_2

    .line 275838
    iget v0, p0, LX/1a1;->f:I

    add-int/2addr v0, p1

    iput v0, p0, LX/1a1;->f:I

    .line 275839
    :cond_2
    iget v0, p0, LX/1a1;->b:I

    add-int/2addr v0, p1

    iput v0, p0, LX/1a1;->b:I

    .line 275840
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 275841
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1a3;->c:Z

    .line 275842
    :cond_3
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 275843
    if-nez p1, :cond_1

    .line 275844
    const/16 v0, 0x400

    invoke-virtual {p0, v0}, LX/1a1;->b(I)V

    .line 275845
    :cond_0
    :goto_0
    return-void

    .line 275846
    :cond_1
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    .line 275847
    invoke-direct {p0}, LX/1a1;->x()V

    .line 275848
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 275849
    if-eqz p1, :cond_1

    iget v0, p0, LX/1a1;->n:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p0, LX/1a1;->n:I

    .line 275850
    iget v0, p0, LX/1a1;->n:I

    if-gez v0, :cond_2

    .line 275851
    const/4 v0, 0x0

    iput v0, p0, LX/1a1;->n:I

    .line 275852
    const-string v0, "View"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275853
    :cond_0
    :goto_1
    return-void

    .line 275854
    :cond_1
    iget v0, p0, LX/1a1;->n:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275855
    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, LX/1a1;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 275856
    iget v0, p0, LX/1a1;->l:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/1a1;->l:I

    goto :goto_1

    .line 275857
    :cond_3
    if-eqz p1, :cond_0

    iget v0, p0, LX/1a1;->n:I

    if-nez v0, :cond_0

    .line 275858
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, LX/1a1;->l:I

    goto :goto_1
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 275859
    iget v0, p0, LX/1a1;->l:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 275860
    iget v0, p0, LX/1a1;->l:I

    or-int/2addr v0, p1

    iput v0, p0, LX/1a1;->l:I

    .line 275861
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 275789
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 275790
    iget v0, p0, LX/1a1;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/1a1;->b:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/1a1;->f:I

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 275791
    iget-object v0, p0, LX/1a1;->k:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    .line 275792
    const/4 v0, -0x1

    .line 275793
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1a1;->k:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I

    move-result v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 275736
    iget-object v0, p0, LX/1a1;->o:LX/1Od;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 275737
    iget-object v0, p0, LX/1a1;->o:LX/1Od;

    invoke-virtual {v0, p0}, LX/1Od;->b(LX/1a1;)V

    .line 275738
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 275739
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 275740
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, LX/1a1;->l:I

    .line 275741
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 275742
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, LX/1a1;->l:I

    .line 275743
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 275744
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mb_()V
    .locals 2

    .prologue
    .line 275745
    iget v0, p0, LX/1a1;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 275746
    iget v0, p0, LX/1a1;->b:I

    iput v0, p0, LX/1a1;->c:I

    .line 275747
    :cond_0
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 275748
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 275749
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 275750
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 275751
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 275752
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 275753
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 275754
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 275755
    :cond_0
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, LX/1a1;->l:I

    .line 275756
    return-void
.end method

.method public final t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275757
    iget v0, p0, LX/1a1;->l:I

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_2

    .line 275758
    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1a1;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 275759
    :cond_0
    sget-object v0, LX/1a1;->m:Ljava/util/List;

    .line 275760
    :goto_0
    return-object v0

    .line 275761
    :cond_1
    iget-object v0, p0, LX/1a1;->j:Ljava/util/List;

    goto :goto_0

    .line 275762
    :cond_2
    sget-object v0, LX/1a1;->m:Ljava/util/List;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 275763
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolder{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1a1;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/1a1;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", oldPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1a1;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pLpos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1a1;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 275764
    invoke-virtual {p0}, LX/1a1;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " scrap"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275765
    :cond_0
    invoke-virtual {p0}, LX/1a1;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275766
    :cond_1
    invoke-virtual {p0}, LX/1a1;->p()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, " unbound"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275767
    :cond_2
    invoke-virtual {p0}, LX/1a1;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, " update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275768
    :cond_3
    invoke-virtual {p0}, LX/1a1;->q()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, " removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275769
    :cond_4
    invoke-virtual {p0}, LX/1a1;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, " ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275770
    :cond_5
    invoke-virtual {p0}, LX/1a1;->o()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, " changed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275771
    :cond_6
    invoke-virtual {p0}, LX/1a1;->r()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, " tmpDetached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275772
    :cond_7
    invoke-virtual {p0}, LX/1a1;->v()Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " not recyclable("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/1a1;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275773
    :cond_8
    invoke-direct {p0}, LX/1a1;->w()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "undefined adapter position"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275774
    :cond_9
    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_a

    const-string v1, " no parent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275775
    :cond_a
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275776
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 275777
    iput v3, p0, LX/1a1;->l:I

    .line 275778
    iput v2, p0, LX/1a1;->b:I

    .line 275779
    iput v2, p0, LX/1a1;->c:I

    .line 275780
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1a1;->d:J

    .line 275781
    iput v2, p0, LX/1a1;->f:I

    .line 275782
    iput v3, p0, LX/1a1;->n:I

    .line 275783
    iput-object v4, p0, LX/1a1;->g:LX/1a1;

    .line 275784
    iput-object v4, p0, LX/1a1;->h:LX/1a1;

    .line 275785
    invoke-virtual {p0}, LX/1a1;->s()V

    .line 275786
    iput v3, p0, LX/1a1;->p:I

    .line 275787
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 275788
    iget v0, p0, LX/1a1;->l:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
