.class public final LX/0nP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _cachedDeserializers:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public final _incompleteDeserializers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 135298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135299
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x40

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    iput-object v0, p0, LX/0nP;->_cachedDeserializers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 135300
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    .line 135301
    return-void
.end method

.method private static a(LX/0n3;LX/0lO;LX/0lJ;)LX/0lJ;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 135261
    invoke-virtual {p0}, LX/0n3;->f()LX/0lU;

    move-result-object v4

    .line 135262
    invoke-virtual {v4, p1}, LX/0lU;->r(LX/0lO;)Ljava/lang/Class;

    move-result-object v1

    .line 135263
    if-eqz v1, :cond_7

    .line 135264
    :try_start_0
    invoke-virtual {p2, v1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 135265
    :goto_0
    invoke-virtual {v2}, LX/0lJ;->l()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135266
    invoke-virtual {v4, p1}, LX/0lU;->s(LX/0lO;)Ljava/lang/Class;

    move-result-object v5

    .line 135267
    if-eqz v5, :cond_5

    .line 135268
    instance-of v1, v2, LX/1Xo;

    if-nez v1, :cond_0

    .line 135269
    new-instance v1, LX/28E;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Illegal key-type annotation: type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a Map(-like) type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135270
    :catch_0
    move-exception v2

    .line 135271
    new-instance v4, LX/28E;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to narrow type "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with concrete-type annotation (value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "), method \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\': "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v3, v2}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v4

    .line 135272
    :cond_0
    :try_start_1
    move-object v0, v2

    check-cast v0, LX/1Xo;

    move-object v1, v0

    invoke-virtual {v1, v5}, LX/1Xo;->h(Ljava/lang/Class;)LX/0lJ;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 135273
    :goto_1
    invoke-virtual {v1}, LX/0lJ;->q()LX/0lJ;

    move-result-object v2

    .line 135274
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 135275
    invoke-virtual {v4, p1}, LX/0lU;->p(LX/0lO;)Ljava/lang/Object;

    move-result-object v2

    .line 135276
    if-eqz v2, :cond_1

    .line 135277
    invoke-virtual {p0, p1, v2}, LX/0n3;->c(LX/0lO;Ljava/lang/Object;)LX/1Xt;

    move-result-object v2

    .line 135278
    if-eqz v2, :cond_1

    .line 135279
    check-cast v1, LX/1Xo;

    invoke-virtual {v1, v2}, LX/1Xo;->i(Ljava/lang/Object;)LX/1Xo;

    move-result-object v1

    .line 135280
    :cond_1
    invoke-virtual {v4, p1}, LX/0lU;->t(LX/0lO;)Ljava/lang/Class;

    move-result-object v2

    .line 135281
    if-eqz v2, :cond_2

    .line 135282
    :try_start_2
    invoke-virtual {v1, v2}, LX/0lJ;->e(Ljava/lang/Class;)LX/0lJ;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 135283
    :cond_2
    invoke-virtual {v1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v2

    .line 135284
    invoke-virtual {v2}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    .line 135285
    invoke-virtual {v4, p1}, LX/0lU;->q(LX/0lO;)Ljava/lang/Object;

    move-result-object v2

    .line 135286
    if-eqz v2, :cond_3

    .line 135287
    instance-of v4, v2, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-nez v4, :cond_4

    .line 135288
    const-string v4, "findContentDeserializer"

    const-class v5, Lcom/fasterxml/jackson/databind/JsonDeserializer$None;

    invoke-static {v2, v4, v5}, LX/0nP;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 135289
    if-eqz v2, :cond_4

    .line 135290
    invoke-virtual {p0, p1, v2}, LX/0n3;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 135291
    :goto_2
    if-eqz v2, :cond_3

    .line 135292
    invoke-virtual {v1, v2}, LX/0lJ;->d(Ljava/lang/Object;)LX/0lJ;

    move-result-object v1

    .line 135293
    :cond_3
    :goto_3
    return-object v1

    .line 135294
    :catch_1
    move-exception v1

    .line 135295
    new-instance v4, LX/28E;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to narrow key type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " with key-type annotation ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "): "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v3, v1}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v4

    .line 135296
    :catch_2
    move-exception v4

    .line 135297
    new-instance v5, LX/28E;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to narrow content type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " with content-type annotation ("

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1, v3, v4}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v5

    :cond_4
    move-object v2, v3

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto/16 :goto_1

    :cond_6
    move-object v1, v2

    goto :goto_3

    :cond_7
    move-object v2, p2

    goto/16 :goto_0
.end method

.method private a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135258
    if-nez p1, :cond_0

    .line 135259
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null JavaType passed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135260
    :cond_0
    iget-object v0, p0, LX/0nP;->_cachedDeserializers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    return-object v0
.end method

.method private a(LX/0n3;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0lO;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135252
    invoke-virtual {p1}, LX/0n3;->f()LX/0lU;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0lU;->o(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135253
    if-nez v0, :cond_0

    .line 135254
    const/4 v0, 0x0

    .line 135255
    :goto_0
    return-object v0

    .line 135256
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/0n3;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 135257
    invoke-static {p1, p2, v0}, LX/0nP;->a(LX/0n3;LX/0lO;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/0n3;LX/0lO;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0lO;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135141
    invoke-static {p0, p1}, LX/0nP;->b(LX/0n3;LX/0lO;)LX/1Xr;

    move-result-object v1

    .line 135142
    if-nez v1, :cond_0

    .line 135143
    :goto_0
    return-object p2

    .line 135144
    :cond_0
    invoke-interface {v1}, LX/1Xr;->b()LX/0lJ;

    move-result-object v2

    .line 135145
    new-instance v0, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;

    invoke-direct {v0, v1, v2, p2}, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    move-object p2, v0

    goto :goto_0
.end method

.method private static a(LX/0n3;LX/0n6;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0n6;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135220
    iget-object v0, p0, LX/0n3;->_config:LX/0mu;

    move-object v0, v0

    .line 135221
    invoke-virtual {p2}, LX/0lJ;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135222
    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 135223
    :goto_0
    return-object v0

    .line 135224
    :cond_0
    invoke-virtual {p2}, LX/0lJ;->l()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135225
    invoke-virtual {p2}, LX/0lJ;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135226
    check-cast p2, LX/4ra;

    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/4ra;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135227
    :cond_1
    invoke-virtual {p2}, LX/0lJ;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 135228
    check-cast p2, LX/1Xo;

    .line 135229
    invoke-virtual {p2}, LX/1Xo;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135230
    check-cast p2, LX/1Xn;

    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/1Xn;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135231
    :cond_2
    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/1Xo;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135232
    :cond_3
    invoke-virtual {p2}, LX/0lJ;->m()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135233
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, LX/0lS;->a(LX/4pN;)LX/4pN;

    move-result-object v1

    .line 135234
    if-eqz v1, :cond_4

    .line 135235
    iget-object v2, v1, LX/4pN;->b:LX/2BA;

    move-object v1, v2

    .line 135236
    sget-object v2, LX/2BA;->OBJECT:LX/2BA;

    if-eq v1, v2, :cond_6

    .line 135237
    :cond_4
    check-cast p2, LX/268;

    .line 135238
    invoke-virtual {p2}, LX/268;->x()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 135239
    check-cast p2, LX/267;

    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/267;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135240
    :cond_5
    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->a(LX/0n3;LX/268;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135241
    :cond_6
    const-class v1, LX/0lF;

    .line 135242
    iget-object v2, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 135243
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 135244
    invoke-virtual {p1, v0, p2, p3}, LX/0n6;->a(LX/0mu;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135245
    :cond_7
    invoke-virtual {p1, p0, p2, p3}, LX/0n6;->c(LX/0n3;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135213
    if-nez p0, :cond_1

    move-object p0, v0

    .line 135214
    :cond_0
    :goto_0
    return-object p0

    .line 135215
    :cond_1
    instance-of v1, p0, Ljava/lang/Class;

    if-nez v1, :cond_2

    .line 135216
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "() returned value of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": expected type JsonSerializer or Class<JsonSerializer> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135217
    :cond_2
    check-cast p0, Ljava/lang/Class;

    .line 135218
    if-eq p0, p2, :cond_3

    const-class v1, LX/1Xp;

    if-ne p0, v1, :cond_0

    :cond_3
    move-object p0, v0

    .line 135219
    goto :goto_0
.end method

.method private static b(LX/0n3;LX/0lO;)LX/1Xr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0lO;",
            ")",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135209
    invoke-virtual {p0}, LX/0n3;->f()LX/0lU;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0lU;->u(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135210
    if-nez v0, :cond_0

    .line 135211
    const/4 v0, 0x0

    .line 135212
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0mz;->a(LX/0lO;Ljava/lang/Object;)LX/1Xr;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(LX/0n3;LX/0n6;LX/0lJ;)LX/1Xt;
    .locals 2

    .prologue
    .line 135246
    invoke-virtual {p1, p0, p2}, LX/0n6;->a(LX/0n3;LX/0lJ;)LX/1Xt;

    move-result-object v1

    .line 135247
    if-nez v1, :cond_1

    .line 135248
    invoke-static {p2}, LX/0nP;->c(LX/0lJ;)LX/1Xt;

    move-result-object v1

    .line 135249
    :cond_0
    :goto_0
    return-object v1

    .line 135250
    :cond_1
    instance-of v0, v1, LX/1Xx;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 135251
    check-cast v0, LX/1Xx;

    invoke-interface {v0, p0}, LX/1Xx;->a(LX/0n3;)V

    goto :goto_0
.end method

.method private static b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135205
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135206
    invoke-static {v0}, LX/1Xw;->d(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135207
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not find a Value deserializer for abstract type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135208
    :cond_0
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not find a Value deserializer for type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(LX/0lJ;)LX/1Xt;
    .locals 3

    .prologue
    .line 135204
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not find a (Map) Key deserializer for type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0n6;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135188
    iget-object v1, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    monitor-enter v1

    .line 135189
    :try_start_0
    invoke-direct {p0, p3}, LX/0nP;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 135190
    if-eqz v0, :cond_0

    .line 135191
    monitor-exit v1

    .line 135192
    :goto_0
    return-object v0

    .line 135193
    :cond_0
    iget-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    .line 135194
    if-lez v2, :cond_1

    .line 135195
    iget-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 135196
    if-eqz v0, :cond_1

    .line 135197
    monitor-exit v1

    goto :goto_0

    .line 135198
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 135199
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/0nP;->d(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 135200
    if-nez v2, :cond_2

    iget-object v2, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 135201
    iget-object v2, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 135202
    :catchall_1
    move-exception v0

    if-nez v2, :cond_3

    iget-object v2, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 135203
    iget-object v2, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    :cond_3
    throw v0
.end method

.method private d(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0n6;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135175
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LX/0nP;->e(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 135176
    if-nez v1, :cond_1

    move-object v1, v0

    .line 135177
    :cond_0
    :goto_0
    return-object v1

    .line 135178
    :catch_0
    move-exception v1

    .line 135179
    new-instance v2, LX/28E;

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v2

    .line 135180
    :cond_1
    instance-of v0, v1, LX/1Xx;

    .line 135181
    invoke-virtual {v1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->isCachable()Z

    move-result v2

    .line 135182
    if-eqz v0, :cond_2

    .line 135183
    iget-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v0, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 135184
    check-cast v0, LX/1Xx;

    invoke-interface {v0, p1}, LX/1Xx;->a(LX/0n3;)V

    .line 135185
    iget-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135186
    :cond_2
    if-eqz v2, :cond_0

    .line 135187
    iget-object v0, p0, LX/0nP;->_cachedDeserializers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private e(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0n6;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135154
    iget-object v0, p1, LX/0n3;->_config:LX/0mu;

    move-object v2, v0

    .line 135155
    invoke-virtual {p3}, LX/0lJ;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, LX/0lJ;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, LX/0lJ;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135156
    :cond_0
    invoke-virtual {p2, v2, p3}, LX/0n6;->a(LX/0mu;LX/0lJ;)LX/0lJ;

    move-result-object p3

    .line 135157
    :cond_1
    invoke-virtual {v2, p3}, LX/0mu;->b(LX/0lJ;)LX/0lS;

    move-result-object v0

    .line 135158
    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    invoke-direct {p0, p1, v1}, LX/0nP;->a(LX/0n3;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 135159
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 135160
    :goto_0
    return-object v0

    .line 135161
    :cond_2
    invoke-virtual {v0}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    invoke-static {p1, v1, p3}, LX/0nP;->a(LX/0n3;LX/0lO;LX/0lJ;)LX/0lJ;

    move-result-object v1

    .line 135162
    if-eq v1, p3, :cond_3

    .line 135163
    invoke-virtual {v2, v1}, LX/0mu;->b(LX/0lJ;)LX/0lS;

    move-result-object v0

    move-object p3, v1

    .line 135164
    :cond_3
    invoke-virtual {v0}, LX/0lS;->t()Ljava/lang/Class;

    move-result-object v1

    .line 135165
    if-eqz v1, :cond_4

    .line 135166
    invoke-virtual {p2, p1, p3, v1}, LX/0n6;->a(LX/0n3;LX/0lJ;Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135167
    :cond_4
    invoke-virtual {v0}, LX/0lS;->r()LX/1Xr;

    move-result-object v3

    .line 135168
    if-nez v3, :cond_5

    .line 135169
    invoke-static {p1, p2, p3, v0}, LX/0nP;->a(LX/0n3;LX/0n6;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0

    .line 135170
    :cond_5
    invoke-interface {v3}, LX/1Xr;->b()LX/0lJ;

    move-result-object v4

    .line 135171
    iget-object v1, p3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 135172
    invoke-virtual {v4, v1}, LX/0lJ;->g(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 135173
    invoke-virtual {v2, v4}, LX/0mu;->b(LX/0lJ;)LX/0lS;

    move-result-object v0

    .line 135174
    :cond_6
    new-instance v1, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;

    invoke-static {p1, p2, v4, v0}, LX/0nP;->a(LX/0n3;LX/0n6;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    invoke-direct {v1, v3, v4, v0}, Lcom/fasterxml/jackson/databind/deser/std/StdDelegatingDeserializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0n3;",
            "LX/0n6;",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135148
    invoke-direct {p0, p3}, LX/0nP;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 135149
    if-eqz v0, :cond_1

    .line 135150
    :cond_0
    :goto_0
    return-object v0

    .line 135151
    :cond_1
    invoke-direct {p0, p1, p2, p3}, LX/0nP;->c(LX/0n3;LX/0n6;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 135152
    if-nez v0, :cond_0

    .line 135153
    invoke-static {p3}, LX/0nP;->b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135146
    iget-object v0, p0, LX/0nP;->_incompleteDeserializers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 135147
    return-object p0
.end method
