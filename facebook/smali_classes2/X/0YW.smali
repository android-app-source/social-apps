.class public final LX/0YW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YX;


# instance fields
.field public final synthetic a:LX/0Xk;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0YZ;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/IntentFilter;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/0Xk;)V
    .locals 1

    .prologue
    .line 81726
    iput-object p1, p0, LX/0YW;->a:LX/0Xk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81727
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0YW;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/IntentFilter;)LX/0YX;
    .locals 0

    .prologue
    .line 81728
    iput-object p1, p0, LX/0YW;->c:Landroid/content/IntentFilter;

    .line 81729
    return-object p0
.end method

.method public final a(Landroid/os/Handler;)LX/0YX;
    .locals 0

    .prologue
    .line 81730
    iput-object p1, p0, LX/0YW;->d:Landroid/os/Handler;

    .line 81731
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/0YZ;)LX/0YX;
    .locals 1

    .prologue
    .line 81732
    iget-object v0, p0, LX/0YW;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81733
    return-object p0
.end method

.method public final a()LX/0Yb;
    .locals 5

    .prologue
    .line 81734
    new-instance v1, LX/0Yb;

    iget-object v0, p0, LX/0YW;->a:LX/0Xk;

    iget-object v2, p0, LX/0YW;->b:Ljava/util/Map;

    iget-object v3, p0, LX/0YW;->c:Landroid/content/IntentFilter;

    iget-object v4, p0, LX/0YW;->d:Landroid/os/Handler;

    invoke-direct {v1, v0, v2, v3, v4}, LX/0Yb;-><init>(LX/0Xk;Ljava/util/Map;Landroid/content/IntentFilter;Landroid/os/Handler;)V

    .line 81735
    iget-object v0, p0, LX/0YW;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YZ;

    .line 81736
    instance-of v3, v0, LX/0Yg;

    if-eqz v3, :cond_0

    .line 81737
    check-cast v0, LX/0Yg;

    .line 81738
    iput-object v1, v0, LX/0Yg;->a:LX/0Yb;

    .line 81739
    goto :goto_0

    .line 81740
    :cond_1
    return-object v1
.end method
