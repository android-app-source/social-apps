.class public final LX/1Ms;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;)V
    .locals 0

    .prologue
    .line 236361
    iput-object p1, p0, LX/1Ms;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 236362
    iget-object v0, p0, LX/1Ms;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    const/4 v1, 0x0

    .line 236363
    iput-boolean v1, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->h:Z

    .line 236364
    iget-object v0, p0, LX/1Ms;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    iget-object v0, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->g:LX/03V;

    const-string v1, "composer_session_delete_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 236365
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 236366
    iget-object v0, p0, LX/1Ms;->a:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    const/4 v1, 0x0

    .line 236367
    iput-boolean v1, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->h:Z

    .line 236368
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 236369
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/1Ms;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
