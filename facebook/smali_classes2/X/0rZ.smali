.class public LX/0rZ;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0rd;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0rd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 149792
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0rd;
    .locals 5

    .prologue
    .line 149793
    sget-object v0, LX/0rZ;->a:LX/0rd;

    if-nez v0, :cond_1

    .line 149794
    const-class v1, LX/0rZ;

    monitor-enter v1

    .line 149795
    :try_start_0
    sget-object v0, LX/0rZ;->a:LX/0rd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 149796
    if-eqz v2, :cond_0

    .line 149797
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 149798
    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v3

    check-cast v3, LX/0pi;

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v4

    check-cast v4, LX/0rb;

    invoke-static {v3, p0, v4}, LX/0rc;->a(LX/0pi;LX/0Ot;LX/0rb;)LX/0rd;

    move-result-object v3

    move-object v0, v3

    .line 149799
    sput-object v0, LX/0rZ;->a:LX/0rd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 149801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149802
    :cond_1
    sget-object v0, LX/0rZ;->a:LX/0rd;

    return-object v0

    .line 149803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 149804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 149805
    invoke-static {p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v0

    check-cast v0, LX/0pi;

    const/16 v1, 0x259

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v1

    check-cast v1, LX/0rb;

    invoke-static {v0, v2, v1}, LX/0rc;->a(LX/0pi;LX/0Ot;LX/0rb;)LX/0rd;

    move-result-object v0

    return-object v0
.end method
