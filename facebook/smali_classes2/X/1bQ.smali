.class public LX/1bQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 280541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280542
    iput-object p1, p0, LX/1bQ;->a:Landroid/content/Context;

    .line 280543
    iput-object p2, p0, LX/1bQ;->b:LX/0wM;

    .line 280544
    return-void
.end method

.method public static a(I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 280535
    invoke-static {p0}, LX/1bX;->a(I)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 280536
    iget-object p0, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, p0

    .line 280537
    return-object v0
.end method

.method public static b(LX/0QB;)LX/1bQ;
    .locals 3

    .prologue
    .line 280539
    new-instance v2, LX/1bQ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v1}, LX/1bQ;-><init>(Landroid/content/Context;LX/0wM;)V

    .line 280540
    return-object v2
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 280538
    iget-object v0, p0, LX/1bQ;->b:LX/0wM;

    const v1, 0x7f0207ed

    const v2, -0xa76f01

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
