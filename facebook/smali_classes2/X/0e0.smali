.class public LX/0e0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/content/SecureContextHelper;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile l:LX/0e0;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qt;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation runtime Lcom/facebook/content/ExternalIntentRewriter;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2n1;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation runtime Lcom/facebook/content/InternalIntentRewriter;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2n1;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/48R;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1qx;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/48S;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1v9;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/iface/ActivityChoreographer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91386
    const-class v0, LX/0e0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0e0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/content/ExternalIntentRewriter;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/content/InternalIntentRewriter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/1qt;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2n1;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2n1;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/48R;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1qx;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/48S;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1v9;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/iface/ActivityChoreographer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91375
    iput-object p1, p0, LX/0e0;->b:Ljava/lang/String;

    .line 91376
    iput-object p2, p0, LX/0e0;->c:LX/0Ot;

    .line 91377
    iput-object p3, p0, LX/0e0;->d:LX/0Ot;

    .line 91378
    iput-object p4, p0, LX/0e0;->e:LX/0Ot;

    .line 91379
    iput-object p5, p0, LX/0e0;->f:LX/0Ot;

    .line 91380
    iput-object p6, p0, LX/0e0;->g:LX/0Ot;

    .line 91381
    iput-object p7, p0, LX/0e0;->h:LX/0Ot;

    .line 91382
    iput-object p8, p0, LX/0e0;->i:LX/0Ot;

    .line 91383
    iput-object p9, p0, LX/0e0;->j:LX/0Ot;

    .line 91384
    iput-object p10, p0, LX/0e0;->k:LX/0Ot;

    .line 91385
    return-void
.end method

.method public static a(LX/0QB;)LX/0e0;
    .locals 14

    .prologue
    .line 91352
    sget-object v0, LX/0e0;->l:LX/0e0;

    if-nez v0, :cond_1

    .line 91353
    const-class v1, LX/0e0;

    monitor-enter v1

    .line 91354
    :try_start_0
    sget-object v0, LX/0e0;->l:LX/0e0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91355
    if-eqz v2, :cond_0

    .line 91356
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91357
    new-instance v3, LX/0e0;

    invoke-static {v0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v5, 0x459

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 91358
    new-instance v7, LX/0e1;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    invoke-direct {v7, v8}, LX/0e1;-><init>(LX/0QB;)V

    move-object v7, v7

    .line 91359
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    invoke-static {v7, v8}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v7

    move-object v7, v7

    .line 91360
    new-instance v8, LX/0e2;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v9

    invoke-direct {v8, v9}, LX/0e2;-><init>(LX/0QB;)V

    move-object v8, v8

    .line 91361
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v9

    invoke-static {v8, v9}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v8

    move-object v8, v8

    .line 91362
    new-instance v9, LX/0e3;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    invoke-direct {v9, v10}, LX/0e3;-><init>(LX/0QB;)V

    move-object v9, v9

    .line 91363
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    invoke-static {v9, v10}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v9

    move-object v9, v9

    .line 91364
    new-instance v10, LX/0e4;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v11

    invoke-direct {v10, v11}, LX/0e4;-><init>(LX/0QB;)V

    move-object v10, v10

    .line 91365
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v11

    invoke-static {v10, v11}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v10

    move-object v10, v10

    .line 91366
    const/16 v11, 0x1a33

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x454

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x239

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/0e0;-><init>(Ljava/lang/String;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 91367
    move-object v0, v3

    .line 91368
    sput-object v0, LX/0e0;->l:LX/0e0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91369
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91370
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91371
    :cond_1
    sget-object v0, LX/0e0;->l:LX/0e0;

    return-object v0

    .line 91372
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 91345
    const/16 v0, 0x2710

    .line 91346
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.dexopt-pause"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91347
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    const v2, 0xf4240

    mul-int/2addr v2, v0

    int-to-long v5, v2

    add-long/2addr v3, v5

    .line 91348
    const-string v2, "com.facebook.dexopt-unpause-time"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 91349
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 91350
    const-string v1, "send pause-optimization broadcast"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/02P;->safeFmt(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 91351
    return-void
.end method

.method private d(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 91342
    iget-object v0, p0, LX/0e0;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2n1;

    .line 91343
    invoke-virtual {v0, p1, p2}, LX/2n1;->a(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/Intent;

    goto :goto_0

    .line 91344
    :cond_0
    return-void
.end method

.method private f(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 91339
    iget-object v0, p0, LX/0e0;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2n1;

    .line 91340
    invoke-virtual {v0, p1, p2}, LX/2n1;->a(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/Intent;

    goto :goto_0

    .line 91341
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3qU;)V
    .locals 7

    .prologue
    .line 91387
    const/4 v3, 0x0

    .line 91388
    iget-object v0, p1, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Landroid/content/Intent;

    .line 91389
    array-length v0, v2

    if-nez v0, :cond_2

    move-object v0, v2

    .line 91390
    :goto_0
    move-object v3, v0

    .line 91391
    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 91392
    iget-object v6, p0, LX/0e0;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0e0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qt;

    iget-object v1, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v6, v0, v1, v5}, LX/2A3;->b(Ljava/lang/String;LX/1qt;LX/03V;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91393
    :goto_2
    return-void

    .line 91394
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 91395
    :cond_1
    invoke-virtual {p1}, LX/3qU;->a()V

    goto :goto_2

    .line 91396
    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p1, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v0, 0x1000c000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    .line 91397
    const/4 v0, 0x1

    move v1, v0

    :goto_3
    array-length v0, v2

    if-ge v1, v0, :cond_3

    .line 91398
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p1, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    aput-object v3, v2, v1

    .line 91399
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move-object v0, v2

    .line 91400
    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)V
    .locals 3

    .prologue
    .line 91333
    iget-object v2, p0, LX/0e0;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0e0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qt;

    iget-object v1, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v2, v0, v1, p1}, LX/2A3;->b(Ljava/lang/String;LX/1qt;LX/03V;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91334
    :goto_0
    return-void

    .line 91335
    :cond_0
    invoke-direct {p0, p1, p3}, LX/0e0;->d(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91336
    iget-object v0, p0, LX/0e0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qx;

    .line 91337
    invoke-interface {v0, p1, p2, p3}, LX/1qx;->a(Landroid/content/Intent;ILandroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 91338
    :cond_2
    iget-object v0, p0, LX/0e0;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1v9;

    invoke-virtual {v0, p1, p2, p3}, LX/1v9;->a(Landroid/content/Intent;ILandroid/app/Activity;)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 91327
    iget-object v2, p0, LX/0e0;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0e0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qt;

    iget-object v1, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v2, v0, v1, p1}, LX/2A3;->b(Ljava/lang/String;LX/1qt;LX/03V;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91328
    :goto_0
    return-void

    .line 91329
    :cond_0
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0e0;->d(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91330
    iget-object v0, p0, LX/0e0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qx;

    .line 91331
    invoke-interface {v0, p1, p2, p3}, LX/1qx;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 91332
    :cond_2
    iget-object v0, p0, LX/0e0;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1v9;

    invoke-virtual {v0, p1, p2, p3}, LX/1v9;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 91307
    iget-object v2, p0, LX/0e0;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0e0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qt;

    iget-object v1, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v2, v0, v1, p1}, LX/2A3;->b(Ljava/lang/String;LX/1qt;LX/03V;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91308
    :goto_0
    return-void

    .line 91309
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 91310
    const-class v0, LX/0ey;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91311
    iget-object v0, p0, LX/0e0;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/111;

    .line 91312
    iget-object v3, v0, LX/111;->c:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->c()Z

    move-result v3

    if-nez v3, :cond_4
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91313
    :cond_1
    :goto_1
    invoke-direct {p0, p1, p2}, LX/0e0;->d(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91314
    iget-object v0, p0, LX/0e0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qx;

    .line 91315
    invoke-interface {v0, p1, p2}, LX/1qx;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 91316
    :catch_0
    move-exception v0

    .line 91317
    sget-object v1, LX/0e0;->a:Ljava/lang/String;

    const-string v2, "Unable to track activity launch."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 91318
    :cond_3
    iget-object v0, p0, LX/0e0;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1v9;

    invoke-virtual {v0, p1, p2}, LX/1v9;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    goto :goto_0

    .line 91319
    :cond_4
    invoke-static {v0}, LX/111;->a(LX/111;)V

    .line 91320
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 91321
    iput-object v1, v0, LX/111;->e:Ljava/lang/Class;

    .line 91322
    iget-object v4, v0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v4, :cond_5

    .line 91323
    iget-object v4, v0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const v6, -0x204d008f

    invoke-static {v4, v5, v6}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 91324
    :cond_5
    iput-object v3, v0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    .line 91325
    iget-object v3, v0, LX/111;->b:LX/0Sg;

    iget-object v4, v0, LX/111;->f:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v3, v4}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 91326
    iget-object v3, v0, LX/111;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lcom/facebook/common/appchoreographer/ActivityChoreographerImpl$1;

    invoke-direct {v4, v0, v1}, Lcom/facebook/common/appchoreographer/ActivityChoreographerImpl$1;-><init>(LX/111;Ljava/lang/Class;)V

    const-wide/16 v5, 0x5

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6, v7}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, v0, LX/111;->g:Ljava/util/concurrent/Future;

    goto :goto_1
.end method

.method public final b(Landroid/content/Intent;ILandroid/app/Activity;)V
    .locals 4

    .prologue
    .line 91298
    invoke-static {p3}, LX/0e0;->a(Landroid/content/Context;)V

    .line 91299
    invoke-direct {p0, p1, p3}, LX/0e0;->f(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91300
    iget-object v0, p0, LX/0e0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48R;

    .line 91301
    invoke-interface {v0, p1, p2, p3}, LX/48R;->a(Landroid/content/Intent;ILandroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91302
    :goto_0
    return-void

    .line 91303
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0e0;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48S;

    invoke-virtual {v0, p1, p2, p3}, LX/48S;->a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91304
    :catch_0
    iget-object v0, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ExternalIntentSecurityException"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SecurityException when launching external intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91305
    const/high16 v0, 0x7f080000

    invoke-virtual {p3, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 91306
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 91289
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0e0;->a(Landroid/content/Context;)V

    .line 91290
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0e0;->f(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91291
    iget-object v0, p0, LX/0e0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48R;

    .line 91292
    invoke-interface {v0, p1, p2, p3}, LX/48R;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91293
    :goto_0
    return-void

    .line 91294
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0e0;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48S;

    invoke-virtual {v0, p1, p2, p3}, LX/48S;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91295
    :catch_0
    iget-object v0, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ExternalIntentSecurityException"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SecurityException when launching external intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91296
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {p3, v1}, Landroid/support/v4/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 91297
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 91280
    invoke-static {p2}, LX/0e0;->a(Landroid/content/Context;)V

    .line 91281
    invoke-direct {p0, p1, p2}, LX/0e0;->f(Landroid/content/Intent;Landroid/content/Context;)V

    .line 91282
    iget-object v0, p0, LX/0e0;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48R;

    .line 91283
    invoke-interface {v0, p1, p2}, LX/48R;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91284
    :goto_0
    return-void

    .line 91285
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0e0;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48S;

    invoke-virtual {v0, p1, p2}, LX/48S;->a(Landroid/content/Intent;Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91286
    :catch_0
    iget-object v0, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "ExternalIntentSecurityException"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SecurityException when launching external intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 91287
    const/high16 v0, 0x7f080000

    invoke-virtual {p2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 91288
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 91258
    iget-object v2, p0, LX/0e0;->b:Ljava/lang/String;

    iget-object v0, p0, LX/0e0;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qt;

    iget-object v1, p0, LX/0e0;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    .line 91259
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 91260
    const/4 v3, 0x1

    .line 91261
    :goto_0
    move v0, v3

    .line 91262
    if-nez v0, :cond_0

    .line 91263
    const/4 v0, 0x0

    .line 91264
    :goto_1
    return-object v0

    .line 91265
    :cond_0
    iget-object v0, p0, LX/0e0;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 91266
    :cond_1
    iget-object v0, p0, LX/0e0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qx;

    .line 91267
    invoke-interface {v0, p1, p2}, LX/1qx;->b(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    .line 91268
    if-eqz v0, :cond_2

    goto :goto_1

    .line 91269
    :cond_3
    iget-object v0, p0, LX/0e0;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1v9;

    invoke-virtual {v0, p1, p2}, LX/1v9;->b(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_1

    .line 91270
    :cond_4
    iget-object v3, v0, LX/1qt;->a:Landroid/content/pm/PackageManager;

    iget-object v4, v0, LX/1qt;->b:Landroid/content/pm/ApplicationInfo;

    .line 91271
    const/16 v5, 0x40

    invoke-virtual {v3, p1, v5}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 91272
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 91273
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 91274
    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 91275
    invoke-static {v5, v3, v4}, LX/1qt;->a(Landroid/content/pm/ComponentInfo;Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 91276
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 91277
    :cond_6
    invoke-static {v6}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v5

    move-object v3, v5

    .line 91278
    move-object v3, v3

    .line 91279
    invoke-static {v2, v1, p1, v3}, LX/2A3;->a(Ljava/lang/String;LX/03V;Landroid/content/Intent;Ljava/util/List;)Z

    move-result v3

    goto :goto_0
.end method
