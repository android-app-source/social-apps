.class public LX/0hs;
.super LX/0ht;
.source ""


# static fields
.field public static final a:Landroid/view/View$OnTouchListener;


# instance fields
.field public l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:Lcom/facebook/resources/ui/FbTextView;

.field public o:Landroid/widget/ImageView;

.field public p:Landroid/widget/ImageView;

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:Z

.field private v:Ljava/lang/Runnable;

.field public w:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118790
    new-instance v0, LX/5Oa;

    invoke-direct {v0}, LX/5Oa;-><init>()V

    sput-object v0, LX/0hs;->a:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118788
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 118789
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 118786
    invoke-static {p1, p2}, LX/0hs;->a(Landroid/content/Context;I)I

    move-result v0

    const v1, 0x7f03061c

    invoke-direct {p0, p1, v0, v1}, LX/0hs;-><init>(Landroid/content/Context;II)V

    .line 118787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    .line 118761
    invoke-static {p1, p2}, LX/0hs;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/0ht;-><init>(Landroid/content/Context;I)V

    .line 118762
    new-instance v0, Lcom/facebook/fbui/tooltip/Tooltip$2;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/tooltip/Tooltip$2;-><init>(LX/0hs;)V

    iput-object v0, p0, LX/0hs;->v:Ljava/lang/Runnable;

    .line 118763
    const/4 p2, 0x0

    .line 118764
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 118765
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v1, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/0hs;->w:Landroid/os/Handler;

    .line 118766
    iput-boolean p2, p0, LX/0hs;->u:Z

    .line 118767
    const/16 v1, 0xbb8

    iput v1, p0, LX/0hs;->t:I

    .line 118768
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 118769
    const p1, 0x7f0b0b37

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, LX/0hs;->q:I

    .line 118770
    const p1, 0x7f0b0b38

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, LX/0hs;->r:I

    .line 118771
    const p1, 0x7f0b0b39

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/0hs;->s:I

    .line 118772
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, LX/0ht;->a(Landroid/graphics/drawable/Drawable;)V

    .line 118773
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1, p2, p2, p2, p2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setPadding(IIII)V

    .line 118774
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/0ht;->b(F)V

    .line 118775
    invoke-virtual {p0, p2}, LX/0ht;->d(Z)V

    .line 118776
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    new-instance p1, LX/5Ob;

    invoke-direct {p1, p0}, LX/5Ob;-><init>(LX/0hs;)V

    invoke-virtual {v1, p1}, LX/5OY;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118777
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 118778
    const v0, 0x7f0d0940

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/0hs;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 118779
    const v0, 0x7f0d0941

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/0hs;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 118780
    const v0, 0x7f0d0942

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 118781
    const v0, 0x7f0d0943

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/0hs;->o:Landroid/widget/ImageView;

    .line 118782
    const v0, 0x7f0d0944

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/0hs;->p:Landroid/widget/ImageView;

    .line 118783
    invoke-virtual {p0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 118784
    iget-object v0, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, LX/0hs;->a:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118785
    return-void
.end method

.method private static a(Landroid/content/Context;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 118752
    if-ne p1, v3, :cond_1

    .line 118753
    const p1, 0x7f0e0257

    .line 118754
    :cond_0
    :goto_0
    return p1

    .line 118755
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 118756
    const p1, 0x7f0e0258

    goto :goto_0

    .line 118757
    :cond_2
    const/high16 v0, 0x1000000

    if-ge p1, v0, :cond_0

    .line 118758
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 118759
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010251

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 118760
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118745
    iget-object v0, p0, LX/0hs;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 118746
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118747
    iget-object v0, p0, LX/0hs;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 118748
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118749
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 118750
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118751
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 118741
    iget-object v0, p0, LX/0hs;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 118742
    iget-object v1, p0, LX/0hs;->m:Lcom/facebook/resources/ui/FbTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 118743
    return-void

    .line 118744
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(LX/3AV;)V
    .locals 2

    .prologue
    .line 118737
    sget-object v0, LX/3AV;->CENTER:LX/3AV;

    if-ne p1, v0, :cond_0

    .line 118738
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tooltips should be anchored to a view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118739
    :cond_0
    invoke-super {p0, p1}, LX/0ht;->a(LX/3AV;)V

    .line 118740
    return-void
.end method

.method public final a(LX/5Od;)V
    .locals 2

    .prologue
    .line 118791
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    new-instance v1, LX/5Oc;

    invoke-direct {v1, p0, p1}, LX/5Oc;-><init>(LX/0hs;LX/5Od;)V

    invoke-virtual {v0, v1}, LX/5OY;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118792
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 118735
    invoke-virtual {p0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 118736
    return-void
.end method

.method public a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 15

    .prologue
    .line 118669
    invoke-direct {p0}, LX/0hs;->a()V

    .line 118670
    invoke-virtual {p0}, LX/0ht;->n()I

    move-result v6

    .line 118671
    invoke-virtual {p0}, LX/0ht;->o()I

    move-result v7

    .line 118672
    invoke-virtual {p0}, LX/0ht;->p()I

    move-result v2

    .line 118673
    invoke-virtual {p0}, LX/0ht;->q()I

    move-result v4

    .line 118674
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    .line 118675
    iget v1, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v6

    sub-int/2addr v1, v7

    .line 118676
    iget v3, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v3, v2

    sub-int/2addr v3, v4

    .line 118677
    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 118678
    const/high16 v5, -0x80000000

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 118679
    iget-object v5, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v5, v1, v3}, LX/5OY;->measure(II)V

    .line 118680
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v1}, LX/5OY;->getMeasuredWidth()I

    move-result v9

    .line 118681
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v1}, LX/5OY;->getMeasuredHeight()I

    move-result v5

    .line 118682
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v10

    .line 118683
    move-object/from16 v0, p3

    iput v9, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 118684
    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 118685
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 118686
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 118687
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 118688
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v11

    .line 118689
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v12

    .line 118690
    const/4 v13, 0x0

    aget v13, v3, v13

    iget v14, p0, LX/0ht;->h:I

    add-int/2addr v13, v14

    .line 118691
    const/4 v14, 0x1

    aget v3, v3, v14

    iget v14, p0, LX/0ht;->i:I

    add-int/2addr v14, v3

    .line 118692
    div-int/lit8 v3, v11, 0x2

    add-int v11, v13, v3

    .line 118693
    sub-int v2, v14, v2

    if-gt v5, v2, :cond_2

    const/4 v2, 0x1

    move v3, v2

    .line 118694
    :goto_0
    add-int v2, v14, v12

    add-int/2addr v2, v5

    iget v13, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v4, v13, v4

    if-gt v2, v4, :cond_3

    const/4 v2, 0x1

    .line 118695
    :goto_1
    invoke-virtual {p0}, LX/0ht;->i()LX/3AV;

    move-result-object v4

    .line 118696
    if-eqz v2, :cond_4

    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    if-eq v4, v2, :cond_0

    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    if-ne v4, v2, :cond_4

    if-nez v3, :cond_4

    :cond_0
    const/4 v2, 0x1

    move v4, v2

    .line 118697
    :goto_2
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 118698
    if-eqz v4, :cond_5

    .line 118699
    add-int v3, v14, v12

    iget v4, p0, LX/0hs;->r:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118700
    const/4 v3, 0x0

    .line 118701
    const v4, 0x7f0e028b

    move-object/from16 v0, p3

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118702
    const/16 v4, 0x33

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118703
    iget-object v1, p0, LX/0hs;->o:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118704
    iget-object v1, p0, LX/0hs;->p:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118705
    iget-object v1, p0, LX/0hs;->o:Landroid/widget/ImageView;

    move v4, v3

    move-object v3, v1

    .line 118706
    :goto_3
    div-int/lit8 v1, v9, 0x2

    sub-int v1, v11, v1

    .line 118707
    if-ge v1, v6, :cond_7

    move v5, v6

    .line 118708
    :goto_4
    move-object/from16 v0, p3

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v1, v5

    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 118709
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118710
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118711
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 118712
    iget v2, p0, LX/0hs;->s:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v11, v2

    sub-int/2addr v2, v5

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118713
    iget-object v2, p0, LX/0hs;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingLeft()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 118714
    iget-object v5, p0, LX/0hs;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getPaddingRight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    .line 118715
    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ge v6, v2, :cond_8

    .line 118716
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 118717
    :cond_1
    :goto_5
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118718
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, p0, LX/0hs;->s:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    invoke-virtual {v2, v1, v4}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(II)V

    .line 118719
    :goto_6
    return-void

    .line 118720
    :cond_2
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 118721
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 118722
    :cond_4
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_2

    .line 118723
    :cond_5
    if-eqz v3, :cond_6

    .line 118724
    sub-int v3, v10, v14

    iget v4, p0, LX/0hs;->q:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 118725
    const v3, 0x7f0e028c

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 118726
    const/16 v3, 0x53

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 118727
    iget-object v1, p0, LX/0hs;->o:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118728
    iget-object v1, p0, LX/0hs;->p:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118729
    iget-object v1, p0, LX/0hs;->p:Landroid/widget/ImageView;

    move-object v3, v1

    move v4, v5

    goto/16 :goto_3

    .line 118730
    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p3

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    goto :goto_6

    .line 118731
    :cond_7
    add-int v5, v1, v9

    iget v6, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_9

    .line 118732
    iget v1, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v7

    sub-int/2addr v1, v9

    move v5, v1

    goto/16 :goto_4

    .line 118733
    :cond_8
    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v6, p0, LX/0hs;->s:I

    add-int/2addr v2, v6

    sub-int v6, v9, v5

    if-le v2, v6, :cond_1

    .line 118734
    sub-int v2, v9, v5

    iget v5, p0, LX/0hs;->s:I

    sub-int/2addr v2, v5

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_5

    :cond_9
    move v5, v1

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 118637
    iget-object v0, p0, LX/0hs;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 118638
    iget-object v1, p0, LX/0hs;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 118639
    return-void

    .line 118640
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 118665
    iget-object v0, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 118666
    iget-object v1, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 118667
    return-void

    .line 118668
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 118663
    iget-object v0, p0, LX/0hs;->l:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118664
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 118659
    iget-object v0, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 118660
    iget-object v1, p0, LX/0hs;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 118661
    return-void

    .line 118662
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 118657
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 118658
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 118646
    iget-boolean v0, p0, LX/0ht;->b:Z

    move v0, v0

    .line 118647
    if-nez v0, :cond_1

    const-string v0, "suppress_non_modal_tooltip"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118648
    :cond_0
    :goto_0
    return-void

    .line 118649
    :cond_1
    invoke-super {p0}, LX/0ht;->d()V

    .line 118650
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 118651
    if-nez v0, :cond_0

    .line 118652
    iget-boolean v0, p0, LX/0hs;->u:Z

    if-eqz v0, :cond_2

    .line 118653
    iget-object v0, p0, LX/0hs;->w:Landroid/os/Handler;

    iget-object v1, p0, LX/0hs;->v:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 118654
    :cond_2
    iget v0, p0, LX/0hs;->t:I

    if-lez v0, :cond_0

    .line 118655
    iget-object v0, p0, LX/0hs;->w:Landroid/os/Handler;

    iget-object v1, p0, LX/0hs;->v:Ljava/lang/Runnable;

    iget v2, p0, LX/0hs;->t:I

    int-to-long v2, v2

    const v4, 0x67c3b3a9

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 118656
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0hs;->u:Z

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 118641
    invoke-super {p0}, LX/0ht;->l()V

    .line 118642
    iget-boolean v0, p0, LX/0hs;->u:Z

    if-eqz v0, :cond_0

    .line 118643
    iget-object v0, p0, LX/0hs;->w:Landroid/os/Handler;

    iget-object v1, p0, LX/0hs;->v:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 118644
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0hs;->u:Z

    .line 118645
    :cond_0
    return-void
.end method
