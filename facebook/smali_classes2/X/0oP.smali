.class public LX/0oP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0oP;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/0Vv;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Vv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 140178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140179
    iput-object p1, p0, LX/0oP;->a:Lcom/facebook/content/SecureContextHelper;

    .line 140180
    iput-object p2, p0, LX/0oP;->b:LX/0Vv;

    .line 140181
    return-void
.end method

.method public static a(LX/0QB;)LX/0oP;
    .locals 5

    .prologue
    .line 140182
    sget-object v0, LX/0oP;->c:LX/0oP;

    if-nez v0, :cond_1

    .line 140183
    const-class v1, LX/0oP;

    monitor-enter v1

    .line 140184
    :try_start_0
    sget-object v0, LX/0oP;->c:LX/0oP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 140185
    if-eqz v2, :cond_0

    .line 140186
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 140187
    new-instance p0, LX/0oP;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Vv;->a(LX/0QB;)LX/0Vv;

    move-result-object v4

    check-cast v4, LX/0Vv;

    invoke-direct {p0, v3, v4}, LX/0oP;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Vv;)V

    .line 140188
    move-object v0, p0

    .line 140189
    sput-object v0, LX/0oP;->c:LX/0oP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140190
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 140191
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 140192
    :cond_1
    sget-object v0, LX/0oP;->c:LX/0oP;

    return-object v0

    .line 140193
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 140194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
