.class public final enum LX/14S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/14S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/14S;

.field public static final enum FLATBUFFER:LX/14S;

.field public static final enum FLATBUFFER_IDL:LX/14S;

.field public static final enum JSON:LX/14S;

.field public static final enum JSONPARSER:LX/14S;

.field public static final enum STREAM:LX/14S;

.field public static final enum STRING:LX/14S;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 178863
    new-instance v0, LX/14S;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v3}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->STRING:LX/14S;

    .line 178864
    new-instance v0, LX/14S;

    const-string v1, "JSON"

    invoke-direct {v0, v1, v4}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->JSON:LX/14S;

    .line 178865
    new-instance v0, LX/14S;

    const-string v1, "JSONPARSER"

    invoke-direct {v0, v1, v5}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->JSONPARSER:LX/14S;

    .line 178866
    new-instance v0, LX/14S;

    const-string v1, "STREAM"

    invoke-direct {v0, v1, v6}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->STREAM:LX/14S;

    .line 178867
    new-instance v0, LX/14S;

    const-string v1, "FLATBUFFER"

    invoke-direct {v0, v1, v7}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->FLATBUFFER:LX/14S;

    .line 178868
    new-instance v0, LX/14S;

    const-string v1, "FLATBUFFER_IDL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/14S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/14S;->FLATBUFFER_IDL:LX/14S;

    .line 178869
    const/4 v0, 0x6

    new-array v0, v0, [LX/14S;

    sget-object v1, LX/14S;->STRING:LX/14S;

    aput-object v1, v0, v3

    sget-object v1, LX/14S;->JSON:LX/14S;

    aput-object v1, v0, v4

    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    aput-object v1, v0, v5

    sget-object v1, LX/14S;->STREAM:LX/14S;

    aput-object v1, v0, v6

    sget-object v1, LX/14S;->FLATBUFFER:LX/14S;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/14S;->FLATBUFFER_IDL:LX/14S;

    aput-object v2, v0, v1

    sput-object v0, LX/14S;->$VALUES:[LX/14S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 178860
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/14S;
    .locals 1

    .prologue
    .line 178862
    const-class v0, LX/14S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/14S;

    return-object v0
.end method

.method public static values()[LX/14S;
    .locals 1

    .prologue
    .line 178861
    sget-object v0, LX/14S;->$VALUES:[LX/14S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/14S;

    return-object v0
.end method
