.class public LX/15P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/15P;


# instance fields
.field public final a:LX/0c4;

.field public b:Landroid/app/Activity;


# direct methods
.method public constructor <init>(LX/0c4;)V
    .locals 0
    .param p1    # LX/0c4;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180551
    iput-object p1, p0, LX/15P;->a:LX/0c4;

    .line 180552
    return-void
.end method

.method public static a(LX/0QB;)LX/15P;
    .locals 4

    .prologue
    .line 180553
    sget-object v0, LX/15P;->c:LX/15P;

    if-nez v0, :cond_1

    .line 180554
    const-class v1, LX/15P;

    monitor-enter v1

    .line 180555
    :try_start_0
    sget-object v0, LX/15P;->c:LX/15P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180556
    if-eqz v2, :cond_0

    .line 180557
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180558
    new-instance p0, LX/15P;

    invoke-static {v0}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v3

    check-cast v3, LX/0c4;

    invoke-direct {p0, v3}, LX/15P;-><init>(LX/0c4;)V

    .line 180559
    move-object v0, p0

    .line 180560
    sput-object v0, LX/15P;->c:LX/15P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180561
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180562
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180563
    :cond_1
    sget-object v0, LX/15P;->c:LX/15P;

    return-object v0

    .line 180564
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
