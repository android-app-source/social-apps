.class public LX/1Hd;
.super Ljava/security/SecureRandom;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 227268
    invoke-direct {p0}, Ljava/security/SecureRandom;-><init>()V

    return-void
.end method


# virtual methods
.method public final nextBoolean()Z
    .locals 1

    .prologue
    .line 227269
    invoke-static {}, LX/40H;->a()V

    .line 227270
    invoke-super {p0}, Ljava/security/SecureRandom;->nextBoolean()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized nextBytes([B)V
    .locals 1

    .prologue
    .line 227271
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/40H;->a()V

    .line 227272
    invoke-super {p0, p1}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227273
    monitor-exit p0

    return-void

    .line 227274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final nextDouble()D
    .locals 2

    .prologue
    .line 227275
    invoke-static {}, LX/40H;->a()V

    .line 227276
    invoke-super {p0}, Ljava/security/SecureRandom;->nextDouble()D

    move-result-wide v0

    return-wide v0
.end method

.method public final nextFloat()F
    .locals 1

    .prologue
    .line 227277
    invoke-static {}, LX/40H;->a()V

    .line 227278
    invoke-super {p0}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v0

    return v0
.end method

.method public final declared-synchronized nextGaussian()D
    .locals 2

    .prologue
    .line 227279
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/40H;->a()V

    .line 227280
    invoke-super {p0}, Ljava/security/SecureRandom;->nextGaussian()D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 227281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final nextInt()I
    .locals 1

    .prologue
    .line 227282
    invoke-static {}, LX/40H;->a()V

    .line 227283
    invoke-super {p0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    return v0
.end method

.method public final nextInt(I)I
    .locals 1

    .prologue
    .line 227284
    invoke-static {}, LX/40H;->a()V

    .line 227285
    invoke-super {p0, p1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    return v0
.end method

.method public final nextLong()J
    .locals 2

    .prologue
    .line 227286
    invoke-static {}, LX/40H;->a()V

    .line 227287
    invoke-super {p0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    return-wide v0
.end method
