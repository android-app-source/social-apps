.class public final LX/18I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic b:Z

.field public final synthetic c:I

.field public final synthetic d:LX/0fz;


# direct methods
.method public constructor <init>(LX/0fz;Lcom/facebook/graphql/model/GraphQLPageInfo;ZI)V
    .locals 0

    .prologue
    .line 206236
    iput-object p1, p0, LX/18I;->d:LX/0fz;

    iput-object p2, p0, LX/18I;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-boolean p3, p0, LX/18I;->b:Z

    iput p4, p0, LX/18I;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206234
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 206235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/18I;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Gap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/18I;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/18I;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
