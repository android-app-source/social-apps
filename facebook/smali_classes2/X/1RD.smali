.class public LX/1RD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 245753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;TE;",
            "LX/1Qx;",
            ")",
            "LX/0Px",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation

    .prologue
    .line 245754
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 245755
    invoke-interface {p0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245756
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Root MultiRowGroupPartDefinition is not needed for the given props."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245757
    :cond_0
    const/4 v1, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/1RE;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1RE;LX/0Pz;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/1RE;

    .line 245758
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
