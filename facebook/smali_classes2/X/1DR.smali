.class public LX/1DR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:I

.field private static volatile f:LX/1DR;


# instance fields
.field public final b:Landroid/view/Display;

.field private final c:LX/0gw;

.field private final d:LX/0gy;

.field public final e:Landroid/graphics/Point;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217626
    const/4 v0, -0x1

    sput v0, LX/1DR;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowManager;LX/0gw;LX/0gy;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217621
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/1DR;->b:Landroid/view/Display;

    .line 217622
    iput-object p2, p0, LX/1DR;->c:LX/0gw;

    .line 217623
    iput-object p3, p0, LX/1DR;->d:LX/0gy;

    .line 217624
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/1DR;->e:Landroid/graphics/Point;

    .line 217625
    return-void
.end method

.method public static a(LX/0QB;)LX/1DR;
    .locals 6

    .prologue
    .line 217607
    sget-object v0, LX/1DR;->f:LX/1DR;

    if-nez v0, :cond_1

    .line 217608
    const-class v1, LX/1DR;

    monitor-enter v1

    .line 217609
    :try_start_0
    sget-object v0, LX/1DR;->f:LX/1DR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 217610
    if-eqz v2, :cond_0

    .line 217611
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 217612
    new-instance p0, LX/1DR;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-static {v0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v4

    check-cast v4, LX/0gw;

    invoke-static {v0}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v5

    check-cast v5, LX/0gy;

    invoke-direct {p0, v3, v4, v5}, LX/1DR;-><init>(Landroid/view/WindowManager;LX/0gw;LX/0gy;)V

    .line 217613
    move-object v0, p0

    .line 217614
    sput-object v0, LX/1DR;->f:LX/1DR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217615
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 217616
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 217617
    :cond_1
    sget-object v0, LX/1DR;->f:LX/1DR;

    return-object v0

    .line 217618
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 217619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 217627
    iget-object v0, p0, LX/1DR;->c:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217628
    iget-object v0, p0, LX/1DR;->b:Landroid/view/Display;

    iget-object v1, p0, LX/1DR;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 217629
    iget-object v0, p0, LX/1DR;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    move v0, v0

    .line 217630
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1DR;->d:LX/0gy;

    invoke-virtual {v0}, LX/0gy;->c()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 217606
    invoke-virtual {p0}, LX/1DR;->a()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f6147ae    # 0.88f

    mul-float/2addr v0, v1

    invoke-static {p1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    add-int/lit8 v0, v0, -0x14

    return v0
.end method

.method public final a(Landroid/content/Context;F)I
    .locals 2

    .prologue
    .line 217604
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p2

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 217605
    invoke-virtual {p0}, LX/1DR;->a()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 217602
    iget-object v0, p0, LX/1DR;->b:Landroid/view/Display;

    iget-object v1, p0, LX/1DR;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 217603
    iget-object v0, p0, LX/1DR;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, LX/1DR;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method
