.class public LX/1rt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:LX/0tG;

.field public final d:LX/0tI;

.field public final e:LX/0sU;

.field public final f:LX/0Uh;

.field public final g:LX/0rq;

.field public final h:LX/1rU;

.field public final i:LX/1ry;

.field public final j:LX/0ad;

.field public final k:Landroid/content/res/Resources;

.field public final l:LX/0se;

.field private final m:LX/0wo;

.field public final n:LX/0sa;

.field public final o:LX/0tE;

.field public final p:LX/1rw;

.field private final q:LX/0wp;

.field public final r:LX/0xX;


# direct methods
.method public constructor <init>(LX/0Or;Ljava/lang/String;LX/0tG;LX/0tI;LX/0sU;LX/0Uh;LX/0rq;LX/1rU;LX/1rw;LX/1ry;LX/0ad;Landroid/content/res/Resources;LX/0se;LX/0wo;LX/0sa;LX/0tE;LX/0wp;LX/0xX;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/notifications/annotations/NotificationsCommunityFilter;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/notifications/annotations/NotificationsEnvironmentFilter;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/0tG;",
            "LX/0tI;",
            "LX/0sU;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0rq;",
            "LX/1rU;",
            "LX/1rw;",
            "LX/1ry;",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            "LX/0se;",
            "LX/0wo;",
            "LX/0sa;",
            "LX/0tE;",
            "LX/0wp;",
            "LX/0xX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333430
    iput-object p1, p0, LX/1rt;->a:LX/0Or;

    .line 333431
    iput-object p2, p0, LX/1rt;->b:Ljava/lang/String;

    .line 333432
    iput-object p3, p0, LX/1rt;->c:LX/0tG;

    .line 333433
    iput-object p4, p0, LX/1rt;->d:LX/0tI;

    .line 333434
    iput-object p5, p0, LX/1rt;->e:LX/0sU;

    .line 333435
    iput-object p6, p0, LX/1rt;->f:LX/0Uh;

    .line 333436
    iput-object p7, p0, LX/1rt;->g:LX/0rq;

    .line 333437
    iput-object p8, p0, LX/1rt;->h:LX/1rU;

    .line 333438
    iput-object p9, p0, LX/1rt;->p:LX/1rw;

    .line 333439
    iput-object p11, p0, LX/1rt;->j:LX/0ad;

    .line 333440
    iput-object p12, p0, LX/1rt;->k:Landroid/content/res/Resources;

    .line 333441
    iput-object p10, p0, LX/1rt;->i:LX/1ry;

    .line 333442
    iput-object p13, p0, LX/1rt;->l:LX/0se;

    .line 333443
    iput-object p14, p0, LX/1rt;->m:LX/0wo;

    .line 333444
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1rt;->n:LX/0sa;

    .line 333445
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1rt;->o:LX/0tE;

    .line 333446
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1rt;->q:LX/0wp;

    .line 333447
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1rt;->r:LX/0xX;

    .line 333448
    return-void
.end method

.method public static b(LX/0QB;)LX/1rt;
    .locals 20

    .prologue
    .line 333449
    new-instance v1, LX/1rt;

    const/16 v2, 0x15ff

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static/range {p0 .. p0}, LX/1ru;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static/range {p0 .. p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v4

    check-cast v4, LX/0tG;

    invoke-static/range {p0 .. p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v5

    check-cast v5, LX/0tI;

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v6

    check-cast v6, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v9

    check-cast v9, LX/1rU;

    invoke-static/range {p0 .. p0}, LX/1rw;->a(LX/0QB;)LX/1rw;

    move-result-object v10

    check-cast v10, LX/1rw;

    invoke-static/range {p0 .. p0}, LX/1ry;->a(LX/0QB;)LX/1ry;

    move-result-object v11

    check-cast v11, LX/1ry;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v14

    check-cast v14, LX/0se;

    invoke-static/range {p0 .. p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v15

    check-cast v15, LX/0wo;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v16

    check-cast v16, LX/0sa;

    invoke-static/range {p0 .. p0}, LX/0tE;->a(LX/0QB;)LX/0tE;

    move-result-object v17

    check-cast v17, LX/0tE;

    invoke-static/range {p0 .. p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v18

    check-cast v18, LX/0wp;

    invoke-static/range {p0 .. p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v19

    check-cast v19, LX/0xX;

    invoke-direct/range {v1 .. v19}, LX/1rt;-><init>(LX/0Or;Ljava/lang/String;LX/0tG;LX/0tI;LX/0sU;LX/0Uh;LX/0rq;LX/1rU;LX/1rw;LX/1ry;LX/0ad;Landroid/content/res/Resources;LX/0se;LX/0wo;LX/0sa;LX/0tE;LX/0wp;LX/0xX;)V

    .line 333450
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Z)LX/0gW;
    .locals 5
    .param p1    # Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 333451
    if-eqz p2, :cond_c

    .line 333452
    iget-object v0, p0, LX/1rt;->h:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 333453
    new-instance v0, LX/BAu;

    invoke-direct {v0}, LX/BAu;-><init>()V

    move-object v0, v0

    .line 333454
    :goto_0
    move-object v0, v0

    .line 333455
    :goto_1
    if-eqz p2, :cond_0

    .line 333456
    if-eqz p1, :cond_0

    .line 333457
    const-string v1, "should_force_deltas"

    .line 333458
    iget-boolean p2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->o:Z

    move p2, p2

    .line 333459
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333460
    const-string v1, "notification_stories_cache_IDs"

    .line 333461
    iget-object p2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->h:Ljava/util/List;

    move-object p2, p2

    .line 333462
    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 333463
    :cond_0
    const/4 p2, 0x1

    const/4 v4, 0x0

    .line 333464
    if-eqz p1, :cond_4

    .line 333465
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    move-object v1, v1

    .line 333466
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333467
    sget-object v1, LX/2ub;->UNKNOWN:LX/2ub;

    invoke-virtual {v1}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v1

    .line 333468
    :cond_1
    const-string v2, "notification_request_source"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333469
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 333470
    if-eqz v1, :cond_2

    .line 333471
    const-string v1, "before_notification_stories"

    .line 333472
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 333473
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333474
    :cond_2
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 333475
    if-eqz v1, :cond_3

    .line 333476
    const-string v1, "after_notification_stories"

    .line 333477
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 333478
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333479
    :cond_3
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    move-object v1, v1

    .line 333480
    if-eqz v1, :cond_4

    .line 333481
    const-string v1, "device_id"

    .line 333482
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->l:Ljava/lang/String;

    move-object v2, v2

    .line 333483
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333484
    :cond_4
    iput-boolean p2, v0, LX/0gW;->l:Z

    .line 333485
    const-string v1, "profile_image_size"

    iget-object v2, p0, LX/1rt;->k:Landroid/content/res/Resources;

    const v3, 0x7f0b0a8b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "image_preview_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333486
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v1

    .line 333487
    if-nez v1, :cond_5

    .line 333488
    sget-object v1, LX/0wB;->a:LX/0wC;

    .line 333489
    :cond_5
    const-string v2, "icon_scale"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 333490
    iget-object v1, p0, LX/1rt;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 333491
    const-string v1, "environment_notification_stories"

    iget-object v2, p0, LX/1rt;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333492
    :cond_6
    iget-object v1, p0, LX/1rt;->r:LX/0xX;

    sget-object v2, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v1, v2}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 333493
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 333494
    const-string v2, "VIDEO_HOME"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333495
    const-string v2, "excluded_environments"

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 333496
    :cond_7
    iget-object v1, p0, LX/1rt;->a:LX/0Or;

    if-eqz v1, :cond_8

    .line 333497
    const-string v2, "community_notification_stories"

    iget-object v1, p0, LX/1rt;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333498
    :cond_8
    const-string v1, "angora_attachment_cover_image_size"

    iget-object v2, p0, LX/1rt;->n:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333499
    const-string v1, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333500
    const-string v1, "reading_attachment_profile_image_width"

    iget-object v2, p0, LX/1rt;->n:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333501
    const-string v1, "reading_attachment_profile_image_height"

    iget-object v2, p0, LX/1rt;->n:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333502
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v1

    .line 333503
    const-string v2, "default_image_scale"

    if-nez v1, :cond_9

    sget-object v1, LX/0wB;->a:LX/0wC;

    :cond_9
    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 333504
    const-string v1, "max_comments"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333505
    iget-object v1, p0, LX/1rt;->o:LX/0tE;

    invoke-virtual {v1, v0}, LX/0tE;->b(LX/0gW;)V

    .line 333506
    iget-object v1, p0, LX/1rt;->c:LX/0tG;

    invoke-virtual {v1, v0}, LX/0tG;->a(LX/0gW;)V

    .line 333507
    iget-object v1, p0, LX/1rt;->d:LX/0tI;

    invoke-virtual {v1, v0}, LX/0tI;->a(LX/0gW;)V

    .line 333508
    invoke-static {v0}, LX/0wo;->a(LX/0gW;)V

    .line 333509
    iget-object v1, p0, LX/1rt;->l:LX/0se;

    iget-object v2, p0, LX/1rt;->g:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->c()LX/0wF;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 333510
    const-string v1, "image_large_aspect_height"

    iget-object v2, p0, LX/1rt;->n:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333511
    const-string v1, "image_large_aspect_width"

    iget-object v2, p0, LX/1rt;->n:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333512
    const-string v1, "fetch_reshare_counts"

    iget-object v2, p0, LX/1rt;->j:LX/0ad;

    sget-short v3, LX/0wn;->aF:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333513
    const-string v1, "notif_option_set_context"

    iget-object v2, p0, LX/1rt;->p:LX/1rw;

    invoke-virtual {v2}, LX/1rw;->a()LX/3T1;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 333514
    const-string v1, "rich_notifications_enabled"

    .line 333515
    iget-boolean v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    move v2, v2

    .line 333516
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333517
    iget-boolean v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->m:Z

    move v1, v1

    .line 333518
    if-eqz v1, :cond_a

    .line 333519
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 333520
    iget-object v1, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    move-object v1, v1

    .line 333521
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 333522
    const-string v1, "reaction_context"

    .line 333523
    iget-object v2, p1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->n:Ljava/lang/String;

    move-object v2, v2

    .line 333524
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333525
    :cond_a
    iget-object v1, p0, LX/1rt;->e:LX/0sU;

    invoke-virtual {v1, v0}, LX/0sU;->a(LX/0gW;)V

    .line 333526
    const-string v1, "num_full_relevant_comments"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333527
    const-string v1, "should_fetch_full_relevant_comments"

    iget-object v2, p0, LX/1rt;->i:LX/1ry;

    invoke-virtual {v2}, LX/1ry;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333528
    const-string v1, "first_n_feedback_reactions"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333529
    const-string v1, "feedback_reaction_type"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333530
    const-string v1, "feedback_reactions_floating_effect"

    iget-object v2, p0, LX/1rt;->f:LX/0Uh;

    const/16 v3, 0x4b0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333531
    const/4 v1, 0x1

    move v1, v1

    .line 333532
    if-eqz v1, :cond_b

    .line 333533
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333534
    :cond_b
    const-string v1, "skip_sample_entities_fields"

    iget-object v2, p0, LX/1rt;->j:LX/0ad;

    sget-short v3, LX/0wn;->ar:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 333535
    return-object v0

    .line 333536
    :cond_c
    iget-object v0, p0, LX/1rt;->h:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 333537
    new-instance v0, LX/BAv;

    invoke-direct {v0}, LX/BAv;-><init>()V

    move-object v0, v0

    .line 333538
    :goto_2
    move-object v0, v0

    .line 333539
    goto/16 :goto_1

    .line 333540
    :cond_d
    new-instance v0, LX/BAt;

    invoke-direct {v0}, LX/BAt;-><init>()V

    move-object v0, v0

    .line 333541
    goto/16 :goto_0

    .line 333542
    :cond_e
    new-instance v0, LX/3Sz;

    invoke-direct {v0}, LX/3Sz;-><init>()V

    move-object v0, v0

    .line 333543
    goto :goto_2
.end method
