.class public LX/0qS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/io/File;

.field private final b:LX/0Uh;

.field private final c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/2tK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;LX/0Uh;)V
    .locals 2
    .param p1    # Ljava/io/File;
        .annotation runtime Lcom/facebook/api/feedcache/db/storage/FeedCacheStorageDirectory;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147641
    iput-object p1, p0, LX/0qS;->a:Ljava/io/File;

    .line 147642
    iput-object p2, p0, LX/0qS;->b:LX/0Uh;

    .line 147643
    new-instance v0, LX/0aq;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0qS;->c:LX/0aq;

    .line 147644
    return-void
.end method

.method public static a(LX/0QB;)LX/0qS;
    .locals 3

    .prologue
    .line 147637
    new-instance v2, LX/0qS;

    invoke-static {p0}, LX/0qL;->b(LX/0QB;)Ljava/io/File;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/0qS;-><init>(Ljava/io/File;LX/0Uh;)V

    .line 147638
    move-object v0, v2

    .line 147639
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/2tK;
    .locals 3

    .prologue
    .line 147631
    iget-object v0, p0, LX/0qS;->b:LX/0Uh;

    const/16 v1, 0x80

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 147632
    iget-object v0, p0, LX/0qS;->c:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2tK;

    .line 147633
    if-nez v0, :cond_0

    .line 147634
    new-instance v0, LX/2tK;

    iget-object v2, p0, LX/0qS;->a:Ljava/io/File;

    invoke-direct {v0, v2, v1}, LX/2tK;-><init>(Ljava/io/File;Z)V

    .line 147635
    iget-object v1, p0, LX/0qS;->c:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147636
    :cond_0
    return-object v0
.end method
