.class public LX/1EZ;
.super LX/16B;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile D:LX/1EZ;

.field public static final a:Z


# instance fields
.field public final A:LX/0ad;

.field public B:LX/8Kb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:LX/8Ka;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8L4;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0cW;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ef;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Ea;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8KY;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/16I;

.field public final k:LX/0SF;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/process/ProcessUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0d9;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/concurrent/ExecutorService;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Kp;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/1Ec;

.field private final s:LX/0kb;

.field private final t:LX/0cX;

.field public u:Z

.field public final v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/8Kw;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public final w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public final x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public final y:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field

.field public final z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 220440
    const-string v0, "MediaUpload"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/1EZ;->a:Z

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ea;LX/0Ot;LX/16I;LX/0SF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/1Ec;LX/0kb;LX/0ad;LX/0cX;)V
    .locals 4
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p15    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8L4;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0cW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8LX;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ef;",
            ">;",
            "LX/1Ea;",
            "LX/0Ot",
            "<",
            "LX/8KY;",
            ">;",
            "LX/16I;",
            "LX/0SF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/process/ProcessUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0d9;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/8Kp;",
            ">;",
            "LX/1Ec;",
            "LX/0kb;",
            "LX/0ad;",
            "LX/0cX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220441
    invoke-direct {p0}, LX/16B;-><init>()V

    .line 220442
    iput-object p1, p0, LX/1EZ;->b:LX/0Ot;

    .line 220443
    iput-object p2, p0, LX/1EZ;->c:LX/0Ot;

    .line 220444
    iput-object p3, p0, LX/1EZ;->d:LX/0Ot;

    .line 220445
    iput-object p4, p0, LX/1EZ;->e:LX/0Ot;

    .line 220446
    iput-object p5, p0, LX/1EZ;->f:LX/0Ot;

    .line 220447
    iput-object p6, p0, LX/1EZ;->g:LX/0Ot;

    .line 220448
    iput-object p7, p0, LX/1EZ;->h:LX/1Ea;

    .line 220449
    iput-object p8, p0, LX/1EZ;->i:LX/0Ot;

    .line 220450
    iput-object p9, p0, LX/1EZ;->j:LX/16I;

    .line 220451
    iput-object p10, p0, LX/1EZ;->k:LX/0SF;

    .line 220452
    iput-object p11, p0, LX/1EZ;->l:LX/0Ot;

    .line 220453
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1EZ;->m:LX/0Ot;

    .line 220454
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1EZ;->n:LX/0Ot;

    .line 220455
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/1EZ;->v:Ljava/util/Map;

    .line 220456
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/1EZ;->w:Ljava/util/Map;

    .line 220457
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/1EZ;->x:Ljava/util/Map;

    .line 220458
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    .line 220459
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, LX/1EZ;->z:Ljava/util/Set;

    .line 220460
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1EZ;->A:LX/0ad;

    .line 220461
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1EZ;->o:LX/0Ot;

    .line 220462
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1EZ;->s:LX/0kb;

    .line 220463
    iget-object v1, p0, LX/1EZ;->j:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/photos/upload/manager/UploadManager$1;

    invoke-direct {v3, p0}, Lcom/facebook/photos/upload/manager/UploadManager$1;-><init>(LX/1EZ;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 220464
    iget-object v1, p0, LX/1EZ;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ef;

    invoke-virtual {v1, p0}, LX/1Ef;->a(LX/1EZ;)V

    .line 220465
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1EZ;->p:Ljava/util/concurrent/ExecutorService;

    .line 220466
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1EZ;->q:LX/0Ot;

    .line 220467
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1EZ;->r:LX/1Ec;

    .line 220468
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1EZ;->t:LX/0cX;

    .line 220469
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1EZ;->u:Z

    .line 220470
    return-void
.end method

.method public static a(LX/0QB;)LX/1EZ;
    .locals 3

    .prologue
    .line 220471
    sget-object v0, LX/1EZ;->D:LX/1EZ;

    if-nez v0, :cond_1

    .line 220472
    const-class v1, LX/1EZ;

    monitor-enter v1

    .line 220473
    :try_start_0
    sget-object v0, LX/1EZ;->D:LX/1EZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 220474
    if-eqz v2, :cond_0

    .line 220475
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1EZ;->b(LX/0QB;)LX/1EZ;

    move-result-object v0

    sput-object v0, LX/1EZ;->D:LX/1EZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220476
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 220477
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 220478
    :cond_1
    sget-object v0, LX/1EZ;->D:LX/1EZ;

    return-object v0

    .line 220479
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 220480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8LS;)LX/742;
    .locals 1

    .prologue
    .line 220481
    sget-object v0, LX/8LS;->VIDEO:LX/8LS;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8LS;->LIVE_VIDEO:LX/8LS;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/8LS;->GIF:LX/8LS;

    if-ne p0, v0, :cond_1

    .line 220482
    :cond_0
    sget-object v0, LX/742;->CHUNKED:LX/742;

    .line 220483
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/742;->NOT_RELEVANT:LX/742;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1EZ;
    .locals 23

    .prologue
    .line 220484
    new-instance v2, LX/1EZ;

    const/16 v3, 0x542

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2ee2

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf38

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2ee5

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf42

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/1Ea;->a(LX/0QB;)LX/1Ea;

    move-result-object v9

    check-cast v9, LX/1Ea;

    const/16 v10, 0x2ed9

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v11

    check-cast v11, LX/16I;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SF;

    const/16 v13, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x271

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x2d3

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xf3f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v17

    check-cast v17, Ljava/util/concurrent/ExecutorService;

    const/16 v18, 0x2edd

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const-class v19, LX/1Ec;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/1Ec;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v20

    check-cast v20, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v21

    check-cast v21, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v22

    check-cast v22, LX/0cX;

    invoke-direct/range {v2 .. v22}, LX/1EZ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ea;LX/0Ot;LX/16I;LX/0SF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/1Ec;LX/0kb;LX/0ad;LX/0cX;)V

    .line 220485
    return-object v2
.end method

.method public static final f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;
    .locals 1

    .prologue
    .line 220486
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 220487
    invoke-static {v0}, LX/1EZ;->a(LX/8LS;)LX/742;

    move-result-object v0

    return-object v0
.end method

.method private static g(Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 220488
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 220489
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 220490
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v0

    .line 220491
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 220492
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 220493
    iget-object v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v3, v4

    .line 220494
    sget-object v4, LX/4gQ;->Video:LX/4gQ;

    if-ne v3, v4, :cond_0

    .line 220495
    iget-object v3, v0, Lcom/facebook/ipc/media/MediaItem;->f:Ljava/lang/String;

    move-object v0, v3

    .line 220496
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 220497
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private h(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 3

    .prologue
    .line 220498
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220499
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    .line 220500
    iget-object v1, v0, LX/8Kp;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220501
    if-eqz v1, :cond_2

    .line 220502
    iget-boolean v0, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v1, v0

    .line 220503
    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 220504
    if-eqz v0, :cond_0

    .line 220505
    const/4 v1, 0x0

    .line 220506
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220507
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 220508
    if-eqz v0, :cond_3

    .line 220509
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    .line 220510
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 220511
    invoke-virtual {v0, v2}, LX/8Kp;->b(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    .line 220512
    iget-boolean v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v2, v2

    .line 220513
    if-nez v2, :cond_3

    .line 220514
    :goto_1
    move-object v0, v0

    .line 220515
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 220516
    :cond_0
    return-object p1

    .line 220517
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private i(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 220518
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kw;

    .line 220519
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/8Kw;->a:LX/1ML;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/8Kw;->a:LX/1ML;

    invoke-virtual {v1}, LX/0SQ;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 220520
    iget-object v0, v0, LX/8Kw;->a:LX/1ML;

    invoke-virtual {v0}, LX/1ML;->cancelOperation()Z

    .line 220521
    :goto_0
    return-void

    .line 220522
    :cond_0
    const-string v0, "UploadManager"

    const-string v1, "Can\'t cancel local upload %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private j(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 3

    .prologue
    .line 220523
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 220524
    :cond_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220525
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220526
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 220527
    invoke-static {v2, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220528
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 7

    .prologue
    .line 220529
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220530
    iget-object v0, p0, LX/1EZ;->p:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/photos/upload/manager/UploadManager$3;

    const-string v2, "UploadManager"

    const-string v3, "CleanupAllPersistedFiles"

    invoke-direct {v1, p0, v2, v3}, Lcom/facebook/photos/upload/manager/UploadManager$3;-><init>(LX/1EZ;Ljava/lang/String;Ljava/lang/String;)V

    const v2, -0x23a3a7c3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 220531
    :goto_0
    return-void

    .line 220532
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v0

    .line 220533
    iget-object v6, p0, LX/1EZ;->p:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/photos/upload/manager/UploadManager$4;

    const-string v2, "UploadManager"

    const-string v3, "CleanupPersistedFiles"

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/upload/manager/UploadManager$4;-><init>(LX/1EZ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    const v1, -0x34cd58d8    # -1.17082E7f

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static k(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 220534
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220535
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v0

    .line 220536
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220537
    iget-object v0, p0, LX/1EZ;->w:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220538
    iget-object v0, p0, LX/1EZ;->x:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220539
    iget-object v0, p0, LX/1EZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cW;

    iget-object v2, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    .line 220540
    iget-boolean v3, v0, LX/0cW;->u:Z

    if-eqz v3, :cond_0

    .line 220541
    if-nez v2, :cond_1

    .line 220542
    iget-object v3, v0, LX/0cW;->n:LX/0cY;

    invoke-virtual {v3}, LX/0cY;->a()V

    .line 220543
    :cond_0
    :goto_0
    monitor-enter p0

    .line 220544
    :try_start_0
    iget-object v0, p0, LX/1EZ;->z:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 220545
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 220546
    :cond_1
    iget-object v3, v0, LX/0cW;->n:LX/0cY;

    invoke-virtual {v3, p1}, LX/0cY;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public static l(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 7

    .prologue
    .line 220547
    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220548
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 220549
    iget-object v0, p0, LX/1EZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cW;

    .line 220550
    iget-boolean v1, v0, LX/0cW;->u:Z

    if-eqz v1, :cond_0

    .line 220551
    iget-object v1, v0, LX/0cW;->o:LX/0cY;

    invoke-virtual {v1, p1}, LX/0cY;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220552
    :cond_0
    iget-object v0, p0, LX/1EZ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0d9;

    .line 220553
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/0d9;->a(LX/0d9;I)V

    .line 220554
    :goto_0
    iget-object v0, p0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8L4;

    invoke-virtual {v0, p1}, LX/8L4;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220555
    return-void

    .line 220556
    :cond_1
    iget-object v0, p0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, p1}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    const-string v1, "2.0"

    invoke-virtual {p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v2

    invoke-static {p1}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v3

    .line 220557
    invoke-static {v0, v1, v2, v3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v4

    .line 220558
    invoke-static {v0, v4, p1}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220559
    sget-object v5, LX/74R;->MEDIA_UPLOAD_RESTART_FAILURE:LX/74R;

    const/4 v6, 0x0

    invoke-static {v0, v5, v4, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 220560
    goto :goto_0
.end method

.method private m(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 220561
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 220562
    :cond_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220563
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220564
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v2

    .line 220565
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 220566
    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220567
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    .line 220568
    iget-object v0, p0, LX/1EZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cW;

    iget-object v1, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 220569
    iget-boolean v2, v0, LX/0cW;->u:Z

    if-eqz v2, :cond_1

    .line 220570
    if-nez v1, :cond_3

    .line 220571
    iget-object v2, v0, LX/0cW;->o:LX/0cY;

    invoke-virtual {v2}, LX/0cY;->a()V

    .line 220572
    :cond_1
    :goto_0
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220573
    iget-object v0, p0, LX/1EZ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0d9;

    invoke-virtual {v0}, LX/0d9;->b()V

    .line 220574
    :cond_2
    return-void

    .line 220575
    :cond_3
    iget-object v2, v0, LX/0cW;->o:LX/0cY;

    invoke-virtual {v2, p1}, LX/0cY;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0
.end method

.method public static n(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 220576
    invoke-direct {p0, p1}, LX/1EZ;->q(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 220577
    :cond_0
    :goto_0
    return-void

    .line 220578
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 220579
    iget-object v1, p0, LX/1EZ;->x:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220580
    iget-object v1, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220581
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220582
    invoke-direct {p0, v1}, LX/1EZ;->i(Ljava/lang/String;)V

    .line 220583
    iget-object v1, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private p()I
    .locals 1

    .prologue
    .line 220252
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method private static p(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/743;
    .locals 2

    .prologue
    .line 220584
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 220585
    sget-object v1, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    if-ne v0, v1, :cond_0

    .line 220586
    sget-object v0, LX/743;->PROFILE_VIDEO:LX/743;

    .line 220587
    :goto_0
    return-object v0

    .line 220588
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v0

    .line 220589
    sget-object v1, LX/8LS;->LIVE_VIDEO:LX/8LS;

    if-ne v0, v1, :cond_1

    .line 220590
    sget-object v0, LX/743;->FACECAST_LIVE_VIDEO:LX/743;

    goto :goto_0

    .line 220591
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/upload/operation/UploadOperation;->Q()Ljava/lang/String;

    move-result-object v0

    .line 220592
    const-string v1, "ANIMATED_GIFS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 220593
    sget-object v0, LX/743;->GIF:LX/743;

    goto :goto_0

    .line 220594
    :cond_2
    const-string v1, "CORE_VIDEOS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220595
    sget-object v0, LX/743;->COMPOSER:LX/743;

    goto :goto_0

    .line 220596
    :cond_3
    sget-object v0, LX/743;->NOT_RELEVANT:LX/743;

    goto :goto_0
.end method

.method private q(Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 2

    .prologue
    .line 220597
    iget-object v0, p0, LX/1EZ;->s:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220598
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move v0, v0

    .line 220599
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EZ;->x:Ljava/util/Map;

    .line 220600
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220601
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 6

    .prologue
    .line 220602
    iget-object v0, p0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v2

    .line 220603
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220604
    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0, v2, v3}, LX/1Ea;->b(Lcom/facebook/photos/upload/operation/UploadOperation;J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 220605
    const/4 v1, 0x0

    .line 220606
    iget-object v5, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 220607
    iput-boolean v1, v5, LX/8LU;->j:Z

    .line 220608
    iget-object v1, p0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8L4;

    invoke-virtual {v1, v0}, LX/8L4;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_0

    .line 220609
    :cond_1
    return-void
.end method

.method public static t(LX/1EZ;)Z
    .locals 3

    .prologue
    .line 220610
    iget-object v0, p0, LX/1EZ;->A:LX/0ad;

    const v1, -0x6872

    sget-object v2, LX/8K0;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 220435
    iget-object v0, p0, LX/1EZ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ef;

    invoke-virtual {v0, p1, p2}, LX/1Ef;->a(J)V

    .line 220436
    return-void
.end method

.method public final a(LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 220437
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220438
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/8Kp;->a(Ljava/util/List;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V

    .line 220439
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 220171
    sget-object v0, LX/8Kx;->InitialPost:LX/8Kx;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220172
    return-void
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 220173
    invoke-direct/range {p0 .. p1}, LX/1EZ;->q(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220174
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->x:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 220175
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->x:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220176
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0cW;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/0cW;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220177
    :goto_0
    return-void

    .line 220178
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 220179
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0VT;

    invoke-virtual {v2}, LX/0VT;->a()LX/00G;

    move-result-object v3

    .line 220180
    invoke-virtual {v3}, LX/00G;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 220181
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "MediaUpload"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Uploads not supported from process "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/00G;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 220182
    :cond_2
    invoke-static/range {p0 .. p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 220183
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Kp;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/8Kp;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220184
    :cond_3
    :goto_1
    invoke-direct/range {p0 .. p1}, LX/1EZ;->h(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v14

    .line 220185
    const/4 v2, 0x0

    .line 220186
    sget-object v3, LX/8Kv;->a:[I

    invoke-virtual/range {p2 .. p2}, LX/8Kx;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_2
    move v13, v2

    .line 220187
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8LX;

    invoke-virtual {v2, v14}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v2

    .line 220188
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->az()Z

    move-result v3

    if-nez v3, :cond_4

    .line 220189
    sget-object v3, LX/8Kx;->InitialPost:LX/8Kx;

    move-object/from16 v0, p2

    if-ne v0, v3, :cond_6

    .line 220190
    const-string v3, "2.0"

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v4

    invoke-static {v14}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v5

    invoke-static {v14}, LX/1EZ;->p(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/743;

    move-result-object v6

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v8

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->M()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->D()Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    move-result-object v10

    iget-object v10, v10, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->e:Ljava/lang/String;

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->C()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, LX/1EZ;->p()I

    move-result v12

    invoke-virtual/range {v2 .. v12}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;LX/743;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;I)V

    .line 220191
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8L4;

    invoke-virtual {v2, v14}, LX/8L4;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220192
    invoke-static/range {p0 .. p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 220193
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->v:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 220194
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    goto/16 :goto_0

    .line 220195
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->v:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 220196
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v3, "MediaUpload"

    const-string v4, "double-enqueue"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 220197
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v14, v4, v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(J)V

    move v13, v2

    .line 220198
    goto/16 :goto_3

    .line 220199
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v14, v2, v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->b(J)V

    .line 220200
    const/4 v2, 0x1

    move v13, v2

    .line 220201
    goto/16 :goto_3

    .line 220202
    :pswitch_2
    const/4 v2, 0x1

    move v13, v2

    .line 220203
    goto/16 :goto_3

    .line 220204
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v14, v4, v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->d(J)V

    goto/16 :goto_2

    .line 220205
    :cond_6
    const-string v3, "2.0"

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v4

    invoke-static {v14}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v6

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v8

    sub-long v8, v6, v8

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/1EZ;->u:Z

    move-object v6, v14

    move v7, v13

    move-object/from16 v10, p3

    invoke-virtual/range {v2 .. v11}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ZJLjava/lang/String;Z)V

    goto/16 :goto_4

    .line 220206
    :cond_7
    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {v14}, LX/1EZ;->g(Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 220207
    :cond_8
    const-string v2, "multimedia_upload_op"

    move-object v3, v2

    .line 220208
    :goto_5
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 220209
    const-string v2, "uploadOp"

    invoke-virtual {v4, v2, v14}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 220210
    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->V()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 220211
    const-string v2, "overridden_viewer_context"

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->V()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 220212
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0aG;

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v6, 0x591ff7bb

    invoke-static {v2, v3, v4, v5, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 220213
    invoke-static/range {p0 .. p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 220214
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Kp;

    invoke-virtual {v2, v14}, LX/8Kp;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220215
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->v:Ljava/util/Map;

    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/8Kw;

    invoke-direct {v5, v3, v14}, LX/8Kw;-><init>(LX/1ML;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220216
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0cW;

    invoke-virtual {v2, v14}, LX/0cW;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220217
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/1EZ;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220218
    new-instance v4, LX/8Kt;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v14, v1, v13}, LX/8Kt;-><init>(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/photos/upload/operation/UploadOperation;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EZ;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 220219
    :cond_b
    invoke-virtual {v14}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 220220
    const-string v2, "video_upload_op"

    move-object v3, v2

    goto/16 :goto_5

    .line 220221
    :cond_c
    const-string v2, "photo_upload_op"

    move-object v3, v2

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 220222
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220223
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 220224
    invoke-direct {p0, v0}, LX/1EZ;->i(Ljava/lang/String;)V

    .line 220225
    iget-object v0, p0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8L4;

    invoke-virtual {v0, p1}, LX/8L4;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220226
    invoke-static {p0, p1}, LX/1EZ;->k(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220227
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220228
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    invoke-virtual {v0, p1}, LX/8Kp;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220229
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 220230
    invoke-direct {p0, v0}, LX/1EZ;->j(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    .line 220231
    if-eqz v4, :cond_1

    .line 220232
    invoke-direct {p0, v4}, LX/1EZ;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220233
    iget-object v0, p0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, v4}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    const-string v1, "2.0"

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v2

    invoke-static {v4}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v3

    .line 220234
    iget-object v5, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v5, v5

    .line 220235
    iget-object v6, p0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v6}, LX/0SF;->a()J

    move-result-wide v6

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v8

    sub-long/2addr v6, v8

    move-object v8, p2

    invoke-virtual/range {v0 .. v8}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73y;JLjava/lang/String;)V

    .line 220236
    :cond_1
    invoke-static {p0, p1}, LX/1EZ;->j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220237
    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 220238
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EZ;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/8Kx;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 220239
    sget-object v0, LX/8Kx;->UserRetry:LX/8Kx;

    if-ne p2, v0, :cond_1

    .line 220240
    const-string v0, "Composer user retry"

    move-object v1, v0

    .line 220241
    :goto_0
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220242
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 220243
    invoke-static {v4, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220244
    invoke-virtual {p0, v0, p2, v1}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220245
    const/4 v0, 0x1

    .line 220246
    :goto_1
    return v0

    .line 220247
    :cond_1
    iget-object v0, p0, LX/1EZ;->h:LX/1Ea;

    invoke-virtual {v0}, LX/1Ea;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 220248
    if-nez v0, :cond_2

    move v0, v2

    .line 220249
    goto :goto_1

    .line 220250
    :cond_2
    const-string v0, "Composer auto retry"

    move-object v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 220251
    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 220253
    iget-object v0, p0, LX/1EZ;->s:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220254
    iget-object v0, p0, LX/1EZ;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 220255
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220256
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220257
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 220258
    sget-object v2, LX/8Kx;->Resume:LX/8Kx;

    const-string v3, "Wi-Fi connected retry"

    invoke-virtual {p0, v0, v2, v3}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    goto :goto_0

    .line 220259
    :cond_0
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 220260
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kw;

    .line 220261
    iget-object v0, v0, LX/8Kw;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {p0, v0}, LX/1EZ;->n(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220262
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 220263
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220264
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220265
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    .line 220266
    iget-object p0, v0, LX/8Kp;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 220267
    invoke-static {v0}, LX/8Kp;->a(LX/8Kp;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 220268
    invoke-static {v0, p1}, LX/8Kp;->c(LX/8Kp;Ljava/lang/String;)V

    .line 220269
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/photos/upload/operation/UploadOperation;)Z
    .locals 9

    .prologue
    .line 220270
    iget-object v0, p0, LX/1EZ;->x:Ljava/util/Map;

    .line 220271
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220272
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220273
    const/4 v0, 0x0

    .line 220274
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    .line 220275
    iget-object v2, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    move-object v2, v2

    .line 220276
    iget v3, v2, LX/8LU;->d:I

    move v3, v3

    .line 220277
    const/16 v4, 0xa

    if-ge v3, v4, :cond_1

    .line 220278
    iget-wide v7, v2, LX/8LU;->b:J

    move-wide v2, v7

    .line 220279
    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 220280
    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;LX/8Kx;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 220281
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220282
    iget-object v3, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v3, v3

    .line 220283
    invoke-static {v3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220284
    const-string v2, "Composer auto retry"

    invoke-virtual {p0, v0, p2, v2}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    move v0, v1

    .line 220285
    :goto_0
    return v0

    .line 220286
    :cond_1
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 220287
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 220288
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kw;

    iget-object v0, v0, LX/8Kw;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220289
    iput-boolean v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->I:Z

    .line 220290
    new-instance v2, LX/8LQ;

    invoke-direct {v2, v0}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    move v0, v1

    .line 220291
    goto :goto_0

    .line 220292
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 8

    .prologue
    .line 220293
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220294
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    .line 220295
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220296
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220297
    invoke-virtual {p0, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220298
    :goto_0
    return-void

    .line 220299
    :cond_0
    iget-object v0, p0, LX/1EZ;->w:Ljava/util/Map;

    .line 220300
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220301
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220302
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 220303
    invoke-direct {p0, v0}, LX/1EZ;->i(Ljava/lang/String;)V

    .line 220304
    invoke-static {p0, p1}, LX/1EZ;->j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220305
    iget-object v0, p0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8L4;

    .line 220306
    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220307
    iget-object v2, v0, LX/8L4;->s:LX/7m8;

    .line 220308
    iget-object v3, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v3, v3

    .line 220309
    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v4, v6

    .line 220310
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 220311
    const-string v7, "com.facebook.STREAM_PUBLISH_RESTART"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 220312
    const-string v7, "extra_request_id"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220313
    const-string v7, "extra_target_id"

    invoke-virtual {v6, v7, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 220314
    iget-object v7, v2, LX/7m8;->a:LX/0Xl;

    invoke-interface {v7, v6}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 220315
    :goto_1
    goto :goto_0

    .line 220316
    :catch_0
    move-exception v2

    .line 220317
    iget-object v3, v0, LX/8L4;->j:LX/03V;

    const-string v4, "Upload restarted throwable"

    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final c(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 220318
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220319
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    .line 220320
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220321
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220322
    invoke-virtual {p0, p1, p2, p3}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220323
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 220324
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220325
    iget-object v0, p0, LX/1EZ;->h:LX/1Ea;

    iget-object v1, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    .line 220326
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->a()V

    .line 220327
    iget-object v2, v0, LX/1Ea;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 220328
    invoke-virtual {v0}, LX/1Ea;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 220329
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 220330
    :cond_0
    :goto_0
    invoke-direct {p0}, LX/1EZ;->r()V

    .line 220331
    return-void

    .line 220332
    :cond_1
    invoke-virtual {p0}, LX/1EZ;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220333
    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220334
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v2, v4, v5}, LX/1Ea;->b(Lcom/facebook/photos/upload/operation/UploadOperation;J)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 220335
    sget-object v3, LX/8Kx;->AutoRetry:LX/8Kx;

    .line 220336
    invoke-virtual {p0, v2, v3, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220337
    goto :goto_0

    .line 220338
    :cond_3
    invoke-static {v1, v4, v5}, LX/1Ea;->a(Ljava/util/LinkedList;J)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 220339
    if-eqz v2, :cond_0

    .line 220340
    iget-object v3, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v3}, LX/1Eb;->b()Z

    move-result v3

    if-nez v3, :cond_5

    .line 220341
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->c()J

    move-result-wide v2

    add-long/2addr v2, v4

    .line 220342
    :cond_4
    :goto_1
    invoke-virtual {p0, v2, v3}, LX/1EZ;->a(J)V

    goto :goto_0

    .line 220343
    :cond_5
    invoke-virtual {p0}, LX/1EZ;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 220344
    sget-object v3, LX/8Kx;->AutoRetry:LX/8Kx;

    .line 220345
    invoke-virtual {p0, v2, v3, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220346
    goto :goto_0

    .line 220347
    :cond_6
    iget-object v3, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v3}, LX/1Eb;->c()J

    move-result-wide v6

    .line 220348
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->n()J

    move-result-wide v2

    add-long/2addr v2, v6

    .line 220349
    cmp-long v8, v2, v4

    if-gtz v8, :cond_4

    add-long v2, v4, v6

    goto :goto_1
.end method

.method public final d(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 13

    .prologue
    .line 220350
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220351
    invoke-static {p0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220352
    iget-object v0, p0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    invoke-virtual {v0, p1}, LX/8Kp;->b(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220353
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v0

    .line 220354
    invoke-direct {p0, v0}, LX/1EZ;->j(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    .line 220355
    if-eqz v4, :cond_2

    .line 220356
    invoke-direct {p0, v4}, LX/1EZ;->m(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220357
    iget-object v0, p0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8L4;

    invoke-virtual {v0, v4}, LX/8L4;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220358
    iget-object v0, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    move-object v5, v0

    .line 220359
    iget-object v0, p0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, v4}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    const-string v1, "2.0"

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v2

    invoke-static {v4}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v3

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v5

    invoke-virtual {v5}, LX/0P1;->size()I

    move-result v5

    :goto_0
    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v6

    .line 220360
    iget-object v7, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->F:Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-object v7, v7

    .line 220361
    iget-object v8, p0, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v8}, LX/0SF;->a()J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 220362
    invoke-static {v0, v1, v2, v3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v10

    .line 220363
    invoke-static {v0, v10, v4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220364
    const-string v11, "multi_success"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220365
    const-string v11, "may_auto_retry"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220366
    invoke-static {v10, v7}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 220367
    invoke-static {v10, v8, v9}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 220368
    sget-object v11, LX/74R;->MEDIA_UPLOAD_FLOW_GIVEUP:LX/74R;

    const/4 v12, 0x0

    invoke-static {v0, v11, v10, v12}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 220369
    iget-object v10, v0, LX/73w;->F:LX/74E;

    .line 220370
    const-string v11, "FlowMarker"

    invoke-static {v10, v11}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 220371
    iget-object v11, v10, LX/74E;->b:LX/11i;

    sget-object v12, LX/74E;->a:LX/74D;

    invoke-interface {v11, v12}, LX/11i;->b(LX/0Pq;)V

    .line 220372
    invoke-static {p0, v4}, LX/1EZ;->j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220373
    :goto_1
    return-void

    .line 220374
    :cond_1
    const/4 v5, -0x1

    goto :goto_0

    .line 220375
    :cond_2
    const-string v0, "GiveUpUpload"

    invoke-virtual {p0, p1, v0}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 220376
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220377
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    .line 220378
    iget-object v0, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220379
    iget-object v0, p0, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8LX;

    invoke-virtual {v0, v4}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v0

    .line 220380
    const-string v1, "2.0"

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v2

    invoke-static {v4}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v3

    iget-object v5, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    .line 220381
    invoke-static {v0, v1, v2, v3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v8

    .line 220382
    invoke-static {v0, v8, v4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220383
    const-string v9, "upload_retry_loop_uuid"

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220384
    const-string v9, "upload_manager_queue_count"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220385
    sget-object v9, LX/74R;->MEDIA_UPLOAD_RETRY_QUEUE_CHECK:LX/74R;

    const/4 v10, 0x0

    invoke-static {v0, v9, v8, v10}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 220386
    goto :goto_0

    .line 220387
    :cond_0
    iget-object v0, p0, LX/1EZ;->h:LX/1Ea;

    iget-object v1, p0, LX/1EZ;->y:Ljava/util/LinkedList;

    .line 220388
    invoke-virtual {v0}, LX/1Ea;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 220389
    :cond_1
    :goto_1
    invoke-direct {p0}, LX/1EZ;->r()V

    .line 220390
    return-void

    .line 220391
    :cond_2
    iget-object v2, v0, LX/1Ea;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 220392
    invoke-static {v1, v4, v5}, LX/1Ea;->a(Ljava/util/LinkedList;J)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v6

    .line 220393
    if-eqz v6, :cond_1

    .line 220394
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->a()V

    .line 220395
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->c()J

    move-result-wide v8

    .line 220396
    iget-object v2, v0, LX/1Ea;->c:LX/1Eb;

    invoke-virtual {v2}, LX/1Eb;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 220397
    invoke-virtual {v6}, Lcom/facebook/photos/upload/operation/UploadOperation;->n()J

    move-result-wide v2

    add-long/2addr v2, v8

    .line 220398
    cmp-long v7, v2, v4

    if-gtz v7, :cond_3

    invoke-virtual {p0}, LX/1EZ;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 220399
    sget-object v2, LX/8Kx;->AutoRetry:LX/8Kx;

    .line 220400
    invoke-virtual {p0, v6, v2, p1}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V

    .line 220401
    goto :goto_1

    .line 220402
    :cond_3
    cmp-long v6, v2, v4

    if-lez v6, :cond_4

    :goto_2
    invoke-virtual {p0, v2, v3}, LX/1EZ;->a(J)V

    goto :goto_1

    :cond_4
    add-long v2, v4, v8

    goto :goto_2

    .line 220403
    :cond_5
    add-long v2, v4, v8

    invoke-virtual {p0, v2, v3}, LX/1EZ;->a(J)V

    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 2

    .prologue
    .line 220404
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220405
    const/4 v1, 0x0

    .line 220406
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kw;

    .line 220407
    if-eqz v0, :cond_2

    .line 220408
    iget-object v0, v0, LX/8Kw;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220409
    :goto_0
    if-nez v0, :cond_0

    .line 220410
    invoke-direct {p0, p1}, LX/1EZ;->j(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    .line 220411
    :cond_0
    if-nez v0, :cond_1

    .line 220412
    iget-object v0, p0, LX/1EZ;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 220413
    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 220414
    invoke-super {p0}, LX/16B;->e()V

    .line 220415
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, Lcom/facebook/photos/upload/manager/UploadManager$5;

    invoke-direct {v1, p0}, Lcom/facebook/photos/upload/manager/UploadManager$5;-><init>(LX/1EZ;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 220416
    return-void
.end method

.method public final e(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 2

    .prologue
    .line 220417
    iget-object v0, p0, LX/1EZ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 220418
    iget-object v0, p0, LX/1EZ;->v:Ljava/util/Map;

    .line 220419
    iget-object v1, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v1

    .line 220420
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220421
    invoke-static {p0, p1}, LX/1EZ;->l(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 220422
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 220423
    iget-object v1, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1EZ;->x:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220424
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, LX/1EZ;->j(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 220425
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1EZ;->z:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220426
    monitor-exit p0

    return-void

    .line 220427
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220428
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 220429
    monitor-enter p0

    .line 220430
    :try_start_0
    iget-object v0, p0, LX/1EZ;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220431
    iget-object v3, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 220432
    iget-object v3, p0, LX/1EZ;->v:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kw;

    iget-object v0, v0, LX/8Kw;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220433
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220434
    return-object v1
.end method
