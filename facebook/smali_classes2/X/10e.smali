.class public final enum LX/10e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/10e;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/10e;

.field public static final enum CLOSED:LX/10e;

.field public static final enum SHOWING_LEFT:LX/10e;

.field public static final enum SHOWING_RIGHT:LX/10e;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169219
    new-instance v0, LX/10e;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2}, LX/10e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10e;->CLOSED:LX/10e;

    .line 169220
    new-instance v0, LX/10e;

    const-string v1, "SHOWING_LEFT"

    invoke-direct {v0, v1, v3}, LX/10e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10e;->SHOWING_LEFT:LX/10e;

    .line 169221
    new-instance v0, LX/10e;

    const-string v1, "SHOWING_RIGHT"

    invoke-direct {v0, v1, v4}, LX/10e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10e;->SHOWING_RIGHT:LX/10e;

    .line 169222
    const/4 v0, 0x3

    new-array v0, v0, [LX/10e;

    sget-object v1, LX/10e;->CLOSED:LX/10e;

    aput-object v1, v0, v2

    sget-object v1, LX/10e;->SHOWING_LEFT:LX/10e;

    aput-object v1, v0, v3

    sget-object v1, LX/10e;->SHOWING_RIGHT:LX/10e;

    aput-object v1, v0, v4

    sput-object v0, LX/10e;->$VALUES:[LX/10e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/10e;
    .locals 1

    .prologue
    .line 169217
    const-class v0, LX/10e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/10e;

    return-object v0
.end method

.method public static values()[LX/10e;
    .locals 1

    .prologue
    .line 169216
    sget-object v0, LX/10e;->$VALUES:[LX/10e;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/10e;

    return-object v0
.end method
