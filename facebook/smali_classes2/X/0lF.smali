.class public abstract LX/0lF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lG;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0lG;",
        "Ljava/lang/Iterable",
        "<",
        "LX/0lF;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 128741
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    return-object v0
.end method

.method public abstract B()Ljava/lang/String;
.end method

.method public C()I
    .locals 1

    .prologue
    .line 128742
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0lF;->b(I)I

    move-result v0

    return v0
.end method

.method public D()J
    .locals 2

    .prologue
    .line 128743
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/0lF;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public E()D
    .locals 2

    .prologue
    .line 128744
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, LX/0lF;->a(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 128745
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0lF;->a(Z)Z

    move-result v0

    return v0
.end method

.method public G()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128746
    sget-object v0, LX/32T;->a:LX/32T;

    move-object v0, v0

    .line 128747
    return-object v0
.end method

.method public H()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 128748
    sget-object v0, LX/32T;->a:LX/32T;

    move-object v0, v0

    .line 128749
    return-object v0
.end method

.method public a(D)D
    .locals 1

    .prologue
    .line 128750
    return-wide p1
.end method

.method public a(J)J
    .locals 1

    .prologue
    .line 128751
    return-wide p1
.end method

.method public abstract a(I)LX/0lF;
.end method

.method public a(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 128752
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Z)Z
    .locals 0

    .prologue
    .line 128753
    return p1
.end method

.method public b(I)I
    .locals 0

    .prologue
    .line 128754
    return p1
.end method

.method public abstract b(Ljava/lang/String;)LX/0lF;
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 128755
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract d()LX/0lF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lF;",
            ">()TT;"
        }
    .end annotation
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 128756
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 128757
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 128758
    const/4 v0, 0x0

    return v0
.end method

.method public abstract e(Ljava/lang/String;)LX/0lF;
.end method

.method public abstract f(Ljava/lang/String;)LX/0lF;
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 128737
    sget-object v0, LX/4pq;->a:[I

    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v1

    invoke-virtual {v1}, LX/0nH;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 128738
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 128739
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 128716
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->MISSING:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 128717
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->ARRAY:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 128718
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->OBJECT:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128719
    invoke-virtual {p0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128720
    sget-object v0, LX/32T;->a:LX/32T;

    move-object v0, v0

    .line 128721
    return-object v0
.end method

.method public abstract k()LX/0nH;
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 128722
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->POJO:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 128723
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->NUMBER:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 128724
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 128725
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->STRING:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 128726
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->BOOLEAN:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 128727
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->NULL:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 128728
    invoke-virtual {p0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->BINARY:LX/0nH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128729
    const/4 v0, 0x0

    return-object v0
.end method

.method public t()[B
    .locals 1

    .prologue
    .line 128730
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 128731
    const/4 v0, 0x0

    return v0
.end method

.method public v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 128732
    const/4 v0, 0x0

    return-object v0
.end method

.method public w()I
    .locals 1

    .prologue
    .line 128733
    const/4 v0, 0x0

    return v0
.end method

.method public x()J
    .locals 2

    .prologue
    .line 128734
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public y()D
    .locals 2

    .prologue
    .line 128735
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public z()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 128736
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    return-object v0
.end method
