.class public LX/1nv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/1Up;

.field public static final b:LX/1Up;

.field public static final c:LX/1Up;

.field public static final d:LX/1Up;

.field public static final e:LX/1Up;

.field private static final f:LX/1Up;

.field private static final g:LX/1Up;

.field private static m:LX/0Xm;


# instance fields
.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1nz;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1nx;

.field private final k:LX/0W3;

.field private final l:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317712
    sget-object v0, LX/1Uo;->b:LX/1Up;

    sput-object v0, LX/1nv;->f:LX/1Up;

    .line 317713
    sget-object v0, LX/1Uo;->a:LX/1Up;

    sput-object v0, LX/1nv;->g:LX/1Up;

    .line 317714
    sget-object v0, LX/1nv;->f:LX/1Up;

    sput-object v0, LX/1nv;->a:LX/1Up;

    .line 317715
    sget-object v0, LX/1nv;->g:LX/1Up;

    sput-object v0, LX/1nv;->b:LX/1Up;

    .line 317716
    sget-object v0, LX/1nv;->g:LX/1Up;

    sput-object v0, LX/1nv;->c:LX/1Up;

    .line 317717
    sget-object v0, LX/1nv;->g:LX/1Up;

    sput-object v0, LX/1nv;->d:LX/1Up;

    .line 317718
    sget-object v0, LX/1nv;->g:LX/1Up;

    sput-object v0, LX/1nv;->e:LX/1Up;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1nx;LX/0W3;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1nz;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/1nx;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 317719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317720
    iput-object p1, p0, LX/1nv;->h:LX/0Or;

    .line 317721
    iput-object p2, p0, LX/1nv;->i:LX/0Or;

    .line 317722
    iput-object p3, p0, LX/1nv;->j:LX/1nx;

    .line 317723
    iput-object p4, p0, LX/1nv;->k:LX/0W3;

    .line 317724
    iput-object p5, p0, LX/1nv;->l:LX/0ad;

    .line 317725
    return-void
.end method

.method public static a(LX/0QB;)LX/1nv;
    .locals 9

    .prologue
    .line 317726
    const-class v1, LX/1nv;

    monitor-enter v1

    .line 317727
    :try_start_0
    sget-object v0, LX/1nv;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 317728
    sput-object v2, LX/1nv;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 317729
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317730
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 317731
    new-instance v3, LX/1nv;

    const/16 v4, 0x3a6

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x509

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1nx;->a(LX/0QB;)LX/1nx;

    move-result-object v6

    check-cast v6, LX/1nx;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1nv;-><init>(LX/0Or;LX/0Or;LX/1nx;LX/0W3;LX/0ad;)V

    .line 317732
    move-object v0, v3

    .line 317733
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 317734
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1nv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317735
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 317736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/1Pp;ZLX/4Ab;LX/1Up;Landroid/graphics/PointF;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;LX/1dc;FLandroid/graphics/ColorFilter;LX/33B;Ljava/lang/String;LX/1Ai;LX/1f9;)LX/1Dg;
    .locals 6
    .param p2    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pp;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/4Ab;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Landroid/graphics/PointF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p11    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p13    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p15    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p18    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p19    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p20    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p21    # Landroid/graphics/ColorFilter;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p22    # LX/33B;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p23    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p24    # LX/1Ai;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p25    # LX/1f9;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "TE;Z",
            "LX/4Ab;",
            "LX/1Up;",
            "Landroid/graphics/PointF;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;F",
            "Landroid/graphics/ColorFilter;",
            "LX/33B;",
            "Ljava/lang/String;",
            "LX/1Ai;",
            "LX/1f9;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 317737
    if-nez p2, :cond_0

    .line 317738
    sget-object p2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 317739
    :cond_0
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    invoke-static {p3}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1bX;->a(LX/1ny;)LX/1bX;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-virtual {v2, v0}, LX/1bX;->a(LX/33B;)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v3

    .line 317740
    if-eqz p6, :cond_1

    if-nez p5, :cond_1

    .line 317741
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "environment cannot be null if prefetching"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 317742
    :cond_1
    iget-object v2, p0, LX/1nv;->l:LX/0ad;

    sget-short v4, LX/0fe;->D:S

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 317743
    if-eqz p5, :cond_3

    if-nez p6, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    move-object v2, p5

    check-cast v2, LX/1Pu;

    invoke-interface {v2}, LX/1Pu;->q()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 317744
    if-eqz p25, :cond_4

    move-object v2, p5

    .line 317745
    check-cast v2, LX/1Pt;

    move-object/from16 v0, p25

    invoke-interface {v2, v0, v3, p4}, LX/1Pt;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 317746
    :cond_3
    :goto_0
    iget-object v2, p0, LX/1nv;->k:LX/0W3;

    sget-wide v4, LX/0X5;->fk:J

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 317747
    iget-object v2, p0, LX/1nv;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nz;

    invoke-virtual {v2, p1}, LX/1nz;->c(LX/1De;)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1o1;->a(Landroid/net/Uri;)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/1o1;->b(Ljava/lang/String;)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/1o1;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/1o1;->a(LX/1dc;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/1o1;->a(LX/1Up;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/1o1;->b(LX/1dc;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/1o1;->c(LX/1Up;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/1o1;->c(LX/1dc;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/1o1;->d(LX/1Up;)LX/1o1;

    move-result-object v2

    move/from16 v0, p16

    invoke-virtual {v2, v0}, LX/1o1;->h(I)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/1o1;->d(LX/1dc;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/1o1;->e(LX/1Up;)LX/1o1;

    move-result-object v2

    move/from16 v0, p20

    invoke-virtual {v2, v0}, LX/1o1;->c(F)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/1o1;->a(LX/4Ab;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/1o1;->e(LX/1dc;)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/1o1;->b(LX/1Up;)LX/1o1;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/1o1;->a(Landroid/graphics/PointF;)LX/1o1;

    move-result-object v2

    move-object/from16 v0, p21

    invoke-virtual {v2, v0}, LX/1o1;->a(Landroid/graphics/ColorFilter;)LX/1o1;

    move-result-object v2

    .line 317748
    goto :goto_1

    .line 317749
    :goto_1
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 317750
    :goto_2
    return-object v2

    :cond_4
    move-object v2, p5

    .line 317751
    check-cast v2, LX/1Pt;

    invoke-interface {v2, v3, p4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 317752
    :cond_5
    iget-object v2, p0, LX/1nv;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v3}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, p4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    move-object/from16 v0, p24

    invoke-virtual {v2, v0}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 317753
    if-eqz p5, :cond_6

    .line 317754
    move-object/from16 v0, p23

    invoke-interface {p5, v2, v0, v3, p4}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 317755
    :cond_6
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/1up;->a(LX/1dc;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/1up;->a(LX/1Up;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/1up;->b(LX/1dc;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/1up;->c(LX/1Up;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/1up;->c(LX/1dc;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/1up;->d(LX/1Up;)LX/1up;

    move-result-object v2

    move/from16 v0, p16

    invoke-virtual {v2, v0}, LX/1up;->i(I)LX/1up;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/1up;->d(LX/1dc;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/1up;->e(LX/1Up;)LX/1up;

    move-result-object v2

    move/from16 v0, p20

    invoke-virtual {v2, v0}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/1up;->e(LX/1dc;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object v2

    move-object/from16 v0, p21

    invoke-virtual {v2, v0}, LX/1up;->a(Landroid/graphics/ColorFilter;)LX/1up;

    move-result-object v2

    .line 317756
    goto :goto_3

    .line 317757
    :goto_3
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_2
.end method
