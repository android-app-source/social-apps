.class public final LX/0UZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/0UW;

.field public c:Z

.field private d:LX/4CT;

.field public e:LX/0UY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 66343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66344
    iput-object p1, p0, LX/0UZ;->a:Landroid/content/Context;

    .line 66345
    return-void
.end method


# virtual methods
.method public final b()LX/0Uh;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 66346
    iget-object v0, p0, LX/0UZ;->b:LX/0UW;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 66347
    iget-boolean v0, p0, LX/0UZ;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "sessionless_gatekeepers"

    .line 66348
    :goto_1
    iget-object v2, p0, LX/0UZ;->a:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 66349
    new-instance v2, LX/0Ua;

    iget-object v1, p0, LX/0UZ;->b:LX/0UW;

    invoke-direct {v2, v1, v0}, LX/0Ua;-><init>(LX/0UW;Ljava/io/File;)V

    .line 66350
    iget-boolean v1, p0, LX/0UZ;->c:Z

    if-eqz v1, :cond_2

    const/4 v5, 0x0

    .line 66351
    :goto_2
    new-instance v0, LX/0Uh;

    iget-object v1, p0, LX/0UZ;->b:LX/0UW;

    iget-object v3, p0, LX/0UZ;->d:LX/4CT;

    iget-object v4, p0, LX/0UZ;->e:LX/0UY;

    invoke-direct/range {v0 .. v5}, LX/0Uh;-><init>(LX/0UW;LX/0Ua;LX/4CT;LX/0UY;LX/0Ug;)V

    return-object v0

    :cond_0
    move v0, v1

    .line 66352
    goto :goto_0

    .line 66353
    :cond_1
    const-string v0, "gatekeepers"

    goto :goto_1

    .line 66354
    :cond_2
    new-instance v5, LX/0Ug;

    iget-object v1, p0, LX/0UZ;->b:LX/0UW;

    invoke-direct {v5, v1, v0}, LX/0Ug;-><init>(LX/0UW;Ljava/io/File;)V

    goto :goto_2
.end method
