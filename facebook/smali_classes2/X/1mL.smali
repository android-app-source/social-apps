.class public interface abstract LX/1mL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract cancel(Ljava/lang/String;)Z
.end method

.method public abstract changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z
.end method

.method public abstract registerCompletionHandler(Ljava/lang/String;LX/1qB;)Z
.end method

.method public abstract startOperation(Ljava/lang/String;Landroid/os/Bundle;ZLcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
.end method

.method public abstract startOperationWithCompletionHandler(Ljava/lang/String;Landroid/os/Bundle;ZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
.end method

.method public abstract startOperationWithCompletionHandlerAppInit(Ljava/lang/String;Landroid/os/Bundle;ZZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;
.end method
