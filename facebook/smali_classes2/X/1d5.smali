.class public LX/1d5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2yv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 284075
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 284076
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 284077
    new-instance v0, LX/3ql;

    invoke-direct {v0}, LX/3ql;-><init>()V

    sput-object v0, LX/1d5;->a:LX/2yv;

    .line 284078
    :goto_0
    return-void

    .line 284079
    :cond_0
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 284080
    new-instance v0, LX/3jt;

    invoke-direct {v0}, LX/3jt;-><init>()V

    sput-object v0, LX/1d5;->a:LX/2yv;

    goto :goto_0

    .line 284081
    :cond_1
    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 284082
    new-instance v0, LX/2yu;

    invoke-direct {v0}, LX/2yu;-><init>()V

    sput-object v0, LX/1d5;->a:LX/2yv;

    goto :goto_0

    .line 284083
    :cond_2
    new-instance v0, LX/2vG;

    invoke-direct {v0}, LX/2vG;-><init>()V

    sput-object v0, LX/1d5;->a:LX/2yv;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284059
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 284073
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;)V

    .line 284074
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;FF)V
    .locals 1

    .prologue
    .line 284071
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0, p1, p2}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 284072
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 284069
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0, p1}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 284070
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;IIII)V
    .locals 6

    .prologue
    .line 284067
    sget-object v0, LX/1d5;->a:LX/2yv;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 284068
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 284065
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0, p1}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 284066
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 284063
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0, p1}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 284064
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .prologue
    .line 284061
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0, p1}, LX/2yv;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 284062
    return-void
.end method

.method public static c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 284060
    sget-object v0, LX/1d5;->a:LX/2yv;

    invoke-interface {v0, p0}, LX/2yv;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
