.class public LX/1XF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:Landroid/graphics/Typeface;

.field public static final d:Landroid/text/Layout$Alignment;

.field public static final e:Ljava/lang/Integer;

.field public static final f:Ljava/lang/Integer;

.field public static final g:I

.field private static final h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field private final j:LX/1z9;

.field private final k:LX/1xu;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 270770
    const v0, 0x7f0a0158

    sput v0, LX/1XF;->a:I

    .line 270771
    const v0, 0x7f0b1064

    sput v0, LX/1XF;->b:I

    .line 270772
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    sput-object v0, LX/1XF;->c:Landroid/graphics/Typeface;

    .line 270773
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/1XF;->d:Landroid/text/Layout$Alignment;

    .line 270774
    sput-object v1, LX/1XF;->e:Ljava/lang/Integer;

    .line 270775
    sput-object v1, LX/1XF;->f:Ljava/lang/Integer;

    .line 270776
    const v0, 0x7f0a010c

    sput v0, LX/1XF;->g:I

    .line 270777
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 270778
    sput-object v0, LX/1XF;->h:Landroid/util/SparseArray;

    const v1, 0x7f0d0081

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 270779
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 270780
    sput-object v0, LX/1XF;->i:Landroid/util/SparseArray;

    const v1, 0x7f0d0081

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 270781
    return-void
.end method

.method public constructor <init>(LX/1z9;LX/1xu;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1z9;",
            "LX/1xu;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270783
    iput-object p1, p0, LX/1XF;->j:LX/1z9;

    .line 270784
    iput-object p2, p0, LX/1XF;->k:LX/1xu;

    .line 270785
    iput-object p3, p0, LX/1XF;->l:LX/0Ot;

    .line 270786
    return-void
.end method

.method public static a(LX/0QB;)LX/1XF;
    .locals 6

    .prologue
    .line 270787
    const-class v1, LX/1XF;

    monitor-enter v1

    .line 270788
    :try_start_0
    sget-object v0, LX/1XF;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 270789
    sput-object v2, LX/1XF;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 270792
    new-instance v5, LX/1XF;

    const-class v3, LX/1z9;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1z9;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v4

    check-cast v4, LX/1xu;

    const/16 p0, 0x12c4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/1XF;-><init>(LX/1z9;LX/1xu;LX/0Ot;)V

    .line 270793
    move-object v0, v5

    .line 270794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 270795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1XF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 270797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;ILjava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/Typeface;Landroid/text/Layout$Alignment;ZLX/1xz;IIIF)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # LX/1xz;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Typeface;",
            "Landroid/text/Layout$Alignment;",
            "Z",
            "LX/1xz;",
            "IIIF)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 270798
    if-eqz p5, :cond_0

    if-nez p6, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 270799
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 270800
    sget v1, LX/1XF;->g:I

    move/from16 v0, p13

    if-ne v0, v1, :cond_7

    .line 270801
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f01029b

    move/from16 v0, p13

    invoke-static {v1, v2, v0}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v5

    .line 270802
    :goto_1
    if-eqz p10, :cond_3

    .line 270803
    :goto_2
    iget-object v2, p0, LX/1XF;->k:LX/1xu;

    move-object v1, p3

    check-cast v1, LX/1Pr;

    move-object/from16 v0, p10

    invoke-virtual {v2, v0, v1}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 270804
    check-cast p3, LX/1Pr;

    invoke-interface/range {p10 .. p10}, LX/1xz;->a()LX/1KL;

    move-result-object v1

    invoke-static {p2}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v2

    invoke-interface {p3, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yT;

    .line 270805
    const/4 v2, 0x0

    move/from16 v0, p11

    invoke-static {p1, v2, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    iget-object v1, v1, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    .line 270806
    if-eqz p6, :cond_4

    .line 270807
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1ne;->m(I)LX/1ne;

    .line 270808
    :goto_3
    if-lez p12, :cond_1

    .line 270809
    move/from16 v0, p12

    invoke-virtual {v1, v0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    .line 270810
    :cond_1
    const v2, 0x7f0b1063

    invoke-virtual {v1, v2}, LX/1ne;->s(I)LX/1ne;

    move-result-object v1

    move/from16 v0, p14

    invoke-virtual {v1, v0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-static {p1}, LX/1WE;->d(LX/1De;)LX/1dQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-static {p2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, LX/1XF;->h:Landroid/util/SparseArray;

    :goto_4
    invoke-interface {v2, v1}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 270811
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 270812
    :cond_3
    iget-object v1, p0, LX/1XF;->j:LX/1z9;

    const/4 v2, 0x0

    invoke-virtual {v3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    move-object v3, p3

    check-cast v3, LX/1Po;

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    move-object v3, p2

    move/from16 v6, p9

    invoke-virtual/range {v1 .. v7}, LX/1z9;->a(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;IIZLX/1PT;)LX/1zA;

    move-result-object p10

    goto/16 :goto_2

    .line 270813
    :cond_4
    if-eqz p5, :cond_5

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_5

    .line 270814
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    goto :goto_3

    .line 270815
    :cond_5
    const v2, 0x7f01029a

    sget v3, LX/1XF;->a:I

    invoke-virtual {v1, v2, v3}, LX/1ne;->f(II)LX/1ne;

    goto :goto_3

    .line 270816
    :cond_6
    sget-object v1, LX/1XF;->i:Landroid/util/SparseArray;

    goto :goto_4

    :cond_7
    move/from16 v5, p13

    goto/16 :goto_1
.end method
