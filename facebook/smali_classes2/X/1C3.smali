.class public LX/1C3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0So;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7K9;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215339
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    .line 215340
    iput-object p1, p0, LX/1C3;->a:LX/0So;

    .line 215341
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1C3;->c:Z

    .line 215342
    return-void
.end method

.method public static a(LX/0QB;)LX/1C3;
    .locals 2

    .prologue
    .line 215343
    new-instance v1, LX/1C3;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-direct {v1, v0}, LX/1C3;-><init>(LX/0So;)V

    .line 215344
    move-object v0, v1

    .line 215345
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 215346
    iget-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7K9;

    .line 215347
    iget-object v2, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 215348
    iput-boolean v1, p0, LX/1C3;->c:Z

    .line 215349
    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 215350
    iget-boolean v0, p0, LX/1C3;->c:Z

    if-eqz v0, :cond_1

    .line 215351
    :cond_0
    :goto_0
    return v8

    .line 215352
    :cond_1
    iget-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/7K9;

    .line 215353
    iget-object v9, p0, LX/1C3;->b:Ljava/util/Map;

    new-instance v0, LX/7K9;

    iget-object v1, p0, LX/1C3;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/7K9;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-interface {v9, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215354
    iget-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x19

    if-lt v0, v1, :cond_2

    move v0, v7

    :goto_1
    iput-boolean v0, p0, LX/1C3;->c:Z

    .line 215355
    if-nez v6, :cond_0

    move v8, v7

    goto :goto_0

    :cond_2
    move v0, v8

    .line 215356
    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 215357
    iget-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215358
    return-void
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 215359
    iget-object v0, p0, LX/1C3;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
