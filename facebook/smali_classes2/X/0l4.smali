.class public LX/0l4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 128247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128248
    return-void
.end method

.method public static createInstance__com_facebook_fbservice_ops_CriticalServiceExceptionChecker__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0l4;
    .locals 1

    .prologue
    .line 128249
    new-instance v0, LX/0l4;

    invoke-direct {v0}, LX/0l4;-><init>()V

    .line 128250
    return-object v0
.end method


# virtual methods
.method public isInvalidSessionException(Ljava/lang/Throwable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 128236
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-nez v0, :cond_0

    move v0, v1

    .line 128237
    :goto_0
    return v0

    .line 128238
    :cond_0
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 128239
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 128240
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 128241
    goto :goto_0

    .line 128242
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 128243
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 128244
    if-nez v0, :cond_2

    move v0, v1

    .line 128245
    goto :goto_0

    .line 128246
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    const/16 v3, 0xbe

    if-eq v2, v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v2, 0x66

    if-ne v0, v2, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
