.class public LX/11P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172162
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/11P;->a:Ljava/util/HashMap;

    .line 172163
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UserTimelineQuery"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "InitialUserTimelineQueryPlutonium"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TimelineFirstUnitsUser"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TimelineFirstUnitsUserPlutonium"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/11P;->b:Ljava/util/HashSet;

    .line 172164
    return-void
.end method

.method public static a(LX/0QB;)LX/11P;
    .locals 1

    .prologue
    .line 172151
    new-instance v0, LX/11P;

    invoke-direct {v0}, LX/11P;-><init>()V

    .line 172152
    move-object v0, v0

    .line 172153
    return-object v0
.end method

.method private static c(LX/14N;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 172154
    new-instance v2, Ljava/lang/StringBuilder;

    .line 172155
    iget-object v0, p0, LX/14N;->c:Ljava/lang/String;

    move-object v0, v0

    .line 172156
    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 172157
    invoke-virtual {p0}, LX/14N;->h()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 172158
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x3a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172159
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 172160
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/14N;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 172149
    iget-object v0, p0, LX/11P;->a:Ljava/util/HashMap;

    invoke-static {p1}, LX/11P;->c(LX/14N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172150
    return-void
.end method

.method public final a(LX/14N;)Z
    .locals 2

    .prologue
    .line 172146
    iget-object v0, p0, LX/11P;->b:Ljava/util/HashSet;

    .line 172147
    iget-object v1, p1, LX/14N;->a:Ljava/lang/String;

    move-object v1, v1

    .line 172148
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/14N;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 172145
    iget-object v0, p0, LX/11P;->a:Ljava/util/HashMap;

    invoke-static {p1}, LX/11P;->c(LX/14N;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
