.class public final LX/0jj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field private static final d:[LX/0cQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 124626
    const-string v0, "treehouse_basic_model"

    sput-object v0, LX/0jj;->a:Ljava/lang/String;

    .line 124627
    const-string v0, "treehouse_name_hint"

    sput-object v0, LX/0jj;->b:Ljava/lang/String;

    .line 124628
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "chromeless:content:fragment:tag"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ufi:popover:content:fragment:tag"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "transliteration:fragment:tag"

    aput-object v2, v0, v1

    sput-object v0, LX/0jj;->c:[Ljava/lang/String;

    .line 124629
    invoke-static {}, LX/0cQ;->values()[LX/0cQ;

    move-result-object v0

    sput-object v0, LX/0jj;->d:[LX/0cQ;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124622
    return-void
.end method

.method public static a(I)LX/0cQ;
    .locals 1

    .prologue
    .line 124623
    if-ltz p0, :cond_0

    sget-object v0, LX/0jj;->d:[LX/0cQ;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 124624
    sget-object v0, LX/0jj;->d:[LX/0cQ;

    aget-object v0, v0, p0

    .line 124625
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
