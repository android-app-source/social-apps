.class public LX/0tf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field public b:Z

.field public c:[J

.field public d:[Ljava/lang/Object;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155404
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0tf;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 155363
    const/16 v0, 0xa

    invoke-direct {p0, v0}, LX/0tf;-><init>(I)V

    .line 155364
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155395
    iput-boolean v2, p0, LX/0tf;->b:Z

    .line 155396
    if-nez p1, :cond_0

    .line 155397
    sget-object v0, LX/01M;->b:[J

    iput-object v0, p0, LX/0tf;->c:[J

    .line 155398
    sget-object v0, LX/01M;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/0tf;->d:[Ljava/lang/Object;

    .line 155399
    :goto_0
    iput v2, p0, LX/0tf;->e:I

    .line 155400
    return-void

    .line 155401
    :cond_0
    invoke-static {p1}, LX/01M;->b(I)I

    move-result v0

    .line 155402
    new-array v1, v0, [J

    iput-object v1, p0, LX/0tf;->c:[J

    .line 155403
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/0tf;->d:[Ljava/lang/Object;

    goto :goto_0
.end method

.method public static d(LX/0tf;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 155379
    iget v3, p0, LX/0tf;->e:I

    .line 155380
    iget-object v4, p0, LX/0tf;->c:[J

    .line 155381
    iget-object v5, p0, LX/0tf;->d:[Ljava/lang/Object;

    move v1, v2

    move v0, v2

    .line 155382
    :goto_0
    if-ge v1, v3, :cond_2

    .line 155383
    aget-object v6, v5, v1

    .line 155384
    sget-object v7, LX/0tf;->a:Ljava/lang/Object;

    if-eq v6, v7, :cond_1

    .line 155385
    if-eq v1, v0, :cond_0

    .line 155386
    aget-wide v8, v4, v1

    aput-wide v8, v4, v0

    .line 155387
    aput-object v6, v5, v0

    .line 155388
    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 155389
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 155390
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 155391
    :cond_2
    iput-boolean v2, p0, LX/0tf;->b:Z

    .line 155392
    iput v0, p0, LX/0tf;->e:I

    .line 155393
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 155376
    iget-boolean v0, p0, LX/0tf;->b:Z

    if-eqz v0, :cond_0

    .line 155377
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155378
    :cond_0
    iget v0, p0, LX/0tf;->e:I

    return v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)I"
        }
    .end annotation

    .prologue
    .line 155369
    iget-boolean v0, p0, LX/0tf;->b:Z

    if-eqz v0, :cond_0

    .line 155370
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155371
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/0tf;->e:I

    if-ge v0, v1, :cond_2

    .line 155372
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_1

    .line 155373
    :goto_1
    return v0

    .line 155374
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155375
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(J)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TE;"
        }
    .end annotation

    .prologue
    .line 155368
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/0tf;->a(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTE;)TE;"
        }
    .end annotation

    .prologue
    .line 155365
    iget-object v0, p0, LX/0tf;->c:[J

    iget v1, p0, LX/0tf;->e:I

    invoke-static {v0, v1, p1, p2}, LX/01M;->a([JIJ)I

    move-result v0

    .line 155366
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, LX/0tf;->a:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    .line 155367
    :cond_0
    :goto_0
    return-object p3

    :cond_1
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object p3, v1, v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 155405
    iget-object v0, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    sget-object v1, LX/0tf;->a:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 155406
    iget-object v0, p0, LX/0tf;->d:[Ljava/lang/Object;

    sget-object v1, LX/0tf;->a:Ljava/lang/Object;

    aput-object v1, v0, p1

    .line 155407
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0tf;->b:Z

    .line 155408
    :cond_0
    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 155296
    iget-boolean v0, p0, LX/0tf;->b:Z

    if-eqz v0, :cond_0

    .line 155297
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155298
    :cond_0
    iget-object v0, p0, LX/0tf;->c:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 155299
    iget v2, p0, LX/0tf;->e:I

    .line 155300
    iget-object v3, p0, LX/0tf;->d:[Ljava/lang/Object;

    move v0, v1

    .line 155301
    :goto_0
    if-ge v0, v2, :cond_0

    .line 155302
    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 155303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155304
    :cond_0
    iput v1, p0, LX/0tf;->e:I

    .line 155305
    iput-boolean v1, p0, LX/0tf;->b:Z

    .line 155306
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 155307
    iget-object v0, p0, LX/0tf;->c:[J

    iget v1, p0, LX/0tf;->e:I

    invoke-static {v0, v1, p1, p2}, LX/01M;->a([JIJ)I

    move-result v0

    .line 155308
    if-ltz v0, :cond_0

    .line 155309
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, LX/0tf;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 155310
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    sget-object v2, LX/0tf;->a:Ljava/lang/Object;

    aput-object v2, v1, v0

    .line 155311
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0tf;->b:Z

    .line 155312
    :cond_0
    return-void
.end method

.method public final b(JLjava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 155313
    iget-object v0, p0, LX/0tf;->c:[J

    iget v1, p0, LX/0tf;->e:I

    invoke-static {v0, v1, p1, p2}, LX/01M;->a([JIJ)I

    move-result v0

    .line 155314
    if-ltz v0, :cond_0

    .line 155315
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 155316
    :goto_0
    return-void

    .line 155317
    :cond_0
    xor-int/lit8 v0, v0, -0x1

    .line 155318
    iget v1, p0, LX/0tf;->e:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, LX/0tf;->a:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    .line 155319
    iget-object v1, p0, LX/0tf;->c:[J

    aput-wide p1, v1, v0

    .line 155320
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    goto :goto_0

    .line 155321
    :cond_1
    iget-boolean v1, p0, LX/0tf;->b:Z

    if-eqz v1, :cond_2

    iget v1, p0, LX/0tf;->e:I

    iget-object v2, p0, LX/0tf;->c:[J

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 155322
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155323
    iget-object v0, p0, LX/0tf;->c:[J

    iget v1, p0, LX/0tf;->e:I

    invoke-static {v0, v1, p1, p2}, LX/01M;->a([JIJ)I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    .line 155324
    :cond_2
    iget v1, p0, LX/0tf;->e:I

    iget-object v2, p0, LX/0tf;->c:[J

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 155325
    iget v1, p0, LX/0tf;->e:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, LX/01M;->b(I)I

    move-result v1

    .line 155326
    new-array v2, v1, [J

    .line 155327
    new-array v1, v1, [Ljava/lang/Object;

    .line 155328
    iget-object v3, p0, LX/0tf;->c:[J

    iget-object v4, p0, LX/0tf;->c:[J

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155329
    iget-object v3, p0, LX/0tf;->d:[Ljava/lang/Object;

    iget-object v4, p0, LX/0tf;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155330
    iput-object v2, p0, LX/0tf;->c:[J

    .line 155331
    iput-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    .line 155332
    :cond_3
    iget v1, p0, LX/0tf;->e:I

    sub-int/2addr v1, v0

    if-eqz v1, :cond_4

    .line 155333
    iget-object v1, p0, LX/0tf;->c:[J

    iget-object v2, p0, LX/0tf;->c:[J

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, LX/0tf;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155334
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    iget-object v2, p0, LX/0tf;->d:[Ljava/lang/Object;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, LX/0tf;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155335
    :cond_4
    iget-object v1, p0, LX/0tf;->c:[J

    aput-wide p1, v1, v0

    .line 155336
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 155337
    iget v0, p0, LX/0tf;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0tf;->e:I

    goto/16 :goto_0
.end method

.method public final c(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 155338
    iget-boolean v0, p0, LX/0tf;->b:Z

    if-eqz v0, :cond_0

    .line 155339
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155340
    :cond_0
    iget-object v0, p0, LX/0tf;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final c(JLjava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 155341
    iget v0, p0, LX/0tf;->e:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0tf;->c:[J

    iget v1, p0, LX/0tf;->e:I

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 155342
    invoke-virtual {p0, p1, p2, p3}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 155343
    :goto_0
    return-void

    .line 155344
    :cond_0
    iget-boolean v0, p0, LX/0tf;->b:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/0tf;->e:I

    iget-object v1, p0, LX/0tf;->c:[J

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 155345
    invoke-static {p0}, LX/0tf;->d(LX/0tf;)V

    .line 155346
    :cond_1
    iget v0, p0, LX/0tf;->e:I

    .line 155347
    iget-object v1, p0, LX/0tf;->c:[J

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 155348
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, LX/01M;->b(I)I

    move-result v1

    .line 155349
    new-array v2, v1, [J

    .line 155350
    new-array v1, v1, [Ljava/lang/Object;

    .line 155351
    iget-object v3, p0, LX/0tf;->c:[J

    iget-object v4, p0, LX/0tf;->c:[J

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155352
    iget-object v3, p0, LX/0tf;->d:[Ljava/lang/Object;

    iget-object v4, p0, LX/0tf;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155353
    iput-object v2, p0, LX/0tf;->c:[J

    .line 155354
    iput-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    .line 155355
    :cond_2
    iget-object v1, p0, LX/0tf;->c:[J

    aput-wide p1, v1, v0

    .line 155356
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 155357
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0tf;->e:I

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155358
    const/4 v1, 0x0

    .line 155359
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tf;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155360
    :try_start_1
    iget-object v1, p0, LX/0tf;->c:[J

    invoke-virtual {v1}, [J->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    iput-object v1, v0, LX/0tf;->c:[J

    .line 155361
    iget-object v1, p0, LX/0tf;->d:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, LX/0tf;->d:[Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 155362
    :goto_0
    return-object v0

    :catch_0
    move-object v0, v1

    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 155278
    invoke-virtual {p0}, LX/0tf;->a()I

    move-result v0

    if-gtz v0, :cond_0

    .line 155279
    const-string v0, "{}"

    .line 155280
    :goto_0
    return-object v0

    .line 155281
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v0, p0, LX/0tf;->e:I

    mul-int/lit8 v0, v0, 0x1c

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 155282
    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155283
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, LX/0tf;->e:I

    if-ge v0, v2, :cond_3

    .line 155284
    if-lez v0, :cond_1

    .line 155285
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155286
    :cond_1
    invoke-virtual {p0, v0}, LX/0tf;->b(I)J

    move-result-wide v2

    .line 155287
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 155288
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155289
    invoke-virtual {p0, v0}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v2

    .line 155290
    if-eq v2, p0, :cond_2

    .line 155291
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155292
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155293
    :cond_2
    const-string v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 155294
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155295
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
