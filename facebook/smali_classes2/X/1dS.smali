.class public LX/1dS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 285959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285960
    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 285961
    sget-boolean v0, LX/1V5;->a:Z

    if-nez v0, :cond_1

    .line 285962
    :cond_0
    return-void

    .line 285963
    :cond_1
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285964
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method should be called while holding the lock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 285965
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 285966
    sget-boolean v0, LX/1V5;->a:Z

    if-eqz v0, :cond_0

    sget-boolean v0, LX/1V5;->d:Z

    if-eqz v0, :cond_1

    .line 285967
    :cond_0
    return-void

    .line 285968
    :cond_1
    invoke-static {}, LX/1dS;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 285969
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This should run on the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 285970
    sget-boolean v0, LX/1V5;->a:Z

    if-nez v0, :cond_1

    .line 285971
    :cond_0
    return-void

    .line 285972
    :cond_1
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285973
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method should be called outside the lock."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
