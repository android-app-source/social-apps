.class public LX/1lT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1kS;


# direct methods
.method public constructor <init>(LX/1kS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 311939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311940
    iput-object p1, p0, LX/1lT;->a:LX/1kS;

    .line 311941
    return-void
.end method

.method private b(LX/0Px;)LX/0Px;
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311942
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 311943
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kK;

    .line 311944
    invoke-direct {p0, v0}, LX/1lT;->b(LX/1kK;)LX/1lP;

    move-result-object v4

    .line 311945
    iget-wide v6, v4, LX/1lP;->d:D

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_0

    .line 311946
    new-instance v5, LX/32d;

    invoke-direct {v5}, LX/32d;-><init>()V

    invoke-virtual {v5, v0}, LX/32d;->a(LX/1kK;)LX/32d;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/32d;->a(LX/1lP;)LX/32d;

    move-result-object v0

    invoke-virtual {v0}, LX/32d;->a()LX/1RN;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311947
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 311948
    :cond_1
    new-instance v0, LX/1lU;

    invoke-direct {v0, p0}, LX/1lU;-><init>(LX/1lT;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 311949
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/1kK;)LX/1lP;
    .locals 6

    .prologue
    .line 311950
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, LX/1kW;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 311951
    if-eqz v0, :cond_0

    .line 311952
    check-cast p1, LX/1kW;

    check-cast p1, LX/1kW;

    .line 311953
    iget-object v0, p1, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v0

    .line 311954
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->t()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v4, v5, v0}, LX/1lP;->a(Ljava/lang/String;Ljava/lang/String;DLjava/lang/String;)LX/1lP;

    move-result-object v0

    .line 311955
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1lT;->a:LX/1kS;

    invoke-virtual {v0, p1}, LX/1kS;->a(LX/1kK;)LX/1lP;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1lT;
    .locals 2

    .prologue
    .line 311956
    new-instance v1, LX/1lT;

    invoke-static {p0}, LX/1kS;->a(LX/0QB;)LX/1kS;

    move-result-object v0

    check-cast v0, LX/1kS;

    invoke-direct {v1, v0}, LX/1lT;-><init>(LX/1kS;)V

    .line 311957
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311958
    invoke-direct {p0, p1}, LX/1lT;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
