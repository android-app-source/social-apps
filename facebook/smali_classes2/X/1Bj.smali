.class public LX/1Bj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private final a:LX/7hv;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7hv;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7hv;",
            "LX/0Or",
            "<",
            "LX/0SF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213833
    iput-object p1, p0, LX/1Bj;->a:LX/7hv;

    .line 213834
    iput-object p2, p0, LX/1Bj;->b:LX/0Or;

    .line 213835
    return-void
.end method

.method private static a(LX/0EC;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213813
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213814
    const-string p2, "/"

    .line 213815
    :cond_0
    iget-object v0, p0, LX/0EC;->e:Ljava/lang/String;

    .line 213816
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213817
    const-string v0, "/"

    .line 213818
    :cond_1
    const-string v2, "/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 213819
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 213820
    :cond_2
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 213821
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213822
    :cond_3
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    move v0, v2

    .line 213823
    if-nez v0, :cond_4

    move v0, v1

    .line 213824
    :goto_0
    return v0

    .line 213825
    :cond_4
    invoke-virtual {p0, p1}, LX/0EC;->a(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 213826
    goto :goto_0

    .line 213827
    :cond_5
    const-string v0, "http"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/0EC;->g:Z

    if-eqz v0, :cond_6

    move v0, v1

    .line 213828
    goto :goto_0

    .line 213829
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1Bj;
    .locals 3

    .prologue
    .line 213830
    new-instance v1, LX/1Bj;

    invoke-static {p0}, LX/7hv;->a(LX/0QB;)LX/7hv;

    move-result-object v0

    check-cast v0, LX/7hv;

    const/16 v2, 0x2e3

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/1Bj;-><init>(LX/7hv;LX/0Or;)V

    .line 213831
    return-object v1
.end method

.method private b(ILandroid/net/Uri;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 213836
    invoke-virtual {p2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/IDN;->toASCII(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 213837
    if-nez v2, :cond_1

    .line 213838
    :cond_0
    :goto_0
    return-object v0

    .line 213839
    :cond_1
    iget-object v1, p0, LX/1Bj;->a:LX/7hv;

    invoke-virtual {v1, p1, v2}, LX/7hv;->a(ILjava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 213840
    if-eqz v1, :cond_0

    .line 213841
    const/4 v3, 0x0

    .line 213842
    :try_start_0
    new-instance v4, Ljava/net/URI;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    const/4 p1, 0x0

    invoke-direct {v4, v5, v6, v7, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213843
    invoke-virtual {v4}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 213844
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    .line 213845
    if-eqz v4, :cond_4

    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    const/4 v5, 0x1

    :goto_2
    move v2, v5

    .line 213846
    if-eqz v2, :cond_0

    .line 213847
    new-instance v2, Ljava/util/Date;

    iget-object v0, p0, LX/1Bj;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v6

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 213848
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 213849
    :cond_2
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213850
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EC;

    .line 213851
    invoke-static {v0, v2, v3, v4}, LX/1Bj;->a(LX/0EC;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 213852
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 213853
    goto :goto_0

    .line 213854
    :catch_0
    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(ILandroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213778
    invoke-direct {p0, p1, p2}, LX/1Bj;->b(ILandroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    .line 213779
    if-nez v0, :cond_0

    .line 213780
    const/4 v0, 0x0

    .line 213781
    :goto_0
    return-object v0

    .line 213782
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 213783
    const/4 v1, 0x0

    .line 213784
    :goto_1
    move-object v0, v1

    .line 213785
    goto :goto_0

    .line 213786
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 213787
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 213788
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0EC;

    .line 213789
    iget-object p1, v1, LX/0EC;->c:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const/16 p2, 0x3d

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v1, v1, LX/0EC;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213790
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v2, v1, :cond_2

    .line 213791
    const/16 v1, 0x3b

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 213792
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 213793
    :cond_3
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 213794
    iget-object v0, p0, LX/1Bj;->a:LX/7hv;

    const/4 v1, 0x0

    .line 213795
    invoke-static {v0, p1}, LX/7hv;->c(LX/7hv;I)Ljava/io/File;

    move-result-object v2

    .line 213796
    if-nez v2, :cond_1

    .line 213797
    :cond_0
    :goto_0
    move-object v0, v1

    .line 213798
    return-object v0

    .line 213799
    :cond_1
    invoke-static {v0}, LX/7hv;->a(LX/7hv;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 213800
    invoke-static {v0, v2, p1}, LX/7hv;->a(LX/7hv;Ljava/io/File;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    .line 213801
    const/4 v2, 0x0

    .line 213802
    :goto_1
    if-eqz p0, :cond_0

    .line 213803
    const/4 v1, 0x0

    .line 213804
    :try_start_0
    const-string v0, "SELECT creation_utc, host_key,name,value,path,expires_utc,secure,httponly FROM cookies"

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 213805
    invoke-static {v0}, LX/7hv;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v1

    .line 213806
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 213807
    :goto_2
    move-object v1, v1

    .line 213808
    if-eqz v2, :cond_0

    .line 213809
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 213810
    :cond_2
    invoke-static {v2}, LX/7hv;->a(Ljava/io/File;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    .line 213811
    const/4 v2, 0x1

    goto :goto_1

    .line 213812
    :catch_0
    goto :goto_2
.end method
