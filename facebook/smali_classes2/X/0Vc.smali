.class public final LX/0Vc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Sg;


# direct methods
.method public constructor <init>(LX/0Sg;)V
    .locals 0

    .prologue
    .line 67947
    iput-object p1, p0, LX/0Vc;->a:LX/0Sg;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    .line 67948
    iget-object v0, p0, LX/0Vc;->a:LX/0Sg;

    iget-object v0, v0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 67949
    :try_start_0
    iget-object v0, p0, LX/0Vc;->a:LX/0Sg;

    const/4 v1, 0x1

    .line 67950
    iput-boolean v1, v0, LX/0Sg;->A:Z

    .line 67951
    iget-object v0, p0, LX/0Vc;->a:LX/0Sg;

    iget-object v0, v0, LX/0Sg;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67952
    iget-object v0, p0, LX/0Vc;->a:LX/0Sg;

    iget-object v0, v0, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 67953
    return-void

    .line 67954
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0Vc;->a:LX/0Sg;

    iget-object v1, v1, LX/0Sg;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 67955
    sget-object v0, LX/0Sg;->a:Ljava/lang/Class;

    const-string v1, "Task failed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67956
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 67946
    return-void
.end method
