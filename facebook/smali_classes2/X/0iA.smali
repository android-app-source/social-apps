.class public LX/0iA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile s:LX/0iA;


# instance fields
.field private final b:LX/117;

.field public final c:LX/119;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/03V;

.field private final f:LX/0SG;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/11A;

.field private final i:LX/0Wd;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Xp;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "LX/16Q;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/16P;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private o:Lcom/facebook/interstitial/manager/InterstitialLogger;

.field private p:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private q:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private r:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119873
    const-class v0, LX/0iA;

    sput-object v0, LX/0iA;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/117;LX/119;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0Wd;LX/0Ot;LX/0Ot;LX/0SG;LX/0Ot;LX/11A;LX/0Ot;)V
    .locals 1
    .param p5    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/117;",
            "LX/119;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Xp;",
            ">;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;",
            "LX/11A;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v0, -0x80000000

    .line 119856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119857
    iput v0, p0, LX/0iA;->p:I

    .line 119858
    iput v0, p0, LX/0iA;->q:I

    .line 119859
    iput-object p1, p0, LX/0iA;->b:LX/117;

    .line 119860
    iput-object p2, p0, LX/0iA;->c:LX/119;

    .line 119861
    iput-object p3, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 119862
    iput-object p4, p0, LX/0iA;->e:LX/03V;

    .line 119863
    iput-object p5, p0, LX/0iA;->i:LX/0Wd;

    .line 119864
    iput-object p6, p0, LX/0iA;->j:LX/0Ot;

    .line 119865
    iput-object p7, p0, LX/0iA;->k:LX/0Ot;

    .line 119866
    iput-object p9, p0, LX/0iA;->g:LX/0Ot;

    .line 119867
    iput-object p10, p0, LX/0iA;->h:LX/11A;

    .line 119868
    iput-object p11, p0, LX/0iA;->l:LX/0Ot;

    .line 119869
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    .line 119870
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0iA;->n:Ljava/util/Map;

    .line 119871
    iput-object p8, p0, LX/0iA;->f:LX/0SG;

    .line 119872
    return-void
.end method

.method public static a(LX/0QB;)LX/0iA;
    .locals 15

    .prologue
    .line 119874
    sget-object v0, LX/0iA;->s:LX/0iA;

    if-nez v0, :cond_1

    .line 119875
    const-class v1, LX/0iA;

    monitor-enter v1

    .line 119876
    :try_start_0
    sget-object v0, LX/0iA;->s:LX/0iA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 119877
    if-eqz v2, :cond_0

    .line 119878
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 119879
    new-instance v3, LX/0iA;

    invoke-static {v0}, LX/116;->b(LX/0QB;)LX/116;

    move-result-object v4

    check-cast v4, LX/117;

    invoke-static {v0}, LX/119;->a(LX/0QB;)LX/119;

    move-result-object v5

    check-cast v5, LX/119;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v8

    check-cast v8, LX/0Wd;

    const/16 v9, 0xb83

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2560

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    const/16 v12, 0x3e5

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/11A;->a(LX/0QB;)LX/11A;

    move-result-object v13

    check-cast v13, LX/11A;

    const/16 v14, 0x103d

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/0iA;-><init>(LX/117;LX/119;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03V;LX/0Wd;LX/0Ot;LX/0Ot;LX/0SG;LX/0Ot;LX/11A;LX/0Ot;)V

    .line 119880
    move-object v0, v3

    .line 119881
    sput-object v0, LX/0iA;->s:LX/0iA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119882
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 119883
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119884
    :cond_1
    sget-object v0, LX/0iA;->s:LX/0iA;

    return-object v0

    .line 119885
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 119886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0iA;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)LX/16Q;
    .locals 2

    .prologue
    .line 119887
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    .line 119888
    if-nez v0, :cond_0

    .line 119889
    new-instance v0, LX/16Q;

    invoke-direct {v0, p1, p2}, LX/16Q;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)V

    .line 119890
    iget-object v1, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119891
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized a(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "LX/16Q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119892
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119893
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119894
    :goto_0
    monitor-exit p0

    return-object v0

    .line 119895
    :cond_1
    :try_start_1
    const-string v0, "InterstitialManager#restoreControllerIdsIfNeeded"

    const v1, 0x60690c2f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 119896
    :try_start_2
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 119897
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119898
    invoke-static {p0, v0}, LX/0iA;->c(LX/0iA;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 119899
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 119900
    :catchall_0
    move-exception v0

    const v1, -0x3f6335c4

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 119901
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119902
    :cond_3
    :try_start_4
    invoke-static {p0, v1}, LX/0iA;->b(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 119903
    const v1, 0x2b8ba07e

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method private declared-synchronized a(LX/0hN;)V
    .locals 2

    .prologue
    .line 119904
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/11b;->h:LX/0Tn;

    invoke-static {p0}, LX/0iA;->h(LX/0iA;)I

    move-result v1

    invoke-interface {p1, v0, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 119905
    const/high16 v0, -0x80000000

    iput v0, p0, LX/0iA;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119906
    monitor-exit p0

    return-void

    .line 119907
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LX/0hN;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "LX/16Q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119908
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 119909
    iget-object v1, p0, LX/0iA;->c:LX/119;

    invoke-virtual {v1, p1, v0}, LX/119;->d(LX/0hN;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    .line 119910
    invoke-static {p1, v0}, LX/119;->c(LX/0hN;Ljava/util/Collection;)V

    .line 119911
    invoke-static {p1, p2}, LX/119;->a(LX/0hN;Ljava/util/Map;)V

    .line 119912
    return-void
.end method

.method private static a(LX/0iA;LX/0hN;Ljava/util/List;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hN;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "LX/16Q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119913
    const-string v0, "InterstitialManager#cacheInterstitialData"

    const v1, 0x6821c1ae

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 119914
    :try_start_0
    iget-object v0, p0, LX/0iA;->c:LX/119;

    invoke-virtual {v0, p1, p2}, LX/119;->a(LX/0hN;Ljava/util/List;)V

    .line 119915
    invoke-direct {p0, p1, p3}, LX/0iA;->a(LX/0hN;Ljava/util/Map;)V

    .line 119916
    invoke-direct {p0, p1}, LX/0iA;->b(LX/0hN;)V

    .line 119917
    invoke-direct {p0, p1}, LX/0iA;->a(LX/0hN;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119918
    const v0, 0x5588375f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 119919
    return-void

    .line 119920
    :catchall_0
    move-exception v0

    const v1, -0x47e817e5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static declared-synchronized a(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z
    .locals 2
    .param p2    # Lcom/facebook/interstitial/manager/InterstitialTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "LX/0i1;",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 119921
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, LX/0iA;->c(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 119922
    if-eqz p3, :cond_0

    .line 119923
    invoke-direct {p0, p2, p3}, LX/0iA;->d(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)V

    .line 119924
    :cond_0
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119925
    if-nez v0, :cond_1

    .line 119926
    const/4 v0, 0x0

    .line 119927
    :goto_0
    monitor-exit p0

    return v0

    .line 119928
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LX/16Q;->a()V

    .line 119929
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/16Q;->a(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 119930
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/0iA;Lcom/facebook/interstitial/api/FetchInterstitialResult;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 119931
    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Lcom/facebook/interstitial/api/FetchInterstitialResult;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v2}, LX/0iA;->b(LX/0iA;Ljava/util/List;)Ljava/util/Set;

    move-result-object v2

    .line 119932
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 119933
    :goto_0
    invoke-static {p0, v2}, LX/0iA;->d(LX/0iA;Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119934
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    .line 119935
    goto :goto_0

    .line 119936
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/16P;)Z
    .locals 6

    .prologue
    .line 119937
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, LX/16P;->d()LX/0Px;

    move-result-object v0

    .line 119938
    iget-object v1, p0, LX/0iA;->e:LX/03V;

    invoke-virtual {p2, p1, v1}, LX/16P;->a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/03V;)Z

    move-result v1

    .line 119939
    if-nez v1, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119940
    const/4 v0, 0x0

    .line 119941
    :goto_0
    monitor-exit p0

    return v0

    .line 119942
    :cond_0
    :try_start_1
    invoke-virtual {p2}, LX/16P;->a()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 119943
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v2

    .line 119944
    invoke-virtual {p2}, LX/16P;->e()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v3

    .line 119945
    invoke-static {v2, v3}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v1

    .line 119946
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 119947
    iget-object v5, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16Q;

    .line 119948
    if-eqz v1, :cond_1

    .line 119949
    iget-object v5, p2, LX/16P;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/16Q;->b(Ljava/lang/String;)Z

    goto :goto_1

    .line 119950
    :cond_2
    invoke-static {v3, v2}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v1

    .line 119951
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 119952
    iget-object v5, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-static {p0, v1, v5}, LX/0iA;->a(LX/0iA;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)LX/16Q;

    move-result-object v1

    .line 119953
    iget v5, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->rank:I

    invoke-virtual {v1, p2, v5}, LX/16Q;->a(LX/16P;I)Z

    .line 119954
    invoke-virtual {v1}, LX/16Q;->c()V

    goto :goto_2

    .line 119955
    :cond_3
    invoke-static {v2, v3}, LX/0RA;->b(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v1

    .line 119956
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 119957
    iget-object v3, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-static {p0, v1, v3}, LX/0iA;->a(LX/0iA;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)LX/16Q;

    move-result-object v1

    .line 119958
    iget v3, p1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->rank:I

    invoke-virtual {v1, p2, v3}, LX/16Q;->b(LX/16P;I)Z

    goto :goto_3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119959
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 119960
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/0iA;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 119990
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 119991
    iget-object v1, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119992
    monitor-exit p0

    return-void

    .line 119993
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "LX/16Q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119961
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119962
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119963
    :goto_0
    monitor-exit p0

    return-object v0

    .line 119964
    :cond_1
    :try_start_1
    const v6, 0x30018

    .line 119965
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119966
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 119967
    const/4 v2, 0x0

    .line 119968
    iget-object v0, p0, LX/0iA;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 119969
    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 119970
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 119971
    iget-object v5, p0, LX/0iA;->c:LX/119;

    const/4 v7, 0x0

    .line 119972
    invoke-static {v1}, LX/11b;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v8

    .line 119973
    iget-object v9, v5, LX/119;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v9, v8, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 119974
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 119975
    :cond_2
    :goto_2
    move-object v5, v7

    .line 119976
    if-eqz v5, :cond_3

    .line 119977
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 119978
    :cond_3
    if-nez v2, :cond_4

    .line 119979
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 119980
    :cond_4
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 119981
    :cond_5
    const/4 v1, 0x2

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 119982
    move-object v0, v3

    .line 119983
    invoke-static {p0, v0}, LX/0iA;->b(LX/0iA;Ljava/util/List;)Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 119984
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119985
    :cond_6
    :try_start_2
    iget-object v9, v5, LX/119;->e:LX/0lC;

    invoke-virtual {v9, v8}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 119986
    if-eqz v8, :cond_2

    .line 119987
    sget-object v9, LX/16M;->CACHE:LX/16M;

    invoke-static {v5, v9, v8}, LX/119;->a(LX/119;LX/16M;LX/0lF;)Lcom/facebook/interstitial/api/FetchInterstitialResult;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v7

    goto :goto_2

    .line 119988
    :catch_0
    move-exception v8

    .line 119989
    iget-object v9, v5, LX/119;->f:LX/03V;

    const-string v10, "InterstitialRepository"

    const-string p1, "Failed to de-serialize interstitial data"

    invoke-virtual {v9, v10, p1, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private static b(LX/0iA;Ljava/util/List;)Ljava/util/Set;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "LX/16Q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120074
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120075
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    .line 120076
    :goto_0
    return-object v0

    .line 120077
    :cond_1
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 120078
    const-string v0, "InterstitialManager#forceRestoreTriggerStateFromInsterstitialResult"

    const v1, -0x1d2a8df

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 120079
    iget-object v0, p0, LX/0iA;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 120080
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    .line 120081
    iget-object v6, v1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    .line 120082
    const v2, 0x30014

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120083
    if-eqz v6, :cond_2

    .line 120084
    const v2, 0x30014

    :try_start_1
    invoke-interface {v0, v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 120085
    :cond_2
    iget-object v2, p0, LX/0iA;->b:LX/117;

    invoke-virtual {v2, v6}, LX/117;->a(Ljava/lang/String;)LX/0i1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 120086
    if-nez v2, :cond_3

    .line 120087
    const v1, 0x30014

    const/4 v2, 0x2

    :try_start_2
    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 120088
    :catchall_0
    move-exception v0

    const v1, 0x379d6733

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 120089
    :cond_3
    :try_start_3
    invoke-direct {p0, v2}, LX/0iA;->c(LX/0i1;)LX/16P;

    move-result-object v7

    .line 120090
    iget-object v4, p0, LX/0iA;->e:LX/03V;

    invoke-virtual {v7, v1, v4}, LX/16P;->a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/03V;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    .line 120091
    if-nez v4, :cond_4

    .line 120092
    const v1, 0x30014

    const/4 v2, 0x2

    :try_start_4
    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 120093
    :cond_4
    :try_start_5
    invoke-interface {v2}, LX/0i1;->c()LX/0Px;

    move-result-object v8

    .line 120094
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v9, :cond_5

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 120095
    invoke-static {p0, v2, v6}, LX/0iA;->a(LX/0iA;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)LX/16Q;

    move-result-object v2

    .line 120096
    iget v10, v1, Lcom/facebook/interstitial/api/FetchInterstitialResult;->rank:I

    invoke-virtual {v2, v7, v10}, LX/16Q;->a(LX/16P;I)Z

    .line 120097
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 120098
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 120099
    :cond_5
    const v1, 0x30014

    const/4 v2, 0x2

    :try_start_6
    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_1

    :catchall_1
    move-exception v1

    const v2, 0x30014

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 120100
    :cond_6
    const v0, -0x1ab300c8

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v3

    .line 120101
    goto/16 :goto_0
.end method

.method private declared-synchronized b(LX/0hN;)V
    .locals 2

    .prologue
    .line 120070
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/11b;->g:LX/0Tn;

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 120071
    const/high16 v0, -0x80000000

    iput v0, p0, LX/0iA;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120072
    monitor-exit p0

    return-void

    .line 120073
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(LX/0i1;)V
    .locals 2

    .prologue
    .line 120061
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120062
    const-string v0, "InterstitialManager#restoreControllersIfNeeded"

    const v1, 0x4369f0d5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 120063
    :try_start_1
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    .line 120064
    invoke-static {p0, v0}, LX/0iA;->c(LX/0iA;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120065
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/0iA;->b(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120066
    :cond_0
    const v0, -0x54207de

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 120067
    monitor-exit p0

    return-void

    .line 120068
    :catchall_0
    move-exception v0

    const v1, -0x1ceb959e

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 120069
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 120045
    invoke-interface {p1, p2}, LX/0i1;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;

    move-result-object v2

    sget-object v3, LX/10S;->ELIGIBLE:LX/10S;

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 120046
    :cond_0
    :goto_0
    return v0

    .line 120047
    :cond_1
    iget-object v2, p0, LX/0iA;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    .line 120048
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    .line 120049
    invoke-interface {p1}, LX/0i1;->a()J

    move-result-wide v8

    .line 120050
    cmp-long v3, v8, v6

    if-lez v3, :cond_0

    .line 120051
    iget-object v3, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3, v2, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 120052
    add-long v2, v6, v8

    cmp-long v2, v4, v2

    if-ltz v2, :cond_3

    .line 120053
    :goto_1
    if-nez v0, :cond_0

    .line 120054
    iget-object v1, p0, LX/0iA;->h:LX/11A;

    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    .line 120055
    iget-object v10, v1, LX/11A;->a:LX/0Zb;

    const-string v11, "interstitial_invalidated_by_min_delay_time"

    const/4 p0, 0x0

    invoke-interface {v10, v11, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v10

    .line 120056
    invoke-virtual {v10}, LX/0oG;->a()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 120057
    const-string v11, "interstitial_id"

    invoke-virtual {v10, v11, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v11

    const-string p0, "trigger"

    invoke-virtual {v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v11, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v11

    const-string p0, "now_ms"

    invoke-virtual {v11, p0, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v11

    const-string p0, "last_impression_timestamp_ms"

    invoke-virtual {v11, p0, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v11

    const-string p0, "min_impression_delay_ms"

    invoke-virtual {v11, p0, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 120058
    invoke-virtual {v10}, LX/0oG;->d()V

    .line 120059
    :cond_2
    goto :goto_0

    :cond_3
    move v0, v1

    .line 120060
    goto :goto_1
.end method

.method private c(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 120102
    const-string v0, "InterstitialManager#getBestInterstitialForTrigger(%s)"

    const v1, 0x10ed0423

    invoke-static {v0, p1, v1}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 120103
    :try_start_0
    invoke-direct {p0, p1}, LX/0iA;->c(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 120104
    invoke-direct {p0, p1, p2}, LX/0iA;->d(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)V

    .line 120105
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120106
    if-nez v0, :cond_0

    .line 120107
    const v0, 0x28d0c3

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-object v3

    .line 120108
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/16Q;->a()V

    .line 120109
    invoke-virtual {v0}, LX/16Q;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 120110
    invoke-virtual {v0}, LX/16P;->b()LX/0i1;

    move-result-object v2

    .line 120111
    if-nez v2, :cond_2

    .line 120112
    sget-object v1, LX/0iA;->a:Ljava/lang/Class;

    const-string v2, "Interstitial with id %s is not initialized!"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, LX/16P;->a:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-static {v1, v2, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 120113
    :catchall_0
    move-exception v0

    const v1, -0x15086561

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 120114
    :cond_2
    :try_start_2
    iget-object v1, p0, LX/0iA;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 120115
    const v5, 0x30013

    invoke-interface {v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 120116
    const v5, 0x30013

    invoke-virtual {p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 120117
    const v5, 0x30013

    iget-object v0, v0, LX/16P;->a:Ljava/lang/String;

    invoke-interface {v1, v5, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 120118
    invoke-static {p0, v2, p1}, LX/0iA;->b(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    .line 120119
    const v5, 0x30013

    const/4 v6, 0x2

    invoke-interface {v1, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 120120
    if-eqz v0, :cond_1

    .line 120121
    invoke-virtual {p2, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v2

    .line 120122
    :goto_2
    const v1, 0x47810e78    # 66076.94f

    invoke-static {v1}, LX/02m;->a(I)V

    move-object v3, v0

    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 120123
    goto :goto_2

    .line 120124
    :cond_4
    const v0, -0x2d1f3e27

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0
.end method

.method private declared-synchronized c(LX/0i1;)LX/16P;
    .locals 3

    .prologue
    .line 120038
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    .line 120039
    iget-object v0, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 120040
    if-nez v0, :cond_0

    .line 120041
    new-instance v0, LX/16P;

    invoke-direct {v0, p1}, LX/16P;-><init>(LX/0i1;)V

    .line 120042
    iget-object v2, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120043
    :cond_0
    monitor-exit p0

    return-object v0

    .line 120044
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 120034
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0iA;->e(LX/0iA;)V

    .line 120035
    invoke-static {p0}, LX/0iA;->d(LX/0iA;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120036
    monitor-exit p0

    return-void

    .line 120037
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(LX/0hN;)V
    .locals 1

    .prologue
    .line 120027
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0iA;->b:LX/117;

    invoke-virtual {v0}, LX/117;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-static {p1, v0}, LX/119;->a(LX/0hN;Ljava/util/Collection;)V

    .line 120028
    iget-object v0, p0, LX/0iA;->c:LX/119;

    invoke-virtual {v0}, LX/119;->b()Ljava/util/List;

    move-result-object v0

    .line 120029
    invoke-static {p1}, LX/119;->a(LX/0hN;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120030
    if-nez v0, :cond_0

    .line 120031
    :goto_0
    monitor-exit p0

    return-void

    .line 120032
    :cond_0
    :try_start_1
    invoke-static {p1, v0}, LX/119;->b(LX/0hN;Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120033
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 8

    .prologue
    .line 119994
    monitor-enter p0

    :try_start_0
    const-string v0, "InterstitialManager#restoreLazyTriggerIds"

    const v1, -0x3063c1e9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119995
    :try_start_1
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    .line 119996
    if-eqz v0, :cond_3

    .line 119997
    iget-boolean v1, v0, LX/16Q;->b:Z

    move v1, v1

    .line 119998
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 119999
    if-eqz v0, :cond_0

    .line 120000
    const v0, 0x55250858

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 120001
    :goto_1
    monitor-exit p0

    return-void

    .line 120002
    :cond_0
    :try_start_3
    invoke-direct {p0}, LX/0iA;->c()V

    .line 120003
    iget-object v0, p0, LX/0iA;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 120004
    const v1, 0x30016

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 120005
    iget-object v1, p0, LX/0iA;->c:LX/119;

    invoke-virtual {v1, p1}, LX/119;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Ljava/util/List;

    move-result-object v1

    .line 120006
    const v2, 0x30016

    invoke-virtual {p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 120007
    const v2, 0x30016

    invoke-static {v1}, LX/119;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 120008
    if-nez v1, :cond_2

    .line 120009
    iget-object v1, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16Q;

    .line 120010
    if-eqz v1, :cond_1

    .line 120011
    iget-boolean v2, v1, LX/16Q;->b:Z

    move v2, v2

    .line 120012
    if-nez v2, :cond_1

    .line 120013
    iget-object v2, p0, LX/0iA;->e:LX/03V;

    const-string v3, "InterstitialManagerBadTriggerMapping"

    const-string v4, "Inconsistent Interstitial Trigger %s state on disk. Debug Info: %s"

    invoke-virtual {v1}, LX/16Q;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, p1, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Could not restore trigger "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 120014
    iget-object v7, v1, LX/16Q;->f:Ljava/lang/Throwable;

    move-object v1, v7

    .line 120015
    invoke-direct {v5, v6, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2, v3, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 120016
    invoke-static {p0}, LX/0iA;->j(LX/0iA;)V

    .line 120017
    const v1, 0x30016

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 120018
    const v0, -0x6969d1d2

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 120019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 120020
    :cond_1
    const v1, 0x30016

    const/4 v2, 0x4

    :try_start_5
    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 120021
    const v0, 0x4089c0bf

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 120022
    :cond_2
    :try_start_7
    const-string v2, "FromTriggerIds"

    invoke-static {p0, p1, v2}, LX/0iA;->a(LX/0iA;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/String;)LX/16Q;

    move-result-object v2

    .line 120023
    invoke-static {p0, v1}, LX/0iA;->a(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;

    .line 120024
    invoke-virtual {v2}, LX/16Q;->c()V

    .line 120025
    const v1, 0x30016

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 120026
    const v0, 0x57885c5f

    :try_start_8
    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    const v1, -0x77b7a4a3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private static declared-synchronized c(LX/0iA;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 119846
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 119847
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/16P;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 119848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/0iA;)V
    .locals 2

    .prologue
    .line 119849
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0iA;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 119850
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 119851
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/0iA;->g(LX/0iA;)I

    move-result v0

    .line 119852
    invoke-static {p0}, LX/0iA;->h(LX/0iA;)I

    move-result v1

    .line 119853
    if-eq v0, v1, :cond_0

    .line 119854
    invoke-virtual {p0}, LX/0iA;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 119855
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/0iA;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/16Q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119686
    monitor-enter p0

    if-nez p1, :cond_1

    .line 119687
    :cond_0
    monitor-exit p0

    return-void

    .line 119688
    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    .line 119689
    invoke-virtual {v0}, LX/16Q;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 119690
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 119691
    iget-object v0, p0, LX/0iA;->b:LX/117;

    iget-object v1, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {v0, v1}, LX/117;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)Ljava/util/Collection;

    move-result-object v0

    .line 119692
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119693
    iget-object v2, p0, LX/0iA;->b:LX/117;

    const-class v3, LX/13E;

    invoke-virtual {v2, v0, v3}, LX/117;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13E;

    .line 119694
    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119695
    invoke-interface {v0, p1}, LX/13E;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    goto :goto_0

    .line 119696
    :cond_1
    return-void
.end method

.method private static declared-synchronized e(LX/0iA;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 119697
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/0iA;->i(LX/0iA;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 119698
    if-ne v1, v0, :cond_0

    .line 119699
    :goto_0
    monitor-exit p0

    return-void

    .line 119700
    :cond_0
    :try_start_1
    const-string v2, "InterstitialManager#maybeUpgradePreviousStore"

    const v3, -0x53e27a6b

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119701
    if-gtz v1, :cond_2

    :goto_1
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "We have no upgrade logic for version "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 119702
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 119703
    if-ltz v1, :cond_1

    if-gtz v1, :cond_1

    .line 119704
    invoke-static {p0}, LX/0iA;->j(LX/0iA;)V

    .line 119705
    invoke-static {p0}, LX/0iA;->f(LX/0iA;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-static {p0, v0, v1, v2}, LX/0iA;->a(LX/0iA;LX/0hN;Ljava/util/List;Ljava/util/Map;)V

    .line 119706
    :cond_1
    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119707
    const v0, -0x31974af5

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 119708
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119709
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 119710
    :catchall_1
    move-exception v0

    const v1, 0x733eadbb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static declared-synchronized f(LX/0iA;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119711
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 119712
    iget-object v0, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 119713
    invoke-virtual {v0}, LX/16P;->c()Lcom/facebook/interstitial/api/FetchInterstitialResult;

    move-result-object v0

    .line 119714
    if-eqz v0, :cond_0

    .line 119715
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 119716
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119717
    :cond_1
    monitor-exit p0

    return-object v1
.end method

.method private static declared-synchronized g(LX/0iA;)I
    .locals 3

    .prologue
    const/high16 v1, -0x80000000

    .line 119718
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0iA;->p:I

    if-ne v0, v1, :cond_0

    .line 119719
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/11b;->h:LX/0Tn;

    const/high16 v2, -0x80000000

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    iput v0, p0, LX/0iA;->p:I

    .line 119720
    :cond_0
    iget v0, p0, LX/0iA;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 119721
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static h(LX/0iA;)I
    .locals 1

    .prologue
    .line 119722
    iget-object v0, p0, LX/0iA;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WV;

    invoke-virtual {v0}, LX/0WV;->b()I

    move-result v0

    return v0
.end method

.method private static declared-synchronized i(LX/0iA;)I
    .locals 3

    .prologue
    .line 119723
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0iA;->q:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 119724
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/11b;->g:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    iput v0, p0, LX/0iA;->q:I

    .line 119725
    :cond_0
    iget v0, p0, LX/0iA;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 119726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/0iA;)V
    .locals 2

    .prologue
    .line 119727
    monitor-enter p0

    :try_start_0
    const-string v0, "InterstitialManager#RestoreInterstitialDataFromPreferences"

    const v1, 0x7928d320

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119728
    :try_start_1
    iget-object v0, p0, LX/0iA;->b:LX/117;

    invoke-virtual {v0}, LX/117;->a()Ljava/util/Collection;

    move-result-object v0

    .line 119729
    invoke-static {p0, v0}, LX/0iA;->a(LX/0iA;Ljava/util/Collection;)Ljava/util/Set;

    .line 119730
    invoke-direct {p0}, LX/0iA;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119731
    const v0, -0x61fa3223

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119732
    monitor-exit p0

    return-void

    .line 119733
    :catchall_0
    move-exception v0

    const v1, 0x397956e5

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 119734
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 119735
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    .line 119736
    invoke-virtual {v0}, LX/16Q;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 119737
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119738
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;
    .locals 1

    .prologue
    .line 119739
    monitor-enter p0

    :try_start_0
    const-class v0, LX/0i1;

    invoke-virtual {p0, p1, v0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 119740
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, LX/0iA;->c(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    .line 119741
    if-eqz v1, :cond_0

    .line 119742
    iget-object v0, p0, LX/0iA;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x230013

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "interstitial="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 119743
    invoke-virtual {p0, v1}, LX/0iA;->a(LX/0i1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119744
    :cond_0
    monitor-exit p0

    return-object v1

    .line 119745
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)LX/0i1;
    .locals 1

    .prologue
    .line 119746
    monitor-enter p0

    :try_start_0
    const-class v0, LX/0i1;

    invoke-virtual {p0, p1, v0}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 119747
    monitor-enter p0

    :try_start_0
    const-string v0, "InterstitialManager#getInterstitialControllerForId"

    const v1, 0x7aa6a62f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119748
    :try_start_1
    iget-object v0, p0, LX/0iA;->b:LX/117;

    invoke-virtual {v0, p1, p2}, LX/117;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 119749
    if-eqz v0, :cond_0

    .line 119750
    invoke-direct {p0, v0}, LX/0iA;->b(LX/0i1;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119751
    :cond_0
    const v1, -0x41239df4

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x398d3ce1

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 119752
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Class;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 119753
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 119754
    if-nez v1, :cond_1

    .line 119755
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {p0, v1, p3, p2}, LX/0iA;->a(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 119756
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Lcom/facebook/interstitial/manager/InterstitialLogger;
    .locals 4

    .prologue
    .line 119757
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0iA;->o:Lcom/facebook/interstitial/manager/InterstitialLogger;

    if-nez v0, :cond_0

    .line 119758
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialLogger;

    iget-object v1, p0, LX/0iA;->i:LX/0Wd;

    iget-object v2, p0, LX/0iA;->j:LX/0Ot;

    iget-object v3, p0, LX/0iA;->k:LX/0Ot;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;-><init>(LX/0iA;LX/0Wd;LX/0Ot;LX/0Ot;)V

    iput-object v0, p0, LX/0iA;->o:Lcom/facebook/interstitial/manager/InterstitialLogger;

    .line 119759
    :cond_0
    iget-object v0, p0, LX/0iA;->o:Lcom/facebook/interstitial/manager/InterstitialLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 119760
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0i1;)V
    .locals 4

    .prologue
    .line 119761
    invoke-interface {p1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0iA;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, v0, v2, v3}, LX/0iA;->a$redex0(LX/0iA;Ljava/lang/String;J)V

    .line 119762
    return-void
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119763
    monitor-enter p0

    if-nez p1, :cond_0

    .line 119764
    :goto_0
    monitor-exit p0

    return-void

    .line 119765
    :cond_0
    :try_start_0
    const-string v0, "InterstitialManager#resetEligibleInterstitialsWithFetchResults"

    const v1, 0x106ff1f8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119766
    :try_start_1
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 119767
    invoke-direct {p0, v0}, LX/0iA;->c(LX/0hN;)V

    .line 119768
    iget-object v1, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 119769
    invoke-static {p0, p1}, LX/0iA;->b(LX/0iA;Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    .line 119770
    invoke-static {p0, v1}, LX/0iA;->d(LX/0iA;Ljava/util/Collection;)V

    .line 119771
    iget-object v1, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-static {p0, v0, p1, v1}, LX/0iA;->a(LX/0iA;LX/0hN;Ljava/util/List;Ljava/util/Map;)V

    .line 119772
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 119773
    iget-object v0, p0, LX/0iA;->h:LX/11A;

    .line 119774
    iget-object v1, v0, LX/11A;->a:LX/0Zb;

    const-string v2, "interstitials_fetch_replaced"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 119775
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119776
    const-string v2, "interstitial_ids"

    invoke-static {p1}, LX/11A;->c(Ljava/util/List;)LX/162;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 119777
    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 119778
    :cond_1
    const v0, -0x60e79aa6

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 119779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119780
    :catchall_1
    move-exception v0

    const v1, 0x6dd3c09c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/interstitial/api/FetchInterstitialResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119781
    monitor-enter p0

    if-nez p2, :cond_0

    .line 119782
    :goto_0
    monitor-exit p0

    return-void

    .line 119783
    :cond_0
    :try_start_0
    const-string v0, "InterstitialManager#updateEligibleInterstitialsWithFetchResults"

    const v1, 0x54dab70e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119784
    :try_start_1
    invoke-static {p0}, LX/0iA;->j(LX/0iA;)V

    .line 119785
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 119786
    invoke-static {p1}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v3

    .line 119787
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;

    .line 119788
    iget-object v1, v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    .line 119789
    iget-object v5, p0, LX/0iA;->b:LX/117;

    invoke-virtual {v5, v1}, LX/117;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v5

    .line 119790
    if-eqz v5, :cond_1

    .line 119791
    iget-object v5, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16P;

    .line 119792
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/16P;->a()Z

    move-result v5

    if-nez v5, :cond_3

    .line 119793
    :cond_2
    invoke-static {p0, v0}, LX/0iA;->a(LX/0iA;Lcom/facebook/interstitial/api/FetchInterstitialResult;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119794
    iget-object v0, v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 119795
    :catchall_0
    move-exception v0

    const v1, 0x44cf8214

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119796
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119797
    :cond_3
    :try_start_3
    invoke-direct {p0, v0, v1}, LX/0iA;->a(Lcom/facebook/interstitial/api/FetchInterstitialResult;LX/16P;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119798
    iget-object v0, v0, Lcom/facebook/interstitial/api/FetchInterstitialResult;->interstitialId:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 119799
    :cond_4
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 119800
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 119801
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119802
    invoke-static {v0}, LX/11b;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    .line 119803
    invoke-interface {v2, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 119804
    iget-object v4, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16P;

    .line 119805
    if-eqz v0, :cond_5

    .line 119806
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 119807
    :cond_6
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16Q;

    .line 119808
    invoke-virtual {v0, v1}, LX/16Q;->a(Ljava/util/Collection;)Z

    goto :goto_3

    .line 119809
    :cond_7
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-static {p0, v2, p2, v0}, LX/0iA;->a(LX/0iA;LX/0hN;Ljava/util/List;Ljava/util/Map;)V

    .line 119810
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 119811
    iget-object v0, p0, LX/0iA;->h:LX/11A;

    .line 119812
    iget-object v1, v0, LX/11A;->a:LX/0Zb;

    const-string v2, "interstitials_fetch_updated"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 119813
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 119814
    const-string v2, "interstitial_ids"

    invoke-static {p2}, LX/11A;->c(Ljava/util/List;)LX/162;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 119815
    invoke-virtual {v1}, LX/0oG;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119816
    :cond_8
    const v0, 0x4cbe2491    # 9.9689608E7f

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 2

    .prologue
    .line 119817
    monitor-enter p0

    :try_start_0
    const-string v0, "InterstitialManager#canShowInterstitialForTrigger"

    const v1, -0x3e64678a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119818
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, p1, p2, v0}, LX/0iA;->a(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, LX/0iA;->b(LX/0iA;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 119819
    :goto_0
    const v1, -0x69ac4f92

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    .line 119820
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 119821
    :catchall_0
    move-exception v0

    const v1, -0x67f14caf

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 119822
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 119823
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0iA;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 119824
    :goto_0
    monitor-exit p0

    return-void

    .line 119825
    :cond_0
    :try_start_1
    const-string v0, "InterstitialManager#forceOnAppUpgrade"

    const v1, -0x40def843

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119826
    :try_start_2
    iget-object v0, p0, LX/0iA;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 119827
    invoke-static {p0}, LX/0iA;->j(LX/0iA;)V

    .line 119828
    iget-object v1, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-direct {p0, v0, v1}, LX/0iA;->a(LX/0hN;Ljava/util/Map;)V

    .line 119829
    invoke-direct {p0, v0}, LX/0iA;->a(LX/0hN;)V

    .line 119830
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 119831
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0iA;->r:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 119832
    const v0, 0x17550103

    :try_start_3
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 119833
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 119834
    :catchall_1
    move-exception v0

    const v1, 0x7df0ce9c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 1

    .prologue
    .line 119835
    monitor-enter p0

    :try_start_0
    const-class v0, LX/0i1;

    invoke-virtual {p0, p1, v0}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 119840
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "InterstitialManager#hasInterstitialForTrigger "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, -0x1ed8367

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 119841
    :try_start_1
    invoke-direct {p0, p1, p2}, LX/0iA;->c(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 119842
    :goto_0
    const v1, 0x32f2da9a

    :try_start_2
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    .line 119843
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 119844
    :catchall_0
    move-exception v0

    const v1, -0x2a59637d

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 119845
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 119836
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0iA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 119837
    iget-object v0, p0, LX/0iA;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119838
    monitor-exit p0

    return-void

    .line 119839
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
