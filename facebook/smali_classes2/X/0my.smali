.class public abstract LX/0my;
.super LX/0mz;
.source ""


# static fields
.field public static final a:LX/0lJ;

.field public static final b:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final _config:LX/0m2;

.field public _dateFormat:Ljava/text/DateFormat;

.field public _keySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _knownSerializers:LX/2Ai;

.field public _nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public _nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final _rootNames:LX/0m1;

.field public final _serializationView:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final _serializerCache:LX/0n0;

.field public final _serializerFactory:LX/0nS;

.field public _unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 133391
    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, LX/0li;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    sput-object v0, LX/0my;->a:LX/0lJ;

    .line 133392
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/FailingSerializer;

    const-string v1, "Null key for a Map not allowed in JSON (use a converting NullKeySerializer?)"

    invoke-direct {v0, v1}, Lcom/fasterxml/jackson/databind/ser/impl/FailingSerializer;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/0my;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133393
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/UnknownSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/impl/UnknownSerializer;-><init>()V

    sput-object v0, LX/0my;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133394
    invoke-direct {p0}, LX/0mz;-><init>()V

    .line 133395
    sget-object v0, LX/0my;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133396
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/NullSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/NullSerializer;

    iput-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133397
    sget-object v0, LX/0my;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133398
    iput-object v1, p0, LX/0my;->_config:LX/0m2;

    .line 133399
    iput-object v1, p0, LX/0my;->_serializerFactory:LX/0nS;

    .line 133400
    new-instance v0, LX/0n0;

    invoke-direct {v0}, LX/0n0;-><init>()V

    iput-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    .line 133401
    iput-object v1, p0, LX/0my;->_knownSerializers:LX/2Ai;

    .line 133402
    new-instance v0, LX/0m1;

    invoke-direct {v0}, LX/0m1;-><init>()V

    iput-object v0, p0, LX/0my;->_rootNames:LX/0m1;

    .line 133403
    iput-object v1, p0, LX/0my;->_serializationView:Ljava/lang/Class;

    .line 133404
    return-void
.end method

.method public constructor <init>(LX/0my;LX/0m2;LX/0nS;)V
    .locals 1

    .prologue
    .line 133405
    invoke-direct {p0}, LX/0mz;-><init>()V

    .line 133406
    sget-object v0, LX/0my;->c:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133407
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/NullSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/NullSerializer;

    iput-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133408
    sget-object v0, LX/0my;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133409
    if-nez p2, :cond_0

    .line 133410
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 133411
    :cond_0
    iput-object p3, p0, LX/0my;->_serializerFactory:LX/0nS;

    .line 133412
    iput-object p2, p0, LX/0my;->_config:LX/0m2;

    .line 133413
    iget-object v0, p1, LX/0my;->_serializerCache:LX/0n0;

    iput-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    .line 133414
    iget-object v0, p1, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133415
    iget-object v0, p1, LX/0my;->_keySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_keySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133416
    iget-object v0, p1, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133417
    iget-object v0, p1, LX/0my;->_nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    iput-object v0, p0, LX/0my;->_nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133418
    iget-object v0, p1, LX/0my;->_rootNames:LX/0m1;

    iput-object v0, p0, LX/0my;->_rootNames:LX/0m1;

    .line 133419
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v0}, LX/0n0;->a()LX/2Ai;

    move-result-object v0

    iput-object v0, p0, LX/0my;->_knownSerializers:LX/2Ai;

    .line 133420
    iget-object v0, p2, LX/0m3;->_view:Ljava/lang/Class;

    move-object v0, v0

    .line 133421
    iput-object v0, p0, LX/0my;->_serializationView:Ljava/lang/Class;

    .line 133422
    return-void
.end method

.method private a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133423
    :try_start_0
    invoke-direct {p0, p1}, LX/0my;->b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133424
    if-eqz v0, :cond_0

    .line 133425
    iget-object v1, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v1, p1, v0, p0}, LX/0n0;->a(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0my;)V

    .line 133426
    :cond_0
    return-object v0

    .line 133427
    :catch_0
    move-exception v0

    .line 133428
    new-instance v1, LX/28E;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133429
    instance-of v0, p1, LX/2B9;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 133430
    check-cast v0, LX/2B9;

    invoke-interface {v0, p0}, LX/2B9;->a(LX/0my;)V

    .line 133431
    :cond_0
    invoke-direct {p0, p1, p2}, LX/0my;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133432
    :try_start_0
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0, p1}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0my;->b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133433
    if-eqz v0, :cond_0

    .line 133434
    iget-object v1, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v1, p1, v0, p0}, LX/0n0;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0my;)V

    .line 133435
    :cond_0
    return-object v0

    .line 133436
    :catch_0
    move-exception v0

    .line 133437
    new-instance v1, LX/28E;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/Object;LX/0lJ;)V
    .locals 3

    .prologue
    .line 133438
    invoke-virtual {p1}, LX/0lJ;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133439
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 133440
    invoke-static {v0}, LX/1Xw;->g(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 133441
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133442
    return-void

    .line 133443
    :cond_0
    new-instance v0, LX/28E;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incompatible types: declared root type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133465
    iget-object v0, p0, LX/0my;->_serializerFactory:LX/0nS;

    invoke-virtual {v0, p0, p1}, LX/0nS;->a(LX/0my;LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133444
    instance-of v0, p1, LX/0nU;

    if-eqz v0, :cond_0

    .line 133445
    check-cast p1, LX/0nU;

    invoke-interface {p1, p0, p2}, LX/0nU;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object p1

    .line 133446
    :cond_0
    return-object p1
.end method

.method private o()Ljava/text/DateFormat;
    .locals 1

    .prologue
    .line 133447
    iget-object v0, p0, LX/0my;->_dateFormat:Ljava/text/DateFormat;

    if-eqz v0, :cond_0

    .line 133448
    iget-object v0, p0, LX/0my;->_dateFormat:Ljava/text/DateFormat;

    .line 133449
    :goto_0
    return-object v0

    .line 133450
    :cond_0
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->o()Ljava/text/DateFormat;

    move-result-object v0

    .line 133451
    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    iput-object v0, p0, LX/0my;->_dateFormat:Ljava/text/DateFormat;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()LX/0m4;
    .locals 1

    .prologue
    .line 133452
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 133453
    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;LX/4pS;)LX/4rX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/4pS",
            "<*>;)",
            "LX/4rX;"
        }
    .end annotation
.end method

.method public final a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133454
    iget-object v0, p0, LX/0my;->_knownSerializers:LX/2Ai;

    invoke-virtual {v0, p1}, LX/2Ai;->b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133455
    if-nez v0, :cond_0

    .line 133456
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v0, p1}, LX/0n0;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133457
    if-nez v0, :cond_0

    .line 133458
    invoke-direct {p0, p1}, LX/0my;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133459
    if-nez v0, :cond_0

    .line 133460
    iget-object v0, p0, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133461
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0, p2}, LX/0my;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0lJ;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Z",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133357
    iget-object v0, p0, LX/0my;->_knownSerializers:LX/2Ai;

    invoke-virtual {v0, p1}, LX/2Ai;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133358
    if-eqz v0, :cond_1

    .line 133359
    :cond_0
    :goto_0
    return-object v0

    .line 133360
    :cond_1
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v0, p1}, LX/0n0;->b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133361
    if-nez v0, :cond_0

    .line 133362
    invoke-virtual {p0, p1, p3}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 133363
    iget-object v0, p0, LX/0my;->_serializerFactory:LX/0nS;

    iget-object v2, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0, v2, p1}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v0

    .line 133364
    if-eqz v0, :cond_2

    .line 133365
    invoke-virtual {v0, p3}, LX/4qz;->a(LX/2Ay;)LX/4qz;

    move-result-object v2

    .line 133366
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/TypeWrappedSerializer;

    invoke-direct {v0, v2, v1}, Lcom/fasterxml/jackson/databind/ser/impl/TypeWrappedSerializer;-><init>(LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 133367
    :goto_1
    if-eqz p2, :cond_0

    .line 133368
    iget-object v1, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v1, p1, v0}, LX/0n0;->a(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133462
    instance-of v0, p1, LX/2B9;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 133463
    check-cast v0, LX/2B9;

    invoke-interface {v0, p0}, LX/2B9;->a(LX/0my;)V

    .line 133464
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133369
    iget-object v0, p0, LX/0my;->_knownSerializers:LX/2Ai;

    invoke-virtual {v0, p1}, LX/2Ai;->b(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133370
    if-nez v0, :cond_0

    .line 133371
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v0, p1}, LX/0n0;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133372
    if-nez v0, :cond_0

    .line 133373
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    iget-object v1, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v1, p1}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0n0;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133374
    if-nez v0, :cond_0

    .line 133375
    invoke-direct {p0, p1}, LX/0my;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133376
    if-nez v0, :cond_0

    .line 133377
    iget-object v0, p0, LX/0my;->_unknownTypeSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133378
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0, p2}, LX/0my;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133379
    iget-object v0, p0, LX/0my;->_knownSerializers:LX/2Ai;

    invoke-virtual {v0, p1}, LX/2Ai;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133380
    if-eqz v0, :cond_1

    .line 133381
    :cond_0
    :goto_0
    return-object v0

    .line 133382
    :cond_1
    iget-object v0, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v0, p1}, LX/0n0;->b(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133383
    if-nez v0, :cond_0

    .line 133384
    invoke-virtual {p0, p1, p3}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 133385
    iget-object v0, p0, LX/0my;->_serializerFactory:LX/0nS;

    iget-object v2, p0, LX/0my;->_config:LX/0m2;

    iget-object v3, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v3, p1}, LX/0m4;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v0

    .line 133386
    if-eqz v0, :cond_2

    .line 133387
    invoke-virtual {v0, p3}, LX/4qz;->a(LX/2Ay;)LX/4qz;

    move-result-object v2

    .line 133388
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/impl/TypeWrappedSerializer;

    invoke-direct {v0, v2, v1}, Lcom/fasterxml/jackson/databind/ser/impl/TypeWrappedSerializer;-><init>(LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 133389
    :goto_1
    if-eqz p2, :cond_0

    .line 133390
    iget-object v1, p0, LX/0my;->_serializerCache:LX/0n0;

    invoke-virtual {v1, p1, v0}, LX/0n0;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(JLX/0nX;)V
    .locals 3

    .prologue
    .line 133321
    sget-object v0, LX/0mt;->WRITE_DATE_KEYS_AS_TIMESTAMPS:LX/0mt;

    invoke-virtual {p0, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133322
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 133323
    :goto_0
    return-void

    .line 133324
    :cond_0
    invoke-direct {p0}, LX/0my;->o()Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0nX;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/0nX;)V
    .locals 2

    .prologue
    .line 133325
    iget-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133326
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 133327
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 133328
    if-nez p1, :cond_0

    .line 133329
    iget-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133330
    invoke-virtual {v0, v2, p2, p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 133331
    :goto_0
    return-void

    .line 133332
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 133333
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, LX/0my;->a(Ljava/lang/Class;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Date;LX/0nX;)V
    .locals 2

    .prologue
    .line 133334
    sget-object v0, LX/0mt;->WRITE_DATES_AS_TIMESTAMPS:LX/0mt;

    invoke-virtual {p0, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133335
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 133336
    :goto_0
    return-void

    .line 133337
    :cond_0
    invoke-direct {p0}, LX/0my;->o()Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/0mt;)Z
    .locals 1

    .prologue
    .line 133338
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0, p1}, LX/0m2;->c(LX/0mt;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/2Ay;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133339
    iget-object v0, p0, LX/0my;->_serializerFactory:LX/0nS;

    iget-object v1, p0, LX/0my;->_config:LX/0m2;

    iget-object v2, p0, LX/0my;->_keySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v0, v1, p1, v2}, LX/0nS;->a(LX/0m2;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 133340
    invoke-direct {p0, v0, p2}, LX/0my;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public final b(Ljava/util/Date;LX/0nX;)V
    .locals 2

    .prologue
    .line 133341
    sget-object v0, LX/0mt;->WRITE_DATE_KEYS_AS_TIMESTAMPS:LX/0mt;

    invoke-virtual {p0, v0}, LX/0my;->a(LX/0mt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133342
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 133343
    :goto_0
    return-void

    .line 133344
    :cond_0
    invoke-direct {p0}, LX/0my;->o()Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()LX/0li;
    .locals 1

    .prologue
    .line 133345
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->n()LX/0li;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0m2;
    .locals 1

    .prologue
    .line 133346
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    return-object v0
.end method

.method public final e()LX/0lU;
    .locals 1

    .prologue
    .line 133347
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/4rN;
    .locals 1

    .prologue
    .line 133348
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    .line 133349
    iget-object p0, v0, LX/0m2;->_filterProvider:LX/4rN;

    move-object v0, p0

    .line 133350
    return-object v0
.end method

.method public final h()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 133351
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->p()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 133352
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->q()Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133353
    iget-object v0, p0, LX/0my;->_nullKeySerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133354
    return-object v0
.end method

.method public final l()Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133355
    iget-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v0, v0

    .line 133356
    return-object v0
.end method
