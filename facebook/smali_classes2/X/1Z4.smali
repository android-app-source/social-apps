.class public LX/1Z4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3nq",
            "<+",
            "Lcom/facebook/components/ComponentView;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zk;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Z5;

.field public final d:[I

.field private final e:LX/1De;

.field private final f:Landroid/app/Activity;

.field private final g:LX/1DR;

.field private final h:Ljava/lang/Runnable;

.field public i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1DR;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274281
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/1Z4;->d:[I

    .line 274282
    const/4 v0, -0x1

    iput v0, p0, LX/1Z4;->i:I

    .line 274283
    iput-object p2, p0, LX/1Z4;->g:LX/1DR;

    .line 274284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    .line 274285
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1Z4;->b:Ljava/util/List;

    .line 274286
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LX/1Z4;->f:Landroid/app/Activity;

    .line 274287
    iget-object v0, p0, LX/1Z4;->f:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 274288
    new-instance v0, Ljava/lang/Error;

    const-string v1, "FloatingComponentsManager requires an activity context"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274289
    :cond_0
    new-instance v0, LX/1Z5;

    iget-object v1, p0, LX/1Z4;->f:Landroid/app/Activity;

    invoke-direct {v0, v1}, LX/1Z5;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1Z4;->c:LX/1Z5;

    .line 274290
    new-instance v0, LX/1De;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1Z4;->e:LX/1De;

    .line 274291
    new-instance v0, Lcom/facebook/feed/floatingcomponents/FloatingComponentsManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/floatingcomponents/FloatingComponentsManager$1;-><init>(LX/1Z4;)V

    iput-object v0, p0, LX/1Z4;->h:Ljava/lang/Runnable;

    .line 274292
    return-void
.end method

.method public static a(LX/0QB;)LX/1Z4;
    .locals 5

    .prologue
    .line 274359
    const-class v1, LX/1Z4;

    monitor-enter v1

    .line 274360
    :try_start_0
    sget-object v0, LX/1Z4;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 274361
    sput-object v2, LX/1Z4;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 274362
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274363
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 274364
    new-instance p0, LX/1Z4;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-direct {p0, v3, v4}, LX/1Z4;-><init>(Landroid/content/Context;LX/1DR;)V

    .line 274365
    move-object v0, p0

    .line 274366
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 274367
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Z4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274368
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 274369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1X1;)LX/1dV;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 274370
    iget-object v0, p0, LX/1Z4;->e:LX/1De;

    invoke-static {v0, p1}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/7zi;)LX/3nq;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Lcom/facebook/components/ComponentView;",
            ">(",
            "LX/7zi",
            "<TV;>;)",
            "LX/3nq",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 274352
    new-instance v0, LX/3nq;

    iget-object v1, p0, LX/1Z4;->f:Landroid/app/Activity;

    iget-object v2, p0, LX/1Z4;->e:LX/1De;

    invoke-virtual {p1, v2}, LX/7zi;->a(Landroid/content/Context;)Lcom/facebook/components/ComponentView;

    move-result-object v2

    .line 274353
    iget-object v3, p1, LX/7zi;->a:Ljava/lang/Class;

    move-object v3, v3

    .line 274354
    invoke-direct {v0, v1, v2, v3}, LX/3nq;-><init>(Landroid/content/Context;Lcom/facebook/components/ComponentView;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 274355
    iget-object v0, p0, LX/1Z4;->c:LX/1Z5;

    invoke-virtual {v0}, LX/1Z5;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 274356
    if-eqz v0, :cond_0

    .line 274357
    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/1Z4;->c:LX/1Z5;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 274358
    :cond_0
    return-void
.end method

.method public final a(LX/1X1;ZLX/1np;LX/1np;)V
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;Z",
            "LX/1np",
            "<",
            "LX/1dV;",
            ">;",
            "LX/1np",
            "<",
            "LX/1no;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 274341
    invoke-direct {p0, p1}, LX/1Z4;->a(LX/1X1;)LX/1dV;

    move-result-object v0

    .line 274342
    new-instance v1, LX/1no;

    invoke-direct {v1}, LX/1no;-><init>()V

    .line 274343
    iget-object v2, p0, LX/1Z4;->g:LX/1DR;

    invoke-virtual {v2}, LX/1DR;->a()I

    move-result v2

    .line 274344
    if-lez v2, :cond_0

    .line 274345
    invoke-virtual {v0, p1}, LX/1dV;->a(LX/1X1;)V

    .line 274346
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, LX/1mh;->a(II)I

    move-result v2

    invoke-static {v4, v4}, LX/1mh;->a(II)I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, LX/1dV;->a(IILX/1no;)V

    .line 274347
    if-eqz p2, :cond_0

    .line 274348
    invoke-virtual {v0}, LX/1dV;->h()V

    .line 274349
    :cond_0
    iput-object v1, p4, LX/1np;->a:Ljava/lang/Object;

    .line 274350
    iput-object v0, p3, LX/1np;->a:Ljava/lang/Object;

    .line 274351
    return-void
.end method

.method public final a(LX/7zk;LX/1dV;)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 274307
    const/4 v1, 0x0

    .line 274308
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    .line 274309
    iget-object v2, p1, LX/7zk;->b:LX/3nq;

    move-object v2, v2

    .line 274310
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274311
    iget-object v0, p1, LX/7zk;->b:LX/3nq;

    move-object v0, v0

    .line 274312
    iget-object v1, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 274313
    :goto_0
    if-nez v0, :cond_1

    .line 274314
    iget-object v0, p1, LX/7zk;->a:LX/7zi;

    move-object v0, v0

    .line 274315
    invoke-direct {p0, v0}, LX/1Z4;->a(LX/7zi;)LX/3nq;

    move-result-object v0

    .line 274316
    iget-object v1, p0, LX/1Z4;->c:LX/1Z5;

    .line 274317
    const/4 v2, 0x0

    .line 274318
    invoke-virtual {v1}, LX/1Z5;->getChildCount()I

    move-result v5

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_0

    .line 274319
    invoke-virtual {v1, v4}, LX/1Z5;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 274320
    const/4 v2, 0x1

    .line 274321
    :cond_0
    move v2, v2

    .line 274322
    if-nez v2, :cond_1

    .line 274323
    invoke-virtual {v1, v0}, LX/1Z5;->addView(Landroid/view/View;)V

    .line 274324
    :cond_1
    iput-object p1, v0, LX/3nq;->a:LX/7zk;

    .line 274325
    invoke-virtual {v0}, LX/3nq;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 274326
    invoke-virtual {p1, v0}, LX/7zk;->a(LX/3nq;)V

    .line 274327
    invoke-virtual {v0, v3}, LX/3nq;->setVisibility(I)V

    .line 274328
    iget-object v1, p0, LX/1Z4;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274329
    iget-object v1, p0, LX/1Z4;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/3nq;->post(Ljava/lang/Runnable;)Z

    .line 274330
    return-void

    .line 274331
    :cond_2
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 274332
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_4

    .line 274333
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nq;

    .line 274334
    iget-object v5, p1, LX/7zk;->a:LX/7zi;

    move-object v5, v5

    .line 274335
    iget-object v6, v5, LX/7zi;->a:Ljava/lang/Class;

    move-object v5, v6

    .line 274336
    iget-object v6, v0, LX/3nq;->b:Ljava/lang/Class;

    invoke-virtual {v6, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    move v0, v6

    .line 274337
    if-eqz v0, :cond_3

    .line 274338
    iget-object v0, p0, LX/1Z4;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3nq;

    goto :goto_0

    .line 274339
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 274340
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 274293
    iget v0, p0, LX/1Z4;->i:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 274294
    iget-object v0, p0, LX/1Z4;->c:LX/1Z5;

    iget-object v1, p0, LX/1Z4;->d:[I

    invoke-virtual {v0, v1}, LX/1Z5;->getLocationOnScreen([I)V

    .line 274295
    iget-object v0, p0, LX/1Z4;->d:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, LX/1Z4;->i:I

    .line 274296
    :cond_0
    iget-object v0, p0, LX/1Z4;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    .line 274297
    iget-object v0, p0, LX/1Z4;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zk;

    .line 274298
    iget-object v4, p0, LX/1Z4;->d:[I

    invoke-virtual {v0, v4}, LX/7zk;->getLocationOnScreen([I)V

    .line 274299
    iget-object v4, p0, LX/1Z4;->d:[I

    aget v4, v4, v2

    if-nez v4, :cond_1

    iget-object v4, p0, LX/1Z4;->d:[I

    aget v4, v4, v7

    if-eqz v4, :cond_2

    .line 274300
    :cond_1
    iget-object v4, p0, LX/1Z4;->d:[I

    aget v4, v4, v2

    .line 274301
    iget-object v5, p0, LX/1Z4;->d:[I

    aget v5, v5, v7

    iget v6, p0, LX/1Z4;->i:I

    sub-int/2addr v5, v6

    .line 274302
    iget-object v6, v0, LX/7zk;->b:LX/3nq;

    move-object v0, v6

    .line 274303
    int-to-float v4, v4

    invoke-virtual {v0, v4}, LX/3nq;->setTranslationX(F)V

    .line 274304
    int-to-float v4, v5

    invoke-virtual {v0, v4}, LX/3nq;->setTranslationY(F)V

    .line 274305
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 274306
    :cond_3
    return-void
.end method
