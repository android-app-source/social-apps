.class public LX/0WN;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76127
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 76128
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Sg;LX/0Uh;)LX/0WP;
    .locals 5
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/LightSharedPrefExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 76129
    const/16 v0, 0x4d4

    invoke-virtual {p3, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76130
    new-instance v0, LX/0WO;

    invoke-direct {v0, p0}, LX/0WO;-><init>(Landroid/content/Context;)V

    .line 76131
    iput-object p1, v0, LX/0WO;->b:Ljava/util/concurrent/Executor;

    .line 76132
    move-object v0, v0

    .line 76133
    invoke-virtual {v0}, LX/0WO;->a()LX/0WP;

    move-result-object v0

    .line 76134
    invoke-static {v1}, LX/0WR;->a(I)V

    .line 76135
    :goto_0
    const-string v1, "LightSharedPreferencesModule-waitForInitialized"

    new-instance v2, Lcom/facebook/prefs/light/LightSharedPreferencesModule$1;

    invoke-direct {v2, v0}, Lcom/facebook/prefs/light/LightSharedPreferencesModule$1;-><init>(LX/0WP;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {p2, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 76136
    return-object v0

    .line 76137
    :cond_0
    new-instance v0, LX/0WO;

    invoke-direct {v0, p0}, LX/0WO;-><init>(Landroid/content/Context;)V

    .line 76138
    iput-object p1, v0, LX/0WO;->b:Ljava/util/concurrent/Executor;

    .line 76139
    move-object v0, v0

    .line 76140
    invoke-virtual {v0}, LX/0WO;->a()LX/0WP;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 76141
    return-void
.end method
