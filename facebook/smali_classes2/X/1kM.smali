.class public final LX/1kM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kL;


# direct methods
.method public constructor <init>(LX/1kL;)V
    .locals 0

    .prologue
    .line 309447
    iput-object p1, p0, LX/1kM;->a:LX/1kL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 309448
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 309449
    check-cast p1, LX/0Px;

    .line 309450
    iget-object v1, p0, LX/1kM;->a:LX/1kL;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 309451
    iput-object v0, v1, LX/1kL;->i:LX/0Px;

    .line 309452
    iget-object v0, p0, LX/1kM;->a:LX/1kL;

    iget-object v0, v0, LX/1kL;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const/16 v2, 0xd

    const-string v3, "fetcher"

    const-class v4, LX/1kL;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    .line 309453
    return-void
.end method
