.class public LX/0nh;
.super LX/0ni;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136498
    invoke-direct {p0}, LX/0ni;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lJ;LX/0mu;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/0mu;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136494
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 136495
    const-class v1, LX/0am;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136496
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;

    invoke-direct {v0, p1}, Lcom/fasterxml/jackson/datatype/guava/deser/GuavaOptionalDeserializer;-><init>(LX/0lJ;)V

    .line 136497
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0ni;->a(LX/0lJ;LX/0mu;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xn;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136481
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 136482
    const-class v1, LX/0P1;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136483
    const-class v1, LX/146;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136484
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSortedMapDeserializer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSortedMapDeserializer;-><init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 136485
    :goto_0
    return-object v0

    .line 136486
    :cond_0
    const-class v1, LX/0Rh;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136487
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableBiMapDeserializer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableBiMapDeserializer;-><init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136488
    :cond_1
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableMapDeserializer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableMapDeserializer;-><init>(LX/1Xn;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136489
    :cond_2
    const-class v1, LX/0Ri;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 136490
    const-class v1, LX/4xV;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136491
    const-class v1, LX/4xW;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136492
    const-class v1, LX/1Ei;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136493
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Xo;",
            "LX/1Xt;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136446
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 136447
    const-class v1, LX/18f;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136448
    const-class v1, LX/3CE;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136449
    const-class v1, LX/2Q6;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136450
    :cond_0
    const-class v1, LX/0Xu;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136451
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/fasterxml/jackson/datatype/guava/deser/MultimapDeserializer;-><init>(LX/1Xo;LX/1Xt;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 136452
    :goto_0
    return-object v0

    .line 136453
    :cond_1
    const-class v1, LX/0kD;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136454
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/267;",
            "LX/4qw;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 136455
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 136456
    const-class v1, LX/0Py;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 136457
    const-class v1, LX/0Px;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136458
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableListDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableListDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 136459
    :goto_0
    return-object v0

    .line 136460
    :cond_0
    const-class v1, LX/4yO;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136461
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableMultisetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136462
    :cond_1
    const-class v1, LX/0Rf;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 136463
    const-class v1, LX/0dW;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 136464
    invoke-virtual {p1}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    .line 136465
    iget-object v2, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v2

    .line 136466
    const-class v2, Ljava/lang/Comparable;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 136467
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not handle ImmutableSortedSet with elements that are not Comparable<?> ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136468
    :cond_2
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSortedSetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSortedSetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136469
    :cond_3
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableSetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136470
    :cond_4
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableListDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/ImmutableListDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136471
    :cond_5
    const-class v1, LX/1M1;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 136472
    const-class v1, LX/4z2;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 136473
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/LinkedHashMultisetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/LinkedHashMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto :goto_0

    .line 136474
    :cond_6
    const-class v1, LX/1Ly;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 136475
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto/16 :goto_0

    .line 136476
    :cond_7
    const-class v1, LX/4xX;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 136477
    const-class v1, LX/51O;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 136478
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/TreeMultisetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/TreeMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto/16 :goto_0

    .line 136479
    :cond_8
    new-instance v0, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;

    invoke-direct {v0, p1, p2, p3}, Lcom/fasterxml/jackson/datatype/guava/deser/HashMultisetDeserializer;-><init>(LX/267;LX/4qw;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    goto/16 :goto_0

    .line 136480
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
