.class public LX/1Vt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/82I;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAP;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAR;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 266690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266691
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 266692
    iput-object v0, p0, LX/1Vt;->b:LX/0Ot;

    .line 266693
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 266694
    iput-object v0, p0, LX/1Vt;->c:LX/0Ot;

    .line 266695
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 266696
    iput-object v0, p0, LX/1Vt;->d:LX/0Ot;

    .line 266697
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vt;
    .locals 8

    .prologue
    .line 266698
    const-class v1, LX/1Vt;

    monitor-enter v1

    .line 266699
    :try_start_0
    sget-object v0, LX/1Vt;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266700
    sput-object v2, LX/1Vt;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266703
    new-instance v3, LX/1Vt;

    invoke-direct {v3}, LX/1Vt;-><init>()V

    .line 266704
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x1cc8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x20a6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x20a7

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    .line 266705
    iput-object v4, v3, LX/1Vt;->a:LX/03V;

    iput-object v5, v3, LX/1Vt;->b:LX/0Ot;

    iput-object v6, v3, LX/1Vt;->c:LX/0Ot;

    iput-object v7, v3, LX/1Vt;->d:LX/0Ot;

    iput-object p0, v3, LX/1Vt;->e:LX/0ad;

    .line 266706
    move-object v0, v3

    .line 266707
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266708
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266709
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266710
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 266711
    const v0, 0x1172f9ae

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 266712
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
