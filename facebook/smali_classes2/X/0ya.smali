.class public LX/0ya;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 165151
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 165152
    return-void
.end method

.method public static a(LX/0Or;)LX/0yh;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/0yh;"
        }
    .end annotation

    .prologue
    .line 165143
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0yh;->DIALTONE:LX/0yh;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0yh;->NORMAL:LX/0yh;

    goto :goto_0
.end method

.method public static a(LX/03R;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)Ljava/lang/Boolean;
    .locals 3
    .param p0    # LX/03R;
        .annotation runtime Lcom/facebook/zero/common/annotations/IsZeroRatingAvailable;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/CurrentlyActiveTokenType;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/03R;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/0yh;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 165145
    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165146
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 165147
    :goto_0
    return-object v0

    .line 165148
    :cond_1
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yh;

    invoke-virtual {v0}, LX/0yh;->getCampaignIdKey()LX/0Tn;

    move-result-object v0

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165149
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yh;

    invoke-virtual {v0}, LX/0yh;->getStatusKey()LX/0Tn;

    move-result-object v0

    const-string v2, ""

    invoke-interface {p1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165150
    invoke-static {v1}, Lcom/facebook/zero/sdk/token/ZeroToken;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 165144
    return-void
.end method
