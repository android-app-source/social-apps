.class public LX/11N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/11N;


# instance fields
.field public final a:LX/0W9;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Zh;


# direct methods
.method public constructor <init>(LX/0W9;LX/0Or;LX/0Zh;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .param p3    # LX/0Zh;
        .annotation runtime Lcom/facebook/http/annotations/ParamsCollectionPoolForFbHttpModule;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0W9;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Zh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 172100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172101
    iput-object p1, p0, LX/11N;->a:LX/0W9;

    .line 172102
    iput-object p2, p0, LX/11N;->b:LX/0Or;

    .line 172103
    iput-object p3, p0, LX/11N;->c:LX/0Zh;

    .line 172104
    return-void
.end method

.method public static a(LX/0QB;)LX/11N;
    .locals 6

    .prologue
    .line 172105
    sget-object v0, LX/11N;->d:LX/11N;

    if-nez v0, :cond_1

    .line 172106
    const-class v1, LX/11N;

    monitor-enter v1

    .line 172107
    :try_start_0
    sget-object v0, LX/11N;->d:LX/11N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 172108
    if-eqz v2, :cond_0

    .line 172109
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 172110
    new-instance v5, LX/11N;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    const/16 v4, 0x15ec

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/11O;->a(LX/0QB;)LX/0Zh;

    move-result-object v4

    check-cast v4, LX/0Zh;

    invoke-direct {v5, v3, p0, v4}, LX/11N;-><init>(LX/0W9;LX/0Or;LX/0Zh;)V

    .line 172111
    move-object v0, v5

    .line 172112
    sput-object v0, LX/11N;->d:LX/11N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172113
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 172114
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 172115
    :cond_1
    sget-object v0, LX/11N;->d:LX/11N;

    return-object v0

    .line 172116
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 172117
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/14N;)LX/0n9;
    .locals 6

    .prologue
    .line 172118
    iget-object v0, p1, LX/14N;->h:LX/0n9;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 172119
    if-eqz v0, :cond_1

    .line 172120
    invoke-virtual {p1}, LX/14N;->j()LX/0n9;

    move-result-object v0

    .line 172121
    :goto_1
    const-string v1, "locale"

    iget-object v2, p0, LX/11N;->a:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v2

    .line 172122
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 172123
    iget-object v1, p0, LX/11N;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 172124
    if-eqz v1, :cond_0

    .line 172125
    const-string v2, "client_country_code"

    .line 172126
    invoke-static {v0, v2, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 172127
    :cond_0
    return-object v0

    .line 172128
    :cond_1
    iget-object v0, p0, LX/11N;->c:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 172129
    invoke-virtual {p1}, LX/14N;->h()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 172130
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 172131
    invoke-static {v1, v5, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 172132
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
