.class public final LX/0bh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0bi;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0bi;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 87207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87208
    iput-object p1, p0, LX/0bh;->a:LX/0QB;

    .line 87209
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 87181
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0bh;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 87183
    packed-switch p2, :pswitch_data_0

    .line 87184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87185
    :pswitch_0
    new-instance v0, LX/03j;

    const/16 v1, 0x1e7

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/03j;-><init>(LX/0Ot;)V

    .line 87186
    move-object v0, v0

    .line 87187
    :goto_0
    return-object v0

    .line 87188
    :pswitch_1
    new-instance v0, LX/0bj;

    const/16 v1, 0x2c0

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0bj;-><init>(LX/0Ot;)V

    .line 87189
    move-object v0, v0

    .line 87190
    goto :goto_0

    .line 87191
    :pswitch_2
    new-instance v0, LX/0bk;

    const/16 v1, 0x1927

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0bk;-><init>(LX/0Ot;)V

    .line 87192
    move-object v0, v0

    .line 87193
    goto :goto_0

    .line 87194
    :pswitch_3
    invoke-static {p1}, LX/0bl;->a(LX/0QB;)LX/0bl;

    move-result-object v0

    goto :goto_0

    .line 87195
    :pswitch_4
    new-instance v0, LX/0bm;

    const/16 v1, 0x479

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0bm;-><init>(LX/0Ot;)V

    .line 87196
    move-object v0, v0

    .line 87197
    goto :goto_0

    .line 87198
    :pswitch_5
    invoke-static {p1}, LX/0bn;->a(LX/0QB;)LX/0bn;

    move-result-object v0

    goto :goto_0

    .line 87199
    :pswitch_6
    invoke-static {p1}, LX/0bo;->a(LX/0QB;)LX/0bo;

    move-result-object v0

    goto :goto_0

    .line 87200
    :pswitch_7
    invoke-static {p1}, LX/0bp;->a(LX/0QB;)LX/0bp;

    move-result-object v0

    goto :goto_0

    .line 87201
    :pswitch_8
    invoke-static {p1}, LX/0bq;->a(LX/0QB;)LX/0bq;

    move-result-object v0

    goto :goto_0

    .line 87202
    :pswitch_9
    invoke-static {p1}, LX/0br;->a(LX/0QB;)LX/0br;

    move-result-object v0

    goto :goto_0

    .line 87203
    :pswitch_a
    invoke-static {p1}, LX/0bs;->a(LX/0QB;)LX/0bs;

    move-result-object v0

    goto :goto_0

    .line 87204
    :pswitch_b
    invoke-static {p1}, LX/0bt;->a(LX/0QB;)LX/0bt;

    move-result-object v0

    goto :goto_0

    .line 87205
    :pswitch_c
    invoke-static {p1}, LX/0bu;->a(LX/0QB;)LX/0bu;

    move-result-object v0

    goto :goto_0

    .line 87206
    :pswitch_d
    invoke-static {p1}, LX/0bv;->a(LX/0QB;)LX/0bv;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 87182
    const/16 v0, 0xe

    return v0
.end method
