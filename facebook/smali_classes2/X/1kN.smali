.class public final LX/1kN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/io/Serializable;",
        ">;",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kL;


# direct methods
.method public constructor <init>(LX/1kL;)V
    .locals 0

    .prologue
    .line 309454
    iput-object p1, p0, LX/1kN;->a:LX/1kL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 309455
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 309456
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 309457
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 309458
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0P1;

    .line 309459
    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v4

    .line 309460
    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v5

    .line 309461
    iget-object v2, p0, LX/1kN;->a:LX/1kL;

    iget-object v2, v2, LX/1kL;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 309462
    iget-object v2, p0, LX/1kN;->a:LX/1kL;

    iget-object v2, v2, LX/1kL;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Axw;

    invoke-virtual {v2}, LX/Axw;->b()LX/0am;

    move-result-object v2

    .line 309463
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v6, v8

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v10, p0, LX/1kN;->a:LX/1kL;

    iget-object v10, v10, LX/1kL;->d:LX/0ad;

    sget v11, LX/1kO;->H:I

    const/16 v12, 0x18

    invoke-interface {v10, v11, v12}, LX/0ad;->a(II)I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v2, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    cmp-long v2, v8, v10

    if-gtz v2, :cond_1

    .line 309464
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 309465
    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 309466
    goto :goto_0

    .line 309467
    :cond_1
    invoke-virtual {v5}, LX/0Py;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 309468
    sget-object v2, LX/1zb;->a:LX/1zb;

    move-object v2, v2

    .line 309469
    invoke-virtual {v1}, LX/0P1;->values()LX/0Py;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1sm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 309470
    sub-long v8, v6, v8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, LX/1kN;->a:LX/1kL;

    iget-object v2, v2, LX/1kL;->d:LX/0ad;

    sget v5, LX/1kO;->L:I

    const/4 v10, 0x6

    invoke-interface {v2, v5, v10}, LX/0ad;->a(II)I

    move-result v2

    int-to-long v10, v2

    invoke-virtual {v1, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    cmp-long v1, v8, v10

    if-gtz v1, :cond_2

    .line 309471
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 309472
    goto :goto_1

    .line 309473
    :cond_2
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 309474
    iget-object v2, p0, LX/1kN;->a:LX/1kL;

    iget-object v2, v2, LX/1kL;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AyR;

    invoke-virtual {v2, v1, v6, v7}, LX/AyR;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;J)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v1

    .line 309475
    if-eqz v1, :cond_3

    .line 309476
    new-instance v2, LX/Ayb;

    invoke-direct {v2, v1}, LX/Ayb;-><init>(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)V

    .line 309477
    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 309478
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 309479
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 309480
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 309481
    goto :goto_1
.end method
