.class public LX/0e9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 91534
    invoke-virtual {p0, p0}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 91535
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 91536
    const-string v2, "fb"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91537
    const-string v0, "FB Hash"

    .line 91538
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 91539
    :cond_1
    const-string v2, "qz"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91540
    const-string v0, "\u103b\u1019\u1014\u1039\u1019\u102c\u1018\u102c\u101e\u102c"

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Locale;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 91541
    const/16 v0, 0x5f

    invoke-static {p0, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 91542
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 91543
    const/16 v0, 0x2d

    invoke-static {p0, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    .line 91544
    :goto_0
    const-string v0, ""

    invoke-static {v2, v0}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91545
    const-string v1, ""

    invoke-static {v2, v3, v1}, LX/0Ph;->a(Ljava/lang/Iterable;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 91546
    const/4 v3, 0x2

    const-string v4, ""

    invoke-static {v2, v3, v4}, LX/0Ph;->a(Ljava/lang/Iterable;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 91547
    new-instance v3, Ljava/util/Locale;

    invoke-direct {v3, v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    :cond_0
    move-object v2, v0

    goto :goto_0
.end method
