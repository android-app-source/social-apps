.class public final LX/0uP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hN;


# instance fields
.field public final synthetic a:LX/0TG;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0TG;)V
    .locals 2

    .prologue
    .line 156356
    iput-object p1, p0, LX/0uP;->a:LX/0TG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156357
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0uP;->b:Ljava/util/Map;

    return-void
.end method

.method private a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    .locals 1

    .prologue
    .line 156352
    iget-object v0, p0, LX/0uP;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156353
    iget-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 156354
    iget-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 156355
    :cond_0
    return-object p0
.end method

.method private a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156349
    iget-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 156350
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    .line 156351
    :cond_0
    iget-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0Tn;)LX/0hN;
    .locals 1

    .prologue
    .line 156320
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0uP;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156321
    iget-object v0, p0, LX/0uP;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156322
    monitor-exit p0

    return-object p0

    .line 156323
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;D)LX/0hN;
    .locals 2

    .prologue
    .line 156348
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;F)LX/0hN;
    .locals 1

    .prologue
    .line 156347
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;I)LX/0hN;
    .locals 1

    .prologue
    .line 156346
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;J)LX/0hN;
    .locals 2

    .prologue
    .line 156345
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Tn;Ljava/lang/String;)LX/0hN;
    .locals 4

    .prologue
    .line 156338
    monitor-enter p0

    if-nez p2, :cond_0

    .line 156339
    :try_start_0
    invoke-virtual {p0, p1}, LX/0uP;->a(LX/0Tn;)LX/0hN;

    .line 156340
    iget-object v0, p0, LX/0uP;->a:LX/0TG;

    .line 156341
    iget-object v1, v0, LX/0TG;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v2, LX/0TG;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p2, "Wrote null pref to "

    invoke-direct {v3, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156342
    :goto_0
    monitor-exit p0

    return-object p0

    .line 156343
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156344
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0Tn;)LX/0hN;
    .locals 3

    .prologue
    .line 156332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0uP;->a:LX/0TG;

    invoke-virtual {v0, p1}, LX/0TG;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 156333
    invoke-direct {p0}, LX/0uP;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 156334
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 156335
    iget-object p1, p0, LX/0uP;->b:Ljava/util/Map;

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156336
    :cond_0
    monitor-exit p0

    return-object p0

    .line 156337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized commit()V
    .locals 4

    .prologue
    .line 156325
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, LX/0uP;->b:Ljava/util/Map;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 156326
    iget-object v0, p0, LX/0uP;->c:Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 156327
    :goto_0
    iget-object v2, p0, LX/0uP;->a:LX/0TG;

    .line 156328
    iget-object v3, v2, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v3, v1, v0}, LX/0Tj;->a(Ljava/util/Map;Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156329
    monitor-exit p0

    return-void

    .line 156330
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, LX/0uP;->c:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized putBoolean(LX/0Tn;Z)LX/0hN;
    .locals 1

    .prologue
    .line 156324
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/0uP;->a(LX/0Tn;Ljava/lang/Object;)LX/0hN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
