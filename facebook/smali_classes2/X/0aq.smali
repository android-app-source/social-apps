.class public LX/0aq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:I

.field private final j:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 85701
    const v0, 0x7fffffff

    invoke-direct {p0, v0, p1}, LX/0aq;-><init>(II)V

    .line 85702
    return-void
.end method

.method private constructor <init>(II)V
    .locals 1

    .prologue
    .line 85825
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0aq;-><init>(III)V

    .line 85826
    return-void
.end method

.method private constructor <init>(III)V
    .locals 4

    .prologue
    .line 85815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85816
    if-gtz p1, :cond_0

    .line 85817
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85818
    :cond_0
    if-gtz p2, :cond_1

    .line 85819
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxEntries <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85820
    :cond_1
    iput p3, p0, LX/0aq;->i:I

    .line 85821
    iput p1, p0, LX/0aq;->c:I

    .line 85822
    iput p2, p0, LX/0aq;->j:I

    .line 85823
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x0

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    .line 85824
    return-void
.end method

.method public static a(LX/0aq;I)V
    .locals 5

    .prologue
    .line 85792
    monitor-enter p0

    .line 85793
    :try_start_0
    iget v0, p0, LX/0aq;->b:I

    if-gt v0, p1, :cond_0

    .line 85794
    monitor-exit p0

    .line 85795
    :goto_0
    return-void

    .line 85796
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85797
    const/4 v0, 0x0

    move v1, v0

    .line 85798
    :goto_1
    monitor-enter p0

    .line 85799
    :try_start_1
    iget v0, p0, LX/0aq;->b:I

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/0aq;->b:I

    if-eqz v0, :cond_2

    .line 85800
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".sizeOf() is reporting inconsistent results!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85801
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 85802
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 85803
    :cond_2
    :try_start_3
    iget v0, p0, LX/0aq;->i:I

    if-lt v1, v0, :cond_3

    iget v0, p0, LX/0aq;->b:I

    if-le v0, p1, :cond_4

    :cond_3
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 85804
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 85805
    :cond_5
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 85806
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 85807
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 85808
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85809
    iget v0, p0, LX/0aq;->b:I

    invoke-direct {p0, v2, v3}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    sub-int/2addr v0, v4

    iput v0, p0, LX/0aq;->b:I

    .line 85810
    iget v0, p0, LX/0aq;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aq;->f:I

    .line 85811
    add-int/lit8 v0, v1, 0x1

    .line 85812
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 85813
    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, LX/0aq;->a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    move v1, v0

    .line 85814
    goto :goto_1
.end method

.method public static b(LX/0aq;I)V
    .locals 5

    .prologue
    .line 85771
    monitor-enter p0

    .line 85772
    :try_start_0
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 85773
    monitor-exit p0

    .line 85774
    :goto_0
    return-void

    .line 85775
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85776
    const/4 v0, 0x0

    move v1, v0

    .line 85777
    :goto_1
    monitor-enter p0

    .line 85778
    :try_start_1
    iget v0, p0, LX/0aq;->i:I

    if-lt v1, v0, :cond_1

    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-le v0, p1, :cond_2

    :cond_1
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 85779
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 85780
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 85781
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 85782
    :cond_3
    :try_start_3
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 85783
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 85784
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 85785
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85786
    iget v0, p0, LX/0aq;->b:I

    invoke-direct {p0, v2, v3}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    sub-int/2addr v0, v4

    iput v0, p0, LX/0aq;->b:I

    .line 85787
    iget v0, p0, LX/0aq;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aq;->f:I

    .line 85788
    add-int/lit8 v0, v1, 0x1

    .line 85789
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 85790
    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, LX/0aq;->a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    move v1, v0

    .line 85791
    goto :goto_1
.end method

.method private c(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 85767
    invoke-virtual {p0, p1, p2}, LX/0aq;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 85768
    if-gez v0, :cond_0

    .line 85769
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Negative size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85770
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 85741
    if-nez p1, :cond_0

    .line 85742
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85743
    :cond_0
    monitor-enter p0

    .line 85744
    :try_start_0
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 85745
    if-eqz v0, :cond_1

    .line 85746
    iget v1, p0, LX/0aq;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/0aq;->g:I

    .line 85747
    monitor-exit p0

    .line 85748
    :goto_0
    return-object v0

    .line 85749
    :cond_1
    iget v0, p0, LX/0aq;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aq;->h:I

    .line 85750
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85751
    const/4 v0, 0x0

    move-object v1, v0

    .line 85752
    if-nez v1, :cond_2

    .line 85753
    const/4 v0, 0x0

    goto :goto_0

    .line 85754
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 85755
    :cond_2
    monitor-enter p0

    .line 85756
    :try_start_2
    iget v0, p0, LX/0aq;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aq;->e:I

    .line 85757
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 85758
    if-eqz v0, :cond_3

    .line 85759
    iget-object v2, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85760
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 85761
    if-eqz v0, :cond_4

    .line 85762
    const/4 v2, 0x0

    invoke-virtual {p0, v2, p1, v1, v0}, LX/0aq;->a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 85763
    :cond_3
    :try_start_3
    iget v2, p0, LX/0aq;->b:I

    invoke-direct {p0, p1, v1}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, LX/0aq;->b:I

    goto :goto_1

    .line 85764
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 85765
    :cond_4
    iget v0, p0, LX/0aq;->c:I

    invoke-static {p0, v0}, LX/0aq;->a(LX/0aq;I)V

    move-object v0, v1

    .line 85766
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 85725
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 85726
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null || value == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85727
    :cond_1
    monitor-enter p0

    .line 85728
    :try_start_0
    iget v0, p0, LX/0aq;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0aq;->d:I

    .line 85729
    iget v0, p0, LX/0aq;->b:I

    invoke-direct {p0, p1, p2}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/0aq;->b:I

    .line 85730
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 85731
    if-eqz v0, :cond_2

    .line 85732
    iget v1, p0, LX/0aq;->b:I

    invoke-direct {p0, p1, v0}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/0aq;->b:I

    .line 85733
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85734
    if-eqz v0, :cond_3

    .line 85735
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1, v0, p2}, LX/0aq;->a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 85736
    :cond_3
    iget v1, p0, LX/0aq;->c:I

    iget v2, p0, LX/0aq;->j:I

    .line 85737
    invoke-static {p0, v1}, LX/0aq;->a(LX/0aq;I)V

    .line 85738
    invoke-static {p0, v2}, LX/0aq;->b(LX/0aq;I)V

    .line 85739
    return-object v0

    .line 85740
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 85723
    const/4 v0, -0x1

    invoke-static {p0, v0}, LX/0aq;->a(LX/0aq;I)V

    .line 85724
    return-void
.end method

.method public a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;TV;TV;)V"
        }
    .end annotation

    .prologue
    .line 85722
    return-void
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 85721
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0aq;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 85720
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 85709
    if-nez p1, :cond_0

    .line 85710
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85711
    :cond_0
    monitor-enter p0

    .line 85712
    :try_start_0
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 85713
    if-eqz v0, :cond_1

    .line 85714
    iget v1, p0, LX/0aq;->b:I

    invoke-direct {p0, p1, v0}, LX/0aq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/0aq;->b:I

    .line 85715
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85716
    if-eqz v0, :cond_2

    .line 85717
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p1, v0, v2}, LX/0aq;->a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 85718
    :cond_2
    return-object v0

    .line 85719
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 85708
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 85707
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, LX/0aq;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 85703
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/0aq;->g:I

    iget v2, p0, LX/0aq;->h:I

    add-int/2addr v1, v2

    .line 85704
    if-eqz v1, :cond_0

    iget v0, p0, LX/0aq;->g:I

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    .line 85705
    :cond_0
    const-string v1, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/0aq;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LX/0aq;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, LX/0aq;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 85706
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
