.class public LX/1hL;
.super Lcom/facebook/proxygen/NetworkStatusMonitor;
.source ""

# interfaces
.implements LX/0YR;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:LX/18b;

.field public final e:LX/0kb;

.field private final f:LX/1Mk;

.field private final g:Landroid/content/Context;

.field private h:LX/0Xl;

.field private i:LX/0Uo;

.field private j:I

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/proxygen/EventBase;IIILX/18b;LX/0kb;LX/0Xl;LX/1Mk;LX/0Uo;Lcom/facebook/proxygen/AnalyticsLogger;Z)V
    .locals 1

    .prologue
    .line 295820
    invoke-direct {p0, p2}, Lcom/facebook/proxygen/NetworkStatusMonitor;-><init>(Lcom/facebook/proxygen/EventBase;)V

    .line 295821
    iput-object p1, p0, LX/1hL;->g:Landroid/content/Context;

    .line 295822
    iput p3, p0, LX/1hL;->a:I

    .line 295823
    iput p4, p0, LX/1hL;->c:I

    .line 295824
    iput p5, p0, LX/1hL;->b:I

    .line 295825
    iput-object p6, p0, LX/1hL;->d:LX/18b;

    .line 295826
    iput-object p7, p0, LX/1hL;->e:LX/0kb;

    .line 295827
    iput-object p8, p0, LX/1hL;->h:LX/0Xl;

    .line 295828
    iput-object p9, p0, LX/1hL;->f:LX/1Mk;

    .line 295829
    iput-object p10, p0, LX/1hL;->i:LX/0Uo;

    .line 295830
    iput-object p11, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mAnalyticsLogger:Lcom/facebook/proxygen/AnalyticsLogger;

    .line 295831
    iput-boolean p12, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mRadioMonitorEnabled:Z

    .line 295832
    iget v0, p0, LX/1hL;->c:I

    .line 295833
    iput v0, p0, Lcom/facebook/proxygen/NetworkStatusMonitor;->mCacheDurationInSeconds:I

    .line 295834
    iget v0, p0, LX/1hL;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->init(I)V

    .line 295835
    iget-object v0, p0, LX/1hL;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1hL;->a(Landroid/net/NetworkInfo;)V

    .line 295836
    invoke-direct {p0}, LX/1hL;->j()V

    .line 295837
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 295791
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 295792
    iget-object v0, p0, LX/1hL;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_time"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1hL;->j:I

    .line 295793
    iget-object v0, p0, LX/1hL;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_time_zone"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1hL;->k:I

    .line 295794
    :goto_0
    return-void

    .line 295795
    :cond_0
    iget-object v0, p0, LX/1hL;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_time"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1hL;->j:I

    .line 295796
    iget-object v0, p0, LX/1hL;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_time_zone"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/1hL;->k:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1hM;
    .locals 48
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 295839
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getNetworkStatus()Lcom/facebook/proxygen/NetworkStatus;

    move-result-object v46

    .line 295840
    if-nez v46, :cond_0

    .line 295841
    const/4 v2, 0x0

    .line 295842
    :goto_0
    return-object v2

    .line 295843
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1hL;->e:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->f()LX/0am;

    move-result-object v2

    .line 295844
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1hL;->d:LX/18b;

    invoke-virtual {v3}, LX/18b;->j()LX/0am;

    move-result-object v3

    .line 295845
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget v2, v0, LX/1hL;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-gez v2, :cond_2

    const/4 v2, 0x1

    move v12, v2

    .line 295846
    :goto_1
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget v4, v0, LX/1hL;->a:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    const/4 v2, 0x1

    move v13, v2

    .line 295847
    :goto_2
    const/4 v5, 0x0

    .line 295848
    const/4 v4, 0x0

    .line 295849
    const/4 v2, 0x0

    .line 295850
    const/4 v3, 0x0

    .line 295851
    move-object/from16 v0, p0

    iget v6, v0, LX/1hL;->l:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    .line 295852
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1hL;->e:LX/0kb;

    invoke-virtual {v5}, LX/0kb;->o()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    .line 295853
    if-eqz v5, :cond_f

    .line 295854
    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v4

    .line 295855
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v3, v6, :cond_1

    .line 295856
    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v2

    .line 295857
    :cond_1
    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v3

    move/from16 v47, v3

    move v3, v2

    move/from16 v2, v47

    .line 295858
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1hL;->e:LX/0kb;

    invoke-virtual {v5}, LX/0kb;->s()I

    move-result v5

    move/from16 v28, v2

    move/from16 v27, v3

    move/from16 v26, v4

    move/from16 v25, v5

    .line 295859
    :goto_4
    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getPriReqInflight()[J

    move-result-object v8

    .line 295860
    const-wide/16 v6, -0x1

    .line 295861
    const-wide/16 v4, -0x1

    .line 295862
    const-wide/16 v2, -0x1

    .line 295863
    if-eqz v8, :cond_d

    array-length v9, v8

    const/4 v10, 0x3

    if-lt v9, v10, :cond_d

    .line 295864
    const/4 v2, 0x2

    aget-wide v6, v8, v2

    .line 295865
    const/4 v2, 0x1

    aget-wide v4, v8, v2

    .line 295866
    const/4 v2, 0x0

    aget-wide v2, v8, v2

    move-wide/from16 v30, v2

    move-wide/from16 v32, v4

    move-wide/from16 v22, v6

    .line 295867
    :goto_5
    new-instance v2, LX/1hM;

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getIngressAvg()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getIngressMax()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getEgressAvg()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getEgressMax()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReadCount()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getWriteCount()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttAvg()J

    move-result-wide v10

    const-wide/16 v14, -0x1

    cmp-long v9, v10, v14

    if-eqz v9, :cond_7

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttAvg()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    :goto_6
    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttMax()J

    move-result-wide v10

    const-wide/16 v14, -0x1

    cmp-long v10, v10, v14

    if-eqz v10, :cond_8

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttMax()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    :goto_7
    const-wide/high16 v14, -0x4010000000000000L    # -1.0

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttStdDev()D

    move-result-wide v16

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->ulp(D)D

    move-result-wide v16

    cmpl-double v11, v14, v16

    if-lez v11, :cond_9

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getRttStdDev()D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    :goto_8
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getOpenedConn()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getClosedConn()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getInflightConn()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getEnqueuedReq()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getDequeuedReq()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getFinishedReq()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getInflightReq()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getInQueueReq()J

    move-result-wide v34

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    const-wide/16 v34, -0x1

    cmp-long v24, v22, v34

    if-eqz v24, :cond_a

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    :goto_9
    const-wide/16 v34, -0x1

    cmp-long v23, v32, v34

    if-eqz v23, :cond_b

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    :goto_a
    const-wide/16 v32, -0x1

    cmp-long v24, v30, v32

    if-eqz v24, :cond_c

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    :goto_b
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1hL;->i:LX/0Uo;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, LX/0Uo;->j()Z

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1hL;->i:LX/0Uo;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, LX/0Uo;->c()J

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1hL;->i:LX/0Uo;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, LX/0Uo;->d()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getMayHaveNetwork()Z

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v32

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getMayHaveIdledMS()J

    move-result-wide v34

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getActiveReadSeconds()J

    move-result-wide v34

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getActiveWriteSeconds()J

    move-result-wide v36

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v35

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwIngressSize()J

    move-result-wide v36

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v36

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwIngressAvg()J

    move-result-wide v38

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwIngressMax()J

    move-result-wide v38

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v38

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwIngressStdDev()D

    move-result-wide v40

    invoke-static/range {v40 .. v41}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v39

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwEgressSize()J

    move-result-wide v40

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v40

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwEgressAvg()J

    move-result-wide v42

    invoke-static/range {v42 .. v43}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v41

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwEgressMax()J

    move-result-wide v42

    invoke-static/range {v42 .. v43}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v42

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getReqBwEgressStdDev()D

    move-result-wide v44

    invoke-static/range {v44 .. v45}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v43

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getLatencyQuality()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getUploadBwQuality()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v46 .. v46}, Lcom/facebook/proxygen/NetworkStatus;->getDownloadBwQuality()Ljava/lang/String;

    move-result-object v46

    invoke-direct/range {v2 .. v46}, LX/1hM;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 295868
    :cond_2
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_1

    .line 295869
    :cond_4
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_2

    :cond_5
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_2

    .line 295870
    :cond_6
    move-object/from16 v0, p0

    iget v6, v0, LX/1hL;->l:I

    if-nez v6, :cond_e

    .line 295871
    move-object/from16 v0, p0

    iget-object v6, v0, LX/1hL;->f:LX/1Mk;

    move-object/from16 v0, p0

    iget v7, v0, LX/1hL;->m:I

    invoke-virtual {v6, v7}, LX/1Mk;->a(I)Landroid/telephony/CellSignalStrength;

    move-result-object v6

    .line 295872
    if-eqz v6, :cond_e

    .line 295873
    invoke-virtual {v6}, Landroid/telephony/CellSignalStrength;->getLevel()I

    move-result v5

    .line 295874
    invoke-virtual {v6}, Landroid/telephony/CellSignalStrength;->getDbm()I

    move-result v4

    move/from16 v28, v3

    move/from16 v27, v2

    move/from16 v26, v4

    move/from16 v25, v5

    goto/16 :goto_4

    .line 295875
    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_6

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_7

    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_8

    :cond_a
    const/16 v22, 0x0

    goto/16 :goto_9

    :cond_b
    const/16 v23, 0x0

    goto/16 :goto_a

    :cond_c
    const/16 v24, 0x0

    goto/16 :goto_b

    :cond_d
    move-wide/from16 v30, v2

    move-wide/from16 v32, v4

    move-wide/from16 v22, v6

    goto/16 :goto_5

    :cond_e
    move/from16 v28, v3

    move/from16 v27, v2

    move/from16 v26, v4

    move/from16 v25, v5

    goto/16 :goto_4

    :cond_f
    move/from16 v47, v3

    move v3, v2

    move/from16 v2, v47

    goto/16 :goto_3
.end method

.method public final a(LX/0YR;)V
    .locals 0

    .prologue
    .line 295838
    return-void
.end method

.method public final a(Landroid/net/NetworkInfo;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 295797
    const/4 v0, -0x1

    iput v0, p0, LX/1hL;->l:I

    .line 295798
    const/4 v0, 0x0

    iput v0, p0, LX/1hL;->m:I

    .line 295799
    sget-object v1, LX/1hN;->NOCONN:LX/1hN;

    .line 295800
    sget-object v0, LX/1hO;->NOT_CELLULAR:LX/1hO;

    .line 295801
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295802
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    iput v1, p0, LX/1hL;->l:I

    .line 295803
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    iput v1, p0, LX/1hL;->m:I

    .line 295804
    iget v1, p0, LX/1hL;->l:I

    sparse-switch v1, :sswitch_data_0

    .line 295805
    sget-object v1, LX/1hN;->OTHER:LX/1hN;

    .line 295806
    :goto_0
    iget v2, p0, LX/1hL;->l:I

    if-nez v2, :cond_0

    .line 295807
    iget v0, p0, LX/1hL;->m:I

    packed-switch v0, :pswitch_data_0

    .line 295808
    sget-object v0, LX/1hO;->UNKNOWN:LX/1hO;

    .line 295809
    :cond_0
    :goto_1
    iget v1, v1, LX/1hN;->value:I

    iget v0, v0, LX/1hO;->value:I

    invoke-virtual {p0, v1, v0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->setNetworkType(II)V

    .line 295810
    return-void

    .line 295811
    :sswitch_0
    sget-object v1, LX/1hN;->WIFI:LX/1hN;

    goto :goto_0

    .line 295812
    :sswitch_1
    sget-object v1, LX/1hN;->CELLULAR:LX/1hN;

    .line 295813
    sget-object v0, LX/1hO;->G4:LX/1hO;

    goto :goto_0

    .line 295814
    :sswitch_2
    sget-object v1, LX/1hN;->CELLULAR:LX/1hN;

    goto :goto_0

    .line 295815
    :pswitch_0
    sget-object v0, LX/1hO;->G2:LX/1hO;

    goto :goto_1

    .line 295816
    :pswitch_1
    sget-object v0, LX/1hO;->G3:LX/1hO;

    goto :goto_1

    .line 295817
    :pswitch_2
    sget-object v0, LX/1hO;->G4:LX/1hO;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b()LX/1hP;
    .locals 3

    .prologue
    .line 295818
    invoke-direct {p0}, LX/1hL;->j()V

    .line 295819
    new-instance v0, LX/1hP;

    iget v1, p0, LX/1hL;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, LX/1hL;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1hP;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 295780
    iget v0, p0, LX/1hL;->a:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 295781
    iget v0, p0, LX/1hL;->c:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 295782
    iget v0, p0, LX/1hL;->b:I

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 295783
    const/4 v0, 0x1

    return v0
.end method

.method public final g()V
    .locals 5

    .prologue
    .line 295784
    new-instance v0, LX/1hQ;

    invoke-direct {v0, p0}, LX/1hQ;-><init>(LX/1hL;)V

    .line 295785
    new-instance v1, LX/1hR;

    invoke-direct {v1, p0}, LX/1hR;-><init>(LX/1hL;)V

    .line 295786
    new-instance v2, LX/1hS;

    invoke-direct {v2, p0}, LX/1hS;-><init>(LX/1hL;)V

    .line 295787
    iget-object v3, p0, LX/1hL;->h:LX/0Xl;

    invoke-interface {v3}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v3, v4, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 295788
    iget-object v0, p0, LX/1hL;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v0, v3, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 295789
    iget-object v0, p0, LX/1hL;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 295790
    return-void
.end method
