.class public final LX/1XU;
.super LX/1Rm;
.source ""


# instance fields
.field public final synthetic a:LX/1XQ;

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/facebook/graphql/model/FeedUnit;

.field public f:I

.field private g:I


# direct methods
.method public constructor <init>(LX/1XQ;LX/0Wd;)V
    .locals 0

    .prologue
    .line 271291
    iput-object p1, p0, LX/1XU;->a:LX/1XQ;

    .line 271292
    invoke-direct {p0, p2}, LX/1Rm;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 271293
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 271294
    :try_start_0
    iget v0, p0, LX/1XU;->g:I

    if-nez v0, :cond_3

    .line 271295
    const/4 v0, 0x0

    iput-object v0, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 271296
    const/4 v0, 0x0

    iput v0, p0, LX/1XU;->f:I

    .line 271297
    iget-object v0, p0, LX/1XU;->a:LX/1XQ;

    iget v1, p0, LX/1XU;->d:I

    invoke-static {v0, v1}, LX/1XQ;->a$redex0(LX/1XQ;I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    iput-object v0, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;

    .line 271298
    iget-object v0, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 271299
    iget v0, p0, LX/1XU;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->d:I

    .line 271300
    iget v0, p0, LX/1XU;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->g:I

    .line 271301
    iget v0, p0, LX/1XU;->g:I

    iget v1, p0, LX/1XU;->f:I

    if-lt v0, v1, :cond_0

    .line 271302
    iput v5, p0, LX/1XU;->g:I

    .line 271303
    :cond_0
    :goto_0
    return-void

    .line 271304
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1XU;->a:LX/1XQ;

    iget-object v0, v0, LX/1XQ;->b:LX/1UQ;

    iget v1, p0, LX/1XU;->d:I

    invoke-interface {v0, v1}, LX/1Qr;->c(I)I

    move-result v0

    iput v0, p0, LX/1XU;->f:I

    .line 271305
    iget-object v0, p0, LX/1XU;->a:LX/1XQ;

    iget-object v0, v0, LX/1XQ;->a:LX/1LV;

    iget-object v1, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v2, 0x0

    .line 271306
    invoke-static {v0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v4

    array-length v6, v4

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v7, v4, v3

    .line 271307
    invoke-interface {v7, v1}, LX/1B3;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 271308
    const/4 v2, 0x1

    .line 271309
    :cond_2
    move v0, v2

    .line 271310
    if-nez v0, :cond_3

    .line 271311
    iget v0, p0, LX/1XU;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1XU;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271312
    iget v0, p0, LX/1XU;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->d:I

    .line 271313
    iget v0, p0, LX/1XU;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->g:I

    .line 271314
    iget v0, p0, LX/1XU;->g:I

    iget v1, p0, LX/1XU;->f:I

    if-lt v0, v1, :cond_0

    .line 271315
    iput v5, p0, LX/1XU;->g:I

    goto :goto_0

    .line 271316
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/1XU;->a:LX/1XQ;

    iget-object v0, v0, LX/1XQ;->h:LX/1XS;

    iget-object v1, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;

    iget v2, p0, LX/1XU;->g:I

    iget v3, p0, LX/1XU;->f:I

    invoke-virtual {v0, v1, v2, v3}, LX/1XS;->b(Lcom/facebook/graphql/model/FeedUnit;II)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 271317
    iget v0, p0, LX/1XU;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->d:I

    .line 271318
    iget v0, p0, LX/1XU;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->g:I

    .line 271319
    iget v0, p0, LX/1XU;->g:I

    iget v1, p0, LX/1XU;->f:I

    if-lt v0, v1, :cond_0

    .line 271320
    iput v5, p0, LX/1XU;->g:I

    goto :goto_0

    .line 271321
    :cond_4
    :try_start_3
    iget-object v0, p0, LX/1XU;->a:LX/1XQ;

    iget-object v0, v0, LX/1XQ;->e:LX/1XT;

    iget-object v1, p0, LX/1XU;->e:Lcom/facebook/graphql/model/FeedUnit;

    iget v2, p0, LX/1XU;->f:I

    iget v3, p0, LX/1XU;->d:I

    iget v4, p0, LX/1XU;->g:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1XT;->a(Lcom/facebook/graphql/model/FeedUnit;III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271322
    iget v0, p0, LX/1XU;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->d:I

    .line 271323
    iget v0, p0, LX/1XU;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1XU;->g:I

    .line 271324
    iget v0, p0, LX/1XU;->g:I

    iget v1, p0, LX/1XU;->f:I

    if-lt v0, v1, :cond_0

    .line 271325
    iput v5, p0, LX/1XU;->g:I

    goto/16 :goto_0

    .line 271326
    :catchall_0
    move-exception v0

    iget v1, p0, LX/1XU;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1XU;->d:I

    .line 271327
    iget v1, p0, LX/1XU;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1XU;->g:I

    .line 271328
    iget v1, p0, LX/1XU;->g:I

    iget v2, p0, LX/1XU;->f:I

    if-lt v1, v2, :cond_5

    .line 271329
    iput v5, p0, LX/1XU;->g:I

    :cond_5
    throw v0

    .line 271330
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 271331
    iget v0, p0, LX/1XU;->d:I

    iget v1, p0, LX/1XU;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
