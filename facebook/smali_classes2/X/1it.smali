.class public LX/1it;
.super LX/1iY;
.source ""


# instance fields
.field private final a:LX/1iu;

.field private b:Z


# direct methods
.method public constructor <init>(LX/1iu;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 299291
    invoke-direct {p0}, LX/1iY;-><init>()V

    .line 299292
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1it;->b:Z

    .line 299293
    iput-object p1, p0, LX/1it;->a:LX/1iu;

    .line 299294
    return-void
.end method


# virtual methods
.method public final a(LX/1iv;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 1
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299295
    iget-boolean v0, p0, LX/1it;->b:Z

    if-eqz v0, :cond_0

    .line 299296
    iget-object v0, p0, LX/1it;->a:LX/1iu;

    invoke-virtual {v0}, LX/1iu;->b()V

    .line 299297
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1it;->b:Z

    .line 299298
    :cond_0
    return-void
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 1

    .prologue
    .line 299299
    iget-object v0, p0, LX/1it;->a:LX/1iu;

    invoke-virtual {v0}, LX/1iu;->a()V

    .line 299300
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1it;->b:Z

    .line 299301
    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 1

    .prologue
    .line 299302
    iget-boolean v0, p0, LX/1it;->b:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 299303
    iget-object v0, p0, LX/1it;->a:LX/1iu;

    invoke-virtual {v0}, LX/1iu;->b()V

    .line 299304
    return-void
.end method
