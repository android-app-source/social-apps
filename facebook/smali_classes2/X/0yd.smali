.class public LX/0yd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field private static volatile e:LX/0yd;


# instance fields
.field public b:LX/0ye;

.field public c:LX/0lC;

.field private final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165681
    const-class v0, LX/0yd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0yd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ye;LX/0lC;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 165682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165683
    iput-object p1, p0, LX/0yd;->b:LX/0ye;

    .line 165684
    iput-object p2, p0, LX/0yd;->c:LX/0lC;

    .line 165685
    iput-object p3, p0, LX/0yd;->d:LX/0Uh;

    .line 165686
    return-void
.end method

.method public static a(LX/0QB;)LX/0yd;
    .locals 6

    .prologue
    .line 165687
    sget-object v0, LX/0yd;->e:LX/0yd;

    if-nez v0, :cond_1

    .line 165688
    const-class v1, LX/0yd;

    monitor-enter v1

    .line 165689
    :try_start_0
    sget-object v0, LX/0yd;->e:LX/0yd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 165690
    if-eqz v2, :cond_0

    .line 165691
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 165692
    new-instance p0, LX/0yd;

    invoke-static {v0}, LX/0ye;->a(LX/0QB;)LX/0ye;

    move-result-object v3

    check-cast v3, LX/0ye;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/0yd;-><init>(LX/0ye;LX/0lC;LX/0Uh;)V

    .line 165693
    move-object v0, p0

    .line 165694
    sput-object v0, LX/0yd;->e:LX/0yd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165695
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 165696
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 165697
    :cond_1
    sget-object v0, LX/0yd;->e:LX/0yd;

    return-object v0

    .line 165698
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 165699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Set;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 165700
    if-nez p0, :cond_0

    move v0, v1

    .line 165701
    :goto_0
    return v0

    .line 165702
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    .line 165703
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165704
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 165705
    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/net/Uri;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 165706
    iget-object v0, p0, LX/0yd;->d:LX/0Uh;

    const/16 v2, 0x67e

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 165707
    :goto_0
    return v0

    .line 165708
    :cond_0
    iget-object v0, p0, LX/0yd;->b:LX/0ye;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0ye;->a(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 165709
    if-nez v2, :cond_1

    move v0, v1

    .line 165710
    goto :goto_0

    .line 165711
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_5

    .line 165712
    iget-object v0, p0, LX/0yd;->b:LX/0ye;

    .line 165713
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 165714
    iget-object v7, v0, LX/0ye;->e:Landroid/util/Pair;

    if-eqz v7, :cond_2

    iget-wide v7, v0, LX/0ye;->b:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_2

    iget-wide v7, v0, LX/0ye;->b:J

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x6ddd00

    cmp-long v5, v5, v7

    if-lez v5, :cond_3

    .line 165715
    :cond_2
    new-instance v5, Landroid/util/Pair;

    iget-object v6, v0, LX/0ye;->g:LX/0W3;

    sget-wide v7, LX/0X5;->bX:J

    sget-object v9, LX/0ye;->n:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-interface {v6, v7, v8, v9}, LX/0W4;->a(JI)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, v0, LX/0ye;->g:LX/0W3;

    sget-wide v9, LX/0X5;->bY:J

    sget-object v8, LX/0ye;->o:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JI)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v5, v0, LX/0ye;->e:Landroid/util/Pair;

    .line 165716
    :cond_3
    iget-object v5, v0, LX/0ye;->e:Landroid/util/Pair;

    move-object v3, v5

    .line 165717
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v4, v0, :cond_4

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_4

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 165718
    goto/16 :goto_0
.end method
