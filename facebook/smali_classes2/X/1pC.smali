.class public LX/1pC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 327914
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x29

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 327915
    sput-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x64

    const-string v2, "Continue"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327916
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x65

    const-string v2, "Switching Protocols"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327917
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc8

    const-string v2, "OK"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327918
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc9

    const-string v2, "Created"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327919
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xca

    const-string v2, "Accepted"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327920
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcb

    const-string v2, "Non-Authoritative Information"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327921
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcc

    const-string v2, "No Content"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327922
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcd

    const-string v2, "Reset Content"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327923
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0xce

    const-string v2, "Partial Content"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327924
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12c

    const-string v2, "Multiple Choices"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327925
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12d

    const-string v2, "Moved Permanently"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327926
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12e

    const-string v2, "Found"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327927
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12f

    const-string v2, "See Other"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327928
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x130

    const-string v2, "Not Modified"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327929
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x131

    const-string v2, "Use Proxy"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327930
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x133

    const-string v2, "Temporary Redirect"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327931
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x190

    const-string v2, "Bad Request"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327932
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x191

    const-string v2, "Unauthorized"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327933
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x192

    const-string v2, "Payment Required"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327934
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x193

    const-string v2, "Forbidden"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327935
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x194

    const-string v2, "Not Found"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327936
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x195

    const-string v2, "Method Not Allowed"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327937
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x196

    const-string v2, "Not Acceptable"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327938
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x197

    const-string v2, "Proxy Authentication Required"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327939
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x198

    const-string v2, "Request Timeout"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327940
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x199

    const-string v2, "Conflict"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327941
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19a

    const-string v2, "Gone"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327942
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19b

    const-string v2, "Length Required"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327943
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19c

    const-string v2, "Precondition Failed"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327944
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19d

    const-string v2, "Payload Too Large"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327945
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19e

    const-string v2, "URI Too Long"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327946
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19f

    const-string v2, "Unsupported Media Type"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327947
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1a0

    const-string v2, "Range Not Satisfiable"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327948
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1a1

    const-string v2, "Expectation Failed"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327949
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1aa

    const-string v2, "Upgrade Required"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327950
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f4

    const-string v2, "Internal Server Error"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327951
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f5

    const-string v2, "Not Implemented"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327952
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f6

    const-string v2, "Bad Gateway"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327953
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f7

    const-string v2, "Service Unavailable"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327954
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f8

    const-string v2, "Gateway Timeout"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327955
    sget-object v0, LX/1pC;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f9

    const-string v2, "HTTP Version Not Supported"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 327956
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 327957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
