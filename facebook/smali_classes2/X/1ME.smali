.class public final enum LX/1ME;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1ME;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1ME;

.field public static final enum BY_ERROR_CODE:LX/1ME;

.field public static final enum BY_EXCEPTION:LX/1ME;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 234942
    new-instance v0, LX/1ME;

    const-string v1, "BY_ERROR_CODE"

    invoke-direct {v0, v1, v2}, LX/1ME;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    .line 234943
    new-instance v0, LX/1ME;

    const-string v1, "BY_EXCEPTION"

    invoke-direct {v0, v1, v3}, LX/1ME;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1ME;->BY_EXCEPTION:LX/1ME;

    .line 234944
    const/4 v0, 0x2

    new-array v0, v0, [LX/1ME;

    sget-object v1, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    aput-object v1, v0, v2

    sget-object v1, LX/1ME;->BY_EXCEPTION:LX/1ME;

    aput-object v1, v0, v3

    sput-object v0, LX/1ME;->$VALUES:[LX/1ME;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 234939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1ME;
    .locals 1

    .prologue
    .line 234941
    const-class v0, LX/1ME;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1ME;

    return-object v0
.end method

.method public static values()[LX/1ME;
    .locals 1

    .prologue
    .line 234940
    sget-object v0, LX/1ME;->$VALUES:[LX/1ME;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1ME;

    return-object v0
.end method
