.class public final LX/1ps;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 329765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)LX/1pt;
    .locals 3

    .prologue
    .line 329766
    new-instance v0, LX/1pt;

    invoke-direct {v0}, LX/1pt;-><init>()V

    .line 329767
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 329768
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 329769
    const/4 v0, 0x0

    .line 329770
    :cond_0
    return-object v0

    .line 329771
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 329772
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 329773
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 329774
    invoke-static {v0, v1, p0}, LX/1ps;->a(LX/1pt;Ljava/lang/String;LX/15w;)Z

    .line 329775
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0
.end method

.method private static a(LX/1pt;Ljava/lang/String;LX/15w;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 329776
    const-string v2, "name"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329777
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_0

    :goto_0
    iput-object v0, p0, LX/1pt;->a:Ljava/lang/String;

    move v0, v1

    .line 329778
    :goto_1
    return v0

    .line 329779
    :cond_0
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 329780
    :cond_1
    const-string v2, "modifier"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 329781
    invoke-virtual {p2}, LX/15w;->F()J

    move-result-wide v2

    iput-wide v2, p0, LX/1pt;->b:J

    move v0, v1

    .line 329782
    goto :goto_1

    .line 329783
    :cond_2
    const-string v2, "type"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 329784
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_3

    :goto_2
    iput-object v0, p0, LX/1pt;->c:Ljava/lang/String;

    move v0, v1

    .line 329785
    goto :goto_1

    .line 329786
    :cond_3
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 329787
    :cond_4
    const-string v2, "callsite"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 329788
    invoke-virtual {p2}, LX/15w;->H()Z

    move-result v0

    iput-boolean v0, p0, LX/1pt;->d:Z

    move v0, v1

    .line 329789
    goto :goto_1

    .line 329790
    :cond_5
    const-string v2, "buckets"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 329791
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_8

    .line 329792
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 329793
    :cond_6
    :goto_3
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_8

    .line 329794
    new-instance v2, LX/5MT;

    invoke-direct {v2}, LX/5MT;-><init>()V

    .line 329795
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_c

    .line 329796
    invoke-virtual {p2}, LX/15w;->f()LX/15w;

    .line 329797
    const/4 v2, 0x0

    .line 329798
    :cond_7
    move-object v2, v2

    .line 329799
    if-eqz v2, :cond_6

    .line 329800
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 329801
    :cond_8
    iput-object v0, p0, LX/1pt;->e:Ljava/util/List;

    move v0, v1

    .line 329802
    goto :goto_1

    .line 329803
    :cond_9
    const-string v2, "override"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 329804
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v2, v3, :cond_a

    :goto_4
    iput-object v0, p0, LX/1pt;->f:Ljava/lang/String;

    move v0, v1

    .line 329805
    goto/16 :goto_1

    .line 329806
    :cond_a
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 329807
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 329808
    :cond_c
    :goto_5
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 329809
    invoke-virtual {p2}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 329810
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    .line 329811
    const/4 v4, 0x0

    .line 329812
    const-string v5, "name"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 329813
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v5, v7, :cond_d

    :goto_6
    iput-object v4, v2, LX/5MT;->a:Ljava/lang/String;

    .line 329814
    :goto_7
    invoke-virtual {p2}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 329815
    :cond_d
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    .line 329816
    :cond_e
    const-string v5, "strategy"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 329817
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v5, v7, :cond_f

    :goto_8
    iput-object v4, v2, LX/5MT;->b:Ljava/lang/String;

    .line 329818
    goto :goto_7

    .line 329819
    :cond_f
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 329820
    :cond_10
    const-string v5, "values"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 329821
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v7, :cond_14

    .line 329822
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 329823
    :cond_11
    :goto_9
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p1, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, p1, :cond_13

    .line 329824
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v7, p1, :cond_12

    move-object v7, v4

    .line 329825
    :goto_a
    if-eqz v7, :cond_11

    .line 329826
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 329827
    :cond_12
    invoke-virtual {p2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    goto :goto_a

    :cond_13
    move-object v4, v5

    .line 329828
    :cond_14
    iput-object v4, v2, LX/5MT;->c:Ljava/util/List;

    .line 329829
    goto :goto_7

    .line 329830
    :cond_15
    goto :goto_7
.end method
