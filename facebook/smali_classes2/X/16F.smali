.class public LX/16F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/16F;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2l6;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0W3;

.field private final c:LX/16H;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/0Uh;

.field private final f:LX/16I;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0W3;LX/16H;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/16I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/2l6;",
            ">;>;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/16H;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/16I;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 184714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184715
    iput-object p1, p0, LX/16F;->a:LX/0Ot;

    .line 184716
    iput-object p2, p0, LX/16F;->b:LX/0W3;

    .line 184717
    iput-object p3, p0, LX/16F;->c:LX/16H;

    .line 184718
    iput-object p4, p0, LX/16F;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 184719
    iput-object p5, p0, LX/16F;->e:LX/0Uh;

    .line 184720
    iput-object p6, p0, LX/16F;->f:LX/16I;

    .line 184721
    return-void
.end method

.method public static a(LX/0QB;)LX/16F;
    .locals 10

    .prologue
    .line 184701
    sget-object v0, LX/16F;->g:LX/16F;

    if-nez v0, :cond_1

    .line 184702
    const-class v1, LX/16F;

    monitor-enter v1

    .line 184703
    :try_start_0
    sget-object v0, LX/16F;->g:LX/16F;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 184704
    if-eqz v2, :cond_0

    .line 184705
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 184706
    new-instance v3, LX/16F;

    invoke-static {v0}, LX/16G;->a(LX/0QB;)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v6

    check-cast v6, LX/16H;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v9

    check-cast v9, LX/16I;

    invoke-direct/range {v3 .. v9}, LX/16F;-><init>(LX/0Ot;LX/0W3;LX/16H;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/16I;)V

    .line 184707
    move-object v0, v3

    .line 184708
    sput-object v0, LX/16F;->g:LX/16F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184709
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 184710
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184711
    :cond_1
    sget-object v0, LX/16F;->g:LX/16F;

    return-object v0

    .line 184712
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 184713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 184699
    iget-object v0, p0, LX/16F;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->e:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 184700
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 184675
    const-class v0, LX/0f0;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f0;

    .line 184676
    if-nez v0, :cond_1

    .line 184677
    invoke-direct {p0, v2}, LX/16F;->a(Z)V

    .line 184678
    :cond_0
    :goto_0
    return-void

    .line 184679
    :cond_1
    const-class v1, LX/0f3;

    invoke-interface {v0, v1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f3;

    .line 184680
    if-eqz v0, :cond_3

    .line 184681
    const/4 v1, 0x0

    invoke-direct {p0, v1}, LX/16F;->a(Z)V

    .line 184682
    new-instance v1, LX/0hs;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 184683
    const/4 v2, -0x1

    .line 184684
    iput v2, v1, LX/0hs;->t:I

    .line 184685
    const v2, 0x7f0208ed

    invoke-virtual {v1, v2}, LX/0hs;->c(I)V

    .line 184686
    invoke-virtual {v1, p2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 184687
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 184688
    invoke-virtual {v1, p3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 184689
    :cond_2
    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-interface {v0, v2, v1}, LX/0f3;->a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V

    .line 184690
    iget-object v0, p0, LX/16F;->c:LX/16H;

    .line 184691
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "saved_bookmark_nux_imp"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "native_newsfeed"

    .line 184692
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 184693
    move-object v1, v1

    .line 184694
    const-string v2, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 184695
    iget-object v2, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 184696
    iget-object v0, p0, LX/16F;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2l6;

    .line 184697
    invoke-interface {v0}, LX/2l6;->a()V

    goto :goto_1

    .line 184698
    :cond_3
    invoke-direct {p0, v2}, LX/16F;->a(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 4

    .prologue
    .line 184665
    iget-object v0, p0, LX/16F;->b:LX/0W3;

    sget-wide v2, LX/0X5;->hj:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1vD;->convertString(Ljava/lang/String;)LX/1vD;

    move-result-object v0

    .line 184666
    sget-object v1, LX/1vD;->INELIGIBLE:LX/1vD;

    invoke-virtual {v1, v0}, LX/1vD;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/16F;->f:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184667
    iget-object v1, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-ne v1, v2, :cond_3

    .line 184668
    iget-object v1, p0, LX/16F;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1vE;->e:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v1, v1

    .line 184669
    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 184670
    iget-object v2, p0, LX/16F;->e:LX/0Uh;

    const/16 p1, 0x60d

    invoke-virtual {v2, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/1vD;->FORCE:LX/1vD;

    invoke-virtual {v2, v0}, LX/1vD;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v1, v1

    .line 184671
    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 184672
    if-eqz v0, :cond_2

    .line 184673
    :cond_1
    const/4 v0, 0x0

    .line 184674
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
