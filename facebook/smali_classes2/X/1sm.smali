.class public abstract LX/1sm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 335286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1sm;[Ljava/lang/Object;III)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>([TE;III)I"
        }
    .end annotation

    .prologue
    .line 335287
    aget-object v1, p1, p4

    .line 335288
    aget-object v0, p1, p3

    aput-object v0, p1, p4

    .line 335289
    aput-object v1, p1, p3

    move v0, p2

    .line 335290
    :goto_0
    if-ge p2, p3, :cond_1

    .line 335291
    aget-object v2, p1, p2

    invoke-virtual {p0, v2, v1}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_0

    .line 335292
    invoke-static {p1, v0, p2}, LX/0P8;->a([Ljava/lang/Object;II)V

    .line 335293
    add-int/lit8 v0, v0, 0x1

    .line 335294
    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 335295
    :cond_1
    invoke-static {p1, p3, v0}, LX/0P8;->a([Ljava/lang/Object;II)V

    .line 335296
    return v0
.end method

.method public static a(Ljava/util/Comparator;)LX/1sm;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<TT;>;)",
            "LX/1sm",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 335297
    instance-of v0, p0, LX/1sm;

    if-eqz v0, :cond_0

    check-cast p0, LX/1sm;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4xK;

    invoke-direct {v0, p0}, LX/4xK;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public a()LX/1sm;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 335298
    new-instance v0, LX/50V;

    invoke-direct {v0, p0}, LX/50V;-><init>(LX/1sm;)V

    return-object v0
.end method

.method public final a(LX/0QK;)LX/1sm;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QK",
            "<TF;+TT;>;)",
            "LX/1sm",
            "<TF;>;"
        }
    .end annotation

    .prologue
    .line 335299
    new-instance v0, LX/1ze;

    invoke-direct {v0, p1, p0}, LX/1ze;-><init>(LX/0QK;LX/1sm;)V

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 335300
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1sm;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(TE;TE;)TE;"
        }
    .end annotation

    .prologue
    .line 335301
    invoke-virtual {p0, p1, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/util/Iterator",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 335302
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 335303
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335304
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1sm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 335305
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable",
            "<TE;>;I)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 335233
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 335234
    check-cast v0, Ljava/util/Collection;

    .line 335235
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0x2

    int-to-long v6, p2

    mul-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 335236
    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 335237
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 335238
    array-length v1, v0

    if-le v1, p2, :cond_0

    .line 335239
    invoke-static {v0, p2}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 335240
    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 335241
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v5, 0x0

    .line 335242
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335243
    const-string v1, "k"

    invoke-static {p2, v1}, LX/0P6;->a(ILjava/lang/String;)I

    .line 335244
    if-eqz p2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    .line 335245
    :cond_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 335246
    :goto_1
    move-object v0, v1

    .line 335247
    goto :goto_0

    .line 335248
    :cond_3
    const v1, 0x3fffffff    # 1.9999999f

    if-lt p2, v1, :cond_5

    .line 335249
    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 335250
    invoke-static {v1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 335251
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p2, :cond_4

    .line 335252
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 335253
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->trimToSize()V

    .line 335254
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 335255
    :cond_5
    mul-int/lit8 v7, p2, 0x2

    .line 335256
    new-array v1, v7, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    .line 335257
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 335258
    aput-object v3, v1, v5

    .line 335259
    const/4 v2, 0x1

    .line 335260
    :goto_2
    if-ge v2, p2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 335261
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 335262
    add-int/lit8 v4, v2, 0x1

    aput-object v6, v1, v2

    .line 335263
    invoke-virtual {p0, v3, v6}, LX/1sm;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move v2, v4

    .line 335264
    goto :goto_2

    :cond_6
    move v2, p2

    .line 335265
    :cond_7
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 335266
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 335267
    invoke-virtual {p0, v6, v3}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_7

    .line 335268
    add-int/lit8 v4, v2, 0x1

    aput-object v6, v1, v2

    .line 335269
    if-ne v4, v7, :cond_b

    .line 335270
    add-int/lit8 v2, v7, -0x1

    move v4, v2

    move v6, v5

    move v2, v5

    .line 335271
    :goto_4
    if-ge v6, v4, :cond_9

    .line 335272
    add-int v3, v6, v4

    add-int/lit8 v3, v3, 0x1

    ushr-int/lit8 v3, v3, 0x1

    .line 335273
    invoke-static {p0, v1, v6, v4, v3}, LX/1sm;->a(LX/1sm;[Ljava/lang/Object;III)I

    move-result v3

    .line 335274
    if-le v3, p2, :cond_8

    .line 335275
    add-int/lit8 v3, v3, -0x1

    move v4, v3

    goto :goto_4

    .line 335276
    :cond_8
    if-ge v3, p2, :cond_9

    .line 335277
    add-int/lit8 v2, v6, 0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v6, v2

    move v2, v3

    .line 335278
    goto :goto_4

    .line 335279
    :cond_9
    aget-object v3, v1, v2

    .line 335280
    add-int/lit8 v2, v2, 0x1

    :goto_5
    if-ge v2, p2, :cond_6

    .line 335281
    aget-object v4, v1, v2

    invoke-virtual {p0, v3, v4}, LX/1sm;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 335282
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 335283
    :cond_a
    invoke-static {v1, v5, v2, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 335284
    invoke-static {v2, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 335285
    invoke-static {v1, v2}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_1

    :cond_b
    move v2, v4

    goto :goto_3
.end method

.method public b()LX/1sm;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 335306
    new-instance v0, LX/50G;

    invoke-direct {v0, p0}, LX/50G;-><init>(LX/1sm;)V

    return-object v0
.end method

.method public final b(Ljava/util/Comparator;)LX/1sm;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:TT;>(",
            "Ljava/util/Comparator",
            "<-TU;>;)",
            "LX/1sm",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 335232
    new-instance v1, LX/209;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, p0, v0}, LX/209;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v1
.end method

.method public b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 335231
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1sm;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(TE;TE;)TE;"
        }
    .end annotation

    .prologue
    .line 335214
    invoke-virtual {p0, p1, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method

.method public b(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/util/Iterator",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 335227
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 335228
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335229
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1sm;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 335230
    :cond_0
    return-object v0
.end method

.method public c()LX/1sm;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 335226
    new-instance v0, LX/50H;

    invoke-direct {v0, p0}, LX/50H;-><init>(LX/1sm;)V

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 335223
    invoke-static {p1}, LX/0Ph;->e(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 335224
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 335225
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation
.end method

.method public final d(Ljava/lang/Iterable;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:TT;>(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 335217
    invoke-static {p1}, LX/0Ph;->e(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 335218
    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 335219
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 335221
    :cond_0
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 335222
    invoke-static {v0}, LX/0Px;->asImmutableList([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:TT;>()",
            "LX/1sm",
            "<",
            "Ljava/util/Map$Entry",
            "<TT2;*>;>;"
        }
    .end annotation

    .prologue
    .line 335215
    sget-object v0, LX/2zy;->KEY:LX/2zy;

    move-object v0, v0

    .line 335216
    invoke-virtual {p0, v0}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v0

    return-object v0
.end method
