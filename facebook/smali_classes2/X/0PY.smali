.class public final LX/0PY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/0eu;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    .line 56805
    check-cast p1, LX/0eu;

    check-cast p2, LX/0eu;

    .line 56806
    iget-wide v6, p1, LX/0eu;->e:J

    move-wide v0, v6

    .line 56807
    iget-wide v6, p1, LX/0eu;->f:J

    move-wide v2, v6

    .line 56808
    sub-long/2addr v0, v2

    .line 56809
    iget-wide v6, p2, LX/0eu;->e:J

    move-wide v2, v6

    .line 56810
    iget-wide v6, p2, LX/0eu;->f:J

    move-wide v4, v6

    .line 56811
    sub-long/2addr v2, v4

    .line 56812
    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    .line 56813
    const/4 v0, -0x1

    .line 56814
    :goto_0
    return v0

    .line 56815
    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 56816
    const/4 v0, 0x1

    goto :goto_0

    .line 56817
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
