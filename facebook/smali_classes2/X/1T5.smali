.class public LX/1T5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1T6;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Ot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 249670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249671
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1T5;->b:Ljava/util/List;

    .line 249672
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1T5;->a:Ljava/util/List;

    .line 249673
    return-void
.end method

.method public static a()LX/1T5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1PW;",
            ">()",
            "LX/1T5",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 249674
    new-instance v0, LX/1T5;

    invoke-direct {v0}, LX/1T5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;LX/0Ot;)LX/1T5;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TP;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<-TP;-TE;>;>;)",
            "LX/1T5",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 249675
    iget-object v0, p0, LX/1T5;->a:Ljava/util/List;

    .line 249676
    new-instance v1, LX/1T6;

    sget-object v2, LX/1T7;->FEED_UNIT:LX/1T7;

    invoke-direct {v1, p1, v2}, LX/1T6;-><init>(Ljava/lang/Class;LX/1T7;)V

    move-object v1, v1

    .line 249677
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249678
    iget-object v0, p0, LX/1T5;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249679
    return-object p0
.end method

.method public final a(LX/1RF;Ljava/lang/Object;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 249680
    instance-of v0, p2, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v2, v0

    :goto_0
    move v3, v4

    .line 249681
    :goto_1
    iget-object v0, p0, LX/1T5;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 249682
    iget-object v0, p0, LX/1T5;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1T6;

    iget-object v0, v0, LX/1T6;->a:Ljava/lang/Class;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 249683
    iget-object v0, p0, LX/1T5;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 249684
    iget-object v1, p0, LX/1T5;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1T6;

    iget-object v1, v1, LX/1T6;->b:LX/1T7;

    sget-object v6, LX/1T7;->FEED_UNIT_PROPS:LX/1T7;

    if-ne v1, v6, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v4, v5

    .line 249685
    :cond_0
    :goto_2
    return v4

    .line 249686
    :cond_1
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0

    .line 249687
    :cond_2
    iget-object v1, p0, LX/1T5;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1T6;

    iget-object v1, v1, LX/1T6;->b:LX/1T7;

    sget-object v6, LX/1T7;->FEED_UNIT:LX/1T7;

    if-ne v1, v6, :cond_3

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v4, v5

    .line 249688
    goto :goto_2

    .line 249689
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Class;LX/0Ot;)LX/1T5;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<*-TE;>;>;)",
            "LX/1T5",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 249690
    iget-object v0, p0, LX/1T5;->a:Ljava/util/List;

    .line 249691
    new-instance v1, LX/1T6;

    sget-object v2, LX/1T7;->FEED_UNIT_PROPS:LX/1T7;

    invoke-direct {v1, p1, v2}, LX/1T6;-><init>(Ljava/lang/Class;LX/1T7;)V

    move-object v1, v1

    .line 249692
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249693
    iget-object v0, p0, LX/1T5;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249694
    return-object p0
.end method
