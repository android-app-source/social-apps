.class public LX/1Ow;
.super LX/0vn;
.source ""


# instance fields
.field public final b:Landroid/support/v7/widget/RecyclerView;

.field public final c:LX/0vn;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 242643
    invoke-direct {p0}, LX/0vn;-><init>()V

    .line 242644
    new-instance v0, LX/1Ox;

    invoke-direct {v0, p0}, LX/1Ox;-><init>(LX/1Ow;)V

    iput-object v0, p0, LX/1Ow;->c:LX/0vn;

    .line 242645
    iput-object p1, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    .line 242646
    return-void
.end method

.method public static c(LX/1Ow;)Z
    .locals 1

    .prologue
    .line 242642
    iget-object v0, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->i()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 1

    .prologue
    .line 242637
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 242638
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 242639
    invoke-static {p0}, LX/1Ow;->c(LX/1Ow;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242640
    iget-object v0, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1OR;->a(LX/3sp;)V

    .line 242641
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 242631
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242632
    const/4 v0, 0x1

    .line 242633
    :goto_0
    return v0

    .line 242634
    :cond_0
    invoke-static {p0}, LX/1Ow;->c(LX/1Ow;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 242635
    iget-object v0, p0, LX/1Ow;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/1OR;->a(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0

    .line 242636
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 242624
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 242625
    const-class v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 242626
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1Ow;->c(LX/1Ow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242627
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 242628
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242629
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1OR;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 242630
    :cond_0
    return-void
.end method
