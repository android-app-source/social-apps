.class public final LX/1l5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Rx;


# direct methods
.method public constructor <init>(LX/1Rx;)V
    .locals 0

    .prologue
    .line 311035
    iput-object p1, p0, LX/1l5;->a:LX/1Rx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 311036
    check-cast p1, Ljava/util/List;

    .line 311037
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311038
    invoke-static {p1}, LX/0Ph;->f(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
