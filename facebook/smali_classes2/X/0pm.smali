.class public LX/0pm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0pm;


# instance fields
.field private final a:LX/0pn;

.field private final b:LX/0qU;

.field private final c:LX/0qV;


# direct methods
.method public constructor <init>(LX/0pn;LX/0qU;LX/0qV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 145265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145266
    iput-object p1, p0, LX/0pm;->a:LX/0pn;

    .line 145267
    iput-object p2, p0, LX/0pm;->b:LX/0qU;

    .line 145268
    iput-object p3, p0, LX/0pm;->c:LX/0qV;

    .line 145269
    return-void
.end method

.method public static a(LX/0QB;)LX/0pm;
    .locals 6

    .prologue
    .line 145252
    sget-object v0, LX/0pm;->d:LX/0pm;

    if-nez v0, :cond_1

    .line 145253
    const-class v1, LX/0pm;

    monitor-enter v1

    .line 145254
    :try_start_0
    sget-object v0, LX/0pm;->d:LX/0pm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 145255
    if-eqz v2, :cond_0

    .line 145256
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 145257
    new-instance p0, LX/0pm;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v3

    check-cast v3, LX/0pn;

    invoke-static {v0}, LX/0qU;->b(LX/0QB;)LX/0qU;

    move-result-object v4

    check-cast v4, LX/0qU;

    invoke-static {v0}, LX/0qV;->a(LX/0QB;)LX/0qV;

    move-result-object v5

    check-cast v5, LX/0qV;

    invoke-direct {p0, v3, v4, v5}, LX/0pm;-><init>(LX/0pn;LX/0qU;LX/0qV;)V

    .line 145258
    move-object v0, p0

    .line 145259
    sput-object v0, LX/0pm;->d:LX/0pm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145260
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 145261
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145262
    :cond_1
    sget-object v0, LX/0pm;->d:LX/0pm;

    return-object v0

    .line 145263
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 145264
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 7

    .prologue
    .line 145235
    const-string v0, "FeedFetcherProcessor.processFeedResult"

    const v1, 0x45cbb414

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 145236
    :try_start_0
    invoke-virtual {p0, p1}, LX/0pm;->b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v2

    .line 145237
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145238
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v3, v1

    .line 145239
    invoke-virtual {v2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 145240
    iget-object v6, p0, LX/0pm;->c:LX/0qV;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-virtual {v6, v0, v3}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    .line 145241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 145242
    :cond_0
    invoke-static {v2}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145243
    const v0, -0x7daf0c62

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v2

    :catchall_0
    move-exception v0

    const v1, -0xb68194d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 2

    .prologue
    .line 145244
    const-string v0, "FeedFetcherProcessor.filterFeedResult"

    const v1, -0xae75c23

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 145245
    :try_start_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145246
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v1

    .line 145247
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v1, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 145248
    :goto_0
    iget-object v1, p0, LX/0pm;->b:LX/0qU;

    invoke-virtual {v1, p1, v0}, LX/0qU;->a(Lcom/facebook/api/feed/FetchFeedResult;Z)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 145249
    const v1, 0x2192273e

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 145250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 145251
    :catchall_0
    move-exception v0

    const v1, 0x3a31d9b0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
