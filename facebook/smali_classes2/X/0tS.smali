.class public LX/0tS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Landroid/util/DisplayMetrics;

.field private final c:LX/0hB;

.field private final d:LX/0fO;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/0fO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154647
    iput-object p1, p0, LX/0tS;->a:Landroid/content/Context;

    .line 154648
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, LX/0tS;->b:Landroid/util/DisplayMetrics;

    .line 154649
    iput-object p2, p0, LX/0tS;->c:LX/0hB;

    .line 154650
    iput-object p3, p0, LX/0tS;->d:LX/0fO;

    .line 154651
    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 154652
    const/high16 v0, 0x3f400000    # 0.75f

    int-to-float v1, p0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(LX/0QB;)LX/0tS;
    .locals 1

    .prologue
    .line 154634
    invoke-static {p0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0tS;
    .locals 4

    .prologue
    .line 154644
    new-instance v3, LX/0tS;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v2

    check-cast v2, LX/0fO;

    invoke-direct {v3, v0, v1, v2}, LX/0tS;-><init>(Landroid/content/Context;LX/0hB;LX/0fO;)V

    .line 154645
    return-object v3
.end method

.method public static e(LX/0tS;)V
    .locals 2

    .prologue
    .line 154653
    iget-object v0, p0, LX/0tS;->a:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, LX/0tS;->b:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 154654
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 2

    .prologue
    .line 154642
    invoke-static {p0}, LX/0tS;->e(LX/0tS;)V

    .line 154643
    iget-object v0, p0, LX/0tS;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    invoke-virtual {p0}, LX/0tS;->d()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 154640
    invoke-static {p0}, LX/0tS;->e(LX/0tS;)V

    .line 154641
    iget-object v0, p0, LX/0tS;->b:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    const v1, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 154635
    iget-object v0, p0, LX/0tS;->d:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154636
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/0tS;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->e()I

    move-result v0

    .line 154637
    :goto_0
    return v0

    .line 154638
    :cond_0
    iget-object v0, p0, LX/0tS;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    goto :goto_0

    .line 154639
    :cond_1
    iget-object v0, p0, LX/0tS;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    goto :goto_0
.end method
