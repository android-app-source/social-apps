.class public abstract LX/1RF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/multirow/api/MultiRowSubParts",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 245831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)Z"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;)Z"
        }
    .end annotation
.end method

.method public a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 245825
    instance-of v0, p1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v0, :cond_0

    .line 245826
    check-cast p1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-virtual {p0, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z

    move-result v0

    .line 245827
    :goto_0
    return v0

    .line 245828
    :cond_0
    instance-of v0, p1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v0, :cond_1

    .line 245829
    check-cast p1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p0, p1, p2}, LX/1RF;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 245830
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown MultiRowPart "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ZLX/0Ot;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(Z",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 245816
    if-nez p1, :cond_0

    .line 245817
    const/4 v0, 0x0

    .line 245818
    :goto_0
    return v0

    .line 245819
    :cond_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 245820
    instance-of v1, v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v1, :cond_1

    .line 245821
    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-virtual {p0, v0, p3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 245822
    :cond_1
    instance-of v1, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v1, :cond_2

    .line 245823
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p0, v0, p3}, LX/1RF;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 245824
    :cond_2
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown MultiRowPart "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
