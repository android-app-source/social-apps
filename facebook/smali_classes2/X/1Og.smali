.class public LX/1Og;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Object;

.field public b:LX/1Oj;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 241974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241975
    const/16 v0, 0xe

    if-lt p1, v0, :cond_0

    .line 241976
    new-instance v0, LX/1Oh;

    invoke-direct {v0}, LX/1Oh;-><init>()V

    iput-object v0, p0, LX/1Og;->b:LX/1Oj;

    .line 241977
    :goto_0
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    invoke-interface {v0, p2, p3}, LX/1Oj;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/1Og;->a:Ljava/lang/Object;

    .line 241978
    return-void

    .line 241979
    :cond_0
    const/16 v0, 0x9

    if-lt p1, v0, :cond_1

    .line 241980
    new-instance v0, LX/1Oi;

    invoke-direct {v0}, LX/1Oi;-><init>()V

    iput-object v0, p0, LX/1Og;->b:LX/1Oj;

    goto :goto_0

    .line 241981
    :cond_1
    new-instance v0, LX/3tk;

    invoke-direct {v0}, LX/3tk;-><init>()V

    iput-object v0, p0, LX/1Og;->b:LX/1Oj;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 241972
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, v0, p1, p2}, LX/1Og;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 241973
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/1Og;
    .locals 1

    .prologue
    .line 241971
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1Og;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;
    .locals 1

    .prologue
    .line 241970
    new-instance v0, LX/1Og;

    invoke-direct {v0, p0, p1}, LX/1Og;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 6

    .prologue
    .line 241968
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, LX/1Oj;->a(Ljava/lang/Object;IIII)V

    .line 241969
    return-void
.end method

.method public final a(IIIII)V
    .locals 7

    .prologue
    .line 241966
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, LX/1Oj;->a(Ljava/lang/Object;IIIII)V

    .line 241967
    return-void
.end method

.method public final a(IIIIIIII)V
    .locals 10

    .prologue
    .line 241961
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-interface/range {v0 .. v9}, LX/1Oj;->a(Ljava/lang/Object;IIIIIIII)V

    .line 241962
    return-void
.end method

.method public final a(IIIIIIIIII)V
    .locals 12

    .prologue
    .line 241964
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-interface/range {v0 .. v11}, LX/1Oj;->a(Ljava/lang/Object;IIIIIIIIII)V

    .line 241965
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 241982
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 241963
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 241960
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 241959
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->g(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 241958
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->h(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final f()F
    .locals 2

    .prologue
    .line 241957
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->d(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 241956
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->e(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 241954
    iget-object v0, p0, LX/1Og;->b:LX/1Oj;

    iget-object v1, p0, LX/1Og;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/1Oj;->f(Ljava/lang/Object;)V

    .line 241955
    return-void
.end method
