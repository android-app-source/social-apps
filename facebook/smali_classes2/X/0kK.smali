.class public LX/0kK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/0kK;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0kN;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:LX/0kL;

.field public g:Landroid/content/Context;

.field public h:LX/0gc;

.field public i:LX/6G9;

.field public j:LX/6GC;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0kL;LX/0Ot;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/bugreporter/annotations/IsRageShakeAvailable;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0kL;",
            "LX/0Ot",
            "<",
            "LX/1fX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 126980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126981
    iput-object p1, p0, LX/0kK;->a:LX/0Or;

    .line 126982
    iput-object p2, p0, LX/0kK;->b:LX/0Or;

    .line 126983
    iput-object p3, p0, LX/0kK;->f:LX/0kL;

    .line 126984
    new-instance v0, LX/0kN;

    invoke-direct {v0, p0}, LX/0kN;-><init>(LX/0kK;)V

    iput-object v0, p0, LX/0kK;->c:LX/0kN;

    .line 126985
    iput-object p4, p0, LX/0kK;->d:LX/0Ot;

    .line 126986
    return-void
.end method

.method public static a(LX/0QB;)LX/0kK;
    .locals 7

    .prologue
    .line 126987
    sget-object v0, LX/0kK;->k:LX/0kK;

    if-nez v0, :cond_1

    .line 126988
    const-class v1, LX/0kK;

    monitor-enter v1

    .line 126989
    :try_start_0
    sget-object v0, LX/0kK;->k:LX/0kK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 126990
    if-eqz v2, :cond_0

    .line 126991
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 126992
    new-instance v4, LX/0kK;

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v3, 0x1459

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    const/16 p0, 0x273

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v6, v3, p0}, LX/0kK;-><init>(LX/0Or;LX/0Or;LX/0kL;LX/0Ot;)V

    .line 126993
    move-object v0, v4

    .line 126994
    sput-object v0, LX/0kK;->k:LX/0kK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126995
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 126996
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 126997
    :cond_1
    sget-object v0, LX/0kK;->k:LX/0kK;

    return-object v0

    .line 126998
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 126999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static f(LX/0kK;)Lcom/facebook/bugreporter/RageShakeDialogFragment;
    .locals 2

    .prologue
    .line 127000
    iget-object v0, p0, LX/0kK;->h:LX/0gc;

    const-string v1, "BugReportFragmentDialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;

    return-object v0
.end method
