.class public LX/0jP;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0hl;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13f;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13a;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x8;",
            ">;"
        }
    .end annotation
.end field

.field public g:J

.field public h:LX/13Z;

.field public i:LX/0f7;

.field public j:Landroid/app/Activity;

.field public k:Lcom/facebook/widget/CustomViewPager;

.field public l:Landroid/view/View;

.field public m:Landroid/view/ViewStub;

.field public final n:LX/0x7;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 123066
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 123067
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123068
    iput-object v0, p0, LX/0jP;->a:LX/0Ot;

    .line 123069
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123070
    iput-object v0, p0, LX/0jP;->b:LX/0Ot;

    .line 123071
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123072
    iput-object v0, p0, LX/0jP;->c:LX/0Ot;

    .line 123073
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123074
    iput-object v0, p0, LX/0jP;->d:LX/0Ot;

    .line 123075
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123076
    iput-object v0, p0, LX/0jP;->e:LX/0Ot;

    .line 123077
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 123078
    iput-object v0, p0, LX/0jP;->f:LX/0Ot;

    .line 123079
    new-instance v0, LX/13Y;

    invoke-direct {v0, p0}, LX/13Y;-><init>(LX/0jP;)V

    iput-object v0, p0, LX/0jP;->n:LX/0x7;

    .line 123080
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0jP;->g:J

    .line 123081
    sget-object v0, LX/13Z;->DSM_INDICATOR_DISABLED:LX/13Z;

    iput-object v0, p0, LX/0jP;->h:LX/13Z;

    .line 123082
    return-void
.end method

.method public static a$redex0(LX/0jP;I)V
    .locals 4

    .prologue
    .line 123062
    iget-object v0, p0, LX/0jP;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 123063
    :goto_0
    return-void

    .line 123064
    :cond_0
    iget-object v0, p0, LX/0jP;->l:Landroid/view/View;

    iget-object v1, p0, LX/0jP;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/0jP;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, LX/0jP;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 123065
    iget-object v0, p0, LX/0jP;->k:Lcom/facebook/widget/CustomViewPager;

    new-instance v1, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentDataSensitivityController$3;

    invoke-direct {v1, p0}, Lcom/facebook/katana/fragment/maintab/controllercallbacks/FbMainTabFragmentDataSensitivityController$3;-><init>(LX/0jP;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 123061
    iget-object v0, p0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 123055
    if-eqz p1, :cond_0

    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 123056
    :cond_0
    :goto_0
    return-void

    .line 123057
    :cond_1
    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123058
    sget-object v1, LX/0ax;->fH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123059
    iget-object v0, p0, LX/0jP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0tK;->d(Z)V

    .line 123060
    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 123051
    iget-object v0, p0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13a;

    invoke-virtual {v0}, LX/13a;->c()V

    .line 123052
    invoke-direct {p0}, LX/0jP;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123053
    iget-object v0, p0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x8;

    invoke-virtual {v0}, LX/0x8;->a()V

    .line 123054
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 123047
    iget-object v0, p0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13a;

    invoke-virtual {v0}, LX/13a;->d()V

    .line 123048
    invoke-direct {p0}, LX/0jP;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123049
    iget-object v0, p0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 123050
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 10

    .prologue
    .line 123013
    iget-object v0, p0, LX/0jP;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13f;

    iget-object v1, p0, LX/0jP;->j:Landroid/app/Activity;

    iget-object v2, p0, LX/0jP;->j:Landroid/app/Activity;

    const v3, 0x7f0d0037

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    const/4 v7, 0x0

    .line 123014
    iget-object v3, v0, LX/13f;->e:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 123015
    iget-object v3, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v3}, LX/0tK;->q()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/13f;->a:LX/0ad;

    sget-char v4, LX/0wm;->D:C

    sget-object v5, LX/49A;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123016
    :goto_0
    iget-object v4, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v4}, LX/0tK;->q()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/13f;->a:LX/0ad;

    sget-short v5, LX/0wm;->E:S

    invoke-interface {v4, v5, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 123017
    :goto_1
    iget-object v5, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v5}, LX/0tK;->q()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v0, LX/13f;->a:LX/0ad;

    sget-short v6, LX/0wm;->C:S

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 123018
    :goto_2
    if-eqz v3, :cond_0

    iget-boolean v6, v0, LX/13f;->f:Z

    if-nez v6, :cond_0

    iget-object v6, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v6}, LX/0tK;->f()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/13f;->d:LX/0tK;

    .line 123019
    sget-object v6, LX/2nr;->d:LX/0Tn;

    invoke-virtual {v6, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    .line 123020
    iget-object v8, v5, LX/0tK;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    move v5, v6

    .line 123021
    if-eqz v5, :cond_4

    .line 123022
    :cond_0
    :goto_3
    iget-object v0, p0, LX/0jP;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jP;->a(Landroid/content/Intent;)V

    .line 123023
    return-void

    .line 123024
    :cond_1
    iget-object v3, v0, LX/13f;->a:LX/0ad;

    sget-char v4, LX/0wm;->j:C

    sget-object v5, LX/13i;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 123025
    :cond_2
    iget-object v4, v0, LX/13f;->a:LX/0ad;

    sget-short v5, LX/0wm;->k:S

    invoke-interface {v4, v5, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_1

    .line 123026
    :cond_3
    iget-object v5, v0, LX/13f;->a:LX/0ad;

    sget-short v6, LX/0wm;->i:S

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    goto :goto_2

    .line 123027
    :cond_4
    if-nez v4, :cond_5

    iget-object v4, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v4}, LX/0tK;->c()Z

    move-result v4

    if-nez v4, :cond_0

    .line 123028
    :cond_5
    iget-object v4, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v4, v7}, LX/0tK;->a(Z)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 123029
    iget-object v4, v0, LX/13f;->d:LX/0tK;

    invoke-virtual {v4, v3}, LX/0tK;->b(Ljava/lang/String;)V

    goto :goto_3

    .line 123030
    :cond_6
    iget-object v4, v0, LX/13f;->c:LX/13h;

    .line 123031
    sget-object v5, LX/499;->NUX_DIALOG_PRE_SHOW:LX/499;

    invoke-static {v4, v5}, LX/13h;->a(LX/13h;LX/499;)V

    .line 123032
    iget-object v4, v0, LX/13f;->b:LX/13g;

    new-instance v5, LX/Bhb;

    invoke-direct {v5, v0, v3, v1, v2}, LX/Bhb;-><init>(LX/13f;Ljava/lang/String;Landroid/content/Context;Landroid/view/View;)V

    const/4 v2, 0x0

    .line 123033
    iget-object v3, v4, LX/13g;->b:LX/0tK;

    invoke-virtual {v3}, LX/0tK;->q()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, v4, LX/13g;->a:LX/0ad;

    sget-char v6, LX/0wm;->B:C

    const-string v7, ""

    invoke-interface {v3, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123034
    :goto_4
    iget-object v6, v4, LX/13g;->b:LX/0tK;

    invoke-virtual {v6}, LX/0tK;->q()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, v4, LX/13g;->a:LX/0ad;

    sget-char v7, LX/0wm;->A:C

    const-string v8, ""

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 123035
    :goto_5
    new-instance v8, LX/6WI;

    invoke-direct {v8, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    .line 123036
    iget-object v7, v4, LX/13g;->b:LX/0tK;

    invoke-virtual {v7}, LX/0tK;->q()Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, v4, LX/13g;->a:LX/0ad;

    sget-short v9, LX/0wm;->M:S

    invoke-interface {v7, v9, v2}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 123037
    :goto_6
    if-eqz v7, :cond_a

    .line 123038
    const v7, 0x7f020409

    invoke-virtual {v8, v7}, LX/6WI;->c(I)LX/6WI;

    .line 123039
    :goto_7
    invoke-virtual {v8, v3}, LX/6WI;->a(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/6WI;->b(Ljava/lang/CharSequence;)LX/6WI;

    move-result-object v3

    const v6, 0x7f08002d

    new-instance v7, LX/Bha;

    invoke-direct {v7, v4, v5}, LX/Bha;-><init>(LX/13g;LX/Bhb;)V

    invoke-virtual {v3, v6, v7}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v3

    const v6, 0x7f080022

    new-instance v7, LX/BhZ;

    invoke-direct {v7, v4, v5}, LX/BhZ;-><init>(LX/13g;LX/Bhb;)V

    invoke-virtual {v3, v6, v7}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v3

    invoke-virtual {v3}, LX/6WI;->b()LX/6WJ;

    .line 123040
    iget-object v3, v0, LX/13f;->c:LX/13h;

    .line 123041
    sget-object v4, LX/499;->NUX_DIALOG_SHOWN:LX/499;

    invoke-static {v3, v4}, LX/13h;->a(LX/13h;LX/499;)V

    .line 123042
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/13f;->f:Z

    goto/16 :goto_3

    .line 123043
    :cond_7
    iget-object v3, v4, LX/13g;->a:LX/0ad;

    sget-char v6, LX/0wm;->h:C

    const-string v7, ""

    invoke-interface {v3, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 123044
    :cond_8
    iget-object v6, v4, LX/13g;->a:LX/0ad;

    sget-char v7, LX/0wm;->g:C

    const-string v8, ""

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    .line 123045
    :cond_9
    iget-object v7, v4, LX/13g;->a:LX/0ad;

    sget-short v9, LX/0wm;->s:S

    invoke-interface {v7, v9, v2}, LX/0ad;->a(SZ)Z

    move-result v7

    goto :goto_6

    .line 123046
    :cond_a
    const v7, 0x7f02040d

    invoke-virtual {v8, v7}, LX/6WI;->c(I)LX/6WI;

    goto :goto_7
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 123012
    return-void
.end method
