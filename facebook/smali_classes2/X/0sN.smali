.class public LX/0sN;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/00G;

.field private final b:LX/00G;

.field private final c:Landroid/content/pm/ApplicationInfo;

.field private final d:Landroid/view/accessibility/AccessibilityManager;


# direct methods
.method public constructor <init>(LX/00G;LX/00G;Landroid/content/pm/ApplicationInfo;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 151665
    invoke-direct {p0}, LX/0RV;-><init>()V

    .line 151666
    iput-object p1, p0, LX/0sN;->a:LX/00G;

    .line 151667
    iput-object p2, p0, LX/0sN;->b:LX/00G;

    .line 151668
    iput-object p3, p0, LX/0sN;->c:Landroid/content/pm/ApplicationInfo;

    .line 151669
    :try_start_0
    const-string v0, "accessibility"

    invoke-virtual {p4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151670
    :goto_0
    move-object v0, v0

    .line 151671
    iput-object v0, p0, LX/0sN;->d:Landroid/view/accessibility/AccessibilityManager;

    .line 151672
    return-void

    .line 151673
    :catch_0
    move-exception v0

    .line 151674
    const-string p1, "Fb4aUserAgentOptionsProvider"

    const-string p2, "Could not initialize accessibility manager"

    invoke-static {p1, p2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151675
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 151677
    const/4 v0, 0x1

    .line 151678
    iget-object v1, p0, LX/0sN;->a:LX/00G;

    invoke-virtual {v1}, LX/00G;->c()Ljava/lang/String;

    move-result-object v1

    .line 151679
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0sN;->b:LX/00G;

    invoke-virtual {v2}, LX/00G;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 151680
    const/4 v0, 0x3

    .line 151681
    :cond_0
    iget-object v1, p0, LX/0sN;->c:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 151682
    or-int/lit8 v0, v0, 0x8

    .line 151683
    :cond_1
    iget-object v1, p0, LX/0sN;->c:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_2

    .line 151684
    or-int/lit8 v0, v0, 0x10

    .line 151685
    :cond_2
    iget-object v1, p0, LX/0sN;->d:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0sN;->d:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0sN;->d:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151686
    or-int/lit8 v0, v0, 0x40

    .line 151687
    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 151676
    invoke-virtual {p0}, LX/0sN;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
