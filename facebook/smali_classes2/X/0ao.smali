.class public final enum LX/0ao;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ao;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ao;

.field public static final enum ASYNC_CACHE_SEEN_STORY:LX/0ao;

.field public static final enum ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

.field public static final enum ASYNC_FRESH_FAST_NETWORK_FETCH:LX/0ao;

.field public static final enum ASYNC_NO_CACHE:LX/0ao;

.field public static final enum DEFAULT_CACHED_FRESH_WITHOUT_NETWORK_FETCH:LX/0ao;

.field public static final enum DEFAULT_FRESH_NETWORK_FETCH:LX/0ao;

.field public static final enum FRESH_FEED_CACHE:LX/0ao;

.field public static final enum FRESH_FEED_CACHE_UNREAD:LX/0ao;

.field public static final enum FRESH_FEED_NETWORK:LX/0ao;

.field public static final enum NOT_SET:LX/0ao;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 85661
    new-instance v0, LX/0ao;

    const-string v1, "NOT_SET"

    invoke-direct {v0, v1, v3}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->NOT_SET:LX/0ao;

    .line 85662
    new-instance v0, LX/0ao;

    const-string v1, "DEFAULT_FRESH_NETWORK_FETCH"

    invoke-direct {v0, v1, v4}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->DEFAULT_FRESH_NETWORK_FETCH:LX/0ao;

    .line 85663
    new-instance v0, LX/0ao;

    const-string v1, "DEFAULT_CACHED_FRESH_WITHOUT_NETWORK_FETCH"

    invoke-direct {v0, v1, v5}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->DEFAULT_CACHED_FRESH_WITHOUT_NETWORK_FETCH:LX/0ao;

    .line 85664
    new-instance v0, LX/0ao;

    const-string v1, "ASYNC_CACHE_UNSEEN_STORY"

    invoke-direct {v0, v1, v6}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

    .line 85665
    new-instance v0, LX/0ao;

    const-string v1, "ASYNC_CACHE_SEEN_STORY"

    invoke-direct {v0, v1, v7}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->ASYNC_CACHE_SEEN_STORY:LX/0ao;

    .line 85666
    new-instance v0, LX/0ao;

    const-string v1, "ASYNC_NO_CACHE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->ASYNC_NO_CACHE:LX/0ao;

    .line 85667
    new-instance v0, LX/0ao;

    const-string v1, "ASYNC_FRESH_FAST_NETWORK_FETCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->ASYNC_FRESH_FAST_NETWORK_FETCH:LX/0ao;

    .line 85668
    new-instance v0, LX/0ao;

    const-string v1, "FRESH_FEED_NETWORK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->FRESH_FEED_NETWORK:LX/0ao;

    .line 85669
    new-instance v0, LX/0ao;

    const-string v1, "FRESH_FEED_CACHE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->FRESH_FEED_CACHE:LX/0ao;

    .line 85670
    new-instance v0, LX/0ao;

    const-string v1, "FRESH_FEED_CACHE_UNREAD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0ao;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0ao;->FRESH_FEED_CACHE_UNREAD:LX/0ao;

    .line 85671
    const/16 v0, 0xa

    new-array v0, v0, [LX/0ao;

    sget-object v1, LX/0ao;->NOT_SET:LX/0ao;

    aput-object v1, v0, v3

    sget-object v1, LX/0ao;->DEFAULT_FRESH_NETWORK_FETCH:LX/0ao;

    aput-object v1, v0, v4

    sget-object v1, LX/0ao;->DEFAULT_CACHED_FRESH_WITHOUT_NETWORK_FETCH:LX/0ao;

    aput-object v1, v0, v5

    sget-object v1, LX/0ao;->ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

    aput-object v1, v0, v6

    sget-object v1, LX/0ao;->ASYNC_CACHE_SEEN_STORY:LX/0ao;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0ao;->ASYNC_NO_CACHE:LX/0ao;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0ao;->ASYNC_FRESH_FAST_NETWORK_FETCH:LX/0ao;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0ao;->FRESH_FEED_NETWORK:LX/0ao;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0ao;->FRESH_FEED_CACHE:LX/0ao;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0ao;->FRESH_FEED_CACHE_UNREAD:LX/0ao;

    aput-object v2, v0, v1

    sput-object v0, LX/0ao;->$VALUES:[LX/0ao;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85672
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ao;
    .locals 1

    .prologue
    .line 85673
    const-class v0, LX/0ao;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ao;

    return-object v0
.end method

.method public static values()[LX/0ao;
    .locals 1

    .prologue
    .line 85674
    sget-object v0, LX/0ao;->$VALUES:[LX/0ao;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ao;

    return-object v0
.end method
