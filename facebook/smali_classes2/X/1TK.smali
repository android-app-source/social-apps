.class public LX/1TK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 250515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250516
    iput-object p1, p0, LX/1TK;->a:LX/0Ot;

    .line 250517
    iput-object p2, p0, LX/1TK;->b:LX/0Ot;

    .line 250518
    return-void
.end method

.method public static a(LX/0QB;)LX/1TK;
    .locals 5

    .prologue
    .line 250519
    const-class v1, LX/1TK;

    monitor-enter v1

    .line 250520
    :try_start_0
    sget-object v0, LX/1TK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 250521
    sput-object v2, LX/1TK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 250522
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250523
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 250524
    new-instance v3, LX/1TK;

    const/16 v4, 0x1efd

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1f17

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1TK;-><init>(LX/0Ot;LX/0Ot;)V

    .line 250525
    move-object v0, v3

    .line 250526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 250527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 250529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 250530
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 250531
    sget-object v0, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250532
    sget-object v0, LX/3Xd;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250533
    sget-object v0, LX/3Xf;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250534
    sget-object v0, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250535
    sget-object v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250536
    sget-object v0, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250537
    sget-object v0, LX/3Xj;->b:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250538
    sget-object v0, LX/3Xl;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250539
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250540
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250541
    sget-object v0, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250542
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250543
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250544
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250545
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250546
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250547
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250548
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250549
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250550
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 250551
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 250552
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    iget-object v1, p0, LX/1TK;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 250553
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    iget-object v1, p0, LX/1TK;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 250554
    return-void
.end method
