.class public final LX/0az;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0b1;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0b1;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 86464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86465
    iput-object p1, p0, LX/0az;->a:LX/0QB;

    .line 86466
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 86515
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0az;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 86468
    packed-switch p2, :pswitch_data_0

    .line 86469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86470
    :pswitch_0
    new-instance v1, LX/0b0;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x17b7

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0b0;-><init>(LX/0b3;LX/0Ot;)V

    .line 86471
    move-object v0, v1

    .line 86472
    :goto_0
    return-object v0

    .line 86473
    :pswitch_1
    new-instance v1, LX/0b8;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x17b7

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0b8;-><init>(LX/0b3;LX/0Ot;)V

    .line 86474
    move-object v0, v1

    .line 86475
    goto :goto_0

    .line 86476
    :pswitch_2
    new-instance v1, LX/0b9;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x17b7

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0b9;-><init>(LX/0b3;LX/0Ot;)V

    .line 86477
    move-object v0, v1

    .line 86478
    goto :goto_0

    .line 86479
    :pswitch_3
    new-instance v1, LX/0bA;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x17ee

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bA;-><init>(LX/0b3;LX/0Ot;)V

    .line 86480
    move-object v0, v1

    .line 86481
    goto :goto_0

    .line 86482
    :pswitch_4
    new-instance v1, LX/0bB;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x17ee

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bB;-><init>(LX/0b3;LX/0Ot;)V

    .line 86483
    move-object v0, v1

    .line 86484
    goto :goto_0

    .line 86485
    :pswitch_5
    new-instance v1, LX/0bC;

    invoke-static {p1}, LX/0bD;->a(LX/0QB;)LX/0bD;

    move-result-object v0

    check-cast v0, LX/0bD;

    const/16 p0, 0x61e

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bC;-><init>(LX/0bD;LX/0Ot;)V

    .line 86486
    move-object v0, v1

    .line 86487
    goto :goto_0

    .line 86488
    :pswitch_6
    new-instance v1, LX/0bG;

    invoke-static {p1}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    const/16 p0, 0xf60

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bG;-><init>(LX/0bH;LX/0Ot;)V

    .line 86489
    move-object v0, v1

    .line 86490
    goto :goto_0

    .line 86491
    :pswitch_7
    new-instance v1, LX/0bK;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x25b4

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bK;-><init>(LX/0b3;LX/0Ot;)V

    .line 86492
    move-object v0, v1

    .line 86493
    goto/16 :goto_0

    .line 86494
    :pswitch_8
    new-instance v1, LX/0bM;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x25b4

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bM;-><init>(LX/0b3;LX/0Ot;)V

    .line 86495
    move-object v0, v1

    .line 86496
    goto/16 :goto_0

    .line 86497
    :pswitch_9
    new-instance v1, LX/0bN;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x2eeb

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bN;-><init>(LX/0b3;LX/0Ot;)V

    .line 86498
    move-object v0, v1

    .line 86499
    goto/16 :goto_0

    .line 86500
    :pswitch_a
    new-instance v1, LX/0bO;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x2eeb

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bO;-><init>(LX/0b3;LX/0Ot;)V

    .line 86501
    move-object v0, v1

    .line 86502
    goto/16 :goto_0

    .line 86503
    :pswitch_b
    new-instance v1, LX/0bQ;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x2eeb

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bQ;-><init>(LX/0b3;LX/0Ot;)V

    .line 86504
    move-object v0, v1

    .line 86505
    goto/16 :goto_0

    .line 86506
    :pswitch_c
    new-instance v1, LX/0bR;

    invoke-static {p1}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v0

    check-cast v0, LX/0b3;

    const/16 p0, 0x2eeb

    invoke-static {p1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bR;-><init>(LX/0b3;LX/0Ot;)V

    .line 86507
    move-object v0, v1

    .line 86508
    goto/16 :goto_0

    .line 86509
    :pswitch_d
    new-instance v1, LX/0bS;

    invoke-static {p1}, LX/0bT;->a(LX/0QB;)LX/0bT;

    move-result-object v0

    check-cast v0, LX/0bT;

    const/16 p0, 0x10bd

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0bS;-><init>(LX/0bT;LX/0Ot;)V

    .line 86510
    move-object v0, v1

    .line 86511
    goto/16 :goto_0

    .line 86512
    :pswitch_e
    new-instance v1, LX/0ba;

    invoke-static {p1}, LX/0bT;->a(LX/0QB;)LX/0bT;

    move-result-object v0

    check-cast v0, LX/0bT;

    const/16 p0, 0x10bd

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/0ba;-><init>(LX/0bT;LX/0Ot;)V

    .line 86513
    move-object v0, v1

    .line 86514
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 86467
    const/16 v0, 0xf

    return v0
.end method
