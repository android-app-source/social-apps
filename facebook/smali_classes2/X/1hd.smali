.class public LX/1hd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile o:LX/1hd;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;

.field public final d:LX/0kb;

.field public final e:LX/0oz;

.field public final f:LX/0p8;

.field public final g:LX/0p7;

.field public final h:LX/0So;

.field public final i:LX/03V;

.field private final j:LX/0Zb;

.field public final k:LX/0YR;

.field public final l:LX/1hI;

.field public final m:LX/1MY;

.field public final n:LX/1hg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296182
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1hd;->a:LX/1he;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0ad;LX/0kb;LX/0oz;LX/0p8;LX/0p7;LX/0So;LX/03V;LX/0Zb;LX/0YR;LX/1Mk;LX/0y2;LX/0Or;LX/1MY;)V
    .locals 1
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/ShouldReportFullNetworkState;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1iZ;",
            ">;>;",
            "LX/0ad;",
            "LX/0kb;",
            "LX/0oz;",
            "LX/0p8;",
            "LX/0p7;",
            "LX/0So;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Zb;",
            "LX/0YR;",
            "LX/1Mk;",
            "LX/0y2;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1MY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 296183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296184
    iput-object p1, p0, LX/1hd;->b:LX/0Or;

    .line 296185
    iput-object p2, p0, LX/1hd;->c:LX/0ad;

    .line 296186
    iput-object p3, p0, LX/1hd;->d:LX/0kb;

    .line 296187
    iput-object p4, p0, LX/1hd;->e:LX/0oz;

    .line 296188
    iput-object p5, p0, LX/1hd;->f:LX/0p8;

    .line 296189
    iput-object p6, p0, LX/1hd;->g:LX/0p7;

    .line 296190
    iput-object p7, p0, LX/1hd;->h:LX/0So;

    .line 296191
    iput-object p8, p0, LX/1hd;->i:LX/03V;

    .line 296192
    iput-object p9, p0, LX/1hd;->j:LX/0Zb;

    .line 296193
    iput-object p10, p0, LX/1hd;->k:LX/0YR;

    .line 296194
    new-instance v0, LX/1hI;

    invoke-direct {v0, p11, p13, p12, p2}, LX/1hI;-><init>(LX/1Mk;LX/0Or;LX/0y2;LX/0ad;)V

    iput-object v0, p0, LX/1hd;->l:LX/1hI;

    .line 296195
    iput-object p14, p0, LX/1hd;->m:LX/1MY;

    .line 296196
    new-instance v0, LX/1hg;

    invoke-direct {v0, p6, p7}, LX/1hg;-><init>(LX/0p7;LX/0So;)V

    iput-object v0, p0, LX/1hd;->n:LX/1hg;

    .line 296197
    return-void
.end method

.method public static a(LX/0QB;)LX/1hd;
    .locals 3

    .prologue
    .line 296198
    sget-object v0, LX/1hd;->o:LX/1hd;

    if-nez v0, :cond_1

    .line 296199
    const-class v1, LX/1hd;

    monitor-enter v1

    .line 296200
    :try_start_0
    sget-object v0, LX/1hd;->o:LX/1hd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 296201
    if-eqz v2, :cond_0

    .line 296202
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1hd;->b(LX/0QB;)LX/1hd;

    move-result-object v0

    sput-object v0, LX/1hd;->o:LX/1hd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 296204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 296205
    :cond_1
    sget-object v0, LX/1hd;->o:LX/1hd;

    return-object v0

    .line 296206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 296207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/1hd;
    .locals 15

    .prologue
    .line 296208
    new-instance v0, LX/1hd;

    invoke-static {p0}, LX/1hf;->a(LX/0QB;)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {p0}, LX/0p8;->a(LX/0QB;)LX/0p8;

    move-result-object v5

    check-cast v5, LX/0p8;

    invoke-static {p0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v6

    check-cast v6, LX/0p7;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v10

    check-cast v10, LX/0YR;

    invoke-static {p0}, LX/1Mk;->a(LX/0QB;)LX/1Mk;

    move-result-object v11

    check-cast v11, LX/1Mk;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v12

    check-cast v12, LX/0y2;

    const/16 v13, 0x323

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {p0}, LX/1MX;->a(LX/0QB;)LX/1MX;

    move-result-object v14

    check-cast v14, LX/1MY;

    invoke-direct/range {v0 .. v14}, LX/1hd;-><init>(LX/0Or;LX/0ad;LX/0kb;LX/0oz;LX/0p8;LX/0p7;LX/0So;LX/03V;LX/0Zb;LX/0YR;LX/1Mk;LX/0y2;LX/0Or;LX/1MY;)V

    .line 296209
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 296210
    iget-object v0, p0, LX/1hd;->j:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 296211
    return-void
.end method
