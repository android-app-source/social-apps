.class public LX/1tC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/SharedPreferences;

.field public b:LX/059;

.field public c:Landroid/os/PowerManager;

.field public d:LX/05A;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1tD;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 336028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336029
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1tC;->e:Ljava/util/List;

    .line 336030
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1tC;->f:Z

    .line 336031
    return-void
.end method

.method private static declared-synchronized c(LX/1tC;)V
    .locals 8

    .prologue
    const/16 v7, 0x31

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 336032
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 336033
    iget-object v0, p0, LX/1tC;->a:Landroid/content/SharedPreferences;

    const-string v1, "publish_result_history"

    const-string v4, ""

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336034
    const-string v1, "([01][01])*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336035
    const-string v0, ""

    :cond_0
    move v5, v3

    .line 336036
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0x2

    if-ge v5, v1, :cond_3

    .line 336037
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_1

    move v4, v2

    .line 336038
    :goto_1
    add-int/lit8 v1, v5, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_2

    move v1, v2

    .line 336039
    :goto_2
    new-instance v6, LX/1tD;

    invoke-direct {v6, p0, v4, v1}, LX/1tD;-><init>(LX/1tC;ZZ)V

    .line 336040
    iget-object v1, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336041
    add-int/lit8 v1, v5, 0x2

    move v5, v1

    goto :goto_0

    :cond_1
    move v4, v3

    .line 336042
    goto :goto_1

    :cond_2
    move v1, v3

    .line 336043
    goto :goto_2

    .line 336044
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1tC;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336045
    monitor-exit p0

    return-void

    .line 336046
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 6

    .prologue
    const/16 v1, 0x31

    const/16 v2, 0x30

    .line 336047
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 336048
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1tD;

    .line 336049
    iget-boolean v3, v0, LX/1tD;->a:Z

    if-eqz v3, :cond_0

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v0, v0, LX/1tD;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336050
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v3, v2

    .line 336051
    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    .line 336052
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/1tC;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "publish_result_history"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336053
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/1tE;
    .locals 11

    .prologue
    const/16 v10, 0xa

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x0

    .line 336054
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1tC;->f:Z

    if-nez v0, :cond_0

    .line 336055
    invoke-static {p0}, LX/1tC;->c(LX/1tC;)V

    .line 336056
    :cond_0
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x32

    if-ge v0, v2, :cond_1

    .line 336057
    sget-object v0, LX/1tE;->UNSURE:LX/1tE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336058
    :goto_0
    monitor-exit p0

    return-object v0

    .line 336059
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v1

    move v2, v1

    move v3, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1tD;

    .line 336060
    iget-boolean v6, v0, LX/1tD;->a:Z

    if-eqz v6, :cond_2

    .line 336061
    add-int/lit8 v3, v3, 0x1

    .line 336062
    iget-boolean v0, v0, LX/1tD;->b:Z

    if-eqz v0, :cond_9

    .line 336063
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 336064
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 336065
    iget-boolean v0, v0, LX/1tD;->b:Z

    if-eqz v0, :cond_9

    .line 336066
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    move v2, v3

    :goto_2
    move v3, v2

    move v2, v1

    move v1, v0

    .line 336067
    goto :goto_1

    .line 336068
    :cond_3
    if-ge v2, v10, :cond_4

    .line 336069
    sget-object v0, LX/1tE;->UNSURE:LX/1tE;

    goto :goto_0

    .line 336070
    :cond_4
    int-to-double v0, v1

    mul-double/2addr v0, v8

    int-to-double v6, v2

    div-double/2addr v0, v6

    .line 336071
    const-wide v6, 0x3fc70a3d70a3d70aL    # 0.18

    cmpl-double v2, v0, v6

    if-lez v2, :cond_5

    .line 336072
    sget-object v0, LX/1tE;->NO:LX/1tE;

    goto :goto_0

    .line 336073
    :cond_5
    if-ge v3, v10, :cond_6

    .line 336074
    sget-object v0, LX/1tE;->UNSURE:LX/1tE;

    goto :goto_0

    .line 336075
    :cond_6
    int-to-double v4, v4

    mul-double/2addr v4, v8

    int-to-double v2, v3

    div-double v2, v4, v2

    .line 336076
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v4, v2, v4

    if-gez v4, :cond_7

    .line 336077
    sget-object v0, LX/1tE;->UNSURE:LX/1tE;

    goto :goto_0

    .line 336078
    :cond_7
    div-double v0, v2, v0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_8

    .line 336079
    sget-object v0, LX/1tE;->YES:LX/1tE;

    goto :goto_0

    .line 336080
    :cond_8
    sget-object v0, LX/1tE;->UNSURE:LX/1tE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 336081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2
.end method

.method public final declared-synchronized a(ZZ)V
    .locals 4

    .prologue
    .line 336082
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1tC;->f:Z

    if-nez v0, :cond_0

    .line 336083
    invoke-static {p0}, LX/1tC;->c(LX/1tC;)V

    .line 336084
    :cond_0
    iget-object v0, p0, LX/1tC;->b:LX/059;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336085
    invoke-virtual {v0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 336086
    if-eqz v3, :cond_6

    .line 336087
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    .line 336088
    if-ne v3, v1, :cond_5

    .line 336089
    :goto_0
    move v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336090
    if-eqz v0, :cond_2

    .line 336091
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 336092
    :cond_2
    :try_start_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 336093
    iget-object v0, p0, LX/1tC;->c:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    move v0, v0

    .line 336094
    if-nez v0, :cond_1

    .line 336095
    :cond_3
    iget-object v0, p0, LX/1tC;->d:LX/05A;

    invoke-virtual {v0}, LX/05A;->a()LX/05B;

    move-result-object v1

    .line 336096
    invoke-virtual {v1}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-object v0, v0, LX/05C;->c:LX/05B;

    invoke-virtual {v0}, LX/05B;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/05C;

    iget-object v0, v0, LX/05C;->c:LX/05B;

    invoke-virtual {v0}, LX/05B;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    .line 336097
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    new-instance v1, LX/1tD;

    invoke-direct {v1, p0, p1, p2}, LX/1tD;-><init>(LX/1tC;ZZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336098
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_4

    .line 336099
    iget-object v0, p0, LX/1tC;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 336100
    :cond_4
    invoke-direct {p0}, LX/1tC;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 336101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v1, v2

    .line 336102
    goto :goto_0

    :cond_6
    move v1, v2

    .line 336103
    goto :goto_0
.end method
