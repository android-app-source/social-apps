.class public LX/1j4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1j5;

.field private static final b:LX/1j6;

.field private static final c:LX/1j7;

.field private static final d:LX/1j8;

.field private static final e:LX/1j9;

.field private static final f:LX/1jA;

.field private static final g:LX/1jB;

.field public static final h:LX/1jC;

.field public static final i:LX/1jD;

.field private static final j:LX/1j0;

.field private static final k:LX/1jE;

.field private static final l:LX/1jF;

.field private static final m:LX/1jG;

.field private static final n:LX/1jH;

.field private static final o:LX/1jI;

.field private static final p:LX/1jJ;

.field private static final q:LX/1jK;

.field private static final r:LX/1jL;

.field private static final s:LX/1jM;

.field private static final t:LX/1jN;

.field private static final u:LX/1jP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299968
    new-instance v0, LX/1j5;

    invoke-direct {v0}, LX/1j5;-><init>()V

    sput-object v0, LX/1j4;->a:LX/1j5;

    .line 299969
    new-instance v0, LX/1j6;

    invoke-direct {v0}, LX/1j6;-><init>()V

    sput-object v0, LX/1j4;->b:LX/1j6;

    .line 299970
    new-instance v0, LX/1j7;

    invoke-direct {v0}, LX/1j7;-><init>()V

    sput-object v0, LX/1j4;->c:LX/1j7;

    .line 299971
    new-instance v0, LX/1j8;

    invoke-direct {v0}, LX/1j8;-><init>()V

    sput-object v0, LX/1j4;->d:LX/1j8;

    .line 299972
    new-instance v0, LX/1j9;

    invoke-direct {v0}, LX/1j9;-><init>()V

    sput-object v0, LX/1j4;->e:LX/1j9;

    .line 299973
    new-instance v0, LX/1jA;

    invoke-direct {v0}, LX/1jA;-><init>()V

    sput-object v0, LX/1j4;->f:LX/1jA;

    .line 299974
    new-instance v0, LX/1jB;

    invoke-direct {v0}, LX/1jB;-><init>()V

    sput-object v0, LX/1j4;->g:LX/1jB;

    .line 299975
    new-instance v0, LX/1jC;

    invoke-direct {v0}, LX/1jC;-><init>()V

    sput-object v0, LX/1j4;->h:LX/1jC;

    .line 299976
    new-instance v0, LX/1jD;

    invoke-direct {v0}, LX/1jD;-><init>()V

    sput-object v0, LX/1j4;->i:LX/1jD;

    .line 299977
    new-instance v0, LX/1j0;

    invoke-direct {v0}, LX/1j0;-><init>()V

    sput-object v0, LX/1j4;->j:LX/1j0;

    .line 299978
    new-instance v0, LX/1jE;

    invoke-direct {v0}, LX/1jE;-><init>()V

    sput-object v0, LX/1j4;->k:LX/1jE;

    .line 299979
    new-instance v0, LX/1jF;

    invoke-direct {v0}, LX/1jF;-><init>()V

    sput-object v0, LX/1j4;->l:LX/1jF;

    .line 299980
    new-instance v0, LX/1jG;

    invoke-direct {v0}, LX/1jG;-><init>()V

    sput-object v0, LX/1j4;->m:LX/1jG;

    .line 299981
    new-instance v0, LX/1jH;

    invoke-direct {v0}, LX/1jH;-><init>()V

    sput-object v0, LX/1j4;->n:LX/1jH;

    .line 299982
    new-instance v0, LX/1jI;

    invoke-direct {v0}, LX/1jI;-><init>()V

    sput-object v0, LX/1j4;->o:LX/1jI;

    .line 299983
    new-instance v0, LX/1jJ;

    invoke-direct {v0}, LX/1jJ;-><init>()V

    sput-object v0, LX/1j4;->p:LX/1jJ;

    .line 299984
    new-instance v0, LX/1jK;

    invoke-direct {v0}, LX/1jK;-><init>()V

    sput-object v0, LX/1j4;->q:LX/1jK;

    .line 299985
    new-instance v0, LX/1jL;

    invoke-direct {v0}, LX/1jL;-><init>()V

    sput-object v0, LX/1j4;->r:LX/1jL;

    .line 299986
    new-instance v0, LX/1jM;

    invoke-direct {v0}, LX/1jM;-><init>()V

    sput-object v0, LX/1j4;->s:LX/1jM;

    .line 299987
    new-instance v0, LX/1jN;

    invoke-direct {v0}, LX/1jN;-><init>()V

    sput-object v0, LX/1j4;->t:LX/1jN;

    .line 299988
    new-instance v0, LX/1jO;

    invoke-direct {v0}, LX/1jO;-><init>()V

    sput-object v0, LX/1j4;->u:LX/1jP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 299799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299800
    return-void
.end method

.method public static declared-synchronized a(Ljava/nio/ByteBuffer;)Lcom/facebook/tigon/iface/TigonRequest;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 299866
    const-class v2, LX/1j4;

    monitor-enter v2

    :try_start_0
    sget-object v1, LX/1j4;->j:LX/1j0;

    .line 299867
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v3, v4

    .line 299868
    iput v3, v1, LX/1j0;->a:I

    iput-object p0, v1, LX/1j0;->b:Ljava/nio/ByteBuffer;

    move-object v3, v1

    .line 299869
    move-object v3, v3

    .line 299870
    new-instance v4, Lcom/facebook/tigon/iface/TigonRequestBuilder;

    invoke-direct {v4}, Lcom/facebook/tigon/iface/TigonRequestBuilder;-><init>()V

    .line 299871
    const/4 v1, 0x4

    invoke-virtual {v3, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_f

    iget-object v5, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v1, v6

    invoke-static {v5, v1}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 299872
    iput-object v1, v4, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a:Ljava/lang/String;

    .line 299873
    const/4 v1, 0x6

    invoke-virtual {v3, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_10

    iget-object v5, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v1, v6

    invoke-static {v5, v1}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 299874
    iput-object v1, v4, Lcom/facebook/tigon/iface/TigonRequestBuilder;->b:Ljava/lang/String;

    .line 299875
    new-instance v1, LX/1iO;

    .line 299876
    const/16 v5, 0xa

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_11

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v7, v3, LX/0eW;->a:I

    add-int/2addr v5, v7

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v5

    const v6, 0xffff

    and-int/2addr v5, v6

    :goto_2
    move v5, v5

    .line 299877
    const/16 v6, 0xc

    invoke-virtual {v3, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_12

    iget-object v7, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v8, v3, LX/0eW;->a:I

    add-int/2addr v6, v8

    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v6

    const v7, 0xffff

    and-int/2addr v6, v7

    :goto_3
    move v6, v6

    .line 299878
    invoke-direct {v1, v5, v6}, LX/1iO;-><init>(II)V

    .line 299879
    iput-object v1, v4, Lcom/facebook/tigon/iface/TigonRequestBuilder;->d:LX/1iO;

    .line 299880
    move v1, v0

    .line 299881
    :goto_4
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_13

    invoke-virtual {v3, v5}, LX/0eW;->d(I)I

    move-result v5

    :goto_5
    move v5, v5

    .line 299882
    if-ge v1, v5, :cond_0

    .line 299883
    invoke-virtual {v3, v1}, LX/1j0;->f(I)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v3, v6}, LX/1j0;->f(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299884
    add-int/lit8 v1, v1, 0x2

    goto :goto_4

    .line 299885
    :cond_0
    sget-object v1, LX/1j4;->k:LX/1jE;

    .line 299886
    const/16 v5, 0xe

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_14

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299887
    iput v5, v1, LX/1jE;->a:I

    iput-object v6, v1, LX/1jE;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299888
    :goto_6
    move-object v1, v5

    .line 299889
    if-eqz v1, :cond_1

    .line 299890
    sget-object v5, LX/1iP;->b:LX/1he;

    new-instance v6, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfoImpl;

    .line 299891
    const/4 v7, 0x4

    invoke-virtual {v1, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_15

    iget-object v8, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v9, v1, LX/0eW;->a:I

    add-int/2addr v7, v9

    invoke-static {v8, v7}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v7

    :goto_7
    move-object v7, v7

    .line 299892
    const/4 v8, 0x6

    invoke-virtual {v1, v8}, LX/0eW;->a(I)I

    move-result v8

    if-eqz v8, :cond_16

    iget-object v9, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v8, p0

    invoke-static {v9, v8}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v8

    :goto_8
    move-object v1, v8

    .line 299893
    invoke-direct {v6, v7, v1}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfoImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299894
    :cond_1
    sget-object v1, LX/1j4;->m:LX/1jG;

    .line 299895
    const/16 v5, 0x10

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_17

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299896
    iput v5, v1, LX/1jG;->a:I

    iput-object v6, v1, LX/1jG;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299897
    :goto_9
    move-object v1, v5

    .line 299898
    if-eqz v1, :cond_2

    .line 299899
    sget-object v5, LX/1iP;->c:LX/1he;

    new-instance v6, LX/2uy;

    .line 299900
    const/4 v10, 0x4

    invoke-virtual {v1, v10}, LX/0eW;->a(I)I

    move-result v10

    if-eqz v10, :cond_18

    iget-object v11, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v12, v1, LX/0eW;->a:I

    add-int/2addr v10, v12

    invoke-virtual {v11, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v10

    :goto_a
    move-wide v8, v10

    .line 299901
    invoke-direct {v6, v8, v9}, LX/2uy;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299902
    :cond_2
    sget-object v1, LX/1j4;->l:LX/1jF;

    .line 299903
    const/16 v5, 0x12

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_19

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299904
    iput v5, v1, LX/1jF;->a:I

    iput-object v6, v1, LX/1jF;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299905
    :goto_b
    move-object v1, v5

    .line 299906
    if-eqz v1, :cond_3

    .line 299907
    sget-object v5, LX/1iP;->a:LX/1he;

    new-instance v6, LX/4mB;

    .line 299908
    const/4 v10, 0x4

    invoke-virtual {v1, v10}, LX/0eW;->a(I)I

    move-result v10

    if-eqz v10, :cond_1a

    iget-object v11, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v12, v1, LX/0eW;->a:I

    add-int/2addr v10, v12

    invoke-virtual {v11, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v10

    :goto_c
    move-wide v8, v10

    .line 299909
    invoke-direct {v6, v8, v9}, LX/4mB;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299910
    :cond_3
    sget-object v1, LX/1j4;->n:LX/1jH;

    .line 299911
    const/16 v5, 0x14

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_1b

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299912
    iput v5, v1, LX/1jH;->a:I

    iput-object v6, v1, LX/1jH;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299913
    :goto_d
    move-object v1, v5

    .line 299914
    if-eqz v1, :cond_4

    .line 299915
    sget-object v5, LX/1iP;->g:LX/1he;

    new-instance v6, LX/4mC;

    .line 299916
    const/4 v10, 0x4

    invoke-virtual {v1, v10}, LX/0eW;->a(I)I

    move-result v10

    if-eqz v10, :cond_1c

    iget-object v11, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v12, v1, LX/0eW;->a:I

    add-int/2addr v10, v12

    invoke-virtual {v11, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v10

    :goto_e
    move-wide v8, v10

    .line 299917
    invoke-direct {v6, v8, v9}, LX/4mC;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299918
    :cond_4
    sget-object v1, LX/1j4;->o:LX/1jI;

    .line 299919
    const/16 v5, 0x16

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_1d

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299920
    iput v5, v1, LX/1jI;->a:I

    iput-object v6, v1, LX/1jI;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299921
    :goto_f
    move-object v1, v5

    .line 299922
    if-eqz v1, :cond_6

    .line 299923
    sget-object v5, LX/1iP;->h:LX/1he;

    new-instance v6, Lcom/facebook/tigon/iface/TigonRetrierRequestInfoImpl;

    const/4 v7, 0x0

    .line 299924
    const/4 v8, 0x4

    invoke-virtual {v1, v8}, LX/0eW;->a(I)I

    move-result v8

    if-eqz v8, :cond_5

    iget-object v9, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v10, v1, LX/0eW;->a:I

    add-int/2addr v8, v10

    invoke-virtual {v9, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    if-eqz v8, :cond_5

    const/4 v7, 0x1

    :cond_5
    move v1, v7

    .line 299925
    invoke-direct {v6, v1}, Lcom/facebook/tigon/iface/TigonRetrierRequestInfoImpl;-><init>(Z)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299926
    :cond_6
    sget-object v1, LX/1j4;->p:LX/1jJ;

    .line 299927
    const/16 v5, 0x18

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_1e

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299928
    iput v5, v1, LX/1jJ;->a:I

    iput-object v6, v1, LX/1jJ;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299929
    :goto_10
    move-object v1, v5

    .line 299930
    if-eqz v1, :cond_8

    .line 299931
    sget-object v5, LX/1iP;->e:LX/1he;

    new-instance v6, LX/1v1;

    .line 299932
    const/4 v7, 0x4

    invoke-virtual {v1, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_1f

    iget-object v8, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v9, v1, LX/0eW;->a:I

    add-int/2addr v7, v9

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v7

    :goto_11
    move v7, v7

    .line 299933
    const/4 v8, 0x0

    .line 299934
    const/4 v9, 0x6

    invoke-virtual {v1, v9}, LX/0eW;->a(I)I

    move-result v9

    if-eqz v9, :cond_7

    iget-object v10, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v11, v1, LX/0eW;->a:I

    add-int/2addr v9, v11

    invoke-virtual {v10, v9}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    if-eqz v9, :cond_7

    const/4 v8, 0x1

    :cond_7
    move v1, v8

    .line 299935
    invoke-direct {v6, v7, v1}, LX/1v1;-><init>(IZ)V

    invoke-virtual {v4, v5, v6}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299936
    :cond_8
    sget-object v1, LX/1j4;->q:LX/1jK;

    .line 299937
    const/16 v5, 0x1c

    invoke-virtual {v3, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_20

    iget v6, v3, LX/0eW;->a:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v5}, LX/0eW;->b(I)I

    move-result v5

    iget-object v6, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299938
    iput v5, v1, LX/1jK;->a:I

    iput-object v6, v1, LX/1jK;->b:Ljava/nio/ByteBuffer;

    move-object v5, v1

    .line 299939
    :goto_12
    move-object v1, v5

    .line 299940
    if-eqz v1, :cond_a

    .line 299941
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_21

    invoke-virtual {v1, v5}, LX/0eW;->d(I)I

    move-result v5

    :goto_13
    move v5, v5

    .line 299942
    new-array v5, v5, [Ljava/lang/String;

    .line 299943
    :goto_14
    array-length v6, v5

    if-ge v0, v6, :cond_9

    .line 299944
    const/4 v6, 0x4

    invoke-virtual {v1, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_22

    iget-object v7, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v6}, LX/0eW;->e(I)I

    move-result v6

    mul-int/lit8 v8, v0, 0x4

    add-int/2addr v6, v8

    invoke-static {v7, v6}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v6

    :goto_15
    move-object v6, v6

    .line 299945
    aput-object v6, v5, v0

    .line 299946
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 299947
    :cond_9
    sget-object v0, LX/1iP;->j:LX/1he;

    new-instance v1, LX/4mD;

    invoke-direct {v1, v5}, LX/4mD;-><init>([Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299948
    :cond_a
    sget-object v0, LX/1j4;->r:LX/1jL;

    .line 299949
    const/16 v1, 0x1e

    invoke-virtual {v3, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_23

    iget v5, v3, LX/0eW;->a:I

    add-int/2addr v1, v5

    invoke-virtual {v3, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v5, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299950
    iput v1, v0, LX/1jL;->a:I

    iput-object v5, v0, LX/1jL;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 299951
    :goto_16
    move-object v0, v1

    .line 299952
    if-eqz v0, :cond_c

    .line 299953
    sget-object v1, LX/1iP;->d:LX/1he;

    new-instance v5, LX/1iT;

    const/4 v6, 0x0

    .line 299954
    const/4 v7, 0x4

    invoke-virtual {v0, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_b

    iget-object v8, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v9, v0, LX/0eW;->a:I

    add-int/2addr v7, v9

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    if-eqz v7, :cond_b

    const/4 v6, 0x1

    :cond_b
    move v6, v6

    .line 299955
    const/4 v7, 0x6

    invoke-virtual {v0, v7}, LX/0eW;->a(I)I

    move-result v7

    if-eqz v7, :cond_24

    iget-object v8, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v9, v0, LX/0eW;->a:I

    add-int/2addr v7, v9

    invoke-static {v8, v7}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v7

    :goto_17
    move-object v7, v7

    .line 299956
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, LX/0eW;->a(I)I

    move-result v8

    if-eqz v8, :cond_25

    iget-object v9, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v10, v0, LX/0eW;->a:I

    add-int/2addr v8, v10

    invoke-static {v9, v8}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v8

    :goto_18
    move-object v0, v8

    .line 299957
    invoke-direct {v5, v6, v7, v0}, LX/1iT;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v1, v5}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299958
    :cond_c
    sget-object v0, LX/1j4;->t:LX/1jN;

    .line 299959
    const/16 v1, 0x20

    invoke-virtual {v3, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_26

    iget v5, v3, LX/0eW;->a:I

    add-int/2addr v1, v5

    invoke-virtual {v3, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v5, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299960
    iput v1, v0, LX/1jN;->a:I

    iput-object v5, v0, LX/1jN;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 299961
    :goto_19
    move-object v0, v1

    .line 299962
    if-eqz v0, :cond_e

    .line 299963
    sget-object v1, LX/1iP;->f:LX/1he;

    new-instance v3, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfoImpl;

    const/4 v5, 0x0

    .line 299964
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/0eW;->a(I)I

    move-result v6

    if-eqz v6, :cond_d

    iget-object v7, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v8, v0, LX/0eW;->a:I

    add-int/2addr v6, v8

    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    if-eqz v6, :cond_d

    const/4 v5, 0x1

    :cond_d
    move v0, v5

    .line 299965
    invoke-direct {v3, v0}, Lcom/facebook/tigon/iface/AndroidRedirectRequestInfoImpl;-><init>(Z)V

    invoke-virtual {v4, v1, v3}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a(LX/1he;Ljava/lang/Object;)Lcom/facebook/tigon/iface/TigonRequestBuilder;

    .line 299966
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/tigon/iface/TigonRequestBuilder;->a()Lcom/facebook/tigon/iface/TigonRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v2

    return-object v0

    .line 299967
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_f
    :try_start_1
    const/4 v1, 0x0

    goto/16 :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_10
    :try_start_2
    const/4 v1, 0x0

    goto/16 :goto_1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_11
    :try_start_3
    const/4 v5, 0x0

    goto/16 :goto_2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_12
    :try_start_4
    const/4 v6, 0x0

    goto/16 :goto_3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_13
    :try_start_5
    const/4 v5, 0x0

    goto/16 :goto_5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_14
    :try_start_6
    const/4 v5, 0x0

    goto/16 :goto_6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_15
    :try_start_7
    const/4 v7, 0x0

    goto/16 :goto_7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_16
    :try_start_8
    const/4 v8, 0x0

    goto/16 :goto_8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_17
    :try_start_9
    const/4 v5, 0x0

    goto/16 :goto_9
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_18
    :try_start_a
    const-wide/16 v10, 0x0

    goto/16 :goto_a
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_19
    :try_start_b
    const/4 v5, 0x0

    goto/16 :goto_b
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_1a
    :try_start_c
    const-wide/16 v10, 0x0

    goto/16 :goto_c
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_1b
    :try_start_d
    const/4 v5, 0x0

    goto/16 :goto_d
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_1c
    :try_start_e
    const-wide/16 v10, 0x0

    goto/16 :goto_e
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_1d
    :try_start_f
    const/4 v5, 0x0

    goto/16 :goto_f
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_1e
    :try_start_10
    const/4 v5, 0x0

    goto/16 :goto_10
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :cond_1f
    :try_start_11
    const/4 v7, 0x0

    goto/16 :goto_11
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_20
    :try_start_12
    const/4 v5, 0x0

    goto/16 :goto_12
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_21
    :try_start_13
    const/4 v5, 0x0

    goto/16 :goto_13
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_22
    :try_start_14
    const/4 v6, 0x0

    goto/16 :goto_15
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :cond_23
    :try_start_15
    const/4 v1, 0x0

    goto/16 :goto_16
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :cond_24
    :try_start_16
    const/4 v7, 0x0

    goto/16 :goto_17
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :cond_25
    :try_start_17
    const/4 v8, 0x0

    goto/16 :goto_18
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :cond_26
    const/4 v1, 0x0

    goto :goto_19
.end method

.method private static a(LX/1j6;)Lcom/facebook/tigon/tigonapi/TigonError;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 299854
    sget-object v0, LX/1j4;->c:LX/1j7;

    .line 299855
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_1

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 299856
    iput v1, v0, LX/1j7;->a:I

    iput-object v2, v0, LX/1j7;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 299857
    :goto_0
    move-object v1, v1

    .line 299858
    if-nez v1, :cond_0

    .line 299859
    const/4 v0, 0x0

    .line 299860
    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/tigon/tigonapi/TigonError;

    .line 299861
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_2

    iget-object v3, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v4, v1, LX/0eW;->a:I

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    :goto_2
    move v2, v2

    .line 299862
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_3

    iget-object v4, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-static {v4, v3}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    move-object v3, v3

    .line 299863
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v5, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_4
    move v4, v4

    .line 299864
    const/16 v5, 0xa

    invoke-virtual {v1, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_5

    iget-object v6, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v5, p0

    invoke-static {v6, v5}, LX/1jQ;->a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;

    move-result-object v5

    :goto_5
    move-object v1, v5

    .line 299865
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/facebook/tigon/tigonapi/TigonError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    goto :goto_5
.end method

.method private static a(ILX/1jP;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1jP;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299847
    rem-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    .line 299848
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Received odd number of strings; keys and vals unmatched"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299849
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    div-int/lit8 v0, p0, 0x2

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 299850
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_1

    .line 299851
    invoke-interface {p1, v0}, LX/1jP;->a(I)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-interface {p1, v3}, LX/1jP;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299852
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 299853
    :cond_1
    return-object v1
.end method

.method public static declared-synchronized b(Ljava/nio/ByteBuffer;)LX/1pB;
    .locals 6

    .prologue
    .line 299837
    const-class v1, LX/1j4;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1j4;->a:LX/1j5;

    .line 299838
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v2, v3

    .line 299839
    iput v2, v0, LX/1j5;->a:I

    iput-object p0, v0, LX/1j5;->b:Ljava/nio/ByteBuffer;

    .line 299840
    sget-object v0, LX/1j4;->a:LX/1j5;

    .line 299841
    const/4 v2, 0x6

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, LX/0eW;->d(I)I

    move-result v2

    :goto_0
    move v0, v2

    .line 299842
    sget-object v2, LX/1j4;->u:LX/1jP;

    invoke-static {v0, v2}, LX/1j4;->a(ILX/1jP;)Ljava/util/HashMap;

    move-result-object v0

    .line 299843
    new-instance v2, LX/1pB;

    sget-object v3, LX/1j4;->a:LX/1j5;

    .line 299844
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_1

    iget-object v5, v3, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p0, v3, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_1
    move v3, v4

    .line 299845
    invoke-direct {v2, v3, v0}, LX/1pB;-><init>(ILjava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v2

    .line 299846
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    const/4 v2, 0x0

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static b(LX/1j6;)LX/1pK;
    .locals 30
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 299809
    sget-object v2, LX/1j4;->d:LX/1j8;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1j6;->a(LX/1j8;)LX/1j8;

    move-result-object v29

    .line 299810
    if-nez v29, :cond_0

    .line 299811
    const/4 v2, 0x0

    .line 299812
    :goto_0
    return-object v2

    .line 299813
    :cond_0
    const/4 v3, 0x0

    .line 299814
    sget-object v2, LX/1j4;->e:LX/1j9;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, LX/1j8;->a(LX/1j9;)LX/1j9;

    move-result-object v2

    .line 299815
    if-eqz v2, :cond_6

    .line 299816
    new-instance v3, LX/1pH;

    invoke-virtual {v2}, LX/1j9;->a()J

    move-result-wide v4

    invoke-virtual {v2}, LX/1j9;->b()J

    move-result-wide v6

    invoke-virtual {v2}, LX/1j9;->c()J

    move-result-wide v8

    invoke-virtual {v2}, LX/1j9;->d()J

    move-result-wide v10

    invoke-virtual {v2}, LX/1j9;->e()J

    move-result-wide v12

    invoke-virtual {v2}, LX/1j9;->f()J

    move-result-wide v14

    invoke-virtual {v2}, LX/1j9;->g()J

    move-result-wide v16

    invoke-virtual {v2}, LX/1j9;->h()J

    move-result-wide v18

    invoke-virtual {v2}, LX/1j9;->i()I

    move-result v20

    invoke-virtual {v2}, LX/1j9;->j()J

    move-result-wide v21

    invoke-virtual {v2}, LX/1j9;->k()J

    move-result-wide v23

    invoke-virtual {v2}, LX/1j9;->l()I

    move-result v25

    invoke-virtual {v2}, LX/1j9;->m()I

    move-result v26

    invoke-virtual {v2}, LX/1j9;->n()I

    move-result v27

    invoke-direct/range {v3 .. v27}, LX/1pH;-><init>(JJJJJJJJIJJIII)V

    move-object/from16 v27, v3

    .line 299817
    :goto_1
    const/4 v2, 0x0

    .line 299818
    sget-object v3, LX/1j4;->f:LX/1jA;

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, LX/1j8;->a(LX/1jA;)LX/1jA;

    move-result-object v3

    .line 299819
    if-eqz v3, :cond_5

    .line 299820
    new-instance v2, LX/HcV;

    invoke-virtual {v3}, LX/1jA;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/HcV;-><init>(Ljava/lang/String;)V

    move-object/from16 v28, v2

    .line 299821
    :goto_2
    const/4 v5, 0x0

    .line 299822
    sget-object v2, LX/1j4;->g:LX/1jB;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, LX/1j8;->a(LX/1jB;)LX/1jB;

    move-result-object v2

    .line 299823
    if-eqz v2, :cond_1

    .line 299824
    new-instance v3, LX/1pI;

    invoke-virtual {v2}, LX/1jB;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/1jB;->b()Z

    move-result v5

    invoke-virtual {v2}, LX/1jB;->c()I

    move-result v6

    invoke-virtual {v2}, LX/1jB;->d()I

    move-result v7

    invoke-virtual {v2}, LX/1jB;->e()I

    move-result v8

    invoke-virtual {v2}, LX/1jB;->f()I

    move-result v9

    invoke-virtual {v2}, LX/1jB;->g()I

    move-result v10

    invoke-virtual {v2}, LX/1jB;->h()I

    move-result v11

    invoke-virtual {v2}, LX/1jB;->i()I

    move-result v12

    invoke-virtual {v2}, LX/1jB;->j()J

    move-result-wide v13

    invoke-virtual {v2}, LX/1jB;->k()Z

    move-result v15

    invoke-virtual {v2}, LX/1jB;->l()J

    move-result-wide v16

    invoke-virtual {v2}, LX/1jB;->m()J

    move-result-wide v18

    invoke-virtual {v2}, LX/1jB;->n()J

    move-result-wide v20

    invoke-virtual {v2}, LX/1jB;->o()J

    move-result-wide v22

    invoke-virtual {v2}, LX/1jB;->p()J

    move-result-wide v24

    invoke-virtual {v2}, LX/1jB;->q()Z

    move-result v26

    invoke-direct/range {v3 .. v26}, LX/1pI;-><init>(Ljava/lang/String;ZIIIIIIIJZJJJJJZ)V

    move-object v5, v3

    .line 299825
    :cond_1
    const/4 v6, 0x0

    .line 299826
    sget-object v2, LX/1j4;->h:LX/1jC;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, LX/1j8;->a(LX/1jC;)LX/1jC;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 299827
    sget-object v2, LX/1j4;->h:LX/1jC;

    invoke-virtual {v2}, LX/1jC;->a()I

    move-result v2

    new-instance v3, LX/HcR;

    invoke-direct {v3}, LX/HcR;-><init>()V

    invoke-static {v2, v3}, LX/1j4;->a(ILX/1jP;)Ljava/util/HashMap;

    move-result-object v2

    .line 299828
    new-instance v6, LX/HcU;

    invoke-direct {v6, v2}, LX/HcU;-><init>(Ljava/util/Map;)V

    .line 299829
    :cond_2
    const/4 v8, 0x0

    .line 299830
    sget-object v2, LX/1j4;->i:LX/1jD;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, LX/1j8;->a(LX/1jD;)LX/1jD;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 299831
    sget-object v2, LX/1j4;->i:LX/1jD;

    invoke-virtual {v2}, LX/1jD;->a()I

    move-result v2

    new-instance v3, LX/HcS;

    invoke-direct {v3}, LX/HcS;-><init>()V

    invoke-static {v2, v3}, LX/1j4;->a(ILX/1jP;)Ljava/util/HashMap;

    move-result-object v2

    .line 299832
    new-instance v8, LX/HcT;

    invoke-direct {v8, v2}, LX/HcT;-><init>(Ljava/util/Map;)V

    .line 299833
    :cond_3
    const/4 v7, 0x0

    .line 299834
    sget-object v2, LX/1j4;->s:LX/1jM;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, LX/1j8;->a(LX/1jM;)LX/1jM;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 299835
    new-instance v7, LX/1pJ;

    sget-object v2, LX/1j4;->s:LX/1jM;

    invoke-virtual {v2}, LX/1jM;->a()I

    move-result v2

    sget-object v3, LX/1j4;->s:LX/1jM;

    invoke-virtual {v3}, LX/1jM;->b()I

    move-result v3

    sget-object v4, LX/1j4;->s:LX/1jM;

    invoke-virtual {v4}, LX/1jM;->c()I

    move-result v4

    sget-object v9, LX/1j4;->s:LX/1jM;

    invoke-virtual {v9}, LX/1jM;->d()I

    move-result v9

    invoke-direct {v7, v2, v3, v4, v9}, LX/1pJ;-><init>(IIII)V

    .line 299836
    :cond_4
    new-instance v2, LX/1pK;

    move-object/from16 v3, v27

    move-object/from16 v4, v28

    invoke-direct/range {v2 .. v8}, LX/1pK;-><init>(LX/1pH;LX/HcV;LX/1pI;LX/HcU;LX/1pJ;LX/HcT;)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v28, v2

    goto/16 :goto_2

    :cond_6
    move-object/from16 v27, v3

    goto/16 :goto_1
.end method

.method public static declared-synchronized c(Ljava/nio/ByteBuffer;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/tigon/tigonapi/TigonError;",
            "Lcom/facebook/tigon/tigonapi/TigonSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299801
    const-class v1, LX/1j4;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1j4;->b:LX/1j6;

    .line 299802
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v2, v3

    .line 299803
    iput v2, v0, LX/1j6;->a:I

    iput-object p0, v0, LX/1j6;->b:Ljava/nio/ByteBuffer;

    move-object v2, v0

    .line 299804
    move-object v0, v2

    .line 299805
    invoke-static {v0}, LX/1j4;->a(LX/1j6;)Lcom/facebook/tigon/tigonapi/TigonError;

    move-result-object v2

    .line 299806
    invoke-static {v0}, LX/1j4;->b(LX/1j6;)LX/1pK;

    move-result-object v0

    .line 299807
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v3

    .line 299808
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
