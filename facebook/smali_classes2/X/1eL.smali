.class public final LX/1eL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/1cN;

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Multiplexer.this"
    .end annotation
.end field

.field private e:F
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Multiplexer.this"
    .end annotation
.end field

.field public f:LX/1cW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Multiplexer.this"
    .end annotation
.end field

.field private g:LX/1eN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cN",
            "<TK;TT;>.Multiplexer.ForwardingConsumer;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Multiplexer.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1cN;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 287976
    iput-object p1, p0, LX/1eL;->a:LX/1cN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287977
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    move-object v0, v0

    .line 287978
    iput-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 287979
    iput-object p2, p0, LX/1eL;->b:Ljava/lang/Object;

    .line 287980
    return-void
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 2

    .prologue
    .line 287971
    if-eqz p0, :cond_0

    .line 287972
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287973
    :cond_0
    return-void

    .line 287974
    :catch_0
    move-exception v0

    .line 287975
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a$redex0(LX/1eL;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 288096
    monitor-enter p0

    .line 288097
    :try_start_0
    iget-object v3, p0, LX/1eL;->f:LX/1cW;

    if-nez v3, :cond_0

    move v3, v1

    :goto_0
    invoke-static {v3}, LX/03g;->a(Z)V

    .line 288098
    iget-object v3, p0, LX/1eL;->g:LX/1eN;

    if-nez v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 288099
    iget-object v1, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288100
    iget-object v1, p0, LX/1eL;->a:LX/1cN;

    iget-object v2, p0, LX/1eL;->b:Ljava/lang/Object;

    invoke-static {v1, v2, p0}, LX/1cN;->a$redex0(LX/1cN;Ljava/lang/Object;LX/1eL;)V

    .line 288101
    monitor-exit p0

    .line 288102
    :goto_2
    return-void

    :cond_0
    move v3, v2

    .line 288103
    goto :goto_0

    :cond_1
    move v1, v2

    .line 288104
    goto :goto_1

    .line 288105
    :cond_2
    iget-object v1, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, LX/1cW;

    move-object v6, v0

    .line 288106
    new-instance v1, LX/1cW;

    .line 288107
    iget-object v0, v6, LX/1cW;->a:LX/1bf;

    move-object v2, v0

    .line 288108
    iget-object v0, v6, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v0

    .line 288109
    iget-object v0, v6, LX/1cW;->c:LX/1BV;

    move-object v4, v0

    .line 288110
    iget-object v0, v6, LX/1cW;->d:Ljava/lang/Object;

    move-object v5, v0

    .line 288111
    iget-object v0, v6, LX/1cW;->e:LX/1bY;

    move-object v6, v0

    .line 288112
    invoke-direct {p0}, LX/1eL;->c()Z

    move-result v7

    invoke-direct {p0}, LX/1eL;->e()Z

    move-result v8

    invoke-direct {p0}, LX/1eL;->g()LX/1bc;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, LX/1cW;-><init>(LX/1bf;Ljava/lang/String;LX/1BV;Ljava/lang/Object;LX/1bY;ZZLX/1bc;)V

    iput-object v1, p0, LX/1eL;->f:LX/1cW;

    .line 288113
    new-instance v1, LX/1eN;

    invoke-direct {v1, p0}, LX/1eN;-><init>(LX/1eL;)V

    iput-object v1, p0, LX/1eL;->g:LX/1eN;

    .line 288114
    iget-object v1, p0, LX/1eL;->f:LX/1cW;

    .line 288115
    iget-object v2, p0, LX/1eL;->g:LX/1eN;

    .line 288116
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288117
    iget-object v3, p0, LX/1eL;->a:LX/1cN;

    iget-object v3, v3, LX/1cN;->b:LX/1cF;

    invoke-interface {v3, v2, v1}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_2

    .line 288118
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static declared-synchronized b(LX/1eL;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 288092
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1eL;->f:LX/1cW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 288093
    const/4 v0, 0x0

    .line 288094
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1eL;->f:LX/1cW;

    invoke-direct {p0}, LX/1eL;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/1cW;->a(Z)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 288095
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()Z
    .locals 2

    .prologue
    .line 288087
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 288088
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1cW;

    invoke-virtual {v0}, LX/1cW;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 288089
    const/4 v0, 0x0

    .line 288090
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 288091
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized d$redex0(LX/1eL;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 288083
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1eL;->f:LX/1cW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 288084
    const/4 v0, 0x0

    .line 288085
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1eL;->f:LX/1cW;

    invoke-direct {p0}, LX/1eL;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/1cW;->b(Z)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 288086
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()Z
    .locals 2

    .prologue
    .line 288078
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 288079
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1cW;

    invoke-virtual {v0}, LX/1cW;->h()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 288080
    const/4 v0, 0x1

    .line 288081
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 288082
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f$redex0(LX/1eL;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/imagepipeline/producers/ProducerContextCallbacks;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 288119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1eL;->f:LX/1cW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 288120
    const/4 v0, 0x0

    .line 288121
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1eL;->f:LX/1cW;

    invoke-direct {p0}, LX/1eL;->g()LX/1bc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1cW;->a(LX/1bc;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 288122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()LX/1bc;
    .locals 3

    .prologue
    .line 288072
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/1bc;->LOW:LX/1bc;

    .line 288073
    iget-object v1, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 288074
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1cW;

    invoke-virtual {v0}, LX/1cW;->g()LX/1bc;

    move-result-object v0

    invoke-static {v1, v0}, LX/1bc;->getHigherPriority(LX/1bc;LX/1bc;)LX/1bc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move-object v1, v0

    .line 288075
    goto :goto_0

    .line 288076
    :cond_0
    monitor-exit p0

    return-object v1

    .line 288077
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1eN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cN",
            "<TK;TT;>.Multiplexer.ForwardingConsumer;)V"
        }
    .end annotation

    .prologue
    .line 288061
    monitor-enter p0

    .line 288062
    :try_start_0
    iget-object v0, p0, LX/1eL;->g:LX/1eN;

    if-eq v0, p1, :cond_0

    .line 288063
    monitor-exit p0

    .line 288064
    :goto_0
    return-void

    .line 288065
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eL;->g:LX/1eN;

    .line 288066
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eL;->f:LX/1cW;

    .line 288067
    iget-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    invoke-static {v0}, LX/1eL;->a(Ljava/io/Closeable;)V

    .line 288068
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    .line 288069
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288070
    invoke-static {p0}, LX/1eL;->a$redex0(LX/1eL;)V

    goto :goto_0

    .line 288071
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/1eN;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cN",
            "<TK;TT;>.Multiplexer.ForwardingConsumer;F)V"
        }
    .end annotation

    .prologue
    .line 288048
    monitor-enter p0

    .line 288049
    :try_start_0
    iget-object v0, p0, LX/1eL;->g:LX/1eN;

    if-eq v0, p1, :cond_1

    .line 288050
    monitor-exit p0

    .line 288051
    :cond_0
    return-void

    .line 288052
    :cond_1
    iput p2, p0, LX/1eL;->e:F

    .line 288053
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 288054
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 288055
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288056
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 288057
    monitor-enter v1

    .line 288058
    :try_start_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/1cd;

    invoke-virtual {v0, p2}, LX/1cd;->b(F)V

    .line 288059
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 288060
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a(LX/1eN;Ljava/io/Closeable;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cN",
            "<TK;TT;>.Multiplexer.ForwardingConsumer;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 288030
    monitor-enter p0

    .line 288031
    :try_start_0
    iget-object v0, p0, LX/1eL;->g:LX/1eN;

    if-eq v0, p1, :cond_1

    .line 288032
    monitor-exit p0

    .line 288033
    :cond_0
    return-void

    .line 288034
    :cond_1
    iget-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    invoke-static {v0}, LX/1eL;->a(Ljava/io/Closeable;)V

    .line 288035
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    .line 288036
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 288037
    if-nez p3, :cond_2

    .line 288038
    iget-object v0, p0, LX/1eL;->a:LX/1cN;

    invoke-virtual {v0, p2}, LX/1cN;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    iput-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    .line 288039
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 288040
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288041
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 288042
    monitor-enter v1

    .line 288043
    :try_start_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/1cd;

    invoke-virtual {v0, p2, p3}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 288044
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 288045
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 288046
    iget-object v0, p0, LX/1eL;->a:LX/1cN;

    iget-object v1, p0, LX/1eL;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1cN;->a$redex0(LX/1cN;Ljava/lang/Object;LX/1eL;)V

    goto :goto_0

    .line 288047
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a(LX/1eN;Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cN",
            "<TK;TT;>.Multiplexer.ForwardingConsumer;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288014
    monitor-enter p0

    .line 288015
    :try_start_0
    iget-object v0, p0, LX/1eL;->g:LX/1eN;

    if-eq v0, p1, :cond_1

    .line 288016
    monitor-exit p0

    .line 288017
    :cond_0
    return-void

    .line 288018
    :cond_1
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 288019
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 288020
    iget-object v0, p0, LX/1eL;->a:LX/1cN;

    iget-object v1, p0, LX/1eL;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1cN;->a$redex0(LX/1cN;Ljava/lang/Object;LX/1eL;)V

    .line 288021
    iget-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    invoke-static {v0}, LX/1eL;->a(Ljava/io/Closeable;)V

    .line 288022
    const/4 v0, 0x0

    iput-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    .line 288023
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 288024
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288025
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 288026
    monitor-enter v1

    .line 288027
    :try_start_1
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/1cd;

    invoke-virtual {v0, p2}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 288028
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 288029
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a(LX/1cd;LX/1cW;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 287981
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    .line 287982
    monitor-enter p0

    .line 287983
    :try_start_0
    iget-object v2, p0, LX/1eL;->a:LX/1cN;

    iget-object v3, p0, LX/1eL;->b:Ljava/lang/Object;

    invoke-static {v2, v3}, LX/1cN;->a$redex0(LX/1cN;Ljava/lang/Object;)LX/1eL;

    move-result-object v2

    if-eq v2, p0, :cond_0

    .line 287984
    monitor-exit p0

    .line 287985
    :goto_0
    return v0

    .line 287986
    :cond_0
    iget-object v0, p0, LX/1eL;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 287987
    invoke-static {p0}, LX/1eL;->b(LX/1eL;)Ljava/util/List;

    move-result-object v2

    .line 287988
    invoke-static {p0}, LX/1eL;->f$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v3

    .line 287989
    invoke-static {p0}, LX/1eL;->d$redex0(LX/1eL;)Ljava/util/List;

    move-result-object v4

    .line 287990
    iget-object v0, p0, LX/1eL;->d:Ljava/io/Closeable;

    .line 287991
    iget v5, p0, LX/1eL;->e:F

    .line 287992
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287993
    invoke-static {v2}, LX/1cW;->a(Ljava/util/List;)V

    .line 287994
    invoke-static {v3}, LX/1cW;->c(Ljava/util/List;)V

    .line 287995
    invoke-static {v4}, LX/1cW;->b(Ljava/util/List;)V

    .line 287996
    monitor-enter v1

    .line 287997
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 287998
    :try_start_2
    iget-object v2, p0, LX/1eL;->d:Ljava/io/Closeable;

    if-eq v0, v2, :cond_4

    .line 287999
    const/4 v0, 0x0

    .line 288000
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 288001
    if-eqz v0, :cond_3

    .line 288002
    const/4 v2, 0x0

    cmpl-float v2, v5, v2

    if-lez v2, :cond_2

    .line 288003
    :try_start_3
    invoke-virtual {p1, v5}, LX/1cd;->b(F)V

    .line 288004
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 288005
    invoke-static {v0}, LX/1eL;->a(Ljava/io/Closeable;)V

    .line 288006
    :cond_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 288007
    new-instance v0, LX/1eM;

    invoke-direct {v0, p0, v1}, LX/1eM;-><init>(LX/1eL;Landroid/util/Pair;)V

    invoke-virtual {p2, v0}, LX/1cW;->a(LX/1cg;)V

    .line 288008
    const/4 v0, 0x1

    goto :goto_0

    .line 288009
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 288010
    :cond_4
    if-eqz v0, :cond_1

    .line 288011
    :try_start_5
    iget-object v2, p0, LX/1eL;->a:LX/1cN;

    invoke-virtual {v2, v0}, LX/1cN;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    goto :goto_1

    .line 288012
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0

    .line 288013
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0
.end method
