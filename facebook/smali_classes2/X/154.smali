.class public LX/154;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/154;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:I

.field private h:I

.field private i:Ljava/util/Locale;

.field public j:Ljava/text/DecimalFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180074
    new-instance v0, Ljava/util/Locale;

    const-string v1, "zh"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Locale;

    const-string v2, "ja"

    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "ko"

    invoke-direct {v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/154;->b:Ljava/util/Set;

    .line 180075
    new-instance v0, Ljava/util/Locale;

    const-string v1, "en"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/154;->c:Ljava/util/Set;

    .line 180076
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "PT"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/154;->d:Ljava/util/Set;

    .line 180077
    new-instance v0, Ljava/util/Locale;

    const-string v1, "es"

    const-string v2, "ES"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/util/Locale;

    const-string v2, "pt"

    const-string v3, "PT"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/154;->e:Ljava/util/Set;

    .line 180078
    iput-object p1, p0, LX/154;->a:Landroid/content/Context;

    .line 180079
    invoke-static {p0}, LX/154;->a$redex0(LX/154;)V

    .line 180080
    new-instance v0, LX/0Yd;

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    new-instance v2, LX/155;

    invoke-direct {v2, p0}, LX/155;-><init>(LX/154;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    .line 180081
    iget-object v1, p0, LX/154;->a:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 180082
    return-void
.end method

.method public static a(LX/154;D)I
    .locals 5

    .prologue
    .line 180066
    const/4 v1, 0x0

    .line 180067
    const/4 v0, 0x1

    .line 180068
    :goto_0
    iget v2, p0, LX/154;->g:I

    add-int/2addr v2, v1

    iget v3, p0, LX/154;->h:I

    if-gt v2, v3, :cond_0

    .line 180069
    iget v2, p0, LX/154;->f:I

    mul-int/2addr v0, v2

    .line 180070
    int-to-double v2, v0

    cmpl-double v2, v2, p1

    if-gtz v2, :cond_0

    .line 180071
    iget v2, p0, LX/154;->g:I

    add-int/2addr v1, v2

    goto :goto_0

    .line 180072
    :cond_0
    return v1
.end method

.method public static a(LX/0QB;)LX/154;
    .locals 4

    .prologue
    .line 180053
    sget-object v0, LX/154;->k:LX/154;

    if-nez v0, :cond_1

    .line 180054
    const-class v1, LX/154;

    monitor-enter v1

    .line 180055
    :try_start_0
    sget-object v0, LX/154;->k:LX/154;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180056
    if-eqz v2, :cond_0

    .line 180057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180058
    new-instance p0, LX/154;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/154;-><init>(Landroid/content/Context;)V

    .line 180059
    move-object v0, p0

    .line 180060
    sput-object v0, LX/154;->k:LX/154;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180061
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180062
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180063
    :cond_1
    sget-object v0, LX/154;->k:LX/154;

    return-object v0

    .line 180064
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 179989
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 179990
    iget-object v1, p0, LX/154;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/154;III)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    .line 180044
    if-le p2, p3, :cond_0

    .line 180045
    int-to-double v0, p1

    sub-int v2, p2, p3

    int-to-double v2, v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int p1, v0

    .line 180046
    :goto_0
    int-to-double v0, p1

    int-to-double v2, p3

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 180047
    int-to-double v2, p1

    int-to-double v4, p3

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    rem-double/2addr v2, v4

    double-to-int v1, v2

    .line 180048
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    if-eqz v1, :cond_1

    .line 180049
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/154;->j:Ljava/text/DecimalFormat;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/154;->j:Ljava/text/DecimalFormat;

    invoke-virtual {v2}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    invoke-virtual {v2}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180050
    :goto_1
    return-object v0

    :cond_0
    move p3, p2

    .line 180051
    goto :goto_0

    .line 180052
    :cond_1
    iget-object v1, p0, LX/154;->j:Ljava/text/DecimalFormat;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/154;Ljava/lang/String;Ljava/lang/Integer;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 180040
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 180041
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 180042
    :pswitch_0
    invoke-direct {p0, p1, p3}, LX/154;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180043
    :pswitch_1
    invoke-direct {p0, p1, p3}, LX/154;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 180032
    packed-switch p2, :pswitch_data_0

    .line 180033
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid multiplier: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180034
    :pswitch_1
    const v0, 0x7f0f00bb

    invoke-direct {p0, v0, p1}, LX/154;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180035
    :goto_0
    return-object v0

    .line 180036
    :pswitch_2
    const v0, 0x7f0f00bc

    invoke-direct {p0, v0, p1}, LX/154;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180037
    :pswitch_3
    const v0, 0x7f0f00bd

    invoke-direct {p0, v0, p1}, LX/154;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180038
    :pswitch_4
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f081952

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180039
    :pswitch_5
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f081953

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/154;Ljava/util/Set;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Locale;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 180027
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 180028
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 180029
    iget-object v2, p0, LX/154;->i:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 180030
    :goto_0
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/154;->i:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 180031
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method public static a$redex0(LX/154;)V
    .locals 1

    .prologue
    .line 180016
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/154;->i:Ljava/util/Locale;

    .line 180017
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    iput-object v0, p0, LX/154;->j:Ljava/text/DecimalFormat;

    .line 180018
    iget-object v0, p0, LX/154;->b:Ljava/util/Set;

    invoke-static {p0, v0}, LX/154;->a(LX/154;Ljava/util/Set;)Z

    move-result v0

    move v0, v0

    .line 180019
    if-nez v0, :cond_0

    .line 180020
    const/16 v0, 0x3e8

    iput v0, p0, LX/154;->f:I

    .line 180021
    const/4 v0, 0x3

    iput v0, p0, LX/154;->g:I

    .line 180022
    const/16 v0, 0x9

    iput v0, p0, LX/154;->h:I

    .line 180023
    :goto_0
    return-void

    .line 180024
    :cond_0
    const/16 v0, 0x2710

    iput v0, p0, LX/154;->f:I

    .line 180025
    const/4 v0, 0x4

    iput v0, p0, LX/154;->g:I

    .line 180026
    const/16 v0, 0x8

    iput v0, p0, LX/154;->h:I

    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 180000
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 180001
    sparse-switch p2, :sswitch_data_0

    .line 180002
    :cond_0
    :goto_0
    move v0, v0

    .line 180003
    if-eqz v0, :cond_1

    .line 180004
    packed-switch p2, :pswitch_data_0

    .line 180005
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid multiplier: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180006
    :pswitch_1
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f08194d

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 180007
    :goto_1
    return-object v0

    .line 180008
    :pswitch_2
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f08194f

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 180009
    :pswitch_3
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f081951

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 180010
    :pswitch_4
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f08194e

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 180011
    :pswitch_5
    iget-object v0, p0, LX/154;->a:Landroid/content/Context;

    const v1, 0x7f081950

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 180012
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 180013
    :sswitch_0
    iget-object v0, p0, LX/154;->c:Ljava/util/Set;

    invoke-static {p0, v0}, LX/154;->a(LX/154;Ljava/util/Set;)Z

    move-result v0

    goto :goto_0

    .line 180014
    :sswitch_1
    iget-object v4, p0, LX/154;->d:Ljava/util/Set;

    invoke-static {p0, v4}, LX/154;->a(LX/154;Ljava/util/Set;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 180015
    :sswitch_2
    iget-object v4, p0, LX/154;->e:Ljava/util/Set;

    invoke-static {p0, v4}, LX/154;->a(LX/154;Ljava/util/Set;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x6 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 179999
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)Ljava/lang/String;
    .locals 5

    .prologue
    .line 179991
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 179992
    iget v1, p0, LX/154;->f:I

    if-lt p1, v1, :cond_0

    .line 179993
    int-to-double v1, p1

    invoke-static {p0, v1, v2}, LX/154;->a(LX/154;D)I

    move-result v1

    .line 179994
    invoke-static {p0, p1, v1, p2}, LX/154;->a(LX/154;III)Ljava/lang/String;

    move-result-object v2

    .line 179995
    invoke-static {p0, v2, v0, v1}, LX/154;->a(LX/154;Ljava/lang/String;Ljava/lang/Integer;I)Ljava/lang/String;

    move-result-object v1

    .line 179996
    if-eqz v1, :cond_0

    .line 179997
    :goto_0
    move-object v0, v1

    .line 179998
    return-object v0

    :cond_0
    iget-object v1, p0, LX/154;->j:Ljava/text/DecimalFormat;

    int-to-long v3, p1

    invoke-virtual {v1, v3, v4}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
