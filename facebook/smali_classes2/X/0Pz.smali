.class public final LX/0Pz;
.super LX/0Q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Q0",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57661
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/0Pz;-><init>(I)V

    .line 57662
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 57653
    invoke-direct {p0, p1}, LX/0Q0;-><init>(I)V

    .line 57654
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Iterable;)LX/0P7;
    .locals 1

    .prologue
    .line 57655
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Iterable;)LX/0P7;

    .line 57656
    move-object v0, p0

    .line 57657
    return-object v0
.end method

.method public final synthetic a(Ljava/util/Iterator;)LX/0P7;
    .locals 1

    .prologue
    .line 57658
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/util/Iterator;)LX/0P7;

    .line 57659
    move-object v0, p0

    .line 57660
    return-object v0
.end method

.method public final synthetic a([Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 57663
    invoke-super {p0, p1}, LX/0Q0;->a([Ljava/lang/Object;)LX/0P7;

    .line 57664
    move-object v0, p0

    .line 57665
    return-object v0
.end method

.method public final synthetic a()LX/0Py;
    .locals 1

    .prologue
    .line 57649
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)LX/0Q0;
    .locals 1

    .prologue
    .line 57650
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Object;)LX/0Q0;

    .line 57651
    move-object v0, p0

    .line 57652
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 57637
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Object;)LX/0Q0;

    .line 57638
    move-object v0, p0

    .line 57639
    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57640
    iget-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    iget v1, p0, LX/0Q0;->b:I

    invoke-static {v0, v1}, LX/0Px;->asImmutableList([Ljava/lang/Object;I)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Iterable;)LX/0Pz;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0Pz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57641
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Iterable;)LX/0P7;

    .line 57642
    return-object p0
.end method

.method public final b(Ljava/util/Iterator;)LX/0Pz;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LX/0Pz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57647
    invoke-super {p0, p1}, LX/0P7;->a(Ljava/util/Iterator;)LX/0P7;

    .line 57648
    return-object p0
.end method

.method public final varargs b([Ljava/lang/Object;)LX/0Pz;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LX/0Pz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57643
    invoke-super {p0, p1}, LX/0Q0;->a([Ljava/lang/Object;)LX/0P7;

    .line 57644
    return-object p0
.end method

.method public final c(Ljava/lang/Object;)LX/0Pz;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/0Pz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57645
    invoke-super {p0, p1}, LX/0Q0;->a(Ljava/lang/Object;)LX/0Q0;

    .line 57646
    return-object p0
.end method
