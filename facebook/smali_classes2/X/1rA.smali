.class public LX/1rA;
.super LX/1rB;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/1rA;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1sH;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rW;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rh;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3FA;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2U7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1sH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1rW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1rh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3FA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1wW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331809
    invoke-direct {p0}, LX/1rB;-><init>()V

    .line 331810
    iput-object p1, p0, LX/1rA;->a:LX/0Ot;

    .line 331811
    iput-object p2, p0, LX/1rA;->b:LX/0Ot;

    .line 331812
    iput-object p3, p0, LX/1rA;->c:LX/0Ot;

    .line 331813
    iput-object p4, p0, LX/1rA;->d:LX/0Ot;

    .line 331814
    iput-object p5, p0, LX/1rA;->e:LX/0Ot;

    .line 331815
    iput-object p6, p0, LX/1rA;->f:LX/0Ot;

    .line 331816
    iput-object p7, p0, LX/1rA;->g:LX/0Ot;

    .line 331817
    return-void
.end method

.method public static a(LX/0QB;)LX/1rA;
    .locals 11

    .prologue
    .line 331796
    sget-object v0, LX/1rA;->h:LX/1rA;

    if-nez v0, :cond_1

    .line 331797
    const-class v1, LX/1rA;

    monitor-enter v1

    .line 331798
    :try_start_0
    sget-object v0, LX/1rA;->h:LX/1rA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331799
    if-eqz v2, :cond_0

    .line 331800
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331801
    new-instance v3, LX/1rA;

    const/16 v4, 0x3e2

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x792

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xb9d

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xc90

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xe62

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xf55

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1386

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/1rA;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 331802
    move-object v0, v3

    .line 331803
    sput-object v0, LX/1rA;->h:LX/1rA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331804
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331805
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331806
    :cond_1
    sget-object v0, LX/1rA;->h:LX/1rA;

    return-object v0

    .line 331807
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331808
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 331775
    iget-object v0, p0, LX/1rA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iX;

    .line 331776
    iget-object v1, v0, LX/0iX;->a:LX/0iY;

    const/4 v2, 0x0

    .line 331777
    iput-boolean v2, v1, LX/0iY;->t:Z

    .line 331778
    iget-object v1, v0, LX/0iX;->c:LX/0ad;

    sget v2, LX/1rJ;->p:I

    const/16 v3, 0x1e

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    .line 331779
    iget-object v3, v0, LX/0iX;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iget-wide v5, v0, LX/0iX;->j:J

    sub-long/2addr v3, v5

    cmp-long v1, v3, v1

    if-lez v1, :cond_2

    .line 331780
    invoke-static {v0}, LX/0iX;->i(LX/0iX;)V

    .line 331781
    :cond_0
    :goto_0
    iget-object v1, v0, LX/0iX;->d:LX/0iZ;

    sget-object v2, LX/0ia;->FOREGROUND:LX/0ia;

    invoke-virtual {v1, v2}, LX/0iZ;->a(LX/0ia;)V

    .line 331782
    iget-object v0, p0, LX/1rA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sH;

    .line 331783
    iget-boolean v1, v0, LX/1sH;->g:Z

    if-nez v1, :cond_3

    .line 331784
    :goto_1
    iget-object v0, p0, LX/1rA;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rW;

    .line 331785
    iget-object v1, v0, LX/1rW;->c:Landroid/os/Handler;

    iget-object v2, v0, LX/1rW;->u:Ljava/lang/Runnable;

    const v3, 0x3ac392e3

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 331786
    iget-object v0, p0, LX/1rA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rh;

    .line 331787
    iget-object v1, v0, LX/1rh;->c:LX/1wV;

    invoke-virtual {v1}, LX/1wV;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331788
    iget-object v2, v0, LX/1rh;->b:LX/1rj;

    iget-object v1, v0, LX/1rh;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v2, v1}, LX/1rj;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 331789
    :cond_1
    iget-object v0, p0, LX/1rA;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wW;

    invoke-virtual {v0}, LX/1wW;->a()V

    .line 331790
    return-void

    .line 331791
    :cond_2
    iget-boolean v1, v0, LX/0iX;->m:Z

    iget-object v2, v0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v2}, LX/0iY;->g()Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 331792
    invoke-static {v0}, LX/0iX;->j(LX/0iX;)V

    goto :goto_0

    .line 331793
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, LX/1sH;->k:J

    .line 331794
    const-wide/16 v1, 0x0

    iput-wide v1, v0, LX/1sH;->l:J

    .line 331795
    iget-object v1, v0, LX/1sH;->i:Landroid/os/Handler;

    iget-object v2, v0, LX/1sH;->h:Ljava/lang/Runnable;

    iget-wide v3, v0, LX/1sH;->k:J

    iget-object v5, v0, LX/1sH;->f:Ljava/util/Random;

    const/16 v6, 0x258

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    add-long/2addr v3, v5

    const v5, -0x49f96a43

    invoke-static {v1, v2, v3, v4, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 331755
    iget-object v0, p0, LX/1rA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U7;

    .line 331756
    const/4 v1, 0x1

    iput v1, v0, LX/2U7;->f:I

    .line 331757
    iget-object v0, p0, LX/1rA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iX;

    .line 331758
    iget-object v1, v0, LX/0iX;->a:LX/0iY;

    const/4 v2, 0x1

    .line 331759
    iput-boolean v2, v1, LX/0iY;->t:Z

    .line 331760
    iget-object v1, v0, LX/0iX;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/0iX;->j:J

    .line 331761
    sget-object v1, LX/1rN;->UNKNOWN:LX/1rN;

    iput-object v1, v0, LX/0iX;->l:LX/1rN;

    .line 331762
    iget-object v1, v0, LX/0iX;->a:LX/0iY;

    invoke-virtual {v1}, LX/0iY;->g()Z

    move-result v1

    iput-boolean v1, v0, LX/0iX;->m:Z

    .line 331763
    iget-object v1, v0, LX/0iX;->d:LX/0iZ;

    sget-object v2, LX/0ia;->BACKGROUND:LX/0ia;

    invoke-virtual {v1, v2}, LX/0iZ;->a(LX/0ia;)V

    .line 331764
    iget-object v0, p0, LX/1rA;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sH;

    .line 331765
    iget-boolean v1, v0, LX/1sH;->g:Z

    if-nez v1, :cond_1

    .line 331766
    :goto_0
    iget-object v0, p0, LX/1rA;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rW;

    .line 331767
    iget-object v1, v0, LX/1rW;->c:Landroid/os/Handler;

    iget-object v2, v0, LX/1rW;->v:Ljava/lang/Runnable;

    const v3, 0x771578a

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 331768
    iget-object v0, p0, LX/1rA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3FA;

    .line 331769
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3FA;->i:Z

    .line 331770
    iget-object v0, p0, LX/1rA;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wW;

    .line 331771
    iget-object v1, v0, LX/1wW;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xX;

    invoke-virtual {v1}, LX/0xX;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1wW;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xX;

    sget-object v2, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v1, v2}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 331772
    :cond_0
    :goto_1
    return-void

    .line 331773
    :cond_1
    iget-object v1, v0, LX/1sH;->i:Landroid/os/Handler;

    iget-object v2, v0, LX/1sH;->h:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 331774
    :cond_2
    iget-object v1, v0, LX/1wW;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hn;

    invoke-virtual {v1}, LX/0hn;->f()V

    goto :goto_1
.end method
