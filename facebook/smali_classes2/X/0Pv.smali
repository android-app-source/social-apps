.class public LX/0Pv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field public static final logger:Ljava/util/logging/Logger;

.field private static final startFinalizer:Ljava/lang/reflect/Method;


# instance fields
.field public final frqRef:Ljava/lang/ref/PhantomReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/PhantomReference",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final queue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final threadStarted:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57398
    const-class v0, LX/0Pv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/0Pv;->logger:Ljava/util/logging/Logger;

    .line 57399
    const/4 v0, 0x3

    new-array v0, v0, [LX/4vv;

    const/4 v1, 0x0

    new-instance v2, LX/4vy;

    invoke-direct {v2}, LX/4vy;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, LX/4vw;

    invoke-direct {v2}, LX/4vw;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/4vx;

    invoke-direct {v2}, LX/4vx;-><init>()V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Pv;->loadFinalizer([LX/4vv;)Ljava/lang/Class;

    move-result-object v0

    .line 57400
    invoke-static {v0}, LX/0Pv;->getStartFinalizer(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LX/0Pv;->startFinalizer:Ljava/lang/reflect/Method;

    .line 57401
    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57403
    new-instance v2, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v2, p0, LX/0Pv;->queue:Ljava/lang/ref/ReferenceQueue;

    .line 57404
    new-instance v2, Ljava/lang/ref/PhantomReference;

    iget-object v3, p0, LX/0Pv;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p0, v3}, Ljava/lang/ref/PhantomReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    iput-object v2, p0, LX/0Pv;->frqRef:Ljava/lang/ref/PhantomReference;

    .line 57405
    :try_start_0
    sget-object v2, LX/0Pv;->startFinalizer:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-class v6, Lcom/google/common/base/FinalizableReference;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, LX/0Pv;->queue:Ljava/lang/ref/ReferenceQueue;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, LX/0Pv;->frqRef:Ljava/lang/ref/PhantomReference;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 57406
    :goto_0
    iput-boolean v0, p0, LX/0Pv;->threadStarted:Z

    .line 57407
    return-void

    .line 57408
    :catch_0
    move-exception v0

    .line 57409
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 57410
    :catch_1
    move-exception v0

    .line 57411
    sget-object v2, LX/0Pv;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "Failed to start reference finalizer thread. Reference cleanup will only occur when new references are created."

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0
.end method

.method public static getStartFinalizer(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .prologue
    .line 57412
    :try_start_0
    const-string v0, "startFinalizer"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Ljava/lang/ref/ReferenceQueue;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Ljava/lang/ref/PhantomReference;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 57413
    :catch_0
    move-exception v0

    .line 57414
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static varargs loadFinalizer([LX/4vv;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/4vv;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 57415
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 57416
    invoke-interface {v2}, LX/4vv;->loadFinalizer()Ljava/lang/Class;

    move-result-object v2

    .line 57417
    if-eqz v2, :cond_0

    .line 57418
    return-object v2

    .line 57419
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57420
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    .line 57421
    iget-object v0, p0, LX/0Pv;->frqRef:Ljava/lang/ref/PhantomReference;

    invoke-virtual {v0}, Ljava/lang/ref/PhantomReference;->enqueue()Z

    .line 57422
    iget-boolean v0, p0, LX/0Pv;->threadStarted:Z

    if-eqz v0, :cond_1

    .line 57423
    :cond_0
    return-void

    .line 57424
    :cond_1
    :goto_0
    iget-object v0, p0, LX/0Pv;->queue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57425
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->clear()V

    .line 57426
    :try_start_0
    check-cast v0, Lcom/google/common/base/FinalizableReference;

    invoke-interface {v0}, Lcom/google/common/base/FinalizableReference;->finalizeReferent()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57427
    :catch_0
    move-exception v0

    .line 57428
    sget-object v1, LX/0Pv;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Error cleaning up after reference."

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
