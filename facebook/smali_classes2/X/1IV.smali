.class public final LX/1IV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 228765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isUpperCase(C)Z
    .locals 1

    .prologue
    .line 228752
    const/16 v0, 0x41

    if-lt p0, v0, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toLowerCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 228753
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 228754
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 228755
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, LX/1IV;->isUpperCase(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 228756
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 228757
    :goto_1
    if-ge v0, v1, :cond_1

    .line 228758
    aget-char v3, v2, v0

    .line 228759
    invoke-static {v3}, LX/1IV;->isUpperCase(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 228760
    xor-int/lit8 v3, v3, 0x20

    int-to-char v3, v3

    aput-char v3, v2, v0

    .line 228761
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228762
    :cond_1
    invoke-static {v2}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object p0

    .line 228763
    :cond_2
    return-object p0

    .line 228764
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
