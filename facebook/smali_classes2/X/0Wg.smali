.class public final LX/0Wg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0QR",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final delegate:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile transient initialized:Z

.field public transient value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 76614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76615
    iput-object p1, p0, LX/0Wg;->delegate:LX/0QR;

    .line 76616
    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 76618
    iget-boolean v0, p0, LX/0Wg;->initialized:Z

    if-nez v0, :cond_1

    .line 76619
    monitor-enter p0

    .line 76620
    :try_start_0
    iget-boolean v0, p0, LX/0Wg;->initialized:Z

    if-nez v0, :cond_0

    .line 76621
    iget-object v0, p0, LX/0Wg;->delegate:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    .line 76622
    iput-object v0, p0, LX/0Wg;->value:Ljava/lang/Object;

    .line 76623
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/0Wg;->initialized:Z

    .line 76624
    monitor-exit p0

    .line 76625
    :goto_0
    return-object v0

    .line 76626
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76627
    :cond_1
    iget-object v0, p0, LX/0Wg;->value:Ljava/lang/Object;

    goto :goto_0

    .line 76628
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76617
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Suppliers.memoize("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0Wg;->delegate:LX/0QR;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
