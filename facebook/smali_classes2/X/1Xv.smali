.class public final enum LX/1Xv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Xv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Xv;

.field public static final enum DYNAMIC:LX/1Xv;

.field public static final enum STATIC:LX/1Xv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272596
    new-instance v0, LX/1Xv;

    const-string v1, "DYNAMIC"

    invoke-direct {v0, v1, v2}, LX/1Xv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xv;->DYNAMIC:LX/1Xv;

    .line 272597
    new-instance v0, LX/1Xv;

    const-string v1, "STATIC"

    invoke-direct {v0, v1, v3}, LX/1Xv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Xv;->STATIC:LX/1Xv;

    .line 272598
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Xv;

    sget-object v1, LX/1Xv;->DYNAMIC:LX/1Xv;

    aput-object v1, v0, v2

    sget-object v1, LX/1Xv;->STATIC:LX/1Xv;

    aput-object v1, v0, v3

    sput-object v0, LX/1Xv;->$VALUES:[LX/1Xv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 272599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Xv;
    .locals 1

    .prologue
    .line 272600
    const-class v0, LX/1Xv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Xv;

    return-object v0
.end method

.method public static values()[LX/1Xv;
    .locals 1

    .prologue
    .line 272601
    sget-object v0, LX/1Xv;->$VALUES:[LX/1Xv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Xv;

    return-object v0
.end method
