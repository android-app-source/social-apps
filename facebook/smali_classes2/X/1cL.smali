.class public LX/1cL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Ao;

.field private final c:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Fh;LX/1Ao;LX/1cF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281899
    iput-object p1, p0, LX/1cL;->a:LX/1Fh;

    .line 281900
    iput-object p2, p0, LX/1cL;->b:LX/1Ao;

    .line 281901
    iput-object p3, p0, LX/1cL;->c:LX/1cF;

    .line 281902
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 281903
    iget-object v1, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v1

    .line 281904
    iget-object v2, p2, LX/1cW;->c:LX/1BV;

    move-object v2, v2

    .line 281905
    const-string v3, "EncodedMemoryCacheProducer"

    invoke-interface {v2, v1, v3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 281906
    iget-object v3, p2, LX/1cW;->a:LX/1bf;

    move-object v3, v3

    .line 281907
    iget-object v4, p0, LX/1cL;->b:LX/1Ao;

    .line 281908
    iget-object v5, p2, LX/1cW;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 281909
    invoke-virtual {v4, v3, v5}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v3

    .line 281910
    iget-object v4, p0, LX/1cL;->a:LX/1Fh;

    invoke-interface {v4, v3}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v4

    .line 281911
    if-eqz v4, :cond_1

    .line 281912
    :try_start_0
    new-instance v5, LX/1FL;

    invoke-direct {v5, v4}, LX/1FL;-><init>(LX/1FJ;)V

    .line 281913
    iput-object v3, v5, LX/1FL;->i:LX/1bh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 281914
    :try_start_1
    const-string v3, "EncodedMemoryCacheProducer"

    invoke-interface {v2, v1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v0, "cached_value_found"

    const-string v6, "true"

    invoke-static {v0, v6}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :cond_0
    invoke-interface {v2, v1, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 281915
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, LX/1cd;->b(F)V

    .line 281916
    const/4 v0, 0x1

    invoke-virtual {p1, v5, v0}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281917
    :try_start_2
    invoke-static {v5}, LX/1FL;->d(LX/1FL;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 281918
    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    .line 281919
    :goto_0
    return-void

    .line 281920
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v5}, LX/1FL;->d(LX/1FL;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 281921
    :catchall_1
    move-exception v0

    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 281922
    :cond_1
    :try_start_4
    iget-object v5, p2, LX/1cW;->e:LX/1bY;

    move-object v5, v5

    .line 281923
    invoke-virtual {v5}, LX/1bY;->getValue()I

    move-result v5

    sget-object v6, LX/1bY;->ENCODED_MEMORY_CACHE:LX/1bY;

    invoke-virtual {v6}, LX/1bY;->getValue()I

    move-result v6

    if-lt v5, v6, :cond_3

    .line 281924
    const-string v3, "EncodedMemoryCacheProducer"

    invoke-interface {v2, v1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v0, "cached_value_found"

    const-string v5, "false"

    invoke-static {v0, v5}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :cond_2
    invoke-interface {v2, v1, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 281925
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 281926
    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 281927
    :cond_3
    :try_start_5
    new-instance v5, LX/1ee;

    iget-object v6, p0, LX/1cL;->a:LX/1Fh;

    invoke-direct {v5, p1, v6, v3}, LX/1ee;-><init>(LX/1cd;LX/1Fh;LX/1bh;)V

    .line 281928
    const-string v3, "EncodedMemoryCacheProducer"

    invoke-interface {v2, v1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v0, "cached_value_found"

    const-string v6, "false"

    invoke-static {v0, v6}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :cond_4
    invoke-interface {v2, v1, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 281929
    iget-object v0, p0, LX/1cL;->c:LX/1cF;

    invoke-interface {v0, v5, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 281930
    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0
.end method
