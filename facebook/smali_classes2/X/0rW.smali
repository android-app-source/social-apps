.class public LX/0rW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0rW;


# instance fields
.field public final a:Z

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rX;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vR;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 149756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149757
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 149758
    iput-object v0, p0, LX/0rW;->b:LX/0Ot;

    .line 149759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 149760
    iput-object v0, p0, LX/0rW;->c:LX/0Ot;

    .line 149761
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 149762
    iput-object v0, p0, LX/0rW;->d:LX/0Ot;

    .line 149763
    sget-short v0, LX/0fe;->bu:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0rW;->a:Z

    .line 149764
    return-void
.end method

.method public static a(LX/0QB;)LX/0rW;
    .locals 6

    .prologue
    .line 149741
    sget-object v0, LX/0rW;->e:LX/0rW;

    if-nez v0, :cond_1

    .line 149742
    const-class v1, LX/0rW;

    monitor-enter v1

    .line 149743
    :try_start_0
    sget-object v0, LX/0rW;->e:LX/0rW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 149744
    if-eqz v2, :cond_0

    .line 149745
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 149746
    new-instance v4, LX/0rW;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v3}, LX/0rW;-><init>(LX/0ad;)V

    .line 149747
    const/16 v3, 0x68c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v5, 0x68d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x2e3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 149748
    iput-object v3, v4, LX/0rW;->b:LX/0Ot;

    iput-object v5, v4, LX/0rW;->c:LX/0Ot;

    iput-object p0, v4, LX/0rW;->d:LX/0Ot;

    .line 149749
    move-object v0, v4

    .line 149750
    sput-object v0, LX/0rW;->e:LX/0rW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149751
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 149752
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149753
    :cond_1
    sget-object v0, LX/0rW;->e:LX/0rW;

    return-object v0

    .line 149754
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 149755
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/feed/Vpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149726
    invoke-static {p1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149727
    iget-object v0, p0, LX/0rW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rX;

    .line 149728
    if-eqz p1, :cond_0

    .line 149729
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 149730
    iget-object v4, v0, LX/0rY;->a:LX/0re;

    invoke-virtual {v4, v1}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149731
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 149732
    :cond_0
    invoke-static {p2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149733
    iget-object v0, p0, LX/0rW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vR;

    .line 149734
    if-eqz p2, :cond_1

    .line 149735
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/Vpv;

    .line 149736
    iget-object v4, v0, LX/0rY;->a:LX/0re;

    .line 149737
    iget-object p0, v1, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    move-object v1, p0

    .line 149738
    invoke-virtual {v4, v1}, LX/0re;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149739
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 149740
    :cond_1
    return-void
.end method

.method public final a(LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 4

    .prologue
    .line 149695
    iget-boolean v0, p0, LX/0rW;->a:Z

    if-eqz v0, :cond_0

    .line 149696
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 149697
    iget-object v0, p5, Lcom/facebook/api/feed/FetchFeedParams;->j:LX/0Px;

    move-object v0, v0

    .line 149698
    if-nez v0, :cond_1

    .line 149699
    const/4 v1, 0x0

    .line 149700
    :goto_0
    move-object v0, v1

    .line 149701
    invoke-virtual {p1, p3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 149702
    :goto_1
    return-void

    .line 149703
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, p4, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 149704
    iget-object v0, p5, Lcom/facebook/api/feed/FetchFeedParams;->i:LX/0Px;

    move-object v0, v0

    .line 149705
    invoke-virtual {p1, p2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto :goto_1

    .line 149706
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 149707
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, p0, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/Vpv;

    .line 149708
    new-instance p2, LX/4J8;

    invoke-direct {p2}, LX/4J8;-><init>()V

    .line 149709
    iget-object p4, v1, Lcom/facebook/api/feed/Vpv;->a:Ljava/lang/String;

    .line 149710
    const-string p5, "qid"

    invoke-virtual {p2, p5, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149711
    iget-object p4, v1, Lcom/facebook/api/feed/Vpv;->b:Ljava/lang/String;

    .line 149712
    const-string p5, "vsid"

    invoke-virtual {p2, p5, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 149713
    iget p4, v1, Lcom/facebook/api/feed/Vpv;->c:I

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    .line 149714
    const-string p5, "timestamp"

    invoke-virtual {p2, p5, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149715
    move-object v1, p2

    .line 149716
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149717
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 149718
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149719
    iget-boolean v0, p0, LX/0rW;->a:Z

    if-eqz v0, :cond_0

    .line 149720
    iget-object v0, p0, LX/0rW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vR;

    invoke-virtual {v0, p2, p3, p4}, LX/1vR;->a(Lcom/facebook/graphql/model/FeedUnit;J)V

    .line 149721
    :goto_0
    return-void

    .line 149722
    :cond_0
    iget-object v0, p0, LX/0rW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rX;

    .line 149723
    if-nez p1, :cond_1

    .line 149724
    :goto_1
    goto :goto_0

    .line 149725
    :cond_1
    iget-object p0, v0, LX/0rY;->a:LX/0re;

    invoke-virtual {p0, p1, p1}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
