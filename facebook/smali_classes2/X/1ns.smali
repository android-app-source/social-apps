.class public final LX/1ns;
.super Landroid/text/TextPaint;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 317549
    invoke-direct {p0}, Landroid/text/TextPaint;-><init>()V

    .line 317550
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 317551
    invoke-direct {p0, p1}, Landroid/text/TextPaint;-><init>(I)V

    .line 317552
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 317553
    invoke-direct {p0, p1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 317554
    return-void
.end method


# virtual methods
.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 317555
    invoke-virtual {p0}, LX/1ns;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 317556
    invoke-virtual {p0}, LX/1ns;->getColor()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    .line 317557
    mul-int/lit8 v2, v2, 0x1f

    invoke-virtual {p0}, LX/1ns;->getTextSize()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int/2addr v2, v3

    .line 317558
    mul-int/lit8 v2, v2, 0x1f

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Typeface;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 317559
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/1ns;->a:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 317560
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/1ns;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 317561
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/1ns;->c:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 317562
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/1ns;->d:I

    add-int/2addr v0, v2

    .line 317563
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Landroid/text/TextPaint;->linkColor:I

    add-int/2addr v0, v2

    .line 317564
    iget-object v2, p0, Landroid/text/TextPaint;->drawableState:[I

    if-nez v2, :cond_2

    .line 317565
    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    .line 317566
    :cond_0
    return v0

    :cond_1
    move v0, v1

    .line 317567
    goto :goto_0

    .line 317568
    :cond_2
    :goto_1
    iget-object v2, p0, Landroid/text/TextPaint;->drawableState:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 317569
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Landroid/text/TextPaint;->drawableState:[I

    aget v2, v2, v1

    add-int/2addr v0, v2

    .line 317570
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final setShadowLayer(FFFI)V
    .locals 0

    .prologue
    .line 317571
    iput p1, p0, LX/1ns;->c:F

    .line 317572
    iput p2, p0, LX/1ns;->a:F

    .line 317573
    iput p3, p0, LX/1ns;->b:F

    .line 317574
    iput p4, p0, LX/1ns;->d:I

    .line 317575
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 317576
    return-void
.end method
