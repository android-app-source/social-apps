.class public abstract LX/0iV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/nux/ui/NuxBubbleView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/apptab/state/TabTag;

.field public e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 120906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120907
    iput-object v0, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    .line 120908
    iput-object v0, p0, LX/0iV;->d:Lcom/facebook/apptab/state/TabTag;

    .line 120909
    iput-object p1, p0, LX/0iV;->e:Landroid/content/res/Resources;

    .line 120910
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nux/ui/NuxBubbleView;Lcom/facebook/apptab/state/TabTag;)V
    .locals 2
    .param p1    # Lcom/facebook/nux/ui/NuxBubbleView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 120911
    iget-object v0, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0iV;->g()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 120912
    :cond_0
    :goto_0
    return-void

    .line 120913
    :cond_1
    if-nez p2, :cond_2

    .line 120914
    invoke-virtual {p1}, Lcom/facebook/nux/ui/NuxBubbleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 120915
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 120916
    :cond_2
    new-instance v0, LX/7gD;

    invoke-direct {v0, p0, p2, p1}, LX/7gD;-><init>(LX/0iV;Lcom/facebook/apptab/state/TabTag;Lcom/facebook/nux/ui/NuxBubbleView;)V

    .line 120917
    iput-object v0, p1, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    .line 120918
    iput-object p1, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    .line 120919
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    iget-object v1, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    .line 120920
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 120921
    goto :goto_0
.end method

.method public final b(Lcom/facebook/apptab/state/TabTag;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x3

    .line 120922
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 120923
    :cond_0
    :goto_0
    return-void

    .line 120924
    :cond_1
    invoke-virtual {p0}, LX/0iV;->g()I

    move-result v1

    .line 120925
    if-eqz v1, :cond_0

    .line 120926
    iput-object p1, p0, LX/0iV;->d:Lcom/facebook/apptab/state/TabTag;

    .line 120927
    iget-object v0, p0, LX/0iV;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 120928
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->getPointerWidth()I

    move-result v0

    .line 120929
    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    .line 120930
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->getPointerLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v4

    .line 120931
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 120932
    iget-object v5, p0, LX/0iV;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-ge v2, v5, :cond_2

    .line 120933
    mul-int/2addr v1, v2

    .line 120934
    iput v6, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 120935
    iput v3, v4, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 120936
    iput v6, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 120937
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 120938
    :cond_2
    iget-object v5, p0, LX/0iV;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int v2, v5, v2

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    .line 120939
    iput v7, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 120940
    iput v3, v4, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 120941
    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 120942
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method

.method public final g()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 120943
    iget-object v1, p0, LX/0iV;->b:Ljava/util/List;

    if-nez v1, :cond_1

    .line 120944
    :cond_0
    :goto_0
    return v0

    .line 120945
    :cond_1
    iget-object v1, p0, LX/0iV;->e:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 120946
    iget-object v2, p0, LX/0iV;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 120947
    if-eqz v2, :cond_0

    .line 120948
    div-int v0, v1, v2

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 120949
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    if-nez v0, :cond_0

    .line 120950
    :goto_0
    return-void

    .line 120951
    :cond_0
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->e()V

    .line 120952
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-virtual {v0}, Lcom/facebook/nux/ui/NuxBubbleView;->a()V

    .line 120953
    iget-object v0, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 120954
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    iget-object v1, p0, LX/0iV;->c:Landroid/view/ViewGroup;

    .line 120955
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 120956
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 120957
    iget-object v0, p0, LX/0iV;->a:Lcom/facebook/nux/ui/NuxBubbleView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
