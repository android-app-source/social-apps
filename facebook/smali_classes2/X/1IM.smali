.class public final LX/1IM;
.super LX/1IA;
.source ""


# static fields
.field public static final INSTANCE:LX/1IM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228632
    new-instance v0, LX/1IM;

    invoke-direct {v0}, LX/1IM;-><init>()V

    sput-object v0, LX/1IM;->INSTANCE:LX/1IM;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 228633
    invoke-direct {p0}, LX/1IA;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228634
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, LX/1IA;->apply(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final matches(C)Z
    .locals 1

    .prologue
    .line 228635
    invoke-static {p1}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228636
    const-string v0, "CharMatcher.javaLowerCase()"

    return-object v0
.end method
