.class public final LX/1ni;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/1ni;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1nm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/1nk;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 317299
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1ni;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 317328
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 317329
    new-instance v0, LX/1nk;

    invoke-direct {v0}, LX/1nk;-><init>()V

    iput-object v0, p0, LX/1ni;->c:LX/1nk;

    .line 317330
    return-void
.end method

.method public static declared-synchronized a()LX/1ni;
    .locals 2

    .prologue
    .line 317324
    const-class v1, LX/1ni;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1ni;->a:LX/1ni;

    if-nez v0, :cond_0

    .line 317325
    new-instance v0, LX/1ni;

    invoke-direct {v0}, LX/1ni;-><init>()V

    sput-object v0, LX/1ni;->a:LX/1ni;

    .line 317326
    :cond_0
    sget-object v0, LX/1ni;->a:LX/1ni;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 317327
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1De;)LX/1nm;
    .locals 2

    .prologue
    .line 317331
    new-instance v0, LX/1nj;

    invoke-direct {v0}, LX/1nj;-><init>()V

    .line 317332
    sget-object v1, LX/1ni;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nm;

    .line 317333
    if-nez v1, :cond_0

    .line 317334
    new-instance v1, LX/1nm;

    invoke-direct {v1}, LX/1nm;-><init>()V

    .line 317335
    :cond_0
    invoke-virtual {v1, p0, v0}, LX/1nm;->a(LX/1De;LX/1nj;)V

    .line 317336
    move-object v0, v1

    .line 317337
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 317312
    iget-object v0, p0, LX/1ni;->c:LX/1nk;

    check-cast p2, LX/1nj;

    iget v1, p2, LX/1nj;->a:I

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 317313
    iget-object p0, v0, LX/1nk;->a:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1o8;

    .line 317314
    if-nez p0, :cond_2

    .line 317315
    new-instance p0, LX/1o8;

    const/16 p1, 0xa

    invoke-direct {p0, p1}, LX/1o8;-><init>(I)V

    .line 317316
    iget-object p1, v0, LX/1nk;->a:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2, p0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, p0

    .line 317317
    :goto_0
    invoke-virtual {p1}, LX/1o8;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    .line 317318
    if-nez p0, :cond_0

    .line 317319
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 317320
    :cond_0
    invoke-virtual {p1}, LX/1o8;->b()I

    move-result p2

    if-nez p2, :cond_1

    .line 317321
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1o8;->a(Ljava/lang/Object;)Z

    .line 317322
    :cond_1
    move-object v0, p0

    .line 317323
    return-object v0

    :cond_2
    move-object p1, p0

    goto :goto_0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 2

    .prologue
    .line 317300
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 317301
    iget-object v0, p0, LX/1ni;->c:LX/1nk;

    check-cast p3, LX/1nj;

    iget v1, p3, LX/1nj;->a:I

    .line 317302
    iget-object p0, v0, LX/1nk;->a:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1o8;

    .line 317303
    if-nez p0, :cond_0

    .line 317304
    new-instance p0, LX/1o8;

    const/16 p1, 0xa

    invoke-direct {p0, p1}, LX/1o8;-><init>(I)V

    .line 317305
    iget-object p1, v0, LX/1nk;->a:LX/0aq;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p3, p0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317306
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 317307
    sget-object p1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 317308
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0xb

    if-lt p1, p3, :cond_1

    .line 317309
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 317310
    :cond_1
    invoke-virtual {p0, p2}, LX/1o8;->a(Ljava/lang/Object;)Z

    .line 317311
    return-void
.end method
