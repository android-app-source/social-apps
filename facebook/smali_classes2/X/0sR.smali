.class public LX/0sR;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0sQ;

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/debug/fieldusage/FieldTrackable$QueryTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151992
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0sR;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 151990
    invoke-virtual {p0}, LX/15w;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/12V;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 151937
    if-eqz p0, :cond_3

    const-string v0, "viewer"

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "response"

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, "^[0-9]*$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 151938
    :cond_0
    invoke-virtual {p0}, LX/12V;->a()LX/12V;

    move-result-object v0

    invoke-static {v0}, LX/0sR;->a(LX/12V;)Ljava/lang/String;

    move-result-object v0

    .line 151939
    if-nez v0, :cond_2

    .line 151940
    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    .line 151941
    :cond_1
    :goto_0
    return-object v0

    .line 151942
    :cond_2
    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 151943
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 151944
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151945
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 151946
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 151982
    const-class v1, LX/0sR;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/0sR;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 151983
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 151984
    :cond_1
    :try_start_1
    instance-of v0, p0, LX/15w;

    if-eqz v0, :cond_2

    .line 151985
    check-cast p0, LX/15w;

    invoke-static {p0}, LX/0sR;->a(LX/15w;)Ljava/lang/Object;

    move-result-object p0

    .line 151986
    :cond_2
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 151987
    sget-object v2, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151988
    sget-object v2, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151989
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a()Z
    .locals 2

    .prologue
    .line 151981
    const-class v1, LX/0sR;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0sR;->a:LX/0sQ;

    if-eqz v0, :cond_0

    sget-object v0, LX/0sR;->a:LX/0sQ;

    invoke-virtual {v0}, LX/0sQ;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/Object;LX/49J;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 151968
    const-class v1, LX/0sR;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/0sR;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 151969
    :cond_0
    :goto_0
    monitor-exit v1

    return v0

    .line 151970
    :cond_1
    :try_start_1
    invoke-static {}, LX/0sR;->b()V

    .line 151971
    sget-object v2, LX/0sR;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    int-to-long v2, v2

    sget-object v4, LX/0sR;->a:LX/0sQ;

    .line 151972
    iget-object v6, v4, LX/0sQ;->e:LX/0ad;

    sget v7, LX/16l;->b:I

    const/16 v8, 0xa

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    int-to-long v6, v6

    move-wide v4, v6

    .line 151973
    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 151974
    instance-of v2, p0, LX/15w;

    if-eqz v2, :cond_2

    .line 151975
    check-cast p0, LX/15w;

    invoke-static {p0}, LX/0sR;->a(LX/15w;)Ljava/lang/Object;

    move-result-object p0

    .line 151976
    :cond_2
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 151977
    sget-object v3, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 151978
    sget-object v0, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151979
    const/4 v0, 0x1

    goto :goto_0

    .line 151980
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Ljava/lang/Object;)LX/49J;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 151960
    const-class v1, LX/0sR;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/0sR;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 151961
    :cond_0
    :goto_0
    monitor-exit v1

    return-object v0

    .line 151962
    :cond_1
    :try_start_1
    instance-of v2, p0, LX/15w;

    if-eqz v2, :cond_2

    .line 151963
    check-cast p0, LX/15w;

    invoke-static {p0}, LX/0sR;->a(LX/15w;)Ljava/lang/Object;

    move-result-object p0

    .line 151964
    :cond_2
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 151965
    sget-object v3, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 151966
    sget-object v0, LX/0sR;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/49J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151967
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b()V
    .locals 16

    .prologue
    .line 151947
    const-class v2, LX/0sR;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/0sR;->a:LX/0sQ;

    if-eqz v0, :cond_2

    .line 151948
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 151949
    sget-object v0, LX/0sR;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 151950
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/49J;

    .line 151951
    iget-object v12, v1, LX/49J;->g:LX/0So;

    invoke-interface {v12}, LX/0So;->now()J

    move-result-wide v12

    iget-wide v14, v1, LX/49J;->f:J

    sub-long/2addr v12, v14

    move-wide v6, v12

    .line 151952
    sget-object v1, LX/0sR;->a:LX/0sQ;

    .line 151953
    iget-object v12, v1, LX/0sQ;->e:LX/0ad;

    sget v13, LX/16l;->a:I

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, LX/0ad;->a(II)I

    move-result v12

    mul-int/lit8 v12, v12, 0x2

    int-to-long v12, v12

    move-wide v8, v12

    .line 151954
    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    .line 151955
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151956
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 151957
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 151958
    sget-object v3, LX/0sR;->b:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 151959
    :cond_2
    monitor-exit v2

    return-void
.end method
