.class public LX/1fu;
.super LX/1Cd;
.source ""

# interfaces
.implements LX/1Ce;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2yK;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1fv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6W7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 292440
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 292441
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292442
    iput-object v0, p0, LX/1fu;->a:LX/0Ot;

    .line 292443
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 292444
    iput-object v0, p0, LX/1fu;->c:LX/0Ot;

    .line 292445
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0ih;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 292418
    sget-object v4, LX/Aoz;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 292419
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 292420
    :pswitch_0
    sget-object v0, LX/0ig;->y:LX/0ih;

    .line 292421
    :goto_1
    return-object v0

    .line 292422
    :pswitch_1
    sget-object v0, LX/0ig;->z:LX/0ih;

    goto :goto_1

    .line 292423
    :pswitch_2
    sget-object v0, LX/0ig;->A:LX/0ih;

    goto :goto_1

    .line 292424
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLStorySet;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292438
    invoke-static {p0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 292439
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 292446
    invoke-static {p1}, LX/1fu;->c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v1

    .line 292447
    if-nez v1, :cond_1

    .line 292448
    :cond_0
    :goto_0
    return-void

    .line 292449
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 292450
    iget-object v0, p0, LX/1fu;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6W7;

    .line 292451
    const-string v2, "single_creator_set_impression"

    invoke-static {v2}, LX/6W7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "story_set_id"

    invoke-static {v1}, LX/6W7;->c(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "creator_id"

    invoke-static {v1}, LX/6W7;->d(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "videos_map"

    const/4 v6, 0x1

    .line 292452
    invoke-static {v1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v4

    .line 292453
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v5, "{"

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 292454
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v6

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 292455
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    const/4 p1, 0x0

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 292456
    if-eqz v4, :cond_a

    .line 292457
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-eq p1, v6, :cond_2

    .line 292458
    const-string p1, ","

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292459
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p1, ":"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 292460
    add-int/lit8 v4, v5, 0x1

    :goto_2
    move v5, v4

    .line 292461
    goto :goto_1

    .line 292462
    :cond_3
    const-string v4, "}"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292463
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 292464
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 292465
    invoke-static {v0, v2}, LX/6W7;->a(LX/6W7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 292466
    :cond_4
    invoke-static {v1}, LX/1fu;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0ih;

    move-result-object v2

    .line 292467
    if-eqz v2, :cond_0

    .line 292468
    iget-object v0, p0, LX/1fu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yK;

    .line 292469
    iget-object v3, v0, LX/2yK;->a:LX/0if;

    invoke-static {v0, v1}, LX/2yK;->a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S

    move-result v4

    int-to-long v5, v4

    invoke-virtual {v3, v2, v5, v6}, LX/0if;->a(LX/0ih;J)V

    .line 292470
    iget-object v0, p0, LX/1fu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yK;

    .line 292471
    invoke-static {v0, v1}, LX/2yK;->a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S

    move-result v3

    .line 292472
    sget-object v4, LX/2yK;->d:LX/0ih;

    invoke-virtual {v2, v4}, LX/0ih;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 292473
    iget-object v5, v0, LX/2yK;->a:LX/0if;

    sget-object v6, LX/2yK;->d:LX/0ih;

    int-to-long v7, v3

    const-string v9, "video_sets_tag:v3"

    invoke-virtual {v5, v6, v7, v8, v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 292474
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 292475
    iget-object v6, v0, LX/2yK;->b:LX/1fv;

    .line 292476
    iget-object v7, v6, LX/1fv;->c:Ljava/lang/Boolean;

    if-nez v7, :cond_5

    .line 292477
    iget-object v7, v6, LX/1fv;->a:LX/0ad;

    sget-object v8, LX/0c0;->Live:LX/0c0;

    sget-object v9, LX/0c1;->Off:LX/0c1;

    sget-short v10, LX/1fw;->f:S

    const/4 v11, 0x0

    invoke-interface {v7, v8, v9, v10, v11}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, LX/1fv;->c:Ljava/lang/Boolean;

    .line 292478
    :cond_5
    iget-object v7, v6, LX/1fv;->c:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v6, v7

    .line 292479
    if-eqz v6, :cond_6

    .line 292480
    const-string v6, "Recommended"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292481
    :cond_6
    const-string v6, "BasicBlingbar"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292482
    iget-object v6, v0, LX/2yK;->b:LX/1fv;

    .line 292483
    iget-object v7, v6, LX/1fv;->e:Ljava/lang/Boolean;

    if-nez v7, :cond_7

    .line 292484
    iget-object v7, v6, LX/1fv;->a:LX/0ad;

    sget-object v8, LX/0c0;->Live:LX/0c0;

    sget-object v9, LX/0c1;->Off:LX/0c1;

    sget-short v10, LX/1fw;->d:S

    const/4 v11, 0x0

    invoke-interface {v7, v8, v9, v10, v11}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, LX/1fv;->e:Ljava/lang/Boolean;

    .line 292485
    :cond_7
    iget-object v7, v6, LX/1fv;->e:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v6, v7

    .line 292486
    if-eqz v6, :cond_8

    .line 292487
    const-string v6, "NewAspectRatio"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292488
    :cond_8
    sget-object v6, LX/2yK;->d:LX/0ih;

    invoke-static {v0, v6, v3, v5}, LX/2yK;->a(LX/2yK;LX/0ih;SLjava/util/List;)V

    .line 292489
    :cond_9
    :goto_3
    goto/16 :goto_0

    :cond_a
    move v4, v5

    goto/16 :goto_2

    .line 292490
    :cond_b
    sget-object v4, LX/2yK;->e:LX/0ih;

    invoke-virtual {v2, v4}, LX/0ih;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 292491
    iget-object v5, v0, LX/2yK;->a:LX/0if;

    sget-object v6, LX/2yK;->e:LX/0ih;

    int-to-long v7, v3

    const-string v9, "link_sets_tag:v1"

    invoke-virtual {v5, v6, v7, v8, v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 292492
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 292493
    iget-object v6, v0, LX/2yK;->b:LX/1fv;

    .line 292494
    iget-object v7, v6, LX/1fv;->d:Ljava/lang/Boolean;

    if-nez v7, :cond_c

    .line 292495
    iget-object v7, v6, LX/1fv;->a:LX/0ad;

    sget-object v8, LX/0c0;->Live:LX/0c0;

    sget-object v9, LX/0c1;->Off:LX/0c1;

    sget-short v10, LX/1fw;->e:S

    const/4 v11, 0x0

    invoke-interface {v7, v8, v9, v10, v11}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v6, LX/1fv;->d:Ljava/lang/Boolean;

    .line 292496
    :cond_c
    iget-object v7, v6, LX/1fv;->d:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v6, v7

    .line 292497
    if-eqz v6, :cond_d

    .line 292498
    const-string v6, "Recommended"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292499
    :cond_d
    sget-object v6, LX/2yK;->e:LX/0ih;

    invoke-static {v0, v6, v3, v5}, LX/2yK;->a(LX/2yK;LX/0ih;SLjava/util/List;)V

    .line 292500
    goto :goto_3

    .line 292501
    :cond_e
    sget-object v4, LX/2yK;->f:LX/0ih;

    invoke-virtual {v2, v4}, LX/0ih;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 292502
    iget-object v5, v0, LX/2yK;->a:LX/0if;

    sget-object v6, LX/2yK;->f:LX/0ih;

    int-to-long v7, v3

    const-string v9, "single_creator_video_tag"

    invoke-virtual {v5, v6, v7, v8, v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 292503
    goto :goto_3
.end method

.method public final a()Z
    .locals 6

    .prologue
    .line 292433
    iget-object v0, p0, LX/1fu;->b:LX/1fv;

    .line 292434
    iget-object v1, v0, LX/1fv;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 292435
    iget-object v1, v0, LX/1fv;->a:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/1fw;->c:S

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1fv;->b:Ljava/lang/Boolean;

    .line 292436
    :cond_0
    iget-object v1, v0, LX/1fv;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 292437
    if-nez v0, :cond_1

    iget-object v0, p0, LX/1fu;->b:LX/1fv;

    invoke-virtual {v0}, LX/1fv;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 292425
    invoke-static {p1}, LX/1fu;->c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v1

    .line 292426
    if-nez v1, :cond_1

    .line 292427
    :cond_0
    :goto_0
    return-void

    .line 292428
    :cond_1
    invoke-static {v1}, LX/1fu;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0ih;

    move-result-object v2

    .line 292429
    if-eqz v2, :cond_0

    .line 292430
    iget-object v0, p0, LX/1fu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yK;

    .line 292431
    iget-object v3, v0, LX/2yK;->a:LX/0if;

    invoke-static {v0, v1}, LX/2yK;->a(LX/2yK;Lcom/facebook/graphql/model/GraphQLStorySet;)S

    move-result v4

    int-to-long v5, v4

    invoke-virtual {v3, v2, v5, v6}, LX/0if;->c(LX/0ih;J)V

    .line 292432
    goto :goto_0
.end method
