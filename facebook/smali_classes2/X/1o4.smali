.class public LX/1o4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:[Landroid/widget/ImageView$ScaleType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 318315
    invoke-static {}, Landroid/widget/ImageView$ScaleType;->values()[Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    sput-object v0, LX/1o4;->a:[Landroid/widget/ImageView$ScaleType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;IILX/1no;LX/1dc;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1no;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 318317
    if-nez p4, :cond_0

    .line 318318
    iput v0, p3, LX/1no;->a:I

    .line 318319
    iput v0, p3, LX/1no;->b:I

    .line 318320
    :goto_0
    return-void

    .line 318321
    :cond_0
    invoke-static {p0, p4}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/graphics/drawable/Drawable;

    .line 318322
    if-eqz v6, :cond_1

    :try_start_0
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-gtz v0, :cond_2

    .line 318323
    :cond_1
    const/4 v0, 0x0

    iput v0, p3, LX/1no;->a:I

    .line 318324
    const/4 v0, 0x0

    iput v0, p3, LX/1no;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318325
    invoke-static {p0, v6, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto :goto_0

    .line 318326
    :cond_2
    :try_start_1
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 318327
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 318328
    invoke-static {p1}, LX/1mh;->a(I)I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p2}, LX/1mh;->a(I)I

    move-result v0

    if-nez v0, :cond_3

    .line 318329
    iput v2, p3, LX/1no;->a:I

    .line 318330
    iput v3, p3, LX/1no;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318331
    invoke-static {p0, v6, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto :goto_0

    .line 318332
    :cond_3
    int-to-float v0, v2

    int-to-float v1, v3

    div-float v4, v0, v1

    move v0, p1

    move v1, p2

    move-object v5, p3

    .line 318333
    :try_start_2
    invoke-static/range {v0 .. v5}, LX/1oC;->a(IIIIFLX/1no;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 318334
    invoke-static {p0, v6, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0, v6, p4}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    throw v0
.end method

.method public static a(LX/1De;LX/1Dg;LX/1dc;Landroid/widget/ImageView$ScaleType;LX/1np;LX/1np;LX/1np;)V
    .locals 4
    .param p2    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p3    # Landroid/widget/ImageView$ScaleType;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/components/ComponentLayout;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/widget/ImageView$ScaleType;",
            "LX/1np",
            "<",
            "LX/1oA;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318335
    invoke-static {p0, p2}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 318336
    :try_start_0
    invoke-virtual {p1}, LX/1Dg;->h()I

    move-result v1

    invoke-virtual {p1}, LX/1Dg;->f()I

    move-result v2

    add-int/2addr v1, v2

    .line 318337
    invoke-virtual {p1}, LX/1Dg;->e()I

    move-result v2

    invoke-virtual {p1}, LX/1Dg;->g()I

    move-result v3

    add-int/2addr v2, v3

    .line 318338
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    if-eq v3, p3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    if-gtz v3, :cond_1

    .line 318339
    :cond_0
    const/4 v3, 0x0

    .line 318340
    iput-object v3, p4, LX/1np;->a:Ljava/lang/Object;

    .line 318341
    invoke-virtual {p1}, LX/1Dg;->c()I

    move-result v3

    sub-int v1, v3, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 318342
    iput-object v1, p5, LX/1np;->a:Ljava/lang/Object;

    .line 318343
    invoke-virtual {p1}, LX/1Dg;->d()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 318344
    iput-object v1, p6, LX/1np;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318345
    :goto_0
    invoke-static {p0, v0, p2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 318346
    return-void

    .line 318347
    :cond_1
    :try_start_1
    invoke-virtual {p1}, LX/1Dg;->c()I

    move-result v3

    sub-int v1, v3, v1

    invoke-virtual {p1}, LX/1Dg;->d()I

    move-result v3

    sub-int v2, v3, v2

    invoke-static {v0, p3, v1, v2}, LX/1oA;->a(Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView$ScaleType;II)LX/1oA;

    move-result-object v1

    .line 318348
    iput-object v1, p4, LX/1np;->a:Ljava/lang/Object;

    .line 318349
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 318350
    iput-object v1, p5, LX/1np;->a:Ljava/lang/Object;

    .line 318351
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 318352
    iput-object v1, p6, LX/1np;->a:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318353
    goto :goto_0

    .line 318354
    :catchall_0
    move-exception v1

    invoke-static {p0, v0, p2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    throw v1
.end method
