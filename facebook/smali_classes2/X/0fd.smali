.class public LX/0fd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0fF;

.field public final b:LX/0fG;

.field public c:LX/0h2;

.field public d:LX/0gx;

.field public e:LX/0gg;

.field public f:Z

.field public g:LX/03R;

.field public h:Z


# direct methods
.method public constructor <init>(LX/0fF;LX/0fG;)V
    .locals 1

    .prologue
    .line 108315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108316
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0fd;->f:Z

    .line 108317
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0fd;->g:LX/03R;

    .line 108318
    iput-object p1, p0, LX/0fd;->a:LX/0fF;

    .line 108319
    iput-object p2, p0, LX/0fd;->b:LX/0fG;

    .line 108320
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 108377
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    .line 108378
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 108379
    const-string v1, "target_tab_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108380
    invoke-virtual {v0, v1}, LX/0gx;->b(Ljava/lang/String;)I

    move-result v2

    .line 108381
    iget-object p0, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {p0, v2}, LX/0fd;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108382
    iget-object v2, v0, LX/0gx;->v:LX/0fd;

    .line 108383
    sget-object p0, LX/03R;->NO:LX/03R;

    .line 108384
    iput-object p0, v2, LX/0fd;->g:LX/03R;

    .line 108385
    iget-object v2, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->i()I

    move-result v2

    invoke-virtual {v0, v2}, LX/0gx;->a(I)V

    .line 108386
    :cond_0
    const-string v2, "POP_TO_ROOT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108387
    iget-object v1, v0, LX/0gx;->v:LX/0fd;

    .line 108388
    iget-object v2, v1, LX/0fd;->b:LX/0fG;

    invoke-interface {v2, p1}, LX/0fG;->c(Landroid/content/Intent;)V

    .line 108389
    :cond_1
    return-void
.end method

.method public final b()Lcom/facebook/apptab/state/TabTag;
    .locals 1

    .prologue
    .line 108374
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    if-nez v0, :cond_0

    .line 108375
    const/4 v0, 0x0

    .line 108376
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 108365
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    .line 108366
    if-nez p1, :cond_1

    .line 108367
    sget-object v1, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v2, v0, LX/0gx;->n:LX/0go;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 108368
    iget-object v1, v0, LX/0gx;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Yi;

    .line 108369
    iget-boolean v2, v1, LX/0Yi;->M:Z

    if-eqz v2, :cond_2

    .line 108370
    :cond_0
    :goto_0
    iget-object v1, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->i()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0gx;->a(IF)V

    .line 108371
    iget-object v1, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->u()V

    .line 108372
    :cond_1
    return-void

    .line 108373
    :cond_2
    iget-object v2, v1, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xa0058

    const/4 p0, 0x2

    const/4 p1, 0x0

    invoke-interface {v2, v3, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISI)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/0hO;
    .locals 1

    .prologue
    .line 108364
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v0, p1}, LX/0gx;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108361
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    if-nez v0, :cond_0

    .line 108362
    const/4 v0, 0x0

    .line 108363
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 108357
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 108358
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    invoke-virtual {v0, p1}, LX/0gg;->a(I)V

    .line 108359
    const/4 v0, 0x1

    .line 108360
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 108356
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v0, p1}, LX/0gx;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 108337
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    .line 108338
    iget-object v1, v0, LX/0gx;->z:Ljava/lang/String;

    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108339
    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gx;->d(Ljava/lang/String;)V

    .line 108340
    :cond_0
    :goto_0
    return-void

    .line 108341
    :cond_1
    invoke-virtual {v0}, LX/0gx;->e()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0gx;->z:Ljava/lang/String;

    .line 108342
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 108343
    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/apptab/state/TabTag;

    .line 108344
    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    .line 108345
    iget-object p0, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {p0, v3}, LX/0fd;->e(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object p0

    .line 108346
    if-eqz p0, :cond_3

    .line 108347
    invoke-virtual {v0, v3}, LX/0gx;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v3

    .line 108348
    if-eqz v3, :cond_2

    .line 108349
    invoke-virtual {v0, v1, v3}, LX/0gx;->a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V

    .line 108350
    :cond_2
    iget-object v1, v0, LX/0gx;->v:LX/0fd;

    .line 108351
    iget-object v3, v1, LX/0fd;->e:LX/0gg;

    invoke-virtual {v3, p0, v2}, LX/0gg;->a(Lcom/facebook/katana/fragment/FbChromeFragment;I)V

    .line 108352
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 108353
    :cond_4
    iget-object v1, v0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->h()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v1

    .line 108354
    if-eqz v1, :cond_0

    .line 108355
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108334
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    if-nez v0, :cond_0

    .line 108335
    const/4 v0, 0x0

    .line 108336
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    invoke-virtual {v0, p1}, LX/0gg;->a(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108333
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    invoke-virtual {v0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/facebook/apptab/state/NavigationConfig;
    .locals 1

    .prologue
    .line 108328
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    if-nez v0, :cond_0

    .line 108329
    const/4 v0, 0x0

    .line 108330
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fd;->d:LX/0gx;

    .line 108331
    iget-object p0, v0, LX/0gx;->n:LX/0go;

    invoke-virtual {p0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object p0

    move-object v0, p0

    .line 108332
    goto :goto_0
.end method

.method public final h()Lcom/facebook/katana/fragment/FbChromeFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108325
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    if-nez v0, :cond_0

    .line 108326
    const/4 v0, 0x0

    .line 108327
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    invoke-virtual {v0}, LX/0gg;->b()Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 108324
    iget-object v0, p0, LX/0fd;->e:LX/0gg;

    invoke-virtual {v0}, LX/0gg;->d()I

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 108323
    iget-object v0, p0, LX/0fd;->a:LX/0fF;

    invoke-interface {v0}, LX/0fF;->z()Z

    move-result v0

    return v0
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 108321
    iget-object v0, p0, LX/0fd;->b:LX/0fG;

    invoke-interface {v0}, LX/0fG;->D()V

    .line 108322
    return-void
.end method
