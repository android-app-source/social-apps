.class public LX/1O1;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# static fields
.field public static final b:Landroid/view/animation/Interpolator;

.field public static final c:Landroid/view/animation/Interpolator;

.field public static final d:Landroid/view/animation/Interpolator;

.field private static final e:Landroid/view/animation/Interpolator;


# instance fields
.field public a:Z

.field private final f:[I

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1O5;

.field private i:F

.field private j:Landroid/content/res/Resources;

.field private k:Landroid/view/View;

.field public l:Landroid/view/animation/Animation;

.field public m:F

.field private n:D

.field private o:D

.field private final p:Landroid/graphics/drawable/Drawable$Callback;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 238610
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, LX/1O1;->b:Landroid/view/animation/Interpolator;

    .line 238611
    new-instance v0, LX/1O2;

    invoke-direct {v0}, LX/1O2;-><init>()V

    sput-object v0, LX/1O1;->c:Landroid/view/animation/Interpolator;

    .line 238612
    new-instance v0, LX/1O3;

    invoke-direct {v0}, LX/1O3;-><init>()V

    sput-object v0, LX/1O1;->d:Landroid/view/animation/Interpolator;

    .line 238613
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, LX/1O1;->e:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 238593
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 238594
    new-array v0, v3, [I

    const/4 v1, 0x0

    const/high16 v2, -0x1000000

    aput v2, v0, v1

    iput-object v0, p0, LX/1O1;->f:[I

    .line 238595
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1O1;->g:Ljava/util/ArrayList;

    .line 238596
    new-instance v0, LX/1O4;

    invoke-direct {v0, p0}, LX/1O4;-><init>(LX/1O1;)V

    iput-object v0, p0, LX/1O1;->p:Landroid/graphics/drawable/Drawable$Callback;

    .line 238597
    iput-object p2, p0, LX/1O1;->k:Landroid/view/View;

    .line 238598
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/1O1;->j:Landroid/content/res/Resources;

    .line 238599
    new-instance v0, LX/1O5;

    iget-object v1, p0, LX/1O1;->p:Landroid/graphics/drawable/Drawable$Callback;

    invoke-direct {v0, v1}, LX/1O5;-><init>(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238600
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    iget-object v1, p0, LX/1O1;->f:[I

    invoke-virtual {v0, v1}, LX/1O5;->a([I)V

    .line 238601
    invoke-virtual {p0, v3}, LX/1O1;->a(I)V

    .line 238602
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238603
    new-instance v1, LX/1O6;

    invoke-direct {v1, p0, v0}, LX/1O6;-><init>(LX/1O1;LX/1O5;)V

    .line 238604
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 238605
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 238606
    sget-object v2, LX/1O1;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 238607
    new-instance v2, LX/1O7;

    invoke-direct {v2, p0, v0}, LX/1O7;-><init>(LX/1O1;LX/1O5;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 238608
    iput-object v1, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    .line 238609
    return-void
.end method

.method private a(DDDDFF)V
    .locals 5

    .prologue
    .line 238576
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238577
    iget-object v1, p0, LX/1O1;->j:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 238578
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 238579
    float-to-double v2, v1

    mul-double/2addr v2, p1

    iput-wide v2, p0, LX/1O1;->n:D

    .line 238580
    float-to-double v2, v1

    mul-double/2addr v2, p3

    iput-wide v2, p0, LX/1O1;->o:D

    .line 238581
    double-to-float v2, p7

    mul-float/2addr v2, v1

    .line 238582
    iput v2, v0, LX/1O5;->h:F

    .line 238583
    iget-object v3, v0, LX/1O5;->b:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 238584
    invoke-static {v0}, LX/1O5;->l(LX/1O5;)V

    .line 238585
    float-to-double v2, v1

    mul-double/2addr v2, p5

    invoke-virtual {v0, v2, v3}, LX/1O5;->a(D)V

    .line 238586
    const/4 v2, 0x0

    .line 238587
    iput v2, v0, LX/1O5;->k:I

    .line 238588
    mul-float v2, p9, v1

    mul-float/2addr v1, p10

    .line 238589
    float-to-int v3, v2

    iput v3, v0, LX/1O5;->s:I

    .line 238590
    float-to-int v3, v1

    iput v3, v0, LX/1O5;->t:I

    .line 238591
    iget-wide v2, p0, LX/1O1;->n:D

    double-to-int v1, v2

    iget-wide v2, p0, LX/1O1;->o:D

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, LX/1O5;->a(II)V

    .line 238592
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 238571
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238572
    iget p0, v0, LX/1O5;->q:F

    cmpl-float p0, p1, p0

    if-eqz p0, :cond_0

    .line 238573
    iput p1, v0, LX/1O5;->q:F

    .line 238574
    invoke-static {v0}, LX/1O5;->l(LX/1O5;)V

    .line 238575
    :cond_0
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 238568
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0, p1}, LX/1O5;->b(F)V

    .line 238569
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0, p2}, LX/1O5;->c(F)V

    .line 238570
    return-void
.end method

.method public final a(I)V
    .locals 14
    .param p1    # I
        .annotation build Landroid/support/v4/widget/MaterialProgressDrawable$ProgressDrawableSize;
        .end annotation
    .end param

    .prologue
    const-wide/high16 v2, 0x404c000000000000L    # 56.0

    const-wide/high16 v12, 0x4044000000000000L    # 40.0

    .line 238564
    if-nez p1, :cond_0

    .line 238565
    const-wide/high16 v6, 0x4029000000000000L    # 12.5

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    const/high16 v10, 0x41400000    # 12.0f

    const/high16 v11, 0x40c00000    # 6.0f

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v1 .. v11}, LX/1O1;->a(DDDDFF)V

    .line 238566
    :goto_0
    return-void

    .line 238567
    :cond_0
    const-wide v6, 0x4021800000000000L    # 8.75

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    const/high16 v10, 0x41200000    # 10.0f

    const/high16 v11, 0x40a00000    # 5.0f

    move-object v1, p0

    move-wide v2, v12

    move-wide v4, v12

    invoke-direct/range {v1 .. v11}, LX/1O1;->a(DDDDFF)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 238562
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0, p1}, LX/1O5;->a(Z)V

    .line 238563
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 238559
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238560
    iput p1, v0, LX/1O5;->w:I

    .line 238561
    return-void
.end method

.method public final c(F)V
    .locals 0

    .prologue
    .line 238556
    iput p1, p0, LX/1O1;->i:F

    .line 238557
    invoke-virtual {p0}, LX/1O1;->invalidateSelf()V

    .line 238558
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 238537
    invoke-virtual {p0}, LX/1O1;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 238538
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 238539
    iget v2, p0, LX/1O1;->i:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v4

    invoke-virtual {p1, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 238540
    iget-object v2, p0, LX/1O1;->h:LX/1O5;

    const/high16 v9, 0x43b40000    # 360.0f

    .line 238541
    iget-object v6, v2, LX/1O5;->a:Landroid/graphics/RectF;

    .line 238542
    invoke-virtual {v6, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 238543
    iget v5, v2, LX/1O5;->i:F

    iget v7, v2, LX/1O5;->i:F

    invoke-virtual {v6, v5, v7}, Landroid/graphics/RectF;->inset(FF)V

    .line 238544
    iget v5, v2, LX/1O5;->e:F

    iget v7, v2, LX/1O5;->g:F

    add-float/2addr v5, v7

    mul-float v7, v5, v9

    .line 238545
    iget v5, v2, LX/1O5;->f:F

    iget v8, v2, LX/1O5;->g:F

    add-float/2addr v5, v8

    mul-float/2addr v5, v9

    .line 238546
    sub-float v8, v5, v7

    .line 238547
    iget-object v5, v2, LX/1O5;->b:Landroid/graphics/Paint;

    iget-object v9, v2, LX/1O5;->j:[I

    iget v10, v2, LX/1O5;->k:I

    aget v9, v9, v10

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 238548
    const/4 v9, 0x0

    iget-object v10, v2, LX/1O5;->b:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 238549
    invoke-static {v2, p1, v7, v8, v0}, LX/1O5;->a(LX/1O5;Landroid/graphics/Canvas;FFLandroid/graphics/Rect;)V

    .line 238550
    iget v5, v2, LX/1O5;->u:I

    const/16 v6, 0xff

    if-ge v5, v6, :cond_0

    .line 238551
    iget-object v5, v2, LX/1O5;->v:Landroid/graphics/Paint;

    iget v6, v2, LX/1O5;->w:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 238552
    iget-object v5, v2, LX/1O5;->v:Landroid/graphics/Paint;

    iget v6, v2, LX/1O5;->u:I

    rsub-int v6, v6, 0xff

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 238553
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    iget-object v8, v2, LX/1O5;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 238554
    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 238555
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 238492
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238493
    iget p0, v0, LX/1O5;->u:I

    move v0, p0

    .line 238494
    return v0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 238536
    iget-wide v0, p0, LX/1O1;->o:D

    double-to-int v0, v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 238535
    iget-wide v0, p0, LX/1O1;->n:D

    double-to-int v0, v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 238534
    const/4 v0, -0x3

    return v0
.end method

.method public final isRunning()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 238525
    iget-object v3, p0, LX/1O1;->g:Ljava/util/ArrayList;

    .line 238526
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 238527
    :goto_0
    if-ge v2, v4, :cond_1

    .line 238528
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 238529
    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238530
    const/4 v0, 0x1

    .line 238531
    :goto_1
    return v0

    .line 238532
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 238533
    goto :goto_1
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 238522
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238523
    iput p1, v0, LX/1O5;->u:I

    .line 238524
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 238518
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238519
    iget-object p0, v0, LX/1O5;->b:Landroid/graphics/Paint;

    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 238520
    invoke-static {v0}, LX/1O5;->l(LX/1O5;)V

    .line 238521
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    .line 238502
    iget-object v0, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 238503
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0}, LX/1O5;->j()V

    .line 238504
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238505
    iget v1, v0, LX/1O5;->f:F

    move v0, v1

    .line 238506
    iget-object v1, p0, LX/1O1;->h:LX/1O5;

    .line 238507
    iget v2, v1, LX/1O5;->e:F

    move v1, v2

    .line 238508
    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 238509
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1O1;->a:Z

    .line 238510
    iget-object v0, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x29a

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238511
    iget-object v0, p0, LX/1O1;->k:Landroid/view/View;

    iget-object v1, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 238512
    :goto_0
    return-void

    .line 238513
    :cond_0
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    const/4 v1, 0x0

    .line 238514
    iput v1, v0, LX/1O5;->k:I

    .line 238515
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0}, LX/1O5;->k()V

    .line 238516
    iget-object v0, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x535

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 238517
    iget-object v0, p0, LX/1O1;->k:Landroid/view/View;

    iget-object v1, p0, LX/1O1;->l:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238495
    iget-object v0, p0, LX/1O1;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 238496
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1O1;->c(F)V

    .line 238497
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0, v1}, LX/1O5;->a(Z)V

    .line 238498
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    .line 238499
    iput v1, v0, LX/1O5;->k:I

    .line 238500
    iget-object v0, p0, LX/1O1;->h:LX/1O5;

    invoke-virtual {v0}, LX/1O5;->k()V

    .line 238501
    return-void
.end method
