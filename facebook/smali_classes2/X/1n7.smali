.class public final LX/1n7;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 314403
    invoke-static {}, LX/1n3;->a()LX/1n3;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 314404
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314405
    const-string v0, "DrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 314406
    if-ne p0, p1, :cond_0

    .line 314407
    const/4 v0, 0x1

    .line 314408
    :goto_0
    return v0

    .line 314409
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 314410
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 314411
    :cond_2
    check-cast p1, LX/1n7;

    .line 314412
    iget-object v0, p0, LX/1n7;->a:Landroid/graphics/drawable/Drawable;

    iget-object v1, p1, LX/1n7;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, LX/3CK;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 314413
    iget-object v0, p0, LX/1n7;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1n7;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
