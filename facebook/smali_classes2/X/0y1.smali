.class public final enum LX/0y1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0y1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0y1;

.field public static final enum CHARGING_AC:LX/0y1;

.field public static final enum CHARGING_USB:LX/0y1;

.field public static final enum CHARGING_WIRELESS:LX/0y1;

.field public static final enum DISCHARGING:LX/0y1;

.field public static final enum FULL:LX/0y1;

.field public static final enum NOT_CHARGING:LX/0y1;

.field public static final enum UNKNOWN:LX/0y1;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 163855
    new-instance v0, LX/0y1;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->UNKNOWN:LX/0y1;

    .line 163856
    new-instance v0, LX/0y1;

    const-string v1, "DISCHARGING"

    invoke-direct {v0, v1, v4}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->DISCHARGING:LX/0y1;

    .line 163857
    new-instance v0, LX/0y1;

    const-string v1, "NOT_CHARGING"

    invoke-direct {v0, v1, v5}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->NOT_CHARGING:LX/0y1;

    .line 163858
    new-instance v0, LX/0y1;

    const-string v1, "CHARGING_USB"

    invoke-direct {v0, v1, v6}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->CHARGING_USB:LX/0y1;

    .line 163859
    new-instance v0, LX/0y1;

    const-string v1, "CHARGING_AC"

    invoke-direct {v0, v1, v7}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->CHARGING_AC:LX/0y1;

    .line 163860
    new-instance v0, LX/0y1;

    const-string v1, "CHARGING_WIRELESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->CHARGING_WIRELESS:LX/0y1;

    .line 163861
    new-instance v0, LX/0y1;

    const-string v1, "FULL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0y1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0y1;->FULL:LX/0y1;

    .line 163862
    const/4 v0, 0x7

    new-array v0, v0, [LX/0y1;

    sget-object v1, LX/0y1;->UNKNOWN:LX/0y1;

    aput-object v1, v0, v3

    sget-object v1, LX/0y1;->DISCHARGING:LX/0y1;

    aput-object v1, v0, v4

    sget-object v1, LX/0y1;->NOT_CHARGING:LX/0y1;

    aput-object v1, v0, v5

    sget-object v1, LX/0y1;->CHARGING_USB:LX/0y1;

    aput-object v1, v0, v6

    sget-object v1, LX/0y1;->CHARGING_AC:LX/0y1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0y1;->CHARGING_WIRELESS:LX/0y1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0y1;->FULL:LX/0y1;

    aput-object v2, v0, v1

    sput-object v0, LX/0y1;->$VALUES:[LX/0y1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 163863
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0y1;
    .locals 1

    .prologue
    .line 163864
    const-class v0, LX/0y1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0y1;

    return-object v0
.end method

.method public static values()[LX/0y1;
    .locals 1

    .prologue
    .line 163865
    sget-object v0, LX/0y1;->$VALUES:[LX/0y1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0y1;

    return-object v0
.end method
