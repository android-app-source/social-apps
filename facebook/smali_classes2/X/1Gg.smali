.class public final LX/1Gg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public d:J

.field public e:J

.field public f:J

.field public g:LX/1GU;

.field public h:LX/1GQ;

.field public i:LX/1GE;

.field public j:LX/0pr;

.field public k:Z

.field public final l:Landroid/content/Context;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 225821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225822
    const/4 v0, 0x1

    iput v0, p0, LX/1Gg;->a:I

    .line 225823
    const-string v0, "image_cache"

    iput-object v0, p0, LX/1Gg;->b:Ljava/lang/String;

    .line 225824
    const-wide/32 v0, 0x2800000

    iput-wide v0, p0, LX/1Gg;->d:J

    .line 225825
    const-wide/32 v0, 0xa00000

    iput-wide v0, p0, LX/1Gg;->e:J

    .line 225826
    const-wide/32 v0, 0x200000

    iput-wide v0, p0, LX/1Gg;->f:J

    .line 225827
    new-instance v0, LX/1Gh;

    invoke-direct {v0}, LX/1Gh;-><init>()V

    iput-object v0, p0, LX/1Gg;->g:LX/1GU;

    .line 225828
    iput-object p1, p0, LX/1Gg;->l:Landroid/content/Context;

    .line 225829
    return-void
.end method


# virtual methods
.method public final a()LX/1Gf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 225830
    iget-object v0, p0, LX/1Gg;->c:LX/1Gd;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Gg;->l:Landroid/content/Context;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Either a non-null context or a base directory path or supplier must be provided."

    invoke-static {v0, v2}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 225831
    iget-object v0, p0, LX/1Gg;->c:LX/1Gd;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1Gg;->l:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 225832
    new-instance v0, LX/43f;

    invoke-direct {v0, p0}, LX/43f;-><init>(LX/1Gg;)V

    iput-object v0, p0, LX/1Gg;->c:LX/1Gd;

    .line 225833
    :cond_1
    new-instance v0, LX/1Gf;

    invoke-direct {v0, p0}, LX/1Gf;-><init>(LX/1Gg;)V

    return-object v0

    :cond_2
    move v0, v1

    .line 225834
    goto :goto_0
.end method
