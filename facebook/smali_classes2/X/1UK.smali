.class public LX/1UK;
.super LX/1UE;
.source ""


# instance fields
.field private final a:LX/1Dc;


# direct methods
.method public constructor <init>(LX/1Rq;LX/1Dc;)V
    .locals 0
    .param p1    # LX/1Rq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255769
    invoke-direct {p0, p1}, LX/1UE;-><init>(LX/1Rq;)V

    .line 255770
    iput-object p2, p0, LX/1UK;->a:LX/1Dc;

    .line 255771
    return-void
.end method

.method public static synthetic a(LX/1UK;LX/1a1;I)V
    .locals 0

    .prologue
    .line 255772
    invoke-super {p0, p1, p2}, LX/1UE;->a(LX/1a1;I)V

    return-void
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 255773
    iget-object v0, p0, LX/1UK;->a:LX/1Dc;

    sget-object v1, LX/1aA;->BIND:LX/1aA;

    new-instance v2, LX/1dk;

    invoke-direct {v2, p0, p1, p2}, LX/1dk;-><init>(LX/1UK;LX/1a1;I)V

    invoke-virtual {v0, v1, v2}, LX/1Dc;->a(LX/1aA;Ljava/util/concurrent/Callable;)V

    .line 255774
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    .line 255775
    instance-of v0, v1, LX/1a5;

    if-nez v0, :cond_1

    .line 255776
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, v1

    .line 255777
    check-cast v0, LX/1a5;

    .line 255778
    invoke-virtual {v0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object v2

    .line 255779
    invoke-static {v2}, LX/1aL;->b(Landroid/view/View;)LX/1Ra;

    move-result-object v2

    .line 255780
    if-eqz v2, :cond_0

    .line 255781
    iget-object v3, p0, LX/1UK;->a:LX/1Dc;

    invoke-virtual {v3, v2, v1}, LX/1Dc;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 255782
    instance-of v1, v2, LX/1RZ;

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 255783
    check-cast v1, LX/1RZ;

    .line 255784
    iget-object v2, v1, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 255785
    iget-object v2, v0, LX/1a5;->a:LX/1Rn;

    .line 255786
    iput-object v1, v2, LX/1Rn;->o:Ljava/lang/String;

    .line 255787
    goto :goto_0
.end method
