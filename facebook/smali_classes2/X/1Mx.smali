.class public final LX/1Mx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/1Mw;


# direct methods
.method public constructor <init>(LX/1Mw;)V
    .locals 0

    .prologue
    .line 236435
    iput-object p1, p0, LX/1Mx;->a:LX/1Mw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, 0x36c110c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 236436
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 236437
    iget-object v3, p0, LX/1Mx;->a:LX/1Mw;

    sget-object v0, LX/0oz;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    .line 236438
    iput-object v0, v3, LX/1Mw;->f:LX/0p3;

    .line 236439
    iget-object v3, p0, LX/1Mx;->a:LX/1Mw;

    sget-object v0, LX/0oz;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    .line 236440
    iput-object v0, v3, LX/1Mw;->h:LX/0p3;

    .line 236441
    iget-object v0, p0, LX/1Mx;->a:LX/1Mw;

    sget-object v3, LX/0oz;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 236442
    iput-boolean v2, v0, LX/1Mw;->i:Z

    .line 236443
    iget-object v0, p0, LX/1Mx;->a:LX/1Mw;

    iget-object v0, v0, LX/1Mw;->f:LX/0p3;

    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    if-ne v0, v2, :cond_0

    .line 236444
    iget-object v0, p0, LX/1Mx;->a:LX/1Mw;

    iget-object v2, p0, LX/1Mx;->a:LX/1Mw;

    iget-object v2, v2, LX/1Mw;->d:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    .line 236445
    iput-object v2, v0, LX/1Mw;->g:LX/0p3;

    .line 236446
    :cond_0
    iget-object v0, p0, LX/1Mx;->a:LX/1Mw;

    iget-object v0, v0, LX/1Mw;->c:LX/Bhd;

    if-eqz v0, :cond_1

    .line 236447
    iget-object v0, p0, LX/1Mx;->a:LX/1Mw;

    iget-object v0, v0, LX/1Mw;->c:LX/Bhd;

    invoke-virtual {v0}, LX/Bhd;->invalidate()V

    .line 236448
    :cond_1
    const/16 v0, 0x27

    const v2, -0x7867c39b

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
