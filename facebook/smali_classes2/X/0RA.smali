.class public final LX/0RA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 59709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Set;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59703
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 59704
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    .line 59705
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    .line 59706
    goto :goto_0

    :cond_0
    move v2, v1

    .line 59707
    goto :goto_1

    .line 59708
    :cond_1
    return v0
.end method

.method public static varargs a(Ljava/lang/Enum;[Ljava/lang/Enum;)LX/0Rf;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum",
            "<TE;>;>(TE;[TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59702
    invoke-static {p0, p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, LX/1Y6;->a(Ljava/util/EnumSet;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)LX/0Rf;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum",
            "<TE;>;>(",
            "Ljava/lang/Iterable",
            "<TE;>;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59686
    instance-of v0, p0, LX/1Y6;

    if-eqz v0, :cond_0

    .line 59687
    check-cast p0, LX/1Y6;

    .line 59688
    :goto_0
    return-object p0

    .line 59689
    :cond_0
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 59690
    check-cast p0, Ljava/util/Collection;

    .line 59691
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59692
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object p0, v0

    .line 59693
    goto :goto_0

    .line 59694
    :cond_1
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, LX/1Y6;->a(Ljava/util/EnumSet;)LX/0Rf;

    move-result-object p0

    goto :goto_0

    .line 59695
    :cond_2
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 59696
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59697
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 59698
    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 59699
    invoke-static {v0}, LX/1Y6;->a(Ljava/util/EnumSet;)LX/0Rf;

    move-result-object p0

    goto :goto_0

    .line 59700
    :cond_3
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object p0, v0

    .line 59701
    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum",
            "<TE;>;>(",
            "Ljava/lang/Iterable",
            "<TE;>;",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Ljava/util/EnumSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59683
    invoke-static {p1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 59684
    invoke-static {v0, p0}, LX/0Ph;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 59685
    return-object v0
.end method

.method public static a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/HashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59682
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method

.method public static a(I)Ljava/util/HashSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/HashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59681
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, LX/0PM;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    return-object v0
.end method

.method public static varargs a([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "Ljava/util/HashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59678
    array-length v0, p0

    invoke-static {v0}, LX/0RA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    .line 59679
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 59680
    return-object v0
.end method

.method public static a(Ljava/util/NavigableSet;LX/0Rl;)Ljava/util/NavigableSet;
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/NavigableSet",
            "<TE;>;",
            "LX/0Rl",
            "<-TE;>;)",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 59673
    instance-of v0, p0, LX/302;

    if-eqz v0, :cond_0

    .line 59674
    check-cast p0, LX/302;

    .line 59675
    iget-object v0, p0, LX/303;->b:LX/0Rl;

    invoke-static {v0, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v2

    .line 59676
    new-instance v1, LX/50a;

    iget-object v0, p0, LX/303;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    invoke-direct {v1, v0, v2}, LX/50a;-><init>(Ljava/util/NavigableSet;LX/0Rl;)V

    move-object v0, v1

    .line 59677
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, LX/50a;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rl;

    invoke-direct {v2, v0, v1}, LX/50a;-><init>(Ljava/util/NavigableSet;LX/0Rl;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/util/Set;LX/0Rl;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TE;>;",
            "LX/0Rl",
            "<-TE;>;)",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 59658
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 59659
    check-cast p0, Ljava/util/SortedSet;

    .line 59660
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_2

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, LX/0RA;->a(Ljava/util/NavigableSet;LX/0Rl;)Ljava/util/NavigableSet;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 59661
    :goto_1
    return-object v0

    .line 59662
    :cond_0
    instance-of v0, p0, LX/302;

    if-eqz v0, :cond_1

    .line 59663
    check-cast p0, LX/302;

    .line 59664
    iget-object v0, p0, LX/303;->b:LX/0Rl;

    invoke-static {v0, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v2

    .line 59665
    new-instance v1, LX/302;

    iget-object v0, p0, LX/303;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-direct {v1, v0, v2}, LX/302;-><init>(Ljava/util/Set;LX/0Rl;)V

    move-object v0, v1

    goto :goto_1

    .line 59666
    :cond_1
    new-instance v2, LX/302;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rl;

    invoke-direct {v2, v0, v1}, LX/302;-><init>(Ljava/util/Set;LX/0Rl;)V

    move-object v0, v2

    goto :goto_1

    .line 59667
    :cond_2
    instance-of v0, p0, LX/302;

    if-eqz v0, :cond_3

    .line 59668
    check-cast p0, LX/302;

    .line 59669
    iget-object v0, p0, LX/303;->b:LX/0Rl;

    invoke-static {v0, p1}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v2

    .line 59670
    new-instance v1, LX/50Z;

    iget-object v0, p0, LX/303;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-direct {v1, v0, v2}, LX/50Z;-><init>(Ljava/util/SortedSet;LX/0Rl;)V

    move-object v0, v1

    .line 59671
    :goto_2
    move-object v0, v0

    .line 59672
    goto :goto_0

    :cond_3
    new-instance v2, LX/50Z;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rl;

    invoke-direct {v2, v0, v1}, LX/50Z;-><init>(Ljava/util/SortedSet;LX/0Rl;)V

    move-object v0, v2

    goto :goto_2
.end method

.method public static a(Ljava/util/Comparator;)Ljava/util/TreeSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;)",
            "Ljava/util/TreeSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59710
    new-instance v1, Ljava/util/TreeSet;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    return-object v1
.end method

.method public static a(Ljava/util/Set;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59650
    if-ne p0, p1, :cond_1

    .line 59651
    :cond_0
    :goto_0
    return v0

    .line 59652
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_3

    .line 59653
    check-cast p1, Ljava/util/Set;

    .line 59654
    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 59655
    :catch_0
    move v0, v1

    goto :goto_0

    .line 59656
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 59657
    goto :goto_0
.end method

.method public static a(Ljava/util/Set;Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 59644
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59645
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_0

    .line 59646
    check-cast p1, LX/1M1;

    invoke-interface {p1}, LX/1M1;->d()Ljava/util/Set;

    move-result-object p1

    .line 59647
    :cond_0
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 59648
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, LX/0RZ;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    .line 59649
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, LX/0RA;->a(Ljava/util/Set;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Set;Ljava/util/Iterator;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<*>;",
            "Ljava/util/Iterator",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 59640
    const/4 v0, 0x0

    .line 59641
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59642
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 59643
    :cond_0
    return v0
.end method

.method public static b(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TE;>;",
            "Ljava/util/Set",
            "<*>;)",
            "LX/0Ro",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59615
    const-string v0, "set1"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59616
    const-string v0, "set2"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59617
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    .line 59618
    new-instance v1, LX/50Y;

    invoke-direct {v1, p0, v0, p1}, LX/50Y;-><init>(Ljava/util/Set;LX/0Rl;Ljava/util/Set;)V

    return-object v1
.end method

.method public static b(Ljava/lang/Iterable;)Ljava/util/HashSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Ljava/util/HashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59633
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    .line 59634
    check-cast p0, Ljava/util/Collection;

    move-object v1, p0

    .line 59635
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 59636
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 59637
    invoke-static {v1, v0}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 59638
    move-object v0, v1

    .line 59639
    goto :goto_0
.end method

.method public static b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59632
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, LX/0P9;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TE;>;",
            "Ljava/util/Set",
            "<*>;)",
            "LX/0Ro",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59628
    const-string v0, "set1"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59629
    const-string v0, "set2"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59630
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-static {v0}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v0

    .line 59631
    new-instance v1, LX/0Rn;

    invoke-direct {v1, p0, v0, p1}, LX/0Rn;-><init>(Ljava/util/Set;LX/0Rl;Ljava/util/Set;)V

    return-object v1
.end method

.method public static c()Ljava/util/LinkedHashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/LinkedHashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59627
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    return-object v0
.end method

.method public static c(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Ljava/util/LinkedHashSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59620
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 59621
    new-instance v0, Ljava/util/LinkedHashSet;

    .line 59622
    check-cast p0, Ljava/util/Collection;

    move-object v1, p0

    .line 59623
    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 59624
    :goto_0
    return-object v0

    .line 59625
    :cond_0
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 59626
    invoke-static {v0, p0}, LX/0Ph;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0
.end method

.method public static d()Ljava/util/TreeSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ljava/lang/Comparable;",
            ">()",
            "Ljava/util/TreeSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 59619
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    return-object v0
.end method
