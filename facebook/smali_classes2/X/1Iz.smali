.class public LX/1Iz;
.super LX/0jZ;
.source ""


# static fields
.field public static final b:Ljava/lang/String;

.field private static final c:[I


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AjQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0pn;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/0qz;

.field private final g:LX/0tX;

.field public final h:LX/0pm;

.field public final i:LX/187;

.field public final j:LX/1J1;

.field private final k:LX/0oy;

.field public final l:LX/0pV;

.field public final m:LX/0pW;

.field public final n:LX/0qf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 229560
    const-class v0, LX/1Iz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Iz;->b:Ljava/lang/String;

    .line 229561
    new-array v0, v2, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    sput-object v0, LX/1Iz;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;LX/1J1;LX/0pn;Ljava/util/concurrent/ExecutorService;LX/0qz;LX/0tX;LX/0pm;LX/187;LX/0oy;LX/0pV;LX/0pW;LX/0qf;)V
    .locals 2
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1J1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/FeedFetchExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229546
    sget-object v0, LX/1Iz;->c:[I

    invoke-direct {p0, p1, v0}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 229547
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1Iz;->a:Ljava/util/ArrayList;

    .line 229548
    iput-object p3, p0, LX/1Iz;->d:LX/0pn;

    .line 229549
    iput-object p4, p0, LX/1Iz;->e:Ljava/util/concurrent/ExecutorService;

    .line 229550
    iput-object p5, p0, LX/1Iz;->f:LX/0qz;

    .line 229551
    iput-object p6, p0, LX/1Iz;->g:LX/0tX;

    .line 229552
    iput-object p7, p0, LX/1Iz;->h:LX/0pm;

    .line 229553
    iput-object p8, p0, LX/1Iz;->i:LX/187;

    .line 229554
    iput-object p2, p0, LX/1Iz;->j:LX/1J1;

    .line 229555
    iput-object p9, p0, LX/1Iz;->k:LX/0oy;

    .line 229556
    iput-object p10, p0, LX/1Iz;->l:LX/0pV;

    .line 229557
    iput-object p11, p0, LX/1Iz;->m:LX/0pW;

    .line 229558
    iput-object p12, p0, LX/1Iz;->n:LX/0qf;

    .line 229559
    return-void
.end method

.method private a(LX/AjQ;)V
    .locals 2

    .prologue
    .line 229535
    const-string v0, "FreshFeedNetworkHandler.doFreeNetworkCallback"

    const v1, 0x58277510

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229536
    :try_start_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 229537
    iput-object v1, p1, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    .line 229538
    iput-object v1, p1, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    .line 229539
    iput-boolean v0, p1, LX/AjQ;->e:Z

    .line 229540
    iput v0, p1, LX/AjQ;->f:I

    .line 229541
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p1, LX/AjQ;->a:LX/0Pz;

    .line 229542
    iget-object v0, p0, LX/1Iz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229543
    const v0, -0x650a4b8f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229544
    return-void

    .line 229545
    :catchall_0
    move-exception v0

    const v1, 0x60ad8cbd    # 1.00044625E20f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static b(LX/1Iz;LX/AjQ;)V
    .locals 1

    .prologue
    .line 229533
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, LX/1Iz;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1Iz;->sendMessage(Landroid/os/Message;)Z

    .line 229534
    return-void
.end method

.method private b(LX/1J0;)V
    .locals 7

    .prologue
    .line 229515
    const-string v0, "FreshFeedNetworkHandler.doFetchNewStoriesFromNetwork"

    const v1, 0x5d4acd26

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 229516
    :try_start_0
    iget-object v0, p0, LX/1Iz;->d:LX/0pn;

    iget-object v1, p1, LX/1J0;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v1}, LX/0pn;->c(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v2

    .line 229517
    iget-object v0, p0, LX/1Iz;->f:LX/0qz;

    iget-object v1, p1, LX/1J0;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, p0, LX/1Iz;->k:LX/0oy;

    invoke-virtual {v3}, LX/0oy;->n()I

    move-result v3

    iget-boolean v4, p1, LX/1J0;->i:Z

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    iget-object v6, p1, LX/1J0;->h:LX/0gf;

    invoke-virtual/range {v0 .. v6}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v1

    .line 229518
    iget-object v0, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v0}, LX/0rB;->b()LX/0rn;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 229519
    iget-object v0, p0, LX/1Iz;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229520
    iget-object v0, p0, LX/1Iz;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AjQ;

    .line 229521
    :goto_0
    iput-object p1, v0, LX/AjQ;->c:LX/1J0;

    .line 229522
    iput-object v1, v0, LX/AjQ;->d:Lcom/facebook/api/feed/FetchFeedParams;

    .line 229523
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/AjQ;->e:Z

    .line 229524
    const/4 v3, 0x0

    iput v3, v0, LX/AjQ;->f:I

    .line 229525
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    iput-object v3, v0, LX/AjQ;->a:LX/0Pz;

    .line 229526
    iget-object v3, p0, LX/1Iz;->l:LX/0pV;

    sget-object v4, LX/1Iz;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Starting New Stories Fetch. Cursor: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 229527
    iget-object v2, p1, LX/1J0;->c:LX/0rB;

    invoke-virtual {v2}, LX/0rB;->b()LX/0rn;

    move-result-object v2

    iget-object v3, p1, LX/1J0;->d:Ljava/lang/String;

    invoke-virtual {v2, v1, v3, v0}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;

    move-result-object v0

    .line 229528
    iget-object v1, p0, LX/1Iz;->g:LX/0tX;

    iget-object v2, p0, LX/1Iz;->e:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1, v0, v2}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229529
    const v0, -0x421335c4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 229530
    return-void

    .line 229531
    :cond_0
    :try_start_1
    new-instance v0, LX/AjQ;

    invoke-direct {v0, p0}, LX/AjQ;-><init>(LX/1Iz;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 229532
    :catchall_0
    move-exception v0

    const v1, 0x7b3c6f09

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 229510
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 229511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229512
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1J0;

    invoke-direct {p0, v0}, LX/1Iz;->b(LX/1J0;)V

    .line 229513
    :goto_0
    return-void

    .line 229514
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/AjQ;

    invoke-direct {p0, v0}, LX/1Iz;->a(LX/AjQ;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
