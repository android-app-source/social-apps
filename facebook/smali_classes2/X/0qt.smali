.class public final LX/0qt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0qs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0qs",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0fz;


# direct methods
.method public constructor <init>(LX/0fz;)V
    .locals 0

    .prologue
    .line 148613
    iput-object p1, p0, LX/0qt;->a:LX/0fz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/Object;Z)V
    .locals 3

    .prologue
    .line 148606
    check-cast p3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148607
    iget-object v0, p0, LX/0qt;->a:LX/0fz;

    iget-object v0, v0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->b()I

    move-result v0

    .line 148608
    iget-object v1, p0, LX/0qt;->a:LX/0fz;

    iget-object v1, v1, LX/0fz;->h:LX/0qr;

    add-int v2, p1, v0

    add-int/2addr v0, p2

    invoke-virtual {v1, v2, v0, p3, p4}, LX/0qr;->a(IILjava/lang/Object;Z)V

    .line 148609
    return-void
.end method

.method public final a(ILjava/lang/Object;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 148614
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    check-cast p3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148615
    iget-object v0, p0, LX/0qt;->a:LX/0fz;

    iget-object v0, v0, LX/0fz;->h:LX/0qr;

    iget-object v1, p0, LX/0qt;->a:LX/0fz;

    iget-object v1, v1, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/0qr;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 148616
    return-void
.end method

.method public final a(ILjava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 148610
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148611
    iget-object v0, p0, LX/0qt;->a:LX/0fz;

    iget-object v0, v0, LX/0fz;->h:LX/0qr;

    iget-object v1, p0, LX/0qt;->a:LX/0fz;

    iget-object v1, v1, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2, p3}, LX/0qr;->a(ILjava/lang/Object;Z)V

    .line 148612
    return-void
.end method

.method public final b(ILjava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 148603
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148604
    iget-object v0, p0, LX/0qt;->a:LX/0fz;

    iget-object v0, v0, LX/0fz;->h:LX/0qr;

    iget-object v1, p0, LX/0qt;->a:LX/0fz;

    iget-object v1, v1, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2, p3}, LX/0qr;->b(ILjava/lang/Object;Z)V

    .line 148605
    return-void
.end method
