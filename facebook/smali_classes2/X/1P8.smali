.class public final LX/1P8;
.super Landroid/database/Observable;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/database/Observable",
        "<",
        "LX/1OD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 243653
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 243651
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/1P8;->a(IILjava/lang/Object;)V

    .line 243652
    return-void
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 243647
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 243648
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2, p3}, LX/1OD;->a(IILjava/lang/Object;)V

    .line 243649
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 243650
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 243654
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 243643
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 243644
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0}, LX/1OD;->a()V

    .line 243645
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 243646
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 243639
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 243640
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2}, LX/1OD;->b(II)V

    .line 243641
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 243642
    :cond_0
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 243635
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 243636
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    invoke-virtual {v0, p1, p2}, LX/1OD;->c(II)V

    .line 243637
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 243638
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 3

    .prologue
    .line 243631
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 243632
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OD;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, p2, v2}, LX/1OD;->a(III)V

    .line 243633
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 243634
    :cond_0
    return-void
.end method
