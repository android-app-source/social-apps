.class public LX/1BA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1iq;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/localstats/LocalStatsValuesListener;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1B9;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1BB;

.field private f:LX/1BB;

.field private g:I


# direct methods
.method public constructor <init>(LX/13S;Ljava/util/Set;Ljava/util/Set;)V
    .locals 4
    .param p2    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/13S;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/localstats/LocalStatsValuesListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/1B9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212341
    new-instance v0, LX/1BB;

    invoke-direct {v0}, LX/1BB;-><init>()V

    iput-object v0, p0, LX/1BA;->e:LX/1BB;

    .line 212342
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1BA;->a:Ljava/util/Set;

    .line 212343
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1BA;->c:Ljava/util/Set;

    .line 212344
    const/4 v0, -0x1

    iput v0, p0, LX/1BA;->g:I

    .line 212345
    iput-object p2, p0, LX/1BA;->b:Ljava/util/Set;

    .line 212346
    iget-object v0, p0, LX/1BA;->c:Ljava/util/Set;

    new-instance v1, LX/1B9;

    const-string v2, "counters"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/1B9;-><init>(Ljava/lang/String;LX/0P1;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 212347
    iget-object v0, p0, LX/1BA;->c:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 212348
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 212349
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B9;

    .line 212350
    iget-object v3, v0, LX/1B9;->b:LX/0P1;

    move-object v0, v3

    .line 212351
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 212352
    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 212353
    :cond_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1BA;->d:LX/0Rf;

    .line 212354
    iget-object v0, p0, LX/1BA;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B9;

    .line 212355
    iget-object v2, v0, LX/1B9;->a:Ljava/lang/String;

    move-object v2, v2

    .line 212356
    const-string v3, "quick_data"

    new-instance p2, LX/1BD;

    invoke-direct {p2, p0, v0}, LX/1BD;-><init>(LX/1BA;LX/1B9;)V

    invoke-virtual {p1, v2, v3, p2}, LX/13S;->a(Ljava/lang/String;Ljava/lang/String;LX/13W;)LX/13S;

    goto :goto_1

    .line 212357
    :cond_2
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 212336
    iget-object v1, p0, LX/1BA;->a:Ljava/util/Set;

    monitor-enter v1

    .line 212337
    :try_start_0
    iget-object v0, p0, LX/1BA;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iq;

    .line 212338
    invoke-interface {v0}, LX/1iq;->a()V

    goto :goto_0

    .line 212339
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static b(LX/1BA;I)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 212331
    iget v0, p0, LX/1BA;->g:I

    if-eq v0, p1, :cond_0

    .line 212332
    iput p1, p0, LX/1BA;->g:I

    .line 212333
    invoke-direct {p0}, LX/1BA;->a()V

    .line 212334
    iget-object v0, p0, LX/1BA;->e:LX/1BB;

    invoke-virtual {v0}, LX/1BB;->b()LX/1BB;

    move-result-object v0

    iput-object v0, p0, LX/1BA;->f:LX/1BB;

    .line 212335
    :cond_0
    return-void
.end method

.method public static b(LX/1BA;ISJ)V
    .locals 10

    .prologue
    .line 212326
    iget-object v0, p0, LX/1BA;->b:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 212327
    iget-object v0, p0, LX/1BA;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ph;

    .line 212328
    const/16 v3, 0x20

    const/16 v4, 0x33

    const v5, 0xffff

    and-int/2addr v5, p2

    const/high16 v6, -0x80000000

    or-int/2addr v6, v5

    move v5, p1

    move-wide v7, p3

    invoke-static/range {v3 .. v8}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 212329
    goto :goto_0

    .line 212330
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/1B9;I)LX/0lF;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1B9;",
            "I)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 212320
    invoke-static {p0, p2}, LX/1BA;->b(LX/1BA;I)V

    .line 212321
    iget-object v0, p0, LX/1BA;->f:LX/1BB;

    .line 212322
    iget v1, v0, LX/1BB;->b:I

    if-nez v1, :cond_1

    iget-object v1, v0, LX/1BB;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 212323
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1BA;->f:LX/1BB;

    .line 212324
    iget-object v1, p1, LX/1B9;->b:LX/0P1;

    move-object v1, v1

    .line 212325
    iget-object v2, p0, LX/1BA;->d:LX/0Rf;

    invoke-virtual {v0, v1, v2}, LX/1BB;->a(LX/0P1;LX/0Rf;)LX/0lF;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    .line 212317
    iget-object v0, p0, LX/1BA;->e:LX/1BB;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/1BB;->a(ISJ)V

    .line 212318
    invoke-static {p0, p1, v1, v2, v3}, LX/1BA;->b(LX/1BA;ISJ)V

    .line 212319
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 212314
    iget-object v0, p0, LX/1BA;->e:LX/1BB;

    invoke-virtual {v0, p1, v1, p2, p3}, LX/1BB;->a(ISJ)V

    .line 212315
    invoke-static {p0, p1, v1, p2, p3}, LX/1BA;->b(LX/1BA;ISJ)V

    .line 212316
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 212312
    iget-object v0, p0, LX/1BA;->e:LX/1BB;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, LX/1BB;->a(ISLjava/lang/String;)V

    .line 212313
    return-void
.end method

.method public final a(IS)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 212306
    iget-object v0, p0, LX/1BA;->e:LX/1BB;

    invoke-virtual {v0, p1, p2, v2, v3}, LX/1BB;->a(ISJ)V

    .line 212307
    invoke-static {p0, p1, p2, v2, v3}, LX/1BA;->b(LX/1BA;ISJ)V

    .line 212308
    return-void
.end method

.method public final a(LX/1iq;)V
    .locals 2

    .prologue
    .line 212309
    iget-object v1, p0, LX/1BA;->a:Ljava/util/Set;

    monitor-enter v1

    .line 212310
    :try_start_0
    iget-object v0, p0, LX/1BA;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 212311
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
