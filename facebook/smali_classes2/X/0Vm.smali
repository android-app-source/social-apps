.class public final enum LX/0Vm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Vm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Vm;

.field public static final enum BACKGROUND:LX/0Vm;

.field public static final enum UI:LX/0Vm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68046
    new-instance v0, LX/0Vm;

    const-string v1, "UI"

    invoke-direct {v0, v1, v2}, LX/0Vm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Vm;->UI:LX/0Vm;

    .line 68047
    new-instance v0, LX/0Vm;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v3}, LX/0Vm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Vm;->BACKGROUND:LX/0Vm;

    .line 68048
    const/4 v0, 0x2

    new-array v0, v0, [LX/0Vm;

    sget-object v1, LX/0Vm;->UI:LX/0Vm;

    aput-object v1, v0, v2

    sget-object v1, LX/0Vm;->BACKGROUND:LX/0Vm;

    aput-object v1, v0, v3

    sput-object v0, LX/0Vm;->$VALUES:[LX/0Vm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68045
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Vm;
    .locals 1

    .prologue
    .line 68044
    const-class v0, LX/0Vm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Vm;

    return-object v0
.end method

.method public static values()[LX/0Vm;
    .locals 1

    .prologue
    .line 68043
    sget-object v0, LX/0Vm;->$VALUES:[LX/0Vm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Vm;

    return-object v0
.end method
