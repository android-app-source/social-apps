.class public LX/1nu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1nu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1nv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 317595
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 317596
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1nu;->b:LX/0Zi;

    .line 317597
    iput-object p1, p0, LX/1nu;->a:LX/0Ot;

    .line 317598
    return-void
.end method

.method public static a(LX/0QB;)LX/1nu;
    .locals 4

    .prologue
    .line 317599
    const-class v1, LX/1nu;

    monitor-enter v1

    .line 317600
    :try_start_0
    sget-object v0, LX/1nu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 317601
    sput-object v2, LX/1nu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 317602
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317603
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 317604
    new-instance v3, LX/1nu;

    const/16 p0, 0x964

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1nu;-><init>(LX/0Ot;)V

    .line 317605
    move-object v0, v3

    .line 317606
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 317607
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1nu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317608
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 317609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 27

    .prologue
    .line 317610
    check-cast p2, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    .line 317611
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1nu;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nv;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->d:LX/1Pp;

    move-object/from16 v0, p2

    iget-boolean v7, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->e:Z

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->f:LX/4Ab;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->g:LX/1Up;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->h:Landroid/graphics/PointF;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->j:LX/1Up;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->k:LX/1dc;

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->l:LX/1Up;

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->m:LX/1dc;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->n:LX/1Up;

    move-object/from16 v16, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->o:I

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->p:LX/1dc;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->q:LX/1Up;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->s:F

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->t:Landroid/graphics/ColorFilter;

    move-object/from16 v22, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->u:LX/33B;

    move-object/from16 v23, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->v:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->w:LX/1Ai;

    move-object/from16 v25, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->x:LX/1f9;

    move-object/from16 v26, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v26}, LX/1nv;->a(LX/1De;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/1Pp;ZLX/4Ab;LX/1Up;Landroid/graphics/PointF;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;LX/1dc;FLandroid/graphics/ColorFilter;LX/33B;Ljava/lang/String;LX/1Ai;LX/1f9;)LX/1Dg;

    move-result-object v1

    .line 317612
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317613
    invoke-static {}, LX/1dS;->b()V

    .line 317614
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1nw;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1nu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 317615
    new-instance v1, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;-><init>(LX/1nu;)V

    .line 317616
    iget-object v2, p0, LX/1nu;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nw;

    .line 317617
    if-nez v2, :cond_0

    .line 317618
    new-instance v2, LX/1nw;

    invoke-direct {v2, p0}, LX/1nw;-><init>(LX/1nu;)V

    .line 317619
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1nw;->a$redex0(LX/1nw;LX/1De;IILcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;)V

    .line 317620
    move-object v1, v2

    .line 317621
    move-object v0, v1

    .line 317622
    return-object v0
.end method
