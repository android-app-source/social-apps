.class public LX/1On;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Oo;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1lw;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1lw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Oq;

.field public d:Ljava/lang/Runnable;

.field public final e:Z

.field public final f:LX/1Or;

.field private g:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "LX/1lw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Oq;)V
    .locals 1

    .prologue
    .line 242333
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1On;-><init>(LX/1Oq;Z)V

    .line 242334
    return-void
.end method

.method private constructor <init>(LX/1Oq;Z)V
    .locals 2

    .prologue
    .line 242325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242326
    new-instance v0, LX/0Zj;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    iput-object v0, p0, LX/1On;->g:LX/0Zk;

    .line 242327
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    .line 242328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    .line 242329
    iput-object p1, p0, LX/1On;->c:LX/1Oq;

    .line 242330
    iput-boolean p2, p0, LX/1On;->e:Z

    .line 242331
    new-instance v0, LX/1Or;

    invoke-direct {v0, p0}, LX/1Or;-><init>(LX/1Oo;)V

    iput-object v0, p0, LX/1On;->f:LX/1Or;

    .line 242332
    return-void
.end method

.method private a(LX/1lw;I)V
    .locals 3

    .prologue
    .line 242319
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v0, p1}, LX/1Oq;->a(LX/1lw;)V

    .line 242320
    iget v0, p1, LX/1lw;->a:I

    packed-switch v0, :pswitch_data_0

    .line 242321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "only remove and update ops can be dispatched in first pass"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242322
    :pswitch_0
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->d:I

    invoke-interface {v0, p2, v1}, LX/1Oq;->a(II)V

    .line 242323
    :goto_0
    return-void

    .line 242324
    :pswitch_1
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->d:I

    iget-object v2, p1, LX/1lw;->c:Ljava/lang/Object;

    invoke-interface {v0, p2, v1, v2}, LX/1Oq;->a(IILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1lw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242313
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 242314
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 242315
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    .line 242316
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 242317
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 242318
    return-void
.end method

.method private c(II)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    .line 242073
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 242074
    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v1, p1

    :goto_0
    if-ltz v4, :cond_e

    .line 242075
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242076
    iget v2, v0, LX/1lw;->a:I

    if-ne v2, v6, :cond_9

    .line 242077
    iget v2, v0, LX/1lw;->b:I

    iget v3, v0, LX/1lw;->d:I

    if-ge v2, v3, :cond_1

    .line 242078
    iget v3, v0, LX/1lw;->b:I

    .line 242079
    iget v2, v0, LX/1lw;->d:I

    .line 242080
    :goto_1
    if-lt v1, v3, :cond_6

    if-gt v1, v2, :cond_6

    .line 242081
    iget v2, v0, LX/1lw;->b:I

    if-ne v3, v2, :cond_3

    .line 242082
    if-nez p2, :cond_2

    .line 242083
    iget v2, v0, LX/1lw;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1lw;->d:I

    .line 242084
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    .line 242085
    :goto_3
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 242086
    :cond_1
    iget v3, v0, LX/1lw;->d:I

    .line 242087
    iget v2, v0, LX/1lw;->b:I

    goto :goto_1

    .line 242088
    :cond_2
    if-ne p2, v5, :cond_0

    .line 242089
    iget v2, v0, LX/1lw;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/1lw;->d:I

    goto :goto_2

    .line 242090
    :cond_3
    if-nez p2, :cond_5

    .line 242091
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1lw;->b:I

    .line 242092
    :cond_4
    :goto_4
    add-int/lit8 v0, v1, -0x1

    goto :goto_3

    .line 242093
    :cond_5
    if-ne p2, v5, :cond_4

    .line 242094
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/1lw;->b:I

    goto :goto_4

    .line 242095
    :cond_6
    iget v2, v0, LX/1lw;->b:I

    if-ge v1, v2, :cond_8

    .line 242096
    if-nez p2, :cond_7

    .line 242097
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1lw;->b:I

    .line 242098
    iget v2, v0, LX/1lw;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1lw;->d:I

    move v0, v1

    goto :goto_3

    .line 242099
    :cond_7
    if-ne p2, v5, :cond_8

    .line 242100
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/1lw;->b:I

    .line 242101
    iget v2, v0, LX/1lw;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/1lw;->d:I

    :cond_8
    move v0, v1

    .line 242102
    goto :goto_3

    .line 242103
    :cond_9
    iget v2, v0, LX/1lw;->b:I

    if-gt v2, v1, :cond_b

    .line 242104
    iget v2, v0, LX/1lw;->a:I

    if-nez v2, :cond_a

    .line 242105
    iget v0, v0, LX/1lw;->d:I

    sub-int v0, v1, v0

    goto :goto_3

    .line 242106
    :cond_a
    iget v2, v0, LX/1lw;->a:I

    if-ne v2, v5, :cond_d

    .line 242107
    iget v0, v0, LX/1lw;->d:I

    add-int/2addr v0, v1

    goto :goto_3

    .line 242108
    :cond_b
    if-nez p2, :cond_c

    .line 242109
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/1lw;->b:I

    move v0, v1

    goto :goto_3

    .line 242110
    :cond_c
    if-ne p2, v5, :cond_d

    .line 242111
    iget v2, v0, LX/1lw;->b:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/1lw;->b:I

    :cond_d
    move v0, v1

    goto :goto_3

    .line 242112
    :cond_e
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_5
    if-ltz v2, :cond_12

    .line 242113
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242114
    iget v3, v0, LX/1lw;->a:I

    if-ne v3, v6, :cond_11

    .line 242115
    iget v3, v0, LX/1lw;->d:I

    iget v4, v0, LX/1lw;->b:I

    if-eq v3, v4, :cond_f

    iget v3, v0, LX/1lw;->d:I

    if-gez v3, :cond_10

    .line 242116
    :cond_f
    iget-object v3, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 242117
    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    .line 242118
    :cond_10
    :goto_6
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_5

    .line 242119
    :cond_11
    iget v3, v0, LX/1lw;->d:I

    if-gtz v3, :cond_10

    .line 242120
    iget-object v3, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 242121
    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    goto :goto_6

    .line 242122
    :cond_12
    return v1
.end method

.method public static c(LX/1On;I)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242299
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 242300
    :goto_0
    if-ge v3, v4, :cond_3

    .line 242301
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242302
    iget v5, v0, LX/1lw;->a:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 242303
    iget v0, v0, LX/1lw;->d:I

    add-int/lit8 v5, v3, 0x1

    invoke-direct {p0, v0, v5}, LX/1On;->d(II)I

    move-result v0

    if-ne v0, p1, :cond_2

    move v0, v1

    .line 242304
    :goto_1
    return v0

    .line 242305
    :cond_0
    iget v5, v0, LX/1lw;->a:I

    if-nez v5, :cond_2

    .line 242306
    iget v5, v0, LX/1lw;->b:I

    iget v6, v0, LX/1lw;->d:I

    add-int/2addr v5, v6

    .line 242307
    iget v0, v0, LX/1lw;->b:I

    :goto_2
    if-ge v0, v5, :cond_2

    .line 242308
    add-int/lit8 v6, v3, 0x1

    invoke-direct {p0, v0, v6}, LX/1On;->d(II)I

    move-result v6

    if-ne v6, p1, :cond_1

    move v0, v1

    .line 242309
    goto :goto_1

    .line 242310
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 242311
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 242312
    goto :goto_1
.end method

.method private d(II)I
    .locals 5

    .prologue
    .line 242280
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, p1

    .line 242281
    :goto_0
    if-ge p2, v2, :cond_4

    .line 242282
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242283
    iget v3, v0, LX/1lw;->a:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 242284
    iget v3, v0, LX/1lw;->b:I

    if-ne v3, v1, :cond_1

    .line 242285
    iget v1, v0, LX/1lw;->d:I

    .line 242286
    :cond_0
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 242287
    :cond_1
    iget v3, v0, LX/1lw;->b:I

    if-ge v3, v1, :cond_2

    .line 242288
    add-int/lit8 v1, v1, -0x1

    .line 242289
    :cond_2
    iget v0, v0, LX/1lw;->d:I

    if-gt v0, v1, :cond_0

    .line 242290
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 242291
    :cond_3
    iget v3, v0, LX/1lw;->b:I

    if-gt v3, v1, :cond_0

    .line 242292
    iget v3, v0, LX/1lw;->a:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 242293
    iget v3, v0, LX/1lw;->b:I

    iget v4, v0, LX/1lw;->d:I

    add-int/2addr v3, v4

    if-ge v1, v3, :cond_5

    .line 242294
    const/4 v1, -0x1

    .line 242295
    :cond_4
    return v1

    .line 242296
    :cond_5
    iget v0, v0, LX/1lw;->d:I

    sub-int/2addr v1, v0

    goto :goto_1

    .line 242297
    :cond_6
    iget v3, v0, LX/1lw;->a:I

    if-nez v3, :cond_0

    .line 242298
    iget v0, v0, LX/1lw;->d:I

    add-int/2addr v1, v0

    goto :goto_1
.end method

.method public static e(LX/1On;LX/1lw;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 242251
    iget v0, p1, LX/1lw;->a:I

    if-eqz v0, :cond_0

    iget v0, p1, LX/1lw;->a:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 242252
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "should not dispatch add or move for pre layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242253
    :cond_1
    iget v0, p1, LX/1lw;->b:I

    iget v3, p1, LX/1lw;->a:I

    invoke-direct {p0, v0, v3}, LX/1On;->c(II)I

    move-result v4

    .line 242254
    iget v3, p1, LX/1lw;->b:I

    .line 242255
    iget v0, p1, LX/1lw;->a:I

    packed-switch v0, :pswitch_data_0

    .line 242256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "op should be remove or update."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v0, v1

    :goto_0
    move v5, v1

    move v6, v4

    move v4, v3

    move v3, v1

    .line 242257
    :goto_1
    iget v7, p1, LX/1lw;->d:I

    if-ge v3, v7, :cond_6

    .line 242258
    iget v7, p1, LX/1lw;->b:I

    mul-int v8, v0, v3

    add-int/2addr v7, v8

    .line 242259
    iget v8, p1, LX/1lw;->a:I

    invoke-direct {p0, v7, v8}, LX/1On;->c(II)I

    move-result v8

    .line 242260
    iget v7, p1, LX/1lw;->a:I

    packed-switch v7, :pswitch_data_1

    move v7, v2

    .line 242261
    :goto_2
    if-eqz v7, :cond_4

    .line 242262
    add-int/lit8 v5, v5, 0x1

    .line 242263
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_1
    move v0, v2

    .line 242264
    goto :goto_0

    .line 242265
    :pswitch_2
    add-int/lit8 v7, v6, 0x1

    if-ne v8, v7, :cond_2

    move v7, v1

    goto :goto_2

    :cond_2
    move v7, v2

    goto :goto_2

    .line 242266
    :pswitch_3
    if-ne v8, v6, :cond_3

    move v7, v1

    goto :goto_2

    :cond_3
    move v7, v2

    goto :goto_2

    .line 242267
    :cond_4
    iget v7, p1, LX/1lw;->a:I

    iget-object v9, p1, LX/1lw;->c:Ljava/lang/Object;

    invoke-virtual {p0, v7, v6, v5, v9}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v6

    .line 242268
    invoke-direct {p0, v6, v4}, LX/1On;->a(LX/1lw;I)V

    .line 242269
    invoke-virtual {p0, v6}, LX/1On;->a(LX/1lw;)V

    .line 242270
    iget v6, p1, LX/1lw;->a:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_5

    .line 242271
    add-int/2addr v4, v5

    :cond_5
    move v5, v1

    move v6, v8

    .line 242272
    goto :goto_3

    .line 242273
    :cond_6
    iget-object v0, p1, LX/1lw;->c:Ljava/lang/Object;

    .line 242274
    invoke-virtual {p0, p1}, LX/1On;->a(LX/1lw;)V

    .line 242275
    if-lez v5, :cond_7

    .line 242276
    iget v1, p1, LX/1lw;->a:I

    invoke-virtual {p0, v1, v6, v5, v0}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v0

    .line 242277
    invoke-direct {p0, v0, v4}, LX/1On;->a(LX/1lw;I)V

    .line 242278
    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    .line 242279
    :cond_7
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static g(LX/1On;LX/1lw;)V
    .locals 4

    .prologue
    .line 242243
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242244
    iget v0, p1, LX/1lw;->a:I

    packed-switch v0, :pswitch_data_0

    .line 242245
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown update op type for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242246
    :pswitch_0
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-interface {v0, v1, v2}, LX/1Oq;->c(II)V

    .line 242247
    :goto_0
    return-void

    .line 242248
    :pswitch_1
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-interface {v0, v1, v2}, LX/1Oq;->d(II)V

    goto :goto_0

    .line 242249
    :pswitch_2
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    invoke-interface {v0, v1, v2}, LX/1Oq;->b(II)V

    goto :goto_0

    .line 242250
    :pswitch_3
    iget-object v0, p0, LX/1On;->c:LX/1Oq;

    iget v1, p1, LX/1lw;->b:I

    iget v2, p1, LX/1lw;->d:I

    iget-object v3, p1, LX/1lw;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, LX/1Oq;->a(IILjava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 242242
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1On;->d(II)I

    move-result v0

    return v0
.end method

.method public final a(IIILjava/lang/Object;)LX/1lw;
    .locals 1

    .prologue
    .line 242234
    iget-object v0, p0, LX/1On;->g:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242235
    if-nez v0, :cond_0

    .line 242236
    new-instance v0, LX/1lw;

    invoke-direct {v0, p1, p2, p3, p4}, LX/1lw;-><init>(IIILjava/lang/Object;)V

    .line 242237
    :goto_0
    return-object v0

    .line 242238
    :cond_0
    iput p1, v0, LX/1lw;->a:I

    .line 242239
    iput p2, v0, LX/1lw;->b:I

    .line 242240
    iput p3, v0, LX/1lw;->d:I

    .line 242241
    iput-object p4, v0, LX/1lw;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 242231
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, LX/1On;->a(Ljava/util/List;)V

    .line 242232
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, LX/1On;->a(Ljava/util/List;)V

    .line 242233
    return-void
.end method

.method public final a(LX/1lw;)V
    .locals 1

    .prologue
    .line 242227
    iget-boolean v0, p0, LX/1On;->e:Z

    if-nez v0, :cond_0

    .line 242228
    const/4 v0, 0x0

    iput-object v0, p1, LX/1lw;->c:Ljava/lang/Object;

    .line 242229
    iget-object v0, p0, LX/1On;->g:LX/0Zk;

    invoke-interface {v0, p1}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 242230
    :cond_0
    return-void
.end method

.method public final a(III)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 242221
    if-ne p1, p2, :cond_1

    .line 242222
    :cond_0
    :goto_0
    return v0

    .line 242223
    :cond_1
    if-eq p3, v1, :cond_2

    .line 242224
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Moving more than 1 item is not supported yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242225
    :cond_2
    iget-object v2, p0, LX/1On;->a:Ljava/util/ArrayList;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, p1, p2, v4}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242226
    iget-object v2, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 13

    .prologue
    .line 242148
    iget-object v0, p0, LX/1On;->f:LX/1Or;

    iget-object v1, p0, LX/1On;->a:Ljava/util/ArrayList;

    .line 242149
    :goto_0
    const/4 v3, 0x0

    .line 242150
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v4, v2, -0x1

    :goto_1
    if-ltz v4, :cond_f

    .line 242151
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1lw;

    .line 242152
    iget v2, v2, LX/1lw;->a:I

    const/4 v5, 0x3

    if-ne v2, v5, :cond_e

    .line 242153
    if-eqz v3, :cond_10

    move v2, v4

    .line 242154
    :goto_2
    move v2, v2

    .line 242155
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 242156
    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v1, v2, v3}, LX/1Or;->a(LX/1Or;Ljava/util/List;II)V

    goto :goto_0

    .line 242157
    :cond_0
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 242158
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_d

    .line 242159
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242160
    iget v3, v0, LX/1lw;->a:I

    packed-switch v3, :pswitch_data_0

    .line 242161
    :goto_4
    iget-object v0, p0, LX/1On;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 242162
    iget-object v0, p0, LX/1On;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 242163
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 242164
    :pswitch_0
    invoke-static {p0, v0}, LX/1On;->g(LX/1On;LX/1lw;)V

    goto :goto_4

    .line 242165
    :pswitch_1
    const/4 v11, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 242166
    iget v10, v0, LX/1lw;->b:I

    .line 242167
    iget v3, v0, LX/1lw;->b:I

    iget v6, v0, LX/1lw;->d:I

    add-int v7, v3, v6

    .line 242168
    const/4 v8, -0x1

    .line 242169
    iget v6, v0, LX/1lw;->b:I

    move v9, v5

    :goto_5
    if-ge v6, v7, :cond_5

    .line 242170
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v3, v6}, LX/1Oq;->a(I)LX/1a1;

    move-result-object v3

    .line 242171
    if-nez v3, :cond_2

    invoke-static {p0, v6}, LX/1On;->c(LX/1On;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 242172
    :cond_2
    if-nez v8, :cond_13

    .line 242173
    invoke-virtual {p0, v4, v10, v9, v11}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    .line 242174
    invoke-static {p0, v3}, LX/1On;->e(LX/1On;LX/1lw;)V

    move v3, v4

    :goto_6
    move v8, v4

    .line 242175
    :goto_7
    if-eqz v3, :cond_4

    .line 242176
    sub-int v3, v6, v9

    .line 242177
    sub-int v6, v7, v9

    move v7, v4

    .line 242178
    :goto_8
    add-int/lit8 v3, v3, 0x1

    move v9, v7

    move v7, v6

    move v6, v3

    goto :goto_5

    .line 242179
    :cond_3
    if-ne v8, v4, :cond_12

    .line 242180
    invoke-virtual {p0, v4, v10, v9, v11}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    .line 242181
    invoke-static {p0, v3}, LX/1On;->g(LX/1On;LX/1lw;)V

    move v3, v4

    :goto_9
    move v8, v5

    .line 242182
    goto :goto_7

    .line 242183
    :cond_4
    add-int/lit8 v3, v9, 0x1

    move v12, v6

    move v6, v7

    move v7, v3

    move v3, v12

    goto :goto_8

    .line 242184
    :cond_5
    iget v3, v0, LX/1lw;->d:I

    if-eq v9, v3, :cond_6

    .line 242185
    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    .line 242186
    invoke-virtual {p0, v4, v10, v9, v11}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v0

    .line 242187
    :cond_6
    if-nez v8, :cond_11

    .line 242188
    invoke-static {p0, v0}, LX/1On;->e(LX/1On;LX/1lw;)V

    .line 242189
    :goto_a
    goto :goto_4

    .line 242190
    :pswitch_2
    const/4 v7, 0x1

    const/4 v11, 0x2

    const/4 v4, 0x0

    .line 242191
    iget v5, v0, LX/1lw;->b:I

    .line 242192
    iget v3, v0, LX/1lw;->b:I

    iget v6, v0, LX/1lw;->d:I

    add-int v9, v3, v6

    .line 242193
    const/4 v3, -0x1

    .line 242194
    iget v6, v0, LX/1lw;->b:I

    move v8, v3

    move v3, v4

    :goto_b
    if-ge v6, v9, :cond_b

    .line 242195
    iget-object v10, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v10, v6}, LX/1Oq;->a(I)LX/1a1;

    move-result-object v10

    .line 242196
    if-nez v10, :cond_7

    invoke-static {p0, v6}, LX/1On;->c(LX/1On;I)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 242197
    :cond_7
    if-nez v8, :cond_8

    .line 242198
    iget-object v8, v0, LX/1lw;->c:Ljava/lang/Object;

    invoke-virtual {p0, v11, v5, v3, v8}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    .line 242199
    invoke-static {p0, v3}, LX/1On;->e(LX/1On;LX/1lw;)V

    move v3, v4

    move v5, v6

    :cond_8
    move v8, v5

    move v5, v3

    move v3, v7

    .line 242200
    :goto_c
    add-int/lit8 v5, v5, 0x1

    .line 242201
    add-int/lit8 v6, v6, 0x1

    move v12, v3

    move v3, v5

    move v5, v8

    move v8, v12

    goto :goto_b

    .line 242202
    :cond_9
    if-ne v8, v7, :cond_a

    .line 242203
    iget-object v8, v0, LX/1lw;->c:Ljava/lang/Object;

    invoke-virtual {p0, v11, v5, v3, v8}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v3

    .line 242204
    invoke-static {p0, v3}, LX/1On;->g(LX/1On;LX/1lw;)V

    move v3, v4

    move v5, v6

    :cond_a
    move v8, v5

    move v5, v3

    move v3, v4

    .line 242205
    goto :goto_c

    .line 242206
    :cond_b
    iget v4, v0, LX/1lw;->d:I

    if-eq v3, v4, :cond_c

    .line 242207
    iget-object v4, v0, LX/1lw;->c:Ljava/lang/Object;

    .line 242208
    invoke-virtual {p0, v0}, LX/1On;->a(LX/1lw;)V

    .line 242209
    invoke-virtual {p0, v11, v5, v3, v4}, LX/1On;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v0

    .line 242210
    :cond_c
    if-nez v8, :cond_14

    .line 242211
    invoke-static {p0, v0}, LX/1On;->e(LX/1On;LX/1lw;)V

    .line 242212
    :goto_d
    goto/16 :goto_4

    .line 242213
    :pswitch_3
    invoke-static {p0, v0}, LX/1On;->g(LX/1On;LX/1lw;)V

    goto/16 :goto_4

    .line 242214
    :cond_d
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 242215
    return-void

    .line 242216
    :cond_e
    const/4 v2, 0x1

    .line 242217
    :goto_e
    add-int/lit8 v4, v4, -0x1

    move v3, v2

    goto/16 :goto_1

    .line 242218
    :cond_f
    const/4 v2, -0x1

    goto/16 :goto_2

    :cond_10
    move v2, v3

    goto :goto_e

    .line 242219
    :cond_11
    invoke-static {p0, v0}, LX/1On;->g(LX/1On;LX/1lw;)V

    goto :goto_a

    :cond_12
    move v3, v5

    goto/16 :goto_9

    :cond_13
    move v3, v5

    goto/16 :goto_6

    .line 242220
    :cond_14
    invoke-static {p0, v0}, LX/1On;->g(LX/1On;LX/1lw;)V

    goto :goto_d

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 242142
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 242143
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 242144
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    invoke-interface {v3, v0}, LX/1Oq;->b(LX/1lw;)V

    .line 242145
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 242146
    :cond_0
    iget-object v0, p0, LX/1On;->b:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, LX/1On;->a(Ljava/util/List;)V

    .line 242147
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 242141
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 242123
    invoke-virtual {p0}, LX/1On;->c()V

    .line 242124
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 242125
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 242126
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lw;

    .line 242127
    iget v3, v0, LX/1lw;->a:I

    packed-switch v3, :pswitch_data_0

    .line 242128
    :goto_1
    iget-object v0, p0, LX/1On;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 242129
    iget-object v0, p0, LX/1On;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 242130
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 242131
    :pswitch_0
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v3, v0}, LX/1Oq;->b(LX/1lw;)V

    .line 242132
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    iget v4, v0, LX/1lw;->b:I

    iget v0, v0, LX/1lw;->d:I

    invoke-interface {v3, v4, v0}, LX/1Oq;->c(II)V

    goto :goto_1

    .line 242133
    :pswitch_1
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v3, v0}, LX/1Oq;->b(LX/1lw;)V

    .line 242134
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    iget v4, v0, LX/1lw;->b:I

    iget v0, v0, LX/1lw;->d:I

    invoke-interface {v3, v4, v0}, LX/1Oq;->a(II)V

    goto :goto_1

    .line 242135
    :pswitch_2
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v3, v0}, LX/1Oq;->b(LX/1lw;)V

    .line 242136
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    iget v4, v0, LX/1lw;->b:I

    iget v5, v0, LX/1lw;->d:I

    iget-object v0, v0, LX/1lw;->c:Ljava/lang/Object;

    invoke-interface {v3, v4, v5, v0}, LX/1Oq;->a(IILjava/lang/Object;)V

    goto :goto_1

    .line 242137
    :pswitch_3
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    invoke-interface {v3, v0}, LX/1Oq;->b(LX/1lw;)V

    .line 242138
    iget-object v3, p0, LX/1On;->c:LX/1Oq;

    iget v4, v0, LX/1lw;->b:I

    iget v0, v0, LX/1lw;->d:I

    invoke-interface {v3, v4, v0}, LX/1Oq;->d(II)V

    goto :goto_1

    .line 242139
    :cond_1
    iget-object v0, p0, LX/1On;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, LX/1On;->a(Ljava/util/List;)V

    .line 242140
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
