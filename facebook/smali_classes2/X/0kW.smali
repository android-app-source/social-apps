.class public LX/0kW;
.super LX/0kX;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0WJ;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13l;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field public final e:Landroid/app/Activity;

.field private final f:LX/0QA;

.field private g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

.field public h:Ljava/lang/String;

.field public final i:LX/0hx;

.field public final j:LX/03V;

.field public k:LX/0fK;

.field private final l:LX/0l0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127312
    const-class v0, LX/0kW;

    sput-object v0, LX/0kW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 127316
    invoke-direct {p0}, LX/0kX;-><init>()V

    .line 127317
    const/4 v0, 0x0

    iput-object v0, p0, LX/0kW;->h:Ljava/lang/String;

    .line 127318
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127319
    iput-object p1, p0, LX/0kW;->e:Landroid/app/Activity;

    .line 127320
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    iput-object v0, p0, LX/0kW;->f:LX/0QA;

    .line 127321
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v0

    check-cast v0, LX/0hx;

    iput-object v0, p0, LX/0kW;->i:LX/0hx;

    .line 127322
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/0kW;->j:LX/03V;

    .line 127323
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    iput-object v0, p0, LX/0kW;->b:LX/0WJ;

    .line 127324
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/0l0;->a(LX/0QB;)LX/0l0;

    move-result-object v0

    check-cast v0, LX/0l0;

    iput-object v0, p0, LX/0kW;->l:LX/0l0;

    .line 127325
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    const/16 v1, 0x134d

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/0kW;->c:LX/0Ot;

    .line 127326
    return-void
.end method

.method private a(LX/0fK;)V
    .locals 1

    .prologue
    .line 127313
    new-instance v0, LX/10r;

    invoke-direct {v0, p0}, LX/10r;-><init>(LX/0kW;)V

    .line 127314
    iput-object v0, p1, LX/0fK;->m:LX/10s;

    .line 127315
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 127308
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    const v1, 0x7f0d11fe

    const v2, 0x7f0b2085

    .line 127309
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 127310
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p0}, LX/0iT;->a(Landroid/view/View;I)V

    .line 127311
    return-void
.end method

.method public static n(LX/0kW;)LX/0fK;
    .locals 1

    .prologue
    .line 127305
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    instance-of v0, v0, LX/0fD;

    if-eqz v0, :cond_0

    .line 127306
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    check-cast v0, LX/0fD;

    invoke-interface {v0}, LX/0fD;->e()LX/0fK;

    move-result-object v0

    .line 127307
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0kW;->k:LX/0fK;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 127302
    packed-switch p1, :pswitch_data_0

    .line 127303
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 127304
    :pswitch_0
    new-instance v0, LX/Gtx;

    iget-object v1, p0, LX/0kW;->e:Landroid/app/Activity;

    const v2, 0x7f0e0a54

    invoke-direct {v0, v1, v2}, LX/Gtx;-><init>(Landroid/app/Activity;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x28d04514
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 127288
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 127289
    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    .line 127290
    :goto_0
    if-nez v1, :cond_3

    .line 127291
    :cond_0
    :goto_1
    instance-of v0, p1, LX/0fD;

    if-eqz v0, :cond_1

    .line 127292
    check-cast p1, LX/0fD;

    invoke-interface {p1}, LX/0fD;->e()LX/0fK;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0kW;->a(LX/0fK;)V

    .line 127293
    :cond_1
    return-void

    .line 127294
    :cond_2
    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v1, v0

    goto :goto_0

    .line 127295
    :cond_3
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    instance-of v0, v0, LX/10W;

    if-eqz v0, :cond_4

    .line 127296
    new-instance v0, LX/Gtd;

    invoke-direct {v0, p0}, LX/Gtd;-><init>(LX/0kW;)V

    .line 127297
    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 127298
    :cond_4
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    instance-of v0, v0, LX/10X;

    if-eqz v0, :cond_0

    .line 127299
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    check-cast v0, LX/10X;

    invoke-interface {v0}, LX/10X;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 127300
    new-instance v0, LX/Gte;

    invoke-direct {v0, p0}, LX/Gte;-><init>(LX/0kW;)V

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 127301
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    goto :goto_1
.end method

.method public final a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 127286
    invoke-direct {p0}, LX/0kW;->l()V

    .line 127287
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 127327
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v0

    .line 127328
    if-eqz v0, :cond_0

    .line 127329
    invoke-virtual {v0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 127330
    :cond_0
    return-void
.end method

.method public final a(ILandroid/app/Dialog;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127281
    packed-switch p1, :pswitch_data_0

    .line 127282
    :goto_0
    return v3

    .line 127283
    :pswitch_0
    sget-object v0, LX/Gtw;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 127284
    iget-object v0, p0, LX/0kW;->j:LX/03V;

    const-string v1, "MobileCanvas"

    const-string v2, "Trying to open a Mobile Canvas Dialog with a null URL"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127285
    :cond_0
    iget-object v0, p0, LX/0kW;->j:LX/03V;

    const-string v1, "MobileCanvas"

    const-string v2, "Trying to open a Mobile Canvas Dialog on a non-faceweb view."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x28d04514
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127259
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 127260
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 127261
    if-eqz v0, :cond_0

    .line 127262
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/3zo;->a(LX/0QB;)LX/3zo;

    move-result-object v0

    check-cast v0, LX/3zo;

    .line 127263
    invoke-static {v0}, LX/3zo;->b(LX/3zo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127264
    iget-object v1, v0, LX/3zo;->d:LX/3zq;

    sget-object p2, LX/3zr;->BACK_PRESSED:LX/3zr;

    .line 127265
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127266
    new-instance v0, Lcom/facebook/analytics/useractions/utils/UserActionEvent;

    invoke-direct {v0, p2}, Lcom/facebook/analytics/useractions/utils/UserActionEvent;-><init>(LX/3zr;)V

    invoke-static {v1, v0}, LX/3zq;->a(LX/3zq;Lcom/facebook/analytics/useractions/utils/UserActionEvent;)V

    .line 127267
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 127268
    const/4 v0, 0x1

    .line 127269
    :goto_0
    move v0, v0

    .line 127270
    if-eqz v0, :cond_2

    .line 127271
    iget-object v0, p0, LX/0kW;->k:LX/0fK;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0kW;->k:LX/0fK;

    invoke-virtual {v0}, LX/0fK;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 127272
    iget-object v0, p0, LX/0kW;->k:LX/0fK;

    invoke-virtual {v0}, LX/0fK;->fU_()Z

    .line 127273
    const/4 v0, 0x1

    .line 127274
    :goto_1
    move v0, v0

    .line 127275
    if-nez v0, :cond_1

    .line 127276
    invoke-virtual {p1}, Landroid/app/Activity;->onBackPressed()V

    .line 127277
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 127278
    :goto_2
    return-object v0

    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 127279
    :cond_4
    iget-object v0, p0, LX/0kW;->i:LX/0hx;

    const-string v1, "back_button"

    const-string p2, "android_button"

    const/4 p3, 0x0

    invoke-virtual {v0, v1, p2, p3}, LX/0hx;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127280
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 127237
    iget-object v0, p0, LX/0kW;->g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    if-eqz v0, :cond_0

    .line 127238
    iget-object v0, p0, LX/0kW;->g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    .line 127239
    iget-object p1, v0, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->b:Landroid/os/Handler;

    invoke-static {p1, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 127240
    const/4 v0, 0x0

    iput-object v0, p0, LX/0kW;->g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    .line 127241
    :cond_0
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    instance-of v0, v0, LX/0l6;

    if-nez v0, :cond_2

    .line 127242
    const/4 v0, 0x0

    .line 127243
    iget-object v1, p0, LX/0kW;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 127244
    if-nez v1, :cond_3

    .line 127245
    :cond_1
    :goto_0
    move v0, v0

    .line 127246
    if-eqz v0, :cond_2

    .line 127247
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 127248
    :goto_1
    return-void

    .line 127249
    :cond_2
    invoke-direct {p0}, LX/0kW;->l()V

    .line 127250
    iget-object v0, p0, LX/0kW;->e:Landroid/app/Activity;

    .line 127251
    invoke-static {v0}, LX/0l0;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 127252
    iget-object p1, p0, LX/0kW;->j:LX/03V;

    invoke-virtual {p1, v1}, LX/03V;->d(Ljava/lang/String;)V

    .line 127253
    goto :goto_1

    .line 127254
    :cond_3
    iget-object p1, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, p1

    .line 127255
    if-eqz v1, :cond_1

    .line 127256
    iget-object p1, p0, LX/0kW;->h:Ljava/lang/String;

    if-nez p1, :cond_4

    .line 127257
    iput-object v1, p0, LX/0kW;->h:Ljava/lang/String;

    .line 127258
    :cond_4
    iget-object p1, p0, LX/0kW;->h:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 127236
    iget-object v0, p0, LX/0kW;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 127227
    iget-object v0, p0, LX/0kW;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 127228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127229
    iget-object v1, p0, LX/0kW;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127230
    iget-object v1, p0, LX/0kW;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, LX/Ehu;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 127231
    if-eqz v1, :cond_0

    .line 127232
    const/16 v2, 0x40

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 127233
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127234
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0kW;->d:Ljava/lang/String;

    .line 127235
    :cond_1
    iget-object v0, p0, LX/0kW;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 127211
    iget-object v0, p0, LX/0kW;->g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    invoke-static {v0}, LX/0sL;->b(Ljava/lang/Object;)V

    .line 127212
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, LX/Gu0;

    invoke-direct {v1}, LX/Gu0;-><init>()V

    const v2, 0x88b8

    invoke-static {v0, v1, v2}, Lcom/facebook/katana/activity/faceweb/FacewebAssassin;->a(Landroid/os/Handler;LX/Gtz;I)Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    move-result-object v0

    iput-object v0, p0, LX/0kW;->g:Lcom/facebook/katana/activity/faceweb/FacewebAssassin;

    .line 127213
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 127216
    invoke-static {p0}, LX/0kW;->n(LX/0kW;)LX/0fK;

    move-result-object v0

    .line 127217
    if-nez v0, :cond_0

    .line 127218
    iget-object v0, p0, LX/0kW;->f:LX/0QA;

    invoke-static {v0}, LX/10Y;->b(LX/0QB;)LX/0fK;

    move-result-object v0

    check-cast v0, LX/0fK;

    .line 127219
    invoke-direct {p0, v0}, LX/0kW;->a(LX/0fK;)V

    .line 127220
    iput-object v0, p0, LX/0kW;->k:LX/0fK;

    .line 127221
    :cond_0
    iget-object v1, p0, LX/0kW;->e:Landroid/app/Activity;

    invoke-virtual {v0, v1}, LX/0fK;->a(Landroid/app/Activity;)V

    .line 127222
    invoke-virtual {v0}, LX/0fK;->g()V

    .line 127223
    invoke-static {p0}, LX/0kW;->n(LX/0kW;)LX/0fK;

    move-result-object v0

    .line 127224
    if-eqz v0, :cond_1

    .line 127225
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/0fK;->a_(Z)V

    .line 127226
    :cond_1
    return-void
.end method

.method public final g()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127214
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 127215
    return-object v0
.end method
