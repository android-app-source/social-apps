.class public LX/18E;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final g:LX/0Tn;

.field private static final h:LX/0Tn;

.field private static volatile m:LX/18E;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AnM;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Ym;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0qj;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 206039
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "inv_notifier/time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/18E;->g:LX/0Tn;

    .line 206040
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "inv_notifier/count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/18E;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 206041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206042
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 206043
    iput-object v0, p0, LX/18E;->j:LX/0Ot;

    .line 206044
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 206045
    iput-object v0, p0, LX/18E;->k:LX/0Ot;

    .line 206046
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, LX/18E;->l:Ljava/util/concurrent/atomic/AtomicReference;

    .line 206047
    return-void
.end method

.method public static a(LX/0QB;)LX/18E;
    .locals 12

    .prologue
    .line 206048
    sget-object v0, LX/18E;->m:LX/18E;

    if-nez v0, :cond_1

    .line 206049
    const-class v1, LX/18E;

    monitor-enter v1

    .line 206050
    :try_start_0
    sget-object v0, LX/18E;->m:LX/18E;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206051
    if-eqz v2, :cond_0

    .line 206052
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 206053
    new-instance v3, LX/18E;

    invoke-direct {v3}, LX/18E;-><init>()V

    .line 206054
    const/16 v4, 0x1d66

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, Landroid/content/Context;

    const-class v6, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v5, v6}, LX/0QC;->getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v5

    const/16 v6, 0x60a

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0xe8

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xf9a

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x24d

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v11, 0xdf4

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0xbc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 206055
    iput-object v4, v3, LX/18E;->a:LX/0Or;

    iput-object v5, v3, LX/18E;->b:LX/0Or;

    iput-object v6, v3, LX/18E;->c:LX/0Or;

    iput-object v7, v3, LX/18E;->d:LX/0Or;

    iput-object v8, v3, LX/18E;->e:LX/0Or;

    iput-object v9, v3, LX/18E;->f:LX/0Or;

    iput-object v10, v3, LX/18E;->i:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v11, v3, LX/18E;->j:LX/0Ot;

    iput-object p0, v3, LX/18E;->k:LX/0Ot;

    .line 206056
    move-object v0, v3

    .line 206057
    sput-object v0, LX/18E;->m:LX/18E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206058
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206059
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206060
    :cond_1
    sget-object v0, LX/18E;->m:LX/18E;

    return-object v0

    .line 206061
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/18E;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 206063
    iget-object v0, p0, LX/18E;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 206064
    iget-object v1, p0, LX/18E;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AnM;

    .line 206065
    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 206066
    invoke-static {v0, v7, v1, v7}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 206067
    new-instance v2, LX/2HB;

    invoke-direct {v2, v0}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0218e4

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    const v3, 0x7f0824fb

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0118

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, p1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 206068
    iput-object v1, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 206069
    move-object v1, v2

    .line 206070
    invoke-virtual {v1, v8}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v1

    .line 206071
    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 206072
    const/16 v2, 0x130

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 206073
    return-void
.end method

.method private static a(LX/18E;ZZJJJIIII)V
    .locals 3

    .prologue
    .line 206074
    iget-object v0, p0, LX/18E;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "fb4a_offline_inventory_notification"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 206075
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206076
    const-string v1, "notificationShown"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 206077
    const-string v1, "wasConnected"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 206078
    const-string v1, "notificationTime"

    invoke-virtual {v0, v1, p3, p4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 206079
    const-string v1, "priorNotificationTime"

    invoke-virtual {v0, v1, p5, p6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 206080
    const-string v1, "minDelayTimeBetweenNotifs"

    invoke-virtual {v0, v1, p7, p8}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 206081
    const-string v1, "unseenCount"

    invoke-virtual {v0, v1, p9}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 206082
    const-string v1, "minUnseenStoryCount"

    invoke-virtual {v0, v1, p10}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 206083
    const-string v1, "lastCount"

    invoke-virtual {v0, v1, p11}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 206084
    const-string v1, "maxNotifs"

    invoke-virtual {v0, v1, p12}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 206085
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 206086
    :cond_0
    return-void
.end method

.method public static e(LX/18E;)V
    .locals 18

    .prologue
    .line 206087
    move-object/from16 v0, p0

    iget-object v2, v0, LX/18E;->l:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206088
    move-object/from16 v0, p0

    iget-object v2, v0, LX/18E;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0qj;

    .line 206089
    invoke-virtual {v2}, LX/0qj;->a()Z

    move-result v5

    .line 206090
    move-object/from16 v0, p0

    iget-object v2, v0, LX/18E;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 206091
    sget-object v3, LX/18E;->g:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v2, v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 206092
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 206093
    const-wide/32 v10, 0x36ee80

    move-object/from16 v0, p0

    iget-object v3, v0, LX/18E;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v12, LX/0X5;->jF:J

    invoke-interface {v3, v12, v13}, LX/0W4;->c(J)J

    move-result-wide v12

    mul-long/2addr v10, v12

    .line 206094
    move-object/from16 v0, p0

    iget-object v3, v0, LX/18E;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v12, LX/0X5;->jI:J

    const/4 v4, 0x5

    invoke-interface {v3, v12, v13, v4}, LX/0W4;->a(JI)I

    move-result v15

    .line 206095
    sget-object v3, LX/18E;->h:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v14

    .line 206096
    move-object/from16 v0, p0

    iget-object v3, v0, LX/18E;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0pn;

    .line 206097
    move-object/from16 v0, p0

    iget-object v4, v0, LX/18E;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Ym;

    .line 206098
    invoke-virtual {v4}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0pn;->h(Lcom/facebook/api/feedtype/FeedType;)LX/69J;

    move-result-object v3

    .line 206099
    invoke-virtual {v3}, LX/69J;->d()I

    move-result v12

    .line 206100
    move-object/from16 v0, p0

    iget-object v3, v0, LX/18E;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0W3;

    sget-wide v16, LX/0X5;->jG:J

    const/16 v4, 0xa

    move-wide/from16 v0, v16

    invoke-interface {v3, v0, v1, v4}, LX/0W4;->a(JI)I

    move-result v13

    .line 206101
    if-nez v5, :cond_1

    sub-long v16, v6, v8

    cmp-long v3, v16, v10

    if-ltz v3, :cond_1

    if-lt v12, v13, :cond_1

    if-ge v14, v15, :cond_1

    const/4 v4, 0x1

    .line 206102
    :goto_0
    if-eqz v4, :cond_0

    .line 206103
    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/18E;->g:LX/0Tn;

    invoke-interface {v2, v3, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    sget-object v3, LX/18E;->h:LX/0Tn;

    add-int/lit8 v16, v14, 0x1

    move/from16 v0, v16

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 206104
    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/18E;->a(LX/18E;I)V

    :cond_0
    move-object/from16 v3, p0

    .line 206105
    invoke-static/range {v3 .. v15}, LX/18E;->a(LX/18E;ZZJJJIIII)V

    .line 206106
    return-void

    .line 206107
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 206108
    iget-object v5, p0, LX/18E;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0W3;

    sget-wide v7, LX/0X5;->jE:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    move v0, v5

    .line 206109
    if-nez v0, :cond_1

    .line 206110
    :cond_0
    :goto_0
    return-void

    .line 206111
    :cond_1
    iget-object v0, p0, LX/18E;->l:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 206112
    iget-object v0, p0, LX/18E;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->jH:J

    invoke-interface {v0, v2, v3}, LX/0W4;->c(J)J

    move-result-wide v0

    .line 206113
    iget-object v2, p0, LX/18E;->i:Ljava/util/concurrent/ScheduledExecutorService;

    .line 206114
    new-instance v3, Lcom/facebook/feed/notifier/InventoryNotifier$1;

    invoke-direct {v3, p0}, Lcom/facebook/feed/notifier/InventoryNotifier$1;-><init>(LX/18E;)V

    move-object v3, v3

    .line 206115
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 206116
    iget-object v1, p0, LX/18E;->l:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 206117
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 206118
    iget-object v0, p0, LX/18E;->l:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    .line 206119
    if-eqz v0, :cond_0

    .line 206120
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 206121
    :cond_0
    return-void
.end method
