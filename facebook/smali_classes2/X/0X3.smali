.class public LX/0X3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SI;


# instance fields
.field public final a:LX/0WJ;

.field private final b:Landroid/content/Context;

.field private c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>(LX/0WJ;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 76998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76999
    new-instance v0, LX/0X4;

    invoke-direct {v0, p0}, LX/0X4;-><init>(LX/0X3;)V

    iput-object v0, p0, LX/0X3;->d:Ljava/lang/ThreadLocal;

    .line 77000
    iput-object p1, p0, LX/0X3;->a:LX/0WJ;

    .line 77001
    iput-object p2, p0, LX/0X3;->b:Landroid/content/Context;

    .line 77002
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 77003
    iget-object v0, p0, LX/0X3;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2

    .prologue
    .line 77004
    iget-object v0, p0, LX/0X3;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Application;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot override viewer context on the application context"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 77005
    iput-object p1, p0, LX/0X3;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 77006
    return-void

    .line 77007
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;
    .locals 1

    .prologue
    .line 77008
    if-nez p1, :cond_0

    .line 77009
    sget-object v0, LX/1mW;->a:LX/1mW;

    .line 77010
    :goto_0
    return-object v0

    .line 77011
    :cond_0
    iget-object v0, p0, LX/0X3;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77012
    new-instance v0, LX/3Ss;

    invoke-direct {v0, p0, p1}, LX/3Ss;-><init>(LX/0X3;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method

.method public final b()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 77013
    iget-object v0, p0, LX/0X3;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final c()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1

    .prologue
    .line 77014
    iget-object v0, p0, LX/0X3;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public final d()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2

    .prologue
    .line 77015
    iget-object v0, p0, LX/0X3;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 77016
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 77017
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 77018
    :goto_0
    move-object v0, v0

    .line 77019
    iget-object v1, p0, LX/0X3;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-nez v1, :cond_0

    .line 77020
    iput-object v0, p0, LX/0X3;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 77021
    :cond_0
    return-object v0

    .line 77022
    :cond_1
    iget-object v0, p0, LX/0X3;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_2

    .line 77023
    iget-object v0, p0, LX/0X3;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0

    .line 77024
    :cond_2
    iget-object v0, p0, LX/0X3;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2

    .prologue
    .line 77025
    invoke-virtual {p0}, LX/0X3;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 77026
    invoke-virtual {p0}, LX/0X3;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 77027
    const/4 v0, 0x0

    .line 77028
    :cond_0
    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 77029
    iget-object v0, p0, LX/0X3;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 77030
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77031
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 77032
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 77033
    return-void
.end method
