.class public LX/0qx;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I


# instance fields
.field public b:LX/0qw;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148696
    const/4 v0, 0x4

    sput v0, LX/0qx;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148698
    sget-object v0, LX/0qw;->FULL:LX/0qw;

    iput-object v0, p0, LX/0qx;->b:LX/0qw;

    .line 148699
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0qx;->c:Ljava/util/List;

    .line 148700
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 148701
    iget-object v0, p0, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 148702
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 148703
    iget-object v0, p0, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
