.class public LX/0lT;
.super LX/0lU;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129718
    invoke-direct {p0}, LX/0lU;-><init>()V

    return-void
.end method

.method private static A(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "LX/1Xt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129713
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129714
    if-eqz v0, :cond_0

    .line 129715
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->keyUsing()Ljava/lang/Class;

    move-result-object v0

    .line 129716
    const-class v1, LX/1Xs;

    if-eq v0, v1, :cond_0

    .line 129717
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static B(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 129708
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129709
    if-eqz v0, :cond_0

    .line 129710
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->contentUsing()Ljava/lang/Class;

    move-result-object v0

    .line 129711
    const-class v1, Lcom/fasterxml/jackson/databind/JsonDeserializer$None;

    if-eq v0, v1, :cond_0

    .line 129712
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static C(LX/0lO;)Z
    .locals 1

    .prologue
    .line 129706
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIgnore;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIgnore;

    .line 129707
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIgnore;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0m4;LX/0lO;LX/0lJ;)LX/4qy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lO;",
            "LX/0lJ;",
            ")",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 129683
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;

    .line 129684
    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JsonTypeResolver;

    invoke-virtual {p1, v1}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/fasterxml/jackson/databind/annotation/JsonTypeResolver;

    .line 129685
    if-eqz v1, :cond_4

    .line 129686
    if-nez v0, :cond_1

    .line 129687
    :cond_0
    :goto_0
    return-object v3

    .line 129688
    :cond_1
    invoke-interface {v1}, Lcom/fasterxml/jackson/databind/annotation/JsonTypeResolver;->value()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/0m4;->a(LX/0lO;Ljava/lang/Class;)LX/4qy;

    move-result-object v1

    move-object v2, v1

    .line 129689
    :goto_1
    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JsonTypeIdResolver;

    invoke-virtual {p1, v1}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/fasterxml/jackson/databind/annotation/JsonTypeIdResolver;

    .line 129690
    if-nez v1, :cond_6

    move-object v1, v3

    .line 129691
    :goto_2
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->use()LX/4pP;

    move-result-object v3

    invoke-interface {v2, v3, v1}, LX/4qy;->a(LX/4pP;LX/4qx;)LX/4qy;

    move-result-object v2

    .line 129692
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->include()LX/4pO;

    move-result-object v1

    .line 129693
    sget-object v3, LX/4pO;->EXTERNAL_PROPERTY:LX/4pO;

    if-ne v1, v3, :cond_2

    instance-of v3, p1, LX/0lN;

    if-eqz v3, :cond_2

    .line 129694
    sget-object v1, LX/4pO;->PROPERTY:LX/4pO;

    .line 129695
    :cond_2
    invoke-interface {v2, v1}, LX/4qy;->a(LX/4pO;)LX/4qy;

    move-result-object v1

    .line 129696
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->property()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/4qy;->a(Ljava/lang/String;)LX/4qy;

    move-result-object v1

    .line 129697
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->defaultImpl()Ljava/lang/Class;

    move-result-object v2

    .line 129698
    const-class v3, LX/4pQ;

    if-eq v2, v3, :cond_3

    .line 129699
    invoke-interface {v1, v2}, LX/4qy;->a(Ljava/lang/Class;)LX/4qy;

    move-result-object v1

    .line 129700
    :cond_3
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->a()Z

    move-result v0

    invoke-interface {v1, v0}, LX/4qy;->a(Z)LX/4qy;

    move-result-object v3

    goto :goto_0

    .line 129701
    :cond_4
    if-eqz v0, :cond_0

    .line 129702
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeInfo;->use()LX/4pP;

    move-result-object v1

    sget-object v2, LX/4pP;->NONE:LX/4pP;

    if-ne v1, v2, :cond_5

    .line 129703
    invoke-static {}, LX/4rE;->b()LX/4rE;

    move-result-object v3

    goto :goto_0

    .line 129704
    :cond_5
    invoke-static {}, LX/0lT;->c()LX/4rE;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 129705
    :cond_6
    invoke-interface {v1}, Lcom/fasterxml/jackson/databind/annotation/JsonTypeIdResolver;->value()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/0m4;->b(LX/0lO;Ljava/lang/Class;)LX/4qx;

    move-result-object v1

    goto :goto_2
.end method

.method private static c()LX/4rE;
    .locals 1

    .prologue
    .line 129682
    new-instance v0, LX/4rE;

    invoke-direct {v0}, LX/4rE;-><init>()V

    return-object v0
.end method

.method private static x(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 129677
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129678
    if-eqz v0, :cond_0

    .line 129679
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->keyUsing()Ljava/lang/Class;

    move-result-object v0

    .line 129680
    const-class v1, Lcom/fasterxml/jackson/databind/JsonSerializer$None;

    if-eq v0, v1, :cond_0

    .line 129681
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static y(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 129620
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129621
    if-eqz v0, :cond_0

    .line 129622
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->contentUsing()Ljava/lang/Class;

    move-result-object v0

    .line 129623
    const-class v1, Lcom/fasterxml/jackson/databind/JsonSerializer$None;

    if-eq v0, v1, :cond_0

    .line 129624
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static z(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 129660
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p0, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129661
    if-eqz v0, :cond_0

    .line 129662
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->using()Ljava/lang/Class;

    move-result-object v0

    .line 129663
    const-class v1, Lcom/fasterxml/jackson/databind/JsonDeserializer$None;

    if-eq v0, v1, :cond_0

    .line 129664
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lN;LX/0lW;)LX/0lW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lN;",
            "LX/0lW",
            "<*>;)",
            "LX/0lW",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129658
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonAutoDetect;

    .line 129659
    if-nez v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    invoke-interface {p2, v0}, LX/0lW;->a(Lcom/fasterxml/jackson/annotation/JsonAutoDetect;)LX/0lW;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(LX/0lO;LX/0nr;)LX/0nr;
    .locals 2

    .prologue
    .line 129646
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonInclude;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonInclude;

    .line 129647
    if-eqz v0, :cond_1

    .line 129648
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonInclude;->value()LX/0nr;

    move-result-object p2

    .line 129649
    :cond_0
    :goto_0
    return-object p2

    .line 129650
    :cond_1
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129651
    if-eqz v0, :cond_0

    .line 129652
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->include()LX/1Xu;

    move-result-object v0

    .line 129653
    sget-object v1, LX/4qq;->a:[I

    invoke-virtual {v0}, LX/1Xu;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 129654
    :pswitch_0
    sget-object p2, LX/0nr;->ALWAYS:LX/0nr;

    goto :goto_0

    .line 129655
    :pswitch_1
    sget-object p2, LX/0nr;->NON_NULL:LX/0nr;

    goto :goto_0

    .line 129656
    :pswitch_2
    sget-object p2, LX/0nr;->NON_DEFAULT:LX/0nr;

    goto :goto_0

    .line 129657
    :pswitch_3
    sget-object p2, LX/0nr;->NON_EMPTY:LX/0nr;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/0lN;)LX/2Vb;
    .locals 2

    .prologue
    .line 129642
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonRootName;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonRootName;

    .line 129643
    if-nez v0, :cond_0

    .line 129644
    const/4 v0, 0x0

    .line 129645
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/2Vb;

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonRootName;->value()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2Vb;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/2An;)LX/4pn;
    .locals 1

    .prologue
    .line 129634
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonManagedReference;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonManagedReference;

    .line 129635
    if-eqz v0, :cond_0

    .line 129636
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonManagedReference;->value()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4pn;->a(Ljava/lang/String;)LX/4pn;

    move-result-object v0

    .line 129637
    :goto_0
    return-object v0

    .line 129638
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonBackReference;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonBackReference;

    .line 129639
    if-eqz v0, :cond_1

    .line 129640
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonBackReference;->value()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4pn;->b(Ljava/lang/String;)LX/4pn;

    move-result-object v0

    goto :goto_0

    .line 129641
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0lO;)LX/4qt;
    .locals 4

    .prologue
    .line 129630
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;

    .line 129631
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;->generator()Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/4pU;

    if-ne v1, v2, :cond_1

    .line 129632
    :cond_0
    const/4 v0, 0x0

    .line 129633
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/4qt;

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;->property()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;->scope()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIdentityInfo;->generator()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/4qt;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/0lO;LX/4qt;)LX/4qt;
    .locals 1

    .prologue
    .line 129626
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIdentityReference;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIdentityReference;

    .line 129627
    if-eqz v0, :cond_0

    .line 129628
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIdentityReference;->a()Z

    move-result v0

    invoke-virtual {p2, v0}, LX/4qt;->a(Z)LX/4qt;

    move-result-object p2

    .line 129629
    :cond_0
    return-object p2
.end method

.method public final a(LX/0m4;LX/0lN;LX/0lJ;)LX/4qy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lN;",
            "LX/0lJ;",
            ")",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129625
    invoke-static {p1, p2, p3}, LX/0lT;->a(LX/0m4;LX/0lO;LX/0lJ;)LX/4qy;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0m4;LX/2An;LX/0lJ;)LX/4qy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/2An;",
            "LX/0lJ;",
            ")",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129727
    invoke-virtual {p3}, LX/0lJ;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 129728
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, p3}, LX/0lT;->a(LX/0m4;LX/0lO;LX/0lJ;)LX/4qy;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/2Am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129720
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129721
    if-eqz v0, :cond_0

    .line 129722
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->value()Ljava/lang/String;

    move-result-object v0

    .line 129723
    :goto_0
    return-object v0

    .line 129724
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonView;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129725
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 129726
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2At;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129777
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonGetter;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonGetter;

    .line 129778
    if-eqz v0, :cond_0

    .line 129779
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonGetter;->value()Ljava/lang/String;

    move-result-object v0

    .line 129780
    :goto_0
    return-object v0

    .line 129781
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129782
    if-eqz v0, :cond_1

    .line 129783
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129784
    :cond_1
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonView;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 129785
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 129786
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/2Vd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129772
    if-eqz p1, :cond_0

    .line 129773
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129774
    if-eqz v0, :cond_0

    .line 129775
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->value()Ljava/lang/String;

    move-result-object v0

    .line 129776
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/annotation/Annotation;)Z
    .locals 2

    .prologue
    .line 129771
    invoke-interface {p1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/annotation/JacksonAnnotationsInside;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0m4;LX/2An;LX/0lJ;)LX/4qy;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/2An;",
            "LX/0lJ;",
            ")",
            "LX/4qy",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129768
    invoke-virtual {p3}, LX/0lJ;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129769
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Must call method with a container type (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129770
    :cond_0
    invoke-static {p1, p2, p3}, LX/0lT;->a(LX/0m4;LX/0lO;LX/0lJ;)LX/4qy;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/2An;)LX/4ro;
    .locals 2

    .prologue
    .line 129761
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonUnwrapped;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonUnwrapped;

    .line 129762
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonUnwrapped;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 129763
    :cond_0
    const/4 v0, 0x0

    .line 129764
    :goto_0
    return-object v0

    .line 129765
    :cond_1
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonUnwrapped;->prefix()Ljava/lang/String;

    move-result-object v1

    .line 129766
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonUnwrapped;->suffix()Ljava/lang/String;

    move-result-object v0

    .line 129767
    invoke-static {v1, v0}, LX/4ro;->a(Ljava/lang/String;Ljava/lang/String;)LX/4ro;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/0lN;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129759
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;

    .line 129760
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/2Am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129787
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129788
    if-eqz v0, :cond_0

    .line 129789
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->value()Ljava/lang/String;

    move-result-object v0

    .line 129790
    :goto_0
    return-object v0

    .line 129791
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonView;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonBackReference;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonManagedReference;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129792
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 129793
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2At;)Z
    .locals 1

    .prologue
    .line 129757
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonValue;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonValue;

    .line 129758
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonValue;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0lO;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 129755
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;

    .line 129756
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;->a()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/0lN;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129753
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreType;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonIgnoreType;

    .line 129754
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonIgnoreType;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/2At;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129743
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonSetter;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonSetter;

    .line 129744
    if-eqz v0, :cond_0

    .line 129745
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonSetter;->value()Ljava/lang/String;

    move-result-object v0

    .line 129746
    :goto_0
    return-object v0

    .line 129747
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129748
    if-eqz v0, :cond_1

    .line 129749
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129750
    :cond_1
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonView;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonBackReference;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    const-class v0, Lcom/fasterxml/jackson/annotation/JsonManagedReference;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 129751
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 129752
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/0lO;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/4qu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129735
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonSubTypes;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonSubTypes;

    .line 129736
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 129737
    :cond_0
    return-object v0

    .line 129738
    :cond_1
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonSubTypes;->a()[Lcom/fasterxml/jackson/annotation/JsonSubTypes$Type;

    move-result-object v2

    .line 129739
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 129740
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 129741
    new-instance v5, LX/4qu;

    invoke-interface {v4}, Lcom/fasterxml/jackson/annotation/JsonSubTypes$Type;->value()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v4}, Lcom/fasterxml/jackson/annotation/JsonSubTypes$Type;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v6, v4}, LX/4qu;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129742
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final c(LX/2An;)Z
    .locals 1

    .prologue
    .line 129734
    invoke-static {p1}, LX/0lT;->C(LX/0lO;)Z

    move-result v0

    return v0
.end method

.method public final d(LX/0lN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129729
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonFilter;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonFilter;

    .line 129730
    if-eqz v0, :cond_0

    .line 129731
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonFilter;->value()Ljava/lang/String;

    move-result-object v0

    .line 129732
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 129733
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(LX/2An;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129665
    const-class v0, Lcom/fasterxml/jackson/annotation/JacksonInject;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JacksonInject;

    .line 129666
    if-nez v0, :cond_1

    .line 129667
    const/4 v0, 0x0

    .line 129668
    :cond_0
    :goto_0
    return-object v0

    .line 129669
    :cond_1
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JacksonInject;->value()Ljava/lang/String;

    move-result-object v0

    .line 129670
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 129671
    instance-of v0, p1, LX/2At;

    if-nez v0, :cond_2

    .line 129672
    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 129673
    check-cast v0, LX/2At;

    .line 129674
    invoke-virtual {v0}, LX/2At;->l()I

    move-result v1

    if-nez v1, :cond_3

    .line 129675
    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129676
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2At;->a(I)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(LX/2At;)Z
    .locals 1

    .prologue
    .line 129719
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonAnySetter;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public final d(LX/0lO;)[Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129504
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonView;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonView;

    .line 129505
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonView;->a()[Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(LX/0lO;)LX/4pN;
    .locals 2

    .prologue
    .line 129548
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonFormat;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonFormat;

    .line 129549
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/4pN;

    invoke-direct {v1, v0}, LX/4pN;-><init>(Lcom/fasterxml/jackson/annotation/JsonFormat;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final e(LX/2An;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129544
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonProperty;

    .line 129545
    if-eqz v0, :cond_0

    .line 129546
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonProperty;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 129547
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(LX/0lN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129542
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonNaming;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonNaming;

    .line 129543
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonNaming;->value()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(LX/2At;)Z
    .locals 1

    .prologue
    .line 129541
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonAnyGetter;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public final f(LX/2An;)LX/4pN;
    .locals 1

    .prologue
    .line 129540
    invoke-virtual {p0, p1}, LX/0lU;->f(LX/2An;)LX/4pN;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/0lO;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129530
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129531
    if-eqz v0, :cond_0

    .line 129532
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->using()Ljava/lang/Class;

    move-result-object v0

    .line 129533
    const-class v1, Lcom/fasterxml/jackson/databind/JsonSerializer$None;

    if-eq v0, v1, :cond_0

    .line 129534
    :goto_0
    return-object v0

    .line 129535
    :cond_0
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonRawValue;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonRawValue;

    .line 129536
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonRawValue;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129537
    invoke-virtual {p1}, LX/0lO;->d()Ljava/lang/Class;

    move-result-object v1

    .line 129538
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/RawSerializer;

    invoke-direct {v0, v1}, Lcom/fasterxml/jackson/databind/ser/std/RawSerializer;-><init>(Ljava/lang/Class;)V

    goto :goto_0

    .line 129539
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(LX/0lN;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129528
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonTypeName;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonTypeName;

    .line 129529
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonTypeName;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(LX/2An;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129527
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonTypeId;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic g(LX/0lO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129526
    invoke-static {p1}, LX/0lT;->x(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final g(LX/0lN;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 129524
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;

    .line 129525
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;->a()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h(LX/0lN;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 129522
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;

    .line 129523
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/annotation/JsonPropertyOrder;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic h(LX/0lO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129521
    invoke-static {p1}, LX/0lT;->y(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final h(LX/2An;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129516
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129517
    if-eqz v0, :cond_0

    .line 129518
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->contentConverter()Ljava/lang/Class;

    move-result-object v0

    .line 129519
    const-class v1, LX/1Xq;

    if-eq v0, v1, :cond_0

    .line 129520
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129511
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129512
    if-eqz v0, :cond_0

    .line 129513
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->as()Ljava/lang/Class;

    move-result-object v0

    .line 129514
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129515
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(LX/0lN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129551
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonValueInstantiator;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonValueInstantiator;

    .line 129552
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonValueInstantiator;->value()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final i(LX/2An;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129506
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129507
    if-eqz v0, :cond_0

    .line 129508
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->contentConverter()Ljava/lang/Class;

    move-result-object v0

    .line 129509
    const-class v1, LX/1Xq;

    if-eq v0, v1, :cond_0

    .line 129510
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(LX/0lN;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lN;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129587
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129588
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->builder()Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/1Xp;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->builder()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public final j(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129615
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129616
    if-eqz v0, :cond_0

    .line 129617
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->keyAs()Ljava/lang/Class;

    move-result-object v0

    .line 129618
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129619
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k(LX/0lN;)LX/2zN;
    .locals 2

    .prologue
    .line 129613
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonPOJOBuilder;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonPOJOBuilder;

    .line 129614
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/2zN;

    invoke-direct {v1, v0}, LX/2zN;-><init>(Lcom/fasterxml/jackson/databind/annotation/JsonPOJOBuilder;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final k(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129608
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129609
    if-eqz v0, :cond_0

    .line 129610
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->contentAs()Ljava/lang/Class;

    move-result-object v0

    .line 129611
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129612
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(LX/0lO;)LX/1Xv;
    .locals 1

    .prologue
    .line 129606
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129607
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->typing()LX/1Xv;

    move-result-object v0

    goto :goto_0
.end method

.method public final m(LX/0lO;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129601
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 129602
    if-eqz v0, :cond_0

    .line 129603
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;->converter()Ljava/lang/Class;

    move-result-object v0

    .line 129604
    const-class v1, LX/1Xq;

    if-eq v0, v1, :cond_0

    .line 129605
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n(LX/0lO;)LX/2Vb;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 129591
    instance-of v1, p1, LX/2Am;

    if-eqz v1, :cond_1

    .line 129592
    check-cast p1, LX/2Am;

    invoke-virtual {p0, p1}, LX/0lU;->a(LX/2Am;)Ljava/lang/String;

    move-result-object v1

    .line 129593
    :goto_0
    if-eqz v1, :cond_0

    .line 129594
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 129595
    sget-object v0, LX/2Vb;->a:LX/2Vb;

    .line 129596
    :cond_0
    :goto_1
    return-object v0

    .line 129597
    :cond_1
    instance-of v1, p1, LX/2At;

    if-eqz v1, :cond_2

    .line 129598
    check-cast p1, LX/2At;

    invoke-virtual {p0, p1}, LX/0lU;->a(LX/2At;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 129599
    goto :goto_0

    .line 129600
    :cond_3
    new-instance v0, LX/2Vb;

    invoke-direct {v0, v1}, LX/2Vb;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final synthetic o(LX/0lO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129590
    invoke-static {p1}, LX/0lT;->z(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic p(LX/0lO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129589
    invoke-static {p1}, LX/0lT;->A(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q(LX/0lO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129550
    invoke-static {p1}, LX/0lT;->B(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public final r(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129582
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129583
    if-eqz v0, :cond_0

    .line 129584
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->as()Ljava/lang/Class;

    move-result-object v0

    .line 129585
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129586
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129577
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129578
    if-eqz v0, :cond_0

    .line 129579
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->keyAs()Ljava/lang/Class;

    move-result-object v0

    .line 129580
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129581
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t(LX/0lO;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129572
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129573
    if-eqz v0, :cond_0

    .line 129574
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->contentAs()Ljava/lang/Class;

    move-result-object v0

    .line 129575
    const-class v1, LX/1Xp;

    if-eq v0, v1, :cond_0

    .line 129576
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u(LX/0lO;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 129567
    const-class v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    invoke-virtual {p1, v0}, LX/0lO;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;

    .line 129568
    if-eqz v0, :cond_0

    .line 129569
    invoke-interface {v0}, Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;->converter()Ljava/lang/Class;

    move-result-object v0

    .line 129570
    const-class v1, LX/1Xq;

    if-eq v0, v1, :cond_0

    .line 129571
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v(LX/0lO;)LX/2Vb;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 129555
    instance-of v1, p1, LX/2Am;

    if-eqz v1, :cond_1

    .line 129556
    check-cast p1, LX/2Am;

    invoke-virtual {p0, p1}, LX/0lU;->b(LX/2Am;)Ljava/lang/String;

    move-result-object v1

    .line 129557
    :goto_0
    if-eqz v1, :cond_0

    .line 129558
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 129559
    sget-object v0, LX/2Vb;->a:LX/2Vb;

    .line 129560
    :cond_0
    :goto_1
    return-object v0

    .line 129561
    :cond_1
    instance-of v1, p1, LX/2At;

    if-eqz v1, :cond_2

    .line 129562
    check-cast p1, LX/2At;

    invoke-virtual {p0, p1}, LX/0lU;->c(LX/2At;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 129563
    :cond_2
    instance-of v1, p1, LX/2Vd;

    if-eqz v1, :cond_3

    .line 129564
    check-cast p1, LX/2Vd;

    invoke-virtual {p0, p1}, LX/0lU;->a(LX/2Vd;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 129565
    goto :goto_0

    .line 129566
    :cond_4
    new-instance v0, LX/2Vb;

    invoke-direct {v0, v1}, LX/2Vb;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 129554
    sget-object v0, LX/4pz;->VERSION:LX/0ne;

    return-object v0
.end method

.method public final w(LX/0lO;)Z
    .locals 1

    .prologue
    .line 129553
    const-class v0, Lcom/fasterxml/jackson/annotation/JsonCreator;

    invoke-virtual {p1, v0}, LX/0lO;->b(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
