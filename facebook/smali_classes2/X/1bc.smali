.class public final enum LX/1bc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1bc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1bc;

.field public static final enum HIGH:LX/1bc;

.field public static final enum LOW:LX/1bc;

.field public static final enum MEDIUM:LX/1bc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280730
    new-instance v0, LX/1bc;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, LX/1bc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bc;->LOW:LX/1bc;

    .line 280731
    new-instance v0, LX/1bc;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/1bc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bc;->MEDIUM:LX/1bc;

    .line 280732
    new-instance v0, LX/1bc;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, LX/1bc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bc;->HIGH:LX/1bc;

    .line 280733
    const/4 v0, 0x3

    new-array v0, v0, [LX/1bc;

    sget-object v1, LX/1bc;->LOW:LX/1bc;

    aput-object v1, v0, v2

    sget-object v1, LX/1bc;->MEDIUM:LX/1bc;

    aput-object v1, v0, v3

    sget-object v1, LX/1bc;->HIGH:LX/1bc;

    aput-object v1, v0, v4

    sput-object v0, LX/1bc;->$VALUES:[LX/1bc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 280734
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getHigherPriority(LX/1bc;LX/1bc;)LX/1bc;
    .locals 2
    .param p0    # LX/1bc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/1bc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280735
    if-nez p0, :cond_1

    .line 280736
    :cond_0
    :goto_0
    return-object p1

    .line 280737
    :cond_1
    if-nez p1, :cond_2

    move-object p1, p0

    .line 280738
    goto :goto_0

    .line 280739
    :cond_2
    invoke-virtual {p0}, LX/1bc;->ordinal()I

    move-result v0

    invoke-virtual {p1}, LX/1bc;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    move-object p1, p0

    .line 280740
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1bc;
    .locals 1

    .prologue
    .line 280741
    const-class v0, LX/1bc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bc;

    return-object v0
.end method

.method public static values()[LX/1bc;
    .locals 1

    .prologue
    .line 280742
    sget-object v0, LX/1bc;->$VALUES:[LX/1bc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bc;

    return-object v0
.end method
