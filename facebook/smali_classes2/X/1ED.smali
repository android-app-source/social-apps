.class public LX/1ED;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 219525
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 219526
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 219527
    invoke-virtual {p0, p1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v0, v0

    .line 219528
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 219529
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 219530
    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 219531
    invoke-virtual {p0, p1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    move v0, v0

    .line 219532
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method
