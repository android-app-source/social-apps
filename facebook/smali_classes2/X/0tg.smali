.class public LX/0tg;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLBatchRunner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 155409
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 155410
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/locks/ReadWriteLock;LX/0v6;)Lcom/facebook/graphql/executor/GraphQLBatchRunner;
    .locals 14

    .prologue
    .line 155411
    new-instance v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    const/16 v1, 0xb0f

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v4

    check-cast v4, LX/0tc;

    invoke-static {p0}, LX/11E;->b(LX/0QB;)LX/11E;

    move-result-object v5

    check-cast v5, LX/11E;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v6

    check-cast v6, LX/11H;

    invoke-static {p0}, LX/0sf;->a(LX/0QB;)LX/0sf;

    move-result-object v7

    check-cast v7, LX/0sf;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v9

    check-cast v9, LX/0t2;

    invoke-static {p0}, LX/0tj;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v10

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v11

    check-cast v11, LX/0tA;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v12

    check-cast v12, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/1kv;->b(LX/0QB;)LX/1kv;

    move-result-object v13

    check-cast v13, LX/1kv;

    move-object v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v13}, Lcom/facebook/graphql/executor/GraphQLBatchRunner;-><init>(Ljava/util/concurrent/locks/ReadWriteLock;LX/0v6;LX/0Ot;LX/0tc;LX/11E;LX/11H;LX/0sf;LX/03V;LX/0t2;Ljava/util/Set;LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1kv;)V

    .line 155412
    return-object v0
.end method
