.class public final LX/0bq;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/0yc;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0bq;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87326
    const/16 v0, 0x326

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;I)V

    .line 87327
    iput-object p2, p0, LX/0bq;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 87328
    return-void
.end method

.method public static a(LX/0QB;)LX/0bq;
    .locals 5

    .prologue
    .line 87329
    sget-object v0, LX/0bq;->c:LX/0bq;

    if-nez v0, :cond_1

    .line 87330
    const-class v1, LX/0bq;

    monitor-enter v1

    .line 87331
    :try_start_0
    sget-object v0, LX/0bq;->c:LX/0bq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87332
    if-eqz v2, :cond_0

    .line 87333
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87334
    new-instance v4, LX/0bq;

    const/16 v3, 0x4e0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v4, p0, v3}, LX/0bq;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 87335
    move-object v0, v4

    .line 87336
    sput-object v0, LX/0bq;->c:LX/0bq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87337
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87338
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87339
    :cond_1
    sget-object v0, LX/0bq;->c:LX/0bq;

    return-object v0

    .line 87340
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 87342
    check-cast p3, LX/0yc;

    .line 87343
    iget-object v0, p0, LX/0bq;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->w:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 87344
    sget-object v0, LX/32P;->GATEKEEPER_CHANGED:LX/32P;

    invoke-virtual {p3, v0}, LX/0yc;->a(LX/32P;)V

    .line 87345
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87346
    const-string v0, "dialtone_gatekeeper_turned_off"

    invoke-virtual {p3, v0}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 87347
    :cond_0
    return-void
.end method
