.class public LX/0x8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/ViewStub;

.field public b:Lcom/facebook/datasensitivity/DataSaverBar;

.field public c:Landroid/app/Activity;

.field public d:LX/13Z;

.field private final e:LX/3GW;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 161962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161963
    new-instance v0, LX/3GV;

    invoke-direct {v0, p0}, LX/3GV;-><init>(LX/0x8;)V

    iput-object v0, p0, LX/0x8;->e:LX/3GW;

    .line 161964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161965
    iput-object v0, p0, LX/0x8;->f:LX/0Ot;

    .line 161966
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 161967
    iput-object v0, p0, LX/0x8;->g:LX/0Ot;

    .line 161968
    sget-object v0, LX/13Z;->DSM_INDICATOR_DISABLED:LX/13Z;

    iput-object v0, p0, LX/0x8;->d:LX/13Z;

    .line 161969
    return-void
.end method

.method private static a(LX/0x8;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0x8;",
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161961
    iput-object p1, p0, LX/0x8;->f:LX/0Ot;

    iput-object p2, p0, LX/0x8;->g:LX/0Ot;

    return-void
.end method

.method public static b(LX/0QB;)LX/0x8;
    .locals 3

    .prologue
    .line 161958
    new-instance v0, LX/0x8;

    invoke-direct {v0}, LX/0x8;-><init>()V

    .line 161959
    const/16 v1, 0x497

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2eb

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0x8;->a(LX/0x8;LX/0Ot;LX/0Ot;)V

    .line 161960
    return-object v0
.end method

.method public static c(LX/0x8;)V
    .locals 2

    .prologue
    .line 161949
    iget-object v0, p0, LX/0x8;->c:Landroid/app/Activity;

    .line 161950
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-nez v0, :cond_2

    .line 161951
    :cond_0
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    if-eqz v0, :cond_1

    .line 161952
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/datasensitivity/DataSaverBar;->setVisibility(I)V

    .line 161953
    :cond_1
    :goto_0
    return-void

    .line 161954
    :cond_2
    invoke-direct {p0}, LX/0x8;->e()V

    .line 161955
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v1, p0, LX/0x8;->d:LX/13Z;

    invoke-virtual {v0, v1}, Lcom/facebook/datasensitivity/DataSaverBar;->setIndicatorState(LX/13Z;)V

    .line 161956
    iget-object v0, p0, LX/0x8;->d:LX/13Z;

    sget-object v1, LX/13Z;->DSM_OFF:LX/13Z;

    if-ne v0, v1, :cond_1

    .line 161957
    invoke-direct {p0}, LX/0x8;->d()V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 161944
    iget-object v0, p0, LX/0x8;->c:Landroid/app/Activity;

    .line 161945
    if-eqz v0, :cond_0

    .line 161946
    const v1, 0x7f010297

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    .line 161947
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v1}, LX/470;->a(Landroid/view/Window;I)V

    .line 161948
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 161970
    iget-object v0, p0, LX/0x8;->a:Landroid/view/ViewStub;

    .line 161971
    if-eqz v0, :cond_0

    .line 161972
    const/4 v1, 0x0

    iput-object v1, p0, LX/0x8;->a:Landroid/view/ViewStub;

    .line 161973
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/datasensitivity/DataSaverBar;

    iput-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    .line 161974
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v1, p0, LX/0x8;->e:LX/3GW;

    .line 161975
    iput-object v1, v0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    .line 161976
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)I
    .locals 2

    .prologue
    .line 161939
    iget-object v0, p0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161940
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0}, Lcom/facebook/datasensitivity/DataSaverBar;->getHeight()I

    move-result v0

    .line 161941
    :goto_0
    return v0

    .line 161942
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0

    .line 161943
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 161936
    iget-object v0, p0, LX/0x8;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161937
    :goto_0
    return-void

    .line 161938
    :cond_0
    invoke-static {p0}, LX/0x8;->c(LX/0x8;)V

    goto :goto_0
.end method

.method public final a(LX/13Z;)V
    .locals 0

    .prologue
    .line 161924
    iput-object p1, p0, LX/0x8;->d:LX/13Z;

    .line 161925
    invoke-static {p0}, LX/0x8;->c(LX/0x8;)V

    .line 161926
    return-void
.end method

.method public final declared-synchronized a(Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 161933
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/0x8;->a:Landroid/view/ViewStub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161934
    monitor-exit p0

    return-void

    .line 161935
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161927
    iput-object v1, p0, LX/0x8;->c:Landroid/app/Activity;

    .line 161928
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    if-eqz v0, :cond_0

    .line 161929
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0, v1}, Lcom/facebook/datasensitivity/DataSaverBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161930
    iget-object v0, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0}, Lcom/facebook/datasensitivity/DataSaverBar;->a()V

    .line 161931
    iput-object v1, p0, LX/0x8;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    .line 161932
    :cond_0
    return-void
.end method
