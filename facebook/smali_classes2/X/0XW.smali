.class public LX/0XW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/performancelogger/PerformanceLogger;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0XW;


# instance fields
.field private final b:LX/0Y7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Y7;)V
    .locals 0
    .param p2    # LX/0Y7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 78916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78917
    iput-object p1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78918
    iput-object p2, p0, LX/0XW;->b:LX/0Y7;

    .line 78919
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78920
    if-nez p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0XW;
    .locals 5

    .prologue
    .line 78921
    sget-object v0, LX/0XW;->d:LX/0XW;

    if-nez v0, :cond_1

    .line 78922
    const-class v1, LX/0XW;

    monitor-enter v1

    .line 78923
    :try_start_0
    sget-object v0, LX/0XW;->d:LX/0XW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 78924
    if-eqz v2, :cond_0

    .line 78925
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 78926
    new-instance p0, LX/0XW;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Y6;->a(LX/0QB;)LX/0Y7;

    move-result-object v4

    check-cast v4, LX/0Y7;

    invoke-direct {p0, v3, v4}, LX/0XW;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Y7;)V

    .line 78927
    move-object v0, p0

    .line 78928
    sput-object v0, LX/0XW;->d:LX/0XW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78929
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 78930
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 78931
    :cond_1
    sget-object v0, LX/0XW;->d:LX/0XW;

    return-object v0

    .line 78932
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 78933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 78934
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 78935
    return-void
.end method

.method public final a(ILjava/lang/String;D)V
    .locals 3

    .prologue
    .line 78936
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    double-to-int v2, p3

    invoke-interface {v0, p1, v1, p2, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;I)V

    .line 78937
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78938
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78939
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, p1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 78940
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78941
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 78942
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x2

    move v1, p1

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 78943
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 79010
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 79011
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "tag_name"

    invoke-interface {v1, p1, v0, v2, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 79012
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "tag_value"

    invoke-interface {v1, p1, v0, v2, p5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 79013
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, p1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 79014
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 78944
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 78945
    if-nez p5, :cond_0

    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->h(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 78946
    :cond_0
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "tag_name"

    invoke-interface {v1, p1, v0, v2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 78947
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "tag_value"

    invoke-interface {v1, p1, v0, v2, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 78948
    :cond_1
    return-void
.end method

.method public final a(LX/0Yj;)V
    .locals 4

    .prologue
    .line 78949
    iget v0, p1, LX/0Yj;->a:I

    move v0, v0

    .line 78950
    iget-object v1, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v1, v1

    .line 78951
    iget-object v2, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v2, v2

    .line 78952
    invoke-static {v1, v2}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 78953
    iget-object p1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {p1, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 78954
    return-void
.end method

.method public final a(LX/0Yj;D)V
    .locals 6

    .prologue
    .line 78955
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78956
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78957
    const/4 v2, 0x2

    .line 78958
    iget-object v3, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v3, v3

    .line 78959
    double-to-int v4, p2

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;I)V

    .line 78960
    return-void
.end method

.method public final a(LX/0Yj;Z)V
    .locals 12

    .prologue
    .line 78961
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, v0

    .line 78962
    iget-object v1, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 78963
    invoke-static {v0, v1}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 78964
    if-nez p2, :cond_0

    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78965
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78966
    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 78967
    :cond_0
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78968
    iget v0, p1, LX/0Yj;->a:I

    move v2, v0

    .line 78969
    iget v0, p1, LX/0Yj;->b:I

    move v3, v0

    .line 78970
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v5, v0

    .line 78971
    invoke-virtual {p1}, LX/0Yj;->f()J

    move-result-wide v6

    .line 78972
    iget-boolean v0, p1, LX/0Yj;->j:Z

    move v8, v0

    .line 78973
    iget-boolean v0, p1, LX/0Yj;->i:Z

    move v9, v0

    .line 78974
    iget-object v0, p1, LX/0Yj;->s:LX/00q;

    move-object v10, v0

    .line 78975
    iget-object v0, p1, LX/0Yj;->u:LX/03R;

    move-object v11, v0

    .line 78976
    invoke-interface/range {v1 .. v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIILjava/lang/String;JZZLX/00q;LX/03R;)V

    .line 78977
    :cond_1
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78978
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78979
    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 78980
    iget-object v0, p1, LX/0Yj;->k:Ljava/util/Set;

    move-object v0, v0

    .line 78981
    if-eqz v0, :cond_2

    .line 78982
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78983
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78984
    iget-object v2, p1, LX/0Yj;->k:Ljava/util/Set;

    move-object v2, v2

    .line 78985
    invoke-interface {v0, v1, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/util/Collection;)V

    .line 78986
    :cond_2
    iget-object v0, p1, LX/0Yj;->l:Ljava/util/Map;

    move-object v0, v0

    .line 78987
    if-eqz v0, :cond_3

    .line 78988
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78989
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78990
    iget-object v2, p1, LX/0Yj;->l:Ljava/util/Map;

    move-object v2, v2

    .line 78991
    invoke-interface {v0, v1, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/util/Map;)V

    .line 78992
    :cond_3
    iget-object v0, p1, LX/0Yj;->o:Ljava/lang/String;

    move-object v0, v0

    .line 78993
    if-eqz v0, :cond_4

    .line 78994
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78995
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78996
    const-string v2, "tag_name"

    .line 78997
    iget-object v3, p1, LX/0Yj;->o:Ljava/lang/String;

    move-object v3, v3

    .line 78998
    invoke-interface {v0, v1, v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 78999
    :cond_4
    iget-object v0, p1, LX/0Yj;->p:Ljava/lang/String;

    move-object v0, v0

    .line 79000
    if-eqz v0, :cond_5

    .line 79001
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 79002
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 79003
    const-string v2, "tag_value"

    .line 79004
    iget-object v3, p1, LX/0Yj;->p:Ljava/lang/String;

    move-object v3, v3

    .line 79005
    invoke-interface {v0, v1, v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 79006
    :cond_5
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79007
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(Ljava/lang/Object;)V

    .line 79008
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 79009
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b()Z

    move-result v0

    return v0
.end method

.method public final b(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 78911
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 78912
    return-void
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78913
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78914
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;)V

    .line 78915
    return-void
.end method

.method public final b(LX/0Yj;)V
    .locals 9

    .prologue
    .line 78833
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, v0

    .line 78834
    iget-object v1, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 78835
    invoke-static {v0, v1}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 78836
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78837
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78838
    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78839
    iget-object v0, p1, LX/0Yj;->l:Ljava/util/Map;

    move-object v0, v0

    .line 78840
    if-eqz v0, :cond_0

    .line 78841
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78842
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78843
    iget-object v3, p1, LX/0Yj;->l:Ljava/util/Map;

    move-object v3, v3

    .line 78844
    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/util/Map;)V

    .line 78845
    :cond_0
    iget-object v0, p1, LX/0Yj;->o:Ljava/lang/String;

    move-object v0, v0

    .line 78846
    if-eqz v0, :cond_1

    .line 78847
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78848
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78849
    const-string v3, "tag_name"

    .line 78850
    iget-object v4, p1, LX/0Yj;->o:Ljava/lang/String;

    move-object v4, v4

    .line 78851
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 78852
    :cond_1
    iget-object v0, p1, LX/0Yj;->p:Ljava/lang/String;

    move-object v0, v0

    .line 78853
    if-eqz v0, :cond_2

    .line 78854
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78855
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78856
    const-string v3, "tag_value"

    .line 78857
    iget-object v4, p1, LX/0Yj;->p:Ljava/lang/String;

    move-object v4, v4

    .line 78858
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 78859
    :cond_2
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78860
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78861
    const/4 v3, 0x2

    .line 78862
    iget-wide v7, p1, LX/0Yj;->h:J

    move-wide v4, v7

    .line 78863
    iget-object v6, p1, LX/0Yj;->u:LX/03R;

    move-object v6, v6

    .line 78864
    invoke-interface/range {v0 .. v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IISJLX/03R;)V

    .line 78865
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 78866
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c()Z

    move-result v0

    return v0
.end method

.method public final c(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 78867
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 78868
    return-void
.end method

.method public final c(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78869
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78870
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x3

    invoke-interface {v1, p1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 78871
    return-void
.end method

.method public final c(LX/0Yj;)V
    .locals 1

    .prologue
    .line 78872
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/0XW;->a(LX/0Yj;Z)V

    .line 78873
    return-void
.end method

.method public final d(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 78874
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;)V

    .line 78875
    return-void
.end method

.method public final d(LX/0Yj;)V
    .locals 8

    .prologue
    .line 78902
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, v0

    .line 78903
    iget-object v1, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 78904
    invoke-static {v0, v1}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 78905
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78906
    iget v1, p1, LX/0Yj;->a:I

    move v1, v1

    .line 78907
    const/4 v3, 0x3

    .line 78908
    iget-wide v6, p1, LX/0Yj;->h:J

    move-wide v4, v6

    .line 78909
    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 78910
    return-void
.end method

.method public final d(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 78876
    invoke-static {p2, p3}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78877
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    return v0
.end method

.method public final e(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 78878
    const/4 v0, 0x0

    invoke-static {p2, v0}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78879
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78880
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IILjava/lang/String;)V

    .line 78881
    :cond_0
    return-void
.end method

.method public final e(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78882
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 78883
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 78884
    return-void
.end method

.method public final e(LX/0Yj;)Z
    .locals 3

    .prologue
    .line 78885
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, v0

    .line 78886
    iget-object v1, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 78887
    invoke-static {v0, v1}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78888
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78889
    iget v2, p1, LX/0Yj;->a:I

    move v2, v2

    .line 78890
    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    return v0
.end method

.method public final f(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 78891
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 78892
    return-void
.end method

.method public final f(LX/0Yj;)V
    .locals 3

    .prologue
    .line 78893
    iget-object v0, p1, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, v0

    .line 78894
    iget-object v1, p1, LX/0Yj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 78895
    invoke-static {v0, v1}, LX/0XW;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 78896
    iget-object v1, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 78897
    iget v2, p1, LX/0Yj;->a:I

    move v2, v2

    .line 78898
    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 78899
    return-void
.end method

.method public final g(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 78900
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    return v0
.end method

.method public final h(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 78901
    iget-object v0, p0, LX/0XW;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(II)Z

    move-result v0

    return v0
.end method
