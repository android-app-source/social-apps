.class public LX/11B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/11B;


# instance fields
.field public a:LX/0XZ;

.field public b:Ljava/util/Random;


# direct methods
.method public constructor <init>(LX/0XZ;Ljava/util/Random;)V
    .locals 0
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170614
    iput-object p1, p0, LX/11B;->a:LX/0XZ;

    .line 170615
    iput-object p2, p0, LX/11B;->b:Ljava/util/Random;

    .line 170616
    return-void
.end method

.method public static a(LX/0QB;)LX/11B;
    .locals 5

    .prologue
    .line 170617
    sget-object v0, LX/11B;->c:LX/11B;

    if-nez v0, :cond_1

    .line 170618
    const-class v1, LX/11B;

    monitor-enter v1

    .line 170619
    :try_start_0
    sget-object v0, LX/11B;->c:LX/11B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 170620
    if-eqz v2, :cond_0

    .line 170621
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 170622
    new-instance p0, LX/11B;

    invoke-static {v0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object v3

    check-cast v3, LX/0XZ;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v4

    check-cast v4, Ljava/util/Random;

    invoke-direct {p0, v3, v4}, LX/11B;-><init>(LX/0XZ;Ljava/util/Random;)V

    .line 170623
    move-object v0, p0

    .line 170624
    sput-object v0, LX/11B;->c:LX/11B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 170626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170627
    :cond_1
    sget-object v0, LX/11B;->c:LX/11B;

    return-object v0

    .line 170628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 170629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
