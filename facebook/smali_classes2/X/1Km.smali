.class public LX/1Km;
.super LX/1Kn;
.source ""

# interfaces
.implements LX/1DD;
.implements LX/1KT;
.implements LX/0hk;
.implements LX/1KU;
.implements LX/0fy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/1Ce;",
        ">",
        "LX/1Kn;",
        "LX/1DD;",
        "LX/1KT;",
        "LX/0hk;",
        "LX/1KU;",
        "LX/0fy;"
    }
.end annotation


# instance fields
.field public final a:LX/1Kt;

.field private final b:LX/0bH;

.field private final c:LX/0fx;

.field private final d:LX/0Uh;

.field private final e:LX/0ad;

.field public f:LX/1DK;

.field public g:LX/1Ce;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field

.field public h:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;LX/1AP;LX/0bH;LX/0Or;LX/0Or;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/1AP;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "LX/23N;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1k3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233147
    invoke-direct {p0}, LX/1Kn;-><init>()V

    .line 233148
    iput-object p1, p0, LX/1Km;->d:LX/0Uh;

    .line 233149
    iput-object p2, p0, LX/1Km;->e:LX/0ad;

    .line 233150
    iput-object p4, p0, LX/1Km;->b:LX/0bH;

    .line 233151
    new-instance v0, LX/1Ko;

    invoke-direct {v0, p0}, LX/1Ko;-><init>(LX/1Km;)V

    move-object v0, v0

    .line 233152
    invoke-virtual {p3, v0}, LX/1AP;->b(LX/0fx;)LX/1Ks;

    move-result-object v0

    iput-object v0, p0, LX/1Km;->c:LX/0fx;

    .line 233153
    iget-object v0, p0, LX/1Km;->e:LX/0ad;

    sget-short v1, LX/0fe;->G:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    :goto_0
    iput-object v0, p0, LX/1Km;->a:LX/1Kt;

    .line 233154
    return-void

    .line 233155
    :cond_0
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kt;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 233145
    iget-object v0, p0, LX/1Km;->c:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 233146
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 233143
    iget-object v0, p0, LX/1Km;->c:LX/0fx;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 233144
    return-void
.end method

.method public final a(LX/0gf;)V
    .locals 3

    .prologue
    .line 233136
    invoke-virtual {p1}, LX/0gf;->isAutoRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    .line 233137
    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    .line 233138
    iget-object v1, v0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p0

    .line 233139
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    .line 233140
    iget-object v1, v0, LX/1Kt;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ce;

    iget-object p1, v0, LX/1Kt;->g:LX/01J;

    invoke-interface {v1, p1}, LX/1Ce;->a(LX/01J;)V

    .line 233141
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 233142
    :cond_0
    return-void
.end method

.method public final a(LX/1OP;)V
    .locals 2

    .prologue
    .line 233133
    iget-object v0, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    if-nez v0, :cond_1

    .line 233134
    :cond_0
    :goto_0
    return-void

    .line 233135
    :cond_1
    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    iget-object v1, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v1}, LX/1DK;->a()LX/0g8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/0g8;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 233112
    iget-object v0, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    .line 233113
    if-nez v0, :cond_0

    .line 233114
    :goto_0
    return-void

    .line 233115
    :cond_0
    iget-object v1, p0, LX/1Km;->a:LX/1Kt;

    invoke-virtual {v1, p1, v0}, LX/1Kt;->a(ZLX/0g8;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 233131
    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    iget-object v1, p0, LX/1Km;->g:LX/1Ce;

    invoke-virtual {v0, v1}, LX/1Kt;->b(LX/1Ce;)V

    .line 233132
    return-void
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 233125
    check-cast p1, LX/1Zh;

    .line 233126
    iget-object v0, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    .line 233127
    if-nez v0, :cond_1

    .line 233128
    :cond_0
    :goto_0
    return-void

    .line 233129
    :cond_1
    iget-boolean v1, p1, LX/1Zh;->a:Z

    if-nez v1, :cond_0

    .line 233130
    iget-object v1, p0, LX/1Km;->a:LX/1Kt;

    invoke-virtual {v1, v0}, LX/1Kt;->c(LX/0g8;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 233119
    iget-object v0, p0, LX/1Km;->b:LX/0bH;

    invoke-virtual {v0, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 233120
    iget-object v0, p0, LX/1Km;->d:LX/0Uh;

    const/16 v1, 0x2d5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Km;->h:LX/1Iu;

    .line 233121
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 233122
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233123
    :cond_0
    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    const/4 v1, 0x1

    iget-object v2, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v2}, LX/1DK;->a()LX/0g8;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 233124
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 233116
    iget-object v0, p0, LX/1Km;->b:LX/0bH;

    invoke-virtual {v0, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 233117
    iget-object v0, p0, LX/1Km;->a:LX/1Kt;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1Km;->f:LX/1DK;

    invoke-interface {v2}, LX/1DK;->a()LX/0g8;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 233118
    return-void
.end method
