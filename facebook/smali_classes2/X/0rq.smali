.class public LX/0rq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0rq;


# instance fields
.field private final a:F

.field private final b:I

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Boolean;

.field private final f:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/webp/annotation/IsWebpEnabled;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/webp/annotation/IsWebpForProfilePicturesEnabled;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/WindowManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 150961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150962
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, LX/0rq;->a:F

    .line 150963
    const/16 v0, 0x438

    iput v0, p0, LX/0rq;->b:I

    .line 150964
    iput-object p1, p0, LX/0rq;->f:Landroid/view/WindowManager;

    .line 150965
    iput-object p2, p0, LX/0rq;->c:LX/0Or;

    .line 150966
    iput-object p3, p0, LX/0rq;->d:LX/0Or;

    .line 150967
    iput-object p4, p0, LX/0rq;->e:Ljava/lang/Boolean;

    .line 150968
    return-void
.end method

.method public static a(LX/0QB;)LX/0rq;
    .locals 7

    .prologue
    .line 150985
    sget-object v0, LX/0rq;->g:LX/0rq;

    if-nez v0, :cond_1

    .line 150986
    const-class v1, LX/0rq;

    monitor-enter v1

    .line 150987
    :try_start_0
    sget-object v0, LX/0rq;->g:LX/0rq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 150988
    if-eqz v2, :cond_0

    .line 150989
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 150990
    new-instance v5, LX/0rq;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    const/16 v4, 0x159c

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v4, 0x159d

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-direct {v5, v3, v6, p0, v4}, LX/0rq;-><init>(Landroid/view/WindowManager;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V

    .line 150991
    move-object v0, v5

    .line 150992
    sput-object v0, LX/0rq;->g:LX/0rq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150993
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 150994
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 150995
    :cond_1
    sget-object v0, LX/0rq;->g:LX/0rq;

    return-object v0

    .line 150996
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 150997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Z)LX/0wF;
    .locals 1

    .prologue
    .line 150984
    if-eqz p0, :cond_0

    sget-object v0, LX/0wF;->IMAGEWEBP:LX/0wF;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0wF;->IMAGEXAUTO:LX/0wF;

    goto :goto_0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150983
    sget-object v0, LX/0wG;->CONTAIN_FIT:LX/0wG;

    invoke-virtual {v0}, LX/0wG;->styleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()I
    .locals 3

    .prologue
    .line 150979
    invoke-virtual {p0}, LX/0rq;->e()I

    move-result v0

    .line 150980
    iget-object v1, p0, LX/0rq;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150981
    iget-object v1, p0, LX/0rq;->f:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 150982
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()LX/0wF;
    .locals 1

    .prologue
    .line 150978
    iget-object v0, p0, LX/0rq;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, LX/0rq;->a(Z)LX/0wF;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0wF;
    .locals 1

    .prologue
    .line 150977
    iget-object v0, p0, LX/0rq;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, LX/0rq;->a(Z)LX/0wF;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0wF;
    .locals 1

    .prologue
    .line 150974
    iget-object v0, p0, LX/0rq;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150975
    sget-object v0, LX/0wF;->IMAGEWEBP:LX/0wF;

    .line 150976
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0wF;->IMAGEJPEG:LX/0wF;

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 150972
    iget-object v0, p0, LX/0rq;->f:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 150973
    return v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 150971
    invoke-direct {p0}, LX/0rq;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 150970
    invoke-direct {p0}, LX/0rq;->i()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 150969
    invoke-direct {p0}, LX/0rq;->i()I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
