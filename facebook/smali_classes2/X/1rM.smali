.class public LX/1rM;
.super LX/1Yf;
.source ""

# interfaces
.implements LX/0dc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile w:LX/1rM;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation runtime Lcom/facebook/zero/annotations/IsInZeroInterstitialGatekeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0yH;

.field public final i:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0tX;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Landroid/content/res/Resources;

.field public final m:LX/0Uh;

.field public final n:LX/0yV;

.field private final o:LX/17Y;

.field private final p:Lcom/facebook/content/SecureContextHelper;

.field private final q:Landroid/content/Context;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Xw;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Y6;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/zero/protocol/graphql/ZeroOptinGraphQLModels$FetchZeroOptinQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public v:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0yH;LX/0Or;LX/0tX;LX/0Ot;Landroid/content/res/Resources;LX/0Uh;LX/0yV;Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/annotations/IsInZeroInterstitialGatekeeper;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/common/IsDialtoneEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0yV;",
            "Landroid/content/Context;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0yI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Xw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Y6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332137
    invoke-direct {p0}, LX/1Yf;-><init>()V

    .line 332138
    iput-object p1, p0, LX/1rM;->a:LX/0Ot;

    .line 332139
    iput-object p2, p0, LX/1rM;->b:LX/0Ot;

    .line 332140
    iput-object p3, p0, LX/1rM;->c:LX/0Ot;

    .line 332141
    iput-object p4, p0, LX/1rM;->d:LX/0Or;

    .line 332142
    iput-object p5, p0, LX/1rM;->e:LX/0Ot;

    .line 332143
    iput-object p6, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 332144
    iput-object p7, p0, LX/1rM;->g:LX/0Ot;

    .line 332145
    iput-object p8, p0, LX/1rM;->h:LX/0yH;

    .line 332146
    iput-object p9, p0, LX/1rM;->i:LX/0Or;

    .line 332147
    iput-object p10, p0, LX/1rM;->j:LX/0tX;

    .line 332148
    iput-object p11, p0, LX/1rM;->k:LX/0Ot;

    .line 332149
    iput-object p12, p0, LX/1rM;->l:Landroid/content/res/Resources;

    .line 332150
    iput-object p13, p0, LX/1rM;->m:LX/0Uh;

    .line 332151
    iput-object p14, p0, LX/1rM;->n:LX/0yV;

    .line 332152
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1rM;->q:Landroid/content/Context;

    .line 332153
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1rM;->o:LX/17Y;

    .line 332154
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1rM;->p:Lcom/facebook/content/SecureContextHelper;

    .line 332155
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1rM;->r:LX/0Ot;

    .line 332156
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1rM;->s:LX/0Ot;

    .line 332157
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1rM;->t:LX/0Ot;

    .line 332158
    return-void
.end method

.method public static a(LX/0QB;)LX/1rM;
    .locals 3

    .prologue
    .line 332127
    sget-object v0, LX/1rM;->w:LX/1rM;

    if-nez v0, :cond_1

    .line 332128
    const-class v1, LX/1rM;

    monitor-enter v1

    .line 332129
    :try_start_0
    sget-object v0, LX/1rM;->w:LX/1rM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332130
    if-eqz v2, :cond_0

    .line 332131
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1rM;->b(LX/0QB;)LX/1rM;

    move-result-object v0

    sput-object v0, LX/1rM;->w:LX/1rM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332132
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332133
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332134
    :cond_1
    sget-object v0, LX/1rM;->w:LX/1rM;

    return-object v0

    .line 332135
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1rM;Ljava/lang/String;J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 332117
    iget-object v1, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v2, "last_displayed_time_key"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 332118
    cmp-long v0, p2, v4

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1rM;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    sget-object v1, LX/0yY;->DATA_SAVING_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332119
    const-wide/32 p2, 0x5265c00

    .line 332120
    :cond_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1rM;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1rM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    add-long/2addr v2, p2

    iget-object v0, p0, LX/1rM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 332121
    iget-object v0, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v0, LX/0df;->x:LX/0Tn;

    const-string v1, "last_displayed_time_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, LX/1rM;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 332122
    invoke-direct {p0}, LX/1rM;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332123
    :cond_1
    :goto_0
    return-void

    .line 332124
    :cond_2
    iget-object v0, p0, LX/1rM;->o:LX/17Y;

    iget-object v1, p0, LX/1rM;->q:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 332125
    if-eqz v0, :cond_1

    .line 332126
    iget-object v1, p0, LX/1rM;->p:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/1rM;->q:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/1rM;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 332107
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 332108
    iget-object v0, p0, LX/1rM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "zero_rating"

    const-string v2, "Error fetching zero interstitial request"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332109
    return-void
.end method

.method private static b(LX/0QB;)LX/1rM;
    .locals 23

    .prologue
    .line 332115
    new-instance v2, LX/1rM;

    const/16 v3, 0x2e3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x13ec

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x15ad

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x245

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v9, 0x13f1

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v10

    check-cast v10, LX/0yH;

    const/16 v11, 0x1483

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    const/16 v13, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v14

    check-cast v14, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v15

    check-cast v15, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0yV;->a(LX/0QB;)LX/0yV;

    move-result-object v16

    check-cast v16, LX/0yV;

    const-class v17, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v18

    check-cast v18, LX/17Y;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v19

    check-cast v19, Lcom/facebook/content/SecureContextHelper;

    const/16 v20, 0x13f6

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x3937

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x393a

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, LX/1rM;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0yH;LX/0Or;LX/0tX;LX/0Ot;Landroid/content/res/Resources;LX/0Uh;LX/0yV;Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 332116
    return-object v2
.end method

.method private g()Z
    .locals 4

    .prologue
    .line 332159
    iget-object v0, p0, LX/1rM;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    invoke-virtual {v0}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->e()LX/0Rf;

    move-result-object v0

    .line 332160
    sget-object v1, LX/0yY;->OPTIN_GROUP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/0yY;->DATA_SAVING_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332161
    iget-object v0, p0, LX/1rM;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Xw;

    .line 332162
    iget-object v1, v0, LX/7Xw;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/13n;

    .line 332163
    iget-object v2, v1, LX/13n;->a:Landroid/app/Activity;

    move-object v1, v2

    .line 332164
    instance-of v2, v1, Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_0

    .line 332165
    new-instance v2, LX/6WI;

    invoke-direct {v2, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v3, 0x7f02040e

    invoke-virtual {v2, v3}, LX/6WI;->c(I)LX/6WI;

    move-result-object v2

    const v3, 0x7f080e88

    invoke-virtual {v2, v3}, LX/6WI;->a(I)LX/6WI;

    move-result-object v2

    const v3, 0x7f080e89

    invoke-virtual {v2, v3}, LX/6WI;->b(I)LX/6WI;

    move-result-object v2

    const v3, 0x7f080e8a

    new-instance p0, LX/7Xt;

    invoke-direct {p0, v0, v1}, LX/7Xt;-><init>(LX/7Xw;Landroid/app/Activity;)V

    invoke-virtual {v2, v3, p0}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v2

    const v3, 0x7f080e8b

    new-instance p0, LX/7Xs;

    invoke-direct {p0, v0}, LX/7Xs;-><init>(LX/7Xw;)V

    invoke-virtual {v2, v3, p0}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v2

    .line 332166
    new-instance v3, Lcom/facebook/zero/ui/DSMOptinProvider$3;

    invoke-direct {v3, v0, v2}, Lcom/facebook/zero/ui/DSMOptinProvider$3;-><init>(LX/7Xw;LX/6WI;)V

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 332167
    :cond_0
    const/4 v0, 0x1

    .line 332168
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/1rM;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 332110
    iget-object v0, p0, LX/1rM;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 332111
    iget-object v0, p0, LX/1rM;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 332112
    :cond_0
    iget-object v0, p0, LX/1rM;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 332113
    iget-object v0, p0, LX/1rM;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 332114
    :cond_1
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332104
    invoke-static {p0}, LX/1rM;->h(LX/1rM;)V

    .line 332105
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 332106
    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 332062
    iget-object v0, p0, LX/1rM;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 332063
    const/4 v0, 0x0

    .line 332064
    :goto_0
    move v0, v0

    .line 332065
    if-eqz v0, :cond_2

    .line 332066
    const/4 v2, 0x0

    .line 332067
    iget-object v0, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->r:LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332068
    iget-object v0, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->r:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 332069
    :cond_0
    invoke-static {p0}, LX/1rM;->h(LX/1rM;)V

    .line 332070
    iget-object v0, p0, LX/1rM;->m:LX/0Uh;

    const/16 v1, 0x2e2

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_6

    .line 332071
    iget-object v0, p0, LX/1rM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v0

    .line 332072
    new-instance v1, LX/7XB;

    invoke-direct {v1}, LX/7XB;-><init>()V

    move-object v1, v1

    .line 332073
    const-string v2, "screen_scale"

    iget-object v3, p0, LX/1rM;->l:Landroid/content/res/Resources;

    invoke-static {v3}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "carrier_mcc"

    .line 332074
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v3, v3

    .line 332075
    iget-object v4, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v3, v4

    .line 332076
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "carrier_mnc"

    .line 332077
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->b:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v3, v3

    .line 332078
    iget-object v4, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v3, v4

    .line 332079
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "sim_mcc"

    .line 332080
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v3, v3

    .line 332081
    iget-object v4, v3, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->a:Ljava/lang/String;

    move-object v3, v4

    .line 332082
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "sim_mnc"

    .line 332083
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;->c:Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;

    move-object v0, v3

    .line 332084
    iget-object v3, v0, Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc$MccMncPair;->b:Ljava/lang/String;

    move-object v0, v3

    .line 332085
    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "interface"

    iget-object v0, p0, LX/1rM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7XB;

    .line 332086
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 332087
    iget-object v1, p0, LX/1rM;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/1rM;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 332088
    iget-object v1, p0, LX/1rM;->u:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/7Xk;

    invoke-direct {v2, p0}, LX/7Xk;-><init>(LX/1rM;)V

    iget-object v0, p0, LX/1rM;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 332089
    :cond_1
    :goto_1
    return-void

    .line 332090
    :cond_2
    iget-object v0, p0, LX/1rM;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    sget-object v1, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1rM;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yI;

    sget-object v1, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 332091
    iget-object v0, p0, LX/1rM;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0df;->L:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 332092
    iget-object v0, p0, LX/1rM;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Y6;

    .line 332093
    iget-object v1, v0, LX/7Y6;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0df;->N:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 332094
    iget-object v1, v0, LX/7Y6;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0df;->O:LX/0Tn;

    const-string p0, ""

    invoke-interface {v1, v3, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 332095
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 p0, 0x0

    aput-object v2, v3, p0

    const/4 p0, 0x1

    aput-object v1, v3, p0

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 332096
    iget-object v1, v0, LX/7Y6;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7WS;

    new-instance v3, LX/7Y4;

    iget-object v2, v0, LX/7Y6;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v0, v2}, LX/7Y4;-><init>(LX/7Y6;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-virtual {v1, v3}, LX/7WS;->a(LX/0TF;)V

    .line 332097
    :cond_3
    :goto_2
    goto :goto_1

    .line 332098
    :cond_4
    iget-object v0, p0, LX/1rM;->h:LX/0yH;

    sget-object v1, LX/0yY;->DIALTONE_OPTIN:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 332099
    iget-object v0, p0, LX/1rM;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto/16 :goto_0

    .line 332100
    :cond_5
    iget-object v0, p0, LX/1rM;->h:LX/0yH;

    sget-object v1, LX/0yY;->OPTIN_GROUP_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    goto/16 :goto_0

    .line 332101
    :cond_6
    new-instance v2, LX/7Xl;

    invoke-direct {v2, p0}, LX/7Xl;-><init>(LX/1rM;)V

    .line 332102
    iget-object v0, p0, LX/1rM;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    new-instance v3, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;

    iget-object v1, p0, LX/1rM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pA;

    invoke-virtual {v1}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v4

    iget-object v1, p0, LX/1rM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1pA;

    invoke-virtual {v1}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;)V

    invoke-interface {v0, v3, v2}, LX/33e;->a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialEligibilityParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/1rM;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_1

    .line 332103
    :cond_7
    invoke-static {v0, v2, v1}, LX/7Y6;->a$redex0(LX/7Y6;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final e_(Z)V
    .locals 0

    .prologue
    .line 332060
    invoke-virtual {p0}, LX/1rM;->c()V

    .line 332061
    return-void
.end method
