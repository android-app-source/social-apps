.class public final LX/1l0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0QK;

.field public final synthetic b:LX/1kR;


# direct methods
.method public constructor <init>(LX/1kR;LX/0QK;)V
    .locals 0

    .prologue
    .line 310857
    iput-object p1, p0, LX/1l0;->b:LX/1kR;

    iput-object p2, p0, LX/1l0;->a:LX/0QK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 310858
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310859
    iget-object v0, p0, LX/1l0;->a:LX/0QK;

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    move v0, v1

    .line 310860
    :goto_0
    if-eqz v0, :cond_3

    .line 310861
    iget-object v0, p0, LX/1l0;->b:LX/1kR;

    invoke-static {v0}, LX/1kR;->b(LX/1kR;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 310862
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 310863
    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/1l0;->a:LX/0QK;

    invoke-interface {v0, p1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 310864
    :cond_3
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method
