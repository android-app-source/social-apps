.class public LX/0uK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0u4;


# instance fields
.field private final a:LX/0p9;


# direct methods
.method public constructor <init>(LX/0p9;)V
    .locals 0

    .prologue
    .line 156237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156238
    iput-object p1, p0, LX/0uK;->a:LX/0p9;

    .line 156239
    return-void
.end method


# virtual methods
.method public final a(J)LX/0uE;
    .locals 5

    .prologue
    .line 156240
    long-to-int v0, p1

    packed-switch v0, :pswitch_data_0

    .line 156241
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 156242
    :pswitch_0
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uK;->a:LX/0p9;

    invoke-virtual {v1}, LX/0p9;->b()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LX/0uE;-><init>(D)V

    goto :goto_0

    .line 156243
    :pswitch_1
    new-instance v0, LX/0uE;

    iget-object v1, p0, LX/0uK;->a:LX/0p9;

    invoke-virtual {v1}, LX/0p9;->a()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LX/0uE;-><init>(D)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156244
    new-instance v0, LX/0u5;

    const-string v1, "offline_fraction_in_fg"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, p0, v2, v3}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    new-instance v1, LX/0u5;

    const-string v2, "offline_fraction_in_bg"

    const-wide/16 v4, 0x2

    invoke-direct {v1, v2, p0, v4, v5}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
