.class public LX/1Hv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Hh;


# static fields
.field public static final a:J

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/io/File;

.field private final d:Z

.field public final e:Ljava/io/File;

.field public final f:LX/1GQ;

.field public final g:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 227878
    const-class v0, LX/1Hv;

    sput-object v0, LX/1Hv;->b:Ljava/lang/Class;

    .line 227879
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/1Hv;->a:J

    return-void
.end method

.method public constructor <init>(Ljava/io/File;ILX/1GQ;)V
    .locals 7

    .prologue
    .line 227880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227881
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227882
    iput-object p1, p0, LX/1Hv;->c:Ljava/io/File;

    .line 227883
    const/4 v0, 0x0

    .line 227884
    const/4 v2, 0x0

    .line 227885
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 227886
    if-eqz v1, :cond_0

    .line 227887
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 227888
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    .line 227889
    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 227890
    const/4 v0, 0x1

    .line 227891
    :cond_0
    :goto_0
    move v0, v0

    .line 227892
    iput-boolean v0, p0, LX/1Hv;->d:Z

    .line 227893
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/1Hv;->c:Ljava/io/File;

    .line 227894
    const/4 v2, 0x0

    const-string v3, "%s.ols%d.%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "v2"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 227895
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/1Hv;->e:Ljava/io/File;

    .line 227896
    iput-object p3, p0, LX/1Hv;->f:LX/1GQ;

    .line 227897
    const/4 v0, 0x1

    .line 227898
    const/4 v1, 0x0

    .line 227899
    iget-object v2, p0, LX/1Hv;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 227900
    :goto_1
    if-eqz v0, :cond_1

    .line 227901
    :try_start_1
    iget-object v0, p0, LX/1Hv;->e:Ljava/io/File;

    invoke-static {v0}, LX/04M;->a(Ljava/io/File;)V
    :try_end_1
    .catch LX/0Fu; {:try_start_1 .. :try_end_1} :catch_1

    .line 227902
    :cond_1
    :goto_2
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 227903
    iput-object v0, p0, LX/1Hv;->g:LX/0SG;

    .line 227904
    return-void

    .line 227905
    :catch_0
    move-exception v1

    .line 227906
    sget-object v3, LX/43W;->OTHER:LX/43W;

    sget-object v4, LX/1Hv;->b:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to read folder to check if external: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v3, v4, v2, v1}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 227907
    :cond_2
    iget-object v2, p0, LX/1Hv;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 227908
    iget-object v1, p0, LX/1Hv;->c:Ljava/io/File;

    invoke-static {v1}, LX/2W9;->b(Ljava/io/File;)Z

    goto :goto_1

    .line 227909
    :catch_1
    iget-object v0, p0, LX/1Hv;->f:LX/1GQ;

    sget-object v1, LX/43W;->WRITE_CREATE_DIR:LX/43W;

    sget-object v2, LX/1Hv;->b:Ljava/lang/Class;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "version directory could not be created: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/1Hv;->e:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 227910
    :try_start_0
    invoke-static {p1}, LX/04M;->a(Ljava/io/File;)V
    :try_end_0
    .catch LX/0Fu; {:try_start_0 .. :try_end_0} :catch_0

    .line 227911
    return-void

    .line 227912
    :catch_0
    move-exception v0

    .line 227913
    iget-object v1, p0, LX/1Hv;->f:LX/1GQ;

    sget-object v2, LX/43W;->WRITE_CREATE_DIR:LX/43W;

    sget-object v3, LX/1Hv;->b:Ljava/lang/Class;

    invoke-interface {v1, v2, v3, p2, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227914
    throw v0
.end method

.method private a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 227915
    invoke-virtual {p0, p1}, LX/1Hv;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 227916
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 227917
    if-eqz p2, :cond_0

    if-eqz v1, :cond_0

    .line 227918
    iget-object v2, p0, LX/1Hv;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 227919
    :cond_0
    return v1
.end method

.method public static b(LX/1Hv;Ljava/io/File;)LX/1gF;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 227933
    const/16 v6, 0x2e

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 227934
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 227935
    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 227936
    if-gtz v3, :cond_2

    move-object v1, v2

    .line 227937
    :goto_0
    move-object v1, v1

    .line 227938
    if-nez v1, :cond_1

    .line 227939
    :cond_0
    :goto_1
    return-object v0

    .line 227940
    :cond_1
    iget-object v2, v1, LX/1gF;->b:Ljava/lang/String;

    invoke-direct {p0, v2}, LX/1Hv;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 227941
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 227942
    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_1

    .line 227943
    :cond_2
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 227944
    invoke-static {v4}, LX/1gG;->fromExtension(Ljava/lang/String;)LX/1gG;

    move-result-object v4

    .line 227945
    if-nez v4, :cond_3

    move-object v1, v2

    .line 227946
    goto :goto_0

    .line 227947
    :cond_3
    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 227948
    sget-object v3, LX/1gG;->TEMP:LX/1gG;

    invoke-virtual {v4, v3}, LX/1gG;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 227949
    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 227950
    if-gtz v3, :cond_4

    move-object v1, v2

    .line 227951
    goto :goto_0

    .line 227952
    :cond_4
    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 227953
    :cond_5
    new-instance v2, LX/1gF;

    invoke-direct {v2, v4, v1}, LX/1gF;-><init>(LX/1gG;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public static b(LX/1Hv;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 227920
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    rem-int/lit8 v0, v0, 0x64

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 227921
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/1Hv;->e:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 227922
    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1}, LX/1Hv;->b(LX/1Hv;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/35g;)J
    .locals 5

    .prologue
    .line 227923
    check-cast p1, LX/35g;

    .line 227924
    iget-object v0, p1, LX/35g;->b:LX/1gH;

    move-object v0, v0

    .line 227925
    iget-object v1, v0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v1

    .line 227926
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 227927
    const-wide/16 v2, 0x0

    .line 227928
    :cond_0
    :goto_0
    move-wide v0, v2

    .line 227929
    return-wide v0

    .line 227930
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 227931
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_0

    .line 227932
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/32a;
    .locals 5

    .prologue
    .line 227868
    new-instance v0, LX/1gF;

    sget-object v1, LX/1gG;->TEMP:LX/1gG;

    invoke-direct {v0, v1, p1}, LX/1gF;-><init>(LX/1gG;Ljava/lang/String;)V

    .line 227869
    iget-object v1, v0, LX/1gF;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, LX/1Hv;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 227870
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 227871
    const-string v2, "insert"

    invoke-direct {p0, v1, v2}, LX/1Hv;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 227872
    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, LX/1gF;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".tmp"

    invoke-static {v2, v3, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 227873
    move-object v0, v2

    .line 227874
    new-instance v1, LX/32a;

    invoke-direct {v1, p0, p1, v0}, LX/32a;-><init>(LX/1Hv;Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 227875
    :catch_0
    move-exception v0

    .line 227876
    iget-object v1, p0, LX/1Hv;->f:LX/1GQ;

    sget-object v2, LX/43W;->WRITE_CREATE_TEMPFILE:LX/43W;

    sget-object v3, LX/1Hv;->b:Ljava/lang/Class;

    const-string v4, "insert"

    invoke-interface {v1, v2, v3, v4, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227877
    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 227844
    new-instance v0, Ljava/io/File;

    .line 227845
    new-instance v1, LX/1gF;

    sget-object v2, LX/1gG;->CONTENT:LX/1gG;

    invoke-direct {v1, v2, p1}, LX/1gF;-><init>(LX/1gG;Ljava/lang/String;)V

    .line 227846
    iget-object v2, v1, LX/1gF;->b:Ljava/lang/String;

    invoke-static {p0, v2}, LX/1Hv;->b(LX/1Hv;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 227847
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object p0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object p0, v1, LX/1gF;->b:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object p0, v1, LX/1gF;->a:LX/1gG;

    iget-object p0, p0, LX/1gG;->extension:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 227848
    move-object v1, v1

    .line 227849
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 227867
    iget-boolean v0, p0, LX/1Hv;->d:Z

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)LX/1gI;
    .locals 4

    .prologue
    .line 227862
    invoke-virtual {p0, p1}, LX/1Hv;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 227863
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227864
    iget-object v1, p0, LX/1Hv;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 227865
    invoke-static {v0}, LX/1gH;->a(Ljava/io/File;)LX/1gH;

    move-result-object v0

    .line 227866
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 227860
    iget-object v0, p0, LX/1Hv;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 227861
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 227858
    iget-object v0, p0, LX/1Hv;->c:Ljava/io/File;

    new-instance v1, LX/43c;

    invoke-direct {v1, p0}, LX/43c;-><init>(LX/1Hv;)V

    invoke-static {v0, v1}, LX/2W9;->a(Ljava/io/File;LX/32Z;)V

    .line 227859
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 227857
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1Hv;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 227855
    iget-object v0, p0, LX/1Hv;->c:Ljava/io/File;

    invoke-static {v0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 227856
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 227854
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1Hv;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 227850
    new-instance v0, LX/2uw;

    invoke-direct {v0, p0}, LX/2uw;-><init>(LX/1Hv;)V

    .line 227851
    iget-object v1, p0, LX/1Hv;->e:Ljava/io/File;

    invoke-static {v1, v0}, LX/2W9;->a(Ljava/io/File;LX/32Z;)V

    .line 227852
    iget-object v1, v0, LX/2uw;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 227853
    return-object v0
.end method
