.class public LX/11C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/11C;


# instance fields
.field private final a:LX/0Uh;

.field private b:Ljava/lang/Boolean;

.field private final c:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/00G;LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 170810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170811
    const/4 v0, 0x0

    iput-object v0, p0, LX/11C;->b:Ljava/lang/Boolean;

    .line 170812
    iput-object p3, p0, LX/11C;->a:LX/0Uh;

    .line 170813
    iget-object v0, p2, LX/00G;->b:Ljava/lang/String;

    move-object v0, v0

    .line 170814
    if-eqz v0, :cond_0

    .line 170815
    :goto_0
    new-instance v1, Ljava/io/File;

    const-string v2, "funnel_backup"

    const/4 p3, 0x0

    invoke-virtual {p1, v2, p3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 170816
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 170817
    new-instance v0, Ljava/io/File;

    const-string v2, "backup_for_all"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v0

    .line 170818
    iput-object v0, p0, LX/11C;->c:Ljava/io/File;

    .line 170819
    return-void

    .line 170820
    :cond_0
    const-string v0, "default"

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/11C;
    .locals 6

    .prologue
    .line 170630
    sget-object v0, LX/11C;->d:LX/11C;

    if-nez v0, :cond_1

    .line 170631
    const-class v1, LX/11C;

    monitor-enter v1

    .line 170632
    :try_start_0
    sget-object v0, LX/11C;->d:LX/11C;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 170633
    if-eqz v2, :cond_0

    .line 170634
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 170635
    new-instance p0, LX/11C;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VX;->b(LX/0QB;)LX/00G;

    move-result-object v4

    check-cast v4, LX/00G;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/11C;-><init>(Landroid/content/Context;LX/00G;LX/0Uh;)V

    .line 170636
    move-object v0, p0

    .line 170637
    sput-object v0, LX/11C;->d:LX/11C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170638
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 170639
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170640
    :cond_1
    sget-object v0, LX/11C;->d:LX/11C;

    return-object v0

    .line 170641
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 170642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/io/DataInputStream;)LX/1Zr;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 170753
    new-instance v0, LX/2o7;

    invoke-direct {v0}, LX/2o7;-><init>()V

    move-object v3, v0

    .line 170754
    move v2, v1

    .line 170755
    :goto_0
    if-nez v2, :cond_2

    .line 170756
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    move-result v0

    .line 170757
    packed-switch v0, :pswitch_data_0

    .line 170758
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " while loading funnels"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170759
    :pswitch_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0ih;->b(Ljava/lang/String;)LX/0ih;

    move-result-object v0

    .line 170760
    iput-object v0, v3, LX/2o7;->a:LX/0ih;

    .line 170761
    goto :goto_0

    .line 170762
    :pswitch_1
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 170763
    iput-wide v4, v3, LX/2o7;->b:J

    .line 170764
    goto :goto_0

    .line 170765
    :pswitch_2
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 170766
    iput v0, v3, LX/2o7;->c:I

    .line 170767
    goto :goto_0

    .line 170768
    :pswitch_3
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 170769
    iput-wide v4, v3, LX/2o7;->d:J

    .line 170770
    goto :goto_0

    .line 170771
    :pswitch_4
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 170772
    iput-wide v4, v3, LX/2o7;->e:J

    .line 170773
    goto :goto_0

    .line 170774
    :pswitch_5
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v0

    .line 170775
    iput-boolean v0, v3, LX/2o7;->h:Z

    .line 170776
    goto :goto_0

    .line 170777
    :pswitch_6
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 170778
    new-instance v5, LX/0UE;

    invoke-direct {v5, v4}, LX/0UE;-><init>(I)V

    move v0, v1

    .line 170779
    :goto_1
    if-ge v0, v4, :cond_0

    .line 170780
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 170781
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 170782
    :cond_0
    iput-object v5, v3, LX/2o7;->f:LX/0UE;

    .line 170783
    goto :goto_0

    .line 170784
    :pswitch_7
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 170785
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 170786
    :goto_2
    if-ge v0, v4, :cond_1

    .line 170787
    invoke-static {p0}, LX/11C;->b(Ljava/io/DataInputStream;)LX/1Zt;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170788
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 170789
    :cond_1
    iput-object v5, v3, LX/2o7;->g:Ljava/util/List;

    .line 170790
    goto/16 :goto_0

    .line 170791
    :pswitch_8
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v0

    .line 170792
    iput-boolean v0, v3, LX/2o7;->i:Z

    .line 170793
    goto/16 :goto_0

    .line 170794
    :pswitch_9
    const/4 v0, 0x1

    move v2, v0

    .line 170795
    goto/16 :goto_0

    .line 170796
    :cond_2
    new-instance v0, LX/1Zr;

    invoke-direct {v0, v3}, LX/1Zr;-><init>(LX/2o7;)V

    move-object v0, v0

    .line 170797
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method private static b(Ljava/io/DataInputStream;)LX/1Zt;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 170798
    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v1, v0

    .line 170799
    :goto_0
    if-nez v0, :cond_0

    .line 170800
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    move-result v5

    .line 170801
    packed-switch v5, :pswitch_data_0

    .line 170802
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while loading funnels"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170803
    :pswitch_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 170804
    :pswitch_1
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 170805
    :pswitch_2
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 170806
    :pswitch_3
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    goto :goto_0

    .line 170807
    :pswitch_4
    const/4 v0, 0x1

    .line 170808
    goto :goto_0

    .line 170809
    :cond_0
    new-instance v0, LX/1Zt;

    invoke-direct {v0, v4, v1, v3, v2}, LX/1Zt;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2bd
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private static b(LX/11C;)Z
    .locals 3

    .prologue
    .line 170750
    iget-object v0, p0, LX/11C;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 170751
    iget-object v0, p0, LX/11C;->a:LX/0Uh;

    const/16 v1, 0x8d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/11C;->b:Ljava/lang/Boolean;

    .line 170752
    :cond_0
    iget-object v0, p0, LX/11C;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1Zr;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 170728
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 170729
    iget-object v2, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 170730
    :goto_0
    monitor-exit p0

    return-object v0

    .line 170731
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/11C;->b(LX/11C;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 170732
    iget-object v1, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170734
    :cond_1
    :try_start_2
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, LX/11C;->c:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v5, 0x400

    invoke-direct {v3, v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170735
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readByte()B

    move-result v3

    .line 170736
    if-eq v3, v6, :cond_2

    .line 170737
    const-string v1, "FunnelBackupStorageFileImpl"

    const-string v4, "Expected version %d, found version %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v1, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 170738
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 170739
    :cond_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 170740
    :goto_1
    if-ge v1, v3, :cond_4

    .line 170741
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 170742
    invoke-static {v2}, LX/11C;->a(Ljava/io/DataInputStream;)LX/1Zr;

    move-result-object v5

    .line 170743
    iget-object v6, v5, LX/1Zr;->a:LX/0ih;

    move-object v6, v6

    .line 170744
    if-eqz v6, :cond_3

    .line 170745
    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170746
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 170747
    :cond_3
    const-string v5, "FunnelBackupStorageFileImpl"

    const-string v6, "Parsed funnel instance with null FunnelDefinition for key: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 170748
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    throw v0

    :cond_4
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V

    .line 170749
    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1Zr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170643
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/11C;->b(LX/11C;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 170644
    :cond_0
    iget-object v0, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 170645
    :goto_0
    return-void

    .line 170646
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".tmp"

    iget-object v2, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 170647
    new-instance v3, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v4, 0x400

    invoke-direct {v0, v1, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 170648
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 170649
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170650
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 170651
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Zr;

    .line 170652
    iget-object v5, v1, LX/1Zr;->a:LX/0ih;

    move-object v1, v5

    .line 170653
    if-nez v1, :cond_3

    .line 170654
    :cond_2
    const-string v1, "FunnelBackupStorageFileImpl"

    const-string v5, "null FunnelDefinition for key %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v1, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 170655
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    throw v0

    .line 170656
    :cond_3
    :try_start_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170657
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zr;

    .line 170658
    iget-boolean v1, v0, LX/1Zr;->e:Z

    move v1, v1

    .line 170659
    if-eqz v1, :cond_4

    .line 170660
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170661
    invoke-virtual {v0}, LX/1Zr;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170662
    const/4 v8, 0x5

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170663
    iget-wide v10, v0, LX/1Zr;->f:J

    move-wide v8, v10

    .line 170664
    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 170665
    const/16 v8, 0xa

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170666
    iget-boolean v8, v0, LX/1Zr;->e:Z

    move v8, v8

    .line 170667
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 170668
    const/16 v8, 0x9

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170669
    goto :goto_1

    .line 170670
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170671
    invoke-virtual {v0}, LX/1Zr;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170672
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170673
    invoke-static {v0}, LX/1Zr;->p(LX/1Zr;)V

    .line 170674
    iget-wide v10, v0, LX/1Zr;->b:J

    move-wide v8, v10

    .line 170675
    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 170676
    const/4 v8, 0x3

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170677
    invoke-static {v0}, LX/1Zr;->p(LX/1Zr;)V

    .line 170678
    iget v8, v0, LX/1Zr;->c:I

    move v8, v8

    .line 170679
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170680
    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170681
    iget-wide v10, v0, LX/1Zr;->d:J

    move-wide v8, v10

    .line 170682
    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 170683
    const/4 v8, 0x5

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170684
    iget-wide v10, v0, LX/1Zr;->f:J

    move-wide v8, v10

    .line 170685
    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 170686
    invoke-static {v0}, LX/1Zr;->p(LX/1Zr;)V

    .line 170687
    iget-object v8, v0, LX/1Zr;->g:LX/0UE;

    move-object v8, v8

    .line 170688
    if-eqz v8, :cond_5

    .line 170689
    const/4 v9, 0x6

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170690
    invoke-interface {v8}, Ljava/util/Set;->size()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170691
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 170692
    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_2

    .line 170693
    :cond_5
    invoke-static {v0}, LX/1Zr;->p(LX/1Zr;)V

    .line 170694
    iget-object v8, v0, LX/1Zr;->h:Ljava/util/List;

    move-object v8, v8

    .line 170695
    if-eqz v8, :cond_8

    .line 170696
    const/4 v9, 0x7

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170697
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v3, v9}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170698
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1Zt;

    .line 170699
    const/16 v11, 0x2c0

    .line 170700
    const/16 v10, 0x2bd

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170701
    iget-object v10, v8, LX/1Zt;->a:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170702
    const/16 v10, 0x2be

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170703
    iget v10, v8, LX/1Zt;->e:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170704
    iget-object v10, v8, LX/1Zt;->b:Ljava/lang/String;

    if-eqz v10, :cond_6

    .line 170705
    const/16 v10, 0x2bf

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170706
    iget-object v10, v8, LX/1Zt;->b:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170707
    :cond_6
    iget-object v10, v8, LX/1Zt;->c:LX/1rQ;

    if-eqz v10, :cond_b

    .line 170708
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170709
    iget-object v10, v8, LX/1Zt;->c:LX/1rQ;

    .line 170710
    iget-object v11, v10, LX/1rQ;->a:LX/0m9;

    move-object v10, v11

    .line 170711
    invoke-virtual {v10}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 170712
    :cond_7
    :goto_4
    const/16 v10, 0x2c1

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170713
    goto :goto_3

    .line 170714
    :cond_8
    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170715
    invoke-virtual {v0}, LX/1Zr;->l()Z

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 170716
    const/16 v8, 0x9

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170717
    goto/16 :goto_1

    .line 170718
    :cond_9
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    .line 170719
    monitor-enter p0

    .line 170720
    :try_start_2
    iget-object v0, p0, LX/11C;->c:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 170721
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 170722
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to replace the current preference file!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170723
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_a
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 170724
    invoke-interface {p1}, Ljava/util/Map;->size()I

    goto/16 :goto_0

    .line 170725
    :cond_b
    iget-object v10, v8, LX/1Zt;->d:Ljava/lang/String;

    if-eqz v10, :cond_7

    .line 170726
    invoke-virtual {v3, v11}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 170727
    iget-object v10, v8, LX/1Zt;->d:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_4
.end method
