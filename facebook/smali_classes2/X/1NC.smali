.class public LX/1NC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ND;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1ND;"
    }
.end annotation


# instance fields
.field public volatile a:LX/3Bq;

.field private b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 237057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237058
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 237059
    iput-object v0, p0, LX/1NC;->b:LX/0Rf;

    .line 237060
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 237061
    iput-object v0, p0, LX/1NC;->c:LX/0Rf;

    .line 237062
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1NC;->d:Ljava/util/List;

    .line 237063
    const/4 v0, 0x0

    iput-object v0, p0, LX/1NC;->a:LX/3Bq;

    .line 237064
    iput-boolean p1, p0, LX/1NC;->e:Z

    .line 237065
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237049
    iget-object v0, p0, LX/1NC;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237050
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 237051
    iget-object v0, p0, LX/1NC;->c:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 237052
    iget-object v0, p0, LX/1NC;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 237053
    invoke-virtual {v1, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_0

    .line 237054
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1NC;->c:LX/0Rf;

    .line 237055
    iget-object v0, p0, LX/1NC;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 237056
    :cond_1
    iget-object v0, p0, LX/1NC;->c:LX/0Rf;

    return-object v0
.end method

.method public final a(Ljava/util/Set;)LX/1ND;
    .locals 1

    .prologue
    .line 237046
    if-nez p1, :cond_0

    .line 237047
    :goto_0
    return-object p0

    .line 237048
    :cond_0
    iget-object v0, p0, LX/1NC;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 2

    .prologue
    .line 237011
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 237012
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_0

    .line 237013
    sget-object p1, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 237014
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237038
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 237039
    :goto_0
    return-void

    .line 237040
    :cond_0
    if-nez p1, :cond_2

    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 237041
    :goto_1
    if-eqz p2, :cond_1

    .line 237042
    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 237043
    invoke-static {p2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    iput-object v1, p0, LX/1NC;->b:LX/0Rf;

    .line 237044
    :cond_1
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1NC;->c:LX/0Rf;

    goto :goto_0

    .line 237045
    :cond_2
    invoke-static {p1}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/1NB;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 237027
    iget-object v0, p1, LX/1NB;->f:LX/1ND;

    instance-of v0, v0, LX/1NC;

    if-eqz v0, :cond_3

    .line 237028
    iget-object v0, p1, LX/1NB;->f:LX/1ND;

    check-cast v0, LX/1NC;

    .line 237029
    iget-boolean v1, v0, LX/1NC;->e:Z

    move v1, v1

    .line 237030
    if-eqz v1, :cond_0

    .line 237031
    iget-boolean v1, p0, LX/1NC;->e:Z

    move v1, v1

    .line 237032
    if-nez v1, :cond_1

    :cond_0
    move v0, v2

    .line 237033
    :goto_0
    return v0

    .line 237034
    :cond_1
    iget-object v1, p0, LX/1NC;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 237035
    invoke-virtual {v0}, LX/1NC;->a()LX/0Rf;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 237036
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 237037
    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 3

    .prologue
    .line 237015
    const-string v0, "GraphQLReadMutex.partiallyUpdateStale"

    const v1, -0x4dc130b9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 237016
    :try_start_0
    iget-object v0, p0, LX/1NC;->a:LX/3Bq;

    .line 237017
    if-eqz v0, :cond_0

    .line 237018
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 237019
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/3Bq;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    .line 237020
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 237021
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 237022
    move-object v0, v1

    .line 237023
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 237024
    const v1, -0x357ebbec    # -4235786.0f

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 237025
    :cond_0
    :try_start_1
    sget-object v0, LX/0tc;->a:Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237026
    const v1, 0x6362b821

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x3e298076

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
