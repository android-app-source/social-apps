.class public final LX/1kB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "LX/1RN;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;)V
    .locals 0

    .prologue
    .line 309100
    iput-object p1, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 309101
    iget-object v0, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-boolean v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->a:Z

    if-nez v0, :cond_0

    .line 309102
    iget-object v0, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v0, v0, LX/1QZ;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const-string v2, "prefetch_source"

    iget-object v3, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v3, v3, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 309103
    :cond_0
    iget-object v0, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v0, v0, LX/1QZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kD;

    .line 309104
    iget-object v1, v0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 309105
    iget-object v0, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->b:Ljava/lang/String;

    const-string v1, "on_ptr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 309106
    :goto_0
    iget-object v0, p0, LX/1kB;->a:Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;

    iget-object v0, v0, Lcom/facebook/productionprompts/common/InlineComposerPromptHolder$1;->d:LX/1QZ;

    iget-object v0, v0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rx;

    invoke-virtual {v0, v1}, LX/1Rx;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 309107
    new-instance v1, LX/1l7;

    invoke-direct {v1, p0}, LX/1l7;-><init>(LX/1kB;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 309108
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method
