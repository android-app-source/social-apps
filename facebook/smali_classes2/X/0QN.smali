.class public final LX/0QN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<+",
            "LX/0QP;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0QS;

.field public static final c:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/0QP;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0QV;

.field private static final u:Ljava/util/logging/Logger;


# instance fields
.field public e:Z

.field public f:I

.field public g:I

.field public h:J

.field public i:J

.field public j:LX/0Ql;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ql",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field public k:LX/0QX;

.field public l:LX/0QX;

.field public m:J

.field public n:J

.field public o:J

.field public p:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Qn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qn",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field public s:LX/0QV;

.field public t:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<+",
            "LX/0QP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 58059
    new-instance v0, LX/0QO;

    invoke-direct {v0}, LX/0QO;-><init>()V

    .line 58060
    new-instance v1, LX/0QQ;

    invoke-direct {v1, v0}, LX/0QQ;-><init>(Ljava/lang/Object;)V

    move-object v0, v1

    .line 58061
    sput-object v0, LX/0QN;->a:LX/0QR;

    .line 58062
    new-instance v1, LX/0QS;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    move-wide v10, v2

    move-wide v12, v2

    invoke-direct/range {v1 .. v13}, LX/0QS;-><init>(JJJJJJ)V

    sput-object v1, LX/0QN;->b:LX/0QS;

    .line 58063
    new-instance v0, LX/0QT;

    invoke-direct {v0}, LX/0QT;-><init>()V

    sput-object v0, LX/0QN;->c:LX/0QR;

    .line 58064
    new-instance v0, LX/0QU;

    invoke-direct {v0}, LX/0QU;-><init>()V

    sput-object v0, LX/0QN;->d:LX/0QV;

    .line 58065
    const-class v0, LX/0QN;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/0QN;->u:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 58066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58067
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0QN;->e:Z

    .line 58068
    iput v1, p0, LX/0QN;->f:I

    .line 58069
    iput v1, p0, LX/0QN;->g:I

    .line 58070
    iput-wide v2, p0, LX/0QN;->h:J

    .line 58071
    iput-wide v2, p0, LX/0QN;->i:J

    .line 58072
    iput-wide v2, p0, LX/0QN;->m:J

    .line 58073
    iput-wide v2, p0, LX/0QN;->n:J

    .line 58074
    iput-wide v2, p0, LX/0QN;->o:J

    .line 58075
    sget-object v0, LX/0QN;->a:LX/0QR;

    iput-object v0, p0, LX/0QN;->t:LX/0QR;

    .line 58076
    return-void
.end method

.method public static newBuilder()LX/0QN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QN",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58077
    new-instance v0, LX/0QN;

    invoke-direct {v0}, LX/0QN;-><init>()V

    return-object v0
.end method

.method private s()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 58078
    iget-object v2, p0, LX/0QN;->j:LX/0Ql;

    if-nez v2, :cond_2

    .line 58079
    iget-wide v2, p0, LX/0QN;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :goto_0
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 58080
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 58081
    goto :goto_0

    .line 58082
    :cond_2
    iget-boolean v2, p0, LX/0QN;->e:Z

    if-eqz v2, :cond_4

    .line 58083
    iget-wide v2, p0, LX/0QN;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :goto_2
    const-string v1, "weigher requires maximumWeight"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 58084
    :cond_4
    iget-wide v0, p0, LX/0QN;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 58085
    sget-object v0, LX/0QN;->u:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "ignoring weigher specified without maximumWeight"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0QM;)LX/0QJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "LX/0QM",
            "<-TK1;TV1;>;)",
            "LX/0QJ",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 58086
    invoke-direct {p0}, LX/0QN;->s()V

    .line 58087
    new-instance v0, LX/0Qb;

    invoke-direct {v0, p0, p1}, LX/0Qb;-><init>(LX/0QN;LX/0QM;)V

    return-object v0
.end method

.method public final a(J)LX/0QN;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58094
    iget-wide v4, p0, LX/0QN;->h:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58095
    iget-wide v4, p0, LX/0QN;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58096
    iget-object v0, p0, LX/0QN;->j:LX/0Ql;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "maximum size can not be combined with weigher"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 58097
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_3

    :goto_3
    const-string v0, "maximum size must not be negative"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 58098
    iput-wide p1, p0, LX/0QN;->h:J

    .line 58099
    return-object p0

    :cond_0
    move v0, v2

    .line 58100
    goto :goto_0

    :cond_1
    move v0, v2

    .line 58101
    goto :goto_1

    :cond_2
    move v0, v2

    .line 58102
    goto :goto_2

    :cond_3
    move v1, v2

    .line 58103
    goto :goto_3
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)LX/0QN;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58088
    iget-wide v4, p0, LX/0QN;->m:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->m:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58089
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58090
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0QN;->m:J

    .line 58091
    return-object p0

    :cond_0
    move v0, v2

    .line 58092
    goto :goto_0

    :cond_1
    move v0, v2

    .line 58093
    goto :goto_1
.end method

.method public final a(LX/0QX;)LX/0QN;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QX;",
            ")",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58115
    iget-object v0, p0, LX/0QN;->k:LX/0QX;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/0QN;->k:LX/0QX;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58116
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QX;

    iput-object v0, p0, LX/0QN;->k:LX/0QX;

    .line 58117
    return-object p0

    :cond_0
    move v0, v2

    .line 58118
    goto :goto_0
.end method

.method public final a(LX/0Ql;)LX/0QN;
    .locals 8
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "To be supported"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "LX/0Ql",
            "<-TK1;-TV1;>;)",
            "LX/0QN",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58108
    iget-object v0, p0, LX/0QN;->j:LX/0Ql;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 58109
    iget-boolean v0, p0, LX/0QN;->e:Z

    if-eqz v0, :cond_0

    .line 58110
    iget-wide v4, p0, LX/0QN;->h:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "weigher can not be combined with maximum size"

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v4, p0, LX/0QN;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58111
    :cond_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ql;

    iput-object v0, p0, LX/0QN;->j:LX/0Ql;

    .line 58112
    return-object p0

    :cond_1
    move v0, v2

    .line 58113
    goto :goto_0

    :cond_2
    move v0, v2

    .line 58114
    goto :goto_1
.end method

.method public final a(LX/0Qn;)LX/0QN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>(",
            "LX/0Qn",
            "<-TK1;-TV1;>;)",
            "LX/0QN",
            "<TK1;TV1;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 58104
    iget-object v0, p0, LX/0QN;->r:LX/0Qn;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 58105
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Qn;

    iput-object v0, p0, LX/0QN;->r:LX/0Qn;

    .line 58106
    return-object p0

    .line 58107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/0QN;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58045
    iget v0, p0, LX/0QN;->g:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "concurrency level was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, LX/0QN;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58046
    if-lez p1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 58047
    iput p1, p0, LX/0QN;->g:I

    .line 58048
    return-object p0

    :cond_0
    move v0, v2

    .line 58049
    goto :goto_0

    :cond_1
    move v1, v2

    .line 58050
    goto :goto_1
.end method

.method public final b(J)LX/0QN;
    .locals 11
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "To be supported"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58051
    iget-wide v4, p0, LX/0QN;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "maximum weight was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58052
    iget-wide v4, p0, LX/0QN;->h:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "maximum size was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58053
    iput-wide p1, p0, LX/0QN;->i:J

    .line 58054
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    :goto_2
    const-string v0, "maximum weight must not be negative"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 58055
    return-object p0

    :cond_0
    move v0, v2

    .line 58056
    goto :goto_0

    :cond_1
    move v0, v2

    .line 58057
    goto :goto_1

    :cond_2
    move v1, v2

    .line 58058
    goto :goto_2
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)LX/0QN;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57998
    iget-wide v4, p0, LX/0QN;->n:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0QN;->n:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 57999
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58000
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/0QN;->n:J

    .line 58001
    return-object p0

    :cond_0
    move v0, v2

    .line 58002
    goto :goto_0

    :cond_1
    move v0, v2

    .line 58003
    goto :goto_1
.end method

.method public final b(LX/0QX;)LX/0QN;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QX;",
            ")",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58004
    iget-object v0, p0, LX/0QN;->l:LX/0QX;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Value strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/0QN;->l:LX/0QX;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58005
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QX;

    iput-object v0, p0, LX/0QN;->l:LX/0QX;

    .line 58006
    return-object p0

    :cond_0
    move v0, v2

    .line 58007
    goto :goto_0
.end method

.method public final e()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 58008
    iget-wide v2, p0, LX/0QN;->m:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/0QN;->n:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 58009
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, LX/0QN;->j:LX/0Ql;

    if-nez v0, :cond_2

    iget-wide v0, p0, LX/0QN;->h:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, LX/0QN;->i:J

    goto :goto_0
.end method

.method public final g()LX/0QN;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.lang.ref.WeakReference"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58010
    sget-object v0, LX/0QX;->WEAK:LX/0QX;

    invoke-virtual {p0, v0}, LX/0QN;->a(LX/0QX;)LX/0QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0QX;
    .locals 2

    .prologue
    .line 58011
    iget-object v0, p0, LX/0QN;->k:LX/0QX;

    sget-object v1, LX/0QX;->STRONG:LX/0QX;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QX;

    return-object v0
.end method

.method public final i()LX/0QN;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.lang.ref.WeakReference"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58012
    sget-object v0, LX/0QX;->WEAK:LX/0QX;

    invoke-virtual {p0, v0}, LX/0QN;->b(LX/0QX;)LX/0QN;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0QX;
    .locals 2

    .prologue
    .line 58013
    iget-object v0, p0, LX/0QN;->l:LX/0QX;

    sget-object v1, LX/0QX;->STRONG:LX/0QX;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QX;

    return-object v0
.end method

.method public final l()J
    .locals 4

    .prologue
    .line 58014
    iget-wide v0, p0, LX/0QN;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0QN;->m:J

    goto :goto_0
.end method

.method public final m()J
    .locals 4

    .prologue
    .line 58015
    iget-wide v0, p0, LX/0QN;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0QN;->n:J

    goto :goto_0
.end method

.method public final n()J
    .locals 4

    .prologue
    .line 58016
    iget-wide v0, p0, LX/0QN;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/0QN;->o:J

    goto :goto_0
.end method

.method public final q()LX/0QI;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K1:TK;V1:TV;>()",
            "LX/0QI",
            "<TK1;TV1;>;"
        }
    .end annotation

    .prologue
    .line 58017
    invoke-direct {p0}, LX/0QN;->s()V

    .line 58018
    iget-wide v1, p0, LX/0QN;->o:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "refreshAfterWrite requires a LoadingCache"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 58019
    new-instance v0, LX/0Qc;

    invoke-direct {v0, p0}, LX/0Qc;-><init>(LX/0QN;)V

    return-object v0

    .line 58020
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, -0x1

    const-wide/16 v6, -0x1

    .line 58021
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    .line 58022
    iget v1, p0, LX/0QN;->f:I

    if-eq v1, v3, :cond_0

    .line 58023
    const-string v1, "initialCapacity"

    iget v2, p0, LX/0QN;->f:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    .line 58024
    :cond_0
    iget v1, p0, LX/0QN;->g:I

    if-eq v1, v3, :cond_1

    .line 58025
    const-string v1, "concurrencyLevel"

    iget v2, p0, LX/0QN;->g:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    .line 58026
    :cond_1
    iget-wide v2, p0, LX/0QN;->h:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 58027
    const-string v1, "maximumSize"

    iget-wide v2, p0, LX/0QN;->h:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    .line 58028
    :cond_2
    iget-wide v2, p0, LX/0QN;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 58029
    const-string v1, "maximumWeight"

    iget-wide v2, p0, LX/0QN;->i:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    .line 58030
    :cond_3
    iget-wide v2, p0, LX/0QN;->m:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 58031
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LX/0QN;->m:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 58032
    :cond_4
    iget-wide v2, p0, LX/0QN;->n:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    .line 58033
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LX/0QN;->n:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 58034
    :cond_5
    iget-object v1, p0, LX/0QN;->k:LX/0QX;

    if-eqz v1, :cond_6

    .line 58035
    const-string v1, "keyStrength"

    iget-object v2, p0, LX/0QN;->k:LX/0QX;

    invoke-virtual {v2}, LX/0QX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1IV;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 58036
    :cond_6
    iget-object v1, p0, LX/0QN;->l:LX/0QX;

    if-eqz v1, :cond_7

    .line 58037
    const-string v1, "valueStrength"

    iget-object v2, p0, LX/0QN;->l:LX/0QX;

    invoke-virtual {v2}, LX/0QX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1IV;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 58038
    :cond_7
    iget-object v1, p0, LX/0QN;->p:LX/0Qj;

    if-eqz v1, :cond_8

    .line 58039
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, LX/0zA;->addValue(Ljava/lang/Object;)LX/0zA;

    .line 58040
    :cond_8
    iget-object v1, p0, LX/0QN;->q:LX/0Qj;

    if-eqz v1, :cond_9

    .line 58041
    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, LX/0zA;->addValue(Ljava/lang/Object;)LX/0zA;

    .line 58042
    :cond_9
    iget-object v1, p0, LX/0QN;->r:LX/0Qn;

    if-eqz v1, :cond_a

    .line 58043
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, LX/0zA;->addValue(Ljava/lang/Object;)LX/0zA;

    .line 58044
    :cond_a
    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
