.class public LX/1XQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1LV;

.field public final b:LX/1UQ;

.field private final c:LX/1XS;

.field private final d:LX/1XS;

.field public final e:LX/1XT;

.field private final f:LX/0Wd;

.field public g:I

.field public h:LX/1XS;

.field private i:LX/0g7;

.field public j:LX/1XU;


# direct methods
.method public constructor <init>(LX/0g7;LX/1UQ;LX/1LV;Landroid/content/res/Resources;LX/1XR;LX/0Wd;LX/0Or;)V
    .locals 1
    .param p1    # LX/0g7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1UQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g7;",
            "Lcom/facebook/feed/rows/adapter/api/NewsFeedMultiAdapter;",
            "LX/1LV;",
            "Landroid/content/res/Resources;",
            "LX/1XR;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Or",
            "<",
            "LX/1XS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 271180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271181
    iput-object p3, p0, LX/1XQ;->a:LX/1LV;

    .line 271182
    iput-object p2, p0, LX/1XQ;->b:LX/1UQ;

    .line 271183
    iput-object p1, p0, LX/1XQ;->i:LX/0g7;

    .line 271184
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XS;

    iput-object v0, p0, LX/1XQ;->c:LX/1XS;

    .line 271185
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XS;

    iput-object v0, p0, LX/1XQ;->d:LX/1XS;

    .line 271186
    invoke-virtual {p4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/1XQ;->g:I

    .line 271187
    new-instance p4, LX/1XT;

    invoke-static {p5}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v0

    check-cast v0, LX/1Db;

    invoke-static {p5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p3

    check-cast p3, LX/03V;

    invoke-direct {p4, p1, p2, v0, p3}, LX/1XT;-><init>(LX/0g8;LX/1UQ;LX/1Db;LX/03V;)V

    .line 271188
    move-object v0, p4

    .line 271189
    iput-object v0, p0, LX/1XQ;->e:LX/1XT;

    .line 271190
    invoke-static {p0}, LX/1XQ;->c(LX/1XQ;)V

    .line 271191
    iput-object p6, p0, LX/1XQ;->f:LX/0Wd;

    .line 271192
    new-instance v0, LX/1XU;

    invoke-direct {v0, p0, p6}, LX/1XU;-><init>(LX/1XQ;LX/0Wd;)V

    iput-object v0, p0, LX/1XQ;->j:LX/1XU;

    .line 271193
    return-void
.end method

.method public static a$redex0(LX/1XQ;I)Lcom/facebook/graphql/model/FeedUnit;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 271173
    :try_start_0
    iget-object v0, p0, LX/1XQ;->b:LX/1UQ;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 271174
    instance-of v1, v0, LX/0jQ;

    if-nez v1, :cond_0

    .line 271175
    const/4 v0, 0x0

    .line 271176
    :goto_0
    return-object v0

    .line 271177
    :catch_0
    move-exception v0

    .line 271178
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "first stories position: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {v3}, LX/1UQ;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " last stories position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {v3}, LX/1UQ;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 271179
    :cond_0
    check-cast v0, LX/0jQ;

    invoke-interface {v0}, LX/0jQ;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/1XQ;I)I
    .locals 3

    .prologue
    .line 271167
    iget-object v0, p0, LX/1XQ;->i:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->o()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1XQ;->i:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->t()I

    move-result v0

    sub-int v0, p1, v0

    .line 271168
    :goto_0
    iget-object v1, p0, LX/1XQ;->b:LX/1UQ;

    invoke-interface {v1, v0}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/1XQ;->i:LX/0g7;

    invoke-virtual {v2, p1}, LX/0g7;->f(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    .line 271169
    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 271170
    return v0

    :cond_0
    move v0, p1

    .line 271171
    goto :goto_0

    .line 271172
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static c(LX/1XQ;)V
    .locals 2

    .prologue
    .line 271149
    iget v0, p0, LX/1XQ;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/1XQ;->c:LX/1XS;

    :goto_0
    iput-object v0, p0, LX/1XQ;->h:LX/1XS;

    .line 271150
    iget-object v0, p0, LX/1XQ;->e:LX/1XT;

    iget-object v1, p0, LX/1XQ;->h:LX/1XS;

    .line 271151
    iput-object v1, v0, LX/1XT;->g:LX/1XS;

    .line 271152
    return-void

    .line 271153
    :cond_0
    iget-object v0, p0, LX/1XQ;->d:LX/1XS;

    goto :goto_0
.end method

.method public static c(LX/1XQ;Lcom/facebook/graphql/model/FeedUnit;I)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 271162
    iget-object v0, p0, LX/1XQ;->b:LX/1UQ;

    invoke-interface {v0, p2}, LX/1Qr;->c(I)I

    move-result v1

    .line 271163
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 271164
    iget-object v2, p0, LX/1XQ;->e:LX/1XT;

    add-int v3, p2, v0

    invoke-virtual {v2, p1, v1, v3, v0}, LX/1XT;->a(Lcom/facebook/graphql/model/FeedUnit;III)V

    .line 271165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 271166
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 271154
    iget-object v0, p0, LX/1XQ;->j:LX/1XU;

    invoke-virtual {v0}, LX/1Rm;->b()V

    .line 271155
    new-instance v0, LX/1XU;

    iget-object v1, p0, LX/1XQ;->f:LX/0Wd;

    invoke-direct {v0, p0, v1}, LX/1XU;-><init>(LX/1XQ;LX/0Wd;)V

    iput-object v0, p0, LX/1XQ;->j:LX/1XU;

    .line 271156
    iget-object v0, p0, LX/1XQ;->j:LX/1XU;

    .line 271157
    iget-object v1, v0, LX/1XU;->a:LX/1XQ;

    iget-object v1, v1, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->b()I

    move-result v1

    iput v1, v0, LX/1XU;->b:I

    .line 271158
    iget-object v1, v0, LX/1XU;->a:LX/1XQ;

    iget-object v1, v1, LX/1XQ;->b:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->c()I

    move-result v1

    iput v1, v0, LX/1XU;->c:I

    .line 271159
    iget v1, v0, LX/1XU;->b:I

    iput v1, v0, LX/1XU;->d:I

    .line 271160
    iget-object v0, p0, LX/1XQ;->j:LX/1XU;

    invoke-virtual {v0}, LX/1Rm;->a()V

    .line 271161
    return-void
.end method
