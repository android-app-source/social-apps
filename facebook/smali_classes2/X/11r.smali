.class public LX/11r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/support/v4/app/Fragment;

.field private final b:LX/11s;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1Lb;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/base/fragment/FbFragmentOverrider;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/11s;)V
    .locals 1

    .prologue
    .line 173345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173346
    iput-object p1, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    .line 173347
    iput-object p2, p0, LX/11r;->b:LX/11s;

    .line 173348
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, LX/11r;->d:Ljava/util/Set;

    .line 173349
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, LX/11r;->c:Ljava/util/Set;

    .line 173350
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)LX/0am;
    .locals 3
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            "Landroid/os/Bundle;",
            ")",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173380
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lc;

    .line 173381
    invoke-virtual {v0, p1, p2, p3}, LX/1Lc;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)LX/0am;

    move-result-object v0

    .line 173382
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 173383
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 173384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/view/MenuItem;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173375
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lc;

    .line 173376
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2, p1}, LX/1Lc;->a(Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)LX/0am;

    move-result-object v0

    .line 173377
    invoke-virtual {v0}, LX/0am;->isPresent()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 173378
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 173379
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 173371
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173372
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, LX/1Lb;->a(Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173374
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 173367
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173368
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2, p1, p2, p3}, LX/1Lb;->a(Landroid/support/v4/app/Fragment;IILandroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173370
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/1Iu;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Iu",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173362
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 173363
    iget-object v0, p1, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 173364
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 173365
    :cond_1
    monitor-exit p0

    return-void

    .line 173366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1Lb;)V
    .locals 1

    .prologue
    .line 173355
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173356
    instance-of v0, p1, LX/1Lc;

    if-eqz v0, :cond_0

    .line 173357
    check-cast p1, LX/1Lc;

    .line 173358
    iget-object v0, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, v0}, LX/1Lc;->f(Landroid/support/v4/app/Fragment;)V

    .line 173359
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173360
    :cond_0
    monitor-exit p0

    return-void

    .line 173361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 173351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173352
    invoke-interface {v0, p1}, LX/1Lb;->a(Landroid/content/res/Configuration;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173354
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173330
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173331
    invoke-interface {v0, p1}, LX/1Lb;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173332
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173333
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173341
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173342
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2, p1, p2}, LX/1Lb;->a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173344
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 173339
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173340
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 3

    .prologue
    .line 173334
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lc;

    .line 173335
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2, p1}, LX/1Lc;->a(Landroid/support/v4/app/Fragment;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 173336
    const/4 v0, 0x1

    .line 173337
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 173338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173385
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lc;

    .line 173386
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173387
    if-eqz v0, :cond_0

    .line 173388
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 173389
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1Lb;)V
    .locals 1

    .prologue
    .line 173287
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 173288
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173289
    monitor-exit p0

    return-void

    .line 173290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173292
    invoke-interface {v0}, LX/1Lb;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173294
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Z)V
    .locals 2

    .prologue
    .line 173295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173296
    invoke-interface {v0, p1}, LX/1Lb;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173298
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 173299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lc;

    .line 173300
    invoke-virtual {v0}, LX/1Lc;->e()Landroid/view/MenuInflater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 173301
    if-eqz v0, :cond_0

    .line 173302
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 173303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 173304
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173305
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 173306
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173307
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, LX/1Lb;->d(Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173308
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173309
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized e()V
    .locals 3

    .prologue
    .line 173310
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173311
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, LX/1Lb;->c(Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173313
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized f()V
    .locals 3

    .prologue
    .line 173314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173315
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, LX/1Lb;->b(Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173317
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized g()V
    .locals 3

    .prologue
    .line 173318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173319
    iget-object v2, p0, LX/11r;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v2}, LX/1Lb;->e(Landroid/support/v4/app/Fragment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173321
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized h()V
    .locals 2

    .prologue
    .line 173322
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173323
    invoke-interface {v0}, LX/1Lb;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173325
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized i()V
    .locals 2

    .prologue
    .line 173326
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/11r;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lb;

    .line 173327
    invoke-interface {v0}, LX/1Lb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173329
    :cond_0
    monitor-exit p0

    return-void
.end method
