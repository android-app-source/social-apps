.class public LX/0d4;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89974
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 89975
    return-void
.end method

.method public static a(LX/0cb;)LX/0d6;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 89976
    invoke-interface {p0}, LX/0cb;->c()LX/0d6;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/common/perftest/PerfTestConfig;LX/0Or;LX/0Or;)Ljava/lang/Boolean;
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserTrustedTester;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/abtest/qe/annotations/IsUserTrustedWithQEInternals;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 89977
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89978
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 89979
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, LX/03R;->YES:LX/03R;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/03R;->YES:LX/03R;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/abtest/qe/annotations/LoggedInUserIdHash;
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 89980
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/03l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 89981
    return-void
.end method
