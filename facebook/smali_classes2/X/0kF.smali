.class public final LX/0kF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0T3;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0T3;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 126472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126473
    iput-object p1, p0, LX/0kF;->a:LX/0QB;

    .line 126474
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 126458
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0kF;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 126460
    packed-switch p2, :pswitch_data_0

    .line 126461
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126462
    :pswitch_0
    invoke-static {p1}, LX/0kJ;->a(LX/0QB;)LX/0kJ;

    move-result-object v0

    .line 126463
    :goto_0
    return-object v0

    .line 126464
    :pswitch_1
    invoke-static {p1}, LX/0kK;->a(LX/0QB;)LX/0kK;

    move-result-object v0

    check-cast v0, LX/0kK;

    invoke-static {v0}, LX/0kO;->a(LX/0kK;)LX/0kN;

    move-result-object v0

    move-object v0, v0

    .line 126465
    goto :goto_0

    .line 126466
    :pswitch_2
    invoke-static {p1}, LX/0kP;->a(LX/0QB;)LX/0Ux;

    move-result-object v0

    goto :goto_0

    .line 126467
    :pswitch_3
    invoke-static {p1}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v0

    check-cast v0, LX/0Sy;

    invoke-static {v0}, LX/0kR;->a(LX/0Sy;)LX/0Sz;

    move-result-object v0

    move-object v0, v0

    .line 126468
    goto :goto_0

    .line 126469
    :pswitch_4
    invoke-static {p1}, LX/0kS;->a(LX/0QB;)LX/0kS;

    move-result-object v0

    goto :goto_0

    .line 126470
    :pswitch_5
    invoke-static {p1}, LX/0kT;->a(LX/0QB;)LX/0kX;

    move-result-object v0

    goto :goto_0

    .line 126471
    :pswitch_6
    invoke-static {p1}, LX/0l1;->a(LX/0QB;)LX/0l1;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 126459
    const/4 v0, 0x7

    return v0
.end method
