.class public final enum LX/0qw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0qw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0qw;

.field public static final enum CHUNKED_INITIAL:LX/0qw;

.field public static final enum CHUNKED_REMAINDER:LX/0qw;

.field public static final enum DUPLICATE:LX/0qw;

.field public static final enum FULL:LX/0qw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 148690
    new-instance v0, LX/0qw;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v2}, LX/0qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0qw;->FULL:LX/0qw;

    .line 148691
    new-instance v0, LX/0qw;

    const-string v1, "CHUNKED_INITIAL"

    invoke-direct {v0, v1, v3}, LX/0qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    .line 148692
    new-instance v0, LX/0qw;

    const-string v1, "CHUNKED_REMAINDER"

    invoke-direct {v0, v1, v4}, LX/0qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    .line 148693
    new-instance v0, LX/0qw;

    const-string v1, "DUPLICATE"

    invoke-direct {v0, v1, v5}, LX/0qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0qw;->DUPLICATE:LX/0qw;

    .line 148694
    const/4 v0, 0x4

    new-array v0, v0, [LX/0qw;

    sget-object v1, LX/0qw;->FULL:LX/0qw;

    aput-object v1, v0, v2

    sget-object v1, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    aput-object v1, v0, v3

    sget-object v1, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    aput-object v1, v0, v4

    sget-object v1, LX/0qw;->DUPLICATE:LX/0qw;

    aput-object v1, v0, v5

    sput-object v0, LX/0qw;->$VALUES:[LX/0qw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 148695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0qw;
    .locals 1

    .prologue
    .line 148689
    const-class v0, LX/0qw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0qw;

    return-object v0
.end method

.method public static values()[LX/0qw;
    .locals 1

    .prologue
    .line 148688
    sget-object v0, LX/0qw;->$VALUES:[LX/0qw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0qw;

    return-object v0
.end method
