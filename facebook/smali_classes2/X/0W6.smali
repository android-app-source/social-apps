.class public final enum LX/0W6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0W6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0W6;

.field public static final enum FEW:LX/0W6;

.field public static final enum MANY:LX/0W6;

.field public static final enum ONE:LX/0W6;

.field public static final enum OTHER:LX/0W6;

.field private static final STRING_TO_PLURAL_CATEGORY:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0W6;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TWO:LX/0W6;

.field public static final enum ZERO:LX/0W6;

.field private static final map:[LX/0W6;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75473
    new-instance v0, LX/0W6;

    const-string v1, "ZERO"

    invoke-direct {v0, v1, v3}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->ZERO:LX/0W6;

    .line 75474
    new-instance v0, LX/0W6;

    const-string v1, "ONE"

    invoke-direct {v0, v1, v4}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->ONE:LX/0W6;

    .line 75475
    new-instance v0, LX/0W6;

    const-string v1, "TWO"

    invoke-direct {v0, v1, v5}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->TWO:LX/0W6;

    .line 75476
    new-instance v0, LX/0W6;

    const-string v1, "FEW"

    invoke-direct {v0, v1, v6}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->FEW:LX/0W6;

    .line 75477
    new-instance v0, LX/0W6;

    const-string v1, "MANY"

    invoke-direct {v0, v1, v7}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->MANY:LX/0W6;

    .line 75478
    new-instance v0, LX/0W6;

    const-string v1, "OTHER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0W6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0W6;->OTHER:LX/0W6;

    .line 75479
    const/4 v0, 0x6

    new-array v0, v0, [LX/0W6;

    sget-object v1, LX/0W6;->ZERO:LX/0W6;

    aput-object v1, v0, v3

    sget-object v1, LX/0W6;->ONE:LX/0W6;

    aput-object v1, v0, v4

    sget-object v1, LX/0W6;->TWO:LX/0W6;

    aput-object v1, v0, v5

    sget-object v1, LX/0W6;->FEW:LX/0W6;

    aput-object v1, v0, v6

    sget-object v1, LX/0W6;->MANY:LX/0W6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0W6;->OTHER:LX/0W6;

    aput-object v2, v0, v1

    sput-object v0, LX/0W6;->$VALUES:[LX/0W6;

    .line 75480
    const/4 v0, 0x6

    new-array v0, v0, [LX/0W6;

    sget-object v1, LX/0W6;->ZERO:LX/0W6;

    aput-object v1, v0, v3

    sget-object v1, LX/0W6;->ONE:LX/0W6;

    aput-object v1, v0, v4

    sget-object v1, LX/0W6;->TWO:LX/0W6;

    aput-object v1, v0, v5

    sget-object v1, LX/0W6;->FEW:LX/0W6;

    aput-object v1, v0, v6

    sget-object v1, LX/0W6;->MANY:LX/0W6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0W6;->OTHER:LX/0W6;

    aput-object v2, v0, v1

    sput-object v0, LX/0W6;->map:[LX/0W6;

    .line 75481
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "zero"

    sget-object v2, LX/0W6;->ZERO:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "one"

    sget-object v2, LX/0W6;->ONE:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "two"

    sget-object v2, LX/0W6;->TWO:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "few"

    sget-object v2, LX/0W6;->FEW:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "many"

    sget-object v2, LX/0W6;->MANY:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "other"

    sget-object v2, LX/0W6;->OTHER:LX/0W6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/0W6;->STRING_TO_PLURAL_CATEGORY:LX/0P1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromFakeText(Ljava/lang/CharSequence;)LX/0W6;
    .locals 2

    .prologue
    .line 75454
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    .line 75455
    if-ltz v0, :cond_0

    sget-object v1, LX/0W6;->map:[LX/0W6;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 75456
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Requesting a PluralCategory that does not exists"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75457
    :cond_1
    sget-object v1, LX/0W6;->map:[LX/0W6;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public static of(I)LX/0W6;
    .locals 3

    .prologue
    .line 75463
    packed-switch p0, :pswitch_data_0

    .line 75464
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No plural category for code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75465
    :pswitch_0
    sget-object v0, LX/0W6;->ZERO:LX/0W6;

    .line 75466
    :goto_0
    return-object v0

    .line 75467
    :pswitch_1
    sget-object v0, LX/0W6;->ONE:LX/0W6;

    goto :goto_0

    .line 75468
    :pswitch_2
    sget-object v0, LX/0W6;->TWO:LX/0W6;

    goto :goto_0

    .line 75469
    :pswitch_3
    sget-object v0, LX/0W6;->FEW:LX/0W6;

    goto :goto_0

    .line 75470
    :pswitch_4
    sget-object v0, LX/0W6;->MANY:LX/0W6;

    goto :goto_0

    .line 75471
    :pswitch_5
    sget-object v0, LX/0W6;->OTHER:LX/0W6;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static of(Ljava/lang/String;)LX/0W6;
    .locals 3

    .prologue
    .line 75460
    sget-object v0, LX/0W6;->STRING_TO_PLURAL_CATEGORY:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75461
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No plural category for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75462
    :cond_0
    sget-object v0, LX/0W6;->STRING_TO_PLURAL_CATEGORY:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W6;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0W6;
    .locals 1

    .prologue
    .line 75459
    const-class v0, LX/0W6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0W6;

    return-object v0
.end method

.method public static values()[LX/0W6;
    .locals 1

    .prologue
    .line 75458
    sget-object v0, LX/0W6;->$VALUES:[LX/0W6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0W6;

    return-object v0
.end method
