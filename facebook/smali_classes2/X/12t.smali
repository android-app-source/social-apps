.class public final LX/12t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/0W6;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/0W6;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175786
    iput-object p1, p0, LX/12t;->a:LX/0P1;

    .line 175787
    return-void
.end method


# virtual methods
.method public final a(LX/0W6;)Z
    .locals 1

    .prologue
    .line 175788
    iget-object v0, p0, LX/12t;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/0W6;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 175789
    iget-object v0, p0, LX/12t;->a:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 175790
    instance-of v0, p1, LX/12t;

    if-eqz v0, :cond_0

    check-cast p1, LX/12t;

    .line 175791
    iget-object v0, p0, LX/12t;->a:LX/0P1;

    iget-object v1, p1, LX/12t;->a:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 175792
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
