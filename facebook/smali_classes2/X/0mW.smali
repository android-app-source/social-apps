.class public LX/0mW;
.super LX/0Q6;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ProviderUsage"
    }
.end annotation

.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132600
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 132601
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0mI;LX/0mI;LX/0Or;LX/0Ot;LX/0mM;LX/0mO;LX/0mP;Ljava/lang/Class;LX/0Ot;LX/0mR;LX/0Ot;Ljava/lang/Class;LX/0Ot;LX/0mU;LX/40D;)LX/0mh;
    .locals 6
    .param p1    # LX/0mI;
        .annotation runtime Lcom/facebook/analytics2/loggermodule/NormalPriEventListener;
        .end annotation
    .end param
    .param p2    # LX/0mI;
        .annotation runtime Lcom/facebook/analytics2/loggermodule/HighPriEventListener;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/session/AnalyticsBackgroundState;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/analytics2/loggermodule/SamplingPolicyConfigClass;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/analytics2/loggermodule/HandlerThreadFactoryClass;
        .end annotation
    .end param
    .param p14    # LX/0mU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/40D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0mI;",
            "LX/0mI;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "LX/0mM;",
            "Lcom/facebook/analytics2/logger/ProcessPolicy;",
            "Lcom/facebook/flexiblesampling/SamplingPolicy;",
            "Ljava/lang/Class;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0mR;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "Ljava/lang/Class;",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "Lcom/facebook/analytics2/logger/BeginWritingBlock;",
            "Lcom/facebook/analytics2/logger/Analytics2EventSchemaValidationManager;",
            ")",
            "LX/0mh;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 132602
    new-instance v1, LX/0mX;

    invoke-direct {v1}, LX/0mX;-><init>()V

    .line 132603
    new-instance v2, LX/0mX;

    invoke-direct {v2}, LX/0mX;-><init>()V

    .line 132604
    invoke-virtual {v1, p1}, LX/0mX;->a(LX/0mI;)V

    .line 132605
    invoke-virtual {v2, p2}, LX/0mX;->a(LX/0mI;)V

    .line 132606
    new-instance v3, LX/0mZ;

    invoke-direct {v3, p0}, LX/0mZ;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, LX/0mZ;->a(LX/0mI;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0mZ;->b(LX/0mI;)LX/0mZ;

    move-result-object v3

    new-instance v4, LX/0ma;

    invoke-direct {v4, p3}, LX/0ma;-><init>(LX/0Or;)V

    invoke-virtual {v3, v4}, LX/0mZ;->a(LX/0ma;)LX/0mZ;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, LX/0mZ;->a(LX/0mR;)LX/0mZ;

    move-result-object v3

    new-instance v4, LX/0mb;

    invoke-direct {v4, p4}, LX/0mb;-><init>(LX/0Ot;)V

    invoke-virtual {v3, v4}, LX/0mZ;->a(LX/0mb;)LX/0mZ;

    move-result-object v3

    new-instance v4, LX/0mc;

    move-object/from16 v0, p11

    invoke-direct {v4, v0}, LX/0mc;-><init>(LX/0Ot;)V

    invoke-virtual {v3, v4}, LX/0mZ;->a(LX/0mc;)LX/0mZ;

    move-result-object v3

    new-instance v4, LX/0md;

    move-object/from16 v0, p13

    invoke-direct {v4, v0, p9}, LX/0md;-><init>(LX/0Ot;LX/0Ot;)V

    invoke-virtual {v3, v4}, LX/0mZ;->a(LX/0me;)LX/0mZ;

    move-result-object v3

    new-instance v4, LX/0mf;

    move-object/from16 v0, p13

    invoke-direct {v4, v0, p9}, LX/0mf;-><init>(LX/0Ot;LX/0Ot;)V

    invoke-virtual {v3, v4}, LX/0mZ;->a(LX/0mg;)LX/0mZ;

    move-result-object v3

    const-class v4, Lcom/facebook/analytics2/uploader/fbhttp/FbHttpUploader;

    invoke-virtual {v3, v4}, LX/0mZ;->a(Ljava/lang/Class;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/0mZ;->a(LX/0mM;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/0mZ;->a(LX/0mO;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/0mZ;->a(LX/0mP;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3, p8}, LX/0mZ;->b(Ljava/lang/Class;)LX/0mZ;

    move-result-object v3

    move-object/from16 v0, p12

    invoke-virtual {v3, v0}, LX/0mZ;->c(Ljava/lang/Class;)LX/0mZ;

    move-result-object v3

    move-object/from16 v0, p14

    invoke-virtual {v3, v0}, LX/0mZ;->a(LX/0mU;)LX/0mZ;

    move-result-object v3

    move-object/from16 v0, p15

    invoke-virtual {v3, v0}, LX/0mZ;->a(LX/40D;)LX/0mZ;

    move-result-object v3

    invoke-virtual {v3}, LX/0mZ;->a()LX/0mh;

    move-result-object v3

    .line 132607
    new-instance v4, LX/0mo;

    invoke-static {p0, v3}, LX/0mp;->a(Landroid/content/Context;LX/0mh;)LX/0mp;

    move-result-object v5

    invoke-direct {v4, v5}, LX/0mo;-><init>(LX/0mp;)V

    invoke-virtual {v1, v4}, LX/0mX;->a(LX/0mI;)V

    .line 132608
    new-instance v1, LX/0mo;

    invoke-static {p0, v3}, LX/0mp;->b(Landroid/content/Context;LX/0mh;)LX/0mp;

    move-result-object v4

    invoke-direct {v1, v4}, LX/0mo;-><init>(LX/0mp;)V

    invoke-virtual {v2, v1}, LX/0mX;->a(LX/0mI;)V

    .line 132609
    return-object v3
.end method

.method public static a(Landroid/content/Context;LX/0Uh;)LX/40D;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 132610
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 132611
    if-nez v1, :cond_1

    .line 132612
    :cond_0
    :goto_0
    return-object v0

    .line 132613
    :cond_1
    invoke-static {}, LX/0i9;->a()Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v1, 0x63

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132614
    :cond_2
    new-instance v0, LX/40D;

    new-instance v1, LX/4lQ;

    invoke-direct {v1, p0}, LX/4lQ;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/40D;-><init>(LX/4lQ;)V

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 132615
    return-void
.end method
