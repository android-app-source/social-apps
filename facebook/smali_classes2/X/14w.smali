.class public LX/14w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile j:LX/14w;


# instance fields
.field public final b:LX/14x;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/153;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/03V;

.field private g:LX/0ad;

.field private h:LX/0Uh;

.field public i:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179612
    const-class v0, LX/14w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/14w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14x;LX/0Or;LX/153;LX/0Or;LX/03V;LX/0ad;LX/0Uh;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14x;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/153;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 179613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179614
    iput-object p1, p0, LX/14w;->b:LX/14x;

    .line 179615
    iput-object p2, p0, LX/14w;->c:LX/0Or;

    .line 179616
    iput-object p3, p0, LX/14w;->d:LX/153;

    .line 179617
    iput-object p4, p0, LX/14w;->e:LX/0Or;

    .line 179618
    iput-object p5, p0, LX/14w;->f:LX/03V;

    .line 179619
    iput-object p6, p0, LX/14w;->g:LX/0ad;

    .line 179620
    iput-object p7, p0, LX/14w;->h:LX/0Uh;

    .line 179621
    return-void
.end method

.method public static a(LX/0QB;)LX/14w;
    .locals 11

    .prologue
    .line 179622
    sget-object v0, LX/14w;->j:LX/14w;

    if-nez v0, :cond_1

    .line 179623
    const-class v1, LX/14w;

    monitor-enter v1

    .line 179624
    :try_start_0
    sget-object v0, LX/14w;->j:LX/14w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 179625
    if-eqz v2, :cond_0

    .line 179626
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 179627
    new-instance v3, LX/14w;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v4

    check-cast v4, LX/14x;

    const/16 v5, 0x19e

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/153;->b(LX/0QB;)LX/153;

    move-result-object v6

    check-cast v6, LX/153;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, LX/14w;-><init>(LX/14x;LX/0Or;LX/153;LX/0Or;LX/03V;LX/0ad;LX/0Uh;)V

    .line 179628
    move-object v0, v3

    .line 179629
    sput-object v0, LX/14w;->j:LX/14w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179630
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 179631
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 179632
    :cond_1
    sget-object v0, LX/14w;->j:LX/14w;

    return-object v0

    .line 179633
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 179634
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/17E;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/6XF;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, -0x4dba1a9d

    aput v2, v1, v3

    invoke-static {v0, v1}, LX/14w;->a(LX/6XF;[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179636
    :cond_0
    const/4 v0, 0x0

    .line 179637
    :goto_0
    return-object v0

    .line 179638
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 179639
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 179640
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179641
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 179642
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/1WQ;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 179643
    invoke-interface {p0}, LX/1WQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1WQ;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-interface {p0}, LX/1WQ;->b()LX/6XH;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1WQ;->b()LX/6XH;

    move-result-object v0

    invoke-interface {v0}, LX/6XH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 179644
    :goto_0
    return v0

    .line 179645
    :cond_1
    invoke-interface {p0}, LX/1WQ;->b()LX/6XH;

    move-result-object v0

    invoke-interface {v0}, LX/6XH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6XG;

    .line 179646
    invoke-interface {v0}, LX/6XG;->b()LX/0Px;

    move-result-object v0

    .line 179647
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 179648
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v5

    .line 179649
    invoke-interface {p0}, LX/1WQ;->b()LX/6XH;

    move-result-object v0

    invoke-interface {v0}, LX/6XH;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    .line 179650
    :goto_1
    if-ge v4, v7, :cond_5

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6XG;

    .line 179651
    invoke-interface {v0}, LX/6XG;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v3, :cond_2

    invoke-interface {v0}, LX/6XG;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel$AllSubstoriesModel$NodesModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v5, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/6XG;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6XF;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, LX/14w;->a(LX/6XF;[I)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move v0, v2

    .line 179652
    goto :goto_0

    :cond_3
    move v0, v2

    .line 179653
    goto :goto_0

    .line 179654
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v0, v3

    .line 179655
    goto :goto_0

    nop

    :array_0
    .array-data 4
        -0x658b3fa6
        -0x4dba1a9d
        0x25d6af
    .end array-data
.end method

.method public static varargs a(LX/6XF;[I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 179656
    invoke-interface {p0}, LX/6XF;->a()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/6XF;->a()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 179657
    :cond_0
    :goto_0
    return v0

    .line 179658
    :cond_1
    invoke-interface {p0}, LX/6XF;->a()Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel$TargetModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    .line 179659
    array-length v3, p1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, p1, v1

    .line 179660
    if-ne v2, v4, :cond_2

    .line 179661
    const/4 v0, 0x1

    goto :goto_0

    .line 179662
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179663
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 179664
    if-ne p0, p1, :cond_0

    .line 179665
    const/4 v0, 0x1

    .line 179666
    :goto_0
    return v0

    .line 179667
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 179668
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 179669
    :cond_2
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    .line 179670
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    .line 179671
    :cond_3
    invoke-interface {p0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179672
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179673
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179674
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179675
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 179676
    move-object v1, v0

    .line 179677
    :goto_0
    if-eqz v1, :cond_2

    .line 179678
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179679
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 179680
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 179682
    const/4 v0, 0x1

    .line 179683
    :goto_1
    move v0, v0

    .line 179684
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 179685
    :cond_1
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 179686
    move-object v1, v0

    goto :goto_0

    .line 179687
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 179494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179688
    if-eqz p0, :cond_0

    .line 179689
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179690
    if-eqz v0, :cond_0

    .line 179691
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179692
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179693
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179694
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179695
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179696
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->S()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179761
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179762
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179763
    invoke-static {p0}, LX/14w;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/14w;->f(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179697
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179698
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179699
    if-nez v0, :cond_0

    move v0, v1

    .line 179700
    :goto_0
    return v0

    .line 179701
    :cond_0
    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 179702
    goto :goto_0

    .line 179703
    :cond_1
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 179704
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    .line 179705
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179706
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 179707
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179708
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179709
    :goto_0
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 179710
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 179711
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    goto :goto_0

    .line 179712
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 179713
    :cond_1
    const/4 v0, 0x0

    .line 179714
    :cond_2
    return-object v0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 179715
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 179716
    invoke-static {p0}, LX/14w;->u(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v0

    .line 179717
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 179718
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 179719
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 179720
    if-eqz v1, :cond_2

    .line 179721
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_4

    const/4 p0, 0x1

    :goto_2
    move p0, p0

    .line 179722
    if-eqz p0, :cond_2

    .line 179723
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 179724
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 179725
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 179726
    goto :goto_0

    :cond_4
    const/4 p0, 0x0

    goto :goto_2
.end method

.method public static h(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 179727
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179728
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179729
    if-nez v0, :cond_0

    move v0, v1

    .line 179730
    :goto_0
    return v0

    .line 179731
    :cond_0
    invoke-static {p0}, LX/14w;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v0

    .line 179732
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static j(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 179733
    invoke-static {p0}, LX/0x1;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    .line 179734
    if-nez v0, :cond_0

    move v0, v1

    .line 179735
    :goto_0
    return v0

    .line 179736
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->j()LX/0Px;

    move-result-object v0

    .line 179737
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 179738
    goto :goto_0

    .line 179739
    :cond_2
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->a()LX/0Px;

    move-result-object v0

    .line 179740
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 179741
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179742
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179743
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 179744
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 179745
    :cond_0
    :goto_0
    return v2

    .line 179746
    :cond_1
    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v5

    move v1, v2

    .line 179747
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 179748
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v0, :cond_2

    move v2, v4

    .line 179749
    goto :goto_0

    .line 179750
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v3, v2

    .line 179751
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 179752
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v2, v4

    .line 179753
    goto :goto_0

    .line 179754
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2
.end method

.method public static n(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179755
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 179756
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 179757
    invoke-static {p0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179758
    :cond_0
    :goto_0
    return v0

    .line 179759
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 179760
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final o(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 179603
    invoke-static {p0}, LX/14w;->p(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 179604
    invoke-static {p0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 179605
    :goto_0
    return-object v0

    .line 179606
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 179607
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 179608
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179609
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto :goto_0

    .line 179610
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 179611
    goto :goto_0
.end method

.method public static final p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 179416
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179417
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179418
    invoke-static {p0}, LX/14w;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179419
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179420
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179421
    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 179422
    invoke-static {p0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 179423
    :goto_0
    return-object v0

    .line 179424
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 179425
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 179426
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179427
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto :goto_0

    .line 179428
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 179429
    goto :goto_0
.end method

.method public static r(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179430
    invoke-static {p0}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 179431
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 179432
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_1
    return-object v0

    .line 179433
    :cond_0
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179434
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 179435
    goto :goto_1
.end method

.method public static r(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 179436
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x41e065f

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 179437
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179438
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179439
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v2

    move-object v1, v0

    .line 179440
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179441
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 179442
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_0

    .line 179443
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v0

    .line 179444
    goto :goto_0

    .line 179445
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 179446
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    .line 179447
    :goto_1
    return-object v0

    .line 179448
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 179449
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 179450
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 179451
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static s(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 179452
    if-nez p0, :cond_1

    .line 179453
    :cond_0
    :goto_0
    return v0

    .line 179454
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    .line 179455
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->m()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;->FACEBOOK_VOICE:Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    const/4 v4, 0x0

    .line 179456
    if-nez v1, :cond_2

    move v3, v4

    .line 179457
    :goto_1
    move v1, v3

    .line 179458
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 179459
    :cond_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    move v5, v4

    :goto_2
    if-ge v5, p0, :cond_4

    .line 179460
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLStoryHeaderStyle;

    .line 179461
    if-ne v3, v2, :cond_3

    .line 179462
    const/4 v3, 0x1

    goto :goto_1

    .line 179463
    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_4
    move v3, v4

    .line 179464
    goto :goto_1
.end method

.method public static t(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLSponsoredData;"
        }
    .end annotation

    .prologue
    .line 179465
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179466
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179467
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    move-object v3, v0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 179468
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 179469
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179470
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v3, v0

    goto :goto_0

    .line 179471
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    return-object v0
.end method

.method public static t(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 179472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v0

    const v1, -0x452d50ee

    invoke-static {v0, v1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 179473
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179474
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179475
    if-nez v0, :cond_0

    move-object v0, v1

    .line 179476
    :goto_0
    return-object v0

    .line 179477
    :cond_0
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 179478
    if-eqz v0, :cond_2

    invoke-static {p0}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {v2}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179479
    :goto_1
    move-object v0, v0

    .line 179480
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 179481
    goto :goto_0

    .line 179482
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->n()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 179483
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179484
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_1
.end method

.method public static w(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 179485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/153;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/0Px",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 179486
    invoke-static {p1}, LX/14w;->w(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 179487
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 179488
    instance-of p1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz p1, :cond_1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->w(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179489
    const/4 v0, 0x1

    .line 179490
    :goto_1
    move v0, v0

    .line 179491
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 179492
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 179493
    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179495
    const/4 v0, 0x0

    .line 179496
    if-eqz p1, :cond_0

    .line 179497
    iget-object v1, p0, LX/14w;->d:LX/153;

    invoke-virtual {v1, p1}, LX/153;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v1

    .line 179498
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 179499
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 179500
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 179501
    move-object v0, v0

    .line 179502
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 179503
    :cond_0
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 179504
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/14w;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179505
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 179506
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179507
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 179508
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 179509
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/14w;->h(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 179510
    :cond_1
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 179511
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179512
    invoke-virtual {p0, v0}, LX/14w;->h(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 179513
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 179514
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179515
    if-nez p1, :cond_0

    move v0, v1

    .line 179516
    :goto_0
    return v0

    .line 179517
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179518
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 179519
    goto :goto_0

    .line 179520
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179521
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/14w;->h:LX/0Uh;

    const/16 v3, 0xd

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 179522
    goto :goto_0

    .line 179523
    :cond_2
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179524
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 179525
    goto :goto_0

    .line 179526
    :cond_3
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179527
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 179528
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179529
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    .line 179530
    goto :goto_0

    .line 179531
    :cond_4
    iget-object v0, p0, LX/14w;->g:LX/0ad;

    sget-short v2, LX/0wi;->b:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final k(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179532
    if-nez p1, :cond_1

    .line 179533
    :cond_0
    :goto_0
    return v1

    .line 179534
    :cond_1
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 179535
    if-eqz v3, :cond_0

    .line 179536
    invoke-virtual {p0, v3}, LX/14w;->j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179537
    invoke-static {v3}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 179538
    :goto_1
    invoke-virtual {p0, v3}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v3

    if-eqz v0, :cond_5

    :goto_2
    add-int/2addr v1, v3

    goto :goto_0

    .line 179539
    :cond_2
    invoke-static {p1}, LX/14w;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v3}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v1, v2

    .line 179540
    goto :goto_2
.end method

.method public final l(Lcom/facebook/graphql/model/GraphQLStory;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179541
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/14w;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final m(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 179542
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 179543
    invoke-static {p1}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 179544
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179545
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 179546
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v5

    .line 179547
    if-eqz v4, :cond_0

    if-nez v1, :cond_2

    .line 179548
    :cond_0
    invoke-static {p1}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 179549
    const-string v4, "%s parent:%s, url:%s mediaUrl:%s title:%s subtitle:%s dedupkey:%s"

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, LX/14w;->a:Ljava/lang/String;

    aput-object v6, v5, v3

    if-eqz v1, :cond_1

    .line 179550
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v6

    .line 179551
    :goto_0
    aput-object v1, v5, v2

    const/4 v1, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x3

    invoke-static {v0}, LX/1VO;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x5

    invoke-static {v0}, LX/1VO;->y(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NPE of attachment story"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 179552
    iput-boolean v2, v0, LX/0VK;->f:Z

    .line 179553
    move-object v0, v0

    .line 179554
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 179555
    iget-object v1, p0, LX/14w;->f:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    move v0, v3

    .line 179556
    :goto_1
    return v0

    .line 179557
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 179558
    :cond_2
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->QUOTED_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v6}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v6

    move v0, v6

    .line 179559
    if-eqz v0, :cond_3

    move v0, v2

    .line 179560
    goto :goto_1

    .line 179561
    :cond_3
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 179562
    invoke-interface {v6, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 179563
    :goto_2
    invoke-interface {v6}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 179564
    invoke-interface {v6}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 179565
    invoke-static {v0, v1}, LX/14w;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    .line 179566
    goto :goto_1

    .line 179567
    :cond_4
    const/4 v7, 0x0

    .line 179568
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v9

    .line 179569
    if-eqz v9, :cond_5

    if-nez v5, :cond_9

    :cond_5
    move v4, v7

    .line 179570
    :goto_3
    move v4, v4

    .line 179571
    if-eqz v4, :cond_6

    move v0, v3

    .line 179572
    goto :goto_1

    .line 179573
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 179574
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 179575
    :cond_7
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 179576
    :cond_8
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Something must be wrong with the attachmentProps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179577
    :cond_9
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result p0

    move v8, v7

    :goto_4
    if-ge v8, p0, :cond_b

    .line 179578
    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 179579
    const/4 v4, 0x1

    goto :goto_3

    .line 179580
    :cond_a
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_4

    :cond_b
    move v4, v7

    .line 179581
    goto :goto_3
.end method

.method public final o(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 179582
    if-nez p1, :cond_0

    .line 179583
    const/4 v0, -0x1

    .line 179584
    :goto_0
    return v0

    .line 179585
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 179586
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 179587
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 179588
    invoke-virtual {p0, p1}, LX/14w;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    goto :goto_0

    .line 179589
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 179590
    invoke-static {p1}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 179591
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GAMES_INSTANT_PLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v3, :cond_7

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 179592
    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 179593
    :goto_1
    return v0

    .line 179594
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 179595
    iget-object v2, p0, LX/14w;->i:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 179596
    iget-object v2, p0, LX/14w;->b:LX/14x;

    invoke-virtual {v2}, LX/14x;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, LX/14w;->i:Ljava/lang/Boolean;

    .line 179597
    :cond_2
    iget-object v2, p0, LX/14w;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 179598
    if-nez v2, :cond_3

    iget-object v2, p0, LX/14w;->h:LX/0Uh;

    const/16 v3, 0x78

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, LX/14w;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 179599
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 179600
    if-eqz v0, :cond_6

    :cond_5
    move v0, v1

    .line 179601
    goto :goto_1

    .line 179602
    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_0
.end method
