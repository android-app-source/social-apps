.class public final enum LX/1Wi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Wi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Wi;

.field public static final enum COMPACT_STORIES:LX/1Wi;

.field public static final enum HAS_FOLLOWUP_SECTION:LX/1Wi;

.field public static final enum HAS_INLINE_COMMENTS:LX/1Wi;

.field public static final enum HAS_INLINE_SURVEY:LX/1Wi;

.field public static final enum LAST_SUB_STORY:LX/1Wi;

.field public static final enum PAGE:LX/1Wi;

.field public static final enum PAGE_FOUR_BUTTON:LX/1Wi;

.field public static final enum PERMALINK:LX/1Wi;

.field public static final enum PHOTOS_FEED:LX/1Wi;

.field public static final enum SUB_STORY:LX/1Wi;

.field public static final enum SUB_STORY_BOX_WITHOUT_COMMENTS:LX/1Wi;

.field public static final enum SUB_STORY_BOX_WITH_COMMENTS:LX/1Wi;

.field public static final enum TOP:LX/1Wi;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 269367
    new-instance v0, LX/1Wi;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->TOP:LX/1Wi;

    .line 269368
    new-instance v0, LX/1Wi;

    const-string v1, "SUB_STORY"

    invoke-direct {v0, v1, v4}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->SUB_STORY:LX/1Wi;

    .line 269369
    new-instance v0, LX/1Wi;

    const-string v1, "LAST_SUB_STORY"

    invoke-direct {v0, v1, v5}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->LAST_SUB_STORY:LX/1Wi;

    .line 269370
    new-instance v0, LX/1Wi;

    const-string v1, "PERMALINK"

    invoke-direct {v0, v1, v6}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->PERMALINK:LX/1Wi;

    .line 269371
    new-instance v0, LX/1Wi;

    const-string v1, "HAS_INLINE_COMMENTS"

    invoke-direct {v0, v1, v7}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->HAS_INLINE_COMMENTS:LX/1Wi;

    .line 269372
    new-instance v0, LX/1Wi;

    const-string v1, "HAS_INLINE_SURVEY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->HAS_INLINE_SURVEY:LX/1Wi;

    .line 269373
    new-instance v0, LX/1Wi;

    const-string v1, "HAS_FOLLOWUP_SECTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->HAS_FOLLOWUP_SECTION:LX/1Wi;

    .line 269374
    new-instance v0, LX/1Wi;

    const-string v1, "PHOTOS_FEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->PHOTOS_FEED:LX/1Wi;

    .line 269375
    new-instance v0, LX/1Wi;

    const-string v1, "SUB_STORY_BOX_WITH_COMMENTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->SUB_STORY_BOX_WITH_COMMENTS:LX/1Wi;

    .line 269376
    new-instance v0, LX/1Wi;

    const-string v1, "SUB_STORY_BOX_WITHOUT_COMMENTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->SUB_STORY_BOX_WITHOUT_COMMENTS:LX/1Wi;

    .line 269377
    new-instance v0, LX/1Wi;

    const-string v1, "PAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->PAGE:LX/1Wi;

    .line 269378
    new-instance v0, LX/1Wi;

    const-string v1, "PAGE_FOUR_BUTTON"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->PAGE_FOUR_BUTTON:LX/1Wi;

    .line 269379
    new-instance v0, LX/1Wi;

    const-string v1, "COMPACT_STORIES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/1Wi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Wi;->COMPACT_STORIES:LX/1Wi;

    .line 269380
    const/16 v0, 0xd

    new-array v0, v0, [LX/1Wi;

    sget-object v1, LX/1Wi;->TOP:LX/1Wi;

    aput-object v1, v0, v3

    sget-object v1, LX/1Wi;->SUB_STORY:LX/1Wi;

    aput-object v1, v0, v4

    sget-object v1, LX/1Wi;->LAST_SUB_STORY:LX/1Wi;

    aput-object v1, v0, v5

    sget-object v1, LX/1Wi;->PERMALINK:LX/1Wi;

    aput-object v1, v0, v6

    sget-object v1, LX/1Wi;->HAS_INLINE_COMMENTS:LX/1Wi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1Wi;->HAS_INLINE_SURVEY:LX/1Wi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1Wi;->HAS_FOLLOWUP_SECTION:LX/1Wi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1Wi;->PHOTOS_FEED:LX/1Wi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1Wi;->SUB_STORY_BOX_WITH_COMMENTS:LX/1Wi;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1Wi;->SUB_STORY_BOX_WITHOUT_COMMENTS:LX/1Wi;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1Wi;->PAGE:LX/1Wi;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1Wi;->PAGE_FOUR_BUTTON:LX/1Wi;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1Wi;->COMPACT_STORIES:LX/1Wi;

    aput-object v2, v0, v1

    sput-object v0, LX/1Wi;->$VALUES:[LX/1Wi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 269366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Wi;
    .locals 1

    .prologue
    .line 269365
    const-class v0, LX/1Wi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Wi;

    return-object v0
.end method

.method public static values()[LX/1Wi;
    .locals 1

    .prologue
    .line 269364
    sget-object v0, LX/1Wi;->$VALUES:[LX/1Wi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Wi;

    return-object v0
.end method
