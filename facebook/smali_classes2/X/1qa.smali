.class public LX/1qa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1VL;

.field private final c:LX/0rq;

.field private final d:LX/1qb;

.field private e:LX/0sd;


# direct methods
.method public constructor <init>(LX/0Or;LX/1VL;LX/0rq;LX/1qb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0sd;",
            ">;",
            "LX/1VL;",
            "LX/0rq;",
            "LX/1qb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331144
    iput-object p1, p0, LX/1qa;->a:LX/0Or;

    .line 331145
    iput-object p2, p0, LX/1qa;->b:LX/1VL;

    .line 331146
    iput-object p3, p0, LX/1qa;->c:LX/0rq;

    .line 331147
    invoke-virtual {p0}, LX/1qa;->a()V

    .line 331148
    iput-object p4, p0, LX/1qa;->d:LX/1qb;

    .line 331149
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;
    .locals 4

    .prologue
    .line 331133
    if-eqz p0, :cond_1

    .line 331134
    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 331135
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    if-lez v1, :cond_0

    .line 331136
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 331137
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 331138
    move-object v0, v0

    .line 331139
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 331140
    :goto_0
    return-object v0

    .line 331141
    :cond_0
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 331142
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1qa;
    .locals 7

    .prologue
    .line 331122
    const-class v1, LX/1qa;

    monitor-enter v1

    .line 331123
    :try_start_0
    sget-object v0, LX/1qa;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 331124
    sput-object v2, LX/1qa;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 331125
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331126
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 331127
    new-instance v6, LX/1qa;

    const/16 v3, 0x1224

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v3

    check-cast v3, LX/1VL;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v5

    check-cast v5, LX/1qb;

    invoke-direct {v6, p0, v3, v4, v5}, LX/1qa;-><init>(LX/0Or;LX/1VL;LX/0rq;LX/1qb;)V

    .line 331128
    move-object v0, v6

    .line 331129
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 331130
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331131
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 331132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1qa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 4

    .prologue
    .line 331114
    iget-object v0, p0, LX/1qa;->b:LX/1VL;

    invoke-virtual {v0, p1, p2}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    .line 331115
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    sget-object v2, LX/26P;->Album:LX/26P;

    invoke-virtual {p0, v1, v0, v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331116
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 331117
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eq v0, v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-ne v0, v2, :cond_1

    .line 331118
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331119
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {p2}, LX/1VL;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v2

    iget-object v3, p0, LX/1qa;->b:LX/1VL;

    invoke-virtual {v3, p1}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 331120
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331121
    :cond_2
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 331107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331108
    :goto_0
    return-object v0

    .line 331109
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p1, :cond_1

    .line 331110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 331111
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p1, :cond_2

    .line 331112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 331113
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLImage;)Z
    .locals 2

    .prologue
    .line 331150
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/1qa;->d:LX/1qb;

    const v1, 0x3ff745d1

    invoke-virtual {v0, p1, v1}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/26P;)I
    .locals 2

    .prologue
    .line 331095
    sget-object v0, LX/Ajm;->a:[I

    invoke-virtual {p1}, LX/26P;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 331096
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 331097
    :pswitch_0
    iget-object v0, p0, LX/1qa;->e:LX/0sd;

    .line 331098
    iget v1, v0, LX/0sd;->b:I

    move v0, v1

    .line 331099
    goto :goto_0

    .line 331100
    :pswitch_1
    iget-object v0, p0, LX/1qa;->e:LX/0sd;

    .line 331101
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 331102
    goto :goto_0

    .line 331103
    :pswitch_2
    iget-object v0, p0, LX/1qa;->e:LX/0sd;

    .line 331104
    iget v1, v0, LX/0sd;->a:I

    move v0, v1

    .line 331105
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/1bf;"
        }
    .end annotation

    .prologue
    .line 331092
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 331093
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0, v0, p2}, LX/1qa;->a(LX/1qa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331094
    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;
    .locals 1

    .prologue
    .line 331090
    invoke-virtual {p0, p1, p2}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331091
    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .param p3    # LX/26P;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331078
    if-nez p1, :cond_1

    .line 331079
    const/4 v0, 0x0

    .line 331080
    :cond_0
    :goto_0
    return-object v0

    .line 331081
    :cond_1
    sget-object v0, LX/26P;->Share:LX/26P;

    if-ne p3, v0, :cond_4

    .line 331082
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331083
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 331084
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 331085
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 331086
    :cond_3
    iget-object v0, p0, LX/1qa;->e:LX/0sd;

    .line 331087
    iget p0, v0, LX/0sd;->a:I

    move p2, p0

    .line 331088
    :cond_4
    invoke-static {p1, p2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 331089
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 331076
    iget-object v0, p0, LX/1qa;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sd;

    iput-object v0, p0, LX/1qa;->e:LX/0sd;

    .line 331077
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331075
    invoke-virtual {p0, p2}, LX/1qa;->a(LX/26P;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method
