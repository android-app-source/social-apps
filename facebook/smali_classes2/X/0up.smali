.class public LX/0up;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

.field private final b:LX/0uk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0uk",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/0uk;)V
    .locals 0
    .param p2    # LX/0uk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/prefetch/GraphQLPrefetchController;",
            "LX/0uk",
            "<TT;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 157150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157151
    iput-object p1, p0, LX/0up;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    .line 157152
    iput-object p2, p0, LX/0up;->b:LX/0uk;

    .line 157153
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0TF;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157154
    iget-object v0, p0, LX/0up;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/0up;->b:LX/0uk;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;Ljava/lang/String;LX/0TF;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 157155
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "LX/6BE",
            "<TT;>;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157156
    iget-object v0, p0, LX/0up;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, p0, LX/0up;->b:LX/0uk;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;Ljava/lang/String;LX/0TF;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/concurrent/Executor;)V

    .line 157157
    return-void
.end method
