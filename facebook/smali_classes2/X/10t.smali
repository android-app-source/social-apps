.class public LX/10t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/10t;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Uq;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GQW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uq;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Uq;",
            "LX/0Ot",
            "<",
            "LX/GQW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169459
    iput-object p1, p0, LX/10t;->a:LX/0Or;

    .line 169460
    iput-object p2, p0, LX/10t;->b:LX/0Uq;

    .line 169461
    iput-object p3, p0, LX/10t;->c:LX/0Ot;

    .line 169462
    return-void
.end method

.method public static a(LX/0QB;)LX/10t;
    .locals 6

    .prologue
    .line 169463
    sget-object v0, LX/10t;->d:LX/10t;

    if-nez v0, :cond_1

    .line 169464
    const-class v1, LX/10t;

    monitor-enter v1

    .line 169465
    :try_start_0
    sget-object v0, LX/10t;->d:LX/10t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169466
    if-eqz v2, :cond_0

    .line 169467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169468
    new-instance v4, LX/10t;

    const/16 v3, 0x1439

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v3

    check-cast v3, LX/0Uq;

    const/16 p0, 0x1740

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v3, p0}, LX/10t;-><init>(LX/0Or;LX/0Uq;LX/0Ot;)V

    .line 169469
    move-object v0, v4

    .line 169470
    sput-object v0, LX/10t;->d:LX/10t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169471
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169472
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169473
    :cond_1
    sget-object v0, LX/10t;->d:LX/10t;

    return-object v0

    .line 169474
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
