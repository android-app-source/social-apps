.class public final enum LX/0gH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0gH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0gH;

.field public static final enum HAS_LEFT_DIVEBAR:LX/0gH;

.field public static final enum NO_LEFT_DIVEBAR:LX/0gH;

.field public static final enum OPEN_TO_LEFT_DIVEBAR:LX/0gH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111815
    new-instance v0, LX/0gH;

    const-string v1, "HAS_LEFT_DIVEBAR"

    invoke-direct {v0, v1, v2}, LX/0gH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    .line 111816
    new-instance v0, LX/0gH;

    const-string v1, "OPEN_TO_LEFT_DIVEBAR"

    invoke-direct {v0, v1, v3}, LX/0gH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    .line 111817
    new-instance v0, LX/0gH;

    const-string v1, "NO_LEFT_DIVEBAR"

    invoke-direct {v0, v1, v4}, LX/0gH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    .line 111818
    const/4 v0, 0x3

    new-array v0, v0, [LX/0gH;

    sget-object v1, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    aput-object v1, v0, v2

    sget-object v1, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    aput-object v1, v0, v3

    sget-object v1, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    aput-object v1, v0, v4

    sput-object v0, LX/0gH;->$VALUES:[LX/0gH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 111819
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0gH;
    .locals 1

    .prologue
    .line 111820
    const-class v0, LX/0gH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0gH;

    return-object v0
.end method

.method public static values()[LX/0gH;
    .locals 1

    .prologue
    .line 111821
    sget-object v0, LX/0gH;->$VALUES:[LX/0gH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0gH;

    return-object v0
.end method
