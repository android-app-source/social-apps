.class public LX/1S9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final i:Ljava/lang/String;

.field private static volatile m:LX/1S9;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1S5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Wd;
    .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuStore$Callback;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2tX;

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247734
    const-class v0, LX/1S9;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1S9;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247736
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/1S9;->j:Ljava/util/Set;

    .line 247737
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1S9;->l:Z

    .line 247738
    return-void
.end method

.method public static a(LX/0QB;)LX/1S9;
    .locals 11

    .prologue
    .line 247739
    sget-object v0, LX/1S9;->m:LX/1S9;

    if-nez v0, :cond_1

    .line 247740
    const-class v1, LX/1S9;

    monitor-enter v1

    .line 247741
    :try_start_0
    sget-object v0, LX/1S9;->m:LX/1S9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 247742
    if-eqz v2, :cond_0

    .line 247743
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 247744
    new-instance v3, LX/1S9;

    invoke-direct {v3}, LX/1S9;-><init>()V

    .line 247745
    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v0}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->b(LX/0QB;)Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-static {v0}, LX/1S5;->b(LX/0QB;)LX/1S5;

    move-result-object v6

    check-cast v6, LX/1S5;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v10

    check-cast v10, LX/0Wd;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    .line 247746
    iput-object v4, v3, LX/1S9;->a:Ljava/util/concurrent/Executor;

    iput-object v5, v3, LX/1S9;->b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    iput-object v6, v3, LX/1S9;->c:LX/1S5;

    iput-object v7, v3, LX/1S9;->d:LX/0if;

    iput-object v8, v3, LX/1S9;->e:LX/0Uh;

    iput-object v9, v3, LX/1S9;->f:LX/0tX;

    iput-object v10, v3, LX/1S9;->g:LX/0Wd;

    iput-object p0, v3, LX/1S9;->h:Ljava/util/concurrent/Executor;

    .line 247747
    move-object v0, v3

    .line 247748
    sput-object v0, LX/1S9;->m:LX/1S9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247749
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 247750
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 247751
    :cond_1
    sget-object v0, LX/1S9;->m:LX/1S9;

    return-object v0

    .line 247752
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 247753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/1S9;LX/2ta;)V
    .locals 4

    .prologue
    .line 247754
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2ta;->a:LX/3Aj;

    if-nez v0, :cond_1

    .line 247755
    :cond_0
    :goto_0
    return-void

    .line 247756
    :cond_1
    new-instance v0, LX/326;

    invoke-direct {v0}, LX/326;-><init>()V

    move-object v0, v0

    .line 247757
    new-instance v1, LX/34l;

    invoke-direct {v1}, LX/34l;-><init>()V

    .line 247758
    iget-object v2, p1, LX/2ta;->a:LX/3Aj;

    .line 247759
    const-string v3, "viewer_coordinates"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 247760
    iget-object v2, p1, LX/2ta;->b:LX/3Ak;

    if-eqz v2, :cond_2

    .line 247761
    iget-object v2, p1, LX/2ta;->b:LX/3Ak;

    .line 247762
    const-string v3, "wifi_data"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 247763
    :cond_2
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 247764
    iget-object v1, p0, LX/1S9;->f:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 247765
    iget-object v1, p0, LX/1S9;->d:LX/0if;

    sget-object v2, LX/0ig;->av:LX/0ih;

    const-string v3, "fetch_question"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 247766
    new-instance v1, LX/34m;

    invoke-direct {v1, p0}, LX/34m;-><init>(LX/1S9;)V

    iget-object v2, p0, LX/1S9;->h:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static c(LX/1S9;)V
    .locals 3

    .prologue
    .line 247767
    iget-object v0, p0, LX/1S9;->d:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "fetch_location"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 247768
    iget-object v0, p0, LX/1S9;->b:Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuLocationHelper;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/2tY;

    invoke-direct {v1, p0}, LX/2tY;-><init>(LX/1S9;)V

    iget-object v2, p0, LX/1S9;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 247769
    return-void
.end method
