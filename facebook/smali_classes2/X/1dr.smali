.class public LX/1dr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/82e;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Handler;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 286608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286609
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1dr;->b:Ljava/util/Set;

    .line 286610
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1dr;->a:Ljava/util/Map;

    .line 286611
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/1dr;->c:Landroid/os/Handler;

    .line 286612
    return-void
.end method

.method public static a(LX/0QB;)LX/1dr;
    .locals 3

    .prologue
    .line 286639
    const-class v1, LX/1dr;

    monitor-enter v1

    .line 286640
    :try_start_0
    sget-object v0, LX/1dr;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 286641
    sput-object v2, LX/1dr;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 286642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 286644
    new-instance v0, LX/1dr;

    invoke-direct {v0}, LX/1dr;-><init>()V

    .line 286645
    move-object v0, v0

    .line 286646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 286647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1dr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 286649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 6
    .param p0    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 286629
    if-nez p0, :cond_0

    .line 286630
    const-string v0, ""

    .line 286631
    :goto_0
    return-object v0

    .line 286632
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286633
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286634
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move-object v2, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 286635
    instance-of v5, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_1

    .line 286636
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286637
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_1

    .line 286638
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 3
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;"
        }
    .end annotation

    .prologue
    .line 286650
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 286651
    :goto_0
    iget-boolean v1, p0, LX/1dr;->d:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_2

    .line 286652
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 286653
    :goto_1
    return-object v0

    .line 286654
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 286655
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_0

    .line 286656
    :cond_2
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 286657
    iget-object v1, p0, LX/1dr;->b:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 286658
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    goto :goto_1

    .line 286659
    :cond_3
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 286660
    if-eqz v1, :cond_4

    invoke-virtual {p0, v1}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/graphics/drawable/Drawable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286615
    iget-boolean v0, p0, LX/1dr;->d:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 286616
    :cond_0
    :goto_0
    return-void

    .line 286617
    :cond_1
    instance-of v0, p2, LX/82e;

    .line 286618
    instance-of v1, p2, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 286619
    check-cast p2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 286620
    instance-of v0, p2, LX/82e;

    move v1, v0

    move-object v0, p2

    .line 286621
    :goto_1
    if-eqz v1, :cond_0

    .line 286622
    check-cast v0, LX/82e;

    .line 286623
    invoke-static {p1}, LX/1dr;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 286624
    iget-object v1, p0, LX/1dr;->a:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 286625
    if-nez v1, :cond_2

    .line 286626
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 286627
    iget-object v3, p0, LX/1dr;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286628
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v1, v0

    move-object v0, p2

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 286613
    iput-boolean p1, p0, LX/1dr;->d:Z

    .line 286614
    return-void
.end method
