.class public LX/120;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static A:LX/0Xm;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static w:Z

.field private static x:Z


# instance fields
.field public a:Z

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yH;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Xl;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12L;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0yI;

.field private final l:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/121;

.field private final n:LX/0W3;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12J;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Yb;

.field public q:Landroid/view/ViewStub;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private s:LX/124;

.field private final t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/12I;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/127;

.field private final v:LX/0Uh;

.field public y:Z

.field private final z:LX/122;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173708
    const-class v0, LX/120;

    sput-object v0, LX/120;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yI;LX/0Or;LX/121;LX/0W3;LX/0Uh;LX/0Ot;)V
    .locals 4
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/dialtone/gk/IsDialtoneEligibleGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0yH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/12L;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0yI;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/121;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/12J;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173710
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/120;->a:Z

    .line 173711
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/120;->y:Z

    .line 173712
    new-instance v1, LX/122;

    invoke-direct {v1, p0}, LX/122;-><init>(LX/120;)V

    iput-object v1, p0, LX/120;->z:LX/122;

    .line 173713
    iput-object p1, p0, LX/120;->c:LX/0Ot;

    .line 173714
    iput-object p2, p0, LX/120;->d:LX/0Ot;

    .line 173715
    iput-object p3, p0, LX/120;->e:LX/0Ot;

    .line 173716
    iput-object p4, p0, LX/120;->f:LX/0Xl;

    .line 173717
    iput-object p5, p0, LX/120;->g:LX/0Ot;

    .line 173718
    iput-object p8, p0, LX/120;->h:LX/0Or;

    .line 173719
    iput-object p6, p0, LX/120;->i:LX/0Ot;

    .line 173720
    iput-object p7, p0, LX/120;->j:LX/0Ot;

    .line 173721
    iput-object p9, p0, LX/120;->k:LX/0yI;

    .line 173722
    iput-object p10, p0, LX/120;->l:LX/0Or;

    .line 173723
    iput-object p11, p0, LX/120;->m:LX/121;

    .line 173724
    move-object/from16 v0, p12

    iput-object v0, p0, LX/120;->n:LX/0W3;

    .line 173725
    move-object/from16 v0, p14

    iput-object v0, p0, LX/120;->o:LX/0Ot;

    .line 173726
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, LX/120;->t:Ljava/util/Set;

    .line 173727
    move-object/from16 v0, p13

    iput-object v0, p0, LX/120;->v:LX/0Uh;

    .line 173728
    new-instance v1, LX/123;

    invoke-direct {v1, p0}, LX/123;-><init>(LX/120;)V

    iput-object v1, p0, LX/120;->s:LX/124;

    .line 173729
    iget-object v1, p0, LX/120;->f:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.zero.ZERO_RATING_STATE_CHANGED"

    new-instance v3, LX/125;

    invoke-direct {v3, p0}, LX/125;-><init>(LX/120;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.feed.util.NEWS_FEED_NEW_STORIES"

    new-instance v3, LX/126;

    invoke-direct {v3, p0}, LX/126;-><init>(LX/120;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, p0, LX/120;->p:LX/0Yb;

    .line 173730
    new-instance v1, LX/127;

    invoke-direct {v1, p0}, LX/127;-><init>(LX/120;)V

    iput-object v1, p0, LX/120;->u:LX/127;

    .line 173731
    return-void
.end method

.method public static a(LX/0QB;)LX/120;
    .locals 3

    .prologue
    .line 173732
    const-class v1, LX/120;

    monitor-enter v1

    .line 173733
    :try_start_0
    sget-object v0, LX/120;->A:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 173734
    sput-object v2, LX/120;->A:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 173735
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173736
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/120;->b(LX/0QB;)LX/120;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 173737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/120;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 173739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/120;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 173740
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 173741
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 173742
    move-object v1, v0

    .line 173743
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173744
    iget-object v0, p0, LX/120;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 173745
    return-void
.end method

.method public static a$redex0(LX/120;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 173746
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "dialtone"

    .line 173747
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 173748
    move-object v1, v0

    .line 173749
    const-string v0, "ref"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173750
    const-string v2, "carrier_id"

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 173751
    iget-object v0, p0, LX/120;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 173752
    return-void
.end method

.method private static b(LX/0QB;)LX/120;
    .locals 15

    .prologue
    .line 173753
    new-instance v0, LX/120;

    const/16 v1, 0x13ba

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xf9a

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    const/16 v5, 0xbc

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x4e0

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x4e8

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x14d1

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v9

    check-cast v9, LX/0yI;

    const/16 v10, 0x312

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v11

    check-cast v11, LX/121;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    const/16 v14, 0x4f4

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, LX/120;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0yI;LX/0Or;LX/121;LX/0W3;LX/0Uh;LX/0Ot;)V

    .line 173754
    return-object v0
.end method

.method public static h(LX/120;)Z
    .locals 3

    .prologue
    .line 173759
    iget-object v0, p0, LX/120;->v:LX/0Uh;

    const/16 v1, 0x4b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized i(LX/120;)V
    .locals 1

    .prologue
    .line 173755
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 173756
    :goto_0
    monitor-exit p0

    return-void

    .line 173757
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173758
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 173774
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->n:LX/0W3;

    sget-wide v2, LX/0X5;->bO:J

    const/4 v1, 0x3

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v2, v0

    .line 173775
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 173776
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 173777
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/120;->k:LX/0yI;

    sget-object v1, LX/0yY;->DIALTONE_OPTOUT_REMINDER:LX/0yY;

    invoke-virtual {v0, v1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->H:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173778
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->I:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 173779
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 173780
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 173781
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v4, v0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 173782
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, p0, LX/120;->d:LX/0Ot;

    iget-object v2, p0, LX/120;->g:LX/0Ot;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(LX/0Ot;LX/0Ot;)V

    .line 173783
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->H:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static k$redex0(LX/120;)V
    .locals 1

    .prologue
    .line 173785
    iget-object v0, p0, LX/120;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/120;->y:Z

    if-nez v0, :cond_0

    .line 173786
    invoke-static {p0}, LX/120;->m(LX/120;)V

    .line 173787
    :goto_0
    return-void

    .line 173788
    :cond_0
    invoke-direct {p0}, LX/120;->n()V

    goto :goto_0
.end method

.method public static l$redex0(LX/120;)V
    .locals 1

    .prologue
    .line 173770
    invoke-virtual {p0}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173771
    if-eqz v0, :cond_0

    .line 173772
    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->f()V

    .line 173773
    :cond_0
    return-void
.end method

.method public static m(LX/120;)V
    .locals 3

    .prologue
    .line 173760
    iget-object v0, p0, LX/120;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yH;

    sget-object v1, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 173761
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 173762
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v0

    .line 173763
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02d8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 173764
    :cond_0
    invoke-direct {p0}, LX/120;->n()V

    .line 173765
    :goto_0
    return-void

    .line 173766
    :cond_1
    invoke-virtual {p0}, LX/120;->c()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->c()V

    .line 173767
    invoke-virtual {p0}, LX/120;->g()V

    .line 173768
    const-string v0, "dialtone_switcher_impression"

    invoke-static {p0, v0}, LX/120;->a$redex0(LX/120;Ljava/lang/String;)V

    .line 173769
    invoke-direct {p0}, LX/120;->r()V

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 173694
    invoke-virtual {p0}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173695
    if-nez v0, :cond_0

    .line 173696
    :goto_0
    return-void

    .line 173697
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->b()V

    .line 173698
    invoke-direct {p0}, LX/120;->s()V

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 173699
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, LX/120;->w:Z

    if-nez v0, :cond_1

    .line 173700
    sput-boolean v2, LX/120;->w:Z

    .line 173701
    sput-boolean v1, LX/120;->x:Z

    .line 173702
    invoke-direct {p0}, LX/120;->q()V

    .line 173703
    :cond_0
    :goto_0
    return-void

    .line 173704
    :cond_1
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, LX/120;->x:Z

    if-nez v0, :cond_0

    .line 173705
    sput-boolean v1, LX/120;->w:Z

    .line 173706
    sput-boolean v2, LX/120;->x:Z

    .line 173707
    invoke-direct {p0}, LX/120;->p()V

    goto :goto_0
.end method

.method private declared-synchronized p()V
    .locals 7

    .prologue
    .line 173594
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 173595
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 173596
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->B:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v6

    .line 173597
    const/4 v0, 0x3

    if-ge v6, v0, :cond_0

    .line 173598
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173599
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080644

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173600
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080645

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 173601
    :goto_1
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v3, p0, LX/120;->d:LX/0Ot;

    iget-object v4, p0, LX/120;->g:LX/0Ot;

    const/16 v5, 0x1388

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ot;LX/0Ot;I)V

    .line 173602
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->B:LX/0Tn;

    add-int/lit8 v2, v6, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173603
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173604
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173605
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080642

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173606
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080648

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 173607
    :cond_3
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080642

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173608
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080643

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_1
.end method

.method private declared-synchronized q()V
    .locals 7

    .prologue
    .line 173609
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 173610
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 173611
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->A:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v6

    .line 173612
    if-gtz v6, :cond_0

    .line 173613
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08062c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173614
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0df;->j:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173615
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173616
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080646

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173617
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080647

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 173618
    :goto_1
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v3, p0, LX/120;->d:LX/0Ot;

    iget-object v4, p0, LX/120;->g:LX/0Ot;

    const/16 v5, 0x1388

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ot;LX/0Ot;I)V

    .line 173619
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->A:LX/0Tn;

    add-int/lit8 v2, v6, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173620
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173621
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173622
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080640

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173623
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080641

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 173624
    :cond_3
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08063f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173625
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080641

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_1
.end method

.method private r()V
    .locals 2

    .prologue
    .line 173626
    iget-object v0, p0, LX/120;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12I;

    .line 173627
    invoke-interface {v0}, LX/12I;->a()V

    goto :goto_0

    .line 173628
    :cond_0
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 173629
    iget-object v0, p0, LX/120;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12I;

    .line 173630
    invoke-interface {v0}, LX/12I;->b()V

    goto :goto_0

    .line 173631
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 173632
    iget-object v0, p0, LX/120;->p:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 173633
    iget-object v0, p0, LX/120;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12J;

    iget-object v1, p0, LX/120;->z:LX/122;

    .line 173634
    iput-object v1, v0, LX/12J;->k:LX/122;

    .line 173635
    iget-object v0, p0, LX/120;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12L;

    iget-object v1, p0, LX/120;->u:LX/127;

    .line 173636
    iput-object v1, v0, LX/12L;->g:LX/127;

    .line 173637
    invoke-static {p0}, LX/120;->k$redex0(LX/120;)V

    .line 173638
    return-void
.end method

.method public final declared-synchronized a(LX/12I;)V
    .locals 1
    .param p1    # LX/12I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173639
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173640
    monitor-exit p0

    return-void

    .line 173641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 173642
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/120;->q:Landroid/view/ViewStub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173643
    monitor-exit p0

    return-void

    .line 173644
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 173645
    iget-object v0, p0, LX/120;->p:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 173646
    iget-object v0, p0, LX/120;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12J;

    .line 173647
    const/4 v1, 0x0

    iput-object v1, v0, LX/12J;->k:LX/122;

    .line 173648
    iget-object v0, p0, LX/120;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12L;

    .line 173649
    const/4 v1, 0x0

    iput-object v1, v0, LX/12L;->g:LX/127;

    .line 173650
    return-void
.end method

.method public final declared-synchronized b(LX/12I;)V
    .locals 1
    .param p1    # LX/12I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173651
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173652
    monitor-exit p0

    return-void

    .line 173653
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    .locals 5

    .prologue
    .line 173654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    if-nez v0, :cond_1

    .line 173655
    iget-object v0, p0, LX/120;->q:Landroid/view/ViewStub;

    if-nez v0, :cond_0

    .line 173656
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mDialtoneManualSwitcherStub should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173657
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173658
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/120;->q:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iput-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    .line 173659
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, p0, LX/120;->s:LX/124;

    .line 173660
    iput-object v1, v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->f:LX/124;

    .line 173661
    iget-object v1, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0df;->j:LX/0Tn;

    iget-object v3, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    invoke-virtual {v3}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080619

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173662
    iput-object v0, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->c:Ljava/lang/String;

    .line 173663
    const/4 v0, 0x0

    iput-object v0, p0, LX/120;->q:Landroid/view/ViewStub;

    .line 173664
    :cond_1
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    .locals 1

    .prologue
    .line 173665
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/120;->r:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 173666
    iget-object v0, p0, LX/120;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/120;->k:LX/0yI;

    sget-object v2, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final g()V
    .locals 8

    .prologue
    const/16 v2, 0x15

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 173667
    invoke-virtual {p0}, LX/120;->c()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v1

    .line 173668
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 173669
    invoke-virtual {v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e()V

    .line 173670
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 173671
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 173672
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 173673
    :cond_0
    iget-object v0, p0, LX/120;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yH;

    sget-object v2, LX/0yY;->DIALTONE_SWITCHER_NUX:LX/0yY;

    invoke-virtual {v0, v2}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->C:LX/0Tn;

    invoke-interface {v0, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 173674
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcherNuxActivity;

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173675
    const/high16 v0, 0x4000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 173676
    iget-object v0, p0, LX/120;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 173677
    :cond_1
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->I:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->H:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 173678
    :cond_2
    :goto_0
    invoke-direct {p0}, LX/120;->o()V

    .line 173679
    return-void

    .line 173680
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->d()V

    .line 173681
    iget-object v0, p0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 173682
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_4

    if-eqz v1, :cond_4

    .line 173683
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 173684
    :cond_4
    iget-object v0, p0, LX/120;->k:LX/0yI;

    sget-object v2, LX/0yY;->SWITCH_TO_DIALTONE:LX/0yY;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->D:LX/0Tn;

    invoke-interface {v0, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 173685
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 173686
    const-string v0, "dialtone://switch_to_dialtone"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 173687
    const-string v0, "ref"

    const-string v3, "force_switch_to_dialtone"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173688
    iget-object v0, p0, LX/120;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 173689
    :cond_5
    invoke-direct {p0}, LX/120;->j()V

    .line 173690
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->I:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 173691
    iget-object v0, p0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->I:LX/0Tn;

    .line 173692
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 173693
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto/16 :goto_0
.end method
