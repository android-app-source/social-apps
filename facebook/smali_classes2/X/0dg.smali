.class public final LX/0dg;
.super LX/0dM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0dM",
        "<",
        "LX/0dh;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0dg;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90697
    sget-object v0, LX/0dh;->d:LX/0Tn;

    invoke-direct {p0, p1, v0}, LX/0dM;-><init>(LX/0Ot;LX/0Tn;)V

    .line 90698
    return-void
.end method

.method public static a(LX/0QB;)LX/0dg;
    .locals 4

    .prologue
    .line 90699
    sget-object v0, LX/0dg;->b:LX/0dg;

    if-nez v0, :cond_1

    .line 90700
    const-class v1, LX/0dg;

    monitor-enter v1

    .line 90701
    :try_start_0
    sget-object v0, LX/0dg;->b:LX/0dg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 90702
    if-eqz v2, :cond_0

    .line 90703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 90704
    new-instance v3, LX/0dg;

    const/16 p0, 0x13bc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0dg;-><init>(LX/0Ot;)V

    .line 90705
    move-object v0, v3

    .line 90706
    sput-object v0, LX/0dg;->b:LX/0dg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90707
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 90708
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90709
    :cond_1
    sget-object v0, LX/0dg;->b:LX/0dg;

    return-object v0

    .line 90710
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 90711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 90712
    check-cast p3, LX/0dh;

    .line 90713
    sget-object v0, LX/0dh;->d:LX/0Tn;

    invoke-virtual {p2, v0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90714
    invoke-virtual {p3}, LX/0dP;->b()V

    .line 90715
    :cond_0
    return-void
.end method
