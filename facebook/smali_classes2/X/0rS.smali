.class public final enum LX/0rS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rS;

.field public static final enum CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

.field public static final enum DO_NOT_CHECK_SERVER:LX/0rS;

.field public static final enum PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

.field public static final enum STALE_DATA_OKAY:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149595
    new-instance v0, LX/0rS;

    const-string v1, "PREFER_CACHE_IF_UP_TO_DATE"

    invoke-direct {v0, v1, v2}, LX/0rS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 149596
    new-instance v0, LX/0rS;

    const-string v1, "CHECK_SERVER_FOR_NEW_DATA"

    invoke-direct {v0, v1, v3}, LX/0rS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 149597
    new-instance v0, LX/0rS;

    const-string v1, "DO_NOT_CHECK_SERVER"

    invoke-direct {v0, v1, v4}, LX/0rS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 149598
    new-instance v0, LX/0rS;

    const-string v1, "STALE_DATA_OKAY"

    invoke-direct {v0, v1, v5}, LX/0rS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 149599
    const/4 v0, 0x4

    new-array v0, v0, [LX/0rS;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    aput-object v1, v0, v2

    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    aput-object v1, v0, v3

    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    aput-object v1, v0, v4

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    aput-object v1, v0, v5

    sput-object v0, LX/0rS;->$VALUES:[LX/0rS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rS;
    .locals 1

    .prologue
    .line 149593
    const-class v0, LX/0rS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rS;

    return-object v0
.end method

.method public static values()[LX/0rS;
    .locals 1

    .prologue
    .line 149592
    sget-object v0, LX/0rS;->$VALUES:[LX/0rS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rS;

    return-object v0
.end method
