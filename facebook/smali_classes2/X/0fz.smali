.class public LX/0fz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g0;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g0",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0qq;

.field public final d:LX/0qm;

.field private final e:LX/03V;

.field private final f:Z

.field public final g:LX/0qu;

.field public final h:LX/0qr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qr",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0qv;

.field public final j:LX/0qx;

.field private final k:LX/0qt;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Z

.field public p:LX/22x;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109941
    const-class v0, LX/0fz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0fz;->b:Ljava/lang/String;

    .line 109942
    sget-object v0, LX/0ql;->a:Ljava/util/Comparator;

    sput-object v0, LX/0fz;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0qm;LX/03V;Ljava/lang/Boolean;LX/0Sh;)V
    .locals 3
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/api/feed/annotation/IsErrorReporterLoggingForFeedUnitCollectionEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 109795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109796
    new-instance v0, LX/0qq;

    invoke-direct {v0, p0}, LX/0qq;-><init>(LX/0fz;)V

    iput-object v0, p0, LX/0fz;->a:LX/0qq;

    .line 109797
    new-instance v0, LX/0qr;

    invoke-direct {v0}, LX/0qr;-><init>()V

    iput-object v0, p0, LX/0fz;->h:LX/0qr;

    .line 109798
    new-instance v0, LX/0qt;

    invoke-direct {v0, p0}, LX/0qt;-><init>(LX/0fz;)V

    iput-object v0, p0, LX/0fz;->k:LX/0qt;

    .line 109799
    iput-object v2, p0, LX/0fz;->l:Ljava/lang/String;

    .line 109800
    iput-object v2, p0, LX/0fz;->m:Ljava/lang/String;

    .line 109801
    iput-boolean v1, p0, LX/0fz;->n:Z

    .line 109802
    iput-boolean v1, p0, LX/0fz;->o:Z

    .line 109803
    new-instance v0, LX/0qu;

    invoke-direct {v0, p4}, LX/0qu;-><init>(LX/0Sh;)V

    iput-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109804
    iput-object p1, p0, LX/0fz;->d:LX/0qm;

    .line 109805
    iput-object p2, p0, LX/0fz;->e:LX/03V;

    .line 109806
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->f:Z

    .line 109807
    new-instance v0, LX/0qv;

    invoke-direct {v0}, LX/0qv;-><init>()V

    iput-object v0, p0, LX/0fz;->i:LX/0qv;

    .line 109808
    new-instance v0, LX/0qx;

    invoke-direct {v0}, LX/0qx;-><init>()V

    iput-object v0, p0, LX/0fz;->j:LX/0qx;

    .line 109809
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    iget-object v1, p0, LX/0fz;->h:LX/0qr;

    .line 109810
    iget-object v2, v0, LX/0qm;->b:LX/0qp;

    .line 109811
    iput-object v1, v2, LX/0qp;->f:LX/0qs;

    .line 109812
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    iget-object v1, p0, LX/0fz;->k:LX/0qt;

    .line 109813
    iget-object v2, v0, LX/0qu;->a:LX/0qp;

    .line 109814
    iput-object v1, v2, LX/0qp;->f:LX/0qs;

    .line 109815
    return-void
.end method

.method public static A(LX/0fz;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 109792
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 109793
    const/4 v0, 0x0

    .line 109794
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static F(LX/0fz;)V
    .locals 6

    .prologue
    .line 109786
    iget-boolean v0, p0, LX/0fz;->f:Z

    if-nez v0, :cond_0

    .line 109787
    :goto_0
    return-void

    .line 109788
    :cond_0
    invoke-virtual {p0}, LX/0fz;->size()I

    move-result v0

    .line 109789
    invoke-virtual {p0}, LX/0fz;->u()Z

    move-result v1

    .line 109790
    invoke-virtual {p0}, LX/0fz;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, LX/0fz;->n:Z

    iget-boolean v5, p0, LX/0fz;->o:Z

    invoke-static {v2, v3, v4, v5}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    move-object v2, v2

    .line 109791
    iget-object v3, p0, LX/0fz;->e:LX/03V;

    sget-object v4, LX/0fz;->b:Ljava/lang/String;

    new-instance v5, LX/18I;

    invoke-direct {v5, p0, v2, v1, v0}, LX/18I;-><init>(LX/0fz;Lcom/facebook/graphql/model/GraphQLPageInfo;ZI)V

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;LX/0VI;)V

    goto :goto_0
.end method

.method private G()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109779
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->b()I

    move-result v2

    .line 109780
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 109781
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    .line 109782
    invoke-static {v0}, LX/0fz;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109783
    :goto_1
    return-object v0

    .line 109784
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 109785
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private I()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 109776
    iput-object v0, p0, LX/0fz;->l:Ljava/lang/String;

    .line 109777
    iput-object v0, p0, LX/0fz;->m:Ljava/lang/String;

    .line 109778
    return-void
.end method

.method public static J(LX/0fz;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 109772
    invoke-virtual {p0}, LX/0fz;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, LX/0fz;->f(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109773
    invoke-static {p0}, LX/0fz;->M(LX/0fz;)V

    .line 109774
    const/4 v0, 0x1

    .line 109775
    :cond_0
    return v0
.end method

.method public static M(LX/0fz;)V
    .locals 3

    .prologue
    .line 109647
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109648
    iget-object v1, v0, LX/0qu;->b:LX/0Sh;

    const-string v2, "CallOnUiThreadOnly"

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 109649
    invoke-static {v0}, LX/0qu;->j(LX/0qu;)V

    .line 109650
    invoke-virtual {v0}, LX/0qu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109651
    iget-object v1, v0, LX/0qu;->a:LX/0qp;

    .line 109652
    iget-object v2, v1, LX/0qp;->e:Ljava/util/List;

    move-object v2, v2

    .line 109653
    iget-object v1, v0, LX/0qu;->e:Ljava/util/List;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0, v1}, LX/0qu;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109654
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/0fz;
    .locals 1

    .prologue
    .line 109767
    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 109764
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/0jR;

    if-eqz v0, :cond_0

    .line 109765
    invoke-static {p0, p1}, LX/0x1;->c(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109766
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 109757
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_1

    .line 109758
    check-cast p0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109759
    invoke-static {p0, p1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109760
    :cond_0
    :goto_0
    return-void

    .line 109761
    :cond_1
    instance-of v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-eqz v0, :cond_0

    .line 109762
    check-cast p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 109763
    invoke-static {p0, p1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;ZLX/0qw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;Z",
            "LX/0qw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109737
    sget-object v0, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-ne p3, v0, :cond_0

    .line 109738
    invoke-static {p0}, LX/0fz;->M(LX/0fz;)V

    .line 109739
    :cond_0
    invoke-virtual {p0}, LX/0fz;->r()I

    .line 109740
    if-eqz p2, :cond_3

    .line 109741
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 109742
    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 109743
    const/4 v1, 0x1

    const/4 p2, 0x0

    .line 109744
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 109745
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object p2, v0

    .line 109746
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 109747
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109748
    sget-object p3, LX/0fz;->c:Ljava/util/Comparator;

    invoke-interface {p3, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    if-gez p3, :cond_5

    .line 109749
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move-object p2, v0

    goto :goto_2

    :cond_1
    move v0, p2

    .line 109750
    goto :goto_1

    .line 109751
    :cond_2
    move-object v0, p2

    .line 109752
    invoke-static {p0, v0}, LX/0fz;->j(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 109753
    if-eqz v0, :cond_3

    .line 109754
    const-string v1, "Head_Fetch_Gap"

    invoke-static {v0, v1}, LX/239;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;

    move-result-object v0

    .line 109755
    invoke-static {p0, v0}, LX/0fz;->c(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109756
    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    move-object v0, p2

    goto :goto_3
.end method

.method public static a$redex0(LX/0fz;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 7

    .prologue
    .line 109697
    if-nez p1, :cond_1

    .line 109698
    :cond_0
    :goto_0
    return-void

    .line 109699
    :cond_1
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    .line 109700
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 109701
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 109702
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0, p1}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    goto :goto_0

    .line 109703
    :cond_2
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qm;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109704
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0qm;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 109705
    :cond_3
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 109706
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109707
    new-instance v2, LX/1u8;

    invoke-direct {v2}, LX/1u8;-><init>()V

    .line 109708
    iput-object p1, v2, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 109709
    move-object v2, v2

    .line 109710
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v3

    .line 109711
    iput-object v3, v2, LX/1u8;->d:Ljava/lang/String;

    .line 109712
    move-object v2, v2

    .line 109713
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v3

    .line 109714
    iput-object v3, v2, LX/1u8;->i:Ljava/lang/String;

    .line 109715
    move-object v2, v2

    .line 109716
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v3

    .line 109717
    iput-object v3, v2, LX/1u8;->c:Ljava/lang/String;

    .line 109718
    move-object v2, v2

    .line 109719
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v4

    .line 109720
    iput-wide v4, v2, LX/1u8;->h:D

    .line 109721
    move-object v2, v2

    .line 109722
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v3

    .line 109723
    iput-object v3, v2, LX/1u8;->f:Ljava/lang/String;

    .line 109724
    move-object v2, v2

    .line 109725
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v3

    .line 109726
    iput-boolean v3, v2, LX/1u8;->e:Z

    .line 109727
    move-object v2, v2

    .line 109728
    invoke-virtual {v2}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 109729
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109730
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109731
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 109732
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-static {v3}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v3

    invoke-static {p1, v3}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 109733
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-static {v3}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109734
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-static {v3}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109735
    invoke-static {v0}, LX/0fz;->d(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109736
    :cond_4
    invoke-virtual {p0, v2}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/0fz;
    .locals 5

    .prologue
    .line 109693
    new-instance v4, LX/0fz;

    invoke-static {p0}, LX/0qm;->b(LX/0QB;)LX/0qm;

    move-result-object v0

    check-cast v0, LX/0qm;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    .line 109694
    invoke-static {}, LX/0eG;->e()Ljava/lang/Boolean;

    move-result-object v2

    move-object v2, v2

    .line 109695
    check-cast v2, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/0fz;-><init>(LX/0qm;LX/03V;Ljava/lang/Boolean;LX/0Sh;)V

    .line 109696
    return-object v4
.end method

.method private static b(LX/0fz;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 109676
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109677
    iget-object v2, v0, LX/0qu;->b:LX/0Sh;

    const-string v3, "CallOnUiThreadOnly"

    invoke-virtual {v2, v3}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 109678
    iget-boolean v2, v0, LX/0qu;->c:Z

    if-eqz v2, :cond_0

    .line 109679
    iget-object v2, v0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v2}, LX/0qp;->values()Ljava/util/Collection;

    move-result-object v2

    move-object v2, v2

    .line 109680
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109681
    iget-object v4, v0, LX/0qu;->d:Ljava/util/List;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109682
    :cond_0
    iget-object v2, v0, LX/0qu;->a:LX/0qp;

    invoke-virtual {v2}, LX/0qp;->clear()V

    .line 109683
    invoke-static {v0}, LX/0qu;->i(LX/0qu;)V

    .line 109684
    iput-boolean v1, p0, LX/0fz;->n:Z

    .line 109685
    iput-boolean v1, p0, LX/0fz;->o:Z

    .line 109686
    invoke-direct {p0}, LX/0fz;->I()V

    .line 109687
    if-eqz p1, :cond_1

    .line 109688
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->d()V

    .line 109689
    :goto_1
    iget-object v0, p0, LX/0fz;->i:LX/0qv;

    invoke-virtual {v0}, LX/0qv;->b()V

    .line 109690
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V

    .line 109691
    return-void

    .line 109692
    :cond_1
    invoke-virtual {p0}, LX/0fz;->d()V

    goto :goto_1
.end method

.method private b(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "LX/0qw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109655
    sget-object v0, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-ne p3, v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109656
    invoke-static {p0}, LX/0fz;->M(LX/0fz;)V

    .line 109657
    :cond_0
    :goto_0
    return-void

    .line 109658
    :cond_1
    if-eqz p2, :cond_2

    .line 109659
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->n:Z

    .line 109660
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->o:Z

    .line 109661
    :cond_2
    invoke-static {p2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109662
    :cond_3
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    .line 109663
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    invoke-direct {p0, p1, v1, p3}, LX/0fz;->a(Ljava/util/List;ZLX/0qw;)V

    .line 109664
    invoke-static {p0, p1}, LX/0fz;->c(LX/0fz;Ljava/util/List;)V

    .line 109665
    sget-object v1, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-ne p3, v1, :cond_5

    .line 109666
    if-nez v0, :cond_4

    .line 109667
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->n:Z

    .line 109668
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->o:Z

    .line 109669
    :cond_4
    :goto_1
    invoke-static {p0}, LX/0fz;->J(LX/0fz;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109670
    invoke-static {p0}, LX/0fz;->y(LX/0fz;)V

    .line 109671
    invoke-direct {p0}, LX/0fz;->G()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0fz;->l:Ljava/lang/String;

    goto :goto_0

    .line 109672
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v1

    iput-boolean v1, p0, LX/0fz;->n:Z

    .line 109673
    if-nez v0, :cond_4

    .line 109674
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->o:Z

    goto :goto_1

    .line 109675
    :cond_6
    invoke-static {p0}, LX/0fz;->A(LX/0fz;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0fz;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public static c(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 109827
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qu;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 109828
    if-eqz v0, :cond_0

    sget-object v1, LX/0fz;->c:Ljava/util/Comparator;

    invoke-interface {v1, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_2

    .line 109829
    :cond_0
    if-eqz v0, :cond_1

    .line 109830
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v1, v0}, LX/0qu;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109831
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0fz;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109832
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, p1}, LX/0qu;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109833
    :cond_2
    return-void
.end method

.method public static c(LX/0fz;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109834
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109835
    invoke-static {p0, v0}, LX/0fz;->c(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109836
    instance-of v2, v0, Lcom/facebook/feed/model/GapFeedEdge;

    if-eqz v2, :cond_0

    .line 109837
    invoke-static {p0, v0}, LX/0fz;->e(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)I

    move-result v2

    .line 109838
    add-int/lit8 v3, v2, -0x1

    .line 109839
    add-int/lit8 v2, v2, 0x1

    .line 109840
    if-ltz v3, :cond_2

    invoke-virtual {p0}, LX/0fz;->v()I

    move-result p1

    if-ge v3, p1, :cond_2

    .line 109841
    invoke-virtual {p0, v3}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 109842
    instance-of v3, v3, Lcom/facebook/feed/model/GapFeedEdge;

    if-eqz v3, :cond_2

    .line 109843
    invoke-virtual {p0, v0}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109844
    :cond_0
    :goto_1
    goto :goto_0

    .line 109845
    :cond_1
    invoke-static {p0}, LX/0fz;->y(LX/0fz;)V

    .line 109846
    return-void

    .line 109847
    :cond_2
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 109848
    invoke-virtual {p0, v2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 109849
    instance-of v3, v2, Lcom/facebook/feed/model/GapFeedEdge;

    if-eqz v3, :cond_0

    .line 109850
    invoke-virtual {p0, v2}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_1
.end method

.method private static d(LX/0fz;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 109851
    const/4 v0, 0x0

    .line 109852
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109853
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/0qu;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 109854
    if-eqz v0, :cond_1

    .line 109855
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 109856
    goto :goto_0

    .line 109857
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 109858
    instance-of v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-nez v0, :cond_1

    .line 109859
    :cond_0
    :goto_0
    return-void

    .line 109860
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 109861
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109862
    check-cast p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 109863
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 109864
    iget v0, p0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->v:I

    move v0, v0

    .line 109865
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109866
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "synthetic_cursor"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)I
    .locals 2

    .prologue
    .line 109867
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 109868
    if-ltz v0, :cond_1

    .line 109869
    :cond_0
    :goto_0
    return v0

    .line 109870
    :cond_1
    invoke-direct {p0, p1}, LX/0fz;->f(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)I

    move-result v0

    .line 109871
    if-ltz v0, :cond_0

    .line 109872
    iget-object v1, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private f(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)I
    .locals 2

    .prologue
    .line 109873
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qu;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 109874
    if-nez v0, :cond_0

    .line 109875
    const/4 v0, -0x1

    .line 109876
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    .line 109877
    iget-object p0, v1, LX/0qu;->a:LX/0qp;

    invoke-virtual {p0, v0}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result p0

    move v0, p0

    .line 109878
    goto :goto_0
.end method

.method public static i(LX/0fz;I)V
    .locals 3

    .prologue
    .line 109879
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    :goto_0
    if-le v0, p1, :cond_0

    .line 109880
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109881
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 109882
    :cond_0
    if-nez v0, :cond_1

    .line 109883
    invoke-virtual {p0}, LX/0fz;->m()V

    .line 109884
    :cond_1
    invoke-direct {p0}, LX/0fz;->I()V

    .line 109885
    return-void
.end method

.method public static j(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109886
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 109887
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v1, v0}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 109888
    sget-object v2, LX/0fz;->c:Ljava/util/Comparator;

    invoke-interface {v2, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_0

    move-object v0, v1

    .line 109889
    :goto_1
    return-object v0

    .line 109890
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109891
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static y(LX/0fz;)V
    .locals 6

    .prologue
    .line 109892
    const-string v0, "FeedUnitCollection.setFeedUnitRerankingData"

    const v1, 0x5c2411ac

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109893
    const/4 v0, 0x0

    .line 109894
    :try_start_0
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v1}, LX/0qu;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109895
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 109896
    if-eqz v3, :cond_0

    .line 109897
    invoke-static {v3, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 109898
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0YN;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 109899
    if-nez v4, :cond_1

    .line 109900
    const-string v4, "0"

    invoke-static {v3, v4}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109901
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109902
    invoke-static {v0}, LX/0fz;->d(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109903
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 109904
    goto :goto_0

    .line 109905
    :cond_1
    if-lez v4, :cond_2

    .line 109906
    const-string v4, "1"

    invoke-static {v3, v4}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 109907
    :catchall_0
    move-exception v0

    const v1, -0x76b819d2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 109908
    :cond_2
    :try_start_1
    const-string v4, "-1"

    invoke-static {v3, v4}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 109909
    :cond_3
    const v0, 0x19f6f9ed

    invoke-static {v0}, LX/02m;->a(I)V

    .line 109910
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 109911
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 109912
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109913
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 109914
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 109915
    instance-of v4, v3, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v4, :cond_1

    instance-of v3, v3, Lcom/facebook/graphql/model/Sponsorable;

    if-nez v3, :cond_0

    .line 109916
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109917
    :cond_2
    move-object v0, v1

    .line 109918
    invoke-static {p0, v0}, LX/0fz;->d(LX/0fz;Ljava/util/List;)I

    move-result v1

    iget-object v2, p0, LX/0fz;->d:LX/0qm;

    .line 109919
    const/4 v3, 0x0

    .line 109920
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    move v5, v3

    :cond_3
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109921
    iget-object v4, v2, LX/0qm;->b:LX/0qp;

    invoke-virtual {v4}, LX/0qp;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 109922
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-static {v4, v3}, LX/0rh;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 109923
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    .line 109924
    goto :goto_1

    .line 109925
    :cond_5
    move v0, v5

    .line 109926
    sub-int v0, v1, v0

    return v0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 109927
    invoke-virtual {p0, p1}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109928
    if-nez p1, :cond_0

    .line 109929
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 109930
    :goto_0
    return-object v0

    .line 109931
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 109932
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 109933
    iget-object v2, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v2, v0}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 109934
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 109935
    if-eqz v3, :cond_1

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 109936
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 109937
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109938
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109939
    iget-object v0, p0, LX/0fz;->h:LX/0qr;

    invoke-virtual {v0, p1}, LX/0qr;->a(LX/0qs;)V

    .line 109940
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 2

    .prologue
    .line 109768
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0fz;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109769
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, p1}, LX/0qu;->c(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109770
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V

    .line 109771
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109816
    const-string v0, "FeedUnitCollection.addElementsAtTail"

    const v1, 0x38e2212a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109817
    :try_start_0
    invoke-static {p2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109818
    :cond_0
    :goto_0
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109819
    const v0, -0x5b8c11ac

    invoke-static {v0}, LX/02m;->a(I)V

    .line 109820
    return-void

    .line 109821
    :catchall_0
    move-exception v0

    const v1, 0x2195b05d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 109822
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0fz;->o:Z

    .line 109823
    invoke-static {p0, p1}, LX/0fz;->c(LX/0fz;Ljava/util/List;)V

    .line 109824
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0fz;->m:Ljava/lang/String;

    .line 109825
    invoke-static {p0}, LX/0fz;->J(LX/0fz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109826
    invoke-static {p0}, LX/0fz;->y(LX/0fz;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "LX/0qw;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 109415
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;ZZ)V

    .line 109416
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "LX/0qw;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 109541
    const-string v0, "FeedUnitCollection.addElementsAtHead"

    const v1, -0x753d2a92

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109542
    :try_start_0
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    .line 109543
    if-eqz p5, :cond_0

    if-lez v0, :cond_0

    iget-object v1, p0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v1}, LX/0qx;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 109544
    iget-object v1, p0, LX/0fz;->j:LX/0qx;

    .line 109545
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 109546
    :cond_0
    if-eqz p4, :cond_3

    if-lez v0, :cond_3

    .line 109547
    iget-object v0, p0, LX/0fz;->i:LX/0qv;

    .line 109548
    invoke-static {p2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 109549
    :cond_1
    iget-boolean v1, v0, LX/0qv;->f:Z

    if-eqz v1, :cond_2

    .line 109550
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/0qv;->e:Z

    .line 109551
    :cond_2
    :goto_0
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109552
    const v0, -0x1db41ccf

    invoke-static {v0}, LX/02m;->a(I)V

    .line 109553
    return-void

    .line 109554
    :cond_3
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LX/0fz;->b(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 109555
    :catchall_0
    move-exception v0

    const v1, -0x6e621513

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 109556
    :cond_4
    :try_start_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, LX/0qx;->a:I

    sub-int v3, v2, v3

    .line 109557
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sget p5, LX/0qx;->a:I

    if-lt v2, p5, :cond_6

    .line 109558
    invoke-virtual {v1}, LX/0qx;->a()V

    .line 109559
    :cond_5
    const/4 v2, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    sget v3, LX/0qx;->a:I

    if-ge v2, v3, :cond_0

    .line 109560
    iget-object v3, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p5

    invoke-interface {v3, v2, p5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 109561
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 109562
    :cond_6
    if-lez v3, :cond_5

    .line 109563
    iget-object v2, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-ltz v2, :cond_5

    if-lez v3, :cond_5

    .line 109564
    iget-object p5, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {p5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 109565
    add-int/lit8 v3, v3, -0x1

    .line 109566
    add-int/lit8 v2, v2, -0x1

    goto :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 109567
    :cond_7
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0qv;->f:Z

    .line 109568
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0qv;->c:Ljava/lang/String;

    .line 109569
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/0qv;->e:Z

    .line 109570
    sget-object v1, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-eq p3, v1, :cond_8

    .line 109571
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/0qv;->d:Z

    .line 109572
    :cond_8
    iput-object p3, v0, LX/0qv;->b:LX/0qw;

    .line 109573
    iget-object v1, v0, LX/0qv;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;IZ)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 109505
    invoke-static {p0, p1}, LX/0fz;->e(LX/0fz;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)I

    move-result v1

    .line 109506
    if-ltz v1, :cond_0

    iget-object v2, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v2}, LX/0qm;->b()I

    move-result v2

    if-lt v1, v2, :cond_1

    :cond_0
    iget-object v2, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v2}, LX/0qm;->b()I

    move-result v2

    if-lt p2, v2, :cond_1

    invoke-virtual {p0}, LX/0fz;->size()I

    move-result v2

    if-gt v1, v2, :cond_1

    invoke-virtual {p0}, LX/0fz;->size()I

    move-result v2

    if-gt p2, v2, :cond_1

    if-ne v1, p2, :cond_2

    .line 109507
    :cond_1
    :goto_0
    return v0

    .line 109508
    :cond_2
    if-nez p3, :cond_4

    .line 109509
    invoke-virtual {p0}, LX/0fz;->w()I

    move-result v2

    .line 109510
    if-ge v1, v2, :cond_3

    if-gt p2, v2, :cond_1

    :cond_3
    if-ge p2, v2, :cond_4

    if-gt v1, v2, :cond_1

    .line 109511
    :cond_4
    invoke-virtual {p0, p2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v1

    .line 109512
    new-instance v4, LX/1u8;

    invoke-direct {v4}, LX/1u8;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 109513
    iput-object v5, v4, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 109514
    move-object v4, v4

    .line 109515
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v5

    .line 109516
    iput-object v5, v4, LX/1u8;->d:Ljava/lang/String;

    .line 109517
    move-object v4, v4

    .line 109518
    iput-object v0, v4, LX/1u8;->i:Ljava/lang/String;

    .line 109519
    move-object v4, v4

    .line 109520
    const-string v5, "synthetic_cursor"

    .line 109521
    iput-object v5, v4, LX/1u8;->c:Ljava/lang/String;

    .line 109522
    move-object v4, v4

    .line 109523
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v6

    .line 109524
    iput-wide v6, v4, LX/1u8;->h:D

    .line 109525
    move-object v4, v4

    .line 109526
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v5

    .line 109527
    iput-object v5, v4, LX/1u8;->f:Ljava/lang/String;

    .line 109528
    move-object v4, v4

    .line 109529
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v5

    .line 109530
    iput-boolean v5, v4, LX/1u8;->e:Z

    .line 109531
    move-object v4, v4

    .line 109532
    invoke-virtual {v4}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v4

    .line 109533
    const-string v5, "synthetic_cursor"

    invoke-static {v4, v5}, LX/0x1;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109534
    invoke-static {v4, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109535
    move-object v0, v4

    .line 109536
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0qu;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 109537
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eq v2, v3, :cond_5

    .line 109538
    iget-object v2, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v2, v1}, LX/0qu;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109539
    :cond_5
    invoke-virtual {p0, v0}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109540
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 2

    .prologue
    .line 109502
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->b()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 109503
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109504
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    iget-object v1, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 109501
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v0, p1}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109499
    iget-object v0, p0, LX/0fz;->h:LX/0qr;

    invoke-virtual {v0, p1}, LX/0qr;->b(LX/0qs;)V

    .line 109500
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V
    .locals 1

    .prologue
    .line 109495
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, p1}, LX/0qu;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 109496
    invoke-direct {p0}, LX/0fz;->I()V

    .line 109497
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V

    .line 109498
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 109441
    const-string v0, "FeedUnitCollection.addAtEnd"

    const v2, 0x31b8d1a2

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 109442
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109443
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0qu;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109444
    const/4 v0, 0x1

    .line 109445
    :goto_0
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109446
    if-nez v0, :cond_1

    .line 109447
    const v0, 0x2c5f0855

    invoke-static {v0}, LX/02m;->a(I)V

    .line 109448
    :goto_1
    return-void

    .line 109449
    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 109450
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3}, LX/0qu;->b()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 109451
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3, v0}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 109452
    instance-of v4, v3, Lcom/facebook/feed/model/GapFeedEdge;

    if-eqz v4, :cond_2

    .line 109453
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109454
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 109455
    :cond_3
    move-object v0, v2

    .line 109456
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109457
    invoke-virtual {p0, v2}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_3

    .line 109458
    :cond_4
    move-object v4, v0

    .line 109459
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 109460
    const/4 v0, 0x0

    :goto_4
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3}, LX/0qu;->b()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 109461
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3, v0}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 109462
    new-instance v5, LX/55l;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, LX/55l;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109463
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 109464
    :cond_5
    move-object v5, v2

    .line 109465
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109466
    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0qu;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 109467
    new-instance v3, LX/55l;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, LX/55l;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 109468
    :catchall_0
    move-exception v0

    const v1, 0x4da5c0c7    # 3.47609312E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 109469
    :cond_7
    :try_start_2
    new-instance v0, LX/55k;

    invoke-direct {v0}, LX/55k;-><init>()V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 109470
    move v2, v1

    .line 109471
    :goto_6
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->b()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 109472
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, v2}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 109473
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/55l;

    .line 109474
    iget-object v6, v0, LX/55l;->a:Ljava/lang/String;

    move-object v0, v6

    .line 109475
    invoke-static {v3, v0}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109476
    add-int/lit8 v1, v1, 0x1

    .line 109477
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 109478
    :cond_8
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    .line 109479
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    move v3, v1

    :cond_9
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109480
    iget-object v1, p0, LX/0fz;->g:LX/0qu;

    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/0qu;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 109481
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/55l;

    .line 109482
    iget-object v7, v1, LX/55l;->a:Ljava/lang/String;

    move-object v1, v7

    .line 109483
    invoke-static {v0, v1}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 109484
    add-int/lit8 v3, v3, 0x1

    .line 109485
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {v1, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 109486
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    const-string v7, "0"

    invoke-static {v1, v7}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 109487
    add-int/lit8 v1, v2, 0x1

    .line 109488
    invoke-virtual {p0, v0}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    move v2, v1

    .line 109489
    goto :goto_7

    .line 109490
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109491
    invoke-virtual {p0, v0}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_8

    .line 109492
    :cond_b
    invoke-direct {p0}, LX/0fz;->I()V

    .line 109493
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 109494
    const v0, -0x10376c0c

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109435
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109436
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_2

    .line 109437
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 109438
    :goto_0
    move-object v2, v2

    .line 109439
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 109440
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 109432
    iget-object v0, p0, LX/0fz;->i:LX/0qv;

    .line 109433
    iget-boolean p0, v0, LX/0qv;->f:Z

    move v0, p0

    .line 109434
    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 109429
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 109430
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 109431
    return-void
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 109422
    invoke-virtual {p0}, LX/0fz;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109423
    const/4 v0, 0x0

    .line 109424
    :goto_0
    return v0

    .line 109425
    :cond_0
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    .line 109426
    invoke-static {p0, p1}, LX/0fz;->i(LX/0fz;I)V

    .line 109427
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v1

    sub-int/2addr v0, v1

    .line 109428
    invoke-static {p0}, LX/0fz;->F(LX/0fz;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 109421
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v0}, LX/0qx;->d()Z

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 2

    .prologue
    .line 109417
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109418
    invoke-static {v0}, LX/0qu;->j(LX/0qu;)V

    .line 109419
    iget-object v1, v0, LX/0qu;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 109420
    return v0
.end method

.method public final g()V
    .locals 6

    .prologue
    .line 109404
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v0}, LX/0qx;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109405
    :goto_0
    return-void

    .line 109406
    :cond_0
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    .line 109407
    iget-object v1, v0, LX/0qx;->c:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 109408
    iget-object v1, p0, LX/0fz;->j:LX/0qx;

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 109409
    iget-object v3, v1, LX/0qx;->c:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 109410
    iget-object v2, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v3

    .line 109411
    iget-object v2, v1, LX/0qx;->c:Ljava/util/List;

    iget-object v4, v1, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v2

    .line 109412
    :goto_1
    const/4 v4, 0x1

    invoke-static {v3, v2, v5, v4}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    move-object v1, v2

    .line 109413
    iget-object v2, p0, LX/0fz;->j:LX/0qx;

    iget-object v2, v2, LX/0qx;->b:LX/0qw;

    invoke-direct {p0, v0, v1, v2}, LX/0fz;->b(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V

    .line 109414
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v0}, LX/0qx;->a()V

    goto :goto_0

    :cond_1
    move-object v3, v2

    goto :goto_1
.end method

.method public final h()V
    .locals 7

    .prologue
    .line 109628
    invoke-virtual {p0}, LX/0fz;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 109629
    :cond_0
    :goto_0
    return-void

    .line 109630
    :cond_1
    iget-object v0, p0, LX/0fz;->i:LX/0qv;

    .line 109631
    iget-object v1, v0, LX/0qv;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 109632
    iget-object v1, p0, LX/0fz;->i:LX/0qv;

    .line 109633
    iget-object v2, v1, LX/0qv;->b:LX/0qw;

    move-object v1, v2

    .line 109634
    iget-object v2, p0, LX/0fz;->i:LX/0qv;

    .line 109635
    invoke-virtual {v2}, LX/0qv;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, LX/0qv;->c:Ljava/lang/String;

    iget-boolean v5, v2, LX/0qv;->d:Z

    iget-boolean v6, v2, LX/0qv;->e:Z

    invoke-static {v3, v4, v5, v6}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    move-object v2, v3

    .line 109636
    invoke-direct {p0, v0, v2, v1}, LX/0fz;->b(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V

    .line 109637
    iget-object v2, p0, LX/0fz;->i:LX/0qv;

    invoke-virtual {v2}, LX/0qv;->b()V

    .line 109638
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/0fz;->p:LX/22x;

    if-eqz v2, :cond_0

    .line 109639
    iget-object v2, p0, LX/0fz;->p:LX/22x;

    .line 109640
    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 109641
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 109642
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 109643
    instance-of v6, v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v6, :cond_2

    .line 109644
    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 109645
    :cond_3
    iget-object v3, v2, LX/22x;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v3, v3, Lcom/facebook/feed/data/FeedDataLoader;->N:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/23G;

    iget-object v5, v2, LX/22x;->a:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v5, v5, LX/0gD;->b:LX/0fz;

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v4, v6, v1}, LX/23G;->a(LX/0fz;Ljava/util/List;ZLX/0qw;)V

    .line 109646
    :cond_4
    goto :goto_0
.end method

.method public final i()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 109574
    iget-object v2, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v2}, LX/0qu;->b()I

    move-result v2

    if-nez v2, :cond_1

    .line 109575
    :cond_0
    :goto_0
    return v0

    .line 109576
    :cond_1
    const-string v2, "0"

    iget-object v3, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v3, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 109577
    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109578
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109579
    invoke-virtual {p0}, LX/0fz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109580
    iget-object v0, p0, LX/0fz;->i:LX/0qv;

    invoke-virtual {v0}, LX/0qv;->g()Ljava/lang/String;

    move-result-object v0

    .line 109581
    :goto_0
    invoke-static {v0}, LX/0fz;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109582
    :goto_1
    return-object v0

    .line 109583
    :cond_0
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    invoke-virtual {v0}, LX/0qx;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 109584
    iget-object v0, p0, LX/0fz;->j:LX/0qx;

    .line 109585
    const/4 v1, 0x0

    .line 109586
    iget-object v2, v0, LX/0qx;->c:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/0qx;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 109587
    iget-object v1, v0, LX/0qx;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v1

    .line 109588
    :cond_1
    move-object v0, v1

    .line 109589
    goto :goto_0

    .line 109590
    :cond_2
    iget-object v0, p0, LX/0fz;->l:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 109591
    iget-object v0, p0, LX/0fz;->l:Ljava/lang/String;

    .line 109592
    :goto_2
    move-object v0, v0

    .line 109593
    goto :goto_0

    .line 109594
    :cond_3
    invoke-direct {p0}, LX/0fz;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {p0}, LX/0fz;->A(LX/0fz;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 109595
    invoke-virtual {p0}, LX/0fz;->size()I

    move-result v0

    iget-object v1, p0, LX/0fz;->i:LX/0qv;

    invoke-virtual {v1}, LX/0qv;->f()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final l()LX/0qm;
    .locals 1

    .prologue
    .line 109596
    iget-object v0, p0, LX/0fz;->d:LX/0qm;

    return-object v0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 109597
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0fz;->b(LX/0fz;Z)V

    .line 109598
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 109599
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0fz;->b(LX/0fz;Z)V

    .line 109600
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 109601
    iget-object v0, p0, LX/0fz;->p:LX/22x;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 109602
    invoke-virtual {p0}, LX/0fz;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109603
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/0qu;->b(I)I

    move-result v1

    move v0, v1

    .line 109604
    invoke-virtual {p0, v0}, LX/0fz;->e(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109605
    const/4 v0, 0x0

    .line 109606
    iget-object v1, p0, LX/0fz;->m:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 109607
    iget-object v0, p0, LX/0fz;->m:Ljava/lang/String;

    .line 109608
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0fz;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109609
    :goto_1
    return-object v0

    .line 109610
    :cond_1
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v1

    if-lez v1, :cond_0

    .line 109611
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109612
    :cond_2
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 109613
    :goto_2
    if-ltz v1, :cond_4

    .line 109614
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0, v1}, LX/0qu;->d(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    .line 109615
    invoke-static {v0}, LX/0fz;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109616
    :goto_3
    move-object v0, v0

    .line 109617
    goto :goto_1

    .line 109618
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 109619
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 109620
    invoke-virtual {p0}, LX/0fz;->v()I

    move-result v0

    iget-object v1, p0, LX/0fz;->d:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 109621
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->d()Z

    move-result v0

    return v0
.end method

.method public final v()I
    .locals 1

    .prologue
    .line 109622
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    invoke-virtual {v0}, LX/0qu;->b()I

    move-result v0

    return v0
.end method

.method public final w()I
    .locals 2

    .prologue
    .line 109623
    iget-object v0, p0, LX/0fz;->g:LX/0qu;

    .line 109624
    invoke-static {v0}, LX/0qu;->j(LX/0qu;)V

    .line 109625
    invoke-virtual {v0}, LX/0qu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/0qu;->e:Ljava/util/List;

    const/4 p0, 0x0

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    move v0, v1

    .line 109626
    return v0

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 109627
    invoke-virtual {p0}, LX/0fz;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
