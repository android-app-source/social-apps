.class public LX/1QG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ps;


# instance fields
.field private a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

.field private b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

.field private c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244471
    return-void
.end method

.method public static a(LX/0QB;)LX/1QG;
    .locals 1

    .prologue
    .line 244467
    new-instance v0, LX/1QG;

    invoke-direct {v0}, LX/1QG;-><init>()V

    .line 244468
    move-object v0, v0

    .line 244469
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 244461
    iput-object p1, p0, LX/1QG;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 244462
    iput-object p2, p0, LX/1QG;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 244463
    iput-object p3, p0, LX/1QG;->c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 244464
    iput-object p4, p0, LX/1QG;->d:Ljava/lang/Object;

    .line 244465
    iput-object p5, p0, LX/1QG;->e:Ljava/lang/Object;

    .line 244466
    return-void
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 244460
    iget-object v0, p0, LX/1QG;->b:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 244455
    iget-object v0, p0, LX/1QG;->c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    instance-of v0, v0, Lcom/facebook/components/feed/StackComponentPartDefinition;

    if-nez v0, :cond_0

    .line 244456
    iget-object v0, p0, LX/1QG;->c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 244457
    :goto_0
    return-object v0

    .line 244458
    :cond_0
    iget-object v0, p0, LX/1QG;->e:Ljava/lang/Object;

    check-cast v0, LX/242;

    .line 244459
    iget-object v0, v0, LX/242;->c:LX/243;

    iget-object v0, v0, LX/243;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 244438
    iget-object v0, p0, LX/1QG;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    instance-of v0, v0, Lcom/facebook/components/feed/StackComponentPartDefinition;

    if-nez v0, :cond_0

    .line 244439
    iget-object v0, p0, LX/1QG;->d:Ljava/lang/Object;

    .line 244440
    :goto_0
    return-object v0

    .line 244441
    :cond_0
    iget-object v0, p0, LX/1QG;->d:Ljava/lang/Object;

    check-cast v0, LX/242;

    .line 244442
    iget-object v0, v0, LX/242;->d:LX/243;

    iget-object v0, v0, LX/243;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 244450
    iget-object v0, p0, LX/1QG;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    instance-of v0, v0, Lcom/facebook/components/feed/StackComponentPartDefinition;

    if-nez v0, :cond_0

    .line 244451
    iget-object v0, p0, LX/1QG;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 244452
    :goto_0
    return-object v0

    .line 244453
    :cond_0
    iget-object v0, p0, LX/1QG;->d:Ljava/lang/Object;

    check-cast v0, LX/242;

    .line 244454
    iget-object v0, v0, LX/242;->d:LX/243;

    iget-object v0, v0, LX/243;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 244445
    iget-object v0, p0, LX/1QG;->c:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    instance-of v0, v0, Lcom/facebook/components/feed/StackComponentPartDefinition;

    if-nez v0, :cond_0

    .line 244446
    iget-object v0, p0, LX/1QG;->e:Ljava/lang/Object;

    .line 244447
    :goto_0
    return-object v0

    .line 244448
    :cond_0
    iget-object v0, p0, LX/1QG;->e:Ljava/lang/Object;

    check-cast v0, LX/242;

    .line 244449
    iget-object v0, v0, LX/242;->c:LX/243;

    iget-object v0, v0, LX/243;->b:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final k()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 244443
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 244444
    return-void
.end method
