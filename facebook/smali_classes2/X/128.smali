.class public LX/128;
.super LX/121;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:LX/12M;

.field public final e:LX/0yH;

.field private final f:LX/12N;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173934
    const-class v0, LX/128;

    sput-object v0, LX/128;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/12M;LX/0yH;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 2
    .param p4    # LX/0Or;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/dialogprovider/DataControlWithoutUpsellDialogProvider;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/dialogprovider/NoDataControlNoUpsellDialogProvider;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/dialogprovider/UpsellWithDataControlDialogProvider;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/dialogprovider/UpsellWithoutDataControlDialogProvider;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/12M;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173924
    invoke-direct {p0}, LX/121;-><init>()V

    .line 173925
    iput-object p1, p0, LX/128;->c:Landroid/content/res/Resources;

    .line 173926
    iput-object p2, p0, LX/128;->d:LX/12M;

    .line 173927
    iput-object p3, p0, LX/128;->e:LX/0yH;

    .line 173928
    new-instance v0, LX/12N;

    invoke-direct {v0, p0}, LX/12N;-><init>(LX/128;)V

    iput-object v0, p0, LX/128;->f:LX/12N;

    .line 173929
    iput-object p4, p0, LX/128;->g:LX/0Or;

    .line 173930
    iput-object p5, p0, LX/128;->h:LX/0Or;

    .line 173931
    iput-object p6, p0, LX/128;->i:LX/0Or;

    .line 173932
    iput-object p7, p0, LX/128;->j:LX/0Or;

    .line 173933
    return-void
.end method

.method public static a(LX/0QB;)LX/128;
    .locals 1

    .prologue
    .line 173923
    invoke-static {p0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/128;
    .locals 8

    .prologue
    .line 173921
    new-instance v0, LX/128;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/12M;->a(LX/0QB;)LX/12M;

    move-result-object v2

    check-cast v2, LX/12M;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v3

    check-cast v3, LX/0yH;

    const/16 v4, 0x2565

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x3938

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2566

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2567

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/128;-><init>(Landroid/content/res/Resources;LX/12M;LX/0yH;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 173922
    return-object v0
.end method

.method private b(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;
    .locals 4

    .prologue
    .line 173935
    iget-object v0, p0, LX/128;->e:LX/0yH;

    .line 173936
    sget-object v1, LX/0yY;->NATIVE_UPSELL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    move v0, v1

    .line 173937
    invoke-static {p0}, LX/128;->e(LX/128;)Z

    move-result v1

    .line 173938
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 173939
    sget-object v0, LX/4g1;->UPSELL_WITH_DATA_CONTROL:LX/4g1;

    .line 173940
    :goto_0
    move-object v0, v0

    .line 173941
    sget-object v1, LX/6YH;->c:[I

    invoke-virtual {v0}, LX/4g1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 173942
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unsupported dialog state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173943
    :pswitch_0
    iget-object v0, p0, LX/128;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 173944
    :goto_1
    return-object v0

    .line 173945
    :pswitch_1
    iget-object v0, p0, LX/128;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_1

    .line 173946
    :pswitch_2
    iget-object v0, p0, LX/128;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_1

    .line 173947
    :pswitch_3
    iget-object v0, p0, LX/128;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_1

    .line 173948
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 173949
    sget-object v0, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    goto :goto_0

    .line 173950
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 173951
    sget-object v0, LX/4g1;->DATA_CONTROL_WITHOUT_UPSELL:LX/4g1;

    goto :goto_0

    .line 173952
    :cond_2
    if-nez v0, :cond_3

    if-nez v1, :cond_3

    .line 173953
    sget-object v0, LX/4g1;->NO_DATA_CONTROL_NO_UPSELL:LX/4g1;

    goto :goto_0

    .line 173954
    :cond_3
    sget-object v0, LX/4g1;->UNKOWN:LX/4g1;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(LX/128;)Z
    .locals 2

    .prologue
    .line 173920
    iget-object v0, p0, LX/128;->e:LX/0yH;

    sget-object v1, LX/0yY;->VPN_DATA_CONTROL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;
    .locals 8

    .prologue
    .line 173908
    sget-object v0, LX/6YH;->d:[I

    iget-object v1, p1, LX/3J6;->j:LX/2nT;

    invoke-virtual {v1}, LX/2nT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 173909
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported dialog"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173910
    :pswitch_0
    sget-object v2, LX/0yY;->DIALTONE_PHOTOCAP_SPINNER:LX/0yY;

    invoke-virtual {p3, v2}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->t:Z

    .line 173911
    iget-object v2, p1, LX/3J6;->b:Ljava/lang/String;

    sput-object v2, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->u:Ljava/lang/String;

    .line 173912
    iget-object v2, p1, LX/3J6;->c:Ljava/lang/String;

    sput-object v2, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->v:Ljava/lang/String;

    .line 173913
    sget-object v3, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->u:Ljava/lang/String;

    sget-object v4, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;->v:Ljava/lang/String;

    sget-object v6, LX/4g1;->UNKOWN:LX/4g1;

    iget-object v7, p1, LX/3J6;->h:Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    move-object v2, p3

    move-object v5, p2

    invoke-static/range {v2 .. v7}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;LX/4g1;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)Landroid/os/Bundle;

    move-result-object v2

    .line 173914
    new-instance v3, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;

    invoke-direct {v3}, Lcom/facebook/iorg/common/upsell/ui/SpinnerDialogFragment;-><init>()V

    .line 173915
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 173916
    move-object v0, v3

    .line 173917
    :goto_0
    return-object v0

    .line 173918
    :pswitch_1
    iget-object v0, p0, LX/128;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_0

    .line 173919
    :pswitch_2
    invoke-direct {p0, p1, p2, p3}, LX/128;->b(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 173905
    iget-object v0, p0, LX/128;->d:LX/12M;

    iget-object v1, p0, LX/128;->f:LX/12N;

    invoke-virtual {v1}, LX/12N;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(Ljava/lang/Class;)V

    .line 173906
    iget-object v0, p0, LX/128;->d:LX/12M;

    iget-object v1, p0, LX/128;->f:LX/12N;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 173907
    return-void
.end method

.method public final a(LX/2nT;LX/0yY;)Z
    .locals 1

    .prologue
    .line 173899
    iget-object v0, p0, LX/128;->e:LX/0yH;

    invoke-virtual {v0, p2}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173900
    const/4 v0, 0x0

    .line 173901
    :goto_0
    return v0

    .line 173902
    :cond_0
    sget-object v0, LX/2nT;->DATA_CONTROL_WITHOUT_UPSELL:LX/2nT;

    if-ne p1, v0, :cond_1

    .line 173903
    invoke-static {p0}, LX/128;->e(LX/128;)Z

    move-result v0

    goto :goto_0

    .line 173904
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 173898
    iget-object v0, p0, LX/128;->c:Landroid/content/res/Resources;

    const v1, 0x7f080e0e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
