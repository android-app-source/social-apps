.class public LX/1ro;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0aG;

.field public final c:LX/1dy;

.field public final d:LX/0tX;

.field public final e:LX/1rU;

.field private final f:LX/1dw;

.field public final g:LX/0Uh;

.field private h:LX/0xB;

.field private i:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 333112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/1ro;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/1dy;LX/0tX;LX/0xW;LX/1rU;LX/1dw;LX/0Uh;LX/0xB;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333227
    iput-object p1, p0, LX/1ro;->b:LX/0aG;

    .line 333228
    iput-object p2, p0, LX/1ro;->c:LX/1dy;

    .line 333229
    iput-object p3, p0, LX/1ro;->d:LX/0tX;

    .line 333230
    iput-object p5, p0, LX/1ro;->e:LX/1rU;

    .line 333231
    iput-object p6, p0, LX/1ro;->f:LX/1dw;

    .line 333232
    iput-object p7, p0, LX/1ro;->g:LX/0Uh;

    .line 333233
    iput-object p8, p0, LX/1ro;->h:LX/0xB;

    .line 333234
    invoke-virtual {p4}, LX/0xW;->G()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/1ro;->i:Ljava/lang/Long;

    .line 333235
    return-void
.end method

.method public static a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            ")",
            "LX/0jT;"
        }
    .end annotation

    .prologue
    .line 333236
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 333237
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333238
    new-instance v4, LX/BDC;

    invoke-direct {v4}, LX/BDC;-><init>()V

    .line 333239
    iput-object v0, v4, LX/BDC;->a:Ljava/lang/String;

    .line 333240
    move-object v0, v4

    .line 333241
    iput-object p1, v0, LX/BDC;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 333242
    move-object v0, v0

    .line 333243
    invoke-virtual {v0}, LX/BDC;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333244
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333245
    :cond_0
    new-instance v0, LX/BBz;

    invoke-direct {v0}, LX/BBz;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 333246
    iput-object v1, v0, LX/BBz;->a:LX/0Px;

    .line 333247
    move-object v0, v0

    .line 333248
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 333249
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 333250
    iget-object v6, v0, LX/BBz;->a:LX/0Px;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 333251
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 333252
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 333253
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 333254
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 333255
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 333256
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 333257
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 333258
    new-instance v6, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$MarkNotificationSeenMutationModel;

    invoke-direct {v6, v5}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$MarkNotificationSeenMutationModel;-><init>(LX/15i;)V

    .line 333259
    move-object v0, v6

    .line 333260
    return-object v0
.end method

.method public static a(LX/0QB;)LX/1ro;
    .locals 1

    .prologue
    .line 333267
    invoke-static {p0}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1ro;LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Lcom/facebook/graphql/enums/GraphQLStorySeenState;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333261
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333262
    const-string v1, "graphNotifsUpdateSeenStatePrams"

    new-instance v2, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    invoke-direct {v2, p1, p2}, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;-><init>(Ljava/util/Collection;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 333263
    if-eqz p3, :cond_0

    .line 333264
    const-string v1, "overridden_viewer_context"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 333265
    :cond_0
    iget-object v1, p0, LX/1ro;->b:LX/0aG;

    const-string v2, "graphNotifUpdateSeenStateOnlyOnServer"

    const v3, -0x101da375

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    move-object v0, v0

    .line 333266
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1ro;
    .locals 9

    .prologue
    .line 333219
    new-instance v0, LX/1ro;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v2

    check-cast v2, LX/1dy;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v4

    check-cast v4, LX/0xW;

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v5

    check-cast v5, LX/1rU;

    invoke-static {p0}, LX/1dw;->a(LX/0QB;)LX/1dw;

    move-result-object v6

    check-cast v6, LX/1dw;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/0xB;->a(LX/0QB;)LX/0xB;

    move-result-object v8

    check-cast v8, LX/0xB;

    invoke-direct/range {v0 .. v8}, LX/1ro;-><init>(LX/0aG;LX/1dy;LX/0tX;LX/0xW;LX/1rU;LX/1dw;LX/0Uh;LX/0xB;)V

    .line 333220
    return-object v0
.end method

.method private static d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 333221
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 333222
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 333223
    sget-object v1, LX/1ro;->a:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 333224
    sget-object v1, LX/1ro;->a:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333225
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333177
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 333178
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    .line 333179
    new-instance v4, LX/BDA;

    invoke-direct {v4}, LX/BDA;-><init>()V

    new-instance v5, LX/BDC;

    invoke-direct {v5}, LX/BDC;-><init>()V

    iget-object v6, v0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    .line 333180
    iput-object v6, v5, LX/BDC;->a:Ljava/lang/String;

    .line 333181
    move-object v5, v5

    .line 333182
    iget-object v0, v0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 333183
    iput-object v0, v5, LX/BDC;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 333184
    move-object v0, v5

    .line 333185
    invoke-virtual {v0}, LX/BDC;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    .line 333186
    iput-object v0, v4, LX/BDA;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    .line 333187
    move-object v0, v4

    .line 333188
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 333189
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 333190
    iget-object v8, v0, LX/BDA;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 333191
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 333192
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 333193
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 333194
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 333195
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 333196
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 333197
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 333198
    new-instance v8, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;

    invoke-direct {v8, v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;-><init>(LX/15i;)V

    .line 333199
    move-object v0, v8

    .line 333200
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333202
    :cond_0
    new-instance v0, LX/BD9;

    invoke-direct {v0}, LX/BD9;-><init>()V

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 333203
    iput-object v1, v0, LX/BD9;->a:LX/0Px;

    .line 333204
    move-object v0, v0

    .line 333205
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 333206
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 333207
    iget-object v8, v0, LX/BD9;->a:LX/0Px;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 333208
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 333209
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 333210
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 333211
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 333212
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 333213
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 333214
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 333215
    new-instance v8, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel;

    invoke-direct {v8, v7}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel;-><init>(LX/15i;)V

    .line 333216
    move-object v0, v8

    .line 333217
    iget-object v1, p0, LX/1ro;->c:LX/1dy;

    invoke-virtual {v1, v0}, LX/1dy;->a(LX/0jT;)V

    .line 333218
    return-void
.end method

.method public final a(LX/0Px;Ljava/lang/Long;)V
    .locals 4
    .param p2    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333167
    const/4 v0, 0x0

    .line 333168
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333169
    :goto_0
    return-void

    .line 333170
    :cond_0
    invoke-virtual {p0, p1}, LX/1ro;->a(Ljava/util/List;)V

    .line 333171
    iget-object v1, p0, LX/1ro;->e:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 333172
    invoke-virtual {p0, p1, p2}, LX/1ro;->b(LX/0Px;Ljava/lang/Long;)V

    goto :goto_0

    .line 333173
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, p1, v1, v0}, LX/1ro;->a(LX/1ro;LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333174
    iget-object v1, p0, LX/1ro;->g:LX/0Uh;

    const/16 v2, 0x406

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 333175
    iget-object v1, p0, LX/1ro;->c:LX/1dy;

    new-instance v2, LX/2rR;

    invoke-direct {v2, p1}, LX/2rR;-><init>(LX/0Px;)V

    invoke-virtual {v1, v2}, LX/1dy;->a(LX/4VT;)V

    goto :goto_0

    .line 333176
    :cond_2
    iget-object v1, p0, LX/1ro;->c:LX/1dy;

    new-instance v2, LX/BE7;

    invoke-direct {v2, p1}, LX/BE7;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, LX/1dy;->a(LX/3Bq;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 333142
    invoke-static {p1}, LX/1ro;->d(Ljava/lang/String;)V

    .line 333143
    new-instance v0, LX/BDC;

    invoke-direct {v0}, LX/BDC;-><init>()V

    .line 333144
    iput-object p1, v0, LX/BDC;->a:Ljava/lang/String;

    .line 333145
    move-object v0, v0

    .line 333146
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 333147
    iput-object v1, v0, LX/BDC;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 333148
    move-object v0, v0

    .line 333149
    invoke-virtual {v0}, LX/BDC;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    .line 333150
    iget-object v1, p0, LX/1ro;->e:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333151
    new-instance v0, LX/4HP;

    invoke-direct {v0}, LX/4HP;-><init>()V

    .line 333152
    invoke-virtual {v0, p1}, LX/4HP;->a(Ljava/lang/String;)LX/4HP;

    .line 333153
    const-string v1, "android-jewel"

    invoke-virtual {v0, v1}, LX/4HP;->b(Ljava/lang/String;)LX/4HP;

    .line 333154
    iget-object v1, p0, LX/1ro;->e:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 333155
    new-instance v1, LX/BBw;

    invoke-direct {v1}, LX/BBw;-><init>()V

    move-object v1, v1

    .line 333156
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 333157
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v2, v3}, LX/1ro;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 333158
    iget-object v2, p0, LX/1ro;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333159
    :goto_0
    return-void

    .line 333160
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, LX/1ro;->a(LX/1ro;LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333161
    iget-object v1, p0, LX/1ro;->c:LX/1dy;

    invoke-virtual {v1, v0}, LX/1dy;->a(LX/0jT;)V

    goto :goto_0

    .line 333162
    :cond_1
    new-instance v1, LX/BBu;

    invoke-direct {v1}, LX/BBu;-><init>()V

    move-object v1, v1

    .line 333163
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 333164
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v2, v3}, LX/1ro;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 333165
    iget-object v2, p0, LX/1ro;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333166
    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333138
    iget-object v0, p0, LX/1ro;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 333139
    :cond_0
    return-void

    .line 333140
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333141
    invoke-static {v0}, LX/1ro;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/0Px;Ljava/lang/Long;)V
    .locals 5
    .param p2    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 333113
    new-instance v0, LX/4KQ;

    invoke-direct {v0}, LX/4KQ;-><init>()V

    .line 333114
    const-string v1, "seen"

    invoke-virtual {v0, v1}, LX/4KQ;->a(Ljava/lang/String;)LX/4KQ;

    .line 333115
    invoke-virtual {v0, p1}, LX/4KQ;->a(Ljava/util/List;)LX/4KQ;

    .line 333116
    const-string v1, "android-jewel"

    invoke-virtual {v0, v1}, LX/4KQ;->b(Ljava/lang/String;)LX/4KQ;

    .line 333117
    iget-object v1, p0, LX/1ro;->h:LX/0xB;

    sget-object v2, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 333118
    const-string v2, "tab_badge_count"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 333119
    iget-object v1, p0, LX/1ro;->h:LX/0xB;

    sget-object v2, LX/12j;->NOTIFICATIONS:LX/12j;

    invoke-virtual {v1, v2}, LX/0xB;->a(LX/12j;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 333120
    const-string v2, "app_badge_count"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 333121
    iget-object v1, p0, LX/1ro;->e:LX/1rU;

    .line 333122
    iget-object v2, v1, LX/1rU;->a:LX/0xW;

    .line 333123
    iget-object v3, v2, LX/0xW;->a:LX/0ad;

    sget-short v4, LX/0xc;->w:S

    const/4 v1, 0x0

    invoke-interface {v3, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    move v2, v3

    .line 333124
    move v1, v2

    .line 333125
    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 333126
    invoke-virtual {p2}, Ljava/lang/Long;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4KQ;->a(Ljava/lang/Integer;)LX/4KQ;

    .line 333127
    :cond_0
    iget-object v1, p0, LX/1ro;->e:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 333128
    new-instance v1, LX/BBx;

    invoke-direct {v1}, LX/BBx;-><init>()V

    move-object v1, v1

    .line 333129
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 333130
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p1, v2}, LX/1ro;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 333131
    iget-object v2, p0, LX/1ro;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333132
    :goto_0
    return-void

    .line 333133
    :cond_1
    new-instance v1, LX/BBv;

    invoke-direct {v1}, LX/BBv;-><init>()V

    move-object v1, v1

    .line 333134
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 333135
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p1, v2}, LX/1ro;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 333136
    iget-object v2, p0, LX/1ro;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 333137
    goto :goto_0
.end method
