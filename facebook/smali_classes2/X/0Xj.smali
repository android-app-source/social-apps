.class public LX/0Xj;
.super LX/0Xk;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Xp;


# direct methods
.method public constructor <init>(LX/0Xp;LX/0Sk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79357
    invoke-direct {p0, p2}, LX/0Xk;-><init>(LX/0Sk;)V

    .line 79358
    iput-object p1, p0, LX/0Xj;->a:LX/0Xp;

    .line 79359
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 79360
    invoke-static {p1}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/0Xj;-><init>(LX/0Xp;LX/0Sk;)V

    .line 79361
    return-void
.end method

.method public static a(LX/0QB;)LX/0Xj;
    .locals 5

    .prologue
    .line 79362
    const-class v1, LX/0Xj;

    monitor-enter v1

    .line 79363
    :try_start_0
    sget-object v0, LX/0Xj;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 79364
    sput-object v2, LX/0Xj;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 79365
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79366
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 79367
    new-instance p0, LX/0Xj;

    invoke-static {v0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v3

    check-cast v3, LX/0Xp;

    invoke-static {v0}, LX/0Sk;->a(LX/0QB;)LX/0Sk;

    move-result-object v4

    check-cast v4, LX/0Sk;

    invoke-direct {p0, v3, v4}, LX/0Xj;-><init>(LX/0Xp;LX/0Sk;)V

    .line 79368
    move-object v0, p0

    .line 79369
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 79370
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0Xj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79371
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 79372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/BroadcastReceiver;)V
    .locals 1

    .prologue
    .line 79373
    iget-object v0, p0, LX/0Xj;->a:LX/0Xp;

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;)V

    .line 79374
    return-void
.end method

.method public final a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Handler;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79375
    iget-object v0, p0, LX/0Xj;->a:LX/0Xp;

    .line 79376
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p0

    :goto_0
    invoke-virtual {v0, p1, p2, p0}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Looper;)V

    .line 79377
    return-void

    .line 79378
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 79379
    iget-object v0, p0, LX/0Xj;->a:LX/0Xp;

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 79380
    return-void
.end method
