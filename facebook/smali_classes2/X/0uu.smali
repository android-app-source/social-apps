.class public LX/0uu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 157356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157357
    return-void
.end method

.method public static a()LX/0uw;
    .locals 1

    .prologue
    .line 157348
    new-instance v0, LX/0uv;

    invoke-direct {v0}, LX/0uv;-><init>()V

    return-object v0
.end method

.method public static varargs a([LX/0ux;)LX/0uw;
    .locals 4

    .prologue
    .line 157349
    new-instance v1, LX/0uv;

    invoke-direct {v1}, LX/0uv;-><init>()V

    .line 157350
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 157351
    invoke-virtual {v1, v3}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 157352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157353
    :cond_0
    return-object v1
.end method

.method public static a(LX/0ux;)LX/0ux;
    .locals 1

    .prologue
    .line 157354
    new-instance v0, LX/2RZ;

    invoke-direct {v0, p0}, LX/2RZ;-><init>(LX/0ux;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157359
    new-instance v0, LX/2RY;

    invoke-direct {v0, p0}, LX/2RY;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157355
    new-instance v0, LX/0uy;

    invoke-direct {v0, p0, p1}, LX/0uy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<*>;)",
            "LX/0ux;"
        }
    .end annotation

    .prologue
    .line 157333
    new-instance v0, LX/2RX;

    invoke-direct {v0, p0, p1}, LX/2RX;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157358
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public static b()LX/0uw;
    .locals 1

    .prologue
    .line 157342
    new-instance v0, LX/236;

    invoke-direct {v0}, LX/236;-><init>()V

    return-object v0
.end method

.method public static varargs b([LX/0ux;)LX/0uw;
    .locals 4

    .prologue
    .line 157343
    new-instance v1, LX/236;

    invoke-direct {v1}, LX/236;-><init>()V

    .line 157344
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 157345
    invoke-virtual {v1, v3}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 157346
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157347
    :cond_0
    return-object v1
.end method

.method public static b(Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157341
    new-instance v0, LX/48y;

    invoke-direct {v0, p0}, LX/48y;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157340
    new-instance v0, LX/23I;

    invoke-direct {v0, p0, p1}, LX/23I;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<*>;)",
            "LX/0ux;"
        }
    .end annotation

    .prologue
    .line 157339
    new-instance v0, LX/2RX;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, LX/2RX;-><init>(Ljava/lang/String;Ljava/util/Collection;Z)V

    return-object v0
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157338
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/0uu;->b(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157337
    new-instance v0, LX/23J;

    invoke-direct {v0, p0, p1}, LX/23J;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157336
    new-instance v0, LX/48x;

    invoke-direct {v0, p0, p1}, LX/48x;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157335
    new-instance v0, LX/48w;

    invoke-direct {v0, p0, p1}, LX/48w;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;
    .locals 1

    .prologue
    .line 157334
    new-instance v0, LX/0v0;

    invoke-direct {v0, p0, p1}, LX/0v0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
