.class public LX/0pn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile B:LX/0pn;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field public A:LX/1fO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final g:LX/0pk;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0SG;

.field public final j:LX/0oy;

.field private final k:LX/0ox;

.field private l:LX/15j;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final m:Ljava/io/File;

.field private final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0qN;

.field private final p:LX/0ad;

.field private final q:LX/0pJ;

.field private final r:LX/0qO;

.field private final s:LX/0qT;

.field public final t:LX/0qP;

.field private final u:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final v:LX/0qR;

.field public final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Uh;

.field private final y:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final z:LX/0qS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 146195
    const-class v0, LX/0pn;

    sput-object v0, LX/0pn;->a:Ljava/lang/Class;

    .line 146196
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, LX/0pp;->b:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sget-object v1, LX/0pp;->c:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, LX/0pp;->d:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/0pp;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/0pp;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0pp;->g:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0pp;->h:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0pp;->k:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0pp;->l:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0pp;->m:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0pp;->n:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0pp;->o:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0pp;->p:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0pp;->q:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0pp;->r:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0pp;->s:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0pp;->t:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0pp;->u:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0pp;->w:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0pp;->x:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0pp;->i:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0pp;->j:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/0pp;->y:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/0pp;->z:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/0pn;->b:[Ljava/lang/String;

    .line 146197
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, LX/0pp;->c:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, LX/0pp;->w:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, LX/0pn;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/performancelogger/PerformanceLogger;LX/0pi;LX/0pq;LX/0Ot;LX/0SG;LX/0oy;LX/0ox;Ljava/io/File;LX/0qN;LX/0ad;LX/0pJ;LX/0qO;LX/0qP;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0qR;LX/0Uh;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0qS;)V
    .locals 4
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/api/feed/annotation/FeedDbCacheSize;
        .end annotation
    .end param
    .param p9    # Ljava/io/File;
        .annotation runtime Lcom/facebook/api/feedcache/db/storage/FeedCacheStorageDirectory;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0pi;",
            "LX/0pq;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0SG;",
            "LX/0oy;",
            "LX/0ox;",
            "Ljava/io/File;",
            "LX/0qN;",
            "LX/0ad;",
            "LX/0pJ;",
            "LX/0qO;",
            "LX/0qP;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0qR;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;",
            "LX/0qS;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146171
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/0pn;->d:Ljava/util/List;

    .line 146172
    new-instance v1, LX/0qT;

    invoke-direct {v1, p0}, LX/0qT;-><init>(LX/0pn;)V

    iput-object v1, p0, LX/0pn;->s:LX/0qT;

    .line 146173
    iput-object p11, p0, LX/0pn;->p:LX/0ad;

    .line 146174
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0pn;->q:LX/0pJ;

    .line 146175
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ot;

    iput-object v1, p0, LX/0pn;->e:LX/0Ot;

    .line 146176
    iput-object p2, p0, LX/0pn;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 146177
    const-string v1, "newsfeed"

    invoke-virtual {p3, v1}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iput-object v1, p0, LX/0pn;->g:LX/0pk;

    .line 146178
    invoke-virtual {p4, p0}, LX/0pq;->a(LX/0po;)V

    .line 146179
    iput-object p5, p0, LX/0pn;->h:LX/0Ot;

    .line 146180
    iput-object p6, p0, LX/0pn;->i:LX/0SG;

    .line 146181
    iput-object p7, p0, LX/0pn;->j:LX/0oy;

    .line 146182
    iput-object p9, p0, LX/0pn;->m:Ljava/io/File;

    .line 146183
    iput-object p8, p0, LX/0pn;->k:LX/0ox;

    .line 146184
    const/4 v1, 0x1

    invoke-static {v1}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/0pn;->n:Ljava/util/ArrayList;

    .line 146185
    iput-object p10, p0, LX/0pn;->o:LX/0qN;

    .line 146186
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0pn;->r:LX/0qO;

    .line 146187
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0pn;->t:LX/0qP;

    .line 146188
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0pn;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 146189
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0pn;->v:LX/0qR;

    .line 146190
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0pn;->x:LX/0Uh;

    .line 146191
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0pn;->y:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 146192
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0pn;->w:LX/0Ot;

    .line 146193
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0pn;->z:LX/0qS;

    .line 146194
    return-void
.end method

.method private static a(ILX/15i;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 146161
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    .line 146162
    if-nez v2, :cond_0

    .line 146163
    const-string v1, "FlatBuffer"

    const-string v3, "root viewer object contains no edges"

    invoke-static {v1, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146164
    :cond_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 146165
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    const/4 v4, 0x5

    invoke-virtual {p1, v0, v4}, LX/15i;->g(II)I

    move-result v0

    .line 146166
    const/4 v4, 0x1

    invoke-virtual {p1, v0, v4}, LX/15i;->g(II)I

    move-result v4

    .line 146167
    if-eq p0, v4, :cond_1

    .line 146168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146169
    :cond_1
    return v0
.end method

.method private static a(ILX/15i;Lcom/facebook/graphql/model/GraphQLViewer;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 146150
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 146151
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v1

    move-object v2, v1

    .line 146152
    :goto_0
    if-nez v2, :cond_0

    .line 146153
    const-string v1, "FlatBuffer"

    const-string v3, "root viewer object contains no edges"

    invoke-static {v1, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146154
    :cond_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pT;

    .line 146155
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v0

    const/4 v4, 0x6

    invoke-virtual {p1, v0, v4}, LX/15i;->g(II)I

    move-result v0

    .line 146156
    const/4 v4, 0x1

    invoke-virtual {p1, v0, v4}, LX/15i;->g(II)I

    move-result v4

    .line 146157
    if-eq p0, v4, :cond_2

    .line 146158
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 146159
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;->a()LX/0Px;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 146160
    :cond_2
    return v0
.end method

.method private a(ILcom/facebook/api/feedtype/FeedType;)I
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 146139
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 146140
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146141
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 146142
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146143
    sget-object v1, LX/0pp;->l:LX/0U1;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146144
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "COUNT()"

    aput-object v3, v2, v8

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146145
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146146
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 146147
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 146148
    return v0

    .line 146149
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v8

    goto :goto_0
.end method

.method public static a(LX/0pn;LX/0U1;Ljava/lang/String;II)I
    .locals 6

    .prologue
    .line 146122
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 146123
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 146124
    sget-object v0, LX/0pp;->i:LX/0U1;

    .line 146125
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 146126
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146127
    sget-object v0, LX/0pp;->j:LX/0U1;

    .line 146128
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 146129
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146130
    const/4 v0, 0x0

    .line 146131
    iget-object v3, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v3

    .line 146132
    invoke-static {v3, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 146133
    :try_start_0
    const-string v4, "home_stories"

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 146134
    if-nez v0, :cond_0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146135
    :cond_0
    :goto_0
    return v0

    .line 146136
    :catch_0
    invoke-virtual {p0}, LX/0pn;->U_()V

    goto :goto_0

    .line 146137
    :catch_1
    move-exception v1

    .line 146138
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Update operation failed!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;IZ)I
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 145814
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145815
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145816
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145817
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145818
    sget-object v1, LX/0pp;->i:LX/0U1;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145819
    if-nez p3, :cond_0

    .line 145820
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v1

    .line 145821
    sget-object v2, LX/0pp;->n:LX/0U1;

    const-string v3, "Page"

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145822
    sget-object v2, LX/0pp;->n:LX/0U1;

    const-string v3, "User"

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145823
    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145824
    :cond_0
    sget-object v1, LX/0pp;->l:LX/0U1;

    const-string v2, "0"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145825
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "COUNT("

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LX/0pp;->i:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ")"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145826
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145827
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 145828
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145829
    return v0

    .line 145830
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method private a(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 146102
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 146103
    const v0, 0x43787018

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move v2, v1

    .line 146104
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 146105
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146106
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 146107
    sget-object v5, LX/0pp;->i:LX/0U1;

    .line 146108
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 146109
    const-string v6, "1"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146110
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 146111
    sget-object v6, LX/0pp;->d:LX/0U1;

    invoke-virtual {v6, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146112
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {p2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146113
    const-string v0, "home_stories"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 146114
    add-int/2addr v2, v0

    .line 146115
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 146116
    :cond_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146117
    const v0, 0x419576b2

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146118
    return v2

    .line 146119
    :catchall_0
    move-exception v0

    const v1, -0x16f5bb29

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;
    .locals 56
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146016
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 146017
    const-string v5, "home_stories"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146018
    const/4 v13, 0x0

    .line 146019
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v14

    .line 146020
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0v1;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, LX/0pn;->b:[Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {p0 .. p0}, LX/0pn;->k()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v11, p3

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 146021
    :try_start_1
    sget-object v5, LX/0pp;->a:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 146022
    sget-object v5, LX/0pp;->b:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 146023
    sget-object v5, LX/0pp;->c:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 146024
    sget-object v5, LX/0pp;->d:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 146025
    sget-object v5, LX/0pp;->e:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 146026
    sget-object v5, LX/0pp;->f:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 146027
    sget-object v5, LX/0pp;->g:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 146028
    sget-object v5, LX/0pp;->h:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 146029
    sget-object v5, LX/0pp;->k:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 146030
    sget-object v5, LX/0pp;->l:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 146031
    sget-object v5, LX/0pp;->o:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v20

    .line 146032
    sget-object v5, LX/0pp;->p:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v21

    .line 146033
    sget-object v5, LX/0pp;->q:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v22

    .line 146034
    sget-object v5, LX/0pp;->r:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v23

    .line 146035
    sget-object v5, LX/0pp;->s:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v24

    .line 146036
    sget-object v5, LX/0pp;->t:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v25

    .line 146037
    sget-object v5, LX/0pp;->u:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v26

    .line 146038
    sget-object v5, LX/0pp;->v:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v27

    .line 146039
    sget-object v5, LX/0pp;->w:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v28

    .line 146040
    sget-object v5, LX/0pp;->n:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v29

    .line 146041
    sget-object v5, LX/0pp;->i:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v30

    .line 146042
    sget-object v5, LX/0pp;->j:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v31

    .line 146043
    sget-object v5, LX/0pp;->x:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v32

    .line 146044
    sget-object v5, LX/0pp;->y:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v33

    .line 146045
    sget-object v5, LX/0pp;->z:LX/0U1;

    invoke-virtual {v5, v4}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v34

    .line 146046
    const/4 v5, 0x0

    .line 146047
    :cond_0
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 146048
    const/4 v6, 0x2

    move/from16 v0, v19

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v6, v7, :cond_2

    .line 146049
    if-eqz v5, :cond_0

    .line 146050
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->b(Z)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146051
    :catch_0
    move-object v5, v4

    :goto_1
    :try_start_2
    invoke-static {}, LX/0Px;->of()LX/0Px;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 146052
    if-eqz v5, :cond_1

    .line 146053
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 146054
    :cond_1
    :goto_2
    return-object v4

    .line 146055
    :cond_2
    :try_start_3
    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 146056
    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v36

    .line 146057
    move/from16 v0, v22

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    .line 146058
    move/from16 v0, v23

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    .line 146059
    move/from16 v0, v26

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    const/4 v5, 0x1

    move v8, v5

    .line 146060
    :goto_3
    const/4 v6, 0x0

    .line 146061
    const/4 v5, 0x0

    .line 146062
    if-eqz v8, :cond_c

    .line 146063
    move/from16 v0, v24

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    .line 146064
    if-eqz v7, :cond_3

    array-length v0, v7

    move/from16 v39, v0

    if-lez v39, :cond_3

    .line 146065
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 146066
    :cond_3
    move/from16 v0, v25

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    .line 146067
    if-eqz v7, :cond_b

    array-length v0, v7

    move/from16 v39, v0

    if-lez v39, :cond_b

    .line 146068
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    move-object v7, v6

    move-object v6, v5

    .line 146069
    :goto_4
    invoke-interface {v4, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 146070
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 146071
    move/from16 v0, v27

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 146072
    move/from16 v0, v28

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 146073
    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v43

    .line 146074
    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 146075
    move/from16 v0, v17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/16 v45, 0x1

    move/from16 v0, v45

    if-ne v5, v0, :cond_8

    const/4 v5, 0x1

    .line 146076
    :goto_5
    move/from16 v0, v34

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v45 .. v45}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v45

    .line 146077
    invoke-interface {v4, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v46

    .line 146078
    move/from16 v0, v29

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v48

    .line 146079
    move/from16 v0, v30

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v49

    .line 146080
    move/from16 v0, v31

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v50

    .line 146081
    move/from16 v0, v33

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v52

    .line 146082
    new-instance v51, LX/0x0;

    invoke-direct/range {v51 .. v51}, LX/0x0;-><init>()V

    move-object/from16 v0, v51

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/0x0;->a(Ljava/lang/String;)LX/0x0;

    move-result-object v40

    invoke-interface {v4, v15}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v54

    move-object/from16 v0, v40

    move-wide/from16 v1, v54

    invoke-virtual {v0, v1, v2}, LX/0x0;->a(D)LX/0x0;

    move-result-object v40

    move/from16 v0, v16

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, v40

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, LX/0x0;->i(Ljava/lang/String;)LX/0x0;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v5}, LX/0x0;->b(Z)LX/0x0;

    move-result-object v5

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, LX/0x0;->b(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v45

    invoke-virtual {v5, v0}, LX/0x0;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, LX/0x0;->c(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v42

    invoke-virtual {v5, v0}, LX/0x0;->d(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, LX/0x0;->e(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-virtual {v5, v0}, LX/0x0;->f(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move/from16 v0, v36

    invoke-virtual {v5, v0}, LX/0x0;->b(I)LX/0x0;

    move-result-object v5

    move/from16 v0, v37

    invoke-virtual {v5, v0}, LX/0x0;->d(I)LX/0x0;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, LX/0x0;->c(I)LX/0x0;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/0x0;->a(Z)LX/0x0;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/0x0;->a(Ljava/nio/ByteBuffer;)LX/0x0;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0x0;->b(Ljava/nio/ByteBuffer;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v43

    invoke-virtual {v5, v0}, LX/0x0;->g(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-wide/from16 v0, v46

    invoke-virtual {v5, v0, v1}, LX/0x0;->a(J)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v5, v0}, LX/0x0;->h(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move/from16 v0, v18

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v5, v6}, LX/0x0;->e(I)LX/0x0;

    move-result-object v5

    move/from16 v0, v49

    invoke-virtual {v5, v0}, LX/0x0;->f(I)LX/0x0;

    move-result-object v5

    move/from16 v0, v50

    invoke-virtual {v5, v0}, LX/0x0;->a(I)LX/0x0;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0x0;->a(LX/15j;)LX/0x0;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->o:LX/0qN;

    invoke-virtual {v5, v6}, LX/0x0;->a(LX/0qN;)LX/0x0;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/0x0;->c(Z)LX/0x0;

    move-result-object v5

    move-object/from16 v0, v44

    invoke-virtual {v5, v0}, LX/0x0;->j(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    move-wide/from16 v0, v52

    invoke-virtual {v5, v0, v1}, LX/0x0;->b(J)LX/0x0;

    move-result-object v5

    invoke-virtual {v5}, LX/0x0;->a()Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v5

    .line 146083
    if-eqz v39, :cond_4

    .line 146084
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->t:LX/0qP;

    move-object/from16 v0, v39

    invoke-virtual {v6, v0}, LX/0qP;->a(Ljava/lang/String;)LX/14t;

    move-result-object v6

    .line 146085
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0pn;->v:LX/0qR;

    move-object/from16 v0, v39

    invoke-virtual {v7, v0, v6}, LX/0qR;->a(Ljava/lang/String;LX/14t;)V

    .line 146086
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->v:LX/0qR;

    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-virtual {v6, v0, v1}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146087
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/0eJ;->l:LX/0Tn;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 146088
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    .line 146089
    if-eqz v6, :cond_5

    .line 146090
    const/4 v7, 0x1

    invoke-static {v6, v7}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Z)V

    .line 146091
    invoke-static/range {v49 .. v49}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 146092
    :cond_5
    invoke-virtual {v14, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 146093
    :catchall_0
    move-exception v5

    move-object v13, v4

    move-object v4, v5

    :goto_6
    if-eqz v13, :cond_6

    .line 146094
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4

    .line 146095
    :cond_7
    const/4 v5, 0x0

    move v8, v5

    goto/16 :goto_3

    .line 146096
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 146097
    :cond_9
    if-eqz v4, :cond_a

    .line 146098
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 146099
    :cond_a
    invoke-virtual {v14}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_2

    .line 146100
    :catchall_1
    move-exception v4

    goto :goto_6

    :catchall_2
    move-exception v4

    move-object v13, v5

    goto :goto_6

    .line 146101
    :catch_1
    move-object v5, v13

    goto/16 :goto_1

    :cond_b
    move-object v7, v6

    move-object v6, v5

    goto/16 :goto_4

    :cond_c
    move-object v7, v6

    move-object v6, v5

    goto/16 :goto_4
.end method

.method public static a(LX/0QB;)LX/0pn;
    .locals 3

    .prologue
    .line 146006
    sget-object v0, LX/0pn;->B:LX/0pn;

    if-nez v0, :cond_1

    .line 146007
    const-class v1, LX/0pn;

    monitor-enter v1

    .line 146008
    :try_start_0
    sget-object v0, LX/0pn;->B:LX/0pn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146009
    if-eqz v2, :cond_0

    .line 146010
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0pn;->b(LX/0QB;)LX/0pn;

    move-result-object v0

    sput-object v0, LX/0pn;->B:LX/0pn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146011
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146012
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146013
    :cond_1
    sget-object v0, LX/0pn;->B:LX/0pn;

    return-object v0

    .line 146014
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0pn;Landroid/database/Cursor;Lcom/facebook/api/feed/FetchFeedParams;)LX/23A;
    .locals 54

    .prologue
    .line 145920
    sget-object v4, LX/0pp;->b:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 145921
    sget-object v4, LX/0pp;->c:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 145922
    sget-object v4, LX/0pp;->a:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v14

    .line 145923
    sget-object v4, LX/0pp;->d:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 145924
    sget-object v4, LX/0pp;->e:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 145925
    sget-object v4, LX/0pp;->f:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 145926
    sget-object v4, LX/0pp;->g:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 145927
    sget-object v4, LX/0pp;->h:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 145928
    sget-object v4, LX/0pp;->k:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v20

    .line 145929
    sget-object v4, LX/0pp;->l:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v21

    .line 145930
    sget-object v4, LX/0pp;->n:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v22

    .line 145931
    sget-object v4, LX/0pp;->o:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v23

    .line 145932
    sget-object v4, LX/0pp;->p:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v24

    .line 145933
    sget-object v4, LX/0pp;->q:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v25

    .line 145934
    sget-object v4, LX/0pp;->r:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v26

    .line 145935
    sget-object v4, LX/0pp;->s:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v27

    .line 145936
    sget-object v4, LX/0pp;->t:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v28

    .line 145937
    sget-object v4, LX/0pp;->u:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v29

    .line 145938
    sget-object v4, LX/0pp;->v:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v30

    .line 145939
    sget-object v4, LX/0pp;->w:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v31

    .line 145940
    sget-object v4, LX/0pp;->i:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v32

    .line 145941
    sget-object v4, LX/0pp;->j:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v33

    .line 145942
    sget-object v4, LX/0pp;->x:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v34

    .line 145943
    sget-object v4, LX/0pp;->y:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v35

    .line 145944
    sget-object v4, LX/0pp;->z:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v36

    .line 145945
    const-wide/16 v4, -0x1

    .line 145946
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v37

    move-wide v10, v4

    .line 145947
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 145948
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v4, v5, :cond_3

    .line 145949
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/api/feed/FetchFeedParams;->o()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 145950
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Database_Gap"

    invoke-static/range {v4 .. v9}, LX/239;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;

    move-result-object v4

    .line 145951
    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145952
    :catchall_0
    move-exception v4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    throw v4

    .line 145953
    :cond_1
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/api/feed/FetchFeedParams;->i()LX/0gf;

    move-result-object v4

    sget-object v5, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    if-eq v4, v5, :cond_0

    .line 145954
    :cond_2
    invoke-virtual/range {v37 .. v37}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 145955
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 145956
    sget-object v4, LX/23A;->a:LX/23A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145957
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v4

    .line 145958
    :cond_3
    :try_start_2
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 145959
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 145960
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v38

    .line 145961
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v39

    .line 145962
    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    move v7, v4

    .line 145963
    :goto_2
    const/4 v5, 0x0

    .line 145964
    const/4 v4, 0x0

    .line 145965
    if-eqz v7, :cond_b

    .line 145966
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 145967
    if-eqz v6, :cond_4

    array-length v8, v6

    if-lez v8, :cond_4

    .line 145968
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 145969
    :cond_4
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 145970
    if-eqz v6, :cond_a

    array-length v8, v6

    if-lez v8, :cond_a

    .line 145971
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    move-object v6, v5

    move-object v5, v4

    .line 145972
    :goto_3
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 145973
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 145974
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 145975
    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v43

    .line 145976
    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 145977
    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v45

    .line 145978
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v8, 0x1

    if-ne v4, v8, :cond_7

    const/4 v4, 0x1

    .line 145979
    :goto_4
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v46

    .line 145980
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 145981
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    .line 145982
    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 145983
    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v49

    .line 145984
    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v50

    .line 145985
    new-instance v52, LX/0x0;

    invoke-direct/range {v52 .. v52}, LX/0x0;-><init>()V

    move-object/from16 v0, v52

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/0x0;->a(Ljava/lang/String;)LX/0x0;

    move-result-object v42

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v52

    move-object/from16 v0, v42

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, LX/0x0;->a(D)LX/0x0;

    move-result-object v42

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v42

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/0x0;->i(Ljava/lang/String;)LX/0x0;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, LX/0x0;->b(Z)LX/0x0;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, LX/0x0;->b(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v46

    invoke-virtual {v4, v0}, LX/0x0;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, LX/0x0;->c(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, LX/0x0;->d(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, LX/0x0;->e(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/0x0;->f(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v11}, LX/0x0;->b(I)LX/0x0;

    move-result-object v4

    move/from16 v0, v38

    invoke-virtual {v4, v0}, LX/0x0;->d(I)LX/0x0;

    move-result-object v4

    move/from16 v0, v39

    invoke-virtual {v4, v0}, LX/0x0;->c(I)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/0x0;->a(Z)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/0x0;->a(Ljava/nio/ByteBuffer;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0x0;->b(Ljava/nio/ByteBuffer;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, LX/0x0;->g(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, LX/0x0;->a(J)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, LX/0x0;->h(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, LX/0x0;->e(I)LX/0x0;

    move-result-object v4

    move/from16 v0, v48

    invoke-virtual {v4, v0}, LX/0x0;->f(I)LX/0x0;

    move-result-object v4

    move/from16 v0, v49

    invoke-virtual {v4, v0}, LX/0x0;->a(I)LX/0x0;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/15j;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->o:LX/0qN;

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/0qN;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, LX/0x0;->j(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    move-wide/from16 v0, v50

    invoke-virtual {v4, v0, v1}, LX/0x0;->b(J)LX/0x0;

    move-result-object v4

    invoke-virtual {v4}, LX/0x0;->a()Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v5

    .line 145986
    if-eqz v41, :cond_5

    .line 145987
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0pn;->t:LX/0qP;

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, LX/0qP;->a(Ljava/lang/String;)LX/14t;

    move-result-object v4

    .line 145988
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->v:LX/0qR;

    move-object/from16 v0, v41

    invoke-virtual {v6, v0, v4}, LX/0qR;->a(Ljava/lang/String;LX/14t;)V

    .line 145989
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0pn;->v:LX/0qR;

    move-object/from16 v0, v45

    move-object/from16 v1, v41

    invoke-virtual {v4, v0, v1}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 145990
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v0, v5, v1}, LX/0pn;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145991
    :cond_5
    const/4 v4, 0x0

    .line 145992
    :try_start_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 145993
    :goto_5
    if-eqz v4, :cond_9

    .line 145994
    :try_start_4
    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-wide v10, v8

    .line 145995
    goto/16 :goto_0

    .line 145996
    :cond_6
    const/4 v4, 0x0

    move v7, v4

    goto/16 :goto_2

    .line 145997
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 145998
    :catch_0
    move-exception v6

    .line 145999
    sget-object v7, LX/0pn;->a:Ljava/lang/Class;

    const-string v10, "Not able to load feed unit"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v7, v6, v10, v11}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 146000
    :cond_8
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v6

    .line 146001
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v4

    .line 146002
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static {v6, v4, v7, v8}, LX/16z;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    .line 146003
    new-instance v6, LX/0uq;

    invoke-direct {v6}, LX/0uq;-><init>()V

    invoke-virtual {v6, v5}, LX/0uq;->a(LX/0Px;)LX/0uq;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0uq;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/0uq;

    move-result-object v4

    invoke-virtual {v4}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v5

    .line 146004
    new-instance v4, LX/23A;

    invoke-direct {v4, v5, v10, v11}, LX/23A;-><init>(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 146005
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_9
    move-wide v10, v8

    goto/16 :goto_0

    :cond_a
    move-object v6, v5

    move-object v5, v4

    goto/16 :goto_3

    :cond_b
    move-object v6, v5

    move-object v5, v4

    goto/16 :goto_3
.end method

.method public static a(LX/0pn;LX/0ux;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 145917
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 145918
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "home_stories"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 145919
    return-object v0
.end method

.method private static a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 145908
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145909
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 145910
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145911
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145912
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145913
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1, p2}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145914
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145915
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 145916
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/io/File;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145899
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    .line 145900
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 145901
    :cond_1
    return-object v0

    .line 145902
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 145903
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 145904
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 145905
    invoke-direct {p0, v4}, LX/0pn;->a(Ljava/io/File;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 145906
    :cond_3
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145907
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;J)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List",
            "<",
            "LX/31P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145831
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v8

    .line 145832
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v8

    .line 145833
    :goto_0
    return-object v2

    .line 145834
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0pn;->z:LX/0qS;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0qS;->a(Ljava/lang/String;)LX/2tK;

    move-result-object v12

    .line 145835
    invoke-virtual {v12}, LX/2tK;->b()V

    .line 145836
    move-object/from16 v0, p0

    iget-object v3, v0, LX/0pn;->n:Ljava/util/ArrayList;

    monitor-enter v3

    .line 145837
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0pn;->n:Ljava/util/ArrayList;

    invoke-virtual {v12}, LX/2tK;->a()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145838
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145839
    const/4 v10, -0x1

    .line 145840
    const/4 v2, 0x0

    .line 145841
    const/4 v9, 0x0

    .line 145842
    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move-object v4, v9

    move-object v11, v2

    move v5, v10

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 145843
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 145844
    const/4 v7, 0x0

    .line 145845
    const/4 v6, 0x0

    .line 145846
    :try_start_2
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 145847
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v14

    .line 145848
    const/4 v2, 0x4

    invoke-virtual {v14, v2}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    invoke-virtual {v14, v2}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 145849
    invoke-static {v14}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    if-ne v5, v2, :cond_2

    const/4 v2, 0x1

    move v9, v2

    .line 145850
    :goto_2
    invoke-static {v14}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    .line 145851
    if-nez v9, :cond_d

    .line 145852
    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-virtual {v14, v2}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 145853
    :goto_3
    :try_start_3
    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLViewer;

    if-eqz v2, :cond_3

    .line 145854
    move-object v0, v3

    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    move-object v2, v0

    .line 145855
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->o_()I

    move-result v4

    .line 145856
    invoke-static {v4, v14, v2}, LX/0pn;->a(ILX/15i;Lcom/facebook/graphql/model/GraphQLViewer;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    move-object v2, v7

    .line 145857
    :goto_4
    if-eqz v4, :cond_1

    if-nez v9, :cond_1

    .line 145858
    :try_start_4
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v2

    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v2

    :cond_1
    move/from16 v16, v4

    move-object v4, v3

    move-object v3, v2

    move/from16 v2, v16

    :goto_5
    move v7, v2

    move-object v9, v4

    move v10, v5

    move-object v2, v3

    .line 145859
    :goto_6
    if-nez v2, :cond_a

    if-nez v11, :cond_a

    .line 145860
    :try_start_5
    const-string v2, "FeedDbSerialize_BadLogic"

    const-string v3, "Buffer extraction indicated re-use, but there is nothing to re-use."

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v4, v9

    move v5, v10

    .line 145861
    goto :goto_1

    .line 145862
    :catchall_0
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v2

    .line 145863
    :cond_2
    const/4 v2, 0x0

    move v9, v2

    goto :goto_2

    .line 145864
    :cond_3
    :try_start_7
    instance-of v2, v3, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v2, :cond_4

    .line 145865
    invoke-virtual {v14}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v4

    move-object v2, v7

    goto :goto_4

    .line 145866
    :cond_4
    instance-of v2, v3, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    if-eqz v2, :cond_5

    .line 145867
    move-object v0, v3

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v0

    .line 145868
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->o_()I

    move-result v4

    .line 145869
    invoke-static {v4, v14, v2}, LX/0pn;->a(ILX/15i;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)I

    move-result v4

    move-object v2, v7

    .line 145870
    goto :goto_4

    .line 145871
    :cond_5
    const-string v4, "FlatBuffer"

    const-string v7, "unable to determine root object of flatbuffer: class = %s"

    const/4 v2, 0x1

    new-array v14, v2, [Ljava/lang/Object;

    const/4 v15, 0x0

    if-nez v3, :cond_6

    const-string v2, "null"

    :goto_7
    aput-object v2, v14, v15

    invoke-static {v4, v7, v14}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 145872
    const/4 v4, 0x0

    .line 145873
    :try_start_8
    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-static {v10, v2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 145874
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_4

    .line 145875
    :catch_0
    move-exception v2

    .line 145876
    :goto_8
    :try_start_9
    const-string v6, "FlatBuffer"

    const-string v7, "Error serializing feed unit to flatbuffer"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6, v2, v7, v9}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 145877
    const/4 v2, 0x0

    move v7, v4

    move-object v9, v3

    move v10, v5

    goto :goto_6

    .line 145878
    :cond_6
    :try_start_a
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v2

    goto :goto_7

    .line 145879
    :cond_7
    const/4 v3, 0x0

    .line 145880
    :try_start_b
    invoke-interface {v10}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v2

    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 145881
    invoke-virtual {v14}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-static {v6}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v6

    if-nez v6, :cond_8

    .line 145882
    const-string v2, "FlatBuffer"

    const-string v6, "Trying to write flatbuffer with zero rootObjectPosition"

    invoke-static {v2, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145883
    sget-object v2, LX/16Z;->a:LX/16Z;

    invoke-static {v10, v2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 145884
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_8
    move v7, v3

    move-object v9, v4

    move v10, v5

    .line 145885
    goto/16 :goto_6

    .line 145886
    :cond_9
    const/4 v2, 0x0

    .line 145887
    :try_start_c
    sget-object v3, LX/16Z;->a:LX/16Z;

    invoke-static {v10, v3}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 145888
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_5

    .line 145889
    :catch_1
    move-exception v3

    move-object/from16 v16, v3

    move-object v3, v4

    move v4, v2

    move-object/from16 v2, v16

    goto :goto_8

    .line 145890
    :cond_a
    if-eqz v2, :cond_b

    .line 145891
    :try_start_d
    invoke-virtual {v12, v2}, LX/2tK;->a(Ljava/nio/ByteBuffer;)LX/3Am;

    move-result-object v11

    .line 145892
    invoke-static {v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    .line 145893
    :cond_b
    new-instance v2, LX/31P;

    invoke-virtual {v12}, LX/2tK;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, v11, LX/3Am;->a:J

    iget v6, v11, LX/3Am;->b:I

    invoke-direct/range {v2 .. v7}, LX/31P;-><init>(Ljava/lang/String;JII)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-object v4, v9

    move v5, v10

    .line 145894
    goto/16 :goto_1

    .line 145895
    :cond_c
    invoke-virtual {v12}, LX/2tK;->c()V

    move-object v2, v8

    .line 145896
    goto/16 :goto_0

    .line 145897
    :catchall_1
    move-exception v2

    invoke-virtual {v12}, LX/2tK;->c()V

    throw v2

    .line 145898
    :catch_2
    move-exception v2

    move-object v3, v4

    move v4, v6

    goto/16 :goto_8

    :catch_3
    move-exception v2

    move v4, v6

    goto/16 :goto_8

    :catch_4
    move-exception v2

    move/from16 v16, v3

    move-object v3, v4

    move/from16 v4, v16

    goto/16 :goto_8

    :cond_d
    move-object v3, v4

    goto/16 :goto_3
.end method

.method public static a(LX/0pn;LX/0uw;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 146215
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 146216
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146217
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, LX/0pp;->d:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146218
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 146219
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 146220
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 146221
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146222
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 146223
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 146224
    iget-object v2, p0, LX/0pn;->t:LX/0qP;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0qP;->a(LX/0Px;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146225
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 146226
    return-void
.end method

.method public static a(LX/0pn;LX/37E;I)V
    .locals 4

    .prologue
    .line 146472
    iget-object v0, p0, LX/0pn;->g:LX/0pk;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, p2, v2, v3}, LX/0pk;->a(LX/37E;IJ)V

    .line 146473
    iget-object v0, p0, LX/0pn;->A:LX/1fO;

    if-eqz v0, :cond_0

    .line 146474
    iget-object v0, p0, LX/0pn;->A:LX/1fO;

    .line 146475
    iget-object v1, v0, LX/1fO;->a:LX/1fN;

    invoke-virtual {v1}, LX/1fN;->a()V

    .line 146476
    :cond_0
    return-void
.end method

.method private static a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146458
    if-lez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 146459
    iget-object v0, p0, LX/0pn;->p:LX/0ad;

    sget-short v3, LX/0fe;->aF:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146460
    if-lez p3, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 146461
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0pp;->n:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Ad"

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 146462
    invoke-static {p0, v0, p3}, LX/0pn;->a(LX/0pn;LX/0ux;I)Landroid/database/Cursor;

    move-result-object v0

    .line 146463
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 146464
    const-string v2, "Ad"

    invoke-static {p0, p1, v0, v1, v2}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;Landroid/database/Cursor;ILjava/lang/String;)V

    .line 146465
    :cond_0
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 146466
    invoke-static {p0, v0, p2}, LX/0pn;->a(LX/0pn;LX/0ux;I)Landroid/database/Cursor;

    move-result-object v0

    .line 146467
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 146468
    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;Landroid/database/Cursor;ILjava/lang/String;)V

    .line 146469
    return-void

    :cond_1
    move v0, v2

    .line 146470
    goto :goto_0

    :cond_2
    move v1, v2

    .line 146471
    goto :goto_1
.end method

.method public static a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;Landroid/database/Cursor;ILjava/lang/String;)V
    .locals 6
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 146452
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    .line 146453
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 146454
    sget-object v3, LX/69H;->LT:LX/69H;

    sget-object v5, LX/37E;->CACHE_FULL:LX/37E;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;LX/69H;Ljava/lang/String;LX/37E;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146455
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 146456
    return-void

    .line 146457
    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(LX/0pn;Ljava/lang/String;LX/37E;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 146434
    iget-object v0, p0, LX/0pn;->m:Ljava/io/File;

    invoke-direct {p0, v0}, LX/0pn;->a(Ljava/io/File;)Ljava/util/List;

    move-result-object v10

    .line 146435
    new-array v3, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v2, LX/0pp;->o:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    .line 146436
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "home_stories"

    sget-object v5, LX/0pp;->o:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146437
    :try_start_0
    sget-object v0, LX/0pp;->o:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 146438
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 146439
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 146440
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 146441
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 146442
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146443
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 146444
    :cond_1
    :try_start_1
    invoke-interface {v10, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 146445
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 146446
    iget-object v3, p0, LX/0pn;->n:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146447
    :try_start_2
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/0pn;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 146448
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 146449
    :cond_2
    monitor-exit v3

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 146450
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 146451
    return-void
.end method

.method public static a(LX/0pn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 146429
    :try_start_0
    iget-object v0, p0, LX/0pn;->t:LX/0qP;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0qP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146430
    :goto_0
    return-void

    .line 146431
    :catch_0
    invoke-virtual {p0}, LX/0pn;->U_()V

    goto :goto_0

    .line 146432
    :catch_1
    move-exception v0

    .line 146433
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Update/Insert operation failed!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    .line 146404
    invoke-virtual {p0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 146405
    :cond_0
    return-void

    .line 146406
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    move v2, v4

    :goto_0
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 146407
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 146408
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v7

    if-nez v7, :cond_2

    .line 146409
    invoke-static {v1}, LX/4XL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 146410
    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 146411
    :cond_2
    if-eqz v1, :cond_3

    .line 146412
    iget-object v7, p0, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v7, v7

    .line 146413
    iget-object v8, v7, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v7, v8

    .line 146414
    invoke-virtual {v7}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    .line 146415
    iget-wide v12, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v10, v12

    .line 146416
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x1

    .line 146417
    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v10

    .line 146418
    if-eqz v10, :cond_3

    invoke-virtual {v10, v12}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_3

    .line 146419
    const/4 v11, 0x0

    invoke-virtual {v10, v11, v7}, LX/15i;->a(ILjava/lang/Object;)V

    .line 146420
    invoke-virtual {v10, v12, v8}, LX/15i;->a(ILjava/lang/Object;)V

    .line 146421
    const/4 v11, 0x2

    invoke-virtual {v10, v11, v0}, LX/15i;->a(ILjava/lang/Object;)V

    .line 146422
    const/4 v11, 0x3

    invoke-virtual {v10, v11, v9}, LX/15i;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146423
    :cond_3
    move v0, v2

    .line 146424
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 146425
    :catch_0
    move-exception v0

    .line 146426
    if-nez v2, :cond_4

    .line 146427
    const-string v1, "FlatBuffer"

    const-string v2, "Error serializing feed unit to flatbuffer"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v7}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146428
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146398
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146399
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 146400
    invoke-static {v0}, LX/16u;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object p1

    .line 146401
    if-eqz p1, :cond_0

    .line 146402
    iget-object v1, p0, LX/0pn;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16u;

    invoke-virtual {v1, p1, p2}, LX/16u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146403
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 146354
    invoke-static {p0, p1}, LX/0pn;->e(LX/0pn;Lcom/facebook/api/feed/FetchFeedResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146355
    invoke-static {p0, p1, p2}, LX/0pn;->b(LX/0pn;Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 146356
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 146357
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_7

    .line 146358
    const/4 v0, 0x1

    .line 146359
    :goto_0
    move v0, v0

    .line 146360
    if-nez v0, :cond_1

    move v0, v2

    .line 146361
    :goto_1
    return v0

    .line 146362
    :cond_1
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v3, v0

    .line 146363
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 146364
    if-nez v0, :cond_2

    .line 146365
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 146366
    if-nez v0, :cond_2

    move v0, v2

    .line 146367
    goto :goto_1

    .line 146368
    :cond_2
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v4, v0

    .line 146369
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 146370
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 146371
    const v5, 0x127bcc93

    invoke-static {p2, v5}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146372
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 146373
    invoke-static {p0}, LX/0pn;->g(LX/0pn;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 146374
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 146375
    if-eqz v0, :cond_4

    .line 146376
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 146377
    invoke-static {p0, v0}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146378
    if-eqz v0, :cond_3

    .line 146379
    sget-object v3, LX/0pp;->v:LX/0U1;

    invoke-virtual {v3, v0}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146380
    sget-object v0, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146381
    :cond_3
    :goto_2
    sget-object v0, LX/0pp;->l:LX/0U1;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146382
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {v4}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146383
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "home_stories"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 146384
    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146385
    const v1, -0x18c1e03

    invoke-static {p2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146386
    if-lez v0, :cond_6

    const/4 v0, 0x1

    goto/16 :goto_1

    .line 146387
    :cond_4
    :try_start_1
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 146388
    if-eqz v0, :cond_3

    .line 146389
    iget-object v0, v3, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 146390
    invoke-static {p0, v0}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146391
    if-eqz v0, :cond_3

    .line 146392
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 146393
    :catchall_0
    move-exception v0

    const v1, 0x482a6d06

    invoke-static {p2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 146394
    :cond_5
    :try_start_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/239;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146395
    sget-object v3, LX/0pp;->v:LX/0U1;

    invoke-virtual {v3, v0}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146396
    sget-object v0, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 146397
    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)Z
    .locals 1

    .prologue
    .line 146214
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;->BUMP_UNREAD:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/api/feedtype/FeedType;J)I
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 146342
    iget-object v0, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p2

    .line 146343
    sget-object v0, LX/0pp;->b:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 146344
    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 146345
    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    aput-object v0, v3, v1

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-static {v3}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 146346
    invoke-static {p0, v2}, LX/0pn;->a(LX/0pn;LX/0uw;)V

    .line 146347
    :try_start_0
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "home_stories"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 146348
    const-string v1, "evictStoriesByLastRefreshedTime"

    sget-object v2, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v1, v2}, LX/0pn;->a(LX/0pn;Ljava/lang/String;LX/37E;)V

    .line 146349
    sget-object v1, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v1, v0}, LX/0pn;->a(LX/0pn;LX/37E;I)V

    .line 146350
    :goto_0
    return v0

    .line 146351
    :catch_0
    move-exception v0

    .line 146352
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "evictStoriesByLastRefreshedTime failed"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 146353
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/0pn;
    .locals 23

    .prologue
    .line 146340
    new-instance v2, LX/0pn;

    const/16 v3, 0xea

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static/range {p0 .. p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v6

    check-cast v6, LX/0pq;

    const/16 v7, 0x15c5

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v9

    check-cast v9, LX/0oy;

    const-class v10, LX/0ox;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/0ox;

    invoke-static/range {p0 .. p0}, LX/0qL;->a(LX/0QB;)Ljava/io/File;

    move-result-object v11

    check-cast v11, Ljava/io/File;

    invoke-static/range {p0 .. p0}, LX/0qN;->a(LX/0QB;)LX/0qN;

    move-result-object v12

    check-cast v12, LX/0qN;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v14

    check-cast v14, LX/0pJ;

    invoke-static/range {p0 .. p0}, LX/0qO;->a(LX/0QB;)LX/0qO;

    move-result-object v15

    check-cast v15, LX/0qO;

    invoke-static/range {p0 .. p0}, LX/0qP;->a(LX/0QB;)LX/0qP;

    move-result-object v16

    check-cast v16, LX/0qP;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v17

    check-cast v17, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0qR;->a(LX/0QB;)LX/0qR;

    move-result-object v18

    check-cast v18, LX/0qR;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v19

    check-cast v19, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v20

    check-cast v20, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v21, 0x695

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/0qS;->a(LX/0QB;)LX/0qS;

    move-result-object v22

    check-cast v22, LX/0qS;

    invoke-direct/range {v2 .. v22}, LX/0pn;-><init>(LX/0Ot;Lcom/facebook/performancelogger/PerformanceLogger;LX/0pi;LX/0pq;LX/0Ot;LX/0SG;LX/0oy;LX/0ox;Ljava/io/File;LX/0qN;LX/0ad;LX/0pJ;LX/0qO;LX/0qP;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0qR;LX/0Uh;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0qS;)V

    .line 146341
    return-object v2
.end method

.method private b(Lcom/facebook/api/feed/FetchFeedParams;)LX/23A;
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 146302
    invoke-static {p0}, LX/0pn;->m(LX/0pn;)V

    .line 146303
    invoke-static {p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v0

    invoke-virtual {v0}, LX/15j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146304
    invoke-virtual {p0, v5}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 146305
    invoke-static {p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v0

    invoke-virtual {v0}, LX/15j;->c()V

    .line 146306
    sget-object v0, LX/23A;->a:LX/23A;

    .line 146307
    :goto_0
    return-object v0

    .line 146308
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 146309
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146310
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 146311
    sget-object v1, LX/0pp;->a:LX/0U1;

    .line 146312
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 146313
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146314
    iget-wide v10, p1, Lcom/facebook/api/feed/FetchFeedParams;->n:J

    move-wide v2, v10

    .line 146315
    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 146316
    sget-object v1, LX/0pp;->b:LX/0U1;

    iget-object v6, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146317
    :cond_1
    iget-wide v10, p1, Lcom/facebook/api/feed/FetchFeedParams;->o:J

    move-wide v2, v10

    .line 146318
    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 146319
    sget-object v1, LX/0pp;->b:LX/0U1;

    iget-object v6, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146320
    :cond_2
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    .line 146321
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 146322
    if-eqz v1, :cond_5

    .line 146323
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v1

    .line 146324
    invoke-static {p0, v1}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 146325
    if-eqz v1, :cond_4

    .line 146326
    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146327
    :cond_3
    :goto_1
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    .line 146328
    iget v6, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v6, v6

    .line 146329
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 146330
    invoke-static {p0, v0, p1}, LX/0pn;->a(LX/0pn;Landroid/database/Cursor;Lcom/facebook/api/feed/FetchFeedParams;)LX/23A;

    move-result-object v0

    goto/16 :goto_0

    .line 146331
    :cond_4
    sget-object v0, LX/23A;->a:LX/23A;

    goto/16 :goto_0

    .line 146332
    :cond_5
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 146333
    if-eqz v1, :cond_3

    .line 146334
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 146335
    invoke-static {p0, v1}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 146336
    if-eqz v1, :cond_6

    .line 146337
    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146338
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 146339
    :cond_6
    sget-object v0, LX/23A;->a:LX/23A;

    goto/16 :goto_0
.end method

.method private static b(LX/0pn;Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 146282
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v2

    .line 146283
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 146284
    iget-object v3, v0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 146285
    invoke-static {p0, v3}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 146286
    iget-object v4, v0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 146287
    invoke-static {p0, v4}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 146288
    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    .line 146289
    sget-object v5, LX/0pp;->v:LX/0U1;

    invoke-virtual {v5, v3}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146290
    sget-object v3, LX/0pp;->v:LX/0U1;

    invoke-virtual {v3, v4}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146291
    sget-object v3, LX/0pp;->l:LX/0U1;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146292
    sget-object v3, LX/0pp;->a:LX/0U1;

    .line 146293
    iget-object v4, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v4

    .line 146294
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146295
    const v0, 0x5c0ae192

    invoke-static {p2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146296
    :try_start_0
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "home_stories"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 146297
    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146298
    const v2, 0x844a38a

    invoke-static {p2, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146299
    :goto_0
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 146300
    :catchall_0
    move-exception v0

    const v1, 0x661d336c

    invoke-static {p2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_0
    move v0, v1

    .line 146301
    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c(LX/0pn;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 145468
    const/4 v0, 0x0

    .line 145469
    :try_start_0
    iget-object v1, p0, LX/0pn;->v:LX/0qR;

    invoke-virtual {v1, p1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145470
    if-eqz v0, :cond_1

    .line 145471
    :cond_0
    :goto_0
    return-object v0

    .line 145472
    :cond_1
    invoke-direct {p0, p1}, LX/0pn;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145473
    if-eqz v0, :cond_0

    .line 145474
    iget-object v1, p0, LX/0pn;->v:LX/0qR;

    invoke-virtual {v1, p1, v0}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 145475
    :catch_0
    invoke-virtual {p0}, LX/0pn;->U_()V

    goto :goto_0

    .line 145476
    :catch_1
    move-exception v1

    .line 145477
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Update/Insert operation failed!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private c(Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11

    .prologue
    .line 146227
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v6, v0

    .line 146228
    iget-object v0, v6, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v7, v0

    .line 146229
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 146230
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0pn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 146231
    const/4 v0, 0x1

    .line 146232
    :goto_0
    move v0, v0

    .line 146233
    if-nez v0, :cond_1

    .line 146234
    :cond_0
    :goto_1
    return-void

    .line 146235
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 146236
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v7, v0, v1}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 146237
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 146238
    sget-object v0, LX/0pp;->l:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 146239
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146240
    if-eqz v0, :cond_3

    .line 146241
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 146242
    :cond_3
    :try_start_1
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 146243
    sget-object v0, LX/0pp;->c:LX/0U1;

    invoke-virtual {v0, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 146244
    sget-object v2, LX/0pp;->d:LX/0U1;

    invoke-virtual {v2, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 146245
    sget-object v3, LX/0pp;->v:LX/0U1;

    invoke-virtual {v3, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 146246
    sget-object v3, LX/0pp;->w:LX/0U1;

    invoke-virtual {v3, v8}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 146247
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "Database_Gap"

    invoke-static/range {v0 .. v5}, LX/239;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/model/GapFeedEdge;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 146248
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 146249
    if-nez v1, :cond_4

    .line 146250
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "null gap feed edge"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146251
    :catch_0
    move-exception v0

    .line 146252
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146253
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :catch_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 146254
    :cond_4
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 146255
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146256
    sget-object v0, LX/0pp;->b:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146257
    iget-wide v9, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v9

    .line 146258
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 146259
    sget-object v0, LX/0pp;->l:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146260
    sget-object v0, LX/0pp;->n:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Gap"

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146261
    sget-object v0, LX/0pp;->d:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146262
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146263
    sget-object v0, LX/0pp;->c:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146264
    sget-object v0, LX/0pp;->v:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146265
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146266
    invoke-static {v6}, LX/0pn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    .line 146267
    :goto_3
    const v3, -0x1e22de76

    invoke-static {p2, v3}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146268
    :try_start_3
    const-string v3, "home_stories"

    const-string v4, ""

    const v5, 0xa4e2826

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p2, v3, v4, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, -0x8d7134a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 146269
    invoke-virtual {p2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 146270
    const v0, -0x1505c083

    invoke-static {p2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 146271
    invoke-static {p0}, LX/0pn;->g(LX/0pn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146272
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v0

    .line 146273
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v2

    .line 146274
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {v7}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146275
    sget-object v1, LX/0pp;->l:LX/0U1;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146276
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1, v0}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 146277
    invoke-static {p0, v2}, LX/0pn;->a(LX/0pn;LX/0uw;)V

    .line 146278
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v3, "home_stories"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 146279
    goto/16 :goto_1

    .line 146280
    :cond_5
    const/4 v0, 0x4

    goto :goto_3

    .line 146281
    :catchall_1
    move-exception v0

    const v1, -0x8615acd

    invoke-static {p2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public static c(Lcom/facebook/api/feed/FetchFeedParams;)Z
    .locals 1

    .prologue
    .line 146120
    iget-object v0, p0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 146121
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 146198
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 146199
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146200
    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 146201
    sget-object v1, LX/0pp;->x:LX/0U1;

    .line 146202
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 146203
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 146204
    new-array v2, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/0pp;->d:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 146205
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146206
    :try_start_0
    sget-object v0, LX/0pp;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 146207
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 146208
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 146209
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146210
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Multiple dedup keys found for cache id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146211
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 146212
    :goto_0
    return-object v5

    .line 146213
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private d(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 145282
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145283
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v9, v1

    .line 145284
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145285
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145286
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145287
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {v9}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145288
    sget-object v1, LX/0pp;->l:LX/0U1;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145289
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145290
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145291
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145292
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145293
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v3

    .line 145294
    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {v9}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145295
    sget-object v2, LX/0pp;->l:LX/0U1;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145296
    sget-object v2, LX/0pp;->w:LX/0U1;

    invoke-virtual {v2, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145297
    invoke-static {p0, v3}, LX/0pn;->a(LX/0pn;LX/0uw;)V

    .line 145298
    iget-object v2, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0v1;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v4, "home_stories"

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145299
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145300
    return-void

    .line 145301
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static e(LX/0pn;)LX/15j;
    .locals 2

    .prologue
    .line 145450
    iget-object v0, p0, LX/0pn;->l:LX/15j;

    if-nez v0, :cond_0

    .line 145451
    iget-object v0, p0, LX/0pn;->k:LX/0ox;

    sget-object v1, LX/0pP;->w:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v0

    iput-object v0, p0, LX/0pn;->l:LX/15j;

    .line 145452
    :cond_0
    iget-object v0, p0, LX/0pn;->l:LX/15j;

    return-object v0
.end method

.method public static e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 145439
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145440
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145441
    sget-object v1, LX/0pp;->w:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 145442
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/0pp;->v:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 145443
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0U1;->a:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145444
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145445
    sget-object v0, LX/0pp;->v:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145446
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 145447
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145448
    :goto_0
    return-object v5

    .line 145449
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static e(LX/0pn;Lcom/facebook/api/feed/FetchFeedResult;)Z
    .locals 1

    .prologue
    .line 145430
    invoke-static {p0}, LX/0pn;->g(LX/0pn;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145431
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145432
    if-eqz v0, :cond_1

    .line 145433
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145434
    iget-object p0, v0, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, p0

    .line 145435
    if-eqz v0, :cond_1

    .line 145436
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 145437
    iget-object p0, v0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v0, p0

    .line 145438
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(LX/0pn;)Z
    .locals 2

    .prologue
    .line 145429
    iget-object v0, p0, LX/0pn;->q:LX/0pJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0pJ;->b(Z)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 145428
    iget-object v1, p0, LX/0pn;->p:LX/0ad;

    sget-short v2, LX/0fe;->S:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0pn;->p:LX/0ad;

    sget-short v2, LX/0fe;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private i(Lcom/facebook/api/feedtype/FeedType;)V
    .locals 10

    .prologue
    .line 145416
    invoke-virtual {p0}, LX/0pn;->d()I

    move-result v0

    .line 145417
    iget-object v1, p0, LX/0pn;->p:LX/0ad;

    sget-short v2, LX/0fe;->aE:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145418
    iget-object v1, p0, LX/0pn;->j:LX/0oy;

    invoke-virtual {v1}, LX/0oy;->p()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v0, v1

    .line 145419
    :cond_0
    invoke-virtual {p0, p1}, LX/0pn;->e(Lcom/facebook/api/feedtype/FeedType;)J

    move-result-wide v2

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 145420
    invoke-virtual {p0}, LX/0pn;->d()I

    move-result v0

    .line 145421
    iget-object v4, p0, LX/0pn;->j:LX/0oy;

    invoke-virtual {p0}, LX/0pn;->d()I

    move-result v5

    .line 145422
    iget v6, v4, LX/0oy;->H:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    .line 145423
    iget-object v6, v4, LX/0oy;->a:LX/0W3;

    sget-wide v8, LX/0X5;->gM:J

    invoke-interface {v6, v8, v9, v5}, LX/0W4;->a(JI)I

    move-result v6

    iput v6, v4, LX/0oy;->H:I

    .line 145424
    :cond_1
    iget v6, v4, LX/0oy;->H:I

    move v4, v6

    .line 145425
    move v1, v4

    .line 145426
    invoke-static {p0, p1, v0, v1}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;II)V

    .line 145427
    :cond_2
    return-void
.end method

.method private j(Lcom/facebook/api/feedtype/FeedType;)Ljava/util/HashSet;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 145402
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145403
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145404
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145405
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145406
    sget-object v1, LX/0pp;->i:LX/0U1;

    const-string v2, "1"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145407
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/0pp;->d:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 145408
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 145409
    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145410
    sget-object v0, LX/0pp;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145411
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 145412
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145413
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145414
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145415
    return-object v2
.end method

.method private k()I
    .locals 1

    .prologue
    .line 145401
    invoke-virtual {p0}, LX/0pn;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private static m(LX/0pn;)V
    .locals 3

    .prologue
    .line 145398
    iget-object v0, p0, LX/0pn;->x:LX/0Uh;

    const/16 v1, 0x219

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 145399
    sput-boolean v0, LX/15i;->b:Z

    .line 145400
    return-void
.end method


# virtual methods
.method public final U_()V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 145371
    :try_start_0
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 145372
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    if-nez v1, :cond_0

    .line 145373
    const-string v1, "VACUUM"

    const v2, 0x3e502c17

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6358e475    # 4.0009588E21f

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 145374
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, LX/0pn;->j:LX/0oy;

    invoke-virtual {v2}, LX/0oy;->i()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-direct {p0, v0, v4, v5}, LX/0pn;->b(Lcom/facebook/api/feedtype/FeedType;J)I

    .line 145375
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "count("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    .line 145376
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "home_stories"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, LX/0pp;->a:LX/0U1;

    invoke-virtual {v4}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145377
    iget-object v0, p0, LX/0pn;->j:LX/0oy;

    .line 145378
    iget v8, v0, LX/0oy;->f:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_1

    .line 145379
    iget-object v8, v0, LX/0oy;->a:LX/0W3;

    sget-wide v10, LX/0X5;->gq:J

    const/16 v9, 0xa

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JI)I

    move-result v8

    iput v8, v0, LX/0oy;->f:I

    .line 145380
    :cond_1
    iget v8, v0, LX/0oy;->f:I

    move v0, v8

    .line 145381
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 145382
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-le v2, v0, :cond_2

    .line 145383
    new-instance v2, Lcom/facebook/api/feedtype/FeedType;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/facebook/api/feedtype/FeedType;-><init>(Ljava/lang/Object;Lcom/facebook/api/feedtype/FeedType$Name;)V

    const/4 v3, 0x0

    invoke-static {p0, v2, v0, v3}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 145384
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145385
    iget-object v8, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/32 v10, 0x240c8400

    sub-long/2addr v8, v10

    .line 145386
    sget-object v10, LX/0pp;->b:LX/0U1;

    invoke-virtual {v10}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    .line 145387
    sget-object v9, LX/0pp;->a:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/0pn;->d:Ljava/util/List;

    invoke-static {v9, v10}, LX/0uu;->b(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v9

    .line 145388
    const/4 v10, 0x2

    new-array v10, v10, [LX/0ux;

    const/4 v11, 0x0

    aput-object v8, v10, v11

    const/4 v8, 0x1

    aput-object v9, v10, v8

    invoke-static {v10}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v9

    .line 145389
    invoke-static {p0, v9}, LX/0pn;->a(LX/0pn;LX/0uw;)V

    .line 145390
    iget-object v8, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0v1;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v10, "home_stories"

    invoke-virtual {v9}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v10, v11, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 145391
    const-string v9, "evictStaleStories"

    sget-object v10, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v9, v10}, LX/0pn;->a(LX/0pn;Ljava/lang/String;LX/37E;)V

    .line 145392
    sget-object v9, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v9, v8}, LX/0pn;->a(LX/0pn;LX/37E;I)V

    .line 145393
    return-void

    .line 145394
    :catch_0
    move-exception v0

    .line 145395
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Vacuum failed due to SQLite disk too full"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 145396
    :catch_1
    move-exception v0

    .line 145397
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Vacuum failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public final a(LX/0Px;)I
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 145334
    if-nez p1, :cond_1

    .line 145335
    sget-object v0, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Inconsistent input for deleteStories!"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 145336
    :cond_0
    :goto_0
    return v0

    .line 145337
    :cond_1
    const/4 v2, 0x0

    .line 145338
    :try_start_0
    iget-object v0, p0, LX/0pn;->t:LX/0qP;

    invoke-virtual {v0, p1}, LX/0qP;->a(LX/0Px;)I

    .line 145339
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 145340
    const v0, 0x4b78726e    # 1.6282222E7f

    :try_start_1
    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 145341
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v4

    .line 145342
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145343
    sget-object v6, LX/0pp;->d:LX/0U1;

    invoke-virtual {v6, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145344
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 145345
    :cond_2
    :try_start_2
    const-string v0, "home_stories"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    .line 145346
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 145347
    const v0, 0x71e9befb

    :try_start_4
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    .line 145348
    :goto_3
    if-lez v0, :cond_0

    .line 145349
    iget-object v1, p0, LX/0pn;->A:LX/1fO;

    if-eqz v1, :cond_3

    .line 145350
    iget-object v1, p0, LX/0pn;->A:LX/1fO;

    invoke-virtual {v1}, LX/1fO;->b()V

    .line 145351
    :cond_3
    const-string v1, "mutateAndDeleteInSingleTransaction"

    sget-object v2, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v1, v2}, LX/0pn;->a(LX/0pn;Ljava/lang/String;LX/37E;)V

    goto :goto_0

    .line 145352
    :catch_0
    move-exception v0

    .line 145353
    :try_start_5
    sget-object v3, LX/0pn;->a:Ljava/lang/Class;

    const-string v4, "One Delete operation failed!"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 145354
    :catch_1
    move-exception v0

    move-object v7, v2

    move v2, v1

    move-object v1, v7

    .line 145355
    :goto_4
    :try_start_6
    sget-object v3, LX/0pn;->a:Ljava/lang/Class;

    const-string v4, "Delete Stories failed!"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 145356
    const v0, 0x17b51953

    :try_start_7
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    move v0, v2

    .line 145357
    goto :goto_3

    .line 145358
    :catch_2
    move-exception v0

    .line 145359
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 145360
    goto :goto_3

    .line 145361
    :catch_3
    move-exception v0

    .line 145362
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 145363
    goto :goto_3

    .line 145364
    :catchall_0
    move-exception v0

    .line 145365
    :goto_5
    const v1, 0x5e07123b

    :try_start_8
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 145366
    :goto_6
    throw v0

    .line 145367
    :catch_4
    move-exception v1

    .line 145368
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 145369
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_5

    .line 145370
    :catch_5
    move-exception v0

    move-object v7, v2

    move v2, v1

    move-object v1, v7

    goto :goto_4
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            "J)",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145329
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 145330
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    sget-object v3, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v0, v2

    sget-object v2, LX/0pp;->b:LX/0U1;

    iget-object v3, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 145331
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v1

    .line 145332
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v0, v1}, LX/0pn;->a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 145333
    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 11

    .prologue
    const v8, 0xa000b

    .line 145316
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    .line 145317
    iget-object v0, p0, LX/0pn;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNFDbFeedLoadStories"

    invoke-interface {v0, v8, v1, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 145318
    :try_start_0
    invoke-direct {p0, p1}, LX/0pn;->b(Lcom/facebook/api/feed/FetchFeedParams;)LX/23A;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 145319
    invoke-virtual {v1}, LX/23A;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145320
    iget-object v0, v1, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v0

    .line 145321
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    sget-object v3, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    .line 145322
    iget-wide v9, v1, LX/23A;->c:J

    move-wide v4, v9

    .line 145323
    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 145324
    :goto_0
    iget-object v1, p0, LX/0pn;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "NNFDbFeedLoadStories"

    invoke-interface {v1, v8, v2, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 145325
    return-object v0

    .line 145326
    :catch_0
    move-exception v0

    .line 145327
    new-instance v1, LX/69I;

    invoke-direct {v1, v0}, LX/69I;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 145328
    :cond_0
    invoke-static {p1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 145302
    invoke-static {p0, p1}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145303
    if-nez v1, :cond_0

    .line 145304
    :goto_0
    return-object v5

    .line 145305
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145306
    const-string v2, "home_stories"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145307
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145308
    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {p2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145309
    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145310
    sget-object v1, LX/0pp;->l:LX/0U1;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145311
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145312
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145313
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 145314
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 145315
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 145270
    if-eqz p1, :cond_2

    .line 145271
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {v0}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 145272
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "home_stories"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 145273
    :goto_0
    iget-object v1, p0, LX/0pn;->t:LX/0qP;

    const/4 v5, 0x0

    .line 145274
    iget-object v2, v1, LX/0qP;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0v1;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "home_stories_media"

    invoke-virtual {v2, v3, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145275
    const-string v1, "clearAllForFeedType"

    sget-object v2, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v1, v2}, LX/0pn;->a(LX/0pn;Ljava/lang/String;LX/37E;)V

    .line 145276
    if-eqz p1, :cond_0

    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1, p1}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145277
    :cond_0
    sget-object v1, LX/00a;->a:LX/00a;

    move-object v1, v1

    .line 145278
    invoke-virtual {v1, v4}, LX/00a;->a(Ljava/lang/String;)V

    .line 145279
    :cond_1
    sget-object v1, LX/37E;->CONTENT_STALE:LX/37E;

    invoke-static {p0, v1, v0}, LX/0pn;->a(LX/0pn;LX/37E;I)V

    .line 145280
    return-void

    .line 145281
    :cond_2
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "home_stories"

    invoke-virtual {v0, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;LX/69H;Ljava/lang/String;LX/37E;)V
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 145565
    if-nez p2, :cond_0

    .line 145566
    :goto_0
    return-void

    .line 145567
    :cond_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v1

    .line 145568
    sget-object v0, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145569
    sget-object v0, LX/69G;->a:[I

    invoke-virtual {p3}, LX/69H;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 145570
    :goto_1
    if-eqz p4, :cond_1

    .line 145571
    sget-object v0, LX/0pp;->n:LX/0U1;

    invoke-virtual {v0, p4}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145572
    :cond_1
    invoke-static {p0, v1}, LX/0pn;->a(LX/0pn;LX/0uw;)V

    .line 145573
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "home_stories"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 145574
    const-string v1, "clearBySortKey"

    invoke-static {p0, v1, p5}, LX/0pn;->a(LX/0pn;Ljava/lang/String;LX/37E;)V

    .line 145575
    invoke-static {p0, p5, v0}, LX/0pn;->a(LX/0pn;LX/37E;I)V

    goto :goto_0

    .line 145576
    :pswitch_0
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    .line 145577
    :pswitch_1
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    .line 145578
    :pswitch_2
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    .line 145579
    :pswitch_3
    sget-object v0, LX/0pp;->e:LX/0U1;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 145789
    sget-object v0, LX/0pp;->d:LX/0U1;

    .line 145790
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 145791
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 145792
    const/4 v1, 0x0

    .line 145793
    :try_start_0
    iget-object v0, p0, LX/0pn;->t:LX/0qP;

    invoke-virtual {v0, p1}, LX/0qP;->b(Ljava/lang/String;)V

    .line 145794
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 145795
    const v0, -0x3701bc03

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 145796
    const-string v0, "home_stories"

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145797
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145798
    const v0, 0x5c7e5de0

    :try_start_1
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 145799
    :goto_0
    iget-object v0, p0, LX/0pn;->A:LX/1fO;

    if-eqz v0, :cond_0

    .line 145800
    iget-object v0, p0, LX/0pn;->A:LX/1fO;

    invoke-virtual {v0}, LX/1fO;->b()V

    .line 145801
    :cond_0
    return-void

    .line 145802
    :catch_0
    move-exception v0

    .line 145803
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 145804
    :catch_1
    move-exception v0

    .line 145805
    :try_start_2
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Delete Story failed!"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145806
    const v0, -0xe06f5c0

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 145807
    :catch_2
    move-exception v0

    .line 145808
    sget-object v1, LX/0pn;->a:Ljava/lang/Class;

    const-string v2, "Failed to close the connection to the DB!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 145809
    :catchall_0
    move-exception v0

    .line 145810
    const v2, 0x3853c2ec

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 145811
    :goto_1
    throw v0

    .line 145812
    :catch_3
    move-exception v1

    .line 145813
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "Failed to close the connection to the DB!"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 9

    .prologue
    .line 145751
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 145752
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 145753
    if-eqz p5, :cond_0

    .line 145754
    sget-object v1, LX/0pp;->s:LX/0U1;

    .line 145755
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 145756
    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 145757
    :cond_0
    if-eqz p6, :cond_1

    .line 145758
    sget-object v1, LX/0pp;->t:LX/0U1;

    .line 145759
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 145760
    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 145761
    :cond_1
    const/4 v1, 0x0

    .line 145762
    const v2, -0xa0afc8b

    :try_start_0
    invoke-static {v4, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 145763
    :try_start_1
    const-string v2, "home_stories"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/0pp;->d:LX/0U1;

    .line 145764
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 145765
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = ? AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0pp;->e:LX/0U1;

    .line 145766
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 145767
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = ? AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0pp;->a:LX/0U1;

    .line 145768
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 145769
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = ? AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, LX/0pp;->b:LX/0U1;

    .line 145770
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 145771
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " = ?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object p3, v5, v6

    const/4 v6, 0x2

    aput-object p1, v5, v6

    const/4 v6, 0x3

    aput-object p4, v5, v6

    invoke-virtual {v4, v2, v0, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145772
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145773
    const v0, 0x5faec7cd

    :try_start_2
    invoke-static {v4, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 145774
    :goto_0
    if-eqz v1, :cond_2

    .line 145775
    invoke-virtual {p0}, LX/0pn;->U_()V

    .line 145776
    :cond_2
    return-void

    .line 145777
    :catch_0
    move-exception v0

    .line 145778
    if-eqz p5, :cond_4

    :try_start_3
    array-length v2, p5

    move v3, v2

    .line 145779
    :goto_1
    if-eqz p6, :cond_5

    array-length v2, p6

    .line 145780
    :goto_2
    sget-object v5, LX/0pn;->a:Ljava/lang/Class;

    const-string v6, "Error mutating a story. mutationLength=%d, extraLength=%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v8

    const/4 v3, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v7, v3

    invoke-static {v5, v0, v6, v7}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145781
    instance-of v0, v0, Landroid/database/sqlite/SQLiteFullException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_3

    .line 145782
    const/4 v1, 0x1

    .line 145783
    :cond_3
    const v0, -0x209871a8

    :try_start_4
    invoke-static {v4, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 145784
    :catch_1
    move-exception v0

    .line 145785
    sget-object v2, LX/0pn;->a:Ljava/lang/Class;

    const-string v3, "DB.beginTransaction for mutating a story failure"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 145786
    :cond_4
    const/4 v2, 0x0

    move v3, v2

    goto :goto_1

    .line 145787
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 145788
    :catchall_0
    move-exception v0

    const v2, 0x64a95e8e

    invoke-static {v4, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b(Lcom/facebook/api/feedtype/FeedType;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")",
            "LX/0Px",
            "<",
            "LX/44w",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 145735
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145736
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145737
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145738
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145739
    sget-object v1, LX/0pp;->l:LX/0U1;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145740
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    sget-object v1, LX/0pp;->d:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    sget-object v3, LX/0pp;->e:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 145741
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->e:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, LX/0pn;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145742
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 145743
    sget-object v2, LX/0pp;->d:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 145744
    sget-object v3, LX/0pp;->e:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 145745
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 145746
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 145747
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 145748
    new-instance v6, LX/44w;

    invoke-direct {v6, v5, v4}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145749
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145750
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 145687
    invoke-static {p0}, LX/0pn;->m(LX/0pn;)V

    .line 145688
    invoke-static {p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v0

    invoke-virtual {v0}, LX/15j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145689
    invoke-virtual {p0, v5}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;)V

    .line 145690
    invoke-static {p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v0

    invoke-virtual {v0}, LX/15j;->c()V

    .line 145691
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 145692
    :goto_0
    return-object v0

    .line 145693
    :cond_0
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145694
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145695
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145696
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145697
    sget-object v1, LX/0pp;->i:LX/0U1;

    const-string v2, "1"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145698
    sget-object v1, LX/0pp;->l:LX/0U1;

    const-string v2, "0"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145699
    sget-object v1, LX/0pp;->v:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->d()Ljava/lang/String;

    move-result-object v7

    .line 145700
    if-eqz p2, :cond_1

    .line 145701
    invoke-static {p0, p2}, LX/0pn;->e(LX/0pn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145702
    if-eqz v1, :cond_2

    .line 145703
    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145704
    :cond_1
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const-string v8, "10"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 145705
    new-instance v1, LX/0rT;

    invoke-direct {v1}, LX/0rT;-><init>()V

    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 145706
    iput-object v2, v1, LX/0rT;->a:LX/0rS;

    .line 145707
    move-object v1, v1

    .line 145708
    sget-object v2, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    invoke-virtual {v1, v2}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v1

    const/16 v2, 0xa

    .line 145709
    iput v2, v1, LX/0rT;->c:I

    .line 145710
    move-object v1, v1

    .line 145711
    sget-object v2, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 145712
    iput-object v2, v1, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 145713
    move-object v1, v1

    .line 145714
    iput-boolean v9, v1, LX/0rT;->q:Z

    .line 145715
    move-object v1, v1

    .line 145716
    iput-object p1, v1, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 145717
    move-object v1, v1

    .line 145718
    iput-object p2, v1, LX/0rT;->g:Ljava/lang/String;

    .line 145719
    move-object v1, v1

    .line 145720
    invoke-virtual {v1}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v1

    .line 145721
    invoke-static {p0, v0, v1}, LX/0pn;->a(LX/0pn;Landroid/database/Cursor;Lcom/facebook/api/feed/FetchFeedParams;)LX/23A;

    move-result-object v0

    .line 145722
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 145723
    if-eqz v0, :cond_3

    .line 145724
    iget-object v1, v0, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v1, v1

    .line 145725
    if-eqz v1, :cond_3

    .line 145726
    iget-object v1, v0, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v1, v1

    .line 145727
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 145728
    iget-object v1, v0, LX/23A;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v0, v1

    .line 145729
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v9

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 145730
    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 145731
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 145732
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 145733
    goto/16 :goto_0

    .line 145734
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 145685
    invoke-virtual {p0}, LX/0pn;->U_()V

    .line 145686
    return-void
.end method

.method public final b(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 4

    .prologue
    .line 145676
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->d:LX/0qw;

    move-object v0, v0

    .line 145677
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v1

    .line 145678
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v2

    .line 145679
    iget-object v3, v1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v1, v3

    .line 145680
    if-nez v1, :cond_0

    sget-object v1, LX/0qw;->CHUNKED_REMAINDER:LX/0qw;

    if-ne v0, v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145681
    invoke-direct {p0, p1}, LX/0pn;->d(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 145682
    :cond_0
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 145683
    invoke-direct {p0, p1, v0}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 145684
    return-void
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    .locals 27
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145580
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feed/FetchFeedResult;->e()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v14

    .line 145581
    invoke-virtual {v14}, Lcom/facebook/api/feed/FetchFeedParams;->g()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v15

    .line 145582
    invoke-virtual/range {p0 .. p1}, LX/0pn;->b(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 145583
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feed/FetchFeedResult;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v4}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 145584
    :cond_0
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v4

    .line 145585
    :goto_0
    return-object v4

    .line 145586
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0v1;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v16

    .line 145587
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v17

    .line 145588
    invoke-virtual {v15}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v6

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v4, v6, v7}, LX/0pn;->a(Ljava/util/List;Ljava/lang/String;J)Ljava/util/List;

    move-result-object v18

    .line 145589
    invoke-static {v14}, LX/0pn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v19

    .line 145590
    invoke-virtual {v14}, Lcom/facebook/api/feed/FetchFeedParams;->i()LX/0gf;

    move-result-object v4

    sget-object v5, LX/0gf;->PREFETCH:LX/0gf;

    if-eq v4, v5, :cond_5

    if-eqz v19, :cond_5

    const/4 v4, 0x1

    move v13, v4

    .line 145591
    :goto_1
    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, LX/0pn;->j(Lcom/facebook/api/feedtype/FeedType;)Ljava/util/HashSet;

    move-result-object v4

    move-object v8, v4

    .line 145592
    :goto_2
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 145593
    new-instance v21, Landroid/content/ContentValues;

    const/16 v4, 0x19

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 145594
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v22

    .line 145595
    const v4, 0x7050b440

    move-object/from16 v0, v16

    invoke-static {v0, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 145596
    const/4 v4, 0x0

    move v12, v4

    :goto_3
    :try_start_0
    invoke-virtual/range {v17 .. v17}, LX/0Px;->size()I

    move-result v4

    if-ge v12, v4, :cond_e

    .line 145597
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 145598
    invoke-static {v4}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v10

    .line 145599
    if-eqz v13, :cond_2

    if-eqz v8, :cond_2

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v5

    invoke-static {v5}, LX/0pn;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 145600
    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145601
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    .line 145602
    invoke-static {v6, v10}, LX/0x1;->c(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 145603
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, LX/0x1;->d(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 145604
    const/4 v5, 0x0

    invoke-static {v6, v5}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;Z)V

    .line 145605
    instance-of v5, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_3

    .line 145606
    sget-object v5, LX/0pp;->y:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, v6

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v5, v0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 145607
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, LX/0pn;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    .line 145608
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->r:LX/0qO;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0pn;->s:LX/0qT;

    invoke-virtual {v5, v14, v12, v10, v7}, LX/0qO;->a(Lcom/facebook/api/feed/FetchFeedParams;ILjava/lang/String;LX/0qT;)V

    .line 145609
    sget-object v5, LX/0pp;->a:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145610
    sget-object v5, LX/0pp;->b:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 145611
    sget-object v5, LX/0pp;->c:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145612
    sget-object v5, LX/0pp;->d:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145613
    sget-object v5, LX/0pp;->e:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145614
    sget-object v5, LX/0pp;->v:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145615
    sget-object v5, LX/0pp;->w:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145616
    sget-object v5, LX/0pp;->f:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 145617
    sget-object v5, LX/0pp;->g:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145618
    sget-object v5, LX/0pp;->h:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x1

    :goto_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145619
    sget-object v5, LX/0pp;->i:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145620
    sget-object v5, LX/0pp;->j:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145621
    sget-object v5, LX/0pp;->l:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145622
    invoke-static {v4}, LX/1uc;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v23

    .line 145623
    sget-object v5, LX/0pp;->n:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145624
    sget-object v5, LX/0pp;->z:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->name()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145625
    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/31P;

    .line 145626
    sget-object v7, LX/0pp;->o:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v9, v5, LX/31P;->a:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145627
    sget-object v7, LX/0pp;->p:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v0, v5, LX/31P;->b:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 145628
    sget-object v7, LX/0pp;->q:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    iget v9, v5, LX/31P;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145629
    sget-object v7, LX/0pp;->r:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    iget v9, v5, LX/31P;->d:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145630
    sget-object v7, LX/0pp;->s:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 145631
    sget-object v7, LX/0pp;->t:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 145632
    sget-object v7, LX/0pp;->u:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145633
    const/4 v9, 0x0

    .line 145634
    instance-of v7, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v7, :cond_8

    move-object v0, v6

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 145635
    move-object v0, v6

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v7, v0

    invoke-static {v7}, LX/0x1;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v7

    .line 145636
    sget-object v9, LX/0pp;->k:LX/0U1;

    invoke-virtual {v9}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object/from16 v0, v21

    invoke-virtual {v0, v9, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move v9, v7

    .line 145637
    :goto_5
    sget-object v7, LX/0pp;->x:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145638
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0pn;->v:LX/0qR;

    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11, v10}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 145639
    const-wide/16 v10, 0x0

    .line 145640
    instance-of v7, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v7, :cond_a

    .line 145641
    move-object v0, v6

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v10

    .line 145642
    sget-object v7, LX/0pp;->y:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 145643
    :goto_6
    invoke-virtual/range {v21 .. v21}, Landroid/content/ContentValues;->size()I

    move-result v7

    const/16 v24, 0x19

    move/from16 v0, v24

    if-gt v7, v0, :cond_b

    const/4 v7, 0x1

    :goto_7
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 145644
    if-eqz v19, :cond_c

    const/4 v7, 0x5

    .line 145645
    :goto_8
    const-string v24, "home_stories"

    const-string v25, ""

    const v26, -0x76b6d5bd

    invoke-static/range {v26 .. v26}, LX/03h;->a(I)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v7, -0x631e223a

    invoke-static {v7}, LX/03h;->a(I)V

    .line 145646
    invoke-direct/range {p0 .. p0}, LX/0pn;->h()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 145647
    new-instance v7, LX/0x0;

    invoke-direct {v7}, LX/0x0;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->a(Ljava/lang/String;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v7, v0, v1}, LX/0x0;->a(D)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->i(Ljava/lang/String;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v24

    move/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->b(Z)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->b(Ljava/lang/String;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->c(Ljava/lang/String;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, LX/0x0;->d(Ljava/lang/String;)LX/0x0;

    move-result-object v7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0x0;->e(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    iget-object v7, v5, LX/31P;->a:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/0x0;->f(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    iget-wide v0, v5, LX/31P;->b:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    long-to-int v7, v0

    invoke-virtual {v4, v7}, LX/0x0;->b(I)LX/0x0;

    move-result-object v4

    iget v7, v5, LX/31P;->c:I

    invoke-virtual {v4, v7}, LX/0x0;->d(I)LX/0x0;

    move-result-object v4

    iget v5, v5, LX/31P;->d:I

    invoke-virtual {v4, v5}, LX/0x0;->c(I)LX/0x0;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/0x0;->a(Z)LX/0x0;

    move-result-object v4

    invoke-virtual {v15}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0x0;->g(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v4, v0, v1}, LX/0x0;->a(J)LX/0x0;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, LX/0x0;->h(Ljava/lang/String;)LX/0x0;

    move-result-object v5

    if-eqz v9, :cond_d

    const/4 v4, 0x1

    :goto_9
    invoke-virtual {v5, v4}, LX/0x0;->e(I)LX/0x0;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0x0;->f(I)LX/0x0;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0x0;->a(I)LX/0x0;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/15j;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->o:LX/0qN;

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/0qN;)LX/0x0;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0x0;->c(Z)LX/0x0;

    move-result-object v4

    invoke-interface {v6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0x0;->j(Ljava/lang/String;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, LX/0x0;->b(J)LX/0x0;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0pn;->e(LX/0pn;)LX/15j;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/15j;)LX/0x0;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->o:LX/0qN;

    invoke-virtual {v4, v5}, LX/0x0;->a(LX/0qN;)LX/0x0;

    move-result-object v4

    invoke-virtual {v4}, LX/0x0;->a()Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 145648
    :cond_4
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto/16 :goto_3

    .line 145649
    :cond_5
    const/4 v4, 0x0

    move v13, v4

    goto/16 :goto_1

    .line 145650
    :cond_6
    const/4 v4, 0x0

    move-object v8, v4

    goto/16 :goto_2

    .line 145651
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 145652
    :cond_8
    sget-object v7, LX/0pp;->k:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    .line 145653
    :catchall_0
    move-exception v4

    move-object v5, v4

    const v4, 0xfdbf978

    move-object/from16 v0, v16

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 145654
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 145655
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->n:Ljava/util/ArrayList;

    monitor-enter v6

    .line 145656
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, LX/0pn;->n:Ljava/util/ArrayList;

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/31P;

    iget-object v4, v4, LX/31P;->a:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 145657
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_9
    throw v5

    .line 145658
    :cond_a
    :try_start_2
    sget-object v7, LX/0pp;->y:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v24, 0x0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_6

    .line 145659
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 145660
    :cond_c
    const/4 v7, 0x4

    goto/16 :goto_8

    .line 145661
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 145662
    :cond_e
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145663
    const v4, 0x39a57548

    move-object/from16 v0, v16

    invoke-static {v0, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 145664
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_f

    .line 145665
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0pn;->n:Ljava/util/ArrayList;

    monitor-enter v5

    .line 145666
    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, LX/0pn;->n:Ljava/util/ArrayList;

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/31P;

    iget-object v4, v4, LX/31P;->a:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 145667
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 145668
    :cond_f
    sget-object v4, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v4, v15}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-virtual {v14}, Lcom/facebook/api/feed/FetchFeedParams;->e()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    .line 145669
    invoke-static {}, LX/00a;->a()LX/00a;

    move-result-object v4

    sget-object v5, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/0pn;->c(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/00a;->a(Ljava/lang/String;)V

    .line 145670
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, LX/0pn;->c(Lcom/facebook/api/feed/FetchFeedResult;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 145671
    if-eqz v13, :cond_11

    .line 145672
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v15}, LX/0pn;->a(Ljava/util/List;Lcom/facebook/api/feedtype/FeedType;)I

    .line 145673
    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, LX/0pn;->i(Lcom/facebook/api/feedtype/FeedType;)V

    .line 145674
    invoke-virtual/range {v22 .. v22}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_0

    .line 145675
    :catchall_1
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    :catchall_2
    move-exception v4

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v4
.end method

.method public final c(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 145453
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145454
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145455
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145456
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145457
    sget-object v1, LX/0pp;->l:LX/0U1;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145458
    sget-object v9, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v10, p0, LX/0pn;->j:LX/0oy;

    invoke-virtual {v10}, LX/0oy;->i()I

    move-result v10

    int-to-long v11, v10

    invoke-virtual {v9, v11, v12}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    move-wide v2, v9

    .line 145459
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    .line 145460
    sget-object v1, LX/0pp;->b:LX/0U1;

    iget-object v6, p0, LX/0pn;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145461
    :cond_0
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->c:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145462
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145463
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 145464
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 145465
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145466
    :goto_0
    return-object v5

    .line 145467
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final c(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    .line 145547
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145548
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 145549
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145550
    if-lez v0, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 145551
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145552
    const-string v5, "home_stories"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145553
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v8

    .line 145554
    sget-object v5, LX/0pp;->v:LX/0U1;

    invoke-virtual {v5, p2}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145555
    sget-object v5, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145556
    iget-object v5, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0v1;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, LX/0pn;->b:[Ljava/lang/String;

    invoke-virtual {v8}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v8

    sget-object v10, LX/0pp;->v:LX/0U1;

    invoke-virtual {v10}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object v10, v9

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    move-object v1, v4

    .line 145557
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 145558
    const/4 v0, 0x0

    .line 145559
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145560
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 145561
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145562
    return-object v0

    .line 145563
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 145564
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 6

    .prologue
    .line 145542
    iget-object v1, p0, LX/0pn;->j:LX/0oy;

    iget-object v0, p0, LX/0pn;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 145543
    iget v2, v1, LX/0oy;->G:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 145544
    iget-object v2, v1, LX/0oy;->a:LX/0W3;

    sget-wide v4, LX/0X5;->gL:J

    invoke-interface {v2, v4, v5, v0}, LX/0W4;->a(JI)I

    move-result v2

    iput v2, v1, LX/0oy;->G:I

    .line 145545
    :cond_0
    iget v2, v1, LX/0oy;->G:I

    move v0, v2

    .line 145546
    return v0
.end method

.method public final d(Lcom/facebook/api/feedtype/FeedType;)I
    .locals 1

    .prologue
    .line 145541
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, LX/0pn;->a(ILcom/facebook/api/feedtype/FeedType;)I

    move-result v0

    return v0
.end method

.method public final e(Lcom/facebook/api/feedtype/FeedType;)J
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 145537
    iget-object v0, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "select count("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") from home_stories"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " where "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/0pp;->a:LX/0U1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 145538
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 145539
    const v0, 0x7fa87d51

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v2

    const v0, -0x6882aa4f

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145540
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v2

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method

.method public final f(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 145518
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145519
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145520
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145521
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145522
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/0pp;->c:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    sget-object v3, LX/0pp;->d:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x2

    sget-object v3, LX/0pp;->e:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x3

    sget-object v3, LX/0pp;->l:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 145523
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 145524
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "cache_size: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0pn;->e(Lcom/facebook/api/feedtype/FeedType;)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145525
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->e:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v7

    const-string v8, "100"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145526
    sget-object v0, LX/0pp;->c:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145527
    sget-object v2, LX/0pp;->d:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 145528
    sget-object v3, LX/0pp;->e:LX/0U1;

    invoke-virtual {v3, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 145529
    sget-object v4, LX/0pp;->l:LX/0U1;

    invoke-virtual {v4, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 145530
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 145531
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " :\t "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145532
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " :\t "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145533
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " :\t "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145534
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145535
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145536
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 145506
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145507
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145508
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145509
    sget-object v1, LX/0pp;->l:LX/0U1;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145510
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145511
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/0pn;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/0pp;->v:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->d()Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 145512
    sget-object v0, LX/0pp;->w:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 145513
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145514
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 145515
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145516
    return-object v5

    .line 145517
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final h(Lcom/facebook/api/feedtype/FeedType;)LX/69J;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 145478
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 145479
    const-string v1, "home_stories"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 145480
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 145481
    sget-object v1, LX/0pp;->a:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145482
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v1

    .line 145483
    sget-object v2, LX/0pp;->n:LX/0U1;

    const-string v3, "Page"

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145484
    sget-object v2, LX/0pp;->n:LX/0U1;

    const-string v3, "User"

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145485
    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145486
    sget-object v1, LX/0pp;->l:LX/0U1;

    const-string v2, "0"

    invoke-virtual {v1, v2}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 145487
    iget-object v1, p0, LX/0pn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0v1;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, LX/0pp;->i:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    sget-object v3, LX/0pp;->j:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    const/4 v3, 0x2

    sget-object v6, LX/0pp;->d:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 145488
    :try_start_0
    sget-object v0, LX/0pp;->i:LX/0U1;

    invoke-virtual {v0, v3}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 145489
    sget-object v0, LX/0pp;->j:LX/0U1;

    invoke-virtual {v0, v3}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 145490
    sget-object v0, LX/0pp;->d:LX/0U1;

    invoke-virtual {v0, v3}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    move v1, v8

    move v0, v8

    move v2, v8

    .line 145491
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 145492
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 145493
    if-ne v7, v11, :cond_2

    .line 145494
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 145495
    add-int/lit8 v8, v8, 0x1

    .line 145496
    if-eqz v7, :cond_1

    if-ne v7, v11, :cond_0

    .line 145497
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145498
    :cond_2
    if-nez v7, :cond_0

    .line 145499
    add-int/lit8 v2, v2, 0x1

    .line 145500
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 145501
    iget-object v9, p0, LX/0pn;->t:LX/0qP;

    invoke-virtual {v9, v7}, LX/0qP;->a(Ljava/lang/String;)LX/14t;

    move-result-object v7

    .line 145502
    const-string v9, "PHOTO"

    invoke-virtual {v7, v9}, LX/14t;->b(Ljava/lang/String;)I

    move-result v9

    const-string v10, "PHOTO"

    invoke-virtual {v7, v10}, LX/14t;->a(Ljava/lang/String;)I

    move-result v7

    if-ne v9, v7, :cond_0

    .line 145503
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145504
    :cond_3
    new-instance v4, LX/69J;

    invoke-direct {v4, v8, v2, v0, v1}, LX/69J;-><init>(IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145505
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    return-object v4

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0
.end method
