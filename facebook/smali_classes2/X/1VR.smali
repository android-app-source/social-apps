.class public final LX/1VR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<THelper:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TTItem;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:I

.field private final c:I

.field private final d:LX/1VQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VQ",
            "<TTHelper;TTItem;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTHelper;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "LX/1VQ",
            "<TTHelper;TTItem;>;TTHelper;)V"
        }
    .end annotation

    .prologue
    .line 260278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260279
    iput-object p1, p0, LX/1VR;->a:Ljava/nio/ByteBuffer;

    .line 260280
    iput p2, p0, LX/1VR;->b:I

    .line 260281
    iput p3, p0, LX/1VR;->c:I

    .line 260282
    iput-object p4, p0, LX/1VR;->d:LX/1VQ;

    .line 260283
    iput-object p5, p0, LX/1VR;->e:Ljava/lang/Object;

    .line 260284
    const/4 v0, 0x0

    iput v0, p0, LX/1VR;->f:I

    .line 260285
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 260286
    iget v0, p0, LX/1VR;->f:I

    iget v1, p0, LX/1VR;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTItem;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260287
    iget v0, p0, LX/1VR;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/1VR;->f:I

    iget v1, p0, LX/1VR;->c:I

    if-lt v0, v1, :cond_1

    .line 260288
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Out of bound for iteration"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260289
    :cond_1
    iget-object v0, p0, LX/1VR;->d:LX/1VQ;

    iget-object v1, p0, LX/1VR;->e:Ljava/lang/Object;

    iget-object v2, p0, LX/1VR;->a:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/1VR;->b:I

    iget v4, p0, LX/1VR;->f:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/1VR;->f:I

    invoke-interface {v0, v1, v2, v3, v4}, LX/1VQ;->a(Ljava/lang/Object;Ljava/nio/ByteBuffer;II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 260290
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
