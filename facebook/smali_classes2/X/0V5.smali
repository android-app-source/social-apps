.class public LX/0V5;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67372
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 67373
    return-void
.end method

.method public static a()LX/0V6;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 67374
    new-instance v0, LX/0V6;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0V6;-><init>(Ljava/lang/Runtime;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/diagnostics/DebugLogsDefaultedOnAndroid;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/diagnostics/IsDebugLogsEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 67375
    sget-object v1, LX/0eJ;->g:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 67376
    if-nez v1, :cond_0

    .line 67377
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 67378
    :cond_0
    sget-object v1, LX/0eJ;->b:LX/0Tn;

    invoke-interface {p0, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 67379
    return-void
.end method
