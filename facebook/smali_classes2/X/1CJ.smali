.class public LX/1CJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1CJ;


# instance fields
.field public final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "LX/4Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216044
    new-instance v0, LX/0aq;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1CJ;->a:LX/0aq;

    .line 216045
    return-void
.end method

.method public static a(LX/0QB;)LX/1CJ;
    .locals 3

    .prologue
    .line 216046
    sget-object v0, LX/1CJ;->b:LX/1CJ;

    if-nez v0, :cond_1

    .line 216047
    const-class v1, LX/1CJ;

    monitor-enter v1

    .line 216048
    :try_start_0
    sget-object v0, LX/1CJ;->b:LX/1CJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 216049
    if-eqz v2, :cond_0

    .line 216050
    :try_start_1
    new-instance v0, LX/1CJ;

    invoke-direct {v0}, LX/1CJ;-><init>()V

    .line 216051
    move-object v0, v0

    .line 216052
    sput-object v0, LX/1CJ;->b:LX/1CJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216053
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 216054
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216055
    :cond_1
    sget-object v0, LX/1CJ;->b:LX/1CJ;

    return-object v0

    .line 216056
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 216057
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;
    .locals 3

    .prologue
    .line 216058
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 216059
    iget-object v0, p0, LX/1CJ;->a:LX/0aq;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Zb;

    .line 216060
    if-eqz v0, :cond_0

    .line 216061
    :goto_0
    return-object v0

    .line 216062
    :cond_0
    new-instance v0, LX/4Zb;

    invoke-direct {v0}, LX/4Zb;-><init>()V

    .line 216063
    iget-object v2, p0, LX/1CJ;->a:LX/0aq;

    invoke-virtual {v2, v1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
