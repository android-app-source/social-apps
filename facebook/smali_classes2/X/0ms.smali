.class public final LX/0ms;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Z

.field private static final b:Ljava/lang/String;

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 133134
    const-string v0, "is_ctscan_v2_testing"

    invoke-static {v0}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, LX/0ms;->a:Z

    .line 133135
    const-string v0, "ctscan_v2_logger_events"

    invoke-static {v0}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133136
    sput-object v0, LX/0ms;->b:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ms;->c:Ljava/util/List;

    return-void
.end method
