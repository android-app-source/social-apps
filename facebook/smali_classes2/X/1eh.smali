.class public final LX/1eh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1eh;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 288881
    new-instance v0, LX/1eh;

    invoke-direct {v0}, LX/1eh;-><init>()V

    sput-object v0, LX/1eh;->a:LX/1eh;

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    .prologue
    .line 288882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288883
    const-string v0, "java.runtime.name"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288884
    if-nez v0, :cond_2

    .line 288885
    const/4 v0, 0x0

    .line 288886
    :goto_0
    move v0, v0

    .line 288887
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/1eh;->b:Ljava/util/concurrent/ExecutorService;

    .line 288888
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, LX/1eh;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 288889
    new-instance v0, LX/1ek;

    invoke-direct {v0}, LX/1ek;-><init>()V

    iput-object v0, p0, LX/1eh;->d:Ljava/util/concurrent/Executor;

    .line 288890
    return-void

    .line 288891
    :cond_0
    new-instance v3, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v4, LX/1ei;->a:I

    sget v5, LX/1ei;->b:I

    const-wide/16 v6, 0x1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v9, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v9}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v3 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 288892
    const/4 v2, 0x1

    .line 288893
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x9

    if-lt v4, v5, :cond_1

    .line 288894
    invoke-virtual {v3, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 288895
    :cond_1
    move-object v0, v3

    .line 288896
    goto :goto_1

    :cond_2
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 288897
    sget-object v0, LX/1eh;->a:LX/1eh;

    iget-object v0, v0, LX/1eh;->b:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method
