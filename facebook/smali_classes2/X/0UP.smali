.class public final LX/0UP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Up;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Up;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 64547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64548
    iput-object p1, p0, LX/0UP;->a:LX/0QB;

    .line 64549
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64550
    new-instance v0, LX/0UP;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0UP;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 64551
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64552
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0UP;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 64553
    packed-switch p2, :pswitch_data_0

    .line 64554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64555
    :pswitch_0
    invoke-static {p1}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v0

    .line 64556
    :goto_0
    return-object v0

    .line 64557
    :pswitch_1
    invoke-static {p1}, LX/2Bv;->a(LX/0QB;)LX/2Bv;

    move-result-object v0

    goto :goto_0

    .line 64558
    :pswitch_2
    invoke-static {p1}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v0

    goto :goto_0

    .line 64559
    :pswitch_3
    new-instance v2, LX/29O;

    invoke-static {p1}, LX/29P;->b(LX/0QB;)LX/29P;

    move-result-object v3

    check-cast v3, LX/2W8;

    const/16 v4, 0x29f

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p1}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v5

    check-cast v5, LX/0Sg;

    invoke-static {p1}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p1}, LX/0TE;->a(LX/0QB;)LX/0TE;

    move-result-object v7

    check-cast v7, LX/0TE;

    invoke-direct/range {v2 .. v7}, LX/29O;-><init>(LX/2W8;LX/0Ot;LX/0Sg;Ljava/util/concurrent/Executor;LX/0TE;)V

    .line 64560
    move-object v0, v2

    .line 64561
    goto :goto_0

    .line 64562
    :pswitch_4
    invoke-static {p1}, LX/29l;->a(LX/0QB;)LX/29l;

    move-result-object v0

    goto :goto_0

    .line 64563
    :pswitch_5
    invoke-static {p1}, LX/2CA;->a(LX/0QB;)LX/2CA;

    move-result-object v0

    goto :goto_0

    .line 64564
    :pswitch_6
    invoke-static {p1}, LX/29w;->a(LX/0QB;)LX/29w;

    move-result-object v0

    goto :goto_0

    .line 64565
    :pswitch_7
    invoke-static {p1}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64566
    const/16 v0, 0x8

    return v0
.end method
