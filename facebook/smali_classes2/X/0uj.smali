.class public LX/0uj;
.super LX/0uk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0uk",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0sZ;

.field private final b:LX/0W3;

.field private final c:Z

.field public final d:Z

.field private final e:LX/0SG;

.field public final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sZ;LX/0W3;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/0SG;LX/0Or;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sZ;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/0tr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 156891
    invoke-direct {p0}, LX/0uk;-><init>()V

    .line 156892
    iput-object p1, p0, LX/0uj;->a:LX/0sZ;

    .line 156893
    iput-object p2, p0, LX/0uj;->b:LX/0W3;

    .line 156894
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0uj;->c:Z

    .line 156895
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0uj;->d:Z

    .line 156896
    iput-object p5, p0, LX/0uj;->e:LX/0SG;

    .line 156897
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0uj;->f:Ljava/util/HashMap;

    .line 156898
    iput-object p6, p0, LX/0uj;->g:LX/0Or;

    .line 156899
    return-void
.end method

.method public static h()Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 156890
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    const/16 v2, 0x19

    sget-object v3, LX/21x;->COMPLETE:LX/21x;

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    move-object v6, v1

    move-object v7, v1

    move v10, v9

    move v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method

.method private i()I
    .locals 4

    .prologue
    .line 156889
    iget-object v0, p0, LX/0uj;->b:LX/0W3;

    sget-wide v2, LX/0X5;->cZ:J

    const/16 v1, 0x19

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0zO;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 156883
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    invoke-direct {p0}, LX/0uj;->i()I

    move-result v2

    sget-object v3, LX/21x;->COMPLETE:LX/21x;

    sget-object v4, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    .line 156884
    iget-boolean v1, p0, LX/0uj;->d:Z

    if-nez v1, :cond_0

    .line 156885
    const/4 v1, 0x0

    .line 156886
    :goto_0
    move-object v6, v1

    .line 156887
    const/4 v7, 0x0

    iget-boolean v9, p0, LX/0uj;->c:Z

    const/4 v10, 0x0

    move-object v1, p1

    move v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 156888
    iget-object v1, p0, LX/0uj;->a:LX/0sZ;

    invoke-virtual {v1, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, LX/0uj;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 156869
    iget-boolean v0, p0, LX/0uj;->d:Z

    if-eqz v0, :cond_0

    .line 156870
    iget-object v0, p0, LX/0uj;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156871
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 5

    .prologue
    .line 156880
    iget-object v0, p0, LX/0uj;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tr;

    .line 156881
    iget-object v1, v0, LX/0tr;->a:LX/0uf;

    sget-wide v3, LX/0X5;->cS:J

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v4, v2}, LX/0uf;->a(JZ)Z

    move-result v1

    move v0, v1

    .line 156882
    return v0
.end method

.method public final a(LX/0uh;)Z
    .locals 5

    .prologue
    .line 156875
    iget-object v0, p0, LX/0uj;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tr;

    .line 156876
    iget-object v1, v0, LX/0tr;->a:LX/0uf;

    sget-wide v3, LX/0X5;->cT:J

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v4, v2}, LX/0uf;->a(JZ)Z

    move-result v1

    move v0, v1

    .line 156877
    if-eqz v0, :cond_0

    .line 156878
    sget-object v0, LX/0uh;->LOW:LX/0uh;

    invoke-virtual {p1, v0}, LX/0uh;->isAtLeast(LX/0uh;)Z

    move-result v0

    .line 156879
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/0uh;->HIGH:LX/0uh;

    invoke-virtual {p1, v0}, LX/0uh;->isAtLeast(LX/0uh;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 156900
    iget-object v1, p0, LX/0uj;->b:LX/0W3;

    sget-wide v2, LX/0X5;->cX:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0uj;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 156901
    iget-wide v8, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v8

    .line 156902
    sub-long/2addr v2, v4

    iget-object v1, p0, LX/0uj;->b:LX/0W3;

    sget-wide v4, LX/0X5;->cY:J

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v5, v6, v7}, LX/0W4;->a(JJ)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/0zO;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 156873
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    invoke-direct {p0}, LX/0uj;->i()I

    move-result v2

    sget-object v3, LX/21x;->COMPLETE:LX/21x;

    sget-object v4, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    iget-boolean v9, p0, LX/0uj;->c:Z

    const/4 v10, 0x0

    move-object v1, p1

    move-object v7, v6

    move v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 156874
    iget-object v1, p0, LX/0uj;->a:LX/0sZ;

    invoke-virtual {v1, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 156872
    iget-object v0, p0, LX/0uj;->b:LX/0W3;

    sget-wide v2, LX/0X5;->db:J

    const v1, 0x3f480

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 156866
    iget-boolean v0, p0, LX/0uj;->d:Z

    if-eqz v0, :cond_0

    .line 156867
    iget-object v0, p0, LX/0uj;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156868
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156865
    const-string v0, "comments_prefetch"

    return-object v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 156864
    iget-object v0, p0, LX/0uj;->b:LX/0W3;

    sget-wide v2, LX/0X5;->cW:J

    const/4 v1, 0x1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 156861
    iget-boolean v0, p0, LX/0uj;->d:Z

    if-eqz v0, :cond_0

    .line 156862
    const/4 v0, 0x1

    .line 156863
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0uj;->b:LX/0W3;

    sget-wide v2, LX/0X5;->da:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    goto :goto_0
.end method
