.class public final LX/1fp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1fk;

.field private b:I


# direct methods
.method public constructor <init>(LX/1fk;I)V
    .locals 0

    .prologue
    .line 292361
    iput-object p1, p0, LX/1fp;->a:LX/1fk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292362
    iput p2, p0, LX/1fp;->b:I

    .line 292363
    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292364
    invoke-interface {p1}, LX/1ca;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292365
    iget-object v0, p0, LX/1fp;->a:LX/1fk;

    iget v1, p0, LX/1fp;->b:I

    .line 292366
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v2

    invoke-static {v0, v1, p1, v2}, LX/1fk;->a(LX/1fk;ILX/1ca;Z)V

    .line 292367
    invoke-static {v0}, LX/1fk;->h(LX/1fk;)LX/1ca;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 292368
    const/4 p0, 0x0

    if-nez v1, :cond_2

    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v0, p0, v2}, LX/1cZ;->a(Ljava/lang/Object;Z)Z

    .line 292369
    :cond_0
    :goto_1
    return-void

    .line 292370
    :cond_1
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292371
    iget-object v0, p0, LX/1fp;->a:LX/1fk;

    iget v1, p0, LX/1fp;->b:I

    invoke-static {v0, v1, p1}, LX/1fk;->b$redex0(LX/1fk;ILX/1ca;)V

    goto :goto_1

    .line 292372
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292373
    iget-object v0, p0, LX/1fp;->a:LX/1fk;

    iget v1, p0, LX/1fp;->b:I

    invoke-static {v0, v1, p1}, LX/1fk;->b$redex0(LX/1fk;ILX/1ca;)V

    .line 292374
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292375
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292376
    iget v0, p0, LX/1fp;->b:I

    if-nez v0, :cond_0

    .line 292377
    iget-object v0, p0, LX/1fp;->a:LX/1fk;

    invoke-interface {p1}, LX/1ca;->f()F

    move-result v1

    invoke-virtual {v0, v1}, LX/1cZ;->a(F)Z

    .line 292378
    :cond_0
    return-void
.end method
