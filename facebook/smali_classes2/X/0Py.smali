.class public abstract LX/0Py;
.super Ljava/util/AbstractCollection;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractCollection",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private transient asList:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57636
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public asList()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57632
    iget-object v0, p0, LX/0Py;->asList:LX/0Px;

    .line 57633
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Py;->createAsList()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/0Py;->asList:LX/0Px;

    :cond_0
    return-object v0
.end method

.method public final clear()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57619
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public abstract contains(Ljava/lang/Object;)Z
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public copyIntoArray([Ljava/lang/Object;I)I
    .locals 3

    .prologue
    .line 57628
    invoke-virtual {p0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 57629
    add-int/lit8 v0, p2, 0x1

    aput-object v2, p1, p2

    move p2, v0

    .line 57630
    goto :goto_0

    .line 57631
    :cond_0
    return p2
.end method

.method public createAsList()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57623
    invoke-virtual {p0}, LX/0Py;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 57624
    new-instance v0, LX/12p;

    invoke-virtual {p0}, LX/0Py;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/12p;-><init>(LX/0Py;[Ljava/lang/Object;)V

    :goto_0
    return-object v0

    .line 57625
    :pswitch_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 57626
    goto :goto_0

    .line 57627
    :pswitch_1
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract isPartialView()Z
.end method

.method public abstract iterator()LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 57622
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57621
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57620
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57618
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57612
    invoke-virtual {p0}, LX/0Py;->size()I

    move-result v0

    .line 57613
    if-nez v0, :cond_0

    .line 57614
    sget-object v0, LX/0P8;->a:[Ljava/lang/Object;

    .line 57615
    :goto_0
    return-object v0

    .line 57616
    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 57617
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Py;->copyIntoArray([Ljava/lang/Object;I)I

    goto :goto_0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 57604
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57605
    invoke-virtual {p0}, LX/0Py;->size()I

    move-result v0

    .line 57606
    array-length v1, p1

    if-ge v1, v0, :cond_1

    .line 57607
    invoke-static {p1, v0}, LX/0P9;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 57608
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0Py;->copyIntoArray([Ljava/lang/Object;I)I

    .line 57609
    return-object p1

    .line 57610
    :cond_1
    array-length v1, p1

    if-le v1, v0, :cond_0

    .line 57611
    const/4 v1, 0x0

    aput-object v1, p1, v0

    goto :goto_0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57603
    new-instance v0, LX/4y8;

    invoke-virtual {p0}, LX/0Py;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4y8;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
