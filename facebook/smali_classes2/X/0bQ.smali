.class public final LX/0bQ;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;",
        "LX/8Mf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/8Mf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86931
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86932
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86930
    const-class v0, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 86891
    check-cast p1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;

    check-cast p2, LX/8Mf;

    .line 86892
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86893
    sget-object v1, LX/8KZ;->MEDIA_PROCESSING_SUCCESS:LX/8KZ;

    if-eq v0, v1, :cond_0

    .line 86894
    iget-object v0, p1, LX/0b5;->b:LX/8KZ;

    move-object v0, v0

    .line 86895
    sget-object v1, LX/8KZ;->MEDIA_PROCESSING_FAILED:LX/8KZ;

    if-eq v0, v1, :cond_0

    .line 86896
    :goto_0
    return-void

    .line 86897
    :cond_0
    iget-object v1, p2, LX/8Mf;->c:LX/7m8;

    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    .line 86898
    iget-object v0, p1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->a:Ljava/lang/String;

    move-object v3, v0

    .line 86899
    iget-object v0, p1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v4, v0

    .line 86900
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86901
    iget-object v5, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v5, v5

    .line 86902
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86903
    iget-wide v10, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v6, v10

    .line 86904
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86905
    iget-object v8, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v8, v8

    .line 86906
    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v9, 0x0

    .line 86907
    iput-boolean v9, v0, LX/2rc;->a:Z

    .line 86908
    move-object v0, v0

    .line 86909
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v9

    .line 86910
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 86911
    const-string v10, "com.facebook.STREAM_PUBLISH_MEDIA_PROCESSING_COMPLETE"

    invoke-virtual {v0, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 86912
    const-string v10, "extra_result"

    invoke-virtual {v2}, LX/7m7;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86913
    const-string v10, "extra_legacy_api_post_id"

    invoke-virtual {v0, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86914
    const-string v10, "graphql_story"

    invoke-static {v0, v10, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 86915
    const-string v10, "extra_request_id"

    invoke-virtual {v0, v10, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86916
    const-string v10, "extra_target_id"

    invoke-virtual {v0, v10, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 86917
    const-string v10, "extra_target_type"

    invoke-virtual {v0, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86918
    const-string v10, "extra_error_details"

    invoke-virtual {v0, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86919
    iget-object v10, v1, LX/7m8;->a:LX/0Xl;

    invoke-interface {v10, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 86920
    iget-object v0, p2, LX/8Mf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    .line 86921
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86922
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 86923
    iget-object v2, p1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 86924
    iget-object v3, p1, Lcom/facebook/photos/upload/event/MediaServerProcessingEvent;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v3

    .line 86925
    invoke-static {v0, v1, v2, v3}, LX/7md;->c(LX/7md;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 86926
    iget-object v0, p2, LX/8Mf;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 86927
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86928
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 86929
    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
