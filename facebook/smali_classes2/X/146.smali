.class public final LX/146;
.super LX/147;
.source ""

# interfaces
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/147",
        "<TK;TV;>;",
        "Ljava/util/NavigableMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/146;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/146",
            "<",
            "Ljava/lang/Comparable;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final transient c:LX/50U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50U",
            "<TK;>;"
        }
    .end annotation
.end field

.field public final transient d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TV;>;"
        }
    .end annotation
.end field

.field private transient e:LX/146;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 178143
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 178144
    sput-object v0, LX/146;->a:Ljava/util/Comparator;

    .line 178145
    new-instance v0, LX/146;

    .line 178146
    sget-object v1, LX/1zb;->a:LX/1zb;

    move-object v1, v1

    .line 178147
    invoke-static {v1}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object v1

    .line 178148
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 178149
    invoke-direct {v0, v1, v2}, LX/146;-><init>(LX/50U;LX/0Px;)V

    sput-object v0, LX/146;->b:LX/146;

    return-void
.end method

.method public constructor <init>(LX/50U;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50U",
            "<TK;>;",
            "LX/0Px",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 178074
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/146;-><init>(LX/50U;LX/0Px;LX/146;)V

    .line 178075
    return-void
.end method

.method private constructor <init>(LX/50U;LX/0Px;LX/146;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50U",
            "<TK;>;",
            "LX/0Px",
            "<TV;>;",
            "LX/146",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 178076
    invoke-direct {p0}, LX/147;-><init>()V

    .line 178077
    iput-object p1, p0, LX/146;->c:LX/50U;

    .line 178078
    iput-object p2, p0, LX/146;->d:LX/0Px;

    .line 178079
    iput-object p3, p0, LX/146;->e:LX/146;

    .line 178080
    return-void
.end method

.method private a(II)LX/146;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178081
    if-nez p1, :cond_0

    invoke-virtual {p0}, LX/146;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 178082
    :goto_0
    return-object p0

    .line 178083
    :cond_0
    if-ne p1, p2, :cond_1

    .line 178084
    invoke-virtual {p0}, LX/146;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, LX/146;->a(Ljava/util/Comparator;)LX/146;

    move-result-object p0

    goto :goto_0

    .line 178085
    :cond_1
    new-instance v0, LX/146;

    iget-object v1, p0, LX/146;->c:LX/50U;

    invoke-virtual {v1, p1, p2}, LX/50U;->a(II)LX/50U;

    move-result-object v1

    iget-object v2, p0, LX/146;->d:LX/0Px;

    invoke-virtual {v2, p1, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/146;-><init>(LX/50U;LX/0Px;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Z)LX/146;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178086
    const/4 v0, 0x0

    iget-object v1, p0, LX/146;->c:LX/50U;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, LX/50U;->e(Ljava/lang/Object;Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, LX/146;->a(II)LX/146;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/146;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ZTK;Z)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178087
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178088
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178089
    invoke-virtual {p0}, LX/146;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expected fromKey <= toKey but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 178090
    invoke-direct {p0, p3, p4}, LX/146;->a(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    invoke-direct {v0, p1, p2}, LX/146;->b(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 178091
    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;)LX/146;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178092
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 178093
    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178094
    sget-object v0, LX/146;->b:LX/146;

    move-object v0, v0

    .line 178095
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/146;

    invoke-static {p0}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object v1

    .line 178096
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 178097
    invoke-direct {v0, v1, v2}, LX/146;-><init>(LX/50U;LX/0Px;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;Z)LX/146;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178098
    iget-object v0, p0, LX/146;->c:LX/50U;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/50U;->f(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, LX/146;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, LX/146;->a(II)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)LX/146;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;TK;TV;)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178099
    new-instance v1, LX/146;

    new-instance v2, LX/50U;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v2, v3, v0}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/146;-><init>(LX/50U;LX/0Px;)V

    return-object v1
.end method

.method public static b(Ljava/util/Comparator;Z[Ljava/util/Map$Entry;I)LX/146;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TK;>;Z[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;I)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178100
    packed-switch p3, :pswitch_data_0

    .line 178101
    new-array v5, p3, [Ljava/lang/Object;

    .line 178102
    new-array v6, p3, [Ljava/lang/Object;

    .line 178103
    if-eqz p1, :cond_0

    .line 178104
    :goto_0
    if-ge v2, p3, :cond_2

    .line 178105
    aget-object v0, p2, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 178106
    aget-object v1, p2, v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 178107
    invoke-static {v0, v1}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 178108
    aput-object v0, v5, v2

    .line 178109
    aput-object v1, v6, v2

    .line 178110
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 178111
    :pswitch_0
    invoke-static {p0}, LX/146;->a(Ljava/util/Comparator;)LX/146;

    move-result-object v0

    .line 178112
    :goto_1
    return-object v0

    .line 178113
    :pswitch_1
    aget-object v0, p2, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aget-object v1, p2, v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/146;->b(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)LX/146;

    move-result-object v0

    goto :goto_1

    .line 178114
    :cond_0
    invoke-static {p0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->e()LX/1sm;

    move-result-object v0

    invoke-static {p2, v2, p3, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 178115
    aget-object v0, p2, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 178116
    aput-object v0, v5, v2

    .line 178117
    aget-object v3, p2, v2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v6, v2

    move v3, v1

    .line 178118
    :goto_2
    if-ge v3, p3, :cond_2

    .line 178119
    aget-object v4, p2, v3

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 178120
    aget-object v7, p2, v3

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    .line 178121
    invoke-static {v4, v7}, LX/0P6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 178122
    aput-object v4, v5, v3

    .line 178123
    aput-object v7, v6, v3

    .line 178124
    invoke-interface {p0, v0, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_3
    const-string v7, "key"

    add-int/lit8 v8, v3, -0x1

    aget-object v8, p2, v8

    aget-object v9, p2, v3

    invoke-static {v0, v7, v8, v9}, LX/0P1;->checkNoConflict(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 178125
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move-object v0, v4

    goto :goto_2

    :cond_1
    move v0, v2

    .line 178126
    goto :goto_3

    .line 178127
    :cond_2
    new-instance v0, LX/146;

    new-instance v1, LX/50U;

    new-instance v2, LX/0Q7;

    invoke-direct {v2, v5}, LX/0Q7;-><init>([Ljava/lang/Object;)V

    invoke-direct {v1, v2, p0}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    new-instance v2, LX/0Q7;

    invoke-direct {v2, v6}, LX/0Q7;-><init>([Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, LX/146;-><init>(LX/50U;LX/0Px;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/util/Map;Ljava/util/Comparator;)LX/146;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<+TK;+TV;>;",
            "Ljava/util/Comparator",
            "<-TK;>;)",
            "LX/146",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 178128
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 178129
    check-cast v0, Ljava/util/SortedMap;

    .line 178130
    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 178131
    if-nez v0, :cond_2

    sget-object v0, LX/146;->a:Ljava/util/Comparator;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 178132
    :cond_0
    if-eqz v1, :cond_3

    instance-of v0, p0, LX/146;

    if-eqz v0, :cond_3

    move-object v0, p0

    .line 178133
    check-cast v0, LX/146;

    .line 178134
    invoke-virtual {v0}, LX/146;->isPartialView()Z

    move-result v2

    if-nez v2, :cond_3

    .line 178135
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 178136
    goto :goto_0

    :cond_2
    invoke-interface {p1, v0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 178137
    :cond_3
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 178138
    sget-object v2, LX/0P1;->EMPTY_ENTRY_ARRAY:[Ljava/util/Map$Entry;

    invoke-static {v0, v2}, LX/0Ph;->a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/util/Map$Entry;

    check-cast v2, [Ljava/util/Map$Entry;

    .line 178139
    array-length p0, v2

    invoke-static {p1, v1, v2, p0}, LX/146;->b(Ljava/util/Comparator;Z[Ljava/util/Map$Entry;I)LX/146;

    move-result-object v2

    move-object v0, v2

    .line 178140
    goto :goto_1
.end method


# virtual methods
.method public final ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178142
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/146;->b(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    invoke-virtual {v0}, LX/146;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 178166
    invoke-virtual {p0, p1}, LX/146;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation

    .prologue
    .line 178164
    iget-object v0, p0, LX/146;->c:LX/50U;

    move-object v0, v0

    .line 178165
    invoke-virtual {v0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final createEntrySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 178161
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178162
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 178163
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4yf;

    invoke-direct {v0, p0}, LX/4yf;-><init>(LX/146;)V

    goto :goto_0
.end method

.method public final descendingKeySet()Ljava/util/NavigableSet;
    .locals 1

    .prologue
    .line 178160
    iget-object v0, p0, LX/146;->c:LX/50U;

    invoke-virtual {v0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .locals 3

    .prologue
    .line 178154
    iget-object v0, p0, LX/146;->e:LX/146;

    .line 178155
    if-nez v0, :cond_0

    .line 178156
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178157
    invoke-virtual {p0}, LX/146;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    invoke-static {v0}, LX/146;->a(Ljava/util/Comparator;)LX/146;

    move-result-object v0

    .line 178158
    :cond_0
    :goto_0
    return-object v0

    .line 178159
    :cond_1
    new-instance v1, LX/146;

    iget-object v0, p0, LX/146;->c:LX/50U;

    invoke-virtual {v0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    check-cast v0, LX/50U;

    iget-object v2, p0, LX/146;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v0, v2, p0}, LX/146;-><init>(LX/50U;LX/0Px;LX/146;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final entrySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 178153
    invoke-super {p0}, LX/147;->entrySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 178152
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final firstEntry()Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178141
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method

.method public final firstKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 178150
    iget-object v0, p0, LX/146;->c:LX/50U;

    move-object v0, v0

    .line 178151
    invoke-virtual {v0}, LX/0dW;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178072
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/146;->a(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    invoke-virtual {v0}, LX/146;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 178073
    invoke-virtual {p0, p1}, LX/146;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 178045
    iget-object v0, p0, LX/146;->c:LX/50U;

    invoke-virtual {v0, p1}, LX/50U;->a(Ljava/lang/Object;)I

    move-result v0

    .line 178046
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/146;->d:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 178047
    invoke-direct {p0, p1, p2}, LX/146;->a(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 178071
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/146;->a(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178048
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/146;->b(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    invoke-virtual {v0}, LX/146;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 178049
    invoke-virtual {p0, p1}, LX/146;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 178050
    iget-object v0, p0, LX/146;->c:LX/50U;

    invoke-virtual {v0}, LX/50U;->isPartialView()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/146;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic keySet()LX/0Rf;
    .locals 1

    .prologue
    .line 178051
    iget-object v0, p0, LX/146;->c:LX/50U;

    move-object v0, v0

    .line 178052
    return-object v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 178053
    iget-object v0, p0, LX/146;->c:LX/50U;

    move-object v0, v0

    .line 178054
    return-object v0
.end method

.method public final lastEntry()Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178055
    invoke-virtual {p0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, LX/146;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method

.method public final lastKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 178056
    iget-object v0, p0, LX/146;->c:LX/50U;

    move-object v0, v0

    .line 178057
    invoke-virtual {v0}, LX/0dW;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 178058
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/146;->a(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    invoke-virtual {v0}, LX/146;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TK;"
        }
    .end annotation

    .prologue
    .line 178059
    invoke-virtual {p0, p1}, LX/146;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .locals 1

    .prologue
    .line 178060
    iget-object v0, p0, LX/146;->c:LX/50U;

    return-object v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 178061
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 178062
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 178063
    iget-object v0, p0, LX/146;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 178064
    invoke-direct {p0, p1, p2, p3, p4}, LX/146;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 2

    .prologue
    .line 178065
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, LX/146;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 178066
    invoke-direct {p0, p1, p2}, LX/146;->b(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 178067
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/146;->b(Ljava/lang/Object;Z)LX/146;

    move-result-object v0

    return-object v0
.end method

.method public final values()LX/0Py;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Py",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 178068
    iget-object v0, p0, LX/146;->d:LX/0Px;

    return-object v0
.end method

.method public final bridge synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 178069
    invoke-virtual {p0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 178070
    new-instance v0, LX/4yh;

    invoke-direct {v0, p0}, LX/4yh;-><init>(LX/146;)V

    return-object v0
.end method
