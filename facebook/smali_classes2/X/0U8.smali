.class public final LX/0U8;
.super Ljava/util/AbstractSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractSet",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field public final b:LX/0QC;

.field public final c:[Ljava/lang/Object;

.field public final d:LX/0Sq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Sq",
            "<TT;>;"
        }
    .end annotation
.end field

.field public e:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mItems"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64189
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0U8;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0QC;LX/0Sq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QC;",
            "LX/0Sq",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 64190
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 64191
    iput-object p1, p0, LX/0U8;->b:LX/0QC;

    .line 64192
    iput-object p2, p0, LX/0U8;->d:LX/0Sq;

    .line 64193
    invoke-interface {p2}, LX/0Sq;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/0U8;->c:[Ljava/lang/Object;

    .line 64194
    const/4 v0, 0x0

    iput v0, p0, LX/0U8;->e:I

    .line 64195
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 64196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 64197
    invoke-virtual {p0}, LX/0U8;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 64198
    if-nez p1, :cond_1

    if-nez v1, :cond_0

    .line 64199
    :goto_0
    const/4 v0, 0x1

    .line 64200
    :goto_1
    return v0

    .line 64201
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 64202
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 64203
    new-instance v0, LX/0UF;

    invoke-direct {v0, p0}, LX/0UF;-><init>(LX/0U8;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64205
    iget-object v0, p0, LX/0U8;->c:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method
