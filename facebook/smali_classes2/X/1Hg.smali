.class public LX/1Hg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Hh;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/1Hi;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final c:I

.field public final d:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:LX/1GQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 227352
    const-class v0, LX/1Hg;

    sput-object v0, LX/1Hg;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(ILX/1Gd;Ljava/lang/String;LX/1GQ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1Gd",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/lang/String;",
            "LX/1GQ;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 227345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227346
    iput p1, p0, LX/1Hg;->c:I

    .line 227347
    iput-object p4, p0, LX/1Hg;->f:LX/1GQ;

    .line 227348
    iput-object p2, p0, LX/1Hg;->d:LX/1Gd;

    .line 227349
    iput-object p3, p0, LX/1Hg;->e:Ljava/lang/String;

    .line 227350
    new-instance v0, LX/1Hi;

    invoke-direct {v0, v1, v1}, LX/1Hi;-><init>(Ljava/io/File;LX/1Hh;)V

    iput-object v0, p0, LX/1Hg;->a:LX/1Hi;

    .line 227351
    return-void
.end method

.method public static a(LX/1Hg;Ljava/io/File;)V
    .locals 5
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 227339
    :try_start_0
    invoke-static {p1}, LX/04M;->a(Ljava/io/File;)V
    :try_end_0
    .catch LX/0Fu; {:try_start_0 .. :try_end_0} :catch_0

    .line 227340
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 227341
    return-void

    .line 227342
    :catch_0
    move-exception v0

    .line 227343
    iget-object v1, p0, LX/1Hg;->f:LX/1GQ;

    sget-object v2, LX/43W;->WRITE_CREATE_DIR:LX/43W;

    sget-object v3, LX/1Hg;->b:Ljava/lang/Class;

    const-string v4, "createRootDirectoryIfNecessary"

    invoke-interface {v1, v2, v3, v4, v0}, LX/1GQ;->a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 227344
    throw v0
.end method

.method private declared-synchronized f()LX/1Hh;
    .locals 4
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 227327
    monitor-enter p0

    .line 227328
    :try_start_0
    iget-object v0, p0, LX/1Hg;->a:LX/1Hi;

    .line 227329
    iget-object v1, v0, LX/1Hi;->a:LX/1Hh;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1Hi;->b:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v0, v0, LX/1Hi;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 227330
    if-eqz v0, :cond_2

    .line 227331
    iget-object v0, p0, LX/1Hg;->a:LX/1Hi;

    iget-object v0, v0, LX/1Hi;->a:LX/1Hh;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Hg;->a:LX/1Hi;

    iget-object v0, v0, LX/1Hi;->b:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 227332
    iget-object v0, p0, LX/1Hg;->a:LX/1Hi;

    iget-object v0, v0, LX/1Hi;->b:Ljava/io/File;

    invoke-static {v0}, LX/2W9;->b(Ljava/io/File;)Z

    .line 227333
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, LX/1Hg;->d:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v2, p0, LX/1Hg;->e:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 227334
    invoke-static {p0, v1}, LX/1Hg;->a(LX/1Hg;Ljava/io/File;)V

    .line 227335
    new-instance v0, LX/1Hv;

    iget v2, p0, LX/1Hg;->c:I

    iget-object v3, p0, LX/1Hg;->f:LX/1GQ;

    invoke-direct {v0, v1, v2, v3}, LX/1Hv;-><init>(Ljava/io/File;ILX/1GQ;)V

    .line 227336
    new-instance v2, LX/1Hi;

    invoke-direct {v2, v1, v0}, LX/1Hi;-><init>(Ljava/io/File;LX/1Hh;)V

    iput-object v2, p0, LX/1Hg;->a:LX/1Hi;

    .line 227337
    :cond_2
    iget-object v0, p0, LX/1Hg;->a:LX/1Hi;

    iget-object v0, v0, LX/1Hi;->a:LX/1Hh;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Hh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 227338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/35g;)J
    .locals 2

    .prologue
    .line 227326
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1Hh;->a(LX/35g;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/32a;
    .locals 1

    .prologue
    .line 227325
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1Hh;->a(Ljava/lang/String;Ljava/lang/Object;)LX/32a;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 227323
    :try_start_0
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0}, LX/1Hh;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 227324
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)LX/1gI;
    .locals 1

    .prologue
    .line 227322
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1Hh;->b(Ljava/lang/String;Ljava/lang/Object;)LX/1gI;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227311
    :try_start_0
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0}, LX/1Hh;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 227312
    :goto_0
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 227318
    :try_start_0
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0}, LX/1Hh;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227319
    :goto_0
    return-void

    .line 227320
    :catch_0
    move-exception v0

    .line 227321
    sget-object v1, LX/1Hg;->b:Ljava/lang/Class;

    const-string v2, "purgeUnexpectedResources"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 227317
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1Hh;->c(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 227315
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0}, LX/1Hh;->d()V

    .line 227316
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 227314
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1Hh;->d(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/cache/disk/DiskStorage$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227313
    invoke-direct {p0}, LX/1Hg;->f()LX/1Hh;

    move-result-object v0

    invoke-interface {v0}, LX/1Hh;->e()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
