.class public LX/0Wt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:LX/0Tn;

.field public static b:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static c:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static d:Ljava/lang/String;

.field private static volatile e:LX/0Wt;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76793
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "analytics_counters/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0Wt;->a:LX/0Tn;

    .line 76794
    const-string v0, "data"

    sput-object v0, LX/0Wt;->b:Ljava/lang/String;

    .line 76795
    const-string v0, "last_update_time"

    sput-object v0, LX/0Wt;->c:Ljava/lang/String;

    .line 76796
    const-string v0, "/"

    sput-object v0, LX/0Wt;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76780
    return-void
.end method

.method public static a(LX/0QB;)LX/0Wt;
    .locals 3

    .prologue
    .line 76781
    sget-object v0, LX/0Wt;->e:LX/0Wt;

    if-nez v0, :cond_1

    .line 76782
    const-class v1, LX/0Wt;

    monitor-enter v1

    .line 76783
    :try_start_0
    sget-object v0, LX/0Wt;->e:LX/0Wt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76784
    if-eqz v2, :cond_0

    .line 76785
    :try_start_1
    new-instance v0, LX/0Wt;

    invoke-direct {v0}, LX/0Wt;-><init>()V

    .line 76786
    move-object v0, v0

    .line 76787
    sput-object v0, LX/0Wt;->e:LX/0Wt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76788
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76789
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76790
    :cond_1
    sget-object v0, LX/0Wt;->e:LX/0Wt;

    return-object v0

    .line 76791
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76792
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Tn;)[Ljava/lang/String;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 76776
    sget-object v0, LX/0Wt;->a:LX/0Tn;

    invoke-virtual {p0, v0}, LX/0To;->a(LX/0To;)Z

    move-result v0

    const-string v1, "Invalid counters prefkey"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 76777
    sget-object v0, LX/0Wt;->a:LX/0Tn;

    invoke-virtual {p0, v0}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 76778
    sget-object v1, LX/0Wt;->d:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)LX/0Tn;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 76770
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Should specify counters name"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 76771
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot handle null process name"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 76772
    sget-object v0, LX/0Wt;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/0Wt;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    move-object v0, v0

    .line 76773
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/0Wt;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0

    .line 76774
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 76775
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
