.class public LX/1LQ;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1DD;
.implements LX/0fm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0ft;",
        ">",
        "LX/0hD;",
        "LX/1DD;",
        "LX/0fm;"
    }
.end annotation


# instance fields
.field public a:LX/1PF;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0kb;

.field public c:LX/0ft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233742
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233743
    iput-object p1, p0, LX/1LQ;->b:LX/0kb;

    .line 233744
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 233736
    iget-object v0, p0, LX/1LQ;->a:LX/1PF;

    if-nez v0, :cond_0

    .line 233737
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 233738
    new-instance p1, LX/1PE;

    invoke-direct {p1, p0, v0}, LX/1PE;-><init>(LX/1LQ;Landroid/content/Context;)V

    move-object v0, p1

    .line 233739
    iput-object v0, p0, LX/1LQ;->a:LX/1PF;

    .line 233740
    :cond_0
    iget-object v0, p0, LX/1LQ;->a:LX/1PF;

    invoke-virtual {v0}, LX/14l;->a()V

    .line 233741
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 233734
    const/4 v0, 0x0

    iput-object v0, p0, LX/1LQ;->a:LX/1PF;

    .line 233735
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 233732
    iget-object v0, p0, LX/1LQ;->a:LX/1PF;

    invoke-virtual {v0}, LX/14l;->b()V

    .line 233733
    return-void
.end method
