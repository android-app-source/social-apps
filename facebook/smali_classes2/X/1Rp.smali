.class public LX/1Rp;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0fn;
.implements LX/1Rq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/0fn;",
        "LX/1Rq;"
    }
.end annotation


# instance fields
.field private final a:LX/1KB;

.field public final b:LX/1R2;

.field public final c:LX/1R9;

.field private final d:Z

.field private final e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

.field private final f:LX/1Rt;

.field private final g:LX/03V;

.field private h:Z


# direct methods
.method public constructor <init>(LX/1R2;LX/1R9;ZLX/03V;LX/1KB;)V
    .locals 3
    .param p1    # LX/1R2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1R9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246988
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 246989
    new-instance v0, LX/1Rt;

    invoke-direct {v0}, LX/1Rt;-><init>()V

    iput-object v0, p0, LX/1Rp;->f:LX/1Rt;

    .line 246990
    iput-object p4, p0, LX/1Rp;->g:LX/03V;

    .line 246991
    iput-boolean p3, p0, LX/1Rp;->d:Z

    .line 246992
    iput-object p1, p0, LX/1Rp;->b:LX/1R2;

    .line 246993
    iput-object p2, p0, LX/1Rp;->c:LX/1R9;

    .line 246994
    iput-object p5, p0, LX/1Rp;->a:LX/1KB;

    .line 246995
    new-instance v0, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-direct {v0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;-><init>()V

    iput-object v0, p0, LX/1Rp;->e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    .line 246996
    iget-object v0, p0, LX/1Rp;->e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 246997
    iget-object v0, p0, LX/1Rp;->f:LX/1Rt;

    invoke-super {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 246998
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    instance-of v0, v0, LX/1Ru;

    if-eqz v0, :cond_1

    .line 246999
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    check-cast v0, LX/1Ru;

    new-instance v1, LX/1UB;

    invoke-direct {v1, p0}, LX/1UB;-><init>(LX/1Rp;)V

    .line 247000
    invoke-static {v0}, LX/1Ru;->i(LX/1Ru;)V

    .line 247001
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/1Ru;->f(LX/1Ru;I)V

    .line 247002
    if-nez v1, :cond_0

    sget-object v1, LX/1Ru;->c:LX/1OD;

    :cond_0
    iput-object v1, v0, LX/1Ru;->n:LX/1OD;

    .line 247003
    invoke-virtual {v0}, LX/1Ru;->e()V

    .line 247004
    new-instance v0, LX/1UC;

    invoke-direct {v0, p0}, LX/1UC;-><init>(LX/1Rp;)V

    invoke-virtual {p0, v0}, LX/1Rp;->a(LX/1KR;)V

    .line 247005
    :goto_0
    return-void

    .line 247006
    :cond_1
    new-instance v0, LX/1Rv;

    invoke-direct {v0, p0}, LX/1Rv;-><init>(LX/1Rp;)V

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    goto :goto_0
.end method

.method public static f(LX/1Rp;)V
    .locals 3

    .prologue
    .line 246985
    iget-boolean v0, p0, LX/1Rp;->h:Z

    if-eqz v0, :cond_0

    .line 246986
    iget-object v0, p0, LX/1Rp;->g:LX/03V;

    const-string v1, "BasicMultiRowAdapter"

    const-string v2, "Adapter is disposed. T11310846"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 246987
    :cond_0
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 246983
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246984
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 246976
    iget-object v0, p0, LX/1Rp;->a:LX/1KB;

    invoke-virtual {v0, p2}, LX/1KB;->a(I)LX/1Cz;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 246977
    iget-boolean v0, p0, LX/1Rp;->d:Z

    if-nez v0, :cond_0

    .line 246978
    new-instance v0, LX/1a4;

    invoke-direct {v0, v1}, LX/1a4;-><init>(Landroid/view/View;)V

    .line 246979
    :goto_0
    return-object v0

    .line 246980
    :cond_0
    new-instance v2, LX/1a5;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/1a5;-><init>(Landroid/content/Context;)V

    .line 246981
    invoke-virtual {v2, v1}, LX/1a5;->addView(Landroid/view/View;)V

    .line 246982
    new-instance v0, LX/1a4;

    invoke-direct {v0, v2}, LX/1a4;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1KR;)V
    .locals 1

    .prologue
    .line 246974
    iget-object v0, p0, LX/1Rp;->e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->a(LX/1KR;)V

    .line 246975
    return-void
.end method

.method public final a(LX/1OD;)V
    .locals 1

    .prologue
    .line 246902
    iget-object v0, p0, LX/1Rp;->f:LX/1Rt;

    invoke-virtual {v0, p1}, LX/1Rt;->a(LX/1OD;)V

    .line 246903
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 246953
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    .line 246954
    instance-of v1, v0, LX/1a5;

    if-eqz v1, :cond_2

    check-cast v0, LX/1a5;

    invoke-virtual {v0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 246955
    :goto_0
    invoke-virtual {p0, p2}, LX/1Rp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 246956
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    iget v3, v0, LX/1Rk;->b:I

    .line 246957
    invoke-virtual {v1, v3}, LX/1RA;->e(I)Z

    .line 246958
    iget-object v4, v1, LX/1RA;->k:LX/0Px;

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ra;

    .line 246959
    iget-object v5, v4, LX/1Ra;->a:Landroid/view/View;

    move-object v5, v5

    .line 246960
    if-eqz v5, :cond_0

    .line 246961
    iget-object p0, v1, LX/1RA;->f:LX/03V;

    sget-object p1, LX/1RA;->a:Ljava/lang/String;

    const-string p2, "bindNow called on an already bound Binder"

    invoke-virtual {p0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 246962
    instance-of p0, v5, LX/1a7;

    if-eqz p0, :cond_0

    .line 246963
    invoke-static {v5}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 246964
    :cond_0
    iget-object v5, v1, LX/1RA;->f:LX/03V;

    invoke-static {v2, v4, v5}, LX/1aL;->a(Landroid/view/View;LX/1Ra;LX/03V;)V

    .line 246965
    invoke-virtual {v4, v2}, LX/1Ra;->a(Landroid/view/View;)V

    .line 246966
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    .line 246967
    iget-object v3, v1, LX/1RA;->b:Ljava/lang/Object;

    move-object v1, v3

    .line 246968
    instance-of v3, v1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v3, :cond_1

    .line 246969
    const v3, 0x7f0d0074

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 246970
    iget v0, v0, LX/1Rk;->b:I

    if-nez v0, :cond_1

    .line 246971
    const v0, 0x7f0d008c

    const-string v1, "TOP"

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 246972
    :cond_1
    return-void

    :cond_2
    move-object v2, v0

    .line 246973
    goto :goto_0
.end method

.method public final a(LX/5Mj;)V
    .locals 2

    .prologue
    .line 246949
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246950
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v0, v0

    .line 246951
    iget-object v1, p0, LX/1Rp;->b:LX/1R2;

    invoke-virtual {v0, v1, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 246952
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 246946
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->c()V

    .line 246947
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 246948
    return-void
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 246944
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 246945
    return-void
.end method

.method public final b(LX/1KR;)V
    .locals 1

    .prologue
    .line 246942
    iget-object v0, p0, LX/1Rp;->e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->b(LX/1KR;)V

    .line 246943
    return-void
.end method

.method public final b(LX/1OD;)V
    .locals 1

    .prologue
    .line 246940
    iget-object v0, p0, LX/1Rp;->f:LX/1Rt;

    invoke-virtual {v0, p1}, LX/1Rt;->b(LX/1OD;)V

    .line 246941
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 246938
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->g()V

    .line 246939
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 246937
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->e(I)Z

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 246934
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246935
    invoke-virtual {p0, p1}, LX/1Rp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 246936
    iget-object v0, v0, LX/1Rk;->a:LX/1RA;

    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 246932
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246933
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->f()I

    move-result v0

    return v0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 246927
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->a()V

    .line 246928
    iget-object v0, p0, LX/1Rp;->c:LX/1R9;

    invoke-interface {v0}, LX/1R9;->b()V

    .line 246929
    iget-object v0, p0, LX/1Rp;->e:Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BaseHasNotifyOnceAdapterObservers;->dispose()V

    .line 246930
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Rp;->h:Z

    .line 246931
    return-void
.end method

.method public final e()LX/1R4;
    .locals 1

    .prologue
    .line 246926
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->d()LX/1R4;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)I
    .locals 1

    .prologue
    .line 246924
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246925
    invoke-virtual {p0, p1}, LX/1Rp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    iget v0, v0, LX/1Rk;->b:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246922
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246923
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->a(I)LX/1Rk;

    move-result-object v0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    .line 246906
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246907
    iget-object v0, p0, LX/1Rp;->a:LX/1KB;

    .line 246908
    invoke-virtual {p0, p1}, LX/1Rp;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Rk;

    .line 246909
    iget-object v2, v1, LX/1Rk;->a:LX/1RA;

    iget v1, v1, LX/1Rk;->b:I

    invoke-virtual {v2, v1}, LX/1RA;->b(I)LX/1Cz;

    move-result-object v1

    move-object v1, v1

    .line 246910
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246911
    iget-object v2, v0, LX/1KB;->b:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 246912
    if-nez v2, :cond_2

    .line 246913
    iget-object v2, v0, LX/1KB;->f:LX/0Ot;

    if-nez v2, :cond_0

    .line 246914
    instance-of v2, v1, LX/1Un;

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, LX/1Un;

    iget-object v3, v0, LX/1KB;->c:Landroid/content/Context;

    .line 246915
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "LayoutBasedViewType of "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    iget p1, v2, LX/1Un;->a:I

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 246916
    :goto_0
    iget-object v3, v0, LX/1KB;->e:LX/03V;

    const-string v4, "ROWTYPE_REGISTRATION"

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p0, " is not registered"

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246917
    :cond_0
    invoke-static {v0, v1}, LX/1KB;->c(LX/1KB;LX/1Cz;)I

    move-result v2

    .line 246918
    :goto_1
    move v0, v2

    .line 246919
    return v0

    .line 246920
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 246921
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method

.method public final h_(I)I
    .locals 1

    .prologue
    .line 246904
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246905
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->b(I)I

    move-result v0

    return v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 246897
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246898
    iget-object v0, p0, LX/1Rp;->a:LX/1KB;

    .line 246899
    invoke-static {v0}, LX/1KB;->b(LX/1KB;)V

    .line 246900
    iget-object p0, v0, LX/1KB;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move v0, p0

    .line 246901
    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 246895
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246896
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0}, LX/1R2;->b()I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 246894
    iget-boolean v0, p0, LX/1Rp;->h:Z

    return v0
.end method

.method public final m_(I)I
    .locals 1

    .prologue
    .line 246892
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246893
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->c(I)I

    move-result v0

    return v0
.end method

.method public final n_(I)I
    .locals 1

    .prologue
    .line 246890
    invoke-static {p0}, LX/1Rp;->f(LX/1Rp;)V

    .line 246891
    iget-object v0, p0, LX/1Rp;->b:LX/1R2;

    invoke-interface {v0, p1}, LX/1R2;->d(I)I

    move-result v0

    return v0
.end method
