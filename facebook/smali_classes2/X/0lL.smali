.class public LX/0lL;
.super LX/0lM;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/0lR;

.field public static final b:LX/0lR;

.field public static final c:LX/0lR;

.field public static final d:LX/0lR;

.field public static final e:LX/0lL;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128932
    const-class v0, Ljava/lang/String;

    invoke-static {v0, v2, v2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128933
    const-class v1, Ljava/lang/String;

    invoke-static {v1}, LX/0lH;->h(Ljava/lang/Class;)LX/0lH;

    move-result-object v1

    invoke-static {v2, v1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    sput-object v0, LX/0lL;->a:LX/0lR;

    .line 128934
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v2, v2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128935
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, LX/0lH;->h(Ljava/lang/Class;)LX/0lH;

    move-result-object v1

    invoke-static {v2, v1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    sput-object v0, LX/0lL;->b:LX/0lR;

    .line 128936
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v2, v2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128937
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, LX/0lH;->h(Ljava/lang/Class;)LX/0lH;

    move-result-object v1

    invoke-static {v2, v1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    sput-object v0, LX/0lL;->c:LX/0lR;

    .line 128938
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v0, v2, v2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128939
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, LX/0lH;->h(Ljava/lang/Class;)LX/0lH;

    move-result-object v1

    invoke-static {v2, v1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    sput-object v0, LX/0lL;->d:LX/0lR;

    .line 128940
    new-instance v0, LX/0lL;

    invoke-direct {v0}, LX/0lL;-><init>()V

    sput-object v0, LX/0lL;->e:LX/0lL;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128958
    invoke-direct {p0}, LX/0lM;-><init>()V

    return-void
.end method

.method public static a(LX/0lJ;)LX/0lR;
    .locals 2

    .prologue
    .line 128947
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 128948
    const-class v1, Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 128949
    sget-object v0, LX/0lL;->a:LX/0lR;

    .line 128950
    :goto_0
    return-object v0

    .line 128951
    :cond_0
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v0, v1, :cond_1

    .line 128952
    sget-object v0, LX/0lL;->b:LX/0lR;

    goto :goto_0

    .line 128953
    :cond_1
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v1, :cond_2

    .line 128954
    sget-object v0, LX/0lL;->c:LX/0lR;

    goto :goto_0

    .line 128955
    :cond_2
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, v1, :cond_3

    .line 128956
    sget-object v0, LX/0lL;->d:LX/0lR;

    goto :goto_0

    .line 128957
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0m4;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0m5;",
            ")",
            "LX/0lR;"
        }
    .end annotation

    .prologue
    .line 128941
    invoke-virtual {p0}, LX/0m4;->g()Z

    move-result v1

    .line 128942
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    .line 128943
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 128944
    if-eqz v1, :cond_0

    :goto_0
    invoke-static {v2, v0, p2}, LX/0lN;->b(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128945
    invoke-static {p0, p1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    return-object v0

    .line 128946
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0m4;LX/0lJ;LX/0m5;Z)LX/2Al;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0m5;",
            "Z)",
            "LX/2Al;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128911
    invoke-virtual {p0}, LX/0m4;->g()Z

    move-result v0

    .line 128912
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    .line 128913
    :goto_0
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 128914
    invoke-static {v2, v0, p2}, LX/0lN;->a(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v2

    .line 128915
    if-nez v0, :cond_1

    .line 128916
    :goto_1
    if-nez v1, :cond_2

    const-string v0, "with"

    .line 128917
    :goto_2
    invoke-static {p0, v2, p1, p3, v0}, LX/0lL;->a(LX/0m4;LX/0lN;LX/0lJ;ZLjava/lang/String;)LX/2Al;

    move-result-object v0

    invoke-virtual {v0}, LX/2Al;->k()LX/2Al;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    .line 128918
    goto :goto_0

    .line 128919
    :cond_1
    invoke-virtual {v0, v2}, LX/0lU;->k(LX/0lN;)LX/2zN;

    move-result-object v1

    goto :goto_1

    .line 128920
    :cond_2
    iget-object v0, v1, LX/2zN;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method private static a(LX/0m4;LX/0lJ;LX/0m5;ZLjava/lang/String;)LX/2Al;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0m5;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "LX/2Al;"
        }
    .end annotation

    .prologue
    .line 128927
    invoke-virtual {p0}, LX/0m4;->g()Z

    move-result v0

    .line 128928
    iget-object v1, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 128929
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0, p2}, LX/0lN;->a(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128930
    invoke-static {p0, v0, p1, p3, p4}, LX/0lL;->a(LX/0m4;LX/0lN;LX/0lJ;ZLjava/lang/String;)LX/2Al;

    move-result-object v0

    invoke-virtual {v0}, LX/2Al;->k()LX/2Al;

    move-result-object v0

    return-object v0

    .line 128931
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0m4;LX/0lN;LX/0lJ;ZLjava/lang/String;)LX/2Al;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lN;",
            "LX/0lJ;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "LX/2Al;"
        }
    .end annotation

    .prologue
    .line 128926
    new-instance v0, LX/2Al;

    move-object v1, p0

    move v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Al;-><init>(LX/0m4;ZLX/0lJ;LX/0lN;Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(LX/0m4;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "LX/0m5;",
            ")",
            "LX/0lR;"
        }
    .end annotation

    .prologue
    .line 128921
    invoke-virtual {p0}, LX/0m4;->g()Z

    move-result v0

    .line 128922
    iget-object v1, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 128923
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0, p2}, LX/0lN;->a(Ljava/lang/Class;LX/0lU;LX/0m5;)LX/0lN;

    move-result-object v0

    .line 128924
    invoke-static {p0, p1, v0}, LX/0lR;->a(LX/0m4;LX/0lJ;LX/0lN;)LX/0lR;

    move-result-object v0

    return-object v0

    .line 128925
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 1

    .prologue
    .line 128959
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;Z)LX/2Al;

    move-result-object v0

    invoke-static {v0}, LX/0lR;->a(LX/2Al;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method private f(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 6

    .prologue
    .line 128903
    invoke-static {p2}, LX/0lL;->a(LX/0lJ;)LX/0lR;

    move-result-object v0

    .line 128904
    if-nez v0, :cond_0

    .line 128905
    const/4 v4, 0x0

    const-string v5, "set"

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v1 .. v5}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;ZLjava/lang/String;)LX/2Al;

    move-result-object v0

    invoke-static {v0}, LX/0lR;->a(LX/2Al;)LX/0lR;

    move-result-object v0

    .line 128906
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(LX/0m2;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 6

    .prologue
    .line 128899
    invoke-static {p2}, LX/0lL;->a(LX/0lJ;)LX/0lR;

    move-result-object v0

    .line 128900
    if-nez v0, :cond_0

    .line 128901
    const/4 v4, 0x1

    const-string v5, "set"

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v1 .. v5}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;ZLjava/lang/String;)LX/2Al;

    move-result-object v0

    invoke-static {v0}, LX/0lR;->b(LX/2Al;)LX/0lR;

    move-result-object v0

    .line 128902
    :cond_0
    return-object v0
.end method

.method public a(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;
    .locals 6

    .prologue
    .line 128895
    invoke-static {p2}, LX/0lL;->a(LX/0lJ;)LX/0lR;

    move-result-object v0

    .line 128896
    if-nez v0, :cond_0

    .line 128897
    const/4 v4, 0x0

    const-string v5, "set"

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v1 .. v5}, LX/0lL;->a(LX/0m4;LX/0lJ;LX/0m5;ZLjava/lang/String;)LX/2Al;

    move-result-object v0

    invoke-static {v0}, LX/0lR;->a(LX/2Al;)LX/0lR;

    move-result-object v0

    .line 128898
    :cond_0
    return-object v0
.end method

.method public synthetic b(LX/0m2;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 128894
    invoke-virtual {p0, p1, p2, p3}, LX/0lL;->a(LX/0m2;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/0m4;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 128907
    invoke-static {p1, p2, p3}, LX/0lL;->c(LX/0m4;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 128908
    invoke-direct {p0, p1, p2, p3}, LX/0lL;->f(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 128909
    invoke-direct {p0, p1, p2, p3}, LX/0lL;->e(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(LX/0mu;LX/0lJ;LX/0m5;)LX/0lS;
    .locals 1

    .prologue
    .line 128910
    invoke-virtual {p0, p1, p2, p3}, LX/0lL;->a(LX/0mu;LX/0lJ;LX/0m5;)LX/0lR;

    move-result-object v0

    return-object v0
.end method
