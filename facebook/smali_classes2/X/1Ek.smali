.class public final LX/1Ek;
.super LX/0P4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P4",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final keyHash:I

.field public nextInKToVBucket:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nextInKeyInsertionOrder:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nextInVToKBucket:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public prevInKeyInsertionOrder:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final valueHash:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILjava/lang/Object;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;ITV;I)V"
        }
    .end annotation

    .prologue
    .line 220946
    invoke-direct {p0, p1, p3}, LX/0P4;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220947
    iput p2, p0, LX/1Ek;->keyHash:I

    .line 220948
    iput p4, p0, LX/1Ek;->valueHash:I

    .line 220949
    return-void
.end method
