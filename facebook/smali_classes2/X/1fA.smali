.class public LX/1fA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1fA;


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1JZ;


# direct methods
.method public constructor <init>(LX/1JZ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291034
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1fA;->a:Ljava/util/List;

    .line 291035
    iput-object p1, p0, LX/1fA;->b:LX/1JZ;

    .line 291036
    return-void
.end method

.method public static a(LX/0QB;)LX/1fA;
    .locals 4

    .prologue
    .line 291037
    sget-object v0, LX/1fA;->c:LX/1fA;

    if-nez v0, :cond_1

    .line 291038
    const-class v1, LX/1fA;

    monitor-enter v1

    .line 291039
    :try_start_0
    sget-object v0, LX/1fA;->c:LX/1fA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 291040
    if-eqz v2, :cond_0

    .line 291041
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 291042
    new-instance p0, LX/1fA;

    invoke-static {v0}, LX/1JZ;->b(LX/0QB;)LX/1JZ;

    move-result-object v3

    check-cast v3, LX/1JZ;

    invoke-direct {p0, v3}, LX/1fA;-><init>(LX/1JZ;)V

    .line 291043
    move-object v0, p0

    .line 291044
    sput-object v0, LX/1fA;->c:LX/1fA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291045
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 291046
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291047
    :cond_1
    sget-object v0, LX/1fA;->c:LX/1fA;

    return-object v0

    .line 291048
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 291049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 291050
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fA;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 291051
    iget-object v0, p0, LX/1fA;->b:LX/1JZ;

    if-eqz v0, :cond_0

    .line 291052
    iget-object v0, p0, LX/1fA;->b:LX/1JZ;

    .line 291053
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 291054
    iget-object v2, p0, LX/1fA;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/1JZ;->b(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291055
    :cond_0
    monitor-exit p0

    return-void

    .line 291056
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 3

    .prologue
    .line 291057
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fA;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291058
    iget-object v0, p0, LX/1fA;->b:LX/1JZ;

    .line 291059
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 291060
    iget-object v2, p0, LX/1fA;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/1JZ;->b(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291061
    monitor-exit p0

    return-void

    .line 291062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/photos/prefetch/PrefetchParams;Z)V
    .locals 1

    .prologue
    .line 291063
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 291064
    :try_start_0
    iget-object v0, p0, LX/1fA;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291065
    :cond_0
    monitor-exit p0

    return-void

    .line 291066
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
