.class public LX/0TO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:I

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 62962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62963
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0TO;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 62964
    iput-object p1, p0, LX/0TO;->a:Ljava/lang/String;

    .line 62965
    iput p2, p0, LX/0TO;->b:I

    .line 62966
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0TP;)V
    .locals 1

    .prologue
    .line 62967
    invoke-virtual {p2}, LX/0TP;->getAndroidThreadPriority()I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/0TO;-><init>(Ljava/lang/String;I)V

    .line 62968
    return-void
.end method


# virtual methods
.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 3

    .prologue
    .line 62969
    new-instance v0, Lcom/facebook/common/executors/NamedThreadFactory$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/executors/NamedThreadFactory$1;-><init>(LX/0TO;Ljava/lang/Runnable;)V

    .line 62970
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0TO;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0TO;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, -0x42c1e8db

    invoke-static {v0, v1, v2}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method
