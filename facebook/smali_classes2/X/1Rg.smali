.class public LX/1Rg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1Rg;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246705
    return-void
.end method

.method public static a(LX/0QB;)LX/1Rg;
    .locals 3

    .prologue
    .line 246692
    sget-object v0, LX/1Rg;->a:LX/1Rg;

    if-nez v0, :cond_1

    .line 246693
    const-class v1, LX/1Rg;

    monitor-enter v1

    .line 246694
    :try_start_0
    sget-object v0, LX/1Rg;->a:LX/1Rg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 246695
    if-eqz v2, :cond_0

    .line 246696
    :try_start_1
    new-instance v0, LX/1Rg;

    invoke-direct {v0}, LX/1Rg;-><init>()V

    .line 246697
    move-object v0, v0

    .line 246698
    sput-object v0, LX/1Rg;->a:LX/1Rg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246699
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 246700
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 246701
    :cond_1
    sget-object v0, LX/1Rg;->a:LX/1Rg;

    return-object v0

    .line 246702
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 246703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;
    .locals 1

    .prologue
    .line 246706
    new-instance v0, LX/8sn;

    invoke-direct {v0, p1, p2, p3}, LX/8sn;-><init>(JLandroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method public final a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/8sj;",
            ">;J",
            "Landroid/animation/Animator$AnimatorListener;",
            ")",
            "LX/8sj;"
        }
    .end annotation

    .prologue
    .line 246691
    new-instance v0, LX/8sr;

    invoke-direct {v0, p1, p2, p3, p4}, LX/8sr;-><init>(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;
    .locals 8

    .prologue
    .line 246690
    new-instance v0, LX/8sq;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/8sq;-><init>(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;
    .locals 8

    .prologue
    .line 246684
    instance-of v0, p1, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 246685
    new-instance v0, LX/8st;

    move-object v1, p1

    check-cast v1, Landroid/view/View;

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/8st;-><init>(Landroid/view/View;JFFLandroid/animation/Animator$AnimatorListener;)V

    .line 246686
    :goto_0
    return-object v0

    .line 246687
    :cond_0
    instance-of v0, p1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    if-eqz v0, :cond_1

    .line 246688
    new-instance v0, LX/8ss;

    move-object v1, p1

    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/8ss;-><init>(Landroid/animation/ValueAnimator$AnimatorUpdateListener;JFFLandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 246689
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target object not of suitable type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
