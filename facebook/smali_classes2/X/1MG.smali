.class public LX/1MG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MF;


# static fields
.field public static final sBindingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sIsInOurProcessMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field


# instance fields
.field public mBlueService:LX/1mL;

.field public final mBlueServiceLogicLazy:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceLogic;",
            ">;"
        }
    .end annotation
.end field

.field private mCalledBind:Z

.field private final mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

.field private mCanRunBeforeAppInit:Z

.field public final mContext:Landroid/content/Context;

.field private final mContextServiceBinder:LX/1MH;

.field public final mCriticalServiceExceptionChecker:LX/0l4;

.field public final mCrossFbProcessBroadcast:LX/0Xl;

.field private mDisposableDelegate:LX/0je;

.field public final mErrorPropagation:LX/1ME;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mFbErrorReporter:LX/03V;

.field private mFireAndForget:Z

.field public mHandler:Landroid/os/Handler;

.field public mOnProgressListener:LX/4An;

.field public mOperationId:Ljava/lang/String;

.field private mOperationProgressIndicator:LX/4At;

.field public mOperationState:LX/1MI;

.field private final mOperationType:Ljava/lang/String;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mParam:Landroid/os/Bundle;

.field private final mProcessUtil:LX/0VT;

.field public final mResult:LX/1MK;

.field private final mServiceConnection:LX/1MJ;

.field private mTriedBindingLocally:LX/03R;

.field private final mViewerContextManager:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234995
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LX/1MG;->sIsInOurProcessMap:Ljava/util/Map;

    .line 234996
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LX/1MG;->sBindingMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0VT;LX/1MH;LX/03V;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;LX/0SI;LX/0l4;LX/0Xl;)V
    .locals 4
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1ME;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/0SI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/pm/PackageManager;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceLogic;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "Lcom/facebook/base/service/ContextServiceBinder;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "LX/1ME;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0SI;",
            "LX/0l4;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 234997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234998
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1MG;->mCanRunBeforeAppInit:Z

    .line 234999
    sget-object v1, LX/1MI;->INIT:LX/1MI;

    iput-object v1, p0, LX/1MG;->mOperationState:LX/1MI;

    .line 235000
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, p0, LX/1MG;->mTriedBindingLocally:LX/03R;

    .line 235001
    iput-object p1, p0, LX/1MG;->mContext:Landroid/content/Context;

    .line 235002
    iput-object p2, p0, LX/1MG;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 235003
    new-instance v1, LX/1MJ;

    invoke-direct {v1, p0}, LX/1MJ;-><init>(LX/1MG;)V

    iput-object v1, p0, LX/1MG;->mServiceConnection:LX/1MJ;

    .line 235004
    iput-object p3, p0, LX/1MG;->mBlueServiceLogicLazy:LX/0Ot;

    .line 235005
    iput-object p4, p0, LX/1MG;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 235006
    new-instance v1, LX/1MK;

    invoke-direct {v1, p0}, LX/1MK;-><init>(LX/1MG;)V

    iput-object v1, p0, LX/1MG;->mResult:LX/1MK;

    .line 235007
    iput-object p5, p0, LX/1MG;->mProcessUtil:LX/0VT;

    .line 235008
    iput-object p6, p0, LX/1MG;->mContextServiceBinder:LX/1MH;

    .line 235009
    iput-object p7, p0, LX/1MG;->mFbErrorReporter:LX/03V;

    .line 235010
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    .line 235011
    new-instance v2, Landroid/os/Bundle;

    invoke-static {p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    invoke-direct {v2, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v2, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    .line 235012
    iput-object p10, p0, LX/1MG;->mErrorPropagation:LX/1ME;

    .line 235013
    iput-object p11, p0, LX/1MG;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    .line 235014
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1MG;->mViewerContextManager:LX/0SI;

    .line 235015
    new-instance v1, LX/1MN;

    invoke-direct {v1, p0}, LX/1MN;-><init>(LX/1MG;)V

    iput-object v1, p0, LX/1MG;->mDisposableDelegate:LX/0je;

    .line 235016
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1MG;->mCriticalServiceExceptionChecker:LX/0l4;

    .line 235017
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1MG;->mCrossFbProcessBroadcast:LX/0Xl;

    .line 235018
    iget-object v1, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    const-string v2, "overridden_viewer_context"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 235019
    iget-object v1, p0, LX/1MG;->mViewerContextManager:LX/0SI;

    invoke-interface {v1}, LX/0SI;->e()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 235020
    if-eqz v1, :cond_0

    .line 235021
    iget-object v2, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    const-string v3, "overridden_viewer_context"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 235022
    :cond_0
    iget-object v1, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    const-string v2, "calling_process_name"

    invoke-virtual {p5}, LX/0VT;->a()LX/00G;

    move-result-object v3

    invoke-virtual {v3}, LX/00G;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 235023
    const-class v1, LX/0ez;

    invoke-static {p1, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ez;

    .line 235024
    if-eqz v1, :cond_1

    .line 235025
    iget-object v2, p0, LX/1MG;->mDisposableDelegate:LX/0je;

    invoke-interface {v1, v2}, LX/0ez;->a(LX/0je;)V

    .line 235026
    :cond_1
    return-void
.end method

.method private beginShowingProgress()V
    .locals 1

    .prologue
    .line 235027
    iget-object v0, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    if-eqz v0, :cond_0

    .line 235028
    iget-object v0, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 235029
    :cond_0
    return-void
.end method

.method private bindToBlueService(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 235030
    :try_start_0
    iget-object v0, p0, LX/1MG;->mContextServiceBinder:LX/1MH;

    iget-object v1, p0, LX/1MG;->mServiceConnection:LX/1MJ;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, LX/1MH;->a(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 235031
    if-eqz v0, :cond_0

    .line 235032
    iput-boolean v3, p0, LX/1MG;->mCalledBind:Z

    .line 235033
    :goto_0
    return-void

    .line 235034
    :catch_0
    move-exception v0

    .line 235035
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Binding BlueService for `"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "` threw an exception."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 235036
    :cond_0
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "Bind to BlueService failed"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 235037
    invoke-virtual {p0, v0}, LX/1MG;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V

    goto :goto_0
.end method

.method public static bindToService(LX/1MG;Z)V
    .locals 3

    .prologue
    .line 235038
    invoke-virtual {p0}, LX/1MG;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    if-eq v0, v1, :cond_1

    .line 235039
    :cond_0
    :goto_0
    return-void

    .line 235040
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1MG;->mContext:Landroid/content/Context;

    const-class v2, Lcom/facebook/fbservice/service/DefaultBlueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235041
    if-eqz p1, :cond_2

    .line 235042
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    const-string v2, "Bind intent must specify a component"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235043
    iget-object v1, p0, LX/1MG;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "We can currently only bind to a BlueService that is part of our package."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235044
    invoke-static {p0, v0}, LX/1MG;->doesIntentResolveToOtherProcess(LX/1MG;Landroid/content/Intent;)Z

    move-result v1

    move v1, v1

    .line 235045
    if-eqz v1, :cond_3

    .line 235046
    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    iput-object v1, p0, LX/1MG;->mTriedBindingLocally:LX/03R;

    .line 235047
    invoke-direct {p0, v0}, LX/1MG;->bindToBlueService(Landroid/content/Intent;)V

    goto :goto_0

    .line 235048
    :cond_3
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/1MG;->mTriedBindingLocally:LX/03R;

    .line 235049
    iget-object v0, p0, LX/1MG;->mBlueServiceLogicLazy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mL;

    iput-object v0, p0, LX/1MG;->mBlueService:LX/1mL;

    .line 235050
    invoke-static {p0}, LX/1MG;->maybeStartAndRegister(LX/1MG;)V

    .line 235051
    goto :goto_0

    .line 235052
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static doesIntentResolveToOtherProcess(LX/1MG;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235053
    sget-object v3, LX/1MG;->sIsInOurProcessMap:Ljava/util/Map;

    monitor-enter v3

    .line 235054
    :try_start_0
    sget-object v0, LX/1MG;->sIsInOurProcessMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 235055
    if-nez v0, :cond_1

    .line 235056
    iget-object v0, p0, LX/1MG;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v4}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 235057
    if-nez v0, :cond_0

    .line 235058
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t bind to service specified by "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235059
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 235060
    :cond_0
    :try_start_1
    iget-object v4, p0, LX/1MG;->mProcessUtil:LX/0VT;

    invoke-virtual {v4}, LX/0VT;->a()LX/00G;

    move-result-object v4

    .line 235061
    iget-object v5, v4, LX/00G;->b:Ljava/lang/String;

    move-object v4, v5

    .line 235062
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    .line 235063
    if-eqz v4, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 235064
    sget-object v4, LX/1MG;->sIsInOurProcessMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235065
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0

    :cond_2
    move v0, v2

    .line 235066
    goto :goto_0

    :cond_3
    move v0, v2

    .line 235067
    goto :goto_1
.end method

.method public static maybeStartAndRegister(LX/1MG;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235068
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v3, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    if-ne v0, v3, :cond_4

    .line 235069
    iget-object v0, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Null operation type"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235070
    iget-object v0, p0, LX/1MG;->mOperationId:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Non-null operation id"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235071
    :try_start_0
    iget-object v0, p0, LX/1MG;->mErrorPropagation:LX/1ME;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    if-ne v0, v3, :cond_2

    move v3, v1

    .line 235072
    :goto_2
    iget-object v0, p0, LX/1MG;->mBlueService:LX/1mL;

    iget-object v1, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    iget-object v2, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    iget-boolean v4, p0, LX/1MG;->mCanRunBeforeAppInit:Z

    .line 235073
    new-instance v5, LX/1q9;

    invoke-direct {v5, p0}, LX/1q9;-><init>(LX/1MG;)V

    move-object v5, v5

    .line 235074
    iget-object v6, p0, LX/1MG;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface/range {v0 .. v6}, LX/1mL;->startOperationWithCompletionHandlerAppInit(Ljava/lang/String;Landroid/os/Bundle;ZZLX/1qB;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1MG;->mOperationId:Ljava/lang/String;

    .line 235075
    iget-object v0, p0, LX/1MG;->mBlueService:LX/1mL;

    if-nez v0, :cond_3

    .line 235076
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235077
    :catch_0
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "BlueService.startOperationWithCompletionHandler failed"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 235078
    invoke-virtual {p0, v0}, LX/1MG;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 235079
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 235080
    goto :goto_0

    :cond_1
    move v0, v2

    .line 235081
    goto :goto_1

    :cond_2
    move v3, v2

    .line 235082
    goto :goto_2

    .line 235083
    :cond_3
    :try_start_1
    sget-object v0, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    iput-object v0, p0, LX/1MG;->mOperationState:LX/1MI;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 235084
    :cond_4
    iget-object v0, p0, LX/1MG;->mFbErrorReporter:LX/03V;

    const-string v1, "DefaultBlueServiceOperation_START_AND_REGISTER_CALLED_UNEXPECTINGLY"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "maybeStartAndRegister called in wrong state. triedBindingLocally="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/1MG;->mTriedBindingLocally:LX/03R;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/1MG;->mOperationState:LX/1MI;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", operationType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private onUnknownOperationTypeError()V
    .locals 3

    .prologue
    .line 235085
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown operation type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 235086
    invoke-virtual {p0, v0}, LX/1MG;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 235087
    return-void
.end method

.method private postToCallbackThread(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 235088
    const v0, -0x5b6925e

    invoke-static {p1, v0}, LX/02m;->a(Ljava/lang/String;I)V

    .line 235089
    :try_start_0
    iget-object v0, p0, LX/1MG;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 235090
    const-string v0, "Handler"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 235091
    iget-object v0, p0, LX/1MG;->mHandler:Landroid/os/Handler;

    const v1, 0x6c3dd0e5

    invoke-static {v0, p2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235092
    :goto_0
    const v0, 0x2d281dba

    invoke-static {v0}, LX/02m;->a(I)V

    .line 235093
    return-void

    .line 235094
    :cond_0
    :try_start_1
    const-string v0, "ExecutorService"

    invoke-static {v0}, LX/0PR;->b(Ljava/lang/String;)V

    .line 235095
    iget-object v0, p0, LX/1MG;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    const v1, 0x103d59ba

    invoke-static {v0, p2, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 235096
    :catchall_0
    move-exception v0

    const v1, -0x391abd58

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static safeUnbind(LX/1MG;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 235097
    iget-boolean v0, p0, LX/1MG;->mCalledBind:Z

    if-eqz v0, :cond_0

    .line 235098
    :try_start_0
    iget-object v0, p0, LX/1MG;->mContextServiceBinder:LX/1MH;

    iget-object v1, p0, LX/1MG;->mServiceConnection:LX/1MJ;

    invoke-virtual {v0, v1}, LX/1MH;->a(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235099
    :goto_0
    iput-boolean v2, p0, LX/1MG;->mCalledBind:Z

    .line 235100
    :cond_0
    return-void

    .line 235101
    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method private startOperation(Z)LX/1ML;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235102
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v3, LX/1MI;->INIT:LX/1MI;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Incorrect operation state"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 235103
    sget-object v0, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    iput-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    .line 235104
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 235105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/1MG;->mHandler:Landroid/os/Handler;

    .line 235106
    :cond_0
    invoke-direct {p0}, LX/1MG;->beginShowingProgress()V

    .line 235107
    if-nez p1, :cond_2

    :goto_1
    invoke-static {p0, v1}, LX/1MG;->bindToService(LX/1MG;Z)V

    .line 235108
    iget-object v0, p0, LX/1MG;->mResult:LX/1MK;

    return-object v0

    :cond_1
    move v0, v2

    .line 235109
    goto :goto_0

    :cond_2
    move v1, v2

    .line 235110
    goto :goto_1
.end method

.method public static stopShowingProgress(LX/1MG;)V
    .locals 1

    .prologue
    .line 235111
    iget-object v0, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    if-eqz v0, :cond_0

    .line 235112
    iget-object v0, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 235113
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 2

    .prologue
    .line 235114
    iget-object v0, p0, LX/1MG;->mBlueService:LX/1mL;

    .line 235115
    iget-object v1, p0, LX/1MG;->mOperationId:Ljava/lang/String;

    .line 235116
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 235117
    invoke-interface {v0, v1}, LX/1mL;->cancel(Ljava/lang/String;)Z

    move-result v0

    .line 235118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changePriority(Lcom/facebook/http/interfaces/RequestPriority;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 235119
    :try_start_0
    iget-object v1, p0, LX/1MG;->mBlueService:LX/1mL;

    .line 235120
    iget-object v2, p0, LX/1MG;->mOperationId:Ljava/lang/String;

    .line 235121
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 235122
    invoke-interface {v1, v2, p1}, LX/1mL;->changePriority(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 235123
    :cond_0
    :goto_0
    return v0

    .line 235124
    :catch_0
    move-exception v1

    .line 235125
    const-string v2, "DefaultBlueServiceOperation"

    const-string v3, "Cannot changePriority because of a RemoteException."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    .prologue
    .line 235126
    iget-object v0, p0, LX/1MG;->mDisposableDelegate:LX/0je;

    invoke-virtual {v0}, LX/0je;->dispose()V

    .line 235127
    return-void
.end method

.method public getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 234994
    iget-object v0, p0, LX/1MG;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public getCanRunBeforeAppInit()Z
    .locals 1

    .prologue
    .line 234963
    iget-boolean v0, p0, LX/1MG;->mCanRunBeforeAppInit:Z

    return v0
.end method

.method public getFireAndForget()Z
    .locals 1

    .prologue
    .line 235128
    iget-boolean v0, p0, LX/1MG;->mFireAndForget:Z

    return v0
.end method

.method public getOperationProgressIndicator()LX/4At;
    .locals 1

    .prologue
    .line 234949
    iget-object v0, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    return-object v0
.end method

.method public getOperationState()LX/1MI;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 234962
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    return-object v0
.end method

.method public getOperationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234961
    iget-object v0, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    return-object v0
.end method

.method public getParam()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 234960
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, LX/1MG;->mParam:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 234959
    iget-object v0, p0, LX/1MG;->mDisposableDelegate:LX/0je;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 2

    .prologue
    .line 234958
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->INIT:LX/1MI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->COMPLETED:LX/1MI;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 234950
    invoke-virtual {p0}, LX/1MG;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->COMPLETED:LX/1MI;

    if-ne v0, v1, :cond_1

    .line 234951
    :cond_0
    :goto_0
    return-void

    .line 234952
    :cond_1
    sget-object v0, LX/1MI;->COMPLETED:LX/1MI;

    iput-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    .line 234953
    const/4 v0, 0x0

    iput-object v0, p0, LX/1MG;->mOperationId:Ljava/lang/String;

    .line 234954
    invoke-static {p0}, LX/1MG;->safeUnbind(LX/1MG;)V

    .line 234955
    iget-boolean v0, p0, LX/1MG;->mFireAndForget:Z

    if-eqz v0, :cond_2

    .line 234956
    invoke-virtual {p0}, LX/1MG;->dispose()V

    goto :goto_0

    .line 234957
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportCompleted-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$5;-><init>(LX/1MG;Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-direct {p0, v0, v1}, LX/1MG;->postToCallbackThread(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 234945
    invoke-virtual {p0}, LX/1MG;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234946
    :cond_0
    :goto_0
    return-void

    .line 234947
    :cond_1
    iget-boolean v0, p0, LX/1MG;->mFireAndForget:Z

    if-nez v0, :cond_0

    .line 234948
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportProgress-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1MG;->mOperationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$4;-><init>(LX/1MG;Lcom/facebook/fbservice/service/OperationResult;)V

    invoke-direct {p0, v0, v1}, LX/1MG;->postToCallbackThread(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onServiceConnected(LX/1mL;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 234964
    invoke-virtual {p0}, LX/1MG;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234965
    :goto_0
    return-void

    .line 234966
    :cond_0
    iput-object p1, p0, LX/1MG;->mBlueService:LX/1mL;

    .line 234967
    invoke-static {p0}, LX/1MG;->maybeStartAndRegister(LX/1MG;)V

    goto :goto_0
.end method

.method public onServiceDisconnected()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 234968
    const/4 v0, 0x0

    iput-object v0, p0, LX/1MG;->mBlueService:LX/1mL;

    .line 234969
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    if-ne v0, v1, :cond_0

    .line 234970
    sget-object v0, LX/1nY;->ORCA_SERVICE_IPC_FAILURE:LX/1nY;

    const-string v1, "BlueService disconnected"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 234971
    invoke-virtual {p0, v0}, LX/1MG;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 234972
    :cond_0
    return-void
.end method

.method public setCanRunBeforeAppInit(Z)LX/1MF;
    .locals 0

    .prologue
    .line 234973
    iput-boolean p1, p0, LX/1MG;->mCanRunBeforeAppInit:Z

    .line 234974
    return-object p0
.end method

.method public setFireAndForget(Z)LX/1MF;
    .locals 0

    .prologue
    .line 234975
    iput-boolean p1, p0, LX/1MG;->mFireAndForget:Z

    .line 234976
    return-object p0
.end method

.method public setOnProgressListener(LX/4An;)LX/1MF;
    .locals 0

    .prologue
    .line 234977
    iput-object p1, p0, LX/1MG;->mOnProgressListener:LX/4An;

    .line 234978
    return-object p0
.end method

.method public setOperationProgressIndicator(LX/4At;)LX/1MF;
    .locals 2

    .prologue
    .line 234979
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    if-ne v0, v1, :cond_1

    .line 234980
    :cond_0
    invoke-static {p0}, LX/1MG;->stopShowingProgress(LX/1MG;)V

    .line 234981
    :cond_1
    iput-object p1, p0, LX/1MG;->mOperationProgressIndicator:LX/4At;

    .line 234982
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->OPERATION_QUEUED:LX/1MI;

    if-ne v0, v1, :cond_3

    .line 234983
    :cond_2
    invoke-direct {p0}, LX/1MG;->beginShowingProgress()V

    .line 234984
    :cond_3
    return-object p0
.end method

.method public start()LX/1ML;
    .locals 1

    .prologue
    .line 234985
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/1MG;->startOperation(Z)LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public startOnMainThread()LX/1ML;
    .locals 2

    .prologue
    .line 234986
    iget-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    sget-object v1, LX/1MI;->INIT:LX/1MI;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Incorrect operation state"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 234987
    sget-object v0, LX/1MI;->READY_TO_QUEUE:LX/1MI;

    iput-object v0, p0, LX/1MG;->mOperationState:LX/1MI;

    .line 234988
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/1MG;->mHandler:Landroid/os/Handler;

    .line 234989
    invoke-direct {p0}, LX/1MG;->beginShowingProgress()V

    .line 234990
    const-string v0, "BindToService(false)"

    new-instance v1, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$2;

    invoke-direct {v1, p0}, Lcom/facebook/fbservice/ops/DefaultBlueServiceOperation$2;-><init>(LX/1MG;)V

    invoke-direct {p0, v0, v1}, LX/1MG;->postToCallbackThread(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 234991
    iget-object v0, p0, LX/1MG;->mResult:LX/1MK;

    return-object v0

    .line 234992
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startTryNotToUseMainThread()LX/1ML;
    .locals 1

    .prologue
    .line 234993
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1MG;->startOperation(Z)LX/1ML;

    move-result-object v0

    return-object v0
.end method
