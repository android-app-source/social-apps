.class public final LX/1NH;
.super LX/1NI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1NI",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private f:Lcom/facebook/graphql/executor/GraphQLResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0TF;Ljava/util/concurrent/Executor;LX/03V;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 237171
    move-object v0, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, v1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, LX/1NI;-><init>(LX/0tX;LX/03V;LX/0TF;Ljava/util/concurrent/Executor;Ljava/lang/String;LX/0zO;LX/0t2;)V

    .line 237172
    iget-object v0, p5, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237173
    iput-object p5, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 237174
    invoke-virtual {p5}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v0

    .line 237175
    iput-object v0, p0, LX/1NI;->c:Ljava/util/Set;

    .line 237176
    iget-object v0, p5, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 237177
    if-eqz v0, :cond_0

    .line 237178
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/1NH;->e:Ljava/lang/ref/WeakReference;

    .line 237179
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 237180
    iget-object v0, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v0, :cond_0

    .line 237181
    :goto_0
    return-void

    .line 237182
    :cond_0
    iget-object v0, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    iget-object v1, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, -0x1

    .line 237183
    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v3, v3}, LX/1NB;->c(LX/1NB;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/quicklog/QuickPerformanceLogger;II)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v2

    move-object v0, v2

    .line 237184
    iput-object v0, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    goto :goto_0
.end method

.method public final c()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 237185
    iget-object v1, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_0

    .line 237186
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 237187
    :goto_0
    return-object v0

    .line 237188
    :cond_0
    invoke-virtual {p0}, LX/1NH;->b()V

    .line 237189
    iget-object v1, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/1NH;->f:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 237190
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    if-nez v1, :cond_2

    .line 237191
    :goto_2
    move-object v0, v0

    .line 237192
    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    invoke-virtual {v1, v0}, LX/1NB;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    goto :goto_2
.end method

.method public final f()I
    .locals 1
    .annotation build Lcom/facebook/graphql/executor/iface/GraphQLObservablePusher$SubscriptionStore;
    .end annotation

    .prologue
    .line 237193
    const/4 v0, 0x1

    return v0
.end method
