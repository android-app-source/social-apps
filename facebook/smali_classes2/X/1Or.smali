.class public LX/1Or;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Oo;


# direct methods
.method public constructor <init>(LX/1Oo;)V
    .locals 0

    .prologue
    .line 242367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242368
    iput-object p1, p0, LX/1Or;->a:LX/1Oo;

    .line 242369
    return-void
.end method

.method public static a(LX/1Or;Ljava/util/List;II)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1lw;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 242370
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1lw;

    .line 242371
    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1lw;

    .line 242372
    iget v0, v5, LX/1lw;->a:I

    packed-switch v0, :pswitch_data_0

    .line 242373
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    .line 242374
    const/4 p0, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 242375
    iget v8, v3, LX/1lw;->b:I

    iget p1, v3, LX/1lw;->d:I

    if-ge v8, p1, :cond_9

    .line 242376
    iget v8, v5, LX/1lw;->b:I

    iget p1, v3, LX/1lw;->b:I

    if-ne v8, p1, :cond_18

    iget v8, v5, LX/1lw;->d:I

    iget p1, v3, LX/1lw;->d:I

    iget p2, v3, LX/1lw;->b:I

    sub-int/2addr p1, p2

    if-ne v8, p1, :cond_18

    move v8, v7

    .line 242377
    :goto_1
    iget p1, v3, LX/1lw;->d:I

    iget p2, v5, LX/1lw;->b:I

    if-ge p1, p2, :cond_a

    .line 242378
    iget p1, v5, LX/1lw;->b:I

    add-int/lit8 p1, p1, -0x1

    iput p1, v5, LX/1lw;->b:I

    .line 242379
    :cond_0
    iget p1, v3, LX/1lw;->b:I

    iget p2, v5, LX/1lw;->b:I

    if-gt p1, p2, :cond_b

    .line 242380
    iget v7, v5, LX/1lw;->b:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v5, LX/1lw;->b:I

    move-object v7, p0

    .line 242381
    :goto_2
    if-eqz v8, :cond_c

    .line 242382
    invoke-interface {v1, v2, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242383
    invoke-interface {v1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 242384
    iget-object v6, v0, LX/1Or;->a:LX/1Oo;

    invoke-interface {v6, v3}, LX/1Oo;->a(LX/1lw;)V

    .line 242385
    :cond_1
    :goto_3
    goto :goto_0

    .line 242386
    :pswitch_1
    const/4 v0, 0x0

    .line 242387
    iget v1, v3, LX/1lw;->d:I

    iget v2, v5, LX/1lw;->b:I

    if-ge v1, v2, :cond_2

    .line 242388
    const/4 v0, -0x1

    .line 242389
    :cond_2
    iget v1, v3, LX/1lw;->b:I

    iget v2, v5, LX/1lw;->b:I

    if-ge v1, v2, :cond_3

    .line 242390
    add-int/lit8 v0, v0, 0x1

    .line 242391
    :cond_3
    iget v1, v5, LX/1lw;->b:I

    iget v2, v3, LX/1lw;->b:I

    if-gt v1, v2, :cond_4

    .line 242392
    iget v1, v3, LX/1lw;->b:I

    iget v2, v5, LX/1lw;->d:I

    add-int/2addr v1, v2

    iput v1, v3, LX/1lw;->b:I

    .line 242393
    :cond_4
    iget v1, v5, LX/1lw;->b:I

    iget v2, v3, LX/1lw;->d:I

    if-gt v1, v2, :cond_5

    .line 242394
    iget v1, v3, LX/1lw;->d:I

    iget v2, v5, LX/1lw;->d:I

    add-int/2addr v1, v2

    iput v1, v3, LX/1lw;->d:I

    .line 242395
    :cond_5
    iget v1, v5, LX/1lw;->b:I

    add-int/2addr v0, v1

    iput v0, v5, LX/1lw;->b:I

    .line 242396
    invoke-interface {p1, p2, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242397
    invoke-interface {p1, p3, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242398
    goto :goto_0

    :pswitch_2
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    .line 242399
    const/4 v7, 0x0

    const/4 p2, 0x2

    .line 242400
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->b:I

    if-ge v6, v8, :cond_19

    .line 242401
    iget v6, v5, LX/1lw;->b:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v5, LX/1lw;->b:I

    move-object v6, v7

    .line 242402
    :goto_4
    iget v8, v3, LX/1lw;->b:I

    iget p0, v5, LX/1lw;->b:I

    if-gt v8, p0, :cond_1a

    .line 242403
    iget v8, v5, LX/1lw;->b:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v5, LX/1lw;->b:I

    .line 242404
    :cond_6
    :goto_5
    invoke-interface {v1, v4, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242405
    iget v8, v5, LX/1lw;->d:I

    if-lez v8, :cond_1b

    .line 242406
    invoke-interface {v1, v2, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242407
    :goto_6
    if-eqz v6, :cond_7

    .line 242408
    invoke-interface {v1, v2, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 242409
    :cond_7
    if-eqz v7, :cond_8

    .line 242410
    invoke-interface {v1, v2, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 242411
    :cond_8
    goto/16 :goto_0

    .line 242412
    :cond_9
    iget v8, v5, LX/1lw;->b:I

    iget p1, v3, LX/1lw;->d:I

    add-int/lit8 p1, p1, 0x1

    if-ne v8, p1, :cond_17

    iget v8, v5, LX/1lw;->d:I

    iget p1, v3, LX/1lw;->b:I

    iget p2, v3, LX/1lw;->d:I

    sub-int/2addr p1, p2

    if-ne v8, p1, :cond_17

    move v6, v7

    move v8, v7

    .line 242413
    goto/16 :goto_1

    .line 242414
    :cond_a
    iget p1, v3, LX/1lw;->d:I

    iget p2, v5, LX/1lw;->b:I

    iget p3, v5, LX/1lw;->d:I

    add-int/2addr p2, p3

    if-ge p1, p2, :cond_0

    .line 242415
    iget v6, v5, LX/1lw;->d:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v5, LX/1lw;->d:I

    .line 242416
    iput v7, v3, LX/1lw;->a:I

    .line 242417
    iput v7, v3, LX/1lw;->d:I

    .line 242418
    iget v6, v5, LX/1lw;->d:I

    if-nez v6, :cond_1

    .line 242419
    invoke-interface {v1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 242420
    iget-object v6, v0, LX/1Or;->a:LX/1Oo;

    invoke-interface {v6, v5}, LX/1Oo;->a(LX/1lw;)V

    goto/16 :goto_3

    .line 242421
    :cond_b
    iget p1, v3, LX/1lw;->b:I

    iget p2, v5, LX/1lw;->b:I

    iget p3, v5, LX/1lw;->d:I

    add-int/2addr p2, p3

    if-ge p1, p2, :cond_16

    .line 242422
    iget p1, v5, LX/1lw;->b:I

    iget p2, v5, LX/1lw;->d:I

    add-int/2addr p1, p2

    iget p2, v3, LX/1lw;->b:I

    sub-int/2addr p1, p2

    .line 242423
    iget-object p2, v0, LX/1Or;->a:LX/1Oo;

    iget p3, v3, LX/1lw;->b:I

    add-int/lit8 p3, p3, 0x1

    invoke-interface {p2, v7, p3, p1, p0}, LX/1Oo;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v7

    .line 242424
    iget p0, v3, LX/1lw;->b:I

    iget p1, v5, LX/1lw;->b:I

    sub-int/2addr p0, p1

    iput p0, v5, LX/1lw;->d:I

    goto/16 :goto_2

    .line 242425
    :cond_c
    if-eqz v6, :cond_11

    .line 242426
    if-eqz v7, :cond_e

    .line 242427
    iget v6, v3, LX/1lw;->b:I

    iget v8, v7, LX/1lw;->b:I

    if-le v6, v8, :cond_d

    .line 242428
    iget v6, v3, LX/1lw;->b:I

    iget v8, v7, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->b:I

    .line 242429
    :cond_d
    iget v6, v3, LX/1lw;->d:I

    iget v8, v7, LX/1lw;->b:I

    if-le v6, v8, :cond_e

    .line 242430
    iget v6, v3, LX/1lw;->d:I

    iget v8, v7, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->d:I

    .line 242431
    :cond_e
    iget v6, v3, LX/1lw;->b:I

    iget v8, v5, LX/1lw;->b:I

    if-le v6, v8, :cond_f

    .line 242432
    iget v6, v3, LX/1lw;->b:I

    iget v8, v5, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->b:I

    .line 242433
    :cond_f
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->b:I

    if-le v6, v8, :cond_10

    .line 242434
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->d:I

    .line 242435
    :cond_10
    :goto_7
    invoke-interface {v1, v2, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242436
    iget v6, v3, LX/1lw;->b:I

    iget v8, v3, LX/1lw;->d:I

    if-eq v6, v8, :cond_15

    .line 242437
    invoke-interface {v1, v4, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 242438
    :goto_8
    if-eqz v7, :cond_1

    .line 242439
    invoke-interface {v1, v2, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_3

    .line 242440
    :cond_11
    if-eqz v7, :cond_13

    .line 242441
    iget v6, v3, LX/1lw;->b:I

    iget v8, v7, LX/1lw;->b:I

    if-lt v6, v8, :cond_12

    .line 242442
    iget v6, v3, LX/1lw;->b:I

    iget v8, v7, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->b:I

    .line 242443
    :cond_12
    iget v6, v3, LX/1lw;->d:I

    iget v8, v7, LX/1lw;->b:I

    if-lt v6, v8, :cond_13

    .line 242444
    iget v6, v3, LX/1lw;->d:I

    iget v8, v7, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->d:I

    .line 242445
    :cond_13
    iget v6, v3, LX/1lw;->b:I

    iget v8, v5, LX/1lw;->b:I

    if-lt v6, v8, :cond_14

    .line 242446
    iget v6, v3, LX/1lw;->b:I

    iget v8, v5, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->b:I

    .line 242447
    :cond_14
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->b:I

    if-lt v6, v8, :cond_10

    .line 242448
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->d:I

    sub-int/2addr v6, v8

    iput v6, v3, LX/1lw;->d:I

    goto :goto_7

    .line 242449
    :cond_15
    invoke-interface {v1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_8

    :cond_16
    move-object v7, p0

    goto/16 :goto_2

    :cond_17
    move v8, v6

    move v6, v7

    goto/16 :goto_1

    :cond_18
    move v8, v6

    goto/16 :goto_1

    .line 242450
    :cond_19
    iget v6, v3, LX/1lw;->d:I

    iget v8, v5, LX/1lw;->b:I

    iget p0, v5, LX/1lw;->d:I

    add-int/2addr v8, p0

    if-ge v6, v8, :cond_1c

    .line 242451
    iget v6, v5, LX/1lw;->d:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v5, LX/1lw;->d:I

    .line 242452
    iget-object v6, v0, LX/1Or;->a:LX/1Oo;

    iget v8, v3, LX/1lw;->b:I

    const/4 p0, 0x1

    iget-object p1, v5, LX/1lw;->c:Ljava/lang/Object;

    invoke-interface {v6, p2, v8, p0, p1}, LX/1Oo;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v6

    goto/16 :goto_4

    .line 242453
    :cond_1a
    iget v8, v3, LX/1lw;->b:I

    iget p0, v5, LX/1lw;->b:I

    iget p1, v5, LX/1lw;->d:I

    add-int/2addr p0, p1

    if-ge v8, p0, :cond_6

    .line 242454
    iget v7, v5, LX/1lw;->b:I

    iget v8, v5, LX/1lw;->d:I

    add-int/2addr v7, v8

    iget v8, v3, LX/1lw;->b:I

    sub-int v8, v7, v8

    .line 242455
    iget-object v7, v0, LX/1Or;->a:LX/1Oo;

    iget p0, v3, LX/1lw;->b:I

    add-int/lit8 p0, p0, 0x1

    iget-object p1, v5, LX/1lw;->c:Ljava/lang/Object;

    invoke-interface {v7, p2, p0, v8, p1}, LX/1Oo;->a(IIILjava/lang/Object;)LX/1lw;

    move-result-object v7

    .line 242456
    iget p0, v5, LX/1lw;->d:I

    sub-int v8, p0, v8

    iput v8, v5, LX/1lw;->d:I

    goto/16 :goto_5

    .line 242457
    :cond_1b
    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 242458
    iget-object v8, v0, LX/1Or;->a:LX/1Oo;

    invoke-interface {v8, v5}, LX/1Oo;->a(LX/1lw;)V

    goto/16 :goto_6

    :cond_1c
    move-object v6, v7

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
