.class public LX/0mL;
.super LX/0mM;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0mL;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 132390
    invoke-direct {p0}, LX/0mM;-><init>()V

    .line 132391
    iput-object p1, p0, LX/0mL;->a:LX/0Or;

    .line 132392
    return-void
.end method

.method public static a(LX/0QB;)LX/0mL;
    .locals 4

    .prologue
    .line 132377
    sget-object v0, LX/0mL;->b:LX/0mL;

    if-nez v0, :cond_1

    .line 132378
    const-class v1, LX/0mL;

    monitor-enter v1

    .line 132379
    :try_start_0
    sget-object v0, LX/0mL;->b:LX/0mL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132380
    if-eqz v2, :cond_0

    .line 132381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132382
    new-instance v3, LX/0mL;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0mL;-><init>(LX/0Or;)V

    .line 132383
    move-object v0, v3

    .line 132384
    sput-object v0, LX/0mL;->b:LX/0mL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132387
    :cond_1
    sget-object v0, LX/0mL;->b:LX/0mL;

    return-object v0

    .line 132388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132376
    iget-object v0, p0, LX/0mL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
