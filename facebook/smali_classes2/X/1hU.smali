.class public final LX/1hU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public enableTimestampVerification:Z

.field public enforceCertKeyLengthVerification:Z

.field public trustedReferenceTimestamp:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 296077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296078
    iput-boolean v0, p0, LX/1hU;->enableTimestampVerification:Z

    .line 296079
    iput-boolean v0, p0, LX/1hU;->enforceCertKeyLengthVerification:Z

    .line 296080
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1hU;->trustedReferenceTimestamp:J

    return-void
.end method
