.class public LX/1RW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0kb;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246343
    iput-object p1, p0, LX/1RW;->a:LX/0Zb;

    .line 246344
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1RW;->c:Ljava/lang/String;

    .line 246345
    iput-object p2, p0, LX/1RW;->b:LX/0kb;

    .line 246346
    return-void
.end method

.method public static a(LX/0QB;)LX/1RW;
    .locals 1

    .prologue
    .line 246347
    invoke-static {p0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1RW;
    .locals 3

    .prologue
    .line 246348
    new-instance v2, LX/1RW;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v1

    check-cast v1, LX/0kb;

    invoke-direct {v2, v0, v1}, LX/1RW;-><init>(LX/0Zb;LX/0kb;)V

    .line 246349
    return-object v2
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 246350
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "compost"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "event"

    invoke-virtual {v0, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 246351
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 246352
    move-object v0, v0

    .line 246353
    return-object v0
.end method

.method public static p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 246354
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "compost"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "event"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-object v1, p0, LX/1RW;->c:Ljava/lang/String;

    .line 246355
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 246356
    move-object v0, v0

    .line 246357
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 246358
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "discard_draft"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "media_count"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "char_count"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246359
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246370
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "retry"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246371
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 246360
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "open_draft"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "media_count"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "char_count"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "network_connectivity"

    iget-object v3, p0, LX/1RW;->b:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246361
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIIIIZZZZ)V
    .locals 4

    .prologue
    .line 246362
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "save_draft"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "draft_save_source"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "media_count"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "char_count"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "photo_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "video_count"

    invoke-virtual {v1, v2, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "tag_count"

    invoke-virtual {v1, v2, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "minutiae"

    invoke-virtual {v1, v2, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "checkin"

    invoke-virtual {v1, v2, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "sticker"

    invoke-virtual {v1, v2, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "link"

    invoke-virtual {v1, v2, p11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "network_connectivity"

    iget-object v3, p0, LX/1RW;->b:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246363
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246364
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "draft_disabled_for_content_type"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "content_type"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "details"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246365
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 246366
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "composer_cancel_draft_dialog_action"

    invoke-static {v1, p3}, LX/1RW;->f(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "composer_cancel_dialog_action"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "composer_type"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "media_count"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "char_count"

    invoke-virtual {v1, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "network_connectivity"

    iget-object v3, p0, LX/1RW;->b:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246367
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 246368
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "back_press"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "is_hardware_back"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246369
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246338
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "discard_dialog_opened"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "section"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246339
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246340
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "story_menu_opened"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "section"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246341
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246314
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "discard_dialog_closed"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "section"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246315
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246316
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "draft_disabled_for_content_type"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "content_type"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246317
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 246318
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "inline_composer_draft_entry"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246319
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246320
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "discard_cancelled"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "section"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246321
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246322
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "draft_disabled_for_source_type"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "source_type"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246323
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246324
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "discard_pending"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246325
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246326
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "draft_candidate"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246327
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246328
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "network_status"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "status"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246329
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246330
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "compost_photo_upload_progress"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "status"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246331
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246332
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "trigger_jewel_notification_result"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "status"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246333
    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246334
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "failedHighQualityDraftCheck"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "detailed_info"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246335
    return-void
.end method

.method public final o(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246336
    iget-object v0, p0, LX/1RW;->a:LX/0Zb;

    const-string v1, "passedHighQualityDraftCheck"

    invoke-static {p0, v1}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "detailed_info"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 246337
    return-void
.end method
