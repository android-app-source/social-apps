.class public LX/1LU;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1Ce;
.implements LX/1KT;
.implements LX/0hk;


# instance fields
.field public a:LX/1LV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233816
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233817
    return-void
.end method


# virtual methods
.method public final a(LX/01J;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233814
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1}, LX/1Cd;->a(LX/01J;)V

    .line 233815
    return-void
.end method

.method public final a(LX/0g8;)V
    .locals 1

    .prologue
    .line 233812
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1}, LX/1Cd;->a(LX/0g8;)V

    .line 233813
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 233810
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1, p2, p3}, LX/1Cd;->a(LX/0g8;Ljava/lang/Object;I)V

    .line 233811
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 233808
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Cd;->a(LX/0g8;Ljava/lang/Object;II)V

    .line 233809
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 233818
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1}, LX/1Cd;->a(Ljava/lang/Object;)V

    .line 233819
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 233805
    if-eqz p1, :cond_0

    .line 233806
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    iget-object v1, p0, LX/1LU;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    .line 233807
    :cond_0
    return-void
.end method

.method public final b(LX/0g8;)V
    .locals 1

    .prologue
    .line 233803
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1}, LX/1Cd;->b(LX/0g8;)V

    .line 233804
    return-void
.end method

.method public final b(LX/0g8;Ljava/lang/Object;I)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 233801
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1, p2, p3}, LX/1Cd;->b(LX/0g8;Ljava/lang/Object;I)V

    .line 233802
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 233799
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    invoke-virtual {v0, p1}, LX/1Cd;->b(Ljava/lang/Object;)V

    .line 233800
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 233797
    iget-object v0, p0, LX/1LU;->a:LX/1LV;

    iget-object v1, p0, LX/1LU;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    .line 233798
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 233796
    return-void
.end method
