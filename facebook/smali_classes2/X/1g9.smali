.class public LX/1g9;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fy;


# instance fields
.field private a:Ljava/lang/Boolean;

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 293793
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 293794
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 293795
    iput-object v0, p0, LX/1g9;->b:LX/0Ot;

    .line 293796
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 293797
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 293798
    invoke-interface {p1}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, LX/1gA;->a(Landroid/view/ViewGroup;)V

    .line 293799
    return-void
.end method

.method public final kw_()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 293800
    iget-object v0, p0, LX/1g9;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 293801
    iget-object v0, p0, LX/1g9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/1Dd;->s:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1g9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/1Dd;->g:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1g9;->a:Ljava/lang/Boolean;

    .line 293802
    sget-boolean v0, LX/1V5;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1g9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/1Dd;->p:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    :goto_1
    sput-boolean v2, LX/1V5;->e:Z

    .line 293803
    iget-object v0, p0, LX/1g9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1Dd;->r:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    sput-boolean v0, LX/1V5;->f:Z

    .line 293804
    :cond_2
    iget-object v0, p0, LX/1g9;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_3
    move v0, v1

    .line 293805
    goto :goto_0

    :cond_4
    move v2, v1

    .line 293806
    goto :goto_1
.end method
