.class public LX/1S1;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:LX/1S2;

.field private final b:LX/1S5;

.field private final c:LX/0if;

.field private final d:LX/1S9;

.field private final e:LX/1SA;


# direct methods
.method public constructor <init>(LX/1S2;LX/1S5;LX/1S9;LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247367
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 247368
    new-instance v0, LX/1SA;

    invoke-direct {v0, p0}, LX/1SA;-><init>(LX/1S1;)V

    iput-object v0, p0, LX/1S1;->e:LX/1SA;

    .line 247369
    iput-object p1, p0, LX/1S1;->a:LX/1S2;

    .line 247370
    iput-object p2, p0, LX/1S1;->b:LX/1S5;

    .line 247371
    iput-object p3, p0, LX/1S1;->d:LX/1S9;

    .line 247372
    iput-object p4, p0, LX/1S1;->c:LX/0if;

    .line 247373
    iget-object v0, p0, LX/1S1;->d:LX/1S9;

    iget-object v1, p0, LX/1S1;->e:LX/1SA;

    .line 247374
    iget-object p1, v0, LX/1S9;->j:Ljava/util/Set;

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 247375
    iget-object v0, p0, LX/1S1;->c:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 247376
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 247365
    new-instance v0, Lcom/facebook/components/ComponentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 247319
    iget-object v0, p0, LX/1S1;->d:LX/1S9;

    const/4 v3, 0x0

    .line 247320
    iget-boolean v1, v0, LX/1S9;->l:Z

    if-eqz v1, :cond_0

    .line 247321
    iput-boolean v3, v0, LX/1S9;->l:Z

    .line 247322
    iget-object v1, v0, LX/1S9;->e:LX/0Uh;

    const/16 v2, 0x31f

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 247323
    iget-object v1, v0, LX/1S9;->g:LX/0Wd;

    new-instance v2, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuStore$1;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/crowdsourcing/CrowdsourcingTofuStore$1;-><init>(LX/1S9;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247324
    :cond_0
    :goto_0
    iget-object v1, v0, LX/1S9;->k:LX/2tX;

    move-object v0, v1

    .line 247325
    if-nez v0, :cond_2

    .line 247326
    check-cast p3, Lcom/facebook/components/ComponentView;

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 247327
    :cond_1
    :goto_1
    return-void

    .line 247328
    :cond_2
    sget-object v1, LX/328;->UNANSWERED:LX/328;

    .line 247329
    iget-object v2, v0, LX/2tX;->d:LX/328;

    move-object v2, v2

    .line 247330
    invoke-virtual {v1, v2}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 247331
    iget-object v1, v0, LX/2tX;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 247332
    iget-object v1, p0, LX/1S1;->b:LX/1S5;

    invoke-virtual {v1, v0}, LX/1S5;->b(LX/2tX;)V

    .line 247333
    iget-object v1, p0, LX/1S1;->c:LX/0if;

    sget-object v2, LX/0ig;->av:LX/0ih;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "render_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 247334
    iget-object v4, v0, LX/2tX;->d:LX/328;

    move-object v4, v4

    .line 247335
    invoke-virtual {v4}, LX/328;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 247336
    :cond_3
    :goto_2
    new-instance v1, LX/1De;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 247337
    iget-object v2, p0, LX/1S1;->a:LX/1S2;

    const/4 v3, 0x0

    .line 247338
    new-instance v4, LX/34Y;

    invoke-direct {v4, v2}, LX/34Y;-><init>(LX/1S2;)V

    .line 247339
    sget-object p1, LX/1S2;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/34Z;

    .line 247340
    if-nez p1, :cond_4

    .line 247341
    new-instance p1, LX/34Z;

    invoke-direct {p1}, LX/34Z;-><init>()V

    .line 247342
    :cond_4
    invoke-static {p1, v1, v3, v3, v4}, LX/34Z;->a$redex0(LX/34Z;LX/1De;IILX/34Y;)V

    .line 247343
    move-object v4, p1

    .line 247344
    move-object v3, v4

    .line 247345
    move-object v2, v3

    .line 247346
    iget-object v3, v2, LX/34Z;->a:LX/34Y;

    iput-object v0, v3, LX/34Y;->a:LX/2tX;

    .line 247347
    iget-object v3, v2, LX/34Z;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 247348
    move-object v2, v2

    .line 247349
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 247350
    iget-object v4, v2, LX/34Z;->a:LX/34Y;

    iput-object v3, v4, LX/34Y;->b:Landroid/content/Context;

    .line 247351
    iget-object v4, v2, LX/34Z;->d:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {v4, p1}, Ljava/util/BitSet;->set(I)V

    .line 247352
    move-object v2, v2

    .line 247353
    invoke-static {v1, v2}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v1

    invoke-virtual {v1}, LX/1me;->b()LX/1dV;

    move-result-object v1

    .line 247354
    check-cast p3, Lcom/facebook/components/ComponentView;

    invoke-virtual {p3, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 247355
    iget-object v1, v0, LX/2tX;->d:LX/328;

    move-object v0, v1

    .line 247356
    sget-object v1, LX/328;->UNANSWERED:LX/328;

    invoke-virtual {v0, v1}, LX/328;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 247357
    iget-object v0, p0, LX/1S1;->c:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    const-string v2, "question_removed"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 247358
    iget-object v0, p0, LX/1S1;->d:LX/1S9;

    .line 247359
    const/4 v1, 0x0

    iput-object v1, v0, LX/1S9;->k:LX/2tX;

    .line 247360
    iget-object v0, p0, LX/1S1;->c:LX/0if;

    sget-object v1, LX/0ig;->av:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    goto/16 :goto_1

    .line 247361
    :cond_5
    iget-object v1, p0, LX/1S1;->c:LX/0if;

    sget-object v2, LX/0ig;->av:LX/0ih;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "render_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 247362
    iget-object v4, v0, LX/2tX;->d:LX/328;

    move-object v4, v4

    .line 247363
    invoke-virtual {v4}, LX/328;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247364
    :cond_6
    invoke-static {v0}, LX/1S9;->c(LX/1S9;)V

    goto/16 :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 247366
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247318
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 247317
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 247316
    const/4 v0, 0x1

    return v0
.end method
