.class public LX/1f0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289437
    return-void
.end method

.method public static a(IIIZ)I
    .locals 2

    .prologue
    .line 289438
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 289439
    :cond_0
    :goto_0
    return p0

    .line 289440
    :cond_1
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 289441
    if-eqz p3, :cond_2

    const v1, 0x3f2aaaab

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 289442
    :cond_2
    int-to-float v1, p0

    div-float v0, v1, v0

    float-to-int p0, v0

    goto :goto_0
.end method
