.class public abstract LX/1IA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Ljava/lang/Character;",
        ">;"
    }
.end annotation


# static fields
.field public static final ANY:LX/1IA;

.field public static final ASCII:LX/1IA;

.field public static final BREAKING_WHITESPACE:LX/1IA;

.field public static final DIGIT:LX/1IA;

.field public static final INVISIBLE:LX/1IA;

.field public static final JAVA_DIGIT:LX/1IA;

.field public static final JAVA_ISO_CONTROL:LX/1IA;

.field public static final JAVA_LETTER:LX/1IA;

.field public static final JAVA_LETTER_OR_DIGIT:LX/1IA;

.field public static final JAVA_LOWER_CASE:LX/1IA;

.field public static final JAVA_UPPER_CASE:LX/1IA;

.field public static final NONE:LX/1IA;

.field public static final SINGLE_WIDTH:LX/1IA;

.field public static final WHITESPACE:LX/1IA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228520
    sget-object v0, LX/1IB;->INSTANCE:LX/1IB;

    move-object v0, v0

    .line 228521
    sput-object v0, LX/1IA;->WHITESPACE:LX/1IA;

    .line 228522
    sget-object v0, LX/1IE;->INSTANCE:LX/1IA;

    move-object v0, v0

    .line 228523
    sput-object v0, LX/1IA;->BREAKING_WHITESPACE:LX/1IA;

    .line 228524
    sget-object v0, LX/1IF;->INSTANCE:LX/1IF;

    move-object v0, v0

    .line 228525
    sput-object v0, LX/1IA;->ASCII:LX/1IA;

    .line 228526
    sget-object v0, LX/1IG;->INSTANCE:LX/1IG;

    move-object v0, v0

    .line 228527
    sput-object v0, LX/1IA;->DIGIT:LX/1IA;

    .line 228528
    sget-object v0, LX/1II;->INSTANCE:LX/1II;

    move-object v0, v0

    .line 228529
    sput-object v0, LX/1IA;->JAVA_DIGIT:LX/1IA;

    .line 228530
    sget-object v0, LX/1IJ;->INSTANCE:LX/1IJ;

    move-object v0, v0

    .line 228531
    sput-object v0, LX/1IA;->JAVA_LETTER:LX/1IA;

    .line 228532
    sget-object v0, LX/1IK;->INSTANCE:LX/1IK;

    move-object v0, v0

    .line 228533
    sput-object v0, LX/1IA;->JAVA_LETTER_OR_DIGIT:LX/1IA;

    .line 228534
    sget-object v0, LX/1IL;->INSTANCE:LX/1IL;

    move-object v0, v0

    .line 228535
    sput-object v0, LX/1IA;->JAVA_UPPER_CASE:LX/1IA;

    .line 228536
    sget-object v0, LX/1IM;->INSTANCE:LX/1IM;

    move-object v0, v0

    .line 228537
    sput-object v0, LX/1IA;->JAVA_LOWER_CASE:LX/1IA;

    .line 228538
    sget-object v0, LX/1IN;->INSTANCE:LX/1IN;

    move-object v0, v0

    .line 228539
    sput-object v0, LX/1IA;->JAVA_ISO_CONTROL:LX/1IA;

    .line 228540
    sget-object v0, LX/1IO;->INSTANCE:LX/1IO;

    move-object v0, v0

    .line 228541
    sput-object v0, LX/1IA;->INVISIBLE:LX/1IA;

    .line 228542
    sget-object v0, LX/1IP;->INSTANCE:LX/1IP;

    move-object v0, v0

    .line 228543
    sput-object v0, LX/1IA;->SINGLE_WIDTH:LX/1IA;

    .line 228544
    sget-object v0, LX/1IQ;->INSTANCE:LX/1IQ;

    move-object v0, v0

    .line 228545
    sput-object v0, LX/1IA;->ANY:LX/1IA;

    .line 228546
    sget-object v0, LX/1IR;->INSTANCE:LX/1IR;

    move-object v0, v0

    .line 228547
    sput-object v0, LX/1IA;->NONE:LX/1IA;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 228511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static anyOf(Ljava/lang/CharSequence;)LX/1IA;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228512
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 228513
    new-instance v0, LX/4vr;

    invoke-direct {v0, p0}, LX/4vr;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v0

    .line 228514
    :pswitch_0
    sget-object v0, LX/1IR;->INSTANCE:LX/1IR;

    move-object v0, v0

    .line 228515
    goto :goto_0

    .line 228516
    :pswitch_1
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, LX/1IA;->is(C)LX/1IA;

    move-result-object v0

    goto :goto_0

    .line 228517
    :pswitch_2
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 228518
    new-instance p0, LX/4vs;

    invoke-direct {p0, v0, v1}, LX/4vs;-><init>(CC)V

    move-object v0, p0

    .line 228519
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static is(C)LX/1IA;
    .locals 1

    .prologue
    .line 228461
    new-instance v0, LX/29Q;

    invoke-direct {v0, p0}, LX/29Q;-><init>(C)V

    return-object v0
.end method

.method public static showCharacter(C)Ljava/lang/String;
    .locals 5

    .prologue
    .line 228550
    const-string v1, "0123456789ABCDEF"

    .line 228551
    const/4 v0, 0x6

    new-array v2, v0, [C

    fill-array-data v2, :array_0

    .line 228552
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 228553
    rsub-int/lit8 v3, v0, 0x5

    and-int/lit8 v4, p0, 0xf

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v2, v3

    .line 228554
    shr-int/lit8 v3, p0, 0x4

    int-to-char p0, v3

    .line 228555
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228556
    :cond_0
    invoke-static {v2}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 2
        0x5cs
        0x75s
        0x0s
        0x0s
        0x0s
        0x0s
    .end array-data
.end method


# virtual methods
.method public apply(Ljava/lang/Character;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228548
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, LX/1IA;->matches(C)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228549
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p0, p1}, LX/1IA;->apply(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public indexIn(Ljava/lang/CharSequence;)I
    .locals 1

    .prologue
    .line 228503
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/1IA;->indexIn(Ljava/lang/CharSequence;I)I

    move-result v0

    return v0
.end method

.method public indexIn(Ljava/lang/CharSequence;I)I
    .locals 3

    .prologue
    .line 228504
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 228505
    invoke-static {p2, v1}, LX/0PB;->checkPositionIndex(II)I

    move v0, p2

    .line 228506
    :goto_0
    if-ge v0, v1, :cond_1

    .line 228507
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LX/1IA;->matches(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228508
    :goto_1
    return v0

    .line 228509
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228510
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public abstract matches(C)Z
.end method

.method public matchesAllOf(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 228497
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 228498
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LX/1IA;->matches(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 228499
    const/4 v0, 0x0

    .line 228500
    :goto_1
    return v0

    .line 228501
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 228502
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public or(LX/1IA;)LX/1IA;
    .locals 1

    .prologue
    .line 228496
    new-instance v0, LX/4vt;

    invoke-direct {v0, p0, p1}, LX/4vt;-><init>(LX/1IA;LX/1IA;)V

    return-object v0
.end method

.method public removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 228484
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228485
    invoke-virtual {p0, v0}, LX/1IA;->indexIn(Ljava/lang/CharSequence;)I

    move-result v1

    .line 228486
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 228487
    :goto_0
    return-object v0

    .line 228488
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 228489
    const/4 v0, 0x1

    .line 228490
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 228491
    array-length v2, v3

    if-eq v1, v2, :cond_2

    .line 228492
    aget-char v2, v3, v1

    invoke-virtual {p0, v2}, LX/1IA;->matches(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 228493
    sub-int v2, v1, v0

    aget-char v4, v3, v1

    aput-char v4, v3, v2

    goto :goto_1

    .line 228494
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 228495
    :cond_2
    new-instance v2, Ljava/lang/String;

    const/4 v4, 0x0

    sub-int v0, v1, v0

    invoke-direct {v2, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    move-object v0, v2

    goto :goto_0
.end method

.method public trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 228476
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 228477
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 228478
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, LX/1IA;->matches(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228479
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228480
    :cond_0
    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-le v0, v1, :cond_1

    .line 228481
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LX/1IA;->matches(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228482
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 228483
    :cond_1
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 228469
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 228470
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 228471
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LX/1IA;->matches(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 228472
    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228473
    :goto_1
    return-object v0

    .line 228474
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228475
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public trimTrailingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 228462
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 228463
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 228464
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LX/1IA;->matches(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 228465
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228466
    :goto_1
    return-object v0

    .line 228467
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 228468
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
