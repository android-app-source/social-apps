.class public LX/0cn;
.super Ljava/util/AbstractMap;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;",
        "Ljava/util/concurrent/ConcurrentMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final e:LX/0cp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cp",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Ljava/util/logging/Logger;


# instance fields
.field public final transient a:I

.field public final transient b:I

.field public final transient c:[LX/0d3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0d3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final concurrencyLevel:I

.field public final transient d:LX/0cs;

.field public final expireAfterAccessNanos:J

.field public final expireAfterWriteNanos:J

.field public transient g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field public transient h:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field public transient i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final keyEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final keyStrength:LX/0ci;

.field public final maximumSize:I

.field public final removalListener:LX/0d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0d2",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final removalNotificationQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/4zM",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final ticker:LX/0QV;

.field public final valueEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final valueStrength:LX/0ci;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89330
    const-class v0, LX/0cn;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LX/0cn;->j:Ljava/util/logging/Logger;

    .line 89331
    new-instance v0, LX/0co;

    invoke-direct {v0}, LX/0co;-><init>()V

    sput-object v0, LX/0cn;->e:LX/0cp;

    .line 89332
    new-instance v0, LX/0cq;

    invoke-direct {v0}, LX/0cq;-><init>()V

    sput-object v0, LX/0cn;->f:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(LX/0S8;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 89333
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 89334
    invoke-virtual {p1}, LX/0S8;->d()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/0cn;->concurrencyLevel:I

    .line 89335
    invoke-virtual {p1}, LX/0S8;->f()LX/0ci;

    move-result-object v0

    iput-object v0, p0, LX/0cn;->keyStrength:LX/0ci;

    .line 89336
    iget-object v0, p1, LX/0S8;->g:LX/0ci;

    sget-object v1, LX/0ci;->STRONG:LX/0ci;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ci;

    move-object v0, v0

    .line 89337
    iput-object v0, p0, LX/0cn;->valueStrength:LX/0ci;

    .line 89338
    iget-object v0, p1, LX/0S8;->k:LX/0Qj;

    invoke-virtual {p1}, LX/0S8;->f()LX/0ci;

    move-result-object v1

    invoke-virtual {v1}, LX/0ci;->defaultEquivalence()LX/0Qj;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Qj;

    move-object v0, v0

    .line 89339
    iput-object v0, p0, LX/0cn;->keyEquivalence:LX/0Qj;

    .line 89340
    iget-object v0, p0, LX/0cn;->valueStrength:LX/0ci;

    invoke-virtual {v0}, LX/0ci;->defaultEquivalence()LX/0Qj;

    move-result-object v0

    iput-object v0, p0, LX/0cn;->valueEquivalence:LX/0Qj;

    .line 89341
    iget v0, p1, LX/0S8;->e:I

    iput v0, p0, LX/0cn;->maximumSize:I

    .line 89342
    iget-wide v7, p1, LX/0S8;->i:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_9

    const-wide/16 v7, 0x0

    :goto_0
    move-wide v0, v7

    .line 89343
    iput-wide v0, p0, LX/0cn;->expireAfterAccessNanos:J

    .line 89344
    iget-wide v7, p1, LX/0S8;->h:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_a

    const-wide/16 v7, 0x0

    :goto_1
    move-wide v0, v7

    .line 89345
    iput-wide v0, p0, LX/0cn;->expireAfterWriteNanos:J

    .line 89346
    iget-object v0, p0, LX/0cn;->keyStrength:LX/0ci;

    invoke-virtual {p0}, LX/0cn;->b()Z

    move-result v1

    invoke-virtual {p0}, LX/0cn;->a()Z

    move-result v3

    invoke-static {v0, v1, v3}, LX/0cs;->getFactory(LX/0ci;ZZ)LX/0cs;

    move-result-object v0

    iput-object v0, p0, LX/0cn;->d:LX/0cs;

    .line 89347
    iget-object v0, p1, LX/0S8;->l:LX/0QV;

    .line 89348
    sget-object v1, LX/0QV;->SYSTEM_TICKER:LX/0QV;

    move-object v1, v1

    .line 89349
    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QV;

    move-object v0, v0

    .line 89350
    iput-object v0, p0, LX/0cn;->ticker:LX/0QV;

    .line 89351
    invoke-virtual {p1}, LX/0S9;->a()LX/0d2;

    move-result-object v0

    iput-object v0, p0, LX/0cn;->removalListener:LX/0d2;

    .line 89352
    iget-object v0, p0, LX/0cn;->removalListener:LX/0d2;

    sget-object v1, LX/0d1;->INSTANCE:LX/0d1;

    if-ne v0, v1, :cond_2

    .line 89353
    sget-object v0, LX/0cn;->f:Ljava/util/Queue;

    move-object v0, v0

    .line 89354
    :goto_2
    iput-object v0, p0, LX/0cn;->removalNotificationQueue:Ljava/util/Queue;

    .line 89355
    invoke-virtual {p1}, LX/0S8;->c()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 89356
    invoke-virtual {p0}, LX/0cn;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89357
    iget v1, p0, LX/0cn;->maximumSize:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    move v1, v2

    move v3, v4

    .line 89358
    :goto_3
    iget v5, p0, LX/0cn;->concurrencyLevel:I

    if-ge v1, v5, :cond_3

    invoke-virtual {p0}, LX/0cn;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    mul-int/lit8 v5, v1, 0x2

    iget v6, p0, LX/0cn;->maximumSize:I

    if-gt v5, v6, :cond_3

    .line 89359
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 89360
    shl-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 89361
    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_2

    .line 89362
    :cond_3
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, LX/0cn;->b:I

    .line 89363
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, LX/0cn;->a:I

    .line 89364
    new-array v3, v1, [LX/0d3;

    move-object v3, v3

    .line 89365
    iput-object v3, p0, LX/0cn;->c:[LX/0d3;

    .line 89366
    div-int v3, v0, v1

    .line 89367
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_8

    .line 89368
    add-int/lit8 v0, v3, 0x1

    .line 89369
    :goto_4
    if-ge v2, v0, :cond_4

    .line 89370
    shl-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 89371
    :cond_4
    invoke-virtual {p0}, LX/0cn;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 89372
    iget v0, p0, LX/0cn;->maximumSize:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 89373
    iget v3, p0, LX/0cn;->maximumSize:I

    rem-int v1, v3, v1

    .line 89374
    :goto_5
    iget-object v3, p0, LX/0cn;->c:[LX/0d3;

    array-length v3, v3

    if-ge v4, v3, :cond_7

    .line 89375
    if-ne v4, v1, :cond_5

    .line 89376
    add-int/lit8 v0, v0, -0x1

    .line 89377
    :cond_5
    iget-object v3, p0, LX/0cn;->c:[LX/0d3;

    invoke-direct {p0, v2, v0}, LX/0cn;->a(II)LX/0d3;

    move-result-object v5

    aput-object v5, v3, v4

    .line 89378
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 89379
    :cond_6
    :goto_6
    iget-object v0, p0, LX/0cn;->c:[LX/0d3;

    array-length v0, v0

    if-ge v4, v0, :cond_7

    .line 89380
    iget-object v0, p0, LX/0cn;->c:[LX/0d3;

    const/4 v1, -0x1

    invoke-direct {p0, v2, v1}, LX/0cn;->a(II)LX/0d3;

    move-result-object v1

    aput-object v1, v0, v4

    .line 89381
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 89382
    :cond_7
    return-void

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    iget-wide v7, p1, LX/0S8;->i:J

    goto/16 :goto_0

    :cond_a
    iget-wide v7, p1, LX/0S8;->h:J

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 89383
    iget-object v0, p0, LX/0cn;->keyEquivalence:LX/0Qj;

    invoke-virtual {v0, p1}, LX/0Qj;->hash(Ljava/lang/Object;)I

    move-result v0

    .line 89384
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v1, v0

    .line 89385
    ushr-int/lit8 p0, v1, 0xa

    xor-int/2addr v1, p0

    .line 89386
    shl-int/lit8 p0, v1, 0x3

    add-int/2addr v1, p0

    .line 89387
    ushr-int/lit8 p0, v1, 0x6

    xor-int/2addr v1, p0

    .line 89388
    shl-int/lit8 p0, v1, 0x2

    shl-int/lit8 p1, v1, 0xe

    add-int/2addr p0, p1

    add-int/2addr v1, p0

    .line 89389
    ushr-int/lit8 p0, v1, 0x10

    xor-int/2addr v1, p0

    move v0, v1

    .line 89390
    return v0
.end method

.method private a(II)LX/0d3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0d3",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 89391
    new-instance v0, LX/0d3;

    invoke-direct {v0, p0, p1, p2}, LX/0d3;-><init>(LX/0cn;II)V

    return-object v0
.end method

.method public static a(LX/0qF;LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89392
    invoke-interface {p0, p1}, LX/0qF;->setNextExpirable(LX/0qF;)V

    .line 89393
    invoke-interface {p1, p0}, LX/0qF;->setPreviousExpirable(LX/0qF;)V

    .line 89394
    return-void
.end method

.method public static a(LX/0qF;J)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;J)Z"
        }
    .end annotation

    .prologue
    .line 89395
    invoke-interface {p0}, LX/0qF;->getExpirationTime()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0cn;I)LX/0d3;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0d3",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 89396
    iget-object v0, p0, LX/0cn;->c:[LX/0d3;

    iget v1, p0, LX/0cn;->b:I

    ushr-int v1, p1, v1

    iget v2, p0, LX/0cn;->a:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static b(Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TE;>;)",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 89397
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 89398
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 89399
    return-object v0
.end method

.method public static b(LX/0qF;LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89400
    invoke-interface {p0, p1}, LX/0qF;->setNextEvictable(LX/0qF;)V

    .line 89401
    invoke-interface {p1, p0}, LX/0qF;->setPreviousEvictable(LX/0qF;)V

    .line 89402
    return-void
.end method

.method public static d(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89403
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 89404
    invoke-interface {p0, v0}, LX/0qF;->setNextExpirable(LX/0qF;)V

    .line 89405
    invoke-interface {p0, v0}, LX/0qF;->setPreviousExpirable(LX/0qF;)V

    .line 89406
    return-void
.end method

.method public static e(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89407
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 89408
    invoke-interface {p0, v0}, LX/0qF;->setNextEvictable(LX/0qF;)V

    .line 89409
    invoke-interface {p0, v0}, LX/0qF;->setPreviousEvictable(LX/0qF;)V

    .line 89410
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 89411
    iget v0, p0, LX/0cn;->maximumSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    .line 89412
    iget-wide v1, p0, LX/0cn;->expireAfterWriteNanos:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 89413
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0cn;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 89414
    iget-wide v0, p0, LX/0cn;->expireAfterAccessNanos:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/0qF;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 89415
    iget-object v0, p0, LX/0cn;->ticker:LX/0QV;

    invoke-virtual {v0}, LX/0QV;->read()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LX/0cn;->a(LX/0qF;J)Z

    move-result v0

    return v0
.end method

.method public final clear()V
    .locals 4

    .prologue
    .line 89305
    iget-object v1, p0, LX/0cn;->c:[LX/0d3;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 89306
    invoke-virtual {v3}, LX/0d3;->a()V

    .line 89307
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89308
    :cond_0
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89235
    if-nez p1, :cond_0

    .line 89236
    const/4 v0, 0x0

    .line 89237
    :goto_0
    return v0

    .line 89238
    :cond_0
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89239
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0d3;->b(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89309
    if-nez p1, :cond_0

    .line 89310
    const/4 v0, 0x0

    .line 89311
    :goto_0
    return v0

    .line 89312
    :cond_0
    iget-object v7, p0, LX/0cn;->c:[LX/0d3;

    .line 89313
    const-wide/16 v4, -0x1

    .line 89314
    const/4 v0, 0x0

    move v6, v0

    move-wide v8, v4

    :goto_1
    const/4 v0, 0x3

    if-ge v6, v0, :cond_5

    .line 89315
    const-wide/16 v2, 0x0

    .line 89316
    array-length v10, v7

    const/4 v0, 0x0

    move-wide v4, v2

    move v2, v0

    :goto_2
    if-ge v2, v10, :cond_4

    aget-object v3, v7, v2

    .line 89317
    iget-object v11, v3, LX/0d3;->table:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 89318
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 89319
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qF;

    :goto_4
    if-eqz v0, :cond_2

    .line 89320
    invoke-virtual {v3, v0}, LX/0d3;->a(LX/0qF;)Ljava/lang/Object;

    move-result-object v12

    .line 89321
    if-eqz v12, :cond_1

    iget-object v13, p0, LX/0cn;->valueEquivalence:LX/0Qj;

    invoke-virtual {v13, p1, v12}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 89322
    const/4 v0, 0x1

    goto :goto_0

    .line 89323
    :cond_1
    invoke-interface {v0}, LX/0qF;->getNext()LX/0qF;

    move-result-object v0

    goto :goto_4

    .line 89324
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 89325
    :cond_3
    iget v0, v3, LX/0d3;->modCount:I

    int-to-long v0, v0

    add-long/2addr v4, v0

    .line 89326
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 89327
    :cond_4
    cmp-long v0, v4, v8

    if-eqz v0, :cond_5

    .line 89328
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 89329
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 89240
    iget-object v0, p0, LX/0cn;->keyStrength:LX/0ci;

    sget-object v1, LX/0ci;->STRONG:LX/0ci;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 89241
    iget-object v0, p0, LX/0cn;->valueStrength:LX/0ci;

    sget-object v1, LX/0ci;->STRONG:LX/0ci;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 89242
    iget-object v0, p0, LX/0cn;->i:Ljava/util/Set;

    .line 89243
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4zQ;

    invoke-direct {v0, p0}, LX/4zQ;-><init>(LX/0cn;)V

    iput-object v0, p0, LX/0cn;->i:Ljava/util/Set;

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 89244
    if-nez p1, :cond_0

    .line 89245
    const/4 v0, 0x0

    .line 89246
    :goto_0
    return-object v0

    .line 89247
    :cond_0
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89248
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0d3;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 89249
    iget-object v6, p0, LX/0cn;->c:[LX/0d3;

    move v0, v1

    move-wide v2, v4

    .line 89250
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 89251
    aget-object v7, v6, v0

    iget v7, v7, LX/0d3;->count:I

    if-eqz v7, :cond_1

    .line 89252
    :cond_0
    :goto_1
    return v1

    .line 89253
    :cond_1
    aget-object v7, v6, v0

    iget v7, v7, LX/0d3;->modCount:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 89254
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89255
    :cond_2
    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    move v0, v1

    .line 89256
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 89257
    aget-object v7, v6, v0

    iget v7, v7, LX/0d3;->count:I

    if-nez v7, :cond_0

    .line 89258
    aget-object v7, v6, v0

    iget v7, v7, LX/0d3;->modCount:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 89259
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 89260
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 89261
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 89262
    iget-object v0, p0, LX/0cn;->g:Ljava/util/Set;

    .line 89263
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2GE;

    invoke-direct {v0, p0}, LX/2GE;-><init>(LX/0cn;)V

    iput-object v0, p0, LX/0cn;->g:Ljava/util/Set;

    goto :goto_0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 89264
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89265
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89266
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89267
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, LX/0d3;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 89268
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89269
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LX/0cn;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 89270
    :cond_0
    return-void
.end method

.method public final putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 89271
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89272
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89273
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89274
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, LX/0d3;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 89275
    if-nez p1, :cond_0

    .line 89276
    const/4 v0, 0x0

    .line 89277
    :goto_0
    return-object v0

    .line 89278
    :cond_0
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89279
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, LX/0d3;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89280
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 89281
    :cond_0
    const/4 v0, 0x0

    .line 89282
    :goto_0
    return v0

    .line 89283
    :cond_1
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89284
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, LX/0d3;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 89285
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89286
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89287
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89288
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, LX/0d3;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;TV;)Z"
        }
    .end annotation

    .prologue
    .line 89289
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89290
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89291
    if-nez p2, :cond_0

    .line 89292
    const/4 v0, 0x0

    .line 89293
    :goto_0
    return v0

    .line 89294
    :cond_0
    invoke-direct {p0, p1}, LX/0cn;->a(Ljava/lang/Object;)I

    move-result v0

    .line 89295
    invoke-static {p0, v0}, LX/0cn;->b(LX/0cn;I)LX/0d3;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, LX/0d3;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final size()I
    .locals 6

    .prologue
    .line 89296
    iget-object v1, p0, LX/0cn;->c:[LX/0d3;

    .line 89297
    const-wide/16 v2, 0x0

    .line 89298
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 89299
    aget-object v4, v1, v0

    iget v4, v4, LX/0d3;->count:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 89300
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89301
    :cond_0
    invoke-static {v2, v3}, LX/0a4;->b(J)I

    move-result v0

    return v0
.end method

.method public final values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 89302
    iget-object v0, p0, LX/0cn;->h:Ljava/util/Collection;

    .line 89303
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4zf;

    invoke-direct {v0, p0}, LX/4zf;-><init>(LX/0cn;)V

    iput-object v0, p0, LX/0cn;->h:Ljava/util/Collection;

    goto :goto_0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 89304
    new-instance v1, LX/4zY;

    iget-object v2, p0, LX/0cn;->keyStrength:LX/0ci;

    iget-object v3, p0, LX/0cn;->valueStrength:LX/0ci;

    iget-object v4, p0, LX/0cn;->keyEquivalence:LX/0Qj;

    iget-object v5, p0, LX/0cn;->valueEquivalence:LX/0Qj;

    iget-wide v6, p0, LX/0cn;->expireAfterWriteNanos:J

    iget-wide v8, p0, LX/0cn;->expireAfterAccessNanos:J

    iget v10, p0, LX/0cn;->maximumSize:I

    iget v11, p0, LX/0cn;->concurrencyLevel:I

    iget-object v12, p0, LX/0cn;->removalListener:LX/0d2;

    move-object v13, p0

    invoke-direct/range {v1 .. v13}, LX/4zY;-><init>(LX/0ci;LX/0ci;LX/0Qj;LX/0Qj;JJIILX/0d2;Ljava/util/concurrent/ConcurrentMap;)V

    return-object v1
.end method
