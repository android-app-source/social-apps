.class public final LX/1IU;
.super LX/1I8;
.source ""


# instance fields
.field public final a:[C


# direct methods
.method private constructor <init>(LX/1I9;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 228724
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1I8;-><init>(LX/1I9;Ljava/lang/Character;)V

    .line 228725
    const/16 v0, 0x200

    new-array v0, v0, [C

    iput-object v0, p0, LX/1IU;->a:[C

    .line 228726
    iget-object v0, p1, LX/1I9;->f:[C

    array-length v0, v0

    const/16 v2, 0x10

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 228727
    :goto_1
    const/16 v0, 0x100

    if-ge v1, v0, :cond_1

    .line 228728
    iget-object v0, p0, LX/1IU;->a:[C

    ushr-int/lit8 v2, v1, 0x4

    invoke-virtual {p1, v2}, LX/1I9;->a(I)C

    move-result v2

    aput-char v2, v0, v1

    .line 228729
    iget-object v0, p0, LX/1IU;->a:[C

    or-int/lit16 v2, v1, 0x100

    and-int/lit8 v3, v1, 0xf

    invoke-virtual {p1, v3}, LX/1I9;->a(I)C

    move-result v3

    aput-char v3, v0, v2

    .line 228730
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 228731
    goto :goto_0

    .line 228732
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 228742
    new-instance v0, LX/1I9;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/1I9;-><init>(Ljava/lang/String;[C)V

    invoke-direct {p0, v0}, LX/1IU;-><init>(LX/1I9;)V

    .line 228743
    return-void
.end method


# virtual methods
.method public final a([BLjava/lang/CharSequence;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 228744
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228745
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 228746
    new-instance v0, LX/51z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid input length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/51z;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v0

    .line 228747
    :goto_0
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 228748
    iget-object v2, p0, LX/1I8;->b:LX/1I9;

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, LX/1I9;->a(C)I

    move-result v2

    shl-int/lit8 v2, v2, 0x4

    iget-object v3, p0, LX/1I8;->b:LX/1I9;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {p2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, LX/1I9;->a(C)I

    move-result v3

    or-int/2addr v3, v2

    .line 228749
    add-int/lit8 v2, v1, 0x1

    int-to-byte v3, v3

    aput-byte v3, p1, v1

    .line 228750
    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_0

    .line 228751
    :cond_1
    return v1
.end method

.method public final a(LX/1I9;Ljava/lang/Character;)LX/1I6;
    .locals 1
    .param p2    # Ljava/lang/Character;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228741
    new-instance v0, LX/1IU;

    invoke-direct {v0, p1}, LX/1IU;-><init>(LX/1I9;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Appendable;[BII)V
    .locals 3

    .prologue
    .line 228733
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228734
    add-int v0, p3, p4

    array-length v1, p2

    invoke-static {p3, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    .line 228735
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p4, :cond_0

    .line 228736
    add-int v1, p3, v0

    aget-byte v1, p2, v1

    and-int/lit16 v1, v1, 0xff

    .line 228737
    iget-object v2, p0, LX/1IU;->a:[C

    aget-char v2, v2, v1

    invoke-interface {p1, v2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228738
    iget-object v2, p0, LX/1IU;->a:[C

    or-int/lit16 v1, v1, 0x100

    aget-char v1, v2, v1

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 228739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228740
    :cond_0
    return-void
.end method
