.class public LX/1My;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1NI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4VZ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:LX/0t5;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:LX/1N9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1N9",
            "<",
            "LX/4KY;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1Mz;

.field private final h:LX/0tc;

.field private final i:LX/0t2;

.field private final j:LX/03V;

.field private final k:LX/0sT;

.field private final l:LX/1N8;

.field public volatile m:Z


# direct methods
.method public constructor <init>(LX/0tX;LX/1Mz;LX/0t5;LX/0tc;Ljava/util/concurrent/Executor;LX/03V;LX/0t2;LX/0sT;LX/1N8;)V
    .locals 1
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 236547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236548
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1My;->a:Ljava/util/Map;

    .line 236549
    new-instance v0, LX/1N9;

    invoke-direct {v0}, LX/1N9;-><init>()V

    iput-object v0, p0, LX/1My;->f:LX/1N9;

    .line 236550
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1My;->b:Ljava/util/Map;

    .line 236551
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1My;->m:Z

    .line 236552
    iput-object p3, p0, LX/1My;->d:LX/0t5;

    .line 236553
    iput-object p1, p0, LX/1My;->c:LX/0tX;

    .line 236554
    iput-object p2, p0, LX/1My;->g:LX/1Mz;

    .line 236555
    iput-object p5, p0, LX/1My;->e:Ljava/util/concurrent/Executor;

    .line 236556
    iput-object p4, p0, LX/1My;->h:LX/0tc;

    .line 236557
    iput-object p7, p0, LX/1My;->i:LX/0t2;

    .line 236558
    iput-object p6, p0, LX/1My;->j:LX/03V;

    .line 236559
    iput-object p8, p0, LX/1My;->k:LX/0sT;

    .line 236560
    iput-object p9, p0, LX/1My;->l:LX/1N8;

    .line 236561
    iget-object v0, p0, LX/1My;->d:LX/0t5;

    invoke-virtual {v0, p0}, LX/0t5;->a(LX/1My;)V

    .line 236562
    return-void
.end method

.method public static a(LX/0QB;)LX/1My;
    .locals 1

    .prologue
    .line 236546
    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1NI;)V
    .locals 2

    .prologue
    .line 236544
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    iget-object v1, p1, LX/1NI;->d:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236545
    return-void
.end method

.method public static b(LX/0QB;)LX/1My;
    .locals 10

    .prologue
    .line 236542
    new-instance v0, LX/1My;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Mz;->a(LX/0QB;)LX/1Mz;

    move-result-object v2

    check-cast v2, LX/1Mz;

    invoke-static {p0}, LX/0t5;->a(LX/0QB;)LX/0t5;

    move-result-object v3

    check-cast v3, LX/0t5;

    invoke-static {p0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v4

    check-cast v4, LX/0tc;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0t2;->b(LX/0QB;)LX/0t2;

    move-result-object v7

    check-cast v7, LX/0t2;

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v8

    check-cast v8, LX/0sT;

    invoke-static {p0}, LX/1N8;->a(LX/0QB;)LX/1N8;

    move-result-object v9

    check-cast v9, LX/1N8;

    invoke-direct/range {v0 .. v9}, LX/1My;-><init>(LX/0tX;LX/1Mz;LX/0t5;LX/0tc;Ljava/util/concurrent/Executor;LX/03V;LX/0t2;LX/0sT;LX/1N8;)V

    .line 236543
    return-object v0
.end method


# virtual methods
.method public final a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/0ta;",
            "J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 236539
    iget-object v0, p0, LX/1My;->h:LX/0tc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0tc;->b(Z)LX/1NA;

    move-result-object v7

    .line 236540
    iget-object v0, v7, LX/1NB;->f:LX/1ND;

    invoke-interface {v0, p5}, LX/1ND;->a(Ljava/util/Set;)LX/1ND;

    .line 236541
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;LX/1NB;Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 236529
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236530
    iget-object v0, p1, LX/0zO;->a:LX/0zS;

    move-object v0, v0

    .line 236531
    iget-boolean v0, v0, LX/0zS;->h:Z

    const-string v1, "fetchAndSubscribe() requires disk caching to work"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 236532
    new-instance v0, LX/1NI;

    iget-object v1, p0, LX/1My;->c:LX/0tX;

    iget-object v2, p0, LX/1My;->j:LX/03V;

    iget-object v4, p0, LX/1My;->e:Ljava/util/concurrent/Executor;

    iget-object v7, p0, LX/1My;->i:LX/0t2;

    move-object v3, p2

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, LX/1NI;-><init>(LX/0tX;LX/03V;LX/0TF;Ljava/util/concurrent/Executor;Ljava/lang/String;LX/0zO;LX/0t2;)V

    .line 236533
    invoke-virtual {v0}, LX/1NI;->e()V

    .line 236534
    iput-object v0, p1, LX/0zO;->j:LX/1NI;

    .line 236535
    const/4 v1, 0x1

    .line 236536
    iput-boolean v1, p1, LX/0zO;->p:Z

    .line 236537
    invoke-direct {p0, v0}, LX/1My;->a(LX/1NI;)V

    .line 236538
    iget-object v0, p0, LX/1My;->c:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 236525
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1My;->m:Z

    .line 236526
    iget-object v0, p0, LX/1My;->d:LX/0t5;

    invoke-virtual {v0, p0}, LX/0t5;->b(LX/1My;)V

    .line 236527
    invoke-virtual {p0}, LX/1My;->b()V

    .line 236528
    return-void
.end method

.method public final a(ILjava/util/Set;I)V
    .locals 5
    .param p3    # I
        .annotation build Lcom/facebook/graphql/executor/iface/GraphQLObservablePusher$SubscriptionStore;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 236509
    iget-object v1, p0, LX/1My;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236510
    :try_start_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 236511
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236512
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NI;

    .line 236513
    invoke-virtual {v0}, LX/1NI;->f()I

    move-result v1

    and-int/2addr v1, p3

    if-eqz v1, :cond_0

    .line 236514
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    iget-object v2, v0, LX/1NI;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    move-object v1, p2

    .line 236515
    :goto_1
    iget-object v2, v0, LX/1NI;->c:Ljava/util/Set;

    if-ne v1, v2, :cond_5

    move-object v2, p2

    .line 236516
    :goto_2
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 236517
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236518
    iget v1, v0, LX/1NI;->j:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget v1, v0, LX/1NI;->j:I

    if-eq p1, v1, :cond_3

    .line 236519
    :cond_2
    invoke-static {v0}, LX/1NI;->g(LX/1NI;)V

    .line 236520
    :cond_3
    goto :goto_0

    .line 236521
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 236522
    :cond_4
    iget-object v1, v0, LX/1NI;->c:Ljava/util/Set;

    goto :goto_1

    .line 236523
    :cond_5
    iget-object v2, v0, LX/1NI;->c:Ljava/util/Set;

    goto :goto_2

    .line 236524
    :cond_6
    return-void
.end method

.method public final a(LX/0Pz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236563
    iget-object v1, p0, LX/1My;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236564
    :try_start_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 236565
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236566
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NI;

    .line 236567
    iget-object v2, v0, LX/1NI;->h:LX/0zO;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/1NI;->h:LX/0zO;

    iget-object p0, v0, LX/1NI;->i:LX/0t2;

    invoke-virtual {v2, p0}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v0, v2

    .line 236568
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 236569
    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 236570
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 236571
    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 236489
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236490
    iget-object v0, p0, LX/1My;->k:LX/0sT;

    .line 236491
    iget-object v1, v0, LX/0sT;->a:LX/0ad;

    sget-short v2, LX/1NG;->i:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 236492
    if-eqz v0, :cond_1

    .line 236493
    iget-object v0, p3, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 236494
    instance-of v0, v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-nez v0, :cond_0

    .line 236495
    :goto_0
    return-void

    .line 236496
    :cond_0
    iget-object v1, p0, LX/1My;->l:LX/1N8;

    invoke-virtual {p3}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    new-instance v3, LX/4Un;

    invoke-direct {v3, p0, p3, p1}, LX/4Un;-><init>(LX/1My;Lcom/facebook/graphql/executor/GraphQLResult;LX/0TF;)V

    .line 236497
    iget-object v0, p3, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 236498
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 236499
    new-instance v6, LX/4KY;

    invoke-static {v0}, LX/3DS;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v10

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    invoke-direct/range {v6 .. v10}, LX/4KY;-><init>(LX/1N8;Ljava/util/Collection;LX/4Un;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    move-object v0, v6

    .line 236500
    iget-object v1, p0, LX/1My;->f:LX/1N9;

    invoke-virtual {v1, p2, v0}, LX/1N9;->a(Ljava/lang/String;Ljava/io/Closeable;)V

    goto :goto_0

    .line 236501
    :cond_1
    iget-object v0, p3, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    if-nez v0, :cond_2

    .line 236502
    iget-object v0, p0, LX/1My;->h:LX/0tc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0tc;->b(Z)LX/1NA;

    move-result-object v0

    .line 236503
    iget-object v1, v0, LX/1NB;->f:LX/1ND;

    invoke-virtual {p3}, Lcom/facebook/graphql/executor/GraphQLResult;->e()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ND;->a(Ljava/util/Set;)LX/1ND;

    .line 236504
    invoke-static {p3}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 236505
    iput-object v0, v1, LX/1lO;->d:LX/1NB;

    .line 236506
    move-object v0, v1

    .line 236507
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v5

    .line 236508
    :goto_1
    new-instance v0, LX/1NH;

    iget-object v2, p0, LX/1My;->e:Ljava/util/concurrent/Executor;

    iget-object v3, p0, LX/1My;->j:LX/03V;

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/1NH;-><init>(LX/0TF;Ljava/util/concurrent/Executor;LX/03V;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    invoke-direct {p0, v0}, LX/1My;->a(LX/1NI;)V

    goto :goto_0

    :cond_2
    move-object v5, p3

    goto :goto_1
.end method

.method public final a(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236480
    iget-object v1, p0, LX/1My;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236481
    :try_start_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 236482
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236483
    iget-object v0, p0, LX/1My;->f:LX/1N9;

    invoke-virtual {v0, p1}, LX/1N9;->a(Ljava/util/Set;)V

    .line 236484
    iget-object v1, p0, LX/1My;->b:Ljava/util/Map;

    monitor-enter v1

    .line 236485
    :try_start_1
    iget-object v0, p0, LX/1My;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 236486
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 236487
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 236488
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 236477
    if-nez p1, :cond_0

    .line 236478
    const/4 v0, 0x0

    .line 236479
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 236473
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 236474
    iget-object v0, p0, LX/1My;->f:LX/1N9;

    invoke-virtual {v0}, LX/1N9;->c()V

    .line 236475
    iget-object v0, p0, LX/1My;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 236476
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 236472
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, LX/1My;->f:LX/1N9;

    invoke-virtual {v1}, LX/1N9;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 236463
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1My;->m:Z

    .line 236464
    iget-object v1, p0, LX/1My;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236465
    :try_start_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NI;

    .line 236466
    invoke-virtual {v0}, LX/1NI;->e()V

    goto :goto_0

    .line 236467
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236468
    iget-object v1, p0, LX/1My;->b:Ljava/util/Map;

    monitor-enter v1

    .line 236469
    :try_start_2
    iget-object v0, p0, LX/1My;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4VZ;

    .line 236470
    invoke-virtual {v0}, LX/4VZ;->c()V

    goto :goto_1

    .line 236471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 236452
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1My;->m:Z

    .line 236453
    iget-object v1, p0, LX/1My;->a:Ljava/util/Map;

    monitor-enter v1

    .line 236454
    :try_start_0
    iget-object v0, p0, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1NI;

    .line 236455
    invoke-virtual {v0}, LX/1NI;->d()V

    goto :goto_0

    .line 236456
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236457
    iget-object v0, p0, LX/1My;->f:LX/1N9;

    invoke-virtual {v0}, LX/1N9;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4KY;

    .line 236458
    invoke-virtual {v0}, LX/4KY;->a()V

    goto :goto_1

    .line 236459
    :cond_1
    iget-object v1, p0, LX/1My;->b:Ljava/util/Map;

    monitor-enter v1

    .line 236460
    :try_start_2
    iget-object v0, p0, LX/1My;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4VZ;

    .line 236461
    invoke-virtual {v0}, LX/4VZ;->b()V

    goto :goto_2

    .line 236462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 236449
    invoke-virtual {p0}, LX/1My;->a()V

    .line 236450
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 236451
    return-void
.end method
