.class public LX/0yV;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/0lC;

.field private final c:LX/0lp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164804
    const-class v0, LX/0yV;

    sput-object v0, LX/0yV;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164806
    iput-object p1, p0, LX/0yV;->b:LX/0lC;

    .line 164807
    iput-object p2, p0, LX/0yV;->c:LX/0lp;

    .line 164808
    return-void
.end method

.method public static a(LX/0lF;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164809
    if-nez p0, :cond_0

    .line 164810
    sget-object v0, LX/0yV;->a:Ljava/lang/Class;

    const-string v1, "StringListDataSerializer trying to deserialize null. Returning empty list instead"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 164811
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 164812
    :goto_0
    return-object v0

    .line 164813
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 164814
    invoke-virtual {p0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v2

    .line 164815
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164816
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 164817
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 164818
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0yV;
    .locals 1

    .prologue
    .line 164819
    invoke-static {p0}, LX/0yV;->b(LX/0QB;)LX/0yV;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0yV;
    .locals 3

    .prologue
    .line 164820
    new-instance v2, LX/0yV;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v1

    check-cast v1, LX/0lp;

    invoke-direct {v2, v0, v1}, LX/0yV;-><init>(LX/0lC;LX/0lp;)V

    .line 164821
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164822
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164823
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 164824
    :goto_0
    return-object v0

    .line 164825
    :cond_0
    iget-object v0, p0, LX/0yV;->c:LX/0lp;

    invoke-virtual {v0, p1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 164826
    iget-object v1, p0, LX/0yV;->b:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 164827
    invoke-static {v0}, LX/0yV;->a(LX/0lF;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0Px;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 164828
    iget-object v0, p0, LX/0yV;->b:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
