.class public LX/0ps;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile I:LX/0ps;

.field public static final a:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final d:LX/0Tn;

.field public static e:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/device/resourcemonitor/MonitoredProcess;",
            "Lcom/facebook/device/resourcemonitor/DataUsageInfo;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public static f:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/device/resourcemonitor/DataUsageInfo;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/0V6;

.field private B:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

.field private C:Lcom/facebook/device/resourcemonitor/DataUsageInfo;

.field private D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/device/resourcemonitor/DataUsageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public E:Ljava/lang/Long;

.field public F:Ljava/lang/Long;

.field private G:Z

.field public H:Z

.field public final g:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/facebook/device/resourcemonitor/MemoryUsageChangedListener;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/facebook/device/resourcemonitor/DiskUsageChangedListener;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/lang/Runtime;

.field public final j:LX/0SG;

.field private final k:LX/0q3;

.field public final l:LX/0pt;

.field private final m:LX/0ka;

.field private final n:LX/0qD;

.field private final o:Landroid/app/ActivityManager;

.field public final p:Landroid/view/WindowManager;

.field public final q:LX/03V;

.field private final r:LX/0V8;

.field private final s:LX/0q5;

.field public final t:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final u:LX/0lp;

.field public final v:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
    .end annotation
.end field

.field public final w:LX/0Xl;

.field public x:LX/0Yb;

.field public final y:LX/0YZ;

.field public final z:LX/0YZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 146653
    const-class v0, LX/0ps;

    sput-object v0, LX/0ps;->c:Ljava/lang/Class;

    .line 146654
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "res_man/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 146655
    sput-object v0, LX/0ps;->d:LX/0Tn;

    const-string v1, "data_usage"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0ps;->a:LX/0Tn;

    .line 146656
    sget-object v0, LX/0ps;->d:LX/0Tn;

    const-string v1, "data_usage_v2"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0ps;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0pt;LX/0q3;Ljava/lang/Runtime;LX/0SG;LX/0ka;Landroid/view/WindowManager;Landroid/app/ActivityManager;LX/03V;LX/0V8;LX/0q5;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lp;LX/0Xl;Landroid/os/Handler;)V
    .locals 3
    .param p13    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p14    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 146669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146670
    iput-object p1, p0, LX/0ps;->l:LX/0pt;

    .line 146671
    iput-object p2, p0, LX/0ps;->k:LX/0q3;

    .line 146672
    iput-object p7, p0, LX/0ps;->o:Landroid/app/ActivityManager;

    .line 146673
    iput-object p3, p0, LX/0ps;->i:Ljava/lang/Runtime;

    .line 146674
    iput-object p4, p0, LX/0ps;->j:LX/0SG;

    .line 146675
    iput-object p5, p0, LX/0ps;->m:LX/0ka;

    .line 146676
    iput-object p6, p0, LX/0ps;->p:Landroid/view/WindowManager;

    .line 146677
    iput-object p10, p0, LX/0ps;->s:LX/0q5;

    .line 146678
    iput-object p11, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 146679
    iput-object p12, p0, LX/0ps;->u:LX/0lp;

    .line 146680
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0ps;->w:LX/0Xl;

    .line 146681
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0ps;->v:Landroid/os/Handler;

    .line 146682
    new-instance v1, LX/0S8;

    invoke-direct {v1}, LX/0S8;-><init>()V

    invoke-virtual {v1}, LX/0S8;->e()LX/0S8;

    move-result-object v1

    invoke-virtual {v1}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, LX/0ps;->g:Ljava/util/concurrent/ConcurrentMap;

    .line 146683
    new-instance v1, LX/0S8;

    invoke-direct {v1}, LX/0S8;-><init>()V

    invoke-virtual {v1}, LX/0S8;->e()LX/0S8;

    move-result-object v1

    invoke-virtual {v1}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iput-object v1, p0, LX/0ps;->h:Ljava/util/concurrent/ConcurrentMap;

    .line 146684
    sget-object v1, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->MY_APP:Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    iget v1, v1, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->uid:I

    invoke-static {p0, v1}, LX/0ps;->a(LX/0ps;I)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v1

    iput-object v1, p0, LX/0ps;->B:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    .line 146685
    invoke-static {}, LX/0ps;->l()Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    move-result-object v1

    iput-object v1, p0, LX/0ps;->C:Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    .line 146686
    iput-object p8, p0, LX/0ps;->q:LX/03V;

    .line 146687
    iput-object p9, p0, LX/0ps;->r:LX/0V8;

    .line 146688
    new-instance v1, LX/0qC;

    invoke-direct {v1, p0}, LX/0qC;-><init>(LX/0ps;)V

    iput-object v1, p0, LX/0ps;->n:LX/0qD;

    .line 146689
    iget-object v1, p0, LX/0ps;->m:LX/0ka;

    iget-object v2, p0, LX/0ps;->n:LX/0qD;

    invoke-virtual {v1, v2}, LX/0ka;->a(LX/0qD;)V

    .line 146690
    new-instance v1, LX/0qH;

    invoke-direct {v1, p0}, LX/0qH;-><init>(LX/0ps;)V

    iput-object v1, p0, LX/0ps;->y:LX/0YZ;

    .line 146691
    new-instance v1, LX/0qI;

    invoke-direct {v1, p0}, LX/0qI;-><init>(LX/0ps;)V

    iput-object v1, p0, LX/0ps;->z:LX/0YZ;

    .line 146692
    return-void
.end method

.method public static a(LX/0QB;)LX/0ps;
    .locals 3

    .prologue
    .line 146693
    sget-object v0, LX/0ps;->I:LX/0ps;

    if-nez v0, :cond_1

    .line 146694
    const-class v1, LX/0ps;

    monitor-enter v1

    .line 146695
    :try_start_0
    sget-object v0, LX/0ps;->I:LX/0ps;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 146696
    if-eqz v2, :cond_0

    .line 146697
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0ps;->b(LX/0QB;)LX/0ps;

    move-result-object v0

    sput-object v0, LX/0ps;->I:LX/0ps;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146698
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 146699
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146700
    :cond_1
    sget-object v0, LX/0ps;->I:LX/0ps;

    return-object v0

    .line 146701
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 146702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0ps;I)Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 2

    .prologue
    .line 146619
    iget-object v0, p0, LX/0ps;->s:LX/0q5;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/0q5;->a(II)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v0

    return-object v0
.end method

.method private a(J)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146703
    iget-object v0, p0, LX/0ps;->h:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qJ;

    .line 146704
    iget-object v2, v0, LX/0qJ;->a:LX/0pq;

    invoke-virtual {v2, p1, p2}, LX/0pq;->a(J)V

    .line 146705
    goto :goto_0

    .line 146706
    :cond_0
    return-void
.end method

.method private a(LX/0V6;I)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146707
    iget-object v0, p0, LX/0ps;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35Z;

    .line 146708
    const/16 p0, 0x50

    if-ne p2, p0, :cond_0

    .line 146709
    iget-object p0, v0, LX/35Z;->a:LX/0ra;

    .line 146710
    invoke-static {p0}, LX/0ra;->f(LX/0ra;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146711
    sget-object v0, LX/32G;->OnCloseToDalvikHeapLimit:LX/32G;

    invoke-virtual {p0, v0}, LX/0ra;->a(LX/32G;)V

    .line 146712
    :cond_0
    goto :goto_0

    .line 146713
    :cond_1
    return-void
.end method

.method private a(Lcom/facebook/device/resourcemonitor/DataUsageInfo;Lcom/facebook/device/resourcemonitor/DataUsageBytes;)V
    .locals 4

    .prologue
    .line 146714
    iget-boolean v0, p0, LX/0ps;->G:Z

    if-eqz v0, :cond_0

    .line 146715
    iget-wide v2, p2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v0, v2

    .line 146716
    invoke-virtual {p1, v0, v1}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->a(J)V

    .line 146717
    iget-wide v2, p2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v0, v2

    .line 146718
    invoke-virtual {p1, v0, v1}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->b(J)V

    .line 146719
    :goto_0
    return-void

    .line 146720
    :cond_0
    iget-wide v2, p2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v0, v2

    .line 146721
    invoke-virtual {p1, v0, v1}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->c(J)V

    .line 146722
    iget-wide v2, p2, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v0, v2

    .line 146723
    invoke-virtual {p1, v0, v1}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->d(J)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/0ps;
    .locals 15

    .prologue
    .line 146724
    new-instance v0, LX/0ps;

    invoke-static {p0}, LX/0pt;->a(LX/0QB;)LX/0pt;

    move-result-object v1

    check-cast v1, LX/0pt;

    invoke-static {p0}, LX/0q3;->a(LX/0QB;)LX/0q3;

    move-result-object v2

    check-cast v2, LX/0q3;

    .line 146725
    invoke-static {}, LX/0VV;->d()Ljava/lang/Runtime;

    move-result-object v3

    move-object v3, v3

    .line 146726
    check-cast v3, Ljava/lang/Runtime;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v5

    check-cast v5, LX/0ka;

    invoke-static {p0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-static {p0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v9

    check-cast v9, LX/0V8;

    invoke-static {p0}, LX/0q5;->a(LX/0QB;)LX/0q5;

    move-result-object v10

    check-cast v10, LX/0q5;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v12

    check-cast v12, LX/0lp;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v13

    check-cast v13, LX/0Xl;

    invoke-static {p0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v14

    check-cast v14, Landroid/os/Handler;

    invoke-direct/range {v0 .. v14}, LX/0ps;-><init>(LX/0pt;LX/0q3;Ljava/lang/Runtime;LX/0SG;LX/0ka;Landroid/view/WindowManager;Landroid/app/ActivityManager;LX/03V;LX/0V8;LX/0q5;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lp;LX/0Xl;Landroid/os/Handler;)V

    .line 146727
    return-object v0
.end method

.method public static b(LX/0ps;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/device/resourcemonitor/DataUsageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146728
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 146729
    :cond_0
    iget-object v0, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0ps;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/0ps;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 146730
    :goto_0
    return-void

    .line 146731
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 146732
    iget-object v1, p0, LX/0ps;->u:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->b(Ljava/io/OutputStream;)LX/0nX;

    move-result-object v1

    .line 146733
    invoke-virtual {v1, p1}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 146734
    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146735
    iget-object v1, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0ps;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 146736
    :catch_0
    move-exception v0

    .line 146737
    sget-object v1, LX/0ps;->c:Ljava/lang/Class;

    const-string v2, "Couldn\'t deserialize In Process Data Usage Byte Trackers"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private c(LX/0V6;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146738
    iput-object p1, p0, LX/0ps;->A:LX/0V6;

    .line 146739
    iget-object v0, p0, LX/0ps;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 146740
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/device/resourcemonitor/DataUsageBytes;
    .locals 14

    .prologue
    const-wide/16 v2, 0x0

    .line 146657
    sget-object v0, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->MY_APP:Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    iget v0, v0, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->uid:I

    invoke-static {p0, v0}, LX/0ps;->a(LX/0ps;I)Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v1

    .line 146658
    iget-boolean v0, p0, LX/0ps;->H:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->MY_APP:Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    iget-boolean v0, v0, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->trackForegroundOnly:Z

    if-eqz v0, :cond_0

    .line 146659
    new-instance v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    .line 146660
    :goto_0
    iput-object v1, p0, LX/0ps;->B:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    .line 146661
    return-object v0

    .line 146662
    :cond_0
    iget-object v0, p0, LX/0ps;->B:Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    .line 146663
    new-instance v4, Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    iget-wide v6, v1, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    .line 146664
    iget-wide v12, v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->b:J

    move-wide v8, v12

    .line 146665
    sub-long/2addr v6, v8

    iget-wide v8, v1, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    .line 146666
    iget-wide v12, v0, Lcom/facebook/device/resourcemonitor/DataUsageBytes;->c:J

    move-wide v10, v12

    .line 146667
    sub-long/2addr v8, v10

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/facebook/device/resourcemonitor/DataUsageBytes;-><init>(JJ)V

    move-object v0, v4

    .line 146668
    goto :goto_0
.end method

.method private static declared-synchronized k(LX/0ps;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/device/resourcemonitor/DataUsageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146561
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 146562
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146563
    :goto_0
    monitor-exit p0

    return-object v0

    .line 146564
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146565
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0

    .line 146566
    :cond_1
    iget-object v0, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0ps;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146567
    if-eqz v0, :cond_3

    .line 146568
    iget-object v1, p0, LX/0ps;->u:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 146569
    sget-object v2, LX/0ps;->f:LX/266;

    if-nez v2, :cond_2

    .line 146570
    new-instance v2, LX/49b;

    invoke-direct {v2, p0}, LX/49b;-><init>(LX/0ps;)V

    sput-object v2, LX/0ps;->f:LX/266;

    .line 146571
    :cond_2
    sget-object v2, LX/0ps;->f:LX/266;

    move-object v2, v2

    .line 146572
    invoke-virtual {v1, v2}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object v0, v1

    .line 146573
    iput-object v0, p0, LX/0ps;->D:Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146574
    :cond_3
    :goto_1
    :try_start_2
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 146575
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    goto :goto_0

    .line 146576
    :catch_0
    move-exception v0

    .line 146577
    sget-object v1, LX/0ps;->c:Ljava/lang/Class;

    const-string v2, "Couldn\'t deserialize In Process Data Usage Byte Trackers"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 146578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146579
    :cond_4
    :try_start_3
    iget-object v0, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0ps;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146580
    if-eqz v0, :cond_7

    .line 146581
    iget-object v1, p0, LX/0ps;->u:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 146582
    sget-object v2, LX/0ps;->e:LX/266;

    if-nez v2, :cond_5

    .line 146583
    new-instance v2, LX/49a;

    invoke-direct {v2}, LX/49a;-><init>()V

    sput-object v2, LX/0ps;->e:LX/266;

    .line 146584
    :cond_5
    sget-object v2, LX/0ps;->e:LX/266;

    move-object v2, v2

    .line 146585
    invoke-virtual {v1, v2}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 146586
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 146587
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 146588
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 146589
    sget-object v6, Lcom/facebook/device/resourcemonitor/MonitoredProcess;->MY_APP:Lcom/facebook/device/resourcemonitor/MonitoredProcess;

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 146590
    :cond_6
    move-object v0, v4

    .line 146591
    iput-object v0, p0, LX/0ps;->D:Ljava/util/Map;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 146592
    :cond_7
    :goto_3
    :try_start_4
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    if-nez v0, :cond_8

    .line 146593
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    .line 146594
    :cond_8
    iget-object v0, p0, LX/0ps;->D:Ljava/util/Map;

    goto/16 :goto_0

    .line 146595
    :catch_1
    move-exception v0

    .line 146596
    sget-object v1, LX/0ps;->c:Ljava/lang/Class;

    const-string v2, "Couldn\'t deserialize In Process Data Usage Byte Trackers"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method

.method private static l()Lcom/facebook/device/resourcemonitor/DataUsageInfo;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 146597
    new-instance v1, Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;-><init>(JJJJ)V

    return-object v1
.end method


# virtual methods
.method public final a(LX/0V6;)Z
    .locals 12

    .prologue
    .line 146598
    iget-wide v4, p1, LX/0V6;->d:J

    move-wide v0, v4

    .line 146599
    iget-object v2, p0, LX/0ps;->k:LX/0q3;

    const-wide/16 v8, 0x64

    .line 146600
    iget-object v4, v2, LX/0q3;->a:LX/0V6;

    invoke-virtual {v4}, LX/0V6;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146601
    iget-object v4, v2, LX/0q3;->a:LX/0V6;

    .line 146602
    iget-wide v10, v4, LX/0V6;->e:J

    move-wide v4, v10

    .line 146603
    const-wide/16 v6, 0x1e

    mul-long/2addr v4, v6

    div-long/2addr v4, v8

    .line 146604
    :goto_0
    move-wide v2, v4

    .line 146605
    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object v4, v2, LX/0q3;->a:LX/0V6;

    .line 146606
    iget-wide v10, v4, LX/0V6;->e:J

    move-wide v4, v10

    .line 146607
    const-wide/16 v6, 0xf

    mul-long/2addr v4, v6

    div-long/2addr v4, v8

    goto :goto_0
.end method

.method public final b()LX/0V6;
    .locals 2

    .prologue
    .line 146608
    new-instance v0, LX/0V6;

    iget-object v1, p0, LX/0ps;->i:Ljava/lang/Runtime;

    invoke-direct {v0, v1}, LX/0V6;-><init>(Ljava/lang/Runtime;)V

    return-object v0
.end method

.method public final declared-synchronized c()Lcom/facebook/device/resourcemonitor/DataUsageInfo;
    .locals 1

    .prologue
    .line 146609
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0ps;->g()V

    .line 146610
    iget-object v0, p0, LX/0ps;->C:Lcom/facebook/device/resourcemonitor/DataUsageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 146611
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 146612
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0ps;->l()Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    move-result-object v0

    iput-object v0, p0, LX/0ps;->C:Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    .line 146613
    invoke-static {p0}, LX/0ps;->k(LX/0ps;)Ljava/util/Map;

    move-result-object v1

    .line 146614
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146615
    invoke-static {}, LX/0ps;->l()Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146617
    :cond_0
    :try_start_1
    invoke-static {p0, v1}, LX/0ps;->b(LX/0ps;Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146618
    monitor-exit p0

    return-void
.end method

.method public final e()V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146620
    new-instance v0, LX/0V6;

    iget-object v1, p0, LX/0ps;->i:Ljava/lang/Runtime;

    invoke-direct {v0, v1}, LX/0V6;-><init>(Ljava/lang/Runtime;)V

    .line 146621
    iget-wide v6, v0, LX/0V6;->e:J

    move-wide v2, v6

    .line 146622
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 146623
    iget-object v6, p0, LX/0ps;->F:Ljava/lang/Long;

    if-eqz v6, :cond_0

    iget-wide v6, v0, LX/0V6;->a:J

    iget-object v8, p0, LX/0ps;->F:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 146624
    :cond_0
    iget-wide v6, v0, LX/0V6;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, p0, LX/0ps;->F:Ljava/lang/Long;

    .line 146625
    iget-object v6, p0, LX/0ps;->q:LX/03V;

    const-string v7, "peak_memory_heap_allocation"

    iget-object v8, p0, LX/0ps;->F:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146626
    :cond_1
    invoke-virtual {p0, v0}, LX/0ps;->a(LX/0V6;)Z

    move-result v1

    .line 146627
    iget-object v2, p0, LX/0ps;->q:LX/03V;

    const-string v3, "is_low_on_memory"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146628
    if-eqz v1, :cond_2

    .line 146629
    const/16 v1, 0x50

    invoke-direct {p0, v0, v1}, LX/0ps;->a(LX/0V6;I)V

    .line 146630
    :cond_2
    iget-object v1, p0, LX/0ps;->A:LX/0V6;

    if-eqz v1, :cond_3

    iget-wide v2, v0, LX/0V6;->a:J

    iget-object v1, p0, LX/0ps;->A:LX/0V6;

    iget-wide v4, v1, LX/0V6;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 146631
    const-wide/32 v6, 0x100000

    move-wide v4, v6

    .line 146632
    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    .line 146633
    :cond_3
    invoke-direct {p0, v0}, LX/0ps;->c(LX/0V6;)V

    .line 146634
    :cond_4
    return-void
.end method

.method public final f()V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146635
    iget-object v0, p0, LX/0ps;->r:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 146636
    const-wide/32 v4, 0x500000

    move-wide v2, v4

    .line 146637
    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 146638
    invoke-direct {p0, v0, v1}, LX/0ps;->a(J)V

    .line 146639
    :cond_0
    return-void
.end method

.method public final declared-synchronized g()V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 146640
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0ps;->j()Lcom/facebook/device/resourcemonitor/DataUsageBytes;

    move-result-object v1

    .line 146641
    iget-object v0, p0, LX/0ps;->C:Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    invoke-direct {p0, v0, v1}, LX/0ps;->a(Lcom/facebook/device/resourcemonitor/DataUsageInfo;Lcom/facebook/device/resourcemonitor/DataUsageBytes;)V

    .line 146642
    invoke-static {p0}, LX/0ps;->k(LX/0ps;)Ljava/util/Map;

    move-result-object v2

    .line 146643
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    .line 146644
    invoke-direct {p0, v0, v1}, LX/0ps;->a(Lcom/facebook/device/resourcemonitor/DataUsageInfo;Lcom/facebook/device/resourcemonitor/DataUsageBytes;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146645
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146646
    :cond_0
    :try_start_1
    iget-object v4, p0, LX/0ps;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 146647
    :cond_1
    :goto_1
    iget-object v0, p0, LX/0ps;->m:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/0ps;->G:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146648
    monitor-exit p0

    return-void

    .line 146649
    :cond_2
    iget-object v4, p0, LX/0ps;->j:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 146650
    iget-object v6, p0, LX/0ps;->E:Ljava/lang/Long;

    if-eqz v6, :cond_3

    iget-object v6, p0, LX/0ps;->E:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x2710

    add-long/2addr v6, v8

    cmp-long v6, v4, v6

    if-ltz v6, :cond_1

    .line 146651
    :cond_3
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, LX/0ps;->E:Ljava/lang/Long;

    .line 146652
    invoke-static {p0, v2}, LX/0ps;->b(LX/0ps;Ljava/util/Map;)V

    goto :goto_1
.end method
