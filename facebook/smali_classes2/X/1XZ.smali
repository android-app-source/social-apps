.class public abstract LX/1XZ;
.super LX/13D;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 271364
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 271365
    if-eqz v0, :cond_0

    const-string v0, "3332"

    :goto_0
    sput-object v0, LX/1XZ;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string v0, "1822"

    goto :goto_0
.end method

.method public constructor <init>(LX/13J;)V
    .locals 0

    .prologue
    .line 271362
    invoke-direct {p0, p1}, LX/13D;-><init>(LX/13J;)V

    .line 271363
    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 271361
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271360
    const-string v0, "1822"

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 271359
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271357
    const-string v0, "Megaphone"

    return-object v0
.end method

.method public final l()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271358
    sget-object v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->STANDARD_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    sget-object v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->BRANDED_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    sget-object v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->SURVEY_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    sget-object v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->BLAST_MEGAPHONE:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    sget-object v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->CUSTOM_RENDERED:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
