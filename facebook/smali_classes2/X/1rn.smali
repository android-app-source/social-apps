.class public LX/1rn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile o:LX/1rn;


# instance fields
.field private final b:LX/0aG;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0TD;

.field public final e:LX/0Sh;

.field public final f:LX/1rU;

.field public final g:LX/1ro;

.field private final h:LX/1qv;

.field public final i:LX/1rp;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xB;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 333111
    const-class v0, LX/1rn;

    sput-object v0, LX/1rn;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Ot;LX/0TD;Landroid/os/Handler;LX/0Sh;LX/1rU;LX/1ro;LX/1qv;LX/1rp;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;",
            "LX/0TD;",
            "Landroid/os/Handler;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1rU;",
            "LX/1ro;",
            "LX/1qv;",
            "LX/1rp;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0xB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333098
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1rn;->n:J

    .line 333099
    iput-object p2, p0, LX/1rn;->c:LX/0Ot;

    .line 333100
    iput-object p1, p0, LX/1rn;->b:LX/0aG;

    .line 333101
    iput-object p3, p0, LX/1rn;->d:LX/0TD;

    .line 333102
    iput-object p5, p0, LX/1rn;->e:LX/0Sh;

    .line 333103
    iput-object p6, p0, LX/1rn;->f:LX/1rU;

    .line 333104
    iput-object p7, p0, LX/1rn;->g:LX/1ro;

    .line 333105
    iput-object p8, p0, LX/1rn;->h:LX/1qv;

    .line 333106
    iput-object p9, p0, LX/1rn;->i:LX/1rp;

    .line 333107
    iput-object p10, p0, LX/1rn;->j:LX/0Ot;

    .line 333108
    iput-object p11, p0, LX/1rn;->k:LX/0Ot;

    .line 333109
    iput-object p12, p0, LX/1rn;->l:LX/0Ot;

    .line 333110
    return-void
.end method

.method public static a(LX/0QB;)LX/1rn;
    .locals 3

    .prologue
    .line 333087
    sget-object v0, LX/1rn;->o:LX/1rn;

    if-nez v0, :cond_1

    .line 333088
    const-class v1, LX/1rn;

    monitor-enter v1

    .line 333089
    :try_start_0
    sget-object v0, LX/1rn;->o:LX/1rn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 333090
    if-eqz v2, :cond_0

    .line 333091
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1rn;->b(LX/0QB;)LX/1rn;

    move-result-object v0

    sput-object v0, LX/1rn;->o:LX/1rn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333092
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 333093
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 333094
    :cond_1
    sget-object v0, LX/1rn;->o:LX/1rn;

    return-object v0

    .line 333095
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 333096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/1rn;
    .locals 13

    .prologue
    .line 333085
    new-instance v0, LX/1rn;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    const/16 v2, 0xe5e

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v6

    check-cast v6, LX/1rU;

    invoke-static {p0}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v7

    check-cast v7, LX/1ro;

    invoke-static {p0}, LX/1qv;->b(LX/0QB;)LX/1qv;

    move-result-object v8

    check-cast v8, LX/1qv;

    invoke-static {p0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v9

    check-cast v9, LX/1rp;

    const/16 v10, 0xa71

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xe34

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x245

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, LX/1rn;-><init>(LX/0aG;LX/0Ot;LX/0TD;Landroid/os/Handler;LX/0Sh;LX/1rU;LX/1ro;LX/1qv;LX/1rp;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 333086
    return-object v0
.end method


# virtual methods
.method public final a(J)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333083
    iget-object v0, p0, LX/1rn;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 333084
    iget-object v0, p0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c(J)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333082
    iget-object v0, p0, LX/1rn;->d:LX/0TD;

    new-instance v1, LX/BE9;

    invoke-direct {v1, p0, p1, p2}, LX/BE9;-><init>(LX/1rn;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 333043
    iget-boolean v0, p0, LX/1rn;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1rn;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1rn;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    if-nez v0, :cond_1

    .line 333044
    :cond_0
    :goto_0
    return-void

    .line 333045
    :cond_1
    iget-object v1, p0, LX/1rn;->e:LX/0Sh;

    iget-object v0, p0, LX/1rn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-virtual {v0}, LX/2dj;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/BE8;

    invoke-direct {v2, p0}, LX/BE8;-><init>(LX/1rn;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 4

    .prologue
    .line 333079
    if-nez p1, :cond_0

    .line 333080
    :goto_0
    return-void

    .line 333081
    :cond_0
    iget-object v0, p0, LX/1rn;->d:LX/0TD;

    new-instance v1, Lcom/facebook/notifications/util/NotificationsUtils$1;

    sget-object v2, LX/1rn;->a:Ljava/lang/Class;

    const-string v3, "markStoredNotificationsAsSeen"

    invoke-direct {v1, p0, v2, v3, p1}, Lcom/facebook/notifications/util/NotificationsUtils$1;-><init>(LX/1rn;Ljava/lang/Class;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    const v2, 0x2a0c38e8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 4

    .prologue
    .line 333069
    if-eqz p1, :cond_0

    .line 333070
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v0, v0

    .line 333071
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 333072
    :goto_0
    if-eqz v0, :cond_2

    .line 333073
    iget-object v0, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v0, v0

    .line 333074
    :goto_1
    invoke-virtual {p0, v0}, LX/1rn;->a(Ljava/lang/String;)V

    .line 333075
    :cond_0
    return-void

    .line 333076
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 333077
    :cond_2
    iget-wide v2, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->e:J

    move-wide v0, v2

    .line 333078
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 333067
    iget-object v0, p0, LX/1rn;->g:LX/1ro;

    invoke-virtual {v0, p1}, LX/1ro;->a(Ljava/lang/String;)V

    .line 333068
    return-void
.end method

.method public final a(Ljava/lang/String;LX/BAy;)V
    .locals 3

    .prologue
    .line 333064
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 333065
    iget-object v0, p0, LX/1rn;->d:LX/0TD;

    new-instance v1, Lcom/facebook/notifications/util/NotificationsUtils$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/notifications/util/NotificationsUtils$5;-><init>(LX/1rn;Ljava/lang/String;LX/BAy;)V

    const v2, -0x355115f1    # -5731591.5f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 333066
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 333049
    iget-object v0, p0, LX/1rn;->f:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne p2, v0, :cond_1

    .line 333050
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333051
    iget-object v0, p0, LX/1rn;->g:LX/1ro;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/1rn;->h:LX/1qv;

    invoke-virtual {v3}, LX/1qv;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1ro;->b(LX/0Px;Ljava/lang/Long;)V

    .line 333052
    iget-object v0, p0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, p1, p2, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Z)I

    .line 333053
    :cond_0
    :goto_0
    return-void

    .line 333054
    :cond_1
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 333055
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    const-string v2, "Notification ID list can\'t be empty"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 333056
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333057
    const-string v2, "graphNotifsUpdateSeenStatePrams"

    new-instance v3, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    invoke-direct {v3, p1, p2}, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;-><init>(Ljava/util/Collection;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 333058
    if-eqz p3, :cond_2

    .line 333059
    const-string v2, "overridden_viewer_context"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 333060
    :cond_2
    iget-object v2, p0, LX/1rn;->b:LX/0aG;

    const-string v3, "graphNotifUpdateSeenState"

    const v4, 0x3349a92a

    invoke-static {v2, v3, v0, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 333061
    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    .line 333062
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0

    .line 333063
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 333046
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 333047
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, LX/1rn;->n:J

    .line 333048
    return-void
.end method
