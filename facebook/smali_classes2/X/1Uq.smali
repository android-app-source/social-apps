.class public final LX/1Uq;
.super LX/1Ur;
.source ""


# static fields
.field public static final i:LX/1Up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256841
    new-instance v0, LX/1Uq;

    invoke-direct {v0}, LX/1Uq;-><init>()V

    sput-object v0, LX/1Uq;->i:LX/1Up;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 256842
    invoke-direct {p0}, LX/1Ur;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFFF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 256836
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 256837
    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    .line 256838
    invoke-virtual {p1, p7, p8}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 256839
    add-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v0, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 256840
    return-void
.end method
