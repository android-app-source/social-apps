.class public LX/0XY;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/quicklog/HoneySamplingPolicy;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Xe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79030
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Xe;
    .locals 3

    .prologue
    .line 79031
    sget-object v0, LX/0XY;->a:LX/0Xe;

    if-nez v0, :cond_1

    .line 79032
    const-class v1, LX/0XY;

    monitor-enter v1

    .line 79033
    :try_start_0
    sget-object v0, LX/0XY;->a:LX/0Xe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79034
    if-eqz v2, :cond_0

    .line 79035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79036
    invoke-static {v0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object p0

    check-cast p0, LX/0XZ;

    invoke-static {p0}, LX/0Xd;->a(LX/0XZ;)LX/0Xe;

    move-result-object p0

    move-object v0, p0

    .line 79037
    sput-object v0, LX/0XY;->a:LX/0Xe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79038
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79039
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79040
    :cond_1
    sget-object v0, LX/0XY;->a:LX/0Xe;

    return-object v0

    .line 79041
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79029
    invoke-static {p0}, LX/0XZ;->a(LX/0QB;)LX/0XZ;

    move-result-object v0

    check-cast v0, LX/0XZ;

    invoke-static {v0}, LX/0Xd;->a(LX/0XZ;)LX/0Xe;

    move-result-object v0

    return-object v0
.end method
