.class public LX/0aU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0aU;


# instance fields
.field private final a:LX/01U;


# direct methods
.method public constructor <init>(LX/01U;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84800
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84801
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/01U;

    iput-object v0, p0, LX/0aU;->a:LX/01U;

    .line 84802
    return-void
.end method

.method public static a(LX/0QB;)LX/0aU;
    .locals 4

    .prologue
    .line 84803
    sget-object v0, LX/0aU;->b:LX/0aU;

    if-nez v0, :cond_1

    .line 84804
    const-class v1, LX/0aU;

    monitor-enter v1

    .line 84805
    :try_start_0
    sget-object v0, LX/0aU;->b:LX/0aU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84806
    if-eqz v2, :cond_0

    .line 84807
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84808
    new-instance p0, LX/0aU;

    invoke-static {v0}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v3

    check-cast v3, LX/01U;

    invoke-direct {p0, v3}, LX/0aU;-><init>(LX/01U;)V

    .line 84809
    move-object v0, p0

    .line 84810
    sput-object v0, LX/0aU;->b:LX/0aU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84811
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84812
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84813
    :cond_1
    sget-object v0, LX/0aU;->b:LX/0aU;

    return-object v0

    .line 84814
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84815
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 84816
    const-string v0, "com.facebook.intent.action.%s.%s"

    iget-object v1, p0, LX/0aU;->a:LX/01U;

    invoke-virtual {v1}, LX/01U;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
