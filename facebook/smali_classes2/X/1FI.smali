.class public LX/1FI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222003
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 222004
    if-lez p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 222005
    iput p1, p0, LX/1FI;->c:I

    .line 222006
    iput p2, p0, LX/1FI;->d:I

    .line 222007
    new-instance v0, LX/1FX;

    invoke-direct {v0, p0}, LX/1FX;-><init>(LX/1FI;)V

    iput-object v0, p0, LX/1FI;->e:LX/1FN;

    .line 222008
    return-void

    :cond_0
    move v0, v2

    .line 222009
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222010
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1FN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FN",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222028
    iget-object v0, p0, LX/1FI;->e:LX/1FN;

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 222029
    const/4 v1, 0x0

    .line 222030
    :goto_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 222031
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 222032
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 222033
    invoke-static {v0}, Lcom/facebook/imagepipeline/nativecode/Bitmaps;->a(Landroid/graphics/Bitmap;)V

    .line 222034
    :cond_0
    invoke-virtual {p0, v0}, LX/1FI;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 222035
    new-instance v0, LX/1FM;

    invoke-direct {v0}, LX/1FM;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222036
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 222037
    if-eqz p1, :cond_4

    .line 222038
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 222039
    add-int/lit8 v2, v1, -0x1

    if-lez v1, :cond_1

    .line 222040
    invoke-virtual {p0, v0}, LX/1FI;->b(Landroid/graphics/Bitmap;)V

    .line 222041
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move v1, v2

    .line 222042
    goto :goto_1

    .line 222043
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222044
    :cond_3
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 222045
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 222046
    iget-object v4, p0, LX/1FI;->e:LX/1FN;

    invoke-static {v0, v4}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 222047
    :cond_4
    invoke-static {v3}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 222048
    :cond_5
    return-object v2
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)Z
    .locals 6

    .prologue
    .line 222020
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/1le;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 222021
    iget v1, p0, LX/1FI;->a:I

    iget v2, p0, LX/1FI;->c:I

    if-ge v1, v2, :cond_0

    iget-wide v2, p0, LX/1FI;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget v1, p0, LX/1FI;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 222022
    :cond_0
    const/4 v0, 0x0

    .line 222023
    :goto_0
    monitor-exit p0

    return v0

    .line 222024
    :cond_1
    :try_start_1
    iget v1, p0, LX/1FI;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1FI;->a:I

    .line 222025
    iget-wide v2, p0, LX/1FI;->b:J

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/1FI;->b:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222026
    const/4 v0, 0x1

    goto :goto_0

    .line 222027
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 222011
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/1le;->a(Landroid/graphics/Bitmap;)I

    move-result v3

    .line 222012
    iget v2, p0, LX/1FI;->a:I

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    const-string v4, "No bitmaps registered."

    invoke-static {v2, v4}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 222013
    int-to-long v4, v3

    iget-wide v6, p0, LX/1FI;->b:J

    cmp-long v2, v4, v6

    if-gtz v2, :cond_1

    :goto_1
    const-string v1, "Bitmap size bigger than the total registered size: %d, %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-wide v6, p0, LX/1FI;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v1, v2}, LX/03g;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 222014
    iget-wide v0, p0, LX/1FI;->b:J

    int-to-long v2, v3

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/1FI;->b:J

    .line 222015
    iget v0, p0, LX/1FI;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1FI;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222016
    monitor-exit p0

    return-void

    :cond_0
    move v2, v1

    .line 222017
    goto :goto_0

    :cond_1
    move v0, v1

    .line 222018
    goto :goto_1

    .line 222019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
