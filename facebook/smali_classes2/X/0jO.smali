.class public final enum LX/0jO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0jO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0jO;

.field public static final enum DROP_OUT:LX/0jO;

.field public static final enum RISE_IN:LX/0jO;

.field public static final enum SLIDE_LEFT_IN:LX/0jO;

.field public static final enum SLIDE_RIGHT_OUT:LX/0jO;


# instance fields
.field public final resource:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123002
    new-instance v0, LX/0jO;

    const-string v1, "SLIDE_LEFT_IN"

    const v2, 0x7f0400da

    invoke-direct {v0, v1, v3, v2}, LX/0jO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0jO;->SLIDE_LEFT_IN:LX/0jO;

    .line 123003
    new-instance v0, LX/0jO;

    const-string v1, "SLIDE_RIGHT_OUT"

    const v2, 0x7f0400ed

    invoke-direct {v0, v1, v4, v2}, LX/0jO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0jO;->SLIDE_RIGHT_OUT:LX/0jO;

    .line 123004
    new-instance v0, LX/0jO;

    const-string v1, "DROP_OUT"

    const v2, 0x7f040031

    invoke-direct {v0, v1, v5, v2}, LX/0jO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0jO;->DROP_OUT:LX/0jO;

    .line 123005
    new-instance v0, LX/0jO;

    const-string v1, "RISE_IN"

    const v2, 0x7f0400b7

    invoke-direct {v0, v1, v6, v2}, LX/0jO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0jO;->RISE_IN:LX/0jO;

    .line 123006
    const/4 v0, 0x4

    new-array v0, v0, [LX/0jO;

    sget-object v1, LX/0jO;->SLIDE_LEFT_IN:LX/0jO;

    aput-object v1, v0, v3

    sget-object v1, LX/0jO;->SLIDE_RIGHT_OUT:LX/0jO;

    aput-object v1, v0, v4

    sget-object v1, LX/0jO;->DROP_OUT:LX/0jO;

    aput-object v1, v0, v5

    sget-object v1, LX/0jO;->RISE_IN:LX/0jO;

    aput-object v1, v0, v6

    sput-object v0, LX/0jO;->$VALUES:[LX/0jO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 123008
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 123009
    iput p3, p0, LX/0jO;->resource:I

    .line 123010
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0jO;
    .locals 1

    .prologue
    .line 123011
    const-class v0, LX/0jO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0jO;

    return-object v0
.end method

.method public static values()[LX/0jO;
    .locals 1

    .prologue
    .line 123007
    sget-object v0, LX/0jO;->$VALUES:[LX/0jO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0jO;

    return-object v0
.end method
