.class public LX/0Vu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0Vu;


# instance fields
.field private a:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:Ljava/util/Locale;

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/0W9;


# direct methods
.method public constructor <init>(LX/0W9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 74971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74972
    const/4 v0, 0x0

    iput v0, p0, LX/0Vu;->a:I

    .line 74973
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Vu;->b:Ljava/util/Locale;

    .line 74974
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0Vu;->c:Ljava/util/HashMap;

    .line 74975
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 74976
    iput-object v0, p0, LX/0Vu;->d:LX/0Ot;

    .line 74977
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 74978
    iput-object v0, p0, LX/0Vu;->e:LX/0Ot;

    .line 74979
    iput-object p1, p0, LX/0Vu;->f:LX/0W9;

    .line 74980
    return-void
.end method

.method public static a(LX/0QB;)LX/0Vu;
    .locals 5

    .prologue
    .line 74956
    sget-object v0, LX/0Vu;->g:LX/0Vu;

    if-nez v0, :cond_1

    .line 74957
    const-class v1, LX/0Vu;

    monitor-enter v1

    .line 74958
    :try_start_0
    sget-object v0, LX/0Vu;->g:LX/0Vu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 74959
    if-eqz v2, :cond_0

    .line 74960
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 74961
    new-instance v4, LX/0Vu;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-direct {v4, v3}, LX/0Vu;-><init>(LX/0W9;)V

    .line 74962
    const/16 v3, 0xbc

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0xc1c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 74963
    iput-object v3, v4, LX/0Vu;->d:LX/0Ot;

    iput-object p0, v4, LX/0Vu;->e:LX/0Ot;

    .line 74964
    move-object v0, v4

    .line 74965
    sput-object v0, LX/0Vu;->g:LX/0Vu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74966
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 74967
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 74968
    :cond_1
    sget-object v0, LX/0Vu;->g:LX/0Vu;

    return-object v0

    .line 74969
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 74970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static declared-synchronized a(LX/0Vu;)V
    .locals 5

    .prologue
    .line 74981
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0Vu;->a:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Vu;->b:Ljava/util/Locale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 74982
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 74983
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0Vu;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "fbt_string_batch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 74984
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74985
    iget-object v0, p0, LX/0Vu;->f:LX/0W9;

    iget-object v2, p0, LX/0Vu;->b:Ljava/util/Locale;

    invoke-virtual {v0, v2}, LX/0W9;->a(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 74986
    const-string v2, "locale"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 74987
    iget-object v0, p0, LX/0Vu;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74988
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "string_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 74989
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 74990
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 74991
    :cond_2
    :try_start_2
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 74992
    :cond_3
    iget-object v0, p0, LX/0Vu;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 74993
    const/4 v0, 0x0

    iput v0, p0, LX/0Vu;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/0Vu;J)V
    .locals 5

    .prologue
    .line 74948
    monitor-enter p0

    const-wide/16 v0, 0x0

    .line 74949
    :try_start_0
    iget-object v2, p0, LX/0Vu;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74950
    iget-object v0, p0, LX/0Vu;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 74951
    :cond_0
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 74952
    iget-object v2, p0, LX/0Vu;->c:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74953
    iget v0, p0, LX/0Vu;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Vu;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74954
    monitor-exit p0

    return-void

    .line 74955
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(JLjava/util/Locale;)V
    .locals 3

    .prologue
    .line 74939
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Vu;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zm;

    const-string v1, "fbt_string_batch"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 74940
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 74941
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/0Vu;->b:Ljava/util/Locale;

    if-eq p3, v0, :cond_2

    .line 74942
    invoke-static {p0}, LX/0Vu;->a(LX/0Vu;)V

    .line 74943
    iput-object p3, p0, LX/0Vu;->b:Ljava/util/Locale;

    .line 74944
    :cond_2
    invoke-static {p0, p1, p2}, LX/0Vu;->a(LX/0Vu;J)V

    .line 74945
    iget v0, p0, LX/0Vu;->a:I

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 74946
    invoke-static {p0}, LX/0Vu;->a(LX/0Vu;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 74947
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
