.class public LX/1As;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1At;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/33U;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BtQ;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Au;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 211422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211423
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)LX/1Av;
    .locals 3
    .param p1    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Av;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 211424
    if-nez p1, :cond_0

    .line 211425
    const/4 v0, 0x0

    .line 211426
    :goto_0
    return-object v0

    .line 211427
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 211428
    const-class p1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 211429
    const-string p1, "com.facebook.feed.fragment.NewsFeedFragment"

    .line 211430
    :goto_1
    move-object v0, p1

    .line 211431
    const/4 v1, 0x0

    .line 211432
    if-nez v0, :cond_4

    .line 211433
    :goto_2
    move-object v0, v1

    .line 211434
    goto :goto_0

    .line 211435
    :cond_1
    const-class p1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 211436
    const-string p1, "com.facebook.feed.storypermalink.StoryPermalinkFragment"

    goto :goto_1

    .line 211437
    :cond_2
    const-class p1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 211438
    const-string p1, "com.facebook.notifications.multirow.NotificationsFeedFragment"

    goto :goto_1

    .line 211439
    :cond_3
    const/4 p1, 0x0

    goto :goto_1

    .line 211440
    :cond_4
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_5
    :goto_3
    packed-switch v2, :pswitch_data_0

    goto :goto_2

    .line 211441
    :pswitch_0
    iget-object v1, p0, LX/1As;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Au;

    goto :goto_2

    .line 211442
    :sswitch_0
    const-string p1, "com.facebook.feed.fragment.NewsFeedFragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string p1, "com.facebook.feed.storypermalink.StoryPermalinkFragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string p1, "com.facebook.notifications.multirow.NotificationsFeedFragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    const/4 v2, 0x2

    goto :goto_3

    .line 211443
    :pswitch_1
    iget-object v1, p0, LX/1As;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BtQ;

    goto :goto_2

    .line 211444
    :pswitch_2
    iget-object v1, p0, LX/1As;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/33U;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41ef82b7 -> :sswitch_1
        0x246600ea -> :sswitch_0
        0x44c6e40a -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
