.class public LX/0p7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/0p7;


# instance fields
.field private final a:LX/0W3;

.field private final b:LX/0So;

.field public c:LX/1pb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:J

.field private volatile e:Z

.field public f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0p2;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private final j:[D


# direct methods
.method public constructor <init>(LX/0W3;LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 143715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143716
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/0p7;->d:J

    .line 143717
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p7;->e:Z

    .line 143718
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143719
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0p7;->h:Ljava/util/List;

    .line 143720
    const/16 v0, 0x8

    new-array v0, v0, [D

    iput-object v0, p0, LX/0p7;->j:[D

    .line 143721
    iput-object p1, p0, LX/0p7;->a:LX/0W3;

    .line 143722
    iput-object p2, p0, LX/0p7;->b:LX/0So;

    .line 143723
    return-void
.end method

.method public static a(LX/0QB;)LX/0p7;
    .locals 5

    .prologue
    .line 143702
    sget-object v0, LX/0p7;->k:LX/0p7;

    if-nez v0, :cond_1

    .line 143703
    const-class v1, LX/0p7;

    monitor-enter v1

    .line 143704
    :try_start_0
    sget-object v0, LX/0p7;->k:LX/0p7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143705
    if-eqz v2, :cond_0

    .line 143706
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143707
    new-instance p0, LX/0p7;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/0p7;-><init>(LX/0W3;LX/0So;)V

    .line 143708
    move-object v0, p0

    .line 143709
    sput-object v0, LX/0p7;->k:LX/0p7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143710
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143711
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143712
    :cond_1
    sget-object v0, LX/0p7;->k:LX/0p7;

    return-object v0

    .line 143713
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143714
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 143679
    iget-object v0, p0, LX/0p7;->c:LX/1pb;

    if-nez v0, :cond_0

    move v0, v4

    .line 143680
    :goto_0
    return v0

    .line 143681
    :cond_0
    iget-object v0, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    .line 143682
    sget-object v1, LX/2fb;->a:[I

    invoke-virtual {v0}, LX/0p3;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    move v0, v5

    .line 143683
    goto :goto_0

    .line 143684
    :pswitch_0
    const-wide/16 v2, 0x0

    .line 143685
    invoke-static {p0}, LX/0p7;->h(LX/0p7;)D

    move-result-wide v0

    .line 143686
    :goto_1
    iget-object v6, p0, LX/0p7;->c:LX/1pb;

    invoke-interface {v6}, LX/1pb;->a()D

    move-result-wide v6

    .line 143687
    cmpl-double v8, v6, v0

    if-lez v8, :cond_1

    .line 143688
    invoke-static {p0}, LX/0p7;->m(LX/0p7;)D

    move-result-wide v2

    mul-double/2addr v0, v2

    cmpl-double v0, v6, v0

    if-lez v0, :cond_2

    move v0, v5

    .line 143689
    goto :goto_0

    .line 143690
    :pswitch_1
    invoke-static {p0}, LX/0p7;->h(LX/0p7;)D

    move-result-wide v2

    .line 143691
    invoke-static {p0}, LX/0p7;->i(LX/0p7;)D

    move-result-wide v0

    goto :goto_1

    .line 143692
    :pswitch_2
    invoke-static {p0}, LX/0p7;->i(LX/0p7;)D

    move-result-wide v2

    .line 143693
    invoke-static {p0}, LX/0p7;->j(LX/0p7;)D

    move-result-wide v0

    goto :goto_1

    .line 143694
    :pswitch_3
    invoke-static {p0}, LX/0p7;->j(LX/0p7;)D

    move-result-wide v2

    .line 143695
    invoke-static {p0}, LX/0p7;->k(LX/0p7;)D

    move-result-wide v0

    goto :goto_1

    .line 143696
    :pswitch_4
    invoke-static {p0}, LX/0p7;->k(LX/0p7;)D

    move-result-wide v2

    .line 143697
    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    .line 143698
    goto :goto_1

    .line 143699
    :cond_1
    invoke-static {p0}, LX/0p7;->n(LX/0p7;)D

    move-result-wide v0

    mul-double/2addr v0, v2

    cmpg-double v0, v6, v0

    if-gez v0, :cond_2

    move v0, v5

    .line 143700
    goto :goto_0

    :cond_2
    move v0, v4

    .line 143701
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private declared-synchronized e()LX/0p3;
    .locals 5

    .prologue
    .line 143662
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0p7;->c:LX/1pb;

    if-nez v0, :cond_0

    .line 143663
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143664
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0p7;->c:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    .line 143665
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    .line 143666
    sget-object v2, LX/0p3;->UNKNOWN:LX/0p3;

    .line 143667
    :goto_1
    move-object v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143668
    goto :goto_0

    .line 143669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 143670
    :cond_1
    invoke-static {p0}, LX/0p7;->h(LX/0p7;)D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    .line 143671
    sget-object v2, LX/0p3;->DEGRADED:LX/0p3;

    goto :goto_1

    .line 143672
    :cond_2
    invoke-static {p0}, LX/0p7;->i(LX/0p7;)D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_3

    .line 143673
    sget-object v2, LX/0p3;->POOR:LX/0p3;

    goto :goto_1

    .line 143674
    :cond_3
    invoke-static {p0}, LX/0p7;->j(LX/0p7;)D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_4

    .line 143675
    sget-object v2, LX/0p3;->MODERATE:LX/0p3;

    goto :goto_1

    .line 143676
    :cond_4
    invoke-static {p0}, LX/0p7;->k(LX/0p7;)D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_5

    .line 143677
    sget-object v2, LX/0p3;->GOOD:LX/0p3;

    goto :goto_1

    .line 143678
    :cond_5
    sget-object v2, LX/0p3;->EXCELLENT:LX/0p3;

    goto :goto_1
.end method

.method private g()D
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 143659
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v8

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143660
    iget-object v0, p0, LX/0p7;->j:[D

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v4, LX/0X5;->bz:J

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v5, v6, v7}, LX/0W4;->a(JJ)J

    move-result-wide v4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    aput-wide v2, v0, v8

    .line 143661
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v8

    return-wide v0
.end method

.method public static h(LX/0p7;)D
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 143656
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143657
    iget-object v0, p0, LX/0p7;->j:[D

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bJ:J

    const-wide/16 v4, 0x2

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    aput-wide v2, v0, v6

    .line 143658
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    return-wide v0
.end method

.method public static i(LX/0p7;)D
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 143653
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143654
    iget-object v0, p0, LX/0p7;->j:[D

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bB:J

    const-wide/16 v4, 0x96

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    aput-wide v2, v0, v6

    .line 143655
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    return-wide v0
.end method

.method public static j(LX/0p7;)D
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 143650
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143651
    iget-object v0, p0, LX/0p7;->j:[D

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bC:J

    const-wide/16 v4, 0x226

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    aput-wide v2, v0, v6

    .line 143652
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    return-wide v0
.end method

.method public static k(LX/0p7;)D
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 143647
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143648
    iget-object v0, p0, LX/0p7;->j:[D

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bD:J

    const-wide/16 v4, 0x7d0

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    aput-wide v2, v0, v6

    .line 143649
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    return-wide v0
.end method

.method private l()I
    .locals 7

    .prologue
    const/4 v6, 0x5

    .line 143603
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143604
    iget-object v0, p0, LX/0p7;->j:[D

    iget-object v1, p0, LX/0p7;->a:LX/0W3;

    sget-wide v2, LX/0X5;->bx:J

    const-wide/16 v4, 0x5

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    aput-wide v2, v0, v6

    .line 143605
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v6

    double-to-int v0, v0

    return v0
.end method

.method private static m(LX/0p7;)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    const/4 v4, 0x6

    .line 143643
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v4

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143644
    invoke-static {p0}, LX/0p7;->o(LX/0p7;)J

    move-result-wide v0

    .line 143645
    iget-object v2, p0, LX/0p7;->j:[D

    long-to-double v0, v0

    sub-double v0, v6, v0

    div-double v0, v6, v0

    aput-wide v0, v2, v4

    .line 143646
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v4

    return-wide v0
.end method

.method private static n(LX/0p7;)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    const/4 v4, 0x7

    .line 143639
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v4

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143640
    invoke-static {p0}, LX/0p7;->o(LX/0p7;)J

    move-result-wide v0

    .line 143641
    iget-object v2, p0, LX/0p7;->j:[D

    long-to-double v0, v0

    sub-double v0, v6, v0

    div-double/2addr v0, v6

    aput-wide v0, v2, v4

    .line 143642
    :cond_0
    iget-object v0, p0, LX/0p7;->j:[D

    aget-wide v0, v0, v4

    return-wide v0
.end method

.method private static o(LX/0p7;)J
    .locals 10

    .prologue
    const-wide/16 v2, 0x63

    const-wide/16 v0, 0x0

    .line 143634
    iget-object v4, p0, LX/0p7;->a:LX/0W3;

    sget-wide v6, LX/0X5;->bH:J

    const-wide/16 v8, 0x14

    invoke-interface {v4, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v4

    .line 143635
    cmp-long v6, v4, v0

    if-gez v6, :cond_0

    .line 143636
    :goto_0
    return-wide v0

    .line 143637
    :cond_0
    cmp-long v0, v4, v2

    if-lez v0, :cond_1

    move-wide v0, v2

    .line 143638
    goto :goto_0

    :cond_1
    move-wide v0, v4

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0p2;)LX/0p3;
    .locals 1

    .prologue
    .line 143632
    iget-object v0, p0, LX/0p7;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143633
    iget-object v0, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    return-object v0
.end method

.method public final declared-synchronized a(JJ)V
    .locals 7

    .prologue
    .line 143611
    monitor-enter p0

    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    long-to-double v2, p3

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    mul-double/2addr v0, v2

    .line 143612
    :try_start_0
    iget-object v2, p0, LX/0p7;->c:LX/1pb;

    if-nez v2, :cond_0

    .line 143613
    new-instance v2, LX/1u7;

    invoke-direct {p0}, LX/0p7;->g()D

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/1u7;-><init>(D)V

    iput-object v2, p0, LX/0p7;->c:LX/1pb;

    .line 143614
    :cond_0
    iget-object v2, p0, LX/0p7;->c:LX/1pb;

    invoke-interface {v2, v0, v1}, LX/1pb;->a(D)V

    .line 143615
    iget-object v0, p0, LX/0p7;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/0p7;->d:J

    .line 143616
    iget-boolean v0, p0, LX/0p7;->e:Z

    if-eqz v0, :cond_3

    .line 143617
    iget v0, p0, LX/0p7;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0p7;->i:I

    .line 143618
    invoke-direct {p0}, LX/0p7;->e()LX/0p3;

    move-result-object v0

    iget-object v1, p0, LX/0p7;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 143619
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p7;->e:Z

    .line 143620
    const/4 v0, 0x1

    iput v0, p0, LX/0p7;->i:I

    .line 143621
    :cond_1
    iget v0, p0, LX/0p7;->i:I

    invoke-direct {p0}, LX/0p7;->l()I

    move-result v1

    if-lt v0, v1, :cond_2

    invoke-direct {p0}, LX/0p7;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143622
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p7;->e:Z

    .line 143623
    const/4 v0, 0x1

    iput v0, p0, LX/0p7;->i:I

    .line 143624
    iget-object v0, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, LX/0p7;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143625
    iget-object v0, p0, LX/0p7;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p2;

    .line 143626
    iget-object v1, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0p3;

    invoke-interface {v0, v1}, LX/0p2;->a(LX/0p3;)V

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143627
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 143628
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0p7;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0}, LX/0p7;->e()LX/0p3;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 143629
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0p7;->e:Z

    .line 143630
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, LX/0p7;->e()LX/0p3;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0p7;->g:Ljava/util/concurrent/atomic/AtomicReference;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 143631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()D
    .locals 2

    .prologue
    .line 143610
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0p7;->c:LX/1pb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0p7;->c:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143606
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0p7;->d:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 143607
    invoke-static {}, LX/0am;->absent()LX/0am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 143608
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0p7;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/0p7;->d:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 143609
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
