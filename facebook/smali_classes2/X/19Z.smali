.class public LX/19Z;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Z

.field private final c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "LX/7IG;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208097
    const-class v0, LX/19Z;

    sput-object v0, LX/19Z;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 208098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208099
    new-instance v0, LX/0aq;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/19Z;->c:LX/0aq;

    .line 208100
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/19Z;->d:Z

    .line 208101
    iput-boolean p1, p0, LX/19Z;->b:Z

    .line 208102
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 208103
    if-ltz p1, :cond_0

    iget-boolean v0, p0, LX/19Z;->b:Z

    if-nez v0, :cond_1

    .line 208104
    :cond_0
    :goto_0
    return-void

    .line 208105
    :cond_1
    iget-object v1, p0, LX/19Z;->c:LX/0aq;

    monitor-enter v1

    .line 208106
    :try_start_0
    iget-object v0, p0, LX/19Z;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7IG;

    .line 208107
    if-eqz v0, :cond_2

    .line 208108
    sget-object v2, LX/7IJ;->STOP:LX/7IJ;

    iput-object v2, v0, LX/7IG;->a:LX/7IJ;

    .line 208109
    const/4 v2, -0x1

    iput v2, v0, LX/7IG;->b:I

    .line 208110
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 208111
    iget-boolean v0, p0, LX/19Z;->b:Z

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 208112
    :cond_0
    :goto_0
    return-void

    .line 208113
    :cond_1
    iget-object v1, p0, LX/19Z;->c:LX/0aq;

    monitor-enter v1

    .line 208114
    :try_start_0
    iget-object v0, p0, LX/19Z;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7IG;

    .line 208115
    if-eqz v0, :cond_2

    iget v2, v0, LX/7IG;->b:I

    if-eq v2, p2, :cond_2

    .line 208116
    iput p2, v0, LX/7IG;->b:I

    .line 208117
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ILX/7IG;)V
    .locals 3

    .prologue
    .line 208118
    if-ltz p1, :cond_0

    iget-boolean v0, p0, LX/19Z;->b:Z

    if-nez v0, :cond_1

    .line 208119
    :cond_0
    :goto_0
    return-void

    .line 208120
    :cond_1
    iget-object v1, p0, LX/19Z;->c:LX/0aq;

    monitor-enter v1

    .line 208121
    :try_start_0
    iget-object v0, p0, LX/19Z;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208122
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208123
    invoke-virtual {p2}, LX/7IG;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 208124
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ILX/7IJ;)V
    .locals 4

    .prologue
    .line 208125
    iget-boolean v0, p0, LX/19Z;->b:Z

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 208126
    :cond_0
    :goto_0
    return-void

    .line 208127
    :cond_1
    iget-object v1, p0, LX/19Z;->c:LX/0aq;

    monitor-enter v1

    .line 208128
    :try_start_0
    iget-object v0, p0, LX/19Z;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7IG;

    .line 208129
    if-eqz v0, :cond_2

    iget-object v2, v0, LX/7IG;->a:LX/7IJ;

    sget-object v3, LX/7IJ;->STOP:LX/7IJ;

    if-ne v2, v3, :cond_3

    .line 208130
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 208131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 208132
    :cond_3
    :try_start_1
    iput-object p2, v0, LX/7IG;->a:LX/7IJ;

    .line 208133
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b(I)LX/7IG;
    .locals 3

    .prologue
    .line 208134
    iget-boolean v0, p0, LX/19Z;->b:Z

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 208135
    :cond_0
    const/4 v0, 0x0

    .line 208136
    :goto_0
    return-object v0

    .line 208137
    :cond_1
    iget-object v1, p0, LX/19Z;->c:LX/0aq;

    monitor-enter v1

    .line 208138
    :try_start_0
    iget-object v0, p0, LX/19Z;->c:LX/0aq;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7IG;

    .line 208139
    monitor-exit v1

    goto :goto_0

    .line 208140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
