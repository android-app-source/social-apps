.class public abstract LX/0ZK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEvent::",
        "LX/0ki;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0ZL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ZL",
            "<TTEvent;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 83181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83182
    new-instance v0, LX/0ZL;

    invoke-direct {v0, p1}, LX/0ZL;-><init>(I)V

    iput-object v0, p0, LX/0ZK;->a:LX/0ZL;

    .line 83183
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0ZK;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 83184
    return-void
.end method

.method private declared-synchronized b(LX/0ki;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEvent;)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ZK;->a:LX/0ZL;

    invoke-virtual {v0, p1}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 83186
    iget-object v0, p0, LX/0ZK;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 83187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TTEvent;>;"
        }
    .end annotation

    .prologue
    .line 83188
    iget-object v0, p0, LX/0ZK;->a:LX/0ZL;

    invoke-virtual {v0}, LX/0ZL;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0ki;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTEvent;)V"
        }
    .end annotation

    .prologue
    .line 83189
    invoke-direct {p0, p1}, LX/0ZK;->b(LX/0ki;)Ljava/util/Iterator;

    move-result-object v0

    .line 83190
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83191
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 83192
    :cond_0
    return-void
.end method
