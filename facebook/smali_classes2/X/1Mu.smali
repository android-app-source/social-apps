.class public LX/1Mu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Ck;

.field private b:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKey;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 236389
    iput-object p1, p0, LX/1Mu;->a:LX/1Ck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236390
    iput-object p3, p0, LX/1Mu;->b:LX/0Ve;

    .line 236391
    iput-object p2, p0, LX/1Mu;->c:Ljava/lang/Object;

    .line 236392
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 2

    .prologue
    .line 236384
    iget-object v0, p0, LX/1Mu;->a:LX/1Ck;

    iget-object v1, p0, LX/1Mu;->c:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1Ck;->a$redex0(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 236385
    monitor-enter p0

    .line 236386
    :try_start_0
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 236387
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Mu;->b:LX/0Ve;

    .line 236388
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 236393
    monitor-enter p0

    .line 236394
    :try_start_0
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 236395
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 236379
    iget-object v0, p0, LX/1Mu;->a:LX/1Ck;

    iget-object v1, p0, LX/1Mu;->c:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1Ck;->a$redex0(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 236380
    monitor-enter p0

    .line 236381
    :try_start_0
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    if-eqz v0, :cond_0

    .line 236382
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 236383
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 236374
    iget-object v0, p0, LX/1Mu;->a:LX/1Ck;

    iget-object v1, p0, LX/1Mu;->c:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1Ck;->a$redex0(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 236375
    monitor-enter p0

    .line 236376
    :try_start_0
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    if-eqz v0, :cond_0

    .line 236377
    iget-object v0, p0, LX/1Mu;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 236378
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
