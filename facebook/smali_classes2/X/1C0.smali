.class public LX/1C0;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 214661
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "browser/disabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->a:LX/0Tn;

    .line 214662
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "browser/seen_blues_clues_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->b:LX/0Tn;

    .line 214663
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "browser/first_seen_blues_clues_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->c:LX/0Tn;

    .line 214664
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "browser/last_mobile_sso_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->d:LX/0Tn;

    .line 214665
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "browser/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 214666
    sput-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "last_clear_data_date/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->f:LX/0Tn;

    .line 214667
    sget-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "always_prefetch/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->g:LX/0Tn;

    .line 214668
    sget-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "preview_regardless_network/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->h:LX/0Tn;

    .line 214669
    sget-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "saved_text_zoom_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->i:LX/0Tn;

    .line 214670
    sget-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "last_core_feature_log_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->j:LX/0Tn;

    .line 214671
    sget-object v0, LX/1C0;->e:LX/0Tn;

    const-string v1, "last_sync_to_main_proc_cookie_verify_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1C0;->k:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 214672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
