.class public LX/1eA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287378
    iput-object p1, p0, LX/1eA;->a:LX/0ad;

    .line 287379
    return-void
.end method

.method public static a(LX/0QB;)LX/1eA;
    .locals 4

    .prologue
    .line 287380
    const-class v1, LX/1eA;

    monitor-enter v1

    .line 287381
    :try_start_0
    sget-object v0, LX/1eA;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 287382
    sput-object v2, LX/1eA;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 287383
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287384
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 287385
    new-instance p0, LX/1eA;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/1eA;-><init>(LX/0ad;)V

    .line 287386
    move-object v0, p0

    .line 287387
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 287388
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1eA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287389
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 287390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 287391
    iget-object v0, p0, LX/1eA;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 287392
    iget-object v0, p0, LX/1eA;->a:LX/0ad;

    sget-char v1, LX/C5Z;->e:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1eA;->f:Ljava/lang/String;

    .line 287393
    :cond_0
    iget-object v0, p0, LX/1eA;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 287394
    iget-object v0, p0, LX/1eA;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 287395
    iget-object v0, p0, LX/1eA;->a:LX/0ad;

    sget-char v1, LX/C5Z;->f:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1eA;->g:Ljava/lang/String;

    .line 287396
    :cond_0
    iget-object v0, p0, LX/1eA;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 287397
    iget-object v0, p0, LX/1eA;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 287398
    iget-object v0, p0, LX/1eA;->a:LX/0ad;

    sget-short v1, LX/C5Z;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1eA;->d:Ljava/lang/Boolean;

    .line 287399
    :cond_0
    iget-object v0, p0, LX/1eA;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
