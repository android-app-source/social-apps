.class public final LX/0v4;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 157461
    new-instance v0, LX/0su;

    sget-object v1, LX/0qQ;->a:LX/0U1;

    sget-object v2, LX/0qQ;->d:LX/0U1;

    sget-object v3, LX/0qQ;->c:LX/0U1;

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/0v4;->a:LX/0sv;

    .line 157462
    sget-object v0, LX/0qQ;->a:LX/0U1;

    sget-object v1, LX/0qQ;->d:LX/0U1;

    sget-object v2, LX/0qQ;->b:LX/0U1;

    sget-object v3, LX/0qQ;->c:LX/0U1;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0v4;->b:LX/0Px;

    .line 157463
    sget-object v0, LX/0v4;->a:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0v4;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    .line 157464
    const-string v0, "home_stories_media"

    sget-object v1, LX/0v4;->b:LX/0Px;

    sget-object v2, LX/0v4;->c:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 157465
    iput-boolean p1, p0, LX/0v4;->d:Z

    .line 157466
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 157467
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 157468
    const-string v0, "home_stories_media"

    const-string v1, "dedup_key_index"

    sget-object v2, LX/0qQ;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x326dac5c

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x16ee83cf

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157469
    return-void
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 157470
    iget-boolean v0, p0, LX/0v4;->d:Z

    if-nez v0, :cond_0

    .line 157471
    const-string v0, "DELETE FROM home_stories_media"

    const v1, -0x3364211c    # -8.172112E7f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6cc2a484

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157472
    :cond_0
    return-void
.end method
