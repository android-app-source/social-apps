.class public LX/1MV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MW;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0p7;

.field private final c:LX/0oz;

.field private final d:LX/0So;

.field private final e:LX/1MY;

.field private final f:LX/1Mf;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0p7;LX/0oz;LX/0kb;LX/0So;LX/1MY;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235667
    iput-object p1, p0, LX/1MV;->a:LX/0Zb;

    .line 235668
    iput-object p2, p0, LX/1MV;->b:LX/0p7;

    .line 235669
    iput-object p3, p0, LX/1MV;->c:LX/0oz;

    .line 235670
    iput-object p5, p0, LX/1MV;->d:LX/0So;

    .line 235671
    iput-object p6, p0, LX/1MV;->e:LX/1MY;

    .line 235672
    new-instance v0, LX/1Mf;

    invoke-direct {v0, p1, p3, p4, p5}, LX/1Mf;-><init>(LX/0Zb;LX/0oz;LX/0kb;LX/0So;)V

    iput-object v0, p0, LX/1MV;->f:LX/1Mf;

    .line 235673
    return-void
.end method

.method public static a(LX/0QB;)LX/1MV;
    .locals 8

    .prologue
    .line 235674
    new-instance v1, LX/1MV;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0p7;->a(LX/0QB;)LX/0p7;

    move-result-object v3

    check-cast v3, LX/0p7;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static {p0}, LX/1MX;->a(LX/0QB;)LX/1MX;

    move-result-object v7

    check-cast v7, LX/1MY;

    invoke-direct/range {v1 .. v7}, LX/1MV;-><init>(LX/0Zb;LX/0p7;LX/0oz;LX/0kb;LX/0So;LX/1MY;)V

    .line 235675
    move-object v0, v1

    .line 235676
    return-object v0
.end method

.method private a(Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iW;LX/1iQ;LX/1hI;LX/0YR;)Lcom/facebook/http/executors/liger/LigerTraceEventHandler;
    .locals 14

    .prologue
    .line 235677
    new-instance v0, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;

    iget-object v1, p0, LX/1MV;->a:LX/0Zb;

    iget-object v7, p0, LX/1MV;->b:LX/0p7;

    iget-object v8, p0, LX/1MV;->c:LX/0oz;

    iget-object v9, p0, LX/1MV;->d:LX/0So;

    iget-object v12, p0, LX/1MV;->e:LX/1MY;

    iget-object v13, p0, LX/1MV;->f:LX/1Mf;

    move-object/from16 v2, p4

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v0 .. v13}, Lcom/facebook/http/executors/liger/LigerTraceEventHandler;-><init>(LX/0Zb;LX/1iW;Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iQ;LX/0p7;LX/0oz;LX/0So;LX/1hI;LX/0YR;LX/1MY;LX/1Mf;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic create(Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iW;LX/1iQ;LX/1hI;LX/0YR;)LX/6Xa;
    .locals 1

    .prologue
    .line 235678
    invoke-direct/range {p0 .. p7}, LX/1MV;->a(Ljava/lang/String;Lorg/apache/http/protocol/HttpContext;LX/0p3;LX/1iW;LX/1iQ;LX/1hI;LX/0YR;)Lcom/facebook/http/executors/liger/LigerTraceEventHandler;

    move-result-object v0

    return-object v0
.end method
