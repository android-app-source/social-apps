.class public LX/1iN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[[B

.field private static final e:[[B


# instance fields
.field private f:LX/1hi;

.field private g:B

.field private h:B

.field private i:J

.field private j:B

.field private k:Ljava/io/ByteArrayOutputStream;

.field private l:Ljava/io/DataOutputStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x6

    .line 298299
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "start"

    aput-object v1, v0, v4

    const-string v1, "executing"

    aput-object v1, v0, v5

    const-string v1, "got_result"

    aput-object v1, v0, v6

    const-string v1, "got_exception"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "done"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "fail"

    aput-object v2, v0, v1

    sput-object v0, LX/1iN;->a:[Ljava/lang/String;

    .line 298300
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "start"

    aput-object v1, v0, v4

    const-string v1, "got_eom"

    aput-object v1, v0, v5

    const-string v1, "got_error"

    aput-object v1, v0, v6

    const-string v1, "done"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "fail"

    aput-object v2, v0, v1

    sput-object v0, LX/1iN;->b:[Ljava/lang/String;

    .line 298301
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "handler_start"

    aput-object v1, v0, v4

    const-string v1, "got_result"

    aput-object v1, v0, v5

    const-string v1, "got_exception"

    aput-object v1, v0, v6

    const-string v1, "retry"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "eom"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "error"

    aput-object v2, v0, v1

    const-string v1, "none"

    aput-object v1, v0, v3

    const/4 v1, 0x7

    const-string v2, "request_started"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "handler_complete"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "response"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "body"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "upload"

    aput-object v2, v0, v1

    sput-object v0, LX/1iN;->c:[Ljava/lang/String;

    .line 298302
    new-array v0, v3, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [B

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    sput-object v0, LX/1iN;->d:[[B

    .line 298303
    const/4 v0, 0x5

    new-array v0, v0, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_6

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_7

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_8

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_9

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [B

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    sput-object v0, LX/1iN;->e:[[B

    return-void

    .line 298304
    :array_0
    .array-data 1
        0x1t
        0x5t
        0x5t
        -0x1t
        -0x1t
        0x4t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x5t
        0x2t
        0x3t
        0x0t
        -0x1t
        -0x1t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x5t
        0x5t
        0x5t
        0x5t
        0x4t
        0x4t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x5t
        0x5t
        0x5t
        0x0t
        0x4t
        0x4t
    .end array-data

    nop

    :array_4
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    nop

    :array_5
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    nop

    :array_6
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x1t
        0x2t
    .end array-data

    nop

    :array_7
    .array-data 1
        -0x1t
        0x3t
        0x3t
        0x0t
        0x4t
        0x4t
    .end array-data

    nop

    :array_8
    .array-data 1
        -0x1t
        0x3t
        0x3t
        0x0t
        0x4t
        0x4t
    .end array-data

    nop

    :array_9
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    nop

    :array_a
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method public constructor <init>(LX/1hi;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 298290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298291
    iput-byte v0, p0, LX/1iN;->g:B

    .line 298292
    iput-byte v0, p0, LX/1iN;->h:B

    .line 298293
    iput-object p1, p0, LX/1iN;->f:LX/1hi;

    .line 298294
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/1iN;->i:J

    .line 298295
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, LX/1iN;->k:Ljava/io/ByteArrayOutputStream;

    .line 298296
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, LX/1iN;->k:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, LX/1iN;->l:Ljava/io/DataOutputStream;

    .line 298297
    const/4 v0, 0x6

    invoke-static {p0, v0}, LX/1iN;->a(LX/1iN;B)V

    .line 298298
    return-void
.end method

.method private static a(B[Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 298286
    if-ltz p0, :cond_0

    array-length v0, p1

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    .line 298287
    :goto_0
    if-eqz v0, :cond_1

    aget-object v0, p1, p0

    :goto_1
    return-object v0

    .line 298288
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 298289
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "out of range ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static declared-synchronized a(LX/1iN;B)V
    .locals 2

    .prologue
    .line 298278
    monitor-enter p0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 298279
    :goto_0
    :try_start_0
    iget-byte v1, p0, LX/1iN;->j:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v1, :cond_2

    if-eqz v0, :cond_2

    .line 298280
    :goto_1
    monitor-exit p0

    return-void

    .line 298281
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 298282
    :cond_2
    :try_start_1
    iput-byte p1, p0, LX/1iN;->j:B

    .line 298283
    invoke-direct {p0, p1}, LX/1iN;->c(B)V

    .line 298284
    invoke-direct {p0}, LX/1iN;->s()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 298285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/1iN;B)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 298246
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, LX/1iN;->c(B)V

    .line 298247
    iput-byte p1, p0, LX/1iN;->j:B

    .line 298248
    iget-byte v2, p0, LX/1iN;->g:B

    if-ltz v2, :cond_4

    iget-byte v2, p0, LX/1iN;->g:B

    sget-object v3, LX/1iN;->d:[[B

    array-length v3, v3

    if-ge v2, v3, :cond_4

    move v2, v0

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 298249
    iget-byte v2, p0, LX/1iN;->h:B

    if-ltz v2, :cond_5

    iget-byte v2, p0, LX/1iN;->h:B

    sget-object v3, LX/1iN;->e:[[B

    array-length v3, v3

    if-ge v2, v3, :cond_5

    move v2, v0

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 298250
    if-ltz p1, :cond_6

    sget-object v2, LX/1iN;->d:[[B

    iget-byte v3, p0, LX/1iN;->g:B

    aget-object v2, v2, v3

    array-length v2, v2

    if-ge p1, v2, :cond_6

    move v2, v0

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 298251
    if-ltz p1, :cond_7

    sget-object v2, LX/1iN;->e:[[B

    iget-byte v3, p0, LX/1iN;->h:B

    aget-object v2, v2, v3

    array-length v2, v2

    if-ge p1, v2, :cond_7

    move v2, v0

    :goto_3
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 298252
    iget-byte v3, p0, LX/1iN;->g:B

    .line 298253
    iget-byte v4, p0, LX/1iN;->h:B

    .line 298254
    sget-object v2, LX/1iN;->d:[[B

    iget-byte v5, p0, LX/1iN;->g:B

    aget-object v2, v2, v5

    aget-byte v2, v2, p1

    .line 298255
    sget-object v5, LX/1iN;->e:[[B

    iget-byte v6, p0, LX/1iN;->h:B

    aget-object v5, v5, v6

    aget-byte v5, v5, p1

    .line 298256
    if-eq v2, v7, :cond_0

    .line 298257
    iput-byte v2, p0, LX/1iN;->g:B

    .line 298258
    :cond_0
    if-eq v5, v7, :cond_1

    .line 298259
    iput-byte v5, p0, LX/1iN;->h:B

    .line 298260
    :cond_1
    invoke-direct {p0}, LX/1iN;->s()V

    .line 298261
    iget-byte v2, p0, LX/1iN;->g:B

    if-ltz v2, :cond_8

    iget-byte v2, p0, LX/1iN;->g:B

    sget-object v5, LX/1iN;->d:[[B

    array-length v5, v5

    if-ge v2, v5, :cond_8

    move v2, v0

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 298262
    iget-byte v2, p0, LX/1iN;->h:B

    if-ltz v2, :cond_9

    iget-byte v2, p0, LX/1iN;->h:B

    sget-object v5, LX/1iN;->e:[[B

    array-length v5, v5

    if-ge v2, v5, :cond_9

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 298263
    iget-object v0, p0, LX/1iN;->f:LX/1hi;

    iget-byte v1, p0, LX/1iN;->g:B

    iget-byte v2, p0, LX/1iN;->h:B

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 298264
    if-eq v3, v1, :cond_2

    .line 298265
    const/4 v5, 0x5

    if-ne v1, v5, :cond_2

    .line 298266
    const-string v5, "Handler fail state"

    invoke-virtual {v0, v5, v7, v6}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 298267
    :cond_2
    if-eq v4, v2, :cond_3

    .line 298268
    const/4 v5, 0x4

    if-ne v2, v5, :cond_3

    .line 298269
    const-string v5, "Request fail state"

    invoke-virtual {v0, v5, v7, v6}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298270
    :cond_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v1

    .line 298271
    goto/16 :goto_0

    :cond_5
    move v2, v1

    .line 298272
    goto :goto_1

    :cond_6
    move v2, v1

    .line 298273
    goto :goto_2

    :cond_7
    move v2, v1

    .line 298274
    goto :goto_3

    :cond_8
    move v2, v1

    .line 298275
    goto :goto_4

    :cond_9
    move v0, v1

    .line 298276
    goto :goto_5

    .line 298277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(B)V
    .locals 4

    .prologue
    .line 298241
    :try_start_0
    iget-object v0, p0, LX/1iN;->l:Ljava/io/DataOutputStream;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 298242
    iget-object v0, p0, LX/1iN;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298243
    return-void

    .line 298244
    :catch_0
    move-exception v0

    .line 298245
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static d(B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 298240
    sget-object v0, LX/1iN;->b:[Ljava/lang/String;

    invoke-static {p0, v0}, LX/1iN;->a(B[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 298239
    sget-object v0, LX/1iN;->a:[Ljava/lang/String;

    invoke-static {p0, v0}, LX/1iN;->a(B[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(B)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 298305
    sget-object v0, LX/1iN;->c:[Ljava/lang/String;

    invoke-static {p0, v0}, LX/1iN;->a(B[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 298234
    :try_start_0
    iget-object v0, p0, LX/1iN;->l:Ljava/io/DataOutputStream;

    iget-byte v1, p0, LX/1iN;->g:B

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 298235
    iget-object v0, p0, LX/1iN;->l:Ljava/io/DataOutputStream;

    iget-byte v1, p0, LX/1iN;->h:B

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298236
    return-void

    .line 298237
    :catch_0
    move-exception v0

    .line 298238
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 298227
    monitor-enter p0

    :try_start_0
    iget-byte v2, p0, LX/1iN;->g:B

    if-eq v2, v5, :cond_0

    iget-byte v2, p0, LX/1iN;->h:B

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    :cond_0
    move v3, v0

    .line 298228
    :goto_0
    iget-byte v2, p0, LX/1iN;->g:B

    const/4 v4, 0x5

    if-eq v2, v4, :cond_1

    iget-byte v2, p0, LX/1iN;->h:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v5, :cond_3

    :cond_1
    move v2, v0

    .line 298229
    :goto_1
    if-eqz v3, :cond_4

    if-nez v2, :cond_4

    :goto_2
    monitor-exit p0

    return v0

    :cond_2
    move v3, v1

    .line 298230
    goto :goto_0

    :cond_3
    move v2, v1

    .line 298231
    goto :goto_1

    :cond_4
    move v0, v1

    .line 298232
    goto :goto_2

    .line 298233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 298226
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1iN;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()B
    .locals 1

    .prologue
    .line 298225
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1iN;->g:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298224
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1iN;->g:B

    invoke-static {v0}, LX/1iN;->e(B)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()B
    .locals 1

    .prologue
    .line 298223
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1iN;->h:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298204
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, LX/1iN;->h:B

    invoke-static {v0}, LX/1iN;->d(B)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized r()Ljava/lang/String;
    .locals 6

    .prologue
    .line 298205
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298206
    :try_start_1
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, LX/1iN;->k:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 298207
    :goto_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->available()I

    move-result v2

    if-lez v2, :cond_1

    .line 298208
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 298209
    const-string v2, " |"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298210
    :cond_0
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    iget-wide v4, p0, LX/1iN;->i:J

    sub-long/2addr v2, v4

    .line 298211
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 298212
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 298213
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, LX/1iN;->f(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298214
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 298215
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, LX/1iN;->e(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298216
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 298217
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, LX/1iN;->d(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298218
    :catch_0
    :try_start_2
    const-string v1, " ... unexpected EOF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298219
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 298220
    :catch_1
    move-exception v0

    .line 298221
    :try_start_3
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
