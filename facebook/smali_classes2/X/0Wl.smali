.class public LX/0Wl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/inject/AssistedProviderWithInjector",
        "<TT;>;",
        "LX/0QB;"
    }
.end annotation


# instance fields
.field public mInjector:LX/0QB;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInjector()LX/0QA;
    .locals 1

    .prologue
    .line 76655
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getBinders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76656
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getBinders()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getInjectorThreadStack()LX/0S7;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76657
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 76658
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)TS;"
        }
    .end annotation

    .prologue
    .line 76659
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)TS;"
        }
    .end annotation

    .prologue
    .line 76660
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76652
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76661
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76665
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazy(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 76666
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 76664
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getLazySet(Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 76663
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getLazySet(Ljava/lang/Class;Ljava/lang/Class;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 76653
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;>;)",
            "Lcom/facebook/inject/AssistedProvider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76654
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    return-object v0
.end method

.method public getProcessIdentifier()I
    .locals 1

    .prologue
    .line 76638
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getProcessIdentifier()I

    move-result v0

    return v0
.end method

.method public getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76639
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76640
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76641
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjector()LX/0R6;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76642
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeAwareInjectorInternal()LX/0QC;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 76643
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public getScopeUnawareInjector()LX/0QD;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76644
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    return-object v0
.end method

.method public getSet(LX/0RI;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76645
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(LX/0RI;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76646
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1}, LX/0QC;->getSet(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/util/Set",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 76647
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->getSet(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 76648
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 76649
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QC;->getSetProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TS;>;>;"
        }
    .end annotation

    .prologue
    .line 76650
    invoke-virtual {p0}, LX/0Wl;->getScopeAwareInjectorInternal()LX/0QC;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QC;->getSetProvider(Ljava/lang/Class;Ljava/lang/Class;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 76651
    iget-object v0, p0, LX/0Wl;->mInjector:LX/0QB;

    invoke-interface {v0, p1, p2}, LX/0QC;->hasBinding(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
