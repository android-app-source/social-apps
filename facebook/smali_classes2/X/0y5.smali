.class public LX/0y5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/0y5;


# instance fields
.field public final a:LX/0y6;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/14s;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0qR;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14u;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/data/freshfeed/ranking/ClientRankingSignalStore$ClientRankingSignalMutator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0y6;LX/0qR;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164082
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 164083
    iput-object v0, p0, LX/0y5;->d:LX/0Ot;

    .line 164084
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 164085
    iput-object v0, p0, LX/0y5;->e:LX/0Ot;

    .line 164086
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xfa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0y5;->b:Ljava/util/HashMap;

    .line 164087
    iput-object p1, p0, LX/0y5;->a:LX/0y6;

    .line 164088
    iput-object p2, p0, LX/0y5;->c:LX/0qR;

    .line 164089
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/0y5;->f:Ljava/util/List;

    .line 164090
    return-void
.end method

.method public static a(LX/0QB;)LX/0y5;
    .locals 5

    .prologue
    .line 164066
    sget-object v0, LX/0y5;->g:LX/0y5;

    if-nez v0, :cond_1

    .line 164067
    const-class v1, LX/0y5;

    monitor-enter v1

    .line 164068
    :try_start_0
    sget-object v0, LX/0y5;->g:LX/0y5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164069
    if-eqz v2, :cond_0

    .line 164070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164071
    new-instance p0, LX/0y5;

    invoke-static {v0}, LX/0y6;->a(LX/0QB;)LX/0y6;

    move-result-object v3

    check-cast v3, LX/0y6;

    invoke-static {v0}, LX/0qR;->a(LX/0QB;)LX/0qR;

    move-result-object v4

    check-cast v4, LX/0qR;

    invoke-direct {p0, v3, v4}, LX/0y5;-><init>(LX/0y6;LX/0qR;)V

    .line 164072
    const/16 v3, 0x603

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x132e

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 164073
    iput-object v3, p0, LX/0y5;->d:LX/0Ot;

    iput-object v4, p0, LX/0y5;->e:LX/0Ot;

    .line 164074
    move-object v0, p0

    .line 164075
    sput-object v0, LX/0y5;->g:LX/0y5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164076
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164077
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164078
    :cond_1
    sget-object v0, LX/0y5;->g:LX/0y5;

    return-object v0

    .line 164079
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/14s;LX/14t;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 164053
    monitor-enter p0

    .line 164054
    :try_start_0
    iget-object v1, p2, LX/14t;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v3, v1

    .line 164055
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16v;

    .line 164056
    iget-object v1, v0, LX/16v;->c:Ljava/lang/String;

    move-object v1, v1

    .line 164057
    const-string v5, "VIDEO"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164058
    iget-object v1, p0, LX/0y5;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/19w;

    .line 164059
    iget-object v5, v0, LX/16v;->b:Ljava/lang/String;

    move-object v0, v5

    .line 164060
    invoke-virtual {v1, v0}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164061
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/14s;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164062
    :goto_1
    monitor-exit p0

    return-void

    .line 164063
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 164064
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p1, v0}, LX/14s;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 164065
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(LX/14s;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V
    .locals 1

    .prologue
    .line 164045
    iget v0, p1, LX/14s;->mLiveVideoState:I

    move v0, v0

    .line 164046
    if-nez v0, :cond_0

    .line 164047
    iget-object v0, p0, LX/0y5;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14u;

    .line 164048
    invoke-static {v0, p2}, LX/14u;->c(LX/14u;Lcom/facebook/feed/model/ClientFeedUnitEdge;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 164049
    const/4 p0, 0x1

    .line 164050
    :goto_0
    move v0, p0

    .line 164051
    invoke-virtual {p1, v0}, LX/14s;->c(I)V

    .line 164052
    :cond_0
    return-void

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/14s;
    .locals 3

    .prologue
    .line 164038
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0y5;->b(Ljava/lang/String;)LX/14s;

    move-result-object v0

    .line 164039
    if-nez v0, :cond_0

    .line 164040
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/0y5;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;I)LX/14s;

    move-result-object v0

    .line 164041
    if-nez v0, :cond_0

    .line 164042
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t create ClientRankingSignal for edge: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164043
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164044
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/facebook/feed/model/ClientFeedUnitEdge;I)LX/14s;
    .locals 13

    .prologue
    .line 164000
    monitor-enter p0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 164001
    iget-object v0, p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v1, v0

    .line 164002
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 164003
    if-nez v2, :cond_2

    .line 164004
    const/4 v0, 0x0

    .line 164005
    :cond_0
    :goto_1
    monitor-exit p0

    return-object v0

    .line 164006
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 164007
    :cond_2
    :try_start_1
    invoke-virtual {p0, v2}, LX/0y5;->b(Ljava/lang/String;)LX/14s;

    move-result-object v0

    .line 164008
    if-nez v0, :cond_3

    .line 164009
    new-instance v0, LX/14s;

    invoke-direct {v0}, LX/14s;-><init>()V

    .line 164010
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v3

    invoke-virtual {v0, v3}, LX/14s;->b(I)V

    .line 164011
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v4

    .line 164012
    iget-wide v6, v0, LX/14s;->mRankingWeight:D

    cmpl-double v6, v4, v6

    if-eqz v6, :cond_4

    .line 164013
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/14s;->a:Z

    .line 164014
    :cond_4
    iput-wide v4, v0, LX/14s;->mRankingWeight:D

    .line 164015
    iget-wide v6, p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->u:J

    move-wide v4, v6

    .line 164016
    iget-wide v6, v0, LX/14s;->mFetchedAt:J

    cmp-long v6, v4, v6

    if-eqz v6, :cond_5

    .line 164017
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/14s;->a:Z

    .line 164018
    :cond_5
    iput-wide v4, v0, LX/14s;->mFetchedAt:J

    .line 164019
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v3

    .line 164020
    if-nez v3, :cond_6

    iget-object v4, v0, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    if-eqz v4, :cond_7

    :cond_6
    if-eqz v3, :cond_7

    iget-object v4, v0, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 164021
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/14s;->a:Z

    .line 164022
    :cond_7
    iput-object v3, v0, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    .line 164023
    iput p2, v0, LX/14s;->mResultType:I

    .line 164024
    iget-object v3, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v3, v2}, LX/0qR;->c(Ljava/lang/String;)LX/14t;

    move-result-object v3

    .line 164025
    invoke-virtual {v0, v3}, LX/14s;->a(LX/14t;)V

    .line 164026
    invoke-direct {p0, v0, v3}, LX/0y5;->a(LX/14s;LX/14t;)V

    .line 164027
    invoke-direct {p0, v0, p1}, LX/0y5;->b(LX/14s;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V

    .line 164028
    iget-object v6, p0, LX/0y5;->f:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bo0;

    .line 164029
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, LX/Bo0;->b(LX/Bo0;Ljava/lang/String;)J

    move-result-wide v8

    .line 164030
    iget-wide v10, v0, LX/14s;->mLiveCommentAgeMs:J

    cmp-long v10, v10, v8

    if-eqz v10, :cond_8

    .line 164031
    const/4 v10, 0x1

    iput-boolean v10, v0, LX/14s;->a:Z

    .line 164032
    :cond_8
    iput-wide v8, v0, LX/14s;->mLiveCommentAgeMs:J

    .line 164033
    goto :goto_2

    .line 164034
    :cond_9
    iget-object v3, p0, LX/0y5;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164035
    if-eqz v1, :cond_0

    iget-object v3, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v3, v1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 164036
    iget-object v3, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v3, v1, v2}, LX/0qR;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 164037
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)LX/14s;
    .locals 2

    .prologue
    .line 163968
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0y5;->b:Ljava/util/HashMap;

    iget-object v1, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v1, p1}, LX/0qR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 163995
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/0y5;->a(Ljava/lang/String;)LX/14s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 163996
    if-nez v0, :cond_0

    .line 163997
    :goto_0
    monitor-exit p0

    return-void

    .line 163998
    :cond_0
    :try_start_1
    invoke-virtual {v0, p2}, LX/14s;->b(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163999
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 163985
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v0, p2}, LX/0qR;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 163986
    invoke-virtual {p0, v3}, LX/0y5;->b(Ljava/lang/String;)LX/14s;

    move-result-object v4

    .line 163987
    iget-object v5, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v5, v3}, LX/0qR;->c(Ljava/lang/String;)LX/14t;

    move-result-object v3

    .line 163988
    invoke-virtual {v3, p1, p2, p3}, LX/14t;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 163989
    if-eqz v4, :cond_0

    .line 163990
    invoke-virtual {v4, v3}, LX/14s;->a(LX/14t;)V

    .line 163991
    invoke-direct {p0, v4, v3}, LX/0y5;->a(LX/14s;LX/14t;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163992
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163993
    :cond_1
    monitor-exit p0

    return-void

    .line 163994
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 163975
    monitor-enter p0

    if-nez p1, :cond_1

    .line 163976
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 163977
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v0, p1}, LX/0qR;->c(Ljava/lang/String;)LX/14t;

    move-result-object v0

    .line 163978
    invoke-virtual {v0, p3, p2, p4}, LX/14t;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 163979
    iget-object v1, p0, LX/0y5;->c:LX/0qR;

    invoke-virtual {v1, p2, p1}, LX/0qR;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163980
    invoke-virtual {p0, p1}, LX/0y5;->b(Ljava/lang/String;)LX/14s;

    move-result-object v1

    .line 163981
    if-eqz v1, :cond_0

    .line 163982
    invoke-virtual {v1, v0}, LX/14s;->a(LX/14t;)V

    .line 163983
    invoke-direct {p0, v1, v0}, LX/0y5;->a(LX/14s;LX/14t;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 163984
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)LX/14s;
    .locals 1

    .prologue
    .line 163974
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0y5;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 163969
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/0y5;->b(Ljava/lang/String;)LX/14s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 163970
    if-nez v0, :cond_0

    .line 163971
    :goto_0
    monitor-exit p0

    return-void

    .line 163972
    :cond_0
    :try_start_1
    invoke-virtual {v0, p2}, LX/14s;->c(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
