.class public LX/0pF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0pF;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/analytics2/logger/Uploader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 144047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144048
    iput-object p1, p0, LX/0pF;->b:Landroid/content/Context;

    .line 144049
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0pF;->c:Ljava/util/ArrayList;

    .line 144050
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0pF;->d:Ljava/util/ArrayList;

    .line 144051
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0pF;->e:Ljava/util/ArrayList;

    .line 144052
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/0pF;
    .locals 3

    .prologue
    .line 144006
    const-class v1, LX/0pF;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0pF;->a:LX/0pF;

    if-nez v0, :cond_0

    .line 144007
    new-instance v0, LX/0pF;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0pF;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/0pF;->a:LX/0pF;

    .line 144008
    :cond_0
    sget-object v0, LX/0pF;->a:LX/0pF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 144009
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(LX/0pF;Ljava/util/ArrayList;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 144037
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 144038
    :goto_0
    if-ge v1, v2, :cond_1

    .line 144039
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 144040
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 144041
    :goto_1
    monitor-exit p0

    return-object v0

    .line 144042
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 144043
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    invoke-virtual {p2, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 144044
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/0pF;->b:Landroid/content/Context;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 144045
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 144046
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 144031
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0pF;->a(LX/0pF;Ljava/util/ArrayList;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 144032
    :goto_0
    monitor-exit p0

    return-object v0

    .line 144033
    :catch_0
    move-exception v0

    .line 144034
    :try_start_1
    const-string v1, "ContextConstructorHelper"

    const-string v2, "Cannot find class: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144035
    const/4 v0, 0x0

    goto :goto_0

    .line 144036
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 144017
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/0pF;->a(LX/0pF;Ljava/util/ArrayList;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 144018
    :goto_0
    monitor-exit p0

    return-object v0

    .line 144019
    :catch_0
    move-exception v0

    .line 144020
    :try_start_1
    const-string v1, "ContextConstructorHelper"

    const-string v2, "Cannot find class: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144021
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 144022
    :catch_1
    move-exception v0

    .line 144023
    const-string v1, "ContextConstructorHelper"

    const-string v2, "IllegalAccessException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 144024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 144025
    :catch_2
    move-exception v0

    .line 144026
    :try_start_2
    const-string v1, "ContextConstructorHelper"

    const-string v2, "InstantiationException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 144027
    :catch_3
    move-exception v0

    .line 144028
    const-string v1, "ContextConstructorHelper"

    const-string v2, "NoSuchMethodException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 144029
    :catch_4
    move-exception v0

    .line 144030
    const-string v1, "ContextConstructorHelper"

    const-string v2, "InvocationTargetException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/flexiblesampling/SamplingPolicyConfig;
    .locals 1

    .prologue
    .line 144016
    iget-object v0, p0, LX/0pF;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, LX/0pF;->b(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/analytics2/logger/HandlerThreadFactory;
    .locals 3

    .prologue
    .line 144011
    iget-object v0, p0, LX/0pF;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, LX/0pF;->b(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics2/logger/HandlerThreadFactory;

    .line 144012
    if-nez v0, :cond_0

    .line 144013
    new-instance v0, Lcom/facebook/analytics2/logger/DefaultHandlerThreadFactory;

    iget-object v1, p0, LX/0pF;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/analytics2/logger/DefaultHandlerThreadFactory;-><init>(Landroid/content/Context;)V

    .line 144014
    const-string v1, "ContextConstructorHelper"

    const-string v2, "Unable to create instance for HandlerThreadFactory"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 144015
    :cond_0
    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/analytics2/logger/Uploader;
    .locals 1

    .prologue
    .line 144010
    iget-object v0, p0, LX/0pF;->e:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, LX/0pF;->a(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics2/logger/Uploader;

    return-object v0
.end method
