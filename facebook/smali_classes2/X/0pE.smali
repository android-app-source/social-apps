.class public final LX/0pE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0q7;

.field public final b:LX/0mI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0q6;

.field public final d:LX/0mk;

.field public e:LX/2DB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/2DB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/40D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/HandlerThread;LX/0mI;LX/0q6;LX/0mk;LX/0mU;LX/40D;)V
    .locals 2
    .param p2    # LX/0mI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0mU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/40D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 143993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143994
    new-instance v0, LX/0q7;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p5}, LX/0q7;-><init>(LX/0pE;Landroid/os/Looper;LX/0mU;)V

    iput-object v0, p0, LX/0pE;->a:LX/0q7;

    .line 143995
    iput-object p2, p0, LX/0pE;->b:LX/0mI;

    .line 143996
    iput-object p3, p0, LX/0pE;->c:LX/0q6;

    .line 143997
    iput-object p4, p0, LX/0pE;->d:LX/0mk;

    .line 143998
    iput-object p6, p0, LX/0pE;->g:LX/40D;

    .line 143999
    return-void
.end method


# virtual methods
.method public final a(LX/0q8;)V
    .locals 1

    .prologue
    .line 144000
    iget-object v0, p0, LX/0pE;->a:LX/0q7;

    .line 144001
    const/4 p0, 0x2

    invoke-virtual {v0, p0, p1}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0q7;->sendMessage(Landroid/os/Message;)Z

    .line 144002
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 144003
    iget-object v0, p0, LX/0pE;->a:LX/0q7;

    .line 144004
    const/4 p0, 0x4

    invoke-virtual {v0, p0, p1}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0q7;->sendMessage(Landroid/os/Message;)Z

    .line 144005
    return-void
.end method
