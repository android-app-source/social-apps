.class public LX/1Fj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1Fl;

.field private final b:LX/1Fk;


# direct methods
.method public constructor <init>(LX/1Fk;LX/1Fl;)V
    .locals 0

    .prologue
    .line 224540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224541
    iput-object p1, p0, LX/1Fj;->b:LX/1Fk;

    .line 224542
    iput-object p2, p0, LX/1Fj;->a:LX/1Fl;

    .line 224543
    return-void
.end method

.method private static a(LX/1Fj;Ljava/io/InputStream;LX/1gN;)LX/1FK;
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 224527
    iget-object v0, p0, LX/1Fj;->a:LX/1Fl;

    invoke-virtual {v0, p1, p2}, LX/1Fl;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 224528
    invoke-virtual {p2}, LX/1gN;->c()LX/1FK;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;)LX/1FK;
    .locals 2

    .prologue
    .line 224537
    new-instance v0, LX/1gN;

    iget-object v1, p0, LX/1Fj;->b:LX/1Fk;

    invoke-direct {v0, v1}, LX/1gN;-><init>(LX/1Fk;)V

    .line 224538
    :try_start_0
    invoke-static {p0, p1, v0}, LX/1Fj;->a(LX/1Fj;Ljava/io/InputStream;LX/1gN;)LX/1FK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 224539
    invoke-virtual {v0}, LX/1gN;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1gN;->close()V

    throw v1
.end method

.method public final a(Ljava/io/InputStream;I)LX/1FK;
    .locals 2

    .prologue
    .line 224544
    new-instance v0, LX/1gN;

    iget-object v1, p0, LX/1Fj;->b:LX/1Fk;

    invoke-direct {v0, v1, p2}, LX/1gN;-><init>(LX/1Fk;I)V

    .line 224545
    :try_start_0
    invoke-static {p0, p1, v0}, LX/1Fj;->a(LX/1Fj;Ljava/io/InputStream;LX/1gN;)LX/1FK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 224546
    invoke-virtual {v0}, LX/1gN;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1gN;->close()V

    throw v1
.end method

.method public final a([B)LX/1FK;
    .locals 3

    .prologue
    .line 224530
    new-instance v1, LX/1gN;

    iget-object v0, p0, LX/1Fj;->b:LX/1Fk;

    array-length v2, p1

    invoke-direct {v1, v0, v2}, LX/1gN;-><init>(LX/1Fk;I)V

    .line 224531
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p1

    invoke-virtual {v1, p1, v0, v2}, LX/1gN;->write([BII)V

    .line 224532
    invoke-virtual {v1}, LX/1gN;->c()LX/1FK;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 224533
    invoke-virtual {v1}, LX/1gN;->close()V

    return-object v0

    .line 224534
    :catch_0
    move-exception v0

    .line 224535
    :try_start_1
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224536
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1gN;->close()V

    throw v0
.end method

.method public final a()LX/1gO;
    .locals 2

    .prologue
    .line 224529
    new-instance v0, LX/1gN;

    iget-object v1, p0, LX/1Fj;->b:LX/1Fk;

    invoke-direct {v0, v1}, LX/1gN;-><init>(LX/1Fk;)V

    return-object v0
.end method

.method public final a(I)LX/1gO;
    .locals 2

    .prologue
    .line 224526
    new-instance v0, LX/1gN;

    iget-object v1, p0, LX/1Fj;->b:LX/1Fk;

    invoke-direct {v0, v1, p1}, LX/1gN;-><init>(LX/1Fk;I)V

    return-object v0
.end method
