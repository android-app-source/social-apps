.class public final LX/1nr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field public a:Landroid/text/TextPaint;

.field public b:I

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:Landroid/content/res/ColorStateList;

.field public f:F

.field public g:F

.field public h:Z

.field public i:Landroid/text/TextUtils$TruncateAt;

.field public j:Z

.field public k:I

.field public l:Landroid/text/Layout$Alignment;

.field public m:LX/0zr;

.field public n:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 317515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317516
    new-instance v0, LX/1ns;

    invoke-direct {v0, v2}, LX/1ns;-><init>(I)V

    iput-object v0, p0, LX/1nr;->a:Landroid/text/TextPaint;

    .line 317517
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/1nr;->f:F

    .line 317518
    const/4 v0, 0x0

    iput v0, p0, LX/1nr;->g:F

    .line 317519
    iput-boolean v2, p0, LX/1nr;->h:Z

    .line 317520
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    .line 317521
    iput-boolean v1, p0, LX/1nr;->j:Z

    .line 317522
    const v0, 0x7fffffff

    iput v0, p0, LX/1nr;->k:I

    .line 317523
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/1nr;->l:Landroid/text/Layout$Alignment;

    .line 317524
    sget-object v0, LX/0zo;->c:LX/0zr;

    iput-object v0, p0, LX/1nr;->m:LX/0zr;

    .line 317525
    iput-boolean v1, p0, LX/1nr;->n:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 317526
    iget-boolean v0, p0, LX/1nr;->n:Z

    if-eqz v0, :cond_0

    .line 317527
    new-instance v0, LX/1ns;

    iget-object v1, p0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-direct {v0, v1}, LX/1ns;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, LX/1nr;->a:Landroid/text/TextPaint;

    .line 317528
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1nr;->n:Z

    .line 317529
    :cond_0
    return-void
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 317530
    iget-object v0, p0, LX/1nr;->a:Landroid/text/TextPaint;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 317531
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/1nr;->b:I

    add-int/2addr v0, v3

    .line 317532
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/1nr;->c:I

    add-int/2addr v0, v3

    .line 317533
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/1nr;->f:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int/2addr v0, v3

    .line 317534
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, LX/1nr;->g:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int/2addr v0, v3

    .line 317535
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/1nr;->h:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    .line 317536
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0}, Landroid/text/TextUtils$TruncateAt;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    .line 317537
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/1nr;->j:Z

    if-eqz v3, :cond_4

    :goto_3
    add-int/2addr v0, v2

    .line 317538
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/1nr;->k:I

    add-int/2addr v0, v2

    .line 317539
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/1nr;->l:Landroid/text/Layout$Alignment;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/1nr;->l:Landroid/text/Layout$Alignment;

    invoke-virtual {v0}, Landroid/text/Layout$Alignment;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 317540
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/1nr;->m:LX/0zr;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/1nr;->m:LX/0zr;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 317541
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/1nr;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 317542
    return v0

    :cond_1
    move v0, v1

    .line 317543
    goto :goto_0

    :cond_2
    move v0, v1

    .line 317544
    goto :goto_1

    :cond_3
    move v0, v1

    .line 317545
    goto :goto_2

    :cond_4
    move v2, v1

    .line 317546
    goto :goto_3

    :cond_5
    move v0, v1

    .line 317547
    goto :goto_4

    :cond_6
    move v0, v1

    .line 317548
    goto :goto_5
.end method
