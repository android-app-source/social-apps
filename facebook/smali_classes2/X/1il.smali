.class public LX/1il;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ki;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UsingDefaultJsonDeserializer"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0So;

.field private final c:LX/1Gl;

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:J

.field public p:LX/1iW;

.field public q:J

.field public r:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299062
    const-class v0, LX/1il;

    sput-object v0, LX/1il;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/1Gl;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 299063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299064
    iput-object p1, p0, LX/1il;->b:LX/0So;

    .line 299065
    iput-object p2, p0, LX/1il;->c:LX/1Gl;

    .line 299066
    iput-wide v2, p0, LX/1il;->d:J

    .line 299067
    const/4 v0, -0x1

    iput v0, p0, LX/1il;->n:I

    .line 299068
    iput-wide v2, p0, LX/1il;->o:J

    .line 299069
    iput-wide v2, p0, LX/1il;->q:J

    .line 299070
    return-void
.end method

.method public static a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 298969
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 298970
    const/4 v0, 0x0

    move-object v1, p0

    .line 298971
    :goto_0
    if-eqz v1, :cond_2

    .line 298972
    if-eqz v0, :cond_0

    .line 298973
    const-string v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298974
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298975
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298976
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    move-object v0, v1

    move-object v1, p0

    goto :goto_0

    .line 298977
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 298978
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/237;Ljava/lang/String;LX/1iX;)V
    .locals 6

    .prologue
    .line 299057
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299058
    iget-wide v4, p2, LX/1iX;->a:J

    move-wide v0, v4

    .line 299059
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 299060
    invoke-virtual {p0, p1, v0, v1}, LX/237;->add(Ljava/lang/String;J)LX/237;

    .line 299061
    :cond_0
    return-void
.end method

.method public static c(LX/1il;)V
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 299049
    iget-object v0, p0, LX/1il;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299050
    iget-object v0, p0, LX/1il;->p:LX/1iW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1il;->p:LX/1iW;

    .line 299051
    iget-object v1, v0, LX/1iW;->e:Ljava/lang/String;

    move-object v0, v1

    .line 299052
    if-eqz v0, :cond_0

    .line 299053
    iget-object v0, p0, LX/1il;->p:LX/1iW;

    .line 299054
    iget-object v1, v0, LX/1iW;->e:Ljava/lang/String;

    move-object v0, v1

    .line 299055
    iput-object v0, p0, LX/1il;->i:Ljava/lang/String;

    .line 299056
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 9

    .prologue
    .line 298980
    iget-object v0, p0, LX/1il;->e:Ljava/lang/String;

    move-object v0, v0

    .line 298981
    new-instance v1, LX/237;

    const/4 v2, 0x0

    invoke-direct {v1, v0}, LX/237;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 298982
    const-wide/16 v5, -0x1

    .line 298983
    iget-wide v1, p0, LX/1il;->d:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_0

    .line 298984
    iget-wide v7, p0, LX/1il;->q:J

    move-wide v1, v7

    .line 298985
    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_c

    .line 298986
    iget-wide v7, p0, LX/1il;->q:J

    move-wide v1, v7

    .line 298987
    :goto_0
    iget-wide v3, p0, LX/1il;->d:J

    sub-long/2addr v1, v3

    .line 298988
    const-string v3, "duration_ms"

    invoke-virtual {v0, v3, v1, v2}, LX/237;->add(Ljava/lang/String;J)LX/237;

    .line 298989
    :cond_0
    iget-object v1, p0, LX/1il;->f:Ljava/lang/String;

    move-object v1, v1

    .line 298990
    if-eqz v1, :cond_1

    .line 298991
    const-string v1, "request_name"

    .line 298992
    iget-object v2, p0, LX/1il;->f:Ljava/lang/String;

    move-object v2, v2

    .line 298993
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 298994
    :cond_1
    iget-object v1, p0, LX/1il;->g:Ljava/lang/String;

    move-object v1, v1

    .line 298995
    if-eqz v1, :cond_2

    .line 298996
    const-string v1, "request_priority"

    .line 298997
    iget-object v2, p0, LX/1il;->g:Ljava/lang/String;

    move-object v2, v2

    .line 298998
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 298999
    :cond_2
    const-string v1, "creation_to_stage_time"

    .line 299000
    iget-wide v7, p0, LX/1il;->r:J

    move-wide v3, v7

    .line 299001
    invoke-virtual {v0, v1, v3, v4}, LX/237;->add(Ljava/lang/String;J)LX/237;

    .line 299002
    iget-object v1, p0, LX/1il;->h:Ljava/lang/String;

    move-object v1, v1

    .line 299003
    if-eqz v1, :cond_3

    .line 299004
    const-string v1, "host"

    .line 299005
    iget-object v2, p0, LX/1il;->h:Ljava/lang/String;

    move-object v2, v2

    .line 299006
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299007
    :cond_3
    iget-object v1, p0, LX/1il;->i:Ljava/lang/String;

    move-object v1, v1

    .line 299008
    if-eqz v1, :cond_4

    .line 299009
    const-string v1, "ip_addr"

    .line 299010
    iget-object v2, p0, LX/1il;->i:Ljava/lang/String;

    move-object v2, v2

    .line 299011
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299012
    :cond_4
    iget-object v1, p0, LX/1il;->j:Ljava/lang/String;

    move-object v1, v1

    .line 299013
    if-eqz v1, :cond_5

    .line 299014
    const-string v1, "uri_md5"

    .line 299015
    iget-object v2, p0, LX/1il;->j:Ljava/lang/String;

    move-object v2, v2

    .line 299016
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299017
    :cond_5
    iget v1, p0, LX/1il;->n:I

    move v1, v1

    .line 299018
    const/4 v2, -0x1

    if-eq v1, v2, :cond_6

    .line 299019
    const-string v1, "response_code"

    .line 299020
    iget v2, p0, LX/1il;->n:I

    move v2, v2

    .line 299021
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    .line 299022
    :cond_6
    iget-object v1, p0, LX/1il;->p:LX/1iW;

    move-object v1, v1

    .line 299023
    if-eqz v1, :cond_7

    .line 299024
    const-string v1, "body_bytes_sent"

    .line 299025
    iget-object v2, p0, LX/1il;->p:LX/1iW;

    move-object v2, v2

    .line 299026
    iget-object v2, v2, LX/1iW;->requestBodyBytes:LX/1iX;

    invoke-static {v0, v1, v2}, LX/1il;->a(LX/237;Ljava/lang/String;LX/1iX;)V

    .line 299027
    const-string v1, "body_bytes_read"

    .line 299028
    iget-object v2, p0, LX/1il;->p:LX/1iW;

    move-object v2, v2

    .line 299029
    iget-object v2, v2, LX/1iW;->responseBodyBytes:LX/1iX;

    invoke-static {v0, v1, v2}, LX/1il;->a(LX/237;Ljava/lang/String;LX/1iX;)V

    .line 299030
    const-string v1, "bytes_read_by_app"

    .line 299031
    iget-object v2, p0, LX/1il;->p:LX/1iW;

    move-object v2, v2

    .line 299032
    iget-object v2, v2, LX/1iW;->bytesReadByApp:LX/1iX;

    invoke-static {v0, v1, v2}, LX/1il;->a(LX/237;Ljava/lang/String;LX/1iX;)V

    .line 299033
    :cond_7
    iget-wide v7, p0, LX/1il;->o:J

    move-wide v1, v7

    .line 299034
    cmp-long v1, v1, v5

    if-eqz v1, :cond_8

    .line 299035
    const-string v1, "content_length"

    .line 299036
    iget-wide v7, p0, LX/1il;->o:J

    move-wide v3, v7

    .line 299037
    invoke-virtual {v0, v1, v3, v4}, LX/237;->add(Ljava/lang/String;J)LX/237;

    .line 299038
    :cond_8
    iget-object v1, p0, LX/1il;->l:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 299039
    const-string v1, "using_spdy"

    iget-object v2, p0, LX/1il;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299040
    :cond_9
    iget-object v1, p0, LX/1il;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 299041
    const-string v1, "connection_quality"

    iget-object v2, p0, LX/1il;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299042
    :cond_a
    iget-object v1, p0, LX/1il;->k:Ljava/lang/String;

    move-object v1, v1

    .line 299043
    if-eqz v1, :cond_b

    .line 299044
    const-string v1, "exception"

    .line 299045
    iget-object v2, p0, LX/1il;->k:Ljava/lang/String;

    move-object v2, v2

    .line 299046
    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 299047
    :cond_b
    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 299048
    :cond_c
    iget-object v1, p0, LX/1il;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    goto/16 :goto_0
.end method

.method public getStartTime()J
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time"
    .end annotation

    .prologue
    .line 298979
    iget-wide v0, p0, LX/1il;->d:J

    return-wide v0
.end method
