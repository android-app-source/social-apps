.class public final LX/0v3;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 157441
    new-instance v0, LX/0su;

    sget-object v1, LX/0pp;->a:LX/0U1;

    sget-object v2, LX/0pp;->d:LX/0U1;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/0v3;->a:LX/0sv;

    .line 157442
    sget-object v0, LX/0pp;->a:LX/0U1;

    sget-object v1, LX/0pp;->b:LX/0U1;

    sget-object v2, LX/0pp;->c:LX/0U1;

    sget-object v3, LX/0pp;->d:LX/0U1;

    sget-object v4, LX/0pp;->e:LX/0U1;

    sget-object v5, LX/0pp;->f:LX/0U1;

    sget-object v6, LX/0pp;->g:LX/0U1;

    sget-object v7, LX/0pp;->h:LX/0U1;

    sget-object v8, LX/0pp;->i:LX/0U1;

    sget-object v9, LX/0pp;->j:LX/0U1;

    sget-object v10, LX/0pp;->k:LX/0U1;

    sget-object v11, LX/0pp;->l:LX/0U1;

    const/16 v12, 0xe

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/0pp;->m:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/0pp;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/0pp;->o:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, LX/0pp;->p:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, LX/0pp;->q:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, LX/0pp;->r:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, LX/0pp;->s:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, LX/0pp;->t:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, LX/0pp;->u:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, LX/0pp;->v:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xa

    sget-object v14, LX/0pp;->w:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xb

    sget-object v14, LX/0pp;->x:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xc

    sget-object v14, LX/0pp;->y:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xd

    sget-object v14, LX/0pp;->z:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0v3;->b:LX/0Px;

    .line 157443
    sget-object v0, LX/0v3;->a:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0v3;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    .line 157444
    const-string v0, "home_stories"

    sget-object v1, LX/0v3;->b:LX/0Px;

    sget-object v2, LX/0v3;->c:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 157445
    iput-boolean p1, p0, LX/0v3;->d:Z

    .line 157446
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 157447
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 157448
    const-string v0, "home_stories"

    const-string v1, "home_stories_cursor_index"

    sget-object v2, LX/0pp;->c:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x3fb0b8cb

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x164cdafd

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157449
    const-string v0, "home_stories"

    const-string v1, "home_stories_sort_key_index"

    sget-object v2, LX/0pp;->e:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x6ab5ff4f

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5aa6a629

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157450
    const-string v0, "home_stories"

    const-string v1, "home_stories_seen_state_index"

    sget-object v2, LX/0pp;->i:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x1acdbc6b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x65541999

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157451
    const-string v0, "home_stories"

    const-string v1, "home_stories_client_sort_key_index"

    sget-object v2, LX/0pp;->v:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x7aa5138a

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xc1bff71

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157452
    const-string v0, "home_stories"

    const-string v1, "home_stories_client_cursor_key_index"

    sget-object v2, LX/0pp;->w:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x31ff3645

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x63da2101

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157453
    const-string v0, "home_stories"

    const-string v1, "home_stories_story_type_index"

    sget-object v2, LX/0pp;->n:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0xda2ac4a

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x140124ff

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157454
    const-string v0, "home_stories"

    const-string v1, "cache_id_index"

    sget-object v2, LX/0pp;->x:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0xdfebcff

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x460d8137

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157455
    return-void
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 157456
    iget-boolean v0, p0, LX/0v3;->d:Z

    if-nez v0, :cond_0

    .line 157457
    const-string v0, "DELETE FROM home_stories"

    const v1, 0x3bd5e423

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6c3c0557

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157458
    sget-object v0, LX/00a;->a:LX/00a;

    move-object v0, v0

    .line 157459
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/00a;->a(Ljava/lang/String;)V

    .line 157460
    :cond_0
    return-void
.end method
