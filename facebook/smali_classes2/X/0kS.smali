.class public LX/0kS;
.super LX/0T0;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0kS;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2o9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2o9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127153
    invoke-direct {p0}, LX/0T0;-><init>()V

    .line 127154
    iput-object p1, p0, LX/0kS;->a:LX/0Or;

    .line 127155
    iput-object p2, p0, LX/0kS;->b:LX/0Ot;

    .line 127156
    return-void
.end method

.method public static a(LX/0QB;)LX/0kS;
    .locals 5

    .prologue
    .line 127157
    sget-object v0, LX/0kS;->c:LX/0kS;

    if-nez v0, :cond_1

    .line 127158
    const-class v1, LX/0kS;

    monitor-enter v1

    .line 127159
    :try_start_0
    sget-object v0, LX/0kS;->c:LX/0kS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127160
    if-eqz v2, :cond_0

    .line 127161
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 127162
    new-instance v3, LX/0kS;

    const/16 v4, 0x14d1

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0xbd7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/0kS;-><init>(LX/0Or;LX/0Ot;)V

    .line 127163
    move-object v0, v3

    .line 127164
    sput-object v0, LX/0kS;->c:LX/0kS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127165
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127166
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127167
    :cond_1
    sget-object v0, LX/0kS;->c:LX/0kS;

    return-object v0

    .line 127168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 127170
    iget-object v0, p0, LX/0kS;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127171
    :goto_0
    return-void

    .line 127172
    :cond_0
    iget-object v0, p0, LX/0kS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2o9;

    .line 127173
    iget-object p0, v0, LX/2o9;->c:LX/0yH;

    sget-object p1, LX/0yY;->VPN_DATA_CONTROL:LX/0yY;

    invoke-virtual {p0, p1}, LX/0yH;->a(LX/0yY;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 127174
    :cond_1
    :goto_1
    goto :goto_0

    .line 127175
    :cond_2
    iget-object p0, v0, LX/2o9;->d:Ljava/lang/ref/WeakReference;

    if-eqz p0, :cond_1

    .line 127176
    iget-object p0, v0, LX/2o9;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;

    .line 127177
    if-eqz p0, :cond_1

    .line 127178
    iget-boolean p1, p0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move p1, p1

    .line 127179
    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 127180
    iget-boolean p1, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move p1, p1

    .line 127181
    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    :goto_2
    move p1, p1

    .line 127182
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result p2

    .line 127183
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result p4

    .line 127184
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    .line 127185
    packed-switch p3, :pswitch_data_0

    .line 127186
    iget-object p0, v0, LX/2o9;->b:LX/2oA;

    sget-object p1, LX/2o9;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p4, "onAcitivtyResult unknown resultCode: "

    invoke-direct {p2, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/2oA;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127187
    :goto_3
    const/4 p0, 0x0

    iput-object p0, v0, LX/2o9;->d:Ljava/lang/ref/WeakReference;

    goto :goto_1

    .line 127188
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->j()V

    goto :goto_3

    .line 127189
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->k()V

    goto :goto_3

    .line 127190
    :pswitch_2
    check-cast p0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object p1, LX/6YN;->FETCH_UPSELL:LX/6YN;

    invoke-virtual {p0, p1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1edc
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
