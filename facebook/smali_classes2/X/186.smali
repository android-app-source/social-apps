.class public LX/186;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final c:Ljava/nio/charset/Charset;

.field public static final synthetic m:Z


# instance fields
.field public a:Ljava/nio/ByteBuffer;

.field public b:I

.field public d:I

.field public e:[I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:I

.field public h:[I

.field public i:I

.field public j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation
.end field

.field public k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public l:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204999
    const-class v0, LX/186;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/186;->m:Z

    .line 205000
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/186;->c:Ljava/nio/charset/Charset;

    return-void

    .line 205001
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 205002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205003
    iput v0, p0, LX/186;->d:I

    .line 205004
    iput-object v3, p0, LX/186;->e:[I

    .line 205005
    iput v2, p0, LX/186;->f:I

    .line 205006
    const/16 v1, 0x10

    new-array v1, v1, [I

    iput-object v1, p0, LX/186;->h:[I

    .line 205007
    iput v2, p0, LX/186;->i:I

    .line 205008
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/186;->j:Ljava/util/ArrayList;

    .line 205009
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/186;->k:Ljava/util/ArrayList;

    .line 205010
    iput-object v3, p0, LX/186;->l:[I

    .line 205011
    if-gtz p1, :cond_0

    move p1, v0

    .line 205012
    :cond_0
    iput p1, p0, LX/186;->b:I

    .line 205013
    invoke-static {p1}, LX/186;->e(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    .line 205014
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 205015
    iget v0, p0, LX/186;->d:I

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 205016
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 205017
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: file identifier must be length 4"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 205018
    :cond_0
    const/4 v0, 0x3

    :goto_0
    if-ltz v0, :cond_1

    .line 205019
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, LX/186;->a(B)V

    .line 205020
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 205021
    :cond_1
    invoke-virtual {p0, p1}, LX/186;->b(I)V

    .line 205022
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 205023
    return-void
.end method

.method private static b(Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 205024
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    .line 205025
    const/high16 v0, -0x40000000    # -2.0f

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    .line 205026
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: cannot grow buffer beyond 2 gigabytes."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 205027
    :cond_0
    const/high16 v0, 0x10000

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 205028
    sub-int v1, p1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 205029
    add-int v1, v2, v0

    .line 205030
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 205031
    :try_start_0
    invoke-static {v1}, LX/186;->e(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move p1, v1

    .line 205032
    :goto_0
    sub-int v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 205033
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 205034
    return-object v0

    .line 205035
    :catch_0
    invoke-static {p1}, LX/186;->e(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/186;III)V
    .locals 2

    .prologue
    .line 205036
    invoke-static {p0}, LX/186;->g(LX/186;)V

    .line 205037
    const/4 v0, 0x4

    mul-int v1, p1, p2

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 205038
    mul-int v0, p1, p2

    invoke-virtual {p0, p3, v0}, LX/186;->a(II)V

    .line 205039
    return-void
.end method

.method public static b(LX/186;S)V
    .locals 2

    .prologue
    .line 205040
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 205041
    return-void
.end method

.method public static b(Lcom/facebook/flatbuffers/Flattenable;)[B
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205042
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B
    .locals 3
    .param p1    # LX/16a;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 205043
    if-eqz p1, :cond_1

    invoke-interface {p1, p0}, LX/16a;->a(Lcom/facebook/flatbuffers/Flattenable;)S

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 205044
    :cond_0
    :goto_0
    return-object v0

    .line 205045
    :cond_1
    new-instance v2, LX/186;

    const/16 v1, 0x800

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 205046
    if-nez p1, :cond_2

    invoke-virtual {v2, p0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 205047
    :goto_1
    if-lez v1, :cond_0

    .line 205048
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 205049
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v0

    goto :goto_0

    .line 205050
    :cond_2
    invoke-virtual {v2, p0, p1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v1

    goto :goto_1
.end method

.method public static c(LX/186;II)I
    .locals 1

    .prologue
    .line 205051
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 205052
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, LX/186;->b(II)V

    .line 205053
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p2}, LX/186;->b(II)V

    .line 205054
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method private static e(I)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 205055
    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 205056
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 205057
    return-object v0
.end method

.method private static g(LX/186;)V
    .locals 2

    .prologue
    .line 205058
    iget-object v0, p0, LX/186;->e:[I

    if-eqz v0, :cond_0

    .line 205059
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "FlatBuffers: object serialization must not be nested."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 205060
    :cond_0
    return-void
.end method

.method public static g(LX/186;I)V
    .locals 2

    .prologue
    .line 205061
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 205062
    return-void
.end method

.method public static h(LX/186;I)I
    .locals 1

    .prologue
    .line 205063
    invoke-static {p0, p1}, LX/186;->g(LX/186;I)V

    .line 205064
    invoke-virtual {p0}, LX/186;->b()I

    move-result v0

    return v0
.end method

.method public static i(LX/186;I)V
    .locals 2

    .prologue
    .line 205065
    iget-object v0, p0, LX/186;->e:[I

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205066
    iget v0, p0, LX/186;->f:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 205067
    iget-object v0, p0, LX/186;->e:[I

    invoke-virtual {p0}, LX/186;->b()I

    move-result v1

    aput v1, v0, p1

    .line 205068
    return-void

    .line 205069
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/util/Map;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 205070
    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 205071
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 205072
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 205073
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 205074
    instance-of v2, v0, LX/2au;

    if-eqz v2, :cond_0

    .line 205075
    check-cast v0, LX/2au;

    iget v0, v0, LX/2au;->a:I

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    goto :goto_0

    .line 205076
    :cond_0
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 205077
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, LX/186;->a(III)V

    goto :goto_0

    .line 205078
    :cond_1
    instance-of v2, v0, Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 205079
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    goto :goto_0

    .line 205080
    :cond_2
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 205081
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/186;->a(IZ)V

    goto :goto_0

    .line 205082
    :cond_3
    instance-of v2, v0, Ljava/lang/Double;

    if-eqz v2, :cond_4

    .line 205083
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    goto :goto_0

    .line 205084
    :cond_4
    instance-of v2, v0, Ljava/lang/Enum;

    if-eqz v2, :cond_5

    .line 205085
    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {p0, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    goto :goto_0

    .line 205086
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Object "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0nE;->a(Ljava/lang/String;)Ljava/lang/AssertionError;

    goto/16 :goto_0

    .line 205087
    :cond_6
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/flatbuffers/Flattenable;)I
    .locals 1
    .param p1    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 205088
    if-nez p1, :cond_0

    .line 205089
    const/4 v0, 0x0

    .line 205090
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, p0}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/186;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 205091
    if-nez p1, :cond_0

    .line 205092
    :goto_0
    return v0

    .line 205093
    :cond_0
    invoke-interface {p2, p1}, LX/16a;->a(Lcom/facebook/flatbuffers/Flattenable;)S

    move-result v1

    .line 205094
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 205095
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not resolve reference type for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205096
    :cond_1
    invoke-interface {p1, p0}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/186;)I

    move-result v2

    .line 205097
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 205098
    invoke-virtual {p0, v0, v1, v0}, LX/186;->a(ISI)V

    .line 205099
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 205100
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Enum;)I
    .locals 1

    .prologue
    .line 205101
    if-eqz p1, :cond_0

    .line 205102
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 205103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/4Bv;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "LX/4Bv",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 205104
    if-nez p1, :cond_0

    .line 205105
    const/4 v0, 0x0

    .line 205106
    :goto_0
    return v0

    :cond_0
    invoke-interface {p2, p1, p0}, LX/4Bv;->a(Ljava/lang/Object;LX/186;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 205107
    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)I
    .locals 3

    .prologue
    .line 205108
    invoke-static {p0}, LX/186;->g(LX/186;)V

    .line 205109
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    .line 205110
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 205111
    iget-object v1, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/186;->b:I

    sub-int v0, v2, v0

    iput v0, p0, LX/186;->b:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 205112
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 205113
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-eqz v1, :cond_0

    .line 205114
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected to be position 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205115
    :cond_0
    iget-object v1, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 205116
    invoke-virtual {p0}, LX/186;->b()I

    move-result v0

    sub-int/2addr v0, p2

    return v0
.end method

.method public final a(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 205117
    const/4 v0, 0x1

    const/4 v1, 0x4

    .line 205118
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_2

    .line 205119
    :cond_0
    invoke-static {p0, v1, v3, v1}, LX/186;->b(LX/186;III)V

    .line 205120
    add-int/lit8 v1, v3, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_1

    .line 205121
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0, v1}, LX/186;->g(LX/186;I)V

    .line 205122
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    .line 205123
    :cond_1
    invoke-static {p0, v3}, LX/186;->h(LX/186;I)I

    move-result v1

    .line 205124
    :goto_1
    move v0, v1

    .line 205125
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/util/List;LX/16a;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "LX/16a;",
            "Z)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 205126
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    if-eqz p3, :cond_1

    .line 205127
    :cond_0
    :goto_0
    return v0

    .line 205128
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [I

    .line 205129
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 205130
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v0, p2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)I

    move-result v0

    aput v0, v3, v1

    move v1, v2

    .line 205131
    goto :goto_1

    .line 205132
    :cond_2
    invoke-virtual {p0, v3, p3}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;LX/4Bv;Z)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "LX/4Bv",
            "<TT;>;Z)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 205133
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    .line 205134
    :cond_0
    :goto_0
    return v1

    .line 205135
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 205136
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v0, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 205137
    add-int/lit8 v2, v0, 0x1

    if-eqz v3, :cond_2

    invoke-interface {p2, v3, p0}, LX/4Bv;->a(Ljava/lang/Object;LX/186;)I

    move-result v3

    :goto_2
    aput v3, v4, v0

    move v0, v2

    .line 205138
    goto :goto_1

    :cond_2
    move v3, v1

    .line 205139
    goto :goto_2

    .line 205140
    :cond_3
    invoke-virtual {p0, v4, p3}, LX/186;->a([IZ)I

    move-result v1

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Z)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;Z)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 205141
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 205142
    :cond_0
    :goto_0
    return v2

    .line 205143
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 205144
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 205145
    add-int/lit8 v3, v1, 0x1

    if-eqz v0, :cond_2

    invoke-interface {v0, p0}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/186;)I

    move-result v0

    :goto_2
    aput v0, v4, v1

    move v1, v3

    .line 205146
    goto :goto_1

    :cond_2
    move v0, v2

    .line 205147
    goto :goto_2

    .line 205148
    :cond_3
    invoke-virtual {p0, v4, p2}, LX/186;->a([IZ)I

    move-result v2

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;Z)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 205149
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 205150
    :goto_0
    return v0

    .line 205151
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 205152
    new-instance v1, LX/4C1;

    invoke-direct {v1, v0}, LX/4C1;-><init>(Ljava/util/Set;)V

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 205153
    new-instance v2, LX/4C3;

    invoke-direct {v2, v0}, LX/4C3;-><init>(Ljava/util/Set;)V

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 205154
    invoke-static {p0, v1, v0}, LX/186;->c(LX/186;II)I

    move-result v0

    goto :goto_0
.end method

.method public final a([I)I
    .locals 3

    .prologue
    .line 204978
    const/4 v0, 0x1

    const/4 v2, 0x4

    .line 204979
    if-eqz p1, :cond_2

    array-length v1, p1

    if-nez v1, :cond_0

    if-nez v0, :cond_2

    .line 204980
    :cond_0
    array-length v1, p1

    invoke-static {p0, v2, v1, v2}, LX/186;->b(LX/186;III)V

    .line 204981
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_1

    .line 204982
    aget v2, p1, v1

    invoke-static {p0, v2}, LX/186;->g(LX/186;I)V

    .line 204983
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 204984
    :cond_1
    array-length v1, p1

    invoke-static {p0, v1}, LX/186;->h(LX/186;I)I

    move-result v1

    .line 204985
    :goto_1
    move v0, v1

    .line 204986
    move v0, v0

    .line 204987
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a([IZ)I
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 204988
    if-eqz p1, :cond_3

    array-length v0, p1

    if-nez v0, :cond_0

    if-nez p2, :cond_3

    .line 204989
    :cond_0
    array-length v0, p1

    invoke-static {p0, v1, v0, v1}, LX/186;->b(LX/186;III)V

    .line 204990
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 204991
    aget v1, p1, v0

    if-nez v1, :cond_1

    .line 204992
    const/4 p2, 0x0

    .line 204993
    const/4 v1, 0x4

    invoke-virtual {p0, v1, p2}, LX/186;->a(II)V

    .line 204994
    invoke-static {p0, p2}, LX/186;->g(LX/186;I)V

    .line 204995
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 204996
    :cond_1
    aget v1, p1, v0

    invoke-virtual {p0, v1}, LX/186;->b(I)V

    goto :goto_1

    .line 204997
    :cond_2
    array-length v0, p1

    invoke-static {p0, v0}, LX/186;->h(LX/186;I)I

    move-result v0

    .line 204998
    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a([Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 204853
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204854
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 204855
    :cond_0
    :goto_0
    move v0, v1

    .line 204856
    return v0

    .line 204857
    :cond_1
    array-length v2, p1

    new-array v2, v2, [I

    .line 204858
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_2

    .line 204859
    aget-object v3, p1, v1

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v1

    .line 204860
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 204861
    :cond_2
    invoke-virtual {p0, v2, v0}, LX/186;->a([IZ)I

    move-result v1

    goto :goto_0
.end method

.method public final a(B)V
    .locals 2

    .prologue
    .line 204850
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204851
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 204852
    return-void
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 204847
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204848
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1, p2}, Ljava/nio/ByteBuffer;->putDouble(ID)Ljava/nio/ByteBuffer;

    .line 204849
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 204844
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204845
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x4

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putFloat(IF)Ljava/nio/ByteBuffer;

    .line 204846
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 204841
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204842
    invoke-static {p0, p1}, LX/186;->g(LX/186;I)V

    .line 204843
    return-void
.end method

.method public final a(IDD)V
    .locals 2

    .prologue
    .line 204837
    cmpl-double v0, p2, p4

    if-eqz v0, :cond_0

    .line 204838
    invoke-virtual {p0, p2, p3}, LX/186;->a(D)V

    .line 204839
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204840
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 204826
    iget v0, p0, LX/186;->d:I

    if-le p1, v0, :cond_0

    iput p1, p0, LX/186;->d:I

    .line 204827
    :cond_0
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LX/186;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    xor-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    .line 204828
    add-int v1, v0, p1

    add-int/2addr v1, p2

    .line 204829
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    iget v3, p0, LX/186;->b:I

    sub-int/2addr v2, v3

    .line 204830
    :goto_0
    iget v3, p0, LX/186;->b:I

    if-ge v3, v1, :cond_1

    .line 204831
    iget-object v3, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    .line 204832
    iget-object v4, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    add-int v5, v1, v2

    invoke-static {v4, v5}, LX/186;->b(Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    .line 204833
    iget v4, p0, LX/186;->b:I

    iget-object v5, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v5

    sub-int v3, v5, v3

    add-int/2addr v3, v4

    iput v3, p0, LX/186;->b:I

    goto :goto_0

    .line 204834
    :cond_1
    const/4 v2, 0x0

    .line 204835
    move v1, v2

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v3, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, LX/186;->b:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, LX/186;->b:I

    invoke-virtual {v3, v4, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 204836
    :cond_2
    return-void
.end method

.method public final a(III)V
    .locals 0

    .prologue
    .line 204822
    if-eq p2, p3, :cond_0

    .line 204823
    invoke-virtual {p0, p2}, LX/186;->a(I)V

    .line 204824
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204825
    :cond_0
    return-void
.end method

.method public final a(IJJ)V
    .locals 2

    .prologue
    .line 204818
    cmp-long v0, p2, p4

    if-eqz v0, :cond_0

    .line 204819
    invoke-virtual {p0, p2, p3}, LX/186;->a(J)V

    .line 204820
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204821
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/Enum;)V
    .locals 1

    .prologue
    .line 204814
    if-eqz p2, :cond_0

    .line 204815
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, LX/186;->a(S)V

    .line 204816
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204817
    :cond_0
    return-void
.end method

.method public final a(ISI)V
    .locals 0

    .prologue
    .line 204810
    if-eq p2, p3, :cond_0

    .line 204811
    invoke-virtual {p0, p2}, LX/186;->a(S)V

    .line 204812
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204813
    :cond_0
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 204804
    if-eqz p2, :cond_0

    .line 204805
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204806
    if-eq v0, v1, :cond_0

    .line 204807
    invoke-virtual {p0, v0}, LX/186;->a(B)V

    .line 204808
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204809
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 204801
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204802
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1, p1, p2}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 204803
    return-void
.end method

.method public final a(S)V
    .locals 2

    .prologue
    .line 204798
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204799
    invoke-static {p0, p1}, LX/186;->b(LX/186;S)V

    .line 204800
    return-void
.end method

.method public final a([BII)V
    .locals 2

    .prologue
    .line 204862
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/186;->b:I

    sub-int/2addr v1, p3

    iput v1, p0, LX/186;->b:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 204863
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 204864
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 204865
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p0, LX/186;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 204866
    if-nez p1, :cond_0

    .line 204867
    :goto_0
    return v0

    .line 204868
    :cond_0
    sget-object v1, LX/186;->c:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    .line 204869
    invoke-virtual {p0, v0}, LX/186;->a(B)V

    .line 204870
    array-length v2, v1

    invoke-static {p0, v3, v2, v3}, LX/186;->b(LX/186;III)V

    .line 204871
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/186;->b:I

    array-length v4, v1

    sub-int/2addr v3, v4

    iput v3, p0, LX/186;->b:I

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 204872
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    array-length v3, v1

    invoke-virtual {v2, v1, v0, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 204873
    array-length v0, v1

    invoke-static {p0, v0}, LX/186;->h(LX/186;I)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 204874
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/186;->b(Ljava/util/List;Z)I

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/List;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 204875
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    if-eqz p2, :cond_1

    .line 204876
    :cond_0
    :goto_0
    return v0

    .line 204877
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [I

    .line 204878
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204879
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    aput v0, v3, v1

    move v1, v2

    .line 204880
    goto :goto_1

    .line 204881
    :cond_2
    invoke-virtual {p0, v3, p2}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 204882
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/186;->a(II)V

    .line 204883
    sget-boolean v0, LX/186;->m:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/186;->b()I

    move-result v0

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204884
    :cond_0
    invoke-virtual {p0}, LX/186;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x4

    .line 204885
    invoke-static {p0, v0}, LX/186;->g(LX/186;I)V

    .line 204886
    return-void
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 204887
    if-eqz p2, :cond_0

    .line 204888
    invoke-virtual {p0, p2}, LX/186;->b(I)V

    .line 204889
    invoke-static {p0, p1}, LX/186;->i(LX/186;I)V

    .line 204890
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 204891
    iget v0, p0, LX/186;->d:I

    return v0
.end method

.method public final c(Ljava/util/List;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/List",
            "<TE;>;)I"
        }
    .end annotation

    .prologue
    .line 204892
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 204893
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 204894
    :cond_0
    :goto_0
    move v0, v3

    .line 204895
    return v0

    .line 204896
    :cond_1
    new-array v5, v1, [I

    .line 204897
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    .line 204898
    if-nez v1, :cond_2

    .line 204899
    add-int/lit8 v1, v2, 0x1

    aput v3, v5, v2

    move v2, v1

    goto :goto_1

    .line 204900
    :cond_2
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    aput v1, v5, v2

    move v2, v4

    .line 204901
    goto :goto_1

    .line 204902
    :cond_3
    invoke-virtual {p0, v5, v0}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 204903
    invoke-static {p0}, LX/186;->g(LX/186;)V

    .line 204904
    iget-object v0, p0, LX/186;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 204905
    iget-object v0, p0, LX/186;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    array-length v0, v0

    if-lt v0, p1, :cond_2

    .line 204906
    iget-object v0, p0, LX/186;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, LX/186;->e:[I

    .line 204907
    iget-object v0, p0, LX/186;->e:[I

    invoke-static {v0, v2, p1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 204908
    iget-object v0, p0, LX/186;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 204909
    :cond_0
    iget-object v0, p0, LX/186;->e:[I

    if-nez v0, :cond_1

    .line 204910
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/186;->e:[I

    .line 204911
    :cond_1
    iput p1, p0, LX/186;->f:I

    .line 204912
    invoke-virtual {p0}, LX/186;->b()I

    move-result v0

    iput v0, p0, LX/186;->g:I

    .line 204913
    return-void

    .line 204914
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public final d()I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 204915
    sget-boolean v0, LX/186;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/186;->e:[I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204916
    :cond_0
    invoke-virtual {p0, v1}, LX/186;->a(I)V

    .line 204917
    invoke-virtual {p0}, LX/186;->b()I

    move-result v3

    .line 204918
    iget v0, p0, LX/186;->f:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 204919
    iget-object v0, p0, LX/186;->e:[I

    aget v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/186;->e:[I

    aget v0, v0, v2

    sub-int v0, v3, v0

    .line 204920
    :goto_1
    const/16 v4, 0x7fff

    if-le v0, v4, :cond_2

    .line 204921
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Object size exceeded 32K limit during flatbuffer serialzation"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 204922
    goto :goto_1

    .line 204923
    :cond_2
    int-to-short v0, v0

    .line 204924
    invoke-virtual {p0, v0}, LX/186;->a(S)V

    .line 204925
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 204926
    :cond_3
    iget v0, p0, LX/186;->g:I

    sub-int v0, v3, v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, LX/186;->a(S)V

    .line 204927
    iget v0, p0, LX/186;->f:I

    add-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x2

    int-to-short v0, v0

    invoke-virtual {p0, v0}, LX/186;->a(S)V

    move v0, v1

    .line 204928
    :goto_2
    iget v2, p0, LX/186;->i:I

    if-ge v0, v2, :cond_8

    .line 204929
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    iget-object v4, p0, LX/186;->h:[I

    aget v4, v4, v0

    sub-int v4, v2, v4

    .line 204930
    iget v5, p0, LX/186;->b:I

    .line 204931
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v6

    .line 204932
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v2

    if-ne v6, v2, :cond_5

    .line 204933
    const/4 v2, 0x2

    :goto_3
    if-ge v2, v6, :cond_4

    .line 204934
    iget-object v7, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    add-int v8, v4, v2

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v7

    iget-object v8, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    add-int v9, v5, v2

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v8

    if-ne v7, v8, :cond_5

    .line 204935
    add-int/lit8 v2, v2, 0x2

    goto :goto_3

    .line 204936
    :cond_4
    iget-object v2, p0, LX/186;->h:[I

    aget v0, v2, v0

    .line 204937
    :goto_4
    if-eqz v0, :cond_6

    .line 204938
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    sub-int/2addr v2, v3

    iput v2, p0, LX/186;->b:I

    .line 204939
    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget v4, p0, LX/186;->b:I

    sub-int/2addr v0, v3

    invoke-virtual {v2, v4, v0}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 204940
    :goto_5
    iget-object v0, p0, LX/186;->j:Ljava/util/ArrayList;

    iget-object v2, p0, LX/186;->e:[I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204941
    const/4 v0, 0x0

    iput-object v0, p0, LX/186;->e:[I

    .line 204942
    iput v1, p0, LX/186;->f:I

    .line 204943
    return v3

    .line 204944
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 204945
    :cond_6
    iget v0, p0, LX/186;->i:I

    iget-object v2, p0, LX/186;->h:[I

    array-length v2, v2

    if-ne v0, v2, :cond_7

    iget-object v0, p0, LX/186;->h:[I

    iget v2, p0, LX/186;->i:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, LX/186;->h:[I

    .line 204946
    :cond_7
    iget-object v0, p0, LX/186;->h:[I

    iget v2, p0, LX/186;->i:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, LX/186;->i:I

    invoke-virtual {p0}, LX/186;->b()I

    move-result v4

    aput v4, v0, v2

    .line 204947
    iget-object v0, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    iget-object v2, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/186;->b()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v0, v2, v4}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method public final d(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 204948
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204949
    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    if-nez v0, :cond_5

    .line 204950
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v3, v2, [S

    move v2, v1

    .line 204951
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 204952
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 204953
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    int-to-short v1, v1

    aput-short v1, v3, v2

    .line 204954
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 204955
    :cond_1
    const/4 v1, -0x1

    aput-short v1, v3, v2

    goto :goto_1

    .line 204956
    :cond_2
    const/4 v2, 0x2

    .line 204957
    if-eqz v3, :cond_6

    array-length v1, v3

    if-nez v1, :cond_3

    if-nez v0, :cond_6

    .line 204958
    :cond_3
    array-length v1, v3

    invoke-static {p0, v2, v1, v2}, LX/186;->b(LX/186;III)V

    .line 204959
    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_4

    .line 204960
    aget-short v2, v3, v1

    invoke-static {p0, v2}, LX/186;->b(LX/186;S)V

    .line 204961
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 204962
    :cond_4
    array-length v1, v3

    invoke-static {p0, v1}, LX/186;->h(LX/186;I)I

    move-result v1

    .line 204963
    :goto_3
    move v1, v1

    .line 204964
    :cond_5
    move v0, v1

    .line 204965
    return v0

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final d(I)V
    .locals 4

    .prologue
    .line 204966
    const-string v0, "FLAT"

    invoke-direct {p0, p1, v0}, LX/186;->a(ILjava/lang/String;)V

    .line 204967
    invoke-virtual {p0}, LX/186;->b()I

    move-result v2

    .line 204968
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/186;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 204969
    iget-object v3, p0, LX/186;->k:Ljava/util/ArrayList;

    iget-object v0, p0, LX/186;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 204970
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 204971
    :cond_0
    return-void
.end method

.method public final e()[B
    .locals 4

    .prologue
    .line 204972
    iget v0, p0, LX/186;->b:I

    iget-object v1, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    iget v2, p0, LX/186;->b:I

    sub-int/2addr v1, v2

    .line 204973
    new-array v2, v1, [B

    .line 204974
    iget-object v3, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 204975
    iget-object v3, p0, LX/186;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 204976
    move-object v0, v2

    .line 204977
    return-object v0
.end method
