.class public LX/1AU;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/1Iv;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210503
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 210504
    return-void
.end method


# virtual methods
.method public final a(LX/1EM;)LX/1Iv;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Dispatcher::",
            "LX/0fp;",
            ">(",
            "LX/1EM;",
            ")",
            "LX/1Iv",
            "<TDispatcher;>;"
        }
    .end annotation

    .prologue
    .line 210505
    new-instance v0, LX/1Iv;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0oa;->a(LX/0QB;)LX/0Ot;

    move-result-object v5

    const/16 v1, 0x2d2

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v1, 0x6b5

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    invoke-static {p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v9

    check-cast v9, LX/0pV;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v10

    check-cast v10, LX/0Zr;

    const-class v1, LX/0pS;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/0pS;

    const-class v1, LX/1Iw;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1Iw;

    const-class v1, LX/1Ix;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1Ix;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v14}, LX/1Iv;-><init>(LX/1EM;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0pJ;LX/0pV;LX/0Zr;LX/0pS;LX/1Iw;LX/1Ix;LX/0ad;)V

    .line 210506
    return-object v0
.end method
