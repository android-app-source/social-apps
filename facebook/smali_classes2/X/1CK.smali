.class public LX/1CK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1CK;


# instance fields
.field private final a:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216065
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/1CK;->a:Landroid/util/LruCache;

    .line 216066
    return-void
.end method

.method public static a(LX/0QB;)LX/1CK;
    .locals 3

    .prologue
    .line 216080
    sget-object v0, LX/1CK;->b:LX/1CK;

    if-nez v0, :cond_1

    .line 216081
    const-class v1, LX/1CK;

    monitor-enter v1

    .line 216082
    :try_start_0
    sget-object v0, LX/1CK;->b:LX/1CK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 216083
    if-eqz v2, :cond_0

    .line 216084
    :try_start_1
    new-instance v0, LX/1CK;

    invoke-direct {v0}, LX/1CK;-><init>()V

    .line 216085
    move-object v0, v0

    .line 216086
    sput-object v0, LX/1CK;->b:LX/1CK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216087
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 216088
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216089
    :cond_1
    sget-object v0, LX/1CK;->b:LX/1CK;

    return-object v0

    .line 216090
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 216091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 216079
    invoke-interface {p0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 216092
    invoke-interface {p0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 216073
    invoke-static {p1}, LX/1CK;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216074
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 216075
    :goto_0
    return-object v0

    .line 216076
    :cond_0
    invoke-static {p1}, LX/1CK;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 216077
    iget-object v2, p0, LX/1CK;->a:Landroid/util/LruCache;

    invoke-virtual {v2, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 216078
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V
    .locals 2
    .param p2    # LX/1Pq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pq;",
            ">(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "TE;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216067
    invoke-static {p1}, LX/1CK;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216068
    :cond_0
    :goto_0
    return-void

    .line 216069
    :cond_1
    invoke-static {p1}, LX/1CK;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 216070
    iget-object v1, p0, LX/1CK;->a:Landroid/util/LruCache;

    invoke-virtual {v1, v0, p3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216071
    if-eqz p2, :cond_0

    .line 216072
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-interface {p2, v0}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
