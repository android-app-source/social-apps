.class public LX/0dv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.content.pm.PackageManager.getPackageInfo"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0dv;


# instance fields
.field private final a:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91158
    iput-object p1, p0, LX/0dv;->a:Landroid/content/pm/PackageManager;

    .line 91159
    return-void
.end method

.method public static a(LX/0QB;)LX/0dv;
    .locals 4

    .prologue
    .line 91160
    sget-object v0, LX/0dv;->b:LX/0dv;

    if-nez v0, :cond_1

    .line 91161
    const-class v1, LX/0dv;

    monitor-enter v1

    .line 91162
    :try_start_0
    sget-object v0, LX/0dv;->b:LX/0dv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91163
    if-eqz v2, :cond_0

    .line 91164
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 91165
    new-instance p0, LX/0dv;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-direct {p0, v3}, LX/0dv;-><init>(Landroid/content/pm/PackageManager;)V

    .line 91166
    move-object v0, p0

    .line 91167
    sput-object v0, LX/0dv;->b:LX/0dv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91168
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91169
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91170
    :cond_1
    sget-object v0, LX/0dv;->b:LX/0dv;

    return-object v0

    .line 91171
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 91173
    :try_start_0
    iget-object v1, p0, LX/0dv;->a:Landroid/content/pm/PackageManager;

    const-string v2, "org.torproject.android"

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 91174
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    .line 91175
    :catch_0
    move-exception v1

    .line 91176
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/os/DeadObjectException;

    if-nez v2, :cond_0

    .line 91177
    throw v1

    .line 91178
    :catch_1
    goto :goto_0
.end method
