.class public LX/0eM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0eM;


# instance fields
.field public final a:Landroid/util/Printer;

.field private final b:Ljava/lang/Runnable;

.field public final c:Landroid/os/Handler;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0eL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91754
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, LX/0eM;-><init>(Landroid/os/Handler;)V

    .line 91755
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 91718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91719
    new-instance v0, LX/0eN;

    invoke-direct {v0, p0}, LX/0eN;-><init>(LX/0eM;)V

    iput-object v0, p0, LX/0eM;->a:Landroid/util/Printer;

    .line 91720
    new-instance v0, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;

    invoke-direct {v0, p0}, Lcom/facebook/debug/mainlooper/MainLooperLogMessagesDispatcher$2;-><init>(LX/0eM;)V

    iput-object v0, p0, LX/0eM;->b:Ljava/lang/Runnable;

    .line 91721
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0eM;->d:Ljava/util/List;

    .line 91722
    iput-object p1, p0, LX/0eM;->c:Landroid/os/Handler;

    .line 91723
    return-void
.end method

.method public static a(LX/0QB;)LX/0eM;
    .locals 3

    .prologue
    .line 91724
    sget-object v0, LX/0eM;->e:LX/0eM;

    if-nez v0, :cond_1

    .line 91725
    const-class v1, LX/0eM;

    monitor-enter v1

    .line 91726
    :try_start_0
    sget-object v0, LX/0eM;->e:LX/0eM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 91727
    if-eqz v2, :cond_0

    .line 91728
    :try_start_1
    new-instance v0, LX/0eM;

    invoke-direct {v0}, LX/0eM;-><init>()V

    .line 91729
    move-object v0, v0

    .line 91730
    sput-object v0, LX/0eM;->e:LX/0eM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 91732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 91733
    :cond_1
    sget-object v0, LX/0eM;->e:LX/0eM;

    return-object v0

    .line 91734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 91735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0eL;)V
    .locals 3

    .prologue
    .line 91736
    if-nez p1, :cond_1

    .line 91737
    :cond_0
    :goto_0
    return-void

    .line 91738
    :cond_1
    iget-object v1, p0, LX/0eM;->d:Ljava/util/List;

    monitor-enter v1

    .line 91739
    :try_start_0
    iget-object v0, p0, LX/0eM;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 91740
    iget-object v2, p0, LX/0eM;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91741
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91742
    if-eqz v0, :cond_0

    .line 91743
    iget-object v0, p0, LX/0eM;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0eM;->b:Ljava/lang/Runnable;

    const v2, -0x1b8ee1c9

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 91744
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/0eL;)V
    .locals 3

    .prologue
    .line 91745
    if-nez p1, :cond_1

    .line 91746
    :cond_0
    :goto_0
    return-void

    .line 91747
    :cond_1
    iget-object v1, p0, LX/0eM;->d:Ljava/util/List;

    monitor-enter v1

    .line 91748
    :try_start_0
    iget-object v0, p0, LX/0eM;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 91749
    iget-object v0, p0, LX/0eM;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 91750
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91751
    if-eqz v0, :cond_0

    .line 91752
    iget-object v0, p0, LX/0eM;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0eM;->b:Ljava/lang/Runnable;

    const v2, -0x3bb7e980

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 91753
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
