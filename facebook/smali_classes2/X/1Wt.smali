.class public LX/1Wt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Wu;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0Vq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 270109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270110
    sget-object v0, LX/1Wu;->NOT_SCHEDULED:LX/1Wu;

    iput-object v0, p0, LX/1Wt;->a:LX/1Wu;

    return-void
.end method

.method public static g(LX/1Wt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270111
    iget-object v0, p0, LX/1Wt;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Wt;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 270112
    iget-object v0, p0, LX/1Wt;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sy;

    .line 270113
    iget-object v1, p0, LX/1Wt;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Vq;

    .line 270114
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 270115
    invoke-virtual {v0, v1}, LX/0Sy;->b(LX/0Vq;)V

    .line 270116
    :cond_0
    iput-object v2, p0, LX/1Wt;->d:Ljava/lang/ref/WeakReference;

    .line 270117
    iput-object v2, p0, LX/1Wt;->c:Ljava/lang/ref/WeakReference;

    .line 270118
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 270119
    iget-object v0, p0, LX/1Wt;->a:LX/1Wu;

    sget-object v1, LX/1Wu;->SCHEDULED:LX/1Wu;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1Wt;->a:LX/1Wu;

    sget-object v1, LX/1Wu;->WAITING_FOR_INTERACTION_STOP:LX/1Wu;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 270120
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    .line 270121
    invoke-static {p0}, LX/1Wt;->g(LX/1Wt;)V

    .line 270122
    sget-object v0, LX/1Wu;->NOT_SCHEDULED:LX/1Wu;

    iput-object v0, p0, LX/1Wt;->a:LX/1Wu;

    .line 270123
    return-void
.end method
