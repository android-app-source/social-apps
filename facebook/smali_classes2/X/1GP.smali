.class public LX/1GP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1GP;


# instance fields
.field private final a:LX/03V;

.field private final b:LX/1GR;

.field private final c:LX/1GS;


# direct methods
.method public constructor <init>(LX/03V;LX/1GR;LX/1GS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225441
    iput-object p1, p0, LX/1GP;->a:LX/03V;

    .line 225442
    iput-object p2, p0, LX/1GP;->b:LX/1GR;

    .line 225443
    iput-object p3, p0, LX/1GP;->c:LX/1GS;

    .line 225444
    return-void
.end method

.method public static a(LX/0QB;)LX/1GP;
    .locals 6

    .prologue
    .line 225445
    sget-object v0, LX/1GP;->d:LX/1GP;

    if-nez v0, :cond_1

    .line 225446
    const-class v1, LX/1GP;

    monitor-enter v1

    .line 225447
    :try_start_0
    sget-object v0, LX/1GP;->d:LX/1GP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225448
    if-eqz v2, :cond_0

    .line 225449
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225450
    new-instance p0, LX/1GP;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/1GR;->a(LX/0QB;)LX/1GR;

    move-result-object v4

    check-cast v4, LX/1GR;

    invoke-static {v0}, LX/1GS;->a(LX/0QB;)LX/1GS;

    move-result-object v5

    check-cast v5, LX/1GS;

    invoke-direct {p0, v3, v4, v5}, LX/1GP;-><init>(LX/03V;LX/1GR;LX/1GS;)V

    .line 225451
    move-object v0, p0

    .line 225452
    sput-object v0, LX/1GP;->d:LX/1GP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225453
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225454
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225455
    :cond_1
    sget-object v0, LX/1GP;->d:LX/1GP;

    return-object v0

    .line 225456
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225457
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)LX/0VG;
    .locals 3
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/43W;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")",
            "LX/0VG;"
        }
    .end annotation

    .prologue
    .line 225458
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MEDIACACHE_ERROR_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/43W;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 225459
    if-eqz p3, :cond_0

    .line 225460
    iput-object p3, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 225461
    :cond_0
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6
    .param p4    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/43W;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225462
    invoke-static {p1, p2, p3, p4}, LX/1GP;->b(LX/43W;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)LX/0VG;

    move-result-object v0

    .line 225463
    iget-object v1, p0, LX/1GP;->a:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 225464
    iget-object v1, p0, LX/1GP;->b:LX/1GR;

    .line 225465
    iget-object v2, v1, LX/1GR;->c:LX/0Yw;

    const-string v3, "%s %s %s"

    .line 225466
    iget-object v4, v0, LX/0VG;->a:Ljava/lang/String;

    move-object v4, v4

    .line 225467
    iget-object v5, v0, LX/0VG;->c:Ljava/lang/Throwable;

    move-object v5, v5

    .line 225468
    iget-object p4, v0, LX/0VG;->b:Ljava/lang/String;

    move-object p4, p4

    .line 225469
    invoke-static {v3, v4, v5, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 225470
    iget-object v0, p0, LX/1GP;->c:LX/1GS;

    .line 225471
    sget-object v1, LX/43W;->READ_INVALID_ENTRY:LX/43W;

    if-ne p1, v1, :cond_1

    const-class v1, LX/1Hk;

    if-ne p2, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 225472
    if-eqz v1, :cond_0

    .line 225473
    iget-object v1, v0, LX/1GS;->a:LX/0Zb;

    const-string p1, "disk_storage_cache_read_invalid_entry_event"

    const/4 p2, 0x0

    invoke-interface {v1, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 225474
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 225475
    const-string p1, "message"

    invoke-virtual {v1, p1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 225476
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 225477
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
