.class public LX/1bk;
.super LX/1Ae;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ae",
        "<",
        "LX/1bk;",
        "LX/1bf;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1HI;

.field private final b:LX/4AP;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/4AP;LX/1HI;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/4AP;",
            "LX/1HI;",
            "Ljava/util/Set",
            "<",
            "LX/1Ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 280926
    invoke-direct {p0, p1, p4}, LX/1Ae;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    .line 280927
    iput-object p3, p0, LX/1bk;->a:LX/1HI;

    .line 280928
    iput-object p2, p0, LX/1bk;->b:LX/4AP;

    .line 280929
    return-void
.end method

.method public static a(LX/1bj;)LX/1bY;
    .locals 3

    .prologue
    .line 280920
    sget-object v0, LX/1bl;->a:[I

    invoke-virtual {p0}, LX/1bj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 280921
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cache level"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is not supported. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280922
    :pswitch_0
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    .line 280923
    :goto_0
    return-object v0

    .line 280924
    :pswitch_1
    sget-object v0, LX/1bY;->DISK_CACHE:LX/1bY;

    goto :goto_0

    .line 280925
    :pswitch_2
    sget-object v0, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private p()LX/1bh;
    .locals 3

    .prologue
    .line 280909
    iget-object v0, p0, LX/1Ae;->f:Ljava/lang/Object;

    move-object v0, v0

    .line 280910
    check-cast v0, LX/1bf;

    .line 280911
    iget-object v1, p0, LX/1bk;->a:LX/1HI;

    .line 280912
    iget-object v2, v1, LX/1HI;->i:LX/1Ao;

    move-object v2, v2

    .line 280913
    const/4 v1, 0x0

    .line 280914
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 280915
    iget-object v1, v0, LX/1bf;->m:LX/33B;

    move-object v1, v1

    .line 280916
    if-eqz v1, :cond_0

    .line 280917
    invoke-virtual {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/1Ao;->b(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 280918
    :goto_0
    return-object v0

    .line 280919
    :cond_0
    invoke-virtual {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/1Af;
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280892
    if-nez p1, :cond_0

    .line 280893
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1bk;

    .line 280894
    :goto_0
    return-object v0

    .line 280895
    :cond_0
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {}, LX/1bd;->c()LX/1bd;

    move-result-object v1

    .line 280896
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 280897
    move-object v0, v0

    .line 280898
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 280899
    invoke-super {p0, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1bk;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1bj;)LX/1ca;
    .locals 2

    .prologue
    .line 280907
    check-cast p1, LX/1bf;

    .line 280908
    iget-object v0, p0, LX/1bk;->a:LX/1HI;

    invoke-static {p3}, LX/1bk;->a(LX/1bj;)LX/1bY;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1ca;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1bp;
    .locals 5

    .prologue
    .line 280901
    iget-object v0, p0, LX/1Ae;->q:LX/1aZ;

    move-object v0, v0

    .line 280902
    instance-of v1, v0, LX/1bo;

    if-eqz v1, :cond_0

    .line 280903
    check-cast v0, LX/1bo;

    .line 280904
    invoke-virtual {p0}, LX/1Ae;->l()LX/1Gd;

    move-result-object v1

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, LX/1bk;->p()LX/1bh;

    move-result-object v3

    invoke-virtual {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1bo;->a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)V

    .line 280905
    :goto_0
    return-object v0

    .line 280906
    :cond_0
    iget-object v0, p0, LX/1bk;->b:LX/4AP;

    invoke-virtual {p0}, LX/1Ae;->l()LX/1Gd;

    move-result-object v1

    invoke-static {}, LX/1Ae;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, LX/1bk;->p()LX/1bh;

    move-result-object v3

    invoke-virtual {p0}, LX/1Ae;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/4AP;->a(LX/1Gd;Ljava/lang/String;LX/1bh;Ljava/lang/Object;)LX/1bo;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()LX/1Ae;
    .locals 1

    .prologue
    .line 280900
    return-object p0
.end method
