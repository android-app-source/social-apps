.class public LX/1hM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final A:Ljava/lang/Boolean;

.field private final B:Ljava/lang/Long;

.field private final C:Ljava/lang/Long;

.field private final D:Ljava/lang/Boolean;

.field private final E:Ljava/lang/Long;

.field private final F:Ljava/lang/Long;

.field private final G:Ljava/lang/Long;

.field private final H:Ljava/lang/Long;

.field public final I:Ljava/lang/Long;

.field private final J:Ljava/lang/Long;

.field private final K:Ljava/lang/Double;

.field private final L:Ljava/lang/Long;

.field private final M:Ljava/lang/Long;

.field private final N:Ljava/lang/Long;

.field private final O:Ljava/lang/Double;

.field private final P:Ljava/lang/String;

.field public final Q:Ljava/lang/String;

.field private final R:Ljava/lang/String;

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Long;

.field private final b:Ljava/lang/Long;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/lang/Long;

.field private final e:Ljava/lang/Long;

.field private final f:Ljava/lang/Long;

.field private final g:Ljava/lang/Long;

.field private final h:Ljava/lang/Long;

.field private final i:Ljava/lang/Double;

.field private final j:Ljava/lang/Boolean;

.field private final k:Ljava/lang/Boolean;

.field private final l:Ljava/lang/Long;

.field private final m:Ljava/lang/Long;

.field private final n:Ljava/lang/Long;

.field private final o:Ljava/lang/Long;

.field private final p:Ljava/lang/Long;

.field private final q:Ljava/lang/Long;

.field private final r:Ljava/lang/Long;

.field private final s:Ljava/lang/Long;

.field private final t:Ljava/lang/Long;

.field private final u:Ljava/lang/Long;

.field private final v:Ljava/lang/Long;

.field private final w:Ljava/lang/Integer;

.field private final x:Ljava/lang/Integer;

.field private final y:Ljava/lang/Integer;

.field private final z:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 295876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295877
    const/4 v1, 0x0

    iput-object v1, p0, LX/1hM;->S:Ljava/util/List;

    .line 295878
    const/4 v1, 0x0

    iput-object v1, p0, LX/1hM;->T:Ljava/util/Map;

    .line 295879
    iput-object p1, p0, LX/1hM;->a:Ljava/lang/Long;

    .line 295880
    iput-object p2, p0, LX/1hM;->b:Ljava/lang/Long;

    .line 295881
    iput-object p3, p0, LX/1hM;->c:Ljava/lang/Long;

    .line 295882
    iput-object p4, p0, LX/1hM;->d:Ljava/lang/Long;

    .line 295883
    iput-object p5, p0, LX/1hM;->e:Ljava/lang/Long;

    .line 295884
    iput-object p6, p0, LX/1hM;->f:Ljava/lang/Long;

    .line 295885
    iput-object p7, p0, LX/1hM;->g:Ljava/lang/Long;

    .line 295886
    iput-object p8, p0, LX/1hM;->h:Ljava/lang/Long;

    .line 295887
    iput-object p9, p0, LX/1hM;->i:Ljava/lang/Double;

    .line 295888
    iput-object p10, p0, LX/1hM;->j:Ljava/lang/Boolean;

    .line 295889
    iput-object p11, p0, LX/1hM;->k:Ljava/lang/Boolean;

    .line 295890
    iput-object p12, p0, LX/1hM;->l:Ljava/lang/Long;

    .line 295891
    iput-object p13, p0, LX/1hM;->m:Ljava/lang/Long;

    .line 295892
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1hM;->n:Ljava/lang/Long;

    .line 295893
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1hM;->o:Ljava/lang/Long;

    .line 295894
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1hM;->p:Ljava/lang/Long;

    .line 295895
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1hM;->q:Ljava/lang/Long;

    .line 295896
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1hM;->r:Ljava/lang/Long;

    .line 295897
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1hM;->s:Ljava/lang/Long;

    .line 295898
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1hM;->t:Ljava/lang/Long;

    .line 295899
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1hM;->u:Ljava/lang/Long;

    .line 295900
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1hM;->v:Ljava/lang/Long;

    .line 295901
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1hM;->w:Ljava/lang/Integer;

    .line 295902
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1hM;->x:Ljava/lang/Integer;

    .line 295903
    move-object/from16 v0, p25

    iput-object v0, p0, LX/1hM;->y:Ljava/lang/Integer;

    .line 295904
    move-object/from16 v0, p26

    iput-object v0, p0, LX/1hM;->z:Ljava/lang/Integer;

    .line 295905
    move-object/from16 v0, p27

    iput-object v0, p0, LX/1hM;->A:Ljava/lang/Boolean;

    .line 295906
    move-object/from16 v0, p28

    iput-object v0, p0, LX/1hM;->B:Ljava/lang/Long;

    .line 295907
    move-object/from16 v0, p29

    iput-object v0, p0, LX/1hM;->C:Ljava/lang/Long;

    .line 295908
    move-object/from16 v0, p30

    iput-object v0, p0, LX/1hM;->D:Ljava/lang/Boolean;

    .line 295909
    move-object/from16 v0, p31

    iput-object v0, p0, LX/1hM;->E:Ljava/lang/Long;

    .line 295910
    move-object/from16 v0, p32

    iput-object v0, p0, LX/1hM;->F:Ljava/lang/Long;

    .line 295911
    move-object/from16 v0, p33

    iput-object v0, p0, LX/1hM;->G:Ljava/lang/Long;

    .line 295912
    move-object/from16 v0, p34

    iput-object v0, p0, LX/1hM;->H:Ljava/lang/Long;

    .line 295913
    move-object/from16 v0, p35

    iput-object v0, p0, LX/1hM;->I:Ljava/lang/Long;

    .line 295914
    move-object/from16 v0, p36

    iput-object v0, p0, LX/1hM;->J:Ljava/lang/Long;

    .line 295915
    move-object/from16 v0, p37

    iput-object v0, p0, LX/1hM;->K:Ljava/lang/Double;

    .line 295916
    move-object/from16 v0, p38

    iput-object v0, p0, LX/1hM;->L:Ljava/lang/Long;

    .line 295917
    move-object/from16 v0, p39

    iput-object v0, p0, LX/1hM;->M:Ljava/lang/Long;

    .line 295918
    move-object/from16 v0, p40

    iput-object v0, p0, LX/1hM;->N:Ljava/lang/Long;

    .line 295919
    move-object/from16 v0, p41

    iput-object v0, p0, LX/1hM;->O:Ljava/lang/Double;

    .line 295920
    move-object/from16 v0, p42

    iput-object v0, p0, LX/1hM;->P:Ljava/lang/String;

    .line 295921
    move-object/from16 v0, p43

    iput-object v0, p0, LX/1hM;->Q:Ljava/lang/String;

    .line 295922
    move-object/from16 v0, p44

    iput-object v0, p0, LX/1hM;->R:Ljava/lang/String;

    .line 295923
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 295924
    if-eqz p2, :cond_0

    .line 295925
    iget-object v0, p0, LX/1hM;->T:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295926
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 295927
    if-eqz p2, :cond_0

    .line 295928
    iget-object v0, p0, LX/1hM;->S:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295929
    iget-object v0, p0, LX/1hM;->S:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295930
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 295931
    iget-object v0, p0, LX/1hM;->g:Ljava/lang/Long;

    return-object v0
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295932
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hM;->S:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 295933
    iget-object v0, p0, LX/1hM;->S:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295934
    :goto_0
    monitor-exit p0

    return-object v0

    .line 295935
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1hM;->S:Ljava/util/List;

    .line 295936
    const-string v0, "network_info_ingress_avg"

    iget-object v1, p0, LX/1hM;->a:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295937
    const-string v0, "network_info_ingress_max"

    iget-object v1, p0, LX/1hM;->b:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295938
    const-string v0, "network_info_egress_avg"

    iget-object v1, p0, LX/1hM;->c:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295939
    const-string v0, "network_info_egress_max"

    iget-object v1, p0, LX/1hM;->d:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295940
    const-string v0, "network_info_read_count"

    iget-object v1, p0, LX/1hM;->e:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295941
    const-string v0, "network_info_write_count"

    iget-object v1, p0, LX/1hM;->f:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295942
    const-string v0, "network_info_rtt_avg"

    iget-object v1, p0, LX/1hM;->g:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295943
    const-string v0, "network_info_rtt_max"

    iget-object v1, p0, LX/1hM;->h:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295944
    const-string v0, "network_info_rtt_stddev"

    iget-object v1, p0, LX/1hM;->i:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295945
    const-string v0, "network_info_network_changed"

    iget-object v1, p0, LX/1hM;->j:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295946
    const-string v0, "network_info_celltower_changed"

    iget-object v1, p0, LX/1hM;->k:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295947
    const-string v0, "network_info_opened_conn"

    iget-object v1, p0, LX/1hM;->l:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295948
    const-string v0, "network_info_closed_conn"

    iget-object v1, p0, LX/1hM;->m:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295949
    const-string v0, "network_info_inflight_conn"

    iget-object v1, p0, LX/1hM;->n:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295950
    const-string v0, "network_info_enqueued_req"

    iget-object v1, p0, LX/1hM;->o:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295951
    const-string v0, "network_info_dequeued_req"

    iget-object v1, p0, LX/1hM;->p:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295952
    const-string v0, "network_info_finished_req"

    iget-object v1, p0, LX/1hM;->q:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295953
    const-string v0, "network_info_inflight_req"

    iget-object v1, p0, LX/1hM;->r:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295954
    const-string v0, "network_info_inqueue_req"

    iget-object v1, p0, LX/1hM;->s:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295955
    const-string v0, "network_info_interact_req"

    iget-object v1, p0, LX/1hM;->t:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295956
    const-string v0, "network_info_non_interact_req"

    iget-object v1, p0, LX/1hM;->u:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295957
    const-string v0, "network_info_can_wait_req"

    iget-object v1, p0, LX/1hM;->v:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295958
    const-string v0, "network_info_signal_level"

    iget-object v1, p0, LX/1hM;->w:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295959
    const-string v0, "network_info_signal_dbm"

    iget-object v1, p0, LX/1hM;->x:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295960
    const-string v0, "network_info_frequency_mhz"

    iget-object v1, p0, LX/1hM;->y:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295961
    const-string v0, "network_info_link_speed_mbps"

    iget-object v1, p0, LX/1hM;->z:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295962
    const-string v0, "network_info_app_backgrounded"

    iget-object v1, p0, LX/1hM;->A:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295963
    const-string v0, "network_info_ms_since_launch"

    iget-object v1, p0, LX/1hM;->B:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295964
    const-string v0, "network_info_ms_since_init"

    iget-object v1, p0, LX/1hM;->C:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295965
    const-string v0, "network_info_may_have_network"

    iget-object v1, p0, LX/1hM;->D:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295966
    const-string v0, "network_info_may_have_idled"

    iget-object v1, p0, LX/1hM;->E:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295967
    const-string v0, "network_info_active_read_seconds"

    iget-object v1, p0, LX/1hM;->F:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295968
    const-string v0, "network_info_active_write_seconds"

    iget-object v1, p0, LX/1hM;->G:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295969
    const-string v0, "network_info_req_bw_ingress_size"

    iget-object v1, p0, LX/1hM;->H:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295970
    const-string v0, "network_info_req_bw_ingress_avg"

    iget-object v1, p0, LX/1hM;->I:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295971
    const-string v0, "network_info_req_bw_ingress_max"

    iget-object v1, p0, LX/1hM;->J:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295972
    const-string v0, "network_info_req_bw_ingress_std_dev"

    iget-object v1, p0, LX/1hM;->K:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295973
    const-string v0, "network_info_req_bw_egress_size"

    iget-object v1, p0, LX/1hM;->L:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295974
    const-string v0, "network_info_req_bw_egress_avg"

    iget-object v1, p0, LX/1hM;->M:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295975
    const-string v0, "network_info_req_bw_egress_max"

    iget-object v1, p0, LX/1hM;->N:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295976
    const-string v0, "network_info_req_bw_egress_std_dev"

    iget-object v1, p0, LX/1hM;->O:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295977
    const-string v0, "network_info_latency_quality"

    iget-object v1, p0, LX/1hM;->P:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295978
    const-string v0, "network_info_upload_bw_quality"

    iget-object v1, p0, LX/1hM;->Q:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295979
    const-string v0, "network_info_download_bw_quality"

    iget-object v1, p0, LX/1hM;->R:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295980
    iget-object v0, p0, LX/1hM;->S:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 295981
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295982
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hM;->T:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 295983
    iget-object v0, p0, LX/1hM;->T:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295984
    :goto_0
    monitor-exit p0

    return-object v0

    .line 295985
    :cond_0
    :try_start_1
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/1hM;->T:Ljava/util/Map;

    .line 295986
    const-string v0, "network_info_ingress_avg"

    iget-object v1, p0, LX/1hM;->a:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295987
    const-string v0, "network_info_ingress_max"

    iget-object v1, p0, LX/1hM;->b:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295988
    const-string v0, "network_info_egress_avg"

    iget-object v1, p0, LX/1hM;->c:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295989
    const-string v0, "network_info_egress_max"

    iget-object v1, p0, LX/1hM;->d:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295990
    const-string v0, "network_info_read_count"

    iget-object v1, p0, LX/1hM;->e:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295991
    const-string v0, "network_info_write_count"

    iget-object v1, p0, LX/1hM;->f:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295992
    const-string v0, "network_info_rtt_avg"

    iget-object v1, p0, LX/1hM;->g:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295993
    const-string v0, "network_info_rtt_max"

    iget-object v1, p0, LX/1hM;->h:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295994
    const-string v0, "network_info_rtt_stddev"

    iget-object v1, p0, LX/1hM;->i:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295995
    const-string v0, "network_info_network_changed"

    iget-object v1, p0, LX/1hM;->j:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295996
    const-string v0, "network_info_celltower_changed"

    iget-object v1, p0, LX/1hM;->k:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295997
    const-string v0, "network_info_opened_conn"

    iget-object v1, p0, LX/1hM;->l:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295998
    const-string v0, "network_info_closed_conn"

    iget-object v1, p0, LX/1hM;->m:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295999
    const-string v0, "network_info_inflight_conn"

    iget-object v1, p0, LX/1hM;->n:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296000
    const-string v0, "network_info_enqueued_req"

    iget-object v1, p0, LX/1hM;->o:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296001
    const-string v0, "network_info_dequeued_req"

    iget-object v1, p0, LX/1hM;->p:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296002
    const-string v0, "network_info_finished_req"

    iget-object v1, p0, LX/1hM;->q:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296003
    const-string v0, "network_info_inflight_req"

    iget-object v1, p0, LX/1hM;->r:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296004
    const-string v0, "network_info_inqueue_req"

    iget-object v1, p0, LX/1hM;->s:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296005
    const-string v0, "network_info_interact_req"

    iget-object v1, p0, LX/1hM;->t:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296006
    const-string v0, "network_info_non_interact_req"

    iget-object v1, p0, LX/1hM;->u:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296007
    const-string v0, "network_info_can_wait_req"

    iget-object v1, p0, LX/1hM;->v:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296008
    const-string v0, "network_info_signal_level"

    iget-object v1, p0, LX/1hM;->w:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296009
    const-string v0, "network_info_signal_dbm"

    iget-object v1, p0, LX/1hM;->x:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296010
    const-string v0, "network_info_frequency_mhz"

    iget-object v1, p0, LX/1hM;->y:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296011
    const-string v0, "network_info_link_speed_mbps"

    iget-object v1, p0, LX/1hM;->z:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296012
    const-string v0, "network_info_app_backgrounded"

    iget-object v1, p0, LX/1hM;->A:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296013
    const-string v0, "network_info_ms_since_launch"

    iget-object v1, p0, LX/1hM;->B:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296014
    const-string v0, "network_info_ms_since_init"

    iget-object v1, p0, LX/1hM;->C:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296015
    const-string v0, "network_info_may_have_network"

    iget-object v1, p0, LX/1hM;->D:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296016
    const-string v0, "network_info_may_have_idled"

    iget-object v1, p0, LX/1hM;->E:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296017
    const-string v0, "network_info_active_read_seconds"

    iget-object v1, p0, LX/1hM;->F:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296018
    const-string v0, "network_info_active_write_seconds"

    iget-object v1, p0, LX/1hM;->G:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296019
    const-string v0, "network_info_req_bw_ingress_size"

    iget-object v1, p0, LX/1hM;->H:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296020
    const-string v0, "network_info_req_bw_ingress_avg"

    iget-object v1, p0, LX/1hM;->I:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296021
    const-string v0, "network_info_req_bw_ingress_max"

    iget-object v1, p0, LX/1hM;->J:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296022
    const-string v0, "network_info_req_bw_ingress_std_dev"

    iget-object v1, p0, LX/1hM;->K:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296023
    const-string v0, "network_info_req_bw_egress_size"

    iget-object v1, p0, LX/1hM;->L:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296024
    const-string v0, "network_info_req_bw_egress_avg"

    iget-object v1, p0, LX/1hM;->M:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296025
    const-string v0, "network_info_req_bw_egress_max"

    iget-object v1, p0, LX/1hM;->N:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296026
    const-string v0, "network_info_req_bw_egress_std_dev"

    iget-object v1, p0, LX/1hM;->O:Ljava/lang/Double;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296027
    const-string v0, "network_info_latency_quality"

    iget-object v1, p0, LX/1hM;->P:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296028
    const-string v0, "network_info_upload_bw_quality"

    iget-object v1, p0, LX/1hM;->Q:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296029
    const-string v0, "network_info_download_bw_quality"

    iget-object v1, p0, LX/1hM;->R:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, LX/1hM;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 296030
    iget-object v0, p0, LX/1hM;->T:Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 296031
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
