.class public final enum LX/0wI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wI;

.field public static final enum CUSTOM:LX/0wI;

.field public static final enum SEARCH_TITLES_APP:LX/0wI;

.field public static final enum TEXT:LX/0wI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159569
    new-instance v0, LX/0wI;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0wI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0wI;->TEXT:LX/0wI;

    .line 159570
    new-instance v0, LX/0wI;

    const-string v1, "SEARCH_TITLES_APP"

    invoke-direct {v0, v1, v3}, LX/0wI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    .line 159571
    new-instance v0, LX/0wI;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v4}, LX/0wI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0wI;->CUSTOM:LX/0wI;

    .line 159572
    const/4 v0, 0x3

    new-array v0, v0, [LX/0wI;

    sget-object v1, LX/0wI;->TEXT:LX/0wI;

    aput-object v1, v0, v2

    sget-object v1, LX/0wI;->SEARCH_TITLES_APP:LX/0wI;

    aput-object v1, v0, v3

    sget-object v1, LX/0wI;->CUSTOM:LX/0wI;

    aput-object v1, v0, v4

    sput-object v0, LX/0wI;->$VALUES:[LX/0wI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 159573
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wI;
    .locals 1

    .prologue
    .line 159574
    const-class v0, LX/0wI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wI;

    return-object v0
.end method

.method public static values()[LX/0wI;
    .locals 1

    .prologue
    .line 159575
    sget-object v0, LX/0wI;->$VALUES:[LX/0wI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wI;

    return-object v0
.end method
