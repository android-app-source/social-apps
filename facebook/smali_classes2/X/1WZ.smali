.class public LX/1WZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field public final j:LX/14w;

.field public final k:LX/1VK;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/14w;LX/1VK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultReactionsFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FindFriendsFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/groupinvitation/GroupInvitationFooterComponentPartDefinition;",
            ">;",
            "LX/14w;",
            "LX/1VK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268981
    iput-object p4, p0, LX/1WZ;->a:LX/0Ot;

    .line 268982
    iput-object p5, p0, LX/1WZ;->b:LX/0Ot;

    .line 268983
    iput-object p6, p0, LX/1WZ;->c:LX/0Ot;

    .line 268984
    iput-object p3, p0, LX/1WZ;->d:LX/0Ot;

    .line 268985
    iput-object p8, p0, LX/1WZ;->h:LX/0Ot;

    .line 268986
    iput-object p1, p0, LX/1WZ;->e:LX/0Ot;

    .line 268987
    iput-object p2, p0, LX/1WZ;->f:LX/0Ot;

    .line 268988
    iput-object p7, p0, LX/1WZ;->g:LX/0Ot;

    .line 268989
    iput-object p9, p0, LX/1WZ;->i:LX/0Ot;

    .line 268990
    iput-object p10, p0, LX/1WZ;->j:LX/14w;

    .line 268991
    iput-object p11, p0, LX/1WZ;->k:LX/1VK;

    .line 268992
    return-void
.end method

.method public static a(LX/0QB;)LX/1WZ;
    .locals 15

    .prologue
    .line 268993
    const-class v1, LX/1WZ;

    monitor-enter v1

    .line 268994
    :try_start_0
    sget-object v0, LX/1WZ;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 268995
    sput-object v2, LX/1WZ;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 268996
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268997
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 268998
    new-instance v3, LX/1WZ;

    const/16 v4, 0x8fa

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x8ed

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x8f4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x8f0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1f70

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x8f3

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x8ec

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x8f9

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x947

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v13

    check-cast v13, LX/14w;

    const-class v14, LX/1VK;

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1VK;

    invoke-direct/range {v3 .. v14}, LX/1WZ;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/14w;LX/1VK;)V

    .line 268999
    move-object v0, v3

    .line 269000
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 269001
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269002
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 269003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 269004
    invoke-static {p0}, LX/182;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1Wt;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Wt;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 269005
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 269006
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 269007
    const/4 v4, 0x0

    .line 269008
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 269009
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 269010
    if-nez v5, :cond_3

    move v1, v4

    .line 269011
    :goto_0
    move v5, v1

    .line 269012
    if-nez v5, :cond_0

    move v1, v2

    .line 269013
    :goto_1
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    if-eqz v4, :cond_1

    sget-object v4, LX/1Qt;->FEED:LX/1Qt;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    invoke-interface {v6}, LX/1PT;->a()LX/1Qt;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v2

    .line 269014
    :goto_2
    iget-object v6, p0, LX/1WZ;->a:LX/0Ot;

    invoke-static {p1, v1, v6, v0}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    :goto_3
    iget-object v3, p0, LX/1WZ;->i:LX/0Ot;

    invoke-virtual {v0, v2, v3, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->e:LX/0Ot;

    invoke-virtual {v0, v1, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->f:LX/0Ot;

    invoke-virtual {v0, v1, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->c:LX/0Ot;

    invoke-virtual {v0, v5, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->b:LX/0Ot;

    invoke-virtual {v0, v2, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->h:LX/0Ot;

    invoke-virtual {v0, v1, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, LX/1WZ;->g:LX/0Ot;

    invoke-virtual {v0, v1, v2, p2}, LX/1RG;->a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, LX/1WZ;->d:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 269015
    new-instance v1, LX/1Ws;

    invoke-direct {v1, p2}, LX/1Ws;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 269016
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 269017
    check-cast v0, LX/0jW;

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Wt;

    return-object v0

    :cond_0
    move v1, v3

    .line 269018
    goto :goto_1

    :cond_1
    move v4, v3

    .line 269019
    goto :goto_2

    :cond_2
    move v2, v3

    .line 269020
    goto :goto_3

    .line 269021
    :cond_3
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 269022
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 269023
    invoke-static {v1}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-nez v6, :cond_4

    move v1, v4

    .line 269024
    goto :goto_0

    .line 269025
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-static {v5}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v5

    .line 269026
    if-nez v5, :cond_5

    move v1, v4

    .line 269027
    goto/16 :goto_0

    .line 269028
    :cond_5
    new-instance v6, LX/2oK;

    iget-object v7, p0, LX/1WZ;->k:LX/1VK;

    invoke-direct {v6, p2, v5, v7}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 269029
    invoke-interface {p3, v6, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2oL;

    .line 269030
    iget-object v5, v1, LX/2oL;->h:LX/2oN;

    move-object v1, v5

    .line 269031
    sget-object v5, LX/2oN;->NONE:LX/2oN;

    if-eq v1, v5, :cond_6

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_6
    move v1, v4

    goto/16 :goto_0
.end method
