.class public LX/1eC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 287424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287425
    return-void
.end method

.method public static a(LX/1Fc;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 287410
    invoke-interface {p0}, LX/1Fc;->b()Ljava/lang/String;

    move-result-object v0

    .line 287411
    if-nez v0, :cond_0

    .line 287412
    const/4 v0, 0x0

    .line 287413
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287414
    invoke-static {p0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 287415
    if-eqz v0, :cond_0

    .line 287416
    :goto_0
    return-object v0

    .line 287417
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 287418
    if-nez v0, :cond_1

    .line 287419
    const/4 v0, 0x0

    goto :goto_0

    .line 287420
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 287421
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v1

    .line 287422
    iput-object v0, v1, LX/0x2;->d:Landroid/net/Uri;

    .line 287423
    goto :goto_0
.end method
