.class public LX/1qi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1qi;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;

.field private final c:LX/00H;

.field private d:LX/1t7;

.field public final e:LX/1qj;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;LX/00H;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/00H;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331369
    iput-object p1, p0, LX/1qi;->b:LX/0ad;

    .line 331370
    iput-object p2, p0, LX/1qi;->a:LX/0Or;

    .line 331371
    iput-object p3, p0, LX/1qi;->c:LX/00H;

    .line 331372
    new-instance v0, LX/1qj;

    invoke-direct {v0, p0}, LX/1qj;-><init>(LX/1qi;)V

    iput-object v0, p0, LX/1qi;->e:LX/1qj;

    .line 331373
    return-void
.end method

.method public static a(LX/0QB;)LX/1qi;
    .locals 6

    .prologue
    .line 331337
    sget-object v0, LX/1qi;->f:LX/1qi;

    if-nez v0, :cond_1

    .line 331338
    const-class v1, LX/1qi;

    monitor-enter v1

    .line 331339
    :try_start_0
    sget-object v0, LX/1qi;->f:LX/1qi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331340
    if-eqz v2, :cond_0

    .line 331341
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331342
    new-instance v5, LX/1qi;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 v4, 0x15e8

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v4, LX/00H;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/00H;

    invoke-direct {v5, v3, p0, v4}, LX/1qi;-><init>(LX/0ad;LX/0Or;LX/00H;)V

    .line 331343
    move-object v0, v5

    .line 331344
    sput-object v0, LX/1qi;->f:LX/1qi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331345
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331346
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331347
    :cond_1
    sget-object v0, LX/1qi;->f:LX/1qi;

    return-object v0

    .line 331348
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331349
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1t7;
    .locals 13

    .prologue
    .line 331352
    iget-object v0, p0, LX/1qi;->d:LX/1t7;

    if-eqz v0, :cond_0

    .line 331353
    iget-object v0, p0, LX/1qi;->d:LX/1t7;

    .line 331354
    :goto_0
    return-object v0

    .line 331355
    :cond_0
    iget-object v1, p0, LX/1qi;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    iget-object v0, p0, LX/1qi;->c:LX/00H;

    .line 331356
    iget-object v4, v0, LX/00H;->j:LX/01T;

    move-object v0, v4

    .line 331357
    sget-object v4, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v4, :cond_2

    sget-wide v4, LX/1t6;->a:J

    :goto_1
    const-wide/16 v6, 0x7d3

    invoke-interface/range {v1 .. v7}, LX/0ad;->a(LX/0c0;LX/0c1;JJ)J

    move-result-wide v2

    .line 331358
    iget-object v8, p0, LX/1qi;->a:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 331359
    if-eqz v8, :cond_1

    const-wide/16 v10, 0x1

    cmp-long v9, v2, v10

    if-gez v9, :cond_3

    .line 331360
    :cond_1
    const-wide/16 v8, -0x1

    .line 331361
    :goto_2
    move-wide v4, v8

    .line 331362
    new-instance v1, LX/1t7;

    invoke-direct/range {v1 .. v5}, LX/1t7;-><init>(JJ)V

    iput-object v1, p0, LX/1qi;->d:LX/1t7;

    .line 331363
    iget-object v0, p0, LX/1qi;->d:LX/1t7;

    goto :goto_0

    .line 331364
    :cond_2
    sget-wide v4, LX/1t6;->b:J

    goto :goto_1

    .line 331365
    :cond_3
    new-instance v9, Ljava/util/zip/CRC32;

    invoke-direct {v9}, Ljava/util/zip/CRC32;-><init>()V

    .line 331366
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/util/zip/CRC32;->update([B)V

    .line 331367
    invoke-virtual {v9}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v8

    rem-long/2addr v8, v2

    goto :goto_2
.end method

.method public final b()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 331350
    const/4 v0, 0x0

    iput-object v0, p0, LX/1qi;->d:LX/1t7;

    .line 331351
    return-void
.end method
