.class public LX/0sD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0s9;


# instance fields
.field private final a:LX/0Or;
    .annotation runtime Lcom/facebook/config/application/ApiConnectionType;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/config/application/ApiConnectionType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151297
    iput-object p1, p0, LX/0sD;->a:LX/0Or;

    .line 151298
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151295
    const-string v0, "https://b-api.facebook.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151294
    const-string v0, "https://b-graph.facebook.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151293
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BootstrapHttpConfig should be used only for graph and api requests"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151292
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BootstrapHttpConfig should be used only for graph and api requests"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()Landroid/net/Uri$Builder;
    .locals 1

    .prologue
    .line 151291
    const-string v0, "https://b-graph.secure.facebook.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151290
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BootstrapHttpConfig should be used only for graph and api requests"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()Landroid/net/Uri$Builder;
    .locals 2

    .prologue
    .line 151287
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "BootstrapHttpConfig should be used only for graph and api requests"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151289
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151288
    iget-object v0, p0, LX/0sD;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
