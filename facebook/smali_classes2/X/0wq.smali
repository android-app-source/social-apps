.class public LX/0wq;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final ac:Z


# instance fields
.field public final A:Z

.field public final B:Z

.field public final C:Z

.field public final D:Z

.field public final E:Z

.field public final F:Z

.field public final G:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final H:Z

.field public final I:Z

.field public final J:Z

.field public final K:Z

.field public final L:I

.field public final M:I

.field public final N:Z

.field public final O:Z

.field public final P:I

.field public final Q:Z

.field public final R:I

.field public final S:LX/0xT;

.field public final T:Z

.field public final U:Z

.field public final V:Z

.field public final W:Z

.field public final X:Z

.field public final Y:Z

.field public final Z:Z

.field public final aa:Z

.field public final ab:Z

.field private ad:Z

.field public final ae:LX/0Uh;

.field public final af:LX/0ad;

.field public final b:LX/0wr;

.field public final c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:Z

.field public final q:Z

.field public final r:Z

.field public final s:Z

.field public final t:I

.field public final u:I

.field public final v:Z

.field public final w:Z

.field public final x:I

.field public final y:Z

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 160979
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/0wq;->ac:Z

    .line 160980
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0wq;->a:Ljava/util/List;

    return-void

    .line 160981
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 160909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160910
    iput-object p2, p0, LX/0wq;->ae:LX/0Uh;

    .line 160911
    iput-object p1, p0, LX/0wq;->af:LX/0ad;

    .line 160912
    const/16 v0, 0x2c0

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/0wr;->HTTP_1RT_INTERCEPTING:LX/0wr;

    .line 160913
    :goto_0
    sget-char v3, LX/0ws;->fh:C

    invoke-virtual {v0}, LX/0wr;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160914
    invoke-static {v0}, LX/0wr;->of(Ljava/lang/String;)LX/0wr;

    move-result-object v0

    iput-object v0, p0, LX/0wq;->b:LX/0wr;

    .line 160915
    sget v0, LX/0ws;->eT:I

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->d:I

    .line 160916
    sget v0, LX/0ws;->fw:I

    const/16 v3, 0x1f4

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->c:I

    .line 160917
    sget-short v0, LX/0ws;->eQ:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->e:Z

    .line 160918
    sget-short v0, LX/0ws;->eP:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->f:Z

    .line 160919
    sget-short v0, LX/0ws;->fH:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->g:Z

    .line 160920
    sget-short v0, LX/0ws;->fG:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->h:Z

    .line 160921
    sget v0, LX/0ws;->fl:I

    const/high16 v3, 0x40000

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->i:I

    .line 160922
    sget v0, LX/0ws;->fm:I

    const/16 v3, 0x40

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->j:I

    .line 160923
    sget v0, LX/0ws;->eU:I

    const/high16 v3, 0x10000

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->k:I

    .line 160924
    sget v0, LX/0ws;->eV:I

    const/16 v3, 0x20

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->l:I

    .line 160925
    sget v0, LX/0ws;->fp:I

    const/16 v3, 0xfa

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->m:I

    .line 160926
    sget v0, LX/0ws;->fq:I

    const/16 v3, 0x7d0

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->n:I

    .line 160927
    sget v0, LX/0ws;->fD:I

    const/16 v3, 0x5000

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->o:I

    .line 160928
    sget-short v0, LX/0ws;->fd:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->p:Z

    .line 160929
    sget-short v0, LX/0ws;->fa:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->q:Z

    .line 160930
    sget-short v0, LX/0ws;->fe:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->r:Z

    .line 160931
    sget-short v0, LX/0ws;->fg:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->s:Z

    .line 160932
    sget v0, LX/0ws;->fu:I

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->t:I

    .line 160933
    sget v0, LX/0ws;->fs:I

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->u:I

    .line 160934
    sget-short v0, LX/0ws;->fo:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->v:Z

    .line 160935
    sget-short v0, LX/0ws;->fy:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->w:Z

    .line 160936
    sget v0, LX/0ws;->fx:I

    invoke-interface {p1, v0, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->x:I

    .line 160937
    sget-short v0, LX/0ws;->fz:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->y:Z

    .line 160938
    sget-short v0, LX/0ws;->ev:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->z:Z

    .line 160939
    sget-short v0, LX/0ws;->fc:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->A:Z

    .line 160940
    sget-short v0, LX/0ws;->fb:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->B:Z

    .line 160941
    sget-short v0, LX/0ws;->es:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->C:Z

    .line 160942
    sget-short v0, LX/0ws;->en:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->D:Z

    .line 160943
    sget-short v0, LX/0ws;->eq:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->E:Z

    .line 160944
    sget-short v0, LX/0ws;->ep:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->F:Z

    .line 160945
    const-string v0, "newsfeed,permalink,page_timeline,user_timeline,group,search,saved"

    .line 160946
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 160947
    iget-object v3, p0, LX/0wq;->af:LX/0ad;

    sget-char v6, LX/0ws;->eu:C

    invoke-interface {v3, v6, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 160948
    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_0

    aget-object v8, v6, v3

    .line 160949
    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160950
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 160951
    :cond_0
    move-object v0, v5

    .line 160952
    iput-object v0, p0, LX/0wq;->G:Ljava/util/Set;

    .line 160953
    sget-short v0, LX/0ws;->ew:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->H:Z

    .line 160954
    sget-short v0, LX/0ws;->et:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->I:Z

    .line 160955
    sget-short v0, LX/0ws;->eo:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->J:Z

    .line 160956
    sget-short v0, LX/0ws;->er:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->K:Z

    .line 160957
    sget v0, LX/0ws;->ey:I

    invoke-interface {p1, v0, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->L:I

    .line 160958
    sget v0, LX/0ws;->ex:I

    invoke-interface {p1, v0, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->M:I

    .line 160959
    sget-short v0, LX/0ws;->eW:S

    iget-object v3, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v4, 0x2b1

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->N:Z

    .line 160960
    sget-short v0, LX/0ws;->ff:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->O:Z

    .line 160961
    sget v0, LX/0ws;->eR:I

    const v3, 0xea60

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->P:I

    .line 160962
    sget-short v0, LX/0ws;->eS:S

    iget-object v3, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v4, 0x4cf

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->Q:Z

    .line 160963
    sget v0, LX/0ws;->fn:I

    const/16 v3, 0x140

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0wq;->R:I

    .line 160964
    sget-short v0, LX/0ws;->eX:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    sget-short v0, LX/0ws;->ax:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    sget-short v0, LX/0ws;->dV:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    sget-short v0, LX/0ws;->bO:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v2

    .line 160965
    :goto_2
    sget-char v3, LX/0ws;->ai:C

    sget-object v4, LX/0xT;->STATE_READY:LX/0xT;

    invoke-virtual {v4}, LX/0xT;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 160966
    invoke-static {v3}, LX/0xT;->of(Ljava/lang/String;)LX/0xT;

    move-result-object v3

    iput-object v3, p0, LX/0wq;->S:LX/0xT;

    .line 160967
    sget-short v3, LX/0ws;->ae:S

    invoke-interface {p1, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    iput-boolean v3, p0, LX/0wq;->T:Z

    .line 160968
    sget-short v3, LX/0ws;->ac:S

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    invoke-interface {p1, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->U:Z

    .line 160969
    sget-short v0, LX/0ws;->af:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->V:Z

    .line 160970
    sget-short v0, LX/0ws;->ad:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->W:Z

    .line 160971
    sget-short v0, LX/0ws;->ab:S

    iget-boolean v2, p0, LX/0wq;->C:Z

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->X:Z

    .line 160972
    sget-short v0, LX/0ws;->ah:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->Y:Z

    .line 160973
    sget-short v0, LX/0ws;->aj:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->Z:Z

    .line 160974
    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v2, 0x2c6

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->aa:Z

    .line 160975
    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v2, 0x2a4

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0wq;->ab:Z

    .line 160976
    return-void

    .line 160977
    :cond_2
    sget-object v0, LX/0wr;->HTTP:LX/0wr;

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 160978
    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 160906
    sget-object v0, LX/0wq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt p0, v0, :cond_0

    .line 160907
    sget-object v0, LX/0wq;->a:Ljava/util/List;

    add-int/lit8 v1, p0, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 160908
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0wq;
    .locals 1

    .prologue
    .line 160905
    invoke-static {p0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0wq;
    .locals 3

    .prologue
    .line 160903
    new-instance v2, LX/0wq;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v1}, LX/0wq;-><init>(LX/0ad;LX/0Uh;)V

    .line 160904
    return-object v2
.end method

.method private m()Z
    .locals 5

    .prologue
    .line 160900
    iget-boolean v1, p0, LX/0wq;->ad:Z

    iget-object v2, p0, LX/0wq;->af:LX/0ad;

    sget-short v3, LX/0ws;->fP:S

    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v4, 0x4d6

    invoke-virtual {v0, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    or-int/2addr v0, v1

    iput-boolean v0, p0, LX/0wq;->ad:Z

    .line 160901
    iget-boolean v0, p0, LX/0wq;->ad:Z

    return v0

    .line 160902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 160897
    iget-object v0, p0, LX/0wq;->G:Ljava/util/Set;

    const-string v1, "*"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160898
    const/4 v0, 0x1

    .line 160899
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0wq;->G:Ljava/util/Set;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 160885
    iget-object v1, p0, LX/0wq;->ae:LX/0Uh;

    sget v2, LX/19n;->q:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 160886
    sget-boolean v2, LX/0wq;->ac:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0wq;->af:LX/0ad;

    sget-short v3, LX/0ws;->fM:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 160895
    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    sget v1, LX/19n;->r:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 160896
    iget-object v1, p0, LX/0wq;->af:LX/0ad;

    sget-short v2, LX/0ws;->dj:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 160894
    iget-object v0, p0, LX/0wq;->af:LX/0ad;

    sget-short v1, LX/0ws;->bm:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160893
    sget-boolean v0, LX/0wq;->ac:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, LX/0wq;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v3, p0, LX/0wq;->af:LX/0ad;

    sget-short v4, LX/0ws;->fO:S

    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v5, 0x4cd

    invoke-virtual {v0, v5}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v5, LX/03R;->YES:LX/03R;

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 160892
    invoke-virtual {p0}, LX/0wq;->b()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 160891
    invoke-virtual {p0}, LX/0wq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/0wq;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 160890
    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v1, 0x2b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 160888
    iget-object v1, p0, LX/0wq;->ae:LX/0Uh;

    sget v2, LX/19n;->q:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 160889
    invoke-virtual {p0}, LX/0wq;->g()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/0wq;->af:LX/0ad;

    sget-short v3, LX/0ws;->fF:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, LX/0wq;->af:LX/0ad;

    sget-short v2, LX/0ws;->fS:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/0wq;->af:LX/0ad;

    sget-short v2, LX/0ws;->fR:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    .line 160887
    iget-object v0, p0, LX/0wq;->ae:LX/0Uh;

    const/16 v1, 0x2b3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
