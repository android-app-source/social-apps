.class public LX/1ha;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:LX/1h4;

.field private final c:LX/0ad;

.field private final d:Lcom/facebook/proxygen/HTTPClient;

.field public final e:LX/0Zb;

.field public final f:LX/0Uo;

.field private final g:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296170
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, LX/1ha;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/1h4;Landroid/os/Handler;LX/0Uo;LX/0ad;LX/0Zb;Lcom/facebook/proxygen/HTTPClient;)V
    .locals 3
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/proxygen/HTTPClient;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 296157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296158
    iput-object p2, p0, LX/1ha;->b:LX/1h4;

    .line 296159
    iput-object p5, p0, LX/1ha;->c:LX/0ad;

    .line 296160
    iput-object p7, p0, LX/1ha;->d:Lcom/facebook/proxygen/HTTPClient;

    .line 296161
    iput-object p6, p0, LX/1ha;->e:LX/0Zb;

    .line 296162
    iput-object p4, p0, LX/1ha;->f:LX/0Uo;

    .line 296163
    invoke-interface {p1}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    .line 296164
    new-instance v2, LX/1hb;

    invoke-direct {v2, p0}, LX/1hb;-><init>(LX/1ha;)V

    move-object v2, v2

    .line 296165
    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0, p3}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/1ha;->g:LX/0Yb;

    .line 296166
    iget-object v0, p0, LX/1ha;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 296167
    iget-object v0, p0, LX/1ha;->f:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296168
    new-instance v0, Lcom/facebook/http/executors/liger/LigerForegroundManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/http/executors/liger/LigerForegroundManager$1;-><init>(LX/1ha;)V

    const v1, 0x6b356bc9

    invoke-static {p3, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 296169
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/1ha;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 296144
    iget-object v0, p0, LX/1ha;->c:LX/0ad;

    sget-short v1, LX/0by;->V:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 296145
    :cond_0
    return-void

    .line 296146
    :cond_1
    iget-object v0, p0, LX/1ha;->c:LX/0ad;

    sget v1, LX/0by;->W:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 296147
    iget-object v1, p0, LX/1ha;->b:LX/1h4;

    invoke-virtual {v1, v0}, LX/1h4;->a(I)Ljava/util/List;

    move-result-object v1

    .line 296148
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 296149
    iget-object v2, p0, LX/1ha;->d:Lcom/facebook/proxygen/HTTPClient;

    sget-object v0, LX/1ha;->a:[Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/proxygen/HTTPClient;->connect([Ljava/lang/String;)V

    .line 296150
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 296151
    iget-object v3, p0, LX/1ha;->e:LX/0Zb;

    const-string v4, "liger_foreground_preconnect"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 296152
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-nez v4, :cond_3

    .line 296153
    :goto_1
    goto :goto_0

    .line 296154
    :cond_3
    const-string v4, "host"

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 296155
    const-string v4, "time_since_init"

    iget-object v5, p0, LX/1ha;->f:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->d()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 296156
    invoke-virtual {v3}, LX/0oG;->d()V

    goto :goto_1
.end method
