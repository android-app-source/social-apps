.class public final LX/0sy;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 153324
    sget-object v0, LX/0sz;->a:LX/0U1;

    sget-object v1, LX/0sz;->c:LX/0U1;

    sget-object v2, LX/0sz;->b:LX/0U1;

    sget-object v3, LX/0sz;->d:LX/0U1;

    sget-object v4, LX/0sz;->e:LX/0U1;

    sget-object v5, LX/0sz;->f:LX/0U1;

    sget-object v6, LX/0sz;->g:LX/0U1;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0sy;->a:LX/0Px;

    .line 153325
    new-instance v0, LX/0su;

    sget-object v1, LX/0sz;->a:LX/0U1;

    sget-object v2, LX/0sz;->b:LX/0U1;

    sget-object v3, LX/0sz;->c:LX/0U1;

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0sy;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 153326
    const-string v0, "consistency"

    sget-object v1, LX/0sy;->a:LX/0Px;

    sget-object v2, LX/0sy;->b:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 153327
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 153328
    const/16 v0, 0x3e

    if-lt p2, v0, :cond_0

    .line 153329
    :goto_0
    return-void

    .line 153330
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0
.end method
