.class public LX/159;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PARAMS:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "LX/1pW",
        "<TRESU",
        "LT;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/14N;

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPARAMS;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0e6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/11M;

.field private final e:LX/0lp;

.field private final f:LX/0lC;


# direct methods
.method public constructor <init>(LX/14N;Ljava/lang/Object;LX/0e6;LX/11M;LX/0lp;LX/0lC;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14N;",
            "TPARAMS;",
            "LX/0e6",
            "<TPARAMS;TRESU",
            "LT;",
            ">;",
            "LX/11M;",
            "LX/0lp;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180117
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14N;

    iput-object v0, p0, LX/159;->a:LX/14N;

    .line 180118
    iput-object p2, p0, LX/159;->b:Ljava/lang/Object;

    .line 180119
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    iput-object v0, p0, LX/159;->c:LX/0e6;

    .line 180120
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11M;

    iput-object v0, p0, LX/159;->d:LX/11M;

    .line 180121
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lp;

    iput-object v0, p0, LX/159;->e:LX/0lp;

    .line 180122
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    iput-object v0, p0, LX/159;->f:LX/0lC;

    .line 180123
    return-void
.end method

.method private b(Lorg/apache/http/HttpResponse;)LX/1pN;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180131
    invoke-static {p1}, LX/0rz;->a(Lorg/apache/http/HttpResponse;)Z

    move-result v5

    .line 180132
    const-string v0, "x-flatbuffer-fallback-json"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 180133
    :goto_0
    if-eqz v0, :cond_2

    sget-object v0, LX/14S;->JSONPARSER:LX/14S;

    .line 180134
    :goto_1
    const-string v3, "Content-Type"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    .line 180135
    if-eqz v3, :cond_3

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/x-fb-flatbuffer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 180136
    :goto_2
    iget-object v2, p0, LX/159;->a:LX/14N;

    .line 180137
    iget-object v3, v2, LX/14N;->k:LX/14S;

    move-object v2, v3

    .line 180138
    sget-object v3, LX/14S;->FLATBUFFER_IDL:LX/14S;

    if-ne v2, v3, :cond_0

    if-nez v1, :cond_0

    .line 180139
    sget-object v0, LX/14S;->JSONPARSER:LX/14S;

    .line 180140
    :cond_0
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    if-ne v0, v1, :cond_4

    .line 180141
    iget-object v0, p0, LX/159;->d:LX/11M;

    invoke-virtual {v0, p1}, LX/11M;->a(Lorg/apache/http/HttpResponse;)V

    .line 180142
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 180143
    iget-object v1, p0, LX/159;->e:LX/0lp;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v3

    .line 180144
    iget-object v0, p0, LX/159;->f:LX/0lC;

    invoke-virtual {v3, v0}, LX/15w;->a(LX/0lD;)V

    .line 180145
    new-instance v0, LX/1pN;

    iget-object v1, p0, LX/159;->a:LX/14N;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iget-object v4, p0, LX/159;->d:LX/11M;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/1pN;-><init>(LX/14N;ILjava/lang/Object;LX/11M;ZLjava/util/List;)V

    .line 180146
    :goto_3
    return-object v0

    :cond_1
    move v0, v2

    .line 180147
    goto :goto_0

    .line 180148
    :cond_2
    iget-object v0, p0, LX/159;->a:LX/14N;

    .line 180149
    iget-object v3, v0, LX/14N;->k:LX/14S;

    move-object v0, v3

    .line 180150
    goto :goto_1

    :cond_3
    move v1, v2

    .line 180151
    goto :goto_2

    .line 180152
    :cond_4
    sget-object v1, LX/14S;->STREAM:LX/14S;

    if-eq v0, v1, :cond_5

    sget-object v1, LX/14S;->FLATBUFFER:LX/14S;

    if-eq v0, v1, :cond_5

    sget-object v1, LX/14S;->FLATBUFFER_IDL:LX/14S;

    if-ne v0, v1, :cond_6

    .line 180153
    :cond_5
    iget-object v0, p0, LX/159;->d:LX/11M;

    invoke-virtual {v0, p1}, LX/11M;->a(Lorg/apache/http/HttpResponse;)V

    .line 180154
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 180155
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 180156
    new-instance v0, LX/1pN;

    iget-object v1, p0, LX/159;->a:LX/14N;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iget-object v4, p0, LX/159;->d:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILjava/io/InputStream;LX/11M;Z)V

    goto :goto_3

    .line 180157
    :cond_6
    sget-object v1, LX/14S;->JSON:LX/14S;

    if-ne v0, v1, :cond_7

    .line 180158
    new-instance v0, LX/2CN;

    iget-object v1, p0, LX/159;->f:LX/0lC;

    iget-object v2, p0, LX/159;->d:LX/11M;

    invoke-direct {v0, v1, v2}, LX/2CN;-><init>(LX/0lC;LX/11M;)V

    .line 180159
    invoke-virtual {v0, p1}, LX/2CN;->a(Lorg/apache/http/HttpResponse;)LX/0lF;

    move-result-object v3

    .line 180160
    new-instance v0, LX/1pN;

    iget-object v1, p0, LX/159;->a:LX/14N;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iget-object v4, p0, LX/159;->d:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILX/0lF;LX/11M;Z)V

    goto :goto_3

    .line 180161
    :cond_7
    sget-object v1, LX/14S;->STRING:LX/14S;

    if-ne v0, v1, :cond_8

    .line 180162
    new-instance v0, LX/3Et;

    iget-object v1, p0, LX/159;->d:LX/11M;

    invoke-direct {v0, v1}, LX/3Et;-><init>(LX/11M;)V

    .line 180163
    invoke-virtual {v0, p1}, LX/3Et;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v3

    .line 180164
    new-instance v0, LX/1pN;

    iget-object v1, p0, LX/159;->a:LX/14N;

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    iget-object v4, p0, LX/159;->d:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILjava/lang/String;LX/11M;Z)V

    goto :goto_3

    .line 180165
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown api response type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 180124
    invoke-direct {p0, p1}, LX/159;->b(Lorg/apache/http/HttpResponse;)LX/1pN;

    move-result-object v1

    .line 180125
    :try_start_0
    iget-object v0, p0, LX/159;->c:LX/0e6;

    iget-object v2, p0, LX/159;->b:Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, LX/0e6;->a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;

    move-result-object v0

    .line 180126
    new-instance v2, LX/1pW;

    invoke-direct {v2, v1, v0}, LX/1pW;-><init>(LX/1pN;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180127
    invoke-virtual {v1}, LX/1pN;->i()V

    return-object v2

    .line 180128
    :catch_0
    move-exception v0

    .line 180129
    :try_start_1
    invoke-static {v0}, LX/2BF;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180130
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1pN;->i()V

    throw v0
.end method
