.class public LX/1br;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1br;


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/drawee/components/DeferredReleaser$Releasable;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Handler;

.field public final d:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281341
    const/4 v0, 0x0

    sput-object v0, LX/1br;->a:LX/1br;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 281342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281343
    new-instance v0, Lcom/facebook/drawee/components/DeferredReleaser$1;

    invoke-direct {v0, p0}, Lcom/facebook/drawee/components/DeferredReleaser$1;-><init>(LX/1br;)V

    iput-object v0, p0, LX/1br;->d:Ljava/lang/Runnable;

    .line 281344
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1br;->b:Ljava/util/Set;

    .line 281345
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/1br;->c:Landroid/os/Handler;

    .line 281346
    return-void
.end method

.method public static declared-synchronized a()LX/1br;
    .locals 2

    .prologue
    .line 281347
    const-class v1, LX/1br;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1br;->a:LX/1br;

    if-nez v0, :cond_0

    .line 281348
    new-instance v0, LX/1br;

    invoke-direct {v0}, LX/1br;-><init>()V

    sput-object v0, LX/1br;->a:LX/1br;

    .line 281349
    :cond_0
    sget-object v0, LX/1br;->a:LX/1br;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 281350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 281351
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 281352
    return-void

    .line 281353
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/1bp;)V
    .locals 1

    .prologue
    .line 281354
    invoke-static {}, LX/1br;->c()V

    .line 281355
    iget-object v0, p0, LX/1br;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 281356
    return-void
.end method
