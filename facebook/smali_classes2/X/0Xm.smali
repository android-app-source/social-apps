.class public final LX/0Xm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/0SD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Byte;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:LX/0RB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/0Xn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0S7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Xm;)LX/0Xm;
    .locals 2
    .param p0    # LX/0Xm;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79389
    if-nez p0, :cond_0

    .line 79390
    new-instance p0, LX/0Xm;

    invoke-direct {p0}, LX/0Xm;-><init>()V

    .line 79391
    :cond_0
    iget-object v0, p0, LX/0Xm;->c:LX/0SD;

    if-eqz v0, :cond_1

    .line 79392
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reentrant injection or failed cleanup detected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79393
    :cond_1
    return-object p0
.end method


# virtual methods
.method public final a()LX/0R6;
    .locals 1

    .prologue
    .line 79394
    iget-object v0, p0, LX/0Xm;->g:LX/0S7;

    invoke-virtual {v0}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0QB;)Z
    .locals 3

    .prologue
    .line 79395
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v0

    iput-object v0, p0, LX/0Xm;->c:LX/0SD;

    .line 79396
    iget-object v0, p0, LX/0Xm;->c:LX/0SD;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/0SD;->b(B)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    iput-object v0, p0, LX/0Xm;->d:Ljava/lang/Byte;

    .line 79397
    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    invoke-interface {v0}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 79398
    if-nez v1, :cond_0

    .line 79399
    new-instance v0, LX/4fr;

    const-string v1, "Called context scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79400
    :cond_0
    const-class v0, LX/0RB;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RB;

    iput-object v0, p0, LX/0Xm;->e:LX/0RB;

    .line 79401
    invoke-static {v1}, LX/0RB;->a(Landroid/content/Context;)LX/0Xn;

    move-result-object v0

    iput-object v0, p0, LX/0Xm;->f:LX/0Xn;

    .line 79402
    iget-object v0, p0, LX/0Xm;->f:LX/0Xn;

    if-eqz v0, :cond_1

    .line 79403
    iget-object v0, p0, LX/0Xm;->f:LX/0Xn;

    invoke-interface {v0, p0}, LX/0Xn;->getProperty(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0Xm;->a:Ljava/lang/Object;

    .line 79404
    :goto_0
    iget-object v0, p0, LX/0Xm;->a:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 79405
    const/4 v0, 0x0

    .line 79406
    :goto_1
    return v0

    .line 79407
    :cond_1
    iget-object v0, p0, LX/0Xm;->b:Ljava/lang/Object;

    iput-object v0, p0, LX/0Xm;->a:Ljava/lang/Object;

    goto :goto_0

    .line 79408
    :cond_2
    invoke-interface {p1}, LX/0QB;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    iput-object v0, p0, LX/0Xm;->g:LX/0S7;

    .line 79409
    iget-object v0, p0, LX/0Xm;->e:LX/0RB;

    iget-object v2, p0, LX/0Xm;->g:LX/0S7;

    invoke-virtual {v0, v1, v2}, LX/0RB;->a(Landroid/content/Context;LX/0S7;)V

    .line 79410
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79411
    :try_start_0
    iget-object v0, p0, LX/0Xm;->g:LX/0S7;

    if-eqz v0, :cond_0

    .line 79412
    iget-object v0, p0, LX/0Xm;->g:LX/0S7;

    .line 79413
    invoke-virtual {v0}, LX/0S7;->c()V

    .line 79414
    :cond_0
    iget-object v0, p0, LX/0Xm;->f:LX/0Xn;

    if-eqz v0, :cond_3

    .line 79415
    iget-object v0, p0, LX/0Xm;->a:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 79416
    iget-object v0, p0, LX/0Xm;->f:LX/0Xn;

    iget-object v1, p0, LX/0Xm;->a:Ljava/lang/Object;

    invoke-interface {v0, p0, v1}, LX/0Xn;->setProperty(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79417
    :cond_1
    :goto_0
    iget-object v0, p0, LX/0Xm;->d:Ljava/lang/Byte;

    if-eqz v0, :cond_2

    .line 79418
    iget-object v0, p0, LX/0Xm;->c:LX/0SD;

    iget-object v1, p0, LX/0Xm;->d:Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    .line 79419
    iput-byte v1, v0, LX/0SD;->a:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79420
    :cond_2
    iput-object v2, p0, LX/0Xm;->g:LX/0S7;

    .line 79421
    iput-object v2, p0, LX/0Xm;->e:LX/0RB;

    .line 79422
    iput-object v2, p0, LX/0Xm;->f:LX/0Xn;

    .line 79423
    iput-object v2, p0, LX/0Xm;->d:Ljava/lang/Byte;

    .line 79424
    iput-object v2, p0, LX/0Xm;->c:LX/0SD;

    .line 79425
    iput-object v2, p0, LX/0Xm;->a:Ljava/lang/Object;

    .line 79426
    return-void

    .line 79427
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0Xm;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 79428
    iget-object v0, p0, LX/0Xm;->a:Ljava/lang/Object;

    iput-object v0, p0, LX/0Xm;->b:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 79429
    :catchall_0
    move-exception v0

    iput-object v2, p0, LX/0Xm;->g:LX/0S7;

    .line 79430
    iput-object v2, p0, LX/0Xm;->e:LX/0RB;

    .line 79431
    iput-object v2, p0, LX/0Xm;->f:LX/0Xn;

    .line 79432
    iput-object v2, p0, LX/0Xm;->d:Ljava/lang/Byte;

    .line 79433
    iput-object v2, p0, LX/0Xm;->c:LX/0SD;

    .line 79434
    iput-object v2, p0, LX/0Xm;->a:Ljava/lang/Object;

    throw v0
.end method
