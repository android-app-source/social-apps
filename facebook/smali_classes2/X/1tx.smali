.class public LX/1tx;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 336935
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 336936
    return-void
.end method

.method public static a(LX/1tw;)Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledConsts;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 336915
    invoke-virtual {p0}, LX/1tw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/1tw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 336916
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 336917
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Boolean;LX/03R;LX/00H;)Ljava/lang/Boolean;
    .locals 4
    .param p0    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledConsts;
        .end annotation
    .end param
    .param p2    # LX/03R;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/MessengerVoipAndroid;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336919
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v0, v3, :cond_0

    .line 336920
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 336921
    :goto_0
    return-object v0

    .line 336922
    :cond_0
    iget-object v0, p3, LX/00H;->j:LX/01T;

    move-object v0, v0

    .line 336923
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v3, :cond_1

    .line 336924
    iget-object v0, p3, LX/00H;->j:LX/01T;

    move-object v0, v0

    .line 336925
    sget-object v3, LX/01T;->PHONE:LX/01T;

    if-eq v0, v3, :cond_1

    .line 336926
    iget-object v0, p3, LX/00H;->j:LX/01T;

    move-object v0, v0

    .line 336927
    sget-object v3, LX/01T;->ALOHA:LX/01T;

    if-eq v0, v3, :cond_1

    .line 336928
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 336929
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "android.hardware.microphone"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 336930
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 336931
    :cond_2
    invoke-virtual {p2}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, LX/03R;->asBoolean()Z

    move-result v0

    .line 336932
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 336933
    goto :goto_1

    :cond_4
    move v1, v2

    .line 336934
    goto :goto_2
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 336918
    return-void
.end method
