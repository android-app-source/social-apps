.class public LX/1WS;
.super Landroid/util/LruCache;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Short;",
        ">;"
    }
.end annotation


# instance fields
.field private a:S


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268481
    const/16 v0, 0x96

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 268482
    const/4 v0, 0x0

    iput-short v0, p0, LX/1WS;->a:S

    .line 268483
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)S
    .locals 2

    .prologue
    .line 268484
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/1WS;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    .line 268485
    if-nez v0, :cond_0

    .line 268486
    iget-short v0, p0, LX/1WS;->a:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    .line 268487
    iget-short v1, p0, LX/1WS;->a:S

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    iput-short v1, p0, LX/1WS;->a:S

    .line 268488
    invoke-virtual {p0, p1, v0}, LX/1WS;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268489
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 268490
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 268491
    check-cast p3, Ljava/lang/Short;

    .line 268492
    invoke-virtual {p3}, Ljava/lang/Short;->shortValue()S

    move-result v0

    iput-short v0, p0, LX/1WS;->a:S

    .line 268493
    return-void
.end method
