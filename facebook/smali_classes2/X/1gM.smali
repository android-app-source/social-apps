.class public final LX/1gM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293995
    const-class v0, LX/1gM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1gM;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 293993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293994
    return-void
.end method

.method public static loadClass(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 293988
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 293989
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293990
    :goto_0
    return-void

    .line 293991
    :catch_0
    move-exception v0

    .line 293992
    sget-object v1, LX/1gM;->TAG:Ljava/lang/String;

    const-string v2, "Class %s not found"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static loadIsBackgroundRestartFinishMarker()V
    .locals 1

    .prologue
    .line 293986
    const-string v0, "com.facebook.common.classmarkers.IsBackgroundRestartFinish"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 293987
    return-void
.end method

.method public static loadIsMessengerStartToInboxFinishMarker()V
    .locals 1

    .prologue
    .line 293980
    const-string v0, "com.facebook.common.classmarkers.IsMessengerStartToInboxFinish"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 293981
    return-void
.end method

.method public static loadIsMessengerStartupBeginMarker()V
    .locals 1

    .prologue
    .line 293984
    const-string v0, "com.facebook.common.classmarkers.IsMessengerStartupBegin"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 293985
    return-void
.end method

.method public static loadIsNotColdStartRunMarker()V
    .locals 1

    .prologue
    .line 293982
    const-string v0, "com.facebook.common.classmarkers.IsNotColdStartRun"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 293983
    return-void
.end method
