.class public LX/1sx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[S

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 335684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335685
    const/4 v0, -0x1

    iput v0, p0, LX/1sx;->b:I

    .line 335686
    new-array v0, p1, [S

    iput-object v0, p0, LX/1sx;->a:[S

    .line 335687
    return-void
.end method


# virtual methods
.method public final a()S
    .locals 3

    .prologue
    .line 335670
    iget-object v0, p0, LX/1sx;->a:[S

    iget v1, p0, LX/1sx;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/1sx;->b:I

    aget-short v0, v0, v1

    return v0
.end method

.method public final a(S)V
    .locals 4

    .prologue
    .line 335688
    iget-object v0, p0, LX/1sx;->a:[S

    array-length v0, v0

    iget v1, p0, LX/1sx;->b:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    .line 335689
    const/4 v3, 0x0

    .line 335690
    iget-object v0, p0, LX/1sx;->a:[S

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [S

    .line 335691
    iget-object v1, p0, LX/1sx;->a:[S

    iget-object v2, p0, LX/1sx;->a:[S

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335692
    iput-object v0, p0, LX/1sx;->a:[S

    .line 335693
    :cond_0
    iget-object v0, p0, LX/1sx;->a:[S

    iget v1, p0, LX/1sx;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1sx;->b:I

    aput-short p1, v0, v1

    .line 335694
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 335671
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 335672
    const-string v0, "<ShortStack vector:["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335673
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/1sx;->a:[S

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 335674
    if-eqz v0, :cond_0

    .line 335675
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335676
    :cond_0
    iget v2, p0, LX/1sx;->b:I

    if-ne v0, v2, :cond_1

    .line 335677
    const-string v2, ">>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335678
    :cond_1
    iget-object v2, p0, LX/1sx;->a:[S

    aget-short v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 335679
    iget v2, p0, LX/1sx;->b:I

    if-ne v0, v2, :cond_2

    .line 335680
    const-string v2, "<<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335681
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335682
    :cond_3
    const-string v0, "]>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335683
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
