.class public LX/19X;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/19Z;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/19Z;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 208043
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/19Z;
    .locals 3

    .prologue
    .line 208044
    sget-object v0, LX/19X;->a:LX/19Z;

    if-nez v0, :cond_1

    .line 208045
    const-class v1, LX/19X;

    monitor-enter v1

    .line 208046
    :try_start_0
    sget-object v0, LX/19X;->a:LX/19Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208047
    if-eqz v2, :cond_0

    .line 208048
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208049
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    invoke-static {p0}, LX/19Y;->b(LX/0ad;)LX/19Z;

    move-result-object p0

    move-object v0, p0

    .line 208050
    sput-object v0, LX/19X;->a:LX/19Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208051
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208052
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208053
    :cond_1
    sget-object v0, LX/19X;->a:LX/19Z;

    return-object v0

    .line 208054
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 208056
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {v0}, LX/19Y;->b(LX/0ad;)LX/19Z;

    move-result-object v0

    return-object v0
.end method
