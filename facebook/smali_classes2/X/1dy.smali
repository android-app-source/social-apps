.class public LX/1dy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0sg;

.field public final b:LX/0tA;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0sg;LX/0tA;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287208
    iput-object p1, p0, LX/1dy;->a:LX/0sg;

    .line 287209
    iput-object p2, p0, LX/1dy;->b:LX/0tA;

    .line 287210
    iput-object p3, p0, LX/1dy;->c:LX/0Uh;

    .line 287211
    return-void
.end method

.method public static a(LX/0QB;)LX/1dy;
    .locals 1

    .prologue
    .line 287206
    invoke-static {p0}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1dy;
    .locals 4

    .prologue
    .line 287204
    new-instance v3, LX/1dy;

    invoke-static {p0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v0

    check-cast v0, LX/0sg;

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v1

    check-cast v1, LX/0tA;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/1dy;-><init>(LX/0sg;LX/0tA;LX/0Uh;)V

    .line 287205
    return-object v3
.end method

.method private b(LX/0jT;)LX/3Bq;
    .locals 1

    .prologue
    .line 287201
    iget-object v0, p0, LX/1dy;->a:LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object v0

    .line 287202
    invoke-interface {v0, p1}, LX/2lk;->a(LX/0jT;)Z

    .line 287203
    invoke-interface {v0}, LX/2lk;->d()LX/3Bq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0jT;)V
    .locals 1

    .prologue
    .line 287199
    invoke-direct {p0, p1}, LX/1dy;->b(LX/0jT;)LX/3Bq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1dy;->a(LX/3Bq;)V

    .line 287200
    return-void
.end method

.method public final a(LX/3Bq;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 287187
    iget-object v0, p0, LX/1dy;->b:LX/0tA;

    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0tA;->b(Ljava/lang/String;LX/3Bq;)V

    .line 287188
    return-void
.end method

.method public final a(LX/4VT;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4VT",
            "<**>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 287197
    new-instance v0, LX/4VL;

    iget-object v1, p0, LX/1dy;->c:LX/0Uh;

    const/4 v2, 0x1

    new-array v2, v2, [LX/4VT;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v0, v1, v2}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    invoke-virtual {p0, v0}, LX/1dy;->a(LX/3Bq;)V

    .line 287198
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "LX/0jT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 287195
    invoke-direct {p0, p2}, LX/1dy;->b(LX/0jT;)LX/3Bq;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/3Bq;)V

    .line 287196
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/3Bq;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "LX/3Bq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 287189
    invoke-static {}, LX/0tA;->a()Ljava/lang/String;

    move-result-object v0

    .line 287190
    iget-object v1, p0, LX/1dy;->b:LX/0tA;

    invoke-virtual {v1, v0, p2}, LX/0tA;->a(Ljava/lang/String;LX/3Bq;)V

    .line 287191
    new-instance v1, LX/4V1;

    invoke-direct {v1, p0, v0, p2}, LX/4V1;-><init>(LX/1dy;Ljava/lang/String;LX/3Bq;)V

    .line 287192
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 287193
    invoke-static {p1, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 287194
    return-void
.end method
