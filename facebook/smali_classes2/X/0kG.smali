.class public LX/0kG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0kH;


# instance fields
.field private final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/10t;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13p;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13s;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0l3;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18i;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/10x;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/110;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/112;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13j;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15K;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15L;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0l7;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oO;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15M;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JuI;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15P;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11g;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kd;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oP;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13k;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15Q;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15V;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15Z;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15a;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13l;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ESm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/10t;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13p;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13s;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0l3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/18i;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/10x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/110;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/112;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13j;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15K;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15L;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0l7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15M;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JuI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15P;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11g;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13k;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15Q;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15V;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15Z;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15a;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13l;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ESm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 126766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126767
    iput-object p1, p0, LX/0kG;->a:LX/0Ot;

    .line 126768
    iput-object p2, p0, LX/0kG;->b:LX/0Ot;

    .line 126769
    iput-object p3, p0, LX/0kG;->c:LX/0Ot;

    .line 126770
    iput-object p4, p0, LX/0kG;->d:LX/0Ot;

    .line 126771
    iput-object p5, p0, LX/0kG;->e:LX/0Ot;

    .line 126772
    iput-object p6, p0, LX/0kG;->f:LX/0Ot;

    .line 126773
    iput-object p7, p0, LX/0kG;->g:LX/0Ot;

    .line 126774
    iput-object p8, p0, LX/0kG;->h:LX/0Ot;

    .line 126775
    iput-object p9, p0, LX/0kG;->i:LX/0Ot;

    .line 126776
    iput-object p10, p0, LX/0kG;->j:LX/0Ot;

    .line 126777
    iput-object p11, p0, LX/0kG;->k:LX/0Ot;

    .line 126778
    iput-object p12, p0, LX/0kG;->l:LX/0Ot;

    .line 126779
    iput-object p13, p0, LX/0kG;->m:LX/0Ot;

    .line 126780
    iput-object p14, p0, LX/0kG;->n:LX/0Ot;

    .line 126781
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0kG;->o:LX/0Ot;

    .line 126782
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0kG;->p:LX/0Ot;

    .line 126783
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0kG;->q:LX/0Ot;

    .line 126784
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0kG;->r:LX/0Ot;

    .line 126785
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0kG;->s:LX/0Ot;

    .line 126786
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0kG;->t:LX/0Ot;

    .line 126787
    move-object/from16 v0, p21

    iput-object v0, p0, LX/0kG;->u:LX/0Ot;

    .line 126788
    move-object/from16 v0, p22

    iput-object v0, p0, LX/0kG;->v:LX/0Ot;

    .line 126789
    move-object/from16 v0, p23

    iput-object v0, p0, LX/0kG;->w:LX/0Ot;

    .line 126790
    move-object/from16 v0, p24

    iput-object v0, p0, LX/0kG;->x:LX/0Ot;

    .line 126791
    move-object/from16 v0, p25

    iput-object v0, p0, LX/0kG;->y:LX/0Ot;

    .line 126792
    move-object/from16 v0, p26

    iput-object v0, p0, LX/0kG;->z:LX/0Ot;

    .line 126793
    move-object/from16 v0, p27

    iput-object v0, p0, LX/0kG;->A:LX/0Ot;

    .line 126794
    return-void
.end method

.method public static b(LX/0QB;)LX/0kG;
    .locals 30

    .prologue
    .line 126795
    new-instance v2, LX/0kG;

    const/16 v3, 0x7f

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xb8

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x82

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x175

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x200

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x22e

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x23c

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2b5

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x4e9

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x5ca

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x5df

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xbce

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xc62

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xcf5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x2ac3

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0xeb4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0xf10

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0xf11

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x10c7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x11d3

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x11e3

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x1326

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x1342

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x1347

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x134d

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x3804

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x13f7

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    invoke-direct/range {v2 .. v29}, LX/0kG;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 126796
    return-object v2
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126797
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126798
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 126799
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 14

    .prologue
    .line 126800
    const-string v0, "AldrinStatusCheckActivityListener.onActivityCreate"

    const v1, 0x16069942

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126801
    :try_start_0
    iget-object v0, p0, LX/0kG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10t;

    .line 126802
    iget-object v1, v0, LX/10t;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126803
    :cond_0
    :goto_0
    const v0, -0x1bfe3a03

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126804
    const-string v0, "AuthenticatedActivityHelper.onActivityCreate"

    const v1, -0x1dab0ce

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126805
    :try_start_1
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    .line 126806
    instance-of v1, p1, LX/0l6;

    if-eqz v1, :cond_7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126807
    :goto_1
    const v0, -0x2fa3186e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126808
    const-string v0, "ActivityCleaner.onActivityCreate"

    const v1, 0x55d9aef0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126809
    :try_start_2
    iget-object v0, p0, LX/0kG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10x;

    .line 126810
    invoke-static {v0, p1}, LX/10x;->f(LX/10x;Landroid/app/Activity;)V

    .line 126811
    iget-object v1, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v1}, LX/0fU;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 126812
    const v0, -0x4bc2b14a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126813
    const-string v0, "ChoreographedActivityListener.onActivityCreate"

    const v1, 0x479207ad

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126814
    :try_start_3
    iget-object v0, p0, LX/0kG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/110;

    .line 126815
    invoke-static {v0, p1}, LX/110;->c(LX/110;Landroid/app/Activity;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 126816
    const v0, -0x1edc12dd

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126817
    const-string v0, "IntentLogger.onBeforeActivityCreate"

    const v1, -0x1ca8b588

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126818
    :try_start_4
    iget-object v0, p0, LX/0kG;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/112;

    .line 126819
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 126820
    iget-object v3, v0, LX/112;->c:LX/0Uh;

    const/16 v4, 0x281

    invoke-virtual {v3, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 126821
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "android_security_intent_handling"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 126822
    const-string v4, "intent_info"

    invoke-static {v2}, LX/36g;->a(Landroid/content/Intent;)LX/0m9;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 126823
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 126824
    const-string v5, "activity_class"

    invoke-virtual {v3, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 126825
    iget-object v4, v0, LX/112;->b:LX/113;

    .line 126826
    const/16 v11, 0xa

    const/4 v8, 0x1

    .line 126827
    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->size()I

    move-result v7

    if-ne v7, v11, :cond_1

    .line 126828
    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    .line 126829
    :cond_1
    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    iget-object v9, v4, LX/113;->b:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 126830
    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->size()I

    move-result v7

    if-ge v7, v11, :cond_8

    move-object v7, v4

    .line 126831
    :goto_2
    const/4 v9, 0x0

    move v13, v9

    move-object v9, v7

    move v7, v13

    :goto_3
    iput-boolean v7, v9, LX/113;->d:Z

    .line 126832
    iget-boolean v7, v4, LX/113;->d:Z

    if-eqz v7, :cond_2

    iget-boolean v7, v4, LX/113;->e:Z

    if-nez v7, :cond_2

    .line 126833
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "android_security_intent_handling_too_many"

    invoke-direct {v7, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 126834
    iget-object v9, v4, LX/113;->a:LX/0Zb;

    invoke-interface {v9, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 126835
    const/4 v7, 0x1

    iput-boolean v7, v4, LX/113;->e:Z

    .line 126836
    iput-boolean v8, v4, LX/113;->d:Z

    .line 126837
    :cond_2
    iget-boolean v6, v4, LX/113;->d:Z

    if-nez v6, :cond_3

    .line 126838
    iget-object v6, v4, LX/113;->a:LX/0Zb;

    invoke-interface {v6, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 126839
    :cond_3
    const/4 v6, 0x0

    .line 126840
    iget-object v3, v0, LX/112;->c:LX/0Uh;

    const/16 v4, 0x282

    invoke-virtual {v3, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 126841
    iget-object v3, v0, LX/112;->d:LX/114;

    invoke-virtual {v3}, LX/114;->b()LX/115;

    move-result-object v3

    .line 126842
    invoke-virtual {v3, p1, v2}, LX/115;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 126843
    sget-object v3, LX/112;->a:Ljava/lang/String;

    const-string v4, "Cancelling intent because of server-configured switch-off: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126844
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 126845
    :cond_4
    const v0, -0x2cccfc2b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126846
    const-string v0, "InterstitialActivityListener.onActivityCreate"

    const v1, -0x35632917    # -5139316.5f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126847
    :try_start_5
    iget-object v0, p0, LX/0kG;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l7;

    invoke-virtual {v0, p1}, LX/0l7;->b(Landroid/app/Activity;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 126848
    const v0, -0x3486a73

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126849
    const-string v0, "FB4APerfActivityListener.onActivityCreate"

    const v1, -0xb9cf42f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126850
    :try_start_6
    iget-object v0, p0, LX/0kG;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11g;

    .line 126851
    if-eqz p1, :cond_5

    .line 126852
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/11g;->a(LX/11g;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 126853
    :cond_5
    const v0, -0x54b2b6ce

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126854
    return-void

    .line 126855
    :catchall_0
    move-exception v0

    const v1, -0x143041f3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126856
    :catchall_1
    move-exception v0

    const v1, -0x1aad3b7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126857
    :catchall_2
    move-exception v0

    const v1, 0x420fb047

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126858
    :catchall_3
    move-exception v0

    const v1, -0x1acea5b5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126859
    :catchall_4
    move-exception v0

    const v1, 0x20b5bd29

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126860
    :catchall_5
    move-exception v0

    const v1, 0x6853a6da

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126861
    :catchall_6
    move-exception v0

    const v1, 0x9970438

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126862
    :cond_6
    :try_start_7
    instance-of v1, p1, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;

    if-nez v1, :cond_0

    .line 126863
    iget-object v1, v0, LX/10t;->b:LX/0Uq;

    invoke-virtual {v1}, LX/0Uq;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126864
    instance-of v1, p1, LX/0l6;

    if-nez v1, :cond_0

    .line 126865
    iget-object v1, v0, LX/10t;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GQW;

    invoke-virtual {v1, p1}, LX/GQW;->a(Landroid/content/Context;)V

    goto/16 :goto_0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 126866
    :cond_7
    :try_start_8
    iget-object v1, v0, LX/0l3;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;

    invoke-direct {v2, v0, p1}, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$1;-><init>(LX/0l3;Landroid/app/Activity;)V

    const v3, -0x5ace8822

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 126867
    :cond_8
    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iget-object v7, v4, LX/113;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v7}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    sub-long/2addr v9, v11

    .line 126868
    const-wide/32 v11, 0xea60

    cmp-long v7, v9, v11

    if-gez v7, :cond_9

    move v7, v8

    move-object v9, v4

    goto/16 :goto_3

    :cond_9
    move-object v7, v4

    goto/16 :goto_2
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 126475
    iget-object v0, p0, LX/0kG;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18i;

    .line 126476
    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    invoke-static {p2, p4}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(ILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "isSendClickedFlag"

    const/4 v2, 0x0

    invoke-virtual {p4, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126477
    new-instance v1, LX/0ju;

    invoke-direct {v1, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0818eb

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0818ec

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    invoke-static {p4}, LX/5MF;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p4

    aput-object p4, p2, p3

    invoke-virtual {v2, p0, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0818ed

    new-instance p0, LX/6FQ;

    invoke-direct {p0, v0}, LX/6FQ;-><init>(LX/18i;)V

    invoke-virtual {v1, v2, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 126478
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 126869
    iget-object v0, p0, LX/0kG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10x;

    .line 126870
    iget-object v1, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v1}, LX/0fU;->d()V

    .line 126871
    iget-object v0, p0, LX/0kG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 126872
    iget-object v1, v0, LX/13j;->e:LX/0yc;

    invoke-virtual {v1, p1, p2}, LX/0yc;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    iput-boolean v1, v0, LX/13j;->i:Z

    .line 126873
    iget-object v0, p0, LX/0kG;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JuI;

    .line 126874
    if-eqz p2, :cond_0

    .line 126875
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    .line 126876
    if-eqz p0, :cond_0

    .line 126877
    const-string v1, "EVENT_TYPE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3B2;

    .line 126878
    const-string p1, "NOTIF_LOG"

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 126879
    if-eqz v1, :cond_0

    if-eqz p0, :cond_0

    sget-object p1, LX/3B2;->COMMENT_FROM_TRAY:LX/3B2;

    invoke-virtual {v1, p1}, LX/3B2;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126880
    iget-object v1, v0, LX/JuI;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Yg;

    sget-object p1, LX/3B2;->COMMENT_FROM_TRAY:LX/3B2;

    invoke-virtual {v1, p0, p1}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 126881
    iget-object v1, v0, LX/JuI;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1rn;

    invoke-virtual {v1, p0}, LX/1rn;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 126882
    iget v1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->p:I

    move p1, v1

    .line 126883
    iget-object v1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->q:Ljava/lang/String;

    move-object p0, v1

    .line 126884
    if-ltz p1, :cond_0

    if-eqz p0, :cond_0

    .line 126885
    iget-object v1, v0, LX/JuI;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-virtual {v1, p0, p1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 126886
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 126887
    iget-object v0, p0, LX/0kG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13s;

    .line 126888
    iget-object v1, v0, LX/13s;->e:LX/0gh;

    iget-object v2, v0, LX/13s;->b:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0gh;->a(IZ)V

    .line 126889
    iget-object v0, p0, LX/0kG;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l7;

    .line 126890
    iget-object v1, v0, LX/0l7;->b:LX/0Uq;

    invoke-virtual {v1}, LX/0Uq;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 126891
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0kG;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oO;

    .line 126892
    invoke-static {v0}, LX/0oO;->c(LX/0oO;)Ljava/util/Locale;

    move-result-object v1

    iput-object v1, v0, LX/0oO;->a:Ljava/util/Locale;

    .line 126893
    iget-object v0, p0, LX/0kG;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESm;

    .line 126894
    iget-object v1, v0, LX/ESm;->a:LX/7zZ;

    .line 126895
    iget-boolean v0, v1, LX/7zZ;->g:Z

    if-eqz v0, :cond_1

    .line 126896
    invoke-static {v1}, LX/7zZ;->c(LX/7zZ;)V

    .line 126897
    :cond_1
    return-void

    .line 126898
    :cond_2
    iget-object v1, v0, LX/0l7;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CONFIGURATION_CHANGED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;

    move-result-object v1

    .line 126899
    if-eqz v1, :cond_0

    instance-of v2, v1, LX/0i0;

    if-eqz v2, :cond_0

    .line 126900
    check-cast v1, LX/0i0;

    .line 126901
    iget-object v2, v1, LX/0i0;->b:LX/0iU;

    .line 126902
    invoke-virtual {v2}, LX/0iV;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v2, LX/0iV;->d:Lcom/facebook/apptab/state/TabTag;

    if-nez v1, :cond_4

    .line 126903
    :cond_3
    :goto_1
    goto :goto_0

    .line 126904
    :cond_4
    iget-object v1, v2, LX/0iV;->d:Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v2, v1}, LX/0iV;->b(Lcom/facebook/apptab/state/TabTag;)V

    goto :goto_1
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 126905
    const-string v0, "AuthenticatedActivityHelper.onBeforeSuperOnCreate"

    const v1, 0x47bec59f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126906
    :try_start_0
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    .line 126907
    if-eqz p2, :cond_1

    const-string v1, "loggedInUser"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126908
    const-string v1, "loggedInUser"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0l3;->k:Ljava/lang/String;

    .line 126909
    :goto_0
    invoke-static {v0, p1}, LX/0l3;->e(LX/0l3;Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126910
    :goto_1
    const v0, 0x7e06366f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126911
    const-string v0, "InterstitialActivityListener.onBeforeSuperOnCreate"

    const v1, -0x71ba0d46

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126912
    :try_start_1
    iget-object v0, p0, LX/0kG;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l7;

    const/4 p2, 0x0

    const/4 v3, 0x0

    .line 126913
    iget-object v1, v0, LX/0l7;->b:LX/0Uq;

    invoke-virtual {v1}, LX/0Uq;->c()Z

    move-result v1

    if-nez v1, :cond_3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126914
    :cond_0
    :goto_2
    const v0, 0x10de27e3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126915
    const-string v0, "AppStartupNotifier.onCreated"

    const v1, 0x4453ba1b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126916
    :try_start_2
    iget-object v0, p0, LX/0kG;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kd;

    .line 126917
    iget-object v2, v0, LX/0kd;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 126918
    iput-wide v2, v0, LX/0kd;->f:J

    .line 126919
    invoke-static {v0, v2, v3}, LX/0kd;->e(LX/0kd;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 126920
    const v0, -0x41b4f575

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126921
    return-void

    .line 126922
    :catchall_0
    move-exception v0

    const v1, -0x26f88998

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126923
    :catchall_1
    move-exception v0

    const v1, 0x2617b7ee

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126924
    :catchall_2
    move-exception v0

    const v1, 0x47b7e08e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126925
    :cond_1
    :try_start_3
    iget-object v1, v0, LX/0l3;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v0, LX/0l3;->k:Ljava/lang/String;

    goto :goto_0

    .line 126926
    :cond_2
    iget-object v1, v0, LX/0l3;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GvB;

    invoke-virtual {v1, p1}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 126927
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 126928
    :cond_3
    iget-object v1, v0, LX/0l7;->e:LX/0Uh;

    const/16 v2, 0xe5

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126929
    iget-object v1, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_4

    .line 126930
    iget-object v1, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    invoke-interface {v1, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 126931
    iput-object p2, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    .line 126932
    :cond_4
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/0l7;->h:Ljava/lang/ref/WeakReference;

    .line 126933
    iput-object p2, v0, LX/0l7;->i:Ljava/lang/Boolean;

    .line 126934
    iget-object v1, v0, LX/0l7;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    new-instance v2, LX/6Xu;

    invoke-direct {v2, v0, p1}, LX/6Xu;-><init>(LX/0l7;Landroid/app/Activity;)V

    const v3, 0x6d3efc2a

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    goto/16 :goto_2
.end method

.method public final a(Landroid/app/Activity;Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 126935
    iget-object v0, p0, LX/0kG;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11g;

    .line 126936
    if-eqz p2, :cond_0

    .line 126937
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, LX/11g;->a(LX/11g;Ljava/lang/String;)V

    .line 126938
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 126939
    return-void
.end method

.method public final a(ILandroid/app/Dialog;)Z
    .locals 1

    .prologue
    .line 126765
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    .line 126940
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    const/4 v1, 0x0

    .line 126941
    instance-of p0, p1, LX/0l6;

    if-eqz p0, :cond_2

    .line 126942
    :cond_0
    :goto_0
    move v0, v1

    .line 126943
    if-eqz v0, :cond_1

    .line 126944
    const/4 v0, 0x1

    .line 126945
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 126946
    :cond_2
    iget-object p0, v0, LX/0l3;->e:LX/0l4;

    invoke-virtual {p0, p2}, LX/0l4;->isInvalidSessionException(Ljava/lang/Throwable;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 126947
    invoke-static {v0, p1}, LX/0l3;->d(LX/0l3;Landroid/app/Activity;)V

    .line 126948
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126762
    iget-object v0, p0, LX/0kG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    invoke-virtual {v0, p2}, LX/13l;->a(I)LX/0am;

    move-result-object v0

    .line 126763
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126764
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 7

    .prologue
    .line 126715
    const-string v0, "ActivityCleaner.onStart"

    const v1, 0x602772d4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126716
    :try_start_0
    iget-object v0, p0, LX/0kG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10x;

    .line 126717
    iget-object v2, v0, LX/10x;->f:LX/0fU;

    .line 126718
    iget-object v3, v2, LX/0fU;->d:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0fc;

    .line 126719
    if-nez v3, :cond_1

    .line 126720
    new-instance v4, LX/0fc;

    invoke-direct {v4, p1}, LX/0fc;-><init>(Landroid/app/Activity;)V

    .line 126721
    iget-object v3, v2, LX/0fU;->d:Ljava/util/Map;

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126722
    iget-object v3, v2, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126723
    iget v3, v2, LX/0fU;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, LX/0fU;->c:I

    .line 126724
    iget-object v3, v2, LX/0fU;->b:Ljava/util/List;

    const/4 v5, 0x0

    invoke-static {v3, v5}, LX/0Ph;->c(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0fc;

    .line 126725
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, LX/0fU;->b(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126726
    iget-object v3, v2, LX/0fU;->e:Ljava/util/Set;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v3, v4

    .line 126727
    :cond_1
    move-object v2, v3

    .line 126728
    iget-object v3, v0, LX/10x;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 126729
    iput-wide v4, v2, LX/0fc;->c:J

    .line 126730
    iget-object v2, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v2}, LX/0fU;->d()V

    .line 126731
    iget v2, v0, LX/10x;->c:I

    invoke-static {v0, v2, p1}, LX/10x;->a$redex0(LX/10x;ILandroid/app/Activity;)V

    .line 126732
    iget-object v2, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v2}, LX/0fU;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126733
    const v0, 0x55bda095

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126734
    const-string v0, "DialtoneActivityListener.onStart"

    const v1, 0x19459590

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126735
    :try_start_1
    iget-object v0, p0, LX/0kG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    invoke-virtual {v0, p1}, LX/13j;->a(Landroid/app/Activity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126736
    const v0, 0x77029185

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126737
    const-string v0, "AppStartupNotifier.onStarted"

    const v1, -0x45627102

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126738
    :try_start_2
    iget-object v0, p0, LX/0kG;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kd;

    .line 126739
    iget-object v2, v0, LX/0kd;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 126740
    iput-wide v2, v0, LX/0kd;->g:J

    .line 126741
    invoke-static {v0, v2, v3}, LX/0kd;->e(LX/0kd;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 126742
    const v0, 0x64d522f4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126743
    const-string v0, "SelfUpdateActivityListener.onStart"

    const v1, 0x51d1b55e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126744
    :try_start_3
    iget-object v0, p0, LX/0kG;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13k;

    .line 126745
    iput-object p1, v0, LX/13k;->a:Landroid/app/Activity;

    .line 126746
    iget-object v1, v0, LX/13k;->a:Landroid/app/Activity;

    instance-of v1, v1, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    if-eqz v1, :cond_2

    .line 126747
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/13k;->b:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 126748
    :cond_2
    const v0, -0x497b4de4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126749
    const-string v0, "PlayerActivityManager.onStart"

    const v1, -0x4ae5c42e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126750
    :try_start_4
    iget-object v0, p0, LX/0kG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    invoke-virtual {v0}, LX/13l;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 126751
    const v0, 0x282e4345

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126752
    const-string v0, "ZeroActivityListener.onStart"

    const v1, 0x4fa1ae39

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126753
    :try_start_5
    iget-object v0, p0, LX/0kG;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    invoke-virtual {v0}, LX/13n;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 126754
    const v0, -0x59382491

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126755
    return-void

    .line 126756
    :catchall_0
    move-exception v0

    const v1, 0x2fc8fe1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126757
    :catchall_1
    move-exception v0

    const v1, -0x2e2f36e9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126758
    :catchall_2
    move-exception v0

    const v1, -0x1d688c7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126759
    :catchall_3
    move-exception v0

    const v1, -0x60978833

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126760
    :catchall_4
    move-exception v0

    const v1, 0x5686a8ab

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126761
    :catchall_5
    move-exception v0

    const v1, -0x2b903074

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 126694
    const-string v0, "LanguageSwitcherListener.onBeforeActivityCreate"

    const v1, -0x3a390615

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126695
    :try_start_0
    iget-object v0, p0, LX/0kG;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oO;

    .line 126696
    invoke-static {v0}, LX/0oO;->c(LX/0oO;)Ljava/util/Locale;

    move-result-object v1

    iput-object v1, v0, LX/0oO;->a:Ljava/util/Locale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126697
    const v0, 0x28280084

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126698
    const-string v0, "StringResourcesActivityListener.onBeforeActivityCreate"

    const v1, 0x4819c6ea

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126699
    :try_start_1
    iget-object v0, p0, LX/0kG;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oP;

    .line 126700
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 126701
    instance-of v1, v1, LX/0Vs;

    if-nez v1, :cond_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126702
    :cond_0
    :goto_0
    const v0, -0xd468ca9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126703
    return-void

    .line 126704
    :catchall_0
    move-exception v0

    const v1, -0x45e96a7b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126705
    :catchall_1
    move-exception v0

    const v1, 0xeb605de

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126706
    :cond_1
    iget-object v1, v0, LX/0oP;->b:LX/0Vv;

    invoke-virtual {v1}, LX/0Vv;->g()V

    .line 126707
    iget-object v1, v0, LX/0oP;->b:LX/0Vv;

    invoke-virtual {v1}, LX/0Vv;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 126708
    if-eqz p1, :cond_2

    const-class v1, LX/2JL;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 126709
    if-nez v1, :cond_0

    .line 126710
    new-instance v1, Landroid/content/Intent;

    const-class p0, Lcom/facebook/resources/impl/WaitingForStringsActivity;

    invoke-direct {v1, p1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126711
    const-string p0, "return_intent"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p2

    invoke-virtual {v1, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 126712
    iget-object p0, v0, LX/0oP;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 126713
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 126714
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 126690
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    .line 126691
    iget-object v1, v0, LX/0l3;->k:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 126692
    :goto_0
    return-void

    .line 126693
    :cond_0
    const-string v1, "loggedInUser"

    iget-object p0, v0, LX/0l3;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 126689
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 7

    .prologue
    .line 126568
    const-string v0, "ImpressionActivityListener.onResume"

    const v1, -0x2324d294

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126569
    :try_start_0
    iget-object v0, p0, LX/0kG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13p;

    .line 126570
    iget-object v1, v0, LX/13p;->a:LX/0kv;

    invoke-virtual {v1, p1}, LX/0kv;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126571
    const v0, -0x11c41d30

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126572
    const-string v0, "AnalyticsActivityListener.onResume"

    const v1, 0x37f7f452

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126573
    :try_start_1
    iget-object v0, p0, LX/0kG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13s;

    invoke-virtual {v0, p1}, LX/13s;->a(Landroid/app/Activity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126574
    const v0, 0x78c41d29

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126575
    const-string v0, "AuthenticatedActivityHelper.onResume"

    const v1, -0xe87921c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126576
    :try_start_2
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    .line 126577
    invoke-static {v0, p1}, LX/0l3;->e(LX/0l3;Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 126578
    :goto_0
    const v0, 0x427c6789

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126579
    const-string v0, "DialtoneActivityListener.onResume"

    const v1, -0x8e1b289

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126580
    :try_start_3
    iget-object v0, p0, LX/0kG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    invoke-virtual {v0, p1}, LX/13j;->b(Landroid/app/Activity;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 126581
    const v0, -0x1b451d1e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126582
    const-string v0, "UiMetricsActivityListener.onResume"

    const v1, -0x600e5173

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126583
    :try_start_4
    iget-object v0, p0, LX/0kG;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15K;

    .line 126584
    invoke-static {v0}, LX/15K;->b$redex0(LX/15K;)Z

    move-result v2

    if-nez v2, :cond_9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 126585
    :goto_1
    const v0, 0x21a9f6a4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126586
    const-string v0, "FeedAutoplayActivityListener.onResume"

    const v1, -0x1ff722e5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126587
    :try_start_5
    iget-object v0, p0, LX/0kG;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15L;

    .line 126588
    iget-object v1, v0, LX/15L;->b:Ljava/util/List;

    invoke-static {v1}, LX/13m;->a(Ljava/util/Collection;)V

    .line 126589
    iget-object v1, v0, LX/15L;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 126590
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Aa;

    .line 126591
    if-eqz v1, :cond_0

    .line 126592
    invoke-virtual {v1}, LX/1Aa;->f()V

    goto :goto_2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 126593
    :cond_1
    const v0, 0x550a1091

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126594
    const-string v0, "LanguageSwitcherListener.onResume"

    const v1, -0x1033c71f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126595
    :try_start_6
    iget-object v0, p0, LX/0kG;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oO;

    .line 126596
    iget-object v1, v0, LX/0oO;->a:Ljava/util/Locale;

    if-nez v1, :cond_b
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 126597
    :cond_2
    :goto_3
    const v0, -0x5f96813a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126598
    const-string v0, "ChatHeadsActivityListener.onResume"

    const v1, -0x6ba45d81

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126599
    :try_start_7
    iget-object v0, p0, LX/0kG;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15M;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 126600
    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_d
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 126601
    :cond_3
    :goto_4
    const v0, 0x6deabc06

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126602
    const-string v0, "MessagesForegroundActivityListener.onResume"

    const v1, -0x2432663e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126603
    :try_start_8
    iget-object v0, p0, LX/0kG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15P;

    .line 126604
    iput-object p1, v0, LX/15P;->b:Landroid/app/Activity;

    .line 126605
    iget-object v1, v0, LX/15P;->a:LX/0c4;

    if-eqz v1, :cond_4

    .line 126606
    iget-object v1, v0, LX/15P;->a:LX/0c4;

    sget-object v2, LX/0cB;->f:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0c4;->a(Landroid/net/Uri;Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 126607
    :cond_4
    const v0, -0x4122ca7b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126608
    const-string v0, "ActivityListenerForHandlingSentryRestrictions.onActivityForegrounded"

    const v1, 0x4f137116

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126609
    :try_start_9
    iget-object v0, p0, LX/0kG;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15Q;

    .line 126610
    iget-object v1, v0, LX/15Q;->a:LX/15R;

    .line 126611
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, LX/15R;->f:Ljava/lang/ref/WeakReference;

    .line 126612
    iget-object v2, v1, LX/15R;->e:LX/0Yb;

    if-nez v2, :cond_5

    .line 126613
    iget-object v2, v1, LX/15R;->d:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "BlockAccessRestrictionForGraphQLAction"

    new-instance v0, LX/15U;

    invoke-direct {v0, v1}, LX/15U;-><init>(LX/15R;)V

    invoke-interface {v2, v3, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, v1, LX/15R;->e:LX/0Yb;

    .line 126614
    :cond_5
    iget-object v2, v1, LX/15R;->e:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->a()Z

    move-result v2

    if-nez v2, :cond_6

    .line 126615
    iget-object v2, v1, LX/15R;->e:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    .line 126616
    :cond_6
    const v0, -0x52ce28ac

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126617
    const-string v0, "DownloadVideoUtils.onActivityResumed"

    const v1, 0x79898167

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126618
    :try_start_a
    iget-object v0, p0, LX/0kG;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15V;

    .line 126619
    iput-object p1, v0, LX/15V;->j:Landroid/app/Activity;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 126620
    const v0, -0x3dd9d2fc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126621
    const-string v0, "ProxyActivityListener.onResume"

    const v1, -0x5bc74e0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126622
    :try_start_b
    iget-object v0, p0, LX/0kG;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15Z;

    .line 126623
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/15Z;->a:Z

    .line 126624
    iget-object v1, v0, LX/15Z;->b:LX/0T2;

    if-eqz v1, :cond_7

    .line 126625
    iget-object v1, v0, LX/15Z;->b:LX/0T2;

    invoke-interface {v1, p1}, LX/0T2;->c(Landroid/app/Activity;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 126626
    :cond_7
    const v0, -0x28926ec6    # -2.61207999E14f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126627
    const-string v0, "TextureAttachManager.onResume"

    const v1, 0x540fcd8b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126628
    :try_start_c
    iget-object v0, p0, LX/0kG;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15a;

    invoke-virtual {v0, p1}, LX/15a;->a(Landroid/app/Activity;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    .line 126629
    const v0, 0x2c8389d1

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126630
    const-string v0, "PlayerActivityManager.onResume"

    const v1, -0x4ef90781

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126631
    :try_start_d
    iget-object v0, p0, LX/0kG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    invoke-virtual {v0, p1}, LX/13l;->a(Landroid/app/Activity;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_d

    .line 126632
    const v0, 0x1fd1b420

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126633
    const-string v0, "ZeroActivityListener.onResume"

    const v1, -0x2b5c5acc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 126634
    :try_start_e
    iget-object v0, p0, LX/0kG;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    .line 126635
    iput-object p1, v0, LX/13n;->a:Landroid/app/Activity;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_e

    .line 126636
    const v0, -0x4f592d4e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 126637
    return-void

    .line 126638
    :catchall_0
    move-exception v0

    const v1, -0x2aeb172e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126639
    :catchall_1
    move-exception v0

    const v1, -0x18bc3be7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126640
    :catchall_2
    move-exception v0

    const v1, -0x37d22c60

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126641
    :catchall_3
    move-exception v0

    const v1, 0x4cdd8937    # 1.16148664E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126642
    :catchall_4
    move-exception v0

    const v1, 0x72c5a99

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126643
    :catchall_5
    move-exception v0

    const v1, -0x7e555ada

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126644
    :catchall_6
    move-exception v0

    const v1, -0x7d3211c0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126645
    :catchall_7
    move-exception v0

    const v1, -0x575c00bf

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126646
    :catchall_8
    move-exception v0

    const v1, -0x1b2a796e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126647
    :catchall_9
    move-exception v0

    const v1, 0x461107b0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126648
    :catchall_a
    move-exception v0

    const v1, 0x11305fe7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126649
    :catchall_b
    move-exception v0

    const v1, -0x496185ef

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126650
    :catchall_c
    move-exception v0

    const v1, -0x3e08a5c3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126651
    :catchall_d
    move-exception v0

    const v1, -0x1dcc376f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126652
    :catchall_e
    move-exception v0

    const v1, -0x63b4de9a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 126653
    :cond_8
    :try_start_f
    iget-object v1, v0, LX/0l3;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GvB;

    invoke-virtual {v1, p1}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 126654
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 126655
    :cond_9
    :try_start_10
    iget-object v2, v0, LX/15K;->e:LX/0Zj;

    if-nez v2, :cond_a

    .line 126656
    new-instance v2, LX/0Zj;

    const/16 v3, 0x200

    invoke-direct {v2, v3}, LX/0Zj;-><init>(I)V

    iput-object v2, v0, LX/15K;->e:LX/0Zj;

    .line 126657
    :cond_a
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 126658
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    .line 126659
    iget-object v3, v0, LX/15K;->f:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 126660
    :cond_b
    :try_start_11
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 126661
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/0oO;->a:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 126662
    iput-object v1, v0, LX/0oO;->a:Ljava/util/Locale;

    .line 126663
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_c

    .line 126664
    invoke-virtual {p1}, Landroid/app/Activity;->recreate()V

    .line 126665
    :goto_5
    goto/16 :goto_3

    .line 126666
    :cond_c
    const-string v2, "Finishing activity %s due to locale mismatch: old locale %s new locale %s. This is expected iff language was just switched."

    .line 126667
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, LX/0oO;->a:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 126668
    const-class v3, LX/0oO;

    invoke-static {v3, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 126669
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .line 126670
    :cond_d
    instance-of v1, p1, LX/15O;

    if-eqz v1, :cond_e

    move-object v1, p1

    .line 126671
    check-cast v1, LX/15O;

    .line 126672
    invoke-interface {v1}, LX/15O;->b()LX/6c5;

    move-result-object v1

    sget-object v2, LX/6c5;->SKIP:LX/6c5;

    if-eq v1, v2, :cond_3

    .line 126673
    :cond_e
    iget-object v1, v0, LX/15M;->c:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-ne v1, v2, :cond_11

    iget-object v1, v0, LX/15M;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_11

    move v2, v3

    .line 126674
    :goto_6
    instance-of v1, p1, LX/15O;

    if-eqz v1, :cond_10

    move-object v1, p1

    .line 126675
    check-cast v1, LX/15O;

    .line 126676
    invoke-interface {v1}, LX/15O;->b()LX/6c5;

    move-result-object v5

    sget-object v6, LX/6c5;->HIDE:LX/6c5;

    if-ne v5, v6, :cond_13

    .line 126677
    iget-object v1, v0, LX/15M;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_12

    :goto_7
    move v2, v3

    .line 126678
    :cond_f
    :goto_8
    if-nez v2, :cond_10

    iget-object v1, v0, LX/15M;->d:Landroid/app/Activity;

    if-eqz v1, :cond_10

    .line 126679
    invoke-static {v0}, LX/15M;->a(LX/15M;)V

    .line 126680
    :cond_10
    if-eqz v2, :cond_3

    .line 126681
    iput-object p1, v0, LX/15M;->d:Landroid/app/Activity;

    .line 126682
    iget-object v1, v0, LX/15M;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6c4;

    const-string v2, "activityResumed"

    .line 126683
    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, LX/6c4;->a(LX/6c4;ZLjava/lang/String;)V

    .line 126684
    goto/16 :goto_4

    :cond_11
    move v2, v4

    .line 126685
    goto :goto_6

    :cond_12
    move v3, v4

    .line 126686
    goto :goto_7

    .line 126687
    :cond_13
    invoke-interface {v1}, LX/15O;->b()LX/6c5;

    move-result-object v1

    sget-object v3, LX/6c5;->SHOW:LX/6c5;

    if-ne v1, v3, :cond_f

    move v2, v4

    .line 126688
    goto :goto_8
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 126525
    iget-object v0, p0, LX/0kG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13s;

    .line 126526
    iget-object v1, v0, LX/13s;->e:LX/0gh;

    invoke-virtual {v1, p1}, LX/0gh;->c(Landroid/app/Activity;)V

    .line 126527
    iget-object v0, p0, LX/0kG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10x;

    invoke-virtual {v0, p1}, LX/10x;->c(Landroid/app/Activity;)V

    .line 126528
    iget-object v0, p0, LX/0kG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    .line 126529
    const/4 v1, 0x0

    iput-object v1, v0, LX/13j;->f:Landroid/app/Activity;

    .line 126530
    iget-object v0, p0, LX/0kG;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15K;

    const/4 v2, 0x0

    .line 126531
    iget-object v1, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_7

    iget-object v1, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 126532
    :goto_0
    if-eqz v1, :cond_0

    .line 126533
    iget-object v3, v0, LX/15K;->f:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 126534
    :cond_0
    iput-object v2, v0, LX/15K;->d:Ljava/lang/ref/WeakReference;

    .line 126535
    iget-object v0, p0, LX/0kG;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15L;

    .line 126536
    iget-object v1, v0, LX/15L;->b:Ljava/util/List;

    invoke-static {v1}, LX/13m;->a(Ljava/util/Collection;)V

    .line 126537
    iget-object v1, v0, LX/15L;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 126538
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Aa;

    .line 126539
    if-eqz v1, :cond_1

    .line 126540
    invoke-virtual {v1}, LX/1Aa;->d()V

    goto :goto_1

    .line 126541
    :cond_2
    iget-object v0, p0, LX/0kG;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l7;

    const/4 v3, 0x0

    .line 126542
    iput-object v3, v0, LX/0l7;->h:Ljava/lang/ref/WeakReference;

    .line 126543
    iget-object v1, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_3

    .line 126544
    iget-object v1, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 126545
    iput-object v3, v0, LX/0l7;->g:Ljava/util/concurrent/Future;

    .line 126546
    :cond_3
    iput-object v3, v0, LX/0l7;->i:Ljava/lang/Boolean;

    .line 126547
    iget-object v0, p0, LX/0kG;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15P;

    .line 126548
    const/4 v1, 0x0

    iput-object v1, v0, LX/15P;->b:Landroid/app/Activity;

    .line 126549
    iget-object v1, v0, LX/15P;->a:LX/0c4;

    if-eqz v1, :cond_4

    .line 126550
    iget-object v1, v0, LX/15P;->a:LX/0c4;

    sget-object v2, LX/0cB;->f:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0c4;->a(Landroid/net/Uri;Ljava/lang/Object;)V

    .line 126551
    :cond_4
    iget-object v0, p0, LX/0kG;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15Q;

    .line 126552
    iget-object v1, v0, LX/15Q;->a:LX/15R;

    .line 126553
    iget-object v0, v1, LX/15R;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 126554
    iget-object v0, v1, LX/15R;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 126555
    iget-object v0, p0, LX/0kG;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15V;

    .line 126556
    iget-object v1, v0, LX/15V;->j:Landroid/app/Activity;

    if-ne p1, v1, :cond_5

    .line 126557
    const/4 v1, 0x0

    iput-object v1, v0, LX/15V;->j:Landroid/app/Activity;

    .line 126558
    :cond_5
    iget-object v0, p0, LX/0kG;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15Z;

    .line 126559
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/15Z;->a:Z

    .line 126560
    iget-object v1, v0, LX/15Z;->b:LX/0T2;

    if-eqz v1, :cond_6

    .line 126561
    iget-object v1, v0, LX/15Z;->b:LX/0T2;

    invoke-interface {v1, p1}, LX/0T2;->d(Landroid/app/Activity;)V

    .line 126562
    :cond_6
    iget-object v0, p0, LX/0kG;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15a;

    invoke-virtual {v0, p1}, LX/15a;->b(Landroid/app/Activity;)V

    .line 126563
    iget-object v0, p0, LX/0kG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    invoke-virtual {v0}, LX/13l;->a()V

    .line 126564
    iget-object v0, p0, LX/0kG;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    .line 126565
    const/4 v1, 0x0

    iput-object v1, v0, LX/13n;->a:Landroid/app/Activity;

    .line 126566
    return-void

    :cond_7
    move-object v1, v2

    .line 126567
    goto/16 :goto_0
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 126510
    iget-object v0, p0, LX/0kG;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13j;

    invoke-virtual {v0}, LX/13j;->f()V

    .line 126511
    iget-object v0, p0, LX/0kG;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15M;

    .line 126512
    iget-object v1, v0, LX/15M;->d:Landroid/app/Activity;

    if-ne v1, p1, :cond_0

    .line 126513
    invoke-static {v0}, LX/15M;->a(LX/15M;)V

    .line 126514
    :cond_0
    iget-object v0, p0, LX/0kG;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kd;

    .line 126515
    iget-object v1, v0, LX/0kd;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    .line 126516
    iput-wide v1, v0, LX/0kd;->h:J

    .line 126517
    invoke-static {v0, v1, v2}, LX/0kd;->e(LX/0kd;J)V

    .line 126518
    iget-object v0, p0, LX/0kG;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13k;

    .line 126519
    iget-object v1, v0, LX/13k;->a:Landroid/app/Activity;

    instance-of v1, v1, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    if-eqz v1, :cond_1

    .line 126520
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/13k;->b:Z

    .line 126521
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/13k;->a:Landroid/app/Activity;

    .line 126522
    iget-object v0, p0, LX/0kG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    invoke-virtual {v0}, LX/13l;->c()V

    .line 126523
    iget-object v0, p0, LX/0kG;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13n;

    invoke-virtual {v0}, LX/13n;->d()V

    .line 126524
    return-void
.end method

.method public final f(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 126490
    iget-object v0, p0, LX/0kG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l3;

    const/4 v2, 0x0

    .line 126491
    instance-of v1, p1, LX/0l6;

    if-eqz v1, :cond_4

    .line 126492
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0kG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10x;

    .line 126493
    instance-of v1, p1, LX/0fI;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 126494
    check-cast v1, LX/0fI;

    invoke-interface {v1}, LX/0fI;->v()V

    .line 126495
    :cond_1
    iget-object v1, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v1, p1}, LX/0fU;->d(Landroid/app/Activity;)V

    .line 126496
    iget-object v1, v0, LX/10x;->f:LX/0fU;

    invoke-virtual {v1}, LX/0fU;->d()V

    .line 126497
    iget-object v0, p0, LX/0kG;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15a;

    .line 126498
    iget-object v1, v0, LX/15a;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Co;

    .line 126499
    if-nez v1, :cond_2

    new-instance v1, LX/2Co;

    invoke-direct {v1, v0, p1}, LX/2Co;-><init>(LX/15a;Landroid/app/Activity;)V

    :cond_2
    move-object v1, v1

    .line 126500
    iget-object v2, v1, LX/2Co;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Jg;

    .line 126501
    iget-object p1, v1, LX/2Co;->a:LX/15a;

    iget-object p1, p1, LX/15a;->d:Ljava/util/Map;

    invoke-interface {p1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 126502
    :cond_3
    iget-object v2, v1, LX/2Co;->a:LX/15a;

    iget-object v2, v2, LX/15a;->c:Ljava/util/Map;

    iget-object v0, v1, LX/2Co;->b:Landroid/app/Activity;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126503
    return-void

    .line 126504
    :cond_4
    iget-object v1, v0, LX/0l3;->a:LX/14l;

    if-eqz v1, :cond_5

    .line 126505
    iget-object v1, v0, LX/0l3;->a:LX/14l;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 126506
    iput-object v2, v0, LX/0l3;->a:LX/14l;

    .line 126507
    :cond_5
    iget-object v1, v0, LX/0l3;->l:LX/0Yb;

    if-eqz v1, :cond_0

    .line 126508
    iget-object v1, v0, LX/0l3;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 126509
    iput-object v2, v0, LX/0l3;->l:LX/0Yb;

    goto :goto_0
.end method

.method public final g(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 126487
    iget-object v0, p0, LX/0kG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/110;

    .line 126488
    invoke-static {v0, p1}, LX/110;->c(LX/110;Landroid/app/Activity;)V

    .line 126489
    return-void
.end method

.method public final h(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 126486
    return-void
.end method

.method public final i(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 126479
    iget-object v0, p0, LX/0kG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13s;

    .line 126480
    iget-object p0, v0, LX/13s;->e:LX/0gh;

    .line 126481
    iget-object p1, p0, LX/0gh;->w:Ljava/lang/String;

    move-object p0, p1

    .line 126482
    if-nez p0, :cond_0

    .line 126483
    iget-object p0, v0, LX/13s;->e:LX/0gh;

    const-string p1, "tap_back_button"

    invoke-virtual {p0, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 126484
    :cond_0
    goto :goto_1

    .line 126485
    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0
.end method
