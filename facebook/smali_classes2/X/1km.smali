.class public final LX/1km;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kE;


# direct methods
.method public constructor <init>(LX/1kE;)V
    .locals 0

    .prologue
    .line 310192
    iput-object p1, p0, LX/1km;->a:LX/1kE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 310193
    const/4 v1, 0x0

    .line 310194
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 310195
    iget-object v0, p0, LX/1km;->a:LX/1kE;

    iget-object v3, v0, LX/1kE;->b:LX/1kF;

    iget-object v0, p0, LX/1km;->a:LX/1kE;

    iget-object v0, v0, LX/1kE;->a:LX/1kJ;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v3, v0, v1, v4}, LX/1kF;->a(LX/1lR;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLPromptType;)LX/1lR;

    move-result-object v0

    .line 310196
    invoke-virtual {v0}, LX/1lR;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310197
    iget-object v1, p0, LX/1km;->a:LX/1kE;

    iget-object v1, v1, LX/1kE;->a:LX/1kJ;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1km;->a:LX/1kE;

    iget-object v1, v1, LX/1kE;->a:LX/1kJ;

    .line 310198
    iget-object v3, v1, LX/1kJ;->a:LX/1lR;

    move-object v1, v3

    .line 310199
    invoke-virtual {v1, v0}, LX/1lR;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 310200
    :cond_0
    iget-object v1, p0, LX/1km;->a:LX/1kE;

    new-instance v3, LX/1kJ;

    invoke-direct {v3, v0}, LX/1kJ;-><init>(LX/1lR;)V

    iput-object v3, v1, LX/1kE;->a:LX/1kJ;

    .line 310201
    :cond_1
    iget-object v0, p0, LX/1km;->a:LX/1kE;

    iget-object v0, v0, LX/1kE;->a:LX/1kJ;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 310202
    :cond_2
    iget-object v0, p0, LX/1km;->a:LX/1kE;

    iget-object v0, v0, LX/1kE;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x930001

    const/16 v3, 0xd

    const-string v4, "fetcher"

    const-class v5, LX/1kE;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    .line 310203
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 310204
    :cond_3
    iget-object v0, p0, LX/1km;->a:LX/1kE;

    iget-object v0, v0, LX/1kE;->a:LX/1kJ;

    .line 310205
    iget-object v4, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, v4

    .line 310206
    goto :goto_0
.end method
