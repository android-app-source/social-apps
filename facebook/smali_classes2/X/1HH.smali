.class public LX/1HH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private final b:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 226662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226663
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1HH;->a:Z

    .line 226664
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/1HH;->c:Ljava/util/concurrent/Executor;

    .line 226665
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/1HH;->b:Ljava/util/Deque;

    .line 226666
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 226657
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1HH;->a:Z

    if-eqz v0, :cond_0

    .line 226658
    iget-object v0, p0, LX/1HH;->b:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226659
    :goto_0
    monitor-exit p0

    return-void

    .line 226660
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1HH;->c:Ljava/util/concurrent/Executor;

    const v1, -0x34f062f8    # -9411848.0f

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 226661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 226654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HH;->b:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226655
    monitor-exit p0

    return-void

    .line 226656
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
