.class public LX/19s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Or",
        "<",
        "LX/04j;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:J

.field public c:LX/04j;

.field private final d:Landroid/content/Context;

.field private final e:LX/0ad;

.field private final f:LX/0wq;

.field private g:LX/043;

.field public h:Landroid/net/Uri;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/19Q;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/19Q;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0So;

.field public final m:LX/19r;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Xl;

.field public final p:LX/0kb;

.field public final q:Z

.field public r:LX/0Yb;

.field public s:LX/19t;

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/0p3;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Z

.field private final w:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/0p3;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Z

.field private final y:LX/19k;

.field private final z:Landroid/content/ServiceConnection;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208876
    const-class v0, LX/19s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19s;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/0wq;Ljava/util/Map;LX/0So;LX/19r;LX/0Ot;LX/0Xl;LX/0kb;ZLX/0Ot;Ljava/util/Map;ZLjava/util/Map;LX/19k;Z)V
    .locals 4
    .param p5    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/0wq;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0So;",
            "LX/19r;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Xl;",
            "LX/0kb;",
            "Z",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/0p3;",
            "Ljava/lang/Integer;",
            ">;Z",
            "Ljava/util/Map",
            "<",
            "LX/0p3;",
            "Ljava/lang/Integer;",
            ">;",
            "LX/19k;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 208962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208963
    const-wide/16 v2, -0x6

    iput-wide v2, p0, LX/19s;->b:J

    .line 208964
    const/4 v2, 0x0

    iput-object v2, p0, LX/19s;->c:LX/04j;

    .line 208965
    sget-object v2, LX/19t;->UNKNOWN:LX/19t;

    iput-object v2, p0, LX/19s;->s:LX/19t;

    .line 208966
    new-instance v2, LX/19u;

    invoke-direct {v2, p0}, LX/19u;-><init>(LX/19s;)V

    iput-object v2, p0, LX/19s;->z:Landroid/content/ServiceConnection;

    .line 208967
    iput-object p5, p0, LX/19s;->l:LX/0So;

    .line 208968
    iput-object p1, p0, LX/19s;->d:Landroid/content/Context;

    .line 208969
    iput-object p4, p0, LX/19s;->i:Ljava/util/Map;

    .line 208970
    iput-object p2, p0, LX/19s;->e:LX/0ad;

    .line 208971
    iput-object p3, p0, LX/19s;->f:LX/0wq;

    .line 208972
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, LX/19s;->j:Ljava/util/Set;

    .line 208973
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, LX/19s;->k:Ljava/util/Set;

    .line 208974
    iput-object p6, p0, LX/19s;->m:LX/19r;

    .line 208975
    iput-object p7, p0, LX/19s;->n:LX/0Ot;

    .line 208976
    iput-object p8, p0, LX/19s;->o:LX/0Xl;

    .line 208977
    iput-object p9, p0, LX/19s;->p:LX/0kb;

    .line 208978
    iput-boolean p10, p0, LX/19s;->q:Z

    .line 208979
    iput-object p11, p0, LX/19s;->t:LX/0Ot;

    .line 208980
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    iput-object v2, p0, LX/19s;->u:LX/0P1;

    .line 208981
    move/from16 v0, p13

    iput-boolean v0, p0, LX/19s;->v:Z

    .line 208982
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    iput-object v2, p0, LX/19s;->w:LX/0P1;

    .line 208983
    move-object/from16 v0, p15

    iput-object v0, p0, LX/19s;->y:LX/19k;

    .line 208984
    move/from16 v0, p16

    iput-boolean v0, p0, LX/19s;->x:Z

    .line 208985
    iget-object v2, p0, LX/19s;->d:Landroid/content/Context;

    iget-object v3, p0, LX/19s;->e:LX/0ad;

    invoke-direct {p0, v2, v3}, LX/19s;->a(Landroid/content/Context;LX/0ad;)LX/043;

    move-result-object v2

    iput-object v2, p0, LX/19s;->g:LX/043;

    .line 208986
    const/4 v2, 0x0

    iput-object v2, p0, LX/19s;->h:Landroid/net/Uri;

    .line 208987
    return-void
.end method

.method private a(Landroid/content/Context;LX/0ad;)LX/043;
    .locals 11

    .prologue
    .line 208959
    new-instance v0, LX/043;

    sget-short v1, LX/0ws;->fI:S

    iget-object v2, p0, LX/19s;->f:LX/0wq;

    invoke-virtual {v2}, LX/0wq;->k()Z

    move-result v2

    invoke-interface {p2, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, LX/0ws;->fi:I

    sget v4, LX/043;->a:I

    invoke-interface {p2, v3, v4}, LX/0ad;->a(II)I

    move-result v3

    sget-short v4, LX/0ws;->fL:S

    iget-object v5, p0, LX/19s;->f:LX/0wq;

    .line 208960
    iget-object v6, v5, LX/0wq;->ae:LX/0Uh;

    const/16 v7, 0x2b4

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v5, v6

    .line 208961
    invoke-interface {p2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    sget v5, LX/0ws;->fk:I

    sget v6, LX/043;->b:I

    invoke-interface {p2, v5, v6}, LX/0ad;->a(II)I

    move-result v5

    sget v6, LX/0ws;->fj:I

    sget v7, LX/043;->c:I

    invoke-interface {p2, v6, v7}, LX/0ad;->a(II)I

    move-result v6

    sget-short v7, LX/0ws;->fB:S

    const/4 v8, 0x0

    invoke-interface {p2, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v7

    sget-short v8, LX/0ws;->fK:S

    iget-object v9, p0, LX/19s;->f:LX/0wq;

    invoke-virtual {v9}, LX/0wq;->k()Z

    move-result v9

    invoke-interface {p2, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v8

    sget-short v9, LX/0ws;->fJ:S

    iget-object v10, p0, LX/19s;->f:LX/0wq;

    invoke-virtual {v10}, LX/0wq;->k()Z

    move-result v10

    invoke-interface {p2, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v9

    invoke-direct/range {v0 .. v9}, LX/043;-><init>(ZLjava/lang/String;IZIIZZZ)V

    return-object v0
.end method

.method public static b(LX/19s;Z)V
    .locals 5

    .prologue
    .line 208941
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v2

    .line 208942
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 208943
    iget-object v1, p0, LX/19s;->j:Ljava/util/Set;

    monitor-enter v1

    .line 208944
    :try_start_0
    iget-object v0, p0, LX/19s;->j:Ljava/util/Set;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 208945
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208946
    iget-object v1, p0, LX/19s;->k:Ljava/util/Set;

    monitor-enter v1

    .line 208947
    :try_start_1
    iget-object v0, p0, LX/19s;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208948
    iget-object v0, p0, LX/19s;->k:Ljava/util/Set;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 208949
    iget-object v0, p0, LX/19s;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 208950
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 208951
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19Q;

    .line 208952
    if-eqz p1, :cond_1

    .line 208953
    invoke-interface {v0, v2}, LX/19Q;->a(LX/04j;)V

    .line 208954
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 208955
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 208956
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 208957
    :cond_1
    invoke-interface {v0}, LX/19Q;->a()V

    goto :goto_1

    .line 208958
    :cond_2
    return-void
.end method

.method private declared-synchronized b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 208916
    monitor-enter p0

    :try_start_0
    const-string v0, "VideoPlayerManager.startExoPlayerServiceIfNeeded"

    const v1, -0x1ca2edba

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208917
    :try_start_1
    iget-object v0, p0, LX/19s;->s:LX/19t;

    sget-object v1, LX/19t;->UNKNOWN:LX/19t;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/19s;->p:LX/0kb;

    if-eqz v0, :cond_0

    .line 208918
    invoke-static {p0}, LX/19s;->j(LX/19s;)LX/19t;

    move-result-object v0

    iput-object v0, p0, LX/19s;->s:LX/19t;

    .line 208919
    :cond_0
    iget-object v0, p0, LX/19s;->r:LX/0Yb;

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/19s;->q:Z

    if-nez v0, :cond_4

    .line 208920
    :cond_1
    :goto_0
    iget-object v0, p0, LX/19s;->c:LX/04j;

    if-nez v0, :cond_3

    .line 208921
    iget-object v0, p0, LX/19s;->l:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 208922
    iget-wide v2, p0, LX/19s;->b:J

    sub-long v2, v0, v2

    .line 208923
    const-wide/16 v4, 0x5

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    .line 208924
    iput-wide v0, p0, LX/19s;->b:J

    .line 208925
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/19s;->d:Landroid/content/Context;

    const-class v2, Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208926
    if-eqz p1, :cond_2

    .line 208927
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 208928
    :cond_2
    iget-object v1, p0, LX/19s;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 208929
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/19s;->a(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 208930
    :cond_3
    const v0, 0x5cfca522

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 208931
    :goto_1
    monitor-exit p0

    return-void

    .line 208932
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 208933
    :try_start_3
    iget-object v0, p0, LX/19s;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/19s;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 208934
    const v0, -0x22c31a26

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 208935
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 208936
    :catchall_1
    move-exception v0

    const v1, 0x1bbc756

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 208937
    :cond_4
    new-instance v0, LX/2ps;

    invoke-direct {v0, p0}, LX/2ps;-><init>(LX/19s;)V

    .line 208938
    new-instance v1, LX/2pt;

    invoke-direct {v1, p0}, LX/2pt;-><init>(LX/19s;)V

    .line 208939
    iget-object v2, p0, LX/19s;->o:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v2, v3, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    sget-object v2, LX/0oz;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/19s;->r:LX/0Yb;

    .line 208940
    iget-object v0, p0, LX/19s;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto :goto_0
.end method

.method public static j(LX/19s;)LX/19t;
    .locals 1

    .prologue
    .line 208913
    iget-object v0, p0, LX/19s;->p:LX/0kb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/19s;->p:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 208914
    :cond_0
    sget-object v0, LX/19t;->NONE:LX/19t;

    .line 208915
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/19s;->p:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/19t;->WIFI:LX/19t;

    goto :goto_0

    :cond_2
    sget-object v0, LX/19t;->CELL:LX/19t;

    goto :goto_0
.end method

.method public static l(LX/19s;)V
    .locals 7

    .prologue
    .line 208883
    iget-boolean v0, p0, LX/19s;->x:Z

    if-nez v0, :cond_1

    .line 208884
    :cond_0
    :goto_0
    return-void

    .line 208885
    :cond_1
    iget-object v1, p0, LX/19s;->c:LX/04j;

    .line 208886
    if-eqz v1, :cond_0

    .line 208887
    :try_start_0
    iget-object v0, p0, LX/19s;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    .line 208888
    if-eqz v0, :cond_0

    .line 208889
    iget-object v0, p0, LX/19s;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    .line 208890
    iget-object v0, p0, LX/19s;->p:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, LX/1jR;->getNetworkType(Landroid/net/NetworkInfo;)LX/1jR;

    move-result-object v3

    .line 208891
    iget-object v0, p0, LX/19s;->u:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 208892
    if-eqz v0, :cond_2

    .line 208893
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, LX/04j;->a(I)V

    .line 208894
    :cond_2
    iget-boolean v0, p0, LX/19s;->v:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/19s;->w:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 208895
    :goto_1
    if-nez v0, :cond_4

    iget-object v4, p0, LX/19s;->y:LX/19k;

    .line 208896
    iget v5, v4, LX/19k;->l:I

    .line 208897
    iget-boolean v6, v4, LX/19k;->a:Z

    if-eqz v6, :cond_8

    iget-boolean v6, v4, LX/19k;->b:Z

    if-eqz v6, :cond_8

    .line 208898
    sget-object v6, LX/0p3;->EXCELLENT:LX/0p3;

    if-eq v2, v6, :cond_3

    sget-object v6, LX/0p3;->GOOD:LX/0p3;

    if-eq v2, v6, :cond_3

    sget-object v6, LX/0p3;->MODERATE:LX/0p3;

    if-eq v2, v6, :cond_3

    sget-object v6, LX/0p3;->POOR:LX/0p3;

    if-ne v2, v6, :cond_7

    .line 208899
    :cond_3
    invoke-static {v4, v2}, LX/19k;->a(LX/19k;LX/0p3;)Z

    .line 208900
    :goto_2
    iget v6, v4, LX/19k;->l:I

    if-eq v6, v5, :cond_9

    const/4 v5, 0x1

    :goto_3
    move v2, v5

    .line 208901
    if-eqz v2, :cond_0

    .line 208902
    :cond_4
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_4
    iget-object v2, p0, LX/19s;->y:LX/19k;

    .line 208903
    iget v3, v2, LX/19k;->l:I

    move v2, v3

    .line 208904
    invoke-interface {v1, v0, v2}, LX/04j;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208905
    :catch_0
    move-exception v0

    .line 208906
    sget-object v1, LX/19s;->a:Ljava/lang/String;

    const-string v2, "Exception occcurs while notifying connectivity change"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 208907
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 208908
    :cond_6
    const/4 v0, -0x1

    goto :goto_4

    .line 208909
    :cond_7
    invoke-static {v4, v3}, LX/19k;->a(LX/19k;LX/1jR;)Z

    goto :goto_2

    .line 208910
    :cond_8
    invoke-static {v4, v2}, LX/19k;->a(LX/19k;LX/0p3;)Z

    .line 208911
    invoke-static {v4, v3}, LX/19k;->a(LX/19k;LX/1jR;)Z

    goto :goto_2

    .line 208912
    :cond_9
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public static m(LX/19s;)V
    .locals 3

    .prologue
    .line 208877
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v0

    .line 208878
    if-eqz v0, :cond_0

    .line 208879
    :try_start_0
    iget-object v1, p0, LX/19s;->h:Landroid/net/Uri;

    invoke-interface {v0, v1}, LX/04j;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208880
    :cond_0
    :goto_0
    return-void

    .line 208881
    :catch_0
    move-exception v0

    .line 208882
    sget-object v1, LX/19s;->a:Ljava/lang/String;

    const-string v2, "Exception setting video server base uri in exo service"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)J
    .locals 2

    .prologue
    .line 208866
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v0

    .line 208867
    new-instance v1, LX/37B;

    invoke-direct {v1, p0, p1}, LX/37B;-><init>(LX/19s;Lcom/facebook/exoplayer/ipc/VideoPrefetchRequest;)V

    .line 208868
    if-nez v0, :cond_0

    .line 208869
    iget-object v0, p0, LX/19s;->k:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208870
    invoke-virtual {p0}, LX/19s;->a()V

    .line 208871
    :goto_0
    const-wide/16 v0, 0x0

    return-wide v0

    .line 208872
    :cond_0
    invoke-interface {v1, v0}, LX/19Q;->a(LX/04j;)V

    goto :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 208873
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/19s;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208874
    monitor-exit p0

    return-void

    .line 208875
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0A6;)V
    .locals 1

    .prologue
    .line 208988
    iget-object v0, p0, LX/19s;->m:LX/19r;

    invoke-virtual {v0, p1}, LX/19r;->a(LX/0A6;)V

    .line 208989
    return-void
.end method

.method public final a(LX/19Q;)V
    .locals 2

    .prologue
    .line 208823
    iget-object v1, p0, LX/19s;->j:Ljava/util/Set;

    monitor-enter v1

    .line 208824
    :try_start_0
    iget-object v0, p0, LX/19s;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208825
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 208826
    :try_start_0
    iget-object v0, p0, LX/19s;->d:Landroid/content/Context;

    iget-object v1, p0, LX/19s;->e:LX/0ad;

    invoke-direct {p0, v0, v1}, LX/19s;->a(Landroid/content/Context;LX/0ad;)LX/043;

    move-result-object v0

    iput-object v0, p0, LX/19s;->g:LX/043;

    .line 208827
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/19s;->d:Landroid/content/Context;

    const-class v2, Lcom/facebook/video/vps/VideoPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208828
    const-string v1, "UseExoServiceCache"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget-boolean v2, v2, LX/043;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208829
    iget-object v1, p0, LX/19s;->g:LX/043;

    iget-boolean v1, v1, LX/043;->d:Z

    if-eqz v1, :cond_0

    .line 208830
    const-string v1, "ExoCacheRootDirectory"

    iget-object v2, p0, LX/19s;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208831
    const-string v1, "ExoCacheSize"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget v2, v2, LX/043;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 208832
    const-string v1, "ExoUseSplitCache"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget-boolean v2, v2, LX/043;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208833
    const-string v1, "ExoPrefetchCacheSize"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget v2, v2, LX/043;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 208834
    const-string v1, "ExoPlayCacheSize"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget v2, v2, LX/043;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 208835
    const-string v1, "ExoCacheSaveMetadata"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget-boolean v2, v2, LX/043;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208836
    const-string v1, "UseExoServiceCacheForProgressive"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget-boolean v2, v2, LX/043;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208837
    const-string v1, "UseExoServiceCacheForDash"

    iget-object v2, p0, LX/19s;->g:LX/043;

    iget-boolean v2, v2, LX/043;->l:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208838
    :cond_0
    if-eqz p1, :cond_1

    .line 208839
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 208840
    :cond_1
    iget-object v1, p0, LX/19s;->d:Landroid/content/Context;

    iget-object v2, p0, LX/19s;->z:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    const v4, -0x60cf292d

    invoke-static {v1, v0, v2, v3, v4}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208841
    :goto_0
    return-void

    .line 208842
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 208843
    iget-object v0, p0, LX/19s;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/19s;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 208844
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v0

    .line 208845
    if-eqz v0, :cond_1

    .line 208846
    :try_start_0
    invoke-interface {v0, p1}, LX/04j;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208847
    :cond_0
    :goto_0
    return-void

    .line 208848
    :catch_0
    move-exception v0

    .line 208849
    sget-object v1, LX/19s;->a:Ljava/lang/String;

    const-string v2, "Exception calling onPlayerActivityStateChange"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 208850
    :cond_1
    if-nez p1, :cond_0

    .line 208851
    iget-object v0, p0, LX/19s;->f:LX/0wq;

    .line 208852
    iget-object v1, v0, LX/0wq;->af:LX/0ad;

    sget-short v2, LX/0ws;->fT:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 208853
    if-eqz v0, :cond_0

    .line 208854
    invoke-virtual {p0}, LX/19s;->a()V

    goto :goto_0
.end method

.method public final declared-synchronized b()LX/04j;
    .locals 1

    .prologue
    .line 208855
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19s;->c:LX/04j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 208856
    iget-object v0, p0, LX/19s;->g:LX/043;

    iget-boolean v0, v0, LX/043;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 208857
    iget-object v0, p0, LX/19s;->g:LX/043;

    iget-boolean v0, v0, LX/043;->k:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 208858
    iget-object v0, p0, LX/19s;->g:LX/043;

    iget-boolean v0, v0, LX/043;->l:Z

    return v0
.end method

.method public final g()Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 208859
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v1

    .line 208860
    if-eqz v1, :cond_0

    .line 208861
    :try_start_0
    invoke-interface {v1}, LX/04j;->a()Lcom/facebook/exoplayer/ipc/ExoServicePerformanceMetrics;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 208862
    :cond_0
    :goto_0
    return-object v0

    .line 208863
    :catch_0
    move-exception v1

    .line 208864
    sget-object v2, LX/19s;->a:Ljava/lang/String;

    const-string v3, "Exception querying performance metrics in exo service"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 208865
    invoke-virtual {p0}, LX/19s;->b()LX/04j;

    move-result-object v0

    return-object v0
.end method
