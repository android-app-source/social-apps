.class public final LX/0Ru;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public final synthetic c:Ljava/util/Iterator;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 60513
    iput-object p1, p0, LX/0Ru;->c:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60514
    sget-object v0, LX/0RZ;->a:LX/0Rb;

    move-object v0, v0

    .line 60515
    iput-object v0, p0, LX/0Ru;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 60516
    :goto_0
    iget-object v0, p0, LX/0Ru;->a:Ljava/util/Iterator;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, LX/0Ru;->c:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60517
    iget-object v0, p0, LX/0Ru;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, LX/0Ru;->a:Ljava/util/Iterator;

    goto :goto_0

    .line 60518
    :cond_0
    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 60519
    invoke-virtual {p0}, LX/0Ru;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60520
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 60521
    :cond_0
    iget-object v0, p0, LX/0Ru;->a:Ljava/util/Iterator;

    iput-object v0, p0, LX/0Ru;->b:Ljava/util/Iterator;

    .line 60522
    iget-object v0, p0, LX/0Ru;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 60523
    iget-object v0, p0, LX/0Ru;->b:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 60524
    iget-object v0, p0, LX/0Ru;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 60525
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Ru;->b:Ljava/util/Iterator;

    .line 60526
    return-void

    .line 60527
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
