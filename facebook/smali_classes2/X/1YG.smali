.class public LX/1YG;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fs;


# instance fields
.field private final a:LX/1YI;

.field private final b:LX/1YK;

.field private final c:LX/0bH;

.field public d:LX/1Pq;


# direct methods
.method public constructor <init>(LX/0bH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273435
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 273436
    new-instance v0, LX/1YH;

    invoke-direct {v0, p0}, LX/1YH;-><init>(LX/1YG;)V

    iput-object v0, p0, LX/1YG;->a:LX/1YI;

    .line 273437
    new-instance v0, LX/1YJ;

    invoke-direct {v0, p0}, LX/1YJ;-><init>(LX/1YG;)V

    iput-object v0, p0, LX/1YG;->b:LX/1YK;

    .line 273438
    iput-object p1, p0, LX/1YG;->c:LX/0bH;

    .line 273439
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 273440
    iput-object p3, p0, LX/1YG;->d:LX/1Pq;

    .line 273441
    iget-object v0, p0, LX/1YG;->c:LX/0bH;

    iget-object v1, p0, LX/1YG;->a:LX/1YI;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 273442
    iget-object v0, p0, LX/1YG;->c:LX/0bH;

    iget-object v1, p0, LX/1YG;->b:LX/1YK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 273443
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 273444
    const/4 v0, 0x0

    iput-object v0, p0, LX/1YG;->d:LX/1Pq;

    .line 273445
    iget-object v0, p0, LX/1YG;->c:LX/0bH;

    iget-object v1, p0, LX/1YG;->b:LX/1YK;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 273446
    iget-object v0, p0, LX/1YG;->c:LX/0bH;

    iget-object v1, p0, LX/1YG;->a:LX/1YI;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 273447
    return-void
.end method
