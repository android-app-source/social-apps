.class public final LX/1ne;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1na;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/1nb;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 317166
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "text"

    aput-object v2, v0, v1

    sput-object v0, LX/1ne;->b:[Ljava/lang/String;

    .line 317167
    sput v3, LX/1ne;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 317158
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 317159
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/1ne;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1ne;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1ne;LX/1De;IILX/1nb;)V
    .locals 1

    .prologue
    .line 317168
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 317169
    iput-object p4, p0, LX/1ne;->a:LX/1nb;

    .line 317170
    iget-object v0, p0, LX/1ne;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 317171
    return-void
.end method


# virtual methods
.method public final a(LX/1nd;)LX/1ne;
    .locals 1

    .prologue
    .line 317172
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->v:LX/1nd;

    .line 317173
    return-object p0
.end method

.method public final a(Landroid/content/res/ColorStateList;)LX/1ne;
    .locals 1

    .prologue
    .line 317174
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    .line 317175
    return-object p0
.end method

.method public final a(Landroid/graphics/Typeface;)LX/1ne;
    .locals 1

    .prologue
    .line 317176
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->r:Landroid/graphics/Typeface;

    .line 317177
    return-object p0
.end method

.method public final a(Landroid/text/Layout$Alignment;)LX/1ne;
    .locals 1

    .prologue
    .line 317178
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    .line 317179
    return-object p0
.end method

.method public final a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;
    .locals 1

    .prologue
    .line 317180
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    .line 317181
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/1ne;
    .locals 2

    .prologue
    .line 317182
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-object p1, v0, LX/1nb;->a:Ljava/lang/CharSequence;

    .line 317183
    iget-object v0, p0, LX/1ne;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 317184
    return-object p0
.end method

.method public final a(Z)LX/1ne;
    .locals 1

    .prologue
    .line 317185
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-boolean p1, v0, LX/1nb;->c:Z

    .line 317186
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 317187
    invoke-super {p0}, LX/1X5;->a()V

    .line 317188
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ne;->a:LX/1nb;

    .line 317189
    sget-object v0, LX/1na;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 317190
    return-void
.end method

.method public final b(Z)LX/1ne;
    .locals 1

    .prologue
    .line 317191
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-boolean p1, v0, LX/1nb;->j:Z

    .line 317192
    return-object p0
.end method

.method public final c(F)LX/1ne;
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317193
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->f:F

    .line 317194
    return-object p0
.end method

.method public final c(Z)LX/1ne;
    .locals 1

    .prologue
    .line 317195
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-boolean p1, v0, LX/1nb;->t:Z

    .line 317196
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1na;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 317197
    iget-object v1, p0, LX/1ne;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1ne;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/1ne;->c:I

    if-ge v1, v2, :cond_2

    .line 317198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 317199
    :goto_0
    sget v2, LX/1ne;->c:I

    if-ge v0, v2, :cond_1

    .line 317200
    iget-object v2, p0, LX/1ne;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 317201
    sget-object v2, LX/1ne;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317202
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317203
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317204
    :cond_2
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    .line 317205
    invoke-virtual {p0}, LX/1ne;->a()V

    .line 317206
    return-object v0
.end method

.method public final d(F)LX/1ne;
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317207
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->g:F

    .line 317208
    return-object p0
.end method

.method public final d(Z)LX/1ne;
    .locals 1

    .prologue
    .line 317209
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput-boolean p1, v0, LX/1nb;->A:Z

    .line 317210
    return-object p0
.end method

.method public final e(F)LX/1ne;
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317162
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->h:F

    .line 317163
    return-object p0
.end method

.method public final f(II)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 317164
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1, p2}, LX/1Dp;->a(II)I

    move-result v1

    iput v1, v0, LX/1nb;->k:I

    .line 317165
    return-object p0
.end method

.method public final g(F)LX/1ne;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 317125
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->b(F)I

    move-result v1

    iput v1, v0, LX/1nb;->n:I

    .line 317126
    return-object p0
.end method

.method public final h(F)LX/1ne;
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317127
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->o:F

    .line 317128
    return-object p0
.end method

.method public final h(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 317129
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/1nb;->a:Ljava/lang/CharSequence;

    .line 317130
    iget-object v0, p0, LX/1ne;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 317131
    return-object p0
.end method

.method public final i(F)LX/1ne;
    .locals 2
    .param p1    # F
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param

    .prologue
    .line 317132
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->a(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, LX/1nb;->o:F

    .line 317133
    return-object p0
.end method

.method public final i(I)LX/1ne;
    .locals 1

    .prologue
    .line 317134
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->d:I

    .line 317135
    return-object p0
.end method

.method public final j(F)LX/1ne;
    .locals 1

    .prologue
    .line 317136
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->p:F

    .line 317137
    return-object p0
.end method

.method public final j(I)LX/1ne;
    .locals 1

    .prologue
    .line 317138
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->e:I

    .line 317139
    return-object p0
.end method

.method public final k(I)LX/1ne;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 317140
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->i:I

    .line 317141
    return-object p0
.end method

.method public final l(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 317142
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/1nb;->i:I

    .line 317143
    return-object p0
.end method

.method public final m(I)LX/1ne;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 317144
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->k:I

    .line 317145
    return-object p0
.end method

.method public final n(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 317146
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/1nb;->k:I

    .line 317147
    return-object p0
.end method

.method public final o(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param

    .prologue
    .line 317148
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, LX/1Dp;->a(II)I

    move-result v1

    iput v1, v0, LX/1nb;->k:I

    .line 317149
    return-object p0
.end method

.method public final p(I)LX/1ne;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317150
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->n:I

    .line 317151
    return-object p0
.end method

.method public final q(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 317152
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/1nb;->n:I

    .line 317153
    return-object p0
.end method

.method public final s(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 317154
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->f(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, LX/1nb;->o:F

    .line 317155
    return-object p0
.end method

.method public final t(I)LX/1ne;
    .locals 1

    .prologue
    .line 317156
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    iput p1, v0, LX/1nb;->q:I

    .line 317157
    return-object p0
.end method

.method public final v(I)LX/1ne;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 317160
    iget-object v0, p0, LX/1ne;->a:LX/1nb;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/1nb;->z:I

    .line 317161
    return-object p0
.end method
