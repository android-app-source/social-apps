.class public LX/1kE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ry;


# instance fields
.field public a:LX/1kJ;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1kF;

.field private final c:LX/0TD;

.field public final d:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(LX/1kF;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309165
    iput-object p1, p0, LX/1kE;->b:LX/1kF;

    .line 309166
    iput-object p2, p0, LX/1kE;->c:LX/0TD;

    .line 309167
    iput-object p3, p0, LX/1kE;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 309168
    return-void
.end method

.method public static b(LX/0QB;)LX/1kE;
    .locals 14

    .prologue
    .line 309169
    new-instance v3, LX/1kE;

    .line 309170
    new-instance v4, LX/1kF;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v7

    check-cast v7, LX/1kG;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-static {p0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v9

    check-cast v9, LX/1E1;

    invoke-static {p0}, LX/1kI;->b(LX/0QB;)LX/1kI;

    move-result-object v10

    check-cast v10, LX/1kI;

    invoke-static {p0}, LX/1kD;->a(LX/0QB;)LX/1kD;

    move-result-object v11

    check-cast v11, LX/1kD;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v12

    check-cast v12, LX/11R;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    invoke-direct/range {v4 .. v13}, LX/1kF;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1kG;Ljava/lang/Boolean;LX/1E1;LX/1kI;LX/1kD;LX/11R;Landroid/content/res/Resources;)V

    .line 309171
    move-object v0, v4

    .line 309172
    check-cast v0, LX/1kF;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v2

    check-cast v2, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v3, v0, v1, v2}, LX/1kE;-><init>(LX/1kF;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 309173
    return-object v3
.end method


# virtual methods
.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 309174
    iget-object v0, p0, LX/1kE;->c:LX/0TD;

    new-instance v1, LX/1km;

    invoke-direct {v1, p0}, LX/1km;-><init>(LX/1kE;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309175
    const-class v0, LX/1kJ;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309176
    const/4 v0, 0x0

    iput-object v0, p0, LX/1kE;->a:LX/1kJ;

    .line 309177
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 309178
    const-class v0, LX/1kJ;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309179
    const/4 v0, 0x0

    iput-object v0, p0, LX/1kE;->a:LX/1kJ;

    .line 309180
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 309181
    iget-object v0, p0, LX/1kE;->b:LX/1kF;

    .line 309182
    iget-boolean v1, v0, LX/1kF;->i:Z

    if-nez v1, :cond_0

    .line 309183
    goto :goto_0

    :goto_0
    const/4 v1, 0x1

    move v1, v1

    .line 309184
    if-eqz v1, :cond_0

    .line 309185
    const/4 v1, 0x1

    .line 309186
    :goto_1
    move v0, v1

    .line 309187
    return v0

    .line 309188
    :cond_0
    const-string v1, "Is in work build, V2 prompt and super sprouts"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1kF;->a(LX/1kF;Ljava/lang/String;Ljava/lang/String;)V

    .line 309189
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309190
    const-class v0, LX/1kJ;

    return-object v0
.end method
