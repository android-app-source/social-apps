.class public LX/0q8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0mM;

.field public b:LX/0mk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/2u6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0mM;)V
    .locals 0

    .prologue
    .line 147151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147152
    iput-object p1, p0, LX/0q8;->a:LX/0mM;

    .line 147153
    return-void
.end method

.method private static a(LX/0pE;LX/2u6;)V
    .locals 1
    .param p0    # LX/0pE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147147
    if-eqz p0, :cond_0

    .line 147148
    iget-object v0, p0, LX/0pE;->a:LX/0q7;

    .line 147149
    const/4 p0, 0x3

    invoke-virtual {v0, p0, p1}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0q7;->sendMessage(Landroid/os/Message;)Z

    .line 147150
    :cond_0
    return-void
.end method

.method private static declared-synchronized b(LX/0q8;LX/0mk;)V
    .locals 1

    .prologue
    .line 147138
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0q8;->e:Z

    if-nez v0, :cond_0

    .line 147139
    iput-object p1, p0, LX/0q8;->b:LX/0mk;

    .line 147140
    iget-object v0, p0, LX/0q8;->a:LX/0mM;

    invoke-virtual {v0}, LX/0mM;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0q8;->c:Ljava/lang/String;

    .line 147141
    iget-object v0, p0, LX/0q8;->a:LX/0mM;

    .line 147142
    iget-object p1, v0, LX/0mM;->a:LX/0mN;

    invoke-virtual {p1, p0}, LX/0mN;->registerObserver(Ljava/lang/Object;)V

    .line 147143
    invoke-static {p0}, LX/0q8;->f(LX/0q8;)V

    .line 147144
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0q8;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147145
    :cond_0
    monitor-exit p0

    return-void

    .line 147146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/0q8;)V
    .locals 2

    .prologue
    .line 147134
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0q8;->e:Z

    if-nez v0, :cond_0

    .line 147135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SessionDelegate should have called bootstrapIfNeeded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147137
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static f(LX/0q8;)V
    .locals 3

    .prologue
    .line 147120
    new-instance v0, LX/2u6;

    iget-object v1, p0, LX/0q8;->c:Ljava/lang/String;

    .line 147121
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 147122
    invoke-direct {v0, v1, v2}, LX/2u6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/0q8;->d:LX/2u6;

    .line 147123
    return-void
.end method

.method public static g(LX/0q8;)V
    .locals 0

    .prologue
    .line 147131
    invoke-static {p0}, LX/0q8;->f(LX/0q8;)V

    .line 147132
    invoke-direct {p0}, LX/0q8;->h()V

    .line 147133
    return-void
.end method

.method private declared-synchronized h()V
    .locals 2

    .prologue
    .line 147127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0q8;->b:LX/0mk;

    invoke-virtual {v0}, LX/0mk;->c()LX/0pE;

    move-result-object v0

    iget-object v1, p0, LX/0q8;->d:LX/2u6;

    invoke-static {v0, v1}, LX/0q8;->a(LX/0pE;LX/2u6;)V

    .line 147128
    iget-object v0, p0, LX/0q8;->b:LX/0mk;

    invoke-virtual {v0}, LX/0mk;->a()LX/0pE;

    move-result-object v0

    iget-object v1, p0, LX/0q8;->d:LX/2u6;

    invoke-static {v0, v1}, LX/0q8;->a(LX/0pE;LX/2u6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147129
    monitor-exit p0

    return-void

    .line 147130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0mk;)LX/2u6;
    .locals 1

    .prologue
    .line 147124
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/0q8;->b(LX/0q8;LX/0mk;)V

    .line 147125
    iget-object v0, p0, LX/0q8;->d:LX/2u6;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 147126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
