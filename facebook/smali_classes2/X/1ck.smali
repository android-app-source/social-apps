.class public LX/1ck;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1cm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 282518
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 282519
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 282520
    new-instance v0, LX/1vB;

    invoke-direct {v0}, LX/1vB;-><init>()V

    sput-object v0, LX/1ck;->a:LX/1cm;

    .line 282521
    :goto_0
    return-void

    .line 282522
    :cond_0
    new-instance v0, LX/1cl;

    invoke-direct {v0}, LX/1cl;-><init>()V

    sput-object v0, LX/1ck;->a:LX/1cm;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 282523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282524
    return-void
.end method

.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;)I
    .locals 1

    .prologue
    .line 282525
    sget-object v0, LX/1ck;->a:LX/1cm;

    invoke-interface {v0, p0}, LX/1cm;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .locals 1

    .prologue
    .line 282526
    sget-object v0, LX/1ck;->a:LX/1cm;

    invoke-interface {v0, p0, p1}, LX/1cm;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 282527
    return-void
.end method

.method public static b(Landroid/view/ViewGroup$MarginLayoutParams;)I
    .locals 1

    .prologue
    .line 282528
    sget-object v0, LX/1ck;->a:LX/1cm;

    invoke-interface {v0, p0}, LX/1cm;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/ViewGroup$MarginLayoutParams;I)V
    .locals 1

    .prologue
    .line 282529
    sget-object v0, LX/1ck;->a:LX/1cm;

    invoke-interface {v0, p0, p1}, LX/1cm;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 282530
    return-void
.end method
