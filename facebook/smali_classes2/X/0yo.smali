.class public final enum LX/0yo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yo;

.field public static final enum ATTACHMENT_LINK_CACHE_STATE:LX/0yo;

.field public static final enum ATTACHMENT_MEDIA_CACHE_STATE:LX/0yo;

.field public static final enum ATTACHMENT_MEDIA_EXPECTED:LX/0yo;

.field public static final enum ATTACHMENT_MEDIA_LOADED:LX/0yo;

.field public static final enum ATTACHMENT_TEXT_IS_LOADED:LX/0yo;

.field public static final enum CLIENT_HAS_SEEN:LX/0yo;

.field public static final enum CONNECTION_QUALITY:LX/0yo;

.field public static final enum CUR_CLIENT_STORY_AGE_MS:LX/0yo;

.field public static final enum HAS_ATTACHMENT_TEXT:LX/0yo;

.field public static final enum IMAGE_CACHE_STATE:LX/0yo;

.field public static final enum IS_OFFLINE:LX/0yo;

.field public static final enum LIVE_COMMENT_AGE_MS:LX/0yo;

.field public static final enum LIVE_VIDEO_ENDED:LX/0yo;

.field public static final enum NUM_IMAGES_EXPECTED:LX/0yo;

.field public static final enum NUM_IMAGES_LOADED:LX/0yo;

.field public static final enum OTHER:LX/0yo;

.field public static final enum PHOTO_TAKEN_IN_LAST_N_MINUTES:LX/0yo;

.field public static final enum REACTION_COUNT:LX/0yo;

.field public static final enum STORY_HAS_DOWNLOADED_VIDEO:LX/0yo;

.field public static final enum STORY_HAS_VIDEO:LX/0yo;

.field private static final TAG:Ljava/lang/String;

.field public static final enum VIDEO_CACHE_STATE:LX/0yo;

.field public static final enum VIDEO_PLAY_COUNT:LX/0yo;

.field public static final enum VIDEO_PLAY_SECS:LX/0yo;

.field public static final enum WAITING_FOR_NEW_STORIES:LX/0yo;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 166185
    new-instance v0, LX/0yo;

    const-string v1, "CLIENT_HAS_SEEN"

    invoke-direct {v0, v1, v3}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->CLIENT_HAS_SEEN:LX/0yo;

    .line 166186
    new-instance v0, LX/0yo;

    const-string v1, "CUR_CLIENT_STORY_AGE_MS"

    invoke-direct {v0, v1, v4}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->CUR_CLIENT_STORY_AGE_MS:LX/0yo;

    .line 166187
    new-instance v0, LX/0yo;

    const-string v1, "IMAGE_CACHE_STATE"

    invoke-direct {v0, v1, v5}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->IMAGE_CACHE_STATE:LX/0yo;

    .line 166188
    new-instance v0, LX/0yo;

    const-string v1, "LIVE_VIDEO_ENDED"

    invoke-direct {v0, v1, v6}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->LIVE_VIDEO_ENDED:LX/0yo;

    .line 166189
    new-instance v0, LX/0yo;

    const-string v1, "CONNECTION_QUALITY"

    invoke-direct {v0, v1, v7}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->CONNECTION_QUALITY:LX/0yo;

    .line 166190
    new-instance v0, LX/0yo;

    const-string v1, "IS_OFFLINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->IS_OFFLINE:LX/0yo;

    .line 166191
    new-instance v0, LX/0yo;

    const-string v1, "PHOTO_TAKEN_IN_LAST_N_MINUTES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->PHOTO_TAKEN_IN_LAST_N_MINUTES:LX/0yo;

    .line 166192
    new-instance v0, LX/0yo;

    const-string v1, "VIDEO_CACHE_STATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->VIDEO_CACHE_STATE:LX/0yo;

    .line 166193
    new-instance v0, LX/0yo;

    const-string v1, "STORY_HAS_VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->STORY_HAS_VIDEO:LX/0yo;

    .line 166194
    new-instance v0, LX/0yo;

    const-string v1, "WAITING_FOR_NEW_STORIES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->WAITING_FOR_NEW_STORIES:LX/0yo;

    .line 166195
    new-instance v0, LX/0yo;

    const-string v1, "STORY_HAS_DOWNLOADED_VIDEO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->STORY_HAS_DOWNLOADED_VIDEO:LX/0yo;

    .line 166196
    new-instance v0, LX/0yo;

    const-string v1, "NUM_IMAGES_LOADED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->NUM_IMAGES_LOADED:LX/0yo;

    .line 166197
    new-instance v0, LX/0yo;

    const-string v1, "NUM_IMAGES_EXPECTED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->NUM_IMAGES_EXPECTED:LX/0yo;

    .line 166198
    new-instance v0, LX/0yo;

    const-string v1, "REACTION_COUNT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->REACTION_COUNT:LX/0yo;

    .line 166199
    new-instance v0, LX/0yo;

    const-string v1, "HAS_ATTACHMENT_TEXT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->HAS_ATTACHMENT_TEXT:LX/0yo;

    .line 166200
    new-instance v0, LX/0yo;

    const-string v1, "ATTACHMENT_TEXT_IS_LOADED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->ATTACHMENT_TEXT_IS_LOADED:LX/0yo;

    .line 166201
    new-instance v0, LX/0yo;

    const-string v1, "ATTACHMENT_MEDIA_CACHE_STATE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->ATTACHMENT_MEDIA_CACHE_STATE:LX/0yo;

    .line 166202
    new-instance v0, LX/0yo;

    const-string v1, "ATTACHMENT_MEDIA_LOADED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->ATTACHMENT_MEDIA_LOADED:LX/0yo;

    .line 166203
    new-instance v0, LX/0yo;

    const-string v1, "ATTACHMENT_MEDIA_EXPECTED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->ATTACHMENT_MEDIA_EXPECTED:LX/0yo;

    .line 166204
    new-instance v0, LX/0yo;

    const-string v1, "ATTACHMENT_LINK_CACHE_STATE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->ATTACHMENT_LINK_CACHE_STATE:LX/0yo;

    .line 166205
    new-instance v0, LX/0yo;

    const-string v1, "VIDEO_PLAY_COUNT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->VIDEO_PLAY_COUNT:LX/0yo;

    .line 166206
    new-instance v0, LX/0yo;

    const-string v1, "VIDEO_PLAY_SECS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->VIDEO_PLAY_SECS:LX/0yo;

    .line 166207
    new-instance v0, LX/0yo;

    const-string v1, "LIVE_COMMENT_AGE_MS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->LIVE_COMMENT_AGE_MS:LX/0yo;

    .line 166208
    new-instance v0, LX/0yo;

    const-string v1, "OTHER"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/0yo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0yo;->OTHER:LX/0yo;

    .line 166209
    const/16 v0, 0x18

    new-array v0, v0, [LX/0yo;

    sget-object v1, LX/0yo;->CLIENT_HAS_SEEN:LX/0yo;

    aput-object v1, v0, v3

    sget-object v1, LX/0yo;->CUR_CLIENT_STORY_AGE_MS:LX/0yo;

    aput-object v1, v0, v4

    sget-object v1, LX/0yo;->IMAGE_CACHE_STATE:LX/0yo;

    aput-object v1, v0, v5

    sget-object v1, LX/0yo;->LIVE_VIDEO_ENDED:LX/0yo;

    aput-object v1, v0, v6

    sget-object v1, LX/0yo;->CONNECTION_QUALITY:LX/0yo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0yo;->IS_OFFLINE:LX/0yo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0yo;->PHOTO_TAKEN_IN_LAST_N_MINUTES:LX/0yo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0yo;->VIDEO_CACHE_STATE:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0yo;->STORY_HAS_VIDEO:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0yo;->WAITING_FOR_NEW_STORIES:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0yo;->STORY_HAS_DOWNLOADED_VIDEO:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0yo;->NUM_IMAGES_LOADED:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0yo;->NUM_IMAGES_EXPECTED:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0yo;->REACTION_COUNT:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0yo;->HAS_ATTACHMENT_TEXT:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0yo;->ATTACHMENT_TEXT_IS_LOADED:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0yo;->ATTACHMENT_MEDIA_CACHE_STATE:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0yo;->ATTACHMENT_MEDIA_LOADED:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0yo;->ATTACHMENT_MEDIA_EXPECTED:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0yo;->ATTACHMENT_LINK_CACHE_STATE:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/0yo;->VIDEO_PLAY_COUNT:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/0yo;->VIDEO_PLAY_SECS:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/0yo;->LIVE_COMMENT_AGE_MS:LX/0yo;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/0yo;->OTHER:LX/0yo;

    aput-object v2, v0, v1

    sput-object v0, LX/0yo;->$VALUES:[LX/0yo;

    .line 166210
    const-class v0, LX/0yo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0yo;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 166211
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getEnum(Ljava/lang/String;)LX/0yo;
    .locals 1

    .prologue
    .line 166212
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0yo;->valueOf(Ljava/lang/String;)LX/0yo;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 166213
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/0yo;->OTHER:LX/0yo;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yo;
    .locals 1

    .prologue
    .line 166214
    const-class v0, LX/0yo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yo;

    return-object v0
.end method

.method public static values()[LX/0yo;
    .locals 1

    .prologue
    .line 166215
    sget-object v0, LX/0yo;->$VALUES:[LX/0yo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yo;

    return-object v0
.end method
