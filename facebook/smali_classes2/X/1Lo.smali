.class public LX/1Lo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;
.implements LX/1Ln;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Key:",
        "LX/2WG;",
        ">",
        "Ljava/lang/Object;",
        "LX/0po;",
        "LX/1Ln",
        "<TKey;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/4nN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final c:LX/1Lq;

.field public volatile d:LX/1AA;

.field private e:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234287
    const-class v0, LX/1Lo;

    sput-object v0, LX/1Lo;->a:Ljava/lang/Class;

    .line 234288
    new-instance v0, LX/1Lp;

    invoke-direct {v0}, LX/1Lp;-><init>()V

    sput-object v0, LX/1Lo;->f:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/1Ln;LX/1Lq;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ui/media/cache/SizePolicy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234283
    iput-object p1, p0, LX/1Lo;->b:LX/1Ln;

    .line 234284
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1Lo;->e:J

    .line 234285
    iput-object p2, p0, LX/1Lo;->c:LX/1Lq;

    .line 234286
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/1Lo;J)V
    .locals 12

    .prologue
    .line 234271
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Lo;->e:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/1Lo;->e:J

    .line 234272
    iget-object v0, p0, LX/1Lo;->c:LX/1Lq;

    iget-wide v2, p0, LX/1Lo;->e:J

    const-wide/16 v9, 0x2

    .line 234273
    iget-object v5, v0, LX/1Lq;->a:LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v5, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v5

    .line 234274
    add-long/2addr v5, v2

    .line 234275
    iget-wide v7, v0, LX/1Lq;->b:J

    div-long/2addr v5, v9

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    .line 234276
    div-long v5, v7, v9

    .line 234277
    cmp-long v9, v2, v7

    if-lez v9, :cond_1

    :goto_0
    move-wide v0, v5

    .line 234278
    iget-wide v2, p0, LX/1Lo;->e:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 234279
    invoke-direct {p0, v0, v1}, LX/1Lo;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234280
    :cond_0
    monitor-exit p0

    return-void

    .line 234281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-wide v5, v7

    goto :goto_0
.end method

.method private declared-synchronized b(LX/2WG;LX/3Dd;)LX/3Da;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/3Dd;",
            ")",
            "LX/3Da;"
        }
    .end annotation

    .prologue
    .line 234266
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1Lo;->c(LX/1Lo;)V

    .line 234267
    iget-object v0, p0, LX/1Lo;->b:LX/1Ln;

    invoke-static {p1}, LX/1Lo;->c(LX/2WG;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v0

    .line 234268
    invoke-interface {v0}, LX/3Da;->c()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/1Lo;->a$redex0(LX/1Lo;J)V

    .line 234269
    new-instance v1, LX/3De;

    invoke-direct {v1, p0, p1, v0}, LX/3De;-><init>(LX/1Lo;LX/2WG;LX/3Da;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 234270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(J)V
    .locals 7

    .prologue
    .line 234257
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1Lo;->c(LX/1Lo;)V

    .line 234258
    invoke-direct {p0}, LX/1Lo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nN;

    .line 234259
    iget-wide v2, p0, LX/1Lo;->e:J

    cmp-long v2, v2, p1

    if-lez v2, :cond_0

    .line 234260
    iget-object v2, p0, LX/1Lo;->b:LX/1Ln;

    iget-object v3, v0, LX/4nN;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, LX/1Ln;->a(Ljava/lang/Object;)V

    .line 234261
    iget-wide v2, p0, LX/1Lo;->e:J

    iget-wide v4, v0, LX/4nN;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/1Lo;->e:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234262
    :catch_0
    move-exception v0

    .line 234263
    :try_start_1
    sget-object v1, LX/1Lo;->a:Ljava/lang/Class;

    const-string v2, "Error on trimming"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234264
    :cond_0
    monitor-exit p0

    return-void

    .line 234265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c(LX/2WG;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 234255
    :try_start_0
    invoke-virtual {p0}, LX/2WG;->a()LX/1bh;

    move-result-object v0

    invoke-interface {v0}, LX/1bh;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, LX/03l;->a([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 234256
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 not available, world as we know it is not more"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized c(LX/1Lo;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 234289
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Lo;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 234290
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Lo;->e:J

    .line 234291
    iget-object v0, p0, LX/1Lo;->b:LX/1Ln;

    invoke-interface {v0}, LX/1Ln;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234292
    iget-object v2, p0, LX/1Lo;->b:LX/1Ln;

    invoke-interface {v2, v0}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v0

    .line 234293
    if-eqz v0, :cond_0

    .line 234294
    iget-wide v2, p0, LX/1Lo;->e:J

    invoke-interface {v0}, LX/3Da;->c()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/1Lo;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234296
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/4nN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234247
    iget-object v0, p0, LX/1Lo;->b:LX/1Ln;

    invoke-interface {v0}, LX/1Ln;->a()Ljava/util/List;

    move-result-object v0

    .line 234248
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 234249
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234250
    iget-object v3, p0, LX/1Lo;->b:LX/1Ln;

    invoke-interface {v3, v0}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v0

    .line 234251
    if-eqz v0, :cond_0

    .line 234252
    new-instance v3, LX/4nN;

    invoke-direct {v3, v0}, LX/4nN;-><init>(LX/3Da;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234253
    :cond_1
    sget-object v0, LX/1Lo;->f:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 234254
    return-object v1
.end method


# virtual methods
.method public final declared-synchronized U_()V
    .locals 6

    .prologue
    .line 234235
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1Lo;->d()Ljava/util/List;

    move-result-object v0

    .line 234236
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 234237
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 234238
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 234239
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nN;

    .line 234240
    iget-object v2, p0, LX/1Lo;->b:LX/1Ln;

    iget-object v3, v0, LX/4nN;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, LX/1Ln;->a(Ljava/lang/Object;)V

    .line 234241
    iget-wide v2, p0, LX/1Lo;->e:J

    iget-wide v4, v0, LX/4nN;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/1Lo;->e:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 234242
    :catch_0
    move-exception v0

    .line 234243
    :try_start_2
    sget-object v1, LX/1Lo;->a:Ljava/lang/Class;

    const-string v2, "Error on trimming"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 234244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234245
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/1Lo;->d:LX/1AA;

    if-eqz v0, :cond_0

    .line 234246
    iget-object v0, p0, LX/1Lo;->d:LX/1AA;

    iget-object v1, p0, LX/1Lo;->b:LX/1Ln;

    invoke-interface {v1}, LX/1Ln;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1AA;->a(Ljava/util/List;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/3Dd;)LX/3Da;
    .locals 1

    .prologue
    .line 234233
    check-cast p1, LX/2WG;

    .line 234234
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, LX/1Lo;->b(LX/2WG;LX/3Dd;)LX/3Da;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TKey;>;"
        }
    .end annotation

    .prologue
    .line 234232
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot rehydrate VideoCacheKey\'s"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 234228
    check-cast p1, LX/2WG;

    .line 234229
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lo;->b:LX/1Ln;

    invoke-static {p1}, LX/1Lo;->c(LX/2WG;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ln;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234230
    monitor-exit p0

    return-void

    .line 234231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)LX/3Da;
    .locals 2

    .prologue
    .line 234224
    check-cast p1, LX/2WG;

    .line 234225
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lo;->b:LX/1Ln;

    invoke-static {p1}, LX/1Lo;->c(LX/2WG;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 234226
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, LX/3De;

    invoke-direct {v0, p0, p1, v1}, LX/3De;-><init>(LX/1Lo;LX/2WG;LX/3Da;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 234217
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, LX/1Lo;->b(J)V

    .line 234218
    iget-object v0, p0, LX/1Lo;->d:LX/1AA;

    if-eqz v0, :cond_0

    .line 234219
    iget-object v0, p0, LX/1Lo;->d:LX/1AA;

    .line 234220
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 234221
    invoke-virtual {v0, v1}, LX/1AA;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234222
    :cond_0
    monitor-exit p0

    return-void

    .line 234223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
