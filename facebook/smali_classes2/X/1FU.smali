.class public LX/1FU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/Queue;

.field public d:I


# direct methods
.method public constructor <init>(III)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 222543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222544
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222545
    if-ltz p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222546
    if-ltz p3, :cond_2

    :goto_2
    invoke-static {v1}, LX/03g;->b(Z)V

    .line 222547
    iput p1, p0, LX/1FU;->a:I

    .line 222548
    iput p2, p0, LX/1FU;->b:I

    .line 222549
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/1FU;->c:Ljava/util/Queue;

    .line 222550
    iput p3, p0, LX/1FU;->d:I

    .line 222551
    return-void

    :cond_0
    move v0, v2

    .line 222552
    goto :goto_0

    :cond_1
    move v0, v2

    .line 222553
    goto :goto_1

    :cond_2
    move v1, v2

    .line 222554
    goto :goto_2
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 222555
    iget-object v0, p0, LX/1FU;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 222556
    iget-object v0, p0, LX/1FU;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 222557
    return-void
.end method

.method public d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 222558
    iget-object v0, p0, LX/1FU;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 222559
    iget v0, p0, LX/1FU;->d:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 222560
    iget v0, p0, LX/1FU;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1FU;->d:I

    .line 222561
    return-void

    .line 222562
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
