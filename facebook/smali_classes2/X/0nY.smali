.class public final LX/0nY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:[LX/0nZ;

.field public static final b:[LX/0na;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _additionalKeySerializers:[LX/0nZ;

.field public final _additionalSerializers:[LX/0nZ;

.field public final _modifiers:[LX/0na;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136374
    new-array v0, v1, [LX/0nZ;

    sput-object v0, LX/0nY;->a:[LX/0nZ;

    .line 136375
    new-array v0, v1, [LX/0na;

    sput-object v0, LX/0nY;->b:[LX/0na;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 136356
    invoke-direct {p0, v0, v0, v0}, LX/0nY;-><init>([LX/0nZ;[LX/0nZ;[LX/0na;)V

    .line 136357
    return-void
.end method

.method private constructor <init>([LX/0nZ;[LX/0nZ;[LX/0na;)V
    .locals 0

    .prologue
    .line 136369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136370
    if-nez p1, :cond_0

    sget-object p1, LX/0nY;->a:[LX/0nZ;

    :cond_0
    iput-object p1, p0, LX/0nY;->_additionalSerializers:[LX/0nZ;

    .line 136371
    if-nez p2, :cond_1

    sget-object p2, LX/0nY;->a:[LX/0nZ;

    :cond_1
    iput-object p2, p0, LX/0nY;->_additionalKeySerializers:[LX/0nZ;

    .line 136372
    if-nez p3, :cond_2

    sget-object p3, LX/0nY;->b:[LX/0na;

    :cond_2
    iput-object p3, p0, LX/0nY;->_modifiers:[LX/0na;

    .line 136373
    return-void
.end method


# virtual methods
.method public final a(LX/0nZ;)LX/0nY;
    .locals 4

    .prologue
    .line 136365
    if-nez p1, :cond_0

    .line 136366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can not pass null Serializers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136367
    :cond_0
    iget-object v0, p0, LX/0nY;->_additionalSerializers:[LX/0nZ;

    invoke-static {v0, p1}, LX/0nj;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0nZ;

    .line 136368
    new-instance v1, LX/0nY;

    iget-object v2, p0, LX/0nY;->_additionalKeySerializers:[LX/0nZ;

    iget-object v3, p0, LX/0nY;->_modifiers:[LX/0na;

    invoke-direct {v1, v0, v2, v3}, LX/0nY;-><init>([LX/0nZ;[LX/0nZ;[LX/0na;)V

    return-object v1
.end method

.method public final a(LX/0na;)LX/0nY;
    .locals 4

    .prologue
    .line 136361
    if-nez p1, :cond_0

    .line 136362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can not pass null modifier"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136363
    :cond_0
    iget-object v0, p0, LX/0nY;->_modifiers:[LX/0na;

    invoke-static {v0, p1}, LX/0nj;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0na;

    .line 136364
    new-instance v1, LX/0nY;

    iget-object v2, p0, LX/0nY;->_additionalSerializers:[LX/0nZ;

    iget-object v3, p0, LX/0nY;->_additionalKeySerializers:[LX/0nZ;

    invoke-direct {v1, v2, v3, v0}, LX/0nY;-><init>([LX/0nZ;[LX/0nZ;[LX/0na;)V

    return-object v1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 136376
    iget-object v0, p0, LX/0nY;->_additionalKeySerializers:[LX/0nZ;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 136360
    iget-object v0, p0, LX/0nY;->_modifiers:[LX/0na;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136359
    iget-object v0, p0, LX/0nY;->_additionalSerializers:[LX/0nZ;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136358
    iget-object v0, p0, LX/0nY;->_additionalKeySerializers:[LX/0nZ;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0na;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136355
    iget-object v0, p0, LX/0nY;->_modifiers:[LX/0na;

    invoke-static {v0}, LX/0nj;->b([Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
