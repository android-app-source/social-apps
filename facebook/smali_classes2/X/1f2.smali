.class public LX/1f2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289562
    iput-object p1, p0, LX/1f2;->a:LX/0Zb;

    .line 289563
    return-void
.end method

.method private static a(LX/1f2;Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;LX/1Ad;LX/33B;)LX/1bf;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 289513
    new-instance v6, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 289514
    new-instance v7, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 289515
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eq p2, v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 289516
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289517
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289518
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289519
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289520
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eq p2, v0, :cond_2

    .line 289521
    invoke-static {v6, p2, p5}, LX/1f2;->b(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)V

    .line 289522
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v3, v4

    move-object v1, v5

    move-object v2, v5

    :goto_0
    if-ge v3, v8, :cond_6

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 289523
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    .line 289524
    invoke-static {v0, p5}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;

    move-result-object v2

    .line 289525
    :cond_3
    invoke-static {v0, p2}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 289526
    invoke-static {v6, v0, p5}, LX/1f2;->a(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;

    move-result-object v0

    .line 289527
    if-nez v0, :cond_5

    .line 289528
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to build request for desired image"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289529
    :cond_4
    invoke-static {v6, v0, p5}, LX/1f2;->b(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)V

    move-object v0, v1

    .line 289530
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 289531
    :cond_6
    if-nez v1, :cond_9

    .line 289532
    if-nez p2, :cond_c

    .line 289533
    iget-object v0, p0, LX/1f2;->a:LX/0Zb;

    const-string v1, "android_missing_image_in_medi"

    invoke-interface {v0, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 289534
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 289535
    const-string v1, "error"

    const-string v3, "desired image was null"

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289536
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 289537
    :cond_7
    invoke-static {p1}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    .line 289538
    :cond_8
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 289539
    invoke-static {v6, p2, p5}, LX/1f2;->a(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;

    .line 289540
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    move-object v1, v0

    .line 289541
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [LX/1bf;

    .line 289542
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 289543
    if-eqz p4, :cond_a

    .line 289544
    invoke-virtual {p4, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v3, v4}, LX/1Ae;->a([Ljava/lang/Object;Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    .line 289545
    :cond_a
    if-eqz p3, :cond_b

    .line 289546
    iput-object v1, p3, LX/1f5;->g:LX/1bf;

    .line 289547
    move-object v0, p3

    .line 289548
    iput-object v3, v0, LX/1f5;->f:[LX/1bf;

    .line 289549
    move-object v0, v0

    .line 289550
    iput-object v2, v0, LX/1f5;->h:LX/1bf;

    .line 289551
    :cond_b
    return-object v1

    .line 289552
    :cond_c
    iget-object v0, p0, LX/1f2;->a:LX/0Zb;

    const-string v1, "android_missing_image_in_medi"

    invoke-interface {v0, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 289553
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 289554
    const-string v1, "error"

    const-string v3, "desired image was not found in graphql media. "

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289555
    const-string v1, "desiredImage"

    invoke-static {p2}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289556
    const-string v1, "getImage"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289557
    const-string v1, "getImageHigh"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289558
    const-string v1, "getImageMedium"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289559
    const-string v1, "getImageLow"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 289560
    invoke-virtual {v0}, LX/0oG;->d()V

    goto/16 :goto_1
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 289503
    if-nez p0, :cond_1

    .line 289504
    :cond_0
    :goto_0
    return-object v0

    .line 289505
    :cond_1
    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 289506
    if-eqz v1, :cond_0

    .line 289507
    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 289508
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 289509
    move-object v0, v0

    .line 289510
    iput-object p1, v0, LX/1bX;->j:LX/33B;

    .line 289511
    move-object v0, v0

    .line 289512
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/1bf;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            "LX/33B;",
            ")",
            "LX/1bf;"
        }
    .end annotation

    .prologue
    .line 289465
    const/4 v0, 0x0

    .line 289466
    if-nez p1, :cond_2

    .line 289467
    :cond_0
    :goto_0
    move-object v0, v0

    .line 289468
    if-nez v0, :cond_1

    .line 289469
    const/4 v0, 0x0

    .line 289470
    :goto_1
    return-object v0

    .line 289471
    :cond_1
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 289472
    :cond_2
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 289473
    if-eqz v1, :cond_0

    .line 289474
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->FULL_FETCH:LX/1bY;

    .line 289475
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 289476
    move-object v0, v0

    .line 289477
    iput-object p2, v0, LX/1bX;->j:LX/33B;

    .line 289478
    move-object v0, v0

    .line 289479
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 289492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 289494
    :goto_0
    return-object v0

    .line 289495
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 289496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 289497
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 289498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 289499
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 289500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 289501
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "could not find any non-null images in media object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLImage;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 289489
    if-nez p0, :cond_0

    .line 289490
    const-string v0, "null, "

    .line 289491
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Z
    .locals 2

    .prologue
    .line 289502
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1f2;
    .locals 2

    .prologue
    .line 289487
    new-instance v1, LX/1f2;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/1f2;-><init>(LX/0Zb;)V

    .line 289488
    return-object v1
.end method

.method private static b(Ljava/util/ArrayList;Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/1bf;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            "LX/33B;",
            ")V"
        }
    .end annotation

    .prologue
    .line 289483
    invoke-static {p1, p2}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/33B;)LX/1bf;

    move-result-object v0

    .line 289484
    if-nez v0, :cond_0

    .line 289485
    :goto_0
    return-void

    .line 289486
    :cond_0
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;)LX/1bf;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 289482
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, LX/1f2;->a(LX/1f2;Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;LX/1Ad;LX/33B;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;LX/33B;)LX/1bf;
    .locals 6

    .prologue
    .line 289481
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/1f2;->a(LX/1f2;Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;LX/1Ad;LX/33B;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;)LX/1bf;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 289480
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/1f2;->a(LX/1f2;Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1f5;LX/1Ad;LX/33B;)LX/1bf;

    move-result-object v0

    return-object v0
.end method
