.class public LX/11p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/sequencelogger/SequenceDefinition;",
        ">",
        "Ljava/lang/Object;",
        "LX/11o",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field public final c:LX/0Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:LX/0So;

.field public final f:LX/3nV;

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 173265
    sput-boolean v0, LX/11p;->a:Z

    .line 173266
    sput-boolean v0, LX/11p;->b:Z

    return-void
.end method

.method public constructor <init>(LX/0Pq;Ljava/lang/String;LX/0So;LX/0SG;JLX/0P1;Ljava/lang/Boolean;)V
    .locals 15
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            "LX/0So;",
            "LX/0SG;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173226
    const/4 v4, 0x0

    iput-boolean v4, p0, LX/11p;->j:Z

    .line 173227
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Pq;

    iput-object v4, p0, LX/11p;->c:LX/0Pq;

    .line 173228
    move-object/from16 v0, p2

    iput-object v0, p0, LX/11p;->d:Ljava/lang/String;

    .line 173229
    move-object/from16 v0, p3

    iput-object v0, p0, LX/11p;->e:LX/0So;

    .line 173230
    new-instance v4, LX/3nV;

    invoke-virtual/range {p1 .. p1}, LX/0Pq;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v6

    move-wide/from16 v0, p5

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, LX/11p;->a(JLX/0SG;LX/0So;)J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, LX/0Pq;->a()Z

    move-result v12

    move-object/from16 v7, p2

    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    invoke-direct/range {v4 .. v14}, LX/3nV;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLX/0P1;Ljava/lang/Boolean;)V

    iput-object v4, p0, LX/11p;->f:LX/3nV;

    .line 173231
    const/4 v4, 0x0

    iput v4, p0, LX/11p;->g:I

    .line 173232
    return-void
.end method

.method private static a(JLX/0SG;LX/0So;)J
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 173233
    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v0

    invoke-interface {p3}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 173234
    add-long/2addr v0, p0

    return-wide v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173235
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173236
    iget-object v1, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    move-wide v4, p4

    move-object v6, p3

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, LX/3nV;->a(Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;)V

    .line 173237
    sget-boolean v0, LX/11p;->a:Z

    if-eqz v0, :cond_0

    .line 173238
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    .line 173239
    iget-object v1, v0, LX/0Pq;->a:Ljava/lang/String;

    move-object v0, v1

    .line 173240
    const-string v1, "Started Marker %s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173241
    :cond_0
    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/Boolean;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 173242
    iget-object v1, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    move-wide v4, p4

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-virtual/range {v1 .. v8}, LX/3nV;->a(Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;Z)J

    move-result-wide v6

    .line 173243
    sget-boolean v0, LX/11p;->b:Z

    if-eqz v0, :cond_1

    .line 173244
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    invoke-virtual {v0}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    invoke-static/range {v1 .. v7}, LX/2BZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 173245
    :cond_0
    :goto_0
    return-object p0

    .line 173246
    :cond_1
    sget-boolean v0, LX/11p;->a:Z

    if-eqz v0, :cond_0

    .line 173247
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    invoke-virtual {v0}, LX/0Pq;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Stopped Marker %s (%s); Monotonic Timestamp (ms): %d; Elapsed: %d ms"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v8

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final varargs a(JLX/0P1;Ljava/lang/Boolean;[Ljava/util/List;)J
    .locals 7
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 173248
    iget-object v1, p0, LX/11p;->f:LX/3nV;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, LX/3nV;->a(JLX/0P1;Ljava/lang/Boolean;[Ljava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 173249
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/11p;->a(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 5
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173250
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4, p2}, LX/3nV;->a(Ljava/lang/String;JLX/0P1;)V

    .line 173251
    sget-boolean v0, LX/11p;->a:Z

    if-eqz v0, :cond_0

    .line 173252
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    .line 173253
    iget-object v1, v0, LX/0Pq;->a:Ljava/lang/String;

    move-object v0, v1

    .line 173254
    const-string v1, "Marked Event %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173255
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/11o;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173273
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/11p;->a(Ljava/lang/String;Ljava/lang/String;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)LX/11o;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173256
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/3nV;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 173257
    sget-boolean v0, LX/11p;->a:Z

    if-eqz v0, :cond_0

    .line 173258
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    .line 173259
    iget-object v1, v0, LX/0Pq;->a:Ljava/lang/String;

    move-object v0, v1

    .line 173260
    const-string v1, "Cancelled Marker %s (%s) Was Bg: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {v4}, LX/3nV;->c()Ljava/lang/Boolean;

    move-result-object v4

    .line 173261
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v4, p1

    .line 173262
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173263
    :cond_0
    return-object p0

    :cond_1
    const-string p1, "Unknown"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173264
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/11p;->a(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173267
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, LX/11p;->a(Ljava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 173268
    iget-object v0, p0, LX/11p;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 173269
    iput p1, p0, LX/11p;->g:I

    .line 173270
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 173271
    iput-boolean p1, p0, LX/11p;->h:Z

    .line 173272
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 173221
    iget-object v0, p0, LX/11p;->c:LX/0Pq;

    .line 173222
    iget p0, v0, LX/0Pq;->e:I

    move v0, p0

    .line 173223
    return v0
.end method

.method public final b(Ljava/lang/String;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 173224
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/11p;->b(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/11o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173194
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173195
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173196
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {v0, p1, p2}, LX/3nV;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173197
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173198
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/11p;->b(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173199
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, LX/11p;->b(Ljava/lang/String;Ljava/lang/String;LX/0P1;JLjava/lang/Boolean;)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 173200
    iput-boolean p1, p0, LX/11p;->i:Z

    .line 173201
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 173202
    iget v0, p0, LX/11p;->g:I

    return v0
.end method

.method public final c(Ljava/lang/String;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 173220
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, LX/11p;->c(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;LX/0P1;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173203
    iget-object v0, p0, LX/11p;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/11p;->c(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;LX/0P1;J)LX/11o;
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 173204
    iget-object v1, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x0

    move-object v3, p2

    move-wide v4, p4

    move-object v6, p3

    invoke-virtual/range {v1 .. v8}, LX/3nV;->a(Ljava/lang/String;Ljava/lang/String;JLX/0P1;Ljava/lang/Boolean;Z)J

    move-result-wide v0

    .line 173205
    sget-boolean v2, LX/11p;->a:Z

    if-eqz v2, :cond_0

    .line 173206
    iget-object v2, p0, LX/11p;->c:LX/0Pq;

    .line 173207
    iget-object v3, v2, LX/0Pq;->a:Ljava/lang/String;

    move-object v2, v3

    .line 173208
    const-string v3, "Failed Marker %s (%s); Elapsed: %d ms"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    aput-object p2, v4, v8

    const/4 v5, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173209
    :cond_0
    return-object p0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173210
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {v0, p1, p2}, LX/3nV;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)LX/11o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173211
    const/4 v0, 0x0

    iget-object v1, p0, LX/11p;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/11p;->a(Ljava/lang/String;Ljava/lang/String;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 173212
    iget-boolean v0, p0, LX/11p;->h:Z

    return v0
.end method

.method public final e(Ljava/lang/String;)LX/11o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173213
    const/4 v0, 0x0

    iget-object v1, p0, LX/11p;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/11p;->a(Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 173214
    iget-boolean v0, p0, LX/11p;->i:Z

    return v0
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 173215
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/11p;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 173216
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    iget-object v1, p0, LX/11p;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, LX/3nV;->a(JLjava/lang/String;)V

    .line 173217
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 173218
    iget-object v0, p0, LX/11p;->f:LX/3nV;

    invoke-virtual {v0}, LX/3nV;->a()V

    .line 173219
    return-void
.end method
