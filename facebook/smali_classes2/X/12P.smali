.class public LX/12P;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/12Q;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 175389
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 175390
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 175391
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/12P;->setVisibility(I)V

    .line 175392
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 175393
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/12P;->setVisibility(I)V

    .line 175394
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 175395
    invoke-virtual {p0}, LX/12P;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIndicatorData(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V
    .locals 1

    .prologue
    .line 175396
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/12P;->setText(Ljava/lang/CharSequence;)V

    .line 175397
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/12P;->a:Ljava/lang/String;

    .line 175398
    return-void
.end method

.method public setListener(LX/11x;)V
    .locals 0

    .prologue
    .line 175399
    return-void
.end method
