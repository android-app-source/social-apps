.class public LX/1SO;
.super LX/1SG;
.source ""

# interfaces
.implements LX/1SP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "LX/1SP;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1SQ;LX/1SR;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1SQ;",
            "LX/1SR;",
            "LX/0Ot",
            "<",
            "LX/Jwc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2d8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247868
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 247869
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, LX/1SQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1SQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1SR;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 247870
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/1SK;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1SO;->a:LX/0Ot;

    .line 247871
    :goto_0
    return-void

    .line 247872
    :cond_1
    invoke-static {}, LX/1SK;->a()LX/4fd;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4fd;->a(LX/0Ot;)LX/4fd;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/4fd;->a(LX/0Ot;)LX/4fd;

    move-result-object v0

    new-instance v1, LX/JwU;

    invoke-direct {v1, p0}, LX/JwU;-><init>(LX/1SO;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    .line 247873
    invoke-static {v0}, LX/4fd;->a(LX/4fd;)LX/0Ot;

    move-result-object p1

    invoke-static {p1, v1, v2}, LX/1SK;->a(LX/0Ot;LX/0QK;Ljava/util/concurrent/Executor;)LX/0Ot;

    move-result-object p1

    move-object v0, p1

    .line 247874
    iput-object v0, p0, LX/1SO;->a:LX/0Ot;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 247875
    invoke-virtual {p0}, LX/1SO;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247876
    iget-object v0, p0, LX/1SO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247877
    invoke-virtual {p0}, LX/1SO;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
