.class public abstract LX/0nx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Landroid/support/v4/app/Fragment;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/navigation/HasScrollAwayNavigation",
        "<TF;>;"
    }
.end annotation


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:Landroid/view/View;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8Cx;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0gp;

.field public final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 136736
    const-class v0, LX/0nx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0nx;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 136696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136697
    const/4 v0, 0x0

    iput-object v0, p0, LX/0nx;->a:Landroid/view/View;

    .line 136698
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0nx;->b:Ljava/util/ArrayList;

    .line 136699
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0nx;->e:Ljava/util/ArrayList;

    .line 136700
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0nx;->f:Ljava/util/ArrayList;

    .line 136701
    return-void
.end method

.method private static d(Landroid/support/v4/app/Fragment;I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 136732
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 136733
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    .prologue
    .line 136731
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)V"
        }
    .end annotation

    .prologue
    .line 136734
    iget-object v0, p0, LX/0nx;->b:Ljava/util/ArrayList;

    new-instance v1, LX/8Cx;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, LX/8Cx;-><init>(Landroid/support/v4/app/Fragment;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136735
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/0gp;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;",
            "LX/0gp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136720
    invoke-virtual {p2}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136721
    :cond_0
    :goto_0
    return-void

    .line 136722
    :cond_1
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 136723
    iput-object p2, p0, LX/0nx;->c:LX/0gp;

    .line 136724
    :try_start_0
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 136725
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 136726
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136727
    new-instance v1, LX/8Cw;

    invoke-direct {v1, p0, p1}, LX/8Cw;-><init>(LX/0nx;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 136728
    :catch_0
    sget-object v0, LX/0nx;->d:Ljava/lang/String;

    const-string v1, "Failed to get fragment tree observer"

    .line 136729
    iget-object p0, p2, LX/0gp;->l:LX/03V;

    invoke-virtual {p0, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136730
    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 136710
    iget-object v0, p0, LX/0nx;->c:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136711
    :cond_0
    :goto_0
    return-void

    .line 136712
    :cond_1
    invoke-static {p1, p2}, LX/0nx;->d(Landroid/support/v4/app/Fragment;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/0nx;->a:Landroid/view/View;

    .line 136713
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 136714
    iget-object v0, p0, LX/0nx;->c:LX/0gp;

    .line 136715
    iget-object v1, v0, LX/0gp;->d:LX/0iW;

    move-object v0, v1

    .line 136716
    iget v1, v0, LX/0iW;->g:I

    move v1, v1

    .line 136717
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 136718
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 136719
    iget-object v0, p0, LX/0nx;->a:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1OR;->h(I)V

    goto :goto_0
.end method

.method public final c(Landroid/support/v4/app/Fragment;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 136702
    iget-object v0, p0, LX/0nx;->c:LX/0gp;

    invoke-virtual {v0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136703
    :cond_0
    :goto_0
    return-void

    .line 136704
    :cond_1
    invoke-static {p1, p2}, LX/0nx;->d(Landroid/support/v4/app/Fragment;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 136705
    if-eqz v0, :cond_0

    .line 136706
    iget-object v1, p0, LX/0nx;->c:LX/0gp;

    .line 136707
    iget-object p0, v1, LX/0gp;->d:LX/0iW;

    move-object v1, p0

    .line 136708
    iget p0, v1, LX/0iW;->g:I

    move v1, p0

    .line 136709
    invoke-virtual {v0, v2, v2, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(ZII)V

    goto :goto_0
.end method
