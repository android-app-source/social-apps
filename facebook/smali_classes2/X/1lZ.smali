.class public LX/1lZ;
.super Ljava/io/InputStream;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/1FK;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1FK;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 312053
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 312054
    invoke-virtual {p1}, LX/1FK;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 312055
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    iput-object v0, p0, LX/1lZ;->a:LX/1FK;

    .line 312056
    iput v1, p0, LX/1lZ;->b:I

    .line 312057
    iput v1, p0, LX/1lZ;->c:I

    .line 312058
    return-void

    :cond_0
    move v0, v1

    .line 312059
    goto :goto_0
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 312052
    iget-object v0, p0, LX/1lZ;->a:LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    iget v1, p0, LX/1lZ;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 312050
    iget v0, p0, LX/1lZ;->b:I

    iput v0, p0, LX/1lZ;->c:I

    .line 312051
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 312049
    const/4 v0, 0x1

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 312060
    invoke-virtual {p0}, LX/1lZ;->available()I

    move-result v0

    if-gtz v0, :cond_0

    .line 312061
    const/4 v0, -0x1

    .line 312062
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1lZ;->a:LX/1FK;

    iget v1, p0, LX/1lZ;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1lZ;->b:I

    invoke-virtual {v0, v1}, LX/1FK;->a(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 312048
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/1lZ;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 3

    .prologue
    .line 312037
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 312038
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312039
    :cond_1
    invoke-virtual {p0}, LX/1lZ;->available()I

    move-result v0

    .line 312040
    if-gtz v0, :cond_2

    .line 312041
    const/4 v0, -0x1

    .line 312042
    :goto_0
    return v0

    .line 312043
    :cond_2
    if-gtz p3, :cond_3

    .line 312044
    const/4 v0, 0x0

    goto :goto_0

    .line 312045
    :cond_3
    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 312046
    iget-object v1, p0, LX/1lZ;->a:LX/1FK;

    iget v2, p0, LX/1lZ;->b:I

    invoke-virtual {v1, v2, p1, p2, v0}, LX/1FK;->a(I[BII)V

    .line 312047
    iget v1, p0, LX/1lZ;->b:I

    add-int/2addr v1, v0

    iput v1, p0, LX/1lZ;->b:I

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 312035
    iget v0, p0, LX/1lZ;->c:I

    iput v0, p0, LX/1lZ;->b:I

    .line 312036
    return-void
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 312030
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 312031
    long-to-int v0, p1

    invoke-virtual {p0}, LX/1lZ;->available()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 312032
    iget v1, p0, LX/1lZ;->b:I

    add-int/2addr v1, v0

    iput v1, p0, LX/1lZ;->b:I

    .line 312033
    int-to-long v0, v0

    return-wide v0

    .line 312034
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
