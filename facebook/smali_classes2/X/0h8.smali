.class public final LX/0h8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0ff;

.field public b:Ljava/io/File;

.field public c:J

.field public final synthetic d:LX/0em;


# direct methods
.method public constructor <init>(LX/0em;Landroid/content/Context;LX/0ff;)V
    .locals 2

    .prologue
    .line 115157
    iput-object p1, p0, LX/0h8;->d:LX/0em;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115158
    iput-object p3, p0, LX/0h8;->a:LX/0ff;

    .line 115159
    invoke-virtual {p1, p2, p3}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0h8;->b:Ljava/io/File;

    .line 115160
    iget-object v0, p0, LX/0h8;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, LX/0h8;->c:J

    .line 115161
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 115149
    if-ne p0, p1, :cond_0

    .line 115150
    const/4 v0, 0x1

    .line 115151
    :goto_0
    return v0

    .line 115152
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 115153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 115154
    :cond_2
    check-cast p1, LX/0h8;

    .line 115155
    iget-object v0, p0, LX/0h8;->a:LX/0ff;

    iget-object v1, p1, LX/0h8;->a:LX/0ff;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 115156
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/0h8;->a:LX/0ff;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
