.class public LX/0k4;
.super LX/0k5;
.source ""


# static fields
.field public static a:Z


# instance fields
.field public final b:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/0k7;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/0k7;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public e:LX/0k3;

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126191
    const/4 v0, 0x0

    sput-boolean v0, LX/0k4;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0k3;Z)V
    .locals 1

    .prologue
    .line 126184
    invoke-direct {p0}, LX/0k5;-><init>()V

    .line 126185
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/0k4;->b:LX/0YU;

    .line 126186
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/0k4;->c:LX/0YU;

    .line 126187
    iput-object p1, p0, LX/0k4;->d:Ljava/lang/String;

    .line 126188
    iput-object p2, p0, LX/0k4;->e:LX/0k3;

    .line 126189
    iput-boolean p3, p0, LX/0k4;->f:Z

    .line 126190
    return-void
.end method

.method private c(ILandroid/os/Bundle;LX/1jv;)LX/0k7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "LX/1jv",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/0k7;"
        }
    .end annotation

    .prologue
    .line 126180
    new-instance v0, LX/0k7;

    invoke-direct {v0, p0, p1, p2, p3}, LX/0k7;-><init>(LX/0k4;ILandroid/os/Bundle;LX/1jv;)V

    .line 126181
    invoke-interface {p3, p1}, LX/1jv;->a(I)LX/0k9;

    move-result-object v1

    .line 126182
    iput-object v1, v0, LX/0k7;->d:LX/0k9;

    .line 126183
    return-object v0
.end method

.method private d(ILandroid/os/Bundle;LX/1jv;)LX/0k7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "LX/1jv",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/0k7;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 126176
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0k4;->h:Z

    .line 126177
    invoke-direct {p0, p1, p2, p3}, LX/0k4;->c(ILandroid/os/Bundle;LX/1jv;)LX/0k7;

    move-result-object v0

    .line 126178
    invoke-virtual {p0, v0}, LX/0k4;->a(LX/0k7;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126179
    iput-boolean v1, p0, LX/0k4;->h:Z

    return-object v0

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, LX/0k4;->h:Z

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "LX/1jv",
            "<TD;>;)",
            "LX/0k9",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 126164
    iget-boolean v0, p0, LX/0k4;->h:Z

    if-eqz v0, :cond_0

    .line 126165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126166
    :cond_0
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126167
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initLoader in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": args="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126168
    :cond_1
    if-nez v0, :cond_4

    .line 126169
    invoke-direct {p0, p1, p2, p3}, LX/0k4;->d(ILandroid/os/Bundle;LX/1jv;)LX/0k7;

    move-result-object v0

    .line 126170
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Created new loader "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126171
    :cond_2
    :goto_0
    iget-boolean v1, v0, LX/0k7;->e:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/0k4;->f:Z

    if-eqz v1, :cond_3

    .line 126172
    iget-object v1, v0, LX/0k7;->d:LX/0k9;

    iget-object v2, v0, LX/0k7;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/0k7;->b(LX/0k9;Ljava/lang/Object;)V

    .line 126173
    :cond_3
    iget-object v0, v0, LX/0k7;->d:LX/0k9;

    return-object v0

    .line 126174
    :cond_4
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Re-using existing loader "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126175
    :cond_5
    iput-object p3, v0, LX/0k7;->c:LX/1jv;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 126073
    iget-boolean v0, p0, LX/0k4;->h:Z

    if-eqz v0, :cond_0

    .line 126074
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126075
    :cond_0
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "destroyLoader in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 126076
    :cond_1
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->g(I)I

    move-result v1

    .line 126077
    if-ltz v1, :cond_2

    .line 126078
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126079
    iget-object v2, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v2, v1}, LX/0YU;->d(I)V

    .line 126080
    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126081
    :cond_2
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->g(I)I

    move-result v1

    .line 126082
    if-ltz v1, :cond_3

    .line 126083
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126084
    iget-object v2, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v2, v1}, LX/0YU;->d(I)V

    .line 126085
    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126086
    :cond_3
    iget-object v0, p0, LX/0k4;->e:LX/0k3;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, LX/0k4;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 126087
    iget-object v0, p0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    invoke-virtual {v0}, LX/0jz;->h()V

    .line 126088
    :cond_4
    return-void
.end method

.method public final a(LX/0k7;)V
    .locals 2

    .prologue
    .line 126160
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    iget v1, p1, LX/0k7;->a:I

    invoke-virtual {v0, v1, p1}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 126161
    iget-boolean v0, p0, LX/0k4;->f:Z

    if-eqz v0, :cond_0

    .line 126162
    invoke-virtual {p1}, LX/0k7;->a()V

    .line 126163
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 126141
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 126142
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 126144
    :goto_0
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 126145
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126146
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v4, v1}, LX/0YU;->e(I)I

    move-result v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(I)V

    .line 126147
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0k7;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126148
    invoke-virtual {v0, v3, p2, p3, p4}, LX/0k7;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126149
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 126150
    :cond_0
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 126151
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Inactive Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 126153
    :goto_1
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 126154
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126155
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v3, v2}, LX/0YU;->e(I)I

    move-result v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    .line 126156
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0k7;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126157
    invoke-virtual {v0, v1, p2, p3, p4}, LX/0k7;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126158
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 126159
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 126134
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v4

    move v2, v1

    move v3, v1

    .line 126135
    :goto_0
    if-ge v2, v4, :cond_1

    .line 126136
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126137
    iget-boolean v5, v0, LX/0k7;->h:Z

    if-eqz v5, :cond_0

    iget-boolean v0, v0, LX/0k7;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    or-int/2addr v3, v0

    .line 126138
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 126139
    goto :goto_1

    .line 126140
    :cond_1
    return v3
.end method

.method public final b(I)LX/0k9;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I)",
            "LX/0k9",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 126125
    iget-boolean v0, p0, LX/0k4;->h:Z

    if-eqz v0, :cond_0

    .line 126126
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126127
    :cond_0
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126128
    if-eqz v0, :cond_2

    .line 126129
    iget-object v1, v0, LX/0k7;->n:LX/0k7;

    if-eqz v1, :cond_1

    .line 126130
    iget-object v0, v0, LX/0k7;->n:LX/0k7;

    iget-object v0, v0, LX/0k7;->d:LX/0k9;

    .line 126131
    :goto_0
    return-object v0

    .line 126132
    :cond_1
    iget-object v0, v0, LX/0k7;->d:LX/0k9;

    goto :goto_0

    .line 126133
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/os/Bundle;LX/1jv;)LX/0k9;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            ">(I",
            "Landroid/os/Bundle;",
            "LX/1jv",
            "<TD;>;)",
            "LX/0k9",
            "<TD;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 126099
    iget-boolean v0, p0, LX/0k4;->h:Z

    if-eqz v0, :cond_0

    .line 126100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126101
    :cond_0
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126102
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "restartLoader in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": args="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126103
    :cond_1
    if-eqz v0, :cond_4

    .line 126104
    iget-object v1, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v1, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0k7;

    .line 126105
    if-eqz v1, :cond_9

    .line 126106
    iget-boolean v2, v0, LX/0k7;->e:Z

    if-eqz v2, :cond_5

    .line 126107
    sget-boolean v2, LX/0k4;->a:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Removing last inactive loader: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126108
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/0k7;->f:Z

    .line 126109
    invoke-virtual {v1}, LX/0k7;->f()V

    .line 126110
    :cond_3
    :goto_0
    iget-object v1, v0, LX/0k7;->d:LX/0k9;

    .line 126111
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/0k9;->q:Z

    .line 126112
    iget-object v1, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v1, p1, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 126113
    :cond_4
    :goto_1
    invoke-direct {p0, p1, p2, p3}, LX/0k4;->d(ILandroid/os/Bundle;LX/1jv;)LX/0k7;

    move-result-object v0

    .line 126114
    iget-object v0, v0, LX/0k7;->d:LX/0k9;

    :goto_2
    return-object v0

    .line 126115
    :cond_5
    iget-boolean v1, v0, LX/0k7;->h:Z

    if-nez v1, :cond_6

    .line 126116
    iget-object v1, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v1, p1, v3}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 126117
    invoke-virtual {v0}, LX/0k7;->f()V

    goto :goto_1

    .line 126118
    :cond_6
    iget-object v1, v0, LX/0k7;->n:LX/0k7;

    if-eqz v1, :cond_8

    .line 126119
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Removing pending loader: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, LX/0k7;->n:LX/0k7;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126120
    :cond_7
    iget-object v1, v0, LX/0k7;->n:LX/0k7;

    invoke-virtual {v1}, LX/0k7;->f()V

    .line 126121
    iput-object v3, v0, LX/0k7;->n:LX/0k7;

    .line 126122
    :cond_8
    invoke-direct {p0, p1, p2, p3}, LX/0k4;->c(ILandroid/os/Bundle;LX/1jv;)LX/0k7;

    move-result-object v1

    iput-object v1, v0, LX/0k7;->n:LX/0k7;

    .line 126123
    iget-object v0, v0, LX/0k7;->n:LX/0k7;

    iget-object v0, v0, LX/0k7;->d:LX/0k9;

    goto :goto_2

    .line 126124
    :cond_9
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Making last loader inactive: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 126089
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126090
    :cond_0
    iget-boolean v0, p0, LX/0k4;->f:Z

    if-eqz v0, :cond_2

    .line 126091
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 126092
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 126093
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doStart when already started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126094
    :cond_1
    return-void

    .line 126095
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k4;->f:Z

    .line 126096
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 126097
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    invoke-virtual {v0}, LX/0k7;->a()V

    .line 126098
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 126063
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Stopping in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126064
    :cond_0
    iget-boolean v0, p0, LX/0k4;->f:Z

    if-nez v0, :cond_1

    .line 126065
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 126066
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 126067
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doStop when not started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126068
    :goto_0
    return-void

    .line 126069
    :cond_1
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 126070
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    invoke-virtual {v0}, LX/0k7;->e()V

    .line 126071
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 126072
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0k4;->f:Z

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 126047
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Retaining in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126048
    :cond_0
    iget-boolean v0, p0, LX/0k4;->f:Z

    if-nez v0, :cond_2

    .line 126049
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 126050
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 126051
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doRetain when not started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126052
    :cond_1
    return-void

    .line 126053
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k4;->g:Z

    .line 126054
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0k4;->f:Z

    .line 126055
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 126056
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126057
    sget-boolean v2, LX/0k4;->a:Z

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Retaining: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126058
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/0k7;->i:Z

    .line 126059
    iget-boolean v2, v0, LX/0k7;->h:Z

    iput-boolean v2, v0, LX/0k7;->j:Z

    .line 126060
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0k7;->h:Z

    .line 126061
    const/4 v2, 0x0

    iput-object v2, v0, LX/0k7;->c:LX/1jv;

    .line 126062
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 126038
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 126039
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126040
    iget-boolean v2, v0, LX/0k7;->h:Z

    if-eqz v2, :cond_0

    .line 126041
    iget-boolean v2, v0, LX/0k7;->k:Z

    if-eqz v2, :cond_0

    .line 126042
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/0k7;->k:Z

    .line 126043
    iget-boolean v2, v0, LX/0k7;->e:Z

    if-eqz v2, :cond_0

    .line 126044
    iget-object v2, v0, LX/0k7;->d:LX/0k9;

    iget-object v3, v0, LX/0k7;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, LX/0k7;->b(LX/0k9;Ljava/lang/Object;)V

    .line 126045
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 126046
    :cond_1
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 126026
    iget-boolean v0, p0, LX/0k4;->g:Z

    if-nez v0, :cond_2

    .line 126027
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Destroying Active in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126028
    :cond_0
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 126029
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126030
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 126031
    :cond_1
    iget-object v0, p0, LX/0k4;->b:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 126032
    :cond_2
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Destroying Inactive in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126033
    :cond_3
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_4

    .line 126034
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126035
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 126036
    :cond_4
    iget-object v0, p0, LX/0k4;->c:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 126037
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126019
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 126020
    const-string v1, "LoaderManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126021
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126022
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126023
    iget-object v1, p0, LX/0k4;->e:LX/0k3;

    invoke-static {v1, v0}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 126024
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126025
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
