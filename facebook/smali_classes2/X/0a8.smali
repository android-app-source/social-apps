.class public LX/0a8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0a9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0a9",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0aB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 84346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84347
    new-instance v0, LX/0a9;

    invoke-direct {v0}, LX/0a9;-><init>()V

    iput-object v0, p0, LX/0a8;->b:LX/0a9;

    .line 84348
    iput-object p1, p0, LX/0a8;->a:Ljava/util/concurrent/Executor;

    .line 84349
    return-void
.end method


# virtual methods
.method public final a(LX/0aB;I)V
    .locals 2

    .prologue
    .line 84350
    iget-object v0, p0, LX/0a8;->b:LX/0a9;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 84351
    return-void
.end method

.method public final varargs a(LX/0aB;[I)V
    .locals 4

    .prologue
    .line 84352
    iget-object v0, p0, LX/0a8;->b:LX/0a9;

    .line 84353
    new-instance v2, Ljava/util/HashSet;

    array-length v1, p2

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 84354
    const/4 v1, 0x0

    array-length v3, p2

    :goto_0
    if-ge v1, v3, :cond_0

    .line 84355
    aget p0, p2, v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84357
    :cond_0
    move-object v1, v2

    .line 84358
    invoke-virtual {v0, v1, p1}, LX/0UI;->a(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 84359
    return-void
.end method
