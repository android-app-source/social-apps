.class public final LX/1Ub;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:LX/1Uc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256299
    sget-object v0, LX/1Uc;->a:LX/1Uc;

    invoke-direct {p0, v1, v1, v1, v0}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    .line 256300
    return-void
.end method

.method public constructor <init>(FFFLX/1Uc;)V
    .locals 0

    .prologue
    .line 256301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256302
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256303
    iput p1, p0, LX/1Ub;->a:F

    .line 256304
    iput p2, p0, LX/1Ub;->b:F

    .line 256305
    iput p3, p0, LX/1Ub;->c:F

    .line 256306
    iput-object p4, p0, LX/1Ub;->d:LX/1Uc;

    .line 256307
    return-void
.end method


# virtual methods
.method public final a(LX/1Ub;)LX/1Ub;
    .locals 7

    .prologue
    .line 256308
    new-instance v0, LX/1Ub;

    iget v1, p0, LX/1Ub;->a:F

    .line 256309
    iget v2, p1, LX/1Ub;->a:F

    move v2, v2

    .line 256310
    add-float/2addr v1, v2

    iget v2, p0, LX/1Ub;->b:F

    .line 256311
    iget v3, p1, LX/1Ub;->b:F

    move v3, v3

    .line 256312
    add-float/2addr v2, v3

    iget v3, p0, LX/1Ub;->c:F

    .line 256313
    iget v4, p1, LX/1Ub;->c:F

    move v4, v4

    .line 256314
    add-float/2addr v3, v4

    new-instance v4, LX/1dm;

    iget-object v5, p0, LX/1Ub;->d:LX/1Uc;

    .line 256315
    iget-object v6, p1, LX/1Ub;->d:LX/1Uc;

    move-object v6, v6

    .line 256316
    invoke-direct {v4, v5, v6}, LX/1dm;-><init>(LX/1Uc;LX/1Uc;)V

    invoke-direct {v0, v1, v2, v3, v4}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 256317
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Top:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/1Ub;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Bottom:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1Ub;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Horizontal:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Ub;->d:LX/1Uc;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Uc;->a(I)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " right:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1Ub;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
