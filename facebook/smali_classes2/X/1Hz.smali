.class public LX/1Hz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/crypto/keychain/KeyChain;

.field private final b:LX/1Hq;

.field private final c:LX/1I1;


# direct methods
.method public constructor <init>(Lcom/facebook/crypto/keychain/KeyChain;LX/1Hq;LX/1Hy;)V
    .locals 3

    .prologue
    .line 228049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228050
    new-instance v0, LX/1I0;

    invoke-direct {v0, p1, p3}, LX/1I0;-><init>(Lcom/facebook/crypto/keychain/KeyChain;LX/1Hy;)V

    iput-object v0, p0, LX/1Hz;->a:Lcom/facebook/crypto/keychain/KeyChain;

    .line 228051
    iput-object p2, p0, LX/1Hz;->b:LX/1Hq;

    .line 228052
    new-instance v0, LX/1I1;

    iget-object v1, p0, LX/1Hz;->b:LX/1Hq;

    iget-object v2, p0, LX/1Hz;->a:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-direct {v0, v1, v2, p3}, LX/1I1;-><init>(LX/1Hq;Lcom/facebook/crypto/keychain/KeyChain;LX/1Hy;)V

    iput-object v0, p0, LX/1Hz;->c:LX/1I1;

    .line 228053
    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 228046
    iget-object v0, p0, LX/1Hz;->c:LX/1I1;

    .line 228047
    iget-object v1, v0, LX/1I1;->c:LX/1Hy;

    iget v1, v1, LX/1Hy;->ivLength:I

    add-int/lit8 v1, v1, 0x2

    iget-object p0, v0, LX/1I1;->c:LX/1Hy;

    iget p0, p0, LX/1Hy;->tagLength:I

    add-int/2addr v1, p0

    move v0, v1

    .line 228048
    return v0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;
    .locals 7

    .prologue
    .line 228031
    iget-object v0, p0, LX/1Hz;->c:LX/1I1;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 228032
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v4, v1

    .line 228033
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    int-to-byte v5, v1

    .line 228034
    if-ne v4, v2, :cond_0

    move v1, v2

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "Unexpected crypto version "

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, LX/2X3;->b(ZLjava/lang/String;)V

    .line 228035
    iget-object v1, v0, LX/1I1;->c:LX/1Hy;

    iget-byte v1, v1, LX/1Hy;->cipherId:B

    if-ne v5, v1, :cond_1

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected cipher ID "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/2X3;->b(ZLjava/lang/String;)V

    .line 228036
    iget-object v1, v0, LX/1I1;->c:LX/1Hy;

    iget v1, v1, LX/1Hy;->ivLength:I

    new-array v1, v1, [B

    .line 228037
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->readFully([B)V

    .line 228038
    new-instance v2, Lcom/facebook/crypto/cipher/NativeGCMCipher;

    iget-object v3, v0, LX/1I1;->a:LX/1Hq;

    invoke-direct {v2, v3}, Lcom/facebook/crypto/cipher/NativeGCMCipher;-><init>(LX/1Hq;)V

    .line 228039
    iget-object v3, v0, LX/1I1;->b:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-interface {v3}, Lcom/facebook/crypto/keychain/KeyChain;->a()[B

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->b([B[B)V

    .line 228040
    iget-object v1, p2, LX/1Hb;->c:[B

    move-object v1, v1

    .line 228041
    invoke-static {v2, v4, v5, v1}, LX/1I1;->a(Lcom/facebook/crypto/cipher/NativeGCMCipher;BB[B)V

    .line 228042
    new-instance v1, LX/48l;

    iget-object v3, v0, LX/1I1;->c:LX/1Hy;

    iget v3, v3, LX/1Hy;->tagLength:I

    invoke-direct {v1, p1, v2, v3}, LX/48l;-><init>(Ljava/io/InputStream;Lcom/facebook/crypto/cipher/NativeGCMCipher;I)V

    move-object v0, v1

    .line 228043
    return-object v0

    :cond_0
    move v1, v3

    .line 228044
    goto :goto_0

    :cond_1
    move v2, v3

    .line 228045
    goto :goto_1
.end method

.method public final a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 228054
    iget-object v0, p0, LX/1Hz;->c:LX/1I1;

    const/4 p0, 0x1

    .line 228055
    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write(I)V

    .line 228056
    iget-object v1, v0, LX/1I1;->c:LX/1Hy;

    iget-byte v1, v1, LX/1Hy;->cipherId:B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 228057
    iget-object v1, v0, LX/1I1;->b:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-interface {v1}, Lcom/facebook/crypto/keychain/KeyChain;->b()[B

    move-result-object v1

    .line 228058
    new-instance v2, Lcom/facebook/crypto/cipher/NativeGCMCipher;

    iget-object v3, v0, LX/1I1;->a:LX/1Hq;

    invoke-direct {v2, v3}, Lcom/facebook/crypto/cipher/NativeGCMCipher;-><init>(LX/1Hq;)V

    .line 228059
    iget-object v3, v0, LX/1I1;->b:Lcom/facebook/crypto/keychain/KeyChain;

    invoke-interface {v3}, Lcom/facebook/crypto/keychain/KeyChain;->a()[B

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a([B[B)V

    .line 228060
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 228061
    iget-object v1, p2, LX/1Hb;->c:[B

    move-object v1, v1

    .line 228062
    iget-object v3, v0, LX/1I1;->c:LX/1Hy;

    iget-byte v3, v3, LX/1Hy;->cipherId:B

    invoke-static {v2, p0, v3, v1}, LX/1I1;->a(Lcom/facebook/crypto/cipher/NativeGCMCipher;BB[B)V

    .line 228063
    new-instance v1, LX/2X4;

    iget-object v3, v0, LX/1I1;->c:LX/1Hy;

    iget v3, v3, LX/1Hy;->tagLength:I

    invoke-direct {v1, p1, v2, p3, v3}, LX/2X4;-><init>(Ljava/io/OutputStream;Lcom/facebook/crypto/cipher/NativeGCMCipher;[BI)V

    move-object v0, v1

    .line 228064
    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 228028
    :try_start_0
    iget-object v0, p0, LX/1Hz;->b:LX/1Hq;

    invoke-virtual {v0}, LX/1Hq;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 228029
    const/4 v0, 0x1

    .line 228030
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([BLX/1Hb;)[B
    .locals 2

    .prologue
    .line 228021
    array-length v0, p1

    invoke-direct {p0}, LX/1Hz;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 228022
    new-instance v1, LX/2X1;

    invoke-direct {v1, v0}, LX/2X1;-><init>(I)V

    .line 228023
    const/4 v0, 0x0

    invoke-virtual {p0, v1, p2, v0}, LX/1Hz;->a(Ljava/io/OutputStream;LX/1Hb;[B)Ljava/io/OutputStream;

    move-result-object v0

    .line 228024
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228025
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 228026
    invoke-virtual {v1}, LX/2X1;->a()[B

    move-result-object v0

    return-object v0

    .line 228027
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    throw v1
.end method

.method public final b([BLX/1Hb;)[B
    .locals 5

    .prologue
    .line 228009
    array-length v0, p1

    .line 228010
    invoke-direct {p0}, LX/1Hz;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 228011
    new-instance v1, LX/2X1;

    invoke-direct {v1, v0}, LX/2X1;-><init>(I)V

    .line 228012
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v0, p2}, LX/1Hz;->a(Ljava/io/InputStream;LX/1Hb;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 228013
    const/16 v0, 0x400

    :try_start_1
    new-array v0, v0, [B

    .line 228014
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 228015
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v3}, LX/2X1;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 228016
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 228017
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, LX/2X1;->close()V

    throw v0

    .line 228018
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 228019
    invoke-virtual {v1}, LX/2X1;->close()V

    .line 228020
    invoke-virtual {v1}, LX/2X1;->a()[B

    move-result-object v0

    return-object v0
.end method
