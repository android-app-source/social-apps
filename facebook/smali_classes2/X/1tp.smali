.class public final enum LX/1tp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1tp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1tp;

.field public static final enum BACKGROUND_LOCATION:LX/1tp;

.field public static final enum MQTT_AGGRESSIVELY_NOTIFY:LX/1tp;

.field public static final enum ONE_ON_ONE_OVER_MULTIWAY:LX/1tp;

.field public static final enum SHARED_SECRET:LX/1tp;

.field public static final enum UNUSED:LX/1tp;

.field public static final enum USER_AND_DEVICE_AUTH:LX/1tp;

.field public static final enum VIDEO:LX/1tp;

.field public static final enum VOIP:LX/1tp;

.field public static final enum VOIP_WEB:LX/1tp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 336836
    new-instance v0, LX/1tp;

    const-string v1, "UNUSED"

    invoke-direct {v0, v1, v3}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->UNUSED:LX/1tp;

    .line 336837
    new-instance v0, LX/1tp;

    const-string v1, "VOIP"

    invoke-direct {v0, v1, v4}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->VOIP:LX/1tp;

    .line 336838
    new-instance v0, LX/1tp;

    const-string v1, "BACKGROUND_LOCATION"

    invoke-direct {v0, v1, v5}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->BACKGROUND_LOCATION:LX/1tp;

    .line 336839
    new-instance v0, LX/1tp;

    const-string v1, "VOIP_WEB"

    invoke-direct {v0, v1, v6}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->VOIP_WEB:LX/1tp;

    .line 336840
    new-instance v0, LX/1tp;

    const-string v1, "MQTT_AGGRESSIVELY_NOTIFY"

    invoke-direct {v0, v1, v7}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->MQTT_AGGRESSIVELY_NOTIFY:LX/1tp;

    .line 336841
    new-instance v0, LX/1tp;

    const-string v1, "VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->VIDEO:LX/1tp;

    .line 336842
    new-instance v0, LX/1tp;

    const-string v1, "ONE_ON_ONE_OVER_MULTIWAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->ONE_ON_ONE_OVER_MULTIWAY:LX/1tp;

    .line 336843
    new-instance v0, LX/1tp;

    const-string v1, "SHARED_SECRET"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->SHARED_SECRET:LX/1tp;

    .line 336844
    new-instance v0, LX/1tp;

    const-string v1, "USER_AND_DEVICE_AUTH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1tp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tp;->USER_AND_DEVICE_AUTH:LX/1tp;

    .line 336845
    const/16 v0, 0x9

    new-array v0, v0, [LX/1tp;

    sget-object v1, LX/1tp;->UNUSED:LX/1tp;

    aput-object v1, v0, v3

    sget-object v1, LX/1tp;->VOIP:LX/1tp;

    aput-object v1, v0, v4

    sget-object v1, LX/1tp;->BACKGROUND_LOCATION:LX/1tp;

    aput-object v1, v0, v5

    sget-object v1, LX/1tp;->VOIP_WEB:LX/1tp;

    aput-object v1, v0, v6

    sget-object v1, LX/1tp;->MQTT_AGGRESSIVELY_NOTIFY:LX/1tp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1tp;->VIDEO:LX/1tp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1tp;->ONE_ON_ONE_OVER_MULTIWAY:LX/1tp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1tp;->SHARED_SECRET:LX/1tp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1tp;->USER_AND_DEVICE_AUTH:LX/1tp;

    aput-object v2, v0, v1

    sput-object v0, LX/1tp;->$VALUES:[LX/1tp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 336846
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1tp;
    .locals 1

    .prologue
    .line 336847
    const-class v0, LX/1tp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1tp;

    return-object v0
.end method

.method public static values()[LX/1tp;
    .locals 1

    .prologue
    .line 336848
    sget-object v0, LX/1tp;->$VALUES:[LX/1tp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1tp;

    return-object v0
.end method
