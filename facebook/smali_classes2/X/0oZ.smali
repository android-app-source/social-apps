.class public LX/0oZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jp;
.implements LX/0oY;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 141647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141648
    iput-object p1, p0, LX/0oZ;->a:LX/0Ot;

    .line 141649
    return-void
.end method

.method public static b(LX/0QB;)LX/0oZ;
    .locals 2

    .prologue
    .line 141678
    new-instance v0, LX/0oZ;

    invoke-static {p0}, LX/0oa;->a(LX/0QB;)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0oZ;-><init>(LX/0Ot;)V

    .line 141679
    return-object v0
.end method

.method private c(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 4

    .prologue
    .line 141666
    const-string v0, "feed_type_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141667
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 141668
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 141669
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v0, v1

    .line 141670
    move-object v1, v0

    .line 141671
    :goto_0
    iget-object v0, p0, LX/0oZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 141672
    iget-object v3, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v3, v3

    .line 141673
    iget-object p0, v3, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v3, p0

    .line 141674
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141675
    invoke-virtual {v0, p1}, LX/0rB;->a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    .line 141676
    :goto_1
    return-object v0

    :cond_1
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 141677
    invoke-virtual {p0, p1}, LX/0oZ;->b(Landroid/content/Intent;)Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->d()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141664
    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oh;

    const-class v1, Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0, v1}, LX/0oh;->a(Ljava/lang/Class;)LX/1Av;

    .line 141665
    return-void
.end method

.method public final b(Landroid/content/Intent;)Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;
    .locals 6

    .prologue
    .line 141650
    invoke-direct {p0, p1}, LX/0oZ;->c(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    .line 141651
    const-string v0, "should_update_title_bar"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 141652
    new-instance v2, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    invoke-direct {v2}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;-><init>()V

    .line 141653
    iput-object v1, v2, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 141654
    move-object v2, v2

    .line 141655
    iput-boolean v0, v2, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->c:Z

    .line 141656
    move-object v2, v2

    .line 141657
    iget-object v0, p0, LX/0oZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 141658
    iget-object v4, v1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v4, v4

    .line 141659
    iget-object v5, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v5, v5

    .line 141660
    invoke-virtual {v4, v5}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141661
    invoke-virtual {v0, p1, v1}, LX/0rB;->a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v0

    .line 141662
    iput-object v0, v2, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->b:Ljava/lang/String;

    .line 141663
    :cond_1
    return-object v2
.end method
