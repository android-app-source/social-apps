.class public LX/1kD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/1lA;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1RN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 309156
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1kD;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Zb;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 309157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309158
    iput-object p1, p0, LX/1kD;->b:LX/0Or;

    .line 309159
    iput-object p2, p0, LX/1kD;->a:LX/0Zb;

    .line 309160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1kD;->c:Ljava/util/HashMap;

    .line 309161
    return-void
.end method

.method public static a(LX/0QB;)LX/1kD;
    .locals 8

    .prologue
    .line 309116
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 309117
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 309118
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 309119
    if-nez v1, :cond_0

    .line 309120
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309121
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 309122
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 309123
    sget-object v1, LX/1kD;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 309124
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 309125
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 309126
    :cond_1
    if-nez v1, :cond_4

    .line 309127
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 309128
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 309129
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 309130
    new-instance v7, LX/1kD;

    const/16 v1, 0x2fd

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {v7, p0, v1}, LX/1kD;-><init>(LX/0Or;LX/0Zb;)V

    .line 309131
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309132
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 309133
    if-nez v1, :cond_2

    .line 309134
    sget-object v0, LX/1kD;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 309135
    :goto_1
    if-eqz v0, :cond_3

    .line 309136
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 309137
    :goto_3
    check-cast v0, LX/1kD;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 309138
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 309139
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 309140
    :catchall_1
    move-exception v0

    .line 309141
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 309142
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 309143
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 309144
    :cond_2
    :try_start_8
    sget-object v0, LX/1kD;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kD;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/1kD;Ljava/lang/String;)LX/1lA;
    .locals 2

    .prologue
    .line 309151
    iget-object v0, p0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lA;

    .line 309152
    if-nez v0, :cond_0

    .line 309153
    new-instance v0, LX/1lA;

    invoke-direct {v0}, LX/1lA;-><init>()V

    .line 309154
    iget-object v1, p0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309155
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 309162
    iget-object v0, p0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lA;

    .line 309163
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LX/1lA;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 309148
    invoke-static {p0, p1}, LX/1kD;->c(LX/1kD;Ljava/lang/String;)LX/1lA;

    move-result-object v0

    .line 309149
    iput-object p2, v0, LX/1lA;->b:Ljava/lang/String;

    .line 309150
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/1RN;
    .locals 1

    .prologue
    .line 309146
    iget-object v0, p0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lA;

    .line 309147
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, LX/1lA;->a:LX/1RN;

    goto :goto_0
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309145
    iget-object v0, p0, LX/1kD;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
