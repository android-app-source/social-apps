.class public LX/1HJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public k:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:LX/1cF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:Ljava/util/Map;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public o:Ljava/util/Map;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field public final p:LX/1HK;

.field private final q:LX/1Gj;

.field private final r:Z

.field private final s:Z

.field private final t:LX/1HH;

.field public final u:Z

.field private v:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1HK;LX/1Gj;ZZLX/1HH;Z)V
    .locals 1

    .prologue
    .line 226914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226915
    iput-object p1, p0, LX/1HJ;->p:LX/1HK;

    .line 226916
    iput-object p2, p0, LX/1HJ;->q:LX/1Gj;

    .line 226917
    iput-boolean p3, p0, LX/1HJ;->r:Z

    .line 226918
    iput-boolean p4, p0, LX/1HJ;->s:Z

    .line 226919
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1HJ;->n:Ljava/util/Map;

    .line 226920
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1HJ;->o:Ljava/util/Map;

    .line 226921
    iput-object p5, p0, LX/1HJ;->t:LX/1HH;

    .line 226922
    iput-boolean p6, p0, LX/1HJ;->u:Z

    .line 226923
    return-void
.end method

.method private static a(LX/1HJ;)LX/1cF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226924
    monitor-enter p0

    .line 226925
    :try_start_0
    iget-object v0, p0, LX/1HJ;->e:LX/1cF;

    if-nez v0, :cond_0

    .line 226926
    new-instance v0, LX/4fI;

    invoke-static {p0}, LX/1HJ;->d(LX/1HJ;)LX/1cF;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fI;-><init>(LX/1cF;)V

    iput-object v0, p0, LX/1HJ;->e:LX/1cF;

    .line 226927
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226928
    iget-object v0, p0, LX/1HJ;->e:LX/1cF;

    return-object v0

    .line 226929
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226930
    const/4 v0, 0x1

    new-array v0, v0, [LX/4ey;

    .line 226931
    const/4 v1, 0x0

    iget-object v2, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v2}, LX/1HK;->e()LX/4f1;

    move-result-object v2

    aput-object v2, v0, v1

    .line 226932
    invoke-static {p0, p1, v0}, LX/1HJ;->a(LX/1HJ;LX/1cF;[LX/4ey;)LX/1cF;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1HJ;LX/1cF;[LX/4ey;)LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;[",
            "LX/4ey",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226933
    invoke-static {p0, p1}, LX/1HJ;->c(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    .line 226934
    invoke-static {v0}, LX/1HK;->a(LX/1cF;)LX/1cO;

    move-result-object v1

    .line 226935
    iget-object v2, p0, LX/1HJ;->p:LX/1HK;

    const/4 v3, 0x1

    iget-boolean p1, p0, LX/1HJ;->u:Z

    invoke-virtual {v2, v1, v3, p1}, LX/1HK;->a(LX/1cF;ZZ)LX/1cP;

    move-result-object v1

    .line 226936
    iget-object v2, p0, LX/1HJ;->p:LX/1HK;

    .line 226937
    new-instance v3, LX/4fK;

    const/4 p1, 0x5

    iget-object v0, v2, LX/1HK;->k:LX/1Ft;

    invoke-interface {v0}, LX/1Ft;->e()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-direct {v3, p1, v0, v1}, LX/4fK;-><init>(ILjava/util/concurrent/Executor;LX/1cF;)V

    move-object v1, v3

    .line 226938
    new-instance v2, LX/4fM;

    invoke-direct {v2, p2}, LX/4fM;-><init>([LX/4ey;)V

    move-object v2, v2

    .line 226939
    iget-object v3, p0, LX/1HJ;->p:LX/1HK;

    const/4 p1, 0x1

    iget-boolean v0, p0, LX/1HJ;->u:Z

    invoke-virtual {v3, v2, p1, v0}, LX/1HK;->a(LX/1cF;ZZ)LX/1cP;

    move-result-object v2

    move-object v2, v2

    .line 226940
    new-instance v3, LX/4eo;

    invoke-direct {v3, v2, v1}, LX/4eo;-><init>(LX/1cF;LX/1cF;)V

    move-object v1, v3

    .line 226941
    move-object v0, v1

    .line 226942
    invoke-static {p0, v0}, LX/1HJ;->b(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x1e

    .line 226943
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 226944
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static b(LX/1HJ;)LX/1cF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226945
    monitor-enter p0

    .line 226946
    :try_start_0
    iget-object v0, p0, LX/1HJ;->d:LX/1cF;

    if-nez v0, :cond_0

    .line 226947
    new-instance v0, LX/4fI;

    invoke-static {p0}, LX/1HJ;->h(LX/1HJ;)LX/1cF;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fI;-><init>(LX/1cF;)V

    iput-object v0, p0, LX/1HJ;->d:LX/1cF;

    .line 226948
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226949
    iget-object v0, p0, LX/1HJ;->d:LX/1cF;

    return-object v0

    .line 226950
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226951
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226952
    new-instance v1, LX/1cQ;

    iget-object v2, v0, LX/1HK;->d:LX/1FQ;

    iget-object v3, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v3}, LX/1Ft;->c()Ljava/util/concurrent/Executor;

    move-result-object v3

    iget-object v4, v0, LX/1HK;->e:LX/1GL;

    iget-object v5, v0, LX/1HK;->f:LX/1Gv;

    iget-boolean v6, v0, LX/1HK;->g:Z

    iget-boolean v7, v0, LX/1HK;->h:Z

    iget-boolean v8, v0, LX/1HK;->j:Z

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, LX/1cQ;-><init>(LX/1FQ;Ljava/util/concurrent/Executor;LX/1GL;LX/1Gv;ZZZLX/1cF;)V

    move-object v0, v1

    .line 226953
    invoke-static {p0, v0}, LX/1HJ;->e(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized c()LX/1cF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226954
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->a:LX/1cF;

    if-nez v0, :cond_0

    .line 226955
    invoke-static {p0}, LX/1HJ;->f(LX/1HJ;)LX/1cF;

    move-result-object v0

    invoke-static {p0, v0}, LX/1HJ;->b(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->a:LX/1cF;

    .line 226956
    :cond_0
    iget-object v0, p0, LX/1HJ;->a:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226958
    sget-boolean v0, LX/1cG;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/1HJ;->s:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    if-nez v0, :cond_1

    .line 226959
    :cond_0
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v0, p1}, LX/1HK;->o(LX/1cF;)LX/1cH;

    move-result-object p1

    .line 226960
    :cond_1
    iget-object v2, p0, LX/1HJ;->p:LX/1HK;

    .line 226961
    new-instance v3, LX/1cI;

    iget-object v4, v2, LX/1HK;->o:LX/1Ib;

    invoke-direct {v3, p1, v4}, LX/1cI;-><init>(LX/1cF;LX/1Ib;)V

    move-object v2, v3

    .line 226962
    iget-object v3, p0, LX/1HJ;->p:LX/1HK;

    .line 226963
    new-instance v4, LX/1cJ;

    iget-object v5, v3, LX/1HK;->m:LX/1HY;

    iget-object v6, v3, LX/1HK;->n:LX/1HY;

    iget-object v7, v3, LX/1HK;->r:LX/1Ao;

    iget-object v8, v3, LX/1HK;->t:LX/1IZ;

    move-object v9, v2

    invoke-direct/range {v4 .. v9}, LX/1cJ;-><init>(LX/1HY;LX/1HY;LX/1Ao;LX/1IZ;LX/1cF;)V

    move-object v2, v4

    .line 226964
    iget-object v3, p0, LX/1HJ;->p:LX/1HK;

    .line 226965
    new-instance v4, LX/1cK;

    iget-object v5, v3, LX/1HK;->o:LX/1Ib;

    invoke-direct {v4, v2, v5}, LX/1cK;-><init>(LX/1cF;LX/1Ib;)V

    move-object v2, v4

    .line 226966
    move-object v0, v2

    .line 226967
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    .line 226968
    new-instance v2, LX/1cL;

    iget-object v3, v1, LX/1HK;->p:LX/1Fh;

    iget-object v4, v1, LX/1HK;->r:LX/1Ao;

    invoke-direct {v2, v3, v4, v0}, LX/1cL;-><init>(LX/1Fh;LX/1Ao;LX/1cF;)V

    move-object v0, v2

    .line 226969
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    .line 226970
    new-instance v2, LX/1cM;

    iget-object v3, v1, LX/1HK;->r:LX/1Ao;

    invoke-direct {v2, v3, v0}, LX/1cM;-><init>(LX/1Ao;LX/1cF;)V

    move-object v0, v2

    .line 226971
    return-object v0
.end method

.method private static declared-synchronized d(LX/1HJ;)LX/1cF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226802
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->c:LX/1cF;

    if-nez v0, :cond_0

    .line 226803
    invoke-static {p0}, LX/1HJ;->f(LX/1HJ;)LX/1cF;

    move-result-object v0

    iget-object v1, p0, LX/1HJ;->t:LX/1HH;

    invoke-static {v0, v1}, LX/1HK;->a(LX/1cF;LX/1HH;)LX/1cT;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->c:LX/1cF;

    .line 226804
    :cond_0
    iget-object v0, p0, LX/1HJ;->c:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()LX/1cF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226972
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->g:LX/1cF;

    if-nez v0, :cond_0

    .line 226973
    invoke-static {p0}, LX/1HJ;->d(LX/1HJ;)LX/1cF;

    move-result-object v0

    invoke-static {v0}, LX/1HK;->m(LX/1cF;)LX/24o;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->g:LX/1cF;

    .line 226974
    :cond_0
    iget-object v0, p0, LX/1HJ;->g:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226975
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static e(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226976
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226977
    new-instance v1, LX/1cR;

    iget-object v2, v0, LX/1HK;->q:LX/1Fh;

    iget-object v3, v0, LX/1HK;->r:LX/1Ao;

    invoke-direct {v1, v2, v3, p1}, LX/1cR;-><init>(LX/1Fh;LX/1Ao;LX/1cF;)V

    move-object v0, v1

    .line 226978
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    .line 226979
    new-instance v2, LX/1cS;

    iget-object v3, v1, LX/1HK;->r:LX/1Ao;

    invoke-direct {v2, v3, v0}, LX/1cS;-><init>(LX/1Ao;LX/1cF;)V

    move-object v0, v2

    .line 226980
    iget-object v1, p0, LX/1HJ;->t:LX/1HH;

    invoke-static {v0, v1}, LX/1HK;->a(LX/1cF;LX/1HH;)LX/1cT;

    move-result-object v0

    .line 226981
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    .line 226982
    new-instance v2, LX/1cU;

    iget-object v3, v1, LX/1HK;->q:LX/1Fh;

    iget-object p0, v1, LX/1HK;->r:LX/1Ao;

    invoke-direct {v2, v3, p0, v0}, LX/1cU;-><init>(LX/1Fh;LX/1Ao;LX/1cF;)V

    move-object v0, v2

    .line 226983
    return-object v0
.end method

.method private static e(LX/1bf;)V
    .locals 2

    .prologue
    .line 226909
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226910
    iget-object v0, p0, LX/1bf;->k:LX/1bY;

    move-object v0, v0

    .line 226911
    invoke-virtual {v0}, LX/1bY;->getValue()I

    move-result v0

    sget-object v1, LX/1bY;->ENCODED_MEMORY_CACHE:LX/1bY;

    invoke-virtual {v1}, LX/1bY;->getValue()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 226912
    return-void

    .line 226913
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized f(LX/1HJ;)LX/1cF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226984
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->v:LX/1cF;

    if-nez v0, :cond_0

    .line 226985
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    iget-object v1, p0, LX/1HJ;->q:LX/1Gj;

    .line 226986
    new-instance v2, LX/1cE;

    iget-object v3, v0, LX/1HK;->l:LX/1Fj;

    iget-object v4, v0, LX/1HK;->d:LX/1FQ;

    invoke-direct {v2, v3, v4, v1}, LX/1cE;-><init>(LX/1Fj;LX/1FQ;LX/1Gj;)V

    move-object v0, v2

    .line 226987
    invoke-static {p0, v0}, LX/1HJ;->c(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    .line 226988
    invoke-static {v0}, LX/1HK;->a(LX/1cF;)LX/1cO;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->v:LX/1cF;

    .line 226989
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    iget-object v1, p0, LX/1HJ;->v:LX/1cF;

    iget-boolean v2, p0, LX/1HJ;->r:Z

    iget-boolean v3, p0, LX/1HJ;->u:Z

    invoke-virtual {v0, v1, v2, v3}, LX/1HK;->a(LX/1cF;ZZ)LX/1cP;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->v:LX/1cF;

    .line 226990
    :cond_0
    iget-object v0, p0, LX/1HJ;->v:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226991
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/1HJ;LX/1bf;)LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            ")",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226823
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226824
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 226825
    const-string v1, "Uri is null."

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226826
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226827
    invoke-direct {p0}, LX/1HJ;->c()LX/1cF;

    move-result-object v0

    .line 226828
    :goto_0
    return-object v0

    .line 226829
    :cond_0
    invoke-static {v0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 226830
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/46I;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/46I;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226831
    invoke-direct {p0}, LX/1HJ;->j()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226832
    :cond_1
    invoke-direct {p0}, LX/1HJ;->i()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226833
    :cond_2
    invoke-static {v0}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226834
    invoke-direct {p0}, LX/1HJ;->k()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226835
    :cond_3
    invoke-static {v0}, LX/1be;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 226836
    invoke-direct {p0}, LX/1HJ;->m()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226837
    :cond_4
    invoke-static {v0}, LX/1be;->g(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 226838
    invoke-direct {p0}, LX/1HJ;->l()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226839
    :cond_5
    const-string v1, "data"

    invoke-static {v0}, LX/1be;->i(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 226840
    if-eqz v1, :cond_6

    .line 226841
    invoke-direct {p0}, LX/1HJ;->n()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226842
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported uri scheme! Uri is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1HJ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static declared-synchronized f(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226806
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226807
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226808
    new-instance v1, LX/4fG;

    iget-object v2, v0, LX/1HK;->v:LX/1FZ;

    iget-object v3, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v3}, LX/1Ft;->d()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, LX/4fG;-><init>(LX/1cF;LX/1FZ;Ljava/util/concurrent/Executor;)V

    move-object v0, v1

    .line 226809
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    .line 226810
    new-instance v2, LX/4fA;

    iget-object v3, v1, LX/1HK;->q:LX/1Fh;

    iget-object v4, v1, LX/1HK;->r:LX/1Ao;

    invoke-direct {v2, v3, v4, v0}, LX/4fA;-><init>(LX/1Fh;LX/1Ao;LX/1cF;)V

    move-object v0, v2

    .line 226811
    iget-object v1, p0, LX/1HJ;->n:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226812
    :cond_0
    iget-object v0, p0, LX/1HJ;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()LX/1cF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226814
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->f:LX/1cF;

    if-nez v0, :cond_0

    .line 226815
    invoke-static {p0}, LX/1HJ;->d(LX/1HJ;)LX/1cF;

    move-result-object v0

    invoke-static {v0}, LX/1HK;->m(LX/1cF;)LX/24o;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->f:LX/1cF;

    .line 226816
    :cond_0
    iget-object v0, p0, LX/1HJ;->f:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226817
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/1HJ;LX/1cF;)LX/1cF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226818
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226819
    invoke-static {p1}, LX/1HK;->m(LX/1cF;)LX/24o;

    move-result-object v0

    .line 226820
    iget-object v1, p0, LX/1HJ;->o:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226821
    :cond_0
    iget-object v0, p0, LX/1HJ;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226822
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized h(LX/1HJ;)LX/1cF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226843
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->b:LX/1cF;

    if-nez v0, :cond_0

    .line 226844
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v0}, LX/1HK;->f()LX/4f4;

    move-result-object v0

    .line 226845
    invoke-static {p0, v0}, LX/1HJ;->c(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    .line 226846
    iget-object v1, p0, LX/1HJ;->t:LX/1HH;

    invoke-static {v0, v1}, LX/1HK;->a(LX/1cF;LX/1HH;)LX/1cT;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->b:LX/1cF;

    .line 226847
    :cond_0
    iget-object v0, p0, LX/1HJ;->b:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()LX/1cF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226849
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->h:LX/1cF;

    if-nez v0, :cond_0

    .line 226850
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v0}, LX/1HK;->f()LX/4f4;

    move-result-object v0

    .line 226851
    invoke-static {p0, v0}, LX/1HJ;->a(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->h:LX/1cF;

    .line 226852
    :cond_0
    iget-object v0, p0, LX/1HJ;->h:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226853
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()LX/1cF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226854
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->i:LX/1cF;

    if-nez v0, :cond_0

    .line 226855
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226856
    new-instance v1, LX/4f7;

    iget-object v2, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v2}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4f7;-><init>(Ljava/util/concurrent/Executor;)V

    move-object v0, v1

    .line 226857
    invoke-static {p0, v0}, LX/1HJ;->e(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->i:LX/1cF;

    .line 226858
    :cond_0
    iget-object v0, p0, LX/1HJ;->i:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226859
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()LX/1cF;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226860
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->j:LX/1cF;

    if-nez v0, :cond_0

    .line 226861
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226862
    new-instance v1, LX/4ex;

    iget-object v2, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v2}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, LX/1HK;->l:LX/1Fj;

    iget-object v4, v0, LX/1HK;->a:Landroid/content/ContentResolver;

    iget-boolean v5, v0, LX/1HK;->i:Z

    invoke-direct {v1, v2, v3, v4, v5}, LX/4ex;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;Z)V

    move-object v0, v1

    .line 226863
    const/4 v1, 0x2

    new-array v1, v1, [LX/4ey;

    .line 226864
    const/4 v2, 0x0

    iget-object v3, p0, LX/1HJ;->p:LX/1HK;

    .line 226865
    new-instance v4, LX/4ez;

    iget-object v5, v3, LX/1HK;->k:LX/1Ft;

    invoke-interface {v5}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, v3, LX/1HK;->l:LX/1Fj;

    iget-object v7, v3, LX/1HK;->a:Landroid/content/ContentResolver;

    iget-boolean v8, v3, LX/1HK;->i:Z

    invoke-direct {v4, v5, v6, v7, v8}, LX/4ez;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;Z)V

    move-object v3, v4

    .line 226866
    aput-object v3, v1, v2

    .line 226867
    const/4 v2, 0x1

    iget-object v3, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v3}, LX/1HK;->e()LX/4f1;

    move-result-object v3

    aput-object v3, v1, v2

    .line 226868
    invoke-static {p0, v0, v1}, LX/1HJ;->a(LX/1HJ;LX/1cF;[LX/4ey;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->j:LX/1cF;

    .line 226869
    :cond_0
    iget-object v0, p0, LX/1HJ;->j:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226870
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized l()LX/1cF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226871
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->k:LX/1cF;

    if-nez v0, :cond_0

    .line 226872
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226873
    new-instance v1, LX/4f5;

    iget-object v2, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v2}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, LX/1HK;->l:LX/1Fj;

    iget-object v4, v0, LX/1HK;->b:Landroid/content/res/Resources;

    iget-boolean v5, v0, LX/1HK;->i:Z

    invoke-direct {v1, v2, v3, v4, v5}, LX/4f5;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/res/Resources;Z)V

    move-object v0, v1

    .line 226874
    invoke-static {p0, v0}, LX/1HJ;->a(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->k:LX/1cF;

    .line 226875
    :cond_0
    iget-object v0, p0, LX/1HJ;->k:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226876
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()LX/1cF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226877
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->l:LX/1cF;

    if-nez v0, :cond_0

    .line 226878
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226879
    new-instance v1, LX/4ew;

    iget-object v2, v0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v2}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, v0, LX/1HK;->l:LX/1Fj;

    iget-object v4, v0, LX/1HK;->c:Landroid/content/res/AssetManager;

    iget-boolean v5, v0, LX/1HK;->i:Z

    invoke-direct {v1, v2, v3, v4, v5}, LX/4ew;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/res/AssetManager;Z)V

    move-object v0, v1

    .line 226880
    invoke-static {p0, v0}, LX/1HJ;->a(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->l:LX/1cF;

    .line 226881
    :cond_0
    iget-object v0, p0, LX/1HJ;->l:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226882
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized n()LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226883
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1HJ;->m:LX/1cF;

    if-nez v0, :cond_2

    .line 226884
    iget-object v0, p0, LX/1HJ;->p:LX/1HK;

    .line 226885
    new-instance v1, LX/4eq;

    iget-object v2, v0, LX/1HK;->l:LX/1Fj;

    iget-boolean v3, v0, LX/1HK;->i:Z

    invoke-direct {v1, v2, v3}, LX/4eq;-><init>(LX/1Fj;Z)V

    move-object v0, v1

    .line 226886
    sget-boolean v1, LX/1cG;->a:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/1HJ;->s:Z

    if-eqz v1, :cond_0

    sget-object v1, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    if-nez v1, :cond_1

    .line 226887
    :cond_0
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    invoke-virtual {v1, v0}, LX/1HK;->o(LX/1cF;)LX/1cH;

    move-result-object v0

    .line 226888
    :cond_1
    invoke-static {v0}, LX/1HK;->a(LX/1cF;)LX/1cO;

    move-result-object v0

    .line 226889
    iget-object v1, p0, LX/1HJ;->p:LX/1HK;

    const/4 v2, 0x1

    iget-boolean v3, p0, LX/1HJ;->u:Z

    invoke-virtual {v1, v0, v2, v3}, LX/1HK;->a(LX/1cF;ZZ)LX/1cP;

    move-result-object v0

    .line 226890
    invoke-static {p0, v0}, LX/1HJ;->b(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v0

    iput-object v0, p0, LX/1HJ;->m:LX/1cF;

    .line 226891
    :cond_2
    iget-object v0, p0, LX/1HJ;->m:LX/1cF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 226892
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            ")",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226893
    invoke-static {p1}, LX/1HJ;->e(LX/1bf;)V

    .line 226894
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 226895
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226896
    invoke-static {p0}, LX/1HJ;->a(LX/1HJ;)LX/1cF;

    move-result-object v0

    .line 226897
    :goto_0
    return-object v0

    .line 226898
    :cond_0
    invoke-static {v0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 226899
    invoke-static {p0}, LX/1HJ;->b(LX/1HJ;)LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226900
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported uri scheme for encoded image fetch! Uri is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1HJ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final b(LX/1bf;)LX/1cF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            ")",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226901
    invoke-static {p1}, LX/1HJ;->e(LX/1bf;)V

    .line 226902
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 226903
    invoke-static {v0}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226904
    invoke-direct {p0}, LX/1HJ;->e()LX/1cF;

    move-result-object v0

    .line 226905
    :goto_0
    return-object v0

    .line 226906
    :cond_0
    invoke-static {v0}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 226907
    invoke-direct {p0}, LX/1HJ;->g()LX/1cF;

    move-result-object v0

    goto :goto_0

    .line 226908
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported uri scheme for encoded image fetch! Uri is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1HJ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
