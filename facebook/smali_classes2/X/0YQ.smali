.class public LX/0YQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YR;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0YQ;


# instance fields
.field private final a:Z

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:LX/0YR;

.field private f:LX/1hM;

.field private g:LX/1hP;

.field private h:J

.field private i:J


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 81224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81225
    const/4 v0, 0x2

    iput v0, p0, LX/0YQ;->b:I

    .line 81226
    iput v1, p0, LX/0YQ;->c:I

    .line 81227
    iput v1, p0, LX/0YQ;->d:I

    .line 81228
    const/16 v0, 0x44

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0YQ;->a:Z

    .line 81229
    return-void
.end method

.method public static a(LX/0QB;)LX/0YQ;
    .locals 4

    .prologue
    .line 81230
    sget-object v0, LX/0YQ;->j:LX/0YQ;

    if-nez v0, :cond_1

    .line 81231
    const-class v1, LX/0YQ;

    monitor-enter v1

    .line 81232
    :try_start_0
    sget-object v0, LX/0YQ;->j:LX/0YQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 81233
    if-eqz v2, :cond_0

    .line 81234
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 81235
    new-instance p0, LX/0YQ;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/0YQ;-><init>(LX/0Uh;)V

    .line 81236
    move-object v0, p0

    .line 81237
    sput-object v0, LX/0YQ;->j:LX/0YQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81238
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 81239
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 81240
    :cond_1
    sget-object v0, LX/0YQ;->j:LX/0YQ;

    return-object v0

    .line 81241
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 81242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/1hM;
    .locals 4

    .prologue
    .line 81209
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0YQ;->e:LX/0YR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 81210
    const/4 v0, 0x0

    .line 81211
    :goto_0
    monitor-exit p0

    return-object v0

    .line 81212
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, LX/0YQ;->h:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x3b9aca00

    div-long/2addr v0, v2

    .line 81213
    iget-object v2, p0, LX/0YQ;->f:LX/1hM;

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 81214
    :cond_1
    iget-object v0, p0, LX/0YQ;->e:LX/0YR;

    invoke-interface {v0}, LX/0YR;->a()LX/1hM;

    move-result-object v0

    iput-object v0, p0, LX/0YQ;->f:LX/1hM;

    .line 81215
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0YQ;->h:J

    .line 81216
    :cond_2
    iget-object v0, p0, LX/0YQ;->f:LX/1hM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0YR;)V
    .locals 0

    .prologue
    .line 81222
    iput-object p1, p0, LX/0YQ;->e:LX/0YR;

    .line 81223
    return-void
.end method

.method public final declared-synchronized b()LX/1hP;
    .locals 4

    .prologue
    .line 81243
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0YQ;->e:LX/0YR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 81244
    const/4 v0, 0x0

    .line 81245
    :goto_0
    monitor-exit p0

    return-object v0

    .line 81246
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, LX/0YQ;->i:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x3b9aca00

    div-long/2addr v0, v2

    .line 81247
    iget-object v2, p0, LX/0YQ;->g:LX/1hP;

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 81248
    :cond_1
    iget-object v0, p0, LX/0YQ;->e:LX/0YR;

    invoke-interface {v0}, LX/0YR;->b()LX/1hP;

    move-result-object v0

    iput-object v0, p0, LX/0YQ;->g:LX/1hP;

    .line 81249
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0YQ;->i:J

    .line 81250
    :cond_2
    iget-object v0, p0, LX/0YQ;->g:LX/1hP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 81221
    const/4 v0, 0x5

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 81220
    const/4 v0, 0x2

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 81219
    const/4 v0, 0x5

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 81218
    iget-boolean v0, p0, LX/0YQ;->a:Z

    return v0
.end method
