.class public LX/1pK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1pH;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/HcV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/1pI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/HcU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/1pJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/HcT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1pH;LX/HcV;LX/1pI;LX/HcU;LX/1pJ;LX/HcT;)V
    .locals 0
    .param p1    # LX/1pH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/HcV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1pI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/HcU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1pJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/HcT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 328135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328136
    iput-object p1, p0, LX/1pK;->a:LX/1pH;

    .line 328137
    iput-object p2, p0, LX/1pK;->b:LX/HcV;

    .line 328138
    iput-object p3, p0, LX/1pK;->c:LX/1pI;

    .line 328139
    iput-object p4, p0, LX/1pK;->d:LX/HcU;

    .line 328140
    iput-object p5, p0, LX/1pK;->e:LX/1pJ;

    .line 328141
    iput-object p6, p0, LX/1pK;->f:LX/HcT;

    .line 328142
    return-void
.end method


# virtual methods
.method public final a(LX/1pZ;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1pZ",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 328143
    sget-object v0, LX/1pY;->a:LX/1pZ;

    if-ne p1, v0, :cond_0

    .line 328144
    iget-object v0, p0, LX/1pK;->a:LX/1pH;

    .line 328145
    :goto_0
    return-object v0

    .line 328146
    :cond_0
    sget-object v0, LX/1pY;->b:LX/1pZ;

    if-ne p1, v0, :cond_1

    .line 328147
    iget-object v0, p0, LX/1pK;->b:LX/HcV;

    goto :goto_0

    .line 328148
    :cond_1
    sget-object v0, LX/1pY;->c:LX/1pZ;

    if-ne p1, v0, :cond_2

    .line 328149
    iget-object v0, p0, LX/1pK;->c:LX/1pI;

    goto :goto_0

    .line 328150
    :cond_2
    sget-object v0, LX/1pY;->d:LX/1pZ;

    if-ne p1, v0, :cond_3

    .line 328151
    iget-object v0, p0, LX/1pK;->d:LX/HcU;

    goto :goto_0

    .line 328152
    :cond_3
    sget-object v0, LX/1pY;->e:LX/1pZ;

    if-ne p1, v0, :cond_4

    .line 328153
    iget-object v0, p0, LX/1pK;->e:LX/1pJ;

    goto :goto_0

    .line 328154
    :cond_4
    sget-object v0, LX/1pY;->f:LX/1pZ;

    if-ne p1, v0, :cond_5

    .line 328155
    iget-object v0, p0, LX/1pK;->f:LX/HcT;

    goto :goto_0

    .line 328156
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
