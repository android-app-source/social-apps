.class public LX/1Sr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/03R;

.field private static b:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 249262
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, LX/1Sr;->a:LX/03R;

    .line 249263
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, LX/1Sr;->b:LX/03R;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 249270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0ad;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 249267
    sget-object v0, LX/1Sr;->a:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249268
    sget-short v0, LX/0fe;->r:S

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    sput-object v0, LX/1Sr;->a:LX/03R;

    .line 249269
    :cond_0
    sget-object v0, LX/1Sr;->a:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public static b(LX/0ad;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 249264
    sget-object v0, LX/1Sr;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249265
    sget-short v0, LX/0fe;->q:S

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    sput-object v0, LX/1Sr;->b:LX/03R;

    .line 249266
    :cond_0
    sget-object v0, LX/1Sr;->b:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
