.class public LX/1Jl;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public A:LX/1UQ;

.field private B:LX/1Fn;

.field private final a:LX/1DO;

.field private final b:LX/1Cc;

.field private final c:LX/1Ct;

.field private final d:LX/1D8;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1S1;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1DA;

.field private final g:LX/1Jy;

.field private final h:LX/1DF;

.field private final i:LX/1Jp;

.field private final j:LX/1Jn;

.field private final k:LX/0ad;

.field public final l:LX/1Jm;

.field public final m:LX/1DJ;

.field private final n:LX/0Yl;

.field private final o:LX/0Yi;

.field public final p:LX/1Jg;

.field private final q:LX/1Jz;

.field public final r:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final s:LX/0fO;

.field private final t:LX/1DC;

.field private final u:LX/0Uh;

.field public v:Lcom/facebook/api/feedtype/FeedType;

.field public w:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;"
        }
    .end annotation
.end field

.field public x:Landroid/content/Context;

.field public y:LX/1Pa;

.field public z:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1Jm;LX/0ad;LX/1Jn;LX/1Jp;LX/1Jy;LX/0Yl;LX/0Yi;LX/1Jz;LX/0fO;LX/0Uh;LX/0Or;LX/1Fn;LX/1DO;LX/1Cc;LX/1Ct;LX/1D8;LX/1DA;LX/1DC;LX/1DF;LX/1DJ;LX/1Jg;)V
    .locals 1
    .param p14    # LX/1DO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/1Cc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # LX/1Ct;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/1D8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p18    # LX/1DA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p19    # LX/1DC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p20    # LX/1DF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p21    # LX/1DJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p22    # LX/1Jg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/1Jm;",
            "LX/0ad;",
            "LX/1Jn;",
            "LX/1Jp;",
            "LX/1Jy;",
            "LX/0Yl;",
            "LX/0Yi;",
            "LX/1Jz;",
            "LX/0fO;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/1S1;",
            ">;",
            "LX/1Fn;",
            "LX/1DO;",
            "LX/1Cc;",
            "LX/1Ct;",
            "LX/1D8;",
            "LX/1DA;",
            "LX/1DC;",
            "LX/1DF;",
            "LX/1DJ;",
            "LX/1Jg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 230508
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 230509
    iput-object p1, p0, LX/1Jl;->r:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 230510
    iput-object p7, p0, LX/1Jl;->n:LX/0Yl;

    .line 230511
    iput-object p8, p0, LX/1Jl;->o:LX/0Yi;

    .line 230512
    iput-object p3, p0, LX/1Jl;->k:LX/0ad;

    .line 230513
    iput-object p2, p0, LX/1Jl;->l:LX/1Jm;

    .line 230514
    iput-object p4, p0, LX/1Jl;->j:LX/1Jn;

    .line 230515
    iput-object p5, p0, LX/1Jl;->i:LX/1Jp;

    .line 230516
    iput-object p6, p0, LX/1Jl;->g:LX/1Jy;

    .line 230517
    iput-object p12, p0, LX/1Jl;->e:LX/0Or;

    .line 230518
    iput-object p11, p0, LX/1Jl;->u:LX/0Uh;

    .line 230519
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1Jl;->h:LX/1DF;

    .line 230520
    iput-object p13, p0, LX/1Jl;->B:LX/1Fn;

    .line 230521
    iput-object p14, p0, LX/1Jl;->a:LX/1DO;

    .line 230522
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Jl;->b:LX/1Cc;

    .line 230523
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Jl;->c:LX/1Ct;

    .line 230524
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Jl;->d:LX/1D8;

    .line 230525
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1Jl;->f:LX/1DA;

    .line 230526
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1Jl;->t:LX/1DC;

    .line 230527
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1Jl;->m:LX/1DJ;

    .line 230528
    iput-object p9, p0, LX/1Jl;->q:LX/1Jz;

    .line 230529
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1Jl;->p:LX/1Jg;

    .line 230530
    iput-object p10, p0, LX/1Jl;->s:LX/0fO;

    .line 230531
    return-void
.end method

.method private static a(LX/1Jl;LX/1UQ;)V
    .locals 2

    .prologue
    .line 230389
    iget-object v0, p0, LX/1Jl;->k:LX/0ad;

    invoke-static {v0}, LX/1Sr;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230390
    new-instance v0, LX/9Ag;

    invoke-direct {v0, p1}, LX/9Ag;-><init>(LX/1UQ;)V

    .line 230391
    iget-object v1, p0, LX/1Jl;->i:LX/1Jp;

    .line 230392
    iput-object v0, v1, LX/1Jp;->k:LX/1Jt;

    .line 230393
    iget-object v0, p0, LX/1Jl;->k:LX/0ad;

    invoke-static {v0}, LX/1Sr;->b(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230394
    iget-object v0, p0, LX/1Jl;->i:LX/1Jp;

    iget-object v1, p0, LX/1Jl;->q:LX/1Jz;

    .line 230395
    iget-object p0, v0, LX/1Jp;->e:LX/1Ju;

    .line 230396
    iget-object p1, p0, LX/1Ju;->b:LX/01J;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230397
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/1Qr;
    .locals 15

    .prologue
    .line 230409
    const-string v0, "NewsFeedAdapterConfiguration.createAdapter"

    const v1, -0x2c687d47

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 230410
    :try_start_0
    iget-object v0, p0, LX/1Jl;->m:LX/1DJ;

    .line 230411
    iget-object v1, v0, LX/1DJ;->c:LX/0g5;

    move-object v2, v1

    .line 230412
    iget-object v0, p0, LX/1Jl;->n:LX/0Yl;

    iget-object v1, p0, LX/1Jl;->o:LX/0Yi;

    iget-object v3, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v1, v3}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v1

    iget-object v3, p0, LX/1Jl;->o:LX/0Yi;

    const-string v4, "FeedAdapterInit"

    iget-object v5, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3, v4, v5}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0Yl;->h(ILjava/lang/String;)LX/0Yl;

    .line 230413
    iget-object v0, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 230414
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 230415
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 230416
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230417
    iget-object v1, p0, LX/1Jl;->f:LX/1DA;

    invoke-virtual {v1}, LX/1DA;->b()LX/DBx;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230418
    :cond_0
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 230419
    iget-object v4, v1, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v4, v4

    .line 230420
    instance-of v4, v4, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    if-nez v4, :cond_c

    .line 230421
    const/4 v4, 0x1

    .line 230422
    :goto_0
    move v1, v4

    .line 230423
    if-eqz v1, :cond_2

    .line 230424
    iget-object v1, p0, LX/1Jl;->B:LX/1Fn;

    invoke-virtual {v1}, LX/1Fn;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 230425
    new-instance v1, LX/DC5;

    invoke-direct {v1}, LX/DC5;-><init>()V

    .line 230426
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230427
    :cond_1
    iget-object v1, p0, LX/1Jl;->c:LX/1Ct;

    .line 230428
    iget-object v4, v1, LX/1Ct;->b:LX/1Cu;

    move-object v1, v4

    .line 230429
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230430
    :cond_2
    iget-object v1, p0, LX/1Jl;->s:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 230431
    new-instance v1, LX/1QY;

    invoke-direct {v1}, LX/1QY;-><init>()V

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230432
    :cond_3
    iget-object v1, p0, LX/1Jl;->s:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->o()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 230433
    iget-object v1, p0, LX/1Jl;->t:LX/1DC;

    .line 230434
    iget-object v7, v1, LX/1DC;->c:LX/1DE;

    iget-object v8, v1, LX/1DC;->e:LX/0fD;

    .line 230435
    new-instance v9, LX/BaO;

    const-class v10, Landroid/content/Context;

    invoke-interface {v7, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    const-class v11, LX/BaQ;

    invoke-interface {v7, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/BaQ;

    invoke-static {v7}, LX/AK0;->a(LX/0QB;)LX/AK0;

    move-result-object v12

    check-cast v12, LX/AK0;

    invoke-static {v7}, LX/7ga;->a(LX/0QB;)LX/7ga;

    move-result-object v14

    check-cast v14, LX/7ga;

    move-object v13, v8

    invoke-direct/range {v9 .. v14}, LX/BaO;-><init>(Landroid/content/Context;LX/BaQ;LX/AK0;LX/0fD;LX/7ga;)V

    .line 230436
    move-object v7, v9

    .line 230437
    iput-object v7, v1, LX/1DC;->d:LX/BaO;

    .line 230438
    iget-object v7, v1, LX/1DC;->d:LX/BaO;

    move-object v1, v7

    .line 230439
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230440
    :cond_4
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 230441
    invoke-static {v1}, LX/0pO;->b(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v4, 0x1

    :goto_1
    move v1, v4

    .line 230442
    if-nez v1, :cond_6

    :cond_5
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 230443
    :cond_6
    iget-object v1, p0, LX/1Jl;->a:LX/1DO;

    invoke-virtual {v1}, LX/1DO;->b()LX/1Cw;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230444
    :cond_7
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 230445
    iget-object v0, p0, LX/1Jl;->u:LX/0Uh;

    const/16 v1, 0x31e

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 230446
    iget-object v0, p0, LX/1Jl;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230447
    :cond_8
    iget-object v0, p0, LX/1Jl;->d:LX/1D8;

    .line 230448
    iget-object v1, v0, LX/1D8;->c:LX/1D9;

    new-instance v4, LX/1SB;

    invoke-direct {v4, v0}, LX/1SB;-><init>(LX/1D8;)V

    .line 230449
    new-instance v0, LX/1SC;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-direct {v0, v5, v4}, LX/1SC;-><init>(Ljava/util/concurrent/Executor;LX/1SB;)V

    .line 230450
    move-object v1, v0

    .line 230451
    move-object v0, v1

    .line 230452
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230453
    iget-object v0, p0, LX/1Jl;->b:LX/1Cc;

    invoke-virtual {v0, v2}, LX/1Cc;->c(LX/0g8;)LX/1Cw;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 230454
    :cond_9
    iget-object v4, p0, LX/1Jl;->j:LX/1Jn;

    iget-object v0, p0, LX/1Jl;->w:LX/1Iu;

    .line 230455
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 230456
    check-cast v0, LX/0g1;

    iget-object v5, p0, LX/1Jl;->y:LX/1Pa;

    iget-object v1, p0, LX/1Jl;->k:LX/0ad;

    invoke-static {v1}, LX/1Sr;->a(LX/0ad;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, LX/1Jl;->i:LX/1Jp;

    .line 230457
    :goto_2
    iget-object v6, v4, LX/1Jn;->a:LX/1Db;

    invoke-virtual {v6}, LX/1Db;->a()LX/1St;

    move-result-object v6

    invoke-interface {v2, v6}, LX/0g8;->a(LX/1St;)V

    .line 230458
    iget-object v6, v4, LX/1Jn;->b:LX/1DS;

    iget-object v7, v4, LX/1Jn;->d:LX/0Ot;

    invoke-virtual {v6, v7, v0}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v6

    .line 230459
    iput-object v5, v6, LX/1Ql;->f:LX/1PW;

    .line 230460
    move-object v6, v6

    .line 230461
    invoke-virtual {v6, v2}, LX/1Ql;->a(LX/0g8;)LX/1Ql;

    move-result-object v6

    .line 230462
    const/4 v7, 0x1

    iput-boolean v7, v6, LX/1Ql;->i:Z

    .line 230463
    move-object v6, v6

    .line 230464
    const/4 v7, 0x1

    iput-boolean v7, v6, LX/1Ql;->j:Z

    .line 230465
    move-object v6, v6

    .line 230466
    if-nez v1, :cond_e

    .line 230467
    :goto_3
    move-object v6, v6

    .line 230468
    invoke-virtual {v6, v2}, LX/1Ql;->b(LX/0g8;)LX/1Ql;

    move-result-object v6

    invoke-virtual {v6}, LX/1Ql;->d()LX/1Rq;

    move-result-object v6

    .line 230469
    move-object v0, v6

    .line 230470
    iget-object v1, p0, LX/1Jl;->h:LX/1DF;

    .line 230471
    iget-object v7, v1, LX/1DF;->a:LX/1UO;

    if-nez v7, :cond_a

    .line 230472
    iget-object v7, v1, LX/1DF;->h:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v7}, LX/0pO;->a(Lcom/facebook/api/feedtype/FeedType;)Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, v1, LX/1DF;->b:LX/0pJ;

    const/4 v8, 0x0

    .line 230473
    sget-wide v10, LX/0X5;->dr:J

    const/4 v12, 0x4

    invoke-virtual {v7, v10, v11, v12, v8}, LX/0pK;->a(JIZ)Z

    move-result v10

    move v7, v10

    .line 230474
    if-eqz v7, :cond_12

    .line 230475
    new-instance v8, LX/1ls;

    iget-object v7, v1, LX/1DF;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/C8t;

    const v9, 0x7f030626

    invoke-direct {v8, v7, v9, v0}, LX/1ls;-><init>(LX/C8t;ILX/1OP;)V

    iput-object v8, v1, LX/1DF;->a:LX/1UO;

    .line 230476
    :cond_a
    :goto_4
    iget-object v7, v1, LX/1DF;->a:LX/1UO;

    move-object v1, v7

    .line 230477
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 230478
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/1Jl;->g:LX/1Jy;

    invoke-static {v3, v0, v1, v2, v4}, LX/1UQ;->a(LX/0Px;LX/1Rq;LX/0Px;LX/0g7;LX/1Jy;)LX/1UQ;

    move-result-object v0

    .line 230479
    invoke-static {p0, v0}, LX/1Jl;->a(LX/1Jl;LX/1UQ;)V

    .line 230480
    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 230481
    iget-object v1, p0, LX/1Jl;->n:LX/0Yl;

    iget-object v3, p0, LX/1Jl;->o:LX/0Yi;

    iget-object v4, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3, v4}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v3

    iget-object v4, p0, LX/1Jl;->o:LX/0Yi;

    const-string v5, "FeedAdapterInit"

    iget-object v6, p0, LX/1Jl;->v:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v4, v5, v6}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0Yl;->i(ILjava/lang/String;)LX/0Yl;

    .line 230482
    invoke-virtual {v2, v0}, LX/0g5;->a(LX/1UQ;)V

    .line 230483
    iput-object v0, p0, LX/1Jl;->A:LX/1UQ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230484
    const v1, -0x25374afb

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 230485
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 230486
    :catchall_0
    move-exception v0

    const v1, 0x5fe77294

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 230487
    :cond_c
    :try_start_1
    iget-object v4, v1, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v4, v4

    .line 230488
    check-cast v4, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    .line 230489
    iget-object v5, v4, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    move-object v4, v5

    .line 230490
    const-string v5, "DEFAULT"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_d
    :try_start_2
    const/4 v4, 0x0

    goto/16 :goto_1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 230491
    :cond_e
    :try_start_3
    iput-object v1, v6, LX/1Ql;->n:LX/1Jp;

    .line 230492
    const/4 v4, 0x1

    .line 230493
    iget-boolean v7, v6, LX/1Ql;->k:Z

    if-nez v7, :cond_f

    move v7, v4

    :goto_5
    const-string v1, "withWorkingRanges should only be called once."

    invoke-static {v7, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 230494
    iget-object v7, v6, LX/1Ql;->a:LX/1DS;

    iget-object v7, v7, LX/1DS;->g:LX/0ad;

    invoke-static {v7}, LX/1Sr;->a(LX/0ad;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 230495
    :goto_6
    move-object v6, v6

    .line 230496
    goto/16 :goto_3

    .line 230497
    :cond_f
    const/4 v7, 0x0

    goto :goto_5

    .line 230498
    :cond_10
    iget-object v7, v6, LX/1Ql;->n:LX/1Jp;

    if-nez v7, :cond_11

    .line 230499
    iget-object v7, v6, LX/1Ql;->a:LX/1DS;

    iget-object v7, v7, LX/1DS;->r:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1Jp;

    iput-object v7, v6, LX/1Ql;->n:LX/1Jp;

    .line 230500
    :cond_11
    iput-object v2, v6, LX/1Ql;->h:LX/0g8;

    .line 230501
    iput-boolean v4, v6, LX/1Ql;->k:Z

    goto :goto_6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 230502
    :cond_12
    iget-object v7, v1, LX/1DF;->e:LX/1DG;

    const v8, 0x7f030637

    .line 230503
    new-instance v10, LX/1UN;

    invoke-direct {v10, v8}, LX/1UN;-><init>(I)V

    .line 230504
    invoke-static {v7}, LX/1Fn;->b(LX/0QB;)LX/1Fn;

    move-result-object v9

    check-cast v9, LX/1Fn;

    .line 230505
    iput-object v9, v10, LX/1UN;->b:LX/1Fn;

    .line 230506
    move-object v7, v10

    .line 230507
    iput-object v7, v1, LX/1DF;->a:LX/1UO;

    goto/16 :goto_4
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 230408
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 230398
    iget-object v0, p0, LX/1Jl;->k:LX/0ad;

    invoke-static {v0}, LX/1Sr;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230399
    iget-object v0, p0, LX/1Jl;->i:LX/1Jp;

    .line 230400
    iget-boolean v1, v0, LX/1Jp;->i:Z

    const-string v2, "Must have registered listeners before calling unregisterListeners"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 230401
    iget-object v1, v0, LX/1Jp;->j:LX/9Ac;

    .line 230402
    iget-object v2, v1, LX/9Ac;->a:LX/62M;

    iget-object p0, v1, LX/9Ac;->e:LX/9Ab;

    .line 230403
    iget-object v0, v2, LX/62M;->b:LX/01J;

    invoke-virtual {v0, p0}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230404
    iget-boolean v2, v1, LX/9Ac;->c:Z

    if-eqz v2, :cond_1

    .line 230405
    iget-object v2, v1, LX/9Ac;->b:LX/1Rq;

    iget-object p0, v1, LX/9Ac;->g:LX/9Aa;

    invoke-interface {v2, p0}, LX/1Rr;->b(LX/1KR;)V

    .line 230406
    :cond_0
    :goto_0
    return-void

    .line 230407
    :cond_1
    iget-object v2, v1, LX/9Ac;->b:LX/1Rq;

    iget-object p0, v1, LX/9Ac;->f:LX/9AZ;

    invoke-interface {v2, p0}, LX/1OQ;->b(LX/1OD;)V

    goto :goto_0
.end method
