.class public LX/1AK;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 210280
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;
    .locals 4

    .prologue
    .line 210281
    sget-object v0, LX/1AK;->a:Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    if-nez v0, :cond_1

    .line 210282
    const-class v1, LX/1AK;

    monitor-enter v1

    .line 210283
    :try_start_0
    sget-object v0, LX/1AK;->a:Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210284
    if-eqz v2, :cond_0

    .line 210285
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 210286
    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    invoke-static {v3, p0}, LX/1AL;->a(Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    move-result-object v3

    move-object v0, v3

    .line 210287
    sput-object v0, LX/1AK;->a:Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210288
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210289
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210290
    :cond_1
    sget-object v0, LX/1AK;->a:Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    return-object v0

    .line 210291
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 210293
    invoke-static {p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {v0, v1}, LX/1AL;->a(Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;

    move-result-object v0

    return-object v0
.end method
