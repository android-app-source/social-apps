.class public final enum LX/0uN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0uN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0uN;

.field public static final enum BOOLEAN:LX/0uN;

.field public static final enum FLOAT:LX/0uN;

.field public static final enum INTEGER:LX/0uN;

.field public static final enum STRING:LX/0uN;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156296
    new-instance v0, LX/0uN;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v2}, LX/0uN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uN;->BOOLEAN:LX/0uN;

    .line 156297
    new-instance v0, LX/0uN;

    const-string v1, "INTEGER"

    invoke-direct {v0, v1, v3}, LX/0uN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uN;->INTEGER:LX/0uN;

    .line 156298
    new-instance v0, LX/0uN;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v4}, LX/0uN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uN;->FLOAT:LX/0uN;

    .line 156299
    new-instance v0, LX/0uN;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v5}, LX/0uN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0uN;->STRING:LX/0uN;

    .line 156300
    const/4 v0, 0x4

    new-array v0, v0, [LX/0uN;

    sget-object v1, LX/0uN;->BOOLEAN:LX/0uN;

    aput-object v1, v0, v2

    sget-object v1, LX/0uN;->INTEGER:LX/0uN;

    aput-object v1, v0, v3

    sget-object v1, LX/0uN;->FLOAT:LX/0uN;

    aput-object v1, v0, v4

    sget-object v1, LX/0uN;->STRING:LX/0uN;

    aput-object v1, v0, v5

    sput-object v0, LX/0uN;->$VALUES:[LX/0uN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 156301
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0uN;
    .locals 1

    .prologue
    .line 156295
    const-class v0, LX/0uN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0uN;

    return-object v0
.end method

.method public static values()[LX/0uN;
    .locals 1

    .prologue
    .line 156294
    sget-object v0, LX/0uN;->$VALUES:[LX/0uN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0uN;

    return-object v0
.end method
