.class public LX/1lx;
.super LX/1jx;
.source ""


# instance fields
.field private final a:[LX/1JF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/1JF",
            "<",
            "LX/1mT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([LX/1JF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/1JF",
            "<",
            "LX/1mT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 312710
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 312711
    iput-object p1, p0, LX/1lx;->a:[LX/1JF;

    .line 312712
    return-void
.end method

.method private a(LX/1mT;)V
    .locals 6
    .param p1    # LX/1mT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 312718
    if-eqz p1, :cond_0

    .line 312719
    iget-object v0, p1, LX/1mT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 312720
    if-nez v0, :cond_1

    .line 312721
    :cond_0
    return-void

    .line 312722
    :cond_1
    iget-object v0, p1, LX/1mT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 312723
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 312724
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 312725
    new-instance v4, LX/1mT;

    .line 312726
    iget v5, p1, LX/1mT;->a:I

    move v5, v5

    .line 312727
    invoke-direct {v4, v0, v5}, LX/1mT;-><init>(Lcom/facebook/graphql/model/GraphQLStory;I)V

    invoke-direct {p0, v4}, LX/1lx;->a(LX/1mT;)V

    .line 312728
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 312729
    :cond_2
    new-instance v0, LX/1mT;

    .line 312730
    iget-object v1, p1, LX/1mT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 312731
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 312732
    iget v2, p1, LX/1mT;->a:I

    move v2, v2

    .line 312733
    invoke-direct {v0, v1, v2}, LX/1mT;-><init>(Lcom/facebook/graphql/model/GraphQLStory;I)V

    invoke-direct {p0, v0}, LX/1lx;->a(LX/1mT;)V

    .line 312734
    iget-object v0, p0, LX/1lx;->a:[LX/1JF;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_0

    .line 312735
    iget-object v1, p0, LX/1lx;->a:[LX/1JF;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, LX/1JF;->a(Ljava/lang/Object;)V

    .line 312736
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 1

    .prologue
    .line 312713
    instance-of v0, p1, LX/1mT;

    if-eqz v0, :cond_0

    .line 312714
    check-cast p1, LX/1mT;

    .line 312715
    invoke-direct {p0, p1}, LX/1lx;->a(LX/1mT;)V

    .line 312716
    const/4 v0, 0x0

    .line 312717
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
