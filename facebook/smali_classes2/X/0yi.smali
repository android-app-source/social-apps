.class public final enum LX/0yi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0yi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0yi;

.field public static final enum DIALTONE:LX/0yi;

.field public static final enum NORMAL:LX/0yi;


# instance fields
.field public mBackupRewriteRulesKey:Ljava/lang/String;

.field public mCampaignIdKey:Ljava/lang/String;

.field public mCarrierIdKey:Ljava/lang/String;

.field public mCarrierLogoUrlKey:Ljava/lang/String;

.field public mCarrierNameKey:Ljava/lang/String;

.field public mClearablePreferencesRoot:Ljava/lang/String;

.field public mLastTimeCheckedKey:Ljava/lang/String;

.field public mModeNumber:B

.field public mPoolPricingMapKey:Ljava/lang/String;

.field public mRegistrationStatusKey:Ljava/lang/String;

.field public mRewriteRulesKey:Ljava/lang/String;

.field public mStatusKey:Ljava/lang/String;

.field public mTokenFastHashKey:Ljava/lang/String;

.field public mTokenHashKey:Ljava/lang/String;

.field public mTokenRequestTimeKey:Ljava/lang/String;

.field public mTokenTTLKey:Ljava/lang/String;

.field public mUIFeaturesKey:Ljava/lang/String;

.field public mUnregisteredReasonKey:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 21

    .prologue
    .line 165884
    new-instance v0, LX/0yi;

    const-string v1, "DIALTONE"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "dialtone/clearable/"

    const-string v5, "dialtone/clearable/last_time_checked"

    const-string v6, "dialtone/clearable/current_zero_rating_status"

    const-string v7, "dialtone/clearable/token"

    const-string v8, "dialtone/clearable/ttl"

    const-string v9, "dialtone/clearable/reg_status"

    const-string v10, "dialtone/clearable/carrier_name"

    const-string v11, "dialtone/clearable/carrier_id"

    const-string v12, "dialtone/clearable/carrier_logo_url"

    const-string v13, "dialtone/clearable/enabled_ui_features"

    const-string v14, "dialtone/clearable/rewrite_rules"

    const-string v15, "dialtone/clearable/backup_rewrite_rules"

    const-string v16, "dialtone/clearable/unregistered_reason"

    const-string v17, "dialtone/clearable/token_hash"

    const-string v18, "dialtone/clearable/request_time"

    const-string v19, "dialtone/clearable/fast_hash"

    const-string v20, "pool_pricing_map"

    invoke-direct/range {v0 .. v20}, LX/0yi;-><init>(Ljava/lang/String;IBLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0yi;->DIALTONE:LX/0yi;

    .line 165885
    new-instance v0, LX/0yi;

    const-string v1, "NORMAL"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "zero_rating2/clearable/"

    const-string v5, "zero_rating2/clearable/last_time_checked"

    const-string v6, "zero_rating2/clearable/current_zero_rating_status"

    const-string v7, "zero_rating2/clearable/token"

    const-string v8, "zero_rating2/clearable/ttl"

    const-string v9, "zero_rating2/clearable/reg_status"

    const-string v10, "zero_rating2/clearable/carrier_name"

    const-string v11, "zero_rating2/clearable/carrier_id"

    const-string v12, "zero_rating2/clearable/carrier_logo_url"

    const-string v13, "zero_rating2/clearable/enabled_ui_features"

    const-string v14, "zero_rating2/clearable/rewrite_rules"

    const-string v15, "zero_rating2/clearable/backup_rewrite_rules"

    const-string v16, "zero_rating2/clearable/unregistered_reason"

    const-string v17, "zero_rating2/clearable/token_hash"

    const-string v18, "zero_rating2/clearable/request_time"

    const-string v19, "zero_rating2/clearable/fast_hash"

    const-string v20, "pool_pricing_map"

    invoke-direct/range {v0 .. v20}, LX/0yi;-><init>(Ljava/lang/String;IBLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0yi;->NORMAL:LX/0yi;

    .line 165886
    const/4 v0, 0x2

    new-array v0, v0, [LX/0yi;

    const/4 v1, 0x0

    sget-object v2, LX/0yi;->DIALTONE:LX/0yi;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0yi;->NORMAL:LX/0yi;

    aput-object v2, v0, v1

    sput-object v0, LX/0yi;->$VALUES:[LX/0yi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IBLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 165865
    iput-byte p3, p0, LX/0yi;->mModeNumber:B

    .line 165866
    iput-object p4, p0, LX/0yi;->mClearablePreferencesRoot:Ljava/lang/String;

    .line 165867
    iput-object p5, p0, LX/0yi;->mLastTimeCheckedKey:Ljava/lang/String;

    .line 165868
    iput-object p6, p0, LX/0yi;->mStatusKey:Ljava/lang/String;

    .line 165869
    iput-object p7, p0, LX/0yi;->mCampaignIdKey:Ljava/lang/String;

    .line 165870
    iput-object p8, p0, LX/0yi;->mTokenTTLKey:Ljava/lang/String;

    .line 165871
    iput-object p9, p0, LX/0yi;->mRegistrationStatusKey:Ljava/lang/String;

    .line 165872
    iput-object p10, p0, LX/0yi;->mCarrierNameKey:Ljava/lang/String;

    .line 165873
    iput-object p11, p0, LX/0yi;->mCarrierIdKey:Ljava/lang/String;

    .line 165874
    iput-object p12, p0, LX/0yi;->mCarrierLogoUrlKey:Ljava/lang/String;

    .line 165875
    iput-object p13, p0, LX/0yi;->mUIFeaturesKey:Ljava/lang/String;

    .line 165876
    iput-object p14, p0, LX/0yi;->mRewriteRulesKey:Ljava/lang/String;

    .line 165877
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0yi;->mBackupRewriteRulesKey:Ljava/lang/String;

    .line 165878
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0yi;->mUnregisteredReasonKey:Ljava/lang/String;

    .line 165879
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0yi;->mTokenHashKey:Ljava/lang/String;

    .line 165880
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0yi;->mTokenRequestTimeKey:Ljava/lang/String;

    .line 165881
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0yi;->mTokenFastHashKey:Ljava/lang/String;

    .line 165882
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0yi;->mPoolPricingMapKey:Ljava/lang/String;

    .line 165883
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0yi;
    .locals 1

    .prologue
    .line 165863
    const-class v0, LX/0yi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0yi;

    return-object v0
.end method

.method public static values()[LX/0yi;
    .locals 1

    .prologue
    .line 165862
    sget-object v0, LX/0yi;->$VALUES:[LX/0yi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0yi;

    return-object v0
.end method


# virtual methods
.method public final getBackupRewriteRulesKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165861
    iget-object v0, p0, LX/0yi;->mBackupRewriteRulesKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getCampaignIdKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165860
    iget-object v0, p0, LX/0yi;->mCampaignIdKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getCarrierIdKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165859
    iget-object v0, p0, LX/0yi;->mCarrierIdKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getCarrierLogoUrlKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165858
    iget-object v0, p0, LX/0yi;->mCarrierLogoUrlKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getCarrierNameKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165857
    iget-object v0, p0, LX/0yi;->mCarrierNameKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getClearablePreferencesRoot()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165856
    iget-object v0, p0, LX/0yi;->mClearablePreferencesRoot:Ljava/lang/String;

    return-object v0
.end method

.method public final getLastTimeCheckedKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165887
    iget-object v0, p0, LX/0yi;->mLastTimeCheckedKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getModeNumber()B
    .locals 1

    .prologue
    .line 165855
    iget-byte v0, p0, LX/0yi;->mModeNumber:B

    return v0
.end method

.method public final getPoolPricingMapKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165854
    iget-object v0, p0, LX/0yi;->mPoolPricingMapKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getRegistrationStatusKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165853
    iget-object v0, p0, LX/0yi;->mRegistrationStatusKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getRewriteRulesKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165845
    iget-object v0, p0, LX/0yi;->mRewriteRulesKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getStatusKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165852
    iget-object v0, p0, LX/0yi;->mStatusKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getTokenFastHashKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165851
    iget-object v0, p0, LX/0yi;->mTokenFastHashKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getTokenHashKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165850
    iget-object v0, p0, LX/0yi;->mTokenHashKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getTokenRequestTimeKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165849
    iget-object v0, p0, LX/0yi;->mTokenRequestTimeKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getTokenTTLKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165848
    iget-object v0, p0, LX/0yi;->mTokenTTLKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getUIFeaturesKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165847
    iget-object v0, p0, LX/0yi;->mUIFeaturesKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getUnregisteredReasonKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165846
    iget-object v0, p0, LX/0yi;->mUnregisteredReasonKey:Ljava/lang/String;

    return-object v0
.end method
