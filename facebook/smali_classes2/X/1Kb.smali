.class public LX/1Kb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0bH;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/tooltip/OnlyMeShareDetector$Listener;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Kc;

.field public d:LX/1Zb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0bH;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 232890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232891
    iput-object p1, p0, LX/1Kb;->a:LX/0bH;

    .line 232892
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Kb;->b:Ljava/util/List;

    .line 232893
    new-instance v0, LX/1Kc;

    invoke-direct {v0, p0}, LX/1Kc;-><init>(LX/1Kb;)V

    iput-object v0, p0, LX/1Kb;->c:LX/1Kc;

    .line 232894
    return-void
.end method

.method public static a(LX/0QB;)LX/1Kb;
    .locals 4

    .prologue
    .line 232895
    const-class v1, LX/1Kb;

    monitor-enter v1

    .line 232896
    :try_start_0
    sget-object v0, LX/1Kb;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 232897
    sput-object v2, LX/1Kb;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 232898
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232899
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 232900
    new-instance p0, LX/1Kb;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-direct {p0, v3}, LX/1Kb;-><init>(LX/0bH;)V

    .line 232901
    move-object v0, p0

    .line 232902
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 232903
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Kb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232904
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 232905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()V
    .locals 15

    .prologue
    .line 232906
    iget-object v0, p0, LX/1Kb;->d:LX/1Zb;

    if-nez v0, :cond_0

    .line 232907
    :goto_0
    return-void

    .line 232908
    :cond_0
    iget-object v0, p0, LX/1Kb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1YT;

    .line 232909
    iget-object v2, p0, LX/1Kb;->d:LX/1Zb;

    .line 232910
    iget-object v3, v0, LX/1YT;->f:LX/1Pi;

    .line 232911
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 232912
    iget-object v6, v2, LX/1Zb;->a:Ljava/lang/String;

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 232913
    iget-object v6, v2, LX/1Zb;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 232914
    :cond_1
    iget-object v6, v2, LX/1Zb;->c:Ljava/lang/String;

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 232915
    iget-object v6, v2, LX/1Zb;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 232916
    :cond_2
    iget-object v6, v0, LX/1YT;->d:LX/1YU;

    iget-object v7, v0, LX/1YT;->e:Landroid/content/res/Resources;

    const v8, 0x7f080d35

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v5

    .line 232917
    new-instance v9, LX/CCY;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v6}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v13

    check-cast v13, LX/16H;

    invoke-static {v6}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v14

    check-cast v14, LX/0iA;

    move-object v10, v7

    move-object v11, v5

    invoke-direct/range {v9 .. v14}, LX/CCY;-><init>(Ljava/lang/String;LX/0Rf;LX/03V;LX/16H;LX/0iA;)V

    .line 232918
    move-object v5, v9

    .line 232919
    move-object v4, v5

    .line 232920
    invoke-interface {v3, v4}, LX/1Pi;->a(LX/34p;)V

    .line 232921
    goto :goto_1

    .line 232922
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Kb;->d:LX/1Zb;

    goto :goto_0
.end method
