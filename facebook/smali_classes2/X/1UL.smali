.class public LX/1UL;
.super LX/1UE;
.source ""


# instance fields
.field public final a:LX/198;

.field public b:Z


# direct methods
.method public constructor <init>(LX/1Rq;LX/198;)V
    .locals 1
    .param p1    # LX/1Rq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255788
    invoke-direct {p0, p1}, LX/1UE;-><init>(LX/1Rq;)V

    .line 255789
    iput-object p2, p0, LX/1UL;->a:LX/198;

    .line 255790
    new-instance v0, LX/1UM;

    invoke-direct {v0, p0}, LX/1UM;-><init>(LX/1UL;)V

    invoke-virtual {p0, v0}, LX/1UE;->a(LX/1OD;)V

    .line 255791
    return-void
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 255792
    iget-boolean v0, p0, LX/1UL;->b:Z

    if-eqz v0, :cond_1

    .line 255793
    sget-object v0, LX/1di;->a:LX/1di;

    if-nez v0, :cond_0

    .line 255794
    new-instance v0, LX/1di;

    invoke-direct {v0}, LX/1di;-><init>()V

    sput-object v0, LX/1di;->a:LX/1di;

    .line 255795
    :cond_0
    sget-object v0, LX/1di;->a:LX/1di;

    move-object v0, v0

    .line 255796
    iget-object v1, p0, LX/1UL;->a:LX/198;

    invoke-virtual {v1, v0}, LX/198;->c(LX/1YD;)V

    .line 255797
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1UL;->b:Z

    .line 255798
    :cond_1
    sget-object v0, LX/1dj;->a:LX/1dj;

    if-nez v0, :cond_2

    .line 255799
    new-instance v0, LX/1dj;

    invoke-direct {v0}, LX/1dj;-><init>()V

    sput-object v0, LX/1dj;->a:LX/1dj;

    .line 255800
    :cond_2
    sget-object v0, LX/1dj;->a:LX/1dj;

    move-object v0, v0

    .line 255801
    invoke-virtual {p0, p2}, LX/1UE;->getItemViewType(I)I

    move-result v1

    .line 255802
    iput v1, v0, LX/1dj;->b:I

    .line 255803
    iget-object v1, p0, LX/1UL;->a:LX/198;

    invoke-virtual {v1, v0}, LX/198;->c(LX/1YD;)V

    .line 255804
    invoke-super {p0, p1, p2}, LX/1UE;->a(LX/1a1;I)V

    .line 255805
    return-void
.end method
