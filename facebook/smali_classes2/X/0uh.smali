.class public final enum LX/0uh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0uh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0uh;

.field public static final enum HIGH:LX/0uh;

.field public static final enum LOW:LX/0uh;

.field public static final enum UNKNOWN:LX/0uh;


# instance fields
.field private final mPriority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156846
    new-instance v0, LX/0uh;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, LX/0uh;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0uh;->UNKNOWN:LX/0uh;

    .line 156847
    new-instance v0, LX/0uh;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v3, v3}, LX/0uh;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0uh;->LOW:LX/0uh;

    .line 156848
    new-instance v0, LX/0uh;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4, v4}, LX/0uh;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/0uh;->HIGH:LX/0uh;

    .line 156849
    const/4 v0, 0x3

    new-array v0, v0, [LX/0uh;

    sget-object v1, LX/0uh;->UNKNOWN:LX/0uh;

    aput-object v1, v0, v2

    sget-object v1, LX/0uh;->LOW:LX/0uh;

    aput-object v1, v0, v3

    sget-object v1, LX/0uh;->HIGH:LX/0uh;

    aput-object v1, v0, v4

    sput-object v0, LX/0uh;->$VALUES:[LX/0uh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 156853
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156854
    iput p3, p0, LX/0uh;->mPriority:I

    .line 156855
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0uh;
    .locals 1

    .prologue
    .line 156852
    const-class v0, LX/0uh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0uh;

    return-object v0
.end method

.method public static values()[LX/0uh;
    .locals 1

    .prologue
    .line 156851
    sget-object v0, LX/0uh;->$VALUES:[LX/0uh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0uh;

    return-object v0
.end method


# virtual methods
.method public final isAtLeast(LX/0uh;)Z
    .locals 2

    .prologue
    .line 156850
    iget v0, p0, LX/0uh;->mPriority:I

    iget v1, p1, LX/0uh;->mPriority:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
