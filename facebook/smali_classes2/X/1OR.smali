.class public abstract LX/1OR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public q:LX/1Os;

.field public r:Landroid/support/v7/widget/RecyclerView;

.field public s:LX/25Y;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240939
    iput-boolean v0, p0, LX/1OR;->a:Z

    .line 240940
    iput-boolean v0, p0, LX/1OR;->b:Z

    .line 240941
    return-void
.end method

.method private static a(IIIZ)I
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 240928
    sub-int v2, p0, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 240929
    if-eqz p3, :cond_2

    .line 240930
    if-ltz p2, :cond_1

    .line 240931
    :cond_0
    :goto_0
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    :cond_1
    move v0, v1

    move p2, v1

    .line 240932
    goto :goto_0

    .line 240933
    :cond_2
    if-gez p2, :cond_0

    .line 240934
    const/4 v3, -0x1

    if-ne p2, v3, :cond_3

    move p2, v2

    .line 240935
    goto :goto_0

    .line 240936
    :cond_3
    const/4 v0, -0x2

    if-ne p2, v0, :cond_4

    .line 240937
    const/high16 v0, -0x80000000

    move p2, v2

    goto :goto_0

    :cond_4
    move v0, v1

    move p2, v1

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 240918
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 240919
    if-eqz v0, :cond_0

    .line 240920
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    .line 240921
    invoke-static {v0, p1}, LX/1Os;->e(LX/1Os;I)I

    move-result v1

    .line 240922
    iget-object v2, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v2, v1}, LX/1Ou;->b(I)Landroid/view/View;

    move-result-object v2

    .line 240923
    if-nez v2, :cond_1

    .line 240924
    :cond_0
    :goto_0
    return-void

    .line 240925
    :cond_1
    iget-object p0, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {p0, v1}, LX/1Ov;->c(I)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 240926
    invoke-static {v0, v2}, LX/1Os;->g(LX/1Os;Landroid/view/View;)Z

    .line 240927
    :cond_2
    iget-object v2, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v2, v1}, LX/1Ou;->a(I)V

    goto :goto_0
.end method

.method private a(LX/1Od;ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 240910
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240911
    invoke-virtual {v0}, LX/1a1;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240912
    :goto_0
    return-void

    .line 240913
    :cond_0
    invoke-virtual {v0}, LX/1a1;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/1a1;->o()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->eC_()Z

    move-result v1

    if-nez v1, :cond_1

    .line 240914
    invoke-direct {p0, p2}, LX/1OR;->a(I)V

    .line 240915
    invoke-virtual {p1, v0}, LX/1Od;->a(LX/1a1;)V

    goto :goto_0

    .line 240916
    :cond_1
    invoke-direct {p0, p2}, LX/1OR;->b(I)V

    .line 240917
    invoke-virtual {p1, p3}, LX/1Od;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(LX/1Od;LX/1Ok;LX/3sp;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 240898
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v2}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v2}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240899
    :cond_0
    const/16 v0, 0x2000

    invoke-virtual {p3, v0}, LX/3sp;->a(I)V

    .line 240900
    invoke-virtual {p3, v1}, LX/3sp;->i(Z)V

    .line 240901
    :cond_1
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 240902
    :cond_2
    const/16 v0, 0x1000

    invoke-virtual {p3, v0}, LX/3sp;->a(I)V

    .line 240903
    invoke-virtual {p3, v1}, LX/3sp;->i(Z)V

    .line 240904
    :cond_3
    invoke-virtual {p0, p1, p2}, LX/1OR;->a(LX/1Od;LX/1Ok;)I

    move-result v0

    invoke-virtual {p0, p1, p2}, LX/1OR;->b(LX/1Od;LX/1Ok;)I

    move-result v1

    .line 240905
    const/4 v2, 0x0

    move v2, v2

    .line 240906
    const/4 v3, 0x0

    move v3, v3

    .line 240907
    new-instance p0, LX/3sn;

    sget-object p1, LX/3sp;->a:LX/3sg;

    invoke-interface {p1, v0, v1, v2, v3}, LX/3sg;->a(IIZI)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p0, p1}, LX/3sn;-><init>(Ljava/lang/Object;)V

    move-object v0, p0

    .line 240908
    sget-object v1, LX/3sp;->a:LX/3sg;

    iget-object v2, p3, LX/3sp;->b:Ljava/lang/Object;

    check-cast v0, LX/3sn;

    iget-object v3, v0, LX/3sn;->a:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, LX/3sg;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 240909
    return-void
.end method

.method private a(Landroid/view/View;ILX/1a3;)V
    .locals 2

    .prologue
    .line 240892
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240893
    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240894
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v1, p1}, LX/1Ok;->b(Landroid/view/View;)V

    .line 240895
    :goto_0
    iget-object v1, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v0

    invoke-virtual {v1, p1, p2, p3, v0}, LX/1Os;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 240896
    return-void

    .line 240897
    :cond_0
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v1, p1}, LX/1Ok;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;IZ)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 240864
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 240865
    if-nez p3, :cond_0

    invoke-virtual {v1}, LX/1a1;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240866
    :cond_0
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p1}, LX/1Ok;->b(Landroid/view/View;)V

    .line 240867
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 240868
    invoke-virtual {v1}, LX/1a1;->j()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, LX/1a1;->h()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 240869
    :cond_1
    invoke-virtual {v1}, LX/1a1;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 240870
    invoke-virtual {v1}, LX/1a1;->i()V

    .line 240871
    :goto_1
    iget-object v2, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, p1, p2, v3, v4}, LX/1Os;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 240872
    :cond_2
    :goto_2
    iget-boolean v2, v0, LX/1a3;->d:Z

    if-eqz v2, :cond_3

    .line 240873
    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 240874
    iput-boolean v4, v0, LX/1a3;->d:Z

    .line 240875
    :cond_3
    return-void

    .line 240876
    :cond_4
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {v0, p1}, LX/1Ok;->a(Landroid/view/View;)V

    goto :goto_0

    .line 240877
    :cond_5
    invoke-virtual {v1}, LX/1a1;->k()V

    goto :goto_1

    .line 240878
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    iget-object v3, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-ne v2, v3, :cond_9

    .line 240879
    iget-object v2, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v2, p1}, LX/1Os;->b(Landroid/view/View;)I

    move-result v2

    .line 240880
    if-ne p2, v5, :cond_7

    .line 240881
    iget-object v3, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v3}, LX/1Os;->b()I

    move-result p2

    .line 240882
    :cond_7
    if-ne v2, v5, :cond_8

    .line 240883
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240884
    :cond_8
    if-eq v2, p2, :cond_2

    .line 240885
    iget-object v3, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->p:LX/1OR;

    invoke-direct {v3, v2, p2}, LX/1OR;->d(II)V

    goto :goto_2

    .line 240886
    :cond_9
    iget-object v2, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v2, p1, p2, v4}, LX/1Os;->a(Landroid/view/View;IZ)V

    .line 240887
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/1a3;->c:Z

    .line 240888
    iget-object v2, p0, LX/1OR;->s:LX/25Y;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/1OR;->s:LX/25Y;

    .line 240889
    iget-boolean v3, v2, LX/25Y;->e:Z

    move v2, v3

    .line 240890
    if-eqz v2, :cond_2

    .line 240891
    iget-object v2, p0, LX/1OR;->s:LX/25Y;

    invoke-virtual {v2, p1}, LX/25Y;->a(Landroid/view/View;)V

    goto :goto_2
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 240862
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    invoke-direct {p0, p1}, LX/1OR;->d(I)V

    .line 240863
    return-void
.end method

.method public static b(LX/1OR;LX/25Y;)V
    .locals 1

    .prologue
    .line 240859
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    if-ne v0, p1, :cond_0

    .line 240860
    const/4 v0, 0x0

    iput-object v0, p0, LX/1OR;->s:LX/25Y;

    .line 240861
    :cond_0
    return-void
.end method

.method private b(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 240852
    invoke-static {p1}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v1

    .line 240853
    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 240854
    :cond_0
    :goto_0
    return-void

    .line 240855
    :cond_1
    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, LX/2fO;->a(Z)V

    .line 240856
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-eqz v0, :cond_0

    .line 240857
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->a(I)V

    goto :goto_0

    .line 240858
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 240850
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-direct {p0, p1, p2, v0}, LX/1OR;->a(Landroid/view/View;ILX/1a3;)V

    .line 240851
    return-void
.end method

.method private c(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 240849
    invoke-virtual {p0}, LX/1OR;->s()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 240808
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v0

    return v0
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 240841
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    .line 240842
    invoke-static {v0, p1}, LX/1Os;->e(LX/1Os;I)I

    move-result v1

    .line 240843
    iget-object p0, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {p0, v1}, LX/1Ov;->c(I)Z

    .line 240844
    iget-object p0, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {p0, v1}, LX/1Ou;->c(I)V

    .line 240845
    return-void
.end method

.method private d(II)V
    .locals 3

    .prologue
    .line 240835
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 240836
    if-nez v0, :cond_0

    .line 240837
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot move a child from non-existing index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240838
    :cond_0
    invoke-direct {p0, p1}, LX/1OR;->b(I)V

    .line 240839
    invoke-direct {p0, v0, p2}, LX/1OR;->c(Landroid/view/View;I)V

    .line 240840
    return-void
.end method

.method public static g(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 240833
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 240834
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static h(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 240831
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 240832
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static final i(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 240830
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-static {p0}, LX/1OR;->o(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public static final j(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 240829
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-static {p0}, LX/1OR;->m(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private j(I)Z
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 240815
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_1

    .line 240816
    :cond_0
    :goto_0
    return v1

    .line 240817
    :cond_1
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    move v3, v1

    .line 240818
    :goto_1
    if-nez v3, :cond_2

    if-eqz v0, :cond_0

    .line 240819
    :cond_2
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v3}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    move v1, v2

    .line 240820
    goto :goto_0

    .line 240821
    :sswitch_0
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v4}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 240822
    invoke-virtual {p0}, LX/1OR;->x()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v3

    sub-int/2addr v0, v3

    neg-int v0, v0

    .line 240823
    :goto_2
    iget-object v3, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v4}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 240824
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v3

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v4

    sub-int/2addr v3, v4

    neg-int v3, v3

    move v5, v3

    move v3, v0

    move v0, v5

    goto :goto_1

    .line 240825
    :sswitch_1
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v2}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240826
    invoke-virtual {p0}, LX/1OR;->x()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v3

    sub-int/2addr v0, v3

    .line 240827
    :goto_3
    iget-object v3, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v2}, LX/0vv;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 240828
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v3

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v4

    sub-int/2addr v3, v4

    move v5, v3

    move v3, v0

    move v0, v5

    goto :goto_1

    :cond_3
    move v3, v0

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static final k(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 240814
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-static {p0}, LX/1OR;->p(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static final l(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 240813
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-static {p0}, LX/1OR;->n(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static m(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 240812
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method private static n(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 240811
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method private static o(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 240810
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method private static p(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 240809
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 240946
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()I
    .locals 1

    .prologue
    .line 240945
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 240998
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_1

    .line 240999
    :cond_0
    :goto_0
    return-object v0

    .line 241000
    :cond_1
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 241001
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v2, v1}, LX/1Os;->c(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 241002
    goto :goto_0
.end method

.method public final D()I
    .locals 1

    .prologue
    .line 241003
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    .line 241004
    iget-object p0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, p0

    .line 241005
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    :goto_1
    return v0

    .line 241006
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 241007
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final G()V
    .locals 1

    .prologue
    .line 240989
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    if-eqz v0, :cond_0

    .line 240990
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    invoke-virtual {v0}, LX/25Y;->e()V

    .line 240991
    :cond_0
    return-void
.end method

.method public final H()V
    .locals 1

    .prologue
    .line 241008
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1OR;->a:Z

    .line 241009
    return-void
.end method

.method public a(ILX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 241010
    const/4 v0, 0x0

    return v0
.end method

.method public a(LX/1Od;LX/1Ok;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240760
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-nez v1, :cond_1

    .line 240761
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/1OR;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/1a3;
    .locals 1

    .prologue
    .line 241011
    new-instance v0, LX/1a3;

    invoke-direct {v0, p1, p2}, LX/1a3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup$LayoutParams;)LX/1a3;
    .locals 1

    .prologue
    .line 241012
    instance-of v0, p1, LX/1a3;

    if-eqz v0, :cond_0

    .line 241013
    new-instance v0, LX/1a3;

    check-cast p1, LX/1a3;

    invoke-direct {v0, p1}, LX/1a3;-><init>(LX/1a3;)V

    .line 241014
    :goto_0
    return-object v0

    .line 241015
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 241016
    new-instance v0, LX/1a3;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, LX/1a3;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 241017
    :cond_1
    new-instance v0, LX/1a3;

    invoke-direct {v0, p1}, LX/1a3;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 241018
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 241019
    return-void
.end method

.method public a(ILX/1Od;)V
    .locals 1

    .prologue
    .line 241020
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 241021
    invoke-direct {p0, p1}, LX/1OR;->a(I)V

    .line 241022
    invoke-virtual {p2, v0}, LX/1Od;->a(Landroid/view/View;)V

    .line 241023
    return-void
.end method

.method public a(LX/1OM;LX/1OM;)V
    .locals 0

    .prologue
    .line 241024
    return-void
.end method

.method public a(LX/1Od;)V
    .locals 2

    .prologue
    .line 240992
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 240993
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 240994
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 240995
    invoke-direct {p0, p1, v0, v1}, LX/1OR;->a(LX/1Od;ILandroid/view/View;)V

    .line 240996
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 240997
    :cond_0
    return-void
.end method

.method public a(LX/1Od;LX/1Ok;II)V
    .locals 1

    .prologue
    .line 240986
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    .line 240987
    invoke-static {v0, p3, p4}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;II)V

    .line 240988
    return-void
.end method

.method public a(LX/1Od;LX/1Ok;Landroid/view/View;LX/3sp;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 240979
    invoke-virtual {p0}, LX/1OR;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    .line 240980
    :goto_0
    invoke-virtual {p0}, LX/1OR;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    :goto_1
    move v3, v1

    move v5, v4

    .line 240981
    invoke-static/range {v0 .. v5}, LX/3so;->a(IIIIZZ)LX/3so;

    move-result-object v0

    .line 240982
    invoke-virtual {p4, v0}, LX/3sp;->b(Ljava/lang/Object;)V

    .line 240983
    return-void

    :cond_0
    move v0, v4

    .line 240984
    goto :goto_0

    :cond_1
    move v2, v4

    .line 240985
    goto :goto_1
.end method

.method public final a(LX/25Y;)V
    .locals 2

    .prologue
    .line 240972
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    .line 240973
    iget-boolean v1, v0, LX/25Y;->e:Z

    move v0, v1

    .line 240974
    if-eqz v0, :cond_0

    .line 240975
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    invoke-virtual {v0}, LX/25Y;->e()V

    .line 240976
    :cond_0
    iput-object p1, p0, LX/1OR;->s:LX/25Y;

    .line 240977
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1, p0}, LX/25Y;->a(Landroid/support/v7/widget/RecyclerView;LX/1OR;)V

    .line 240978
    return-void
.end method

.method public final a(LX/3sp;)V
    .locals 2

    .prologue
    .line 240970
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-direct {p0, v0, v1, p1}, LX/1OR;->a(LX/1Od;LX/1Ok;LX/3sp;)V

    .line 240971
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 240969
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240963
    if-nez p1, :cond_0

    .line 240964
    iput-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    .line 240965
    iput-object v0, p0, LX/1OR;->q:LX/1Os;

    .line 240966
    :goto_0
    return-void

    .line 240967
    :cond_0
    iput-object p1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    .line 240968
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->c:LX/1Os;

    iput-object v0, p0, LX/1OR;->q:LX/1Os;

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 240962
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 0
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 240961
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 2

    .prologue
    .line 240959
    const-string v0, "RecyclerView"

    const-string v1, "You must override smoothScrollToPosition to support smooth scrolling"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240960
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 240957
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/1OR;->a(Landroid/view/View;I)V

    .line 240958
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 240955
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/1OR;->a(Landroid/view/View;IZ)V

    .line 240956
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 6

    .prologue
    .line 240947
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 240948
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 240949
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    add-int/2addr v2, p2

    .line 240950
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    add-int/2addr v1, p3

    .line 240951
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v3

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v4

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, LX/1OR;->g()Z

    move-result v5

    invoke-static {v3, v2, v4, v5}, LX/1OR;->a(IIIZ)I

    move-result v2

    .line 240952
    invoke-virtual {p0}, LX/1OR;->x()I

    move-result v3

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v4

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0}, LX/1OR;->h()Z

    move-result v4

    invoke-static {v3, v1, v0, v4}, LX/1OR;->a(IIIZ)I

    move-result v0

    .line 240953
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 240954
    return-void
.end method

.method public a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 240846
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    iget-object v0, v0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 240847
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p2

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int v3, p4, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, p5, v0

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 240848
    return-void
.end method

.method public a(Landroid/view/View;LX/1Od;)V
    .locals 0

    .prologue
    .line 240942
    invoke-virtual {p0, p1}, LX/1OR;->c(Landroid/view/View;)V

    .line 240943
    invoke-virtual {p2, p1}, LX/1Od;->a(Landroid/view/View;)V

    .line 240944
    return-void
.end method

.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    .line 240755
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 240756
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1OR;->q:LX/1Os;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, LX/1Os;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240757
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:LX/1Od;

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    invoke-virtual {p0, v0, v1, p1, p2}, LX/1OR;->a(LX/1Od;LX/1Ok;Landroid/view/View;LX/3sp;)V

    .line 240758
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 240750
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    .line 240751
    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 240752
    :goto_0
    return-void

    .line 240753
    :cond_0
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 240754
    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 240748
    invoke-direct {p0, p1}, LX/1OR;->b(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 240749
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 240745
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240746
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 240747
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 240742
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240743
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 240744
    :cond_0
    return-void
.end method

.method public final a(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 240741
    invoke-direct {p0, p1}, LX/1OR;->j(I)Z

    move-result v0

    return v0
.end method

.method public a(LX/1a3;)Z
    .locals 1

    .prologue
    .line 240740
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 12

    .prologue
    .line 240715
    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v3

    .line 240716
    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v4

    .line 240717
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v1

    sub-int v5, v0, v1

    .line 240718
    invoke-virtual {p0}, LX/1OR;->x()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v1

    sub-int v6, v0, v1

    .line 240719
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p3, Landroid/graphics/Rect;->left:I

    add-int v7, v0, v1

    .line 240720
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p3, Landroid/graphics/Rect;->top:I

    add-int v8, v0, v1

    .line 240721
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int v9, v7, v0

    .line 240722
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int v10, v8, v0

    .line 240723
    const/4 v0, 0x0

    sub-int v1, v7, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 240724
    const/4 v0, 0x0

    sub-int v2, v8, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 240725
    const/4 v0, 0x0

    sub-int v11, v9, v5

    invoke-static {v0, v11}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 240726
    const/4 v11, 0x0

    sub-int v6, v10, v6

    invoke-static {v11, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 240727
    invoke-virtual {p0}, LX/1OR;->t()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 240728
    if-eqz v0, :cond_1

    :goto_0
    move v1, v0

    .line 240729
    :goto_1
    if-eqz v2, :cond_4

    move v0, v2

    .line 240730
    :goto_2
    if-nez v1, :cond_0

    if-eqz v0, :cond_6

    .line 240731
    :cond_0
    if-eqz p4, :cond_5

    .line 240732
    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 240733
    :goto_3
    const/4 v0, 0x1

    .line 240734
    :goto_4
    return v0

    .line 240735
    :cond_1
    sub-int v0, v9, v5

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 240736
    :cond_2
    if-eqz v1, :cond_3

    move v0, v1

    :goto_5
    move v1, v0

    goto :goto_1

    :cond_3
    sub-int v1, v7, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_5

    .line 240737
    :cond_4
    sub-int v0, v8, v4

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    .line 240738
    :cond_5
    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    goto :goto_3

    .line 240739
    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 240714
    invoke-direct {p0, p1}, LX/1OR;->c(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 240712
    const/4 v0, 0x0

    move v0, v0

    .line 240713
    return v0
.end method

.method public b(ILX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 240711
    const/4 v0, 0x0

    return v0
.end method

.method public b(LX/1Od;LX/1Ok;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240709
    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    if-nez v1, :cond_1

    .line 240710
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/1OR;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    goto :goto_0
.end method

.method public b(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240708
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b()LX/1a3;
.end method

.method public b(II)V
    .locals 0

    .prologue
    .line 240707
    return-void
.end method

.method public final b(LX/1Od;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 240690
    invoke-virtual {p1}, LX/1Od;->c()I

    move-result v1

    .line 240691
    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_3

    .line 240692
    invoke-virtual {p1, v0}, LX/1Od;->d(I)Landroid/view/View;

    move-result-object v2

    .line 240693
    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v3

    .line 240694
    invoke-virtual {v3}, LX/1a1;->c()Z

    move-result v4

    if-nez v4, :cond_2

    .line 240695
    invoke-virtual {v3, v5}, LX/1a1;->a(Z)V

    .line 240696
    invoke-virtual {v3}, LX/1a1;->r()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 240697
    iget-object v4, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v2, v5}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 240698
    :cond_0
    iget-object v4, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    if-eqz v4, :cond_1

    .line 240699
    iget-object v4, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    invoke-virtual {v4, v3}, LX/1Of;->c(LX/1a1;)V

    .line 240700
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1a1;->a(Z)V

    .line 240701
    invoke-virtual {p1, v2}, LX/1Od;->b(Landroid/view/View;)V

    .line 240702
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 240703
    :cond_3
    invoke-virtual {p1}, LX/1Od;->d()V

    .line 240704
    if-lez v1, :cond_4

    .line 240705
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 240706
    :cond_4
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 240688
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1OR;->b:Z

    .line 240689
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 1

    .prologue
    .line 240685
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1OR;->b:Z

    .line 240686
    invoke-virtual {p0, p1, p2}, LX/1OR;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 240687
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 240683
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/1OR;->b(Landroid/view/View;I)V

    .line 240684
    return-void
.end method

.method public b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 240681
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1OR;->a(Landroid/view/View;IZ)V

    .line 240682
    return-void
.end method

.method public final b(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 240672
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240673
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    .line 240674
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240671
    const/4 v0, 0x0

    return v0
.end method

.method public c(I)Landroid/view/View;
    .locals 6

    .prologue
    .line 240660
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v2

    .line 240661
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 240662
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 240663
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v3

    .line 240664
    if-eqz v3, :cond_1

    .line 240665
    invoke-virtual {v3}, LX/1a1;->d()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {v3}, LX/1a1;->c()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->e:LX/1Ok;

    .line 240666
    iget-boolean v5, v4, LX/1Ok;->k:Z

    move v4, v5

    .line 240667
    if-nez v4, :cond_0

    invoke-virtual {v3}, LX/1a1;->q()Z

    move-result v3

    if-nez v3, :cond_1

    .line 240668
    :cond_0
    :goto_1
    return-object v0

    .line 240669
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240670
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(ILX/1Od;LX/1Ok;)Landroid/view/View;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 240659
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(II)V
    .locals 0

    .prologue
    .line 240658
    return-void
.end method

.method public final c(LX/1Od;)V
    .locals 2

    .prologue
    .line 240675
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 240676
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 240677
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)LX/1a1;

    move-result-object v1

    invoke-virtual {v1}, LX/1a1;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 240678
    invoke-virtual {p0, v0, p1}, LX/1OR;->a(ILX/1Od;)V

    .line 240679
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 240680
    :cond_1
    return-void
.end method

.method public c(LX/1Od;LX/1Ok;)V
    .locals 2

    .prologue
    .line 240780
    const-string v0, "RecyclerView"

    const-string v1, "You must override onLayoutChildren(Recycler recycler, State state) "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240781
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 240801
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    .line 240802
    iget-object v1, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {v1, p1}, LX/1Ou;->a(Landroid/view/View;)I

    move-result v1

    .line 240803
    if-gez v1, :cond_0

    .line 240804
    :goto_0
    return-void

    .line 240805
    :cond_0
    iget-object p0, v0, LX/1Os;->b:LX/1Ov;

    invoke-virtual {p0, v1}, LX/1Ov;->c(I)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 240806
    invoke-static {v0, p1}, LX/1Os;->g(LX/1Os;Landroid/view/View;)Z

    .line 240807
    :cond_1
    iget-object p0, v0, LX/1Os;->a:LX/1Ou;

    invoke-interface {p0, v1}, LX/1Ou;->a(I)V

    goto :goto_0
.end method

.method public d(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240800
    const/4 v0, 0x0

    return v0
.end method

.method public e(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240799
    const/4 v0, 0x0

    return v0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 240798
    return-void
.end method

.method public final e(II)V
    .locals 1

    .prologue
    .line 240796
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;II)V

    .line 240797
    return-void
.end method

.method public final e(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 240792
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v0, p1}, LX/1Os;->b(Landroid/view/View;)I

    move-result v0

    .line 240793
    if-ltz v0, :cond_0

    .line 240794
    invoke-direct {p0, v0}, LX/1OR;->d(I)V

    .line 240795
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 240791
    const/4 v0, 0x0

    return v0
.end method

.method public f(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240790
    const/4 v0, 0x0

    return v0
.end method

.method public f()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 240789
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 240788
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v0, p1}, LX/1Os;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 240786
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, LX/1OR;->c(Landroid/view/View;I)V

    .line 240787
    return-void
.end method

.method public g(LX/1Ok;)I
    .locals 1

    .prologue
    .line 240785
    const/4 v0, 0x0

    return v0
.end method

.method public g(I)V
    .locals 1

    .prologue
    .line 240782
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240783
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    .line 240784
    :cond_0
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 240759
    const/4 v0, 0x0

    return v0
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 240777
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240778
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->d(I)V

    .line 240779
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 240776
    const/4 v0, 0x0

    return v0
.end method

.method public i(I)V
    .locals 0

    .prologue
    .line 240775
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 240772
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 240773
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 240774
    :cond_0
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 240771
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 240768
    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->s:LX/25Y;

    .line 240769
    iget-boolean p0, v0, LX/25Y;->e:Z

    move v0, p0

    .line 240770
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 240767
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public final v()I
    .locals 1

    .prologue
    .line 240766
    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->q:LX/1Os;

    invoke-virtual {v0}, LX/1Os;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 240765
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 240764
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 240763
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()I
    .locals 1

    .prologue
    .line 240762
    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1OR;->r:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
