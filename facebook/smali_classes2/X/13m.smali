.class public LX/13m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/13m;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177441
    return-void
.end method

.method public static a(III)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177442
    if-gt p1, p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Min:%s is larger than max:%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 177443
    if-ge p0, p1, :cond_1

    .line 177444
    :goto_1
    return p1

    :cond_0
    move v0, v2

    .line 177445
    goto :goto_0

    .line 177446
    :cond_1
    if-le p0, p2, :cond_2

    move p1, p2

    .line 177447
    goto :goto_1

    :cond_2
    move p1, p0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/13m;
    .locals 3

    .prologue
    .line 177448
    sget-object v0, LX/13m;->a:LX/13m;

    if-nez v0, :cond_1

    .line 177449
    const-class v1, LX/13m;

    monitor-enter v1

    .line 177450
    :try_start_0
    sget-object v0, LX/13m;->a:LX/13m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177451
    if-eqz v2, :cond_0

    .line 177452
    :try_start_1
    new-instance v0, LX/13m;

    invoke-direct {v0}, LX/13m;-><init>()V

    .line 177453
    move-object v0, v0

    .line 177454
    sput-object v0, LX/13m;->a:LX/13m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177455
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177456
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177457
    :cond_1
    sget-object v0, LX/13m;->a:LX/13m;

    return-object v0

    .line 177458
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 177460
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 177461
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177462
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 177463
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 177464
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 177465
    :cond_1
    return-void
.end method
