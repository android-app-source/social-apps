.class public LX/0qR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0qR;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/0qR;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/14t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147570
    const-class v0, LX/0qR;

    sput-object v0, LX/0qR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147627
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0qR;->b:Ljava/util/Map;

    .line 147628
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    .line 147629
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0qR;->d:Ljava/util/Map;

    .line 147630
    return-void
.end method

.method public static a(LX/0QB;)LX/0qR;
    .locals 3

    .prologue
    .line 147614
    sget-object v0, LX/0qR;->e:LX/0qR;

    if-nez v0, :cond_1

    .line 147615
    const-class v1, LX/0qR;

    monitor-enter v1

    .line 147616
    :try_start_0
    sget-object v0, LX/0qR;->e:LX/0qR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147617
    if-eqz v2, :cond_0

    .line 147618
    :try_start_1
    new-instance v0, LX/0qR;

    invoke-direct {v0}, LX/0qR;-><init>()V

    .line 147619
    move-object v0, v0

    .line 147620
    sput-object v0, LX/0qR;->e:LX/0qR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147623
    :cond_1
    sget-object v0, LX/0qR;->e:LX/0qR;

    return-object v0

    .line 147624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;LX/14t;)V
    .locals 4

    .prologue
    .line 147596
    monitor-enter p0

    .line 147597
    :try_start_0
    iget-object v0, p2, LX/14t;->a:Ljava/util/List;

    move-object v0, v0

    .line 147598
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16v;

    .line 147599
    iget-object v2, p0, LX/0qR;->c:Ljava/util/Map;

    .line 147600
    iget-object v3, v0, LX/16v;->b:Ljava/lang/String;

    move-object v0, v3

    .line 147601
    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147602
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147603
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0qR;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 147604
    iget-object v0, p0, LX/0qR;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147605
    :goto_1
    monitor-exit p0

    return-void

    .line 147606
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/0qR;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14t;

    .line 147607
    iget-object v1, p2, LX/14t;->a:Ljava/util/List;

    move-object v1, v1

    .line 147608
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/16v;

    .line 147609
    iget-object v3, v1, LX/16v;->c:Ljava/lang/String;

    move-object v3, v3

    .line 147610
    iget-object p1, v1, LX/16v;->b:Ljava/lang/String;

    move-object p1, p1

    .line 147611
    iget p2, v1, LX/16v;->a:I

    move v1, p2

    .line 147612
    invoke-virtual {v0, v3, p1, v1}, LX/14t;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147613
    :cond_2
    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147592
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 147593
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 147594
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0qR;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147595
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 147588
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147589
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147590
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 147591
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147584
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qR;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 147585
    const/4 v0, 0x0

    .line 147586
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0qR;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147587
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 147577
    monitor-enter p0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 147578
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 147579
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 147580
    iget-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147582
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/0qR;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147583
    iget-object v1, p0, LX/0qR;->c:Ljava/util/Map;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)LX/14t;
    .locals 2

    .prologue
    .line 147571
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qR;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14t;

    .line 147572
    if-nez v0, :cond_0

    .line 147573
    new-instance v0, LX/14t;

    invoke-direct {v0}, LX/14t;-><init>()V

    .line 147574
    iget-object v1, p0, LX/0qR;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147575
    :cond_0
    monitor-exit p0

    return-object v0

    .line 147576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
