.class public final enum LX/1lC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1lC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1lC;

.field public static final enum HORIZONTAL:LX/1lC;

.field public static final enum VERTICAL:LX/1lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 311380
    new-instance v0, LX/1lC;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v2}, LX/1lC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lC;->HORIZONTAL:LX/1lC;

    .line 311381
    new-instance v0, LX/1lC;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v3}, LX/1lC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lC;->VERTICAL:LX/1lC;

    .line 311382
    const/4 v0, 0x2

    new-array v0, v0, [LX/1lC;

    sget-object v1, LX/1lC;->HORIZONTAL:LX/1lC;

    aput-object v1, v0, v2

    sget-object v1, LX/1lC;->VERTICAL:LX/1lC;

    aput-object v1, v0, v3

    sput-object v0, LX/1lC;->$VALUES:[LX/1lC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 311383
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1lC;
    .locals 1

    .prologue
    .line 311384
    const-class v0, LX/1lC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1lC;

    return-object v0
.end method

.method public static values()[LX/1lC;
    .locals 1

    .prologue
    .line 311385
    sget-object v0, LX/1lC;->$VALUES:[LX/1lC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1lC;

    return-object v0
.end method
