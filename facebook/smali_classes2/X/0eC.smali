.class public LX/0eC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 91566
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "language_switcher/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 91567
    sput-object v0, LX/0eC;->e:LX/0Tn;

    const-string v1, "account_locale"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eC;->a:LX/0Tn;

    .line 91568
    sget-object v0, LX/0eC;->e:LX/0Tn;

    const-string v1, "application_locale"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eC;->b:LX/0Tn;

    .line 91569
    sget-object v0, LX/0eC;->e:LX/0Tn;

    const-string v1, "locale_last_time_synced"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eC;->c:LX/0Tn;

    .line 91570
    sget-object v0, LX/0eC;->e:LX/0Tn;

    const-string v1, "internal_settings/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0eC;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 91571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91572
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91573
    sget-object v0, LX/0eC;->a:LX/0Tn;

    sget-object v1, LX/0eC;->c:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
