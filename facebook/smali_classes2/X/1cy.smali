.class public LX/1cy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/3Ep;",
            ">;"
        }
    .end annotation
.end field

.field private static B:LX/1oD;

.field private static C:Ljava/lang/Boolean;

.field public static final a:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "LX/1cz;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Z

.field private static final c:Ljava/lang/Object;

.field public static final d:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dN;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Dg;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1mn;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dJ;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/Context;",
            "Landroid/util/SparseArray",
            "<",
            "LX/34V;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final j:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dK;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dW;",
            ">;"
        }
    .end annotation
.end field

.field public static l:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dP;",
            ">;"
        }
    .end annotation
.end field

.field public static m:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dO;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dX;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1dL;",
            ">;"
        }
    .end annotation
.end field

.field private static final p:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1np",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final q:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1ml;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3lz",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final s:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1me;",
            ">;"
        }
    .end annotation
.end field

.field private static final t:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1mg;",
            ">;"
        }
    .end annotation
.end field

.field private static final u:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final v:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/0YU",
            "<",
            "LX/1cu;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final w:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private static final x:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private static final y:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/1mz;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "LX/1my;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/16 v2, 0x100

    const/16 v1, 0x40

    .line 283812
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1cy;->c:Ljava/lang/Object;

    .line 283813
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->d:LX/0Zi;

    .line 283814
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v2}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->e:LX/0Zi;

    .line 283815
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v2}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->f:LX/0Zi;

    .line 283816
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v2}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->g:LX/0Zi;

    .line 283817
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->h:LX/0Zi;

    .line 283818
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    sput-object v0, LX/1cy;->i:Ljava/util/Map;

    .line 283819
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v2}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->j:LX/0Zi;

    .line 283820
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->k:LX/0Zi;

    .line 283821
    sput-object v5, LX/1cy;->l:LX/0Zi;

    .line 283822
    sput-object v5, LX/1cy;->m:LX/0Zi;

    .line 283823
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->n:LX/0Zi;

    .line 283824
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->o:LX/0Zi;

    .line 283825
    new-instance v0, LX/0Zi;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->p:LX/0Zi;

    .line 283826
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v2}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->q:LX/0Zi;

    .line 283827
    new-instance v0, LX/0Zi;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->r:LX/0Zi;

    .line 283828
    new-instance v0, LX/0Zi;

    invoke-direct {v0, v3}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->s:LX/0Zi;

    .line 283829
    new-instance v0, LX/0Zi;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->t:LX/0Zi;

    .line 283830
    new-instance v0, LX/0Zj;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->u:LX/0Zj;

    .line 283831
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v4}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->v:LX/0Zj;

    .line 283832
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v4}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->w:LX/0Zj;

    .line 283833
    new-instance v0, LX/0Zj;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->x:LX/0Zj;

    .line 283834
    new-instance v0, LX/0Zj;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->y:LX/0Zj;

    .line 283835
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v3}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->z:LX/0Zj;

    .line 283836
    new-instance v0, LX/0Zj;

    invoke-direct {v0, v3}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->A:LX/0Zj;

    .line 283837
    new-instance v0, LX/0Zj;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0Zj;-><init>(I)V

    sput-object v0, LX/1cy;->a:LX/0Zk;

    .line 283838
    sput-object v5, LX/1cy;->C:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 283839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283840
    return-void
.end method

.method public static declared-synchronized a(LX/1De;Landroid/content/res/Resources;)LX/1Dg;
    .locals 3

    .prologue
    .line 283841
    const-class v1, LX/1cy;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1cy;->e:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Dg;

    .line 283842
    if-nez v0, :cond_0

    .line 283843
    new-instance v0, LX/1Dg;

    invoke-direct {v0}, LX/1Dg;-><init>()V

    .line 283844
    :cond_0
    invoke-static {}, LX/1cy;->p()LX/1mn;

    move-result-object v2

    invoke-virtual {v0, v2, p0, p1}, LX/1Dg;->a(LX/1mn;LX/1De;Landroid/content/res/Resources;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283845
    monitor-exit v1

    return-object v0

    .line 283846
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1X1;LX/1cn;Ljava/lang/Object;)LX/1dJ;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            ")",
            "LX/1dJ;"
        }
    .end annotation

    .prologue
    .line 283847
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static/range {v0 .. v24}, LX/1cy;->a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;IIILandroid/graphics/Rect;LX/1dc;LX/1dc;Landroid/graphics/Rect;Ljava/lang/String;)LX/1dJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dL;Landroid/graphics/Rect;)LX/1dJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "LX/1dL;",
            "Landroid/graphics/Rect;",
            ")",
            "LX/1dJ;"
        }
    .end annotation

    .prologue
    .line 283848
    sget-object v0, LX/1cy;->h:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dJ;

    .line 283849
    if-nez v0, :cond_0

    .line 283850
    new-instance v0, LX/1dJ;

    invoke-direct {v0}, LX/1dJ;-><init>()V

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 283851
    invoke-virtual/range {v0 .. v5}, LX/1dJ;->a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dL;Landroid/graphics/Rect;)V

    .line 283852
    return-object v0
.end method

.method private static a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;IIILandroid/graphics/Rect;LX/1dc;LX/1dc;Landroid/graphics/Rect;Ljava/lang/String;)LX/1dJ;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Object;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;III",
            "Landroid/graphics/Rect;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/graphics/Rect;",
            "Ljava/lang/String;",
            ")",
            "LX/1dJ;"
        }
    .end annotation

    .prologue
    .line 283853
    sget-object v0, LX/1cy;->h:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dJ;

    .line 283854
    if-nez v0, :cond_0

    .line 283855
    new-instance v0, LX/1dJ;

    invoke-direct {v0}, LX/1dJ;-><init>()V

    :cond_0
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move/from16 v18, p17

    move/from16 v19, p18

    move/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    .line 283856
    invoke-virtual/range {v0 .. v25}, LX/1dJ;->a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;IIILandroid/graphics/Rect;LX/1dc;LX/1dc;Landroid/graphics/Rect;Ljava/lang/String;)V

    .line 283857
    return-object v0
.end method

.method public static a()LX/1dK;
    .locals 1

    .prologue
    .line 283917
    sget-object v0, LX/1cy;->j:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 283918
    if-nez v0, :cond_0

    .line 283919
    new-instance v0, LX/1dK;

    invoke-direct {v0}, LX/1dK;-><init>()V

    .line 283920
    :cond_0
    return-object v0
.end method

.method public static a(LX/1De;LX/1X1;)LX/1me;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)",
            "LX/1me;"
        }
    .end annotation

    .prologue
    .line 283762
    sget-object v0, LX/1cy;->s:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1me;

    .line 283763
    if-nez v0, :cond_0

    .line 283764
    new-instance v0, LX/1me;

    invoke-direct {v0}, LX/1me;-><init>()V

    .line 283765
    :cond_0
    iput-object p0, v0, LX/1me;->a:LX/1De;

    .line 283766
    iput-object p1, v0, LX/1me;->b:LX/1X1;

    .line 283767
    return-object v0
.end method

.method public static a(LX/1mg;)LX/1mg;
    .locals 6

    .prologue
    .line 283858
    sget-object v0, LX/1cy;->t:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mg;

    .line 283859
    if-nez v0, :cond_0

    .line 283860
    new-instance v0, LX/1mg;

    invoke-direct {v0}, LX/1mg;-><init>()V

    .line 283861
    :cond_0
    if-nez p0, :cond_1

    .line 283862
    :goto_0
    return-object v0

    .line 283863
    :cond_1
    iget-object v1, p0, LX/1mg;->b:Ljava/util/Map;

    move-object v1, v1

    .line 283864
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 283865
    iget-object v5, v0, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {v3}, LX/1mg;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 283866
    :cond_2
    iget-object v1, p0, LX/1mg;->c:Ljava/util/Map;

    move-object v1, v1

    .line 283867
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 283868
    iget-object v4, v0, LX/1mg;->c:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 283869
    :cond_3
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 283870
    instance-of v0, p0, LX/1De;

    if-eqz v0, :cond_0

    .line 283871
    check-cast p0, LX/1De;

    invoke-virtual {p0}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 283872
    instance-of v0, p0, LX/1De;

    if-eqz v0, :cond_0

    .line 283873
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Double wrapped ComponentContext."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283874
    :cond_0
    sget-object v2, LX/1cy;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 283875
    :try_start_0
    sget-object v0, LX/1cy;->B:LX/1oD;

    if-nez v0, :cond_2

    sget-boolean v0, LX/1cy;->b:Z

    if-nez v0, :cond_2

    .line 283876
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v0, v3, :cond_1

    .line 283877
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Activity callbacks must be invoked manually below ICS (API level 14)"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283878
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 283879
    :cond_1
    :try_start_1
    new-instance v0, LX/1oD;

    invoke-direct {v0}, LX/1oD;-><init>()V

    sput-object v0, LX/1cy;->B:LX/1oD;

    .line 283880
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    sget-object v3, LX/1cy;->B:LX/1oD;

    invoke-virtual {v0, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 283881
    :cond_2
    sget-object v0, LX/1cy;->i:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 283882
    if-nez v0, :cond_3

    .line 283883
    sget-object v0, LX/1cy;->i:Ljava/util/Map;

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    invoke-interface {v0, p0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283884
    monitor-exit v2

    move-object v0, v1

    .line 283885
    :goto_0
    return-object v0

    .line 283886
    :cond_3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zi;

    .line 283887
    if-nez v0, :cond_4

    .line 283888
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 283889
    :cond_4
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283890
    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0YU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283891
    sget-object v0, LX/1cy;->u:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283892
    return-void
.end method

.method public static a(LX/1dX;)V
    .locals 1

    .prologue
    .line 283893
    const/4 v0, 0x0

    iput v0, p0, LX/1dX;->a:I

    .line 283894
    const/4 v0, 0x0

    iput-object v0, p0, LX/1dX;->b:LX/1dQ;

    .line 283895
    sget-object v0, LX/1cy;->n:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283896
    return-void
.end method

.method public static a(LX/1ml;)V
    .locals 3

    .prologue
    .line 283897
    const/4 v2, -0x1

    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, 0x0

    .line 283898
    iput-object v0, p0, LX/1ml;->f:LX/1X1;

    .line 283899
    iput-object v0, p0, LX/1ml;->a:LX/1dK;

    .line 283900
    iput-object v0, p0, LX/1ml;->b:LX/1dK;

    .line 283901
    iput-object v0, p0, LX/1ml;->c:LX/1dK;

    .line 283902
    iput-object v0, p0, LX/1ml;->d:LX/1dK;

    .line 283903
    iput-object v0, p0, LX/1ml;->e:LX/1dW;

    .line 283904
    iput v1, p0, LX/1ml;->g:F

    .line 283905
    iput v1, p0, LX/1ml;->h:F

    .line 283906
    iput v2, p0, LX/1ml;->i:I

    .line 283907
    iput v2, p0, LX/1ml;->j:I

    .line 283908
    const/4 v0, 0x0

    iget-object v1, p0, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 283909
    iget-object v0, p0, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ml;

    invoke-static {v0}, LX/1cy;->a(LX/1ml;)V

    .line 283910
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 283911
    :cond_0
    iget-object v0, p0, LX/1ml;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 283912
    sget-object v0, LX/1cy;->q:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283913
    return-void
.end method

.method public static a(LX/1mz;)V
    .locals 1

    .prologue
    .line 283914
    invoke-virtual {p0}, LX/1mz;->a()V

    .line 283915
    sget-object v0, LX/1cy;->y:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283916
    return-void
.end method

.method public static a(LX/1np;)V
    .locals 1

    .prologue
    .line 283802
    const/4 v0, 0x0

    iput-object v0, p0, LX/1np;->a:Ljava/lang/Object;

    .line 283803
    sget-object v0, LX/1cy;->p:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283804
    return-void
.end method

.method public static a(LX/3Ep;)V
    .locals 1

    .prologue
    .line 283805
    iget-object v0, p0, LX/3Ep;->d:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    .line 283806
    iget-object v0, p0, LX/3Ep;->d:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 283807
    iget-object v0, p0, LX/3Ep;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 283808
    iget-object v0, p0, LX/3Ep;->b:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 283809
    iget-object v0, p0, LX/3Ep;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 283810
    sget-object v0, LX/1cy;->A:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283811
    return-void
.end method

.method public static a(LX/3lz;)V
    .locals 1

    .prologue
    .line 283722
    const/4 v0, 0x0

    .line 283723
    iput-object v0, p0, LX/3lz;->a:Ljava/lang/Object;

    .line 283724
    iput-object v0, p0, LX/3lz;->b:Ljava/lang/Object;

    .line 283725
    sget-object v0, LX/1cy;->r:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283726
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 283727
    sget-object v0, LX/1cy;->i:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283728
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The MountContentPools has a reference to an activitythat has just been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283729
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;LX/1S3;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 283730
    instance-of v0, p0, LX/1De;

    if-eqz v0, :cond_0

    .line 283731
    check-cast p0, LX/1De;

    invoke-virtual {p0}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 283732
    instance-of v0, p0, LX/1De;

    if-eqz v0, :cond_0

    .line 283733
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Double wrapped ComponentContext."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283734
    :cond_0
    const/4 v1, 0x0

    .line 283735
    sget-object v2, LX/1cy;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 283736
    :try_start_0
    sget-object v0, LX/1cy;->i:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 283737
    if-eqz v0, :cond_1

    .line 283738
    iget v1, p1, LX/1S3;->d:I

    move v1, v1

    .line 283739
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/34V;

    .line 283740
    if-nez v1, :cond_1

    .line 283741
    new-instance v1, LX/34V;

    invoke-virtual {p1}, LX/1S3;->n()I

    move-result v3

    invoke-direct {v1, v3}, LX/34V;-><init>(I)V

    .line 283742
    iget v3, p1, LX/1S3;->d:I

    move v3, v3

    .line 283743
    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 283744
    :cond_1
    if-eqz v1, :cond_2

    .line 283745
    invoke-virtual {v1, p2}, LX/34V;->a(Ljava/lang/Object;)Z

    .line 283746
    :cond_2
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 283747
    invoke-virtual {p0}, Landroid/graphics/Rect;->setEmpty()V

    .line 283748
    sget-object v0, LX/1cy;->x:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283749
    return-void
.end method

.method public static a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 283750
    invoke-virtual {p0}, Landroid/graphics/RectF;->setEmpty()V

    .line 283751
    sget-object v0, LX/1cy;->w:LX/0Zj;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283752
    return-void
.end method

.method public static b()LX/1dL;
    .locals 1

    .prologue
    .line 283753
    sget-object v0, LX/1cy;->o:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dL;

    .line 283754
    if-nez v0, :cond_0

    .line 283755
    new-instance v0, LX/1dL;

    invoke-direct {v0}, LX/1dL;-><init>()V

    .line 283756
    :cond_0
    return-object v0
.end method

.method public static b(LX/1mg;)V
    .locals 1

    .prologue
    .line 283757
    iget-object v0, p0, LX/1mg;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 283758
    iget-object v0, p0, LX/1mg;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 283759
    iget-object v0, p0, LX/1mg;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 283760
    sget-object v0, LX/1cy;->t:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 283761
    return-void
.end method

.method public static f()LX/1np;
    .locals 1

    .prologue
    .line 283768
    sget-object v0, LX/1cy;->p:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1np;

    .line 283769
    if-nez v0, :cond_0

    .line 283770
    new-instance v0, LX/1np;

    invoke-direct {v0}, LX/1np;-><init>()V

    .line 283771
    :cond_0
    return-object v0
.end method

.method public static i()LX/1my;
    .locals 1

    .prologue
    .line 283772
    sget-object v0, LX/1cy;->z:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1my;

    .line 283773
    if-nez v0, :cond_0

    .line 283774
    new-instance v0, LX/1my;

    invoke-direct {v0}, LX/1my;-><init>()V

    .line 283775
    :cond_0
    return-object v0
.end method

.method public static k()LX/0YU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283776
    sget-object v0, LX/1cy;->u:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YU;

    .line 283777
    if-nez v0, :cond_0

    .line 283778
    new-instance v0, LX/0YU;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/0YU;-><init>(I)V

    .line 283779
    :cond_0
    return-object v0
.end method

.method public static m()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 283780
    sget-object v0, LX/1cy;->w:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    .line 283781
    if-nez v0, :cond_0

    .line 283782
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 283783
    :cond_0
    return-object v0
.end method

.method public static n()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 283784
    sget-object v0, LX/1cy;->x:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 283785
    if-nez v0, :cond_0

    .line 283786
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 283787
    :cond_0
    return-object v0
.end method

.method public static o()LX/1mz;
    .locals 1

    .prologue
    .line 283788
    sget-object v0, LX/1cy;->y:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mz;

    .line 283789
    if-nez v0, :cond_0

    .line 283790
    new-instance v0, LX/1mz;

    invoke-direct {v0}, LX/1mz;-><init>()V

    .line 283791
    :cond_0
    return-object v0
.end method

.method private static declared-synchronized p()LX/1mn;
    .locals 3

    .prologue
    .line 283792
    const-class v1, LX/1cy;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1cy;->f:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mn;

    .line 283793
    if-nez v0, :cond_1

    .line 283794
    sget-object v0, LX/1cy;->C:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 283795
    sget-boolean v0, LX/1V5;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/1cy;->C:Ljava/lang/Boolean;

    .line 283796
    :cond_0
    sget-object v0, LX/1cy;->C:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283797
    sget-object v0, Lcom/facebook/csslayout/YogaExperimentalFeature;->ROUNDING:Lcom/facebook/csslayout/YogaExperimentalFeature;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/facebook/csslayout/YogaNode;->a(Lcom/facebook/csslayout/YogaExperimentalFeature;Z)V

    .line 283798
    new-instance v0, Lcom/facebook/csslayout/YogaNode;

    invoke-direct {v0}, Lcom/facebook/csslayout/YogaNode;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283799
    :cond_1
    :goto_0
    monitor-exit v1

    return-object v0

    .line 283800
    :cond_2
    :try_start_1
    new-instance v0, LX/48q;

    invoke-direct {v0}, LX/48q;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 283801
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
