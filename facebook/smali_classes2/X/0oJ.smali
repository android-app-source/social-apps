.class public LX/0oJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 140130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140131
    iput-object p1, p0, LX/0oJ;->a:LX/0ad;

    .line 140132
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140133
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ","

    invoke-direct {v0, p0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 140134
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 140135
    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140136
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 140137
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/0oJ;
    .locals 1

    .prologue
    .line 140138
    invoke-static {p0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0oJ;CCLjava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 140139
    iget-object v0, p0, LX/0oJ;->a:LX/0ad;

    invoke-interface {v0, p1, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0oJ;->a:LX/0ad;

    invoke-interface {v0, p2, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0oJ;SSZ)Z
    .locals 1

    .prologue
    .line 140140
    iget-object v0, p0, LX/0oJ;->a:LX/0ad;

    invoke-interface {v0, p1, p3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0oJ;->a:LX/0ad;

    invoke-interface {v0, p2, p3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0oJ;
    .locals 2

    .prologue
    .line 140141
    new-instance v1, LX/0oJ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/0oJ;-><init>(LX/0ad;)V

    .line 140142
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 140143
    sget-short v0, LX/0ob;->B:S

    sget-short v1, LX/0ob;->p:S

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 140144
    sget-char v0, LX/0ob;->D:C

    sget-char v1, LX/0ob;->r:C

    const-string v2, "camera"

    const-string v3, "camera"

    invoke-static {p0, v0, v1, v2, v3}, LX/0oJ;->a(LX/0oJ;CCLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 140145
    sget-short v0, LX/0ob;->E:S

    sget-short v1, LX/0ob;->s:S

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 140146
    sget-short v0, LX/0ob;->G:S

    sget-short v1, LX/0ob;->u:S

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/0oJ;->a(LX/0oJ;SSZ)Z

    move-result v0

    return v0
.end method
