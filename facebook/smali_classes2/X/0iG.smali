.class public LX/0iG;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fac;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0x9;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0f7;

.field public e:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 120545
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 120546
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120547
    iput-object v0, p0, LX/0iG;->a:LX/0Ot;

    .line 120548
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120549
    iput-object v0, p0, LX/0iG;->b:LX/0Ot;

    .line 120550
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 120551
    iput-object v0, p0, LX/0iG;->c:LX/0Ot;

    .line 120552
    return-void
.end method

.method private static e(LX/0iG;)V
    .locals 7

    .prologue
    .line 120553
    iget-object v0, p0, LX/0iG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    iget-object v1, p0, LX/0iG;->e:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 120554
    iget-object p0, v1, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, p0

    .line 120555
    iget-object p0, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, p0

    .line 120556
    iget-object v2, v0, LX/0x9;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/139;

    .line 120557
    const/4 v4, 0x0

    .line 120558
    iget-object v3, v2, LX/139;->b:LX/0iA;

    sget-object v5, LX/13C;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v6, LX/13C;

    invoke-virtual {v3, v5, v6}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/13G;

    iput-object v3, v2, LX/139;->f:LX/13G;

    .line 120559
    iget-object v3, v2, LX/139;->f:LX/13G;

    if-nez v3, :cond_2

    move-object v3, v4

    .line 120560
    :goto_0
    move-object v3, v3

    .line 120561
    iget-object v4, v2, LX/139;->f:LX/13G;

    if-eqz v4, :cond_0

    if-nez v3, :cond_1

    .line 120562
    :cond_0
    :goto_1
    return-void

    .line 120563
    :cond_1
    iget-object v4, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    .line 120564
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 120565
    iget-object v6, v2, LX/139;->e:LX/03V;

    const-string p0, "SearchAwareness"

    const-string v0, "title and description are empty for tooltip."

    invoke-virtual {v6, p0, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120566
    const/4 v6, 0x0

    .line 120567
    :goto_2
    move-object v4, v6

    .line 120568
    if-eqz v4, :cond_0

    .line 120569
    iget-object v4, v2, LX/139;->c:LX/13A;

    iget-object v5, v2, LX/139;->f:LX/13G;

    invoke-interface {v5}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v6

    sget-object p0, LX/13C;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v4, v3, v5, v6, p0}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v3

    .line 120570
    invoke-virtual {v3}, LX/78A;->a()V

    .line 120571
    new-instance v4, LX/77n;

    invoke-direct {v4}, LX/77n;-><init>()V

    invoke-virtual {v3, v4}, LX/78A;->a(LX/77n;)V

    .line 120572
    goto :goto_1

    .line 120573
    :cond_2
    iget-object v3, v2, LX/139;->f:LX/13G;

    iget-object v5, v2, LX/139;->a:Landroid/content/Context;

    invoke-interface {v3, v5}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 120574
    if-nez v3, :cond_3

    move-object v3, v4

    .line 120575
    goto :goto_0

    .line 120576
    :cond_3
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "qp_definition"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    goto :goto_0

    .line 120577
    :cond_4
    new-instance v6, LX/0hs;

    iget-object p0, v2, LX/139;->a:Landroid/content/Context;

    const/4 v0, 0x2

    invoke-direct {v6, p0, v0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 120578
    invoke-virtual {v6, v4}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 120579
    invoke-virtual {v6, v5}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 120580
    sget-object p0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v6, p0}, LX/0ht;->a(LX/3AV;)V

    .line 120581
    const/4 p0, -0x1

    .line 120582
    iput p0, v6, LX/0hs;->t:I

    .line 120583
    invoke-virtual {v6, v1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 120584
    instance-of v0, p1, LX/0f7;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move-object v0, p1

    .line 120585
    check-cast v0, LX/0f7;

    iput-object v0, p0, LX/0iG;->d:LX/0f7;

    .line 120586
    const v0, 0x7f0d00bc

    invoke-static {p1, v0}, LX/0jc;->a(Landroid/app/Activity;I)LX/0am;

    move-result-object v0

    .line 120587
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 120588
    :goto_0
    return-void

    .line 120589
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iput-object v0, p0, LX/0iG;->e:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 120590
    iget-object v0, p0, LX/0iG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    iget-object v1, p0, LX/0iG;->e:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 120591
    iget-object p1, v1, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->q:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, p1

    .line 120592
    iget-object p1, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, p1

    .line 120593
    iput-object v1, v0, LX/0x9;->j:Landroid/view/View;

    .line 120594
    invoke-static {p0}, LX/0iG;->e(LX/0iG;)V

    .line 120595
    iget-object v0, p0, LX/0iG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 120596
    :goto_1
    goto :goto_0

    .line 120597
    :cond_1
    iget-object v0, p0, LX/0iG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->ae:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0iG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 120598
    iget-object v0, p0, LX/0iG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fac;

    iget-object v1, p0, LX/0iG;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0x9;

    .line 120599
    iput-object v1, v0, LX/Fac;->d:LX/0x9;

    .line 120600
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0iG;->f:Z

    .line 120601
    :cond_2
    iget-object v0, p0, LX/0iG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    invoke-virtual {v0}, LX/0x9;->a()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 120602
    iget-boolean v0, p0, LX/0iG;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0iG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 120603
    :cond_0
    :goto_0
    return-void

    .line 120604
    :cond_1
    if-eqz p1, :cond_2

    .line 120605
    iget-object v0, p0, LX/0iG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fac;

    .line 120606
    new-instance v1, LX/4KB;

    invoke-direct {v1}, LX/4KB;-><init>()V

    .line 120607
    new-instance v2, LX/A0X;

    invoke-direct {v2}, LX/A0X;-><init>()V

    move-object v2, v2

    .line 120608
    const-string p0, "input"

    invoke-virtual {v2, p0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 120609
    :try_start_0
    iget-object v1, v0, LX/Fac;->b:LX/0gX;

    new-instance p0, LX/Fab;

    invoke-direct {p0, v0}, LX/Fab;-><init>(LX/Fac;)V

    invoke-virtual {v1, v2, p0}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v1

    iput-object v1, v0, LX/Fac;->c:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 120610
    :goto_1
    goto :goto_0

    .line 120611
    :cond_2
    iget-object v0, p0, LX/0iG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fac;

    .line 120612
    iget-object v1, v0, LX/Fac;->c:LX/0gM;

    if-eqz v1, :cond_3

    .line 120613
    iget-object v1, v0, LX/Fac;->b:LX/0gX;

    iget-object v2, v0, LX/Fac;->c:LX/0gM;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 120614
    const/4 v1, 0x0

    iput-object v1, v0, LX/Fac;->c:LX/0gM;

    .line 120615
    :cond_3
    goto :goto_0

    .line 120616
    :catch_0
    move-exception v1

    .line 120617
    sget-object v2, LX/Fac;->a:Ljava/lang/Class;

    const-string p0, "Unable to subscribe to search awareness suggestions."

    invoke-static {v2, p0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 120618
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0iG;->d:LX/0f7;

    invoke-interface {v1}, LX/0f7;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/0iG;->a(Z)V

    .line 120619
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 120620
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0iG;->a(Z)V

    .line 120621
    iget-object v0, p0, LX/0iG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x9;

    .line 120622
    iget-object v1, v0, LX/0x9;->i:Ljava/util/Map;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->FORMATTED_TOOLTIP:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fb2;

    .line 120623
    if-eqz v1, :cond_1

    .line 120624
    iget-object p0, v1, LX/Fb2;->m:LX/0hs;

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/Fb2;->m:LX/0hs;

    .line 120625
    iget-boolean v0, p0, LX/0ht;->r:Z

    move p0, v0

    .line 120626
    if-eqz p0, :cond_0

    .line 120627
    iget-object p0, v1, LX/Fb2;->m:LX/0hs;

    invoke-virtual {p0}, LX/0ht;->l()V

    .line 120628
    :cond_0
    invoke-static {v1}, LX/Fb2;->f(LX/Fb2;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 120629
    iget-object p0, v1, LX/Fb2;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/FaY;

    .line 120630
    iget-object v0, p0, LX/FaY;->h:LX/3Af;

    if-eqz v0, :cond_1

    .line 120631
    iget-object v0, p0, LX/FaY;->h:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->dismiss()V

    .line 120632
    :cond_1
    return-void
.end method
