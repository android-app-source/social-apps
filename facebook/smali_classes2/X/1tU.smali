.class public LX/1tU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/05L;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/05K;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 336591
    const-class v0, LX/1tU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1tU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/05K;)V
    .locals 0

    .prologue
    .line 336588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336589
    iput-object p1, p0, LX/1tU;->b:LX/05K;

    .line 336590
    return-void
.end method

.method public static final a(LX/079;)[B
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 336581
    invoke-static {p0}, LX/1tU;->c(LX/079;)Ljava/util/List;

    move-result-object v8

    .line 336582
    new-instance v0, LX/1u5;

    iget-object v1, p0, LX/079;->a:Ljava/lang/String;

    iget-object v2, p0, LX/079;->b:Ljava/lang/String;

    iget-object v3, p0, LX/079;->c:Ljava/lang/String;

    invoke-static {p0}, LX/1tU;->b(LX/079;)LX/1u6;

    move-result-object v4

    iget-object v5, p0, LX/079;->e:Ljava/lang/String;

    iget-object v7, p0, LX/079;->d:LX/078;

    iget-object v10, v7, LX/078;->x:Ljava/util/Map;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v0 .. v10}, LX/1u5;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1u6;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)V

    .line 336583
    new-instance v1, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v1, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 336584
    :try_start_0
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 336585
    return-object v0

    .line 336586
    :catch_0
    move-exception v0

    .line 336587
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 336578
    if-nez p0, :cond_0

    .line 336579
    const/4 v0, 0x0

    .line 336580
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1u4;->a(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method private static b(LX/079;)LX/1u6;
    .locals 27

    .prologue
    .line 336568
    move-object/from16 v0, p0

    iget-object v0, v0, LX/079;->d:LX/078;

    move-object/from16 v26, v0

    .line 336569
    if-nez v26, :cond_0

    .line 336570
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No user name to fill ClientInfo"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 336571
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 336572
    move-object/from16 v0, v26

    iget-object v1, v0, LX/078;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 336573
    invoke-static {v1}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 336574
    if-eqz v3, :cond_1

    .line 336575
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336576
    :cond_1
    sget-object v3, LX/1tU;->a:Ljava/lang/String;

    const-string v4, "Topic %s does not have an id!"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 336577
    :cond_2
    new-instance v1, LX/1u6;

    move-object/from16 v0, v26

    iget-object v2, v0, LX/078;->a:Ljava/lang/Long;

    move-object/from16 v0, v26

    iget-object v3, v0, LX/078;->b:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v4, v0, LX/078;->c:Ljava/lang/Long;

    move-object/from16 v0, v26

    iget-object v5, v0, LX/078;->l:Ljava/lang/Long;

    move-object/from16 v0, v26

    iget v6, v0, LX/078;->m:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v26

    iget-object v7, v0, LX/078;->h:Ljava/lang/Boolean;

    move-object/from16 v0, v26

    iget-object v8, v0, LX/078;->g:Ljava/lang/Boolean;

    move-object/from16 v0, v26

    iget-object v9, v0, LX/078;->i:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v10, v0, LX/078;->k:Ljava/lang/Boolean;

    move-object/from16 v0, v26

    iget-object v11, v0, LX/078;->e:Ljava/lang/Integer;

    move-object/from16 v0, v26

    iget-object v12, v0, LX/078;->f:Ljava/lang/Integer;

    move-object/from16 v0, v26

    iget-object v13, v0, LX/078;->d:Ljava/lang/Long;

    const/4 v14, 0x0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->n:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->q:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, LX/1tU;->a(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->r:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->j:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->w:Ljava/lang/Byte;

    move-object/from16 v22, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->s:Ljava/lang/Long;

    move-object/from16 v23, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->t:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->u:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v26

    iget-object v0, v0, LX/078;->v:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-direct/range {v1 .. v26}, LX/1u6;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Boolean;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/Byte;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static b(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)LX/2Z9;
    .locals 8
    .param p0    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/2Z9;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 336592
    if-eqz p2, :cond_9

    .line 336593
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    move-object v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 336594
    iget-object v5, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-static {v5}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 336595
    if-eqz v5, :cond_0

    .line 336596
    if-nez v3, :cond_8

    .line 336597
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 336598
    :goto_1
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v0

    goto :goto_0

    .line 336599
    :cond_0
    if-nez v1, :cond_1

    .line 336600
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 336601
    :cond_1
    new-instance v5, LX/6mf;

    iget-object v6, v0, LX/0AF;->a:Ljava/lang/String;

    iget v0, v0, LX/0AF;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v5, v6, v0}, LX/6mf;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v5, v1

    move-object v4, v3

    .line 336602
    :goto_2
    if-eqz p3, :cond_7

    .line 336603
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    move-object v3, v2

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 336604
    invoke-static {v0}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    .line 336605
    if-eqz v7, :cond_3

    .line 336606
    if-nez v3, :cond_6

    .line 336607
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 336608
    :goto_4
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v0

    goto :goto_3

    .line 336609
    :cond_3
    if-nez v1, :cond_4

    .line 336610
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 336611
    :cond_4
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move-object v7, v1

    move-object v6, v3

    .line 336612
    :goto_5
    new-instance v0, LX/2Z9;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, LX/2Z9;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 336613
    return-object v0

    :cond_6
    move-object v0, v3

    goto :goto_4

    :cond_7
    move-object v7, v2

    move-object v6, v2

    goto :goto_5

    :cond_8
    move-object v0, v3

    goto :goto_1

    :cond_9
    move-object v5, v2

    move-object v4, v2

    goto :goto_2
.end method

.method public static b(Ljava/util/List;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 336512
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336513
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 336514
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 336515
    iget-object v4, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 336516
    if-eqz v4, :cond_0

    .line 336517
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336518
    :cond_0
    new-instance v4, LX/6mf;

    iget-object v5, v0, LX/0AF;->a:Ljava/lang/String;

    iget v0, v0, LX/0AF;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v4, v5, v0}, LX/6mf;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336519
    :cond_1
    new-instance v0, LX/2ZC;

    invoke-direct {v0, v1, v2}, LX/2ZC;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 336520
    new-instance v1, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v1, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 336521
    :try_start_0
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 336522
    :catch_0
    move-exception v0

    .line 336523
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c(LX/079;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/079;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/6mL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336505
    iget-object v0, p0, LX/079;->f:Ljava/util/List;

    if-nez v0, :cond_0

    .line 336506
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 336507
    :goto_0
    return-object v0

    .line 336508
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/079;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 336509
    iget-object v0, p0, LX/079;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Hx;

    .line 336510
    new-instance v3, LX/6mL;

    iget-object v4, v0, LX/0Hx;->a:Ljava/lang/String;

    iget v5, v0, LX/0Hx;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v0, v0, LX/0Hx;->c:[B

    invoke-direct {v3, v4, v5, v0}, LX/6mL;-><init>(Ljava/lang/String;Ljava/lang/Integer;[B)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 336511
    goto :goto_0
.end method

.method public static c(Ljava/util/List;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 336524
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336525
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 336526
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 336527
    invoke-static {v0}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 336528
    if-eqz v4, :cond_0

    .line 336529
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336530
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336531
    :cond_1
    new-instance v0, LX/6mm;

    invoke-direct {v0, v1, v2}, LX/6mm;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 336532
    new-instance v1, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v1, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 336533
    :try_start_0
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 336534
    :catch_0
    move-exception v0

    .line 336535
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/io/DataOutputStream;LX/07V;)I
    .locals 13

    .prologue
    .line 336541
    iget-object v0, p2, LX/07W;->a:LX/07R;

    move-object v11, v0

    .line 336542
    invoke-virtual {p2}, LX/07V;->a()LX/07U;

    move-result-object v12

    .line 336543
    invoke-virtual {p2}, LX/07V;->b()LX/079;

    move-result-object v10

    .line 336544
    invoke-static {v10}, LX/1tU;->c(LX/079;)Ljava/util/List;

    move-result-object v8

    .line 336545
    new-instance v0, LX/1u5;

    iget-object v1, v10, LX/079;->a:Ljava/lang/String;

    iget-object v2, v10, LX/079;->b:Ljava/lang/String;

    iget-object v3, v10, LX/079;->c:Ljava/lang/String;

    invoke-static {v10}, LX/1tU;->b(LX/079;)LX/1u6;

    move-result-object v4

    iget-object v5, v10, LX/079;->e:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    iget-object v10, v10, LX/079;->d:LX/078;

    iget-object v10, v10, LX/078;->x:Ljava/util/Map;

    invoke-direct/range {v0 .. v10}, LX/1u5;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1u6;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)V

    .line 336546
    new-instance v1, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v1, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 336547
    :try_start_0
    invoke-virtual {v1, v0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    invoke-static {v0}, LX/05K;->a([B)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 336548
    array-length v1, v0

    add-int/lit8 v1, v1, 0xc

    .line 336549
    invoke-static {v11}, LX/07Z;->a(LX/07R;)I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336550
    invoke-static {p1, v1}, LX/07Z;->a(Ljava/io/DataOutputStream;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 336551
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336552
    const/4 v3, 0x6

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336553
    const/16 v3, 0x4d

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336554
    const/16 v3, 0x51

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336555
    const/16 v3, 0x54

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336556
    const/16 v3, 0x54

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336557
    const/16 v3, 0x6f

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336558
    const/16 v3, 0x54

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 336559
    iget v3, v12, LX/07U;->a:I

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->write(I)V

    .line 336560
    invoke-static {v12}, LX/07Z;->a(LX/07U;)I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->write(I)V

    .line 336561
    iget v3, v12, LX/07U;->h:I

    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 336562
    const/4 v3, 0x0

    array-length v4, v0

    invoke-virtual {p1, v0, v3, v4}, Ljava/io/DataOutputStream;->write([BII)V

    .line 336563
    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V

    .line 336564
    add-int v0, v2, v1

    .line 336565
    return v0

    .line 336566
    :catch_0
    move-exception v0

    .line 336567
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336536
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336537
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0AF;

    .line 336538
    iget-object v3, v0, LX/0AF;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0AJ;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 336539
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336540
    :cond_1
    return-object v1
.end method
