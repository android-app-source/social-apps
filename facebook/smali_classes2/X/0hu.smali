.class public LX/0hu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:[Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Av5;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AvB;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AvE;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final l:Landroid/app/Activity;

.field public final m:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 119154
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, LX/0hu;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 119184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119185
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119186
    iput-object v0, p0, LX/0hu;->c:LX/0Ot;

    .line 119187
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119188
    iput-object v0, p0, LX/0hu;->d:LX/0Ot;

    .line 119189
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119190
    iput-object v0, p0, LX/0hu;->e:LX/0Ot;

    .line 119191
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119192
    iput-object v0, p0, LX/0hu;->f:LX/0Ot;

    .line 119193
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119194
    iput-object v0, p0, LX/0hu;->g:LX/0Ot;

    .line 119195
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119196
    iput-object v0, p0, LX/0hu;->h:LX/0Ot;

    .line 119197
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119198
    iput-object v0, p0, LX/0hu;->i:LX/0Ot;

    .line 119199
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 119200
    iput-object v0, p0, LX/0hu;->j:LX/0Ot;

    .line 119201
    new-instance v0, LX/0oi;

    invoke-direct {v0, p0}, LX/0oi;-><init>(LX/0hu;)V

    iput-object v0, p0, LX/0hu;->m:LX/0TF;

    .line 119202
    iput-object p1, p0, LX/0hu;->l:Landroid/app/Activity;

    .line 119203
    return-void
.end method

.method public static a(LX/0hu;Z)LX/0gH;
    .locals 8

    .prologue
    .line 119155
    iget-object v0, p0, LX/0hu;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    .line 119156
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->p:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->i:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    move v0, v1

    .line 119157
    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    .line 119158
    if-eqz p1, :cond_0

    sget-object v0, LX/0gH;->OPEN_TO_LEFT_DIVEBAR:LX/0gH;

    .line 119159
    :goto_0
    return-object v0

    .line 119160
    :cond_0
    sget-object v0, LX/0gH;->HAS_LEFT_DIVEBAR:LX/0gH;

    goto :goto_0

    .line 119161
    :cond_1
    const-wide/16 v6, -0x1f4

    .line 119162
    iget-object v2, p0, LX/0hu;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AvB;

    invoke-virtual {v2, v6, v7}, LX/AvB;->a(J)J

    move-result-wide v4

    .line 119163
    cmp-long v2, v4, v6

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/0hu;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x15180

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 119164
    :goto_1
    iget-object v0, p0, LX/0hu;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;

    .line 119165
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    sget-object v2, LX/0i7;->FETCHING:LX/0i7;

    if-ne v1, v2, :cond_3

    .line 119166
    :goto_2
    sget-object v0, LX/0gH;->NO_LEFT_DIVEBAR:LX/0gH;

    goto :goto_0

    .line 119167
    :cond_2
    iget-object v2, p0, LX/0hu;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AvE;

    iget-object v3, p0, LX/0hu;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/AvB;

    sget-object v4, LX/AvA;->PREFETCH:LX/AvA;

    invoke-virtual {v3, v4}, LX/AvB;->a(LX/AvA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AvE;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 119168
    iget-object v3, p0, LX/0hu;->m:LX/0TF;

    .line 119169
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 119170
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 119171
    :cond_3
    sget-object v1, LX/0i7;->FETCHING:LX/0i7;

    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->x:LX/0i7;

    .line 119172
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r:LX/0tS;

    invoke-virtual {v1}, LX/0tS;->a()F

    move-result v1

    .line 119173
    :goto_3
    const/high16 v2, 0x3f180000    # 0.59375f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_5

    .line 119174
    const-string v1, "ANDROID_16_9"

    .line 119175
    :goto_4
    new-instance v2, LX/AvY;

    invoke-direct {v2}, LX/AvY;-><init>()V

    move-object v2, v2

    .line 119176
    const-string v3, "inspiration_nux_asset_aspect_ratio"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 119177
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->q:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    move-object v1, v1

    .line 119178
    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->y:LX/0Vj;

    iget-object v3, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->u:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 119179
    new-instance v2, LX/Avz;

    invoke-direct {v2, v0}, LX/Avz;-><init>(Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;)V

    iget-object v3, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->v:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 119180
    :cond_4
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/nux/fetch/InspirationNuxStore;->r:LX/0tS;

    invoke-virtual {v2}, LX/0tS;->a()F

    move-result v2

    div-float/2addr v1, v2

    goto :goto_3

    .line 119181
    :cond_5
    const/high16 v2, 0x3f300000    # 0.6875f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_6

    .line 119182
    const-string v1, "ANDROID_16_10"

    goto :goto_4

    .line 119183
    :cond_6
    const-string v1, "ANDROID_4_3"

    goto :goto_4
.end method
