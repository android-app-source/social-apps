.class public LX/1nB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/intent/feed/IFeedIntentBuilder;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/1nB;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0hy;

.field private final c:LX/03V;

.field private final d:LX/17W;

.field private final e:LX/17Y;

.field private final f:LX/1nC;

.field private final g:LX/1nD;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1EQ;

.field private final j:LX/0Uh;

.field private final k:LX/1nF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hy;LX/03V;LX/17W;LX/17Y;LX/1nC;LX/1nD;LX/1EQ;LX/0Uh;LX/1nF;LX/0Or;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0hy;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/17W;",
            "LX/17Y;",
            "LX/1nC;",
            "LX/1nD;",
            "LX/1EQ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1nF;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 314888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314889
    iput-object p1, p0, LX/1nB;->a:Landroid/content/Context;

    .line 314890
    iput-object p3, p0, LX/1nB;->c:LX/03V;

    .line 314891
    iput-object p2, p0, LX/1nB;->b:LX/0hy;

    .line 314892
    iput-object p4, p0, LX/1nB;->d:LX/17W;

    .line 314893
    iput-object p5, p0, LX/1nB;->e:LX/17Y;

    .line 314894
    iput-object p6, p0, LX/1nB;->f:LX/1nC;

    .line 314895
    iput-object p7, p0, LX/1nB;->g:LX/1nD;

    .line 314896
    iput-object p8, p0, LX/1nB;->i:LX/1EQ;

    .line 314897
    iput-object p9, p0, LX/1nB;->j:LX/0Uh;

    .line 314898
    iput-object p11, p0, LX/1nB;->h:LX/0Or;

    .line 314899
    iput-object p10, p0, LX/1nB;->k:LX/1nF;

    .line 314900
    return-void
.end method

.method public static a(LX/0QB;)LX/1nB;
    .locals 15

    .prologue
    .line 314901
    sget-object v0, LX/1nB;->l:LX/1nB;

    if-nez v0, :cond_1

    .line 314902
    const-class v1, LX/1nB;

    monitor-enter v1

    .line 314903
    :try_start_0
    sget-object v0, LX/1nB;->l:LX/1nB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 314904
    if-eqz v2, :cond_0

    .line 314905
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 314906
    new-instance v3, LX/1nB;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {v0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v9

    check-cast v9, LX/1nC;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v10

    check-cast v10, LX/1nD;

    invoke-static {v0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v11

    check-cast v11, LX/1EQ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {v0}, LX/1nF;->a(LX/0QB;)LX/1nF;

    move-result-object v13

    check-cast v13, LX/1nF;

    const/16 v14, 0xc

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/1nB;-><init>(Landroid/content/Context;LX/0hy;LX/03V;LX/17W;LX/17Y;LX/1nC;LX/1nD;LX/1EQ;LX/0Uh;LX/1nF;LX/0Or;)V

    .line 314907
    move-object v0, v3

    .line 314908
    sput-object v0, LX/1nB;->l:LX/1nB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314909
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 314910
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 314911
    :cond_1
    sget-object v0, LX/1nB;->l:LX/1nB;

    return-object v0

    .line 314912
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 314913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;LX/89l;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 314914
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314915
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PROFILE_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314916
    const-string v1, "graphql_feedback_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314917
    const-string v1, "profile_list_type"

    invoke-virtual {p2}, LX/89l;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314918
    return-object v0
.end method

.method private c()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 314919
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/1nB;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 314920
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 314921
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/1nB;->a:Landroid/content/Context;

    const-string v3, "com.facebook.growth.friendfinder.FriendFinderStartActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 314922
    const-string v1, "ci_flow"

    sget-object v2, LX/89v;->NEWS_FEED_FIND_FRIENDS:LX/89v;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 314923
    return-object v0
.end method

.method public final a(JLjava/lang/String;LX/74S;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 314924
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/1nB;->a:Landroid/content/Context;

    const-string v3, "com.facebook.photos.mediagallery.ui.MediaGalleryActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 314925
    const-string v1, "photo_fbid"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 314926
    const-string v1, "photoset_token"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314927
    const-string v1, "fullscreen_gallery_source"

    invoke-virtual {p4}, LX/74S;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314928
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 314929
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314930
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->REACTORS_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314931
    const-string v1, "graphql_feedback_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314932
    const-string v1, "module_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314933
    const-string v1, "graphql_can_viewer_invite_user"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->aa()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 314934
    const-string v1, "reaction_can_viewer_ban_user"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 314935
    const-string v1, "comment_mention_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 314936
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 314937
    new-instance v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-direct {v0, p1}, Lcom/facebook/ipc/feed/ViewPermalinkParams;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 314938
    iget-object v1, p0, LX/1nB;->b:LX/0hy;

    invoke-interface {v1, v0}, LX/0hz;->a(Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Qt;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 314939
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 314940
    const/4 v0, 0x0

    .line 314941
    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne p2, v2, :cond_0

    .line 314942
    const-string v0, "/groups_seen_by?feedback=%s&story=%s"

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 314943
    iget-object v2, p0, LX/1nB;->k:LX/1nF;

    const-string v3, "FBGroupStorySeenByRoute"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, LX/1nF;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 314944
    :cond_0
    if-nez v0, :cond_1

    .line 314945
    sget-object v0, LX/89l;->SEEN_BY_FOR_FEEDBACK_ID:LX/89l;

    invoke-direct {p0, v1, v0}, LX/1nB;->a(Ljava/lang/String;LX/89l;)Landroid/content/Intent;

    move-result-object v0

    .line 314946
    :cond_1
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 314873
    iget-object v0, p0, LX/1nB;->j:LX/0Uh;

    const/16 v1, 0x45e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314874
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314875
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 314876
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/1nB;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 314877
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314878
    const-string v1, "mobile_page"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p2, "/edits/?stoken="

    invoke-direct {v2, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314879
    move-object v0, v0

    .line 314880
    :goto_1
    return-object v0

    .line 314881
    :cond_0
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314882
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EDIT_HISTORY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314883
    const-string v1, "module_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314884
    const-string v1, "story_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314885
    const-string v1, "open_fragment_as_modal"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 314886
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 314887
    sget-object v0, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    invoke-direct {p0, p1, v0}, LX/1nB;->a(Ljava/lang/String;LX/89l;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)Landroid/content/Intent;
    .locals 3
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 314772
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314773
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->COMMENT_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314774
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314775
    const-string v1, "comment"

    invoke-static {v0, v1, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 314776
    const-string v1, "relevant_comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314777
    const-string v1, "feedback_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314778
    const-string v1, "include_comments_disabled_fields"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 314779
    const-string v1, "feedback_logging_params"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 314780
    return-object v0
.end method

.method public final a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 4

    .prologue
    .line 314781
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314782
    iget-object v0, p0, LX/1nB;->c:LX/03V;

    const-string v1, "edit_post_missing_legacy_api_id"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "story: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314783
    :cond_0
    iget-object v0, p0, LX/1nB;->f:LX/1nC;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 314784
    sget-object v1, LX/21D;->INVALID:LX/21D;

    if-eq p1, v1, :cond_3

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 314785
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v3, v2

    :cond_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 314786
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLegacyApiStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCacheId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v3, LX/2rt;->STATUS:LX/2rt;

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEdit(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->P()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 314787
    sget-object v3, LX/21D;->NEWSFEED:LX/21D;

    if-ne p1, v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 314788
    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 314789
    invoke-virtual {v0, p3, v1, v2}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Z)V

    .line 314790
    move-object v0, v1

    .line 314791
    invoke-static {p3}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 314792
    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 314793
    :cond_2
    return-object v0

    :cond_3
    move v1, v3

    .line 314794
    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/21D;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 314795
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 314796
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 314797
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    move-object v2, v0

    .line 314798
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 314799
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 314800
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 314801
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 314802
    if-nez v3, :cond_1

    .line 314803
    invoke-static {v1}, LX/8Y8;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    .line 314804
    :cond_1
    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 314805
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 314806
    invoke-static {v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 314807
    iput-object v1, v2, LX/23u;->k:LX/0Px;

    .line 314808
    move-object v1, v2

    .line 314809
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 314810
    :try_start_0
    invoke-static {v0}, LX/890;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 314811
    :goto_2
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 314812
    if-eqz v1, :cond_3

    move-object p1, v1

    .line 314813
    :cond_3
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v6

    .line 314814
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 314815
    iput-object v1, v6, LX/21A;->a:LX/162;

    .line 314816
    iget-object v7, p0, LX/1nB;->i:LX/1EQ;

    .line 314817
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 314818
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v7, v1, v6}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 314819
    invoke-virtual {v6}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v1

    .line 314820
    invoke-static {v5}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v5

    .line 314821
    iput-object v3, v5, LX/89G;->e:Ljava/lang/String;

    .line 314822
    move-object v3, v5

    .line 314823
    iput-object v4, v3, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 314824
    move-object v3, v3

    .line 314825
    invoke-static {v0}, LX/89F;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    move-result-object v4

    .line 314826
    iput-object v4, v3, LX/89G;->f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 314827
    move-object v3, v3

    .line 314828
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    .line 314829
    iput-object v1, v3, LX/89G;->d:Ljava/lang/String;

    .line 314830
    move-object v1, v3

    .line 314831
    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 314832
    invoke-static {p2, p3, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAttachedStory(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 314833
    sget-object v2, LX/21D;->ON_THIS_DAY_FEED:LX/21D;

    if-ne p2, v2, :cond_5

    .line 314834
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 314835
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 314836
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v5

    .line 314837
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 314838
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {v2}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 314839
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 314840
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 314841
    :cond_5
    return-object v1

    .line 314842
    :catch_0
    move-exception v1

    .line 314843
    iget-object v6, p0, LX/1nB;->c:LX/03V;

    const-string v7, "composer_attachment_preview_error"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Attachment Preview error: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 314844
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v9, "Story id: "

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v9

    :goto_4
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", shareable id "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v9

    :goto_5
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v9, v9

    .line 314845
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_6
    const-string v9, ""

    goto :goto_4

    :cond_7
    const-string v9, ""

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3

    .prologue
    .line 314846
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    .line 314847
    iput-object p1, v0, LX/170;->o:Ljava/lang/String;

    .line 314848
    move-object v0, v0

    .line 314849
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x4984e12

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 314850
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 314851
    move-object v0, v0

    .line 314852
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 314853
    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {p2, p3, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/47I;)Z
    .locals 1

    .prologue
    .line 314854
    iget-object v0, p0, LX/1nB;->d:LX/17W;

    invoke-virtual {v0, p1, p2}, LX/17W;->a(Landroid/content/Context;LX/47I;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 314855
    iget-object v0, p0, LX/1nB;->d:LX/17W;

    invoke-virtual {v0, p1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z
    .locals 1
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 314856
    iget-object v0, p0, LX/1nB;->d:LX/17W;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 314857
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314858
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314859
    return-object v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 314860
    iget-object v0, p0, LX/1nB;->e:LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 314861
    invoke-direct {p0}, LX/1nB;->c()Landroid/content/Intent;

    move-result-object v0

    .line 314862
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PROFILE_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314863
    const-string v1, "graphql_poll_option_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314864
    const-string v1, "profile_list_type"

    sget-object v2, LX/89l;->VOTERS_FOR_POLL_OPTION_ID:LX/89l;

    invoke-virtual {v2}, LX/89l;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 314865
    return-object v0
.end method

.method public final c(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 314866
    new-instance v0, LX/2oI;

    invoke-direct {v0}, LX/2oI;-><init>()V

    .line 314867
    iput-object p1, v0, LX/2oI;->aT:Ljava/lang/String;

    .line 314868
    move-object v0, v0

    .line 314869
    invoke-virtual {v0}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 314870
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, LX/1nB;->a:Landroid/content/Context;

    const-string v4, "com.facebook.video.activity.FullScreenVideoPlayerActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 314871
    const-string v2, "video_graphql_object"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 314872
    return-object v1
.end method
