.class public LX/1Lr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile p:LX/1Lr;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:LX/0ad;

.field public final h:LX/0wq;

.field public final i:LX/0wp;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0ka;

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Uh;

.field private final n:LX/19j;

.field private final o:LX/0W3;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;LX/0wq;LX/0ka;LX/0Uh;LX/0wp;LX/19j;LX/0W3;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/video/abtest/VideoPrefetch;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0wq;",
            "LX/0ka;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0wp;",
            "LX/19j;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 234362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234363
    iput-object p1, p0, LX/1Lr;->g:LX/0ad;

    .line 234364
    iput-object p3, p0, LX/1Lr;->h:LX/0wq;

    .line 234365
    iput-object p6, p0, LX/1Lr;->i:LX/0wp;

    .line 234366
    iput-object p2, p0, LX/1Lr;->j:LX/0Or;

    .line 234367
    iput-object p4, p0, LX/1Lr;->k:LX/0ka;

    .line 234368
    iput-object p5, p0, LX/1Lr;->m:LX/0Uh;

    .line 234369
    iput-object p8, p0, LX/1Lr;->o:LX/0W3;

    .line 234370
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget-object p2, LX/0c0;->Cached:LX/0c0;

    sget-short p3, LX/0ws;->ec:S

    iget-object v0, p0, LX/1Lr;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 p4, 0x0

    invoke-virtual {v0, p4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-interface {v1, p2, p3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    move v0, v0

    .line 234371
    iput-boolean v0, p0, LX/1Lr;->a:Z

    .line 234372
    const/4 p3, 0x1

    .line 234373
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget p2, LX/0ws;->ej:I

    invoke-interface {v0, v1, p2, p3}, LX/0ad;->a(LX/0c0;II)I

    move-result v0

    .line 234374
    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 234375
    const/16 v1, 0x8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 234376
    move v0, v0

    .line 234377
    iput v0, p0, LX/1Lr;->b:I

    .line 234378
    invoke-static {p0}, LX/1Lr;->g(LX/1Lr;)I

    move-result v0

    iput v0, p0, LX/1Lr;->c:I

    .line 234379
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-short v1, LX/0ws;->dU:S

    const/4 p2, 0x0

    invoke-interface {v0, v1, p2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 234380
    iput-boolean v0, p0, LX/1Lr;->d:Z

    .line 234381
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 234382
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-char p2, LX/0ws;->dW:C

    const-string p3, ""

    invoke-interface {v0, p2, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string p2, ","

    invoke-virtual {v0, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    array-length p3, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    aget-object p4, p2, v0

    .line 234383
    invoke-virtual {p4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p4

    invoke-interface {v1, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 234384
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234385
    :cond_0
    move-object v0, v1

    .line 234386
    iput-object v0, p0, LX/1Lr;->l:Ljava/util/Set;

    .line 234387
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-short v1, LX/0ws;->dX:S

    const/4 p2, 0x1

    invoke-interface {v0, v1, p2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 234388
    iput-boolean v0, p0, LX/1Lr;->e:Z

    .line 234389
    sget-short v0, LX/0ws;->ek:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1Lr;->f:Z

    .line 234390
    iput-object p7, p0, LX/1Lr;->n:LX/19j;

    .line 234391
    return-void
.end method

.method public static a(LX/0QB;)LX/1Lr;
    .locals 12

    .prologue
    .line 234349
    sget-object v0, LX/1Lr;->p:LX/1Lr;

    if-nez v0, :cond_1

    .line 234350
    const-class v1, LX/1Lr;

    monitor-enter v1

    .line 234351
    :try_start_0
    sget-object v0, LX/1Lr;->p:LX/1Lr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234352
    if-eqz v2, :cond_0

    .line 234353
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 234354
    new-instance v3, LX/1Lr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0x383

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v6

    check-cast v6, LX/0wq;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v7

    check-cast v7, LX/0ka;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v9

    check-cast v9, LX/0wp;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v10

    check-cast v10, LX/19j;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v11

    check-cast v11, LX/0W3;

    invoke-direct/range {v3 .. v11}, LX/1Lr;-><init>(LX/0ad;LX/0Or;LX/0wq;LX/0ka;LX/0Uh;LX/0wp;LX/19j;LX/0W3;)V

    .line 234355
    move-object v0, v3

    .line 234356
    sput-object v0, LX/1Lr;->p:LX/1Lr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234357
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234358
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234359
    :cond_1
    sget-object v0, LX/1Lr;->p:LX/1Lr;

    return-object v0

    .line 234360
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static g(LX/1Lr;)I
    .locals 3

    .prologue
    .line 234345
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget v1, LX/0ws;->dS:I

    const/16 v2, 0x3e8

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 234346
    iget-object v1, p0, LX/1Lr;->h:LX/0wq;

    invoke-virtual {v1}, LX/0wq;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234347
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget v2, LX/0ws;->dT:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v0

    .line 234348
    :cond_0
    return v0
.end method

.method private static l(LX/1Lr;)Z
    .locals 4

    .prologue
    .line 234304
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/0ws;->dY:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    .line 234305
    iget-object v1, p0, LX/1Lr;->h:LX/0wq;

    invoke-virtual {v1}, LX/0wq;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234306
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/0ws;->dZ:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    .line 234307
    :cond_0
    return v0
.end method

.method private static m(LX/1Lr;)Z
    .locals 4

    .prologue
    .line 234341
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/0ws;->ea:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    .line 234342
    iget-object v1, p0, LX/1Lr;->h:LX/0wq;

    invoke-virtual {v1}, LX/0wq;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234343
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/0ws;->eb:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    .line 234344
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(ZZ)LX/377;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 234316
    if-eqz p2, :cond_1

    .line 234317
    iget-object v0, p0, LX/1Lr;->i:LX/0wp;

    iget v4, v0, LX/0wp;->g:I

    .line 234318
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget v1, LX/0ws;->ei:I

    invoke-interface {v0, v1, v5}, LX/0ad;->a(II)I

    move-result v1

    .line 234319
    if-eqz p1, :cond_0

    .line 234320
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget v2, LX/0ws;->ed:I

    iget-object v3, p0, LX/1Lr;->i:LX/0wp;

    iget v3, v3, LX/0wp;->e:I

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 234321
    :goto_0
    move v2, v0

    move v3, p1

    .line 234322
    :goto_1
    new-instance v0, LX/377;

    invoke-direct/range {v0 .. v5}, LX/377;-><init>(IIZIZ)V

    return-object v0

    .line 234323
    :cond_0
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget v2, LX/0ws;->ee:I

    iget-object v3, p0, LX/1Lr;->i:LX/0wp;

    iget v3, v3, LX/0wp;->e:I

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 234324
    goto :goto_0

    .line 234325
    :cond_1
    if-eqz p1, :cond_3

    .line 234326
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-short v1, LX/0ws;->dQ:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 234327
    iget-object v1, p0, LX/1Lr;->h:LX/0wq;

    invoke-virtual {v1}, LX/0wq;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 234328
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget-short v2, LX/0ws;->dR:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 234329
    :cond_2
    move v0, v0

    .line 234330
    :goto_2
    invoke-static {p0}, LX/1Lr;->g(LX/1Lr;)I

    move-result v4

    .line 234331
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget v2, LX/0ws;->eh:I

    invoke-interface {v1, v2, v5}, LX/0ad;->a(II)I

    move-result v1

    .line 234332
    if-eqz p1, :cond_5

    iget-object v2, p0, LX/1Lr;->g:LX/0ad;

    sget v3, LX/0ws;->eg:I

    iget-object v5, p0, LX/1Lr;->o:LX/0W3;

    sget-wide v6, LX/0X5;->hE:J

    const v8, 0x7a120

    invoke-interface {v5, v6, v7, v8}, LX/0W4;->a(JI)I

    move-result v5

    invoke-interface {v2, v3, v5}, LX/0ad;->a(II)I

    move-result v2

    .line 234333
    :goto_3
    if-eqz p1, :cond_6

    invoke-static {p0}, LX/1Lr;->m(LX/1Lr;)Z

    move-result v5

    move v3, v0

    goto :goto_1

    .line 234334
    :cond_3
    iget-object v0, p0, LX/1Lr;->g:LX/0ad;

    sget-short v1, LX/0ws;->dO:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 234335
    iget-object v1, p0, LX/1Lr;->h:LX/0wq;

    invoke-virtual {v1}, LX/0wq;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 234336
    iget-object v1, p0, LX/1Lr;->g:LX/0ad;

    sget-short v2, LX/0ws;->dP:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 234337
    :cond_4
    move v0, v0

    .line 234338
    goto :goto_2

    .line 234339
    :cond_5
    iget-object v2, p0, LX/1Lr;->g:LX/0ad;

    sget v3, LX/0ws;->ef:I

    iget-object v5, p0, LX/1Lr;->o:LX/0W3;

    sget-wide v6, LX/0X5;->hF:J

    const v8, 0x3e800

    invoke-interface {v5, v6, v7, v8}, LX/0W4;->a(JI)I

    move-result v5

    invoke-interface {v2, v3, v5}, LX/0ad;->a(II)I

    move-result v2

    goto :goto_3

    .line 234340
    :cond_6
    invoke-static {p0}, LX/1Lr;->l(LX/1Lr;)Z

    move-result v5

    move v3, v0

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 234315
    invoke-static {p0}, LX/1Lr;->l(LX/1Lr;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1Lr;->m(LX/1Lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 234308
    iget-object v1, p0, LX/1Lr;->k:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 234309
    :cond_0
    :goto_0
    return v0

    .line 234310
    :cond_1
    if-nez p1, :cond_0

    .line 234311
    iget-object v1, p0, LX/1Lr;->k:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 234312
    if-eqz v1, :cond_2

    .line 234313
    iget-object v2, p0, LX/1Lr;->l:Ljava/util/Set;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 234314
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
