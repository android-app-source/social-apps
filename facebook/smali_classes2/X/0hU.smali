.class public LX/0hU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hV;


# instance fields
.field private final a:LX/0l9;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(LX/0l9;)V
    .locals 3

    .prologue
    .line 117752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117753
    iput-object p1, p0, LX/0hU;->a:LX/0l9;

    .line 117754
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117755
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p1, v0, LX/0eW;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    :goto_0
    move v0, v1

    .line 117756
    iput v0, p0, LX/0hU;->b:I

    .line 117757
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117758
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p1, v0, LX/0eW;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    :goto_1
    move v0, v1

    .line 117759
    iput v0, p0, LX/0hU;->c:I

    .line 117760
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117761
    const/16 v1, 0x12

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v2, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p1, v0, LX/0eW;->a:I

    add-int/2addr v1, p1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    :goto_2
    move v0, v1

    .line 117762
    iput v0, p0, LX/0hU;->d:I

    .line 117763
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117764
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, LX/0eW;->d(I)I

    move-result v1

    :goto_3
    move v0, v1

    .line 117765
    iput v0, p0, LX/0hU;->e:I

    .line 117766
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117767
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, LX/0eW;->d(I)I

    move-result v1

    :goto_4
    move v0, v1

    .line 117768
    iput v0, p0, LX/0hU;->f:I

    .line 117769
    iget-object v0, p0, LX/0hU;->a:LX/0l9;

    .line 117770
    const/16 v1, 0x16

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0, v1}, LX/0eW;->d(I)I

    move-result v1

    :goto_5
    move v0, v1

    .line 117771
    iput v0, p0, LX/0hU;->g:I

    .line 117772
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private static a(LX/0xo;LX/0XJ;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 117773
    const/4 v0, 0x0

    .line 117774
    sget-object v1, LX/0xx;->a:[I

    invoke-virtual {p1}, LX/0XJ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 117775
    :goto_0
    if-nez v0, :cond_0

    .line 117776
    invoke-virtual {p0}, LX/0xo;->a()Ljava/lang/String;

    move-result-object v0

    .line 117777
    :cond_0
    return-object v0

    .line 117778
    :pswitch_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_1

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v0, v0

    .line 117779
    goto :goto_0

    .line 117780
    :pswitch_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_2

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 117781
    goto :goto_0

    .line 117782
    :pswitch_2
    invoke-virtual {p0}, LX/0xo;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(ILX/0XJ;)Ljava/lang/String;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 117743
    iget v1, p0, LX/0hU;->b:I

    sub-int v1, p1, v1

    .line 117744
    if-ltz v1, :cond_0

    iget v2, p0, LX/0hU;->e:I

    if-lt v1, v2, :cond_1

    .line 117745
    :cond_0
    :goto_0
    return-object v0

    .line 117746
    :cond_1
    iget-object v2, p0, LX/0hU;->a:LX/0l9;

    .line 117747
    new-instance v3, LX/0xo;

    invoke-direct {v3}, LX/0xo;-><init>()V

    .line 117748
    const/16 p0, 0xa

    invoke-virtual {v2, p0}, LX/0eW;->a(I)I

    move-result p0

    if-eqz p0, :cond_2

    invoke-virtual {v2, p0}, LX/0eW;->e(I)I

    move-result p0

    mul-int/lit8 p1, v1, 0x4

    add-int/2addr p0, p1

    invoke-virtual {v2, p0}, LX/0eW;->b(I)I

    move-result p0

    iget p1, v2, LX/0l9;->c:I

    if-eq p0, p1, :cond_2

    iget-object p1, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p0, p1}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object p0

    :goto_1
    move-object v3, p0

    .line 117749
    move-object v1, v3

    .line 117750
    if-eqz v1, :cond_0

    .line 117751
    invoke-static {v1, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final b(ILX/0XJ;)LX/12t;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/PluralsRes;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 117683
    iget v1, p0, LX/0hU;->c:I

    sub-int v1, p1, v1

    .line 117684
    if-ltz v1, :cond_0

    iget v2, p0, LX/0hU;->f:I

    if-lt v1, v2, :cond_1

    .line 117685
    :cond_0
    :goto_0
    return-object v0

    .line 117686
    :cond_1
    iget-object v2, p0, LX/0hU;->a:LX/0l9;

    .line 117687
    new-instance v3, LX/12s;

    invoke-direct {v3}, LX/12s;-><init>()V

    .line 117688
    const/16 p0, 0x10

    invoke-virtual {v2, p0}, LX/0eW;->a(I)I

    move-result p0

    if-eqz p0, :cond_8

    invoke-virtual {v2, p0}, LX/0eW;->e(I)I

    move-result p0

    mul-int/lit8 p1, v1, 0x4

    add-int/2addr p0, p1

    invoke-virtual {v2, p0}, LX/0eW;->b(I)I

    move-result p0

    iget p1, v2, LX/0l9;->d:I

    if-eq p0, p1, :cond_8

    iget-object p1, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 117689
    iput p0, v3, LX/12s;->a:I

    iput-object p1, v3, LX/12s;->b:Ljava/nio/ByteBuffer;

    move-object p0, v3

    .line 117690
    :goto_1
    move-object v3, p0

    .line 117691
    move-object v1, v3

    .line 117692
    if-eqz v1, :cond_0

    .line 117693
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 117694
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117695
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_9

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_2
    move-object v0, v3

    .line 117696
    move-object v0, v0

    .line 117697
    if-eqz v0, :cond_2

    .line 117698
    sget-object v3, LX/0W6;->ZERO:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117699
    :cond_2
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117700
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_a

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_3
    move-object v0, v3

    .line 117701
    move-object v0, v0

    .line 117702
    if-eqz v0, :cond_3

    .line 117703
    sget-object v3, LX/0W6;->ONE:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117704
    :cond_3
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117705
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_b

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_4
    move-object v0, v3

    .line 117706
    move-object v0, v0

    .line 117707
    if-eqz v0, :cond_4

    .line 117708
    sget-object v3, LX/0W6;->TWO:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117709
    :cond_4
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117710
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_c

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_5
    move-object v0, v3

    .line 117711
    move-object v0, v0

    .line 117712
    if-eqz v0, :cond_5

    .line 117713
    sget-object v3, LX/0W6;->FEW:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117714
    :cond_5
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117715
    const/16 v3, 0xc

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_d

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_6
    move-object v0, v3

    .line 117716
    move-object v0, v0

    .line 117717
    if-eqz v0, :cond_6

    .line 117718
    sget-object v3, LX/0W6;->MANY:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117719
    :cond_6
    new-instance v0, LX/0xo;

    invoke-direct {v0}, LX/0xo;-><init>()V

    .line 117720
    const/16 v3, 0xe

    invoke-virtual {v1, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_e

    iget p0, v1, LX/0eW;->a:I

    add-int/2addr v3, p0

    invoke-virtual {v1, v3}, LX/0eW;->b(I)I

    move-result v3

    iget-object p0, v1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3, p0}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object v3

    :goto_7
    move-object v0, v3

    .line 117721
    move-object v0, v0

    .line 117722
    if-eqz v0, :cond_7

    .line 117723
    sget-object v1, LX/0W6;->OTHER:LX/0W6;

    invoke-static {v0, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 117724
    :cond_7
    new-instance v0, LX/12t;

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/12t;-><init>(LX/0P1;)V

    goto/16 :goto_0

    :cond_8
    const/4 p0, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_c
    const/4 v3, 0x0

    goto :goto_5

    :cond_d
    const/4 v3, 0x0

    goto :goto_6

    :cond_e
    const/4 v3, 0x0

    goto :goto_7
.end method

.method public final c(ILX/0XJ;)[Ljava/lang/String;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/ArrayRes;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 117725
    iget v1, p0, LX/0hU;->d:I

    sub-int v1, p1, v1

    .line 117726
    if-ltz v1, :cond_0

    iget v2, p0, LX/0hU;->g:I

    if-lt v1, v2, :cond_1

    .line 117727
    :cond_0
    :goto_0
    return-object v0

    .line 117728
    :cond_1
    iget-object v2, p0, LX/0hU;->a:LX/0l9;

    .line 117729
    new-instance v3, LX/318;

    invoke-direct {v3}, LX/318;-><init>()V

    .line 117730
    const/16 p0, 0x16

    invoke-virtual {v2, p0}, LX/0eW;->a(I)I

    move-result p0

    if-eqz p0, :cond_3

    invoke-virtual {v2, p0}, LX/0eW;->e(I)I

    move-result p0

    mul-int/lit8 p1, v1, 0x4

    add-int/2addr p0, p1

    invoke-virtual {v2, p0}, LX/0eW;->b(I)I

    move-result p0

    iget p1, v2, LX/0l9;->e:I

    if-eq p0, p1, :cond_3

    iget-object p1, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 117731
    iput p0, v3, LX/318;->a:I

    iput-object p1, v3, LX/318;->b:Ljava/nio/ByteBuffer;

    move-object p0, v3

    .line 117732
    :goto_1
    move-object v3, p0

    .line 117733
    move-object v2, v3

    .line 117734
    if-eqz v2, :cond_0

    .line 117735
    invoke-virtual {v2}, LX/318;->a()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 117736
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2}, LX/318;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 117737
    new-instance v3, LX/0xo;

    invoke-direct {v3}, LX/0xo;-><init>()V

    .line 117738
    const/4 p0, 0x4

    invoke-virtual {v2, p0}, LX/0eW;->a(I)I

    move-result p0

    if-eqz p0, :cond_4

    invoke-virtual {v2, p0}, LX/0eW;->e(I)I

    move-result p0

    mul-int/lit8 p1, v0, 0x4

    add-int/2addr p0, p1

    invoke-virtual {v2, p0}, LX/0eW;->b(I)I

    move-result p0

    iget-object p1, v2, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p0, p1}, LX/0xo;->a(ILjava/nio/ByteBuffer;)LX/0xo;

    move-result-object p0

    :goto_3
    move-object v3, p0

    .line 117739
    move-object v3, v3

    .line 117740
    invoke-static {v3, p2}, LX/0hU;->a(LX/0xo;LX/0XJ;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 117741
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 117742
    goto :goto_0

    :cond_3
    const/4 p0, 0x0

    goto :goto_1

    :cond_4
    const/4 p0, 0x0

    goto :goto_3
.end method
