.class public abstract LX/0QG;
.super LX/0QH;
.source ""

# interfaces
.implements LX/0QI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0QH;",
        "LX/0QI",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57973
    invoke-direct {p0}, LX/0QH;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 57984
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/concurrent/Callable",
            "<+TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 57985
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 57986
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0}, LX/0QI;->a()V

    .line 57987
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 57980
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Iterable;)V

    .line 57981
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 57982
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 57983
    return-void
.end method

.method public final b()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 57979
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0}, LX/0QI;->b()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 57977
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 57978
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 57975
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    invoke-interface {v0}, LX/0QI;->c()V

    .line 57976
    return-void
.end method

.method public abstract d()LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QI",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57974
    invoke-virtual {p0}, LX/0QG;->d()LX/0QI;

    move-result-object v0

    return-object v0
.end method
