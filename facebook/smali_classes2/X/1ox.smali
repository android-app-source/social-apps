.class public LX/1ox;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1oy;


# static fields
.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:LX/1p0;

.field public final e:LX/0yK;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 327583
    const-class v0, LX/1ox;

    sput-object v0, LX/1ox;->c:Ljava/lang/Class;

    .line 327584
    new-instance v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v1, "^(https?)://(api\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5}))($|\\?.*$|\\/.*$)"

    const-string v2, "$1://b-$2$5"

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v2, "^(https?)://(graph\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5}))($|\\?.*$|\\/.*$)"

    const-string v3, "$1://b-$2$5"

    invoke-direct {v1, v2, v3}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v3, "^(https?)://(free|m|mobile|d|b-m)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|\\/.*$)"

    const-string v4, "$1://m.$3facebook.com$4$5"

    invoke-direct {v2, v3, v4}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v4, "^(https?)://(www|web|z-m-www)\\.([0-9a-zA-Z\\.-]*)?facebook\\.com(:?[0-9]{0,5})($|\\?.*$|\\/.*$)"

    const-string v5, "$1://www.$3facebook.com$4$5"

    invoke-direct {v3, v4, v5}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/1ox;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/1p0;LX/0yK;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1p0;",
            "LX/0yK;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 327585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327586
    new-instance v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v1, "^https?://b-(graph|api)\\.facebook\\.com.*$"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    const-string v2, "^(https?)://api\\.([0-9a-zA-Z\\.-]*)?facebook\\.com\\/method\\/mobile\\.zeroBalanceDetection"

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1ox;->i:LX/0Px;

    .line 327587
    iput-object p1, p0, LX/1ox;->d:LX/1p0;

    .line 327588
    iput-object p2, p0, LX/1ox;->e:LX/0yK;

    .line 327589
    iput-object p3, p0, LX/1ox;->f:LX/0Or;

    .line 327590
    iput-object p4, p0, LX/1ox;->g:LX/0Or;

    .line 327591
    iput-object p5, p0, LX/1ox;->h:LX/0Or;

    .line 327592
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 327593
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327594
    invoke-virtual {p0, v0}, LX/1ox;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327595
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327596
    :goto_0
    return-object p1

    :cond_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 327597
    iget-object v0, p0, LX/1ox;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 327598
    iget-object v0, p0, LX/1ox;->d:LX/1p0;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, LX/1p0;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 327599
    iget-object v0, p0, LX/1ox;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1ox;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 327600
    iget-object v0, p0, LX/1ox;->d:LX/1p0;

    iget-object v1, p0, LX/1ox;->e:LX/0yK;

    invoke-interface {v1}, LX/0yK;->k()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/1p0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 327601
    :cond_0
    :goto_0
    return-object p1

    .line 327602
    :cond_1
    iget-object v0, p0, LX/1ox;->e:LX/0yK;

    invoke-interface {v0}, LX/0yK;->h()LX/0Px;

    move-result-object v3

    .line 327603
    iget-object v0, p0, LX/1ox;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, p0, LX/1ox;->i:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 327604
    invoke-virtual {v0, p1}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327605
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 327606
    :cond_2
    if-eqz v3, :cond_4

    .line 327607
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 327608
    invoke-virtual {v0, p1}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 327609
    invoke-virtual {v0, p1}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327610
    iget-object v1, p0, LX/1ox;->d:LX/1p0;

    iget-object v2, p0, LX/1ox;->e:LX/0yK;

    invoke-interface {v2}, LX/0yK;->k()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, p1, v0, v2}, LX/1p0;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    move-object p1, v0

    .line 327611
    goto :goto_0

    .line 327612
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 327613
    :cond_4
    iget-object v0, p0, LX/1ox;->d:LX/1p0;

    iget-object v1, p0, LX/1ox;->e:LX/0yK;

    invoke-interface {v1}, LX/0yK;->k()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/1p0;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final a(Ljava/net/URI;)Ljava/net/URI;
    .locals 2

    .prologue
    .line 327614
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327615
    invoke-virtual {p0, v0}, LX/1ox;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 327616
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327617
    :goto_0
    return-object p1

    :cond_0
    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)Z
    .locals 5

    .prologue
    .line 327618
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327619
    const/4 v2, 0x0

    .line 327620
    iget-object v1, p0, LX/1ox;->e:LX/0yK;

    invoke-interface {v1}, LX/0yK;->h()LX/0Px;

    move-result-object v4

    .line 327621
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p1

    move v3, v2

    :goto_0
    if-ge v3, p1, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 327622
    invoke-virtual {v1, v0}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327623
    const/4 v1, 0x1

    .line 327624
    :goto_1
    move v0, v1

    .line 327625
    return v0

    .line 327626
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 327627
    goto :goto_1
.end method
