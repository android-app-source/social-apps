.class public final LX/101;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;


# direct methods
.method public constructor <init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V
    .locals 0

    .prologue
    .line 168305
    iput-object p1, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 168306
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 168307
    :cond_0
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 168308
    :goto_0
    return-void

    .line 168309
    :cond_1
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 168310
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a:LX/0wS;

    iget-object v1, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v1, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    .line 168311
    const/4 p0, 0x0

    invoke-static {v0, v1, p0}, LX/0wS;->a(LX/0wS;Landroid/view/View;F)V

    .line 168312
    goto :goto_0

    .line 168313
    :cond_2
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->h:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 168314
    :cond_3
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-static {v0}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 168315
    iget-object v1, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168316
    :goto_0
    iput-object v0, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n:Ljava/lang/String;

    .line 168317
    return-void

    .line 168318
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 168319
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 168320
    iget-object v0, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    iget-object v1, p0, LX/101;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v1, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->n:Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/F5P;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168321
    :cond_0
    return-void
.end method
