.class public LX/1Kg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1Kg;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 232969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232970
    return-void
.end method

.method public static a(LX/0QB;)LX/1Kg;
    .locals 3

    .prologue
    .line 232971
    sget-object v0, LX/1Kg;->a:LX/1Kg;

    if-nez v0, :cond_1

    .line 232972
    const-class v1, LX/1Kg;

    monitor-enter v1

    .line 232973
    :try_start_0
    sget-object v0, LX/1Kg;->a:LX/1Kg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 232974
    if-eqz v2, :cond_0

    .line 232975
    :try_start_1
    new-instance v0, LX/1Kg;

    invoke-direct {v0}, LX/1Kg;-><init>()V

    .line 232976
    move-object v0, v0

    .line 232977
    sput-object v0, LX/1Kg;->a:LX/1Kg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232978
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 232979
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 232980
    :cond_1
    sget-object v0, LX/1Kg;->a:LX/1Kg;

    return-object v0

    .line 232981
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 232982
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 232983
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/activity/ComposerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 232984
    const-class v1, Landroid/app/Service;

    invoke-static {p1, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 232985
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 232986
    :cond_0
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 232987
    const-string v1, "extra_composer_system_data"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 232988
    invoke-virtual {p0}, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 232989
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {p0}, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 232990
    :cond_1
    return-object v0
.end method
