.class public LX/0Zj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Zk",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:[Ljava/lang/Object;

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 83799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83800
    if-gtz p1, :cond_0

    .line 83801
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The max pool size must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83802
    :cond_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LX/0Zj;->a:[Ljava/lang/Object;

    .line 83803
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 83804
    iget v0, p0, LX/0Zj;->b:I

    if-lez v0, :cond_0

    .line 83805
    iget v0, p0, LX/0Zj;->b:I

    add-int/lit8 v2, v0, -0x1

    .line 83806
    iget-object v0, p0, LX/0Zj;->a:[Ljava/lang/Object;

    aget-object v0, v0, v2

    .line 83807
    iget-object v3, p0, LX/0Zj;->a:[Ljava/lang/Object;

    aput-object v1, v3, v2

    .line 83808
    iget v1, p0, LX/0Zj;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/0Zj;->b:I

    .line 83809
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 83810
    const/4 v1, 0x0

    .line 83811
    move v0, v1

    :goto_0
    iget v2, p0, LX/0Zj;->b:I

    if-ge v0, v2, :cond_0

    .line 83812
    iget-object v2, p0, LX/0Zj;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_3

    .line 83813
    const/4 v1, 0x1

    .line 83814
    :cond_0
    move v0, v1

    .line 83815
    if-eqz v0, :cond_1

    .line 83816
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in the pool!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83817
    :cond_1
    iget v0, p0, LX/0Zj;->b:I

    iget-object v1, p0, LX/0Zj;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 83818
    iget-object v0, p0, LX/0Zj;->a:[Ljava/lang/Object;

    iget v1, p0, LX/0Zj;->b:I

    aput-object p1, v0, v1

    .line 83819
    iget v0, p0, LX/0Zj;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0Zj;->b:I

    .line 83820
    const/4 v0, 0x1

    .line 83821
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 83822
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
