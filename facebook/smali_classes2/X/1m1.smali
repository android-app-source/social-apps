.class public LX/1m1;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/video/server/VideoServerListener;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1m2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312976
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1m2;
    .locals 7

    .prologue
    .line 312978
    sget-object v0, LX/1m1;->a:LX/1m2;

    if-nez v0, :cond_1

    .line 312979
    const-class v1, LX/1m1;

    monitor-enter v1

    .line 312980
    :try_start_0
    sget-object v0, LX/1m1;->a:LX/1m2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 312981
    if-eqz v2, :cond_0

    .line 312982
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 312983
    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    const/16 v5, 0x12f4

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v5

    check-cast v5, LX/0Vw;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v3, v4, p0, v5, v6}, LX/19Y;->a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/0Zb;)LX/1m2;

    move-result-object v3

    move-object v0, v3

    .line 312984
    sput-object v0, LX/1m1;->a:LX/1m2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312985
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 312986
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 312987
    :cond_1
    sget-object v0, LX/1m1;->a:LX/1m2;

    return-object v0

    .line 312988
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 312989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 312977
    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v1

    check-cast v1, LX/0kb;

    const/16 v2, 0x12f4

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0Vw;->a(LX/0QB;)LX/0Vw;

    move-result-object v2

    check-cast v2, LX/0Vw;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0, v1, v4, v2, v3}, LX/19Y;->a(LX/0So;LX/0kb;LX/0Ot;LX/0Vw;LX/0Zb;)LX/1m2;

    move-result-object v0

    return-object v0
.end method
