.class public abstract LX/1Av;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TARGET:",
        "Ljava/lang/Object;",
        "TASK:",
        "LX/1Ax;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private c:Ljava/lang/Thread;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 211484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211485
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/1Av;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 211486
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/1Av;->b:Ljava/util/concurrent/CountDownLatch;

    .line 211487
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 211488
    return-void
.end method

.method public final a(LX/0oh;)V
    .locals 2

    .prologue
    .line 211473
    iget-object v0, p0, LX/1Av;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211474
    :goto_0
    return-void

    .line 211475
    :cond_0
    monitor-enter p0

    .line 211476
    :try_start_0
    iget-object v0, p0, LX/1Av;->c:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 211477
    monitor-exit p0

    goto :goto_0

    .line 211478
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 211479
    :cond_1
    :try_start_1
    new-instance v0, Lcom/facebook/common/init/Initializer$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/init/Initializer$1;-><init>(LX/1Av;LX/0oh;)V

    const v1, 0x6559158f

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/1Av;->c:Ljava/lang/Thread;

    .line 211480
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    .line 211481
    iget-object v1, p0, LX/1Av;->c:Ljava/lang/Thread;

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    .line 211482
    iget-object v0, p0, LX/1Av;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 211483
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final b(LX/0oh;)V
    .locals 3

    .prologue
    .line 211465
    iget-object v0, p0, LX/1Av;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211466
    :try_start_0
    const-string v0, "Initializer for %s"

    invoke-virtual {p0}, LX/1Av;->b()Ljava/lang/String;

    move-result-object v1

    const v2, -0x172e6b3f

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 211467
    :try_start_1
    invoke-virtual {p0}, LX/1Av;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211468
    const v0, 0x646c0bb8

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211469
    iget-object v0, p0, LX/1Av;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 211470
    :cond_0
    return-void

    .line 211471
    :catchall_0
    move-exception v0

    const v1, 0x2a93c577

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 211472
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/1Av;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method public abstract c()LX/1Ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTASK;"
        }
    .end annotation
.end method

.method public final c(LX/0oh;)V
    .locals 2

    .prologue
    .line 211453
    monitor-enter p0

    .line 211454
    :try_start_0
    iget-object v0, p0, LX/1Av;->c:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 211455
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    .line 211456
    iget-object v1, p0, LX/1Av;->c:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getPriority()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 211457
    iget-object v1, p0, LX/1Av;->c:Ljava/lang/Thread;

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    .line 211458
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211459
    invoke-virtual {p0, p1}, LX/1Av;->b(LX/0oh;)V

    .line 211460
    iget-object v0, p0, LX/1Av;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 211461
    iget-object v0, p0, LX/1Av;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    .line 211462
    iget-object v0, p0, LX/1Av;->d:Ljava/lang/Throwable;

    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 211463
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 211464
    :cond_1
    return-void
.end method
