.class public LX/1ls;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/1UO;


# instance fields
.field private final a:LX/C8t;

.field private final b:I

.field public final c:LX/1OP;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field private f:LX/1lD;

.field public g:LX/1DI;

.field private h:LX/C8u;


# direct methods
.method public constructor <init>(LX/C8t;ILX/1OP;)V
    .locals 0
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 312475
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 312476
    iput-object p1, p0, LX/1ls;->a:LX/C8t;

    .line 312477
    iput p2, p0, LX/1ls;->b:I

    .line 312478
    iput-object p3, p0, LX/1ls;->c:LX/1OP;

    .line 312479
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 312471
    iput-object v0, p0, LX/1ls;->d:Ljava/lang/String;

    .line 312472
    iput-object v0, p0, LX/1ls;->g:LX/1DI;

    .line 312473
    iput-object v0, p0, LX/1ls;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 312474
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 312443
    iget-object v0, p0, LX/1ls;->h:LX/C8u;

    if-eqz v0, :cond_0

    .line 312444
    iget-object v0, p0, LX/1ls;->f:LX/1lD;

    iget-object v1, p0, LX/1ls;->h:LX/C8u;

    .line 312445
    if-nez v0, :cond_1

    .line 312446
    :cond_0
    :goto_0
    return-void

    .line 312447
    :cond_1
    sget-object v2, LX/C8z;->a:[I

    invoke-virtual {v0}, LX/1lD;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 312448
    :pswitch_0
    iget-object v2, p0, LX/1ls;->c:LX/1OP;

    invoke-interface {v2}, LX/1OP;->ij_()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    .line 312449
    :goto_1
    iget-object v3, v1, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 312450
    if-eqz v2, :cond_4

    .line 312451
    iget-object v3, v1, LX/C8u;->b:Landroid/view/View;

    if-eqz v3, :cond_5

    .line 312452
    :goto_2
    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 312453
    :pswitch_1
    iget-object v2, v1, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 312454
    invoke-static {v1}, LX/C8u;->e(LX/C8u;)V

    .line 312455
    goto :goto_0

    .line 312456
    :pswitch_2
    iget-object v2, p0, LX/1ls;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    if-eqz v2, :cond_3

    .line 312457
    iget-object v2, p0, LX/1ls;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v3, p0, LX/1ls;->g:LX/1DI;

    .line 312458
    iget-object v4, v1, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v4, v2, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 312459
    invoke-static {v1}, LX/C8u;->e(LX/C8u;)V

    .line 312460
    goto :goto_0

    .line 312461
    :cond_3
    iget-object v2, p0, LX/1ls;->d:Ljava/lang/String;

    iget-object v3, p0, LX/1ls;->g:LX/1DI;

    .line 312462
    iget-object v4, v1, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v4, v2, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 312463
    invoke-static {v1}, LX/C8u;->e(LX/C8u;)V

    .line 312464
    goto :goto_0

    .line 312465
    :cond_4
    invoke-static {v1}, LX/C8u;->e(LX/C8u;)V

    goto :goto_2

    .line 312466
    :cond_5
    iget-object v3, v1, LX/C8u;->c:LX/C90;

    .line 312467
    iget-object v4, v3, LX/C90;->a:LX/C8t;

    sget-object p0, LX/C8v;->AUTO_GLOWING:LX/C8v;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v4, p0, v0, v2, v1}, LX/C8t;->a(LX/C8v;Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v4

    move-object v3, v4

    .line 312468
    iput-object v3, v1, LX/C8u;->b:Landroid/view/View;

    .line 312469
    iget-object v3, v1, LX/C8u;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, LX/C8u;->addView(Landroid/view/View;)V

    .line 312470
    iget-object v3, v1, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 312437
    new-instance v0, LX/C8u;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/C8u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1ls;->h:LX/C8u;

    .line 312438
    iget-object v0, p0, LX/1ls;->h:LX/C8u;

    iget v1, p0, LX/1ls;->b:I

    .line 312439
    iget-object v2, v0, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setContentLayout(I)V

    .line 312440
    iget-object v0, p0, LX/1ls;->h:LX/C8u;

    new-instance v1, LX/C90;

    iget-object v2, p0, LX/1ls;->a:LX/C8t;

    invoke-direct {v1, v2}, LX/C90;-><init>(LX/C8t;)V

    .line 312441
    iput-object v1, v0, LX/C8u;->c:LX/C90;

    .line 312442
    iget-object v0, p0, LX/1ls;->h:LX/C8u;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 312433
    invoke-direct {p0}, LX/1ls;->c()V

    .line 312434
    sget-object v0, LX/1lD;->LOADING:LX/1lD;

    iput-object v0, p0, LX/1ls;->f:LX/1lD;

    .line 312435
    invoke-direct {p0}, LX/1ls;->d()V

    .line 312436
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 312480
    invoke-direct {p0}, LX/1ls;->d()V

    .line 312481
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1DI;)V
    .locals 1

    .prologue
    .line 312427
    invoke-direct {p0}, LX/1ls;->c()V

    .line 312428
    sget-object v0, LX/1lD;->ERROR:LX/1lD;

    iput-object v0, p0, LX/1ls;->f:LX/1lD;

    .line 312429
    iput-object p1, p0, LX/1ls;->d:Ljava/lang/String;

    .line 312430
    iput-object p2, p0, LX/1ls;->g:LX/1DI;

    .line 312431
    invoke-direct {p0}, LX/1ls;->d()V

    .line 312432
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 312423
    invoke-direct {p0}, LX/1ls;->c()V

    .line 312424
    sget-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    iput-object v0, p0, LX/1ls;->f:LX/1lD;

    .line 312425
    invoke-direct {p0}, LX/1ls;->d()V

    .line 312426
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 312422
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 312421
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 312420
    const-wide/16 v0, 0x0

    return-wide v0
.end method
