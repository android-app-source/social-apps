.class public LX/1Ar;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0W3;

.field public final b:Z


# direct methods
.method private constructor <init>(LX/0W3;)V
    .locals 4
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 211417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211418
    iput-object p1, p0, LX/1Ar;->a:LX/0W3;

    .line 211419
    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iI:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iR:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/1Ar;->b:Z

    .line 211420
    return-void

    .line 211421
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1Ar;
    .locals 1

    .prologue
    .line 211416
    invoke-static {p0}, LX/1Ar;->b(LX/0QB;)LX/1Ar;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Ar;
    .locals 2

    .prologue
    .line 211414
    new-instance v1, LX/1Ar;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-direct {v1, v0}, LX/1Ar;-><init>(LX/0W3;)V

    .line 211415
    return-object v1
.end method


# virtual methods
.method public final b()Z
    .locals 4

    .prologue
    .line 211408
    iget-boolean v0, p0, LX/1Ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iL:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 211413
    iget-boolean v0, p0, LX/1Ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iQ:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 211412
    iget-boolean v0, p0, LX/1Ar;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iS:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iM:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 211411
    invoke-virtual {p0}, LX/1Ar;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iN:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 8

    .prologue
    .line 211409
    iget-boolean v4, p0, LX/1Ar;->b:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v6, LX/0X5;->iS:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v6, LX/0X5;->iO:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v0, v4

    .line 211410
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Ar;->a:LX/0W3;

    sget-wide v2, LX/0X5;->iP:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
