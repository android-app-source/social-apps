.class public LX/0k9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public m:I

.field public n:LX/0k8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0k8",
            "<TD;>;"
        }
    .end annotation
.end field

.field public o:Landroid/content/Context;

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126378
    iput-boolean v1, p0, LX/0k9;->p:Z

    .line 126379
    iput-boolean v1, p0, LX/0k9;->q:Z

    .line 126380
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k9;->r:Z

    .line 126381
    iput-boolean v1, p0, LX/0k9;->s:Z

    .line 126382
    iput-boolean v1, p0, LX/0k9;->t:Z

    .line 126383
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/0k9;->o:Landroid/content/Context;

    .line 126384
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 126376
    return-void
.end method

.method public final a(ILX/0k8;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0k8",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 126371
    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    if-eqz v0, :cond_0

    .line 126372
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126373
    :cond_0
    iput-object p2, p0, LX/0k9;->n:LX/0k8;

    .line 126374
    iput p1, p0, LX/0k9;->m:I

    .line 126375
    return-void
.end method

.method public final a(LX/0k8;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0k8",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 126365
    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    if-nez v0, :cond_0

    .line 126366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener register"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126367
    :cond_0
    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    if-eq v0, p1, :cond_1

    .line 126368
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to unregister the wrong listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126369
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/0k9;->n:LX/0k8;

    .line 126370
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 126355
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, LX/0k9;->m:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 126356
    const-string v0, " mListener="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 126357
    iget-boolean v0, p0, LX/0k9;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0k9;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0k9;->t:Z

    if-eqz v0, :cond_1

    .line 126358
    :cond_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->p:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126359
    const-string v0, " mContentChanged="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126360
    const-string v0, " mProcessingChange="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 126361
    :cond_1
    iget-boolean v0, p0, LX/0k9;->q:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/0k9;->r:Z

    if-eqz v0, :cond_3

    .line 126362
    :cond_2
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAbandoned="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->q:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126363
    const-string v0, " mReset="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->r:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 126364
    :cond_3
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 126352
    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    if-eqz v0, :cond_0

    .line 126353
    iget-object v0, p0, LX/0k9;->n:LX/0k8;

    invoke-interface {v0, p0, p1}, LX/0k8;->a(LX/0k9;Ljava/lang/Object;)V

    .line 126354
    :cond_0
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 126385
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 126351
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 126350
    return-void
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 126346
    iget-boolean v0, p0, LX/0k9;->s:Z

    .line 126347
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0k9;->s:Z

    .line 126348
    iget-boolean v1, p0, LX/0k9;->t:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, LX/0k9;->t:Z

    .line 126349
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126340
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 126341
    invoke-static {p0, v0}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 126342
    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126343
    iget v1, p0, LX/0k9;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 126344
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126345
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 126338
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0k9;->t:Z

    .line 126339
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 126334
    iget-boolean v0, p0, LX/0k9;->p:Z

    if-eqz v0, :cond_0

    .line 126335
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 126336
    :goto_0
    return-void

    .line 126337
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k9;->s:Z

    goto :goto_0
.end method
