.class public LX/1aw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1DQ;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/1ay;

.field private final d:LX/0gh;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9j7;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9jW;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1Kf;

.field public final i:LX/1Qh;

.field public final j:LX/1DQ;

.field public final k:LX/0ad;

.field public final l:LX/0Uh;

.field public final m:LX/1b0;

.field private final n:LX/1b1;

.field private final o:LX/121;

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Landroid/support/v4/app/FragmentActivity;

.field private final r:LX/0fO;

.field private final s:LX/1b2;

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 278997
    new-instance v0, LX/1ax;

    invoke-direct {v0}, LX/1ax;-><init>()V

    sput-object v0, LX/1aw;->a:LX/1DQ;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1ay;LX/0gh;LX/0Ot;LX/0Or;LX/0Ot;LX/1Kf;LX/0ad;LX/0Uh;LX/1b0;LX/1Qh;LX/1DQ;LX/1b1;LX/121;LX/0Ot;Landroid/support/v4/app/FragmentActivity;LX/0fO;LX/1b2;LX/0Ot;)V
    .locals 2
    .param p11    # LX/1Qh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/1DQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/1ay;",
            "LX/0gh;",
            "LX/0Ot",
            "<",
            "LX/9j7;",
            ">;",
            "LX/0Or",
            "<",
            "LX/9jW;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1Kf;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1b0;",
            "LX/1Qh;",
            "LX/1DQ;",
            "LX/1b1;",
            "LX/121;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0fO;",
            "LX/1b2;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 278976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278977
    iput-object p1, p0, LX/1aw;->b:LX/0Zb;

    .line 278978
    iput-object p2, p0, LX/1aw;->c:LX/1ay;

    .line 278979
    iput-object p3, p0, LX/1aw;->d:LX/0gh;

    .line 278980
    iput-object p4, p0, LX/1aw;->e:LX/0Ot;

    .line 278981
    iput-object p5, p0, LX/1aw;->f:LX/0Or;

    .line 278982
    iput-object p6, p0, LX/1aw;->g:LX/0Ot;

    .line 278983
    iput-object p7, p0, LX/1aw;->h:LX/1Kf;

    .line 278984
    iput-object p11, p0, LX/1aw;->i:LX/1Qh;

    .line 278985
    invoke-static {p12}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1DQ;

    iput-object v1, p0, LX/1aw;->j:LX/1DQ;

    .line 278986
    iput-object p8, p0, LX/1aw;->k:LX/0ad;

    .line 278987
    iput-object p9, p0, LX/1aw;->l:LX/0Uh;

    .line 278988
    iput-object p10, p0, LX/1aw;->m:LX/1b0;

    .line 278989
    iput-object p13, p0, LX/1aw;->n:LX/1b1;

    .line 278990
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1aw;->o:LX/121;

    .line 278991
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1aw;->p:LX/0Ot;

    .line 278992
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1aw;->q:Landroid/support/v4/app/FragmentActivity;

    .line 278993
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1aw;->r:LX/0fO;

    .line 278994
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1aw;->s:LX/1b2;

    .line 278995
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1aw;->t:LX/0Ot;

    .line 278996
    return-void
.end method

.method public static a(LX/1aw;Ljava/lang/String;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 278968
    iget-object v2, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v2}, LX/1Qh;->b()LX/21D;

    move-result-object v2

    invoke-static {v2, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v3, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v3}, LX/1Qh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v3, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v3}, LX/1Qh;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v3, p0, LX/1aw;->k:LX/0ad;

    sget-short v4, LX/1EB;->an:S

    invoke-interface {v3, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v3, p0, LX/1aw;->r:LX/0fO;

    invoke-virtual {v3}, LX/0fO;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/1aw;->r:LX/0fO;

    .line 278969
    invoke-static {v3}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v4

    invoke-virtual {v4}, LX/0i8;->t()Z

    move-result v4

    move v3, v4

    .line 278970
    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseInspirationCam(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 278971
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 278972
    invoke-static {p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 278973
    :cond_0
    iget-object v1, p0, LX/1aw;->j:LX/1DQ;

    invoke-interface {v1, v0}, LX/1DQ;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 278974
    return-object v0

    :cond_1
    move v0, v1

    .line 278975
    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 278965
    iget-object v0, p0, LX/1aw;->d:LX/0gh;

    invoke-virtual {v0, p2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 278966
    iget-object v0, p0, LX/1aw;->c:LX/1ay;

    const/16 v1, 0x6dc

    invoke-virtual {v0, p1, v1, p3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 278967
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278962
    iget-object v0, p0, LX/1aw;->d:LX/0gh;

    invoke-virtual {v0, p3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 278963
    iget-object v0, p0, LX/1aw;->h:LX/1Kf;

    const/16 v1, 0x6dc

    invoke-interface {v0, p1, p2, v1, p4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 278964
    return-void
.end method

.method public static a$redex0(LX/1aw;Landroid/content/Intent;ILandroid/app/Activity;)V
    .locals 1

    .prologue
    .line 278958
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 278959
    iget-object v0, p0, LX/1aw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 278960
    :goto_0
    return-void

    .line 278961
    :cond_0
    iget-object v0, p0, LX/1aw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 278944
    iget-object v0, p0, LX/1aw;->b:LX/0Zb;

    const-string v1, "checkin_button_clicked"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 278945
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278946
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 278947
    :cond_0
    iget-object v0, p0, LX/1aw;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jW;

    .line 278948
    new-instance v1, LX/9jo;

    invoke-direct {v1}, LX/9jo;-><init>()V

    sget-object v2, LX/9jC;->Checkin:LX/9jC;

    .line 278949
    iput-object v2, v1, LX/9jo;->g:LX/9jC;

    .line 278950
    move-object v1, v1

    .line 278951
    invoke-virtual {v0, v1}, LX/9jW;->a(LX/9jo;)V

    .line 278952
    iget-object v0, p0, LX/1aw;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9j7;

    const p0, 0x150016

    .line 278953
    iget-object v1, v0, LX/9j7;->b:LX/0id;

    const-string v2, "CheckInButton"

    invoke-virtual {v1, v2}, LX/0id;->a(Ljava/lang/String;)V

    .line 278954
    iget-object v1, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x150006

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 278955
    iget-object v1, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 278956
    sget-object v1, LX/9jG;->CHECKIN:LX/9jG;

    invoke-static {v0, p0, v1}, LX/9j7;->a(LX/9j7;ILX/9jG;)V

    .line 278957
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 278936
    iget-object v0, p0, LX/1aw;->n:LX/1b1;

    .line 278937
    new-instance v1, LX/9J8;

    iget-object v2, v0, LX/1b1;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/9J8;-><init>(Landroid/content/Context;)V

    .line 278938
    new-instance v2, LX/B0D;

    invoke-direct {v2, v0, v1}, LX/B0D;-><init>(LX/1b1;LX/9J8;)V

    .line 278939
    iget-object p0, v1, LX/9J8;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278940
    new-instance v2, LX/B0E;

    invoke-direct {v2, v0, v1}, LX/B0E;-><init>(LX/1b1;LX/9J8;)V

    .line 278941
    iget-object p0, v1, LX/9J8;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278942
    invoke-virtual {v1}, LX/9J8;->show()V

    .line 278943
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 278859
    const/4 v2, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, LX/1aw;->a(Landroid/app/Activity;ILjava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 278860
    return-void
.end method

.method public final a(Landroid/app/Activity;ILjava/lang/String;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 278912
    iget-object v0, p0, LX/1aw;->s:LX/1b2;

    invoke-virtual {v0, p1}, LX/1b2;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 278913
    new-instance v0, LX/AWP;

    invoke-direct {v0}, LX/AWP;-><init>()V

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 278914
    iput-object v3, v0, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 278915
    move-object v0, v0

    .line 278916
    iget-object v3, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v3}, LX/1Qh;->b()LX/21D;

    move-result-object v3

    .line 278917
    iput-object v3, v0, LX/AWP;->k:LX/21D;

    .line 278918
    move-object v0, v0

    .line 278919
    iput-object p3, v0, LX/AWP;->a:Ljava/lang/String;

    .line 278920
    move-object v3, v0

    .line 278921
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    .line 278922
    :goto_0
    iput-object v0, v3, LX/AWP;->d:Ljava/lang/String;

    .line 278923
    move-object v0, v3

    .line 278924
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 278925
    :cond_0
    iput-object v1, v0, LX/AWP;->e:Ljava/lang/String;

    .line 278926
    move-object v0, v0

    .line 278927
    invoke-virtual {v0}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v0

    .line 278928
    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, p5}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZLcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Landroid/os/Bundle;

    move-result-object v0

    .line 278929
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 278930
    iget-object v0, p0, LX/1aw;->o:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278931
    iget-object v0, p0, LX/1aw;->o:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080e4a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/AnO;

    invoke-direct {v4, p0, v2, p2, p1}, LX/AnO;-><init>(LX/1aw;Landroid/content/Intent;ILandroid/app/Activity;)V

    invoke-virtual {v0, v1, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 278932
    iget-object v0, p0, LX/1aw;->o:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/1aw;->q:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 278933
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 278934
    goto :goto_0

    .line 278935
    :cond_2
    invoke-static {p0, v2, p2, p1}, LX/1aw;->a$redex0(LX/1aw;Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1
.end method

.method public final a(Landroid/app/Activity;LX/B68;)V
    .locals 2

    .prologue
    .line 278909
    invoke-static {p1, p2}, Lcom/facebook/ipc/storyline/StorylineIntent;->a(Landroid/content/Context;LX/B68;)Landroid/content/Intent;

    move-result-object v1

    .line 278910
    iget-object v0, p0, LX/1aw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 278911
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278887
    iget-object v0, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v0}, LX/1Qh;->b()LX/21D;

    move-result-object v0

    invoke-static {v0, p3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v1}, LX/1Qh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1aw;->k:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 278888
    iget-object v1, p0, LX/1aw;->j:LX/1DQ;

    invoke-interface {v1, v0}, LX/1DQ;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 278889
    move-object v0, v0

    .line 278890
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 278891
    new-instance v1, LX/8AA;

    iget-object v2, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v2}, LX/1Qh;->d()LX/8AB;

    move-result-object v2

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    .line 278892
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 278893
    move-object v1, v1

    .line 278894
    invoke-virtual {v1}, LX/8AA;->p()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->q()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->c()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->e()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->s()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    move-result-object v2

    .line 278895
    iget-object v1, p0, LX/1aw;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278896
    invoke-virtual {v2}, LX/8AA;->g()LX/8AA;

    .line 278897
    iget-object v1, p0, LX/1aw;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278898
    invoke-virtual {v2}, LX/8AA;->h()LX/8AA;

    .line 278899
    :cond_0
    iget-object v1, p0, LX/1aw;->l:LX/0Uh;

    const/16 v3, 0x267

    const/4 p3, 0x0

    invoke-virtual {v1, v3, p3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 278900
    invoke-virtual {v2}, LX/8AA;->v()LX/8AA;

    .line 278901
    :cond_1
    invoke-static {p2, v2, p1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 278902
    const-string v1, "tap_photo_button"

    invoke-direct {p0, v0, v1, p2}, LX/1aw;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V

    .line 278903
    iget-object v0, p0, LX/1aw;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jW;

    .line 278904
    new-instance v1, LX/9jo;

    invoke-direct {v1}, LX/9jo;-><init>()V

    sget-object v2, LX/9jC;->Photo:LX/9jC;

    .line 278905
    iput-object v2, v1, LX/9jo;->g:LX/9jC;

    .line 278906
    move-object v1, v1

    .line 278907
    invoke-virtual {v0, v1}, LX/9jW;->a(LX/9jo;)V

    .line 278908
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "Landroid/app/Activity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 278884
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278885
    invoke-static {p0, p2, p3}, LX/1aw;->a(LX/1aw;Ljava/lang/String;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const-string v1, "tap_photo_tray"

    invoke-direct {p0, p1, v0, v1, p5}, LX/1aw;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;Landroid/app/Activity;)V

    .line 278886
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278880
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 278881
    invoke-static {p0, p2, v0}, LX/1aw;->a(LX/1aw;Ljava/lang/String;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 278882
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const-string v1, "tap_status_button"

    invoke-direct {p0, p1, v0, v1, p4}, LX/1aw;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;Landroid/app/Activity;)V

    .line 278883
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278861
    invoke-direct {p0}, LX/1aw;->b()V

    .line 278862
    iget-object v0, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v0}, LX/1Qh;->b()LX/21D;

    move-result-object v0

    invoke-static {v0, p3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v1}, LX/1Qh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1aw;->i:LX/1Qh;

    invoke-interface {v1}, LX/1Qh;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1aw;->k:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 278863
    iget-object v1, p0, LX/1aw;->j:LX/1DQ;

    invoke-interface {v1, v0}, LX/1DQ;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 278864
    move-object v0, v0

    .line 278865
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/4 v3, 0x1

    .line 278866
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v1

    sget-object v2, LX/9jG;->CHECKIN:LX/9jG;

    .line 278867
    iput-object v2, v1, LX/9jF;->q:LX/9jG;

    .line 278868
    move-object v1, v1

    .line 278869
    iput-boolean v3, v1, LX/9jF;->k:Z

    .line 278870
    move-object v1, v1

    .line 278871
    iput-object v0, v1, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 278872
    move-object v1, v1

    .line 278873
    iput-boolean v3, v1, LX/9jF;->t:Z

    .line 278874
    move-object v1, v1

    .line 278875
    if-eqz p1, :cond_0

    .line 278876
    iput-object p1, v1, LX/9jF;->g:Ljava/lang/String;

    .line 278877
    :cond_0
    invoke-virtual {v1}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v1

    invoke-static {p2, v1}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 278878
    const-string v1, "tap_check_in_button"

    invoke-direct {p0, v0, v1, p2}, LX/1aw;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/app/Activity;)V

    .line 278879
    return-void
.end method
