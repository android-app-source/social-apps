.class public LX/15W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0iA;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/0iA;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180827
    iput-object p1, p0, LX/15W;->a:LX/0iA;

    .line 180828
    iput-object p2, p0, LX/15W;->b:Lcom/facebook/content/SecureContextHelper;

    .line 180829
    return-void
.end method

.method public static a(LX/0QB;)LX/15W;
    .locals 1

    .prologue
    .line 180825
    invoke-static {p0}, LX/15W;->b(LX/0QB;)LX/15W;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;ZLjava/lang/Object;)Z
    .locals 4
    .param p5    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 180830
    instance-of v1, p2, LX/16E;

    if-eqz v1, :cond_0

    .line 180831
    check-cast p2, LX/16E;

    invoke-interface {p2, p1, p5}, LX/16E;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 180832
    :goto_0
    return v0

    .line 180833
    :cond_0
    instance-of v1, p2, LX/13G;

    if-eqz v1, :cond_3

    .line 180834
    check-cast p2, LX/13G;

    invoke-interface {p2, p1}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 180835
    if-nez v1, :cond_1

    .line 180836
    const/4 v0, 0x0

    goto :goto_0

    .line 180837
    :cond_1
    if-eqz p4, :cond_2

    .line 180838
    const/high16 v2, 0x10000000

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v3

    or-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 180839
    :cond_2
    iget-object v2, p0, LX/15W;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 180840
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown InterstitialController: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(LX/0QB;)LX/15W;
    .locals 3

    .prologue
    .line 180823
    new-instance v2, LX/15W;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/15W;-><init>(LX/0iA;Lcom/facebook/content/SecureContextHelper;)V

    .line 180824
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 180817
    iget-object v0, p0, LX/15W;->a:LX/0iA;

    invoke-virtual {v0, p2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;

    move-result-object v2

    .line 180818
    if-nez v2, :cond_0

    .line 180819
    :goto_0
    return v4

    .line 180820
    :cond_0
    iget-object v0, p0, LX/15W;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-interface {v2}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 180821
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/15W;->a(Landroid/content/Context;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;ZLjava/lang/Object;)Z

    .line 180822
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z
    .locals 6
    .param p4    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0i1;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 180811
    iget-object v0, p0, LX/15W;->a:LX/0iA;

    invoke-virtual {v0, p2, p3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    .line 180812
    if-nez v2, :cond_0

    .line 180813
    const/4 v0, 0x0

    .line 180814
    :goto_0
    return v0

    .line 180815
    :cond_0
    iget-object v0, p0, LX/15W;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-interface {v2}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 180816
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/15W;->a(Landroid/content/Context;LX/0i1;Lcom/facebook/interstitial/manager/InterstitialTrigger;ZLjava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
