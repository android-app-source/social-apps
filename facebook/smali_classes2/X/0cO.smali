.class public final LX/0cO;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/0cI;


# direct methods
.method public constructor <init>(LX/0cI;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 88245
    iput-object p1, p0, LX/0cO;->a:LX/0cI;

    .line 88246
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 88247
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 88248
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 88249
    iget-object v0, p0, LX/0cO;->a:LX/0cI;

    invoke-static {v0, p1}, LX/0cI;->c(LX/0cI;Landroid/os/Message;)V

    .line 88250
    :cond_0
    :goto_0
    return-void

    .line 88251
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 88252
    invoke-static {v0}, LX/0cK;->a(Landroid/os/Bundle;)LX/0cK;

    move-result-object v0

    .line 88253
    iget-object v1, p0, LX/0cO;->a:LX/0cI;

    iget-object v1, v1, LX/0cI;->k:Ljava/util/concurrent/ConcurrentMap;

    iget v2, v0, LX/0cK;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88254
    iget-object v1, p0, LX/0cO;->a:LX/0cI;

    sget-object v2, LX/1gR;->Outgoing:LX/1gR;

    invoke-static {v1, v0, v2}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;LX/1gR;)V

    goto :goto_0

    .line 88255
    :pswitch_1
    iget-object v0, p0, LX/0cO;->a:LX/0cI;

    invoke-static {v0, p1}, LX/0cI;->b(LX/0cI;Landroid/os/Message;)LX/0cK;

    move-result-object v0

    .line 88256
    if-eqz v0, :cond_0

    .line 88257
    iget-object v1, p0, LX/0cO;->a:LX/0cI;

    invoke-static {v1, v0}, LX/0cI;->a$redex0(LX/0cI;LX/0cK;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
