.class public LX/15F;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/String;

.field public volatile c:LX/15I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile d:Lcom/facebook/http/interfaces/RequestPriority;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile e:Lcom/facebook/http/interfaces/RequestStage;

.field public volatile f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180362
    const-class v0, LX/15F;

    sput-object v0, LX/15F;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 180357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180358
    sget-object v0, Lcom/facebook/http/interfaces/RequestStage;->CREATED:Lcom/facebook/http/interfaces/RequestStage;

    iput-object v0, p0, LX/15F;->e:Lcom/facebook/http/interfaces/RequestStage;

    .line 180359
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/15F;->b:Ljava/lang/String;

    .line 180360
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, LX/15F;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 180361
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0

    .prologue
    .line 180363
    if-eqz p1, :cond_0

    .line 180364
    iput-object p1, p0, LX/15F;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 180365
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 1

    .prologue
    .line 180352
    const-string v0, "Cannot change priority to null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180353
    iput-object p1, p0, LX/15F;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 180354
    iget-object v0, p0, LX/15F;->c:LX/15I;

    if-eqz v0, :cond_0

    .line 180355
    iget-object v0, p0, LX/15F;->c:LX/15I;

    invoke-interface {v0, p1}, LX/15I;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 180356
    :cond_0
    return-void
.end method
