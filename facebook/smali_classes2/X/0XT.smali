.class public LX/0XT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/00j;


# static fields
.field private static a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78810
    const/4 v0, 0x0

    sput-boolean v0, LX/0XT;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 78812
    const-class v1, LX/0XT;

    monitor-enter v1

    .line 78813
    const-wide/32 v2, 0x40000

    :try_start_0
    invoke-static {v2, v3}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, LX/0XT;->a:Z

    if-nez v0, :cond_1

    .line 78814
    const-string v0, "java.vm.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78815
    if-eqz v0, :cond_0

    const-string v2, "0."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "1."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78816
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "ART not supported."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78817
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 78818
    :cond_0
    :try_start_1
    invoke-static {}, LX/4h5;->a()V

    .line 78819
    const/4 v0, 0x1

    sput-boolean v0, LX/0XT;->a:Z

    .line 78820
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 78821
    const-class v1, LX/0XT;

    monitor-enter v1

    .line 78822
    :try_start_0
    sget-boolean v0, LX/0XT;->a:Z

    if-eqz v0, :cond_0

    .line 78823
    invoke-static {}, LX/4h5;->b()V

    .line 78824
    const/4 v0, 0x0

    sput-boolean v0, LX/0XT;->a:Z

    .line 78825
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
