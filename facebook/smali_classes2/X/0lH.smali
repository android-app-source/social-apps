.class public final LX/0lH;
.super LX/0lI;
.source ""


# static fields
.field private static final serialVersionUID:J = -0xb1b80aa96a43468L


# instance fields
.field public final _typeNames:[Ljava/lang/String;

.field public final _typeParameters:[LX/0lJ;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 128819
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 128820
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/String;",
            "[",
            "LX/0lJ;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 128759
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, LX/0lI;-><init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 128760
    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 128761
    :cond_0
    iput-object v6, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    .line 128762
    iput-object v6, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    .line 128763
    :goto_0
    return-void

    .line 128764
    :cond_1
    iput-object p2, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    .line 128765
    iput-object p3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    goto :goto_0
.end method

.method private e(Ljava/lang/Object;)LX/0lH;
    .locals 7

    .prologue
    .line 128818
    new-instance v0, LX/0lH;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    iget-object v3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private f(Ljava/lang/Object;)LX/0lH;
    .locals 7

    .prologue
    .line 128816
    iget-object v0, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 128817
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0lH;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    iget-object v3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static h(Ljava/lang/Class;)LX/0lH;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lH;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 128815
    new-instance v0, LX/0lH;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private static w()LX/0lH;
    .locals 2

    .prologue
    .line 128814
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Simple types have no content types; can not call withContenValueHandler()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private x()LX/0lH;
    .locals 7

    .prologue
    .line 128813
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0lH;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    iget-object v3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/0lJ;
    .locals 1

    .prologue
    .line 128810
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 128811
    :cond_0
    const/4 v0, 0x0

    .line 128812
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 128809
    invoke-direct {p0, p1}, LX/0lH;->e(Ljava/lang/Object;)LX/0lH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 128808
    invoke-direct {p0}, LX/0lH;->x()LX/0lH;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/0lJ;
    .locals 2

    .prologue
    .line 128807
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Simple types have no content types; can not call withContenTypeHandler()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128804
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 128805
    :cond_0
    const/4 v0, 0x0

    .line 128806
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 128803
    invoke-direct {p0, p1}, LX/0lH;->f(Ljava/lang/Object;)LX/0lH;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128802
    new-instance v0, LX/0lH;

    iget-object v2, p0, LX/0lH;->_typeNames:[Ljava/lang/String;

    iget-object v3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 128801
    invoke-static {}, LX/0lH;->w()LX/0lH;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Class;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128800
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Internal error: SimpleType.narrowContentsBy() should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 128784
    if-ne p1, p0, :cond_1

    .line 128785
    :cond_0
    :goto_0
    return v0

    .line 128786
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 128787
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 128788
    :cond_3
    check-cast p1, LX/0lH;

    .line 128789
    iget-object v2, p1, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v3, p0, LX/0lJ;->_class:Ljava/lang/Class;

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 128790
    :cond_4
    iget-object v3, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    .line 128791
    iget-object v4, p1, LX/0lH;->_typeParameters:[LX/0lJ;

    .line 128792
    if-nez v3, :cond_5

    .line 128793
    if-eqz v4, :cond_0

    array-length v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 128794
    :cond_5
    if-nez v4, :cond_6

    move v0, v1

    goto :goto_0

    .line 128795
    :cond_6
    array-length v2, v3

    array-length v5, v4

    if-eq v2, v5, :cond_7

    move v0, v1

    goto :goto_0

    .line 128796
    :cond_7
    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_0

    .line 128797
    aget-object v6, v3, v2

    aget-object v7, v4, v2

    invoke-virtual {v6, v7}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    move v0, v1

    .line 128798
    goto :goto_0

    .line 128799
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public final f(Ljava/lang/Class;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 128783
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Internal error: SimpleType.widenContentsBy() should never be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 128782
    const/4 v0, 0x0

    return v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 128781
    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    array-length v0, v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128778
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 128779
    const-string v1, "[simple type, class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/0lH;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 128780
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 128766
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 128767
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128768
    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 128769
    const/16 v0, 0x3c

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 128770
    const/4 v0, 0x1

    .line 128771
    iget-object v4, p0, LX/0lH;->_typeParameters:[LX/0lJ;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 128772
    if-eqz v0, :cond_0

    move v0, v1

    .line 128773
    :goto_1
    invoke-virtual {v6}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128774
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 128775
    :cond_0
    const/16 v7, 0x2c

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 128776
    :cond_1
    const/16 v0, 0x3e

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 128777
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
