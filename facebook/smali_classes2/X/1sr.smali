.class public LX/1sr;
.super LX/1ss;
.source ""


# instance fields
.field public a:Ljava/io/InputStream;

.field public b:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 335371
    invoke-direct {p0}, LX/1ss;-><init>()V

    .line 335372
    iput-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    .line 335373
    iput-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    .line 335374
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 335375
    invoke-direct {p0}, LX/1ss;-><init>()V

    .line 335376
    iput-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    .line 335377
    iput-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    .line 335378
    iput-object p1, p0, LX/1sr;->a:Ljava/io/InputStream;

    .line 335379
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 335380
    invoke-direct {p0}, LX/1ss;-><init>()V

    .line 335381
    iput-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    .line 335382
    iput-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    .line 335383
    iput-object p1, p0, LX/1sr;->b:Ljava/io/OutputStream;

    .line 335384
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 3

    .prologue
    .line 335385
    iget-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 335386
    new-instance v0, LX/7H6;

    const/4 v1, 0x1

    const-string v2, "Cannot read from null inputStream"

    invoke-direct {v0, v1, v2}, LX/7H6;-><init>(ILjava/lang/String;)V

    throw v0

    .line 335387
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 335388
    if-gez v0, :cond_1

    .line 335389
    new-instance v0, LX/7H6;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/7H6;-><init>(I)V

    throw v0

    .line 335390
    :catch_0
    move-exception v0

    .line 335391
    new-instance v1, LX/7H6;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, LX/7H6;-><init>(ILjava/lang/Throwable;)V

    throw v1

    .line 335392
    :cond_1
    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 335393
    iget-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 335394
    :try_start_0
    iget-object v0, p0, LX/1sr;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335395
    iput-object v1, p0, LX/1sr;->a:Ljava/io/InputStream;

    .line 335396
    :cond_0
    iget-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 335397
    :try_start_1
    iget-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 335398
    iput-object v1, p0, LX/1sr;->b:Ljava/io/OutputStream;

    .line 335399
    :cond_1
    return-void

    .line 335400
    :catch_0
    move-exception v0

    .line 335401
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error closing input stream."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 335402
    :catch_1
    move-exception v0

    .line 335403
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error closing output stream."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b([BII)V
    .locals 3

    .prologue
    .line 335404
    iget-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 335405
    new-instance v0, LX/7H6;

    const/4 v1, 0x1

    const-string v2, "Cannot write to null outputStream"

    invoke-direct {v0, v1, v2}, LX/7H6;-><init>(ILjava/lang/String;)V

    throw v0

    .line 335406
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1sr;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335407
    return-void

    .line 335408
    :catch_0
    move-exception v0

    .line 335409
    new-instance v1, LX/7H6;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, LX/7H6;-><init>(ILjava/lang/Throwable;)V

    throw v1
.end method
