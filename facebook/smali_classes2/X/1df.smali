.class public LX/1df;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<TVH;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TVH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TVH;>;)V"
        }
    .end annotation

    .prologue
    .line 286522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286523
    iput-object p1, p0, LX/1df;->e:Ljava/util/concurrent/Callable;

    .line 286524
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/1df;->d:Ljava/util/Stack;

    .line 286525
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 286526
    iget v0, p0, LX/1df;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1df;->a:I

    .line 286527
    :try_start_0
    iget-object v0, p0, LX/1df;->e:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 286528
    if-nez v0, :cond_0

    .line 286529
    :goto_0
    return-void

    .line 286530
    :cond_0
    iget-object v1, p0, LX/1df;->d:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 286531
    :catch_0
    move-exception v0

    .line 286532
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
