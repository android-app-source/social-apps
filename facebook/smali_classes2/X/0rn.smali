.class public abstract LX/0rn;
.super LX/0ro;
.source ""

# interfaces
.implements LX/0rp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/api/feed/FetchFeedParams;",
        "Lcom/facebook/api/feed/FetchFeedResult;",
        ">;",
        "LX/0rp",
        "<",
        "Lcom/facebook/api/feed/FetchFeedParams;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:LX/0sZ;

.field public final c:LX/0SG;

.field private final d:LX/0sO;

.field private final e:LX/03V;

.field private final f:LX/0Yl;

.field private final g:LX/0ad;

.field private final h:LX/0So;


# direct methods
.method public constructor <init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V
    .locals 0

    .prologue
    .line 150852
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 150853
    iput-object p1, p0, LX/0rn;->d:LX/0sO;

    .line 150854
    iput-object p2, p0, LX/0rn;->e:LX/03V;

    .line 150855
    iput-object p3, p0, LX/0rn;->f:LX/0Yl;

    .line 150856
    iput-object p4, p0, LX/0rn;->c:LX/0SG;

    .line 150857
    iput-object p5, p0, LX/0rn;->h:LX/0So;

    .line 150858
    iput-object p6, p0, LX/0rn;->g:LX/0ad;

    .line 150859
    iput-object p7, p0, LX/0rn;->b:LX/0sZ;

    .line 150860
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V
    .locals 2
    .param p3    # Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 150861
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 150862
    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    .line 150863
    :cond_0
    if-eqz p3, :cond_1

    if-eqz v1, :cond_1

    if-nez v0, :cond_3

    .line 150864
    :cond_1
    iget-object v0, p0, LX/0rn;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 150865
    new-instance v0, LX/55O;

    const-string v1, "Response contained null stories or page info"

    invoke-direct {v0, v1}, LX/55O;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v1, v0

    .line 150866
    goto :goto_0

    .line 150867
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150868
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 150869
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 150870
    invoke-virtual {p0, v0}, LX/0rn;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v4

    .line 150871
    new-instance v5, LX/1u8;

    invoke-direct {v5}, LX/1u8;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    .line 150872
    iput-object v6, v5, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 150873
    move-object v5, v5

    .line 150874
    iput-object v4, v5, LX/1u8;->d:Ljava/lang/String;

    .line 150875
    move-object v4, v5

    .line 150876
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    .line 150877
    iput-object v5, v4, LX/1u8;->i:Ljava/lang/String;

    .line 150878
    move-object v4, v4

    .line 150879
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 150880
    iput-object v0, v4, LX/1u8;->c:Ljava/lang/String;

    .line 150881
    move-object v0, v4

    .line 150882
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 150883
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 150884
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;
    .locals 4

    .prologue
    .line 150885
    new-instance v0, LX/0v6;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_single"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 150886
    invoke-virtual {p0, p1}, LX/0ro;->f(Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    .line 150887
    const-class v2, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, LX/0ro;->b(Ljava/lang/Object;LX/1pN;)I

    move-result v3

    .line 150888
    invoke-static {v1, v2}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object p1

    .line 150889
    iput v3, p1, LX/0zO;->A:I

    .line 150890
    move-object v1, p1

    .line 150891
    invoke-virtual {v0, v1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v2

    .line 150892
    const-string v3, "feed_subscriber"

    invoke-interface {p3, v3}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 150893
    goto :goto_0

    .line 150894
    :goto_0
    return-object v0
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 10

    .prologue
    .line 150895
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, LX/0rn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_deserialize"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 150896
    invoke-virtual {p0, p1}, LX/0rn;->d(Lcom/facebook/api/feed/FetchFeedParams;)I

    move-result v2

    .line 150897
    :try_start_0
    iget-object v0, p0, LX/0rn;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    .line 150898
    invoke-virtual {p0, p1, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v9

    .line 150899
    const-string v0, "%s_%d"

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v8, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 150900
    iget-object v1, p0, LX/0rn;->f:LX/0Yl;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->e(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 150901
    iget-object v0, p0, LX/0rn;->f:LX/0Yl;

    invoke-virtual {v0, v2, v3}, LX/0Yl;->i(ILjava/lang/String;)LX/0Yl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150902
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Server result "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150903
    const-string v0, "fetch_feed_server_failure"

    invoke-direct {p0, v0, p1, v9}, LX/0rn;->a(Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V

    .line 150904
    iget-object v0, p0, LX/0rn;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 150905
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 150906
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 150907
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 150908
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 150909
    :catch_0
    move-exception v0

    .line 150910
    iget-object v1, p0, LX/0rn;->f:LX/0Yl;

    invoke-virtual {v1, v2, v8}, LX/0Yl;->g(ILjava/lang/String;)LX/0Yl;

    .line 150911
    throw v0

    .line 150912
    :cond_1
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    new-instance v1, LX/0rT;

    invoke-direct {v1}, LX/0rT;-><init>()V

    invoke-virtual {v1, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v1

    invoke-virtual {v1}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v1

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 150913
    iget-boolean v2, p2, LX/1pN;->f:Z

    move v6, v2

    .line 150914
    move-object v2, v9

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    return-object v0
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;LX/15w;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 150915
    iget-object v0, p0, LX/0rn;->d:LX/0sO;

    const-class v1, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    invoke-virtual {p0}, LX/0rn;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, LX/0sO;->a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 150916
    return-object v0
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 150917
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 150918
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    if-eqz v1, :cond_0

    .line 150919
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    return-object v0

    .line 150920
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized GraphQLResult:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 150921
    const-string v0, "exception_name"

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    .line 150922
    iget-object v1, p0, LX/0rn;->f:LX/0Yl;

    invoke-virtual {p0, p1}, LX/0rn;->d(Lcom/facebook/api/feed/FetchFeedParams;)I

    move-result v2

    invoke-virtual {p0, p1}, LX/0rn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/0rn;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->d(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 150923
    return-object v4
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150924
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150848
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 150849
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 150850
    :goto_0
    invoke-static {p1}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 150851
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0v6;LX/0zO;LX/0tW;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 150831
    iget-object v0, p0, LX/0rn;->g:LX/0ad;

    sget-short v1, LX/0fe;->u:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 150832
    const-string v1, "feedback_subscriber"

    invoke-interface {p3, v1}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v1

    .line 150833
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 150834
    const-string v0, "feedback_prefetch_id"

    sget-object v2, LX/4Zz;->EACH:LX/4Zz;

    sget-object v3, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {p2, v0, v2, v3}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v2

    .line 150835
    const/4 v0, 0x0

    .line 150836
    iget-object v3, p0, LX/0rn;->g:LX/0ad;

    sget-short v4, LX/0fe;->x:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150837
    const-string v0, "focused_comment_id"

    sget-object v3, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v4, LX/4a0;->ALLOW:LX/4a0;

    invoke-virtual {p2, v0, v3, v4}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v0

    .line 150838
    :cond_0
    invoke-static {}, LX/0uj;->h()Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    move-result-object v3

    .line 150839
    iget-object v4, p0, LX/0rn;->b:LX/0sZ;

    .line 150840
    invoke-static {v4, v3}, LX/0sZ;->b(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0gW;

    move-result-object v5

    .line 150841
    const-string p0, "feedback_id"

    invoke-virtual {v5, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 150842
    if-eqz v0, :cond_1

    .line 150843
    const-string p0, "comment_id"

    invoke-virtual {v5, p0, v0}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 150844
    :cond_1
    invoke-static {v4, v3, v5}, LX/0sZ;->a(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;LX/0gW;)LX/0zO;

    move-result-object v5

    move-object v0, v5

    .line 150845
    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 150846
    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 150847
    :cond_2
    return-void
.end method

.method public a(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 3

    .prologue
    .line 150829
    iget-object v0, p0, LX/0rn;->f:LX/0Yl;

    invoke-virtual {p0, p1}, LX/0rn;->d(Lcom/facebook/api/feed/FetchFeedParams;)I

    move-result v1

    invoke-virtual {p0, p1}, LX/0rn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yl;->h(ILjava/lang/String;)LX/0Yl;

    .line 150830
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedHomeStories;)V
    .locals 1

    .prologue
    .line 150827
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0rn;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->a(LX/0Px;)V

    .line 150828
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 150826
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    return-void
.end method

.method public b(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 3

    .prologue
    .line 150824
    iget-object v0, p0, LX/0rn;->f:LX/0Yl;

    invoke-virtual {p0, p1}, LX/0rn;->d(Lcom/facebook/api/feed/FetchFeedParams;)I

    move-result v1

    invoke-virtual {p0, p1}, LX/0rn;->c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yl;->i(ILjava/lang/String;)LX/0Yl;

    .line 150825
    return-void
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
.end method

.method public synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 150823
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rn;->b(Lcom/facebook/api/feed/FetchFeedParams;)V

    return-void
.end method

.method public abstract d(Lcom/facebook/api/feed/FetchFeedParams;)I
.end method

.method public final i(Ljava/lang/Object;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 2

    .prologue
    .line 150815
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 150816
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 150817
    invoke-virtual {v0}, LX/0gf;->isAutoRefresh()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150818
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 150819
    :goto_0
    return-object v0

    .line 150820
    :cond_0
    sget-object v1, LX/0gf;->PREFETCH:LX/0gf;

    if-ne v0, v1, :cond_1

    .line 150821
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0

    .line 150822
    :cond_1
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
