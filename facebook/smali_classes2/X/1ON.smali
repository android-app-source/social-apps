.class public LX/1ON;
.super LX/1OM;
.source ""

# interfaces
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "LX/1OM",
        "<",
        "LX/1a0;",
        ">;",
        "LX/1OO",
        "<",
        "LX/1a0;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1OO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1OO",
            "<TVH;>;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private final e:LX/1OD;


# direct methods
.method public constructor <init>(LX/1OO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1OO",
            "<TVH;>;)V"
        }
    .end annotation

    .prologue
    .line 240615
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 240616
    new-instance v0, LX/1YE;

    invoke-direct {v0, p0}, LX/1YE;-><init>(LX/1ON;)V

    iput-object v0, p0, LX/1ON;->e:LX/1OD;

    .line 240617
    iput-object p1, p0, LX/1ON;->a:LX/1OO;

    .line 240618
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1ON;->b:Ljava/util/List;

    .line 240619
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1ON;->c:Ljava/util/List;

    .line 240620
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v0}, LX/1OQ;->eC_()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 240621
    return-void
.end method

.method private static f(I)I
    .locals 1

    .prologue
    .line 240622
    mul-int/lit8 v0, p0, 0x2

    rsub-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private g(I)I
    .locals 2

    .prologue
    .line 240623
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    sub-int v0, p1, v0

    iget-object v1, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 240624
    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, -0x2

    return v0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 240625
    iget-object v0, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 240626
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    .line 240627
    if-ge p1, v0, :cond_0

    .line 240628
    invoke-static {p1}, LX/1ON;->f(I)I

    move-result v0

    int-to-long v0, v0

    .line 240629
    :goto_0
    return-wide v0

    .line 240630
    :cond_0
    sub-int v0, p1, v0

    .line 240631
    if-lt v0, v1, :cond_1

    .line 240632
    invoke-direct {p0, p1}, LX/1ON;->g(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 240633
    :cond_1
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1, v0}, LX/1OQ;->C_(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 240634
    if-gez p2, :cond_0

    .line 240635
    rem-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    .line 240636
    neg-int v0, p2

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    .line 240637
    iget-object v1, p0, LX/1ON;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 240638
    :goto_0
    move-object v1, v0

    .line 240639
    new-instance v0, LX/1a0;

    invoke-direct {v0, v1}, LX/1a0;-><init>(Landroid/view/View;)V

    .line 240640
    :goto_1
    return-object v0

    :cond_0
    new-instance v0, LX/1a0;

    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1, p1, p2}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1a0;-><init>(LX/1a1;)V

    goto :goto_1

    .line 240641
    :cond_1
    add-int/lit8 v0, p2, 0x1

    div-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 240642
    iget-object v1, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public final a(LX/1OD;)V
    .locals 2

    .prologue
    .line 240643
    invoke-super {p0, p1}, LX/1OM;->a(LX/1OD;)V

    .line 240644
    iget-boolean v0, p0, LX/1ON;->d:Z

    if-nez v0, :cond_0

    .line 240645
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    iget-object v1, p0, LX/1ON;->e:LX/1OD;

    invoke-interface {v0, v1}, LX/1OQ;->a(LX/1OD;)V

    .line 240646
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ON;->d:Z

    .line 240647
    :cond_0
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 2

    .prologue
    .line 240648
    check-cast p1, LX/1a0;

    .line 240649
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    instance-of v0, v0, LX/1OM;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1a0;->l:LX/1a1;

    if-eqz v0, :cond_0

    .line 240650
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    check-cast v0, LX/1OM;

    .line 240651
    iget-object v1, p1, LX/1a0;->l:LX/1a1;

    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1a1;)V

    .line 240652
    :cond_0
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 240653
    check-cast p1, LX/1a0;

    .line 240654
    iget-object v0, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p2, v0

    .line 240655
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 240656
    :cond_0
    :goto_0
    return-void

    .line 240657
    :cond_1
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    iget-object v2, p1, LX/1a0;->l:LX/1a1;

    invoke-interface {v1, v2, v0}, LX/1OQ;->a(LX/1a1;I)V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240608
    if-nez p1, :cond_0

    .line 240609
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1ON;->b:Ljava/util/List;

    .line 240610
    :goto_0
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 240611
    return-void

    .line 240612
    :cond_0
    iput-object p1, p0, LX/1ON;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 240613
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v0, p1}, LX/1OQ;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 240614
    return-void
.end method

.method public final b(LX/1OD;)V
    .locals 2

    .prologue
    .line 240566
    invoke-super {p0, p1}, LX/1OM;->b(LX/1OD;)V

    .line 240567
    iget-boolean v0, p0, LX/1ON;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ab_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240568
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    iget-object v1, p0, LX/1ON;->e:LX/1OD;

    invoke-interface {v0, v1}, LX/1OQ;->b(LX/1OD;)V

    .line 240569
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ON;->d:Z

    .line 240570
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 240571
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v0, p1}, LX/1OQ;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 240572
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240573
    if-nez p1, :cond_0

    .line 240574
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1ON;->c:Ljava/util/List;

    .line 240575
    :goto_0
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 240576
    return-void

    .line 240577
    :cond_0
    iput-object p1, p0, LX/1ON;->c:Ljava/util/List;

    goto :goto_0
.end method

.method public final b(LX/1a1;)Z
    .locals 2

    .prologue
    .line 240578
    check-cast p1, LX/1a0;

    .line 240579
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    instance-of v0, v0, LX/1OM;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1a0;->l:LX/1a1;

    if-eqz v0, :cond_0

    .line 240580
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    check-cast v0, LX/1OM;

    .line 240581
    iget-object v1, p1, LX/1a0;->l:LX/1a1;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1a1;)Z

    move-result v0

    .line 240582
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/1OM;->b(LX/1a1;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(LX/1a1;)V
    .locals 2

    .prologue
    .line 240583
    check-cast p1, LX/1a0;

    .line 240584
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    instance-of v0, v0, LX/1OM;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1a0;->l:LX/1a1;

    if-eqz v0, :cond_0

    .line 240585
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    check-cast v0, LX/1OM;

    .line 240586
    iget-object v1, p1, LX/1a0;->l:LX/1a1;

    invoke-virtual {v0, v1}, LX/1OM;->c(LX/1a1;)V

    .line 240587
    :cond_0
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 2

    .prologue
    .line 240588
    check-cast p1, LX/1a0;

    .line 240589
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    instance-of v0, v0, LX/1OM;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1a0;->l:LX/1a1;

    if-eqz v0, :cond_0

    .line 240590
    iget-object v0, p0, LX/1ON;->a:LX/1OO;

    check-cast v0, LX/1OM;

    .line 240591
    iget-object v1, p1, LX/1a0;->l:LX/1a1;

    invoke-virtual {v0, v1}, LX/1OM;->d(LX/1a1;)V

    .line 240592
    :cond_0
    return-void
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 240593
    iget-object v0, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 240594
    if-lt p1, v0, :cond_0

    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    add-int/2addr v1, v0

    if-lt p1, v1, :cond_1

    .line 240595
    :cond_0
    const/4 v0, 0x0

    .line 240596
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 240597
    iget-object v0, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 240598
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    .line 240599
    if-ge p1, v0, :cond_0

    .line 240600
    invoke-static {p1}, LX/1ON;->f(I)I

    move-result v0

    .line 240601
    :goto_0
    return v0

    .line 240602
    :cond_0
    sub-int v0, p1, v0

    .line 240603
    if-lt v0, v1, :cond_1

    .line 240604
    invoke-direct {p0, p1}, LX/1ON;->g(I)I

    move-result v0

    goto :goto_0

    .line 240605
    :cond_1
    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1, v0}, LX/1OP;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final ii_()I
    .locals 2

    .prologue
    .line 240606
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RecyclerView shouldn\'t be calling this method"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 240607
    iget-object v0, p0, LX/1ON;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/1ON;->a:LX/1OO;

    invoke-interface {v1}, LX/1OP;->ij_()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/1ON;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
