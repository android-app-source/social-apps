.class public abstract LX/0Px;
.super LX/0Py;
.source ""

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Py",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57547
    invoke-direct {p0}, LX/0Py;-><init>()V

    return-void
.end method

.method public static asImmutableList([Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57548
    array-length v0, p0

    invoke-static {p0, v0}, LX/0Px;->asImmutableList([Ljava/lang/Object;I)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static asImmutableList([Ljava/lang/Object;I)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            "I)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57549
    packed-switch p1, :pswitch_data_0

    .line 57550
    array-length v0, p0

    if-ge p1, v0, :cond_0

    .line 57551
    invoke-static {p0, p1}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    .line 57552
    :cond_0
    new-instance v0, LX/0Q7;

    invoke-direct {v0, p0}, LX/0Q7;-><init>([Ljava/lang/Object;)V

    :goto_0
    return-object v0

    .line 57553
    :pswitch_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 57554
    goto :goto_0

    .line 57555
    :pswitch_1
    new-instance v0, LX/0U4;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, LX/0U4;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static builder()LX/0Pz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Pz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57556
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    return-object v0
.end method

.method private static varargs construct([Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57557
    invoke-static {p0}, LX/0P8;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->asImmutableList([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static copyOf(Ljava/lang/Iterable;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57558
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57559
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf(Ljava/util/Collection;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57560
    instance-of v0, p0, LX/0Py;

    if-eqz v0, :cond_1

    .line 57561
    check-cast p0, LX/0Py;

    invoke-virtual {p0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    .line 57562
    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->asImmutableList([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 57563
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf(Ljava/util/Iterator;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57564
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57565
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 57566
    :goto_0
    return-object v0

    .line 57567
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 57568
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 57569
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 57570
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0Pz;->b(Ljava/util/Iterator;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf([Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57571
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 57572
    new-instance v1, LX/0Q7;

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, LX/0P8;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0Q7;-><init>([Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 57573
    :pswitch_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 57574
    goto :goto_0

    .line 57575
    :pswitch_1
    new-instance v0, LX/0U4;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, LX/0U4;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static of()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57576
    sget-object v0, LX/0Q7;->a:LX/0Px;

    return-object v0
.end method

.method public static of(Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57577
    new-instance v0, LX/0U4;

    invoke-direct {v0, p0}, LX/0U4;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57579
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57578
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57602
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57601
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57600
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57599
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57598
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57597
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57596
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    const/16 v1, 0x9

    aput-object p9, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57595
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    const/16 v1, 0x9

    aput-object p9, v0, v1

    const/16 v1, 0xa

    aput-object p10, v0, v1

    invoke-static {v0}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static varargs of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;TE;[TE;)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57580
    move-object/from16 v0, p12

    array-length v1, v0

    add-int/lit8 v1, v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    .line 57581
    const/4 v2, 0x0

    aput-object p0, v1, v2

    .line 57582
    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 57583
    const/4 v2, 0x2

    aput-object p2, v1, v2

    .line 57584
    const/4 v2, 0x3

    aput-object p3, v1, v2

    .line 57585
    const/4 v2, 0x4

    aput-object p4, v1, v2

    .line 57586
    const/4 v2, 0x5

    aput-object p5, v1, v2

    .line 57587
    const/4 v2, 0x6

    aput-object p6, v1, v2

    .line 57588
    const/4 v2, 0x7

    aput-object p7, v1, v2

    .line 57589
    const/16 v2, 0x8

    aput-object p8, v1, v2

    .line 57590
    const/16 v2, 0x9

    aput-object p9, v1, v2

    .line 57591
    const/16 v2, 0xa

    aput-object p10, v1, v2

    .line 57592
    const/16 v2, 0xb

    aput-object p11, v1, v2

    .line 57593
    const/4 v2, 0x0

    const/16 v3, 0xc

    move-object/from16 v0, p12

    array-length v4, v0

    move-object/from16 v0, p12

    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57594
    invoke-static {v1}, LX/0Px;->construct([Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    return-object v1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 57546
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final add(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57545
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57456
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final asList()LX/0Px;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57457
    return-object p0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57458
    invoke-virtual {p0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public copyIntoArray([Ljava/lang/Object;I)I
    .locals 4

    .prologue
    .line 57459
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    .line 57460
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 57461
    add-int v2, p2, v0

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, p1, v2

    .line 57462
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57463
    :cond_0
    add-int v0, p2, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57464
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57465
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 57466
    :cond_0
    :goto_0
    move v0, v0

    .line 57467
    return v0

    .line 57468
    :cond_1
    instance-of v2, p1, Ljava/util/List;

    if-nez v2, :cond_2

    move v0, v1

    .line 57469
    goto :goto_0

    .line 57470
    :cond_2
    check-cast p1, Ljava/util/List;

    .line 57471
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 57472
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v3, v2, :cond_3

    move v0, v1

    .line 57473
    goto :goto_0

    .line 57474
    :cond_3
    instance-of v2, p0, Ljava/util/RandomAccess;

    if-eqz v2, :cond_5

    instance-of v2, p1, Ljava/util/RandomAccess;

    if-eqz v2, :cond_5

    move v2, v1

    .line 57475
    :goto_1
    if-ge v2, v3, :cond_0

    .line 57476
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    move v0, v1

    .line 57477
    goto :goto_0

    .line 57478
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 57479
    :cond_5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RZ;->a(Ljava/util/Iterator;Ljava/util/Iterator;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 57480
    const/4 v1, 0x1

    .line 57481
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    .line 57482
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 57483
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v1, v3

    .line 57484
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 57485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57486
    :cond_0
    return v1
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57487
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    .line 57488
    :cond_0
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_1

    .line 57489
    const/4 v0, 0x0

    .line 57490
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 57491
    if-nez p1, :cond_6

    .line 57492
    :goto_1
    if-ge v0, v1, :cond_7

    .line 57493
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    .line 57494
    :goto_2
    move v0, v0

    .line 57495
    :goto_3
    move v0, v0

    .line 57496
    goto :goto_0

    .line 57497
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 57498
    :cond_2
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 57499
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 57500
    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    goto :goto_3

    .line 57501
    :cond_3
    const/4 v0, -0x1

    goto :goto_3

    .line 57502
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57503
    :cond_5
    add-int/lit8 v0, v0, 0x1

    :cond_6
    if-ge v0, v1, :cond_7

    .line 57504
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_2

    .line 57505
    :cond_7
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57506
    invoke-virtual {p0}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 57507
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57508
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    .line 57509
    :cond_0
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_2

    .line 57510
    if-nez p1, :cond_6

    .line 57511
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_7

    .line 57512
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    .line 57513
    :cond_1
    :goto_2
    move v0, v0

    .line 57514
    :goto_3
    move v0, v0

    .line 57515
    goto :goto_0

    .line 57516
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 57517
    :cond_3
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 57518
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 57519
    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    goto :goto_3

    .line 57520
    :cond_4
    const/4 v0, -0x1

    goto :goto_3

    .line 57521
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 57522
    :cond_6
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_4
    if-ltz v0, :cond_7

    .line 57523
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 57524
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 57525
    :cond_7
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public listIterator()LX/0Rb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rb",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57526
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0Px;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)LX/0Rb;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Rb",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57527
    new-instance v0, LX/1kl;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, LX/1kl;-><init>(LX/0Px;II)V

    return-object v0
.end method

.method public bridge synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 57528
    invoke-virtual {p0}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 57529
    invoke-virtual {p0, p1}, LX/0Px;->listIterator(I)LX/0Rb;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57530
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public reverse()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57531
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/32c;

    invoke-direct {v0, p0}, LX/32c;-><init>(LX/0Px;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57532
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public subList(II)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57533
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 57534
    sub-int v0, p2, p1

    .line 57535
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 57536
    :goto_0
    return-object p0

    .line 57537
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 57538
    invoke-virtual {p0, p1, p2}, LX/0Px;->subListUnchecked(II)LX/0Px;

    move-result-object p0

    goto :goto_0

    .line 57539
    :pswitch_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p0, v0

    .line 57540
    goto :goto_0

    .line 57541
    :pswitch_1
    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 57542
    invoke-virtual {p0, p1, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public subListUnchecked(II)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57543
    new-instance v0, LX/4y9;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, LX/4y9;-><init>(LX/0Px;II)V

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57544
    new-instance v0, LX/4y8;

    invoke-virtual {p0}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4y8;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
