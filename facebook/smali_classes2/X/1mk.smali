.class public LX/1mk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 314159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314160
    new-instance v0, LX/0tf;

    invoke-direct {v0, v1}, LX/0tf;-><init>(I)V

    iput-object v0, p0, LX/1mk;->a:LX/0tf;

    .line 314161
    new-instance v0, LX/0tf;

    invoke-direct {v0, v1}, LX/0tf;-><init>(I)V

    iput-object v0, p0, LX/1mk;->b:LX/0tf;

    return-void
.end method

.method private static a(J)I
    .locals 2

    .prologue
    .line 314158
    long-to-int v0, p0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method private static a(JI)J
    .locals 4

    .prologue
    .line 314155
    if-ltz p2, :cond_0

    const v0, 0xffff

    if-le p2, v0, :cond_1

    .line 314156
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sequence must be non-negative and no greater than 65535 actual sequence "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314157
    :cond_1
    int-to-long v0, p2

    or-long/2addr v0, p0

    return-wide v0
.end method

.method private static a(LX/1dK;II)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 314102
    if-ltz p1, :cond_0

    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    .line 314103
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Level must be non-negative and no greater than 255 actual level "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314104
    :cond_1
    iget-object v0, p0, LX/1dK;->b:LX/1X1;

    move-object v0, v0

    .line 314105
    if-eqz v0, :cond_2

    .line 314106
    iget-object v0, p0, LX/1dK;->b:LX/1X1;

    move-object v0, v0

    .line 314107
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 314108
    iget v1, v0, LX/1S3;->d:I

    move v0, v1

    .line 314109
    int-to-long v0, v0

    .line 314110
    :goto_0
    const/16 v4, 0x1a

    shl-long/2addr v0, v4

    .line 314111
    int-to-long v4, p1

    const/16 v6, 0x12

    shl-long/2addr v4, v6

    .line 314112
    int-to-long v6, p2

    const/16 v8, 0x10

    shl-long/2addr v6, v8

    .line 314113
    or-long/2addr v0, v2

    or-long/2addr v0, v4

    or-long/2addr v0, v6

    return-wide v0

    :cond_2
    move-wide v0, v2

    .line 314114
    goto :goto_0
.end method

.method private static a(LX/1dW;I)J
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 314143
    if-ltz p1, :cond_0

    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    .line 314144
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Level must be non-negative and no greater than 255 actual level "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314145
    :cond_1
    iget-object v0, p0, LX/1dW;->b:LX/1X1;

    move-object v0, v0

    .line 314146
    if-eqz v0, :cond_2

    .line 314147
    iget-object v0, p0, LX/1dW;->b:LX/1X1;

    move-object v0, v0

    .line 314148
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 314149
    iget v1, v0, LX/1S3;->d:I

    move v0, v1

    .line 314150
    int-to-long v0, v0

    .line 314151
    :goto_0
    const/16 v4, 0x1a

    shl-long/2addr v0, v4

    .line 314152
    int-to-long v4, p1

    const/16 v6, 0x12

    shl-long/2addr v4, v6

    .line 314153
    or-long/2addr v0, v2

    or-long/2addr v0, v4

    return-wide v0

    :cond_2
    move-wide v0, v2

    .line 314154
    goto :goto_0
.end method

.method private static b(J)I
    .locals 4

    .prologue
    .line 314142
    const/16 v0, 0x12

    shr-long v0, p0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 314139
    iget-object v0, p0, LX/1mk;->a:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 314140
    iget-object v0, p0, LX/1mk;->b:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->b()V

    .line 314141
    return-void
.end method

.method public final a(LX/1dK;IIJZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 314126
    invoke-static {p1, p2, p3}, LX/1mk;->a(LX/1dK;II)J

    move-result-wide v4

    .line 314127
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    invoke-static {p4, p5}, LX/1mk;->b(J)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 314128
    invoke-static {p4, p5}, LX/1mk;->a(J)I

    move-result v0

    move v1, v0

    .line 314129
    :goto_0
    iget-object v0, p0, LX/1mk;->a:LX/0tf;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v4, v5, v3}, LX/0tf;->a(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 314130
    if-ge v1, v0, :cond_1

    .line 314131
    add-int/lit8 v1, v0, 0x1

    move v0, v2

    .line 314132
    :goto_1
    iput v0, p1, LX/1dK;->w:I

    .line 314133
    invoke-static {v4, v5, v1}, LX/1mk;->a(JI)J

    move-result-wide v2

    .line 314134
    iput-wide v2, p1, LX/1dK;->a:J

    .line 314135
    iget-object v0, p0, LX/1mk;->a:LX/0tf;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v5, v1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 314136
    return-void

    .line 314137
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 314138
    :cond_1
    if-eqz p6, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public final a(LX/1dW;IJ)V
    .locals 8

    .prologue
    .line 314115
    invoke-static {p1, p2}, LX/1mk;->a(LX/1dW;I)J

    move-result-wide v2

    .line 314116
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    invoke-static {p3, p4}, LX/1mk;->b(J)I

    move-result v0

    if-ne v0, p2, :cond_1

    .line 314117
    invoke-static {p3, p4}, LX/1mk;->a(J)I

    move-result v0

    move v1, v0

    .line 314118
    :goto_0
    iget-object v0, p0, LX/1mk;->b:LX/0tf;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/0tf;->a(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 314119
    if-ge v1, v0, :cond_0

    .line 314120
    add-int/lit8 v1, v0, 0x1

    .line 314121
    :cond_0
    invoke-static {v2, v3, v1}, LX/1mk;->a(JI)J

    move-result-wide v4

    .line 314122
    iput-wide v4, p1, LX/1dW;->a:J

    .line 314123
    iget-object v0, p0, LX/1mk;->b:LX/0tf;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 314124
    return-void

    .line 314125
    :cond_1
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0
.end method
