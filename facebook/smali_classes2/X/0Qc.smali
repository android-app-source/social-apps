.class public LX/0Qc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QI;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0QI",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final localCache:LX/0Qd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qd",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QN",
            "<-TK;-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58227
    new-instance v0, LX/0Qd;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, LX/0Qd;-><init>(LX/0QN;LX/0QM;)V

    invoke-direct {p0, v0}, LX/0Qc;-><init>(LX/0Qd;)V

    .line 58228
    return-void
.end method

.method public constructor <init>(LX/0Qd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qd",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58225
    iput-object p1, p0, LX/0Qc;->localCache:LX/0Qd;

    .line 58226
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58216
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    const/4 p0, 0x1

    .line 58217
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qd;->c(LX/0Qd;Ljava/lang/Object;)I

    move-result v1

    .line 58218
    invoke-static {v0, v1}, LX/0Qd;->b(LX/0Qd;I)LX/0Qx;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, LX/0Qx;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 58219
    if-nez v1, :cond_0

    .line 58220
    iget-object v2, v0, LX/0Qd;->s:LX/0QP;

    invoke-interface {v2, p0}, LX/0QP;->b(I)V

    .line 58221
    :goto_0
    move-object v0, v1

    .line 58222
    return-object v0

    .line 58223
    :cond_0
    iget-object v2, v0, LX/0Qd;->s:LX/0QP;

    invoke-interface {v2, p0}, LX/0QP;->a(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/concurrent/Callable",
            "<+TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 58214
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58215
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    new-instance v1, LX/4wL;

    invoke-direct {v1, p0, p2}, LX/4wL;-><init>(LX/0Qc;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v0, p1, v1}, LX/0Qd;->a(Ljava/lang/Object;LX/0QM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 58212
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    invoke-virtual {v0}, LX/0Qd;->clear()V

    .line 58213
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 58229
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    .line 58230
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    .line 58231
    invoke-virtual {v0, p0}, LX/0Qd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 58232
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 58210
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    invoke-virtual {v0, p1, p2}, LX/0Qd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58211
    return-void
.end method

.method public final b()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58209
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 58206
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58207
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    invoke-virtual {v0, p1}, LX/0Qd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58208
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 58201
    iget-object v0, p0, LX/0Qc;->localCache:LX/0Qd;

    .line 58202
    iget-object v2, v0, LX/0Qd;->d:[LX/0Qx;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object p0, v2, v1

    .line 58203
    invoke-virtual {p0}, LX/0Qx;->c()V

    .line 58204
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58205
    :cond_0
    return-void
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 58200
    new-instance v0, LX/4wI;

    iget-object v1, p0, LX/0Qc;->localCache:LX/0Qd;

    invoke-direct {v0, v1}, LX/4wI;-><init>(LX/0Qd;)V

    return-object v0
.end method
