.class public final LX/0a4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 84289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 84311
    if-ge p0, p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    if-le p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(J)I
    .locals 4

    .prologue
    .line 84301
    long-to-int v0, p0

    .line 84302
    int-to-long v2, v0

    cmp-long v1, v2, p0

    if-eqz v1, :cond_0

    .line 84303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Out of range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84304
    :cond_0
    return v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 7
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation build Ljavax/annotation/CheckForNull;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 84305
    const/16 v0, 0xa

    .line 84306
    invoke-static {p0, v0}, LX/1YA;->a(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v1

    .line 84307
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v2

    int-to-long v5, v2

    cmp-long v2, v3, v5

    if-eqz v2, :cond_1

    .line 84308
    :cond_0
    const/4 v1, 0x0

    .line 84309
    :goto_0
    move-object v0, v1

    .line 84310
    return-object v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public static b(J)I
    .locals 2

    .prologue
    .line 84295
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    .line 84296
    const v0, 0x7fffffff

    .line 84297
    :goto_0
    return v0

    .line 84298
    :cond_0
    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 84299
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 84300
    :cond_1
    long-to-int v0, p0

    goto :goto_0
.end method

.method public static c([IIII)I
    .locals 2

    .prologue
    .line 84290
    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_1

    .line 84291
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 84292
    :goto_1
    return v0

    .line 84293
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84294
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
