.class public LX/1ik;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0kc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1ik;


# instance fields
.field public final a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "LX/1il;",
            "LX/1il;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ZL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ZL",
            "<",
            "LX/1il;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 298946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298947
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/0S8;->c(I)LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/1ik;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 298948
    new-instance v0, LX/0ZL;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0ZL;-><init>(I)V

    iput-object v0, p0, LX/1ik;->b:LX/0ZL;

    .line 298949
    return-void
.end method

.method public static a(LX/0QB;)LX/1ik;
    .locals 3

    .prologue
    .line 298950
    sget-object v0, LX/1ik;->c:LX/1ik;

    if-nez v0, :cond_1

    .line 298951
    const-class v1, LX/1ik;

    monitor-enter v1

    .line 298952
    :try_start_0
    sget-object v0, LX/1ik;->c:LX/1ik;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 298953
    if-eqz v2, :cond_0

    .line 298954
    :try_start_1
    new-instance v0, LX/1ik;

    invoke-direct {v0}, LX/1ik;-><init>()V

    .line 298955
    move-object v0, v0

    .line 298956
    sput-object v0, LX/1ik;->c:LX/1ik;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298957
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 298958
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 298959
    :cond_1
    sget-object v0, LX/1ik;->c:LX/1ik;

    return-object v0

    .line 298960
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 298961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation

    .prologue
    .line 298962
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    .line 298963
    iget-object v1, p0, LX/1ik;->b:LX/0ZL;

    invoke-virtual {v1}, LX/0ZL;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298964
    iget-object v1, p0, LX/1ik;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298965
    return-object v0
.end method

.method public final b(LX/1il;)V
    .locals 1

    .prologue
    .line 298966
    iget-object v0, p0, LX/1ik;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298967
    iget-object v0, p0, LX/1ik;->b:LX/0ZL;

    invoke-virtual {v0, p1}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 298968
    return-void
.end method
