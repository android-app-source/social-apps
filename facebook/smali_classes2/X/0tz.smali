.class public final LX/0tz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0u4;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0u4;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 155846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155847
    iput-object p1, p0, LX/0tz;->a:LX/0QB;

    .line 155848
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0u4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155864
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/0tz;

    invoke-direct {v2, p0}, LX/0tz;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155863
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0tz;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 155850
    packed-switch p2, :pswitch_data_0

    .line 155851
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155852
    :pswitch_0
    invoke-static {p1}, LX/0u1;->a(LX/0QB;)LX/0u3;

    move-result-object v0

    .line 155853
    :goto_0
    return-object v0

    .line 155854
    :pswitch_1
    invoke-static {p1}, LX/0u6;->a(LX/0QB;)LX/0uA;

    move-result-object v0

    goto :goto_0

    .line 155855
    :pswitch_2
    invoke-static {p1}, LX/0uB;->a(LX/0QB;)LX/0uD;

    move-result-object v0

    goto :goto_0

    .line 155856
    :pswitch_3
    invoke-static {p1}, LX/0uF;->a(LX/0QB;)LX/0uH;

    move-result-object v0

    goto :goto_0

    .line 155857
    :pswitch_4
    invoke-static {p1}, LX/0uI;->a(LX/0QB;)LX/0uK;

    move-result-object v0

    goto :goto_0

    .line 155858
    :pswitch_5
    invoke-static {p1}, LX/0uL;->a(LX/0QB;)LX/0uM;

    move-result-object v0

    goto :goto_0

    .line 155859
    :pswitch_6
    invoke-static {p1}, LX/0uR;->a(LX/0QB;)LX/0uT;

    move-result-object v0

    goto :goto_0

    .line 155860
    :pswitch_7
    invoke-static {p1}, LX/0uU;->a(LX/0QB;)LX/0uV;

    move-result-object v0

    goto :goto_0

    .line 155861
    :pswitch_8
    invoke-static {p1}, LX/0uW;->a(LX/0QB;)LX/0uX;

    move-result-object v0

    goto :goto_0

    .line 155862
    :pswitch_9
    invoke-static {p1}, LX/0uY;->a(LX/0QB;)LX/0uZ;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 155849
    const/16 v0, 0xa

    return v0
.end method
