.class public LX/1KV;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1DD;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0hj;",
        ":",
        "LX/1K5;",
        ":",
        "LX/1DD;",
        ":",
        "Ljava/lang/Object;",
        ":",
        "LX/1KT;",
        ":",
        "Ljava/lang/Object;",
        ":",
        "LX/0hk;",
        ":",
        "LX/0hl;",
        ":",
        "LX/0fm;",
        ">",
        "LX/0hD;",
        "LX/1DD;"
    }
.end annotation


# instance fields
.field public a:LX/0hj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field

.field public b:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/1Lb;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 232656
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 232657
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 232658
    iget-object v0, p0, LX/1KV;->b:LX/1Iu;

    check-cast p1, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, p1}, LX/1Iu;->a(Ljava/lang/Object;)V

    .line 232659
    new-instance v0, LX/1LZ;

    invoke-direct {v0, p0}, LX/1LZ;-><init>(LX/1KV;)V

    iput-object v0, p0, LX/1KV;->c:LX/1Lb;

    .line 232660
    iget-object v0, p0, LX/1KV;->b:LX/1Iu;

    .line 232661
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 232662
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    iget-object v1, p0, LX/1KV;->c:LX/1Lb;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 232663
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 232664
    iget-object v0, p0, LX/1KV;->b:LX/1Iu;

    .line 232665
    iget-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 232666
    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    iget-object v1, p0, LX/1KV;->c:LX/1Lb;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(LX/1Lb;)V

    .line 232667
    iget-object v0, p0, LX/1KV;->b:LX/1Iu;

    .line 232668
    const/4 v1, 0x0

    iput-object v1, v0, LX/1Iu;->a:Ljava/lang/Object;

    .line 232669
    const/4 v0, 0x0

    iput-object v0, p0, LX/1KV;->c:LX/1Lb;

    .line 232670
    return-void
.end method
