.class public LX/0kw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Or",
        "<",
        "LX/03R;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127956
    iput-object p1, p0, LX/0kw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 127957
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 127958
    iget-object v0, p0, LX/0kw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0uQ;->g:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 127959
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 127960
    iget-object v0, p0, LX/0kw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127961
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 127962
    :goto_0
    return-object v0

    .line 127963
    :cond_0
    iget-object v0, p0, LX/0kw;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->g:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 127964
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    goto :goto_0
.end method
