.class public final LX/1pG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/12A;

.field public final b:Ljava/io/InputStream;

.field public final c:[B

.field public d:I

.field public e:Z

.field public f:I

.field private g:I

.field private h:I

.field private final i:Z


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/InputStream;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 328084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328085
    iput-boolean v2, p0, LX/1pG;->e:Z

    .line 328086
    iput v1, p0, LX/1pG;->f:I

    .line 328087
    iput-object p1, p0, LX/1pG;->a:LX/12A;

    .line 328088
    iput-object p2, p0, LX/1pG;->b:Ljava/io/InputStream;

    .line 328089
    invoke-virtual {p1}, LX/12A;->e()[B

    move-result-object v0

    iput-object v0, p0, LX/1pG;->c:[B

    .line 328090
    iput v1, p0, LX/1pG;->g:I

    iput v1, p0, LX/1pG;->h:I

    .line 328091
    iput v1, p0, LX/1pG;->d:I

    .line 328092
    iput-boolean v2, p0, LX/1pG;->i:Z

    .line 328093
    return-void
.end method

.method public constructor <init>(LX/12A;[BII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 328073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328074
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1pG;->e:Z

    .line 328075
    iput v1, p0, LX/1pG;->f:I

    .line 328076
    iput-object p1, p0, LX/1pG;->a:LX/12A;

    .line 328077
    const/4 v0, 0x0

    iput-object v0, p0, LX/1pG;->b:Ljava/io/InputStream;

    .line 328078
    iput-object p2, p0, LX/1pG;->c:[B

    .line 328079
    iput p3, p0, LX/1pG;->g:I

    .line 328080
    add-int v0, p3, p4

    iput v0, p0, LX/1pG;->h:I

    .line 328081
    neg-int v0, p3

    iput v0, p0, LX/1pG;->d:I

    .line 328082
    iput-boolean v1, p0, LX/1pG;->i:Z

    .line 328083
    return-void
.end method

.method private a()LX/1pL;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 328054
    const/4 v1, 0x0

    .line 328055
    const/4 v2, 0x4

    invoke-direct {p0, v2}, LX/1pG;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 328056
    iget-object v2, p0, LX/1pG;->c:[B

    iget v3, p0, LX/1pG;->g:I

    aget-byte v2, v2, v3

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    add-int/lit8 v4, v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    add-int/lit8 v4, v4, 0x3

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    .line 328057
    invoke-direct {p0, v2}, LX/1pG;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 328058
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 328059
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    .line 328060
    :goto_1
    iget-object v1, p0, LX/1pG;->a:LX/12A;

    .line 328061
    iput-object v0, v1, LX/12A;->b:LX/1pL;

    .line 328062
    return-object v0

    .line 328063
    :cond_1
    invoke-direct {p0, v2}, LX/1pG;->b(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 328064
    ushr-int/lit8 v2, v2, 0x10

    invoke-direct {p0, v2}, LX/1pG;->c(I)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 328065
    :cond_3
    const/4 v2, 0x2

    invoke-direct {p0, v2}, LX/1pG;->d(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 328066
    iget-object v2, p0, LX/1pG;->c:[B

    iget v3, p0, LX/1pG;->g:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    .line 328067
    invoke-direct {p0, v2}, LX/1pG;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 328068
    :cond_4
    iget v0, p0, LX/1pG;->f:I

    packed-switch v0, :pswitch_data_0

    .line 328069
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Internal error"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328070
    :pswitch_1
    sget-object v0, LX/1pL;->UTF8:LX/1pL;

    goto :goto_1

    .line 328071
    :pswitch_2
    iget-boolean v0, p0, LX/1pG;->e:Z

    if-eqz v0, :cond_5

    sget-object v0, LX/1pL;->UTF16_BE:LX/1pL;

    goto :goto_1

    :cond_5
    sget-object v0, LX/1pL;->UTF16_LE:LX/1pL;

    goto :goto_1

    .line 328072
    :pswitch_3
    iget-boolean v0, p0, LX/1pG;->e:Z

    if-eqz v0, :cond_6

    sget-object v0, LX/1pL;->UTF32_BE:LX/1pL;

    goto :goto_1

    :cond_6
    sget-object v0, LX/1pL;->UTF32_LE:LX/1pL;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 328053
    new-instance v0, Ljava/io/CharConversionException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported UCS-4 endianness ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") detected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/CharConversionException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(I)Z
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 327981
    sparse-switch p1, :sswitch_data_0

    .line 327982
    :goto_0
    ushr-int/lit8 v2, p1, 0x10

    .line 327983
    const v3, 0xfeff

    if-ne v2, v3, :cond_0

    .line 327984
    iget v1, p0, LX/1pG;->g:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/1pG;->g:I

    .line 327985
    iput v4, p0, LX/1pG;->f:I

    .line 327986
    iput-boolean v0, p0, LX/1pG;->e:Z

    .line 327987
    :goto_1
    return v0

    .line 327988
    :sswitch_0
    iput-boolean v0, p0, LX/1pG;->e:Z

    .line 327989
    iget v1, p0, LX/1pG;->g:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, LX/1pG;->g:I

    .line 327990
    iput v3, p0, LX/1pG;->f:I

    goto :goto_1

    .line 327991
    :sswitch_1
    iget v2, p0, LX/1pG;->g:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/1pG;->g:I

    .line 327992
    iput v3, p0, LX/1pG;->f:I

    .line 327993
    iput-boolean v1, p0, LX/1pG;->e:Z

    goto :goto_1

    .line 327994
    :sswitch_2
    const-string v2, "2143"

    invoke-static {v2}, LX/1pG;->a(Ljava/lang/String;)V

    .line 327995
    :sswitch_3
    const-string v2, "3412"

    invoke-static {v2}, LX/1pG;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 327996
    :cond_0
    const v3, 0xfffe

    if-ne v2, v3, :cond_1

    .line 327997
    iget v2, p0, LX/1pG;->g:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/1pG;->g:I

    .line 327998
    iput v4, p0, LX/1pG;->f:I

    .line 327999
    iput-boolean v1, p0, LX/1pG;->e:Z

    goto :goto_1

    .line 328000
    :cond_1
    ushr-int/lit8 v2, p1, 0x8

    const v3, 0xefbbbf

    if-ne v2, v3, :cond_2

    .line 328001
    iget v1, p0, LX/1pG;->g:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, LX/1pG;->g:I

    .line 328002
    iput v0, p0, LX/1pG;->f:I

    .line 328003
    iput-boolean v0, p0, LX/1pG;->e:Z

    goto :goto_1

    :cond_2
    move v0, v1

    .line 328004
    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x1010000 -> :sswitch_3
        -0x20000 -> :sswitch_1
        0xfeff -> :sswitch_0
        0xfffe -> :sswitch_2
    .end sparse-switch
.end method

.method private b()Ljava/io/Reader;
    .locals 7

    .prologue
    .line 328039
    iget-object v0, p0, LX/1pG;->a:LX/12A;

    .line 328040
    iget-object v1, v0, LX/12A;->b:LX/1pL;

    move-object v6, v1

    .line 328041
    sget-object v0, LX/4pg;->a:[I

    invoke-virtual {v6}, LX/1pL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 328042
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Internal error"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328043
    :pswitch_0
    new-instance v0, LX/4pe;

    iget-object v1, p0, LX/1pG;->a:LX/12A;

    iget-object v2, p0, LX/1pG;->b:Ljava/io/InputStream;

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    iget v5, p0, LX/1pG;->h:I

    iget-object v6, p0, LX/1pG;->a:LX/12A;

    .line 328044
    iget-object p0, v6, LX/12A;->b:LX/1pL;

    move-object v6, p0

    .line 328045
    invoke-virtual {v6}, LX/1pL;->isBigEndian()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/4pe;-><init>(LX/12A;Ljava/io/InputStream;[BIIZ)V

    .line 328046
    :goto_0
    return-object v0

    .line 328047
    :pswitch_1
    iget-object v2, p0, LX/1pG;->b:Ljava/io/InputStream;

    .line 328048
    if-nez v2, :cond_0

    .line 328049
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, LX/1pG;->c:[B

    iget v2, p0, LX/1pG;->g:I

    iget v3, p0, LX/1pG;->h:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 328050
    :goto_1
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v6}, LX/1pL;->getJavaName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 328051
    :cond_0
    iget v0, p0, LX/1pG;->g:I

    iget v1, p0, LX/1pG;->h:I

    if-ge v0, v1, :cond_1

    .line 328052
    new-instance v0, LX/4pc;

    iget-object v1, p0, LX/1pG;->a:LX/12A;

    iget-object v3, p0, LX/1pG;->c:[B

    iget v4, p0, LX/1pG;->g:I

    iget v5, p0, LX/1pG;->h:I

    invoke-direct/range {v0 .. v5}, LX/4pc;-><init>(LX/12A;Ljava/io/InputStream;[BII)V

    goto :goto_1

    :cond_1
    move-object v0, v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private b(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 328029
    shr-int/lit8 v2, p1, 0x8

    if-nez v2, :cond_1

    .line 328030
    iput-boolean v1, p0, LX/1pG;->e:Z

    .line 328031
    :goto_0
    const/4 v0, 0x4

    iput v0, p0, LX/1pG;->f:I

    move v0, v1

    .line 328032
    :cond_0
    return v0

    .line 328033
    :cond_1
    const v2, 0xffffff

    and-int/2addr v2, p1

    if-nez v2, :cond_2

    .line 328034
    iput-boolean v0, p0, LX/1pG;->e:Z

    goto :goto_0

    .line 328035
    :cond_2
    const v2, -0xff0001

    and-int/2addr v2, p1

    if-nez v2, :cond_3

    .line 328036
    const-string v0, "3412"

    invoke-static {v0}, LX/1pG;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 328037
    :cond_3
    const v2, -0xff01

    and-int/2addr v2, p1

    if-nez v2, :cond_0

    .line 328038
    const-string v0, "2143"

    invoke-static {v0}, LX/1pG;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 328023
    const v2, 0xff00

    and-int/2addr v2, p1

    if-nez v2, :cond_1

    .line 328024
    iput-boolean v1, p0, LX/1pG;->e:Z

    .line 328025
    :goto_0
    const/4 v0, 0x2

    iput v0, p0, LX/1pG;->f:I

    move v0, v1

    .line 328026
    :cond_0
    return v0

    .line 328027
    :cond_1
    and-int/lit16 v2, p1, 0xff

    if-nez v2, :cond_0

    .line 328028
    iput-boolean v0, p0, LX/1pG;->e:Z

    goto :goto_0
.end method

.method private d(I)Z
    .locals 6

    .prologue
    .line 328011
    iget v0, p0, LX/1pG;->h:I

    iget v1, p0, LX/1pG;->g:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 328012
    :goto_0
    if-ge v1, p1, :cond_2

    .line 328013
    iget-object v0, p0, LX/1pG;->b:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 328014
    const/4 v0, -0x1

    .line 328015
    :goto_1
    if-gtz v0, :cond_1

    .line 328016
    const/4 v0, 0x0

    .line 328017
    :goto_2
    return v0

    .line 328018
    :cond_0
    iget-object v0, p0, LX/1pG;->b:Ljava/io/InputStream;

    iget-object v2, p0, LX/1pG;->c:[B

    iget v3, p0, LX/1pG;->h:I

    iget-object v4, p0, LX/1pG;->c:[B

    array-length v4, v4

    iget v5, p0, LX/1pG;->h:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_1

    .line 328019
    :cond_1
    iget v2, p0, LX/1pG;->h:I

    add-int/2addr v2, v0

    iput v2, p0, LX/1pG;->h:I

    .line 328020
    add-int/2addr v0, v1

    move v1, v0

    .line 328021
    goto :goto_0

    .line 328022
    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public final a(ILX/0lD;LX/0lv;LX/0lt;ZZ)LX/15w;
    .locals 11

    .prologue
    .line 328005
    invoke-direct {p0}, LX/1pG;->a()LX/1pL;

    move-result-object v1

    .line 328006
    sget-object v2, LX/1pL;->UTF8:LX/1pL;

    if-ne v1, v2, :cond_0

    .line 328007
    if-eqz p5, :cond_0

    .line 328008
    move/from16 v0, p6

    invoke-virtual {p3, v0}, LX/0lv;->a(Z)LX/0lv;

    move-result-object v6

    .line 328009
    new-instance v1, LX/1pM;

    iget-object v2, p0, LX/1pG;->a:LX/12A;

    iget-object v4, p0, LX/1pG;->b:Ljava/io/InputStream;

    iget-object v7, p0, LX/1pG;->c:[B

    iget v8, p0, LX/1pG;->g:I

    iget v9, p0, LX/1pG;->h:I

    iget-boolean v10, p0, LX/1pG;->i:Z

    move v3, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v10}, LX/1pM;-><init>(LX/12A;ILjava/io/InputStream;LX/0lD;LX/0lv;[BIIZ)V

    .line 328010
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, LX/15t;

    iget-object v2, p0, LX/1pG;->a:LX/12A;

    invoke-direct {p0}, LX/1pG;->b()Ljava/io/Reader;

    move-result-object v4

    invoke-virtual/range {p4 .. p6}, LX/0lt;->a(ZZ)LX/0lt;

    move-result-object v6

    move v3, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, LX/15t;-><init>(LX/12A;ILjava/io/Reader;LX/0lD;LX/0lt;)V

    goto :goto_0
.end method
