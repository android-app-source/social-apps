.class public final enum LX/1Cm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Cm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Cm;

.field public static final enum DISPOSED:LX/1Cm;

.field public static final enum FAILED:LX/1Cm;

.field public static final enum SUCCESSFUL:LX/1Cm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 216833
    new-instance v0, LX/1Cm;

    const-string v1, "SUCCESSFUL"

    invoke-direct {v0, v1, v2}, LX/1Cm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Cm;->SUCCESSFUL:LX/1Cm;

    .line 216834
    new-instance v0, LX/1Cm;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, LX/1Cm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Cm;->FAILED:LX/1Cm;

    .line 216835
    new-instance v0, LX/1Cm;

    const-string v1, "DISPOSED"

    invoke-direct {v0, v1, v4}, LX/1Cm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Cm;->DISPOSED:LX/1Cm;

    .line 216836
    const/4 v0, 0x3

    new-array v0, v0, [LX/1Cm;

    sget-object v1, LX/1Cm;->SUCCESSFUL:LX/1Cm;

    aput-object v1, v0, v2

    sget-object v1, LX/1Cm;->FAILED:LX/1Cm;

    aput-object v1, v0, v3

    sget-object v1, LX/1Cm;->DISPOSED:LX/1Cm;

    aput-object v1, v0, v4

    sput-object v0, LX/1Cm;->$VALUES:[LX/1Cm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 216837
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Cm;
    .locals 1

    .prologue
    .line 216838
    const-class v0, LX/1Cm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Cm;

    return-object v0
.end method

.method public static values()[LX/1Cm;
    .locals 1

    .prologue
    .line 216839
    sget-object v0, LX/1Cm;->$VALUES:[LX/1Cm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Cm;

    return-object v0
.end method
