.class public LX/1lz;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1mB;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1mB;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312827
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1mB;
    .locals 11

    .prologue
    .line 312828
    sget-object v0, LX/1lz;->a:LX/1mB;

    if-nez v0, :cond_1

    .line 312829
    const-class v1, LX/1lz;

    monitor-enter v1

    .line 312830
    :try_start_0
    sget-object v0, LX/1lz;->a:LX/1mB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 312831
    if-eqz v2, :cond_0

    .line 312832
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 312833
    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v4

    check-cast v4, LX/1m0;

    invoke-static {v0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v5

    check-cast v5, LX/0wp;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v6

    check-cast v6, LX/0oz;

    invoke-static {v0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v7

    check-cast v7, LX/0YR;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v8

    check-cast v8, LX/0ka;

    invoke-static {v0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v9

    check-cast v9, LX/1AA;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static/range {v3 .. v10}, LX/19Y;->a(Landroid/os/Handler;LX/1m0;LX/0wp;LX/0oz;LX/0YR;LX/0ka;LX/1AA;LX/0hB;)LX/1mB;

    move-result-object v3

    move-object v0, v3

    .line 312834
    sput-object v0, LX/1lz;->a:LX/1mB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312835
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 312836
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 312837
    :cond_1
    sget-object v0, LX/1lz;->a:LX/1mB;

    return-object v0

    .line 312838
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 312839
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 312840
    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-static {p0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v1

    check-cast v1, LX/1m0;

    invoke-static {p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v2

    check-cast v2, LX/0wp;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-static {p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v4

    check-cast v4, LX/0YR;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v5

    check-cast v5, LX/0ka;

    invoke-static {p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v6

    check-cast v6, LX/1AA;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static/range {v0 .. v7}, LX/19Y;->a(Landroid/os/Handler;LX/1m0;LX/0wp;LX/0oz;LX/0YR;LX/0ka;LX/1AA;LX/0hB;)LX/1mB;

    move-result-object v0

    return-object v0
.end method
