.class public LX/0sS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0sS;


# instance fields
.field public final a:LX/0W3;

.field public final b:LX/0Zb;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(LX/0W3;LX/0Zb;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151994
    iput-object p1, p0, LX/0sS;->a:LX/0W3;

    .line 151995
    iput-object p2, p0, LX/0sS;->b:LX/0Zb;

    .line 151996
    iput-object p3, p0, LX/0sS;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 151997
    return-void
.end method

.method public static a(LX/0QB;)LX/0sS;
    .locals 6

    .prologue
    .line 151998
    sget-object v0, LX/0sS;->d:LX/0sS;

    if-nez v0, :cond_1

    .line 151999
    const-class v1, LX/0sS;

    monitor-enter v1

    .line 152000
    :try_start_0
    sget-object v0, LX/0sS;->d:LX/0sS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152001
    if-eqz v2, :cond_0

    .line 152002
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152003
    new-instance p0, LX/0sS;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/0sS;-><init>(LX/0W3;LX/0Zb;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 152004
    move-object v0, p0

    .line 152005
    sput-object v0, LX/0sS;->d:LX/0sS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152006
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152007
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152008
    :cond_1
    sget-object v0, LX/0sS;->d:LX/0sS;

    return-object v0

    .line 152009
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
