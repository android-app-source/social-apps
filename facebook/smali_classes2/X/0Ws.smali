.class public LX/0Ws;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/2WA;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/2WA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76756
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/2WA;
    .locals 7

    .prologue
    .line 76757
    sget-object v0, LX/0Ws;->a:LX/2WA;

    if-nez v0, :cond_1

    .line 76758
    const-class v1, LX/0Ws;

    monitor-enter v1

    .line 76759
    :try_start_0
    sget-object v0, LX/0Ws;->a:LX/2WA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76760
    if-eqz v2, :cond_0

    .line 76761
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76762
    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v3

    check-cast v3, LX/0VT;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Wt;->a(LX/0QB;)LX/0Wt;

    move-result-object v5

    check-cast v5, LX/0Wt;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const/16 p0, 0x15db

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3, v4, v5, v6, p0}, LX/0Wu;->a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;LX/0SG;LX/0Or;)LX/2WA;

    move-result-object v3

    move-object v0, v3

    .line 76763
    sput-object v0, LX/0Ws;->a:LX/2WA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76764
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76765
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76766
    :cond_1
    sget-object v0, LX/0Ws;->a:LX/2WA;

    return-object v0

    .line 76767
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76768
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 76769
    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v0

    check-cast v0, LX/0VT;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Wt;->a(LX/0QB;)LX/0Wt;

    move-result-object v2

    check-cast v2, LX/0Wt;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 v4, 0x15db

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0Wu;->a(LX/0VT;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Wt;LX/0SG;LX/0Or;)LX/2WA;

    move-result-object v0

    return-object v0
.end method
