.class public LX/15D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lorg/apache/http/client/methods/HttpUriRequest;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/14Q;

.field public final g:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public final h:Lorg/apache/http/client/RedirectHandler;

.field public final i:LX/15F;

.field public final j:LX/14P;

.field public final k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;>;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:I

.field public final n:J

.field public final o:Z

.field public final p:LX/2BD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180237
    const-class v0, LX/15D;

    sput-object v0, LX/15D;->a:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/14Q;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/RedirectHandler;LX/15F;LX/14P;Ljava/lang/String;IJZLX/0am;LX/2BD;)V
    .locals 2
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # LX/2BD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/lang/String;",
            "LX/14Q;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/client/RedirectHandler;",
            "Lcom/facebook/http/interfaces/HttpRequestState;",
            "LX/14P;",
            "Ljava/lang/String;",
            "IJZ",
            "LX/0am",
            "<",
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;>;",
            "LX/2BD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180239
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/methods/HttpUriRequest;

    iput-object v1, p0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 180240
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, LX/15D;->c:Ljava/lang/String;

    .line 180241
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/14Q;

    iput-object v1, p0, LX/15D;->f:LX/14Q;

    .line 180242
    iput-object p3, p0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 180243
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/ResponseHandler;

    iput-object v1, p0, LX/15D;->g:Lorg/apache/http/client/ResponseHandler;

    .line 180244
    iput-object p4, p0, LX/15D;->e:Ljava/lang/String;

    .line 180245
    iput-object p7, p0, LX/15D;->h:Lorg/apache/http/client/RedirectHandler;

    .line 180246
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15F;

    iput-object v1, p0, LX/15D;->i:LX/15F;

    .line 180247
    invoke-static {p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/14P;

    iput-object v1, p0, LX/15D;->j:LX/14P;

    .line 180248
    iput-object p10, p0, LX/15D;->l:Ljava/lang/String;

    .line 180249
    iput p11, p0, LX/15D;->m:I

    .line 180250
    iput-wide p12, p0, LX/15D;->n:J

    .line 180251
    move/from16 v0, p14

    iput-boolean v0, p0, LX/15D;->o:Z

    .line 180252
    invoke-static/range {p15 .. p15}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, LX/15D;->k:LX/0am;

    .line 180253
    move-object/from16 v0, p16

    iput-object v0, p0, LX/15D;->p:LX/2BD;

    .line 180254
    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/14Q;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/RedirectHandler;LX/15F;LX/14P;Ljava/lang/String;IJZLX/0am;LX/2BD;B)V
    .locals 0

    .prologue
    .line 180255
    invoke-direct/range {p0 .. p16}, LX/15D;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/14Q;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/RedirectHandler;LX/15F;LX/14P;Ljava/lang/String;IJZLX/0am;LX/2BD;)V

    return-void
.end method

.method public static a(LX/15D;)LX/15E;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15D",
            "<TT;>;)",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180256
    new-instance v1, LX/15E;

    invoke-direct {v1}, LX/15E;-><init>()V

    .line 180257
    iget-object v0, p0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 180258
    iput-object v0, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 180259
    move-object v0, v1

    .line 180260
    iget-object v2, p0, LX/15D;->i:LX/15F;

    move-object v2, v2

    .line 180261
    iput-object v2, v0, LX/15E;->i:LX/15F;

    .line 180262
    move-object v0, v0

    .line 180263
    iget-object v2, p0, LX/15D;->e:Ljava/lang/String;

    move-object v2, v2

    .line 180264
    iput-object v2, v0, LX/15E;->e:Ljava/lang/String;

    .line 180265
    move-object v0, v0

    .line 180266
    iget-object v2, p0, LX/15D;->f:LX/14Q;

    move-object v2, v2

    .line 180267
    iput-object v2, v0, LX/15E;->f:LX/14Q;

    .line 180268
    move-object v0, v0

    .line 180269
    iget-object v2, p0, LX/15D;->c:Ljava/lang/String;

    move-object v2, v2

    .line 180270
    iput-object v2, v0, LX/15E;->c:Ljava/lang/String;

    .line 180271
    move-object v0, v0

    .line 180272
    iget-object v2, p0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v2, v2

    .line 180273
    iput-object v2, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 180274
    move-object v0, v0

    .line 180275
    iget-object v2, p0, LX/15D;->j:LX/14P;

    move-object v2, v2

    .line 180276
    iput-object v2, v0, LX/15E;->j:LX/14P;

    .line 180277
    move-object v0, v0

    .line 180278
    iget-object v2, p0, LX/15D;->l:Ljava/lang/String;

    move-object v2, v2

    .line 180279
    iput-object v2, v0, LX/15E;->l:Ljava/lang/String;

    .line 180280
    move-object v0, v0

    .line 180281
    iget-object v2, p0, LX/15D;->h:Lorg/apache/http/client/RedirectHandler;

    move-object v2, v2

    .line 180282
    iput-object v2, v0, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 180283
    move-object v0, v0

    .line 180284
    iget-object v2, p0, LX/15D;->g:Lorg/apache/http/client/ResponseHandler;

    move-object v2, v2

    .line 180285
    iput-object v2, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 180286
    move-object v0, v0

    .line 180287
    iget v2, p0, LX/15D;->m:I

    move v2, v2

    .line 180288
    iput v2, v0, LX/15E;->n:I

    .line 180289
    move-object v0, v0

    .line 180290
    iget-wide v4, p0, LX/15D;->n:J

    move-wide v2, v4

    .line 180291
    iput-wide v2, v0, LX/15E;->o:J

    .line 180292
    move-object v0, v0

    .line 180293
    iget-boolean v2, p0, LX/15D;->o:Z

    move v2, v2

    .line 180294
    iput-boolean v2, v0, LX/15E;->p:Z

    .line 180295
    move-object v0, v0

    .line 180296
    iget-object v2, p0, LX/15D;->p:LX/2BD;

    move-object v2, v2

    .line 180297
    iput-object v2, v0, LX/15E;->q:LX/2BD;

    .line 180298
    iget-object v0, p0, LX/15D;->k:LX/0am;

    move-object v0, v0

    .line 180299
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180300
    iget-object v0, p0, LX/15D;->k:LX/0am;

    move-object v0, v0

    .line 180301
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, LX/15E;->a(Ljava/util/List;)LX/15E;

    .line 180302
    :cond_0
    return-object v1
.end method

.method public static newBuilder()LX/15E;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/15E",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180303
    new-instance v0, LX/15E;

    invoke-direct {v0}, LX/15E;-><init>()V

    return-object v0
.end method

.method private s()Lcom/facebook/http/interfaces/RequestStage;
    .locals 1

    .prologue
    .line 180304
    iget-object v0, p0, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 180305
    iget-object p0, v0, LX/15F;->e:Lcom/facebook/http/interfaces/RequestStage;

    move-object v0, p0

    .line 180306
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 2

    .prologue
    .line 180307
    iget v0, p0, LX/15D;->m:I

    move v0, v0

    .line 180308
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/common/FbHttpUtils;->a(Lcom/facebook/http/interfaces/RequestPriority;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LX/15D;->s()Lcom/facebook/http/interfaces/RequestStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/interfaces/RequestStage;->toChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/15D;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "(big) "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 180309
    iget-object v1, p0, LX/15D;->c:Ljava/lang/String;

    move-object v1, v1

    .line 180310
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "__"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180311
    return-object p1

    .line 180312
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestStage;)V
    .locals 1

    .prologue
    .line 180313
    iget-object v0, p0, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 180314
    iput-object p1, v0, LX/15F;->e:Lcom/facebook/http/interfaces/RequestStage;

    .line 180315
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180235
    iget-object v0, p0, LX/15D;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 180236
    iget-object v0, p0, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final f()Lorg/apache/http/client/ResponseHandler;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 180234
    iget-object v0, p0, LX/15D;->g:Lorg/apache/http/client/ResponseHandler;

    return-object v0
.end method

.method public final h()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 180231
    iget-object v0, p0, LX/15D;->i:LX/15F;

    move-object v0, v0

    .line 180232
    iget-object p0, v0, LX/15F;->d:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, p0

    .line 180233
    return-object v0
.end method

.method public final i()LX/15F;
    .locals 1

    .prologue
    .line 180230
    iget-object v0, p0, LX/15D;->i:LX/15F;

    return-object v0
.end method

.method public final j()LX/14P;
    .locals 1

    .prologue
    .line 180229
    iget-object v0, p0, LX/15D;->j:LX/14P;

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 180228
    iget v0, p0, LX/15D;->m:I

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 180219
    iget-boolean v0, p0, LX/15D;->o:Z

    return v0
.end method

.method public final o()LX/2BD;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 180227
    iget-object v0, p0, LX/15D;->p:LX/2BD;

    return-object v0
.end method

.method public final q()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 180224
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/15D;->n:J

    sub-long/2addr v2, v4

    .line 180225
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 180226
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 180221
    iget-object v0, p0, LX/15D;->c:Ljava/lang/String;

    move-object v0, v0

    .line 180222
    iget-boolean v1, p0, LX/15D;->o:Z

    move v1, v1

    .line 180223
    if-nez v1, :cond_0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "getVideo-1RT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "rangeRequestForVideo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, LX/15D;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
