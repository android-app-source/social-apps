.class public LX/0eg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0eg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0aN;

.field public final c:LX/0aa;

.field public final d:LX/0ag;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field private final g:Z

.field public final h:LX/0ai;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile i:LX/0ej;

.field public volatile j:LX/0ej;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92243
    const-class v0, LX/0eg;

    sput-object v0, LX/0eg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)V
    .locals 3
    .param p7    # LX/0ai;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92245
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 92246
    if-eqz p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 92247
    if-eqz p3, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 92248
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 92249
    iput-object p1, p0, LX/0eg;->b:LX/0aN;

    .line 92250
    iput-object p2, p0, LX/0eg;->c:LX/0aa;

    .line 92251
    iput-object p3, p0, LX/0eg;->d:LX/0ag;

    .line 92252
    iput-object p4, p0, LX/0eg;->e:Ljava/lang/String;

    .line 92253
    iput-boolean p5, p0, LX/0eg;->f:Z

    .line 92254
    iput-boolean p6, p0, LX/0eg;->g:Z

    .line 92255
    iput-object p7, p0, LX/0eg;->h:LX/0ai;

    .line 92256
    return-void

    :cond_0
    move v0, v2

    .line 92257
    goto :goto_0

    :cond_1
    move v0, v2

    .line 92258
    goto :goto_1

    :cond_2
    move v0, v2

    .line 92259
    goto :goto_2

    :cond_3
    move v1, v2

    .line 92260
    goto :goto_3
.end method

.method public static a(LX/0eg;Z)V
    .locals 6

    .prologue
    .line 92261
    iget-object v0, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v0}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v0

    .line 92262
    invoke-static {}, LX/0ag;->a()LX/0ag;

    move-result-object v1

    .line 92263
    new-instance v2, LX/0ej;

    new-instance v3, LX/26w;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, LX/26w;-><init>(I)V

    invoke-direct {v2, v3}, LX/0ej;-><init>(LX/26w;)V

    .line 92264
    new-instance v3, LX/26w;

    iget-object v4, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v4}, LX/0aa;->a()I

    move-result v4

    invoke-direct {v3, v4}, LX/26w;-><init>(I)V

    .line 92265
    iget-object v4, p0, LX/0eg;->d:LX/0ag;

    new-instance v5, LX/26x;

    invoke-direct {v5, v1, v2, v3}, LX/26x;-><init>(LX/0ag;LX/0ej;LX/26w;)V

    invoke-virtual {v4, v5}, LX/0ag;->a(LX/26y;)V

    .line 92266
    :try_start_0
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92267
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    iget-object v4, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v4}, LX/0aa;->d()[B

    move-result-object v4

    invoke-virtual {v1, v2, v0, v4}, LX/0aN;->b(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92268
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v3}, LX/26w;->a()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92269
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch LX/5ol; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92270
    :cond_0
    :goto_0
    return-void

    .line 92271
    :catch_0
    :goto_1
    if-nez p1, :cond_0

    .line 92272
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 92273
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0eg;->a(LX/0eg;Z)V

    goto :goto_0

    .line 92274
    :catch_1
    goto :goto_1
.end method

.method public static b(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)LX/0eg;
    .locals 8
    .param p6    # LX/0ai;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 92275
    new-instance v0, LX/0eg;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/0eg;-><init>(LX/0aN;LX/0aa;LX/0ag;Ljava/lang/String;ZZLX/0ai;)V

    return-object v0
.end method

.method public static f(LX/0eg;)V
    .locals 3

    .prologue
    .line 92276
    iget-object v0, p0, LX/0eg;->c:LX/0aa;

    move-object v0, v0

    .line 92277
    invoke-interface {v0}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v0

    .line 92278
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 92279
    new-instance v1, LX/0ej;

    invoke-direct {v1, v0}, LX/0ej;-><init>(Ljava/nio/ByteBuffer;)V

    .line 92280
    iput-object v1, p0, LX/0eg;->i:LX/0ej;

    .line 92281
    iput-object v1, p0, LX/0eg;->j:LX/0ej;

    .line 92282
    return-void
.end method

.method public static g(LX/0eg;)V
    .locals 3

    .prologue
    .line 92283
    new-instance v0, LX/0ej;

    new-instance v1, LX/26w;

    iget-object v2, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v2}, LX/0aa;->a()I

    move-result v2

    invoke-direct {v1, v2}, LX/26w;-><init>(I)V

    invoke-direct {v0, v1}, LX/0ej;-><init>(LX/26w;)V

    .line 92284
    iput-object v0, p0, LX/0eg;->i:LX/0ej;

    .line 92285
    iput-object v0, p0, LX/0eg;->j:LX/0ej;

    .line 92286
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 92287
    iget-object v0, p0, LX/0eg;->b:LX/0aN;

    iget-object v1, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0aN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92288
    if-nez v0, :cond_2

    iget-boolean v1, p0, LX/0eg;->f:Z

    if-nez v1, :cond_2

    .line 92289
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/0eg;->a(LX/0eg;Z)V

    .line 92290
    :cond_0
    :goto_0
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 92291
    iget-object v0, p0, LX/0eg;->b:LX/0aN;

    iget-object v1, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0aN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92292
    if-nez v0, :cond_3

    iget-boolean v1, p0, LX/0eg;->f:Z

    if-eqz v1, :cond_3

    .line 92293
    sget-object v0, LX/0eg;->a:Ljava/lang/Class;

    const-string v1, "Using temporary empty view for read-only store"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 92294
    iget-object v0, p0, LX/0eg;->h:LX/0ai;

    if-eqz v0, :cond_1

    .line 92295
    iget-object v0, p0, LX/0eg;->h:LX/0ai;

    const-string v1, "qe_store_init_views_read_only"

    const-string v2, "temporary empty view for read-only store"

    invoke-virtual {v0, v1, v2}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92296
    :cond_1
    invoke-static {p0}, LX/0eg;->g(LX/0eg;)V

    .line 92297
    :goto_1
    return-void

    .line 92298
    :cond_2
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/0eg;->f:Z

    if-eqz v0, :cond_0

    .line 92299
    sget-object v0, LX/0eg;->a:Ljava/lang/Class;

    const-string v1, "Cannot create empty view for read-only store"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 92300
    :cond_3
    if-eqz v0, :cond_6

    .line 92301
    iget-object v1, p0, LX/0eg;->c:LX/0aa;

    move-object v1, v1

    .line 92302
    invoke-interface {v1}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 92303
    :try_start_0
    iget-boolean v1, p0, LX/0eg;->f:Z

    if-eqz v1, :cond_9

    .line 92304
    iget-object v0, p0, LX/0eg;->h:LX/0ai;

    if-eqz v0, :cond_4

    .line 92305
    iget-object v0, p0, LX/0eg;->h:LX/0ai;

    const-string v1, "qe_store_init_views_read_only"

    const-string v2, "cannot upgrade read-only store"

    invoke-virtual {v0, v1, v2}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92306
    :cond_4
    sget-object v0, LX/0eg;->a:Ljava/lang/Class;

    const-string v1, "Cannot upgrade read-only store"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 92307
    invoke-static {p0}, LX/0eg;->g(LX/0eg;)V
    :try_end_0
    .catch LX/5ol; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_1

    .line 92308
    :catch_0
    move-exception v0

    .line 92309
    :goto_2
    invoke-static {p0, v4}, LX/0eg;->a(LX/0eg;Z)V

    .line 92310
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    if-eqz v1, :cond_5

    .line 92311
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    const-string v2, "qe_store_init_views_failure"

    invoke-virtual {v1, v2, v0, v3}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 92312
    :cond_5
    sget-object v1, LX/0eg;->a:Ljava/lang/Class;

    const-string v2, "Could not upgrade"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92313
    :cond_6
    :goto_3
    :try_start_1
    invoke-static {p0}, LX/0eg;->f(LX/0eg;)V
    :try_end_1
    .catch LX/5ol; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 92314
    :catch_1
    move-exception v0

    .line 92315
    :goto_4
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    if-eqz v1, :cond_7

    .line 92316
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    const-string v2, "qe_store_load_view_failure"

    invoke-virtual {v1, v2, v0, v3}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 92317
    :cond_7
    invoke-static {p0, v4}, LX/0eg;->a(LX/0eg;Z)V

    .line 92318
    sget-object v1, LX/0eg;->a:Ljava/lang/Class;

    const-string v2, "Could not load current view"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92319
    :try_start_2
    invoke-static {p0}, LX/0eg;->f(LX/0eg;)V
    :try_end_2
    .catch LX/5ol; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_1

    .line 92320
    :catch_2
    move-exception v0

    .line 92321
    :goto_5
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    if-eqz v1, :cond_8

    .line 92322
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    const-string v2, "qe_store_load_empty_view_failure"

    invoke-virtual {v1, v2, v0, v3}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 92323
    :cond_8
    sget-object v1, LX/0eg;->a:Ljava/lang/Class;

    const-string v2, "Could not load empty current view"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92324
    invoke-static {p0}, LX/0eg;->g(LX/0eg;)V

    goto/16 :goto_1

    .line 92325
    :cond_9
    :try_start_3
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    .line 92326
    invoke-static {v1, v2, v0}, LX/0aN;->j(LX/0aN;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 92327
    invoke-static {v5}, LX/0aN;->b(Ljava/io/File;)Ljava/nio/ByteBuffer;

    move-result-object v5

    move-object v1, v5

    .line 92328
    iget-object v2, p0, LX/0eg;->b:LX/0aN;

    iget-object v5, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, LX/0aN;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 92329
    iget-object v5, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v5}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v5

    .line 92330
    new-instance v6, LX/0ag;

    const/4 v7, 0x1

    invoke-direct {v6, v1, v7}, LX/0ag;-><init>(Ljava/nio/ByteBuffer;Z)V

    move-object v6, v6

    .line 92331
    new-instance v7, LX/0ej;

    invoke-direct {v7, v2}, LX/0ej;-><init>(Ljava/nio/ByteBuffer;)V

    .line 92332
    new-instance v8, LX/26w;

    iget-object v9, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v9}, LX/0aa;->a()I

    move-result v9

    invoke-direct {v8, v9}, LX/26w;-><init>(I)V

    .line 92333
    iget-object v9, p0, LX/0eg;->d:LX/0ag;

    new-instance v0, LX/26x;

    invoke-direct {v0, v6, v7, v8}, LX/26x;-><init>(LX/0ag;LX/0ej;LX/26w;)V

    invoke-virtual {v9, v0}, LX/0ag;->a(LX/26y;)V

    .line 92334
    invoke-virtual {v8}, LX/26w;->a()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    move-object v1, v6

    .line 92335
    iget-object v2, p0, LX/0eg;->b:LX/0aN;

    iget-object v6, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v2, v6, v5}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92336
    iget-object v2, p0, LX/0eg;->b:LX/0aN;

    iget-object v6, p0, LX/0eg;->e:Ljava/lang/String;

    iget-object v7, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v7}, LX/0aa;->d()[B

    move-result-object v7

    invoke-virtual {v2, v6, v5, v7}, LX/0aN;->b(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92337
    iget-object v2, p0, LX/0eg;->b:LX/0aN;

    iget-object v6, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v2, v6, v5, v1}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92338
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, LX/0aN;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch LX/5ol; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 92339
    goto/16 :goto_3

    .line 92340
    :catch_3
    move-exception v0

    goto/16 :goto_2

    .line 92341
    :catch_4
    move-exception v0

    goto/16 :goto_4

    .line 92342
    :catch_5
    move-exception v0

    goto/16 :goto_5
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 92343
    iget-boolean v0, p0, LX/0eg;->f:Z

    if-eqz v0, :cond_1

    .line 92344
    :cond_0
    :goto_0
    return-void

    .line 92345
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/0eg;->c:LX/0aa;

    invoke-interface {v0}, LX/0aa;->c()Ljava/lang/String;

    move-result-object v0

    .line 92346
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92347
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/0aN;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 92348
    iget-object v1, p0, LX/0eg;->b:LX/0aN;

    iget-object v2, p0, LX/0eg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0aN;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 92349
    new-instance v1, LX/0ej;

    invoke-direct {v1, v0}, LX/0ej;-><init>(Ljava/nio/ByteBuffer;)V

    .line 92350
    iput-object v1, p0, LX/0eg;->j:LX/0ej;

    .line 92351
    iget-boolean v0, p0, LX/0eg;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0eg;->i:LX/0ej;

    .line 92352
    iget-boolean v2, v0, LX/0ej;->c:Z

    move v0, v2

    .line 92353
    if-eqz v0, :cond_0

    .line 92354
    iput-object v1, p0, LX/0eg;->i:LX/0ej;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/5ol; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 92355
    :catch_0
    move-exception v0

    .line 92356
    :goto_1
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    if-eqz v1, :cond_2

    .line 92357
    iget-object v1, p0, LX/0eg;->h:LX/0ai;

    const-string v2, "qe_store_update_failure"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/0ai;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 92358
    :cond_2
    sget-object v1, LX/0eg;->a:Ljava/lang/Class;

    const-string v2, "Could not update data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 92359
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final c()LX/0ej;
    .locals 1

    .prologue
    .line 92360
    iget-object v0, p0, LX/0eg;->i:LX/0ej;

    return-object v0
.end method

.method public final d()LX/0ej;
    .locals 1

    .prologue
    .line 92361
    iget-object v0, p0, LX/0eg;->j:LX/0ej;

    return-object v0
.end method
