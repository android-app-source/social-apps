.class public LX/10R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 169142
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "apptab/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 169143
    sput-object v0, LX/10R;->e:LX/0Tn;

    const-string v1, "next_immersive_tab_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/10R;->a:LX/0Tn;

    .line 169144
    sget-object v0, LX/10R;->e:LX/0Tn;

    const-string v1, "next_memory_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/10R;->b:LX/0Tn;

    .line 169145
    sget-object v0, LX/10R;->e:LX/0Tn;

    const-string v1, "next_polling_config"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/10R;->c:LX/0Tn;

    .line 169146
    sget-object v0, LX/10R;->e:LX/0Tn;

    const-string v1, "tab_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/10R;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169149
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169147
    sget-object v0, LX/10R;->a:LX/0Tn;

    sget-object v1, LX/10R;->b:LX/0Tn;

    sget-object v2, LX/10R;->c:LX/0Tn;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
