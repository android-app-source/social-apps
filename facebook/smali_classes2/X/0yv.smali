.class public LX/0yv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yn;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0yv;


# instance fields
.field private final a:LX/0yw;

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0yw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166350
    iput-object p1, p0, LX/0yv;->a:LX/0yw;

    .line 166351
    return-void
.end method

.method public static a(LX/0QB;)LX/0yv;
    .locals 4

    .prologue
    .line 166352
    sget-object v0, LX/0yv;->d:LX/0yv;

    if-nez v0, :cond_1

    .line 166353
    const-class v1, LX/0yv;

    monitor-enter v1

    .line 166354
    :try_start_0
    sget-object v0, LX/0yv;->d:LX/0yv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166355
    if-eqz v2, :cond_0

    .line 166356
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 166357
    new-instance p0, LX/0yv;

    invoke-static {v0}, LX/0yw;->a(LX/0QB;)LX/0yw;

    move-result-object v3

    check-cast v3, LX/0yw;

    invoke-direct {p0, v3}, LX/0yv;-><init>(LX/0yw;)V

    .line 166358
    move-object v0, p0

    .line 166359
    sput-object v0, LX/0yv;->d:LX/0yv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166360
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166361
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166362
    :cond_1
    sget-object v0, LX/0yv;->d:LX/0yv;

    return-object v0

    .line 166363
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 166365
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, LX/0yv;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166366
    monitor-exit p0

    return-void

    .line 166367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()J
    .locals 2

    .prologue
    .line 166368
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0yv;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 166369
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    invoke-virtual {v0}, LX/0yw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166370
    :goto_0
    return-void

    .line 166371
    :cond_0
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    .line 166372
    iget-object p0, v0, LX/0yw;->h:LX/0z4;

    invoke-virtual {p0}, LX/0z4;->a()V

    .line 166373
    goto :goto_0
.end method

.method public final a(LX/0y8;)V
    .locals 3

    .prologue
    .line 166374
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    invoke-virtual {v0}, LX/0yw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166375
    :goto_0
    return-void

    .line 166376
    :cond_0
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    invoke-virtual {v0}, LX/0yw;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/0yv;->a(J)V

    .line 166377
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    .line 166378
    sget-object v1, LX/14q;->REACTION:LX/14q;

    .line 166379
    invoke-virtual {v0}, LX/0yw;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 166380
    const/4 v2, 0x0

    .line 166381
    :goto_1
    move v1, v2

    .line 166382
    move v0, v1

    .line 166383
    invoke-virtual {p1, v0}, LX/0y8;->a(I)V

    .line 166384
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    .line 166385
    invoke-virtual {v0}, LX/0yw;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 166386
    const/4 v1, 0x0

    .line 166387
    :goto_2
    move v0, v1

    .line 166388
    invoke-virtual {p1, v0}, LX/0y8;->b(I)V

    .line 166389
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    .line 166390
    invoke-virtual {v0}, LX/0yw;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 166391
    const/4 v1, 0x0

    .line 166392
    :goto_3
    move v0, v1

    .line 166393
    invoke-virtual {p1, v0}, LX/0y8;->c(I)V

    .line 166394
    iget-object v0, p0, LX/0yv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0y8;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 166395
    :cond_1
    iget-object v2, v0, LX/0yw;->h:LX/0z4;

    invoke-virtual {v2}, LX/0z4;->a()V

    .line 166396
    iget-object v2, v0, LX/0yw;->b:LX/0yy;

    .line 166397
    invoke-static {v2, v1}, LX/0yy;->b(LX/0yy;LX/14q;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    move v2, v0

    .line 166398
    goto :goto_1

    .line 166399
    :cond_2
    iget-object v1, v0, LX/0yw;->h:LX/0z4;

    invoke-virtual {v1}, LX/0z4;->a()V

    .line 166400
    iget-object v1, v0, LX/0yw;->c:LX/0z0;

    .line 166401
    iget-object v0, v1, LX/0z0;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    move v1, v0

    .line 166402
    goto :goto_2

    .line 166403
    :cond_3
    iget-object v1, v0, LX/0yw;->h:LX/0z4;

    invoke-virtual {v1}, LX/0z4;->a()V

    .line 166404
    iget-object v1, v0, LX/0yw;->c:LX/0z0;

    .line 166405
    iget-object v0, v1, LX/0z0;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    move v1, v0

    .line 166406
    goto :goto_3
.end method

.method public final a(LX/0yl;)V
    .locals 3

    .prologue
    .line 166407
    const/4 v0, 0x3

    new-array v0, v0, [LX/0yo;

    const/4 v1, 0x0

    sget-object v2, LX/0yo;->REACTION_COUNT:LX/0yo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/0yo;->VIDEO_PLAY_COUNT:LX/0yo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/0yo;->VIDEO_PLAY_SECS:LX/0yo;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, LX/0yl;->a([LX/0yo;)Z

    move-result v0

    move v0, v0

    .line 166408
    if-eqz v0, :cond_0

    .line 166409
    iget-object v0, p1, LX/0yl;->c:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    move-object v0, v0

    .line 166410
    if-nez v0, :cond_1

    .line 166411
    :cond_0
    :goto_0
    return-void

    .line 166412
    :cond_1
    iget-object v0, p0, LX/0yv;->a:LX/0yw;

    .line 166413
    iget-object v1, p1, LX/0yl;->c:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    move-object v1, v1

    .line 166414
    invoke-virtual {v0, v1}, LX/0yw;->a(Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;)V

    .line 166415
    iget-object v0, p1, LX/0yl;->c:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    move-object v0, v0

    .line 166416
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mVersion:Ljava/lang/String;

    move-object v0, v1

    .line 166417
    iput-object v0, p0, LX/0yv;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 166418
    iget-object v1, p0, LX/0yv;->a:LX/0yw;

    invoke-virtual {v1}, LX/0yw;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 166419
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/0yv;->a:LX/0yw;

    invoke-virtual {v1}, LX/0yw;->c()J

    move-result-wide v2

    invoke-direct {p0}, LX/0yv;->c()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
