.class public LX/1cR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Ao;

.field private final c:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Fh;LX/1Ao;LX/1cF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 282072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282073
    iput-object p1, p0, LX/1cR;->a:LX/1Fh;

    .line 282074
    iput-object p2, p0, LX/1cR;->b:LX/1Ao;

    .line 282075
    iput-object p3, p0, LX/1cR;->c:LX/1cF;

    .line 282076
    return-void
.end method


# virtual methods
.method public a(LX/1cd;LX/1bh;)LX/1cd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1bh;",
            ")",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 282071
    new-instance v0, LX/1eO;

    invoke-direct {v0, p0, p1, p2}, LX/1eO;-><init>(LX/1cR;LX/1cd;LX/1bh;)V

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282044
    const-string v0, "BitmapMemoryCacheProducer"

    return-object v0
.end method

.method public final a(LX/1cd;LX/1cW;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 282045
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v2, v0

    .line 282046
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v0

    .line 282047
    invoke-virtual {p0}, LX/1cR;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282048
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 282049
    iget-object v4, p2, LX/1cW;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 282050
    iget-object v5, p0, LX/1cR;->b:LX/1Ao;

    invoke-virtual {v5, v0, v4}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v4

    .line 282051
    iget-object v0, p0, LX/1cR;->a:LX/1Fh;

    invoke-interface {v0, v4}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v5

    .line 282052
    if-eqz v5, :cond_2

    .line 282053
    invoke-virtual {v5}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    invoke-virtual {v0}, LX/1ln;->d()LX/1lk;

    move-result-object v0

    .line 282054
    iget-boolean v6, v0, LX/1lk;->d:Z

    move v6, v6

    .line 282055
    if-eqz v6, :cond_0

    .line 282056
    invoke-virtual {p0}, LX/1cR;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v3}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "cached_value_found"

    const-string v8, "true"

    invoke-static {v0, v8}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    invoke-interface {v2, v3, v7, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 282057
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, LX/1cd;->b(F)V

    .line 282058
    :cond_0
    invoke-virtual {p1, v5, v6}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 282059
    invoke-virtual {v5}, LX/1FJ;->close()V

    .line 282060
    if-eqz v6, :cond_2

    .line 282061
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 282062
    goto :goto_0

    .line 282063
    :cond_2
    iget-object v0, p2, LX/1cW;->e:LX/1bY;

    move-object v0, v0

    .line 282064
    invoke-virtual {v0}, LX/1bY;->getValue()I

    move-result v0

    sget-object v5, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    invoke-virtual {v5}, LX/1bY;->getValue()I

    move-result v5

    if-lt v0, v5, :cond_4

    .line 282065
    invoke-virtual {p0}, LX/1cR;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "cached_value_found"

    const-string v5, "false"

    invoke-static {v0, v5}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_2
    invoke-interface {v2, v3, v4, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 282066
    const/4 v0, 0x1

    invoke-virtual {p1, v1, v0}, LX/1cd;->b(Ljava/lang/Object;Z)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 282067
    goto :goto_2

    .line 282068
    :cond_4
    invoke-virtual {p0, p1, v4}, LX/1cR;->a(LX/1cd;LX/1bh;)LX/1cd;

    move-result-object v0

    .line 282069
    invoke-virtual {p0}, LX/1cR;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v1, "cached_value_found"

    const-string v5, "false"

    invoke-static {v1, v5}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    :cond_5
    invoke-interface {v2, v3, v4, v1}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 282070
    iget-object v1, p0, LX/1cR;->c:LX/1cF;

    invoke-interface {v1, v0, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_1
.end method
