.class public LX/1o7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;IILX/0zr;)Landroid/text/StaticLayout;
    .locals 14

    .prologue
    .line 318427
    new-instance v0, Landroid/text/StaticLayout;

    invoke-static/range {p12 .. p12}, LX/1o7;->a(LX/0zr;)Landroid/text/TextDirectionHeuristic;

    move-result-object v7

    move-object v1, p0

    move v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    invoke-direct/range {v0 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    return-object v0
.end method

.method private static a(LX/0zr;)Landroid/text/TextDirectionHeuristic;
    .locals 1

    .prologue
    .line 318428
    sget-object v0, LX/0zo;->a:LX/0zr;

    if-ne p0, v0, :cond_0

    .line 318429
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 318430
    :goto_0
    return-object v0

    .line 318431
    :cond_0
    sget-object v0, LX/0zo;->b:LX/0zr;

    if-ne p0, v0, :cond_1

    .line 318432
    sget-object v0, Landroid/text/TextDirectionHeuristics;->RTL:Landroid/text/TextDirectionHeuristic;

    goto :goto_0

    .line 318433
    :cond_1
    sget-object v0, LX/0zo;->c:LX/0zr;

    if-ne p0, v0, :cond_2

    .line 318434
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    goto :goto_0

    .line 318435
    :cond_2
    sget-object v0, LX/0zo;->d:LX/0zr;

    if-ne p0, v0, :cond_3

    .line 318436
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_RTL:Landroid/text/TextDirectionHeuristic;

    goto :goto_0

    .line 318437
    :cond_3
    sget-object v0, LX/0zo;->e:LX/0zr;

    if-ne p0, v0, :cond_4

    .line 318438
    sget-object v0, Landroid/text/TextDirectionHeuristics;->ANYRTL_LTR:Landroid/text/TextDirectionHeuristic;

    goto :goto_0

    .line 318439
    :cond_4
    sget-object v0, LX/0zo;->f:LX/0zr;

    if-ne p0, v0, :cond_5

    .line 318440
    sget-object v0, Landroid/text/TextDirectionHeuristics;->LOCALE:Landroid/text/TextDirectionHeuristic;

    goto :goto_0

    .line 318441
    :cond_5
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    goto :goto_0
.end method
