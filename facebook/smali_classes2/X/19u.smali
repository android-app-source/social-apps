.class public final LX/19u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/19s;


# direct methods
.method public constructor <init>(LX/19s;)V
    .locals 0

    .prologue
    .line 208998
    iput-object p1, p0, LX/19u;->a:LX/19s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 208999
    if-nez p2, :cond_0

    .line 209000
    :goto_0
    return-void

    .line 209001
    :cond_0
    const/4 v2, 0x1

    .line 209002
    iget-object v0, p0, LX/19u;->a:LX/19s;

    .line 209003
    if-nez p2, :cond_3

    .line 209004
    const/4 v3, 0x0

    .line 209005
    :goto_1
    move-object v3, v3

    .line 209006
    iput-object v3, v0, LX/19s;->c:LX/04j;

    .line 209007
    :try_start_0
    iget-object v0, p0, LX/19u;->a:LX/19s;

    iget-object v0, v0, LX/19s;->c:LX/04j;

    iget-object v3, p0, LX/19u;->a:LX/19s;

    iget-object v3, v3, LX/19s;->i:Ljava/util/Map;

    invoke-interface {v0, v3}, LX/04j;->a(Ljava/util/Map;)V

    .line 209008
    iget-object v0, p0, LX/19u;->a:LX/19s;

    invoke-static {v0}, LX/19s;->m(LX/19s;)V

    .line 209009
    iget-object v0, p0, LX/19u;->a:LX/19s;

    invoke-static {v0}, LX/19s;->l(LX/19s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 209010
    :goto_2
    :try_start_1
    iget-object v2, p0, LX/19u;->a:LX/19s;

    iget-object v2, v2, LX/19s;->c:LX/04j;

    iget-object v3, p0, LX/19u;->a:LX/19s;

    iget-object v3, v3, LX/19s;->m:LX/19r;

    invoke-interface {v2, v3}, LX/04j;->a(LX/042;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 209011
    :cond_1
    :goto_3
    iget-object v1, p0, LX/19u;->a:LX/19s;

    invoke-static {v1, v0}, LX/19s;->b(LX/19s;Z)V

    goto :goto_0

    .line 209012
    :catch_0
    move-exception v0

    .line 209013
    sget-object v3, LX/19s;->a:Ljava/lang/String;

    const-string v4, "Service RemoteException when setExperimentationConfigs"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209014
    invoke-virtual {v0}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Landroid/os/DeadObjectException;

    if-eqz v0, :cond_2

    move v0, v1

    .line 209015
    goto :goto_2

    .line 209016
    :catch_1
    move-exception v2

    .line 209017
    sget-object v3, LX/19s;->a:Ljava/lang/String;

    const-string v4, "Service RemoteException when setNonPlayerSessionListener"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209018
    invoke-virtual {v2}, Landroid/os/RemoteException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Landroid/os/DeadObjectException;

    if-eqz v2, :cond_1

    move v0, v1

    .line 209019
    goto :goto_3

    :cond_2
    move v0, v2

    goto :goto_2

    .line 209020
    :cond_3
    const-string v3, "com.facebook.exoplayer.ipc.VideoPlayerServiceApi"

    invoke-interface {p2, v3}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v3

    .line 209021
    if-eqz v3, :cond_4

    instance-of v4, v3, LX/04j;

    if-eqz v4, :cond_4

    .line 209022
    check-cast v3, LX/04j;

    goto :goto_1

    .line 209023
    :cond_4
    new-instance v3, LX/04i;

    invoke-direct {v3, p2}, LX/04i;-><init>(Landroid/os/IBinder;)V

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 209024
    iget-object v0, p0, LX/19u;->a:LX/19s;

    const/4 v1, 0x0

    .line 209025
    iput-object v1, v0, LX/19s;->c:LX/04j;

    .line 209026
    iget-object v0, p0, LX/19u;->a:LX/19s;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/19s;->b(LX/19s;Z)V

    .line 209027
    return-void
.end method
