.class public LX/1nH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1nI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/1nJ;

.field private final d:LX/1nK;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final g:LX/0SI;

.field private final h:LX/0ad;

.field private final i:LX/1nL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 315827
    const-class v0, LX/1nH;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1nH;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1nJ;LX/1nK;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0SI;LX/0ad;LX/1nL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 315828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315829
    iput-object p1, p0, LX/1nH;->b:LX/03V;

    .line 315830
    iput-object p2, p0, LX/1nH;->c:LX/1nJ;

    .line 315831
    iput-object p3, p0, LX/1nH;->d:LX/1nK;

    .line 315832
    iput-object p4, p0, LX/1nH;->e:Lcom/facebook/content/SecureContextHelper;

    .line 315833
    iput-object p5, p0, LX/1nH;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 315834
    iput-object p6, p0, LX/1nH;->g:LX/0SI;

    .line 315835
    iput-object p7, p0, LX/1nH;->h:LX/0ad;

    .line 315836
    iput-object p8, p0, LX/1nH;->i:LX/1nL;

    .line 315837
    return-void
.end method

.method public static a(LX/0QB;)LX/1nH;
    .locals 12

    .prologue
    .line 315840
    const-class v1, LX/1nH;

    monitor-enter v1

    .line 315841
    :try_start_0
    sget-object v0, LX/1nH;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 315842
    sput-object v2, LX/1nH;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 315843
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315844
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 315845
    new-instance v3, LX/1nH;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v5

    check-cast v5, LX/1nJ;

    invoke-static {v0}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v6

    check-cast v6, LX/1nK;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v9

    check-cast v9, LX/0SI;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/1nL;->b(LX/0QB;)LX/1nL;

    move-result-object v11

    check-cast v11, LX/1nL;

    invoke-direct/range {v3 .. v11}, LX/1nH;-><init>(LX/03V;LX/1nJ;LX/1nK;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0SI;LX/0ad;LX/1nL;)V

    .line 315846
    move-object v0, v3

    .line 315847
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 315848
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1nH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315849
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 315850
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V
    .locals 8

    .prologue
    .line 315838
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/1nH;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;Ljava/lang/String;)V

    .line 315839
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315721
    if-eqz p2, :cond_1

    .line 315722
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_14

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 315723
    if-nez v0, :cond_2

    .line 315724
    :cond_1
    iget-object v0, p0, LX/1nH;->b:LX/03V;

    sget-object v3, LX/1nH;->a:Ljava/lang/String;

    const-string v4, "Feedback id or legacy api post id must be set"

    invoke-virtual {v0, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 315725
    :cond_2
    iget-object v0, p0, LX/1nH;->c:LX/1nJ;

    .line 315726
    iget-boolean v3, v0, LX/1nJ;->a:Z

    move v0, v3

    .line 315727
    if-eqz v0, :cond_3

    .line 315728
    :goto_1
    return-void

    .line 315729
    :cond_3
    iget-boolean v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v0, v0

    .line 315730
    if-nez v0, :cond_6

    .line 315731
    iget-object v0, p0, LX/1nH;->d:LX/1nK;

    new-instance v3, LX/7H9;

    invoke-direct {v3, p5, p6}, LX/7H9;-><init>(ILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V

    const/4 v7, 0x0

    .line 315732
    iput-boolean v7, v0, LX/1nK;->e:Z

    .line 315733
    iput-boolean v7, v0, LX/1nK;->f:Z

    .line 315734
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390001

    const-string v6, "NNF_FlyoutLoadDBCache"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315735
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390002

    const-string v6, "NNF_FlyoutLoadDBCacheAndRender"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315736
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390003

    const-string v6, "NNF_FlyoutLoadNetwork"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315737
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390004

    const-string v6, "NNF_FlyoutLoadNetworkAndRender"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315738
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390005

    const-string v6, "NNF_FlyoutLoadNetworkNoCache"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315739
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390006

    const-string v6, "NNF_FlyoutLoadNetworkNoCacheAndRender"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315740
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390007

    const-string v6, "NNF_FlyoutLoadCompleteFlow"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315741
    iget-object v4, v0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x390008

    invoke-interface {v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(I)V

    .line 315742
    iget-object v4, v0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p5, 0x390008

    .line 315743
    iget v5, v3, LX/7H9;->a:I

    move v5, v5

    .line 315744
    if-ltz v5, :cond_4

    .line 315745
    const-string v6, "story_index"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p5, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 315746
    :cond_4
    sget-object v5, LX/7H8;->a:[I

    iget-object v6, v3, LX/7H9;->b:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 315747
    const/4 v5, 0x0

    :goto_2
    move-object v5, v5

    .line 315748
    if-eqz v5, :cond_5

    .line 315749
    invoke-interface {v4, p5, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 315750
    :cond_5
    iput-boolean v7, v0, LX/1nK;->d:Z

    .line 315751
    iput-boolean v7, v0, LX/1nK;->c:Z

    .line 315752
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x390009

    const-string v6, "NNF_FlyoutLoadOnCreateTime"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315753
    iget-object v4, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v5, 0x39000a

    const-string v6, "NNF_FlyoutLoadFragmentCreateTime"

    invoke-interface {v4, v5, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315754
    :cond_6
    const/4 v0, 0x0

    .line 315755
    iget-boolean v3, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v3, v3

    .line 315756
    if-nez v3, :cond_15

    .line 315757
    :cond_7
    :goto_3
    move v0, v0

    .line 315758
    if-eqz v0, :cond_13

    .line 315759
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 315760
    invoke-static {v0}, LX/16z;->r(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 315761
    iget-object v0, p0, LX/1nH;->b:LX/03V;

    sget-object v3, LX/1nH;->a:Ljava/lang/String;

    const-string v4, "Reactors list init needs valid reactors count map"

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 315762
    :goto_4
    iget-boolean v3, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v3, v3

    .line 315763
    if-eqz v3, :cond_10

    if-nez v0, :cond_10

    .line 315764
    new-instance v0, LX/8qQ;

    invoke-direct {v0}, LX/8qQ;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v2

    .line 315765
    iput-object v2, v0, LX/8qQ;->a:Ljava/lang/String;

    .line 315766
    move-object v0, v0

    .line 315767
    sget-object v2, LX/89l;->LIKERS_FOR_FEEDBACK_ID:LX/89l;

    .line 315768
    iput-object v2, v0, LX/8qQ;->d:LX/89l;

    .line 315769
    move-object v0, v0

    .line 315770
    iput-boolean v1, v0, LX/8qQ;->g:Z

    .line 315771
    move-object v0, v0

    .line 315772
    iput-boolean v1, v0, LX/8qQ;->f:Z

    .line 315773
    move-object v0, v0

    .line 315774
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v1

    .line 315775
    new-instance v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    invoke-direct {v0}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;-><init>()V

    .line 315776
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 315777
    :cond_8
    :goto_5
    iget-object v1, p0, LX/1nH;->i:LX/1nL;

    invoke-virtual {v1, v0, p1, p3, p4}, LX/1nL;->a(LX/8qC;Landroid/content/Context;Lcom/facebook/ufiservices/flyout/PopoverParams;Z)V

    goto/16 :goto_1

    .line 315778
    :cond_9
    const v0, 0x7f080fe7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 315779
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 315780
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->aa()Z

    move-result v5

    .line 315781
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 315782
    if-eqz v0, :cond_d

    .line 315783
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 315784
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 315785
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v4, v0

    .line 315786
    :goto_6
    if-eqz v5, :cond_a

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 315787
    const v3, 0x7f080ff5

    new-array v5, v1, [Ljava/lang/Object;

    .line 315788
    iget-object v0, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 315789
    iget-object v6, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v6

    .line 315790
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {p1, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 315791
    :cond_a
    if-eqz v4, :cond_e

    move v0, v1

    .line 315792
    :goto_7
    iget-object v5, p0, LX/1nH;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 315793
    iget-object v2, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v6, v2

    .line 315794
    const-string v7, "native_newsfeed"

    if-eqz v0, :cond_f

    sget-object v2, LX/8s1;->OPEN_STORY_PERMALINK:LX/8s1;

    :goto_8
    invoke-interface {v5, v6, v7, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;LX/8s1;)Landroid/content/Intent;

    move-result-object v2

    .line 315795
    if-eqz v0, :cond_b

    .line 315796
    const-string v0, "view_permalink_params"

    new-instance v5, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-direct {v5, v4}, Lcom/facebook/ipc/feed/ViewPermalinkParams;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 315797
    :cond_b
    const-string v0, "fragment_title"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315798
    iget-object v0, p0, LX/1nH;->g:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/1nH;->g:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 315799
    iget-boolean v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v3

    .line 315800
    if-eqz v0, :cond_c

    .line 315801
    const-string v0, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v3, p0, LX/1nH;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 315802
    :cond_c
    const-string v0, "open_fragment_as_modal"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315803
    iget-object v0, p0, LX/1nH;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 315804
    :cond_d
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_6

    :cond_e
    move v0, v2

    .line 315805
    goto :goto_7

    .line 315806
    :cond_f
    sget-object v2, LX/8s1;->NOT_SUPPORTED:LX/8s1;

    goto :goto_8

    .line 315807
    :cond_10
    iget-object v0, p0, LX/1nH;->h:LX/0ad;

    sget-short v1, LX/0wn;->b:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 315808
    if-eqz v0, :cond_12

    .line 315809
    iget-object v0, p0, LX/1nH;->d:LX/1nK;

    .line 315810
    iget-object v1, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x390020

    const-string v3, "NNF_FlyoutBgInflatableFeedbackTotalTime"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315811
    new-instance v0, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;

    invoke-direct {v0}, Lcom/facebook/feedback/ui/BgInflatableFeedbackFragment;-><init>()V

    .line 315812
    invoke-virtual {p2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->v()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 315813
    :goto_9
    invoke-interface {v0, p7}, LX/8qD;->a(Ljava/lang/String;)V

    .line 315814
    iget-object v1, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 315815
    if-eqz v1, :cond_11

    .line 315816
    iget-object v1, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v1, v1

    .line 315817
    iget-object v2, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v2

    .line 315818
    if-nez v1, :cond_8

    .line 315819
    :cond_11
    iget-object v1, p0, LX/1nH;->b:LX/03V;

    sget-object v2, LX/1nH;->a:Ljava/lang/String;

    const-string v3, "feedback logging params or feedback source is null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 315820
    :cond_12
    new-instance v0, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;

    invoke-direct {v0}, Lcom/facebook/feedback/ui/DefaultFeedbackFragment;-><init>()V

    .line 315821
    invoke-virtual {p2}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->v()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_9

    :cond_13
    move v0, v2

    goto/16 :goto_4

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 315822
    :pswitch_0
    const-string v5, "unknown_pri"

    goto/16 :goto_2

    .line 315823
    :pswitch_1
    const-string v5, "high_pri"

    goto/16 :goto_2

    .line 315824
    :pswitch_2
    const-string v5, "low_pri"

    goto/16 :goto_2

    .line 315825
    :cond_15
    iget-object v3, p2, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v3, v3

    .line 315826
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v3

    if-lez v3, :cond_7

    const/4 v0, 0x1

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V
    .locals 7

    .prologue
    .line 315713
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, -0x1

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, LX/1nH;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V

    .line 315714
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;)V
    .locals 7

    .prologue
    .line 315719
    const/4 v4, 0x1

    const/4 v5, -0x1

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, LX/1nH;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V

    .line 315720
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 315717
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, -0x1

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, LX/1nH;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;Ljava/lang/String;)V

    .line 315718
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V
    .locals 7

    .prologue
    .line 315715
    new-instance v0, LX/8qO;

    invoke-direct {v0}, LX/8qO;-><init>()V

    invoke-virtual {v0}, LX/8qO;->a()Lcom/facebook/ufiservices/flyout/PopoverParams;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/1nH;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;Lcom/facebook/ufiservices/flyout/PopoverParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V

    .line 315716
    return-void
.end method
