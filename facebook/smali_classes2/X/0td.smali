.class public LX/0td;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0td;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Class;

.field private final c:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;",
            "LX/3G6;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 2
    .param p2    # Ljava/lang/Class;
        .annotation build Lcom/facebook/graphql/executor/MutationServiceName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 155082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155083
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/0td;->c:LX/0tf;

    .line 155084
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0td;->d:J

    .line 155085
    iput-object p1, p0, LX/0td;->a:Landroid/content/Context;

    .line 155086
    const-class v0, Lcom/facebook/graphql/executor/GraphQLMutationService;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 155087
    iput-object p2, p0, LX/0td;->b:Ljava/lang/Class;

    .line 155088
    return-void
.end method

.method private declared-synchronized a(Lcom/google/common/util/concurrent/SettableFuture;LX/3G6;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "LX/3G6;",
            ")J"
        }
    .end annotation

    .prologue
    .line 155116
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0td;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0td;->d:J

    .line 155117
    iget-object v0, p0, LX/0td;->c:LX/0tf;

    iget-wide v2, p0, LX/0td;->d:J

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3, v1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 155118
    iget-wide v0, p0, LX/0td;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 155119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/0td;
    .locals 5

    .prologue
    .line 155100
    sget-object v0, LX/0td;->e:LX/0td;

    if-nez v0, :cond_1

    .line 155101
    const-class v1, LX/0td;

    monitor-enter v1

    .line 155102
    :try_start_0
    sget-object v0, LX/0td;->e:LX/0td;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155103
    if-eqz v2, :cond_0

    .line 155104
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155105
    new-instance p0, LX/0td;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 155106
    const-class v4, Lcom/facebook/graphql/executor/GraphQLMutationService;

    move-object v4, v4

    .line 155107
    move-object v4, v4

    .line 155108
    check-cast v4, Ljava/lang/Class;

    invoke-direct {p0, v3, v4}, LX/0td;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155109
    move-object v0, p0

    .line 155110
    sput-object v0, LX/0td;->e:LX/0td;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155111
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155112
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155113
    :cond_1
    sget-object v0, LX/0td;->e:LX/0td;

    return-object v0

    .line 155114
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155115
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(J)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;",
            "LX/3G6;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 155095
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0td;->c:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 155096
    iget-object v1, p0, LX/0td;->c:LX/0tf;

    .line 155097
    invoke-virtual {v1, p1, p2}, LX/0tf;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155098
    monitor-exit p0

    return-object v0

    .line 155099
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/3G6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 155089
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 155090
    invoke-direct {p0, v0, p1}, LX/0td;->a(Lcom/google/common/util/concurrent/SettableFuture;LX/3G6;)J

    move-result-wide v2

    .line 155091
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, LX/0td;->a:Landroid/content/Context;

    iget-object v5, p0, LX/0td;->b:Ljava/lang/Class;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155092
    const-string v4, "MUTATION_ID_KEY"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 155093
    iget-object v2, p0, LX/0td;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 155094
    return-object v0
.end method
