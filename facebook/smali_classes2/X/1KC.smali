.class public final LX/1KC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1TE;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1TE;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 231149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231150
    iput-object p1, p0, LX/1KC;->a:LX/0QB;

    .line 231151
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 231152
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1KC;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 231153
    packed-switch p2, :pswitch_data_0

    .line 231154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231155
    :pswitch_0
    new-instance v0, LX/3UO;

    invoke-direct {v0}, LX/3UO;-><init>()V

    .line 231156
    move-object v0, v0

    .line 231157
    move-object v0, v0

    .line 231158
    :goto_0
    return-object v0

    .line 231159
    :pswitch_1
    invoke-static {p1}, LX/3UR;->a(LX/0QB;)LX/3UR;

    move-result-object v0

    goto :goto_0

    .line 231160
    :pswitch_2
    new-instance v0, LX/3UY;

    invoke-direct {v0}, LX/3UY;-><init>()V

    .line 231161
    move-object v0, v0

    .line 231162
    move-object v0, v0

    .line 231163
    goto :goto_0

    .line 231164
    :pswitch_3
    invoke-static {p1}, LX/3Ub;->a(LX/0QB;)LX/3Ub;

    move-result-object v0

    goto :goto_0

    .line 231165
    :pswitch_4
    invoke-static {p1}, LX/3Uw;->a(LX/0QB;)LX/3Uw;

    move-result-object v0

    goto :goto_0

    .line 231166
    :pswitch_5
    invoke-static {p1}, LX/3Wp;->a(LX/0QB;)LX/3Wp;

    move-result-object v0

    goto :goto_0

    .line 231167
    :pswitch_6
    invoke-static {p1}, LX/3XH;->a(LX/0QB;)LX/3XH;

    move-result-object v0

    goto :goto_0

    .line 231168
    :pswitch_7
    invoke-static {p1}, LX/3XI;->a(LX/0QB;)LX/3XI;

    move-result-object v0

    goto :goto_0

    .line 231169
    :pswitch_8
    invoke-static {p1}, LX/3XK;->a(LX/0QB;)LX/3XK;

    move-result-object v0

    goto :goto_0

    .line 231170
    :pswitch_9
    invoke-static {p1}, LX/3XM;->a(LX/0QB;)LX/3XM;

    move-result-object v0

    goto :goto_0

    .line 231171
    :pswitch_a
    invoke-static {p1}, LX/1TD;->a(LX/0QB;)LX/1TD;

    move-result-object v0

    goto :goto_0

    .line 231172
    :pswitch_b
    new-instance v0, LX/3XO;

    invoke-direct {v0}, LX/3XO;-><init>()V

    .line 231173
    move-object v0, v0

    .line 231174
    move-object v0, v0

    .line 231175
    goto :goto_0

    .line 231176
    :pswitch_c
    new-instance v0, LX/3XQ;

    invoke-direct {v0}, LX/3XQ;-><init>()V

    .line 231177
    move-object v0, v0

    .line 231178
    move-object v0, v0

    .line 231179
    goto :goto_0

    .line 231180
    :pswitch_d
    invoke-static {p1}, LX/1TF;->a(LX/0QB;)LX/1TF;

    move-result-object v0

    goto :goto_0

    .line 231181
    :pswitch_e
    invoke-static {p1}, LX/1TH;->a(LX/0QB;)LX/1TH;

    move-result-object v0

    goto :goto_0

    .line 231182
    :pswitch_f
    invoke-static {p1}, LX/1TI;->a(LX/0QB;)LX/1TI;

    move-result-object v0

    goto :goto_0

    .line 231183
    :pswitch_10
    invoke-static {p1}, LX/3XT;->a(LX/0QB;)LX/3XT;

    move-result-object v0

    goto :goto_0

    .line 231184
    :pswitch_11
    invoke-static {p1}, LX/1TJ;->a(LX/0QB;)LX/1TJ;

    move-result-object v0

    goto :goto_0

    .line 231185
    :pswitch_12
    new-instance v0, LX/3XX;

    invoke-direct {v0}, LX/3XX;-><init>()V

    .line 231186
    move-object v0, v0

    .line 231187
    move-object v0, v0

    .line 231188
    goto :goto_0

    .line 231189
    :pswitch_13
    invoke-static {p1}, LX/3Xa;->a(LX/0QB;)LX/3Xa;

    move-result-object v0

    goto :goto_0

    .line 231190
    :pswitch_14
    invoke-static {p1}, LX/1TK;->a(LX/0QB;)LX/1TK;

    move-result-object v0

    goto :goto_0

    .line 231191
    :pswitch_15
    invoke-static {p1}, LX/1TL;->a(LX/0QB;)LX/1TL;

    move-result-object v0

    goto :goto_0

    .line 231192
    :pswitch_16
    invoke-static {p1}, LX/3Xs;->a(LX/0QB;)LX/3Xs;

    move-result-object v0

    goto :goto_0

    .line 231193
    :pswitch_17
    invoke-static {p1}, LX/1TM;->a(LX/0QB;)LX/1TM;

    move-result-object v0

    goto :goto_0

    .line 231194
    :pswitch_18
    invoke-static {p1}, LX/3Xw;->a(LX/0QB;)LX/3Xw;

    move-result-object v0

    goto/16 :goto_0

    .line 231195
    :pswitch_19
    invoke-static {p1}, LX/3Xx;->a(LX/0QB;)LX/3Xx;

    move-result-object v0

    goto/16 :goto_0

    .line 231196
    :pswitch_1a
    invoke-static {p1}, LX/3Xz;->a(LX/0QB;)LX/3Xz;

    move-result-object v0

    goto/16 :goto_0

    .line 231197
    :pswitch_1b
    invoke-static {p1}, LX/1TN;->a(LX/0QB;)LX/1TN;

    move-result-object v0

    goto/16 :goto_0

    .line 231198
    :pswitch_1c
    new-instance v0, LX/3Y2;

    invoke-direct {v0}, LX/3Y2;-><init>()V

    .line 231199
    move-object v0, v0

    .line 231200
    move-object v0, v0

    .line 231201
    goto/16 :goto_0

    .line 231202
    :pswitch_1d
    new-instance v0, LX/3Y4;

    invoke-direct {v0}, LX/3Y4;-><init>()V

    .line 231203
    move-object v0, v0

    .line 231204
    move-object v0, v0

    .line 231205
    goto/16 :goto_0

    .line 231206
    :pswitch_1e
    new-instance v0, LX/3Y7;

    invoke-direct {v0}, LX/3Y7;-><init>()V

    .line 231207
    move-object v0, v0

    .line 231208
    move-object v0, v0

    .line 231209
    goto/16 :goto_0

    .line 231210
    :pswitch_1f
    invoke-static {p1}, LX/1TO;->a(LX/0QB;)LX/1TO;

    move-result-object v0

    goto/16 :goto_0

    .line 231211
    :pswitch_20
    invoke-static {p1}, LX/1TP;->a(LX/0QB;)LX/1TP;

    move-result-object v0

    goto/16 :goto_0

    .line 231212
    :pswitch_21
    invoke-static {p1}, LX/1TQ;->a(LX/0QB;)LX/1TQ;

    move-result-object v0

    goto/16 :goto_0

    .line 231213
    :pswitch_22
    new-instance v0, LX/3Y9;

    invoke-direct {v0}, LX/3Y9;-><init>()V

    .line 231214
    move-object v0, v0

    .line 231215
    move-object v0, v0

    .line 231216
    goto/16 :goto_0

    .line 231217
    :pswitch_23
    invoke-static {p1}, LX/1TS;->a(LX/0QB;)LX/1TS;

    move-result-object v0

    goto/16 :goto_0

    .line 231218
    :pswitch_24
    invoke-static {p1}, LX/1TT;->a(LX/0QB;)LX/1TT;

    move-result-object v0

    goto/16 :goto_0

    .line 231219
    :pswitch_25
    invoke-static {p1}, LX/3YH;->a(LX/0QB;)LX/3YH;

    move-result-object v0

    goto/16 :goto_0

    .line 231220
    :pswitch_26
    invoke-static {p1}, LX/1TV;->a(LX/0QB;)LX/1TV;

    move-result-object v0

    goto/16 :goto_0

    .line 231221
    :pswitch_27
    invoke-static {p1}, LX/3YO;->a(LX/0QB;)LX/3YO;

    move-result-object v0

    goto/16 :goto_0

    .line 231222
    :pswitch_28
    invoke-static {p1}, LX/3YQ;->a(LX/0QB;)LX/3YQ;

    move-result-object v0

    goto/16 :goto_0

    .line 231223
    :pswitch_29
    invoke-static {p1}, LX/3YU;->a(LX/0QB;)LX/3YU;

    move-result-object v0

    goto/16 :goto_0

    .line 231224
    :pswitch_2a
    invoke-static {p1}, LX/3YV;->a(LX/0QB;)LX/3YV;

    move-result-object v0

    goto/16 :goto_0

    .line 231225
    :pswitch_2b
    invoke-static {p1}, LX/3YY;->a(LX/0QB;)LX/3YY;

    move-result-object v0

    goto/16 :goto_0

    .line 231226
    :pswitch_2c
    invoke-static {p1}, LX/3Yh;->a(LX/0QB;)LX/3Yh;

    move-result-object v0

    goto/16 :goto_0

    .line 231227
    :pswitch_2d
    invoke-static {p1}, LX/1Ta;->a(LX/0QB;)LX/1Ta;

    move-result-object v0

    goto/16 :goto_0

    .line 231228
    :pswitch_2e
    invoke-static {p1}, LX/1Tb;->a(LX/0QB;)LX/1Tb;

    move-result-object v0

    goto/16 :goto_0

    .line 231229
    :pswitch_2f
    invoke-static {p1}, LX/1Tc;->a(LX/0QB;)LX/1Tc;

    move-result-object v0

    goto/16 :goto_0

    .line 231230
    :pswitch_30
    invoke-static {p1}, LX/1Td;->a(LX/0QB;)LX/1Td;

    move-result-object v0

    goto/16 :goto_0

    .line 231231
    :pswitch_31
    invoke-static {p1}, LX/1Te;->a(LX/0QB;)LX/1Te;

    move-result-object v0

    goto/16 :goto_0

    .line 231232
    :pswitch_32
    invoke-static {p1}, LX/1Tf;->a(LX/0QB;)LX/1Tf;

    move-result-object v0

    goto/16 :goto_0

    .line 231233
    :pswitch_33
    invoke-static {p1}, LX/1Tg;->a(LX/0QB;)LX/1Tg;

    move-result-object v0

    goto/16 :goto_0

    .line 231234
    :pswitch_34
    invoke-static {p1}, LX/1Th;->a(LX/0QB;)LX/1Th;

    move-result-object v0

    goto/16 :goto_0

    .line 231235
    :pswitch_35
    invoke-static {p1}, LX/3Yo;->a(LX/0QB;)LX/3Yo;

    move-result-object v0

    goto/16 :goto_0

    .line 231236
    :pswitch_36
    invoke-static {p1}, LX/1Ti;->a(LX/0QB;)LX/1Ti;

    move-result-object v0

    goto/16 :goto_0

    .line 231237
    :pswitch_37
    invoke-static {p1}, LX/1Tj;->a(LX/0QB;)LX/1Tj;

    move-result-object v0

    goto/16 :goto_0

    .line 231238
    :pswitch_38
    invoke-static {p1}, LX/1Tk;->a(LX/0QB;)LX/1Tk;

    move-result-object v0

    goto/16 :goto_0

    .line 231239
    :pswitch_39
    invoke-static {p1}, LX/1Tl;->a(LX/0QB;)LX/1Tl;

    move-result-object v0

    goto/16 :goto_0

    .line 231240
    :pswitch_3a
    invoke-static {p1}, LX/1Tm;->a(LX/0QB;)LX/1Tm;

    move-result-object v0

    goto/16 :goto_0

    .line 231241
    :pswitch_3b
    invoke-static {p1}, LX/1Tn;->a(LX/0QB;)LX/1Tn;

    move-result-object v0

    goto/16 :goto_0

    .line 231242
    :pswitch_3c
    invoke-static {p1}, LX/1U1;->a(LX/0QB;)LX/1U1;

    move-result-object v0

    goto/16 :goto_0

    .line 231243
    :pswitch_3d
    invoke-static {p1}, LX/3Z3;->a(LX/0QB;)LX/3Z3;

    move-result-object v0

    goto/16 :goto_0

    .line 231244
    :pswitch_3e
    invoke-static {p1}, LX/3Z5;->a(LX/0QB;)LX/3Z5;

    move-result-object v0

    goto/16 :goto_0

    .line 231245
    :pswitch_3f
    invoke-static {p1}, LX/1U2;->a(LX/0QB;)LX/1U2;

    move-result-object v0

    goto/16 :goto_0

    .line 231246
    :pswitch_40
    invoke-static {p1}, LX/3Z9;->a(LX/0QB;)LX/3Z9;

    move-result-object v0

    goto/16 :goto_0

    .line 231247
    :pswitch_41
    invoke-static {p1}, LX/3ZA;->a(LX/0QB;)LX/3ZA;

    move-result-object v0

    goto/16 :goto_0

    .line 231248
    :pswitch_42
    invoke-static {p1}, LX/1U3;->a(LX/0QB;)LX/1U3;

    move-result-object v0

    goto/16 :goto_0

    .line 231249
    :pswitch_43
    invoke-static {p1}, LX/1U4;->a(LX/0QB;)LX/1U4;

    move-result-object v0

    goto/16 :goto_0

    .line 231250
    :pswitch_44
    new-instance v0, LX/3ZQ;

    invoke-direct {v0}, LX/3ZQ;-><init>()V

    .line 231251
    move-object v0, v0

    .line 231252
    move-object v0, v0

    .line 231253
    goto/16 :goto_0

    .line 231254
    :pswitch_45
    new-instance v0, LX/3ZR;

    invoke-direct {v0}, LX/3ZR;-><init>()V

    .line 231255
    move-object v0, v0

    .line 231256
    move-object v0, v0

    .line 231257
    goto/16 :goto_0

    .line 231258
    :pswitch_46
    new-instance v0, LX/3ZS;

    invoke-direct {v0}, LX/3ZS;-><init>()V

    .line 231259
    move-object v0, v0

    .line 231260
    move-object v0, v0

    .line 231261
    goto/16 :goto_0

    .line 231262
    :pswitch_47
    invoke-static {p1}, LX/3ZU;->a(LX/0QB;)LX/3ZU;

    move-result-object v0

    goto/16 :goto_0

    .line 231263
    :pswitch_48
    new-instance v0, LX/3a0;

    invoke-direct {v0}, LX/3a0;-><init>()V

    .line 231264
    move-object v0, v0

    .line 231265
    move-object v0, v0

    .line 231266
    goto/16 :goto_0

    .line 231267
    :pswitch_49
    new-instance v0, LX/3a2;

    invoke-direct {v0}, LX/3a2;-><init>()V

    .line 231268
    move-object v0, v0

    .line 231269
    move-object v0, v0

    .line 231270
    goto/16 :goto_0

    .line 231271
    :pswitch_4a
    invoke-static {p1}, LX/1U5;->b(LX/0QB;)LX/1U5;

    move-result-object v0

    goto/16 :goto_0

    .line 231272
    :pswitch_4b
    invoke-static {p1}, LX/3a5;->a(LX/0QB;)LX/3a5;

    move-result-object v0

    goto/16 :goto_0

    .line 231273
    :pswitch_4c
    invoke-static {p1}, LX/3aa;->a(LX/0QB;)LX/3aa;

    move-result-object v0

    goto/16 :goto_0

    .line 231274
    :pswitch_4d
    invoke-static {p1}, LX/3bS;->a(LX/0QB;)LX/3bS;

    move-result-object v0

    goto/16 :goto_0

    .line 231275
    :pswitch_4e
    invoke-static {p1}, LX/3ba;->a(LX/0QB;)LX/3ba;

    move-result-object v0

    goto/16 :goto_0

    .line 231276
    :pswitch_4f
    invoke-static {p1}, LX/3bn;->a(LX/0QB;)LX/3bn;

    move-result-object v0

    goto/16 :goto_0

    .line 231277
    :pswitch_50
    invoke-static {p1}, LX/3bs;->a(LX/0QB;)LX/3bs;

    move-result-object v0

    goto/16 :goto_0

    .line 231278
    :pswitch_51
    invoke-static {p1}, LX/3bw;->a(LX/0QB;)LX/3bw;

    move-result-object v0

    goto/16 :goto_0

    .line 231279
    :pswitch_52
    invoke-static {p1}, LX/3c4;->a(LX/0QB;)LX/3c4;

    move-result-object v0

    goto/16 :goto_0

    .line 231280
    :pswitch_53
    invoke-static {p1}, LX/3c6;->a(LX/0QB;)LX/3c6;

    move-result-object v0

    goto/16 :goto_0

    .line 231281
    :pswitch_54
    new-instance v0, LX/3cA;

    invoke-direct {v0}, LX/3cA;-><init>()V

    .line 231282
    move-object v0, v0

    .line 231283
    move-object v0, v0

    .line 231284
    goto/16 :goto_0

    .line 231285
    :pswitch_55
    invoke-static {p1}, LX/3cF;->a(LX/0QB;)LX/3cF;

    move-result-object v0

    goto/16 :goto_0

    .line 231286
    :pswitch_56
    invoke-static {p1}, LX/3cH;->a(LX/0QB;)LX/3cH;

    move-result-object v0

    goto/16 :goto_0

    .line 231287
    :pswitch_57
    new-instance v0, LX/3cP;

    invoke-direct {v0}, LX/3cP;-><init>()V

    .line 231288
    move-object v0, v0

    .line 231289
    move-object v0, v0

    .line 231290
    goto/16 :goto_0

    .line 231291
    :pswitch_58
    invoke-static {p1}, LX/1U9;->a(LX/0QB;)LX/1U9;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 231292
    const/16 v0, 0x59

    return v0
.end method
