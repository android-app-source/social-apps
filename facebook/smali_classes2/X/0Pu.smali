.class public LX/0Pu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0Pt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()LX/78l;
    .locals 3

    .prologue
    .line 57389
    const-class v1, LX/0Pu;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0Pu;->a:LX/0Pt;

    if-nez v0, :cond_0

    .line 57390
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Config Provider should be set in the app\'s onCreate"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57391
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 57392
    :cond_0
    :try_start_1
    sget-object v0, LX/0Pu;->a:LX/0Pt;

    .line 57393
    iget-object v2, v0, LX/0Pt;->a:Landroid/content/Context;

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    invoke-static {v2}, LX/Jbt;->b(LX/0QB;)LX/Jbt;

    move-result-object v2

    check-cast v2, LX/78l;

    move-object v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57394
    monitor-exit v1

    return-object v0
.end method

.method public static declared-synchronized a(LX/0Pt;)V
    .locals 2

    .prologue
    .line 57395
    const-class v0, LX/0Pu;

    monitor-enter v0

    :try_start_0
    sput-object p0, LX/0Pu;->a:LX/0Pt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57396
    monitor-exit v0

    return-void

    .line 57397
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
