.class public LX/1NY;
.super LX/1NZ;
.source ""


# instance fields
.field public final a:LX/0aG;

.field private final b:LX/0bH;

.field private final c:LX/189;

.field private final d:LX/1Ck;

.field private final e:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(LX/0aG;LX/0bH;LX/189;LX/1Ck;Lcom/facebook/feed/fragment/NewsFeedFragment;)V
    .locals 0
    .param p5    # Lcom/facebook/feed/fragment/NewsFeedFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237513
    invoke-direct {p0}, LX/1NZ;-><init>()V

    .line 237514
    iput-object p1, p0, LX/1NY;->a:LX/0aG;

    .line 237515
    iput-object p2, p0, LX/1NY;->b:LX/0bH;

    .line 237516
    iput-object p3, p0, LX/1NY;->c:LX/189;

    .line 237517
    iput-object p4, p0, LX/1NY;->d:LX/1Ck;

    .line 237518
    iput-object p5, p0, LX/1NY;->e:Lcom/facebook/feed/fragment/NewsFeedFragment;

    .line 237519
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 13

    .prologue
    .line 237520
    check-cast p1, LX/1Nj;

    .line 237521
    iget-object v0, p0, LX/1NY;->e:Lcom/facebook/feed/fragment/NewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment;->t()LX/0fz;

    move-result-object v0

    iget-object v1, p1, LX/1Nj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 237522
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 237523
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 237524
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    if-eqz v2, :cond_0

    .line 237525
    iget-object v2, p0, LX/1NY;->c:LX/189;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    iget-object v3, p1, LX/1Nj;->b:Ljava/lang/String;

    .line 237526
    new-instance v10, LX/4YD;

    invoke-direct {v10}, LX/4YD;-><init>()V

    .line 237527
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 237528
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->b:Ljava/lang/String;

    .line 237529
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->E_()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->c:Ljava/lang/String;

    .line 237530
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->F_()J

    move-result-wide v11

    iput-wide v11, v10, LX/4YD;->d:J

    .line 237531
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->e:LX/0Px;

    .line 237532
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->r()LX/0Px;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->f:LX/0Px;

    .line 237533
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->n()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->g:Ljava/lang/String;

    .line 237534
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 237535
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->c()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v10, LX/4YD;->i:Ljava/lang/String;

    .line 237536
    invoke-static {v10, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 237537
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->L_()LX/0x2;

    move-result-object v9

    invoke-virtual {v9}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0x2;

    iput-object v9, v10, LX/4YD;->j:LX/0x2;

    .line 237538
    move-object v5, v10

    .line 237539
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v6

    .line 237540
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 237541
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_1
    if-ge v8, v10, :cond_3

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 237542
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v11

    if-eqz v11, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 237543
    :cond_1
    invoke-virtual {v9, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 237544
    :cond_2
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 237545
    :cond_3
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 237546
    iput-object v6, v5, LX/4YD;->e:LX/0Px;

    .line 237547
    move-object v5, v5

    .line 237548
    iget-object v6, v2, LX/189;->i:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    .line 237549
    iput-wide v7, v5, LX/4YD;->d:J

    .line 237550
    move-object v5, v5

    .line 237551
    new-instance v6, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;-><init>(LX/4YD;)V

    .line 237552
    move-object v5, v6

    .line 237553
    move-object v0, v5

    .line 237554
    iget-object v2, p0, LX/1NY;->b:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 237555
    new-instance v2, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;

    iget-object v3, p1, LX/1Nj;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)V

    .line 237556
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 237557
    const-string v3, "xOutPlaceReviewItemParamKey"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 237558
    iget-object v2, p0, LX/1NY;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task_key_x_out_place_review_item"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, LX/1Nj;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, LX/1Nj;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 237559
    new-instance v4, LX/DBp;

    invoke-direct {v4, p0, v0}, LX/DBp;-><init>(LX/1NY;Landroid/os/Bundle;)V

    move-object v0, v4

    .line 237560
    new-instance v4, LX/DBo;

    invoke-direct {v4, p0}, LX/DBo;-><init>(LX/1NY;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_0

    .line 237561
    :cond_4
    return-void
.end method
