.class public final LX/1O6;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public final synthetic a:LX/1O5;

.field public final synthetic b:LX/1O1;


# direct methods
.method public constructor <init>(LX/1O1;LX/1O5;)V
    .locals 0

    .prologue
    .line 238697
    iput-object p1, p0, LX/1O6;->b:LX/1O1;

    iput-object p2, p0, LX/1O6;->a:LX/1O5;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 11

    .prologue
    const v6, 0x3f4ccccd    # 0.8f

    .line 238698
    iget-object v0, p0, LX/1O6;->b:LX/1O1;

    iget-boolean v0, v0, LX/1O1;->a:Z

    if-eqz v0, :cond_0

    .line 238699
    iget-object v1, p0, LX/1O6;->a:LX/1O5;

    .line 238700
    iget v7, v1, LX/1O5;->n:F

    move v7, v7

    .line 238701
    const v8, 0x3f4ccccd    # 0.8f

    div-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    add-double/2addr v7, v9

    double-to-float v7, v7

    .line 238702
    iget v8, v1, LX/1O5;->l:F

    move v8, v8

    .line 238703
    iget v9, v1, LX/1O5;->m:F

    move v9, v9

    .line 238704
    iget v10, v1, LX/1O5;->l:F

    move v10, v10

    .line 238705
    sub-float/2addr v9, v10

    mul-float/2addr v9, p1

    add-float/2addr v8, v9

    .line 238706
    invoke-virtual {v1, v8}, LX/1O5;->b(F)V

    .line 238707
    iget v8, v1, LX/1O5;->n:F

    move v8, v8

    .line 238708
    iget v9, v1, LX/1O5;->n:F

    move v9, v9

    .line 238709
    sub-float/2addr v7, v9

    mul-float/2addr v7, p1

    add-float/2addr v7, v8

    .line 238710
    invoke-virtual {v1, v7}, LX/1O5;->d(F)V

    .line 238711
    :goto_0
    return-void

    .line 238712
    :cond_0
    iget-object v0, p0, LX/1O6;->a:LX/1O5;

    .line 238713
    iget v1, v0, LX/1O5;->h:F

    move v0, v1

    .line 238714
    float-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v4, p0, LX/1O6;->a:LX/1O5;

    .line 238715
    iget-wide v7, v4, LX/1O5;->r:D

    move-wide v4, v7

    .line 238716
    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 238717
    iget-object v1, p0, LX/1O6;->a:LX/1O5;

    .line 238718
    iget v2, v1, LX/1O5;->m:F

    move v1, v2

    .line 238719
    iget-object v2, p0, LX/1O6;->a:LX/1O5;

    .line 238720
    iget v3, v2, LX/1O5;->l:F

    move v2, v3

    .line 238721
    iget-object v3, p0, LX/1O6;->a:LX/1O5;

    .line 238722
    iget v4, v3, LX/1O5;->n:F

    move v3, v4

    .line 238723
    sub-float v0, v6, v0

    .line 238724
    sget-object v4, LX/1O1;->d:Landroid/view/animation/Interpolator;

    invoke-interface {v4, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    .line 238725
    iget-object v1, p0, LX/1O6;->a:LX/1O5;

    invoke-virtual {v1, v0}, LX/1O5;->c(F)V

    .line 238726
    sget-object v0, LX/1O1;->c:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v6

    add-float/2addr v0, v2

    .line 238727
    iget-object v1, p0, LX/1O6;->a:LX/1O5;

    invoke-virtual {v1, v0}, LX/1O5;->b(F)V

    .line 238728
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p1

    add-float/2addr v0, v3

    .line 238729
    iget-object v1, p0, LX/1O6;->a:LX/1O5;

    invoke-virtual {v1, v0}, LX/1O5;->d(F)V

    .line 238730
    const/high16 v0, 0x43100000    # 144.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x44340000    # 720.0f

    iget-object v2, p0, LX/1O6;->b:LX/1O1;

    iget v2, v2, LX/1O1;->m:F

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 238731
    iget-object v1, p0, LX/1O6;->b:LX/1O1;

    invoke-virtual {v1, v0}, LX/1O1;->c(F)V

    goto :goto_0
.end method
