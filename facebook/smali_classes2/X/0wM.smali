.class public LX/0wM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0wM;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "LX/10E;",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 159821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159822
    iput-object p1, p0, LX/0wM;->a:Landroid/content/res/Resources;

    .line 159823
    new-instance v0, LX/0aq;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0wM;->b:LX/0aq;

    .line 159824
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/0wM;->c:LX/0YU;

    .line 159825
    return-void
.end method

.method public static a(LX/0QB;)LX/0wM;
    .locals 4

    .prologue
    .line 159808
    sget-object v0, LX/0wM;->d:LX/0wM;

    if-nez v0, :cond_1

    .line 159809
    const-class v1, LX/0wM;

    monitor-enter v1

    .line 159810
    :try_start_0
    sget-object v0, LX/0wM;->d:LX/0wM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 159811
    if-eqz v2, :cond_0

    .line 159812
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 159813
    new-instance p0, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 159814
    move-object v0, p0

    .line 159815
    sput-object v0, LX/0wM;->d:LX/0wM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159816
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 159817
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 159818
    :cond_1
    sget-object v0, LX/0wM;->d:LX/0wM;

    return-object v0

    .line 159819
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 159820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)Landroid/graphics/ColorFilter;
    .locals 2

    .prologue
    .line 159826
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0wM;->c:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    .line 159827
    if-nez v0, :cond_0

    .line 159828
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p1, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 159829
    iget-object v1, p0, LX/0wM;->c:LX/0YU;

    invoke-virtual {v1, p1, v0}, LX/0YU;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159830
    :cond_0
    monitor-exit p0

    return-object v0

    .line 159831
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 159807
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(IIZ)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 159785
    monitor-enter p0

    if-nez p1, :cond_1

    .line 159786
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 159787
    :cond_1
    if-eqz p3, :cond_5

    .line 159788
    :try_start_0
    sget-object v0, LX/10E;->a:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/10E;

    .line 159789
    if-nez v0, :cond_2

    .line 159790
    new-instance v0, LX/10E;

    invoke-direct {v0}, LX/10E;-><init>()V

    .line 159791
    :cond_2
    iput p1, v0, LX/10E;->b:I

    .line 159792
    iput p2, v0, LX/10E;->c:I

    .line 159793
    move-object v1, v0

    .line 159794
    iget-object v0, p0, LX/0wM;->b:LX/0aq;

    invoke-virtual {v0, v1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable$ConstantState;

    .line 159795
    :goto_1
    if-nez v0, :cond_4

    .line 159796
    iget-object v0, p0, LX/0wM;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159797
    invoke-virtual {p0, p2}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 159798
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    .line 159799
    if-eqz p3, :cond_3

    if-eqz v2, :cond_3

    .line 159800
    iget-object v3, p0, LX/0wM;->b:LX/0aq;

    invoke-virtual {v3, v1, v2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159801
    const/4 p3, 0x0

    .line 159802
    :cond_3
    :goto_2
    if-eqz p3, :cond_0

    .line 159803
    sget-object v2, LX/10E;->a:LX/0Zj;

    invoke-virtual {v2, v1}, LX/0Zj;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159804
    goto :goto_0

    .line 159805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 159806
    :cond_4
    :try_start_1
    iget-object v2, p0, LX/0wM;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public final declared-synchronized a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 159777
    monitor-enter p0

    if-nez p1, :cond_0

    .line 159778
    const/4 v0, 0x0

    .line 159779
    :goto_0
    monitor-exit p0

    return-object v0

    .line 159780
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    .line 159781
    if-eqz v0, :cond_1

    .line 159782
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159783
    :goto_1
    invoke-virtual {p0, p2}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 159784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, p1

    goto :goto_1
.end method
