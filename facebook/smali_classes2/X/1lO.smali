.class public final LX/1lO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field public b:LX/0ta;

.field public c:J

.field public d:LX/1NB;

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:J

.field public h:J

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/Object;

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 311615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311616
    iput-wide v2, p0, LX/1lO;->g:J

    .line 311617
    iput-wide v2, p0, LX/1lO;->h:J

    .line 311618
    iput-object v0, p0, LX/1lO;->i:Ljava/lang/String;

    .line 311619
    iput-object v0, p0, LX/1lO;->j:Ljava/lang/String;

    .line 311620
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1lO;->n:Z

    .line 311621
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1lO;->o:Z

    return-void
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TV;>;)",
            "LX/1lO",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 311594
    new-instance v0, LX/1lO;

    invoke-direct {v0}, LX/1lO;-><init>()V

    .line 311595
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    iput-object v1, v0, LX/1lO;->k:Ljava/lang/Object;

    .line 311596
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->e:Ljava/lang/Class;

    iput-object v1, v0, LX/1lO;->a:Ljava/lang/Class;

    .line 311597
    iget-object v1, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 311598
    iput-object v1, v0, LX/1lO;->b:LX/0ta;

    .line 311599
    iget-wide v4, p0, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v4

    .line 311600
    iput-wide v2, v0, LX/1lO;->c:J

    .line 311601
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->g:LX/0Rf;

    iput-object v1, v0, LX/1lO;->l:Ljava/util/Set;

    .line 311602
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->h:Ljava/util/Set;

    iput-object v1, v0, LX/1lO;->m:Ljava/util/Set;

    .line 311603
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->a:LX/1NB;

    iput-object v1, v0, LX/1lO;->d:LX/1NB;

    .line 311604
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->k:Ljava/util/Map;

    iput-object v1, v0, LX/1lO;->e:Ljava/util/Map;

    .line 311605
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->l:Ljava/util/Map;

    iput-object v1, v0, LX/1lO;->f:Ljava/util/Map;

    .line 311606
    iget-wide v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->b:J

    iput-wide v2, v0, LX/1lO;->g:J

    .line 311607
    iget-wide v2, p0, Lcom/facebook/graphql/executor/GraphQLResult;->c:J

    iput-wide v2, v0, LX/1lO;->h:J

    .line 311608
    iget-boolean v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->f:Z

    move v1, v1

    .line 311609
    iput-boolean v1, v0, LX/1lO;->n:Z

    .line 311610
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v1, v1

    .line 311611
    iput-object v1, v0, LX/1lO;->i:Ljava/lang/String;

    .line 311612
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->j:Ljava/lang/String;

    move-object v1, v1

    .line 311613
    iput-object v1, v0, LX/1lO;->j:Ljava/lang/String;

    .line 311614
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)LX/1lO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1lO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 311585
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 311586
    iget-object v0, p0, LX/1lO;->l:Ljava/util/Set;

    instance-of v0, v0, Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 311587
    iget-object v0, p0, LX/1lO;->l:Ljava/util/Set;

    if-nez v0, :cond_2

    .line 311588
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/1lO;->l:Ljava/util/Set;

    .line 311589
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1lO;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 311590
    :cond_1
    return-object p0

    .line 311591
    :cond_2
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LX/1lO;->l:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 311592
    iget-object v1, p0, LX/1lO;->l:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 311593
    iput-object v0, p0, LX/1lO;->l:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 311576
    new-instance v2, Lcom/facebook/graphql/executor/GraphQLResult;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1lO;->k:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1lO;->b:LX/0ta;

    move-object/from16 v0, p0

    iget-wide v5, v0, LX/1lO;->c:J

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1lO;->l:Ljava/util/Set;

    if-nez v7, :cond_0

    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v7

    :goto_0
    move-object/from16 v0, p0

    iget-object v8, v0, LX/1lO;->m:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1lO;->d:LX/1NB;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1lO;->e:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/1lO;->f:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/1lO;->h:J

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/1lO;->g:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/1lO;->n:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/1lO;->o:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1lO;->i:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1lO;->j:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-direct/range {v2 .. v20}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLX/0Rf;Ljava/util/Set;LX/1NB;Ljava/util/Map;Ljava/util/Map;JJZZLjava/lang/String;Ljava/lang/String;B)V

    .line 311577
    return-object v2

    .line 311578
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, LX/1lO;->l:Ljava/util/Set;

    invoke-static {v7}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v7

    goto :goto_0
.end method

.method public final b(J)LX/1lO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/1lO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 311583
    iput-wide p1, p0, LX/1lO;->g:J

    .line 311584
    return-object p0
.end method

.method public final b(Ljava/util/Collection;)LX/1lO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1lO;"
        }
    .end annotation

    .prologue
    .line 311579
    if-nez p1, :cond_0

    .line 311580
    const/4 v0, 0x0

    iput-object v0, p0, LX/1lO;->m:Ljava/util/Set;

    .line 311581
    :goto_0
    return-object p0

    .line 311582
    :cond_0
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1lO;->m:Ljava/util/Set;

    goto :goto_0
.end method
