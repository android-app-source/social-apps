.class public LX/189;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20j;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Os;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2fl;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Ou;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Ot;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Ov;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yq;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3BI;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 205677
    const-class v0, LX/189;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/189;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 205678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205679
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205680
    iput-object v0, p0, LX/189;->b:LX/0Ot;

    .line 205681
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205682
    iput-object v0, p0, LX/189;->c:LX/0Ot;

    .line 205683
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205684
    iput-object v0, p0, LX/189;->d:LX/0Ot;

    .line 205685
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205686
    iput-object v0, p0, LX/189;->e:LX/0Ot;

    .line 205687
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205688
    iput-object v0, p0, LX/189;->f:LX/0Ot;

    .line 205689
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205690
    iput-object v0, p0, LX/189;->g:LX/0Ot;

    .line 205691
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205692
    iput-object v0, p0, LX/189;->j:LX/0Ot;

    .line 205693
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 205694
    iput-object v0, p0, LX/189;->k:LX/0Ot;

    .line 205695
    return-void
.end method

.method public static a(II)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 205696
    if-ltz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    add-int/lit8 v0, p0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0Px;LX/0jW;LX/0jW;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Z::",
            "LX/0jW;",
            ">(",
            "LX/0Px",
            "<TZ;>;TZ;TZ;)",
            "LX/0Px",
            "<TZ;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 205697
    if-nez p0, :cond_1

    .line 205698
    :cond_0
    :goto_0
    return-object p0

    .line 205699
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jW;

    .line 205700
    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 205701
    const/4 v0, 0x1

    .line 205702
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 205703
    :cond_2
    if-eqz v1, :cond_0

    .line 205704
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 205705
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_4

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jW;

    .line 205706
    if-eq v0, p2, :cond_3

    .line 205707
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205708
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 205709
    :cond_3
    invoke-virtual {v3, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 205710
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(LX/0Px;Ljava/lang/String;)LX/0Px;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205711
    if-nez p0, :cond_1

    .line 205712
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    .line 205713
    :cond_0
    :goto_0
    return-object p0

    .line 205714
    :cond_1
    invoke-virtual {p0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205715
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/189;
    .locals 1

    .prologue
    .line 205654
    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/189;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 205716
    if-nez p3, :cond_1

    move-object p3, v0

    .line 205717
    :cond_0
    :goto_0
    return-object p3

    .line 205718
    :cond_1
    if-eq p1, p2, :cond_0

    .line 205719
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-ne v1, p2, :cond_3

    .line 205720
    invoke-static {p3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205721
    iput-object p1, v0, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 205722
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 205723
    iget-object v0, p0, LX/189;->h:LX/03V;

    const-string v1, "CORRUPT_CACHED_FEED_STORY"

    const-string v2, "invalid parent pointer"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 205724
    :cond_3
    invoke-static {p3}, LX/16y;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 205725
    invoke-static {p3}, LX/16y;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1, p2}, LX/189;->a(LX/0Px;LX/0jW;LX/0jW;)LX/0Px;

    move-result-object v1

    .line 205726
    invoke-static {p3}, LX/16y;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 205727
    invoke-static {p3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205728
    invoke-static {p3}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-static {v2}, LX/4Z1;->a(Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)LX/4Z1;

    move-result-object v2

    .line 205729
    iput-object v1, v2, LX/4Z1;->c:LX/0Px;

    .line 205730
    move-object v1, v2

    .line 205731
    invoke-virtual {v1}, LX/4Z1;->a()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    .line 205732
    iput-object v1, v0, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 205733
    goto :goto_1

    .line 205734
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v2

    .line 205735
    iput-wide v2, v0, LX/23u;->G:J

    .line 205736
    move-object v0, v0

    .line 205737
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p3

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205738
    if-nez p0, :cond_0

    .line 205739
    const/4 v0, 0x0

    .line 205740
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205741
    iput-object p1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205742
    move-object v0, v0

    .line 205743
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 205744
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 205745
    iput-wide v2, v0, LX/23u;->G:J

    .line 205746
    move-object v0, v0

    .line 205747
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 10

    .prologue
    .line 205748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 205749
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    .line 205750
    new-instance v6, LX/4W3;

    invoke-direct {v6}, LX/4W3;-><init>()V

    .line 205751
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 205752
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->a()D

    move-result-wide v8

    iput-wide v8, v6, LX/4W3;->b:D

    .line 205753
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->j()I

    move-result v7

    iput v7, v6, LX/4W3;->c:I

    .line 205754
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->k()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4W3;->d:Ljava/lang/String;

    .line 205755
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4W3;->e:Ljava/lang/String;

    .line 205756
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->m()I

    move-result v7

    iput v7, v6, LX/4W3;->f:I

    .line 205757
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->n()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4W3;->g:Ljava/lang/String;

    .line 205758
    invoke-static {v6, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 205759
    move-object v1, v6

    .line 205760
    iget-object v2, p1, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 205761
    iput-object v2, v1, LX/4W3;->g:Ljava/lang/String;

    .line 205762
    move-object v1, v1

    .line 205763
    invoke-virtual {v1}, LX/4W3;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    .line 205764
    iget-object v2, p1, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-static {v2}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 205765
    iget-object v3, p1, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-static {v3}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 205766
    invoke-static {v0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    .line 205767
    iput-object v2, v0, LX/4XR;->fm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205768
    move-object v0, v0

    .line 205769
    iput-object v3, v0, LX/4XR;->jM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205770
    move-object v0, v0

    .line 205771
    iput-object v1, v0, LX/4XR;->hr:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 205772
    move-object v0, v0

    .line 205773
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 205774
    invoke-static {p0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    .line 205775
    iput-object v2, v1, LX/39x;->t:Ljava/lang/String;

    .line 205776
    move-object v1, v1

    .line 205777
    iput-object v0, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205778
    move-object v0, v1

    .line 205779
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/189;
    .locals 12

    .prologue
    .line 205780
    new-instance v0, LX/189;

    invoke-direct {v0}, LX/189;-><init>()V

    .line 205781
    const/16 v1, 0x475

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1a3a

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x46f

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1a3c

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1a3b

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1a3d

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    const/16 v9, 0x922

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xcb3

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    .line 205782
    iput-object v1, v0, LX/189;->b:LX/0Ot;

    iput-object v2, v0, LX/189;->c:LX/0Ot;

    iput-object v3, v0, LX/189;->d:LX/0Ot;

    iput-object v4, v0, LX/189;->e:LX/0Ot;

    iput-object v5, v0, LX/189;->f:LX/0Ot;

    iput-object v6, v0, LX/189;->g:LX/0Ot;

    iput-object v7, v0, LX/189;->h:LX/03V;

    iput-object v8, v0, LX/189;->i:LX/0SG;

    iput-object v9, v0, LX/189;->j:LX/0Ot;

    iput-object v10, v0, LX/189;->k:LX/0Ot;

    iput-object v11, v0, LX/189;->l:LX/0Uh;

    .line 205783
    return-object v0
.end method

.method public static b(LX/189;Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;
    .locals 5

    .prologue
    .line 205966
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205967
    iput-wide v2, v0, LX/23u;->G:J

    .line 205968
    move-object v0, v0

    .line 205969
    invoke-static {p1}, LX/16z;->j(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    .line 205970
    new-instance v2, LX/4W5;

    invoke-direct {v2}, LX/4W5;-><init>()V

    .line 205971
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 205972
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->a()I

    move-result v3

    iput v3, v2, LX/4W5;->b:I

    .line 205973
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->j()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/4W5;->c:LX/0Px;

    .line 205974
    invoke-static {v2, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 205975
    move-object v1, v2

    .line 205976
    invoke-static {p1}, LX/16z;->j(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 205977
    iput v2, v1, LX/4W5;->b:I

    .line 205978
    move-object v1, v1

    .line 205979
    invoke-virtual {v1}, LX/4W5;->a()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    .line 205980
    iput-object v1, v0, LX/23u;->z:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 205981
    move-object v0, v0

    .line 205982
    return-object v0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205784
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205785
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205786
    invoke-static {v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205787
    iget-object v1, p0, LX/189;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Os;

    invoke-static {p2}, LX/6Os;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 205788
    iget-object v1, p0, LX/189;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2fl;

    invoke-static {v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    invoke-static {v3, v2}, LX/2fl;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/0Px;

    move-result-object v1

    .line 205789
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-static {v1}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 205790
    iput-object v1, v0, LX/23u;->b:LX/0Px;

    .line 205791
    move-object v0, v0

    .line 205792
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205793
    iput-wide v2, v0, LX/23u;->G:J

    .line 205794
    move-object v0, v0

    .line 205795
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205796
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 7

    .prologue
    .line 205901
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 205902
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205903
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 205904
    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 205905
    :goto_0
    move-object v0, v0

    .line 205906
    iput-object v0, v2, LX/23u;->aq:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 205907
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 205908
    iget-object v0, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ot;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;

    move-result-object v1

    .line 205909
    iget-object v0, p0, LX/189;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 205910
    iget-object v0, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ot;

    .line 205911
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 205912
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 205913
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 205914
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v5

    .line 205915
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 205916
    invoke-static {v0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 205917
    iput-object p2, v1, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 205918
    move-object v1, v1

    .line 205919
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    move-object v0, v1

    .line 205920
    iput-object v0, v5, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205921
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 205922
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 205923
    :goto_2
    iput-object v0, v2, LX/23u;->k:LX/0Px;

    .line 205924
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/189;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 205925
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-static {v0}, LX/4Y6;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/4Y6;

    move-result-object v0

    .line 205926
    iput-object p2, v0, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 205927
    move-object v0, v0

    .line 205928
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 205929
    iput-object v0, v2, LX/23u;->A:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 205930
    :cond_4
    iget-object v0, p0, LX/189;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2fl;

    invoke-static {p1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v1

    invoke-static {v1, p2}, LX/2fl;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/0Px;

    move-result-object v0

    .line 205931
    iput-object v0, v2, LX/23u;->b:LX/0Px;

    .line 205932
    invoke-static {p1}, LX/16y;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 205933
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 205934
    invoke-static {p1}, LX/16y;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_5

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205935
    invoke-direct {p0, v0, p2}, LX/189;->b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205936
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 205937
    :cond_5
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, LX/4Z1;

    invoke-direct {v0}, LX/4Z1;-><init>()V

    .line 205938
    :goto_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 205939
    iput-object v1, v0, LX/4Z1;->c:LX/0Px;

    .line 205940
    move-object v0, v0

    .line 205941
    invoke-virtual {v0}, LX/4Z1;->a()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    .line 205942
    iput-object v0, v2, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 205943
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 205944
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/189;->b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205945
    iput-object v0, v2, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 205946
    :cond_7
    iget-object v0, p0, LX/189;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 205947
    iput-wide v0, v2, LX/23u;->G:J

    .line 205948
    move-object v0, v2

    .line 205949
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    .line 205950
    :cond_8
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-static {v0}, LX/4Z1;->a(Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)LX/4Z1;

    move-result-object v0

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto/16 :goto_2

    .line 205951
    :cond_a
    new-instance v1, LX/4Yv;

    invoke-direct {v1}, LX/4Yv;-><init>()V

    .line 205952
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 205953
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->n()Lcom/facebook/graphql/model/GraphQLSavable;

    move-result-object v3

    iput-object v3, v1, LX/4Yv;->b:Lcom/facebook/graphql/model/GraphQLSavable;

    .line 205954
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->a()I

    move-result v3

    iput v3, v1, LX/4Yv;->c:I

    .line 205955
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->j()I

    move-result v3

    iput v3, v1, LX/4Yv;->d:I

    .line 205956
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->k()Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    move-result-object v3

    iput-object v3, v1, LX/4Yv;->e:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    .line 205957
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v3

    iput-object v3, v1, LX/4Yv;->f:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 205958
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    iput-object v3, v1, LX/4Yv;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 205959
    invoke-static {v1, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 205960
    move-object v1, v1

    .line 205961
    iput-object p2, v1, LX/4Yv;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 205962
    move-object v1, v1

    .line 205963
    new-instance v3, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    invoke-direct {v3, v1}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;-><init>(LX/4Yv;)V

    .line 205964
    move-object v0, v3

    .line 205965
    goto/16 :goto_0
.end method

.method private c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205888
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205889
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205890
    invoke-static {v0}, LX/1VS;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205891
    iget-object v1, p0, LX/189;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Os;

    invoke-static {p2}, LX/6Os;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 205892
    iget-object v1, p0, LX/189;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2fl;

    invoke-static {v0}, LX/1VS;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    invoke-static {v3, v2}, LX/2fl;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/0Px;

    move-result-object v1

    .line 205893
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205894
    iput-object v1, v0, LX/23u;->i:LX/0Px;

    .line 205895
    move-object v0, v0

    .line 205896
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205897
    iput-wide v2, v0, LX/23u;->G:J

    .line 205898
    move-object v0, v0

    .line 205899
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205900
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205867
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205868
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205869
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205870
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 205871
    iget-object v1, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Ou;

    .line 205872
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205873
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205874
    invoke-static {p2}, LX/6Os;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 205875
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v5

    invoke-static {v5, v3}, LX/2fl;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/0Px;

    move-result-object v3

    .line 205876
    invoke-static {v2}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v4

    .line 205877
    iput-object v3, v4, LX/39x;->b:LX/0Px;

    .line 205878
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    move-object v1, v3

    .line 205879
    iget-object v2, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v1}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v1

    .line 205880
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205881
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 205882
    move-object v0, v0

    .line 205883
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205884
    iput-wide v2, v0, LX/23u;->G:J

    .line 205885
    move-object v0, v0

    .line 205886
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205887
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/enums/StoryVisibility;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205853
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205854
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 205855
    :goto_0
    iput-object v0, v1, LX/23u;->ab:Ljava/lang/String;

    .line 205856
    move-object v1, v1

    .line 205857
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205858
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v0

    .line 205859
    iput v0, v1, LX/23u;->ac:I

    .line 205860
    move-object v0, v1

    .line 205861
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205862
    iput-wide v2, v0, LX/23u;->G:J

    .line 205863
    move-object v0, v0

    .line 205864
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205865
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0

    .line 205866
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205851
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205852
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            "Z)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205839
    iget-object v0, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    .line 205840
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 205841
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 205842
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205843
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205844
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205845
    move-object v0, v0

    .line 205846
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v2

    .line 205847
    iput-wide v2, v0, LX/23u;->G:J

    .line 205848
    move-object v0, v0

    .line 205849
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205850
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205811
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205812
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205813
    iget-object v1, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v2, p2, p3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)LX/6PS;

    move-result-object v1

    .line 205814
    if-nez v1, :cond_0

    .line 205815
    :goto_0
    return-object p1

    .line 205816
    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 205817
    if-nez v2, :cond_1

    .line 205818
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205819
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205820
    move-object v0, v0

    .line 205821
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205822
    iput-wide v2, v0, LX/23u;->G:J

    .line 205823
    move-object v0, v0

    .line 205824
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205825
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 205826
    :cond_1
    iget-object v3, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/6PS;->b:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v2, p2, v3}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 205827
    iget-object v3, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v2

    .line 205828
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205829
    iput-object v2, v0, LX/23u;->k:LX/0Px;

    .line 205830
    move-object v0, v0

    .line 205831
    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205832
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205833
    move-object v0, v0

    .line 205834
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205835
    iput-wide v2, v0, LX/23u;->G:J

    .line 205836
    move-object v0, v0

    .line 205837
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205838
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205797
    const/4 v1, 0x0

    .line 205798
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205799
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205800
    sget-object v2, LX/6PF;->a:[I

    invoke-static {p1}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v3

    invoke-virtual {v3}, LX/34O;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    move-object v0, v1

    .line 205801
    :goto_0
    return-object v0

    .line 205802
    :pswitch_0
    invoke-direct {p0, p1, p2}, LX/189;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 205803
    :pswitch_1
    invoke-direct {p0, p1, p2}, LX/189;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 205804
    :pswitch_2
    invoke-static {p1}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/189;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 205805
    :pswitch_3
    iget-object v1, p0, LX/189;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yq;

    invoke-virtual {v1, v0}, LX/1yq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v0

    sget-object v1, LX/34O;->GROUPER:LX/34O;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1, p2}, LX/189;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, LX/189;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 205806
    :pswitch_4
    invoke-static {v0}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 205807
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    const v2, -0x1e53800c

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-eq v2, v0, :cond_3

    .line 205808
    :cond_2
    iget-object v0, p0, LX/189;->l:LX/0Uh;

    const/16 v2, 0x4ad

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, -0x22a42d2a    # -9.8999738E17f

    if-ne v0, v2, :cond_0

    .line 205809
    :cond_3
    :pswitch_5
    invoke-direct {p0, p1, p2}, LX/189;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 205810
    :pswitch_6
    invoke-direct {p0, p1, p2}, LX/189;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205655
    iget-object v0, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205656
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205657
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 205658
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    .line 205659
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-static {v2}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v2

    .line 205660
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v3

    invoke-static {v3, p2, p3}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v3

    .line 205661
    iput-object v3, v2, LX/4XR;->iR:Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    .line 205662
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 205663
    iput-object v2, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205664
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    move-object v1, v1

    .line 205665
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 205666
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205667
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    iget-object v0, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205668
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205669
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-static {v0, v1}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205670
    iput-object v0, v3, LX/23u;->k:LX/0Px;

    .line 205671
    move-object v0, v3

    .line 205672
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 205673
    iput-wide v4, v0, LX/23u;->G:J

    .line 205674
    move-object v0, v0

    .line 205675
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205676
    invoke-virtual {p0, v0, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205205
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205206
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205207
    iget-object v1, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p2, p4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Z)LX/6PS;

    move-result-object v2

    .line 205208
    if-nez v2, :cond_0

    .line 205209
    :goto_0
    return-object p1

    .line 205210
    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 205211
    if-nez v3, :cond_1

    .line 205212
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, v2, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205213
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205214
    move-object v0, v0

    .line 205215
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205216
    iput-wide v2, v0, LX/23u;->G:J

    .line 205217
    move-object v0, v0

    .line 205218
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205219
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 205220
    :cond_1
    iget-object v1, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Ou;

    .line 205221
    if-eqz p4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v4

    if-nez v4, :cond_3

    .line 205222
    :cond_2
    :goto_1
    move-object v1, v3

    .line 205223
    iget-object v3, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v1}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v1

    .line 205224
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205225
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 205226
    move-object v0, v0

    .line 205227
    iget-object v1, v2, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205228
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205229
    move-object v0, v0

    .line 205230
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205231
    iput-wide v2, v0, LX/23u;->G:J

    .line 205232
    move-object v0, v0

    .line 205233
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205234
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-static {v3}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-static {v5}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v6

    invoke-virtual {v1, v6, p2, p3}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v6

    .line 205235
    iput-object v6, v5, LX/4XR;->hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    .line 205236
    move-object v5, v5

    .line 205237
    invoke-virtual {v5}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    .line 205238
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205239
    move-object v4, v4

    .line 205240
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZZ)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205241
    iget-object v0, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205242
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205243
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 205244
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    .line 205245
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v4

    .line 205246
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v1

    invoke-static {v1}, LX/4YT;->a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;)LX/4YT;

    move-result-object v5

    .line 205247
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;->a()LX/0Px;

    move-result-object v6

    .line 205248
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 205249
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_2

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 205250
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 205251
    invoke-static {v1, p3}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLQuestionOption;Z)Lcom/facebook/graphql/model/GraphQLQuestionOption;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205252
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 205253
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v9

    if-eqz v9, :cond_1

    if-eqz p4, :cond_1

    .line 205254
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->m()Z

    move-result v9

    invoke-static {v1, v9}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLQuestionOption;Z)Lcom/facebook/graphql/model/GraphQLQuestionOption;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 205255
    :cond_1
    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 205256
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 205257
    iput-object v1, v5, LX/4YT;->b:LX/0Px;

    .line 205258
    move-object v1, v5

    .line 205259
    invoke-virtual {v1}, LX/4YT;->a()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v1

    .line 205260
    iput-object v1, v4, LX/4XR;->iR:Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    .line 205261
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 205262
    iput-object v1, v3, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205263
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    move-object v1, v1

    .line 205264
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 205265
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205266
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    iget-object v0, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205267
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205268
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-static {v0, v1}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205269
    iput-object v0, v3, LX/23u;->k:LX/0Px;

    .line 205270
    move-object v0, v3

    .line 205271
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 205272
    iput-wide v4, v0, LX/23u;->G:J

    .line 205273
    move-object v0, v0

    .line 205274
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205275
    invoke-virtual {p0, v0, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205276
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205277
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205278
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-nez v1, :cond_0

    .line 205279
    iget-object v1, p0, LX/189;->h:LX/03V;

    sget-object v2, LX/189;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Story feedback is null: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 205280
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 205281
    :goto_0
    return-object v0

    .line 205282
    :cond_0
    iget-object v1, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 205283
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205284
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205285
    move-object v0, v0

    .line 205286
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v2

    .line 205287
    iput-wide v2, v0, LX/23u;->G:J

    .line 205288
    move-object v0, v0

    .line 205289
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205290
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205618
    invoke-static {p2}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v1

    .line 205619
    if-eqz v1, :cond_1

    .line 205620
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205621
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205622
    invoke-static {v1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-static {v2, p1, v0}, LX/189;->a(LX/0Px;LX/0jW;LX/0jW;)LX/0Px;

    move-result-object v2

    .line 205623
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v3

    if-nez v3, :cond_4

    .line 205624
    :cond_0
    new-instance v3, LX/4Yy;

    invoke-direct {v3}, LX/4Yy;-><init>()V

    invoke-virtual {v3}, LX/4Yy;->a()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v3

    .line 205625
    :goto_0
    move-object v3, v3

    .line 205626
    invoke-static {v3}, LX/4Yy;->a(Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;)LX/4Yy;

    move-result-object v3

    invoke-static {v2}, LX/16z;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 205627
    iput-object v2, v3, LX/4Yy;->c:LX/0Px;

    .line 205628
    move-object v2, v3

    .line 205629
    invoke-virtual {v2}, LX/4Yy;->a()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v2

    .line 205630
    invoke-static {v1}, LX/4Yw;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/4Yw;

    move-result-object v3

    .line 205631
    iput-object v2, v3, LX/4Yw;->c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 205632
    move-object v2, v3

    .line 205633
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v4

    .line 205634
    iput-wide v4, v2, LX/4Yw;->h:J

    .line 205635
    move-object v2, v2

    .line 205636
    invoke-virtual {v2}, LX/4Yw;->a()Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v2

    .line 205637
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v0, v2

    .line 205638
    :goto_1
    return-object v0

    .line 205639
    :cond_1
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205640
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v1

    .line 205641
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 205642
    const/4 v2, 0x0

    move v3, v2

    move-object v4, p1

    .line 205643
    :goto_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_2

    .line 205644
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205645
    invoke-static {p0, v4, v0, v2}, LX/189;->a(LX/189;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 205646
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205647
    add-int/lit8 v3, v3, 0x1

    move-object v0, v2

    goto :goto_2

    .line 205648
    :cond_2
    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 205649
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205650
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 205651
    :cond_3
    const/4 v2, 0x0

    invoke-static {p1, v2}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 205652
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v0, v2

    .line 205653
    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205291
    invoke-static {p0, p2}, LX/189;->b(LX/189;Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 205292
    iput-object v1, v0, LX/23u;->m:Ljava/lang/String;

    .line 205293
    move-object v0, v0

    .line 205294
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 205295
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205296
    move-object v0, v0

    .line 205297
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    .line 205298
    iput-wide v2, v0, LX/23u;->v:J

    .line 205299
    move-object v0, v0

    .line 205300
    const/4 v1, 0x0

    .line 205301
    iput-boolean v1, v0, LX/23u;->U:Z

    .line 205302
    move-object v0, v0

    .line 205303
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205304
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Z)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "Z)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205305
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VO;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 205306
    :cond_0
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 205307
    :goto_0
    return-object v0

    .line 205308
    :cond_1
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0, p2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205309
    invoke-static {p0, p1}, LX/189;->b(LX/189;Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    iget-object v2, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205310
    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 205311
    move-object v0, v1

    .line 205312
    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v1

    .line 205313
    iput-object v1, v0, LX/23u;->ab:Ljava/lang/String;

    .line 205314
    move-object v0, v0

    .line 205315
    iput-boolean p3, v0, LX/23u;->U:Z

    .line 205316
    move-object v0, v0

    .line 205317
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205318
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 205319
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 205320
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205321
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 205322
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 205323
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;

    .line 205324
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v6

    invoke-static {v6}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 205325
    new-instance v6, LX/4XX;

    invoke-direct {v6}, LX/4XX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    .line 205326
    iput-object v0, v6, LX/4XX;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 205327
    move-object v0, v6

    .line 205328
    new-instance v6, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;

    invoke-direct {v6, v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;-><init>(LX/4XX;)V

    .line 205329
    move-object v0, v6

    .line 205330
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205331
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 205332
    :cond_2
    new-instance v8, LX/4XU;

    invoke-direct {v8}, LX/4XU;-><init>()V

    .line 205333
    invoke-virtual {p2}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 205334
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->b:Ljava/lang/String;

    .line 205335
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E_()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->c:Ljava/lang/String;

    .line 205336
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->F_()J

    move-result-wide v9

    iput-wide v9, v8, LX/4XU;->d:J

    .line 205337
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->w()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->e:Ljava/lang/String;

    .line 205338
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->x()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->f:Ljava/lang/String;

    .line 205339
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->y()I

    move-result v7

    iput v7, v8, LX/4XU;->g:I

    .line 205340
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205341
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->z()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->i:Ljava/lang/String;

    .line 205342
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->j:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 205343
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205344
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->C()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->l:Ljava/lang/String;

    .line 205345
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->m:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205346
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, LX/4XU;->n:Ljava/lang/String;

    .line 205347
    invoke-static {v8, p2}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 205348
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->L_()LX/0x2;

    move-result-object v7

    invoke-virtual {v7}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0x2;

    iput-object v7, v8, LX/4XU;->o:LX/0x2;

    .line 205349
    move-object v0, v8

    .line 205350
    new-instance v2, LX/4XW;

    invoke-direct {v2}, LX/4XW;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 205351
    iput-object v3, v2, LX/4XW;->b:LX/0Px;

    .line 205352
    move-object v2, v2

    .line 205353
    new-instance v3, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    invoke-direct {v3, v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;-><init>(LX/4XW;)V

    .line 205354
    move-object v2, v3

    .line 205355
    iput-object v2, v0, LX/4XU;->j:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    .line 205356
    move-object v0, v0

    .line 205357
    iget-object v2, p0, LX/189;->i:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 205358
    iput-wide v2, v0, LX/4XU;->d:J

    .line 205359
    move-object v0, v0

    .line 205360
    new-instance v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;-><init>(LX/4XU;)V

    .line 205361
    move-object v0, v2

    .line 205362
    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 205363
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->I_()I

    move-result v2

    invoke-static {v0, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 205364
    invoke-static {p2}, LX/18M;->d(Lcom/facebook/graphql/model/Sponsorable;)I

    move-result v2

    invoke-static {v0, v2}, LX/18M;->a(Lcom/facebook/graphql/model/Sponsorable;I)V

    .line 205365
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;
    .locals 7

    .prologue
    .line 205366
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->I_()I

    move-result v0

    .line 205367
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 205368
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->o()LX/0Px;

    move-result-object v3

    .line 205369
    const/4 v1, 0x0

    move v4, v1

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_6

    .line 205370
    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 205371
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 205372
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 205373
    :goto_1
    move v4, v4

    .line 205374
    if-ltz v4, :cond_0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    if-lt v4, v1, :cond_1

    .line 205375
    :cond_0
    :goto_2
    return-object p1

    .line 205376
    :cond_1
    if-le v0, v4, :cond_2

    add-int/lit8 v0, v0, -0x1

    .line 205377
    :cond_2
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 205378
    if-eq v1, v4, :cond_3

    .line 205379
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205380
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 205381
    :cond_4
    invoke-static {p1}, LX/4Xn;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)LX/4Xn;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v3

    .line 205382
    new-instance v4, LX/4Xm;

    invoke-direct {v4}, LX/4Xm;-><init>()V

    .line 205383
    invoke-virtual {v3}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 205384
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/4Xm;->b:LX/0Px;

    .line 205385
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    iput-object v5, v4, LX/4Xm;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 205386
    invoke-static {v4, v3}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 205387
    move-object v3, v4

    .line 205388
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 205389
    iput-object v2, v3, LX/4Xm;->b:LX/0Px;

    .line 205390
    move-object v2, v3

    .line 205391
    invoke-virtual {v2}, LX/4Xm;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v2

    .line 205392
    iput-object v2, v1, LX/4Xn;->j:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    .line 205393
    move-object v1, v1

    .line 205394
    iget-object v2, p0, LX/189;->i:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 205395
    iput-wide v2, v1, LX/4Xn;->d:J

    .line 205396
    move-object v1, v1

    .line 205397
    invoke-virtual {v1}, LX/4Xn;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    move-result-object p1

    .line 205398
    invoke-static {p1, v0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    goto :goto_2

    .line 205399
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 205400
    :cond_6
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;
    .locals 7

    .prologue
    .line 205401
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 205402
    invoke-static {p2}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 205403
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 205404
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 205405
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205406
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 205407
    :cond_1
    invoke-static {p2}, LX/4Xp;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/4Xp;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v1

    invoke-static {v1}, LX/4Xr;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;)LX/4Xr;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 205408
    iput-object v2, v1, LX/4Xr;->b:LX/0Px;

    .line 205409
    move-object v1, v1

    .line 205410
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 205411
    iput-object v2, v1, LX/4Xr;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 205412
    move-object v1, v1

    .line 205413
    invoke-virtual {v1}, LX/4Xr;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v1

    .line 205414
    iput-object v1, v0, LX/4Xp;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    .line 205415
    move-object v0, v0

    .line 205416
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205417
    iput-wide v2, v0, LX/4Xp;->e:J

    .line 205418
    move-object v0, v0

    .line 205419
    invoke-virtual {v0}, LX/4Xp;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-result-object v0

    .line 205420
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->I_()I

    move-result v1

    invoke-static {v0, v1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 205421
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 205422
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 205423
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205424
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205425
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205426
    iget-object v0, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1, p3, p4}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205427
    iget-object v1, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-static {v1, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205428
    invoke-static {p2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    .line 205429
    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 205430
    move-object v0, v1

    .line 205431
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205432
    iput-wide v2, v0, LX/23u;->G:J

    .line 205433
    move-object v0, v0

    .line 205434
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 205435
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205436
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205437
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205438
    iget-object v0, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1, p3, p4}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205439
    iget-object v1, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-static {v1, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205440
    invoke-static {p2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    .line 205441
    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 205442
    move-object v0, v1

    .line 205443
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205444
    iput-wide v2, v0, LX/23u;->G:J

    .line 205445
    move-object v0, v0

    .line 205446
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 205447
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205448
    iput-wide v2, v0, LX/23u;->G:J

    .line 205449
    move-object v0, v0

    .line 205450
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    .line 205451
    if-nez p1, :cond_0

    .line 205452
    const/4 v0, 0x0

    .line 205453
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205454
    iput-object p2, v0, LX/23u;->k:LX/0Px;

    .line 205455
    move-object v0, v0

    .line 205456
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205457
    iput-wide v2, v0, LX/23u;->G:J

    .line 205458
    move-object v0, v0

    .line 205459
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 205203
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 205204
    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, LX/189;->b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;LX/5vL;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 205460
    invoke-virtual {p0, p1, p2, p3, p4}, LX/189;->b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;LX/5vL;)LX/0Px;

    move-result-object v0

    .line 205461
    if-nez v0, :cond_0

    .line 205462
    :goto_0
    return-object p1

    :cond_0
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    .line 205463
    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 205464
    move-object v0, v1

    .line 205465
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205466
    iput-wide v2, v0, LX/23u;->G:J

    .line 205467
    move-object v0, v0

    .line 205468
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 205469
    invoke-static {p1}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205470
    if-nez v0, :cond_0

    .line 205471
    iget-object v0, p0, LX/189;->h:LX/03V;

    const-string v1, "null_GroupCommerceItemAttachment"

    const-string v2, "ProductItem is null on story after product availability change. t7270764."

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 205472
    const/4 v0, 0x0

    .line 205473
    :goto_0
    return-object v0

    .line 205474
    :cond_0
    iget-object v1, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0, p2}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205475
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    iget-object v2, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-static {v2, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 205476
    iput-object v0, v1, LX/23u;->k:LX/0Px;

    .line 205477
    move-object v0, v1

    .line 205478
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205479
    iput-wide v2, v0, LX/23u;->G:J

    .line 205480
    move-object v0, v0

    .line 205481
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;ZZLcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    const v3, -0x41c8fa46

    .line 205482
    iget-object v0, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 205483
    invoke-static {v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/3dM;->c(Z)LX/3dM;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/3dM;->k(Z)LX/3dM;

    move-result-object v1

    .line 205484
    iput-object p4, v1, LX/3dM;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 205485
    move-object v1, v1

    .line 205486
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 205487
    move-object v0, v1

    .line 205488
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    .line 205489
    iput-object v0, v1, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205490
    invoke-static {p1, v3}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 205491
    iget-object v2, p0, LX/189;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1, v3}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 205492
    if-eqz v2, :cond_1

    const/4 v3, 0x1

    :goto_0
    const-string p2, "The action link is null"

    invoke-static {v3, p2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 205493
    invoke-static {v2}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v3

    .line 205494
    iput-object v0, v3, LX/4Ys;->O:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205495
    move-object v3, v3

    .line 205496
    invoke-virtual {v3}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    move-object v0, v3

    .line 205497
    iget-object v2, p0, LX/189;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object v2

    .line 205498
    invoke-static {v0}, LX/2fl;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 205499
    :goto_1
    move-object v0, v2

    .line 205500
    iput-object v0, v1, LX/23u;->b:LX/0Px;

    .line 205501
    :cond_0
    iget-object v0, p0, LX/189;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 205502
    iput-wide v2, v1, LX/23u;->G:J

    .line 205503
    move-object v0, v1

    .line 205504
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    .line 205505
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 205506
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p2

    .line 205507
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p3

    const/4 v3, 0x0

    move p1, v3

    :goto_2
    if-ge p1, p3, :cond_4

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 205508
    invoke-static {v3}, LX/2fl;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result p4

    if-eqz p4, :cond_3

    .line 205509
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205510
    :goto_3
    add-int/lit8 v3, p1, 0x1

    move p1, v3

    goto :goto_2

    .line 205511
    :cond_3
    invoke-virtual {p2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 205512
    :cond_4
    invoke-virtual {p2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;LX/5vL;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/5vL;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 205513
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205514
    :cond_0
    :goto_0
    return-object v0

    .line 205515
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    .line 205516
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 205517
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 205518
    :goto_1
    move-object v1, v2

    .line 205519
    if-eqz v1, :cond_0

    .line 205520
    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 205521
    sget-object v2, LX/5vL;->ADD:LX/5vL;

    if-ne p4, v2, :cond_4

    .line 205522
    iget-object v2, p0, LX/189;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Ov;

    .line 205523
    const/4 v2, 0x1

    invoke-static {v1, p3, v2}, LX/6Ov;->b(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    move-object v2, v2

    .line 205524
    :goto_2
    move-object v1, v2

    .line 205525
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205526
    move-object v0, v0

    .line 205527
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 205528
    iget-object v1, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v0}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/189;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Ov;

    .line 205529
    const/4 v2, 0x0

    invoke-static {v1, p3, v2}, LX/6Ov;->b(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    move-object v2, v2

    .line 205530
    goto :goto_2
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205531
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205532
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205533
    iget-object v1, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p2}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;

    move-result-object v1

    .line 205534
    if-nez v1, :cond_0

    .line 205535
    :goto_0
    return-object p1

    .line 205536
    :cond_0
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205537
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205538
    move-object v0, v0

    .line 205539
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205540
    iput-wide v2, v0, LX/23u;->G:J

    .line 205541
    move-object v0, v0

    .line 205542
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205543
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/graphql/model/FeedUnit;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .prologue
    .line 205544
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205545
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205546
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205547
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 205548
    if-eqz p2, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 205549
    :goto_0
    iget-object v2, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205550
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 205551
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-ne v2, v0, :cond_1

    .line 205552
    :goto_1
    move-object v1, v1

    .line 205553
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205554
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    .line 205555
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205556
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v3, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v2, v1}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v1

    .line 205557
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 205558
    move-object v0, v0

    .line 205559
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205560
    iput-wide v2, v0, LX/23u;->G:J

    .line 205561
    move-object v0, v0

    .line 205562
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205563
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0

    .line 205564
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 205565
    :cond_1
    const/4 v2, 0x0

    .line 205566
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const p2, 0x285feb

    if-ne v4, p2, :cond_2

    .line 205567
    invoke-static {v3}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v2

    .line 205568
    iput-object v0, v2, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 205569
    move-object v2, v2

    .line 205570
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 205571
    :cond_2
    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    .line 205572
    iput-object v2, v3, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205573
    move-object v2, v3

    .line 205574
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    goto :goto_1
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205575
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 205576
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 205577
    iget-object v1, p0, LX/189;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20j;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v2, p3, p2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;

    move-result-object v1

    .line 205578
    if-nez v1, :cond_0

    .line 205579
    :goto_0
    return-object p1

    .line 205580
    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 205581
    if-nez v2, :cond_1

    .line 205582
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205583
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205584
    move-object v0, v0

    .line 205585
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205586
    iput-wide v2, v0, LX/23u;->G:J

    .line 205587
    move-object v0, v0

    .line 205588
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205589
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 205590
    :cond_1
    iget-object v3, p0, LX/189;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    .line 205591
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    if-nez p2, :cond_3

    .line 205592
    :cond_2
    :goto_1
    move-object v2, v2

    .line 205593
    iget-object v3, p0, LX/189;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v2

    .line 205594
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 205595
    iput-object v2, v0, LX/23u;->k:LX/0Px;

    .line 205596
    move-object v0, v0

    .line 205597
    iget-object v1, v1, LX/6PS;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205598
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 205599
    move-object v0, v0

    .line 205600
    iget-object v1, p0, LX/189;->i:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 205601
    iput-wide v2, v0, LX/23u;->G:J

    .line 205602
    move-object v0, v0

    .line 205603
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 205604
    invoke-virtual {p0, v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    :cond_3
    invoke-static {v2}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-static {v4}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v5

    .line 205605
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 205606
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p3

    const/4 v6, 0x0

    move v7, v6

    :goto_2
    if-ge v7, p3, :cond_5

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 205607
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v2

    .line 205608
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 205609
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 205610
    :cond_4
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2

    .line 205611
    :cond_5
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v5, v6

    .line 205612
    iput-object v5, v4, LX/4XR;->hE:LX/0Px;

    .line 205613
    move-object v4, v4

    .line 205614
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 205615
    iput-object v4, v3, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 205616
    move-object v3, v3

    .line 205617
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    goto :goto_1
.end method
