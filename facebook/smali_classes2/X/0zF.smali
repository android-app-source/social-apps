.class public LX/0zF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166762
    return-void
.end method

.method public static a(LX/0QB;)LX/0zF;
    .locals 3

    .prologue
    .line 166767
    const-class v1, LX/0zF;

    monitor-enter v1

    .line 166768
    :try_start_0
    sget-object v0, LX/0zF;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 166769
    sput-object v2, LX/0zF;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 166770
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166771
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    invoke-static {}, LX/0zF;->b()LX/0zF;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 166772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0zF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 166774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b()LX/0zF;
    .locals 1

    .prologue
    .line 166765
    new-instance v0, LX/0zF;

    invoke-direct {v0}, LX/0zF;-><init>()V

    .line 166766
    return-object v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 166763
    iget-object v0, p0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v0

    .line 166764
    return-object v0
.end method
