.class public LX/1SQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247885
    iput-object p1, p0, LX/1SQ;->a:Landroid/content/Context;

    .line 247886
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/1SQ;->b:Landroid/content/pm/PackageManager;

    .line 247887
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 247883
    iget-object v0, p0, LX/1SQ;->b:Landroid/content/pm/PackageManager;

    iget-object v1, p0, LX/1SQ;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1SQ;
    .locals 2

    .prologue
    .line 247881
    new-instance v1, LX/1SQ;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/1SQ;-><init>(Landroid/content/Context;)V

    .line 247882
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 247888
    invoke-virtual {p0}, LX/1SQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1SQ;->b:Landroid/content/pm/PackageManager;

    const-string v1, "android.hardware.bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 247880
    invoke-virtual {p0}, LX/1SQ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1SQ;->b:Landroid/content/pm/PackageManager;

    const-string v1, "android.hardware.bluetooth_le"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 247879
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    invoke-direct {p0, v0}, LX/1SQ;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 247878
    const-string v0, "android.permission.BLUETOOTH"

    invoke-direct {p0, v0}, LX/1SQ;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
