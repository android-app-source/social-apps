.class public final LX/19G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/19E;


# direct methods
.method public constructor <init>(LX/19E;)V
    .locals 0

    .prologue
    .line 207493
    iput-object p1, p0, LX/19G;->a:LX/19E;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 8

    .prologue
    .line 207494
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    .line 207495
    iget-object v2, p0, LX/19G;->a:LX/19E;

    iget-wide v2, v2, LX/19E;->d:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 207496
    iget-object v2, p0, LX/19G;->a:LX/19E;

    .line 207497
    iput-wide v0, v2, LX/19E;->d:J

    .line 207498
    iget-object v2, p0, LX/19G;->a:LX/19E;

    .line 207499
    iput-wide v0, v2, LX/19E;->e:J

    .line 207500
    iget-object v0, p0, LX/19G;->a:LX/19E;

    iget-object v0, v0, LX/19E;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/19G;->a:LX/19E;

    iget-object v1, v1, LX/19E;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 207501
    :goto_0
    return-void

    .line 207502
    :cond_0
    iget-object v2, p0, LX/19G;->a:LX/19E;

    iget-wide v2, v2, LX/19E;->e:J

    sub-long v2, v0, v2

    .line 207503
    iget-object v4, p0, LX/19G;->a:LX/19E;

    .line 207504
    iput-wide v0, v4, LX/19E;->e:J

    .line 207505
    iget-object v0, p0, LX/19G;->a:LX/19E;

    iget-object v0, v0, LX/19E;->a:LX/19D;

    long-to-int v1, v2

    invoke-virtual {v0, v1}, LX/19D;->a(I)V

    .line 207506
    iget-object v0, p0, LX/19G;->a:LX/19E;

    iget-object v0, v0, LX/19E;->b:Landroid/view/Choreographer;

    iget-object v1, p0, LX/19G;->a:LX/19E;

    iget-object v1, v1, LX/19E;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method
