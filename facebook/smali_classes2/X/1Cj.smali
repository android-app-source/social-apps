.class public LX/1Cj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/1Ck;

.field private final c:LX/0lB;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216712
    iput-object p1, p0, LX/1Cj;->a:LX/0tX;

    .line 216713
    iput-object p2, p0, LX/1Cj;->b:LX/1Ck;

    .line 216714
    iput-object p3, p0, LX/1Cj;->c:LX/0lB;

    .line 216715
    return-void
.end method

.method public static a(LX/1Cj;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GoodwillDailyDialogueMutationType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$FBDailyDialogueUpdateModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 216716
    if-eqz p1, :cond_1

    .line 216717
    :try_start_0
    iget-object v0, p0, LX/1Cj;->c:LX/0lB;

    const-class v2, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;

    invoke-virtual {v0, p1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;

    .line 216718
    iget-object v3, v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->type:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 216719
    :try_start_1
    iget-object v2, v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->id:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 216720
    :try_start_2
    iget-object v0, v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->extra:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v1, v2

    move-object v2, v3

    .line 216721
    :goto_0
    new-instance v3, LX/3k6;

    invoke-direct {v3}, LX/3k6;-><init>()V

    const-string v4, "GOOD_MORNING"

    .line 216722
    const-string v5, "product_name"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216723
    move-object v3, v3

    .line 216724
    const-string v4, "mutation_type"

    invoke-virtual {v3, v4, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216725
    move-object v3, v3

    .line 216726
    if-eqz v2, :cond_0

    .line 216727
    const-string v4, "lightweight_unit_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216728
    move-object v1, v3

    .line 216729
    const-string v4, "lightweight_unit_type"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216730
    move-object v1, v1

    .line 216731
    const-string v2, "lightweight_extra"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216732
    :cond_0
    new-instance v1, LX/399;

    .line 216733
    new-instance v0, LX/3k7;

    invoke-direct {v0}, LX/3k7;-><init>()V

    move-object v0, v0

    .line 216734
    const-string v2, "input"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/3k7;

    invoke-direct {v1, v0}, LX/399;-><init>(LX/0zP;)V

    .line 216735
    iget-object v0, p0, LX/1Cj;->a:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 216736
    :catch_0
    move-object v0, v1

    move-object v2, v1

    :goto_1
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :catch_1
    move-object v0, v1

    move-object v2, v1

    :goto_2
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :catch_2
    move-object v0, v1

    move-object v2, v3

    goto :goto_2

    :catch_3
    move-object v0, v2

    move-object v2, v3

    goto :goto_2

    :catch_4
    move-object v0, v1

    move-object v2, v3

    goto :goto_1

    :catch_5
    move-object v0, v2

    move-object v2, v3

    goto :goto_1

    :cond_1
    move-object v0, v1

    move-object v2, v1

    goto :goto_0
.end method
