.class public LX/0rM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:Lcom/facebook/analytics/logger/HoneyClientEvent;


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149504
    const/4 v0, 0x0

    iput-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149505
    iput-object p1, p0, LX/0rM;->a:LX/0Zb;

    .line 149506
    iput-object p2, p0, LX/0rM;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 149507
    return-void
.end method

.method private static a(J)D
    .locals 4

    .prologue
    .line 149502
    long-to-double v0, p0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(LX/0rM;Ljava/lang/String;LX/0rH;)LX/0rM;
    .locals 2

    .prologue
    .line 149497
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149498
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "clash_manager"

    .line 149499
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 149500
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "location"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149501
    return-object p0
.end method


# virtual methods
.method public final a(JI)LX/0rM;
    .locals 5

    .prologue
    .line 149494
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "previous_slot_taken_time"

    invoke-static {p1, p2}, LX/0rM;->a(J)D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149495
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "previous_clash_sequence_id"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149496
    return-object p0
.end method

.method public final a(LX/0qg;Ljava/lang/String;LX/0rL;)LX/0rM;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149508
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "unit_name"

    invoke-virtual {p1}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149509
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "subunit_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149510
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "subunit_type"

    invoke-virtual {p3}, LX/0rL;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149511
    return-object p0
.end method

.method public final a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;)LX/0rM;
    .locals 3
    .param p1    # LX/0qg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0rL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149487
    if-eqz p1, :cond_0

    .line 149488
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "previous_unit_name"

    invoke-virtual {p1}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149489
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "previous_subunit_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149490
    iget-object v1, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "previous_subunit_type"

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149491
    :cond_0
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "previous_slot_state"

    invoke-virtual {p4}, LX/0rN;->getSlotName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149492
    return-object p0

    .line 149493
    :cond_1
    invoke-virtual {p3}, LX/0rL;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0qg;Ljava/lang/String;LX/0rL;LX/0rN;JI)LX/0rM;
    .locals 5
    .param p1    # LX/0qg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0rL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149478
    if-eqz p1, :cond_0

    .line 149479
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "current_unit_name"

    invoke-virtual {p1}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149480
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "current_subunit_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149481
    iget-object v1, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "current_subunit_type"

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149482
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "slot_taken_time"

    invoke-static {p5, p6}, LX/0rM;->a(J)D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149483
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "clash_sequence_id"

    invoke-virtual {v0, v1, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149484
    :cond_0
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "current_slot_state"

    invoke-virtual {p4}, LX/0rN;->getSlotName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149485
    return-object p0

    .line 149486
    :cond_1
    invoke-virtual {p3}, LX/0rL;->getTypeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0rH;)LX/0rM;
    .locals 1

    .prologue
    .line 149477
    const-string v0, "slot_request"

    invoke-static {p0, v0, p1}, LX/0rM;->a(LX/0rM;Ljava/lang/String;LX/0rH;)LX/0rM;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)LX/0rM;
    .locals 3

    .prologue
    .line 149474
    iget-object v1, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "request_result"

    if-eqz p1, :cond_0

    sget-object v0, LX/0rR;->ALLOW:LX/0rR;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 149475
    return-object p0

    .line 149476
    :cond_0
    sget-object v0, LX/0rR;->DENY:LX/0rR;

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 149469
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-nez v0, :cond_0

    .line 149470
    :goto_0
    return-void

    .line 149471
    :cond_0
    iget-object v0, p0, LX/0rM;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0qh;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149472
    iget-object v0, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 149473
    :cond_1
    iget-object v0, p0, LX/0rM;->a:LX/0Zb;

    iget-object v1, p0, LX/0rM;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
