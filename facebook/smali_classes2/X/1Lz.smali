.class public abstract LX/1Lz;
.super LX/1M0;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/1M0",
        "<TE;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TE;",
            "LX/4xL;",
            ">;"
        }
    .end annotation
.end field

.field public transient b:J


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TE;",
            "LX/4xL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234696
    invoke-direct {p0}, LX/1M0;-><init>()V

    .line 234697
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    .line 234698
    invoke-super {p0}, LX/1M0;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/1Lz;->b:J

    .line 234699
    return-void
.end method

.method private static a(LX/4xL;I)I
    .locals 1

    .prologue
    .line 234693
    if-nez p0, :cond_0

    .line 234694
    const/4 v0, 0x0

    .line 234695
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, LX/4xL;->d(I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 234689
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234690
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 234691
    :cond_0
    iget p0, v0, LX/4xL;->value:I

    move v0, p0

    .line 234692
    goto :goto_0
.end method

.method public a(Ljava/lang/Object;I)I
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234638
    if-nez p2, :cond_0

    .line 234639
    invoke-virtual {p0, p1}, LX/1Lz;->a(Ljava/lang/Object;)I

    move-result v2

    .line 234640
    :goto_0
    return v2

    .line 234641
    :cond_0
    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 234642
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234643
    if-nez v0, :cond_2

    .line 234644
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    new-instance v1, LX/4xL;

    invoke-direct {v1, p2}, LX/4xL;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234645
    :goto_2
    iget-wide v0, p0, LX/1Lz;->b:J

    int-to-long v4, p2

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/1Lz;->b:J

    goto :goto_0

    :cond_1
    move v0, v2

    .line 234646
    goto :goto_1

    .line 234647
    :cond_2
    iget v3, v0, LX/4xL;->value:I

    move v4, v3

    .line 234648
    int-to-long v6, v4

    int-to-long v8, p2

    add-long/2addr v6, v8

    .line 234649
    const-wide/32 v8, 0x7fffffff

    cmp-long v3, v6, v8

    if-gtz v3, :cond_3

    move v3, v1

    :goto_3
    const-string v5, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v3, v5, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 234650
    iget v1, v0, LX/4xL;->value:I

    .line 234651
    add-int v2, v1, p2

    iput v2, v0, LX/4xL;->value:I

    .line 234652
    move v2, v4

    goto :goto_2

    :cond_3
    move v3, v2

    .line 234653
    goto :goto_3
.end method

.method public b(Ljava/lang/Object;I)I
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234674
    if-nez p2, :cond_1

    .line 234675
    invoke-virtual {p0, p1}, LX/1Lz;->a(Ljava/lang/Object;)I

    move-result v2

    .line 234676
    :cond_0
    :goto_0
    return v2

    .line 234677
    :cond_1
    if-lez p2, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 234678
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234679
    if-eqz v0, :cond_0

    .line 234680
    iget v1, v0, LX/4xL;->value:I

    move v1, v1

    .line 234681
    if-le v1, p2, :cond_3

    .line 234682
    :goto_2
    neg-int v2, p2

    invoke-virtual {v0, v2}, LX/4xL;->b(I)I

    .line 234683
    iget-wide v2, p0, LX/1Lz;->b:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/1Lz;->b:J

    move v2, v1

    .line 234684
    goto :goto_0

    :cond_2
    move v0, v2

    .line 234685
    goto :goto_1

    .line 234686
    :cond_3
    iget-object v2, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    goto :goto_2
.end method

.method public final b()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 234687
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 234688
    new-instance v1, LX/1M5;

    invoke-direct {v1, p0, v0}, LX/1M5;-><init>(LX/1Lz;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 234673
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;I)I
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 234663
    const-string v0, "count"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 234664
    if-nez p2, :cond_0

    .line 234665
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234666
    invoke-static {v0, p2}, LX/1Lz;->a(LX/4xL;I)I

    move-result v0

    .line 234667
    :goto_0
    iget-wide v2, p0, LX/1Lz;->b:J

    sub-int v1, p2, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/1Lz;->b:J

    .line 234668
    return v0

    .line 234669
    :cond_0
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234670
    invoke-static {v0, p2}, LX/1Lz;->a(LX/4xL;I)I

    move-result v1

    .line 234671
    if-nez v0, :cond_1

    .line 234672
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    new-instance v2, LX/4xL;

    invoke-direct {v2, p2}, LX/4xL;-><init>(I)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 234656
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 234657
    const/4 v2, 0x0

    .line 234658
    iput v2, v0, LX/4xL;->value:I

    .line 234659
    goto :goto_0

    .line 234660
    :cond_0
    iget-object v0, p0, LX/1Lz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 234661
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Lz;->b:J

    .line 234662
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 234655
    new-instance v0, LX/4wz;

    invoke-direct {v0, p0}, LX/4wz;-><init>(LX/1Lz;)V

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 234654
    iget-wide v0, p0, LX/1Lz;->b:J

    invoke-static {v0, v1}, LX/0a4;->b(J)I

    move-result v0

    return v0
.end method
