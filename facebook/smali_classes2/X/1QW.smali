.class public LX/1QW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pt;
.implements LX/1Pu;


# instance fields
.field private final a:LX/1Jg;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1fA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/0pJ;

.field private final e:LX/0Sh;

.field private f:LX/1Rb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Jg;LX/0Ot;LX/03V;LX/0pJ;LX/0Sh;)V
    .locals 0
    .param p1    # LX/1Jg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Jg;",
            "LX/0Ot",
            "<",
            "LX/1fA;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0pJ;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244671
    iput-object p1, p0, LX/1QW;->a:LX/1Jg;

    .line 244672
    iput-object p2, p0, LX/1QW;->b:LX/0Ot;

    .line 244673
    iput-object p3, p0, LX/1QW;->c:LX/03V;

    .line 244674
    iput-object p4, p0, LX/1QW;->d:LX/0pJ;

    .line 244675
    iput-object p5, p0, LX/1QW;->e:LX/0Sh;

    .line 244676
    return-void
.end method


# virtual methods
.method public final a(LX/1Rb;)V
    .locals 0

    .prologue
    .line 244668
    iput-object p1, p0, LX/1QW;->f:LX/1Rb;

    .line 244669
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 244666
    invoke-virtual {p0}, LX/1QW;->m()LX/1f9;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, LX/1QW;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 244667
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 4

    .prologue
    .line 244651
    if-nez p2, :cond_0

    .line 244652
    iget-object v0, p0, LX/1QW;->c:LX/03V;

    const-string v1, "HasPrefetcherImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null imageParam is used for prefetch: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 244653
    iget-object v3, p3, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 244654
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 244655
    :goto_0
    return-void

    .line 244656
    :cond_0
    iget-object v0, p1, LX/1f9;->b:LX/1Rb;

    move-object v1, v0

    .line 244657
    if-nez v1, :cond_1

    .line 244658
    iget-object v0, p0, LX/1QW;->c:LX/03V;

    const-string v1, "HasPrefetcherImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null rowKey is used for prefetch: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 244659
    iget-object v3, p3, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 244660
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 244661
    :cond_1
    new-instance v2, Lcom/facebook/photos/prefetch/PrefetchParams;

    invoke-direct {v2, p2, p3}, Lcom/facebook/photos/prefetch/PrefetchParams;-><init>(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 244662
    iget-boolean v0, p1, LX/1f9;->c:Z

    move v0, v0

    .line 244663
    if-nez v0, :cond_2

    .line 244664
    iget-object v0, p0, LX/1QW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fA;

    invoke-virtual {v0, v2}, LX/1fA;->a(Lcom/facebook/photos/prefetch/PrefetchParams;)V

    .line 244665
    :cond_2
    iget-object v0, p0, LX/1QW;->a:LX/1Jg;

    invoke-interface {v0, v1, v2}, LX/1Jg;->a(LX/1Rb;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 244677
    const/4 v0, 0x0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244643
    iget-object v0, p0, LX/1QW;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 244644
    iget-object v0, p0, LX/1QW;->f:LX/1Rb;

    if-nez v0, :cond_0

    .line 244645
    sget-object v0, LX/1f9;->a:LX/1f9;

    .line 244646
    :goto_0
    return-object v0

    .line 244647
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1QW;->d:LX/0pJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0pJ;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1QW;->a:LX/1Jg;

    invoke-interface {v0}, LX/1Jg;->c()LX/1R4;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 244648
    iget-object v0, p0, LX/1QW;->a:LX/1Jg;

    invoke-interface {v0}, LX/1Jg;->c()LX/1R4;

    move-result-object v0

    iget-object v1, p0, LX/1QW;->f:LX/1Rb;

    invoke-interface {v0, v1}, LX/1R4;->a(LX/1Rb;)Z

    move-result v1

    .line 244649
    new-instance v0, LX/1f9;

    iget-object v2, p0, LX/1QW;->f:LX/1Rb;

    invoke-direct {v0, v2, v1}, LX/1f9;-><init>(LX/1Rb;Z)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244650
    :catch_0
    :cond_1
    new-instance v0, LX/1f9;

    iget-object v1, p0, LX/1QW;->f:LX/1Rb;

    invoke-direct {v0, v1, v3}, LX/1f9;-><init>(LX/1Rb;Z)V

    goto :goto_0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 244640
    iget-object v0, p0, LX/1QW;->f:LX/1Rb;

    if-nez v0, :cond_0

    .line 244641
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 244642
    :cond_0
    iget-object v0, p0, LX/1QW;->f:LX/1Rb;

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 244638
    const/4 v0, 0x0

    iput-object v0, p0, LX/1QW;->f:LX/1Rb;

    .line 244639
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 244637
    iget-object v0, p0, LX/1QW;->f:LX/1Rb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
