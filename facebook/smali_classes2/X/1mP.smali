.class public LX/1mP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Exception;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/1mP;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 313764
    const-class v0, Ljava/io/IOException;

    const-class v1, Ljava/util/concurrent/CancellationException;

    const-class v2, Ljava/util/concurrent/TimeoutException;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/1mP;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 313772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313773
    return-void
.end method

.method public static a(LX/0QB;)LX/1mP;
    .locals 3

    .prologue
    .line 313774
    sget-object v0, LX/1mP;->b:LX/1mP;

    if-nez v0, :cond_1

    .line 313775
    const-class v1, LX/1mP;

    monitor-enter v1

    .line 313776
    :try_start_0
    sget-object v0, LX/1mP;->b:LX/1mP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 313777
    if-eqz v2, :cond_0

    .line 313778
    :try_start_1
    new-instance v0, LX/1mP;

    invoke-direct {v0}, LX/1mP;-><init>()V

    .line 313779
    move-object v0, v0

    .line 313780
    sput-object v0, LX/1mP;->b:LX/1mP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 313782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 313783
    :cond_1
    sget-object v0, LX/1mP;->b:LX/1mP;

    return-object v0

    .line 313784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 313785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/Throwable;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 313765
    sget-object v0, LX/1mP;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    sget-object v0, LX/1mP;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 313766
    invoke-static {p0}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4, v0}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v0, v4}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 313767
    if-eqz v0, :cond_0

    .line 313768
    const/4 v0, 0x1

    .line 313769
    :goto_1
    return v0

    .line 313770
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 313771
    goto :goto_1
.end method
