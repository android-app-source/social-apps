.class public LX/1J4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public a:LX/1J5;

.field private b:LX/1J5;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 229661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229662
    sget-object v0, LX/1J5;->NOT_SCROLLING:LX/1J5;

    iput-object v0, p0, LX/1J4;->a:LX/1J5;

    .line 229663
    sget-object v0, LX/1J5;->NOT_SCROLLING:LX/1J5;

    iput-object v0, p0, LX/1J4;->b:LX/1J5;

    .line 229664
    iput v1, p0, LX/1J4;->c:I

    .line 229665
    iput v1, p0, LX/1J4;->d:I

    return-void
.end method


# virtual methods
.method public a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 229666
    return-void
.end method

.method public a(LX/0g8;III)V
    .locals 3

    .prologue
    .line 229667
    iget-object v0, p0, LX/1J4;->a:LX/1J5;

    .line 229668
    iget v1, p0, LX/1J4;->d:I

    if-le p2, v1, :cond_2

    .line 229669
    sget-object v0, LX/1J5;->SCROLLING_DOWN:LX/1J5;

    .line 229670
    :cond_0
    :goto_0
    iput p2, p0, LX/1J4;->d:I

    .line 229671
    iget-object v1, p0, LX/1J4;->b:LX/1J5;

    if-ne v0, v1, :cond_3

    .line 229672
    iget v1, p0, LX/1J4;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/1J4;->c:I

    .line 229673
    :goto_1
    iput-object v0, p0, LX/1J4;->b:LX/1J5;

    .line 229674
    iget v1, p0, LX/1J4;->c:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    .line 229675
    iput-object v0, p0, LX/1J4;->a:LX/1J5;

    .line 229676
    :cond_1
    return-void

    .line 229677
    :cond_2
    iget v1, p0, LX/1J4;->d:I

    if-ge p2, v1, :cond_0

    .line 229678
    sget-object v0, LX/1J5;->SCROLLING_UP:LX/1J5;

    goto :goto_0

    .line 229679
    :cond_3
    const/4 v1, 0x1

    iput v1, p0, LX/1J4;->c:I

    goto :goto_1
.end method
