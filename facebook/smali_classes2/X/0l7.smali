.class public LX/0l7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:LX/0l7;


# instance fields
.field public b:LX/0Uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13Q;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LX/0i1;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile i:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128263
    const-class v0, LX/0l7;

    sput-object v0, LX/0l7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 128264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128265
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 128266
    iput-object v0, p0, LX/0l7;->c:LX/0Ot;

    .line 128267
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 128268
    iput-object v0, p0, LX/0l7;->d:LX/0Ot;

    .line 128269
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 128270
    iput-object v0, p0, LX/0l7;->f:LX/0Ot;

    .line 128271
    return-void
.end method

.method public static a(LX/0QB;)LX/0l7;
    .locals 8

    .prologue
    .line 128272
    sget-object v0, LX/0l7;->j:LX/0l7;

    if-nez v0, :cond_1

    .line 128273
    const-class v1, LX/0l7;

    monitor-enter v1

    .line 128274
    :try_start_0
    sget-object v0, LX/0l7;->j:LX/0l7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 128275
    if-eqz v2, :cond_0

    .line 128276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 128277
    new-instance v3, LX/0l7;

    invoke-direct {v3}, LX/0l7;-><init>()V

    .line 128278
    invoke-static {v0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v4

    check-cast v4, LX/0Uq;

    const/16 v5, 0xbd2

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1416

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const/16 p0, 0x8d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 128279
    iput-object v4, v3, LX/0l7;->b:LX/0Uq;

    iput-object v5, v3, LX/0l7;->c:LX/0Ot;

    iput-object v6, v3, LX/0l7;->d:LX/0Ot;

    iput-object v7, v3, LX/0l7;->e:LX/0Uh;

    iput-object p0, v3, LX/0l7;->f:LX/0Ot;

    .line 128280
    move-object v0, v3

    .line 128281
    sput-object v0, LX/0l7;->j:LX/0l7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128282
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 128283
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 128284
    :cond_1
    sget-object v0, LX/0l7;->j:LX/0l7;

    return-object v0

    .line 128285
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 128286
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/0l7;Landroid/app/Activity;)LX/0i1;
    .locals 4

    .prologue
    .line 128287
    const-string v0, "InterstitialActivityListener.getControllerForActivityCreated"

    const v1, -0x76eb8c5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 128288
    :try_start_0
    iget-object v0, p0, LX/0l7;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ACTIVITY_CREATED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 128289
    const v1, 0x3f3ca51

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x7391fc5e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128290
    iget-object v0, p0, LX/0l7;->b:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 128291
    :cond_0
    :goto_0
    return-void

    .line 128292
    :cond_1
    iget-object v0, p0, LX/0l7;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0l7;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_4

    iget-object v0, p0, LX/0l7;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_4

    .line 128293
    iget-object v0, p0, LX/0l7;->g:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128294
    iget-object v0, p0, LX/0l7;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13Q;

    const-string v1, "android.interstitialactivitylistener.onactivitycreate.taskIsDone"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 128295
    :goto_1
    :try_start_0
    iget-object v0, p0, LX/0l7;->g:Ljava/util/concurrent/Future;

    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0i1;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128296
    iput-object v2, p0, LX/0l7;->h:Ljava/lang/ref/WeakReference;

    .line 128297
    iput-object v2, p0, LX/0l7;->g:Ljava/util/concurrent/Future;

    .line 128298
    iput-object v2, p0, LX/0l7;->i:Ljava/lang/Boolean;

    move-object v1, v0

    .line 128299
    :goto_2
    if-eqz v1, :cond_0

    instance-of v0, v1, LX/0i0;

    if-eqz v0, :cond_0

    .line 128300
    iget-object v0, p0, LX/0l7;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 128301
    check-cast v0, LX/0i0;

    .line 128302
    iget-object v1, v0, LX/0i0;->b:LX/0iU;

    .line 128303
    sget-object v0, LX/10Q;->SHOWN_TABS:LX/10Q;

    invoke-static {v1, v0}, LX/0iU;->a$redex0(LX/0iU;LX/10Q;)V

    .line 128304
    goto :goto_0

    .line 128305
    :cond_2
    iget-object v0, p0, LX/0l7;->i:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_3

    .line 128306
    iget-object v0, p0, LX/0l7;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13Q;

    const-string v1, "android.interstitialactivitylistener.onactivitycreate.taskStillRunning"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 128307
    :cond_3
    iget-object v0, p0, LX/0l7;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13Q;

    const-string v1, "android.interstitialactivitylistener.onactivitycreate.taskNotStarted"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 128308
    :catch_0
    move-exception v0

    .line 128309
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 128310
    :cond_4
    invoke-static {p0, p1}, LX/0l7;->c(LX/0l7;Landroid/app/Activity;)LX/0i1;

    move-result-object v1

    goto :goto_2
.end method
