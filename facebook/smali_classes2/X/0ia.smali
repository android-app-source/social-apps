.class public final enum LX/0ia;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0ia;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0ia;

.field public static final enum BACKGROUND:LX/0ia;

.field public static final enum FOREGROUND:LX/0ia;

.field public static final enum MUTE_SWITCH_OFF:LX/0ia;

.field public static final enum MUTE_SWITCH_ON:LX/0ia;

.field public static final enum RESET_BY_BACKGROUND_AUDIO:LX/0ia;

.field public static final enum RESET_BY_MUTE_SWITCH:LX/0ia;

.field public static final enum SHOW_SETTING_NUX:LX/0ia;

.field public static final enum SHOW_TOGGLE_NUX:LX/0ia;

.field public static final enum START_SESSION:LX/0ia;

.field public static final enum TAP_TO_MUTE_SHOWN:LX/0ia;

.field public static final enum TAP_TO_SOUND_SHOWN:LX/0ia;

.field public static final enum TURN_OFF_BY_TOGGLE:LX/0ia;

.field public static final enum TURN_ON_BY_TOGGLE:LX/0ia;

.field public static final enum TURN_ON_BY_VOLUME:LX/0ia;

.field public static final enum VIDEO_HAS_NO_SOUND_SHOWN:LX/0ia;

.field public static final enum VOLUME_DECREASE:LX/0ia;

.field public static final enum VOLUME_INCREASE:LX/0ia;


# instance fields
.field private final actionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 121220
    new-instance v0, LX/0ia;

    const-string v1, "START_SESSION"

    const-string v2, "start_session"

    invoke-direct {v0, v1, v4, v2}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->START_SESSION:LX/0ia;

    .line 121221
    new-instance v0, LX/0ia;

    const-string v1, "TURN_ON_BY_TOGGLE"

    const-string v2, "turn_on_by_toggle"

    invoke-direct {v0, v1, v5, v2}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->TURN_ON_BY_TOGGLE:LX/0ia;

    .line 121222
    new-instance v0, LX/0ia;

    const-string v1, "TURN_OFF_BY_TOGGLE"

    const-string v2, "turn_off_by_toggle"

    invoke-direct {v0, v1, v6, v2}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->TURN_OFF_BY_TOGGLE:LX/0ia;

    .line 121223
    new-instance v0, LX/0ia;

    const-string v1, "TURN_ON_BY_VOLUME"

    const-string v2, "turn_on_by_volume"

    invoke-direct {v0, v1, v7, v2}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->TURN_ON_BY_VOLUME:LX/0ia;

    .line 121224
    new-instance v0, LX/0ia;

    const-string v1, "MUTE_SWITCH_ON"

    const-string v2, "mute_switch_on"

    invoke-direct {v0, v1, v8, v2}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->MUTE_SWITCH_ON:LX/0ia;

    .line 121225
    new-instance v0, LX/0ia;

    const-string v1, "MUTE_SWITCH_OFF"

    const/4 v2, 0x5

    const-string v3, "mute_switch_off"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->MUTE_SWITCH_OFF:LX/0ia;

    .line 121226
    new-instance v0, LX/0ia;

    const-string v1, "RESET_BY_MUTE_SWITCH"

    const/4 v2, 0x6

    const-string v3, "reset_by_mute_switch"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->RESET_BY_MUTE_SWITCH:LX/0ia;

    .line 121227
    new-instance v0, LX/0ia;

    const-string v1, "RESET_BY_BACKGROUND_AUDIO"

    const/4 v2, 0x7

    const-string v3, "reset_by_background_audio"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->RESET_BY_BACKGROUND_AUDIO:LX/0ia;

    .line 121228
    new-instance v0, LX/0ia;

    const-string v1, "SHOW_TOGGLE_NUX"

    const/16 v2, 0x8

    const-string v3, "show_toggle_nux"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->SHOW_TOGGLE_NUX:LX/0ia;

    .line 121229
    new-instance v0, LX/0ia;

    const-string v1, "SHOW_SETTING_NUX"

    const/16 v2, 0x9

    const-string v3, "show_setting_nux"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->SHOW_SETTING_NUX:LX/0ia;

    .line 121230
    new-instance v0, LX/0ia;

    const-string v1, "TAP_TO_SOUND_SHOWN"

    const/16 v2, 0xa

    const-string v3, "tap_to_sound_shown"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->TAP_TO_SOUND_SHOWN:LX/0ia;

    .line 121231
    new-instance v0, LX/0ia;

    const-string v1, "TAP_TO_MUTE_SHOWN"

    const/16 v2, 0xb

    const-string v3, "tap_to_mute_shown"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->TAP_TO_MUTE_SHOWN:LX/0ia;

    .line 121232
    new-instance v0, LX/0ia;

    const-string v1, "VIDEO_HAS_NO_SOUND_SHOWN"

    const/16 v2, 0xc

    const-string v3, "video_has_no_sound_shown"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->VIDEO_HAS_NO_SOUND_SHOWN:LX/0ia;

    .line 121233
    new-instance v0, LX/0ia;

    const-string v1, "VOLUME_INCREASE"

    const/16 v2, 0xd

    const-string v3, "volume_increase"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->VOLUME_INCREASE:LX/0ia;

    .line 121234
    new-instance v0, LX/0ia;

    const-string v1, "VOLUME_DECREASE"

    const/16 v2, 0xe

    const-string v3, "volume_decrease"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->VOLUME_DECREASE:LX/0ia;

    .line 121235
    new-instance v0, LX/0ia;

    const-string v1, "BACKGROUND"

    const/16 v2, 0xf

    const-string v3, "background"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->BACKGROUND:LX/0ia;

    .line 121236
    new-instance v0, LX/0ia;

    const-string v1, "FOREGROUND"

    const/16 v2, 0x10

    const-string v3, "foreground"

    invoke-direct {v0, v1, v2, v3}, LX/0ia;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0ia;->FOREGROUND:LX/0ia;

    .line 121237
    const/16 v0, 0x11

    new-array v0, v0, [LX/0ia;

    sget-object v1, LX/0ia;->START_SESSION:LX/0ia;

    aput-object v1, v0, v4

    sget-object v1, LX/0ia;->TURN_ON_BY_TOGGLE:LX/0ia;

    aput-object v1, v0, v5

    sget-object v1, LX/0ia;->TURN_OFF_BY_TOGGLE:LX/0ia;

    aput-object v1, v0, v6

    sget-object v1, LX/0ia;->TURN_ON_BY_VOLUME:LX/0ia;

    aput-object v1, v0, v7

    sget-object v1, LX/0ia;->MUTE_SWITCH_ON:LX/0ia;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0ia;->MUTE_SWITCH_OFF:LX/0ia;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0ia;->RESET_BY_MUTE_SWITCH:LX/0ia;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0ia;->RESET_BY_BACKGROUND_AUDIO:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0ia;->SHOW_TOGGLE_NUX:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0ia;->SHOW_SETTING_NUX:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0ia;->TAP_TO_SOUND_SHOWN:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0ia;->TAP_TO_MUTE_SHOWN:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0ia;->VIDEO_HAS_NO_SOUND_SHOWN:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0ia;->VOLUME_INCREASE:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0ia;->VOLUME_DECREASE:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0ia;->BACKGROUND:LX/0ia;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0ia;->FOREGROUND:LX/0ia;

    aput-object v2, v0, v1

    sput-object v0, LX/0ia;->$VALUES:[LX/0ia;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 121217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121218
    iput-object p3, p0, LX/0ia;->actionName:Ljava/lang/String;

    .line 121219
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0ia;
    .locals 1

    .prologue
    .line 121238
    const-class v0, LX/0ia;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0ia;

    return-object v0
.end method

.method public static values()[LX/0ia;
    .locals 1

    .prologue
    .line 121216
    sget-object v0, LX/0ia;->$VALUES:[LX/0ia;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0ia;

    return-object v0
.end method


# virtual methods
.method public final getActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121215
    iget-object v0, p0, LX/0ia;->actionName:Ljava/lang/String;

    return-object v0
.end method
