.class public abstract LX/1SM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 247851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247852
    const/4 v0, 0x0

    iput-object v0, p0, LX/1SM;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TV;>;"
        }
    .end annotation
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247853
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1SM;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 247854
    invoke-virtual {p0}, LX/1SM;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/1SM;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 247855
    :cond_0
    iget-object v0, p0, LX/1SM;->a:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 247856
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
