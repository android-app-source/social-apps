.class public final LX/1em;
.super Ljava/util/concurrent/FutureTask;
.source ""

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TT;>;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Fu;

.field private final b:LX/0Sx;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;LX/1Fu;)V
    .locals 1

    .prologue
    .line 288953
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 288954
    new-instance v0, LX/0Sx;

    invoke-direct {v0}, LX/0Sx;-><init>()V

    iput-object v0, p0, LX/1em;->b:LX/0Sx;

    .line 288955
    iput-object p2, p0, LX/1em;->a:LX/1Fu;

    .line 288956
    return-void
.end method


# virtual methods
.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 288960
    iget-object v0, p0, LX/1em;->b:LX/0Sx;

    invoke-virtual {v0, p1, p2}, LX/0Sx;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 288961
    return-void
.end method

.method public final done()V
    .locals 1

    .prologue
    .line 288957
    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->done()V

    .line 288958
    iget-object v0, p0, LX/1em;->b:LX/0Sx;

    invoke-virtual {v0}, LX/0Sx;->a()V

    .line 288959
    return-void
.end method
