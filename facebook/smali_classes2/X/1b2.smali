.class public LX/1b2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1b2;


# instance fields
.field public a:LX/1E5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Class;
    .annotation runtime Lcom/facebook/facecast/launcher/FacecastLauncherActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/Class;
    .annotation runtime Lcom/facebook/facecast/launcher/FacecastNuxActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 279088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279089
    return-void
.end method

.method public static a(LX/0QB;)LX/1b2;
    .locals 7

    .prologue
    .line 279070
    sget-object v0, LX/1b2;->e:LX/1b2;

    if-nez v0, :cond_1

    .line 279071
    const-class v1, LX/1b2;

    monitor-enter v1

    .line 279072
    :try_start_0
    sget-object v0, LX/1b2;->e:LX/1b2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 279073
    if-eqz v2, :cond_0

    .line 279074
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 279075
    new-instance p0, LX/1b2;

    invoke-direct {p0}, LX/1b2;-><init>()V

    .line 279076
    invoke-static {v0}, LX/1E5;->b(LX/0QB;)LX/1E5;

    move-result-object v3

    check-cast v3, LX/1E5;

    invoke-static {v0}, LX/1b3;->b(LX/0QB;)LX/1b3;

    move-result-object v4

    check-cast v4, LX/1b3;

    invoke-static {v0}, LX/1b5;->a(LX/0QB;)Ljava/lang/Class;

    move-result-object v5

    check-cast v5, Ljava/lang/Class;

    .line 279077
    const-class v6, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    move-object v6, v6

    .line 279078
    move-object v6, v6

    .line 279079
    check-cast v6, Ljava/lang/Class;

    .line 279080
    iput-object v3, p0, LX/1b2;->a:LX/1E5;

    iput-object v4, p0, LX/1b2;->b:LX/1b3;

    iput-object v5, p0, LX/1b2;->c:Ljava/lang/Class;

    iput-object v6, p0, LX/1b2;->d:Ljava/lang/Class;

    .line 279081
    move-object v0, p0

    .line 279082
    sput-object v0, LX/1b2;->e:LX/1b2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279083
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 279084
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 279085
    :cond_1
    sget-object v0, LX/1b2;->e:LX/1b2;

    return-object v0

    .line 279086
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 279087
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 279090
    iget-object v0, p0, LX/1b2;->a:LX/1E5;

    invoke-virtual {v0, p2}, LX/1E5;->b(Z)Ljava/lang/String;

    move-result-object v0

    .line 279091
    if-eqz v0, :cond_0

    .line 279092
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 279093
    new-instance v0, Landroid/content/Intent;

    const-class p0, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;

    invoke-direct {v0, p1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "error_exception"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 279094
    :goto_0
    return-object v0

    .line 279095
    :cond_0
    iget-object v0, p0, LX/1b2;->b:LX/1b3;

    invoke-virtual {v0}, LX/1b3;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279096
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1b2;->d:Ljava/lang/Class;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 279097
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1b2;->c:Ljava/lang/Class;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 279069
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1b2;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 279068
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1b2;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
