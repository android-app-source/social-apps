.class public LX/0UR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0UR;


# instance fields
.field private final a:LX/0TJ;

.field public final b:LX/0Sj;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0TR;


# direct methods
.method public constructor <init>(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TJ;",
            "LX/0Sj;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 64584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64585
    iput-object p1, p0, LX/0UR;->a:LX/0TJ;

    .line 64586
    iput-object p2, p0, LX/0UR;->b:LX/0Sj;

    .line 64587
    iput-object p3, p0, LX/0UR;->c:LX/0Or;

    .line 64588
    iput-object p4, p0, LX/0UR;->d:LX/0TR;

    .line 64589
    return-void
.end method

.method public static a(LX/0QB;)LX/0UR;
    .locals 7

    .prologue
    .line 64590
    sget-object v0, LX/0UR;->e:LX/0UR;

    if-nez v0, :cond_1

    .line 64591
    const-class v1, LX/0UR;

    monitor-enter v1

    .line 64592
    :try_start_0
    sget-object v0, LX/0UR;->e:LX/0UR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 64593
    if-eqz v2, :cond_0

    .line 64594
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 64595
    new-instance v6, LX/0UR;

    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v3

    check-cast v3, LX/0TJ;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v4

    check-cast v4, LX/0Sj;

    const/16 v5, 0x1a1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v5

    check-cast v5, LX/0TR;

    invoke-direct {v6, v3, v4, p0, v5}, LX/0UR;-><init>(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)V

    .line 64596
    move-object v0, v6

    .line 64597
    sput-object v0, LX/0UR;->e:LX/0UR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64598
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 64599
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 64600
    :cond_1
    sget-object v0, LX/0UR;->e:LX/0UR;

    return-object v0

    .line 64601
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 64602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;II)LX/0TU;
    .locals 7

    .prologue
    .line 64603
    iget-object v6, p0, LX/0UR;->c:LX/0Or;

    iget-object v0, p0, LX/0UR;->a:LX/0TJ;

    invoke-virtual {v0, p1}, LX/0TJ;->d(Ljava/lang/String;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v3

    iget-object v4, p0, LX/0UR;->b:LX/0Sj;

    iget-object v5, p0, LX/0UR;->d:LX/0TR;

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {v6, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;III)LX/0TU;
    .locals 7

    .prologue
    .line 64604
    iget-object v6, p0, LX/0UR;->c:LX/0Or;

    iget-object v0, p0, LX/0UR;->a:LX/0TJ;

    .line 64605
    iget v1, v0, LX/0TJ;->c:I

    invoke-static {p1, v1, p4}, LX/0TJ;->a(Ljava/lang/String;II)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    move-object v3, v1

    .line 64606
    iget-object v4, p0, LX/0UR;->b:LX/0Sj;

    iget-object v5, p0, LX/0UR;->d:LX/0TR;

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v0

    invoke-static {v6, v0}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v0

    return-object v0
.end method
