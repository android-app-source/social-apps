.class public LX/1Kp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Kq;


# instance fields
.field private final a:I

.field private final b:LX/0So;

.field private c:J


# direct methods
.method public constructor <init>(ILX/0So;)V
    .locals 2

    .prologue
    .line 233173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233174
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1Kp;->c:J

    .line 233175
    iput p1, p0, LX/1Kp;->a:I

    .line 233176
    iput-object p2, p0, LX/1Kp;->b:LX/0So;

    .line 233177
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    .line 233178
    iget-object v0, p0, LX/1Kp;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 233179
    iget-wide v2, p0, LX/1Kp;->c:J

    sub-long v2, v0, v2

    iget v4, p0, LX/1Kp;->a:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 233180
    const/4 v0, 0x1

    .line 233181
    :goto_0
    return v0

    .line 233182
    :cond_0
    iput-wide v0, p0, LX/1Kp;->c:J

    .line 233183
    const/4 v0, 0x0

    goto :goto_0
.end method
