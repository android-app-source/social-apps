.class public final LX/0zu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zt;


# static fields
.field public static final a:LX/0zu;

.field public static final b:LX/0zu;


# instance fields
.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 168070
    new-instance v0, LX/0zu;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/0zu;-><init>(Z)V

    sput-object v0, LX/0zu;->a:LX/0zu;

    .line 168071
    new-instance v0, LX/0zu;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/0zu;-><init>(Z)V

    sput-object v0, LX/0zu;->b:LX/0zu;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 168072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168073
    iput-boolean p1, p0, LX/0zu;->c:Z

    .line 168074
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 168075
    add-int v3, p2, p3

    move v0, v2

    :goto_0
    if-ge p2, v3, :cond_2

    .line 168076
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v4

    .line 168077
    packed-switch v4, :pswitch_data_0

    .line 168078
    const/4 p3, 0x2

    :goto_1
    move v4, p3

    .line 168079
    packed-switch v4, :pswitch_data_1

    .line 168080
    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 168081
    :pswitch_0
    iget-boolean v0, p0, LX/0zu;->c:Z

    if-eqz v0, :cond_1

    move v1, v2

    .line 168082
    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v1

    .line 168083
    goto :goto_2

    .line 168084
    :pswitch_1
    iget-boolean v0, p0, LX/0zu;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 168085
    goto :goto_2

    .line 168086
    :cond_2
    if-eqz v0, :cond_3

    .line 168087
    iget-boolean v0, p0, LX/0zu;->c:Z

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_3

    .line 168088
    :cond_3
    const/4 v1, 0x2

    goto :goto_3

    .line 168089
    :pswitch_2
    const/4 p3, 0x1

    goto :goto_1

    .line 168090
    :pswitch_3
    const/4 p3, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
