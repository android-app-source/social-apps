.class public abstract LX/1eS;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1cW;

.field public final synthetic b:LX/1cQ;

.field private final c:LX/1BV;

.field private final d:LX/1bZ;

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:LX/1eV;


# direct methods
.method public constructor <init>(LX/1cQ;LX/1cd;LX/1cW;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 288318
    iput-object p1, p0, LX/1eS;->b:LX/1cQ;

    .line 288319
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 288320
    iput-object p3, p0, LX/1eS;->a:LX/1cW;

    .line 288321
    iget-object v0, p3, LX/1cW;->c:LX/1BV;

    move-object v0, v0

    .line 288322
    iput-object v0, p0, LX/1eS;->c:LX/1BV;

    .line 288323
    iget-object v0, p3, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 288324
    iget-object v1, v0, LX/1bf;->g:LX/1bZ;

    move-object v0, v1

    .line 288325
    iput-object v0, p0, LX/1eS;->d:LX/1bZ;

    .line 288326
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1eS;->e:Z

    .line 288327
    new-instance v0, LX/1eT;

    invoke-direct {v0, p0, p1, p3}, LX/1eT;-><init>(LX/1eS;LX/1cQ;LX/1cW;)V

    .line 288328
    new-instance v1, LX/1eV;

    iget-object v2, p1, LX/1cQ;->b:Ljava/util/concurrent/Executor;

    iget-object v3, p0, LX/1eS;->d:LX/1bZ;

    iget v3, v3, LX/1bZ;->a:I

    invoke-direct {v1, v2, v0, v3}, LX/1eV;-><init>(Ljava/util/concurrent/Executor;LX/1eU;I)V

    iput-object v1, p0, LX/1eS;->f:LX/1eV;

    .line 288329
    iget-object v0, p0, LX/1eS;->a:LX/1cW;

    new-instance v1, LX/1eX;

    invoke-direct {v1, p0, p1, p4}, LX/1eX;-><init>(LX/1eS;LX/1cQ;Z)V

    invoke-virtual {v0, v1}, LX/1cW;->a(LX/1cg;)V

    .line 288330
    return-void
.end method

.method private static a(LX/1eS;LX/1ln;JLX/1lk;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 6
    .param p0    # LX/1eS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ln;",
            "J",
            "Lcom/facebook/imagepipeline/image/QualityInfo;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288331
    iget-object v0, p0, LX/1eS;->c:LX/1BV;

    iget-object v1, p0, LX/1eS;->a:LX/1cW;

    .line 288332
    iget-object v2, v1, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v2

    .line 288333
    invoke-interface {v0, v1}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288334
    const/4 v0, 0x0

    .line 288335
    :goto_0
    return-object v0

    .line 288336
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 288337
    iget-boolean v1, p4, LX/1lk;->c:Z

    move v1, v1

    .line 288338
    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    .line 288339
    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 288340
    instance-of v3, p1, LX/1ll;

    if-eqz v3, :cond_1

    .line 288341
    check-cast p1, LX/1ll;

    invoke-virtual {p1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 288342
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 288343
    new-instance v4, Ljava/util/HashMap;

    const/16 v5, 0x8

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 288344
    const-string v5, "bitmapSize"

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288345
    const-string v3, "queueTime"

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288346
    const-string v0, "hasGoodQuality"

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288347
    const-string v0, "isFinal"

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288348
    const-string v0, "encodedImageSize"

    invoke-interface {v4, v0, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288349
    const-string v0, "imageFormat"

    invoke-interface {v4, v0, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288350
    const-string v0, "requestedImageSize"

    invoke-interface {v4, v0, p8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288351
    const-string v0, "sampleSize"

    invoke-interface {v4, v0, p9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288352
    invoke-static {v4}, LX/2oC;->a(Ljava/util/Map;)LX/2oC;

    move-result-object v0

    goto :goto_0

    .line 288353
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    const/4 v4, 0x7

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 288354
    const-string v4, "queueTime"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288355
    const-string v0, "hasGoodQuality"

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288356
    const-string v0, "isFinal"

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288357
    const-string v0, "encodedImageSize"

    invoke-interface {v3, v0, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288358
    const-string v0, "imageFormat"

    invoke-interface {v3, v0, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288359
    const-string v0, "requestedImageSize"

    invoke-interface {v3, v0, p8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288360
    const-string v0, "sampleSize"

    invoke-interface {v3, v0, p9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288361
    invoke-static {v3}, LX/2oC;->a(Ljava/util/Map;)LX/2oC;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static a(LX/1eS;LX/1ln;Z)V
    .locals 2

    .prologue
    .line 288416
    invoke-static {p1}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v1

    .line 288417
    :try_start_0
    invoke-static {p0, p2}, LX/1eS;->a(LX/1eS;Z)V

    .line 288418
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288419
    invoke-virtual {v0, v1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288420
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 288421
    return-void

    .line 288422
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method private static a(LX/1eS;Z)V
    .locals 2

    .prologue
    .line 288362
    monitor-enter p0

    .line 288363
    if-eqz p1, :cond_0

    :try_start_0
    iget-boolean v0, p0, LX/1eS;->e:Z

    if-eqz v0, :cond_1

    .line 288364
    :cond_0
    monitor-exit p0

    .line 288365
    :goto_0
    return-void

    .line 288366
    :cond_1
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288367
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/1cd;->b(F)V

    .line 288368
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1eS;->e:Z

    .line 288369
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288370
    iget-object v0, p0, LX/1eS;->f:LX/1eV;

    invoke-virtual {v0}, LX/1eV;->a()V

    goto :goto_0

    .line 288371
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(LX/1eS;LX/1FL;Z)V
    .locals 11

    .prologue
    .line 288372
    invoke-static {p0}, LX/1eS;->e(LX/1eS;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 288373
    :cond_0
    :goto_0
    return-void

    .line 288374
    :cond_1
    iget-object v0, p1, LX/1FL;->c:LX/1lW;

    move-object v0, v0

    .line 288375
    if-eqz v0, :cond_2

    .line 288376
    iget-object v1, v0, LX/1lW;->c:Ljava/lang/String;

    move-object v6, v1

    .line 288377
    :goto_1
    if-eqz p1, :cond_3

    .line 288378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 288379
    iget v1, p1, LX/1FL;->e:I

    move v1, v1

    .line 288380
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 288381
    iget v1, p1, LX/1FL;->f:I

    move v1, v1

    .line 288382
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 288383
    iget v0, p1, LX/1FL;->g:I

    move v0, v0

    .line 288384
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 288385
    :goto_2
    iget-object v0, p0, LX/1eS;->a:LX/1cW;

    .line 288386
    iget-object v1, v0, LX/1cW;->a:LX/1bf;

    move-object v0, v1

    .line 288387
    iget-object v1, v0, LX/1bf;->h:LX/1o9;

    move-object v0, v1

    .line 288388
    if-eqz v0, :cond_4

    .line 288389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v0, LX/1o9;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, LX/1o9;->b:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 288390
    :goto_3
    :try_start_0
    iget-object v0, p0, LX/1eS;->f:LX/1eV;

    invoke-virtual {v0}, LX/1eV;->c()J

    move-result-wide v2

    .line 288391
    if-eqz p2, :cond_5

    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v0

    .line 288392
    :goto_4
    if-eqz p2, :cond_6

    sget-object v4, LX/1lk;->a:LX/1lk;

    .line 288393
    :goto_5
    iget-object v1, p0, LX/1eS;->c:LX/1BV;

    iget-object v5, p0, LX/1eS;->a:LX/1cW;

    .line 288394
    iget-object v10, v5, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v10

    .line 288395
    const-string v10, "DecodeProducer"

    invoke-interface {v1, v5, v10}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288396
    :try_start_1
    iget-object v1, p0, LX/1eS;->b:LX/1cQ;

    iget-object v1, v1, LX/1cQ;->c:LX/1GL;

    iget-object v5, p0, LX/1eS;->d:LX/1bZ;

    invoke-interface {v1, p1, v0, v4, v5}, LX/1GL;->a(LX/1FL;ILX/1lk;LX/1bZ;)LX/1ln;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move-object v0, p0

    move v5, p2

    .line 288397
    :try_start_2
    invoke-static/range {v0 .. v9}, LX/1eS;->a(LX/1eS;LX/1ln;JLX/1lk;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 288398
    iget-object v2, p0, LX/1eS;->c:LX/1BV;

    iget-object v3, p0, LX/1eS;->a:LX/1cW;

    .line 288399
    iget-object v4, v3, LX/1cW;->b:Ljava/lang/String;

    move-object v3, v4

    .line 288400
    const-string v4, "DecodeProducer"

    invoke-interface {v2, v3, v4, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 288401
    invoke-static {p0, v1, p2}, LX/1eS;->a(LX/1eS;LX/1ln;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 288402
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    goto/16 :goto_0

    .line 288403
    :cond_2
    const-string v6, "unknown"

    goto/16 :goto_1

    .line 288404
    :cond_3
    const-string v7, "unknown"

    .line 288405
    const-string v9, "unknown"

    goto :goto_2

    .line 288406
    :cond_4
    const-string v8, "unknown"

    goto :goto_3

    .line 288407
    :cond_5
    :try_start_3
    invoke-virtual {p0, p1}, LX/1eS;->a(LX/1FL;)I

    move-result v0

    goto :goto_4

    .line 288408
    :cond_6
    invoke-virtual {p0}, LX/1eS;->c()LX/1lk;

    move-result-object v4

    goto :goto_5

    .line 288409
    :catch_0
    move-exception v0

    move-object v10, v0

    .line 288410
    const/4 v1, 0x0

    move-object v0, p0

    move v5, p2

    invoke-static/range {v0 .. v9}, LX/1eS;->a(LX/1eS;LX/1ln;JLX/1lk;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 288411
    iget-object v1, p0, LX/1eS;->c:LX/1BV;

    iget-object v2, p0, LX/1eS;->a:LX/1cW;

    .line 288412
    iget-object v3, v2, LX/1cW;->b:Ljava/lang/String;

    move-object v2, v3

    .line 288413
    const-string v3, "DecodeProducer"

    invoke-interface {v1, v2, v3, v10, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    .line 288414
    invoke-static {p0, v10}, LX/1eS;->c(LX/1eS;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 288415
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    throw v0
.end method

.method private static c(LX/1eS;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 288313
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1eS;->a(LX/1eS;Z)V

    .line 288314
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288315
    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 288316
    return-void
.end method

.method private static declared-synchronized e(LX/1eS;)Z
    .locals 1

    .prologue
    .line 288317
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1eS;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/1eS;)V
    .locals 1

    .prologue
    .line 288309
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1eS;->a(LX/1eS;Z)V

    .line 288310
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 288311
    invoke-virtual {v0}, LX/1cd;->b()V

    .line 288312
    return-void
.end method


# virtual methods
.method public abstract a(LX/1FL;)I
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 288307
    invoke-static {p0}, LX/1eS;->f(LX/1eS;)V

    .line 288308
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 288305
    const v0, 0x3f7d70a4    # 0.99f

    mul-float/2addr v0, p1

    invoke-super {p0, v0}, LX/1eP;->a(F)V

    .line 288306
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 288298
    check-cast p1, LX/1FL;

    .line 288299
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/1FL;->e(LX/1FL;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 288300
    new-instance v0, LX/1qd;

    const-string v1, "Encoded image is not valid."

    invoke-direct {v0, v1}, LX/1qd;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, LX/1eS;->c(LX/1eS;Ljava/lang/Throwable;)V

    .line 288301
    :cond_0
    :goto_0
    return-void

    .line 288302
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/1eS;->a(LX/1FL;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288303
    if-nez p2, :cond_2

    iget-object v0, p0, LX/1eS;->a:LX/1cW;

    invoke-virtual {v0}, LX/1cW;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288304
    :cond_2
    iget-object v0, p0, LX/1eS;->f:LX/1eV;

    invoke-virtual {v0}, LX/1eV;->b()Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 288296
    invoke-static {p0, p1}, LX/1eS;->c(LX/1eS;Ljava/lang/Throwable;)V

    .line 288297
    return-void
.end method

.method public a(LX/1FL;Z)Z
    .locals 1

    .prologue
    .line 288295
    iget-object v0, p0, LX/1eS;->f:LX/1eV;

    invoke-virtual {v0, p1, p2}, LX/1eV;->a(LX/1FL;Z)Z

    move-result v0

    return v0
.end method

.method public abstract c()LX/1lk;
.end method
