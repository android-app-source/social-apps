.class public final LX/1R7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[I

.field public b:I


# direct methods
.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 245528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245529
    new-array v0, p1, [I

    iput-object v0, p0, LX/1R7;->a:[I

    .line 245530
    const/4 v0, 0x0

    iput v0, p0, LX/1R7;->b:I

    .line 245531
    return-void
.end method

.method public static a(I)LX/1R7;
    .locals 1

    .prologue
    .line 245553
    new-instance v0, LX/1R7;

    invoke-direct {v0, p0}, LX/1R7;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final b(I)V
    .locals 10

    .prologue
    .line 245545
    const/4 v9, 0x0

    .line 245546
    iget v3, p0, LX/1R7;->b:I

    iget-object v4, p0, LX/1R7;->a:[I

    array-length v4, v4

    if-lt v3, v4, :cond_0

    .line 245547
    iget v3, p0, LX/1R7;->b:I

    add-int/lit8 v3, v3, 0x1

    iget v4, p0, LX/1R7;->b:I

    int-to-double v5, v4

    const-wide v7, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v5, v7

    double-to-int v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 245548
    new-array v3, v3, [I

    .line 245549
    iget-object v4, p0, LX/1R7;->a:[I

    iget v5, p0, LX/1R7;->b:I

    invoke-static {v4, v9, v3, v9, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 245550
    iput-object v3, p0, LX/1R7;->a:[I

    .line 245551
    :cond_0
    iget-object v0, p0, LX/1R7;->a:[I

    iget v1, p0, LX/1R7;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/1R7;->b:I

    aput p1, v0, v1

    .line 245552
    return-void
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 245542
    if-ltz p1, :cond_0

    iget v0, p0, LX/1R7;->b:I

    if-lt p1, v0, :cond_1

    .line 245543
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds. Collection length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/1R7;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245544
    :cond_1
    iget-object v0, p0, LX/1R7;->a:[I

    aget v0, v0, p1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 245540
    const/4 v0, 0x0

    iput v0, p0, LX/1R7;->b:I

    .line 245541
    return-void
.end method

.method public final d(I)I
    .locals 2

    .prologue
    .line 245535
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/1R7;->b:I

    if-ge v0, v1, :cond_1

    .line 245536
    iget-object v1, p0, LX/1R7;->a:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 245537
    :goto_1
    return v0

    .line 245538
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245539
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final d()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 245532
    iget v0, p0, LX/1R7;->b:I

    new-array v0, v0, [I

    .line 245533
    iget-object v1, p0, LX/1R7;->a:[I

    iget v2, p0, LX/1R7;->b:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 245534
    return-object v0
.end method
