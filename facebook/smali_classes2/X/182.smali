.class public LX/182;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 203626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203734
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203735
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203736
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203737
    :goto_0
    return-object p0

    .line 203738
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 203739
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 203740
    invoke-virtual {p0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 203741
    invoke-static {v1, p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 203742
    if-eqz v1, :cond_1

    move-object p0, v1

    .line 203743
    goto :goto_0

    .line 203744
    :cond_1
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 203745
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203746
    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0, p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 203747
    if-eqz v0, :cond_2

    move-object p0, v0

    .line 203748
    goto :goto_0

    .line 203749
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 203750
    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203730
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 203731
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-nez v1, :cond_1

    .line 203732
    :cond_0
    const/4 v0, 0x0

    .line 203733
    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203726
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 203727
    if-eqz v0, :cond_0

    .line 203728
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 203729
    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203722
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 203723
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 203724
    :cond_0
    const/4 v0, 0x0

    .line 203725
    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    .line 203719
    invoke-static {p0}, LX/182;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 203720
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 203721
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public static e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203711
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 203712
    if-eqz v0, :cond_0

    .line 203713
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 203714
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 203715
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 203716
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object p0, v0

    .line 203717
    goto :goto_0

    .line 203718
    :cond_0
    return-object p0
.end method

.method public static f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .prologue
    .line 203707
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 203708
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_0

    .line 203709
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 203710
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStorySet;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203703
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 203704
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-nez v1, :cond_1

    .line 203705
    :cond_0
    const/4 v0, 0x0

    .line 203706
    :goto_0
    return-object v0

    :cond_1
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    goto :goto_0
.end method

.method public static i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 203701
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203702
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public static k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203696
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203697
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203698
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203699
    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    goto :goto_0

    .line 203700
    :cond_0
    return-object p0
.end method

.method public static l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 203694
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 203695
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/182;->m(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 203690
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203691
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203692
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 203693
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .param p0    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 203684
    if-nez p0, :cond_0

    .line 203685
    const/4 v0, 0x0

    .line 203686
    :goto_0
    return v0

    .line 203687
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203688
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203689
    invoke-static {v0}, LX/16y;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 203673
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 203674
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203675
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    .line 203676
    if-eqz v0, :cond_0

    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 203677
    :goto_0
    if-nez v3, :cond_1

    .line 203678
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 203679
    goto :goto_0

    .line 203680
    :cond_1
    invoke-static {p0}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 203681
    invoke-static {v4}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    .line 203682
    goto :goto_1

    .line 203683
    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static q(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 203637
    new-instance v3, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/162;-><init>(LX/0mC;)V

    .line 203638
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203639
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 203640
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 203641
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 203642
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    .line 203643
    instance-of v6, v1, LX/16h;

    if-eqz v6, :cond_1

    .line 203644
    check-cast v1, LX/16h;

    invoke-interface {v1}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    .line 203645
    if-eqz v1, :cond_1

    .line 203646
    invoke-virtual {v3, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 203647
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 203648
    :cond_2
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 203649
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 203650
    if-nez v5, :cond_4

    move-object v1, v4

    .line 203651
    :goto_1
    move-object v1, v1

    .line 203652
    if-eqz v1, :cond_3

    .line 203653
    invoke-virtual {v3, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 203654
    :cond_3
    invoke-static {v0, v3}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 203655
    return-object v3

    .line 203656
    :cond_4
    invoke-static {v5}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v6

    .line 203657
    invoke-static {v5}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v7

    if-le v7, v2, :cond_5

    sget-object v7, LX/16z;->g:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    if-ne v6, v7, :cond_6

    :cond_5
    move-object v1, v4

    .line 203658
    goto :goto_1

    .line 203659
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->COMPACTNESS:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v7, v8}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v5

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;->POST_CHANNEL:Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {v5, v7}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 203660
    :goto_2
    if-nez v2, :cond_8

    move-object v1, v4

    .line 203661
    goto :goto_1

    :cond_7
    move v2, v1

    .line 203662
    goto :goto_2

    .line 203663
    :cond_8
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v5

    .line 203664
    if-eqz v5, :cond_9

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    move-object v1, v4

    .line 203665
    goto :goto_1

    .line 203666
    :cond_a
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v6

    move v2, v1

    .line 203667
    :goto_3
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_c

    .line 203668
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 203669
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 203670
    invoke-virtual {v6}, LX/0lC;->e()LX/0m9;

    move-result-object v1

    const-string v4, "scroll_index"

    invoke-virtual {v1, v4, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v1

    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 203671
    :cond_b
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_c
    move-object v1, v4

    .line 203672
    goto :goto_1
.end method

.method public static s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 203627
    :goto_0
    if-eqz p0, :cond_1

    .line 203628
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203629
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 203630
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 203631
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203632
    const/4 v0, 0x1

    .line 203633
    :goto_1
    return v0

    .line 203634
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object p0, v0

    .line 203635
    goto :goto_0

    .line 203636
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
