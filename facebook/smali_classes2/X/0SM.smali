.class public abstract LX/0SM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<TT;>;",
        "LX/0Or",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;

.field private final b:LX/0R6;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 1

    .prologue
    .line 61028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61029
    iput-object p1, p0, LX/0SM;->a:LX/0QB;

    .line 61030
    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    iput-object v0, p0, LX/0SM;->b:LX/0R6;

    .line 61031
    return-void
.end method


# virtual methods
.method public abstract a(LX/0QB;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")TT;"
        }
    .end annotation
.end method

.method public final get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 61032
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0SM;->a(LX/0QB;)Ljava/lang/Object;

    move-result-object v0

    .line 61033
    if-eqz v0, :cond_0

    .line 61034
    :goto_0
    return-object v0

    .line 61035
    :cond_0
    iget-object v0, p0, LX/0SM;->a:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    .line 61036
    iget-object v1, p0, LX/0SM;->b:LX/0R6;

    invoke-interface {v1}, LX/0R7;->a()Ljava/lang/Object;

    move-result-object v1

    .line 61037
    :try_start_0
    invoke-virtual {p0, v0}, LX/0SM;->a(LX/0QB;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 61038
    iget-object v2, p0, LX/0SM;->b:LX/0R6;

    invoke-interface {v2, v1}, LX/0R7;->a(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/0SM;->b:LX/0R6;

    invoke-interface {v2, v1}, LX/0R7;->a(Ljava/lang/Object;)V

    throw v0
.end method
