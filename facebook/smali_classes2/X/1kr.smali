.class public final LX/1kr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 310352
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPromptType;->MOVIE_EVERGREEN:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    sput-object v0, LX/1kr;->a:LX/0Px;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 310353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
