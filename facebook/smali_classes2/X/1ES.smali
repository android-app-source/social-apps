.class public LX/1ES;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/1ES;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/1ET;

.field private final d:LX/0SG;

.field public final e:LX/12x;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 219949
    const-class v0, LX/1ES;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1ES;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1ET;LX/0SG;LX/12x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219904
    iput-object p1, p0, LX/1ES;->b:Landroid/content/Context;

    .line 219905
    iput-object p2, p0, LX/1ES;->c:LX/1ET;

    .line 219906
    iput-object p3, p0, LX/1ES;->d:LX/0SG;

    .line 219907
    iput-object p4, p0, LX/1ES;->e:LX/12x;

    .line 219908
    return-void
.end method

.method public static a(LX/0QB;)LX/1ES;
    .locals 7

    .prologue
    .line 219909
    sget-object v0, LX/1ES;->f:LX/1ES;

    if-nez v0, :cond_1

    .line 219910
    const-class v1, LX/1ES;

    monitor-enter v1

    .line 219911
    :try_start_0
    sget-object v0, LX/1ES;->f:LX/1ES;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 219912
    if-eqz v2, :cond_0

    .line 219913
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 219914
    new-instance p0, LX/1ES;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1ET;->a(LX/0QB;)LX/1ET;

    move-result-object v4

    check-cast v4, LX/1ET;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v6

    check-cast v6, LX/12x;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1ES;-><init>(Landroid/content/Context;LX/1ET;LX/0SG;LX/12x;)V

    .line 219915
    move-object v0, p0

    .line 219916
    sput-object v0, LX/1ES;->f:LX/1ES;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219917
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 219918
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 219919
    :cond_1
    sget-object v0, LX/1ES;->f:LX/1ES;

    return-object v0

    .line 219920
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 219921
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1ES;Ljava/lang/Class;Z)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/delayedworker/DelayedWorker;",
            ">;Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 219922
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1ES;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/delayedworker/DelayedWorkerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 219923
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/facebook/delayedworker/DelayedWorkerService;->a(Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 219924
    return-object v0
.end method

.method private a(Ljava/lang/Class;JJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/delayedworker/DelayedWorker;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 219925
    iget-object v0, p0, LX/1ES;->c:LX/1ET;

    .line 219926
    sget-object v4, LX/1ET;->a:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    .line 219927
    iget-object v5, v0, LX/1ET;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v6, 0x0

    invoke-interface {v5, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    move-wide v0, v4

    .line 219928
    sub-long v2, p2, v0

    cmp-long v2, v2, p4

    if-lez v2, :cond_0

    .line 219929
    iget-object v2, p0, LX/1ES;->c:LX/1ET;

    .line 219930
    sget-object v3, LX/1ET;->a:LX/0Tn;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    .line 219931
    iget-object v4, v2, LX/1ET;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v3, p2, p3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 219932
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 219933
    iget-object v0, p0, LX/1ES;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, LX/1ES;->a(LX/1ES;Ljava/lang/Class;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 219934
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/delayedworker/DelayedWorker;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 219935
    if-nez p1, :cond_0

    .line 219936
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DelayedWorkerClass can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219937
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_1

    .line 219938
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Delay can\'t be a negative number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219939
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 219940
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p2, p3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 219941
    iget-object v0, p0, LX/1ES;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    move-object v0, p0

    move-object v1, p1

    .line 219942
    invoke-direct/range {v0 .. v5}, LX/1ES;->a(Ljava/lang/Class;JJ)V

    .line 219943
    add-long v0, v2, v4

    const/4 v4, 0x1

    .line 219944
    iget-object v2, p0, LX/1ES;->e:LX/12x;

    const/4 p2, 0x0

    .line 219945
    invoke-static {p0, p1, v4}, LX/1ES;->a(LX/1ES;Ljava/lang/Class;Z)Landroid/content/Intent;

    move-result-object v3

    .line 219946
    iget-object v5, p0, LX/1ES;->b:Landroid/content/Context;

    invoke-static {v5, p2, v3, p2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object v3, v3

    .line 219947
    invoke-virtual {v2, v4, v0, v1, v3}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 219948
    return-void
.end method
