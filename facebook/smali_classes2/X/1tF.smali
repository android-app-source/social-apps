.class public final LX/1tF;
.super LX/1tG;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/mqttlite/MqttService;


# direct methods
.method public constructor <init>(Lcom/facebook/mqttlite/MqttService;)V
    .locals 0

    .prologue
    .line 336211
    iput-object p1, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    invoke-direct {p0}, LX/1tG;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[BILX/76B;)I
    .locals 3

    .prologue
    .line 336205
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336206
    invoke-virtual {v0}, LX/052;->a()V

    .line 336207
    invoke-static {p3}, LX/0AL;->fromInt(I)LX/0AL;

    move-result-object v1

    .line 336208
    :try_start_0
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v2, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    if-eqz p4, :cond_0

    new-instance v0, LX/6mo;

    invoke-direct {v0, p4}, LX/6mo;-><init>(LX/76B;)V

    :goto_0
    invoke-virtual {v2, p1, p2, v1, v0}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 336209
    :catch_0
    move-exception v0

    .line 336210
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, LX/0BK;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(LX/1Me;)V
    .locals 1

    .prologue
    .line 336201
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336202
    invoke-virtual {v0}, LX/052;->a()V

    .line 336203
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->W:LX/1vP;

    invoke-virtual {v0, p1}, LX/1vP;->a(LX/1Me;)V

    .line 336204
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 336195
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336196
    invoke-virtual {v0}, LX/052;->a()V

    .line 336197
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 336198
    iget-object p0, v0, LX/1tA;->x:LX/1tj;

    .line 336199
    iget-object v0, p0, LX/1tj;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mp;

    .line 336200
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 12

    .prologue
    .line 336188
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336189
    invoke-virtual {v0}, LX/052;->a()V

    .line 336190
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 336191
    iget-object v1, v0, LX/1tA;->x:LX/1tj;

    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 336192
    new-instance v2, LX/6mp;

    sget-object v5, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    const-wide/16 v8, -0x1

    move-object v3, p2

    move-object v4, p3

    move v10, v7

    move-object v11, v6

    invoke-direct/range {v2 .. v11}, LX/6mp;-><init>(Ljava/lang/String;[BLX/0AL;LX/0AM;IJILjava/lang/String;)V

    .line 336193
    iget-object v3, v1, LX/1tj;->a:Ljava/util/Map;

    invoke-interface {v3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336194
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336177
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336178
    invoke-virtual {v0}, LX/052;->a()V

    .line 336179
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 336180
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;

    .line 336181
    iget-object v3, v0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    move-object v0, v3

    .line 336182
    new-instance v3, LX/0AF;

    .line 336183
    iget-object v4, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    move-object v4, v4

    .line 336184
    iget p1, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    move v0, p1

    .line 336185
    invoke-direct {v3, v4, v0}, LX/0AF;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 336186
    :cond_0
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v0, v1}, LX/1tA;->g(Ljava/util/List;)V

    .line 336187
    return-void
.end method

.method public final a(Ljava/util/List;LX/768;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/SubscribeTopic;",
            ">;",
            "LX/768;",
            ")V"
        }
    .end annotation

    .prologue
    .line 336163
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336164
    invoke-virtual {v0}, LX/052;->a()V

    .line 336165
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336166
    :cond_0
    :goto_0
    return-void

    .line 336167
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 336168
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    .line 336169
    new-instance v3, LX/0AF;

    .line 336170
    iget-object v4, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    move-object v4, v4

    .line 336171
    iget p1, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    move v0, p1

    .line 336172
    invoke-direct {v3, v4, v0}, LX/0AF;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 336173
    :cond_2
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 336174
    const-string v2, "FbnsConnectionManager"

    const-string v3, "send/subscribe; topics=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v4, p0

    invoke-static {v2, v3, v4}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336175
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/056;->a(Ljava/util/List;Ljava/util/List;)V

    .line 336176
    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336145
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336146
    invoke-virtual {v0}, LX/052;->a()V

    .line 336147
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v0, p1, v1, v1}, LX/1tA;->a(ZLjava/util/List;Ljava/util/List;)V

    .line 336148
    return-void
.end method

.method public final a(ZLjava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/SubscribeTopic;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336159
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336160
    invoke-virtual {v0}, LX/052;->a()V

    .line 336161
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-static {p2}, Lcom/facebook/mqttlite/MqttService;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p3}, LX/1tA;->a(ZLjava/util/List;Ljava/util/List;)V

    .line 336162
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 336156
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336157
    invoke-virtual {v0}, LX/052;->a()V

    .line 336158
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v0}, LX/056;->g()Z

    move-result v0

    return v0
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    .line 336212
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336213
    invoke-virtual {v0}, LX/052;->a()V

    .line 336214
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    const/4 v1, 0x0

    .line 336215
    :try_start_0
    iget-object v2, v0, LX/056;->b:LX/072;

    .line 336216
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/072;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 336217
    :cond_0
    invoke-virtual {v0}, LX/056;->j()V

    .line 336218
    iget-object v2, v0, LX/056;->j:LX/05i;

    sget-object v3, LX/06f;->CONNECT_NOW:LX/06f;

    .line 336219
    iput-object v3, v2, LX/05i;->n:LX/06f;

    .line 336220
    iget-object v2, v0, LX/056;->t:LX/06I;

    invoke-virtual {v2}, LX/06I;->b()Ljava/util/concurrent/Future;

    move-result-object v2

    move-object v2, v2

    .line 336221
    if-eqz v2, :cond_1

    .line 336222
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, 0x69e02367

    invoke-static {v2, p1, p2, v3, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    .line 336223
    :cond_1
    iget-object v2, v0, LX/056;->b:LX/072;

    .line 336224
    if-nez v2, :cond_2

    .line 336225
    const-string v2, "FbnsConnectionManager"

    const-string v3, "connection/client/failed_to_init"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3

    .line 336226
    :goto_0
    move v0, v1

    .line 336227
    return v0

    .line 336228
    :cond_2
    :try_start_1
    invoke-virtual {v2}, LX/072;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 336229
    const/4 v1, 0x1

    goto :goto_0

    .line 336230
    :cond_3
    invoke-virtual {v2, p1, p2}, LX/072;->a(J)V

    .line 336231
    invoke-virtual {v2}, LX/072;->d()Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v1

    goto :goto_0

    .line 336232
    :catch_0
    const-string v2, "FbnsConnectionManager"

    const-string v3, "exception/connect_interrupted"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336233
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 336234
    :catch_1
    move-exception v2

    .line 336235
    const-string v3, "FbnsConnectionManager"

    const-string v4, "exception/execution_exception"

    new-array p0, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, p0}, LX/05D;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 336236
    :catch_2
    move-exception v2

    .line 336237
    const-string v3, "FbnsConnectionManager"

    const-string v4, "exception/cancellation"

    new-array p0, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, p0}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 336238
    :catch_3
    move-exception v2

    .line 336239
    const-string v3, "FbnsConnectionManager"

    const-string v4, "exception/timeout"

    new-array p0, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, p0}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BJLX/76B;J)Z
    .locals 9

    .prologue
    .line 336155
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v8}, LX/1tF;->a(Ljava/lang/String;[BJLX/76B;JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;[BJLX/76B;JLjava/lang/String;)Z
    .locals 9

    .prologue
    .line 336149
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    invoke-static {v0}, Lcom/facebook/mqttlite/MqttService;->h(Lcom/facebook/mqttlite/MqttService;)V

    .line 336150
    :try_start_0
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    if-eqz p5, :cond_0

    new-instance v5, LX/6mo;

    invoke-direct {v5, p5}, LX/6mo;-><init>(LX/76B;)V

    :goto_0
    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v6, p6

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, LX/056;->a(Ljava/lang/String;[BJLX/0AM;JLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v0

    return v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 336151
    :catch_0
    move-exception v0

    .line 336152
    :goto_1
    sget-object v1, Lcom/facebook/mqttlite/MqttService;->Y:Ljava/lang/Class;

    const-string v2, "send/publish/exception; topic=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336153
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 336154
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public final b(LX/1Me;)V
    .locals 1

    .prologue
    .line 336141
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336142
    invoke-virtual {v0}, LX/052;->a()V

    .line 336143
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->W:LX/1vP;

    invoke-virtual {v0, p1}, LX/1vP;->b(LX/1Me;)V

    .line 336144
    return-void
.end method

.method public final b(Ljava/util/List;LX/768;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/768;",
            ")V"
        }
    .end annotation

    .prologue
    .line 336135
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336136
    invoke-virtual {v0}, LX/052;->a()V

    .line 336137
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 336138
    const-string v1, "FbnsConnectionManager"

    const-string v2, "send/unsubscribe; topics=%s"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p1, p0, p2

    invoke-static {v1, v2, p0}, LX/05D;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336139
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, LX/056;->a(Ljava/util/List;Ljava/util/List;)V

    .line 336140
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 336132
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336133
    invoke-virtual {v0}, LX/052;->a()V

    .line 336134
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v0}, LX/056;->h()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336129
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336130
    invoke-virtual {v0}, LX/052;->a()V

    .line 336131
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    invoke-static {v0}, Lcom/facebook/mqttlite/MqttService;->s(Lcom/facebook/mqttlite/MqttService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 336121
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336122
    invoke-virtual {v0}, LX/052;->a()V

    .line 336123
    :try_start_0
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v0}, LX/056;->a()J

    move-result-wide v0

    .line 336124
    iget-object v2, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v2, v2, LX/051;->h:LX/05i;

    invoke-virtual {v2, v0, v1}, LX/05i;->a(J)LX/0Ha;

    move-result-object v0

    .line 336125
    invoke-virtual {v0}, LX/0Ha;->a()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 336126
    :goto_0
    return-object v0

    .line 336127
    :catch_0
    move-exception v0

    .line 336128
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336118
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336119
    invoke-virtual {v0}, LX/052;->a()V

    .line 336120
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->ac:LX/1tC;

    invoke-virtual {v0}, LX/1tC;->a()LX/1tE;

    move-result-object v0

    invoke-virtual {v0}, LX/1tE;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;
    .locals 2

    .prologue
    .line 336115
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    .line 336116
    invoke-virtual {v0}, LX/052;->a()V

    .line 336117
    iget-object v0, p0, LX/1tF;->a:Lcom/facebook/mqttlite/MqttService;

    invoke-virtual {p0}, LX/1tF;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/mqttlite/MqttService;->a(Lcom/facebook/mqttlite/MqttService;Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v0

    return-object v0
.end method
