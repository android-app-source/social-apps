.class public LX/0no;
.super LX/0na;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136604
    invoke-direct {p0}, LX/0na;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/2Ax;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136605
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 136606
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ax;

    .line 136607
    const-class v2, LX/0am;

    invoke-virtual {v0}, LX/2Ax;->h()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136608
    new-instance v2, LX/4sI;

    invoke-direct {v2, v0}, LX/4sI;-><init>(LX/2Ax;)V

    invoke-interface {p1, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 136609
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136610
    :cond_1
    return-object p1
.end method
