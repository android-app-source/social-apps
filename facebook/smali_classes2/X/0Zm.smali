.class public abstract LX/0Zm;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field public final d:LX/0XZ;

.field private final e:LX/0Uh;

.field private final f:LX/0aB;

.field private g:LX/17O;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83838
    const-class v0, LX/0Zm;

    sput-object v0, LX/0Zm;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Uh;LX/0a8;ILX/0XZ;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/gk/store/GatekeeperListeners;",
            "I",
            "LX/0XZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83840
    iput-object p1, p0, LX/0Zm;->b:LX/0Or;

    .line 83841
    iput-object p2, p0, LX/0Zm;->e:LX/0Uh;

    .line 83842
    iput p4, p0, LX/0Zm;->c:I

    .line 83843
    iput-object p5, p0, LX/0Zm;->d:LX/0XZ;

    .line 83844
    new-instance v0, LX/0aA;

    invoke-direct {v0, p0}, LX/0aA;-><init>(LX/0Zm;)V

    iput-object v0, p0, LX/0Zm;->f:LX/0aB;

    .line 83845
    iget-object v0, p0, LX/0Zm;->f:LX/0aB;

    invoke-virtual {p3, v0, p4}, LX/0a8;->a(LX/0aB;I)V

    .line 83846
    return-void
.end method

.method public static d(LX/0Zm;)V
    .locals 3

    .prologue
    .line 83847
    iget-object v0, p0, LX/0Zm;->e:LX/0Uh;

    iget v1, p0, LX/0Zm;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83848
    sget-object v0, LX/17O;->CORE_AND_SAMPLED:LX/17O;

    iput-object v0, p0, LX/0Zm;->g:LX/17O;

    .line 83849
    :goto_0
    return-void

    .line 83850
    :cond_0
    sget-object v0, LX/17O;->CORE:LX/17O;

    iput-object v0, p0, LX/0Zm;->g:LX/17O;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/17O;
    .locals 1

    .prologue
    .line 83851
    iget-object v0, p0, LX/0Zm;->g:LX/17O;

    if-nez v0, :cond_0

    .line 83852
    invoke-static {p0}, LX/0Zm;->d(LX/0Zm;)V

    .line 83853
    :cond_0
    iget-object v0, p0, LX/0Zm;->g:LX/17O;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 83854
    iget-object v0, p0, LX/0Zm;->d:LX/0XZ;

    .line 83855
    iget-object p0, v0, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {p0, p1}, LX/0Xc;->a(Ljava/lang/String;)Z

    move-result p0

    move v0, p0

    .line 83856
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 83857
    invoke-static {}, LX/0i9;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/0mr;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83858
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 83859
    :cond_1
    iget-object v1, p0, LX/0Zm;->d:LX/0XZ;

    invoke-virtual {v1, p1}, LX/0XZ;->a(Ljava/lang/String;)LX/03R;

    move-result-object v1

    .line 83860
    sget-object v2, LX/0n8;->a:[I

    invoke-virtual {v1}, LX/03R;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 83861
    if-eqz p2, :cond_2

    .line 83862
    const/4 v0, 0x1

    .line 83863
    :goto_1
    move v0, v0

    .line 83864
    goto :goto_0

    .line 83865
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/0Zm;->d:LX/0XZ;

    .line 83866
    iget-object p0, v0, LX/0XZ;->d:Ljava/util/Random;

    const/16 p2, 0xc8

    invoke-virtual {p0, p2}, Ljava/util/Random;->nextInt(I)I

    move-result p0

    if-nez p0, :cond_3

    const/4 p0, 0x1

    :goto_2
    move v0, p0

    .line 83867
    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 83868
    iget-object v0, p0, LX/0Zm;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 83869
    if-eqz v0, :cond_0

    .line 83870
    iget-boolean p0, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, p0

    .line 83871
    if-eqz v0, :cond_0

    .line 83872
    const/4 v0, 0x1

    .line 83873
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 83874
    iget-object v0, p0, LX/0Zm;->d:LX/0XZ;

    invoke-virtual {v0}, LX/0XZ;->c()LX/0ap;

    move-result-object v0

    .line 83875
    if-eqz v0, :cond_1

    .line 83876
    iget-object v1, v0, LX/0ap;->e:LX/03R;

    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83877
    const-string v1, "perf"

    invoke-static {v0, v1}, LX/0ap;->a(LX/0ap;Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, v0, LX/0ap;->e:LX/03R;

    .line 83878
    :cond_0
    iget-object v1, v0, LX/0ap;->e:LX/03R;

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    move v1, v1

    .line 83879
    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/0ap;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
