.class public LX/1lN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 311494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<*>;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 311559
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 311560
    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 311561
    :goto_0
    iget-object v1, p0, LX/0zO;->n:LX/0w5;

    move-object v1, v1

    .line 311562
    const-class p1, LX/0jS;

    invoke-virtual {v1, p1}, LX/0w5;->a(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_0

    const-class p1, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;

    invoke-virtual {v1, p1}, LX/0w5;->a(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 311563
    if-eqz v1, :cond_1

    .line 311564
    or-int/lit8 v0, v0, 0x2

    .line 311565
    :cond_1
    return v0

    .line 311566
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(LX/15i;Ljava/lang/String;)LX/15i;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 311554
    new-instance v0, LX/15i;

    invoke-virtual {p0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {p0}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v4, 0x1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 311555
    if-eqz p1, :cond_0

    .line 311556
    invoke-virtual {v0, p1}, LX/15i;->a(Ljava/lang/String;)V

    .line 311557
    :cond_0
    invoke-virtual {p0, v0}, LX/15i;->a(LX/15i;)V

    .line 311558
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 311542
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 311543
    instance-of v2, v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v2, :cond_0

    .line 311544
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 311545
    :goto_0
    return-object v0

    .line 311546
    :cond_0
    instance-of v2, v0, Ljava/util/List;

    if-eqz v2, :cond_2

    .line 311547
    check-cast v0, Ljava/util/List;

    .line 311548
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 311549
    goto :goto_0

    .line 311550
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 311551
    instance-of v2, v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v2, :cond_2

    .line 311552
    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 311553
    goto :goto_0
.end method

.method public static a(IILjava/nio/ByteBuffer;LX/0w5;LX/0t8;Ljava/util/Map;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;
    .locals 8
    .param p6    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/nio/ByteBuffer;",
            "LX/0w5;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/nio/ByteBuffer;",
            "Ljava/nio/ByteBuffer;",
            "Lcom/facebook/flatbuffers/MutableFlatBuffer$FlatBufferCorruptionHandler;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 311535
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v1, p2

    move-object v2, p6

    move-object v3, p7

    move-object/from16 v5, p8

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 311536
    const-string v1, "DiskCacheFlattenableHelper.resolveObjectWithConsistency"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 311537
    invoke-static {p1}, LX/1lN;->b(I)Z

    move-result v6

    .line 311538
    invoke-static {p1}, LX/1lN;->a(I)Z

    move-result v7

    .line 311539
    invoke-interface {p5}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 311540
    new-instance v2, LX/JaO;

    invoke-direct {v2}, LX/JaO;-><init>()V

    invoke-static {p1}, LX/1lN;->a(I)Z

    move-result v5

    move v1, p0

    move-object v3, p4

    move-object v4, p5

    invoke-static/range {v0 .. v6}, LX/4VC;->a(LX/15i;ILX/4Ud;LX/0t8;Ljava/util/Map;ZZ)Z

    .line 311541
    :cond_0
    invoke-static {v0, p3, v7, v6}, LX/1lN;->a(LX/15i;LX/0w5;ZZ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/nio/ByteBuffer;LX/0w5;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;
    .locals 9
    .param p3    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311534
    const/4 v0, 0x0

    const/4 v4, 0x0

    sget-object v5, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-static/range {v0 .. v8}, LX/1lN;->a(IILjava/nio/ByteBuffer;LX/0w5;LX/0t8;Ljava/util/Map;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/15i;LX/0w5;ZZ)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 311567
    if-eqz p2, :cond_1

    .line 311568
    if-eqz p3, :cond_0

    .line 311569
    sget-object v0, LX/16Z;->a:LX/16Z;

    .line 311570
    const/4 p1, 0x0

    invoke-static {p0, p1, v0}, LX/4Bz;->a(LX/15i;Ljava/lang/Class;LX/16a;)Ljava/util/List;

    move-result-object p1

    move-object v0, p1

    .line 311571
    :goto_0
    return-object v0

    .line 311572
    :cond_0
    invoke-virtual {p1, p0}, LX/0w5;->a(LX/15i;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 311573
    :cond_1
    if-eqz p3, :cond_2

    .line 311574
    sget-object v0, LX/16Z;->a:LX/16Z;

    invoke-virtual {p0, v0}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    goto :goto_0

    .line 311575
    :cond_2
    invoke-virtual {p1, p0}, LX/0w5;->b(LX/15i;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 311526
    if-nez p1, :cond_0

    .line 311527
    :goto_0
    return-object v0

    .line 311528
    :cond_0
    invoke-static {p0}, LX/1lN;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, LX/16Z;->a:LX/16Z;

    .line 311529
    :cond_1
    invoke-static {p0}, LX/1lN;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311530
    check-cast p1, Ljava/util/List;

    invoke-static {p1, v0}, LX/4Bz;->a(Ljava/util/List;LX/16a;)[B

    move-result-object v0

    .line 311531
    :goto_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 311532
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 311533
    :cond_2
    check-cast p1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v0

    goto :goto_1
.end method

.method public static a([B)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 311514
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 311515
    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 311516
    const/4 v2, 0x0

    const-class v3, Ljava/util/HashMap;

    sget-object v4, LX/4VD;->a:LX/4VD;

    .line 311517
    invoke-static {v0, v1, v2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v5

    .line 311518
    if-nez v5, :cond_0

    const/4 v5, 0x0

    .line 311519
    :goto_0
    move-object v0, v5

    .line 311520
    return-object v0

    .line 311521
    :cond_0
    const/4 v6, 0x0

    invoke-static {v0, v5, v6}, LX/0ah;->g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;

    move-result-object v6

    .line 311522
    const/4 p0, 0x1

    .line 311523
    sget-object v1, LX/4Br;->a:LX/4Br;

    .line 311524
    invoke-static {v0, v5, p0, v1, v4}, LX/0ah;->b(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v1

    move-object v5, v1

    .line 311525
    invoke-static {v3, v6, v5}, LX/0ah;->a(Ljava/lang/Class;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Map;

    move-result-object v5

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 311513
    and-int/lit8 v1, p0, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 311496
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v0

    .line 311497
    if-nez v1, :cond_0

    .line 311498
    :goto_0
    return-object p1

    .line 311499
    :cond_0
    invoke-static {p0, p1}, LX/1lN;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)I

    move-result v0

    .line 311500
    invoke-static {v0, v1}, LX/1lN;->a(ILjava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 311501
    iget-object v2, p0, LX/0zO;->n:LX/0w5;

    move-object v2, v2

    .line 311502
    move-object v4, v3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, LX/1lN;->a(ILjava/nio/ByteBuffer;LX/0w5;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;LX/15j;)Ljava/lang/Object;

    move-result-object v0

    .line 311503
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v1

    .line 311504
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 311505
    move-object v0, v1

    .line 311506
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 311507
    invoke-static {p1}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v1

    .line 311508
    invoke-static {v0}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v2

    .line 311509
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 311510
    invoke-virtual {v1, v2}, LX/15i;->a(LX/15i;)V

    .line 311511
    :cond_1
    move-object p1, v0

    .line 311512
    goto :goto_0
.end method

.method public static b(I)Z
    .locals 2

    .prologue
    .line 311495
    and-int/lit8 v0, p0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
