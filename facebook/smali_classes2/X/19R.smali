.class public LX/19R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/19R;


# instance fields
.field private final a:LX/0Zr;


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207984
    iput-object p1, p0, LX/19R;->a:LX/0Zr;

    .line 207985
    return-void
.end method

.method public static a(LX/0QB;)LX/19R;
    .locals 4

    .prologue
    .line 207986
    sget-object v0, LX/19R;->b:LX/19R;

    if-nez v0, :cond_1

    .line 207987
    const-class v1, LX/19R;

    monitor-enter v1

    .line 207988
    :try_start_0
    sget-object v0, LX/19R;->b:LX/19R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207989
    if-eqz v2, :cond_0

    .line 207990
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 207991
    new-instance p0, LX/19R;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v3

    check-cast v3, LX/0Zr;

    invoke-direct {p0, v3}, LX/19R;-><init>(LX/0Zr;)V

    .line 207992
    move-object v0, p0

    .line 207993
    sput-object v0, LX/19R;->b:LX/19R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207994
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207995
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207996
    :cond_1
    sget-object v0, LX/19R;->b:LX/19R;

    return-object v0

    .line 207997
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/2q9;
    .locals 2

    .prologue
    .line 207999
    new-instance v0, LX/2q9;

    iget-object v1, p0, LX/19R;->a:LX/0Zr;

    invoke-direct {v0, v1}, LX/2q9;-><init>(LX/0Zr;)V

    return-object v0
.end method
