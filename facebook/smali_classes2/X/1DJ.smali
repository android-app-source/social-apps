.class public LX/1DJ;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fm;
.implements LX/1DK;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0g5;

.field private d:Landroid/view/View;

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 217356
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 217357
    const/16 v0, 0x25d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/1DJ;->e:Z

    .line 217358
    return-void
.end method


# virtual methods
.method public final a()LX/0g8;
    .locals 1

    .prologue
    .line 217359
    iget-object v0, p0, LX/1DJ;->c:LX/0g5;

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 217360
    iput-object p1, p0, LX/1DJ;->d:Landroid/view/View;

    .line 217361
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 217362
    check-cast p1, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p1}, Lcom/facebook/widget/CustomFrameLayout;->asViewGroup()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 217363
    iget-object v1, p0, LX/1DJ;->b:LX/0ad;

    sget-short v2, LX/0fe;->bz:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    iput-boolean v1, p0, LX/1DJ;->f:Z

    .line 217364
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 217365
    new-instance v1, LX/1Oz;

    iget-object v2, p0, LX/1DJ;->a:LX/03V;

    iget-object v3, p0, LX/1DJ;->b:LX/0ad;

    invoke-direct {v1, v0, v2, v3}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/03V;LX/0ad;)V

    .line 217366
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 217367
    iget-boolean v2, p0, LX/1DJ;->f:Z

    .line 217368
    iput-boolean v2, v1, LX/1P0;->g:Z

    .line 217369
    new-instance v1, LX/0g5;

    invoke-direct {v1, v0}, LX/0g5;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    .line 217370
    invoke-virtual {v1, v4}, LX/0g7;->b(Z)V

    .line 217371
    invoke-virtual {v1, v5}, LX/0g7;->d(Z)V

    .line 217372
    iget-boolean v0, p0, LX/1DJ;->e:Z

    if-eqz v0, :cond_0

    .line 217373
    iget-object v0, v1, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 217374
    iput-boolean v5, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 217375
    :cond_0
    move-object v0, v1

    .line 217376
    iput-object v0, p0, LX/1DJ;->c:LX/0g5;

    .line 217377
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217378
    iput-object v0, p0, LX/1DJ;->c:LX/0g5;

    .line 217379
    iput-object v0, p0, LX/1DJ;->d:Landroid/view/View;

    .line 217380
    return-void
.end method
