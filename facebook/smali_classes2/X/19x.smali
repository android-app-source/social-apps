.class public LX/19x;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tz;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field private static volatile h:LX/19x;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 209750
    const-class v0, LX/19x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->b:Ljava/lang/String;

    .line 209751
    new-instance v0, LX/19y;

    invoke-direct {v0}, LX/19y;-><init>()V

    sput-object v0, LX/19x;->a:LX/0Tz;

    .line 209752
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/19z;->a:LX/0U1;

    .line 209753
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209754
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->c:Ljava/lang/String;

    .line 209755
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/19z;->e:LX/0U1;

    .line 209756
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209757
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    iget v1, v1, LX/1A0;->mValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->d:Ljava/lang/String;

    .line 209758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/19z;->e:LX/0U1;

    .line 209759
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209760
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    iget v1, v1, LX/1A0;->mValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->e:Ljava/lang/String;

    .line 209761
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/19z;->h:LX/0U1;

    .line 209762
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209763
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "= ? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/19z;->e:LX/0U1;

    .line 209764
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209765
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "= ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->f:Ljava/lang/String;

    .line 209766
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/19z;->a:LX/0U1;

    .line 209767
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 209768
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/19x;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 209748
    const-string v0, "saved_videos"

    const/4 v1, 0x7

    sget-object v2, LX/19x;->a:LX/0Tz;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 209749
    return-void
.end method

.method public static a(LX/0QB;)LX/19x;
    .locals 3

    .prologue
    .line 209736
    sget-object v0, LX/19x;->h:LX/19x;

    if-nez v0, :cond_1

    .line 209737
    const-class v1, LX/19x;

    monitor-enter v1

    .line 209738
    :try_start_0
    sget-object v0, LX/19x;->h:LX/19x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 209739
    if-eqz v2, :cond_0

    .line 209740
    :try_start_1
    new-instance v0, LX/19x;

    invoke-direct {v0}, LX/19x;-><init>()V

    .line 209741
    move-object v0, v0

    .line 209742
    sput-object v0, LX/19x;->h:LX/19x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209743
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 209744
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 209745
    :cond_1
    sget-object v0, LX/19x;->h:LX/19x;

    return-object v0

    .line 209746
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 209747
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;)LX/7Jg;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 209678
    new-instance v3, LX/7Jg;

    invoke-direct {v3}, LX/7Jg;-><init>()V

    .line 209679
    sget-object v0, LX/19z;->a:LX/0U1;

    .line 209680
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 209681
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->a:Ljava/lang/String;

    .line 209682
    sget-object v0, LX/19z;->b:LX/0U1;

    .line 209683
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 209684
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209685
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, v3, LX/7Jg;->b:Landroid/net/Uri;

    .line 209686
    sget-object v0, LX/19z;->k:LX/0U1;

    .line 209687
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 209688
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209689
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    :goto_1
    iput-object v1, v3, LX/7Jg;->c:Landroid/net/Uri;

    .line 209690
    sget-object v0, LX/19z;->c:LX/0U1;

    .line 209691
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209692
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->d:J

    .line 209693
    sget-object v0, LX/19z;->l:LX/0U1;

    .line 209694
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209695
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->e:J

    .line 209696
    sget-object v0, LX/19z;->o:LX/0U1;

    .line 209697
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209698
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->i:Ljava/lang/String;

    .line 209699
    sget-object v0, LX/19z;->p:LX/0U1;

    .line 209700
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209701
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->j:Ljava/lang/String;

    .line 209702
    sget-object v0, LX/19z;->d:LX/0U1;

    .line 209703
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209704
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->f:J

    .line 209705
    sget-object v0, LX/19z;->m:LX/0U1;

    .line 209706
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209707
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->g:J

    .line 209708
    sget-object v0, LX/19z;->e:LX/0U1;

    .line 209709
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209710
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, LX/1A0;->fromVal(I)LX/1A0;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->l:LX/1A0;

    .line 209711
    sget-object v0, LX/19z;->f:LX/0U1;

    .line 209712
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209713
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->h:Ljava/lang/String;

    .line 209714
    sget-object v0, LX/19z;->n:LX/0U1;

    .line 209715
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209716
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->k:Ljava/lang/String;

    .line 209717
    sget-object v0, LX/19z;->g:LX/0U1;

    .line 209718
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209719
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->m:J

    .line 209720
    sget-object v0, LX/19z;->h:LX/0U1;

    .line 209721
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209722
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, LX/2ft;->fromVal(I)LX/2ft;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->n:LX/2ft;

    .line 209723
    sget-object v0, LX/19z;->i:LX/0U1;

    .line 209724
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209725
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, v3, LX/7Jg;->o:Z

    .line 209726
    sget-object v0, LX/19z;->j:LX/0U1;

    .line 209727
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209728
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, LX/7Jg;->p:J

    .line 209729
    sget-object v0, LX/19z;->q:LX/0U1;

    .line 209730
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 209731
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/7Jg;->q:Ljava/lang/String;

    .line 209732
    return-object v3

    .line 209733
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 209734
    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_1

    .line 209735
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;
    .locals 5

    .prologue
    .line 209669
    sget-object v0, LX/19x;->c:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 209670
    invoke-static {p0, v0, v1, v2}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;

    move-result-object v4

    .line 209671
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 209672
    const/4 v2, 0x0

    .line 209673
    :goto_0
    move-object v0, v2

    .line 209674
    return-object v0

    .line 209675
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v2, :cond_1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 209676
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Jg;

    goto :goto_0

    :cond_1
    move v2, v3

    .line 209677
    goto :goto_1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;LX/1A0;)LX/7Jg;
    .locals 7

    .prologue
    .line 209648
    invoke-static {p0, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209649
    if-nez v0, :cond_0

    .line 209650
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown video id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209651
    :cond_0
    const/4 v2, 0x1

    .line 209652
    const/4 v1, 0x0

    .line 209653
    sget-object v3, LX/7Ja;->a:[I

    invoke-virtual {p2}, LX/1A0;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 209654
    :cond_1
    :goto_0
    move v1, v1

    .line 209655
    if-eqz v1, :cond_2

    .line 209656
    iput-object p2, v0, LX/7Jg;->l:LX/1A0;

    .line 209657
    invoke-static {p0, v0}, LX/19x;->b(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)V

    .line 209658
    :goto_1
    return-object v0

    .line 209659
    :cond_2
    sget-object v1, LX/19x;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    const-string v3, "Invalid status update for video %s from %s to %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, LX/7Jg;->l:LX/1A0;

    invoke-virtual {v6}, LX/1A0;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p2}, LX/1A0;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 209660
    :pswitch_0
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v3, v4, :cond_1

    :cond_3
    :goto_2
    move v1, v2

    .line 209661
    goto :goto_0

    .line 209662
    :pswitch_1
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-ne v3, v4, :cond_1

    goto :goto_2

    .line 209663
    :pswitch_2
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-eq v3, v4, :cond_4

    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v3, v4, :cond_1

    :cond_4
    move v1, v2

    .line 209664
    goto :goto_0

    .line 209665
    :pswitch_3
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-eq v3, v4, :cond_5

    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v3, v4, :cond_1

    :cond_5
    move v1, v2

    .line 209666
    goto :goto_0

    .line 209667
    :pswitch_4
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v3, v4, :cond_1

    goto :goto_2

    .line 209668
    :pswitch_5
    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-eq v3, v4, :cond_3

    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v3, v4, :cond_3

    iget-object v3, v0, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-ne v3, v4, :cond_1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/7Jg;Z)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 209769
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 209770
    if-eqz p1, :cond_0

    .line 209771
    sget-object v0, LX/19z;->a:LX/0U1;

    .line 209772
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209773
    iget-object v2, p0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209774
    sget-object v0, LX/19z;->b:LX/0U1;

    .line 209775
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209776
    iget-object v2, p0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209777
    sget-object v0, LX/19z;->k:LX/0U1;

    .line 209778
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 209779
    iget-object v0, p0, LX/7Jg;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7Jg;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209780
    sget-object v0, LX/19z;->f:LX/0U1;

    .line 209781
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209782
    iget-object v2, p0, LX/7Jg;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209783
    sget-object v0, LX/19z;->n:LX/0U1;

    .line 209784
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209785
    iget-object v2, p0, LX/7Jg;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209786
    sget-object v0, LX/19z;->o:LX/0U1;

    .line 209787
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209788
    iget-object v2, p0, LX/7Jg;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209789
    sget-object v0, LX/19z;->p:LX/0U1;

    .line 209790
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209791
    iget-object v2, p0, LX/7Jg;->j:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209792
    sget-object v0, LX/19z;->h:LX/0U1;

    .line 209793
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209794
    iget-object v2, p0, LX/7Jg;->n:LX/2ft;

    iget v2, v2, LX/2ft;->mValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209795
    sget-object v0, LX/19z;->i:LX/0U1;

    .line 209796
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 209797
    iget-boolean v0, p0, LX/7Jg;->o:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209798
    sget-object v0, LX/19z;->q:LX/0U1;

    .line 209799
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209800
    iget-object v2, p0, LX/7Jg;->q:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 209801
    :cond_0
    sget-object v0, LX/19z;->c:LX/0U1;

    .line 209802
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209803
    iget-wide v2, p0, LX/7Jg;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209804
    sget-object v0, LX/19z;->l:LX/0U1;

    .line 209805
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209806
    iget-wide v2, p0, LX/7Jg;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209807
    sget-object v0, LX/19z;->m:LX/0U1;

    .line 209808
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209809
    iget-wide v2, p0, LX/7Jg;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209810
    sget-object v0, LX/19z;->d:LX/0U1;

    .line 209811
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209812
    iget-wide v2, p0, LX/7Jg;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209813
    sget-object v0, LX/19z;->e:LX/0U1;

    .line 209814
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209815
    iget-object v2, p0, LX/7Jg;->l:LX/1A0;

    iget v2, v2, LX/1A0;->mValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209816
    sget-object v0, LX/19z;->g:LX/0U1;

    .line 209817
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209818
    iget-wide v2, p0, LX/7Jg;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209819
    sget-object v0, LX/19z;->j:LX/0U1;

    .line 209820
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 209821
    iget-wide v2, p0, LX/7Jg;->p:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209822
    return-object v1

    .line 209823
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 209824
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v2, 0x0

    .line 209633
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 209634
    const-string v1, "saved_videos"

    if-ne p3, v10, :cond_0

    move-object v8, v2

    :goto_0
    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 209635
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209636
    if-ne p3, v10, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 209637
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_2

    .line 209638
    invoke-static {v2}, LX/19x;->a(Landroid/database/Cursor;)LX/7Jg;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209639
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 209640
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 209641
    :cond_0
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 209642
    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 209643
    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 209644
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 209645
    :cond_3
    return-object v9

    .line 209646
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 209647
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private static a(LX/7Jg;)V
    .locals 4

    .prologue
    .line 209626
    iget-object v0, p0, LX/7Jg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Jg;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209627
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video id cannot be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209628
    :cond_1
    iget-object v0, p0, LX/7Jg;->b:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 209629
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video URL cannot be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209630
    :cond_2
    iget-wide v0, p0, LX/7Jg;->f:J

    iget-wide v2, p0, LX/7Jg;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 209631
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid stream sizes. Video size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/7Jg;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Downloaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/7Jg;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209632
    :cond_3
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 209618
    invoke-static {p0, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209619
    if-nez v0, :cond_0

    .line 209620
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown video id"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209621
    :cond_0
    iget-wide v2, v0, LX/7Jg;->m:J

    cmp-long v1, p2, v2

    if-gez v1, :cond_1

    .line 209622
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tried to update last check time with older check time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209623
    :cond_1
    iput-wide p2, v0, LX/7Jg;->m:J

    .line 209624
    invoke-static {p0, v0}, LX/19x;->b(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)V

    .line 209625
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/net/Uri;JJJ)V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 209601
    invoke-static {p0, p1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;

    move-result-object v0

    .line 209602
    if-nez v0, :cond_0

    .line 209603
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown video id"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209604
    :cond_0
    iget-object v1, v0, LX/7Jg;->b:Landroid/net/Uri;

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209605
    cmp-long v1, p5, v2

    if-eqz v1, :cond_1

    .line 209606
    iput-wide p5, v0, LX/7Jg;->d:J

    .line 209607
    :cond_1
    iget-wide v2, v0, LX/7Jg;->f:J

    iget-wide v4, v0, LX/7Jg;->d:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 209608
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video download delta exceeds total video size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209609
    :cond_2
    iput-wide p3, v0, LX/7Jg;->f:J

    .line 209610
    :goto_0
    iput-wide p7, v0, LX/7Jg;->p:J

    .line 209611
    invoke-static {p0, v0}, LX/19x;->b(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)V

    .line 209612
    return-void

    .line 209613
    :cond_3
    cmp-long v1, p5, v2

    if-eqz v1, :cond_4

    .line 209614
    iput-wide p5, v0, LX/7Jg;->e:J

    .line 209615
    :cond_4
    iget-wide v2, v0, LX/7Jg;->g:J

    iget-wide v4, v0, LX/7Jg;->e:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_5

    .line 209616
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video download delta exceeds total video size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209617
    :cond_5
    iput-wide p3, v0, LX/7Jg;->g:J

    goto :goto_0
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 209596
    iget-object v1, p1, LX/7Jg;->a:Ljava/lang/String;

    invoke-static {p0, v1}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)LX/7Jg;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 209597
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Record already exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209598
    :cond_0
    invoke-static {p1}, LX/19x;->a(LX/7Jg;)V

    .line 209599
    invoke-static {p1, v0}, LX/19x;->a(LX/7Jg;Z)Landroid/content/ContentValues;

    move-result-object v1

    .line 209600
    const-string v2, "saved_videos"

    const/4 v3, 0x0

    const v4, 0x6cfe44ea

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const v1, -0xb66bb23

    invoke-static {v1}, LX/03h;->a(I)V

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;LX/7Jg;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 209590
    invoke-static {p1}, LX/19x;->a(LX/7Jg;)V

    .line 209591
    invoke-static {p1, v5}, LX/19x;->a(LX/7Jg;Z)Landroid/content/ContentValues;

    move-result-object v0

    .line 209592
    const-string v1, "saved_videos"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/19z;->a:LX/0U1;

    .line 209593
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 209594
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "= ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p1, LX/7Jg;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 209595
    return-void
.end method

.method public static d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7Jg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 209589
    const/4 v0, -0x1

    invoke-static {p0, v1, v1, v0}, LX/19x;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
