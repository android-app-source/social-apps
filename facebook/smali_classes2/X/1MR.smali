.class public LX/1MR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static b:Lcom/facebook/proxygen/utils/CircularEventLog;

.field private static volatile d:LX/1MR;


# instance fields
.field private final c:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235579
    const-class v0, LX/1MR;

    sput-object v0, LX/1MR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235629
    iput-object p1, p0, LX/1MR;->c:LX/0W3;

    .line 235630
    return-void
.end method

.method public static a(LX/0QB;)LX/1MR;
    .locals 4

    .prologue
    .line 235615
    sget-object v0, LX/1MR;->d:LX/1MR;

    if-nez v0, :cond_1

    .line 235616
    const-class v1, LX/1MR;

    monitor-enter v1

    .line 235617
    :try_start_0
    sget-object v0, LX/1MR;->d:LX/1MR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235618
    if-eqz v2, :cond_0

    .line 235619
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235620
    new-instance p0, LX/1MR;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/1MR;-><init>(LX/0W3;)V

    .line 235621
    move-object v0, p0

    .line 235622
    sput-object v0, LX/1MR;->d:LX/1MR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235623
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235624
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235625
    :cond_1
    sget-object v0, LX/1MR;->d:LX/1MR;

    return-object v0

    .line 235626
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/io/File;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 235603
    new-instance v0, Ljava/io/File;

    const-string v2, "fb_liger_reporting"

    invoke-direct {v0, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235604
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 235605
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 235606
    :try_start_0
    new-instance v4, Ljava/io/PrintWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v4, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 235607
    :try_start_1
    sget-object v0, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    invoke-virtual {v0}, Lcom/facebook/proxygen/utils/CircularEventLog;->getLogLines()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 235608
    invoke-virtual {v4, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235609
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235610
    :cond_0
    const/4 v0, 0x0

    :try_start_2
    invoke-static {v4, v0}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235611
    invoke-static {v3, v1}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 235612
    return-object v2

    .line 235613
    :catchall_0
    move-exception v0

    const/4 v2, 0x0

    :try_start_3
    invoke-static {v4, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 235614
    :catchall_1
    move-exception v0

    invoke-static {v3, v1}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 235601
    sget-object v0, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    invoke-virtual {v0}, Lcom/facebook/proxygen/utils/CircularEventLog;->init()V

    .line 235602
    return-void
.end method

.method public static a(Lcom/facebook/proxygen/EventBase;)V
    .locals 2

    .prologue
    .line 235599
    new-instance v0, Lcom/facebook/proxygen/utils/CircularEventLog;

    const/16 v1, 0x64

    invoke-direct {v0, p0, v1}, Lcom/facebook/proxygen/utils/CircularEventLog;-><init>(Lcom/facebook/proxygen/EventBase;I)V

    sput-object v0, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    .line 235600
    return-void
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235589
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 235590
    sget-object v1, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    if-eqz v1, :cond_0

    sget-object v1, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    .line 235591
    iget-boolean v2, v1, Lcom/facebook/proxygen/utils/CircularEventLog;->mInitialized:Z

    move v1, v2

    .line 235592
    if-eqz v1, :cond_0

    .line 235593
    :try_start_0
    invoke-static {p1}, LX/1MR;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 235594
    const-string v2, "fb_liger_reporting"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235595
    :cond_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 235596
    :catch_0
    move-exception v0

    .line 235597
    sget-object v1, LX/1MR;->a:Ljava/lang/Class;

    const-string v2, "Exception saving liger trace"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235598
    throw v0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235582
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 235583
    sget-object v1, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    if-eqz v1, :cond_0

    sget-object v1, LX/1MR;->b:Lcom/facebook/proxygen/utils/CircularEventLog;

    .line 235584
    iget-boolean v2, v1, Lcom/facebook/proxygen/utils/CircularEventLog;->mInitialized:Z

    move v1, v2

    .line 235585
    if-eqz v1, :cond_0

    .line 235586
    invoke-static {p1}, LX/1MR;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 235587
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "fb_liger_reporting"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235588
    :cond_0
    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 235581
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 235580
    iget-object v0, p0, LX/1MR;->c:LX/0W3;

    sget-wide v2, LX/0X5;->aQ:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
