.class public LX/0oI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0oI;


# instance fields
.field private final a:LX/0oJ;

.field private final b:LX/0Yn;

.field private final c:LX/0oK;


# direct methods
.method public constructor <init>(LX/0oJ;LX/0Yn;LX/0oK;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 140125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140126
    iput-object p1, p0, LX/0oI;->a:LX/0oJ;

    .line 140127
    iput-object p2, p0, LX/0oI;->b:LX/0Yn;

    .line 140128
    iput-object p3, p0, LX/0oI;->c:LX/0oK;

    .line 140129
    return-void
.end method

.method public static a(LX/0QB;)LX/0oI;
    .locals 7

    .prologue
    .line 140101
    sget-object v0, LX/0oI;->d:LX/0oI;

    if-nez v0, :cond_1

    .line 140102
    const-class v1, LX/0oI;

    monitor-enter v1

    .line 140103
    :try_start_0
    sget-object v0, LX/0oI;->d:LX/0oI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 140104
    if-eqz v2, :cond_0

    .line 140105
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 140106
    new-instance v6, LX/0oI;

    invoke-static {v0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v3

    check-cast v3, LX/0oJ;

    invoke-static {v0}, LX/0Yn;->a(LX/0QB;)LX/0Yn;

    move-result-object v4

    check-cast v4, LX/0Yn;

    .line 140107
    new-instance p0, LX/0oK;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v5}, LX/0oK;-><init>(LX/0Uh;)V

    .line 140108
    move-object v5, p0

    .line 140109
    check-cast v5, LX/0oK;

    invoke-direct {v6, v3, v4, v5}, LX/0oI;-><init>(LX/0oJ;LX/0Yn;LX/0oK;)V

    .line 140110
    move-object v0, v6

    .line 140111
    sput-object v0, LX/0oI;->d:LX/0oI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140112
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 140113
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 140114
    :cond_1
    sget-object v0, LX/0oI;->d:LX/0oI;

    return-object v0

    .line 140115
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 140116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    .line 140117
    iget-object v0, p0, LX/0oI;->a:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->a()Z

    move-result v0

    .line 140118
    iget-object v1, p0, LX/0oI;->b:LX/0Yn;

    invoke-virtual {v1}, LX/0Yn;->a()Z

    move-result v1

    .line 140119
    iget-object v2, p0, LX/0oI;->c:LX/0oK;

    .line 140120
    iget-object v3, v2, LX/0oK;->a:LX/0Uh;

    const/16 v4, 0x3b6

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 140121
    sget-boolean v3, LX/007;->j:Z

    move v3, v3

    .line 140122
    if-nez v3, :cond_0

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    .line 140123
    :cond_0
    const/4 v0, 0x1

    .line 140124
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
