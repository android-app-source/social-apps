.class public abstract LX/1qA;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/1qB;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 330120
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 330121
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {p0, p0, v0}, LX/1qA;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 330122
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)LX/1qB;
    .locals 2

    .prologue
    .line 330123
    if-nez p0, :cond_0

    .line 330124
    const/4 v0, 0x0

    .line 330125
    :goto_0
    return-object v0

    .line 330126
    :cond_0
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 330127
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/1qB;

    if-eqz v1, :cond_1

    .line 330128
    check-cast v0, LX/1qB;

    goto :goto_0

    .line 330129
    :cond_1
    new-instance v0, LX/4BG;

    invoke-direct {v0, p0}, LX/4BG;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 330130
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 330131
    sparse-switch p1, :sswitch_data_0

    .line 330132
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 330133
    :sswitch_0
    const-string v0, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 330134
    goto :goto_0

    .line 330135
    :sswitch_1
    const-string v2, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 330136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 330137
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 330138
    :cond_0
    invoke-virtual {p0, v0}, LX/1qA;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 330139
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 330140
    goto :goto_0

    .line 330141
    :sswitch_2
    const-string v2, "com.facebook.fbservice.service.ICompletionHandler"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 330142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 330143
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 330144
    :cond_1
    invoke-virtual {p0, v0}, LX/1qA;->onOperationCompleted(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 330145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 330146
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
