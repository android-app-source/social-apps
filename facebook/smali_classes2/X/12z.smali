.class public LX/12z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/12z;


# instance fields
.field private final a:LX/0SF;

.field private final b:Lcom/facebook/common/time/RealtimeSinceBootClock;


# direct methods
.method public constructor <init>(LX/0SF;Lcom/facebook/common/time/RealtimeSinceBootClock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175947
    iput-object p1, p0, LX/12z;->a:LX/0SF;

    .line 175948
    iput-object p2, p0, LX/12z;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    .line 175949
    return-void
.end method

.method public static a(LX/0QB;)LX/12z;
    .locals 5

    .prologue
    .line 175933
    sget-object v0, LX/12z;->c:LX/12z;

    if-nez v0, :cond_1

    .line 175934
    const-class v1, LX/12z;

    monitor-enter v1

    .line 175935
    :try_start_0
    sget-object v0, LX/12z;->c:LX/12z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 175936
    if-eqz v2, :cond_0

    .line 175937
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 175938
    new-instance p0, LX/12z;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SF;

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-direct {p0, v3, v4}, LX/12z;-><init>(LX/0SF;Lcom/facebook/common/time/RealtimeSinceBootClock;)V

    .line 175939
    move-object v0, p0

    .line 175940
    sput-object v0, LX/12z;->c:LX/12z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175941
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 175942
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175943
    :cond_1
    sget-object v0, LX/12z;->c:LX/12z;

    return-object v0

    .line 175944
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 175945
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(IJJLandroid/app/PendingIntent;Landroid/app/AlarmManager;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const-wide/32 v6, 0xdbba0

    const-wide/16 v2, 0x0

    .line 175919
    if-nez p6, :cond_1

    .line 175920
    :cond_0
    :goto_0
    return-void

    .line 175921
    :cond_1
    cmp-long v1, p4, v2

    if-lez v1, :cond_0

    .line 175922
    rem-long v4, p4, v6

    cmp-long v1, v4, v2

    if-eqz v1, :cond_2

    move-object v0, p7

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    .line 175923
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 175924
    :cond_2
    if-eq p1, v0, :cond_3

    if-nez p1, :cond_4

    .line 175925
    :cond_3
    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/12z;->a:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    iget-object v4, p0, LX/12z;->b:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v4}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 175926
    :goto_2
    sub-long v0, p2, v0

    rem-long/2addr v0, v6

    .line 175927
    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 175928
    sub-long v0, p2, v0

    add-long v2, v0, v6

    :goto_3
    move-object v0, p7

    move v1, p1

    move-wide v4, p4

    move-object v6, p6

    .line 175929
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0

    .line 175930
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-wide v0, v2

    .line 175931
    goto :goto_2

    :cond_6
    move-wide v2, p2

    .line 175932
    goto :goto_3
.end method
