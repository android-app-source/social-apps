.class public LX/0Wb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Wb;


# instance fields
.field private final a:LX/0Sg;


# direct methods
.method public constructor <init>(LX/0Sg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 76571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76572
    iput-object p1, p0, LX/0Wb;->a:LX/0Sg;

    .line 76573
    return-void
.end method

.method public static a(LX/0QB;)LX/0Wb;
    .locals 4

    .prologue
    .line 76574
    sget-object v0, LX/0Wb;->b:LX/0Wb;

    if-nez v0, :cond_1

    .line 76575
    const-class v1, LX/0Wb;

    monitor-enter v1

    .line 76576
    :try_start_0
    sget-object v0, LX/0Wb;->b:LX/0Wb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 76577
    if-eqz v2, :cond_0

    .line 76578
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 76579
    new-instance p0, LX/0Wb;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v3

    check-cast v3, LX/0Sg;

    invoke-direct {p0, v3}, LX/0Wb;-><init>(LX/0Sg;)V

    .line 76580
    move-object v0, p0

    .line 76581
    sput-object v0, LX/0Wb;->b:LX/0Wb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76582
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 76583
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76584
    :cond_1
    sget-object v0, LX/0Wb;->b:LX/0Wb;

    return-object v0

    .line 76585
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 76586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;
    .locals 2

    .prologue
    .line 76587
    new-instance v0, LX/0Wd;

    iget-object v1, p0, LX/0Wb;->a:LX/0Sg;

    invoke-direct {v0, v1, p1}, LX/0Wd;-><init>(LX/0Sg;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method
