.class public LX/1pc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1pc;


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329567
    iput-object p1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329568
    return-void
.end method

.method public static a(LX/0QB;)LX/1pc;
    .locals 5

    .prologue
    .line 329569
    sget-object v0, LX/1pc;->b:LX/1pc;

    if-nez v0, :cond_1

    .line 329570
    const-class v1, LX/1pc;

    monitor-enter v1

    .line 329571
    :try_start_0
    sget-object v0, LX/1pc;->b:LX/1pc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 329572
    if-eqz v2, :cond_0

    .line 329573
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 329574
    new-instance v4, LX/1pc;

    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3}, LX/1pc;-><init>(Landroid/content/Context;)V

    .line 329575
    move-object v0, v4

    .line 329576
    sput-object v0, LX/1pc;->b:LX/1pc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329577
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 329578
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 329579
    :cond_1
    sget-object v0, LX/1pc;->b:LX/1pc;

    return-object v0

    .line 329580
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 329581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 3

    .prologue
    .line 329563
    invoke-static {p0}, LX/38I;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 329564
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(me|\\d+)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 329565
    return-object v0
.end method


# virtual methods
.method public final a(LX/0P2;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329529
    iget-object v0, p0, LX/1pc;->a:Landroid/content/Context;

    invoke-static {v0}, LX/38I;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 329530
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 329531
    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    invoke-static {v1}, LX/38I;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "api"

    invoke-virtual {p1, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    iget-object v2, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329532
    const-string v3, "https://api-read.%s/restserver.php"

    invoke-static {v2, v3}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 329533
    const-string v3, "api"

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    iget-object v2, p0, LX/1pc;->a:Landroid/content/Context;

    invoke-static {v2}, LX/38I;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "api"

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    iget-object v2, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329534
    const-string v3, "https://api-video.%s/restserver.php"

    invoke-static {v2, v3}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 329535
    const-string v3, "api_video"

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    iget-object v2, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329536
    new-instance v3, LX/1pd;

    invoke-direct {v3, v2}, LX/1pd;-><init>(Landroid/content/Context;)V

    .line 329537
    iget-object p1, v3, LX/1pd;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 329538
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    move v3, p1

    .line 329539
    if-eqz v3, :cond_0

    .line 329540
    const-string v3, "https://b-api.%s/restserver.php"

    invoke-static {v2, v3}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 329541
    :goto_0
    move-object v2, v3

    .line 329542
    const-string v3, "api"

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "graph"

    invoke-virtual {v1, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329543
    const-string v2, "https://graph-video.%s/"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329544
    const-string v2, "video_upload"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329545
    sget-boolean v2, LX/007;->i:Z

    move v2, v2

    .line 329546
    if-eqz v2, :cond_1

    .line 329547
    const-string v2, "https://www.%s/mobile/android_beta_crash_logs/"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 329548
    :goto_1
    move-object v1, v2

    .line 329549
    const-string v2, "crash_report"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329550
    const-string v2, "https://www.%s/impression.php"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329551
    const-string v2, "log"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329552
    const-string v2, "https://api.%s/method/logging.clientevent"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329553
    const-string v2, "log"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329554
    const-string v2, "https://b-api.%s/method/logging.clientevent"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329555
    const-string v2, "log"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329556
    const-string v2, "https://m.%s/promotion.php"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329557
    const-string v2, "deal"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329558
    const-string v2, "https://www.%s/ai.php"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329559
    const-string v2, "ad_log"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, LX/1pc;->a:Landroid/content/Context;

    .line 329560
    const-string v2, "http://www.%s/images/emoji/unicode/"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 329561
    const-string v2, "emoji"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 329562
    return-void

    :cond_0
    invoke-static {v2}, LX/38I;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_1
    const-string v2, "https://www.%s/mobile/android_crash_logs/"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
