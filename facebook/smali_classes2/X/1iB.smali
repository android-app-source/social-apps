.class public LX/1iB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1iB;


# instance fields
.field private a:I

.field public b:D

.field private c:Z

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/IsLogAllRequestsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297536
    const/16 v0, 0x14

    iput v0, p0, LX/1iB;->a:I

    .line 297537
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget v2, p0, LX/1iB;->a:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/1iB;->b:D

    .line 297538
    iput-object p1, p0, LX/1iB;->d:LX/0Or;

    .line 297539
    iput-object p2, p0, LX/1iB;->e:LX/0Ot;

    .line 297540
    return-void
.end method

.method public static a(LX/0QB;)LX/1iB;
    .locals 5

    .prologue
    .line 297541
    sget-object v0, LX/1iB;->f:LX/1iB;

    if-nez v0, :cond_1

    .line 297542
    const-class v1, LX/1iB;

    monitor-enter v1

    .line 297543
    :try_start_0
    sget-object v0, LX/1iB;->f:LX/1iB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297544
    if-eqz v2, :cond_0

    .line 297545
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297546
    new-instance v3, LX/1iB;

    const/16 v4, 0x321

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0xdf4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1iB;-><init>(LX/0Or;LX/0Ot;)V

    .line 297547
    move-object v0, v3

    .line 297548
    sput-object v0, LX/1iB;->f:LX/1iB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297549
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297550
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297551
    :cond_1
    sget-object v0, LX/1iB;->f:LX/1iB;

    return-object v0

    .line 297552
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297553
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized c(LX/1iB;)V
    .locals 4

    .prologue
    .line 297554
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1iB;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 297555
    :goto_0
    monitor-exit p0

    return-void

    .line 297556
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1iB;->c:Z

    .line 297557
    iget-object v0, p0, LX/1iB;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 297558
    sget-wide v2, LX/0X5;->hY:J

    const/16 v1, 0x14

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1iB;->a:I

    .line 297559
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget v2, p0, LX/1iB;->a:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/1iB;->b:D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 297560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    .line 297561
    iget-object v0, p0, LX/1iB;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    move v0, v0

    .line 297562
    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    .line 297563
    invoke-static {p0}, LX/1iB;->c(LX/1iB;)V

    .line 297564
    iget-wide v4, p0, LX/1iB;->b:D

    move-wide v2, v4

    .line 297565
    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
