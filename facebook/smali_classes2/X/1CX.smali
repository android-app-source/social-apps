.class public LX/1CX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1CW;

.field private final c:LX/0gh;

.field private final d:LX/03V;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/01S;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1CW;LX/0Or;LX/0gh;LX/03V;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1CW;",
            "LX/0Or",
            "<",
            "LX/01S;",
            ">;",
            "LX/0gh;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 216340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216341
    iput-object p1, p0, LX/1CX;->a:Landroid/content/Context;

    .line 216342
    iput-object p2, p0, LX/1CX;->b:LX/1CW;

    .line 216343
    iput-object p3, p0, LX/1CX;->f:LX/0Or;

    .line 216344
    iput-object p4, p0, LX/1CX;->c:LX/0gh;

    .line 216345
    iput-object p5, p0, LX/1CX;->d:LX/03V;

    .line 216346
    iput-object p6, p0, LX/1CX;->e:Lcom/facebook/content/SecureContextHelper;

    .line 216347
    return-void
.end method

.method public static a(LX/0QB;)LX/1CX;
    .locals 10

    .prologue
    .line 216348
    const-class v1, LX/1CX;

    monitor-enter v1

    .line 216349
    :try_start_0
    sget-object v0, LX/1CX;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 216350
    sput-object v2, LX/1CX;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 216351
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216352
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 216353
    new-instance v3, LX/1CX;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v5

    check-cast v5, LX/1CW;

    const/16 v6, 0x3de

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v7

    check-cast v7, LX/0gh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v9}, LX/1CX;-><init>(Landroid/content/Context;LX/1CW;LX/0Or;LX/0gh;LX/03V;Lcom/facebook/content/SecureContextHelper;)V

    .line 216354
    move-object v0, v3

    .line 216355
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 216356
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1CX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216357
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 216358
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/4mm;)LX/2EJ;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 216359
    const/4 v3, 0x0

    .line 216360
    iget-object v0, p1, LX/4mm;->e:Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 216361
    iget-object v0, p0, LX/1CX;->b:LX/1CW;

    iget-object v1, p1, LX/4mm;->e:Lcom/facebook/fbservice/service/ServiceException;

    invoke-virtual {v0, v1, v3, v3}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 216362
    if-nez v0, :cond_1

    .line 216363
    :cond_0
    iget-object v0, p1, LX/4mm;->b:Ljava/lang/String;

    .line 216364
    :cond_1
    if-nez v0, :cond_2

    .line 216365
    iget-object v0, p0, LX/1CX;->a:Landroid/content/Context;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216366
    :cond_2
    iget-boolean v1, p1, LX/4mm;->k:Z

    if-eqz v1, :cond_3

    .line 216367
    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216368
    :cond_3
    move-object v0, v0

    .line 216369
    iget-object v1, p1, LX/4mm;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 216370
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/4mm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216371
    :cond_4
    iget-object v1, p0, LX/1CX;->a:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    iget-object v1, p0, LX/1CX;->a:Landroid/content/Context;

    const-class v3, LX/4mc;

    invoke-static {v1, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_a

    :cond_5
    move v1, v2

    :goto_0
    const-string v3, "ErrorDialogBuilder should only be used with an Activity context or a context that implements CustomDialogHostContext"

    invoke-static {v1, v3}, LX/45Y;->a(ZLjava/lang/String;)V

    .line 216372
    iget-object v1, p1, LX/4mm;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 216373
    if-nez v1, :cond_6

    .line 216374
    new-instance v1, LX/4mo;

    invoke-direct {v1, p0, p1}, LX/4mo;-><init>(LX/1CX;LX/4mm;)V

    .line 216375
    :cond_6
    new-instance v3, LX/4mp;

    invoke-direct {v3, p0, p1}, LX/4mp;-><init>(LX/1CX;LX/4mm;)V

    .line 216376
    new-instance v4, LX/31Y;

    iget-object v5, p0, LX/1CX;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v5, p1, LX/4mm;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    iget-object v5, p0, LX/1CX;->a:Landroid/content/Context;

    const v6, 0x7f080016

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    iget-object v4, p1, LX/4mm;->g:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v1

    .line 216377
    iget-object v4, p1, LX/4mm;->e:Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v4, :cond_7

    iget-object v4, p0, LX/1CX;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    sget-object v5, LX/01S;->PUBLIC:LX/01S;

    if-eq v4, v5, :cond_7

    .line 216378
    iget-object v4, p0, LX/1CX;->a:Landroid/content/Context;

    const v5, 0x7f080045

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 216379
    :cond_7
    iget-object v3, p1, LX/4mm;->d:Landroid/net/Uri;

    if-eqz v3, :cond_8

    .line 216380
    new-instance v3, LX/4mq;

    invoke-direct {v3, p0, p1}, LX/4mq;-><init>(LX/1CX;LX/4mm;)V

    .line 216381
    iget-object v4, p0, LX/1CX;->a:Landroid/content/Context;

    const v5, 0x7f080055

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 216382
    :cond_8
    iget-object v3, p0, LX/1CX;->c:LX/0gh;

    const-string v4, "error_dialog"

    invoke-virtual {v3, v4, v2}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 216383
    iget-boolean v2, p1, LX/4mm;->j:Z

    if-eqz v2, :cond_9

    .line 216384
    iget-object v2, p0, LX/1CX;->d:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dialog-error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, LX/4mm;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216385
    :cond_9
    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0

    .line 216386
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)LX/4mm;
    .locals 1

    .prologue
    .line 216387
    iget-object v0, p0, LX/1CX;->a:Landroid/content/Context;

    invoke-static {v0}, LX/4mm;->a(Landroid/content/Context;)LX/4mn;

    move-result-object v0

    .line 216388
    iput-object p1, v0, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 216389
    move-object v0, v0

    .line 216390
    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    return-object v0
.end method
