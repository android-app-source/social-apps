.class public LX/1tI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ya;",
        "LX/0Or",
        "<",
        "Landroid/os/Looper;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/os/Looper;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 336355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336356
    invoke-static {p0, p1}, LX/1tI;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 336357
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/1tI;

    invoke-static {v0}, LX/1rS;->a(LX/0QB;)Landroid/os/Looper;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, LX/1tI;->a:Landroid/os/Looper;

    return-void
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 336358
    iget-object v0, p0, LX/1tI;->a:Landroid/os/Looper;

    move-object v0, v0

    .line 336359
    return-object v0
.end method
