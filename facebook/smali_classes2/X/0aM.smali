.class public LX/0aM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 84516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84517
    iput-object p1, p0, LX/0aM;->a:Landroid/content/Context;

    .line 84518
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 84519
    iget-object v0, p0, LX/0aM;->a:Landroid/content/Context;

    .line 84520
    const-string v1, "QuickerExperimentUniqueDeviceIdPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 84521
    const-string v1, "qe_device_id"

    const/4 p0, 0x0

    invoke-interface {v2, v1, p0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84522
    if-eqz v1, :cond_0

    .line 84523
    :goto_0
    move-object v0, v1

    .line 84524
    return-object v0

    .line 84525
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 84526
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string p0, "qe_device_id"

    invoke-interface {v2, p0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
