.class public LX/1rW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile x:LX/1rW;


# instance fields
.field private final b:LX/0ad;

.field public final c:Landroid/os/Handler;

.field public final d:LX/0Uo;

.field private final e:LX/0Xl;

.field private final f:LX/0Xl;

.field private final g:LX/0So;

.field public final h:LX/0y3;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rl;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/1ra;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1sN;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Tf;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Lcom/facebook/location/foreground/GeoPixelController;

.field private p:Z

.field private q:LX/0Yb;

.field private r:LX/0Yb;

.field private s:J

.field private t:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public final u:Ljava/lang/Runnable;

.field public final v:Ljava/lang/Runnable;

.field private final w:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332423
    const-class v0, LX/1rW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1rW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;Landroid/os/Handler;LX/0Tf;LX/0Uo;LX/0Xl;LX/0Xl;LX/0So;LX/0y3;LX/0Ot;LX/0Ot;LX/1ra;LX/0Ot;LX/0Ot;Lcom/facebook/location/foreground/GeoPixelController;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Tf;
        .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .param p7    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/os/Handler;",
            "LX/0Tf;",
            "LX/0Uo;",
            "LX/0Xl;",
            "LX/0Xl;",
            "LX/0So;",
            "LX/0y3;",
            "LX/0Ot",
            "<",
            "LX/1rl;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;",
            ">;",
            "LX/1ra;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1sN;",
            ">;",
            "Lcom/facebook/location/foreground/GeoPixelController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332438
    new-instance v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$1;

    invoke-direct {v0, p0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$1;-><init>(LX/1rW;)V

    iput-object v0, p0, LX/1rW;->u:Ljava/lang/Runnable;

    .line 332439
    new-instance v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$2;

    invoke-direct {v0, p0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$2;-><init>(LX/1rW;)V

    iput-object v0, p0, LX/1rW;->v:Ljava/lang/Runnable;

    .line 332440
    new-instance v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;

    invoke-direct {v0, p0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;-><init>(LX/1rW;)V

    iput-object v0, p0, LX/1rW;->w:Ljava/lang/Runnable;

    .line 332441
    iput-object p1, p0, LX/1rW;->b:LX/0ad;

    .line 332442
    iput-object p2, p0, LX/1rW;->c:Landroid/os/Handler;

    .line 332443
    iput-object p4, p0, LX/1rW;->d:LX/0Uo;

    .line 332444
    iput-object p5, p0, LX/1rW;->e:LX/0Xl;

    .line 332445
    iput-object p6, p0, LX/1rW;->f:LX/0Xl;

    .line 332446
    iput-object p7, p0, LX/1rW;->g:LX/0So;

    .line 332447
    iput-object p8, p0, LX/1rW;->h:LX/0y3;

    .line 332448
    iput-object p9, p0, LX/1rW;->i:LX/0Ot;

    .line 332449
    iput-object p10, p0, LX/1rW;->j:LX/0Ot;

    .line 332450
    iput-object p11, p0, LX/1rW;->k:LX/1ra;

    .line 332451
    iput-object p13, p0, LX/1rW;->l:LX/0Ot;

    .line 332452
    iput-object p12, p0, LX/1rW;->n:LX/0Ot;

    .line 332453
    iput-object p3, p0, LX/1rW;->m:LX/0Tf;

    .line 332454
    iput-object p14, p0, LX/1rW;->o:Lcom/facebook/location/foreground/GeoPixelController;

    .line 332455
    return-void
.end method

.method public static a(LX/0QB;)LX/1rW;
    .locals 3

    .prologue
    .line 332427
    sget-object v0, LX/1rW;->x:LX/1rW;

    if-nez v0, :cond_1

    .line 332428
    const-class v1, LX/1rW;

    monitor-enter v1

    .line 332429
    :try_start_0
    sget-object v0, LX/1rW;->x:LX/1rW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332430
    if-eqz v2, :cond_0

    .line 332431
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1rW;->b(LX/0QB;)LX/1rW;

    move-result-object v0

    sput-object v0, LX/1rW;->x:LX/1rW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332432
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332433
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332434
    :cond_1
    sget-object v0, LX/1rW;->x:LX/1rW;

    return-object v0

    .line 332435
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/1rW;J)V
    .locals 3

    .prologue
    .line 332424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rW;->m:LX/0Tf;

    iget-object v1, p0, LX/1rW;->w:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, LX/1rW;->t:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332425
    monitor-exit p0

    return-void

    .line 332426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)LX/1rW;
    .locals 15

    .prologue
    .line 332275
    new-instance v0, LX/1rW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/1rX;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-static {p0}, LX/1rZ;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, LX/0Tf;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {p0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v8

    check-cast v8, LX/0y3;

    const/16 v9, 0xc8f

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc92

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/1ra;->a(LX/0QB;)LX/1ra;

    move-result-object v11

    check-cast v11, LX/1ra;

    const/16 v12, 0x259

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xc93

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {p0}, Lcom/facebook/location/foreground/GeoPixelController;->a(LX/0QB;)Lcom/facebook/location/foreground/GeoPixelController;

    move-result-object v14

    check-cast v14, Lcom/facebook/location/foreground/GeoPixelController;

    invoke-direct/range {v0 .. v14}, LX/1rW;-><init>(LX/0ad;Landroid/os/Handler;LX/0Tf;LX/0Uo;LX/0Xl;LX/0Xl;LX/0So;LX/0y3;LX/0Ot;LX/0Ot;LX/1ra;LX/0Ot;LX/0Ot;Lcom/facebook/location/foreground/GeoPixelController;)V

    .line 332276
    return-object v0
.end method

.method public static declared-synchronized d(LX/1rW;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 332392
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1rW;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 332393
    :goto_0
    monitor-exit p0

    return-void

    .line 332394
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1rW;->p:Z

    .line 332395
    const-wide/16 v0, 0x0

    invoke-static {p0}, LX/1rW;->k(LX/1rW;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 332396
    iget-object v2, p0, LX/1rW;->k:LX/1ra;

    const/4 v8, 0x0

    .line 332397
    invoke-static {v2, v8}, LX/1ra;->a(LX/1ra;Z)V

    .line 332398
    invoke-static {v2, v8}, LX/1ra;->b(LX/1ra;Z)V

    .line 332399
    invoke-static {v2, v8}, LX/1ra;->c(LX/1ra;Z)V

    .line 332400
    invoke-static {v2, v8}, LX/1ra;->d(LX/1ra;Z)V

    .line 332401
    iget-object v6, v2, LX/1ra;->c:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, v2, LX/1ra;->e:J

    .line 332402
    iput v8, v2, LX/1ra;->i:I

    .line 332403
    iput v8, v2, LX/1ra;->j:I

    .line 332404
    iput v8, v2, LX/1ra;->k:I

    .line 332405
    iput v8, v2, LX/1ra;->l:I

    .line 332406
    iput v8, v2, LX/1ra;->m:I

    .line 332407
    iput v8, v2, LX/1ra;->n:I

    .line 332408
    iput v8, v2, LX/1ra;->o:I

    .line 332409
    const-string v6, "fgl_app_foreground"

    invoke-static {v2, v6}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    .line 332410
    if-eqz v6, :cond_1

    .line 332411
    const-string v7, "next_request_delay_ms"

    invoke-virtual {v6, v7, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332412
    invoke-virtual {v6}, LX/0oG;->d()V

    .line 332413
    :cond_1
    invoke-static {p0, v0, v1}, LX/1rW;->a$redex0(LX/1rW;J)V

    .line 332414
    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 332415
    iget-object v0, p0, LX/1rW;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sN;

    .line 332416
    const-string v1, "FOREGROUND_LOCATION_CHECK_SKIPPED"

    invoke-static {v0, v1}, LX/1sN;->a(LX/1sN;Ljava/lang/String;)V

    .line 332417
    :cond_2
    iget-object v0, p0, LX/1rW;->q:LX/0Yb;

    if-nez v0, :cond_3

    .line 332418
    iget-object v0, p0, LX/1rW;->e:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    iget-object v1, p0, LX/1rW;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    sget-object v1, LX/1rz;->b:Ljava/lang/String;

    new-instance v2, LX/1s0;

    invoke-direct {v2, p0}, LX/1s0;-><init>(LX/1rW;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/1rW;->q:LX/0Yb;

    .line 332419
    iget-object v0, p0, LX/1rW;->q:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 332420
    :cond_3
    iget-object v0, p0, LX/1rW;->f:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    new-instance v2, LX/1s2;

    invoke-direct {v2, p0}, LX/1s2;-><init>(LX/1rW;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/1rW;->r:LX/0Yb;

    .line 332421
    iget-object v0, p0, LX/1rW;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 332422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized e(LX/1rW;)V
    .locals 8

    .prologue
    .line 332355
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1rW;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 332356
    :goto_0
    monitor-exit p0

    return-void

    .line 332357
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/1rW;->p:Z

    .line 332358
    iget-object v0, p0, LX/1rW;->r:LX/0Yb;

    if-eqz v0, :cond_2

    .line 332359
    iget-object v0, p0, LX/1rW;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332360
    iget-object v0, p0, LX/1rW;->r:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 332361
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/1rW;->r:LX/0Yb;

    .line 332362
    :cond_2
    invoke-static {p0}, LX/1rW;->j(LX/1rW;)V

    .line 332363
    iget-object v0, p0, LX/1rW;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    invoke-virtual {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b()V

    .line 332364
    iget-object v0, p0, LX/1rW;->k:LX/1ra;

    const/4 v2, 0x0

    const/high16 v7, -0x80000000

    .line 332365
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1ra;->a(LX/1ra;Z)V

    .line 332366
    invoke-static {v0, v2}, LX/1ra;->b(LX/1ra;Z)V

    .line 332367
    invoke-static {v0, v2}, LX/1ra;->c(LX/1ra;Z)V

    .line 332368
    invoke-static {v0, v2}, LX/1ra;->d(LX/1ra;Z)V

    .line 332369
    const-string v1, "fgl_app_background"

    invoke-static {v0, v1}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    .line 332370
    if-eqz v1, :cond_3

    .line 332371
    iget-object v2, v0, LX/1ra;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v3

    .line 332372
    iget-wide v5, v0, LX/1ra;->e:J

    sub-long/2addr v3, v5

    .line 332373
    const-string v2, "session_duration_ms"

    invoke-virtual {v1, v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 332374
    const-string v2, "session_request_count"

    iget v3, v0, LX/1ra;->i:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332375
    const-string v2, "session_scan_count"

    iget v3, v0, LX/1ra;->j:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332376
    const-string v2, "session_scan_fail_count"

    iget v3, v0, LX/1ra;->k:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332377
    const-string v2, "session_scan_success_count"

    iget v3, v0, LX/1ra;->l:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332378
    const-string v2, "session_write_count"

    iget v3, v0, LX/1ra;->m:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332379
    const-string v2, "session_write_fail_count"

    iget v3, v0, LX/1ra;->n:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332380
    const-string v2, "session_write_success_count"

    iget v3, v0, LX/1ra;->o:I

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 332381
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 332382
    :cond_3
    const-wide/high16 v1, -0x8000000000000000L

    iput-wide v1, v0, LX/1ra;->e:J

    .line 332383
    iput v7, v0, LX/1ra;->i:I

    .line 332384
    iput v7, v0, LX/1ra;->j:I

    .line 332385
    iput v7, v0, LX/1ra;->k:I

    .line 332386
    iput v7, v0, LX/1ra;->l:I

    .line 332387
    iput v7, v0, LX/1ra;->m:I

    .line 332388
    iput v7, v0, LX/1ra;->n:I

    .line 332389
    iput v7, v0, LX/1ra;->o:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332390
    goto/16 :goto_0

    .line 332391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f(LX/1rW;)V
    .locals 1

    .prologue
    .line 332456
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1rW;->l(LX/1rW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332457
    invoke-static {p0}, LX/1rW;->i(LX/1rW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332458
    :goto_0
    monitor-exit p0

    return-void

    .line 332459
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/1rW;->j(LX/1rW;)V

    .line 332460
    iget-object v0, p0, LX/1rW;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    invoke-virtual {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/1rW;)V
    .locals 1

    .prologue
    .line 332352
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1rW;->i(LX/1rW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332353
    monitor-exit p0

    return-void

    .line 332354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h$redex0(LX/1rW;)V
    .locals 12

    .prologue
    .line 332295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rW;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/1rW;->s:J

    .line 332296
    iget-object v0, p0, LX/1rW;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;

    .line 332297
    invoke-virtual {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b()V

    .line 332298
    iget-object v2, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->g:LX/1ra;

    invoke-static {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->d(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v3

    invoke-static {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)LX/1sO;

    move-result-object v4

    const/4 v6, 0x0

    .line 332299
    const/4 v5, 0x1

    invoke-static {v2, v5}, LX/1ra;->a(LX/1ra;Z)V

    .line 332300
    invoke-static {v2, v6}, LX/1ra;->b(LX/1ra;Z)V

    .line 332301
    invoke-static {v2, v6}, LX/1ra;->c(LX/1ra;Z)V

    .line 332302
    invoke-static {v2, v6}, LX/1ra;->d(LX/1ra;Z)V

    .line 332303
    iget v5, v2, LX/1ra;->j:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, LX/1ra;->j:I

    .line 332304
    iget-object v5, v2, LX/1ra;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v2, LX/1ra;->f:J

    .line 332305
    iget-wide v5, v2, LX/1ra;->f:J

    iput-wide v5, v2, LX/1ra;->g:J

    .line 332306
    const-string v5, "fgl_scan_start"

    invoke-static {v2, v5}, LX/1ra;->a(LX/1ra;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    .line 332307
    if-eqz v5, :cond_0

    .line 332308
    const-string v6, "location_manager_params"

    .line 332309
    if-nez v3, :cond_1

    .line 332310
    const/4 v8, 0x0

    .line 332311
    :goto_0
    move-object v7, v8

    .line 332312
    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332313
    const-string v6, "wifi_scan_params"

    .line 332314
    if-nez v4, :cond_4

    .line 332315
    const/4 v8, 0x0

    .line 332316
    :goto_1
    move-object v7, v8

    .line 332317
    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 332318
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 332319
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    invoke-static {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->d(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v4

    .line 332320
    iget-object v5, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 332321
    sget-char v7, LX/1rm;->g:C

    sget-wide v9, LX/0X5;->eS:J

    sget-object v8, LX/1sR;->MEDIUM_POWER:LX/1sR;

    invoke-static {v5, v7, v9, v10, v8}, LX/1rl;->a(LX/1rl;CJLX/1sR;)LX/1sR;

    move-result-object v7

    move-object v5, v7

    .line 332322
    invoke-static {v5}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b(LX/1sR;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 332323
    new-instance v5, LX/JcN;

    invoke-direct {v5}, LX/JcN;-><init>()V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 332324
    :goto_2
    move-object v4, v5

    .line 332325
    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)LX/1sO;

    move-result-object v4

    .line 332326
    iget-object v5, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->e:LX/1rl;

    .line 332327
    sget-char v6, LX/1rm;->j:C

    sget-wide v8, LX/0X5;->eT:J

    sget-object v7, LX/1sR;->MEDIUM_POWER:LX/1sR;

    invoke-static {v5, v6, v8, v9, v7}, LX/1rl;->a(LX/1rl;CJLX/1sR;)LX/1sR;

    move-result-object v6

    move-object v5, v6

    .line 332328
    invoke-static {v5}, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->b(LX/1sR;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 332329
    new-instance v5, LX/JcN;

    invoke-direct {v5}, LX/JcN;-><init>()V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 332330
    :goto_3
    move-object v4, v5

    .line 332331
    aput-object v4, v2, v3

    .line 332332
    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 332333
    new-instance v2, LX/2Xf;

    invoke-direct {v2}, LX/2Xf;-><init>()V

    invoke-static {v3, v2}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v2

    .line 332334
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v3, v2

    .line 332335
    move-object v2, v3

    .line 332336
    new-instance v3, LX/2H5;

    invoke-direct {v3, v0}, LX/2H5;-><init>(Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;)V

    invoke-static {v2, v3}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    .line 332337
    iget-object v3, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->o:LX/1Mv;

    .line 332338
    iget-object v4, v3, LX/1Mv;->b:LX/0Ve;

    move-object v3, v4

    .line 332339
    iget-object v4, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332340
    monitor-exit p0

    return-void

    .line 332341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 332342
    :cond_1
    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-virtual {v8}, LX/0mC;->c()LX/0m9;

    move-result-object v8

    const-string v9, "priority"

    iget-object v10, v3, Lcom/facebook/location/FbLocationOperationParams;->a:LX/0yF;

    invoke-virtual {v10}, LX/0yF;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v8

    const-string v9, "desired_age_ms"

    iget-wide v10, v3, Lcom/facebook/location/FbLocationOperationParams;->b:J

    invoke-virtual {v8, v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v8

    const-string v9, "desired_accuracy_meters"

    iget v10, v3, Lcom/facebook/location/FbLocationOperationParams;->c:F

    invoke-virtual {v8, v9, v10}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    move-result-object v8

    const-string v9, "timeout_ms"

    iget-wide v10, v3, Lcom/facebook/location/FbLocationOperationParams;->d:J

    invoke-virtual {v8, v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v9

    .line 332343
    iget-object v8, v3, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v8}, LX/0am;->isPresent()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 332344
    const-string v10, "age_limit_ms"

    iget-object v8, v3, Lcom/facebook/location/FbLocationOperationParams;->e:LX/0am;

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v9, v10, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    .line 332345
    :cond_2
    iget-object v8, v3, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v8}, LX/0am;->isPresent()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 332346
    const-string v10, "accuracy_limit_meters"

    iget-object v8, v3, Lcom/facebook/location/FbLocationOperationParams;->f:LX/0am;

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v9, v10, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    :cond_3
    move-object v8, v9

    .line 332347
    goto/16 :goto_0

    :cond_4
    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-virtual {v8}, LX/0mC;->c()LX/0m9;

    move-result-object v8

    const-string v9, "timeout_ms"

    iget-wide v10, v4, LX/1sO;->a:J

    invoke-virtual {v8, v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v8

    const-string v9, "age_limit_ms"

    iget-wide v10, v4, LX/1sO;->b:J

    invoke-virtual {v8, v9, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v8

    goto/16 :goto_1

    .line 332348
    :cond_5
    iget-object v5, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1sS;

    .line 332349
    sget-object v6, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v4, v6}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2

    .line 332350
    :cond_6
    iget-object v5, v0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkSignalReader;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Cc;

    .line 332351
    invoke-virtual {v5, v4}, LX/2Cc;->a(LX/1sO;)V

    goto/16 :goto_3
.end method

.method private static declared-synchronized i(LX/1rW;)V
    .locals 2

    .prologue
    .line 332290
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1rW;->j(LX/1rW;)V

    .line 332291
    invoke-static {p0}, LX/1rW;->l(LX/1rW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332292
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/1rW;->a$redex0(LX/1rW;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332293
    :cond_0
    monitor-exit p0

    return-void

    .line 332294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/1rW;)V
    .locals 2

    .prologue
    .line 332285
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rW;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 332286
    iget-object v0, p0, LX/1rW;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 332287
    const/4 v0, 0x0

    iput-object v0, p0, LX/1rW;->t:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332288
    :cond_0
    monitor-exit p0

    return-void

    .line 332289
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized k(LX/1rW;)J
    .locals 4

    .prologue
    .line 332281
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1rW;->s:J

    invoke-static {p0}, LX/1rW;->m(LX/1rW;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 332282
    iget-object v2, p0, LX/1rW;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 332283
    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 332284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized l(LX/1rW;)Z
    .locals 2

    .prologue
    .line 332280
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rW;->d:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1rW;->h:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized m(LX/1rW;)J
    .locals 10

    .prologue
    .line 332277
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1rW;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rl;

    .line 332278
    sget-wide v4, LX/1rm;->e:J

    sget-wide v6, LX/0X5;->eK:J

    const-wide/32 v8, 0x15f90

    move-object v3, v0

    invoke-static/range {v3 .. v9}, LX/1rl;->a(LX/1rl;JJJ)J

    move-result-wide v2

    move-wide v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332279
    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
