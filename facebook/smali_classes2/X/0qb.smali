.class public LX/0qb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0SG;

.field private final c:LX/0ad;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0qd;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rM;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0qe;

.field private final h:LX/0Wd;

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0rH;",
            "LX/0rJ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148004
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0qb;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;LX/0SG;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0qd;LX/0Or;LX/0qe;LX/0Wd;)V
    .locals 1
    .param p8    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            ">;",
            "LX/0SG;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0qd;",
            "LX/0Or",
            "<",
            "LX/0rM;",
            ">;",
            "LX/0qe;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148044
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0qb;->i:Ljava/util/Map;

    .line 148045
    iput-object p1, p0, LX/0qb;->a:Ljava/util/Set;

    .line 148046
    iput-object p2, p0, LX/0qb;->b:LX/0SG;

    .line 148047
    iput-object p3, p0, LX/0qb;->c:LX/0ad;

    .line 148048
    iput-object p4, p0, LX/0qb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 148049
    iput-object p5, p0, LX/0qb;->e:LX/0qd;

    .line 148050
    iput-object p6, p0, LX/0qb;->f:LX/0Or;

    .line 148051
    iput-object p7, p0, LX/0qb;->g:LX/0qe;

    .line 148052
    iput-object p8, p0, LX/0qb;->h:LX/0Wd;

    .line 148053
    return-void
.end method

.method public static a(LX/0QB;)LX/0qb;
    .locals 7

    .prologue
    .line 148016
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 148017
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 148018
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 148019
    if-nez v1, :cond_0

    .line 148020
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148021
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 148022
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 148023
    sget-object v1, LX/0qb;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 148024
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 148025
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 148026
    :cond_1
    if-nez v1, :cond_4

    .line 148027
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 148028
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 148029
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/0qb;->b(LX/0QB;)LX/0qb;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 148030
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 148031
    if-nez v1, :cond_2

    .line 148032
    sget-object v0, LX/0qb;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 148033
    :goto_1
    if-eqz v0, :cond_3

    .line 148034
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148035
    :goto_3
    check-cast v0, LX/0qb;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 148036
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 148037
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 148038
    :catchall_1
    move-exception v0

    .line 148039
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 148040
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 148041
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 148042
    :cond_2
    :try_start_8
    sget-object v0, LX/0qb;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qb;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static declared-synchronized a(LX/0qb;LX/0qg;Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;)V
    .locals 4

    .prologue
    .line 148008
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/0qg;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rH;

    .line 148009
    iget-object v1, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rJ;

    .line 148010
    if-nez v1, :cond_0

    .line 148011
    invoke-static {p0, v0}, LX/0qb;->b(LX/0qb;LX/0rH;)LX/0rJ;

    move-result-object v1

    .line 148012
    iget-object v3, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148013
    :cond_0
    invoke-virtual {v1, p1, p2}, LX/0rJ;->a(LX/0qg;Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148014
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 148015
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static a(LX/0qg;)Z
    .locals 1

    .prologue
    .line 148005
    instance-of v0, p0, LX/0qf;

    if-eqz v0, :cond_0

    check-cast p0, LX/0qf;

    .line 148006
    iget-boolean v0, p0, LX/0qf;->d:Z

    move v0, v0

    .line 148007
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/0qb;
    .locals 9

    .prologue
    .line 148000
    new-instance v0, LX/0qb;

    .line 148001
    new-instance v1, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/0qc;

    invoke-direct {v3, p0}, LX/0qc;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 148002
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0qd;->a(LX/0QB;)LX/0qd;

    move-result-object v5

    check-cast v5, LX/0qd;

    const/16 v6, 0x22a

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0qe;->a(LX/0QB;)LX/0qe;

    move-result-object v7

    check-cast v7, LX/0qe;

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v8

    check-cast v8, LX/0Wd;

    invoke-direct/range {v0 .. v8}, LX/0qb;-><init>(Ljava/util/Set;LX/0SG;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0qd;LX/0Or;LX/0qe;LX/0Wd;)V

    .line 148003
    return-object v0
.end method

.method private static declared-synchronized b(LX/0qb;LX/0rH;)LX/0rJ;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147996
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/0rJ;

    iget-object v2, p0, LX/0qb;->f:LX/0Or;

    iget-object v3, p0, LX/0qb;->c:LX/0ad;

    iget-object v1, p0, LX/0qb;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/0qb;->g:LX/0qe;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/0rJ;-><init>(LX/0rH;LX/0Or;LX/0ad;JLX/0qe;)V

    .line 147997
    iget-object v1, p0, LX/0qb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/0qb;->a:Ljava/util/Set;

    iget-object v3, p0, LX/0qb;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, LX/0rJ;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/Set;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147998
    monitor-exit p0

    return-object v0

    .line 147999
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(LX/0qg;)V
    .locals 3

    .prologue
    .line 147990
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/0qg;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rH;

    .line 147991
    iget-object v2, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rJ;

    .line 147992
    if-eqz v0, :cond_0

    .line 147993
    invoke-virtual {v0, p1}, LX/0rJ;->b(LX/0qg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147994
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147995
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized c(LX/0qg;LX/0rH;)LX/0rJ;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 148054
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rJ;

    .line 148055
    if-nez v0, :cond_0

    invoke-static {p1}, LX/0qb;->a(LX/0qg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148056
    invoke-static {p0, p2}, LX/0qb;->b(LX/0qb;LX/0rH;)LX/0rJ;

    move-result-object v0

    .line 148057
    invoke-virtual {v0, p1}, LX/0rJ;->a(LX/0qg;)V

    .line 148058
    iget-object v1, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148059
    :cond_0
    monitor-exit p0

    return-object v0

    .line 148060
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/0rH;)V
    .locals 2

    .prologue
    .line 147985
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147986
    if-nez v0, :cond_0

    .line 147987
    :goto_0
    monitor-exit p0

    return-void

    .line 147988
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0qb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {v0, v1}, LX/0rJ;->b(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147989
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147979
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0qb;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qg;

    .line 147980
    invoke-virtual {v0}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147981
    invoke-virtual {v0}, LX/0qg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;

    invoke-static {p0, v0, v1}, LX/0qb;->a(LX/0qb;LX/0qg;Lcom/facebook/clashmanagement/api/ClashUnitQueryModels$ClashUnitQueryModel$EligibleClashUnitsModel$EdgesModel$NodeModel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147983
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, LX/0qb;->b(LX/0qg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147984
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/0qg;LX/0rH;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 147958
    monitor-enter p0

    :try_start_0
    const-string v0, "ClashManager#isClashUnitEligibleForShowing"

    const v2, 0x34cd30a8

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147959
    :try_start_1
    iget-object v0, p0, LX/0qb;->e:LX/0qd;

    invoke-virtual {v0}, LX/0qd;->a()V

    .line 147960
    invoke-virtual {p0}, LX/0qb;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 147961
    const v0, -0x99fc274    # -1.137034E33f

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    .line 147962
    :cond_0
    :try_start_3
    invoke-virtual {p1, p2}, LX/0qg;->a(LX/0rH;)LX/0qi;

    move-result-object v2

    .line 147963
    iget-boolean v0, v2, LX/0qi;->a:Z

    move v0, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 147964
    if-nez v0, :cond_1

    .line 147965
    const v0, -0x5ea5d999

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    goto :goto_0

    .line 147966
    :cond_1
    :try_start_5
    invoke-direct {p0, p1, p2}, LX/0qb;->c(LX/0qg;LX/0rH;)LX/0rJ;

    move-result-object v3

    .line 147967
    if-nez v3, :cond_2

    .line 147968
    iget-object v0, p0, LX/0qb;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rM;

    invoke-virtual {v0, p2}, LX/0rM;->a(LX/0rH;)LX/0rM;

    move-result-object v0

    .line 147969
    iget-object v3, v2, LX/0qi;->b:Ljava/lang/String;

    move-object v2, v3

    .line 147970
    sget-object v3, LX/0rL;->NORMAL:LX/0rL;

    invoke-virtual {v0, p1, v2, v3}, LX/0rM;->a(LX/0qg;Ljava/lang/String;LX/0rL;)LX/0rM;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0rM;->a(Z)LX/0rM;

    move-result-object v0

    invoke-virtual {v0}, LX/0rM;->a()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 147971
    const v0, 0x3290ca9b

    :try_start_6
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v1

    goto :goto_0

    .line 147972
    :cond_2
    :try_start_7
    invoke-virtual {v3}, LX/0rJ;->a()I

    move-result v1

    .line 147973
    iget-object v0, p0, LX/0qb;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5, p1, v2}, LX/0rJ;->a(JLX/0qg;LX/0qi;)Z

    move-result v0

    .line 147974
    invoke-virtual {v3}, LX/0rJ;->a()I

    move-result v2

    if-le v2, v1, :cond_3

    .line 147975
    iget-object v1, p0, LX/0qb;->h:LX/0Wd;

    new-instance v2, Lcom/facebook/clashmanagement/manager/ClashManager$1;

    invoke-direct {v2, p0, v3}, Lcom/facebook/clashmanagement/manager/ClashManager$1;-><init>(LX/0qb;LX/0rJ;)V

    const v3, -0x51ea9df1

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 147976
    :cond_3
    const v1, 0x480b9b49

    :try_start_8
    invoke-static {v1}, LX/02m;->a(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 147977
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147978
    :catchall_1
    move-exception v0

    const v1, -0x2b7087c7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/0rH;",
            "LX/44w",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/clashmanagement/manager/ClashUnit;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 147939
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 147940
    iget-object v0, p0, LX/0qb;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147941
    new-instance v4, LX/44w;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rJ;

    iget-object v5, p0, LX/0qb;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, LX/0rJ;->a(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rJ;

    invoke-virtual {v1}, LX/0rJ;->b()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v4, v5, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 147942
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147943
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147944
    :cond_0
    monitor-exit p0

    return-object v2
.end method

.method public final declared-synchronized b(LX/0qg;LX/0rH;)Z
    .locals 4

    .prologue
    .line 147950
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0qb;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 147951
    const/4 v0, 0x1

    .line 147952
    :goto_0
    monitor-exit p0

    return v0

    .line 147953
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, LX/0qb;->c(LX/0qg;LX/0rH;)LX/0rJ;

    move-result-object v0

    .line 147954
    if-nez v0, :cond_1

    .line 147955
    const/4 v0, 0x0

    goto :goto_0

    .line 147956
    :cond_1
    iget-object v1, p0, LX/0qb;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, LX/0rJ;->a(JLX/0qg;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 147957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 147947
    iget-object v0, p0, LX/0qb;->c:LX/0ad;

    sget-short v2, LX/0rI;->d:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 147948
    if-nez v0, :cond_0

    .line 147949
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0qb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0qh;->b:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 147946
    invoke-virtual {p0}, LX/0qb;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0qb;->c:LX/0ad;

    sget-short v2, LX/0rI;->g:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 147945
    invoke-virtual {p0}, LX/0qb;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0qb;->c:LX/0ad;

    sget-short v2, LX/0rI;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
