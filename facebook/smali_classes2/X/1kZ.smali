.class public LX/1kZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final s:Ljava/lang/Object;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/storyline/annotation/IsDeviceStorylineCapable;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310030
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/1kZ;->s:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 310031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310032
    return-void
.end method

.method public static a(LX/0QB;)LX/1kZ;
    .locals 8

    .prologue
    .line 310033
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 310034
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 310035
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 310036
    if-nez v1, :cond_0

    .line 310037
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310038
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 310039
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 310040
    sget-object v1, LX/1kZ;->s:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 310041
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 310042
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 310043
    :cond_1
    if-nez v1, :cond_4

    .line 310044
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 310045
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 310046
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 310047
    new-instance p0, LX/1kZ;

    invoke-direct {p0}, LX/1kZ;-><init>()V

    .line 310048
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    .line 310049
    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v7}, LX/1ka;->a(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v7

    move-object v7, v7

    .line 310050
    check-cast v7, Ljava/lang/Boolean;

    .line 310051
    iput-object v1, p0, LX/1kZ;->a:LX/0ad;

    iput-object v7, p0, LX/1kZ;->b:Ljava/lang/Boolean;

    .line 310052
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 310053
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 310054
    if-nez v1, :cond_2

    .line 310055
    sget-object v0, LX/1kZ;->s:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kZ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 310056
    :goto_1
    if-eqz v0, :cond_3

    .line 310057
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 310058
    :goto_3
    check-cast v0, LX/1kZ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 310059
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 310060
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 310061
    :catchall_1
    move-exception v0

    .line 310062
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 310063
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 310064
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 310065
    :cond_2
    :try_start_8
    sget-object v0, LX/1kZ;->s:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kZ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 310066
    iget-object v1, p0, LX/1kZ;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 310067
    iget-object v1, p0, LX/1kZ;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1kZ;->a:LX/0ad;

    sget-short v2, LX/1vU;->m:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1kZ;->c:Ljava/lang/Boolean;

    .line 310068
    :cond_1
    iget-object v0, p0, LX/1kZ;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 5

    .prologue
    .line 310069
    iget-boolean v0, p0, LX/1kZ;->r:Z

    if-nez v0, :cond_0

    .line 310070
    iget-object v0, p0, LX/1kZ;->a:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-char v3, LX/1vU;->a:C

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1kZ;->m:Ljava/lang/String;

    .line 310071
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1kZ;->r:Z

    .line 310072
    :cond_0
    iget-object v0, p0, LX/1kZ;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()I
    .locals 3

    .prologue
    .line 310073
    iget-object v0, p0, LX/1kZ;->o:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 310074
    iget-object v0, p0, LX/1kZ;->a:LX/0ad;

    sget v1, LX/1vU;->k:I

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/1kZ;->o:Ljava/lang/Integer;

    .line 310075
    :cond_0
    iget-object v0, p0, LX/1kZ;->o:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
