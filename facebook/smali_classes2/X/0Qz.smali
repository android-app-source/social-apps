.class public final LX/0Qz;
.super LX/0R0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0R0",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/0Qy;


# direct methods
.method public constructor <init>(LX/0Qy;)V
    .locals 0

    .prologue
    .line 59305
    iput-object p1, p0, LX/0Qz;->c:LX/0Qy;

    invoke-direct {p0}, LX/0R0;-><init>()V

    .line 59306
    iput-object p0, p0, LX/0Qz;->a:LX/0R1;

    .line 59307
    iput-object p0, p0, LX/0Qz;->b:LX/0R1;

    return-void
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 59304
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 59303
    iget-object v0, p0, LX/0Qz;->a:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 59302
    iget-object v0, p0, LX/0Qz;->b:LX/0R1;

    return-object v0
.end method

.method public final setAccessTime(J)V
    .locals 0

    .prologue
    .line 59301
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 59297
    iput-object p1, p0, LX/0Qz;->a:LX/0R1;

    .line 59298
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 59299
    iput-object p1, p0, LX/0Qz;->b:LX/0R1;

    .line 59300
    return-void
.end method
