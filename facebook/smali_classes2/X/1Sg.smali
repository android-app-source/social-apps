.class public LX/1Sg;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source ""


# instance fields
.field private final a:LX/1Sh;

.field private final b:LX/1Sf;

.field private final c:LX/AUO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/database/sqlite/SQLiteDatabase;

.field private final e:I

.field private final f:LX/1Se;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Sh;LX/1Sf;LX/AUO;I)V
    .locals 3
    .param p4    # LX/AUO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 248777
    iget-object v0, p2, LX/1Sh;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 248778
    new-instance v0, LX/1Si;

    invoke-direct {v0, p0}, LX/1Si;-><init>(LX/1Sg;)V

    iput-object v0, p0, LX/1Sg;->f:LX/1Se;

    .line 248779
    iput-object p2, p0, LX/1Sg;->a:LX/1Sh;

    .line 248780
    iput-object p3, p0, LX/1Sg;->b:LX/1Sf;

    .line 248781
    iput-object p4, p0, LX/1Sg;->c:LX/AUO;

    .line 248782
    iput p5, p0, LX/1Sg;->e:I

    .line 248783
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 248784
    iget-object v0, p0, LX/1Sg;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 248785
    :goto_0
    return-void

    .line 248786
    :cond_0
    invoke-virtual {p0, p1}, LX/1Sg;->onConfigure(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 248787
    const-string v0, "DROP TABLE IF EXISTS sqliteproc_metadata"

    const v1, 0x3d71c340

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x343bae52

    invoke-static {v0}, LX/03h;->a(I)V

    .line 248788
    const-string v0, "DROP TABLE IF EXISTS sqliteproc_schema"

    const v1, -0x3993b783

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2375aa21

    invoke-static {v0}, LX/03h;->a(I)V

    .line 248789
    const-string v0, "SELECT name FROM sqlite_master WHERE type == \'table\'"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 248790
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248791
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 248792
    const-string v2, "sqlite_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 248793
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DROP TABLE "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, 0x75d3c553

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2a7ed1a3

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 248794
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 248795
    return-void
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 248796
    invoke-static {p1}, LX/1Sg;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248797
    invoke-virtual {p0, p1}, LX/1Sg;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248798
    return-void
.end method


# virtual methods
.method public final onConfigure(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 248799
    iput-object p1, p0, LX/1Sg;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 248800
    sget-object v0, LX/AUm;->a:LX/AUm;

    move-object v0, v0

    .line 248801
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/AUm;->a(Landroid/database/sqlite/SQLiteDatabase;Z)V

    .line 248802
    return-void
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 248803
    invoke-direct {p0, p1}, LX/1Sg;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248804
    const-string v0, "onCreate"

    const v1, -0x19241a63

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 248805
    :try_start_0
    new-instance v0, LX/1Sh;

    const-string v1, "sqliteproc_metadata"

    const-string v2, "65bedb99a24187d2e4fdf32357c673a9f816f1de"

    invoke-direct {v0, v1, v2}, LX/1Sh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, LX/AUZ;->a:[LX/AUP;

    sget-object v2, LX/AUZ;->b:[LX/AUQ;

    invoke-static {p1, v0, v1, v2}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;[LX/AUQ;)V

    .line 248806
    new-instance v0, LX/1Sh;

    const-string v1, "sqliteproc_schema"

    const-string v2, "cdcb84b9b6db923ff95561159f35658cd7da02d8"

    invoke-direct {v0, v1, v2}, LX/1Sh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, LX/AUe;->a:[LX/AUP;

    sget-object v2, LX/AUe;->b:[LX/AUQ;

    invoke-static {p1, v0, v1, v2}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;[LX/AUQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248807
    const v0, -0x6d56b976

    invoke-static {v0}, LX/03q;->a(I)V

    .line 248808
    return-void

    .line 248809
    :catchall_0
    move-exception v0

    const v1, 0x218143bd

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 248810
    invoke-direct {p0, p1}, LX/1Sg;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248811
    invoke-direct {p0, p1}, LX/1Sg;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248812
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 13

    .prologue
    .line 248813
    invoke-direct {p0, p1}, LX/1Sg;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248814
    new-instance v0, LX/AUA;

    iget-object v1, p0, LX/1Sg;->f:LX/1Se;

    invoke-direct {v0, v1}, LX/AUA;-><init>(LX/1Se;)V

    .line 248815
    const-string v1, "__database__"

    invoke-static {v0, v1}, LX/AUR;->a(LX/AUA;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 248816
    iget-object v1, p0, LX/1Sg;->a:LX/1Sh;

    iget-object v1, v1, LX/1Sh;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 248817
    const-string v1, "migrate"

    const v2, -0x33950501    # -6.1598716E7f

    invoke-static {v1, v2}, LX/03q;->a(Ljava/lang/String;I)V

    .line 248818
    const v1, -0x62c0f6e0

    invoke-static {p1, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248819
    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 248820
    :goto_0
    :try_start_0
    new-instance v1, LX/AUT;

    iget-object v2, p0, LX/1Sg;->f:LX/1Se;

    iget-object v3, p0, LX/1Sg;->b:LX/1Sf;

    iget v4, p0, LX/1Sg;->e:I

    invoke-direct {v1, v2, v3, v4, v0}, LX/AUT;-><init>(LX/1Se;LX/1Sf;IZ)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 248821
    iget-object v2, v1, LX/AUT;->a:LX/1Se;

    invoke-interface {v2}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 248822
    const/4 v5, 0x2

    new-array v5, v5, [LX/1Sh;

    const/4 v6, 0x0

    new-instance v8, LX/1Sh;

    const-string v9, "item"

    const-string v10, "ed2610d2d52976dc860aaafb9733be81828af9ea"

    invoke-direct {v8, v9, v10}, LX/1Sh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v6

    const/4 v6, 0x1

    new-instance v8, LX/1Sh;

    const-string v9, "unread_count"

    const-string v10, "96f1cefc1896e5d0c8f7ab77bbf0d58cc7bd2c91"

    invoke-direct {v8, v9, v10}, LX/1Sh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v8, v5, v6

    move-object v8, v5

    .line 248823
    array-length v9, v8

    move v6, v4

    move v2, v4

    :goto_1
    if-ge v6, v9, :cond_2

    .line 248824
    aget-object v10, v8, v6

    .line 248825
    iget-object v11, v1, LX/AUT;->b:LX/1Sf;

    invoke-virtual {v11, v6}, LX/1Sf;->a(I)[LX/AUP;

    move-result-object v11

    .line 248826
    invoke-static {v1, v7, v10, v11, v6}, LX/AUT;->a(LX/AUT;Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;I)I

    move-result v12

    .line 248827
    if-eq v12, v3, :cond_0

    .line 248828
    iget-object p0, v10, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {v7, p0, v11}, LX/AUR;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[LX/AUP;)V

    .line 248829
    iget-object v11, v10, LX/1Sh;->a:Ljava/lang/String;

    iget-object v10, v10, LX/1Sh;->b:Ljava/lang/String;

    invoke-static {v7, v11, v10}, LX/AUR;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 248830
    :cond_0
    const/4 v10, 0x5

    if-ne v12, v10, :cond_1

    move v2, v3

    .line 248831
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 248832
    :cond_2
    if-eqz v2, :cond_3

    .line 248833
    array-length v3, v8

    move v2, v4

    :goto_2
    if-ge v2, v3, :cond_3

    .line 248834
    aget-object v4, v8, v2

    .line 248835
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "DELETE FROM "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v4, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v6, 0x22fff4e8

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v4, -0xe380f69

    invoke-static {v4}, LX/03h;->a(I)V

    .line 248836
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 248837
    :cond_3
    iget-object v2, v1, LX/AUT;->a:LX/1Se;

    invoke-interface {v2}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iget-object v3, v1, LX/AUT;->b:LX/1Sf;

    invoke-virtual {v3}, LX/1Sf;->a()LX/1Sh;

    move-result-object v3

    iget-object v3, v3, LX/1Sh;->b:Ljava/lang/String;

    .line 248838
    const-string v4, "__database__"

    invoke-static {v2, v4, v3}, LX/AUR;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 248839
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248840
    const v0, 0x7fa57736

    invoke-static {p1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248841
    const v0, 0xf97bd43

    invoke-static {v0}, LX/03q;->a(I)V

    .line 248842
    :cond_4
    return-void

    .line 248843
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 248844
    :catchall_0
    move-exception v0

    const v1, -0x66b77d69

    invoke-static {p1, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 248845
    const v1, -0x57051bc1

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 248846
    invoke-direct {p0, p1}, LX/1Sg;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248847
    invoke-direct {p0, p1}, LX/1Sg;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 248848
    return-void
.end method
