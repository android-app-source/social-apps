.class public LX/0sp;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0sp;


# direct methods
.method public constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153251
    const-string v0, "graphql_response_cache"

    const/16 v1, 0x3e

    new-instance v2, LX/0sq;

    invoke-direct {v2}, LX/0sq;-><init>()V

    new-instance v3, LX/0ss;

    invoke-direct {v3}, LX/0ss;-><init>()V

    new-instance v4, LX/0sw;

    invoke-direct {v4}, LX/0sw;-><init>()V

    new-instance v5, LX/0sy;

    invoke-direct {v5}, LX/0sy;-><init>()V

    new-instance v6, LX/0t0;

    invoke-direct {v6}, LX/0t0;-><init>()V

    invoke-static {v2, v3, v4, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 153252
    return-void
.end method

.method public static a(LX/0QB;)LX/0sp;
    .locals 3

    .prologue
    .line 153253
    sget-object v0, LX/0sp;->a:LX/0sp;

    if-nez v0, :cond_1

    .line 153254
    const-class v1, LX/0sp;

    monitor-enter v1

    .line 153255
    :try_start_0
    sget-object v0, LX/0sp;->a:LX/0sp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 153256
    if-eqz v2, :cond_0

    .line 153257
    :try_start_1
    new-instance v0, LX/0sp;

    invoke-direct {v0}, LX/0sp;-><init>()V

    .line 153258
    move-object v0, v0

    .line 153259
    sput-object v0, LX/0sp;->a:LX/0sp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153260
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 153261
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153262
    :cond_1
    sget-object v0, LX/0sp;->a:LX/0sp;

    return-object v0

    .line 153263
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 153264
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
