.class public LX/0em;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile c:LX/0em;


# instance fields
.field private final b:LX/0en;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 104027
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/0em;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0em;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0en;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 103973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103974
    iput-object p1, p0, LX/0em;->b:LX/0en;

    .line 103975
    return-void
.end method

.method public static a(LX/0QB;)LX/0em;
    .locals 4

    .prologue
    .line 104014
    sget-object v0, LX/0em;->c:LX/0em;

    if-nez v0, :cond_1

    .line 104015
    const-class v1, LX/0em;

    monitor-enter v1

    .line 104016
    :try_start_0
    sget-object v0, LX/0em;->c:LX/0em;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 104017
    if-eqz v2, :cond_0

    .line 104018
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 104019
    new-instance p0, LX/0em;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v3

    check-cast v3, LX/0en;

    invoke-direct {p0, v3}, LX/0em;-><init>(LX/0en;)V

    .line 104020
    move-object v0, p0

    .line 104021
    sput-object v0, LX/0em;->c:LX/0em;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104022
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 104023
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104024
    :cond_1
    sget-object v0, LX/0em;->c:LX/0em;

    return-object v0

    .line 104025
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 104026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0em;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;LX/0gR;)Ljava/io/File;
    .locals 3

    .prologue
    .line 104008
    sget-object v0, LX/0gQ;->DELTA:LX/0gQ;

    invoke-static {p2, p3, p4, p5, v0}, LX/0ff;->a(Ljava/lang/String;JLjava/lang/String;LX/0gQ;)Ljava/lang/String;

    move-result-object v0

    .line 104009
    sget-object v1, LX/0gR;->NONE:LX/0gR;

    if-eq p6, v1, :cond_0

    .line 104010
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p6}, LX/0gR;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104011
    :cond_0
    move-object v0, v0

    .line 104012
    invoke-static {p1}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 104013
    invoke-static {v1, v0}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 104006
    invoke-static {p1}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 104007
    invoke-static {v0, p2}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;
    .locals 5
    .param p2    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "LX/0ff;",
            ">;)",
            "LX/0ff;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103995
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103996
    :cond_0
    const/4 v0, 0x0

    .line 103997
    :goto_0
    return-object v0

    .line 103998
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103999
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 104000
    new-instance v4, LX/0h8;

    invoke-direct {v4, p0, p1, v0}, LX/0h8;-><init>(LX/0em;Landroid/content/Context;LX/0ff;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104001
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 104002
    :cond_2
    move-object v0, v2

    .line 104003
    sget-object v1, LX/0hR;->DESCENDING:LX/0hR;

    .line 104004
    new-instance v2, LX/0hS;

    invoke-direct {v2, p0, v1}, LX/0hS;-><init>(LX/0em;LX/0hR;)V

    move-object v1, v2

    .line 104005
    invoke-static {v0, v1}, Ljava/util/Collections;->min(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h8;

    iget-object v0, v0, LX/0h8;->a:LX/0ff;

    goto :goto_0
.end method

.method public static final c(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    .line 103990
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "strings"

    invoke-static {v0, v1}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 103991
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103992
    sget-object v1, LX/0em;->a:Ljava/lang/String;

    const-string v2, "Cannot create language dir: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103993
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error creating directory for strings file"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103994
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;
    .locals 1
    .param p2    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "LX/0ff;",
            ">;)",
            "LX/0ff;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103986
    invoke-direct {p0, p1, p2}, LX/0em;->b(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;

    move-result-object v0

    .line 103987
    if-eqz v0, :cond_0

    .line 103988
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 103989
    :cond_0
    return-object v0
.end method

.method public final a(LX/0ed;)Ljava/io/File;
    .locals 6

    .prologue
    .line 103976
    iget-object v0, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v1, v0

    .line 103977
    invoke-virtual {p1}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v2

    .line 103978
    invoke-virtual {p1}, LX/0ed;->c()I

    move-result v0

    int-to-long v4, v0

    .line 103979
    sget-object v0, LX/0eh;->FBSTR:LX/0eh;

    .line 103980
    iget-object v3, p1, LX/0ed;->e:LX/0eh;

    move-object v3, v3

    .line 103981
    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 103982
    invoke-static {v1}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 103983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".fbstr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103984
    invoke-static {v0, v1}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0

    .line 103985
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/0ff;)Ljava/io/File;
    .locals 1

    .prologue
    .line 103893
    iget-object v0, p2, LX/0ff;->c:Ljava/lang/String;

    move-object v0, v0

    .line 103894
    invoke-direct {p0, p1, v0}, LX/0em;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;
    .locals 9

    .prologue
    .line 103895
    sget-object v7, LX/0gR;->NEW:LX/0gR;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v1 .. v7}, LX/0em;->a(LX/0em;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;LX/0gR;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 103896
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 103897
    invoke-virtual {p0, p1}, LX/0em;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    .line 103898
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 103899
    invoke-virtual {v0}, LX/0ff;->g()LX/0eh;

    move-result-object v1

    sget-object v7, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v1, v7, :cond_1

    .line 103900
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LX/0ff;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/0ff;->d()I

    move-result v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103901
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 103902
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 103903
    invoke-virtual {v4, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103904
    :cond_0
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103905
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 103906
    :cond_2
    move-object v0, v4

    .line 103907
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 103908
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 103909
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 103910
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ff;

    .line 103911
    sget-object v8, LX/0h7;->a:[I

    invoke-virtual {v1}, LX/0ff;->h()LX/0gQ;

    move-result-object v9

    invoke-virtual {v9}, LX/0gQ;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 103912
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 103913
    :pswitch_0
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 103914
    :pswitch_1
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 103915
    :cond_4
    invoke-virtual {p0, p1, v6}, LX/0em;->a(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;

    move-result-object v0

    .line 103916
    if-eqz v0, :cond_3

    .line 103917
    iget-object v1, v0, LX/0ff;->c:Ljava/lang/String;

    move-object v1, v1

    .line 103918
    invoke-direct {p0, p1, v1}, LX/0em;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 103919
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103920
    invoke-virtual {v0}, LX/0ff;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    .line 103921
    :try_start_0
    invoke-static {v1}, LX/1t3;->b(Ljava/io/File;)[B

    move-result-object v8

    .line 103922
    invoke-static {v8}, LX/03l;->b([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 103923
    :goto_4
    move v3, v7

    .line 103924
    if-nez v3, :cond_5

    .line 103925
    sget-object v0, LX/0em;->a:Ljava/lang/String;

    const-string v3, "Deleting invalid language file %s with file size %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v3, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103926
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 103927
    :cond_5
    iget-object v3, v0, LX/0ff;->c:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\\."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, LX/0gQ;->NEW:LX/0gQ;

    invoke-virtual {v8}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".*$"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\\."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, LX/0gQ;->DELTA:LX/0gQ;

    invoke-virtual {v8}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".*$"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 103928
    invoke-direct {p0, p1, v3}, LX/0em;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 103929
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 103930
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103931
    :goto_5
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 103932
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    :goto_6
    if-ge v1, v6, :cond_3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 103933
    iget-object v7, v0, LX/0ff;->c:Ljava/lang/String;

    move-object v7, v7

    .line 103934
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 103935
    invoke-virtual {p0, p1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 103936
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 103937
    :cond_7
    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_5

    .line 103938
    :cond_8
    return-void

    .line 103939
    :catch_0
    move-exception v8

    .line 103940
    sget-object v9, LX/0em;->a:Ljava/lang/String;

    const-string v10, "failed to read language pack %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v7

    invoke-static {v9, v8, v10, v11}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/0ed;)LX/0ff;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103941
    iget-object v0, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v2, v0

    .line 103942
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 103943
    invoke-virtual {p0, v2}, LX/0em;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ff;

    .line 103944
    invoke-virtual {v0}, LX/0ff;->g()LX/0eh;

    move-result-object v6

    sget-object v7, LX/0eh;->LANGPACK:LX/0eh;

    if-ne v6, v7, :cond_0

    invoke-virtual {v0}, LX/0ff;->h()LX/0gQ;

    move-result-object v6

    sget-object v7, LX/0gQ;->NONE:LX/0gQ;

    if-ne v6, v7, :cond_0

    invoke-virtual {v0}, LX/0ff;->d()I

    move-result v6

    invoke-virtual {p1}, LX/0ed;->c()I

    move-result v7

    if-ne v6, v7, :cond_0

    invoke-virtual {v0}, LX/0ff;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, LX/0ed;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 103945
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103946
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 103947
    :cond_1
    invoke-direct {p0, v2, v3}, LX/0em;->b(Landroid/content/Context;Ljava/util/ArrayList;)LX/0ff;

    move-result-object v0

    .line 103948
    if-eqz v0, :cond_2

    .line 103949
    :goto_1
    return-object v0

    .line 103950
    :cond_2
    invoke-virtual {p1}, LX/0ed;->c()I

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;
    .locals 9

    .prologue
    .line 103951
    sget-object v7, LX/0gR;->FAILED:LX/0gR;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-static/range {v1 .. v7}, LX/0em;->a(LX/0em;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;LX/0gR;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/0ff;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103952
    invoke-static {p1}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 103953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 103954
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 103955
    if-eqz v2, :cond_1

    .line 103956
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 103957
    new-instance v5, LX/0ff;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, LX/0ff;-><init>(Ljava/lang/String;)V

    .line 103958
    invoke-virtual {v5}, LX/0ff;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103959
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103960
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103961
    :cond_1
    return-object v1
.end method

.method public final c(LX/0ed;)Ljava/io/File;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103962
    iget-object v0, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v0, v0

    .line 103963
    invoke-virtual {p0, v0}, LX/0em;->a(Landroid/content/Context;)V

    .line 103964
    invoke-virtual {p0, p1}, LX/0em;->b(LX/0ed;)LX/0ff;

    move-result-object v0

    .line 103965
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 103966
    :cond_0
    iget-object v1, p1, LX/0ed;->a:Landroid/content/Context;

    move-object v1, v1

    .line 103967
    invoke-virtual {p0, v1, v0}, LX/0em;->a(Landroid/content/Context;LX/0ff;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Ljava/io/File;
    .locals 9

    .prologue
    .line 103968
    sget-object v7, LX/0gQ;->NEW:LX/0gQ;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    .line 103969
    invoke-static {v3, v4, v5, v6, v7}, LX/0ff;->a(Ljava/lang/String;JLjava/lang/String;LX/0gQ;)Ljava/lang/String;

    move-result-object v0

    .line 103970
    invoke-static {v2}, LX/0em;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v8

    .line 103971
    invoke-static {v8, v0}, LX/0en;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v0, v0

    .line 103972
    return-object v0
.end method
