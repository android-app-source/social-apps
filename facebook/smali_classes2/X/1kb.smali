.class public LX/1kb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1kb;


# instance fields
.field public a:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 310082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310083
    return-void
.end method

.method public static a(LX/0QB;)LX/1kb;
    .locals 4

    .prologue
    .line 310084
    sget-object v0, LX/1kb;->b:LX/1kb;

    if-nez v0, :cond_1

    .line 310085
    const-class v1, LX/1kb;

    monitor-enter v1

    .line 310086
    :try_start_0
    sget-object v0, LX/1kb;->b:LX/1kb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 310087
    if-eqz v2, :cond_0

    .line 310088
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 310089
    new-instance p0, LX/1kb;

    invoke-direct {p0}, LX/1kb;-><init>()V

    .line 310090
    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    .line 310091
    iput-object v3, p0, LX/1kb;->a:LX/0if;

    .line 310092
    move-object v0, p0

    .line 310093
    sput-object v0, LX/1kb;->b:LX/1kb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310094
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 310095
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 310096
    :cond_1
    sget-object v0, LX/1kb;->b:LX/1kb;

    return-object v0

    .line 310097
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 310098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/1kb;)V
    .locals 2

    .prologue
    .line 310099
    iget-object v0, p0, LX/1kb;->a:LX/0if;

    sget-object v1, LX/0ig;->T:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 310100
    return-void
.end method
