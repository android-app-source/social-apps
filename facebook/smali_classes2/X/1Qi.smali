.class public LX/1Qi;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Qa;
.implements LX/1PW;


# instance fields
.field private final n:LX/1Qk;

.field public final o:LX/1DQ;

.field public final p:LX/1Qh;

.field private final q:LX/1Qa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/1bO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Z


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;LX/1Qh;LX/1DQ;Landroid/content/Context;LX/1PY;LX/1Qa;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Qh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1DQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1Qa;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244988
    invoke-direct {p0, p4, p1, p5}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 244989
    new-instance v0, LX/1Qk;

    invoke-direct {v0}, LX/1Qk;-><init>()V

    iput-object v0, p0, LX/1Qi;->n:LX/1Qk;

    .line 244990
    iput-object p2, p0, LX/1Qi;->p:LX/1Qh;

    .line 244991
    iput-object p3, p0, LX/1Qi;->o:LX/1DQ;

    .line 244992
    iput-object p6, p0, LX/1Qi;->q:LX/1Qa;

    .line 244993
    return-void
.end method


# virtual methods
.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 244994
    iget-object v0, p0, LX/1Qi;->n:LX/1Qk;

    return-object v0
.end method

.method public final a(LX/1RN;LX/5S9;)V
    .locals 1

    .prologue
    .line 244985
    iget-object v0, p0, LX/1Qi;->q:LX/1Qa;

    if-eqz v0, :cond_0

    .line 244986
    iget-object v0, p0, LX/1Qi;->q:LX/1Qa;

    invoke-interface {v0, p1, p2}, LX/1Qa;->a(LX/1RN;LX/5S9;)V

    .line 244987
    :cond_0
    return-void
.end method

.method public final b()LX/1Qh;
    .locals 1

    .prologue
    .line 244995
    iget-object v0, p0, LX/1Qi;->p:LX/1Qh;

    return-object v0
.end method

.method public final b(LX/1RN;LX/5S9;)V
    .locals 1

    .prologue
    .line 244982
    iget-object v0, p0, LX/1Qi;->q:LX/1Qa;

    if-eqz v0, :cond_0

    .line 244983
    iget-object v0, p0, LX/1Qi;->q:LX/1Qa;

    invoke-interface {v0, p1, p2}, LX/1Qa;->b(LX/1RN;LX/5S9;)V

    .line 244984
    :cond_0
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 244980
    sget-object v0, LX/1Qs;->a:LX/1Qs;

    move-object v0, v0

    .line 244981
    return-object v0
.end method

.method public final d()LX/1DQ;
    .locals 1

    .prologue
    .line 244979
    iget-object v0, p0, LX/1Qi;->o:LX/1DQ;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 244978
    const/4 v0, 0x0

    return-object v0
.end method
