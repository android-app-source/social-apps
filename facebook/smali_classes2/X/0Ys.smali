.class public LX/0Ys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public a:LX/03V;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0VI;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0VI;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82644
    iput-object p1, p0, LX/0Ys;->a:LX/03V;

    .line 82645
    iput-object p2, p0, LX/0Ys;->b:LX/0Ot;

    .line 82646
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 82647
    iget-object v0, p0, LX/0Ys;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0VI;

    .line 82648
    iget-object v2, p0, LX/0Ys;->a:LX/03V;

    invoke-interface {v0}, LX/0VI;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;LX/0VI;)V

    goto :goto_0

    .line 82649
    :cond_0
    return-void
.end method
