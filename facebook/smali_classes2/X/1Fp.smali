.class public LX/1Fp;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1FE;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1FE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 224656
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method private a()LX/1FE;
    .locals 32

    .prologue
    .line 224655
    invoke-static/range {p0 .. p0}, LX/1Fq;->a(LX/0QB;)LX/1GA;

    move-result-object v2

    check-cast v2, LX/1GA;

    invoke-static/range {p0 .. p0}, LX/1An;->a(LX/0QB;)LX/1An;

    move-result-object v3

    check-cast v3, LX/1Ao;

    const-class v4, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    invoke-static/range {p0 .. p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v6

    check-cast v6, LX/1Ft;

    invoke-static/range {p0 .. p0}, LX/1GD;->a(LX/0QB;)LX/1GD;

    move-result-object v7

    check-cast v7, LX/1GF;

    invoke-static/range {p0 .. p0}, LX/1GI;->a(LX/0QB;)LX/1GL;

    move-result-object v8

    check-cast v8, LX/1GL;

    const/16 v9, 0x1488

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/1GO;->a(LX/0QB;)LX/1Gf;

    move-result-object v10

    check-cast v10, LX/1Gf;

    invoke-static/range {p0 .. p0}, LX/1Gi;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    const/16 v12, 0x14cf

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/1Ap;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v14

    check-cast v14, LX/0rb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a(LX/0QB;)Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    move-result-object v15

    check-cast v15, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    invoke-static/range {p0 .. p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v16

    check-cast v16, LX/1FZ;

    invoke-static/range {p0 .. p0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v17

    check-cast v17, LX/1FB;

    invoke-static/range {p0 .. p0}, LX/1Gq;->a(LX/0QB;)LX/1Gf;

    move-result-object v18

    check-cast v18, LX/1Gf;

    invoke-static/range {p0 .. p0}, LX/1Gs;->a(LX/0QB;)LX/1Gv;

    move-result-object v19

    check-cast v19, LX/1Gv;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v20

    check-cast v20, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1Gw;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v21

    const/16 v22, 0x483

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x482

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x481

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x488

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0xbab

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v27

    check-cast v27, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v28

    check-cast v28, LX/0tK;

    const-class v29, LX/1Gx;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/1Gx;

    const/16 v30, 0x14cc

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/1Gy;->a(LX/0QB;)LX/1Gy;

    move-result-object v31

    check-cast v31, LX/1Gy;

    invoke-static/range {v2 .. v31}, LX/1Aq;->a(LX/1GA;LX/1Ao;Landroid/content/Context;LX/0Uh;LX/1Ft;LX/1GF;LX/1GL;LX/0Or;LX/1Gf;Ljava/lang/Integer;LX/0Or;Ljava/lang/Boolean;LX/0rb;Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1FZ;LX/1FB;LX/1Gf;LX/1Gv;LX/0ad;Ljava/util/Set;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/03V;LX/0tK;LX/1Gx;LX/0Or;LX/1Gy;)LX/1FE;

    move-result-object v2

    return-object v2
.end method

.method public static a(LX/0QB;)LX/1FE;
    .locals 3

    .prologue
    .line 224645
    sget-object v0, LX/1Fp;->a:LX/1FE;

    if-nez v0, :cond_1

    .line 224646
    const-class v1, LX/1Fp;

    monitor-enter v1

    .line 224647
    :try_start_0
    sget-object v0, LX/1Fp;->a:LX/1FE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 224648
    if-eqz v2, :cond_0

    .line 224649
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Fp;->b(LX/0QB;)LX/1FE;

    move-result-object v0

    sput-object v0, LX/1Fp;->a:LX/1FE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224650
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 224651
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 224652
    :cond_1
    sget-object v0, LX/1Fp;->a:LX/1FE;

    return-object v0

    .line 224653
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 224654
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/1FE;
    .locals 32

    .prologue
    .line 224644
    invoke-static/range {p0 .. p0}, LX/1Fq;->a(LX/0QB;)LX/1GA;

    move-result-object v2

    check-cast v2, LX/1GA;

    invoke-static/range {p0 .. p0}, LX/1An;->a(LX/0QB;)LX/1An;

    move-result-object v3

    check-cast v3, LX/1Ao;

    const-class v4, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    invoke-static/range {p0 .. p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v6

    check-cast v6, LX/1Ft;

    invoke-static/range {p0 .. p0}, LX/1GD;->a(LX/0QB;)LX/1GD;

    move-result-object v7

    check-cast v7, LX/1GF;

    invoke-static/range {p0 .. p0}, LX/1GI;->a(LX/0QB;)LX/1GL;

    move-result-object v8

    check-cast v8, LX/1GL;

    const/16 v9, 0x1488

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/1GO;->a(LX/0QB;)LX/1Gf;

    move-result-object v10

    check-cast v10, LX/1Gf;

    invoke-static/range {p0 .. p0}, LX/1Gi;->a(LX/0QB;)Ljava/lang/Integer;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    const/16 v12, 0x14cf

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/1Ap;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v14

    check-cast v14, LX/0rb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;->a(LX/0QB;)Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    move-result-object v15

    check-cast v15, Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;

    invoke-static/range {p0 .. p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v16

    check-cast v16, LX/1FZ;

    invoke-static/range {p0 .. p0}, LX/1Eu;->a(LX/0QB;)LX/1FB;

    move-result-object v17

    check-cast v17, LX/1FB;

    invoke-static/range {p0 .. p0}, LX/1Gq;->a(LX/0QB;)LX/1Gf;

    move-result-object v18

    check-cast v18, LX/1Gf;

    invoke-static/range {p0 .. p0}, LX/1Gs;->a(LX/0QB;)LX/1Gv;

    move-result-object v19

    check-cast v19, LX/1Gv;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v20

    check-cast v20, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1Gw;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v21

    const/16 v22, 0x483

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x482

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x481

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x488

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0xbab

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v27

    check-cast v27, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v28

    check-cast v28, LX/0tK;

    const-class v29, LX/1Gx;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/1Gx;

    const/16 v30, 0x14cc

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/1Gy;->a(LX/0QB;)LX/1Gy;

    move-result-object v31

    check-cast v31, LX/1Gy;

    invoke-static/range {v2 .. v31}, LX/1Aq;->a(LX/1GA;LX/1Ao;Landroid/content/Context;LX/0Uh;LX/1Ft;LX/1GF;LX/1GL;LX/0Or;LX/1Gf;Ljava/lang/Integer;LX/0Or;Ljava/lang/Boolean;LX/0rb;Lcom/facebook/imagepipeline/internal/FbImageNetworkFetcher;LX/1FZ;LX/1FB;LX/1Gf;LX/1Gv;LX/0ad;Ljava/util/Set;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/03V;LX/0tK;LX/1Gx;LX/0Or;LX/1Gy;)LX/1FE;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224643
    invoke-direct {p0}, LX/1Fp;->a()LX/1FE;

    move-result-object v0

    return-object v0
.end method
