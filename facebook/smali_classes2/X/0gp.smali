.class public LX/0gp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0nx;

.field public d:LX/0iW;

.field public e:LX/0iW;

.field private f:Z

.field private g:Z

.field public h:Z

.field public final i:LX/0nv;

.field private final j:LX/0oI;

.field public final k:LX/0ad;

.field public final l:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 113471
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, LX/0gp;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(LX/0nv;LX/0oI;LX/0ad;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 113472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113473
    sget-object v0, LX/0gp;->a:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, LX/0gp;->b:Ljava/lang/ref/WeakReference;

    .line 113474
    iput-boolean v1, p0, LX/0gp;->f:Z

    .line 113475
    iput-boolean v1, p0, LX/0gp;->g:Z

    .line 113476
    iput-boolean v1, p0, LX/0gp;->h:Z

    .line 113477
    iput-object p1, p0, LX/0gp;->i:LX/0nv;

    .line 113478
    iput-object p2, p0, LX/0gp;->j:LX/0oI;

    .line 113479
    iput-object p3, p0, LX/0gp;->k:LX/0ad;

    .line 113480
    iput-object p4, p0, LX/0gp;->l:LX/03V;

    .line 113481
    return-void
.end method

.method public static a(LX/0QB;)LX/0gp;
    .locals 7

    .prologue
    .line 113482
    const-class v1, LX/0gp;

    monitor-enter v1

    .line 113483
    :try_start_0
    sget-object v0, LX/0gp;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 113484
    sput-object v2, LX/0gp;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 113485
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113486
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 113487
    new-instance p0, LX/0gp;

    .line 113488
    new-instance v3, LX/0nu;

    invoke-direct {v3}, LX/0nu;-><init>()V

    .line 113489
    move-object v3, v3

    .line 113490
    move-object v3, v3

    .line 113491
    check-cast v3, LX/0nv;

    invoke-static {v0}, LX/0oI;->a(LX/0QB;)LX/0oI;

    move-result-object v4

    check-cast v4, LX/0oI;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0gp;-><init>(LX/0nv;LX/0oI;LX/0ad;LX/03V;)V

    .line 113492
    move-object v0, p0

    .line 113493
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 113494
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0gp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113495
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 113496
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static g(LX/0gp;)V
    .locals 3

    .prologue
    .line 113497
    invoke-virtual {p0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113498
    :cond_0
    :goto_0
    return-void

    .line 113499
    :cond_1
    iget-boolean v0, p0, LX/0gp;->h:Z

    if-eqz v0, :cond_0

    .line 113500
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0gp;->h:Z

    .line 113501
    iget-object v0, p0, LX/0gp;->d:LX/0iW;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/0gp;->f:Z

    if-eqz v0, :cond_2

    .line 113502
    iget-object v0, p0, LX/0gp;->d:LX/0iW;

    iget-object v1, p0, LX/0gp;->c:LX/0nx;

    .line 113503
    iget-object v2, v1, LX/0nx;->e:Ljava/util/ArrayList;

    move-object v1, v2

    .line 113504
    invoke-virtual {v0, v1}, LX/0iW;->b(Ljava/util/List;)V

    .line 113505
    :cond_2
    iget-object v0, p0, LX/0gp;->e:LX/0iW;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0gp;->g:Z

    if-eqz v0, :cond_0

    .line 113506
    iget-object v0, p0, LX/0gp;->e:LX/0iW;

    iget-object v1, p0, LX/0gp;->c:LX/0nx;

    .line 113507
    iget-object v2, v1, LX/0nx;->f:Ljava/util/ArrayList;

    move-object v1, v2

    .line 113508
    invoke-virtual {v0, v1}, LX/0iW;->b(Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 113509
    const/16 v0, -0x32

    if-ge p1, v0, :cond_1

    iget-boolean v0, p0, LX/0gp;->h:Z

    if-nez v0, :cond_1

    .line 113510
    invoke-virtual {p0}, LX/0gp;->c()V

    .line 113511
    :cond_0
    :goto_0
    return-void

    .line 113512
    :cond_1
    const/16 v0, 0x32

    if-le p1, v0, :cond_0

    iget-boolean v0, p0, LX/0gp;->h:Z

    if-eqz v0, :cond_0

    .line 113513
    invoke-static {p0}, LX/0gp;->g(LX/0gp;)V

    goto :goto_0
.end method

.method public final varargs a(Landroid/view/View;II[Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 113514
    if-eqz p4, :cond_0

    aget-object v2, p4, v0

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, LX/0gp;->f:Z

    .line 113515
    new-instance v0, LX/0iW;

    move v2, p2

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/0iW;-><init>(ZIILandroid/view/View;[Landroid/view/View;)V

    iput-object v0, p0, LX/0gp;->d:LX/0iW;

    .line 113516
    return-void
.end method

.method public final varargs a([Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 113517
    if-eqz p1, :cond_0

    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0gp;->g:Z

    .line 113518
    new-instance v0, LX/0iW;

    const/4 v4, 0x0

    move v2, v1

    move v3, v1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/0iW;-><init>(ZIILandroid/view/View;[Landroid/view/View;)V

    iput-object v0, p0, LX/0gp;->e:LX/0iW;

    .line 113519
    return-void

    :cond_0
    move v0, v1

    .line 113520
    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 113521
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 113522
    if-nez v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    iget-object v0, p0, LX/0gp;->j:LX/0oI;

    invoke-virtual {v0}, LX/0oI;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    .line 113523
    iget-object v1, p0, LX/0gp;->k:LX/0ad;

    sget-short v2, LX/0fe;->aG:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/0gp;->k:LX/0ad;

    sget-short v2, LX/0hK;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 113524
    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/0gp;->f:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/0gp;->g:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 113525
    invoke-virtual {p0}, LX/0gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113526
    :cond_0
    :goto_0
    return-void

    .line 113527
    :cond_1
    iget-boolean v0, p0, LX/0gp;->h:Z

    if-nez v0, :cond_0

    .line 113528
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0gp;->h:Z

    .line 113529
    iget-object v0, p0, LX/0gp;->d:LX/0iW;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/0gp;->f:Z

    if-eqz v0, :cond_2

    .line 113530
    iget-object v0, p0, LX/0gp;->d:LX/0iW;

    iget-object v1, p0, LX/0gp;->c:LX/0nx;

    .line 113531
    iget-object v2, v1, LX/0nx;->e:Ljava/util/ArrayList;

    move-object v1, v2

    .line 113532
    invoke-virtual {v0, v1}, LX/0iW;->a(Ljava/util/List;)V

    .line 113533
    :cond_2
    iget-object v0, p0, LX/0gp;->e:LX/0iW;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0gp;->g:Z

    if-eqz v0, :cond_0

    .line 113534
    iget-object v0, p0, LX/0gp;->e:LX/0iW;

    iget-object v1, p0, LX/0gp;->c:LX/0nx;

    .line 113535
    iget-object v2, v1, LX/0nx;->f:Ljava/util/ArrayList;

    move-object v1, v2

    .line 113536
    invoke-virtual {v0, v1}, LX/0iW;->a(Ljava/util/List;)V

    goto :goto_0
.end method
