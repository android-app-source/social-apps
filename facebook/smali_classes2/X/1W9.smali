.class public LX/1W9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1WA;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1W9;


# instance fields
.field private a:LX/0Zr;

.field private b:LX/Bnw;


# direct methods
.method public constructor <init>(LX/0Zr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267611
    iput-object p1, p0, LX/1W9;->a:LX/0Zr;

    .line 267612
    return-void
.end method

.method public static a(LX/0QB;)LX/1W9;
    .locals 4

    .prologue
    .line 267613
    sget-object v0, LX/1W9;->c:LX/1W9;

    if-nez v0, :cond_1

    .line 267614
    const-class v1, LX/1W9;

    monitor-enter v1

    .line 267615
    :try_start_0
    sget-object v0, LX/1W9;->c:LX/1W9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 267616
    if-eqz v2, :cond_0

    .line 267617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 267618
    new-instance p0, LX/1W9;

    invoke-static {v0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v3

    check-cast v3, LX/0Zr;

    invoke-direct {p0, v3}, LX/1W9;-><init>(LX/0Zr;)V

    .line 267619
    move-object v0, p0

    .line 267620
    sput-object v0, LX/1W9;->c:LX/1W9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 267622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 267623
    :cond_1
    sget-object v0, LX/1W9;->c:LX/1W9;

    return-object v0

    .line 267624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 267625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/text/Layout;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.Thread.start"
        }
    .end annotation

    .prologue
    .line 267626
    iget-object v0, p0, LX/1W9;->b:LX/Bnw;

    if-nez v0, :cond_0

    .line 267627
    iget-object v0, p0, LX/1W9;->a:LX/0Zr;

    const-string v1, "FbGlyphWarmer"

    sget-object v2, LX/0TP;->NORMAL:LX/0TP;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;Z)Landroid/os/HandlerThread;

    move-result-object v0

    .line 267628
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 267629
    new-instance v1, LX/Bnw;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Bnw;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/1W9;->b:LX/Bnw;

    .line 267630
    :cond_0
    iget-object v0, p0, LX/1W9;->b:LX/Bnw;

    iget-object v1, p0, LX/1W9;->b:LX/Bnw;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, LX/Bnw;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bnw;->sendMessage(Landroid/os/Message;)Z

    .line 267631
    return-void
.end method
