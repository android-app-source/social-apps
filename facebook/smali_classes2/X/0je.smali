.class public abstract LX/0je;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vf;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private isDisposed:Z

.field private listeners:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/common/dispose/DisposeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124528
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0je;->isDisposed:Z

    .line 124529
    const/4 v0, 0x0

    iput-object v0, p0, LX/0je;->listeners:LX/0UE;

    return-void
.end method


# virtual methods
.method public final addDisposeListener(LX/32U;)V
    .locals 2

    .prologue
    .line 124530
    const/4 v0, 0x0

    .line 124531
    monitor-enter p0

    .line 124532
    :try_start_0
    invoke-virtual {p0}, LX/0je;->isDisposed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124533
    const/4 v0, 0x1

    .line 124534
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124535
    if-eqz v0, :cond_0

    .line 124536
    invoke-virtual {p1, p0}, LX/32U;->a(LX/0je;)V

    .line 124537
    :cond_0
    return-void

    .line 124538
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/0je;->listeners:LX/0UE;

    if-nez v1, :cond_2

    .line 124539
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    iput-object v1, p0, LX/0je;->listeners:LX/0UE;

    .line 124540
    :cond_2
    iget-object v1, p0, LX/0je;->listeners:LX/0UE;

    invoke-virtual {v1, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124541
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final dispose()V
    .locals 3

    .prologue
    .line 124542
    monitor-enter p0

    .line 124543
    :try_start_0
    invoke-virtual {p0}, LX/0je;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124544
    monitor-exit p0

    .line 124545
    :cond_0
    :goto_0
    return-void

    .line 124546
    :cond_1
    invoke-virtual {p0}, LX/0je;->disposeInternal()V

    .line 124547
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0je;->isDisposed:Z

    .line 124548
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124549
    iget-object v0, p0, LX/0je;->listeners:LX/0UE;

    if-eqz v0, :cond_0

    .line 124550
    const/4 v0, 0x0

    iget-object v1, p0, LX/0je;->listeners:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 124551
    iget-object v0, p0, LX/0je;->listeners:LX/0UE;

    invoke-virtual {v0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32U;

    invoke-virtual {v0, p0}, LX/32U;->a(LX/0je;)V

    .line 124552
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 124553
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 124554
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/0je;->listeners:LX/0UE;

    goto :goto_0
.end method

.method public abstract disposeInternal()V
.end method

.method public final declared-synchronized isDisposed()Z
    .locals 1

    .prologue
    .line 124555
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0je;->isDisposed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
