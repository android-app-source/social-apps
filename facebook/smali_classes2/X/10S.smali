.class public final enum LX/10S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/10S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/10S;

.field public static final enum ELIGIBLE:LX/10S;

.field public static final enum INELIGIBLE:LX/10S;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169150
    new-instance v0, LX/10S;

    const-string v1, "ELIGIBLE"

    invoke-direct {v0, v1, v2}, LX/10S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 169151
    new-instance v0, LX/10S;

    const-string v1, "INELIGIBLE"

    invoke-direct {v0, v1, v3}, LX/10S;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 169152
    const/4 v0, 0x2

    new-array v0, v0, [LX/10S;

    sget-object v1, LX/10S;->ELIGIBLE:LX/10S;

    aput-object v1, v0, v2

    sget-object v1, LX/10S;->INELIGIBLE:LX/10S;

    aput-object v1, v0, v3

    sput-object v0, LX/10S;->$VALUES:[LX/10S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/10S;
    .locals 1

    .prologue
    .line 169155
    const-class v0, LX/10S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/10S;

    return-object v0
.end method

.method public static values()[LX/10S;
    .locals 1

    .prologue
    .line 169153
    sget-object v0, LX/10S;->$VALUES:[LX/10S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/10S;

    return-object v0
.end method
