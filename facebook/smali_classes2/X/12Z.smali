.class public LX/12Z;
.super LX/0hi;
.source ""


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12a;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/12c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/12a;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/12c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 175498
    invoke-direct {p0}, LX/0hi;-><init>()V

    .line 175499
    iput-object p1, p0, LX/12Z;->a:LX/0Ot;

    .line 175500
    iput-object p2, p0, LX/12Z;->b:LX/0Ot;

    .line 175501
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 175502
    iput-object v0, p0, LX/12Z;->a:LX/0Ot;

    .line 175503
    iput-object v0, p0, LX/12Z;->b:LX/0Ot;

    .line 175504
    invoke-super {p0}, LX/0hi;->a()V

    .line 175505
    return-void
.end method

.method public final a(Landroid/app/Activity;LX/0YX;)V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 175506
    :try_start_0
    const-string v0, "FbMainTabFragmentBroadcastReceiverControllerCallbacksDispatcher.addActionReceiver"

    const v1, 0x541a423e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175507
    iget-object v0, p0, LX/12Z;->a:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12Z;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12Z;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12a;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 175508
    :try_start_1
    const-string v0, "FbMainTabFragmentFeedDataLoaderInitializerController"

    const v1, 0x21f03137

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175509
    iget-object v0, p0, LX/12Z;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12a;

    .line 175510
    iget-object v1, v0, LX/12a;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0hv;

    .line 175511
    const-string p0, "com.facebook.feed.util.NAVIGATE_TO_FEED_INTERACTION"

    new-instance p1, LX/12b;

    invoke-direct {p1, v0, v1}, LX/12b;-><init>(LX/12a;LX/0hv;)V

    invoke-interface {p2, p0, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175512
    const v0, 0x737ac03c

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175513
    :cond_0
    const v0, 0x683eab29

    invoke-static {v0}, LX/02m;->a(I)V

    .line 175514
    return-void

    .line 175515
    :catchall_0
    move-exception v0

    const v1, 0x6c235535

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 175516
    :catchall_1
    move-exception v0

    const v1, -0x1011d32b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Landroid/app/Activity;LX/0YX;)V
    .locals 4
    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 175517
    :try_start_0
    const-string v0, "FbMainTabFragmentBroadcastReceiverControllerCallbacksDispatcher.addBackgroundActionReceiver"

    const v1, 0x5adde272

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175518
    iget-object v0, p0, LX/12Z;->b:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/12Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12c;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 175519
    :try_start_1
    const-string v0, "FbMainTabFragmentAppEnteredLeftBroadcastController"

    const v1, -0x396a58a5

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 175520
    iget-object v0, p0, LX/12Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12c;

    .line 175521
    iget-object v1, v0, LX/12c;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/12d;

    .line 175522
    iget-object v2, v1, LX/12d;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/12e;

    .line 175523
    move-object v1, v2

    .line 175524
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v3, LX/12f;

    invoke-direct {v3, v0, v1, p1}, LX/12f;-><init>(LX/12c;LX/12e;Landroid/app/Activity;)V

    invoke-interface {p2, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance p0, LX/12g;

    invoke-direct {p0, v0, v1, p1}, LX/12g;-><init>(LX/12c;LX/12e;Landroid/app/Activity;)V

    invoke-interface {v2, v3, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175525
    const v0, 0x15c8d496

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175526
    :cond_0
    const v0, -0x7851707b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 175527
    return-void

    .line 175528
    :catchall_0
    move-exception v0

    const v1, 0x5c47a408

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 175529
    :catchall_1
    move-exception v0

    const v1, -0x454843e5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
