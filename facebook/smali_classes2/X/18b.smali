.class public LX/18b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile t:LX/18b;


# instance fields
.field private volatile a:Z

.field private volatile b:Ljava/lang/String;

.field private volatile c:Ljava/lang/String;

.field private volatile d:Ljava/lang/String;

.field public volatile e:Ljava/lang/String;

.field public volatile f:Ljava/lang/String;

.field public volatile g:Ljava/lang/String;

.field public volatile h:Ljava/lang/String;

.field public volatile i:I

.field public j:J

.field public k:Landroid/telephony/PhoneStateListener;

.field public l:Ljava/lang/reflect/Method;

.field public final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/440;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/telephony/SignalStrength;

.field private final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/carrier/CarrierMonitorListener;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Landroid/telephony/TelephonyManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206791
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/18b;->j:J

    .line 206792
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/18b;->a:Z

    .line 206793
    new-instance v0, LX/026;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/18b;->m:Ljava/util/Map;

    .line 206794
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/18b;->o:Ljava/util/Set;

    .line 206795
    return-void
.end method

.method public static a(LX/0QB;)LX/18b;
    .locals 7

    .prologue
    .line 206775
    sget-object v0, LX/18b;->t:LX/18b;

    if-nez v0, :cond_1

    .line 206776
    const-class v1, LX/18b;

    monitor-enter v1

    .line 206777
    :try_start_0
    sget-object v0, LX/18b;->t:LX/18b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 206778
    if-eqz v2, :cond_0

    .line 206779
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 206780
    new-instance p0, LX/18b;

    invoke-direct {p0}, LX/18b;-><init>()V

    .line 206781
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    .line 206782
    iput-object v3, p0, LX/18b;->p:Landroid/content/Context;

    iput-object v4, p0, LX/18b;->q:Landroid/os/Handler;

    iput-object v5, p0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    iput-object v6, p0, LX/18b;->s:LX/0So;

    .line 206783
    move-object v0, p0

    .line 206784
    sput-object v0, LX/18b;->t:LX/18b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206785
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 206786
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 206787
    :cond_1
    sget-object v0, LX/18b;->t:LX/18b;

    return-object v0

    .line 206788
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 206789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(II)Z
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/16 v3, -0x32

    .line 206768
    const/high16 v1, -0x80000000

    if-ne p0, v1, :cond_1

    .line 206769
    :cond_0
    :goto_0
    return v0

    .line 206770
    :cond_1
    if-le p1, v3, :cond_2

    rsub-int/lit8 v1, p1, -0x32

    move v2, v1

    .line 206771
    :goto_1
    if-le p0, v3, :cond_3

    rsub-int/lit8 v1, p0, -0x32

    .line 206772
    :goto_2
    if-ge v2, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 206773
    :cond_2
    add-int/lit8 v1, p1, 0x7d

    move v2, v1

    goto :goto_1

    .line 206774
    :cond_3
    add-int/lit8 v1, p0, 0x7d

    goto :goto_2
.end method

.method public static b(LX/18b;I)V
    .locals 2

    .prologue
    .line 206766
    iget-object v0, p0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    new-instance v1, LX/1iF;

    invoke-direct {v1, p0}, LX/1iF;-><init>(LX/18b;)V

    invoke-virtual {v0, v1, p1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 206767
    return-void
.end method

.method public static k(LX/18b;)V
    .locals 3

    .prologue
    .line 206757
    iget-boolean v0, p0, LX/18b;->a:Z

    if-nez v0, :cond_1

    .line 206758
    monitor-enter p0

    .line 206759
    :try_start_0
    iget-boolean v0, p0, LX/18b;->a:Z

    if-nez v0, :cond_0

    .line 206760
    invoke-static {p0}, LX/18b;->m(LX/18b;)V

    .line 206761
    iget-object v0, p0, LX/18b;->q:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/common/carrier/CarrierMonitor$1;

    invoke-direct {v1, p0}, Lcom/facebook/common/carrier/CarrierMonitor$1;-><init>(LX/18b;)V

    const v2, 0x192d6a98

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 206762
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/18b;->a:Z

    .line 206763
    :cond_0
    monitor-exit p0

    .line 206764
    :cond_1
    return-void

    .line 206765
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static declared-synchronized m(LX/18b;)V
    .locals 2

    .prologue
    .line 206731
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/18b;->b:Ljava/lang/String;

    .line 206732
    iget-object v0, p0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/18b;->c:Ljava/lang/String;

    .line 206733
    const-string v0, ""

    iput-object v0, p0, LX/18b;->d:Ljava/lang/String;

    .line 206734
    const-string v0, ""

    iput-object v0, p0, LX/18b;->e:Ljava/lang/String;

    .line 206735
    const-string v0, ""

    iput-object v0, p0, LX/18b;->f:Ljava/lang/String;

    .line 206736
    const-string v0, ""

    iput-object v0, p0, LX/18b;->g:Ljava/lang/String;

    .line 206737
    const-string v0, ""

    iput-object v0, p0, LX/18b;->h:Ljava/lang/String;

    .line 206738
    const/4 v0, 0x0

    iput v0, p0, LX/18b;->i:I

    .line 206739
    const/4 v0, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206740
    :try_start_1
    invoke-virtual {p0}, LX/18b;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206741
    iget-object v1, p0, LX/18b;->r:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result-object v0

    .line 206742
    :cond_0
    :goto_0
    move-object v0, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206743
    if-nez v0, :cond_2

    .line 206744
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 206745
    :cond_2
    :try_start_3
    instance-of v1, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v1, :cond_3

    .line 206746
    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    .line 206747
    const/4 v1, 0x2

    iput v1, p0, LX/18b;->i:I

    .line 206748
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/18b;->d:Ljava/lang/String;

    .line 206749
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/18b;->e:Ljava/lang/String;

    .line 206750
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/18b;->f:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 206751
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 206752
    :cond_3
    :try_start_4
    instance-of v1, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v1, :cond_1

    .line 206753
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 206754
    const/4 v1, 0x1

    iput v1, p0, LX/18b;->i:I

    .line 206755
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/18b;->g:Ljava/lang/String;

    .line 206756
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/18b;->h:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    goto :goto_0
.end method

.method public static o(LX/18b;)V
    .locals 3

    .prologue
    .line 206726
    iget-object v1, p0, LX/18b;->o:Ljava/util/Set;

    monitor-enter v1

    .line 206727
    :try_start_0
    iget-object v0, p0, LX/18b;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eho;

    .line 206728
    iget-object p0, v0, LX/Eho;->a:LX/2GT;

    invoke-static {p0}, LX/2GT;->f(LX/2GT;)V

    .line 206729
    goto :goto_0

    .line 206730
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/util/Pair;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 206722
    iget-object v0, p0, LX/18b;->n:Landroid/telephony/SignalStrength;

    .line 206723
    if-nez v0, :cond_0

    .line 206724
    const/4 v0, 0x0

    .line 206725
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, LX/18b;->a(ILandroid/telephony/SignalStrength;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILandroid/telephony/SignalStrength;)Landroid/util/Pair;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/telephony/SignalStrength;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/high16 v3, -0x80000000

    .line 206708
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/18b;->l:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 206709
    :try_start_0
    const-string v1, "lte"

    iget-object v0, p0, LX/18b;->l:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 206710
    :goto_0
    return-object v0

    .line 206711
    :catch_0
    :cond_0
    invoke-virtual {p2}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v1

    .line 206712
    const-string v0, "cdma"

    .line 206713
    invoke-virtual {p2}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v2

    invoke-static {v1, v2}, LX/18b;->a(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 206714
    invoke-virtual {p2}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v1

    .line 206715
    const-string v0, "evdo"

    .line 206716
    :cond_1
    invoke-virtual {p2}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v2

    .line 206717
    if-ltz v2, :cond_4

    const/16 v4, 0x1f

    if-gt v2, v4, :cond_4

    .line 206718
    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x71

    .line 206719
    :goto_1
    invoke-static {v1, v2}, LX/18b;->a(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 206720
    const-string v0, "gsm"

    .line 206721
    :goto_2
    if-ne v2, v3, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206706
    invoke-static {p0}, LX/18b;->k(LX/18b;)V

    .line 206707
    iget-object v0, p0, LX/18b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/440;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 206687
    iget-object v0, p0, LX/18b;->q:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/common/carrier/CarrierMonitor$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/common/carrier/CarrierMonitor$3;-><init>(LX/18b;LX/440;)V

    const v2, -0x3f13e45

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 206688
    return-void
.end method

.method public final a(LX/Eho;)V
    .locals 2

    .prologue
    .line 206703
    iget-object v1, p0, LX/18b;->o:Ljava/util/Set;

    monitor-enter v1

    .line 206704
    :try_start_0
    iget-object v0, p0, LX/18b;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 206705
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206701
    invoke-static {p0}, LX/18b;->k(LX/18b;)V

    .line 206702
    iget-object v0, p0, LX/18b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/440;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 206699
    iget-object v0, p0, LX/18b;->q:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/common/carrier/CarrierMonitor$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/common/carrier/CarrierMonitor$4;-><init>(LX/18b;LX/440;)V

    const v2, 0x715b0e90

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 206700
    return-void
.end method

.method public final b(LX/Eho;)V
    .locals 2

    .prologue
    .line 206696
    iget-object v1, p0, LX/18b;->o:Ljava/util/Set;

    monitor-enter v1

    .line 206697
    :try_start_0
    iget-object v0, p0, LX/18b;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 206698
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206694
    invoke-static {p0}, LX/18b;->k(LX/18b;)V

    .line 206695
    iget-object v0, p0, LX/18b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 206692
    :try_start_0
    iget-object v1, p0, LX/18b;->p:Landroid/content/Context;

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/18b;->p:Landroid/content/Context;

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 206693
    :cond_1
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method public final j()LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206689
    iget-wide v0, p0, LX/18b;->j:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 206690
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 206691
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/18b;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/18b;->j:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method
