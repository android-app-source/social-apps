.class public LX/1C7;
.super LX/1C6;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1C8;",
            "Ljava/util/Set",
            "<",
            "LX/04A;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/04A;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/04F;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/1C7;


# instance fields
.field private final b:LX/0So;

.field public final c:LX/03V;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7KD;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 215500
    const-class v0, LX/1C7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1C7;->a:Ljava/lang/String;

    .line 215501
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 215502
    sput-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->REQUESTED_PLAY:LX/1C8;

    sget-object v2, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215503
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->PLAYING:LX/1C8;

    sget-object v2, LX/04A;->VIDEO_START:LX/04A;

    sget-object v3, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    invoke-static {v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215504
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->PAUSED:LX/1C8;

    sget-object v2, LX/04A;->VIDEO_PAUSE:LX/04A;

    sget-object v3, LX/04A;->VIDEO_COMPLETE:LX/04A;

    sget-object v4, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    invoke-static {v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215505
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->ERROR:LX/1C8;

    sget-object v2, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215506
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->SEEKING:LX/1C8;

    sget-object v2, LX/04A;->VIDEO_SEEK:LX/04A;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215507
    sget-object v0, LX/04A;->VIDEO_REQUESTED_PLAYING:LX/04A;

    sget-object v1, LX/04A;->VIDEO_START:LX/04A;

    sget-object v2, LX/04A;->VIDEO_UNPAUSED:LX/04A;

    sget-object v3, LX/04A;->VIDEO_PAUSE:LX/04A;

    sget-object v4, LX/04A;->VIDEO_COMPLETE:LX/04A;

    sget-object v5, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    new-array v6, v12, [LX/04A;

    sget-object v7, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    aput-object v7, v6, v10

    sget-object v7, LX/04A;->VIDEO_SEEK:LX/04A;

    aput-object v7, v6, v11

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1C7;->g:Ljava/util/Set;

    .line 215508
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 215509
    sput-object v7, LX/1C7;->h:Ljava/util/Map;

    sget-object v8, LX/04F;->PLAYER_ORIGIN:LX/04F;

    const-string v0, "newsfeed"

    const-string v1, "user_timeline"

    const-string v2, "page_timeline"

    const-string v3, "permalink"

    const-string v4, "video_home"

    const-string v5, "group"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const-string v9, "event"

    aput-object v9, v6, v10

    const-string v9, "instant_articles"

    aput-object v9, v6, v11

    const-string v9, "other"

    aput-object v9, v6, v12

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-interface {v7, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215510
    return-void
.end method

.method public constructor <init>(LX/0So;LX/03V;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 215494
    const-wide/32 v0, 0xf731400

    const v2, 0x186a0

    invoke-direct {p0, p1, v0, v1, v2}, LX/1C6;-><init>(LX/0So;JI)V

    .line 215495
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1C7;->d:Ljava/util/Map;

    .line 215496
    iput-object p1, p0, LX/1C7;->b:LX/0So;

    .line 215497
    iput-object p2, p0, LX/1C7;->c:LX/03V;

    .line 215498
    iput-object p3, p0, LX/1C7;->e:LX/0ad;

    .line 215499
    return-void
.end method

.method public static a(LX/0QB;)LX/1C7;
    .locals 6

    .prologue
    .line 215481
    sget-object v0, LX/1C7;->i:LX/1C7;

    if-nez v0, :cond_1

    .line 215482
    const-class v1, LX/1C7;

    monitor-enter v1

    .line 215483
    :try_start_0
    sget-object v0, LX/1C7;->i:LX/1C7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 215484
    if-eqz v2, :cond_0

    .line 215485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 215486
    new-instance p0, LX/1C7;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/1C7;-><init>(LX/0So;LX/03V;LX/0ad;)V

    .line 215487
    move-object v0, p0

    .line 215488
    sput-object v0, LX/1C7;->i:LX/1C7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215489
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 215490
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215491
    :cond_1
    sget-object v0, LX/1C7;->i:LX/1C7;

    return-object v0

    .line 215492
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 215493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 215480
    iget-object v0, p0, LX/1C7;->e:LX/0ad;

    sget-short v1, LX/0ws;->eL:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static f(LX/04A;)Z
    .locals 2

    .prologue
    .line 215511
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->PLAYING:LX/1C8;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static g(LX/04A;)Z
    .locals 2

    .prologue
    .line 215479
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->PAUSED:LX/1C8;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static h(LX/04A;)Z
    .locals 2

    .prologue
    .line 215445
    sget-object v0, LX/1C7;->f:Ljava/util/Map;

    sget-object v1, LX/1C8;->REQUESTED_PLAY:LX/1C8;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/7KB;)V
    .locals 0

    .prologue
    .line 215478
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 215447
    invoke-super {p0, p1}, LX/1C6;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 215448
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 215449
    const/4 v0, 0x0

    .line 215450
    invoke-direct {p0}, LX/1C7;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215451
    sget-object v1, LX/04F;->VIDEO_ID:LX/04F;

    iget-object v1, v1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 215452
    const/4 v1, 0x0

    .line 215453
    if-eqz v5, :cond_2

    .line 215454
    invoke-virtual {p0, v5}, LX/1C6;->a(Ljava/lang/String;)LX/7KB;

    move-result-object v3

    .line 215455
    iget-boolean v1, v3, LX/7KB;->d:Z

    move v1, v1

    .line 215456
    if-nez v1, :cond_5

    .line 215457
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Session Entry is not ok"

    const-string v6, "Session transition failure"

    invoke-direct {v0, v1, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 215458
    :goto_0
    iget-object v0, p0, LX/1C7;->d:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7KD;

    .line 215459
    if-nez v0, :cond_0

    .line 215460
    new-instance v0, LX/7KD;

    iget-object v6, p0, LX/1C7;->b:LX/0So;

    invoke-direct {v0, v6}, LX/7KD;-><init>(LX/0So;)V

    .line 215461
    iget-object v6, p0, LX/1C7;->d:Ljava/util/Map;

    invoke-interface {v6, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215462
    :cond_0
    iget-object v5, p0, LX/1C7;->e:LX/0ad;

    sget-short v6, LX/0ws;->eJ:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 215463
    invoke-virtual {v0, p1, v5, v4}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/util/List;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_1
    move-object v0, v3

    .line 215464
    :goto_2
    if-eqz v2, :cond_1

    .line 215465
    const-string v1, ""

    .line 215466
    if-eqz v0, :cond_6

    .line 215467
    invoke-virtual {v0}, LX/7KB;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 215468
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 215469
    const-string v5, "%s\n%s"

    iget-object v6, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v5, v6, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 215470
    iget-object v6, p0, LX/1C7;->c:LX/03V;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p1, LX/1C7;->a:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p1, ", "

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 215471
    :cond_1
    return-void

    .line 215472
    :cond_2
    iget-object v3, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v3, v3

    .line 215473
    sget-object v5, LX/04A;->VIDEO_EXCEPTION:LX/04A;

    iget-object v5, v5, LX/04A;->value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 215474
    new-instance v0, Landroid/util/Pair;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Invalid metadata: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/04F;->VIDEO_ID:LX/04F;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "No video ID for %s.\n%s"

    .line 215475
    iget-object v6, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v6, v6

    .line 215476
    invoke-virtual {p1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 215477
    goto :goto_2

    :cond_3
    move v2, v0

    move-object v0, v1

    goto :goto_2

    :cond_4
    move v2, v1

    goto/16 :goto_1

    :cond_5
    move v1, v0

    goto/16 :goto_0

    :cond_6
    move-object v2, v1

    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 215446
    invoke-direct {p0}, LX/1C7;->e()Z

    move-result v0

    return v0
.end method
