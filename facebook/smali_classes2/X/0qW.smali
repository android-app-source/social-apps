.class public LX/0qW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/api/feedtype/FeedType;

.field private static volatile k:LX/0qW;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0qX;

.field private final e:LX/0oy;

.field private final f:LX/03V;

.field public final g:LX/0qY;

.field public h:LX/0xv;

.field public final i:LX/0qZ;

.field private final j:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147829
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    sput-object v0, LX/0qW;->a:Lcom/facebook/api/feedtype/FeedType;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0ad;LX/0qX;LX/03V;LX/0qY;LX/0qZ;LX/0SG;LX/0oy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0v1;",
            ">;",
            "LX/0ad;",
            "LX/0qX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0qY;",
            "LX/0qZ;",
            "LX/0SG;",
            "LX/0oy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147706
    const/4 v0, 0x0

    iput-object v0, p0, LX/0qW;->h:LX/0xv;

    .line 147707
    iput-object p1, p0, LX/0qW;->c:LX/0Ot;

    .line 147708
    iput-object p2, p0, LX/0qW;->b:LX/0ad;

    .line 147709
    iput-object p3, p0, LX/0qW;->d:LX/0qX;

    .line 147710
    iput-object p4, p0, LX/0qW;->f:LX/03V;

    .line 147711
    iput-object p5, p0, LX/0qW;->g:LX/0qY;

    .line 147712
    iput-object p6, p0, LX/0qW;->i:LX/0qZ;

    .line 147713
    iput-object p7, p0, LX/0qW;->j:LX/0SG;

    .line 147714
    iput-object p8, p0, LX/0qW;->e:LX/0oy;

    .line 147715
    return-void
.end method

.method public static a(LX/0qW;LX/0Px;Ljava/util/List;)I
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 147795
    iget-object v0, p0, LX/0qW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0v1;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 147796
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 147797
    const v0, -0x56757537

    invoke-static {v5, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147798
    const-string v0, "DBFeedRerankHandler.WriteClientRerankingRows"

    const v1, 0x2c9af80e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 147799
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    move v4, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 147800
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 147801
    add-int/lit8 v4, v4, 0x1

    .line 147802
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v7

    .line 147803
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    .line 147804
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 147805
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 147806
    sget-object v9, LX/0pp;->v:LX/0U1;

    .line 147807
    iget-object v10, v9, LX/0U1;->d:Ljava/lang/String;

    move-object v9, v10

    .line 147808
    invoke-virtual {v8, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147809
    sget-object v7, LX/0pp;->w:LX/0U1;

    .line 147810
    iget-object v9, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v9

    .line 147811
    invoke-virtual {v8, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147812
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 147813
    sget-object v7, LX/0pp;->d:LX/0U1;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147814
    sget-object v7, LX/0pp;->a:LX/0U1;

    .line 147815
    iget-object v9, v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->t:Ljava/lang/String;

    move-object v1, v9

    .line 147816
    invoke-virtual {v7, v1}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147817
    const-string v1, "home_stories"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v8, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v2

    .line 147818
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 147819
    :cond_1
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147820
    const v0, 0x45ceebc5

    invoke-static {v0}, LX/02m;->a(I)V

    .line 147821
    const v0, 0x324b3641

    invoke-static {v5, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 147822
    :cond_2
    :goto_2
    return v2

    .line 147823
    :catch_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "rerankedStoriesSize: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", originalOrderSize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147824
    iget-object v1, p0, LX/0qW;->f:LX/03V;

    const-string v3, "diversityRuleRerankIndexOutOfBound"

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147825
    const v0, 0x20b862c2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 147826
    const v0, 0x108e299c

    invoke-static {v5, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_2

    .line 147827
    :catchall_0
    move-exception v0

    const v1, 0x1a173bd9

    invoke-static {v1}, LX/02m;->a(I)V

    .line 147828
    const v1, -0x308460c

    invoke-static {v5, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/0qW;
    .locals 12

    .prologue
    .line 147782
    sget-object v0, LX/0qW;->k:LX/0qW;

    if-nez v0, :cond_1

    .line 147783
    const-class v1, LX/0qW;

    monitor-enter v1

    .line 147784
    :try_start_0
    sget-object v0, LX/0qW;->k:LX/0qW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147785
    if-eqz v2, :cond_0

    .line 147786
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147787
    new-instance v3, LX/0qW;

    const/16 v4, 0xea

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v6

    check-cast v6, LX/0qX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0qY;->a(LX/0QB;)LX/0qY;

    move-result-object v8

    check-cast v8, LX/0qY;

    const-class v9, LX/0qZ;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/0qZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v11

    check-cast v11, LX/0oy;

    invoke-direct/range {v3 .. v11}, LX/0qW;-><init>(LX/0Ot;LX/0ad;LX/0qX;LX/03V;LX/0qY;LX/0qZ;LX/0SG;LX/0oy;)V

    .line 147788
    move-object v0, v3

    .line 147789
    sput-object v0, LX/0qW;->k:LX/0qW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147790
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147791
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147792
    :cond_1
    sget-object v0, LX/0qW;->k:LX/0qW;

    return-object v0

    .line 147793
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/List;)LX/0ux;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0ux;"
        }
    .end annotation

    .prologue
    .line 147773
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147774
    :cond_0
    const/4 v0, 0x0

    .line 147775
    :goto_0
    return-object v0

    .line 147776
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 147777
    sget-object v1, LX/0pp;->n:LX/0U1;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    goto :goto_0

    .line 147778
    :cond_2
    invoke-static {}, LX/0uu;->b()LX/0uw;

    move-result-object v1

    .line 147779
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 147780
    sget-object v3, LX/0pp;->n:LX/0U1;

    invoke-virtual {v3, v0}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 147781
    goto :goto_0
.end method

.method public static b(LX/0qW;Lcom/facebook/api/feedtype/FeedType;)LX/0Px;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147733
    const-string v4, "DBFeedRerankHandler.loadRowsToRerank"

    const v5, 0x6c61a26e

    invoke-static {v4, v5}, LX/02m;->a(Ljava/lang/String;I)V

    .line 147734
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 147735
    :try_start_0
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 147736
    const-string v5, "home_stories"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 147737
    const/16 v5, 0xf

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v7, LX/0pp;->b:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x1

    sget-object v7, LX/0pp;->e:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x2

    sget-object v7, LX/0pp;->c:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x3

    sget-object v7, LX/0pp;->d:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x4

    sget-object v7, LX/0pp;->f:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x5

    sget-object v7, LX/0pp;->g:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x6

    sget-object v7, LX/0pp;->v:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x7

    sget-object v7, LX/0pp;->w:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0x8

    sget-object v7, LX/0pp;->n:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0x9

    sget-object v7, LX/0pp;->k:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0xa

    sget-object v7, LX/0pp;->i:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0xb

    sget-object v7, LX/0pp;->j:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0xc

    sget-object v7, LX/0pp;->x:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0xd

    sget-object v7, LX/0pp;->y:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    const/16 v5, 0xe

    sget-object v7, LX/0pp;->z:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    .line 147738
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v8

    .line 147739
    sget-object v5, LX/0pp;->a:LX/0U1;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147740
    sget-object v5, LX/0pp;->l:LX/0U1;

    const-string v7, "0"

    invoke-virtual {v5, v7}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147741
    invoke-static/range {p0 .. p0}, LX/0qW;->c(LX/0qW;)J

    move-result-wide v10

    .line 147742
    const-wide/16 v14, 0x0

    cmp-long v5, v10, v14

    if-eqz v5, :cond_0

    .line 147743
    sget-object v5, LX/0pp;->b:LX/0U1;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0qW;->j:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v14

    sub-long v10, v14, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0U1;->e(Ljava/lang/String;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147744
    :cond_0
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "Page"

    aput-object v9, v5, v7

    const/4 v7, 0x1

    const-string v9, "User"

    aput-object v9, v5, v7

    invoke-static {v5}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v5}, LX/0qW;->a(Ljava/util/List;)LX/0ux;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 147745
    sget-object v5, LX/0pp;->v:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v11

    .line 147746
    move-object/from16 v0, p0

    iget-object v5, v0, LX/0qW;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0v1;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-virtual {v8}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 147747
    sget-object v4, LX/0pp;->b:LX/0U1;

    invoke-virtual {v4, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 147748
    sget-object v6, LX/0pp;->e:LX/0U1;

    invoke-virtual {v6, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 147749
    sget-object v7, LX/0pp;->c:LX/0U1;

    invoke-virtual {v7, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 147750
    sget-object v8, LX/0pp;->d:LX/0U1;

    invoke-virtual {v8, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 147751
    sget-object v9, LX/0pp;->f:LX/0U1;

    invoke-virtual {v9, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 147752
    sget-object v10, LX/0pp;->g:LX/0U1;

    invoke-virtual {v10, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 147753
    sget-object v11, LX/0pp;->v:LX/0U1;

    invoke-virtual {v11, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 147754
    sget-object v13, LX/0pp;->w:LX/0U1;

    invoke-virtual {v13, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 147755
    sget-object v14, LX/0pp;->n:LX/0U1;

    invoke-virtual {v14, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v14

    .line 147756
    sget-object v15, LX/0pp;->k:LX/0U1;

    invoke-virtual {v15, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 147757
    sget-object v16, LX/0pp;->i:LX/0U1;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 147758
    sget-object v17, LX/0pp;->j:LX/0U1;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 147759
    sget-object v18, LX/0pp;->x:LX/0U1;

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 147760
    sget-object v19, LX/0pp;->y:LX/0U1;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 147761
    sget-object v20, LX/0pp;->z:LX/0U1;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, LX/0U1;->a(Landroid/database/Cursor;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v20

    .line 147762
    :goto_0
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_1

    .line 147763
    invoke-interface {v5, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 147764
    move/from16 v0, v16

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 147765
    move/from16 v0, v17

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 147766
    new-instance v24, LX/0x0;

    invoke-direct/range {v24 .. v24}, LX/0x0;-><init>()V

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, LX/0x0;->a(J)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->a(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->b(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->e(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v9}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, LX/0x0;->a(D)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->i(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->c(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    invoke-interface {v5, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, LX/0x0;->d(Ljava/lang/String;)LX/0x0;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/0x0;->h(Ljava/lang/String;)LX/0x0;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/0x0;->g(Ljava/lang/String;)LX/0x0;

    move-result-object v21

    invoke-interface {v5, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/0x0;->e(I)LX/0x0;

    move-result-object v21

    invoke-virtual/range {v21 .. v22}, LX/0x0;->f(I)LX/0x0;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/0x0;->a(I)LX/0x0;

    move-result-object v21

    move/from16 v0, v18

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, LX/0x0;->j(Ljava/lang/String;)LX/0x0;

    move-result-object v21

    move/from16 v0, v19

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, LX/0x0;->b(J)LX/0x0;

    move-result-object v21

    move/from16 v0, v20

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, LX/0x0;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/0x0;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, LX/0x0;->a()Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v21

    .line 147767
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 147768
    :catchall_0
    move-exception v4

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147769
    :catchall_1
    move-exception v4

    const v5, 0x34b47af4

    invoke-static {v5}, LX/02m;->a(I)V

    throw v4

    .line 147770
    :cond_1
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 147771
    const v4, 0x53067d93

    invoke-static {v4}, LX/02m;->a(I)V

    .line 147772
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    return-object v4
.end method

.method private static c(LX/0qW;)J
    .locals 4

    .prologue
    .line 147732
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/0qW;->e:LX/0oy;

    invoke-virtual {v1}, LX/0oy;->i()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .locals 12

    .prologue
    .line 147716
    const-string v0, "DBFeedRerankHandler.rerank"

    const v1, 0x499e1b89

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 147717
    :try_start_0
    sget-object v0, LX/0qW;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 147718
    invoke-static {p0, v0}, LX/0qW;->b(LX/0qW;Lcom/facebook/api/feedtype/FeedType;)LX/0Px;

    move-result-object v2

    .line 147719
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 147720
    :cond_0
    const/4 v2, 0x0

    .line 147721
    :goto_0
    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147722
    const v1, 0x23a29a3

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, -0x476e0f33

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 147723
    :cond_1
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    .line 147724
    iget-object v6, p0, LX/0qW;->h:LX/0xv;

    if-nez v6, :cond_2

    .line 147725
    iget-object v6, p0, LX/0qW;->g:LX/0qY;

    const-string v7, "{ \"ctr_multiply_values\" : {\"base_values\" : { \"weight_final\" : \"1\", \"seen\" : {\"viewed\" : \"-10000\"}} }, \"ctr_value_features\":  {\"seen\": \"client_has_seen\"}}"

    .line 147726
    sget-wide v9, LX/0X5;->ct:J

    const/4 v11, 0x0

    invoke-virtual {v6, v9, v10, v11, v7}, LX/0pK;->a(JILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object v6, v9

    .line 147727
    iget-object v7, p0, LX/0qW;->i:LX/0qZ;

    const/4 v8, 0x0

    invoke-virtual {v7, v6, v8}, LX/0qZ;->a(Ljava/lang/String;Ljava/lang/String;)LX/0xv;

    move-result-object v6

    iput-object v6, p0, LX/0qW;->h:LX/0xv;

    .line 147728
    :cond_2
    iget-object v6, p0, LX/0qW;->h:LX/0xv;

    move-object v5, v6

    .line 147729
    invoke-virtual {v5, v4}, LX/0xv;->a(Ljava/util/List;)I

    .line 147730
    move-object v3, v4

    .line 147731
    invoke-static {p0, v2, v3}, LX/0qW;->a(LX/0qW;LX/0Px;Ljava/util/List;)I

    move-result v2

    goto :goto_0
.end method
