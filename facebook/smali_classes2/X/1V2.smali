.class public LX/1V2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/14w;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 257112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257113
    iput-object p1, p0, LX/1V2;->a:LX/14w;

    .line 257114
    iput-object p2, p0, LX/1V2;->b:LX/0ad;

    .line 257115
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 257116
    if-nez p0, :cond_0

    .line 257117
    const/4 v0, -0x1

    .line 257118
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1V2;
    .locals 5

    .prologue
    .line 257119
    const-class v1, LX/1V2;

    monitor-enter v1

    .line 257120
    :try_start_0
    sget-object v0, LX/1V2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257121
    sput-object v2, LX/1V2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257122
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257123
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257124
    new-instance p0, LX/1V2;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/1V2;-><init>(LX/14w;LX/0ad;)V

    .line 257125
    move-object v0, p0

    .line 257126
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257127
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257128
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .param p0    # Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 257010
    if-eqz p0, :cond_0

    .line 257011
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    invoke-static {p0, v0}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1X8;)Z

    move-result v0

    move v0, v0

    .line 257012
    if-eqz v0, :cond_1

    :cond_0
    move-object p1, v1

    .line 257013
    :goto_0
    return-object p1

    .line 257014
    :cond_1
    instance-of v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 257015
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 257016
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 257017
    instance-of v3, v2, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v3, :cond_2

    .line 257018
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0

    .line 257019
    :cond_2
    instance-of v3, v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v3, :cond_3

    .line 257020
    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 257021
    :cond_3
    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v2, :cond_4

    .line 257022
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 257023
    :goto_1
    if-eqz v1, :cond_8

    .line 257024
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 257025
    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_7

    .line 257026
    :goto_2
    move-object p1, v1

    .line 257027
    goto :goto_0

    .line 257028
    :cond_4
    instance-of v0, p1, LX/17z;

    if-eqz v0, :cond_5

    .line 257029
    check-cast p1, LX/17z;

    invoke-interface {p1}, LX/17z;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    .line 257030
    :cond_5
    instance-of v0, p1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_6

    .line 257031
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    goto :goto_0

    :cond_6
    move-object p1, v1

    .line 257032
    goto :goto_0

    .line 257033
    :cond_7
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v2

    .line 257034
    goto :goto_1

    .line 257035
    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1X8;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 257109
    instance-of v1, p0, LX/1Vh;

    if-eqz v1, :cond_2

    check-cast p0, LX/1Vh;

    :goto_0
    move-object v1, p0

    .line 257110
    if-nez v1, :cond_1

    .line 257111
    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-interface {v1}, LX/1Vh;->b()LX/1X8;

    move-result-object v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 257089
    if-eqz p0, :cond_0

    .line 257090
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257091
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    move-object v4, v0

    .line 257092
    :goto_0
    if-eqz p1, :cond_1

    .line 257093
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257094
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 257095
    :goto_1
    invoke-static {v4, v0}, LX/14w;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v2

    .line 257096
    :goto_2
    return v0

    :cond_0
    move-object v4, v1

    .line 257097
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 257098
    goto :goto_1

    .line 257099
    :cond_2
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_3

    move v0, v3

    .line 257100
    goto :goto_2

    .line 257101
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v3

    :goto_3
    if-ge v1, v6, :cond_6

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 257102
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 257103
    invoke-static {v0, v4}, LX/14w;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v7

    if-eqz v7, :cond_4

    move v0, v2

    .line 257104
    goto :goto_2

    .line 257105
    :cond_4
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_5

    move v0, v3

    .line 257106
    goto :goto_2

    .line 257107
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_6
    move v0, v3

    .line 257108
    goto :goto_2
.end method

.method public static d(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)Z
    .locals 1

    .prologue
    .line 257088
    sget-object v0, LX/1X8;->NEED_BOTTOM_DIVIDER:LX/1X8;

    invoke-static {p0, v0}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1X8;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ILcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;
    .locals 5
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;I",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Lcom/facebook/multirow/api/PartWithViewType;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "LX/1X9;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 257036
    if-eqz p1, :cond_0

    .line 257037
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257038
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    move-object v2, v0

    .line 257039
    :goto_0
    if-nez v2, :cond_2

    .line 257040
    invoke-static {p3}, LX/1V2;->d(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    .line 257041
    :goto_1
    return-object v0

    :cond_0
    move-object v2, v1

    .line 257042
    goto :goto_0

    .line 257043
    :cond_1
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_1

    .line 257044
    :cond_2
    invoke-static {p4, p6}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 257045
    invoke-static {p5, p7}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 257046
    if-eqz v3, :cond_3

    .line 257047
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 257048
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 257049
    :goto_2
    instance-of v1, p4, LX/1XH;

    if-eqz v1, :cond_4

    .line 257050
    check-cast p4, LX/1XH;

    .line 257051
    invoke-interface {p4, v0, v2}, LX/1XH;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    .line 257052
    if-eqz v0, :cond_4

    .line 257053
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 257054
    goto :goto_2

    .line 257055
    :cond_4
    iget-object v0, p0, LX/1V2;->a:LX/14w;

    invoke-virtual {v0, v3}, LX/14w;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    .line 257056
    iget-object v1, p0, LX/1V2;->a:LX/14w;

    invoke-virtual {v1, v4}, LX/14w;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    .line 257057
    if-ge v0, p2, :cond_5

    .line 257058
    if-le p2, v1, :cond_c

    .line 257059
    sget-object v0, LX/1X9;->BOX:LX/1X9;

    .line 257060
    :goto_3
    move-object v0, v0

    .line 257061
    goto :goto_1

    .line 257062
    :cond_5
    if-le p2, v1, :cond_7

    .line 257063
    sget-object v0, LX/1X8;->MAYBE_HAS_COMMENTS_BELOW:LX/1X8;

    invoke-static {p3, v0}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1X8;)Z

    move-result v0

    move v0, v0

    .line 257064
    if-eqz v0, :cond_e

    instance-of v0, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_e

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257065
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    if-eqz p3, :cond_f

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-static {p3}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result p3

    if-lez p3, :cond_f

    .line 257066
    :cond_6
    :goto_4
    move v0, v0

    .line 257067
    if-eqz v0, :cond_e

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    :goto_5
    move-object v0, v0

    .line 257068
    goto :goto_1

    .line 257069
    :cond_7
    sget-object v1, LX/1X8;->FOLLOWUP_FEEDUNIT:LX/1X8;

    invoke-static {p5, v1}, LX/1V2;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1X8;)Z

    move-result v1

    move v1, v1

    .line 257070
    if-eqz v1, :cond_8

    .line 257071
    sget-object v0, LX/1X9;->FOLLOW_UP:LX/1X9;

    goto :goto_1

    .line 257072
    :cond_8
    invoke-static {p3}, LX/1V2;->d(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 257073
    sget-object v0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    goto :goto_1

    .line 257074
    :cond_9
    const/4 v1, 0x1

    .line 257075
    if-ne v0, p2, :cond_12

    .line 257076
    invoke-static {v3}, LX/1V2;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    invoke-static {p1}, LX/1V2;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result p0

    if-ne v2, p0, :cond_14

    const/4 v2, 0x1

    :goto_6
    move v2, v2

    .line 257077
    if-nez v2, :cond_12

    .line 257078
    :cond_a
    :goto_7
    move v0, v1

    .line 257079
    if-eqz v0, :cond_b

    .line 257080
    sget-object v0, LX/1X9;->DIVIDER_TOP:LX/1X9;

    goto/16 :goto_1

    .line 257081
    :cond_b
    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    goto/16 :goto_1

    .line 257082
    :cond_c
    invoke-static {p3}, LX/1V2;->d(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 257083
    sget-object v0, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    goto :goto_3

    .line 257084
    :cond_d
    sget-object v0, LX/1X9;->TOP:LX/1X9;

    goto :goto_3

    :cond_e
    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_5

    .line 257085
    :cond_f
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    if-eqz p3, :cond_10

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object p3

    if-nez p3, :cond_11

    :cond_10
    move v0, v1

    .line 257086
    goto :goto_4

    .line 257087
    :cond_11
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result p3

    if-gtz p3, :cond_6

    move v0, v1

    goto :goto_4

    :cond_12
    invoke-static {v3, p1}, LX/1V2;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_13

    invoke-static {p1, v3}, LX/1V2;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_13
    const/4 v1, 0x0

    goto :goto_7

    :cond_14
    const/4 v2, 0x0

    goto :goto_6
.end method
