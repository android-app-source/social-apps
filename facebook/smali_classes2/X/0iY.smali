.class public LX/0iY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile v:LX/0iY;


# instance fields
.field public final b:LX/0ad;

.field private final c:Landroid/media/AudioManager;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Landroid/graphics/Rect;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/view/WindowManager;

.field private final h:I

.field private final i:I

.field private final j:LX/0Tn;

.field public final k:LX/0Tn;

.field private final l:I

.field private final m:I

.field private final n:Z

.field private final o:Z

.field public p:I

.field private q:I

.field public r:I

.field public s:I

.field public t:Z

.field private u:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121190
    const-class v0, LX/0iY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0iY;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;Landroid/view/WindowManager;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Landroid/view/WindowManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x1770

    const/16 v5, 0x64

    const/4 v4, 0x0

    .line 121163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121164
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/0iY;->e:Landroid/graphics/Rect;

    .line 121165
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "sound_toggle_label_shown_times"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/0iY;->j:LX/0Tn;

    .line 121166
    const/4 v0, -0x1

    iput v0, p0, LX/0iY;->q:I

    .line 121167
    iput v2, p0, LX/0iY;->u:I

    .line 121168
    iput-object p1, p0, LX/0iY;->b:LX/0ad;

    .line 121169
    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    .line 121170
    iput-object p3, p0, LX/0iY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 121171
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget v1, LX/1rJ;->K:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0iY;->p:I

    .line 121172
    iget-object v0, p0, LX/0iY;->j:LX/0Tn;

    iget-object v1, p0, LX/0iY;->b:LX/0ad;

    sget-char v2, LX/1rJ;->H:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/0iY;->k:LX/0Tn;

    .line 121173
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget v1, LX/1rJ;->q:I

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iget-object v1, p0, LX/0iY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/0iY;->k:LX/0Tn;

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, LX/0iY;->r:I

    .line 121174
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget v1, LX/1rJ;->r:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0iY;->s:I

    .line 121175
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget v1, LX/1rJ;->u:I

    invoke-interface {v0, v1, v5}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0iY;->l:I

    .line 121176
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget v1, LX/1rJ;->n:I

    invoke-interface {v0, v1, v5}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/0iY;->m:I

    .line 121177
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget-short v1, LX/1rJ;->c:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0iY;->o:Z

    .line 121178
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget-short v1, LX/1rJ;->J:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/0iY;->n:Z

    .line 121179
    iput-object p4, p0, LX/0iY;->f:LX/0Ot;

    .line 121180
    iput-object p5, p0, LX/0iY;->g:Landroid/view/WindowManager;

    .line 121181
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 121182
    iget-object v1, p0, LX/0iY;->g:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 121183
    invoke-static {p0}, LX/0iY;->n(LX/0iY;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121184
    iget v1, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/0iY;->i:I

    .line 121185
    iget v0, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/0iY;->h:I

    .line 121186
    :goto_0
    iput-boolean v4, p0, LX/0iY;->t:Z

    .line 121187
    return-void

    .line 121188
    :cond_0
    iget v1, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/0iY;->i:I

    .line 121189
    iget v0, v0, Landroid/graphics/Point;->y:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/0iY;->h:I

    goto :goto_0
.end method

.method private static a(LX/0iY;Lcom/facebook/video/player/RichVideoPlayer;)F
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x41200000    # 10.0f

    .line 121152
    iget v0, p0, LX/0iY;->q:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 121153
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getRootView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d14e3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121154
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, LX/0iY;->q:I

    .line 121155
    :cond_0
    invoke-static {p0}, LX/0iY;->n(LX/0iY;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LX/0iY;->i:I

    .line 121156
    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 121157
    invoke-virtual {p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLocationOnScreen([I)V

    .line 121158
    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    .line 121159
    iget v2, p0, LX/0iY;->q:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, LX/0iY;->q:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    mul-float/2addr v0, v4

    sub-float/2addr v0, v4

    .line 121160
    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    const/high16 v1, -0x40800000    # -1.0f

    mul-float/2addr v1, v0

    mul-float/2addr v0, v1

    const/high16 v1, 0x41a00000    # 20.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0

    .line 121161
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0

    .line 121162
    :cond_2
    iget v0, p0, LX/0iY;->h:I

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/0QB;)LX/0iY;
    .locals 9

    .prologue
    .line 121139
    sget-object v0, LX/0iY;->v:LX/0iY;

    if-nez v0, :cond_1

    .line 121140
    const-class v1, LX/0iY;

    monitor-enter v1

    .line 121141
    :try_start_0
    sget-object v0, LX/0iY;->v:LX/0iY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121142
    if-eqz v2, :cond_0

    .line 121143
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121144
    new-instance v3, LX/0iY;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    invoke-direct/range {v3 .. v8}, LX/0iY;-><init>(LX/0ad;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;Landroid/view/WindowManager;)V

    .line 121145
    move-object v0, v3

    .line 121146
    sput-object v0, LX/0iY;->v:LX/0iY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121147
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121148
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121149
    :cond_1
    sget-object v0, LX/0iY;->v:LX/0iY;

    return-object v0

    .line 121150
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121151
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static n(LX/0iY;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 121137
    iget-object v1, p0, LX/0iY;->g:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 121138
    if-eq v1, v0, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/0iY;)D
    .locals 4

    .prologue
    .line 121133
    invoke-virtual {p0}, LX/0iY;->m()I

    move-result v0

    .line 121134
    const/16 v1, 0x32

    if-gt v0, v1, :cond_0

    .line 121135
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 121136
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, -0x400999999999999aL    # -1.4

    invoke-virtual {p0}, LX/0iY;->m()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x4065400000000000L    # 170.0

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;II)F
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x7d0

    .line 121103
    if-ltz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 121104
    if-ltz p3, :cond_2

    const/16 v0, 0x64

    if-gt p3, v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 121105
    iget-boolean v0, p0, LX/0iY;->o:Z

    if-eqz v0, :cond_0

    .line 121106
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    .line 121107
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    invoke-static {p0}, LX/0iY;->o(LX/0iY;)D

    move-result-wide v6

    mul-double/2addr v6, v8

    sub-double v6, v8, v6

    const-wide/high16 v8, 0x4049000000000000L    # 50.0

    div-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v4, v4

    rsub-int/lit8 v4, v4, 0x64

    invoke-virtual {p0}, LX/0iY;->m()I

    move-result v5

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x64

    move v0, v4

    .line 121108
    invoke-virtual {p0}, LX/0iY;->m()I

    move-result v1

    mul-int/lit16 v1, v1, 0x1770

    div-int/lit8 v1, v1, 0x64

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LX/0iY;->u:I

    .line 121109
    if-ge p3, v0, :cond_3

    if-eqz p3, :cond_3

    iget v0, p0, LX/0iY;->u:I

    div-int/lit8 v0, v0, 0x64

    mul-int/2addr v0, p3

    :goto_2
    iput v0, p0, LX/0iY;->p:I

    .line 121110
    iget v0, p0, LX/0iY;->p:I

    if-ge v0, v3, :cond_0

    .line 121111
    iput v3, p0, LX/0iY;->p:I

    .line 121112
    :cond_0
    iget-object v0, p0, LX/0iY;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 121113
    iget-object v0, p0, LX/0iY;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 121114
    iget-object v1, p0, LX/0iY;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->getHitRect(Landroid/graphics/Rect;)V

    .line 121115
    iget-object v1, p0, LX/0iY;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 121116
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 121117
    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_4

    .line 121118
    const/4 v0, 0x0

    .line 121119
    :goto_3
    return v0

    :cond_1
    move v0, v2

    .line 121120
    goto :goto_0

    :cond_2
    move v1, v2

    .line 121121
    goto :goto_1

    .line 121122
    :cond_3
    iget v1, p0, LX/0iY;->u:I

    div-int/lit8 v1, v1, 0x64

    mul-int/2addr v0, v1

    goto :goto_2

    .line 121123
    :cond_4
    iget-boolean v1, p0, LX/0iY;->n:Z

    if-eqz v1, :cond_6

    .line 121124
    invoke-static {p0, p1}, LX/0iY;->a(LX/0iY;Lcom/facebook/video/player/RichVideoPlayer;)F

    move-result v0

    .line 121125
    :goto_4
    iget v1, p0, LX/0iY;->p:I

    if-le p2, v1, :cond_7

    const/high16 v1, 0x3f800000    # 1.0f

    .line 121126
    :goto_5
    iget-boolean v2, p0, LX/0iY;->o:Z

    if-eqz v2, :cond_8

    invoke-static {p0}, LX/0iY;->o(LX/0iY;)D

    move-result-wide v2

    double-to-float v2, v2

    .line 121127
    :goto_6
    iget v3, p0, LX/0iY;->l:I

    if-le p3, v3, :cond_5

    .line 121128
    iget v2, p0, LX/0iY;->l:I

    int-to-float v2, v2

    int-to-float v3, p3

    div-float/2addr v2, v3

    .line 121129
    :cond_5
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float/2addr v0, v2

    goto :goto_3

    .line 121130
    :cond_6
    const v1, 0x3faaaaab

    mul-float/2addr v1, v0

    mul-float/2addr v0, v1

    const v1, 0x3eaaaaab

    sub-float/2addr v0, v1

    goto :goto_4

    .line 121131
    :cond_7
    int-to-float v1, p2

    iget v2, p0, LX/0iY;->p:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_5

    .line 121132
    :cond_8
    iget v2, p0, LX/0iY;->m:I

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    goto :goto_6
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 121102
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget-short v1, LX/1rJ;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 121091
    iget-object v0, p0, LX/0iY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1rO;->c:LX/0Tn;

    invoke-virtual {p0}, LX/0iY;->c()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 121101
    iget-object v0, p0, LX/0iY;->b:LX/0ad;

    sget-short v1, LX/1rJ;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 121100
    iget-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 121099
    iget-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 121095
    iget-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 121096
    sget-object v0, LX/0iY;->a:Ljava/lang/String;

    const-string v2, "AudioManager is NULL"

    invoke-static {v0, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    .line 121097
    iget-object v0, p0, LX/0iY;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v2}, LX/03V;->a(LX/0VG;)V

    move v0, v1

    .line 121098
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final m()I
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 121092
    iget-object v0, p0, LX/0iY;->c:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 121093
    iget-object v1, p0, LX/0iY;->c:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 121094
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method
