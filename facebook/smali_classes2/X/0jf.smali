.class public LX/0jf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "Lcom/facebook/common/dispose/ListenableDisposable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this while onDestroyEntered == false"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 124556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124557
    iput-object p1, p0, LX/0jf;->a:LX/0Sh;

    .line 124558
    return-void
.end method

.method public static b(LX/0QB;)LX/0jf;
    .locals 2

    .prologue
    .line 124578
    new-instance v1, LX/0jf;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-direct {v1, v0}, LX/0jf;-><init>(LX/0Sh;)V

    .line 124579
    return-object v1
.end method

.method public static declared-synchronized b(LX/0jf;LX/0je;)V
    .locals 1

    .prologue
    .line 124580
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0jf;->b:Z

    if-nez v0, :cond_0

    .line 124581
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124582
    :cond_0
    monitor-exit p0

    return-void

    .line 124583
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 124568
    monitor-enter p0

    .line 124569
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0jf;->b:Z

    .line 124570
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124571
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    if-eqz v0, :cond_1

    .line 124572
    const/4 v0, 0x0

    iget-object v1, p0, LX/0jf;->c:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 124573
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    invoke-virtual {v0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0je;

    invoke-virtual {v0}, LX/0je;->dispose()V

    .line 124574
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124575
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 124576
    :cond_0
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 124577
    :cond_1
    return-void
.end method

.method public final declared-synchronized a(LX/0je;)V
    .locals 2

    .prologue
    .line 124559
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124560
    iget-boolean v0, p0, LX/0jf;->b:Z

    if-eqz v0, :cond_0

    .line 124561
    iget-object v0, p0, LX/0jf;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/common/dispose/DisposableContextHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/common/dispose/DisposableContextHelper$1;-><init>(LX/0jf;LX/0je;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124562
    :goto_0
    monitor-exit p0

    return-void

    .line 124563
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    if-nez v0, :cond_1

    .line 124564
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0jf;->c:LX/0UE;

    .line 124565
    :cond_1
    iget-object v0, p0, LX/0jf;->c:LX/0UE;

    invoke-virtual {v0, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 124566
    new-instance v0, LX/32U;

    invoke-direct {v0, p0}, LX/32U;-><init>(LX/0jf;)V

    invoke-virtual {p1, v0}, LX/0je;->addDisposeListener(LX/32U;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124567
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
