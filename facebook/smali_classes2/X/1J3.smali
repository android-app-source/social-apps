.class public LX/1J3;
.super LX/1J4;
.source ""


# instance fields
.field public final a:LX/0oy;

.field public b:LX/1UQ;

.field public c:LX/0gC;

.field public d:I

.field public e:I

.field public final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/feed/ui/NewsFeedListViewPruner$OnPruneListener;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0ad;


# direct methods
.method public constructor <init>(LX/0oy;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229653
    invoke-direct {p0}, LX/1J4;-><init>()V

    .line 229654
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1J3;->f:Ljava/util/ArrayList;

    .line 229655
    iput-object p1, p0, LX/1J3;->a:LX/0oy;

    .line 229656
    iput-object p2, p0, LX/1J3;->g:LX/0ad;

    .line 229657
    return-void
.end method

.method public static a(LX/1J3;I)I
    .locals 2

    .prologue
    .line 229651
    iget-object v0, p0, LX/1J3;->b:LX/1UQ;

    invoke-virtual {v0}, LX/1UQ;->b()I

    move-result v0

    iget-object v1, p0, LX/1J3;->b:LX/1UQ;

    invoke-virtual {v1}, LX/1UQ;->c()I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 229652
    iget-object v1, p0, LX/1J3;->b:LX/1UQ;

    invoke-interface {v1, v0}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/1J3;
    .locals 3

    .prologue
    .line 229658
    new-instance v2, LX/1J3;

    invoke-static {p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v0

    check-cast v0, LX/0oy;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/1J3;-><init>(LX/0oy;LX/0ad;)V

    .line 229659
    move-object v0, v2

    .line 229660
    return-object v0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 7

    .prologue
    .line 229612
    if-eqz p2, :cond_0

    .line 229613
    :goto_0
    return-void

    .line 229614
    :cond_0
    invoke-interface {p1}, LX/0g8;->s()I

    move-result v0

    iput v0, p0, LX/1J3;->d:I

    .line 229615
    invoke-interface {p1}, LX/0g8;->r()I

    move-result v0

    iput v0, p0, LX/1J3;->e:I

    .line 229616
    iget-object v1, p0, LX/1J3;->g:LX/0ad;

    sget-short v2, LX/0fe;->aG:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 229617
    if-nez v1, :cond_1

    iget-object v1, p0, LX/1J3;->c:LX/0gC;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/1J3;->b:LX/1UQ;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/1J3;->c:LX/0gC;

    invoke-interface {v1}, LX/0gC;->h()LX/0fz;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/1J3;->c:LX/0gC;

    invoke-interface {v1}, LX/0gC;->x()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/1J3;->b:LX/1UQ;

    invoke-interface {v1}, LX/1Qr;->d()I

    move-result v1

    if-nez v1, :cond_2

    .line 229618
    :cond_1
    :goto_1
    goto :goto_0

    .line 229619
    :cond_2
    sget-object v1, LX/1J5;->SCROLLING_UP:LX/1J5;

    .line 229620
    iget-object v2, p0, LX/1J4;->a:LX/1J5;

    move-object v2, v2

    .line 229621
    if-ne v1, v2, :cond_1

    .line 229622
    :try_start_0
    iget v1, p0, LX/1J3;->d:I

    invoke-static {p0, v1}, LX/1J3;->a(LX/1J3;I)I

    move-result v1

    iget v2, p0, LX/1J3;->e:I

    invoke-static {p0, v2}, LX/1J3;->a(LX/1J3;I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    sub-int/2addr v1, v2

    .line 229623
    iget-object v2, p0, LX/1J3;->a:LX/0oy;

    .line 229624
    iget v3, v2, LX/0oy;->h:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 229625
    iget-object v3, v2, LX/0oy;->a:LX/0W3;

    sget-wide v5, LX/0X5;->gs:J

    const/16 v4, 0x14

    invoke-interface {v3, v5, v6, v4}, LX/0W4;->a(JI)I

    move-result v3

    iput v3, v2, LX/0oy;->h:I

    .line 229626
    :cond_3
    iget v3, v2, LX/0oy;->h:I

    move v2, v3

    .line 229627
    if-le v1, v2, :cond_1

    .line 229628
    iget v1, p0, LX/1J3;->e:I

    invoke-static {p0, v1}, LX/1J3;->a(LX/1J3;I)I

    move-result v1

    .line 229629
    iget-object v2, p0, LX/1J3;->a:LX/0oy;

    .line 229630
    iget v3, v2, LX/0oy;->i:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    .line 229631
    iget-object v3, v2, LX/0oy;->a:LX/0W3;

    sget-wide v5, LX/0X5;->gt:J

    const/16 v4, 0xa

    invoke-interface {v3, v5, v6, v4}, LX/0W4;->a(JI)I

    move-result v3

    iput v3, v2, LX/0oy;->i:I

    .line 229632
    :cond_4
    iget v3, v2, LX/0oy;->i:I

    move v2, v3

    .line 229633
    add-int/2addr v1, v2

    .line 229634
    iget-object v2, p0, LX/1J3;->c:LX/0gC;

    invoke-interface {v2}, LX/0gC;->h()LX/0fz;

    move-result-object v2

    .line 229635
    invoke-virtual {v2}, LX/0fz;->v()I

    move-result v3

    if-gt v3, v1, :cond_7

    .line 229636
    :goto_2
    iget-object v2, p0, LX/1J3;->g:LX/0ad;

    sget-short v3, LX/0fe;->by:S

    const/4 v1, 0x0

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 229637
    if-eqz v2, :cond_5

    .line 229638
    iget-object v2, p0, LX/1J3;->c:LX/0gC;

    invoke-interface {v2}, LX/0gC;->z()V

    .line 229639
    :cond_5
    iget-object v2, p0, LX/1J3;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 229640
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_6

    .line 229641
    iget-object v2, p0, LX/1J3;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0g2;

    .line 229642
    iget-object v1, v2, LX/0g2;->W:LX/1CY;

    invoke-virtual {v1}, LX/1CY;->b()V

    .line 229643
    invoke-static {v2}, LX/0g2;->R(LX/0g2;)V

    .line 229644
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 229645
    :cond_6
    goto/16 :goto_1

    .line 229646
    :catch_0
    goto/16 :goto_1

    .line 229647
    :cond_7
    invoke-static {v2, v1}, LX/0fz;->i(LX/0fz;I)V

    .line 229648
    invoke-virtual {v2}, LX/0fz;->u()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, LX/0fz;->v()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, LX/0fz;->f(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 229649
    invoke-static {v2}, LX/0fz;->M(LX/0fz;)V

    .line 229650
    :cond_8
    invoke-static {v2}, LX/0fz;->F(LX/0fz;)V

    goto :goto_2
.end method
