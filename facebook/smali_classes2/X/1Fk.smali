.class public LX/1Fk;
.super LX/1FR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1FR",
        "<",
        "Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final g:[I


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;LX/1F0;)V
    .locals 4

    .prologue
    .line 224547
    invoke-direct {p0, p1, p2, p3}, LX/1FR;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    .line 224548
    iget-object v1, p2, LX/1F7;->c:Landroid/util/SparseIntArray;

    .line 224549
    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/1Fk;->g:[I

    .line 224550
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/1Fk;->g:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 224551
    iget-object v2, p0, LX/1Fk;->g:[I

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    aput v3, v2, v0

    .line 224552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224553
    :cond_0
    invoke-virtual {p0}, LX/1FR;->a()V

    .line 224554
    return-void
.end method


# virtual methods
.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 224555
    new-instance v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    invoke-direct {v0, p1}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;-><init>(I)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 224556
    check-cast p1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 224557
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224558
    invoke-virtual {p1}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->close()V

    .line 224559
    return-void
.end method

.method public final c(I)I
    .locals 4

    .prologue
    .line 224560
    if-gtz p1, :cond_0

    .line 224561
    new-instance v0, LX/4eK;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4eK;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 224562
    :cond_0
    iget-object v2, p0, LX/1Fk;->g:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v0, v2, v1

    .line 224563
    if-lt v0, p1, :cond_2

    move p1, v0

    .line 224564
    :cond_1
    return p1

    .line 224565
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 224566
    check-cast p1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 224567
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224568
    iget v0, p1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->b:I

    move v0, v0

    .line 224569
    return v0
.end method

.method public final d(I)I
    .locals 0

    .prologue
    .line 224570
    return p1
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 224571
    check-cast p1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 224572
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224573
    invoke-virtual {p1}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
