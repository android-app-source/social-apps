.class public LX/0xX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field private static volatile f:LX/0xX;


# instance fields
.field public final c:LX/0ad;

.field public final d:LX/0Uh;

.field private final e:LX/0tQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 163256
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LX/0xX;->a:I

    .line 163257
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LX/0xX;->b:I

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;LX/0tQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163252
    iput-object p1, p0, LX/0xX;->c:LX/0ad;

    .line 163253
    iput-object p2, p0, LX/0xX;->d:LX/0Uh;

    .line 163254
    iput-object p3, p0, LX/0xX;->e:LX/0tQ;

    .line 163255
    return-void
.end method

.method public static a(LX/0QB;)LX/0xX;
    .locals 6

    .prologue
    .line 163238
    sget-object v0, LX/0xX;->f:LX/0xX;

    if-nez v0, :cond_1

    .line 163239
    const-class v1, LX/0xX;

    monitor-enter v1

    .line 163240
    :try_start_0
    sget-object v0, LX/0xX;->f:LX/0xX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 163241
    if-eqz v2, :cond_0

    .line 163242
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 163243
    new-instance p0, LX/0xX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v5

    check-cast v5, LX/0tQ;

    invoke-direct {p0, v3, v4, v5}, LX/0xX;-><init>(LX/0ad;LX/0Uh;LX/0tQ;)V

    .line 163244
    move-object v0, p0

    .line 163245
    sput-object v0, LX/0xX;->f:LX/0xX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163246
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 163247
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 163248
    :cond_1
    sget-object v0, LX/0xX;->f:LX/0xX;

    return-object v0

    .line 163249
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 163250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static t(LX/0xX;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 163235
    iget-object v1, p0, LX/0xX;->e:LX/0tQ;

    .line 163236
    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/0tQ;->s()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, LX/0tQ;->w()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, LX/0tQ;->y()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 163237
    if-nez v1, :cond_1

    iget-object v1, p0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->j:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 163234
    iget-object v1, p0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->t:S

    iget-object v3, p0, LX/0xX;->d:LX/0Uh;

    const/16 v4, 0x2bb

    invoke-virtual {v3, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/0xX;->t(LX/0xX;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final a(LX/1vy;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 163200
    invoke-virtual {p0}, LX/0xX;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 163201
    :goto_0
    return v0

    .line 163202
    :cond_0
    sget-object v1, LX/2nu;->a:[I

    invoke-virtual {p1}, LX/1vy;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 163203
    :pswitch_0
    iget-object v0, p0, LX/0xX;->d:LX/0Uh;

    const/16 v1, 0x2b8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 163204
    goto :goto_0

    .line 163205
    :pswitch_1
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->k:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163206
    goto :goto_0

    .line 163207
    :pswitch_2
    const/4 v0, 0x0

    move v0, v0

    .line 163208
    goto :goto_0

    .line 163209
    :pswitch_3
    invoke-virtual {p0}, LX/0xX;->b()Z

    move-result v0

    goto :goto_0

    .line 163210
    :pswitch_4
    const/4 v0, 0x0

    .line 163211
    invoke-static {p0}, LX/0xX;->t(LX/0xX;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->h:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 163212
    goto :goto_0

    .line 163213
    :pswitch_5
    const/4 v0, 0x0

    .line 163214
    invoke-static {p0}, LX/0xX;->t(LX/0xX;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->u:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 163215
    goto :goto_0

    .line 163216
    :pswitch_6
    const/4 v0, 0x0

    .line 163217
    invoke-static {p0}, LX/0xX;->t(LX/0xX;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/0xX;->c:LX/0ad;

    sget-short v2, LX/0xY;->i:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    move v0, v0

    .line 163218
    goto :goto_0

    .line 163219
    :pswitch_7
    invoke-virtual {p0}, LX/0xX;->d()Z

    move-result v0

    goto :goto_0

    .line 163220
    :pswitch_8
    invoke-virtual {p0}, LX/0xX;->c()Z

    move-result v0

    goto :goto_0

    .line 163221
    :pswitch_9
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->o:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163222
    goto :goto_0

    .line 163223
    :pswitch_a
    iget-object v0, p0, LX/0xX;->d:LX/0Uh;

    const/16 v1, 0x462

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 163224
    goto/16 :goto_0

    .line 163225
    :pswitch_b
    iget-object v0, p0, LX/0xX;->d:LX/0Uh;

    const/16 v1, 0x463

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 163226
    goto/16 :goto_0

    .line 163227
    :pswitch_c
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->C:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163228
    goto/16 :goto_0

    .line 163229
    :pswitch_d
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->q:S

    iget-object v2, p0, LX/0xX;->d:LX/0Uh;

    const/16 v3, 0x661

    const/4 p1, 0x1

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163230
    goto/16 :goto_0

    .line 163231
    :pswitch_e
    invoke-virtual {p0}, LX/0xX;->j()Z

    move-result v0

    goto/16 :goto_0

    .line 163232
    :pswitch_f
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->r:S

    iget-object v2, p0, LX/0xX;->d:LX/0Uh;

    const/16 v3, 0x2ba

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 163233
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 163191
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 163199
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->n:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 163198
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->m:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 163197
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget v1, LX/0xY;->z:I

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 163196
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->s:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 3

    .prologue
    .line 163195
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget v1, LX/0xY;->v:I

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 3

    .prologue
    .line 163194
    iget-object v0, p0, LX/0xX;->c:LX/0ad;

    sget-short v1, LX/0xY;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    .line 163193
    iget-object v0, p0, LX/0xX;->d:LX/0Uh;

    const/16 v1, 0x460

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 163192
    iget-object v0, p0, LX/0xX;->d:LX/0Uh;

    const/16 v1, 0x461

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
