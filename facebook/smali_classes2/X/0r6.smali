.class public final enum LX/0r6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0r6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0r6;

.field public static final enum OFF:LX/0r6;

.field public static final enum ON:LX/0r6;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149053
    new-instance v0, LX/0r6;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v2}, LX/0r6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0r6;->OFF:LX/0r6;

    .line 149054
    new-instance v0, LX/0r6;

    const-string v1, "ON"

    invoke-direct {v0, v1, v3}, LX/0r6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0r6;->ON:LX/0r6;

    .line 149055
    const/4 v0, 0x2

    new-array v0, v0, [LX/0r6;

    sget-object v1, LX/0r6;->OFF:LX/0r6;

    aput-object v1, v0, v2

    sget-object v1, LX/0r6;->ON:LX/0r6;

    aput-object v1, v0, v3

    sput-object v0, LX/0r6;->$VALUES:[LX/0r6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 149052
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0r6;
    .locals 1

    .prologue
    .line 149051
    const-class v0, LX/0r6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0r6;

    return-object v0
.end method

.method public static values()[LX/0r6;
    .locals 1

    .prologue
    .line 149050
    sget-object v0, LX/0r6;->$VALUES:[LX/0r6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0r6;

    return-object v0
.end method
