.class public LX/15K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/15K;


# instance fields
.field public a:LX/0Zb;

.field public b:LX/0kx;

.field public c:LX/0Uh;

.field public d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Zj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zj",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kx;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180458
    const/4 v0, 0x0

    iput-object v0, p0, LX/15K;->e:LX/0Zj;

    .line 180459
    new-instance v0, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbui/uimetrics/UiMetricsActivityListener$1;-><init>(LX/15K;)V

    iput-object v0, p0, LX/15K;->f:Ljava/lang/Runnable;

    .line 180460
    iput-object p1, p0, LX/15K;->a:LX/0Zb;

    .line 180461
    iput-object p2, p0, LX/15K;->b:LX/0kx;

    .line 180462
    iput-object p3, p0, LX/15K;->c:LX/0Uh;

    .line 180463
    return-void
.end method

.method public static a(LX/0QB;)LX/15K;
    .locals 6

    .prologue
    .line 180444
    sget-object v0, LX/15K;->g:LX/15K;

    if-nez v0, :cond_1

    .line 180445
    const-class v1, LX/15K;

    monitor-enter v1

    .line 180446
    :try_start_0
    sget-object v0, LX/15K;->g:LX/15K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180447
    if-eqz v2, :cond_0

    .line 180448
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180449
    new-instance p0, LX/15K;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v4

    check-cast v4, LX/0kx;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/15K;-><init>(LX/0Zb;LX/0kx;LX/0Uh;)V

    .line 180450
    move-object v0, p0

    .line 180451
    sput-object v0, LX/15K;->g:LX/15K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180452
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180453
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180454
    :cond_1
    sget-object v0, LX/15K;->g:LX/15K;

    return-object v0

    .line 180455
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180456
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/15K;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 180433
    instance-of v0, p1, Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    .line 180434
    check-cast p1, Lorg/json/JSONObject;

    invoke-static {p0, p1}, LX/15K;->a$redex0(LX/15K;Lorg/json/JSONObject;)V

    .line 180435
    :cond_0
    :goto_0
    return-void

    .line 180436
    :cond_1
    instance-of v0, p1, Lorg/json/JSONArray;

    if-eqz v0, :cond_0

    .line 180437
    check-cast p1, Lorg/json/JSONArray;

    .line 180438
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 180439
    const/4 v0, 0x0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    .line 180440
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p0, v2}, LX/15K;->a(LX/15K;Ljava/lang/Object;)V

    .line 180441
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180442
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 180443
    :cond_2
    goto :goto_0

    :catch_0
    goto :goto_2
.end method

.method public static a$redex0(LX/15K;Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 180464
    invoke-virtual {p1}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v2

    .line 180465
    const/4 v0, 0x0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 180466
    :try_start_0
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 180467
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 180468
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 180469
    invoke-static {p0, v4}, LX/15K;->a(LX/15K;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180470
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 180471
    :cond_0
    iget-object v0, p0, LX/15K;->e:LX/0Zj;

    invoke-virtual {v0, p1}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 180472
    return-void

    :catch_0
    goto :goto_1
.end method

.method public static b(LX/15K;Landroid/view/View;)Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 180411
    if-nez p1, :cond_0

    .line 180412
    const/4 v0, 0x0

    .line 180413
    :goto_0
    return-object v0

    .line 180414
    :cond_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 180415
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 180416
    check-cast v0, Landroid/view/ViewGroup;

    .line 180417
    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_1

    move-object v0, p1

    .line 180418
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 180419
    invoke-static {p0, v0}, LX/15K;->b(LX/15K;Landroid/view/View;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 180420
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 180421
    :cond_1
    invoke-static {p0}, LX/15K;->d(LX/15K;)Lorg/json/JSONObject;

    move-result-object v0

    .line 180422
    :try_start_0
    invoke-static {p0}, LX/15K;->d(LX/15K;)Lorg/json/JSONObject;

    move-result-object v1

    .line 180423
    const-string v3, "x"

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 180424
    const-string v3, "y"

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 180425
    const-string v3, "width"

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 180426
    const-string v3, "height"

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 180427
    invoke-static {p0}, LX/15K;->d(LX/15K;)Lorg/json/JSONObject;

    move-result-object v3

    .line 180428
    const-string v4, "type"

    const-string v5, "view"

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180429
    const-string v4, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180430
    const-string v4, "rectangle"

    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180431
    const-string v1, "node"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 180432
    const-string v1, "children"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public static b$redex0(LX/15K;)Z
    .locals 2

    .prologue
    .line 180410
    iget-object v0, p0, LX/15K;->c:LX/0Uh;

    const/16 v1, 0x65d

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/15K;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 180406
    iget-object v0, p0, LX/15K;->e:LX/0Zj;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 180407
    if-nez v0, :cond_0

    .line 180408
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 180409
    :cond_0
    return-object v0
.end method
