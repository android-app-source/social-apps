.class public LX/0aJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0aJ;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .param p1    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84454
    iput-object p1, p0, LX/0aJ;->a:LX/0Uh;

    .line 84455
    return-void
.end method

.method public static a(LX/0QB;)LX/0aJ;
    .locals 4

    .prologue
    .line 84456
    sget-object v0, LX/0aJ;->b:LX/0aJ;

    if-nez v0, :cond_1

    .line 84457
    const-class v1, LX/0aJ;

    monitor-enter v1

    .line 84458
    :try_start_0
    sget-object v0, LX/0aJ;->b:LX/0aJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84459
    if-eqz v2, :cond_0

    .line 84460
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84461
    new-instance p0, LX/0aJ;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/0aJ;-><init>(LX/0Uh;)V

    .line 84462
    move-object v0, p0

    .line 84463
    sput-object v0, LX/0aJ;->b:LX/0aJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84464
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84465
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84466
    :cond_1
    sget-object v0, LX/0aJ;->b:LX/0aJ;

    return-object v0

    .line 84467
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84468
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
