.class public LX/1K4;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1K5;
.implements LX/0hl;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Dispatcher::",
        "LX/0fy;",
        ">",
        "LX/0hD;",
        "LX/1K5;",
        "LX/0hl;"
    }
.end annotation


# instance fields
.field private a:LX/0fx;

.field public b:LX/1DK;

.field public c:LX/0fy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDispatcher;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 230839
    invoke-direct {p0}, LX/0hD;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 230840
    iget-object v0, p0, LX/1K4;->b:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    .line 230841
    if-eqz v0, :cond_0

    .line 230842
    invoke-interface {v0}, LX/0g8;->m()V

    .line 230843
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 230844
    iget-object v0, p0, LX/1K4;->a:LX/0fx;

    if-nez v0, :cond_0

    .line 230845
    new-instance v0, LX/1ZG;

    invoke-direct {v0, p0}, LX/1ZG;-><init>(LX/1K4;)V

    move-object v0, v0

    .line 230846
    iput-object v0, p0, LX/1K4;->a:LX/0fx;

    .line 230847
    :cond_0
    iget-object v0, p0, LX/1K4;->b:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    iget-object v1, p0, LX/1K4;->a:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 230848
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 230849
    iget-object v0, p0, LX/1K4;->b:LX/1DK;

    invoke-interface {v0}, LX/1DK;->a()LX/0g8;

    move-result-object v0

    iget-object v1, p0, LX/1K4;->a:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    .line 230850
    return-void
.end method
