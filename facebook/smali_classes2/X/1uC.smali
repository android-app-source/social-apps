.class public final LX/1uC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1iq;


# instance fields
.field public final synthetic a:LX/1hL;

.field public final synthetic b:LX/1MP;


# direct methods
.method public constructor <init>(LX/1MP;LX/1hL;)V
    .locals 0

    .prologue
    .line 339639
    iput-object p1, p0, LX/1uC;->b:LX/1MP;

    iput-object p2, p0, LX/1uC;->a:LX/1hL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 339640
    iget-object v1, p0, LX/1uC;->b:LX/1MP;

    iget-object v0, p0, LX/1uC;->a:LX/1hL;

    invoke-virtual {v0}, Lcom/facebook/proxygen/NetworkStatusMonitor;->getRadioData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 339641
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339642
    :cond_0
    :goto_0
    return-void

    .line 339643
    :cond_1
    const v3, 0x6e0009

    const-string v2, "transfer_data_size"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339644
    const v3, 0x6e000b

    const-string v2, "full_power_time"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339645
    const v3, 0x6e000a

    const-string v2, "low_power_time"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339646
    const v3, 0x6e000d

    const-string v2, "total_up_bytes"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339647
    const v3, 0x6e000e

    const-string v2, "total_down_bytes"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339648
    const v3, 0x6e000f

    const-string v2, "total_request_count"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339649
    const v3, 0x6e0010

    const-string v2, "total_wakeup_count"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v3, v2}, LX/1MP;->a(LX/1MP;ILjava/lang/String;)V

    .line 339650
    iget-object v3, v1, LX/1MP;->z:LX/1BA;

    const p0, 0x6e000c

    const-string v2, "aggr_data_details"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, p0, v2}, LX/1BA;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
