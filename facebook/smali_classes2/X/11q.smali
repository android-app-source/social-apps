.class public final LX/11q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/11h;


# direct methods
.method public constructor <init>(LX/11h;)V
    .locals 0

    .prologue
    .line 173274
    iput-object p1, p0, LX/11q;->a:LX/11h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x56fa4de8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 173275
    iget-object v1, p0, LX/11q;->a:LX/11h;

    .line 173276
    invoke-static {v1}, LX/11h;->c(LX/11h;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 173277
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173278
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11p;

    .line 173279
    if-eqz v2, :cond_0

    .line 173280
    iget-object p1, v2, LX/11p;->c:LX/0Pq;

    move-object p1, p1

    .line 173281
    iget-boolean p2, p1, LX/0Pq;->b:Z

    move p1, p2

    .line 173282
    if-eqz p1, :cond_0

    .line 173283
    iget-object p1, v2, LX/11p;->c:LX/0Pq;

    move-object p1, p1

    .line 173284
    invoke-virtual {v2}, LX/11p;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v2}, LX/11h;->e(LX/11h;LX/0Pq;Ljava/lang/String;)V

    .line 173285
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 173286
    :cond_1
    const/16 v1, 0x27

    const v2, 0x4e6d7ba9    # 9.960761E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
