.class public final LX/0T8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/0Vb",
        "<*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 62562
    check-cast p1, LX/0Vb;

    check-cast p2, LX/0Vb;

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 62563
    iget-object v2, p1, LX/0Vb;->b:LX/0VZ;

    invoke-virtual {v2}, LX/0VZ;->ordinal()I

    move-result v2

    iget-object v3, p2, LX/0Vb;->b:LX/0VZ;

    invoke-virtual {v3}, LX/0VZ;->ordinal()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 62564
    :cond_0
    :goto_0
    return v0

    .line 62565
    :cond_1
    iget-object v2, p1, LX/0Vb;->b:LX/0VZ;

    invoke-virtual {v2}, LX/0VZ;->ordinal()I

    move-result v2

    iget-object v3, p2, LX/0Vb;->b:LX/0VZ;

    invoke-virtual {v3}, LX/0VZ;->ordinal()I

    move-result v3

    if-le v2, v3, :cond_2

    move v0, v1

    .line 62566
    goto :goto_0

    .line 62567
    :cond_2
    iget v2, p1, LX/0Vb;->e:I

    iget v3, p2, LX/0Vb;->e:I

    if-lt v2, v3, :cond_0

    .line 62568
    iget v0, p1, LX/0Vb;->e:I

    iget v2, p2, LX/0Vb;->e:I

    if-le v0, v2, :cond_3

    move v0, v1

    .line 62569
    goto :goto_0

    .line 62570
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
