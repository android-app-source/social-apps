.class public LX/1cv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Object;

.field public c:LX/1cn;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/Object;

.field public f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/1dQ;

.field public h:LX/1dQ;

.field public i:LX/1dQ;

.field public j:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field public s:I

.field public t:LX/1cz;

.field public u:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 283676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283677
    return-void
.end method

.method private static a(Ljava/lang/Object;LX/1mp;LX/1cv;)LX/1cz;
    .locals 2

    .prologue
    .line 283660
    const/4 v0, 0x0

    .line 283661
    if-eqz p1, :cond_3

    .line 283662
    if-eqz p2, :cond_1

    .line 283663
    iget-object v0, p2, LX/1cv;->t:LX/1cz;

    move-object v1, v0

    .line 283664
    if-eqz v1, :cond_0

    move-object v0, p0

    .line 283665
    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0, p1}, LX/1cz;->a(Landroid/graphics/drawable/Drawable;LX/1mp;)V

    :cond_0
    move-object v0, v1

    .line 283666
    :cond_1
    if-nez v0, :cond_2

    .line 283667
    check-cast p0, Landroid/graphics/drawable/Drawable;

    .line 283668
    sget-object v0, LX/1cy;->a:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cz;

    .line 283669
    if-nez v0, :cond_4

    .line 283670
    new-instance v0, LX/1cz;

    invoke-direct {v0, p0, p1}, LX/1cz;-><init>(Landroid/graphics/drawable/Drawable;LX/1mp;)V

    .line 283671
    :goto_0
    move-object v0, v0

    .line 283672
    :cond_2
    const/4 v1, 0x1

    .line 283673
    iput-boolean v1, v0, LX/1cz;->c:Z

    .line 283674
    :cond_3
    return-object v0

    .line 283675
    :cond_4
    invoke-virtual {v0, p0, p1}, LX/1cz;->a(Landroid/graphics/drawable/Drawable;LX/1mp;)V

    goto :goto_0
.end method

.method public static d(I)Z
    .locals 2

    .prologue
    .line 283659
    and-int/lit8 v0, p0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dK;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "LX/1dK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 283678
    invoke-virtual/range {p4 .. p4}, LX/1dK;->i()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual/range {p4 .. p4}, LX/1dK;->j()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual/range {p4 .. p4}, LX/1dK;->k()Landroid/util/SparseArray;

    move-result-object v7

    invoke-virtual/range {p4 .. p4}, LX/1dK;->f()LX/1dQ;

    move-result-object v8

    invoke-virtual/range {p4 .. p4}, LX/1dK;->g()LX/1dQ;

    move-result-object v9

    invoke-virtual/range {p4 .. p4}, LX/1dK;->h()LX/1dQ;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, LX/1dK;->n()LX/1dQ;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, LX/1dK;->o()LX/1dQ;

    move-result-object v12

    invoke-virtual/range {p4 .. p4}, LX/1dK;->p()LX/1dQ;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, LX/1dK;->q()LX/1dQ;

    move-result-object v14

    invoke-virtual/range {p4 .. p4}, LX/1dK;->r()LX/1dQ;

    move-result-object v15

    invoke-virtual/range {p4 .. p4}, LX/1dK;->s()LX/1dQ;

    move-result-object v16

    invoke-virtual/range {p4 .. p4}, LX/1dK;->t()LX/1dQ;

    move-result-object v17

    invoke-virtual/range {p4 .. p4}, LX/1dK;->u()LX/1dQ;

    move-result-object v18

    invoke-virtual/range {p4 .. p4}, LX/1dK;->v()LX/1mp;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v1, v2}, LX/1cv;->a(Ljava/lang/Object;LX/1mp;LX/1cv;)LX/1cz;

    move-result-object v19

    invoke-virtual/range {p4 .. p4}, LX/1dK;->c()I

    move-result v20

    invoke-virtual/range {p4 .. p4}, LX/1dK;->m()I

    move-result v21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-virtual/range {v1 .. v21}, LX/1cv;->a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1cz;II)V

    .line 283679
    return-void
.end method

.method public final a(LX/1X1;LX/1cn;Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Landroid/util/SparseArray;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1dQ;LX/1cz;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            "Ljava/lang/Object;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Object;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ;",
            "LX/1dQ",
            "<",
            "LX/32D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/32A;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48D;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3CI;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/2uO;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48E;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48F;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/48G;",
            ">;",
            "LX/1cz;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 283630
    iput-object p1, p0, LX/1cv;->a:LX/1X1;

    .line 283631
    iput-object p3, p0, LX/1cv;->b:Ljava/lang/Object;

    .line 283632
    iput-object p2, p0, LX/1cv;->c:LX/1cn;

    .line 283633
    iput-object p7, p0, LX/1cv;->g:LX/1dQ;

    .line 283634
    iput-object p8, p0, LX/1cv;->h:LX/1dQ;

    .line 283635
    iput-object p9, p0, LX/1cv;->i:LX/1dQ;

    .line 283636
    iput-object p10, p0, LX/1cv;->j:LX/1dQ;

    .line 283637
    iput-object p11, p0, LX/1cv;->k:LX/1dQ;

    .line 283638
    iput-object p12, p0, LX/1cv;->l:LX/1dQ;

    .line 283639
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1cv;->m:LX/1dQ;

    .line 283640
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1cv;->n:LX/1dQ;

    .line 283641
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1cv;->o:LX/1dQ;

    .line 283642
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1cv;->p:LX/1dQ;

    .line 283643
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1cv;->q:LX/1dQ;

    .line 283644
    move/from16 v0, p19

    iput v0, p0, LX/1cv;->u:I

    .line 283645
    iput-object p4, p0, LX/1cv;->d:Ljava/lang/CharSequence;

    .line 283646
    iput-object p5, p0, LX/1cv;->e:Ljava/lang/Object;

    .line 283647
    iput-object p6, p0, LX/1cv;->f:Landroid/util/SparseArray;

    .line 283648
    move/from16 v0, p20

    iput v0, p0, LX/1cv;->s:I

    .line 283649
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1cv;->t:LX/1cz;

    .line 283650
    iget-object v1, p0, LX/1cv;->b:Ljava/lang/Object;

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_2

    .line 283651
    iget-object v1, p0, LX/1cv;->b:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    .line 283652
    invoke-virtual {v1}, Landroid/view/View;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283653
    iget v2, p0, LX/1cv;->u:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, LX/1cv;->u:I

    .line 283654
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->isLongClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 283655
    iget v2, p0, LX/1cv;->u:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, LX/1cv;->u:I

    .line 283656
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 283657
    iget v1, p0, LX/1cv;->u:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, LX/1cv;->u:I

    .line 283658
    :cond_2
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 283595
    iget-object v0, p0, LX/1cv;->b:Ljava/lang/Object;

    instance-of v0, v0, LX/1cn;

    if-nez v0, :cond_0

    .line 283596
    iget-object v0, p0, LX/1cv;->a:LX/1X1;

    .line 283597
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 283598
    iget-object v1, p0, LX/1cv;->b:Ljava/lang/Object;

    invoke-static {p1, v0, v1}, LX/1cy;->a(Landroid/content/Context;LX/1S3;Ljava/lang/Object;)V

    .line 283599
    :cond_0
    iget-object v0, p0, LX/1cv;->t:LX/1cz;

    if-eqz v0, :cond_1

    .line 283600
    iget-object v0, p0, LX/1cv;->t:LX/1cz;

    .line 283601
    const/4 p1, 0x0

    .line 283602
    invoke-virtual {v0, p1}, LX/1cz;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 283603
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1cz;->c:Z

    .line 283604
    iput-object p1, v0, LX/1cz;->b:LX/1mp;

    .line 283605
    iget-object v1, v0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 283606
    iput-object p1, v0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    .line 283607
    sget-object v1, LX/1cy;->a:LX/0Zk;

    invoke-interface {v1, v0}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 283608
    iput-object v2, p0, LX/1cv;->t:LX/1cz;

    .line 283609
    :cond_1
    iput-object v2, p0, LX/1cv;->a:LX/1X1;

    .line 283610
    iput-object v2, p0, LX/1cv;->c:LX/1cn;

    .line 283611
    iput-object v2, p0, LX/1cv;->b:Ljava/lang/Object;

    .line 283612
    iput v3, p0, LX/1cv;->u:I

    .line 283613
    iput-object v2, p0, LX/1cv;->d:Ljava/lang/CharSequence;

    .line 283614
    iput-object v2, p0, LX/1cv;->g:LX/1dQ;

    .line 283615
    iput-object v2, p0, LX/1cv;->h:LX/1dQ;

    .line 283616
    iput-object v2, p0, LX/1cv;->i:LX/1dQ;

    .line 283617
    iput-object v2, p0, LX/1cv;->j:LX/1dQ;

    .line 283618
    iput-object v2, p0, LX/1cv;->k:LX/1dQ;

    .line 283619
    iput-object v2, p0, LX/1cv;->l:LX/1dQ;

    .line 283620
    iput-object v2, p0, LX/1cv;->m:LX/1dQ;

    .line 283621
    iput-object v2, p0, LX/1cv;->n:LX/1dQ;

    .line 283622
    iput-object v2, p0, LX/1cv;->o:LX/1dQ;

    .line 283623
    iput-object v2, p0, LX/1cv;->p:LX/1dQ;

    .line 283624
    iput-object v2, p0, LX/1cv;->q:LX/1dQ;

    .line 283625
    iput-object v2, p0, LX/1cv;->e:Ljava/lang/Object;

    .line 283626
    iput-object v2, p0, LX/1cv;->f:Landroid/util/SparseArray;

    .line 283627
    iput-boolean v3, p0, LX/1cv;->r:Z

    .line 283628
    iput v3, p0, LX/1cv;->s:I

    .line 283629
    return-void
.end method

.method public t()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 283594
    const/4 v0, 0x0

    return-object v0
.end method

.method public final u()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 283586
    iget-object v1, p0, LX/1cv;->a:LX/1X1;

    if-nez v1, :cond_1

    .line 283587
    :cond_0
    :goto_0
    return v0

    .line 283588
    :cond_1
    iget v1, p0, LX/1cv;->s:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 283589
    iget-object v0, p0, LX/1cv;->o:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->k:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->m:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->p:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->j:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->q:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->l:LX/1dQ;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1cv;->n:LX/1dQ;

    if-eqz v0, :cond_3

    .line 283590
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 283591
    :cond_3
    iget-object v0, p0, LX/1cv;->a:LX/1X1;

    .line 283592
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 283593
    invoke-virtual {v0}, LX/1S3;->h()Z

    move-result v0

    goto :goto_0
.end method
