.class public abstract LX/1su;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public e:LX/1ss;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 335650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335651
    return-void
.end method

.method public constructor <init>(LX/1ss;)V
    .locals 0

    .prologue
    .line 335652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335653
    iput-object p1, p0, LX/1su;->e:LX/1ss;

    .line 335654
    return-void
.end method

.method public static s()Z
    .locals 2

    .prologue
    .line 335655
    new-instance v0, LX/7H0;

    const-string v1, "Peeking into a map not supported, likely because it\'s sized"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static t()Z
    .locals 2

    .prologue
    .line 335656
    new-instance v0, LX/7H0;

    const-string v1, "Peeking into a list not supported, likely because it\'s sized"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static u()Z
    .locals 2

    .prologue
    .line 335649
    new-instance v0, LX/7H0;

    const-string v1, "Peeking into a set not supported, likely because it\'s sized"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(B)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(LX/1sw;)V
.end method

.method public abstract a(LX/1u3;)V
.end method

.method public abstract a(LX/7H3;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(S)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([B)V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()LX/1sv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1sv;"
        }
    .end annotation
.end method

.method public abstract e()V
.end method

.method public abstract f()LX/1sw;
.end method

.method public abstract g()LX/7H3;
.end method

.method public abstract h()LX/1u3;
.end method

.method public abstract i()LX/7H5;
.end method

.method public abstract j()Z
.end method

.method public abstract k()B
.end method

.method public abstract l()S
.end method

.method public abstract m()I
.end method

.method public abstract n()J
.end method

.method public abstract o()D
.end method

.method public abstract p()Ljava/lang/String;
.end method

.method public abstract q()[B
.end method

.method public final r()LX/1sv;
    .locals 1

    .prologue
    .line 335648
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    invoke-virtual {p0}, LX/1su;->d()LX/1sv;

    move-result-object v0

    return-object v0
.end method
