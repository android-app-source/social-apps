.class public final enum LX/0eh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0eh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0eh;

.field public static final enum FBSTR:LX/0eh;

.field public static final enum LANGPACK:LX/0eh;


# instance fields
.field private final mServerValue:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 92376
    new-instance v0, LX/0eh;

    const-string v1, "FBSTR"

    const-string v2, "fbstr"

    const-string v3, "legacy_fbstr"

    invoke-direct {v0, v1, v4, v2, v3}, LX/0eh;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0eh;->FBSTR:LX/0eh;

    .line 92377
    new-instance v0, LX/0eh;

    const-string v1, "LANGPACK"

    const-string v2, "langpack"

    const-string v3, "langpack"

    invoke-direct {v0, v1, v5, v2, v3}, LX/0eh;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0eh;->LANGPACK:LX/0eh;

    .line 92378
    const/4 v0, 0x2

    new-array v0, v0, [LX/0eh;

    sget-object v1, LX/0eh;->FBSTR:LX/0eh;

    aput-object v1, v0, v4

    sget-object v1, LX/0eh;->LANGPACK:LX/0eh;

    aput-object v1, v0, v5

    sput-object v0, LX/0eh;->$VALUES:[LX/0eh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 92373
    iput-object p3, p0, LX/0eh;->mValue:Ljava/lang/String;

    .line 92374
    iput-object p4, p0, LX/0eh;->mServerValue:Ljava/lang/String;

    .line 92375
    return-void
.end method

.method public static fromFormat(Ljava/lang/String;)LX/0eh;
    .locals 5

    .prologue
    .line 92366
    if-eqz p0, :cond_1

    .line 92367
    invoke-static {}, LX/0eh;->values()[LX/0eh;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 92368
    invoke-virtual {v3}, LX/0eh;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 92369
    return-object v3

    .line 92370
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92371
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized language file format : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getMatchAnyPattern()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 92379
    const/4 v0, 0x1

    .line 92380
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92381
    invoke-static {}, LX/0eh;->values()[LX/0eh;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 92382
    if-eqz v0, :cond_0

    move v0, v1

    .line 92383
    :goto_1
    invoke-virtual {v6}, LX/0eh;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92384
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 92385
    :cond_0
    const-string v7, "|"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 92386
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0eh;
    .locals 1

    .prologue
    .line 92365
    const-class v0, LX/0eh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0eh;

    return-object v0
.end method

.method public static values()[LX/0eh;
    .locals 1

    .prologue
    .line 92364
    sget-object v0, LX/0eh;->$VALUES:[LX/0eh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0eh;

    return-object v0
.end method


# virtual methods
.method public final getServerValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92363
    iget-object v0, p0, LX/0eh;->mServerValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92362
    iget-object v0, p0, LX/0eh;->mValue:Ljava/lang/String;

    return-object v0
.end method
