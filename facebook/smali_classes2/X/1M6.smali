.class public LX/1M6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1M7;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1M8;

.field private final c:LX/03V;

.field private final d:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "LX/36s;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final e:LX/1Li;

.field private f:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234828
    const-class v0, LX/1M6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1M6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1M8;LX/03V;LX/1Li;)V
    .locals 1

    .prologue
    .line 234829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234830
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/1M6;->d:Ljava/util/Deque;

    .line 234831
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1M6;->f:Z

    .line 234832
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1M6;->g:Z

    .line 234833
    iput-object p1, p0, LX/1M6;->b:LX/1M8;

    .line 234834
    iput-object p2, p0, LX/1M6;->c:LX/03V;

    .line 234835
    iput-object p3, p0, LX/1M6;->e:LX/1Li;

    .line 234836
    return-void
.end method

.method private f()Z
    .locals 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 234846
    iget-boolean v0, p0, LX/1M6;->g:Z

    if-eqz v0, :cond_0

    .line 234847
    iget-object v0, p0, LX/1M6;->c:LX/03V;

    sget-object v1, LX/1M6;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tried to access methods on a released list: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/1M6;->e:LX/1Li;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 234848
    const/4 v0, 0x1

    .line 234849
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 234837
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1M6;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 234838
    :cond_0
    monitor-exit p0

    return-void

    .line 234839
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 234840
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234841
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36s;

    .line 234842
    iget-object v2, v0, LX/36s;->d:Landroid/net/Uri;

    move-object v0, v2

    .line 234843
    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234844
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234845
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 234816
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/1M6;->f:Z

    .line 234817
    iget-object v0, p0, LX/1M6;->b:LX/1M8;

    .line 234818
    iget-object p1, v0, LX/1M8;->a:LX/1Lg;

    invoke-static {p1}, LX/1Lg;->c(LX/1Lg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234819
    monitor-exit p0

    return-void

    .line 234820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final varargs declared-synchronized a([LX/36s;)V
    .locals 4

    .prologue
    .line 234821
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1M6;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 234822
    :goto_0
    monitor-exit p0

    return-void

    .line 234823
    :cond_0
    :try_start_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 234824
    iget-object v3, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v3, v2}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 234825
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 234826
    :cond_1
    iget-object v0, p0, LX/1M6;->b:LX/1M8;

    invoke-virtual {v0}, LX/1M8;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234827
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 234815
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1M6;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/1M6;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 234811
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1M6;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 234812
    :goto_0
    monitor-exit p0

    return-void

    .line 234813
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234814
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final varargs declared-synchronized b([LX/36s;)V
    .locals 4

    .prologue
    .line 234803
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1M6;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 234804
    :goto_0
    monitor-exit p0

    return-void

    .line 234805
    :cond_0
    :try_start_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v2, p1, v0

    .line 234806
    iget-object v3, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v3, v2}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 234807
    iget-object v3, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v3, v2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 234808
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 234809
    :cond_2
    iget-object v0, p0, LX/1M6;->b:LX/1M8;

    invoke-virtual {v0}, LX/1M8;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234810
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final varargs declared-synchronized c([LX/36s;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 234792
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1M6;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 234793
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 234794
    :cond_1
    :try_start_1
    array-length v2, p1

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, p1, v1

    .line 234795
    iget-object v4, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v4, v3}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 234796
    const/4 v0, 0x1

    .line 234797
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 234798
    :cond_3
    if-eqz v0, :cond_0

    .line 234799
    iget-object v0, p0, LX/1M6;->b:LX/1M8;

    .line 234800
    iget-object v1, v0, LX/1M8;->a:LX/1Lg;

    invoke-static {v1}, LX/1Lg;->c(LX/1Lg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234801
    goto :goto_0

    .line 234802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 234791
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1M6;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/36s;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 234790
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1M6;->d:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
