.class public LX/1cT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1cF",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/1HH;


# direct methods
.method public constructor <init>(LX/1cF;LX/1HH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<TT;>;",
            "LX/1HH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282089
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/1cT;->a:LX/1cF;

    .line 282090
    iput-object p2, p0, LX/1cT;->b:LX/1HH;

    .line 282091
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282092
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v3, v0

    .line 282093
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v0

    .line 282094
    new-instance v0, Lcom/facebook/imagepipeline/producers/ThreadHandoffProducer$1;

    const-string v4, "BackgroundThreadHandoffProducer"

    move-object v1, p0

    move-object v2, p1

    move-object v6, v3

    move-object v7, v5

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/imagepipeline/producers/ThreadHandoffProducer$1;-><init>(LX/1cT;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1BV;Ljava/lang/String;LX/1cd;LX/1cW;)V

    .line 282095
    new-instance v1, LX/1cf;

    invoke-direct {v1, p0, v0}, LX/1cf;-><init>(LX/1cT;Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;)V

    invoke-virtual {p2, v1}, LX/1cW;->a(LX/1cg;)V

    .line 282096
    iget-object v1, p0, LX/1cT;->b:LX/1HH;

    invoke-virtual {v1, v0}, LX/1HH;->a(Ljava/lang/Runnable;)V

    .line 282097
    return-void
.end method
