.class public final LX/1Nx;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public final synthetic a:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V
    .locals 0

    .prologue
    .line 238431
    iput-object p1, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 238432
    iget-object v0, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->G:Z

    if-nez v0, :cond_0

    .line 238433
    iget-object v0, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    iget-object v1, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->b:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 238434
    :goto_0
    iget-object v1, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    iget-object v2, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v2, v2, Landroid/support/v4/widget/SwipeRefreshLayout;->a:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    add-int/2addr v0, v1

    .line 238435
    iget-object v1, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->t:LX/1Nz;

    invoke-virtual {v1}, LX/1Nz;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 238436
    iget-object v1, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v2, 0x0

    .line 238437
    invoke-static {v1, v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a$redex0(Landroid/support/v4/widget/SwipeRefreshLayout;IZ)V

    .line 238438
    iget-object v0, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->w:LX/1O1;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, LX/1O1;->a(F)V

    .line 238439
    return-void

    .line 238440
    :cond_0
    iget-object v0, p0, LX/1Nx;->a:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->C:F

    float-to-int v0, v0

    goto :goto_0
.end method
