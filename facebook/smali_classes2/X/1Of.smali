.class public abstract LX/1Of;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Om;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3x5;",
            ">;"
        }
    .end annotation
.end field

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    const-wide/16 v2, 0x78

    .line 241796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241797
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Of;->a:LX/1Om;

    .line 241798
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Of;->b:Ljava/util/ArrayList;

    .line 241799
    iput-wide v2, p0, LX/1Of;->c:J

    .line 241800
    iput-wide v2, p0, LX/1Of;->d:J

    .line 241801
    iput-wide v4, p0, LX/1Of;->e:J

    .line 241802
    iput-wide v4, p0, LX/1Of;->f:J

    .line 241803
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Of;->g:Z

    .line 241804
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(LX/1a1;Z)V
    .locals 1

    .prologue
    .line 241793
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    if-eqz v0, :cond_0

    .line 241794
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    invoke-interface {v0, p1}, LX/1Om;->d(LX/1a1;)V

    .line 241795
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 241791
    iput-boolean p1, p0, LX/1Of;->g:Z

    .line 241792
    return-void
.end method

.method public abstract a(LX/1a1;)Z
.end method

.method public abstract a(LX/1a1;IIII)Z
.end method

.method public abstract a(LX/1a1;LX/1a1;IIII)Z
.end method

.method public final a(LX/3x5;)Z
    .locals 2

    .prologue
    .line 241805
    invoke-virtual {p0}, LX/1Of;->b()Z

    move-result v0

    .line 241806
    if-eqz p1, :cond_0

    .line 241807
    if-eqz v0, :cond_0

    .line 241808
    iget-object v1, p0, LX/1Of;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241809
    :cond_0
    return v0
.end method

.method public abstract b()Z
.end method

.method public abstract b(LX/1a1;)Z
.end method

.method public abstract c()V
.end method

.method public abstract c(LX/1a1;)V
.end method

.method public final d(LX/1a1;)V
    .locals 1

    .prologue
    .line 241788
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    if-eqz v0, :cond_0

    .line 241789
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    invoke-interface {v0, p1}, LX/1Om;->a(LX/1a1;)V

    .line 241790
    :cond_0
    return-void
.end method

.method public final e(LX/1a1;)V
    .locals 1

    .prologue
    .line 241785
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    if-eqz v0, :cond_0

    .line 241786
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    invoke-interface {v0, p1}, LX/1Om;->c(LX/1a1;)V

    .line 241787
    :cond_0
    return-void
.end method

.method public final f(LX/1a1;)V
    .locals 1

    .prologue
    .line 241776
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    if-eqz v0, :cond_0

    .line 241777
    iget-object v0, p0, LX/1Of;->a:LX/1Om;

    invoke-interface {v0, p1}, LX/1Om;->b(LX/1a1;)V

    .line 241778
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 241779
    iget-object v0, p0, LX/1Of;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 241780
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 241781
    iget-object v2, p0, LX/1Of;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 241782
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241783
    :cond_0
    iget-object v0, p0, LX/1Of;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 241784
    return-void
.end method
