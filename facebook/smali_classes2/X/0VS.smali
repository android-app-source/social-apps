.class public LX/0VS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/03G;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0VS;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/03V;

.field public final c:LX/00G;

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/03V;LX/0VT;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/debug/fblog/AppLoggingPrefix;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67808
    iput-object p1, p0, LX/0VS;->a:Ljava/lang/String;

    .line 67809
    iput-object p2, p0, LX/0VS;->b:LX/03V;

    .line 67810
    invoke-virtual {p3}, LX/0VT;->a()LX/00G;

    move-result-object v0

    iput-object v0, p0, LX/0VS;->c:LX/00G;

    .line 67811
    return-void
.end method

.method public static a(LX/0QB;)LX/0VS;
    .locals 6

    .prologue
    .line 67791
    sget-object v0, LX/0VS;->e:LX/0VS;

    if-nez v0, :cond_1

    .line 67792
    const-class v1, LX/0VS;

    monitor-enter v1

    .line 67793
    :try_start_0
    sget-object v0, LX/0VS;->e:LX/0VS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67794
    if-eqz v2, :cond_0

    .line 67795
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67796
    new-instance p0, LX/0VS;

    .line 67797
    const-string v3, "fb4a"

    move-object v3, v3

    .line 67798
    move-object v3, v3

    .line 67799
    check-cast v3, Ljava/lang/String;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v5

    check-cast v5, LX/0VT;

    invoke-direct {p0, v3, v4, v5}, LX/0VS;-><init>(Ljava/lang/String;LX/03V;LX/0VT;)V

    .line 67800
    move-object v0, p0

    .line 67801
    sput-object v0, LX/0VS;->e:LX/0VS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67802
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67803
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67804
    :cond_1
    sget-object v0, LX/0VS;->e:LX/0VS;

    return-object v0

    .line 67805
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 67785
    new-instance v0, LX/49I;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, LX/49I;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67786
    iget-object v1, p0, LX/0VS;->b:LX/03V;

    invoke-static {p1, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 67787
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 67788
    move-object v0, v2

    .line 67789
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 67790
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 67783
    iput p1, p0, LX/0VS;->d:I

    .line 67784
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67776
    iget-object v0, p0, LX/0VS;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 67777
    :goto_0
    move-object v0, p2

    .line 67778
    invoke-static {p1, v0, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 67779
    return-void

    .line 67780
    :cond_0
    iget-object v0, p0, LX/0VS;->c:LX/00G;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0VS;->c:LX/00G;

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67781
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0VS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 67782
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0VS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0VS;->c:LX/00G;

    invoke-virtual {v1}, LX/00G;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67760
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1, p2}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67761
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 67774
    const/4 v0, 0x5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67775
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 67773
    iget v0, p0, LX/0VS;->d:I

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67771
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1, p2}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67772
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 67769
    const/4 v0, 0x6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67770
    return-void
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 67768
    iget v0, p0, LX/0VS;->d:I

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67765
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0VS;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67766
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1, p2}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67767
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 67762
    invoke-direct {p0, p1, p2, p3}, LX/0VS;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67763
    const/4 v0, 0x6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, LX/0VS;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 67764
    return-void
.end method
