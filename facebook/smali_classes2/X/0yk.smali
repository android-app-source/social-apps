.class public LX/0yk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

.field public b:[Ljava/lang/String;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:[D


# direct methods
.method public constructor <init>(Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;)V
    .locals 0
    .param p1    # Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166103
    iput-object p1, p0, LX/0yk;->a:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    .line 166104
    return-void
.end method


# virtual methods
.method public final a(LX/14s;)[D
    .locals 10

    .prologue
    .line 166105
    iget-object v2, p0, LX/0yk;->b:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 166106
    const-string v4, "weight_final"

    invoke-static {v0, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 166107
    iget-object v4, p0, LX/0yk;->d:[D

    iget-object v5, p0, LX/0yk;->c:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 166108
    iget-wide v8, p1, LX/14s;->mRankingWeight:D

    move-wide v6, v8

    .line 166109
    aput-wide v6, v4, v0

    .line 166110
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 166111
    :cond_1
    iget-object v0, p0, LX/0yk;->d:[D

    return-object v0
.end method
