.class public LX/0S2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0RC;
.implements LX/0RD;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:Ljava/lang/Object;


# instance fields
.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public g:LX/0QA;

.field public h:LX/0RB;

.field private i:LX/0SG;

.field private j:LX/0SH;

.field private k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private l:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60711
    const-class v0, LX/0S2;

    sput-object v0, LX/0S2;->b:Ljava/lang/Class;

    .line 60712
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0S2;->a:Ljava/lang/Object;

    .line 60713
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0S2;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60706
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0S2;->d:Ljava/lang/Object;

    .line 60707
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0S2;->e:Ljava/util/Map;

    .line 60708
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0S2;->f:Ljava/util/Map;

    .line 60709
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0S2;->l:J

    .line 60710
    return-void
.end method

.method public static a(LX/0S7;)V
    .locals 0

    .prologue
    .line 60702
    invoke-virtual {p0}, LX/0S7;->c()V

    .line 60703
    invoke-virtual {p0}, LX/0S7;->b()V

    .line 60704
    return-void
.end method

.method public static a$redex0(LX/0S2;LX/0ov;)LX/0S7;
    .locals 1

    .prologue
    .line 60698
    iget-object v0, p0, LX/0S2;->g:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    .line 60699
    invoke-virtual {v0}, LX/0S7;->a()V

    .line 60700
    invoke-virtual {v0, p1}, LX/0S7;->a(LX/0R6;)V

    .line 60701
    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 60690
    iget-object v1, p0, LX/0S2;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 60691
    :try_start_0
    iget-wide v2, p0, LX/0S2;->l:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 60692
    monitor-exit v1

    .line 60693
    :goto_0
    return-void

    .line 60694
    :cond_0
    iget-object v0, p0, LX/0S2;->i:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/0S2;->l:J

    const-wide/16 v6, 0x7530

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 60695
    iget-object v0, p0, LX/0S2;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 60696
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/0S2;->l:J

    .line 60697
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/0Or;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Or",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60689
    new-instance v0, LX/42b;

    invoke-direct {v0, p0, p1}, LX/42b;-><init>(LX/0S2;LX/0Or;)V

    return-object v0
.end method

.method public final a(LX/0op;)LX/0S7;
    .locals 4

    .prologue
    .line 60679
    iget-object v0, p1, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v2, v0

    .line 60680
    sget-object v0, LX/0S2;->c:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ov;

    .line 60681
    if-eqz v0, :cond_1

    .line 60682
    :cond_0
    :goto_0
    move-object v0, v0

    .line 60683
    invoke-static {p0, v0}, LX/0S2;->a$redex0(LX/0S2;LX/0ov;)LX/0S7;

    move-result-object v0

    return-object v0

    .line 60684
    :cond_1
    new-instance v1, LX/0ov;

    .line 60685
    iget-object v0, p1, LX/0op;->b:LX/0SI;

    move-object v0, v0

    .line 60686
    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/0ov;-><init>(LX/0S2;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 60687
    sget-object v0, LX/0S2;->c:Ljava/lang/Object;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ov;

    .line 60688
    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)LX/0op;
    .locals 5

    .prologue
    .line 60633
    invoke-direct {p0}, LX/0S2;->b()V

    .line 60634
    iget-object v0, p0, LX/0S2;->g:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    .line 60635
    invoke-interface {v0}, LX/0R6;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60636
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 60637
    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    move-object v1, v0

    .line 60638
    :goto_0
    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 60639
    if-nez v0, :cond_6

    .line 60640
    invoke-interface {v1}, LX/0SI;->c()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 60641
    iget-object v2, p0, LX/0S2;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 60642
    if-nez v0, :cond_2

    .line 60643
    :try_start_0
    sget-object v0, LX/0S2;->b:Ljava/lang/Class;

    const-string v1, "Called user scoped provider with no viewer. ViewerContextManager was created with no ViewerContext. Using EmptyViewerContextManager to return fake logged in instance."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 60644
    iget-object v0, p0, LX/0S2;->j:LX/0SH;

    invoke-virtual {v0}, LX/0SH;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 60645
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 60646
    iget-object v0, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    .line 60647
    if-nez v0, :cond_0

    .line 60648
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    .line 60649
    iget-object v3, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60650
    :cond_0
    iget-object v1, p0, LX/0S2;->j:LX/0SH;

    .line 60651
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60652
    :goto_2
    sget-object v2, LX/0op;->a:LX/0ou;

    invoke-virtual {v2}, LX/0ou;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0op;

    .line 60653
    iput-object v1, v2, LX/0op;->b:LX/0SI;

    .line 60654
    iput-object v0, v2, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 60655
    move-object v0, v2

    .line 60656
    return-object v0

    .line 60657
    :cond_1
    invoke-interface {v0}, LX/0R6;->d()LX/0SI;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 60658
    :cond_2
    :try_start_1
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 60659
    iget-object v3, p0, LX/0S2;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 60660
    iget-object v3, p0, LX/0S2;->f:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_1

    .line 60661
    :cond_3
    iget-object v3, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 60662
    iget-object v3, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_1

    .line 60663
    :cond_4
    sget-object v0, LX/0S2;->b:Ljava/lang/Class;

    const-string v1, "Called user scoped provider with no viewer. ViewerContextManager was created with no ViewerContext. Using EmptyViewerContextManager to return fake logged in instance."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 60664
    iget-object v0, p0, LX/0S2;->j:LX/0SH;

    invoke-virtual {v0}, LX/0SH;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 60665
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 60666
    iget-object v0, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    .line 60667
    if-nez v0, :cond_5

    .line 60668
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    .line 60669
    iget-object v3, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60670
    :cond_5
    iget-object v1, p0, LX/0S2;->j:LX/0SH;

    goto :goto_1

    .line 60671
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 60672
    :cond_6
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 60673
    iget-object v3, p0, LX/0S2;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 60674
    :try_start_2
    iget-object v0, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    .line 60675
    if-nez v0, :cond_7

    .line 60676
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    .line 60677
    iget-object v4, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60678
    :cond_7
    monitor-exit v3

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a()V
    .locals 10

    .prologue
    .line 60621
    iget-object v5, p0, LX/0S2;->d:Ljava/lang/Object;

    monitor-enter v5

    .line 60622
    :try_start_0
    iget-object v2, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 60623
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 60624
    instance-of v2, v3, LX/0c5;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 60625
    :try_start_1
    move-object v0, v3

    check-cast v0, LX/0c5;

    move-object v2, v0

    invoke-interface {v2}, LX/0c5;->clearUserData()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 60626
    :catch_0
    move-exception v2

    move-object v4, v2

    .line 60627
    :try_start_2
    iget-object v2, p0, LX/0S2;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v8, "UserScope"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ".clearUserData() failure"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v8, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 60628
    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 60629
    :cond_2
    :try_start_3
    iget-object v2, p0, LX/0S2;->f:Ljava/util/Map;

    iget-object v3, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 60630
    iget-object v2, p0, LX/0S2;->i:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, LX/0S2;->l:J

    .line 60631
    iget-object v2, p0, LX/0S2;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 60632
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final a(LX/0QA;)V
    .locals 1

    .prologue
    .line 60610
    iput-object p1, p0, LX/0S2;->g:LX/0QA;

    .line 60611
    const-class v0, LX/0RB;

    invoke-virtual {p1, v0}, LX/0QA;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RB;

    iput-object v0, p0, LX/0S2;->h:LX/0RB;

    .line 60612
    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, LX/0S2;->i:LX/0SG;

    .line 60613
    new-instance v0, LX/0SH;

    invoke-direct {v0}, LX/0SH;-><init>()V

    .line 60614
    move-object v0, v0

    .line 60615
    move-object v0, v0

    .line 60616
    check-cast v0, LX/0SH;

    iput-object v0, p0, LX/0S2;->j:LX/0SH;

    .line 60617
    const/16 v0, 0x259

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/0S2;->k:LX/0Ot;

    .line 60618
    return-void
.end method

.method public annotationType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60620
    const-class v0, Lcom/facebook/auth/userscope/UserScoped;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 60619
    iget-object v0, p0, LX/0S2;->g:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    invoke-virtual {v0}, LX/0S7;->d()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
