.class public interface abstract LX/0t1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 153343
    new-instance v0, LX/0U1;

    const-string v1, "model_ref"

    const-string v2, "INTEGER NOT NULL REFERENCES models(_id) ON DELETE CASCADE"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0t1;->a:LX/0U1;

    .line 153344
    new-instance v0, LX/0U1;

    const-string v1, "id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0t1;->b:LX/0U1;

    .line 153345
    new-instance v0, LX/0U1;

    const-string v1, "field_path"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0t1;->c:LX/0U1;

    .line 153346
    new-instance v0, LX/0U1;

    const-string v1, "position_in_buffer"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0t1;->d:LX/0U1;

    .line 153347
    new-instance v0, LX/0U1;

    const-string v1, "field_index"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0t1;->e:LX/0U1;

    return-void
.end method
