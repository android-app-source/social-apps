.class public LX/0sa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final c:Ljava/lang/Integer;

.field public static final d:Ljava/lang/Integer;

.field private static volatile i:LX/0sa;


# instance fields
.field public final a:Ljava/lang/Integer;

.field public final b:Ljava/lang/Integer;

.field public final e:Landroid/content/res/Resources;

.field private final f:LX/0sd;

.field private final g:LX/0rq;

.field private final h:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152296
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/0sa;->c:Ljava/lang/Integer;

    .line 152297
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/0sa;->d:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0sd;LX/0rq;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152299
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/0sa;->a:Ljava/lang/Integer;

    .line 152300
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/0sa;->b:Ljava/lang/Integer;

    .line 152301
    iput-object p1, p0, LX/0sa;->e:Landroid/content/res/Resources;

    .line 152302
    iput-object p2, p0, LX/0sa;->f:LX/0sd;

    .line 152303
    iput-object p3, p0, LX/0sa;->g:LX/0rq;

    .line 152304
    iput-object p4, p0, LX/0sa;->h:LX/0ad;

    .line 152305
    return-void
.end method

.method public static a(LX/0QB;)LX/0sa;
    .locals 7

    .prologue
    .line 152306
    sget-object v0, LX/0sa;->i:LX/0sa;

    if-nez v0, :cond_1

    .line 152307
    const-class v1, LX/0sa;

    monitor-enter v1

    .line 152308
    :try_start_0
    sget-object v0, LX/0sa;->i:LX/0sa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152309
    if-eqz v2, :cond_0

    .line 152310
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152311
    new-instance p0, LX/0sa;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0sb;->b(LX/0QB;)LX/0sd;

    move-result-object v4

    check-cast v4, LX/0sd;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/0sa;-><init>(Landroid/content/res/Resources;LX/0sd;LX/0rq;LX/0ad;)V

    .line 152312
    move-object v0, p0

    .line 152313
    sput-object v0, LX/0sa;->i:LX/0sa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152314
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152315
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152316
    :cond_1
    sget-object v0, LX/0sa;->i:LX/0sa;

    return-object v0

    .line 152317
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152319
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152320
    invoke-static {}, LX/0wB;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 152321
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 152322
    :cond_0
    :goto_0
    return v0

    .line 152323
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 152324
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152325
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 152326
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 152327
    if-eqz v0, :cond_0

    const-string v1, "good_friends"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 4

    .prologue
    .line 152328
    invoke-static {p0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152329
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 152330
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x4ed245b

    if-ne v2, v3, :cond_0

    .line 152331
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v0

    .line 152332
    :goto_1
    return v0

    .line 152333
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    goto :goto_0

    .line 152334
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152335
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 152336
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aZ()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 152337
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aZ()Ljava/lang/String;

    move-result-object v0

    .line 152338
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152339
    if-nez p0, :cond_1

    .line 152340
    :cond_0
    :goto_0
    return v0

    .line 152341
    :cond_1
    const v1, -0x3625f733

    invoke-static {p0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 152342
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 152343
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 152344
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static p()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152345
    invoke-static {}, LX/0wB;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static q()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152346
    invoke-static {}, LX/0wB;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static r()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152347
    invoke-static {}, LX/0wB;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152348
    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152349
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 152350
    int-to-float v0, v0

    const v1, 0x3ff745d1

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 152351
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152352
    invoke-virtual {p0}, LX/0sa;->w()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final C()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152295
    invoke-virtual {p0}, LX/0sa;->x()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final D()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152353
    invoke-virtual {p0}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final I()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152258
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b092b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152259
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final J()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152260
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0956

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152261
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final K()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152262
    iget-object v0, p0, LX/0sa;->g:LX/0rq;

    invoke-virtual {v0}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final M()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152263
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0961

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152264
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final N()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152265
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0962

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152266
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152267
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0f5d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152268
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152269
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0932

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152270
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0925

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152271
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152256
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0926

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152257
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152272
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0942

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152273
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152274
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b0944

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152275
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152276
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b09a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 152277
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 152278
    iget-object v0, p0, LX/0sa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b09cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 152279
    iget-object v0, p0, LX/0sa;->h:LX/0ad;

    sget-short v1, LX/0wH;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152280
    iget v1, v0, LX/0sd;->b:I

    move v0, v1

    .line 152281
    :goto_0
    int-to-float v0, v0

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 152282
    :cond_0
    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152283
    iget v1, v0, LX/0sd;->c:I

    move v0, v1

    .line 152284
    goto :goto_0
.end method

.method public final x()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152285
    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152286
    iget p0, v0, LX/0sd;->c:I

    move v0, p0

    .line 152287
    div-int/lit8 v0, v0, 0x2

    .line 152288
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152289
    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152290
    iget p0, v0, LX/0sd;->c:I

    move v0, p0

    .line 152291
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 152292
    iget-object v0, p0, LX/0sa;->f:LX/0sd;

    .line 152293
    iget p0, v0, LX/0sd;->c:I

    move v0, p0

    .line 152294
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
