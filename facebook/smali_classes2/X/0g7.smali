.class public LX/0g7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;
.implements LX/0g8;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:I

.field public final c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0fx;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0fx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/1ON;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Landroid/widget/ListAdapter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:I

.field private n:I

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110690
    const-class v0, LX/0g7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0g7;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 110674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110675
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0g7;->d:Ljava/util/List;

    .line 110676
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0g7;->h:Ljava/util/ArrayList;

    .line 110677
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0g7;->i:Ljava/util/ArrayList;

    .line 110678
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 110679
    iput-object v0, p0, LX/0g7;->j:LX/0Ot;

    .line 110680
    iput v1, p0, LX/0g7;->m:I

    .line 110681
    iput v1, p0, LX/0g7;->n:I

    .line 110682
    iput-boolean v1, p0, LX/0g7;->o:Z

    .line 110683
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1OS;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 110684
    iput-object p1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110685
    new-instance v0, LX/1PD;

    invoke-direct {v0, p0}, LX/1PD;-><init>(LX/0g7;)V

    move-object v0, v0

    .line 110686
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 110687
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const v1, 0x7f0d0043

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setTag(ILjava/lang/Object;)V

    .line 110688
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/0g7;

    const/16 v0, 0x259

    invoke-static {v2, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v2, p0, LX/0g7;->j:LX/0Ot;

    .line 110689
    return-void
.end method

.method private F()I
    .locals 1

    .prologue
    .line 110673
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    invoke-interface {v0}, LX/1OS;->I()I

    move-result v0

    return v0
.end method

.method private G()I
    .locals 1

    .prologue
    .line 110549
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    invoke-interface {v0}, LX/1OS;->n()I

    move-result v0

    return v0
.end method

.method private H()V
    .locals 2

    .prologue
    .line 110669
    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    if-nez v0, :cond_0

    .line 110670
    :goto_0
    return-void

    .line 110671
    :cond_0
    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    iget-object v1, p0, LX/0g7;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/1ON;->a(Ljava/util/ArrayList;)V

    .line 110672
    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    iget-object v1, p0, LX/0g7;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/1ON;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static I()V
    .locals 2

    .prologue
    .line 110668
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "RecyclerViewProxy has not yet implemented this method."

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final A()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 110667
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 110664
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110665
    iget-boolean p0, v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->C:Z

    move v0, p0

    .line 110666
    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 110662
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 110663
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 110660
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 110661
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 110589
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setPadding(IIII)V

    .line 110590
    return-void
.end method

.method public final a(LX/0fu;)V
    .locals 1

    .prologue
    .line 110637
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->m()V

    .line 110638
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 110639
    return-void
.end method

.method public final a(LX/0fx;)V
    .locals 0

    .prologue
    .line 110635
    iput-object p1, p0, LX/0g7;->e:LX/0fx;

    .line 110636
    return-void
.end method

.method public a(LX/1OO;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1OO",
            "<+",
            "LX/1a1;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 110626
    if-nez p1, :cond_0

    .line 110627
    iput-object v1, p0, LX/0g7;->f:LX/1ON;

    .line 110628
    iput-object v1, p0, LX/0g7;->g:Landroid/widget/ListAdapter;

    .line 110629
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 110630
    :goto_0
    return-void

    .line 110631
    :cond_0
    new-instance v0, LX/1Qp;

    invoke-direct {v0, p1}, LX/1Qp;-><init>(LX/1OO;)V

    iput-object v0, p0, LX/0g7;->g:Landroid/widget/ListAdapter;

    .line 110632
    new-instance v0, LX/1ON;

    invoke-direct {v0, p1}, LX/1ON;-><init>(LX/1OO;)V

    iput-object v0, p0, LX/0g7;->f:LX/1ON;

    .line 110633
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110634
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/0g7;->f:LX/1ON;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_0
.end method

.method public final a(LX/1St;)V
    .locals 2

    .prologue
    .line 110622
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 110623
    :goto_0
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setRecyclerListener(LX/1OU;)V

    .line 110624
    return-void

    .line 110625
    :cond_0
    new-instance v0, LX/1Su;

    invoke-direct {v0, p0, p1}, LX/1Su;-><init>(LX/0g7;LX/1St;)V

    goto :goto_0
.end method

.method public final a(LX/1Z7;)V
    .locals 2

    .prologue
    .line 110617
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 110618
    :goto_0
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110619
    iput-object v0, v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->s:LX/1OT;

    .line 110620
    return-void

    .line 110621
    :cond_0
    new-instance v0, LX/1Z8;

    invoke-direct {v0, p0, p1}, LX/1Z8;-><init>(LX/0g7;LX/1Z7;)V

    goto :goto_0
.end method

.method public final a(LX/2i4;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 110604
    invoke-virtual {p0, v5}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v0

    .line 110605
    invoke-virtual {p0}, LX/0g7;->p()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v6

    .line 110606
    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/0g7;->F()I

    move-result v1

    .line 110607
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    neg-int v3, v2

    .line 110608
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 110609
    :goto_2
    if-eqz v6, :cond_4

    invoke-direct {p0}, LX/0g7;->G()I

    move-result v4

    .line 110610
    :goto_3
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v5

    :cond_0
    move-object v0, p1

    .line 110611
    invoke-virtual/range {v0 .. v5}, LX/2i4;->a(IIIII)V

    .line 110612
    return-void

    :cond_1
    move v1, v5

    .line 110613
    goto :goto_0

    :cond_2
    move v3, v5

    .line 110614
    goto :goto_1

    :cond_3
    move v2, v5

    .line 110615
    goto :goto_2

    :cond_4
    move v4, v5

    .line 110616
    goto :goto_3
.end method

.method public final a(LX/2ii;)V
    .locals 2

    .prologue
    .line 110600
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 110601
    :goto_0
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 110602
    return-void

    .line 110603
    :cond_0
    new-instance v0, LX/62c;

    invoke-direct {v0, p0, p1}, LX/62c;-><init>(LX/0g7;LX/2ii;)V

    goto :goto_0
.end method

.method public final a(LX/3TV;)V
    .locals 2

    .prologue
    .line 110596
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 110597
    :goto_0
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemLongClickListener(LX/1Oa;)V

    .line 110598
    return-void

    .line 110599
    :cond_0
    new-instance v0, LX/62d;

    invoke-direct {v0, p0, p1}, LX/62d;-><init>(LX/0g7;LX/3TV;)V

    goto :goto_0
.end method

.method public final a(LX/62F;)V
    .locals 2

    .prologue
    .line 110591
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 110592
    :goto_0
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110593
    iput-object v0, v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->v:LX/1OV;

    .line 110594
    return-void

    .line 110595
    :cond_0
    new-instance v0, LX/62e;

    invoke-direct {v0, p0, p1}, LX/62e;-><init>(LX/0g7;LX/62F;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 110699
    invoke-static {}, LX/0g7;->I()V

    .line 110700
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 110691
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 110692
    return-void
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 110719
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 110720
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110721
    iget-object v0, p0, LX/0g7;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 110722
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110723
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 110724
    iget-object v0, p0, LX/0g7;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110725
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110726
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 110727
    if-nez p1, :cond_0

    .line 110728
    iput-object v1, p0, LX/0g7;->f:LX/1ON;

    .line 110729
    iput-object v1, p0, LX/0g7;->g:Landroid/widget/ListAdapter;

    .line 110730
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 110731
    :goto_0
    return-void

    .line 110732
    :cond_0
    instance-of v0, p1, LX/1Cw;

    if-nez v0, :cond_1

    .line 110733
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "RecyclerViewProxy can only create delegate for Adapters which implement FbListAdapter"

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 110734
    :cond_1
    iput-object p1, p0, LX/0g7;->g:Landroid/widget/ListAdapter;

    .line 110735
    new-instance v0, LX/1ON;

    new-instance v1, LX/1UT;

    check-cast p1, LX/1Cw;

    iget-object v2, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v1, p1, v2}, LX/1UT;-><init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, LX/1ON;-><init>(LX/1OO;)V

    iput-object v0, p0, LX/0g7;->f:LX/1ON;

    .line 110736
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110737
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/0g7;->f:LX/1ON;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 110738
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 110739
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 110740
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setClipToPadding(Z)V

    .line 110741
    return-void
.end method

.method public final b()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 110742
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 110715
    invoke-static {}, LX/0g7;->I()V

    .line 110716
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 110717
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->scrollBy(II)V

    .line 110718
    return-void
.end method

.method public final b(LX/0fu;)V
    .locals 1

    .prologue
    .line 110713
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 110714
    return-void
.end method

.method public final b(LX/0fx;)V
    .locals 1

    .prologue
    .line 110711
    iget-object v0, p0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110712
    return-void
.end method

.method public final b(LX/2i4;)V
    .locals 2

    .prologue
    .line 110707
    iget v0, p1, LX/2i4;->a:I

    move v0, v0

    .line 110708
    iget v1, p1, LX/2i4;->c:I

    move v1, v1

    .line 110709
    neg-int v1, v1

    invoke-virtual {p0, v0, v1}, LX/0g7;->d(II)V

    .line 110710
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110704
    iget-object v0, p0, LX/0g7;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 110705
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110706
    return-void
.end method

.method public final b(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 110701
    iget-object v0, p0, LX/0g7;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110702
    invoke-direct {p0}, LX/0g7;->H()V

    .line 110703
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 110697
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVerticalScrollBarEnabled(Z)V

    .line 110698
    return-void
.end method

.method public final c(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 110694
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, LX/1a9;

    if-eqz v0, :cond_0

    .line 110695
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    .line 110696
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public final c(I)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 110640
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110641
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v2

    .line 110642
    if-nez v0, :cond_1

    .line 110643
    iget-boolean v0, p0, LX/0g7;->o:Z

    if-nez v0, :cond_0

    .line 110644
    iput-boolean v4, p0, LX/0g7;->o:Z

    .line 110645
    iget-object v0, p0, LX/0g7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/0g7;->b:Ljava/lang/String;

    const-string v3, "Attempting to getViewFromIndex without an adapter set (index: %d)"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 110646
    :goto_0
    return-object v0

    .line 110647
    :cond_1
    if-gez p1, :cond_3

    .line 110648
    iget-boolean v0, p0, LX/0g7;->o:Z

    if-nez v0, :cond_2

    .line 110649
    iput-boolean v4, p0, LX/0g7;->o:Z

    .line 110650
    iget-object v0, p0, LX/0g7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/0g7;->b:Ljava/lang/String;

    const-string v3, "Attempting to getViewFromIndex from an invalid index: %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    .line 110651
    goto :goto_0

    .line 110652
    :cond_3
    invoke-virtual {p0}, LX/0g7;->t()I

    move-result v0

    add-int v2, p1, v0

    .line 110653
    invoke-virtual {p0}, LX/0g7;->s()I

    move-result v0

    iget-object v3, p0, LX/0g7;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v3, v0, v3

    .line 110654
    if-lt v2, v3, :cond_5

    .line 110655
    iget-boolean v0, p0, LX/0g7;->o:Z

    if-nez v0, :cond_4

    .line 110656
    iput-boolean v4, p0, LX/0g7;->o:Z

    .line 110657
    iget-object v0, p0, LX/0g7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v4, LX/0g7;->b:Ljava/lang/String;

    const-string v5, "Attempting to getViewFromIndex from an invalid index: %d, footer index: %d"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 110658
    goto :goto_0

    .line 110659
    :cond_5
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/widget/listview/BetterListView;
    .locals 2

    .prologue
    .line 110693
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "RecyclerViewProxy has no BetterListView to expose."

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 110513
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getBetterLayoutManager()LX/1OS;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1OS;->d(II)V

    .line 110514
    return-void
.end method

.method public final c(LX/0fx;)V
    .locals 1

    .prologue
    .line 110544
    iget-object v0, p0, LX/0g7;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 110545
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 110542
    invoke-static {}, LX/0g7;->I()V

    .line 110543
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 110541
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getHeight()I

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 110539
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 110540
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 110535
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getClipToPadding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110536
    :goto_0
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->g(II)V

    .line 110537
    return-void

    .line 110538
    :cond_0
    invoke-virtual {p0}, LX/0g7;->g()I

    move-result v0

    sub-int/2addr p2, v0

    goto :goto_0
.end method

.method public final d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 110533
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, LX/0g7;->a(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 110534
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 110531
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setBroadcastInteractionChanges(Z)V

    .line 110532
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 110530
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingBottom()I

    move-result v0

    return v0
.end method

.method public final e(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 110529
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 110527
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, LX/0g7;->b(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 110528
    return-void
.end method

.method public final f(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 110526
    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    invoke-virtual {v0, p1}, LX/1ON;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110524
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setEmptyView(Landroid/view/View;)V

    .line 110525
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 110521
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110522
    iget p0, v0, Landroid/support/v7/widget/RecyclerView;->M:I

    move v0, p0

    .line 110523
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 110520
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 110517
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110518
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 110519
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 110516
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 110515
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public final h(I)J
    .locals 2

    .prologue
    .line 110512
    int-to-long v0, p1

    return-wide v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 110573
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public final ih_()Landroid/view/View;
    .locals 1

    .prologue
    .line 110588
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 110587
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getClipToPadding()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 110585
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOverScrollMode(I)V

    .line 110586
    return-void
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 110584
    invoke-virtual {p0}, LX/0g7;->p()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0g7;->r()I

    move-result v0

    invoke-virtual {p0}, LX/0g7;->s()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, LX/0g7;->p()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, LX/0g7;->d()I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 110580
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v7, 0x0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 110581
    iget-object v1, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 110582
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 110583
    return-void
.end method

.method public final n()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 110579
    invoke-virtual {p0}, LX/0g7;->p()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, LX/0g7;->F()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final o()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 110578
    iget-object v0, p0, LX/0g7;->g:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 110577
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 110574
    iget v0, p0, LX/0g7;->m:I

    if-eqz v0, :cond_0

    .line 110575
    iget v0, p0, LX/0g7;->k:I

    .line 110576
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/0g7;->F()I

    move-result v0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 110546
    iget v0, p0, LX/0g7;->m:I

    if-eqz v0, :cond_0

    .line 110547
    iget v0, p0, LX/0g7;->l:I

    .line 110548
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/0g7;->G()I

    move-result v0

    goto :goto_0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 110572
    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0g7;->f:LX/1ON;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 110571
    iget-object v0, p0, LX/0g7;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 110570
    iget-object v0, p0, LX/0g7;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 110567
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0}, LX/0g7;->t()I

    move-result v1

    .line 110568
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 110569
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 110565
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->m()V

    .line 110566
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 110563
    iget-object v0, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->destroyDrawingCache()V

    .line 110564
    return-void
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 110562
    iget v0, p0, LX/0g7;->a:I

    return v0
.end method

.method public final z()LX/0P1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110550
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 110551
    invoke-virtual {p0}, LX/0g7;->q()I

    move-result v0

    .line 110552
    invoke-virtual {p0}, LX/0g7;->r()I

    move-result v2

    .line 110553
    iget-object v3, p0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 110554
    iget-object v4, v3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v4

    .line 110555
    :goto_0
    if-gt v0, v2, :cond_1

    .line 110556
    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 110557
    invoke-virtual {p0, v0}, LX/0g7;->e(I)Landroid/view/View;

    move-result-object v4

    .line 110558
    if-eqz v4, :cond_0

    .line 110559
    int-to-long v6, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110560
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110561
    :cond_1
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method
