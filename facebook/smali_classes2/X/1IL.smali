.class public final LX/1IL;
.super LX/1IA;
.source ""


# static fields
.field public static final INSTANCE:LX/1IL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228630
    new-instance v0, LX/1IL;

    invoke-direct {v0}, LX/1IL;-><init>()V

    sput-object v0, LX/1IL;->INSTANCE:LX/1IL;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 228628
    invoke-direct {p0}, LX/1IA;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228631
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, LX/1IA;->apply(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final matches(C)Z
    .locals 1

    .prologue
    .line 228629
    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228627
    const-string v0, "CharMatcher.javaUpperCase()"

    return-object v0
.end method
