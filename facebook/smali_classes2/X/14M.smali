.class public final LX/14M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/0gh;


# direct methods
.method public constructor <init>(LX/0gh;)V
    .locals 0

    .prologue
    .line 178634
    iput-object p1, p0, LX/14M;->a:LX/0gh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x391c160a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 178635
    const-string v1, "chat_heads"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178636
    if-eqz v1, :cond_0

    const-string v2, "open"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "close"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 178637
    :cond_0
    const/16 v1, 0x27

    const v2, -0x6ea95aee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 178638
    :goto_0
    return-void

    .line 178639
    :cond_1
    const-string v2, "open"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/14M;->a:LX/0gh;

    iget-boolean v2, v2, LX/0gh;->A:Z

    if-nez v2, :cond_3

    .line 178640
    iget-object v1, p0, LX/14M;->a:LX/0gh;

    iget-object v2, p0, LX/14M;->a:LX/0gh;

    iget-object v2, v2, LX/0gh;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sget-object v4, LX/14j;->BACKGROUNDED:LX/14j;

    invoke-static {v1, v2, v3, v4}, LX/0gh;->a$redex0(LX/0gh;JLX/14j;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 178641
    const-string v2, "chat_heads"

    .line 178642
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 178643
    iget-object v2, p0, LX/14M;->a:LX/0gh;

    .line 178644
    invoke-static {v2, v1}, LX/0gh;->b$redex0(LX/0gh;Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 178645
    iget-object v1, p0, LX/14M;->a:LX/0gh;

    const/4 v2, 0x1

    .line 178646
    iput-boolean v2, v1, LX/0gh;->A:Z

    .line 178647
    :cond_2
    :goto_1
    const v1, -0x1e590904

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 178648
    :cond_3
    const-string v2, "close"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/14M;->a:LX/0gh;

    iget-boolean v1, v1, LX/0gh;->A:Z

    if-eqz v1, :cond_2

    .line 178649
    iget-object v1, p0, LX/14M;->a:LX/0gh;

    iget-object v2, p0, LX/14M;->a:LX/0gh;

    iget-object v2, v2, LX/0gh;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sget-object v4, LX/14j;->FOREGROUNDED:LX/14j;

    invoke-static {v1, v2, v3, v4}, LX/0gh;->a$redex0(LX/0gh;JLX/14j;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 178650
    const-string v2, "chat_heads"

    .line 178651
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 178652
    iget-object v2, p0, LX/14M;->a:LX/0gh;

    .line 178653
    iget-object v3, v2, LX/0gh;->p:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0mM;

    invoke-virtual {v3}, LX/0mM;->c()V

    .line 178654
    iget-object v3, v2, LX/0gh;->m:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 178655
    iget-object v1, p0, LX/14M;->a:LX/0gh;

    const/4 v2, 0x0

    .line 178656
    iput-boolean v2, v1, LX/0gh;->A:Z

    .line 178657
    goto :goto_1
.end method
