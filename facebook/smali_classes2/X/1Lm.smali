.class public LX/1Lm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ln;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Ln",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0SG;

.field public final c:LX/0lC;

.field public final d:LX/0en;

.field private final e:LX/1H3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1H3",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234184
    const-class v0, LX/1Lm;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Lm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0SG;LX/0lC;LX/0en;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/io/File;",
            ">;",
            "LX/0SG;",
            "LX/0lC;",
            "LX/0en;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234186
    new-instance v0, LX/1H3;

    invoke-direct {v0, p1}, LX/1H3;-><init>(LX/0Or;)V

    iput-object v0, p0, LX/1Lm;->e:LX/1H3;

    .line 234187
    iput-object p2, p0, LX/1Lm;->b:LX/0SG;

    .line 234188
    iput-object p3, p0, LX/1Lm;->c:LX/0lC;

    .line 234189
    iput-object p4, p0, LX/1Lm;->d:LX/0en;

    .line 234190
    return-void
.end method

.method private c(Ljava/lang/String;)LX/3DZ;
    .locals 3

    .prologue
    .line 234191
    new-instance v1, LX/3DZ;

    new-instance v2, Ljava/io/File;

    iget-object v0, p0, LX/1Lm;->e:LX/1H3;

    invoke-virtual {v0}, LX/1H3;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, p0, p1, v2}, LX/3DZ;-><init>(LX/1Lm;Ljava/lang/String;Ljava/io/File;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/3Dd;)LX/3Da;
    .locals 6

    .prologue
    .line 234192
    check-cast p1, Ljava/lang/String;

    .line 234193
    invoke-direct {p0, p1}, LX/1Lm;->c(Ljava/lang/String;)LX/3DZ;

    move-result-object v0

    .line 234194
    invoke-virtual {v0}, LX/3DZ;->f()V

    .line 234195
    iget-object v1, v0, LX/3DZ;->c:Ljava/io/File;

    invoke-static {v1}, LX/04M;->a(Ljava/io/File;)V

    .line 234196
    iput-object p2, v0, LX/3DZ;->e:LX/3Dd;

    .line 234197
    iget-object v2, v0, LX/3DZ;->a:LX/1Lm;

    iget-object v2, v2, LX/1Lm;->c:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->e()LX/0m9;

    move-result-object v2

    .line 234198
    iget-object v3, v0, LX/3DZ;->a:LX/1Lm;

    iget-object v3, v3, LX/1Lm;->c:LX/0lC;

    invoke-virtual {v3}, LX/0lC;->d()LX/0mC;

    .line 234199
    const-string v3, "version"

    const/4 v4, 0x2

    invoke-static {v4}, LX/0mC;->a(I)LX/0rP;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 234200
    const-string v3, "length"

    iget-object v4, v0, LX/3DZ;->e:LX/3Dd;

    iget-wide v4, v4, LX/3Dd;->a:J

    invoke-static {v4, v5}, LX/0mC;->a(J)LX/0rP;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 234201
    const-string v3, "mimeType"

    iget-object v4, v0, LX/3DZ;->e:LX/3Dd;

    iget-object v4, v4, LX/3Dd;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 234202
    iget-object v3, v0, LX/3DZ;->a:LX/1Lm;

    iget-object v3, v3, LX/1Lm;->c:LX/0lC;

    iget-object v4, v0, LX/3DZ;->d:Ljava/io/File;

    invoke-virtual {v3, v4, v2}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 234203
    invoke-static {v0}, LX/3DZ;->n(LX/3DZ;)V

    .line 234204
    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234205
    iget-object v0, p0, LX/1Lm;->e:LX/1H3;

    invoke-virtual {v0}, LX/1H3;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 234206
    if-nez v0, :cond_0

    .line 234207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 234208
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 234209
    check-cast p1, Ljava/lang/String;

    .line 234210
    invoke-direct {p0, p1}, LX/1Lm;->c(Ljava/lang/String;)LX/3DZ;

    move-result-object v0

    .line 234211
    invoke-virtual {v0}, LX/3DZ;->f()V

    .line 234212
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/3Da;
    .locals 2

    .prologue
    .line 234213
    check-cast p1, Ljava/lang/String;

    .line 234214
    invoke-direct {p0, p1}, LX/1Lm;->c(Ljava/lang/String;)LX/3DZ;

    move-result-object v0

    .line 234215
    iget-object v1, v0, LX/3DZ;->e:LX/3Dd;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 234216
    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
