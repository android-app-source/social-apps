.class public LX/0Ym;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0Ym;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Yn;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Yn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82425
    iput-object p1, p0, LX/0Ym;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 82426
    iput-object p2, p0, LX/0Ym;->b:LX/0Yn;

    .line 82427
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ym;
    .locals 5

    .prologue
    .line 82411
    sget-object v0, LX/0Ym;->c:LX/0Ym;

    if-nez v0, :cond_1

    .line 82412
    const-class v1, LX/0Ym;

    monitor-enter v1

    .line 82413
    :try_start_0
    sget-object v0, LX/0Ym;->c:LX/0Ym;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82414
    if-eqz v2, :cond_0

    .line 82415
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 82416
    new-instance p0, LX/0Ym;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Yn;->a(LX/0QB;)LX/0Yn;

    move-result-object v4

    check-cast v4, LX/0Yn;

    invoke-direct {p0, v3, v4}, LX/0Ym;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Yn;)V

    .line 82417
    move-object v0, p0

    .line 82418
    sput-object v0, LX/0Ym;->c:LX/0Ym;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82419
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82420
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82421
    :cond_1
    sget-object v0, LX/0Ym;->c:LX/0Ym;

    return-object v0

    .line 82422
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/api/feedtype/FeedType;
    .locals 4

    .prologue
    .line 82404
    iget-object v0, p0, LX/0Ym;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82405
    const/4 v0, 0x0

    .line 82406
    :goto_0
    return-object v0

    .line 82407
    :cond_0
    iget-object v0, p0, LX/0Ym;->b:LX/0Yn;

    invoke-virtual {v0}, LX/0Yn;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82408
    iget-object v0, p0, LX/0Ym;->b:LX/0Yn;

    invoke-virtual {v0}, LX/0Yn;->b()Ljava/lang/String;

    move-result-object v1

    .line 82409
    new-instance v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;

    new-instance v2, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    const-string v3, "TOP_STORIES"

    invoke-direct {v2, v1, v3}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-static {v1}, LX/2tT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;-><init>(Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;Lcom/facebook/api/feedtype/FeedType$Name;Ljava/lang/String;)V

    goto :goto_0

    .line 82410
    :cond_1
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;)Z
    .locals 1

    .prologue
    .line 82403
    invoke-virtual {p0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
