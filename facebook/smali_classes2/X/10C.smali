.class public LX/10C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/10D;

.field public final b:LX/0wM;


# direct methods
.method public constructor <init>(LX/10D;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 168477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168478
    iput-object p1, p0, LX/10C;->a:LX/10D;

    .line 168479
    iput-object p2, p0, LX/10C;->b:LX/0wM;

    .line 168480
    return-void
.end method

.method public static a(LX/0QB;)LX/10C;
    .locals 5

    .prologue
    .line 168481
    const-class v1, LX/10C;

    monitor-enter v1

    .line 168482
    :try_start_0
    sget-object v0, LX/10C;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 168483
    sput-object v2, LX/10C;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 168484
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168485
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 168486
    new-instance p0, LX/10C;

    invoke-static {v0}, LX/10D;->a(LX/0QB;)LX/10D;

    move-result-object v3

    check-cast v3, LX/10D;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-direct {p0, v3, v4}, LX/10C;-><init>(LX/10D;LX/0wM;)V

    .line 168487
    move-object v0, p0

    .line 168488
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 168489
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/10C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168490
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 168491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
