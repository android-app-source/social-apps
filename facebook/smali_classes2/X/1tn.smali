.class public final LX/1tn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1tr;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1tr;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 336810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336811
    iput-object p1, p0, LX/1tn;->a:LX/0QB;

    .line 336812
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1tr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336813
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/1tn;

    invoke-direct {v2, p0}, LX/1tn;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 336814
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1tn;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 336815
    packed-switch p2, :pswitch_data_0

    .line 336816
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336817
    :pswitch_0
    new-instance v0, LX/1tq;

    invoke-direct {v0}, LX/1tq;-><init>()V

    .line 336818
    move-object v0, v0

    .line 336819
    move-object v0, v0

    .line 336820
    :goto_0
    return-object v0

    .line 336821
    :pswitch_1
    new-instance v0, LX/1ts;

    invoke-direct {v0}, LX/1ts;-><init>()V

    .line 336822
    move-object v0, v0

    .line 336823
    move-object v0, v0

    .line 336824
    goto :goto_0

    .line 336825
    :pswitch_2
    new-instance v2, LX/1tt;

    const/16 v0, 0x1568

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const/16 v0, 0x1563

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v0

    check-cast v0, LX/0ka;

    invoke-static {p1}, LX/1tu;->a(LX/0QB;)LX/1tu;

    move-result-object v1

    check-cast v1, LX/1tu;

    invoke-direct {v2, p0, p2, v0, v1}, LX/1tt;-><init>(LX/0Or;LX/0Or;LX/0ka;LX/1tu;)V

    .line 336826
    move-object v0, v2

    .line 336827
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 336828
    const/4 v0, 0x3

    return v0
.end method
