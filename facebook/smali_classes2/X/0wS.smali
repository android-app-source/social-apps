.class public LX/0wS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0wd;

.field private final d:LX/0wW;

.field public final e:LX/0wc;

.field public final f:LX/0hL;

.field private g:Landroid/animation/ValueAnimator;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 160067
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/0wS;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0wW;LX/0wc;LX/0hL;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 160059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160060
    iput-object p1, p0, LX/0wS;->b:Landroid/content/res/Resources;

    .line 160061
    iput-object p2, p0, LX/0wS;->d:LX/0wW;

    .line 160062
    iput-object p3, p0, LX/0wS;->e:LX/0wc;

    .line 160063
    iget-object v0, p0, LX/0wS;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/0wS;->c:LX/0wd;

    .line 160064
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    sget-object v1, LX/0wS;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 160065
    iput-object p4, p0, LX/0wS;->f:LX/0hL;

    .line 160066
    return-void
.end method

.method public static a(LX/0wS;Landroid/view/View;F)V
    .locals 4

    .prologue
    .line 160055
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 160056
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    new-instance v1, LX/FiZ;

    invoke-direct {v1, p0, p2, p1}, LX/FiZ;-><init>(LX/0wS;FLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 160057
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    float-to-double v2, p2

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 160058
    return-void
.end method

.method public static a(LX/0wS;Landroid/view/View;LX/Fib;ILandroid/animation/Animator$AnimatorListener;)V
    .locals 7
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 160019
    invoke-static {p1}, LX/0wc;->a(Landroid/view/View;)V

    .line 160020
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 160021
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 160022
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 160023
    :cond_0
    iget-object v0, p0, LX/0wS;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b103b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    .line 160024
    sget-object v0, LX/Fib;->SHOW:LX/Fib;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, LX/0wS;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b1038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int/2addr p3, v0

    :cond_1
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 160025
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    .line 160026
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 160027
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 160028
    iget-object v6, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    new-instance v0, LX/FiX;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/FiX;-><init>(LX/0wS;Landroid/view/View;LX/Fib;FLjava/lang/Integer;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 160029
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    new-instance v1, LX/FiY;

    invoke-direct {v1, p0, p1}, LX/FiY;-><init>(LX/0wS;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 160030
    if-eqz p4, :cond_2

    .line 160031
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 160032
    :cond_2
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 160033
    return-void

    .line 160034
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V
    .locals 4
    .param p1    # Landroid/animation/Animator$AnimatorListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 160045
    const-string v0, "alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 160046
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 160047
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 160048
    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 160049
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 160050
    if-eqz p1, :cond_0

    .line 160051
    invoke-virtual {v1, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 160052
    :cond_0
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 160053
    return-void

    .line 160054
    :array_0
    .array-data 4
        0x3e99999a    # 0.3f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static b(LX/0QB;)LX/0wS;
    .locals 5

    .prologue
    .line 160043
    new-instance v4, LX/0wS;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {p0}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v2

    check-cast v2, LX/0wc;

    invoke-static {p0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v3

    check-cast v3, LX/0hL;

    invoke-direct {v4, v0, v1, v2, v3}, LX/0wS;-><init>(Landroid/content/res/Resources;LX/0wW;LX/0wc;LX/0hL;)V

    .line 160044
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 160037
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 160038
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 160039
    iget-object v0, p0, LX/0wS;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 160040
    :cond_0
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    if-eqz v0, :cond_1

    .line 160041
    iget-object v0, p0, LX/0wS;->c:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 160042
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 160035
    sget-object v0, LX/Fib;->UNCHANGED:LX/Fib;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, p2, v1}, LX/0wS;->a(LX/0wS;Landroid/view/View;LX/Fib;ILandroid/animation/Animator$AnimatorListener;)V

    .line 160036
    return-void
.end method
