.class public LX/1Gy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1HR;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/1Gz;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 226176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226177
    iput-object p2, p0, LX/1Gy;->a:LX/0ad;

    .line 226178
    new-instance v0, LX/1Gz;

    invoke-direct {v0, p1}, LX/1Gz;-><init>(Landroid/app/ActivityManager;)V

    iput-object v0, p0, LX/1Gy;->b:LX/1Gz;

    .line 226179
    return-void
.end method

.method public static a(LX/0QB;)LX/1Gy;
    .locals 3

    .prologue
    .line 226180
    new-instance v2, LX/1Gy;

    invoke-static {p0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/1Gy;-><init>(Landroid/app/ActivityManager;LX/0ad;)V

    .line 226181
    move-object v0, v2

    .line 226182
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 226183
    iget-object v0, p0, LX/1Gy;->a:LX/0ad;

    sget v1, LX/1FD;->s:F

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v1

    .line 226184
    iget-object v0, p0, LX/1Gy;->b:LX/1Gz;

    invoke-virtual {v0}, LX/1Gz;->b()LX/1HR;

    move-result-object v5

    .line 226185
    new-instance v0, LX/1HR;

    iget v2, v5, LX/1HR;->a:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, v5, LX/1HR;->b:I

    iget v3, v5, LX/1HR;->c:I

    iget v4, v5, LX/1HR;->d:I

    iget v5, v5, LX/1HR;->e:I

    invoke-direct/range {v0 .. v5}, LX/1HR;-><init>(IIIII)V

    return-object v0
.end method
