.class public final LX/0gY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0gL;


# direct methods
.method public constructor <init>(LX/0gL;)V
    .locals 0

    .prologue
    .line 112270
    iput-object p1, p0, LX/0gY;->a:LX/0gL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 112268
    sget-object v0, LX/0gL;->a:Ljava/lang/String;

    const-string v1, "badge count not available"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112269
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 112262
    check-cast p1, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;

    .line 112263
    if-nez p1, :cond_1

    .line 112264
    :cond_0
    :goto_0
    return-void

    .line 112265
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;->a()I

    move-result v0

    .line 112266
    iget-object v1, p0, LX/0gY;->a:LX/0gL;

    iget-object v1, v1, LX/0gL;->f:LX/AEw;

    if-eqz v1, :cond_0

    .line 112267
    iget-object v1, p0, LX/0gY;->a:LX/0gL;

    iget-object v1, v1, LX/0gL;->f:LX/AEw;

    invoke-virtual {v1, v0}, LX/AEw;->a(I)V

    goto :goto_0
.end method
