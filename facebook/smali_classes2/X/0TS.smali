.class public LX/0TS;
.super LX/0TT;
.source ""

# interfaces
.implements LX/0TU;


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/lang/String;

.field private final d:Ljava/util/concurrent/Executor;

.field public volatile e:I

.field public final f:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Sj;

.field public final h:LX/0TR;

.field private final i:I

.field private final j:Ljava/util/concurrent/Executor;

.field private final k:Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;

.field public final l:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final m:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63104
    const-class v0, LX/0TS;

    sput-object v0, LX/0TS;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/util/concurrent/Executor;Ljava/util/concurrent/BlockingQueue;LX/0Sj;LX/0TR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;",
            "LX/0Sj;",
            "LX/0TR;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 63089
    invoke-direct {p0}, LX/0TT;-><init>()V

    .line 63090
    if-gtz p2, :cond_0

    .line 63091
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "max concurrency must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63092
    :cond_0
    iput-object p1, p0, LX/0TS;->c:Ljava/lang/String;

    .line 63093
    iput-object p3, p0, LX/0TS;->d:Ljava/util/concurrent/Executor;

    .line 63094
    iput p2, p0, LX/0TS;->e:I

    .line 63095
    iput-object p5, p0, LX/0TS;->g:LX/0Sj;

    .line 63096
    iput-object p6, p0, LX/0TS;->h:LX/0TR;

    .line 63097
    iput-object p4, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    .line 63098
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->remainingCapacity()I

    move-result v0

    iput v0, p0, LX/0TS;->i:I

    .line 63099
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/0TS;->j:Ljava/util/concurrent/Executor;

    .line 63100
    new-instance v0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;

    invoke-direct {v0, p0}, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;-><init>(LX/0TS;)V

    iput-object v0, p0, LX/0TS;->k:Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;

    .line 63101
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 63102
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/0TS;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 63103
    return-void
.end method

.method public static a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;
    .locals 7

    .prologue
    .line 63088
    new-instance v0, LX/0TS;

    new-instance v4, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v4, p2}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    move-object v1, p0

    move v2, p1

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/0TS;-><init>(Ljava/lang/String;ILjava/util/concurrent/Executor;Ljava/util/concurrent/BlockingQueue;LX/0Sj;LX/0TR;)V

    return-object v0
.end method

.method private a(LX/0Va;)LX/0Va;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Va",
            "<TT;>;)",
            "LX/0Va",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63078
    iget v0, p0, LX/0TS;->i:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    .line 63079
    new-instance v0, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;

    invoke-direct {v0, p0, p1}, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$TaskCancelledHandler;-><init>(LX/0TS;LX/0Va;)V

    iget-object v1, p0, LX/0TS;->j:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v0, v1}, LX/0Va;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 63080
    :cond_0
    return-object p1
.end method

.method private b(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 63074
    iget-object v0, p0, LX/0TS;->g:LX/0Sj;

    invoke-interface {v0}, LX/0Sj;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63075
    :cond_0
    :goto_0
    return-object p1

    .line 63076
    :cond_1
    instance-of v0, p1, LX/0Va;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/facebook/common/executors/LoggingRunnable;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;

    if-nez v0, :cond_0

    .line 63077
    iget-object v0, p0, LX/0TS;->g:LX/0Sj;

    iget-object v1, p0, LX/0TS;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/facebook/common/executors/LoggingRunnable;->a(Ljava/lang/Runnable;LX/0Sj;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object p1

    goto :goto_0
.end method

.method private g()LX/0P1;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63060
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 63061
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 63062
    invoke-static {v0}, LX/22f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 63063
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 63064
    if-nez v0, :cond_0

    .line 63065
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 63066
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 63067
    :cond_1
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 63068
    new-instance v2, LX/4w0;

    invoke-direct {v2, v1}, LX/4w0;-><init>(Ljava/util/Map;)V

    move-object v2, v2

    .line 63069
    invoke-virtual {v0, v2}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    .line 63070
    sget-object v2, LX/1zb;->a:LX/1zb;

    move-object v2, v2

    .line 63071
    invoke-virtual {v0, v2}, LX/1sm;->b(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    .line 63072
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v1, v2}, LX/146;->b(Ljava/util/Map;Ljava/util/Comparator;)LX/146;

    move-result-object v2

    move-object v0, v2

    .line 63073
    return-object v0
.end method

.method public static h(LX/0TS;)V
    .locals 3

    .prologue
    .line 63081
    iget-object v0, p0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 63082
    :goto_0
    iget v1, p0, LX/0TS;->e:I

    if-ge v0, v1, :cond_0

    .line 63083
    add-int/lit8 v1, v0, 0x1

    .line 63084
    iget-object v2, p0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63085
    iget-object v0, p0, LX/0TS;->d:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/0TS;->k:Lcom/facebook/common/executors/DefaultConstrainedListeningExecutorService$Worker;

    const v2, 0x364d9d9c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 63086
    :cond_0
    return-void

    .line 63087
    :cond_1
    iget-object v0, p0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 63059
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 63053
    iget v0, p0, LX/0TS;->e:I

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 63054
    :goto_0
    iput p1, p0, LX/0TS;->e:I

    .line 63055
    if-eqz v0, :cond_0

    .line 63056
    invoke-static {p0}, LX/0TS;->h(LX/0TS;)V

    .line 63057
    :cond_0
    return-void

    .line 63058
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 63052
    iget-object v0, p0, LX/0TS;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "LX/0Va",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63049
    invoke-direct {p0, p1}, LX/0TS;->b(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v0

    .line 63050
    invoke-super {p0, v0, p2}, LX/0TT;->b(Ljava/lang/Runnable;Ljava/lang/Object;)LX/0Va;

    move-result-object v0

    .line 63051
    invoke-direct {p0, v0}, LX/0TS;->a(LX/0Va;)LX/0Va;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/concurrent/Callable;)LX/0Va;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "LX/0Va",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63030
    iget-object v0, p0, LX/0TS;->g:LX/0Sj;

    iget-object v1, p0, LX/0TS;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/1A8;->a(Ljava/util/concurrent/Callable;LX/0Sj;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    move-object v0, v0

    .line 63031
    invoke-super {p0, v0}, LX/0TT;->b(Ljava/util/concurrent/Callable;)LX/0Va;

    move-result-object v0

    .line 63032
    invoke-direct {p0, v0}, LX/0TS;->a(LX/0Va;)LX/0Va;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 63042
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 63043
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v3}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    .line 63044
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 63045
    instance-of v5, v0, Ljava/util/concurrent/Future;

    if-eqz v5, :cond_0

    .line 63046
    check-cast v0, Ljava/util/concurrent/Future;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 63047
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63048
    :cond_1
    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 63033
    if-nez p1, :cond_0

    .line 63034
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "runnable parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63035
    :cond_0
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {p0, p1}, LX/0TS;->b(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63036
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/0TS;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " queue is full, size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tasks="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, LX/0TS;->g()LX/0P1;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63037
    :cond_1
    iget-object v0, p0, LX/0TS;->f:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    .line 63038
    iget-object v1, p0, LX/0TS;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 63039
    if-le v0, v1, :cond_2

    iget-object v2, p0, LX/0TS;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    .line 63040
    :cond_2
    invoke-static {p0}, LX/0TS;->h(LX/0TS;)V

    .line 63041
    return-void
.end method
