.class public LX/0Wd;
.super LX/0TT;
.source ""

# interfaces
.implements LX/0TD;


# instance fields
.field private final b:LX/0Sg;

.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0Sg;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 76595
    invoke-direct {p0}, LX/0TT;-><init>()V

    .line 76596
    iput-object p1, p0, LX/0Wd;->b:LX/0Sg;

    .line 76597
    iput-object p2, p0, LX/0Wd;->c:Ljava/util/concurrent/ExecutorService;

    .line 76598
    return-void
.end method

.method private static b(Ljava/lang/Runnable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 76599
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 76600
    if-nez v0, :cond_0

    .line 76601
    const-string v0, "DefaultProcessIdleExecutor"

    .line 76602
    :goto_0
    return-object v0

    .line 76603
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DefaultProcessIdleExecutor/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 76604
    instance-of v1, p0, Lcom/facebook/common/executors/NamedRunnable;

    if-eqz v1, :cond_1

    .line 76605
    check-cast p0, Lcom/facebook/common/executors/NamedRunnable;

    invoke-virtual {p0}, Lcom/facebook/common/executors/NamedRunnable;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76606
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76607
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 76608
    iget-object v0, p0, LX/0Wd;->b:LX/0Sg;

    invoke-static {p1}, LX/0Wd;->b(Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    iget-object v3, p0, LX/0Wd;->c:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1, p1, v2, v3}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;Ljava/util/concurrent/ExecutorService;)LX/0Va;

    .line 76609
    return-void
.end method
