.class public LX/1so;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/io/ByteArrayOutputStream;

.field private final b:LX/1sr;

.field private c:LX/1su;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 335359
    new-instance v0, LX/7H1;

    invoke-direct {v0}, LX/7H1;-><init>()V

    invoke-direct {p0, v0}, LX/1so;-><init>(LX/1sq;)V

    .line 335360
    return-void
.end method

.method public constructor <init>(LX/1sq;)V
    .locals 2

    .prologue
    .line 335361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335362
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, LX/1so;->a:Ljava/io/ByteArrayOutputStream;

    .line 335363
    new-instance v0, LX/1sr;

    iget-object v1, p0, LX/1so;->a:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, LX/1sr;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, LX/1so;->b:LX/1sr;

    .line 335364
    iget-object v0, p0, LX/1so;->b:LX/1sr;

    invoke-interface {p1, v0}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    iput-object v0, p0, LX/1so;->c:LX/1su;

    .line 335365
    return-void
.end method


# virtual methods
.method public final a(LX/1u2;)[B
    .locals 1

    .prologue
    .line 335366
    iget-object v0, p0, LX/1so;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 335367
    iget-object v0, p0, LX/1so;->c:LX/1su;

    invoke-interface {p1, v0}, LX/1u2;->a(LX/1su;)V

    .line 335368
    iget-object v0, p0, LX/1so;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
