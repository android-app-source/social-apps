.class public LX/0X7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0W4;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final h:Ljava/math/BigInteger;


# instance fields
.field private b:LX/0eV;

.field private final c:LX/0eT;

.field private final d:LX/0Wx;

.field private final e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/0X0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0X0",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77185
    const-class v0, LX/0X7;

    sput-object v0, LX/0X7;->a:Ljava/lang/Class;

    .line 77186
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/0X7;->h:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(LX/0eT;LX/0Wx;LX/0X0;Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0eT;",
            "LX/0Wx;",
            "LX/0X0",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/mobileconfig/MobileConfigOverridesTable;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 77117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77118
    iput-object v0, p0, LX/0X7;->b:LX/0eV;

    .line 77119
    iput-object p1, p0, LX/0X7;->c:LX/0eT;

    .line 77120
    iput-object p2, p0, LX/0X7;->d:LX/0Wx;

    .line 77121
    iput-object p4, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 77122
    iput-object p5, p0, LX/0X7;->g:Ljava/util/Map;

    .line 77123
    iput-object p3, p0, LX/0X7;->f:LX/0X0;

    .line 77124
    iget-object v1, p0, LX/0X7;->c:LX/0eT;

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/0X7;->c:LX/0eT;

    invoke-interface {v0}, LX/0eT;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 77125
    :cond_0
    if-eqz v0, :cond_1

    .line 77126
    invoke-static {v0}, LX/0eV;->a(Ljava/nio/ByteBuffer;)LX/0eV;

    move-result-object v0

    iput-object v0, p0, LX/0X7;->b:LX/0eV;

    .line 77127
    :cond_1
    return-void
.end method

.method private static a(LX/0oD;D)D
    .locals 6

    .prologue
    .line 77128
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0oD;->a()B

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 77129
    new-instance v0, LX/19p;

    invoke-direct {v0}, LX/19p;-><init>()V

    .line 77130
    invoke-virtual {p0, v0}, LX/0oD;->a(LX/0eW;)LX/0eW;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77131
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->getDouble(I)D

    move-result-wide v3

    :goto_0
    move-wide p1, v3

    .line 77132
    :cond_0
    return-wide p1

    :cond_1
    const-wide/16 v3, 0x0

    goto :goto_0
.end method

.method public static a(LX/0X7;JIZ)I
    .locals 9

    .prologue
    .line 77133
    int-to-long v4, p3

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v6, p4

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JJZZ)J

    move-result-wide v2

    .line 77134
    long-to-int v0, v2

    .line 77135
    int-to-long v4, v0

    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    move p3, v0

    :cond_0
    return p3
.end method

.method private static a(LX/0oD;J)J
    .locals 6

    .prologue
    .line 77136
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0oD;->a()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 77137
    new-instance v0, LX/0oF;

    invoke-direct {v0}, LX/0oF;-><init>()V

    .line 77138
    invoke-virtual {p0, v0}, LX/0oD;->a(LX/0eW;)LX/0eW;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77139
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, LX/0eW;->a(I)I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v3, v5

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v3

    :goto_0
    move-wide p1, v3

    .line 77140
    :cond_0
    return-wide p1

    :cond_1
    const-wide/16 v3, 0x0

    goto :goto_0
.end method

.method private static a(LX/0oD;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 77141
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0oD;->a()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 77142
    new-instance v0, LX/0rG;

    invoke-direct {v0}, LX/0rG;-><init>()V

    .line 77143
    invoke-virtual {p0, v0}, LX/0oD;->a(LX/0eW;)LX/0eW;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77144
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_1

    iget p0, v0, LX/0eW;->a:I

    add-int/2addr v1, p0

    invoke-virtual {v0, v1}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object p1, v1

    .line 77145
    :cond_0
    return-object p1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(LX/0oD;Z)Z
    .locals 3

    .prologue
    .line 77146
    if-eqz p0, :cond_1

    invoke-virtual {p0}, LX/0oD;->a()B

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 77147
    new-instance v0, LX/0ol;

    invoke-direct {v0}, LX/0ol;-><init>()V

    .line 77148
    invoke-virtual {p0, v0}, LX/0oD;->a(LX/0eW;)LX/0eW;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 77149
    const/4 v1, 0x0

    .line 77150
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object p0, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget p1, v0, LX/0eW;->a:I

    add-int/2addr v2, p1

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move p1, v1

    .line 77151
    :cond_1
    return p1
.end method

.method public static j(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 77152
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 77153
    :cond_0
    invoke-static {p0, p1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    .line 77154
    const-wide/16 v4, 0x0

    cmp-long v3, p0, v4

    if-ltz v3, :cond_1

    :goto_1
    move-object v0, v2

    .line 77155
    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v3, LX/0X7;->h:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a(JD)D
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 77206
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v7, v6

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JDZZ)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JDZZ)D
    .locals 5

    .prologue
    .line 77156
    if-nez p6, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasDoubleOverrideForParam(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77157
    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->doubleOverrideForParam(J)D

    move-result-wide p3

    .line 77158
    :cond_0
    :goto_0
    return-wide p3

    .line 77159
    :cond_1
    iget-object v0, p0, LX/0X7;->b:LX/0eV;

    if-eqz v0, :cond_0

    .line 77160
    if-nez p5, :cond_2

    .line 77161
    const-string v0, "auto"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 77162
    :cond_2
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v0

    .line 77163
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v1

    .line 77164
    invoke-static {p1, p2}, LX/0X6;->c(J)LX/0oE;

    move-result-object v2

    .line 77165
    :try_start_0
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3}, LX/0eV;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    sget-object v3, LX/0oE;->DOUBLE:LX/0oE;

    if-ne v2, v3, :cond_0

    .line 77166
    iget-object v2, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v2, v0}, LX/0eV;->f(I)LX/0oC;

    move-result-object v0

    .line 77167
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0oC;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 77168
    invoke-virtual {v0, v1}, LX/0oC;->f(I)LX/0oD;

    move-result-object v0

    invoke-static {v0, p3, p4}, LX/0X7;->a(LX/0oD;D)D
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p3

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final a(JI)I
    .locals 1

    .prologue
    .line 77169
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/0X7;->a(LX/0X7;JIZ)I

    move-result v0

    return v0
.end method

.method public final a(JJ)J
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 77170
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v7, v6

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JJZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(JJZZ)J
    .locals 5

    .prologue
    .line 77171
    if-nez p6, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasIntOverrideForParam(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77172
    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->intOverrideForParam(J)J

    move-result-wide p3

    .line 77173
    :cond_0
    :goto_0
    return-wide p3

    .line 77174
    :cond_1
    iget-object v0, p0, LX/0X7;->b:LX/0eV;

    if-eqz v0, :cond_0

    .line 77175
    if-nez p5, :cond_2

    .line 77176
    const-string v0, "auto"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 77177
    :cond_2
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v0

    .line 77178
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v1

    .line 77179
    invoke-static {p1, p2}, LX/0X6;->c(J)LX/0oE;

    move-result-object v2

    .line 77180
    :try_start_0
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3}, LX/0eV;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    sget-object v3, LX/0oE;->LONG:LX/0oE;

    if-ne v2, v3, :cond_0

    .line 77181
    iget-object v2, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v2, v0}, LX/0eV;->f(I)LX/0oC;

    move-result-object v0

    .line 77182
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0oC;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 77183
    invoke-virtual {v0, v1}, LX/0oC;->f(I)LX/0oD;

    move-result-object v0

    invoke-static {v0, p3, p4}, LX/0X7;->a(LX/0oD;J)J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p3

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 77184
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, v5

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JLjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;ZZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 77101
    if-nez p5, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasStringOverrideForParam(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77102
    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->stringOverrideForParam(J)Ljava/lang/String;

    move-result-object p3

    .line 77103
    :cond_0
    :goto_0
    return-object p3

    .line 77104
    :cond_1
    iget-object v0, p0, LX/0X7;->b:LX/0eV;

    if-eqz v0, :cond_0

    .line 77105
    if-nez p4, :cond_2

    .line 77106
    const-string v0, "auto"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 77107
    :cond_2
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v0

    .line 77108
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v1

    .line 77109
    invoke-static {p1, p2}, LX/0X6;->c(J)LX/0oE;

    move-result-object v2

    .line 77110
    :try_start_0
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3}, LX/0eV;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    sget-object v3, LX/0oE;->STRING:LX/0oE;

    if-ne v2, v3, :cond_0

    .line 77111
    iget-object v2, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v2, v0}, LX/0eV;->f(I)LX/0oC;

    move-result-object v0

    .line 77112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0oC;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 77113
    invoke-virtual {v0, v1}, LX/0oC;->f(I)LX/0oD;

    move-result-object v0

    invoke-static {v0, p3}, LX/0X7;->a(LX/0oD;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Z)V
    .locals 5

    .prologue
    .line 77187
    iget-object v0, p0, LX/0X7;->f:LX/0X0;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    if-nez v0, :cond_1

    .line 77188
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 77189
    :cond_1
    iget-object v0, p0, LX/0X7;->b:LX/0eV;

    if-eqz v0, :cond_0

    .line 77190
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v1

    .line 77191
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v2

    .line 77192
    const/4 v0, 0x0

    .line 77193
    :try_start_0
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3}, LX/0eV;->a()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 77194
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3, v1}, LX/0eV;->f(I)LX/0oC;

    move-result-object v1

    .line 77195
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0oC;->a()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 77196
    invoke-virtual {v1, v2}, LX/0oC;->f(I)LX/0oD;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 77197
    :cond_2
    if-eqz v0, :cond_0

    .line 77198
    if-nez p4, :cond_3

    .line 77199
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_4

    iget-object v2, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    :goto_1
    move v1, v1

    .line 77200
    packed-switch v1, :pswitch_data_0

    .line 77201
    :cond_3
    invoke-virtual {v0}, LX/0oD;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0oD;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 77202
    iget-object v1, p0, LX/0X7;->d:LX/0Wx;

    invoke-virtual {v0}, LX/0oD;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, p3}, LX/0Wx;->logExposure(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77203
    :pswitch_1
    const-string v1, "auto"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 77204
    :pswitch_2
    const-string v1, "man"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 77205
    :catch_0
    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(J)Z
    .locals 1

    .prologue
    .line 77114
    invoke-static {p1, p2}, LX/0ok;->a(J)Z

    move-result v0

    .line 77115
    invoke-virtual {p0, p1, p2, v0}, LX/0X7;->a(JZ)Z

    move-result v0

    return v0
.end method

.method public final a(JZ)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 77116
    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v6, v5

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JZZZ)Z

    move-result v0

    return v0
.end method

.method public final a(JZZZ)Z
    .locals 5

    .prologue
    .line 77041
    if-nez p5, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasBoolOverrideForParam(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77042
    iget-object v0, p0, LX/0X7;->e:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->boolOverrideForParam(J)Z

    move-result p3

    .line 77043
    :cond_0
    :goto_0
    return p3

    .line 77044
    :cond_1
    iget-object v0, p0, LX/0X7;->b:LX/0eV;

    if-eqz v0, :cond_0

    .line 77045
    if-nez p4, :cond_2

    .line 77046
    const-string v0, "auto"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 77047
    :cond_2
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v0

    .line 77048
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v1

    .line 77049
    invoke-static {p1, p2}, LX/0X6;->c(J)LX/0oE;

    move-result-object v2

    .line 77050
    :try_start_0
    iget-object v3, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v3}, LX/0eV;->a()I

    move-result v3

    if-ge v0, v3, :cond_0

    sget-object v3, LX/0oE;->BOOLEAN:LX/0oE;

    if-ne v2, v3, :cond_0

    .line 77051
    iget-object v2, p0, LX/0X7;->b:LX/0eV;

    invoke-virtual {v2, v0}, LX/0eV;->f(I)LX/0oC;

    move-result-object v0

    .line 77052
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0oC;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 77053
    invoke-virtual {v0, v1}, LX/0oC;->f(I)LX/0oD;

    move-result-object v0

    invoke-static {v0, p3}, LX/0X7;->a(LX/0oD;Z)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final b(JLjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 77054
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JLjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(JD)V
    .locals 9

    .prologue
    .line 77055
    iget-object v0, p0, LX/0X7;->f:LX/0X0;

    neg-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    if-nez v0, :cond_1

    .line 77056
    :cond_0
    :goto_0
    return-void

    .line 77057
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/0X7;->g(J)D

    move-result-wide v4

    .line 77058
    sub-double v0, p3, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 77059
    :goto_1
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v6

    .line 77060
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v7

    .line 77061
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 77062
    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    invoke-static {p1, p2}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/0X7;->g:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77063
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final b(JJ)V
    .locals 9

    .prologue
    .line 77064
    iget-object v0, p0, LX/0X7;->f:LX/0X0;

    neg-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    if-nez v0, :cond_1

    .line 77065
    :cond_0
    :goto_0
    return-void

    .line 77066
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/0X7;->c(J)J

    move-result-wide v4

    .line 77067
    cmp-long v0, v4, p3

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 77068
    :goto_1
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v6

    .line 77069
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v7

    .line 77070
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 77071
    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    invoke-static {p1, p2}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/0X7;->g:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77072
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final b(J)Z
    .locals 7

    .prologue
    .line 77073
    invoke-static {p1, p2}, LX/0ok;->a(J)Z

    move-result v4

    .line 77074
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JZZZ)Z

    move-result v0

    return v0
.end method

.method public final b(JZ)Z
    .locals 7

    .prologue
    .line 77075
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JZZZ)Z

    move-result v0

    return v0
.end method

.method public final c(JD)D
    .locals 9

    .prologue
    .line 77076
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JDZZ)D

    move-result-wide v0

    return-wide v0
.end method

.method public final c(J)J
    .locals 3

    .prologue
    .line 77077
    invoke-static {p1, p2}, LX/0ok;->b(J)J

    move-result-wide v0

    .line 77078
    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JJ)J
    .locals 9

    .prologue
    .line 77079
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JJZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(JZ)V
    .locals 7

    .prologue
    .line 77080
    iget-object v0, p0, LX/0X7;->f:LX/0X0;

    neg-long v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0X0;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    if-nez v0, :cond_1

    .line 77081
    :cond_0
    :goto_0
    return-void

    .line 77082
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/0X7;->a(J)Z

    move-result v3

    .line 77083
    if-ne v3, p3, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 77084
    :goto_1
    invoke-static {p1, p2}, LX/0X6;->a(J)I

    move-result v4

    .line 77085
    invoke-static {p1, p2}, LX/0X6;->b(J)I

    move-result v5

    .line 77086
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 77087
    iget-object v0, p0, LX/0X7;->d:LX/0Wx;

    invoke-static {p1, p2}, LX/0X7;->j(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/0X7;->g:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/0Wx;->logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77088
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1
.end method

.method public final d(J)J
    .locals 9

    .prologue
    .line 77089
    invoke-static {p1, p2}, LX/0ok;->b(J)J

    move-result-wide v4

    .line 77090
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JJZZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 77091
    invoke-static {p1, p2}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v0

    .line 77092
    invoke-virtual {p0, p1, p2, v0}, LX/0X7;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 77093
    invoke-static {p1, p2}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v4

    .line 77094
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JLjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(J)D
    .locals 3

    .prologue
    .line 77095
    invoke-static {p1, p2}, LX/0ok;->c(J)D

    move-result-wide v0

    .line 77096
    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final h(J)D
    .locals 9

    .prologue
    .line 77097
    invoke-static {p1, p2}, LX/0ok;->c(J)D

    move-result-wide v4

    .line 77098
    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JDZZ)D

    move-result-wide v0

    return-wide v0
.end method

.method public final i(J)V
    .locals 3

    .prologue
    .line 77099
    const-string v0, "man"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/0X7;->a(JLjava/lang/String;Z)V

    .line 77100
    return-void
.end method
