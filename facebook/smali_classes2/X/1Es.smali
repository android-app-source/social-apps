.class public LX/1Es;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221373
    const-class v0, LX/1Es;

    sput-object v0, LX/1Es;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 221369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221370
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221371
    iput-object p1, p0, LX/1Es;->b:Ljava/io/File;

    .line 221372
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 221374
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221375
    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 221376
    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->canWrite()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 221377
    :goto_0
    move v1, v1

    .line 221378
    if-nez v1, :cond_0

    .line 221379
    :goto_1
    return-object v0

    .line 221380
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 221381
    :cond_1
    const-string p1, ""

    .line 221382
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    .line 221383
    :cond_3
    const-string p2, ".tmp"

    .line 221384
    :cond_4
    const-string v2, ""

    .line 221385
    :cond_5
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 221386
    sget-object v3, LX/0SF;->a:LX/0SF;

    move-object v3, v3

    .line 221387
    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 221388
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 221389
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_5

    move-object v0, v1

    .line 221390
    goto :goto_1

    .line 221391
    :catch_0
    move-exception v1

    .line 221392
    sget-object v3, LX/1Es;->a:Ljava/lang/Class;

    const-string v4, "Error occurred when creating the temporary file %s in directory %s."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    iget-object v6, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3, v1, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    move v1, v2

    .line 221393
    goto :goto_0

    .line 221394
    :cond_7
    iget-object v3, p0, LX/1Es;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v3

    .line 221395
    if-nez v3, :cond_8

    .line 221396
    sget-object v1, LX/1Es;->a:Ljava/lang/Class;

    const-string v3, "Unable to create a directory"

    invoke-static {v1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v1, v2

    .line 221397
    goto :goto_0

    .line 221398
    :cond_8
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, LX/1Es;->b:Ljava/io/File;

    const-string v5, ".nomedia"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 221399
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 221400
    :catch_1
    move-exception v1

    .line 221401
    sget-object v3, LX/1Es;->a:Ljava/lang/Class;

    const-string v4, "error in temp file manager"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v2

    .line 221402
    goto/16 :goto_0
.end method

.method public final a(J)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 221359
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 221360
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 221361
    iget-object v1, p0, LX/1Es;->b:Ljava/io/File;

    new-instance v4, LX/46g;

    invoke-direct {v4}, LX/46g;-><init>()V

    invoke-virtual {v1, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v4

    .line 221362
    if-nez v4, :cond_1

    .line 221363
    :cond_0
    return v0

    .line 221364
    :cond_1
    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 221365
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    sub-long v8, v2, v8

    cmp-long v7, v8, p1

    if-ltz v7, :cond_2

    .line 221366
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_3

    .line 221367
    :cond_2
    const/4 v0, 0x1

    .line 221368
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
