.class public LX/0wX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0wY;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Z

.field private static volatile d:LX/0wX;


# instance fields
.field private b:Landroid/os/Handler;

.field public c:Landroid/view/Choreographer;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 160209
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/0wX;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 160191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160192
    sget-boolean v0, LX/0wX;->a:Z

    if-eqz v0, :cond_0

    .line 160193
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    move-object v0, v0

    .line 160194
    iput-object v0, p0, LX/0wX;->c:Landroid/view/Choreographer;

    .line 160195
    :goto_0
    return-void

    .line 160196
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/0wX;->b:Landroid/os/Handler;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0wX;
    .locals 3

    .prologue
    .line 160197
    sget-object v0, LX/0wX;->d:LX/0wX;

    if-nez v0, :cond_1

    .line 160198
    const-class v1, LX/0wX;

    monitor-enter v1

    .line 160199
    :try_start_0
    sget-object v0, LX/0wX;->d:LX/0wX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 160200
    if-eqz v2, :cond_0

    .line 160201
    :try_start_1
    new-instance v0, LX/0wX;

    invoke-direct {v0}, LX/0wX;-><init>()V

    .line 160202
    move-object v0, v0

    .line 160203
    sput-object v0, LX/0wX;->d:LX/0wX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 160205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 160206
    :cond_1
    sget-object v0, LX/0wX;->d:LX/0wX;

    return-object v0

    .line 160207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 160208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0wa;)V
    .locals 5

    .prologue
    .line 160186
    sget-boolean v0, LX/0wX;->a:Z

    if-eqz v0, :cond_0

    .line 160187
    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v0

    .line 160188
    iget-object v1, p0, LX/0wX;->c:Landroid/view/Choreographer;

    invoke-virtual {v1, v0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 160189
    :goto_0
    return-void

    .line 160190
    :cond_0
    iget-object v0, p0, LX/0wX;->b:Landroid/os/Handler;

    invoke-virtual {p1}, LX/0wa;->b()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    const v4, -0x6bfecaac

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final a(LX/0wa;J)V
    .locals 6

    .prologue
    .line 160181
    sget-boolean v0, LX/0wX;->a:Z

    if-eqz v0, :cond_0

    .line 160182
    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v0

    .line 160183
    iget-object v1, p0, LX/0wX;->c:Landroid/view/Choreographer;

    invoke-virtual {v1, v0, p2, p3}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    .line 160184
    :goto_0
    return-void

    .line 160185
    :cond_0
    iget-object v0, p0, LX/0wX;->b:Landroid/os/Handler;

    invoke-virtual {p1}, LX/0wa;->b()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x11

    add-long/2addr v2, p2

    const v4, -0x7763df9e

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final b(LX/0wa;)V
    .locals 2

    .prologue
    .line 160176
    sget-boolean v0, LX/0wX;->a:Z

    if-eqz v0, :cond_0

    .line 160177
    invoke-virtual {p1}, LX/0wa;->a()Landroid/view/Choreographer$FrameCallback;

    move-result-object v0

    .line 160178
    iget-object v1, p0, LX/0wX;->c:Landroid/view/Choreographer;

    invoke-virtual {v1, v0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 160179
    :goto_0
    return-void

    .line 160180
    :cond_0
    iget-object v0, p0, LX/0wX;->b:Landroid/os/Handler;

    invoke-virtual {p1}, LX/0wa;->b()Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
