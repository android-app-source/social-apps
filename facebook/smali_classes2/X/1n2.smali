.class public LX/1n2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1V8;

.field private final b:LX/1dp;

.field private final c:LX/1dr;


# direct methods
.method public constructor <init>(LX/1V8;LX/1dp;LX/1dr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 314377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314378
    iput-object p1, p0, LX/1n2;->a:LX/1V8;

    .line 314379
    iput-object p2, p0, LX/1n2;->b:LX/1dp;

    .line 314380
    iput-object p3, p0, LX/1n2;->c:LX/1dr;

    .line 314381
    return-void
.end method

.method public static a(LX/0QB;)LX/1n2;
    .locals 6

    .prologue
    .line 314357
    const-class v1, LX/1n2;

    monitor-enter v1

    .line 314358
    :try_start_0
    sget-object v0, LX/1n2;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 314359
    sput-object v2, LX/1n2;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 314360
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314361
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 314362
    new-instance p0, LX/1n2;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v3

    check-cast v3, LX/1V8;

    invoke-static {v0}, LX/1do;->a(LX/0QB;)LX/1do;

    move-result-object v4

    check-cast v4, LX/1dp;

    invoke-static {v0}, LX/1dr;->a(LX/0QB;)LX/1dr;

    move-result-object v5

    check-cast v5, LX/1dr;

    invoke-direct {p0, v3, v4, v5}, LX/1n2;-><init>(LX/1V8;LX/1dp;LX/1dr;)V

    .line 314363
    move-object v0, p0

    .line 314364
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 314365
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1n2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314366
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 314367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;LX/1X9;ILX/1X6;Z)LX/1Dg;
    .locals 17
    .param p2    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1X9;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1X6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;",
            "LX/1X9;",
            "I",
            "LX/1X6;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 314368
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 314369
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1n2;->a:LX/1V8;

    move-object/from16 v0, p5

    iget-object v4, v0, LX/1X6;->b:LX/1Ua;

    move-object/from16 v1, p3

    move/from16 v2, p4

    move-object/from16 v5, p1

    invoke-static/range {v1 .. v6}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 314370
    move-object/from16 v0, p5

    iget v9, v0, LX/1X6;->c:I

    move-object/from16 v0, p5

    iget v10, v0, LX/1X6;->d:I

    move-object/from16 v0, p0

    iget-object v11, v0, LX/1n2;->b:LX/1dp;

    move-object/from16 v0, p5

    iget-object v14, v0, LX/1X6;->b:LX/1Ua;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/1n2;->a:LX/1V8;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/1n2;->c:LX/1dr;

    move-object/from16 v0, p5

    iget-object v2, v0, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v16

    move-object/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v12, p1

    move-object v13, v6

    invoke-static/range {v7 .. v16}, LX/1X7;->a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 314371
    move-object/from16 v0, p5

    iget-object v2, v0, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    move-object/from16 v0, p5

    iget-object v2, v0, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 314372
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1n2;->c:LX/1dr;

    move-object/from16 v0, p5

    iget-object v3, v0, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3, v1}, LX/1dr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/graphics/drawable/Drawable;)V

    .line 314373
    :cond_0
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 314374
    if-eqz p2, :cond_1

    if-eqz p6, :cond_1

    invoke-static/range {p1 .. p2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    .line 314375
    :goto_0
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dc;)LX/1Di;

    move-result-object v1

    const/4 v2, 0x4

    iget v3, v6, Landroid/graphics/Rect;->left:I

    invoke-interface {v1, v2, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, v6, Landroid/graphics/Rect;->top:I

    invoke-interface {v1, v2, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    iget v3, v6, Landroid/graphics/Rect;->right:I

    invoke-interface {v1, v2, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    iget v3, v6, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v1, v2, v3}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 314376
    :cond_1
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    goto :goto_0
.end method
