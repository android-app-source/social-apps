.class public LX/1DO;
.super LX/1Cd;
.source ""

# interfaces
.implements LX/1Ce;
.implements LX/0hk;
.implements LX/0fm;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# static fields
.field private static final b:LX/1DQ;


# instance fields
.field public final a:LX/1Cq;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "LX/1EE;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1DS;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Dy;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1Dz;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1QZ;",
            ">;"
        }
    .end annotation
.end field

.field public final i:I

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BYI;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1E0;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1k4;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0ad;

.field private final n:LX/1E1;

.field private final o:LX/1E2;

.field public final p:Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;

.field private final q:LX/1EG;

.field private final r:LX/1EI;

.field private final s:LX/0oJ;

.field private final t:LX/1EE;

.field public u:Landroid/content/Context;

.field public v:Lcom/facebook/api/feedtype/FeedType;

.field private w:Z

.field public x:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217598
    new-instance v0, LX/1DP;

    invoke-direct {v0}, LX/1DP;-><init>()V

    sput-object v0, LX/1DO;->b:LX/1DQ;

    return-void
.end method

.method public constructor <init>(LX/1DR;LX/1DS;LX/0Ot;LX/1Dy;LX/0Or;LX/1Dz;LX/0Or;LX/0Ot;LX/1E0;LX/0Ot;LX/0ad;LX/1E1;LX/1E2;LX/0oJ;LX/1EE;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DR;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerRootGroupPartDefinition;",
            ">;",
            "LX/1Dy;",
            "LX/0Or",
            "<",
            "LX/1RH;",
            ">;",
            "LX/1Dz;",
            "LX/0Or",
            "<",
            "LX/1QZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BYI;",
            ">;",
            "LX/1E0;",
            "LX/0Ot",
            "<",
            "LX/1k4;",
            ">;",
            "LX/0ad;",
            "LX/1E1;",
            "LX/1E2;",
            "LX/0oJ;",
            "LX/1EE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 217409
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 217410
    new-instance v1, LX/1Cq;

    invoke-direct {v1}, LX/1Cq;-><init>()V

    iput-object v1, p0, LX/1DO;->a:LX/1Cq;

    .line 217411
    invoke-virtual {p1}, LX/1DR;->a()I

    move-result v1

    iput v1, p0, LX/1DO;->i:I

    .line 217412
    iput-object p2, p0, LX/1DO;->c:LX/1DS;

    .line 217413
    iput-object p3, p0, LX/1DO;->d:LX/0Ot;

    .line 217414
    iput-object p4, p0, LX/1DO;->e:LX/1Dy;

    .line 217415
    iput-object p5, p0, LX/1DO;->f:LX/0Or;

    .line 217416
    iput-object p6, p0, LX/1DO;->g:LX/1Dz;

    .line 217417
    iput-object p7, p0, LX/1DO;->h:LX/0Or;

    .line 217418
    iput-object p8, p0, LX/1DO;->j:LX/0Ot;

    .line 217419
    iput-object p9, p0, LX/1DO;->k:LX/1E0;

    .line 217420
    iput-object p10, p0, LX/1DO;->l:LX/0Ot;

    .line 217421
    iput-object p11, p0, LX/1DO;->m:LX/0ad;

    .line 217422
    iput-object p12, p0, LX/1DO;->n:LX/1E1;

    .line 217423
    iput-object p13, p0, LX/1DO;->o:LX/1E2;

    .line 217424
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/1DO;->w:Z

    .line 217425
    new-instance v1, Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;

    invoke-direct {v1}, Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;-><init>()V

    iput-object v1, p0, LX/1DO;->p:Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;

    .line 217426
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1DO;->s:LX/0oJ;

    .line 217427
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1DO;->t:LX/1EE;

    .line 217428
    new-instance v1, LX/1EG;

    invoke-direct {v1, p0}, LX/1EG;-><init>(LX/1DO;)V

    iput-object v1, p0, LX/1DO;->q:LX/1EG;

    .line 217429
    new-instance v1, LX/1EH;

    invoke-direct {v1, p0}, LX/1EH;-><init>(LX/1DO;)V

    iput-object v1, p0, LX/1DO;->r:LX/1EI;

    .line 217430
    return-void
.end method

.method private a(LX/1EE;)Z
    .locals 2

    .prologue
    .line 217597
    iget-object v0, p0, LX/1DO;->n:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->c:LX/32e;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    iget-object v0, v0, LX/1RN;->c:LX/32e;

    iget-object v0, v0, LX/32e;->a:LX/24P;

    sget-object v1, LX/24P;->DISMISSED:LX/24P;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1DO;
    .locals 17

    .prologue
    .line 217595
    new-instance v1, LX/1DO;

    invoke-static/range {p0 .. p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v2

    check-cast v2, LX/1DR;

    invoke-static/range {p0 .. p0}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v3

    check-cast v3, LX/1DS;

    const/16 v4, 0x657

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/1Dy;->a(LX/0QB;)LX/1Dy;

    move-result-object v5

    check-cast v5, LX/1Dy;

    const/16 v6, 0xfd1

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/1Dz;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1Dz;

    const/16 v8, 0xfd0

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x38d6

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/1E0;->a(LX/0QB;)LX/1E0;

    move-result-object v10

    check-cast v10, LX/1E0;

    const/16 v11, 0xfe0

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/1E1;->a(LX/0QB;)LX/1E1;

    move-result-object v13

    check-cast v13, LX/1E1;

    invoke-static/range {p0 .. p0}, LX/1E2;->a(LX/0QB;)LX/1E2;

    move-result-object v14

    check-cast v14, LX/1E2;

    invoke-static/range {p0 .. p0}, LX/0oJ;->a(LX/0QB;)LX/0oJ;

    move-result-object v15

    check-cast v15, LX/0oJ;

    invoke-static/range {p0 .. p0}, LX/1E4;->a(LX/0QB;)LX/1EE;

    move-result-object v16

    check-cast v16, LX/1EE;

    invoke-direct/range {v1 .. v16}, LX/1DO;-><init>(LX/1DR;LX/1DS;LX/0Ot;LX/1Dy;LX/0Or;LX/1Dz;LX/0Or;LX/0Ot;LX/1E0;LX/0Ot;LX/0ad;LX/1E1;LX/1E2;LX/0oJ;LX/1EE;)V

    .line 217596
    return-object v1
.end method

.method private static e(LX/1DO;)LX/1EE;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 217575
    new-instance v0, LX/1EA;

    iget-object v1, p0, LX/1DO;->t:LX/1EE;

    invoke-direct {v0, v1}, LX/1EA;-><init>(LX/1EE;)V

    .line 217576
    iget-object v1, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 217577
    iget-object v2, v1, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v1, v2

    .line 217578
    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v1, v2}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217579
    iget-object v1, p0, LX/1DO;->u:Landroid/content/Context;

    const v2, 0x7f0826fa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 217580
    iput-object v1, v0, LX/1EA;->o:Ljava/lang/String;

    .line 217581
    move-object v1, v0

    .line 217582
    new-instance v2, LX/B0G;

    invoke-direct {v2}, LX/B0G;-><init>()V

    move-object v2, v2

    .line 217583
    const/4 v3, 0x1

    .line 217584
    iput-boolean v3, v2, LX/B0G;->a:Z

    .line 217585
    move-object v2, v2

    .line 217586
    iget-object v3, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    sget-object v4, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3, v4}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 217587
    iput-boolean v3, v2, LX/B0G;->b:Z

    .line 217588
    move-object v2, v2

    .line 217589
    new-instance v3, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    invoke-direct {v3, v2}, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;-><init>(LX/B0G;)V

    move-object v2, v3

    .line 217590
    iput-object v2, v1, LX/1EA;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 217591
    move-object v1, v1

    .line 217592
    iget-object v2, p0, LX/1DO;->s:LX/0oJ;

    invoke-virtual {v2}, LX/0oJ;->j()Z

    move-result v2

    .line 217593
    iput-boolean v2, v1, LX/1EA;->p:Z

    .line 217594
    :cond_0
    invoke-virtual {v0}, LX/1EA;->a()LX/1EE;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/1DO;)V
    .locals 2

    .prologue
    .line 217571
    iget-object v0, p0, LX/1DO;->k:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217572
    iget-object v0, p0, LX/1DO;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BYI;

    .line 217573
    sget-object v1, LX/0zS;->a:LX/0zS;

    iget-object p0, v0, LX/BYI;->c:LX/Ajt;

    invoke-static {v0, v1, p0}, LX/BYI;->a(LX/BYI;LX/0zS;LX/Ajt;)V

    .line 217574
    :cond_0
    return-void
.end method

.method private static j(LX/1DO;)V
    .locals 2

    .prologue
    .line 217568
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    iget-object v1, p0, LX/1DO;->q:LX/1EG;

    .line 217569
    iput-object v1, v0, LX/1QZ;->A:LX/1EG;

    .line 217570
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217554
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 217555
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    .line 217556
    iget-object v1, v0, LX/1RN;->a:LX/1kK;

    instance-of v1, v1, LX/1kW;

    if-nez v1, :cond_1

    .line 217557
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 217558
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 217559
    :cond_1
    iget-object v1, v0, LX/1RN;->a:LX/1kK;

    check-cast v1, LX/1kW;

    .line 217560
    iget-object v5, v1, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, v5

    .line 217561
    invoke-virtual {v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->B()Ljava/lang/String;

    move-result-object v1

    .line 217562
    if-eqz v1, :cond_0

    iget-object v5, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 217563
    iget-object v6, v5, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v5, v6

    .line 217564
    iget-object v6, v5, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v5, v6

    .line 217565
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217566
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 217567
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0gf;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 217543
    iget-object v0, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 217544
    iget-object v3, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v3

    .line 217545
    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217546
    iget-object v0, p0, LX/1DO;->s:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->j()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 217547
    :goto_0
    iget-object v3, p0, LX/1DO;->h:LX/0Or;

    if-eqz v3, :cond_1

    sget-object v3, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-ne p1, v3, :cond_1

    .line 217548
    iget-object v3, p0, LX/1DO;->x:Ljava/lang/Boolean;

    if-nez v3, :cond_0

    .line 217549
    iget-object v3, p0, LX/1DO;->m:LX/0ad;

    sget-short v4, LX/1Nu;->g:S

    const/4 p1, 0x0

    invoke-interface {v3, v4, p1}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, LX/1DO;->x:Ljava/lang/Boolean;

    .line 217550
    :cond_0
    iget-object v3, p0, LX/1DO;->x:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v3, v3

    .line 217551
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 217552
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    const-string v3, "on_ptr"

    invoke-virtual {v0, v2, v3, v1}, LX/1QZ;->a(ZLjava/lang/String;Z)V

    .line 217553
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 217542
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 217531
    instance-of v0, p1, LX/1Rk;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/1Rk;

    .line 217532
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    move-object v0, v1

    .line 217533
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 217534
    instance-of v0, v0, LX/1EE;

    if-eqz v0, :cond_0

    .line 217535
    check-cast p1, LX/1Rk;

    .line 217536
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 217537
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 217538
    check-cast v0, LX/1EE;

    .line 217539
    iget-object v1, p0, LX/1DO;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RH;

    iget-boolean v2, p0, LX/1DO;->w:Z

    invoke-virtual {v0}, LX/1EE;->a()LX/1RN;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/1RI;->a(ZLX/1RN;)V

    .line 217540
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1DO;->w:Z

    .line 217541
    :cond_0
    return-void
.end method

.method public final b()LX/1Cw;
    .locals 13

    .prologue
    .line 217503
    invoke-static {p0}, LX/1DO;->j(LX/1DO;)V

    .line 217504
    iget-object v0, p0, LX/1DO;->k:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217505
    iget-object v0, p0, LX/1DO;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BYI;

    new-instance v1, LX/Ajt;

    invoke-direct {v1, p0}, LX/Ajt;-><init>(LX/1DO;)V

    .line 217506
    iput-object v1, v0, LX/BYI;->c:LX/Ajt;

    .line 217507
    invoke-static {p0}, LX/1DO;->i(LX/1DO;)V

    .line 217508
    :cond_0
    iget-object v0, p0, LX/1DO;->p:Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;

    new-instance v1, LX/1Qg;

    invoke-direct {v1}, LX/1Qg;-><init>()V

    sget-object v2, LX/1DO;->b:LX/1DQ;

    iget-object v3, p0, LX/1DO;->u:Landroid/content/Context;

    sget-object v4, LX/1PU;->b:LX/1PY;

    iget-object v5, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Qa;

    .line 217509
    new-instance v6, LX/1Qi;

    move-object v7, v0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, LX/1Qi;-><init>(Ljava/lang/Runnable;LX/1Qh;LX/1DQ;Landroid/content/Context;LX/1PY;LX/1Qa;)V

    .line 217510
    move-object v0, v6

    .line 217511
    iget-object v1, p0, LX/1DO;->c:LX/1DS;

    iget-object v2, p0, LX/1DO;->d:LX/0Ot;

    iget-object v3, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 217512
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 217513
    move-object v0, v1

    .line 217514
    iget-object v1, p0, LX/1DO;->e:LX/1Dy;

    .line 217515
    iput-object v1, v0, LX/1Ql;->e:LX/1DZ;

    .line 217516
    move-object v0, v0

    .line 217517
    iget-object v1, p0, LX/1DO;->a:LX/1Cq;

    invoke-static {p0}, LX/1DO;->e(LX/1DO;)LX/1EE;

    move-result-object v2

    .line 217518
    iput-object v2, v1, LX/1Cq;->a:Ljava/lang/Object;

    .line 217519
    new-instance v1, LX/1Qo;

    invoke-virtual {v0}, LX/1Ql;->d()LX/1Rq;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1Qo;-><init>(LX/1Rq;)V

    .line 217520
    iget-object v0, p0, LX/1DO;->p:Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;

    .line 217521
    iput-object v1, v0, Lcom/facebook/feed/environment/HasInvalidate$InvalidateRunnable;->a:LX/1Cw;

    .line 217522
    iget-object v0, p0, LX/1DO;->o:LX/1E2;

    iget-object v2, p0, LX/1DO;->r:LX/1EI;

    invoke-virtual {v0, v2}, LX/1E2;->a(LX/1EI;)V

    .line 217523
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    if-eqz v0, :cond_1

    .line 217524
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    .line 217525
    iget-object v2, v0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Rx;

    invoke-virtual {v2}, LX/1Rx;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 217526
    :cond_1
    :goto_0
    return-object v1

    .line 217527
    :cond_2
    iget-object v2, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1k4;

    iget-object p0, v0, LX/1QZ;->w:LX/1Qb;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 217528
    iget-object v2, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1k4;

    iget-object p0, v0, LX/1QZ;->x:LX/1Qd;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 217529
    iget-object v2, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1k4;

    iget-object p0, v0, LX/1QZ;->y:LX/1Qe;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 217530
    iget-object v2, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1k4;

    iget-object p0, v0, LX/1QZ;->z:LX/1Qf;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public final b(LX/0g8;)V
    .locals 1

    .prologue
    .line 217501
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1DO;->w:Z

    .line 217502
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 217473
    iget-object v0, p0, LX/1DO;->n:LX/1E1;

    .line 217474
    iget-object v1, v0, LX/1E1;->a:LX/0ad;

    sget-short v3, LX/1Nu;->e:S

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 217475
    if-eqz v0, :cond_1

    .line 217476
    :cond_0
    :goto_0
    return-void

    .line 217477
    :cond_1
    iget-object v0, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 217478
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 217479
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217480
    iget-object v0, p0, LX/1DO;->s:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217481
    :cond_2
    instance-of v0, p1, LX/1Rk;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/1Rk;

    .line 217482
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    move-object v0, v1

    .line 217483
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 217484
    instance-of v0, v0, LX/1EE;

    if-eqz v0, :cond_0

    .line 217485
    check-cast p1, LX/1Rk;

    .line 217486
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 217487
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 217488
    check-cast v0, LX/1EE;

    .line 217489
    iget-object v1, p0, LX/1DO;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1RH;

    invoke-virtual {v0}, LX/1EE;->a()LX/1RN;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1RI;->a(LX/1RN;)V

    .line 217490
    invoke-static {p0}, LX/1DO;->j(LX/1DO;)V

    .line 217491
    iget-boolean v1, v0, LX/1EE;->b:Z

    move v1, v1

    .line 217492
    if-eqz v1, :cond_4

    .line 217493
    invoke-direct {p0, v0}, LX/1DO;->a(LX/1EE;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217494
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    invoke-virtual {v0}, LX/1QZ;->e()V

    .line 217495
    :cond_3
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    iget-object v1, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v1, v2}, LX/1Cq;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EE;

    invoke-direct {p0, v1}, LX/1DO;->a(LX/1EE;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    const-string v3, "on_exit_viewport"

    invoke-virtual {v0, v1, v3, v2}, LX/1QZ;->a(ZLjava/lang/String;Z)V

    .line 217496
    :cond_4
    iget-object v0, p0, LX/1DO;->k:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217497
    iget-object v0, p0, LX/1DO;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BYI;

    .line 217498
    sget-object v1, LX/0zS;->d:LX/0zS;

    iget-object v2, v0, LX/BYI;->c:LX/Ajt;

    invoke-static {v0, v1, v2}, LX/BYI;->a(LX/BYI;LX/0zS;LX/Ajt;)V

    .line 217499
    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 217500
    goto :goto_1
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 217455
    invoke-static {p0}, LX/1DO;->j(LX/1DO;)V

    .line 217456
    iget-object v0, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v0}, LX/1Cq;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 217457
    :cond_0
    :goto_0
    return-void

    .line 217458
    :cond_1
    iget-object v0, p0, LX/1DO;->v:Lcom/facebook/api/feedtype/FeedType;

    .line 217459
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v1

    .line 217460
    sget-object v1, Lcom/facebook/api/feedtype/FeedType$Name;->c:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217461
    iget-object v0, p0, LX/1DO;->s:LX/0oJ;

    invoke-virtual {v0}, LX/0oJ;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217462
    :cond_2
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    if-eqz v0, :cond_4

    .line 217463
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    .line 217464
    iget-object v1, v0, LX/1QZ;->s:LX/0qb;

    invoke-virtual {v1}, LX/0qb;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1QZ;->a:LX/0Px;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/1QZ;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 217465
    :cond_3
    :goto_1
    iget-object v0, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v0, v2}, LX/1Cq;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EE;

    .line 217466
    iget-boolean v1, v0, LX/1EE;->b:Z

    move v0, v1

    .line 217467
    if-eqz v0, :cond_4

    .line 217468
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    const-string v1, "on_resume"

    invoke-virtual {v0, v2, v1, v2}, LX/1QZ;->a(ZLjava/lang/String;Z)V

    .line 217469
    :cond_4
    invoke-static {p0}, LX/1DO;->i(LX/1DO;)V

    goto :goto_0

    .line 217470
    :cond_5
    invoke-virtual {v0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    if-eqz v1, :cond_6

    invoke-virtual {v0}, LX/1QZ;->a()LX/1RN;

    move-result-object v1

    iget-object v1, v1, LX/1RN;->a:LX/1kK;

    invoke-static {v1}, LX/1QZ;->b(LX/1kK;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, LX/1QZ;->s:LX/0qb;

    invoke-virtual {v1}, LX/0qb;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 217471
    :cond_6
    iget-object v3, v0, LX/1QZ;->s:LX/0qb;

    iget-object v1, v0, LX/1QZ;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qg;

    sget-object v4, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v3, v1, v4}, LX/0qb;->b(LX/0qg;LX/0rH;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 217472
    invoke-virtual {v0}, LX/1QZ;->e()V

    goto :goto_1
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 217447
    iget-object v0, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v0}, LX/1Cq;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 217448
    :cond_0
    :goto_0
    return-void

    .line 217449
    :cond_1
    iget-object v0, p0, LX/1DO;->a:LX/1Cq;

    invoke-virtual {v0, v1}, LX/1Cq;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EE;

    invoke-virtual {v0}, LX/1EE;->a()LX/1RN;

    move-result-object v2

    .line 217450
    if-eqz v2, :cond_0

    .line 217451
    const-class v0, LX/1kJ;

    const-class v3, LX/Ayb;

    invoke-static {v0, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 217452
    iget-object v5, v2, LX/1RN;->a:LX/1kK;

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 217453
    iget-object v1, p0, LX/1DO;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1k4;

    new-instance v2, LX/1k9;

    invoke-direct {v2, v0}, LX/1k9;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 217454
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 217431
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    if-eqz v0, :cond_0

    .line 217432
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    .line 217433
    iget-object v1, v0, LX/1QZ;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Rx;

    invoke-virtual {v1}, LX/1Rx;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 217434
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1DO;->o:LX/1E2;

    iget-object v1, p0, LX/1DO;->r:LX/1EI;

    invoke-virtual {v0, v1}, LX/1E2;->b(LX/1EI;)V

    .line 217435
    iget-object v0, p0, LX/1DO;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1QZ;

    .line 217436
    iget-object v1, v0, LX/1QZ;->h:LX/1Ck;

    const-string p0, "FETCH_PROMPT_FUTURE"

    invoke-virtual {v1, p0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217437
    iget-object v1, v0, LX/1QZ;->h:LX/1Ck;

    const-string p0, "FETCH_PROMPT_FUTURE"

    invoke-virtual {v1, p0}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 217438
    :cond_1
    const/4 v1, 0x0

    .line 217439
    iput-object v1, v0, LX/1QZ;->A:LX/1EG;

    .line 217440
    invoke-virtual {v0}, LX/1QZ;->e()V

    .line 217441
    return-void

    .line 217442
    :cond_2
    iget-object v1, v0, LX/1QZ;->p:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x930001

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 217443
    iget-object v1, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1k4;

    iget-object v2, v0, LX/1QZ;->w:LX/1Qb;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 217444
    iget-object v1, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1k4;

    iget-object v2, v0, LX/1QZ;->x:LX/1Qd;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 217445
    iget-object v1, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1k4;

    iget-object v2, v0, LX/1QZ;->y:LX/1Qe;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 217446
    iget-object v1, v0, LX/1QZ;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1k4;

    iget-object v2, v0, LX/1QZ;->z:LX/1Qf;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method
