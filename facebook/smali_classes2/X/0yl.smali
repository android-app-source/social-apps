.class public LX/0yl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yo;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z[Ljava/lang/String;Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;)V
    .locals 3
    .param p2    # [Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 166124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166125
    iput-boolean p1, p0, LX/0yl;->a:Z

    .line 166126
    if-nez p2, :cond_1

    .line 166127
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 166128
    :cond_0
    move-object v0, v0

    .line 166129
    iput-object v0, p0, LX/0yl;->b:Ljava/util/Set;

    .line 166130
    iput-object p3, p0, LX/0yl;->c:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    .line 166131
    return-void

    .line 166132
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 166133
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object p1, p2, v1

    .line 166134
    invoke-static {p1}, LX/0yo;->getEnum(Ljava/lang/String;)LX/0yo;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final varargs a([LX/0yo;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 166136
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p1, v1

    .line 166137
    iget-boolean v4, p0, LX/0yl;->a:Z

    if-nez v4, :cond_0

    iget-object v4, p0, LX/0yl;->b:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 166138
    if-eqz v3, :cond_2

    .line 166139
    const/4 v0, 0x1

    .line 166140
    :cond_1
    return v0

    .line 166141
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method
