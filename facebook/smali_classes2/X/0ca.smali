.class public LX/0ca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cb;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/0ca;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile k:LX/0ca;


# instance fields
.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/0cb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0cc;

.field public final d:LX/0Xl;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00G;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pb;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0cg;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89094
    const-class v0, LX/0ca;

    sput-object v0, LX/0ca;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0cc;LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0cg;LX/0Ot;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cc;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/00G;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0pb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0cg;",
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 89083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89084
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/0ca;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 89085
    iput-object p1, p0, LX/0ca;->c:LX/0cc;

    .line 89086
    iput-object p2, p0, LX/0ca;->d:LX/0Xl;

    .line 89087
    iput-object p3, p0, LX/0ca;->e:LX/0Ot;

    .line 89088
    iput-object p4, p0, LX/0ca;->f:LX/0Ot;

    .line 89089
    iput-object p5, p0, LX/0ca;->g:LX/0Ot;

    .line 89090
    iput-object p6, p0, LX/0ca;->h:LX/0Ot;

    .line 89091
    iput-object p7, p0, LX/0ca;->i:LX/0cg;

    .line 89092
    iput-object p8, p0, LX/0ca;->j:LX/0Ot;

    .line 89093
    return-void
.end method

.method public static a(LX/0QB;)LX/0ca;
    .locals 12

    .prologue
    .line 89070
    sget-object v0, LX/0ca;->k:LX/0ca;

    if-nez v0, :cond_1

    .line 89071
    const-class v1, LX/0ca;

    monitor-enter v1

    .line 89072
    :try_start_0
    sget-object v0, LX/0ca;->k:LX/0ca;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 89073
    if-eqz v2, :cond_0

    .line 89074
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 89075
    new-instance v3, LX/0ca;

    invoke-static {v0}, LX/0cc;->b(LX/0QB;)LX/0cc;

    move-result-object v4

    check-cast v4, LX/0cc;

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v5

    check-cast v5, LX/0Xl;

    const/16 v6, 0x140f

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2d4

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0cg;->a(LX/0QB;)LX/0cg;

    move-result-object v10

    check-cast v10, LX/0cg;

    const/16 v11, 0x2ba

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/0ca;-><init>(LX/0cc;LX/0Xl;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0cg;LX/0Ot;)V

    .line 89076
    move-object v0, v3

    .line 89077
    sput-object v0, LX/0ca;->k:LX/0ca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89078
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 89079
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89080
    :cond_1
    sget-object v0, LX/0ca;->k:LX/0ca;

    return-object v0

    .line 89081
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 89082
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 89064
    if-nez p1, :cond_0

    move-object v0, v1

    .line 89065
    :goto_0
    return-object v0

    .line 89066
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/0ca;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lB;

    const-class v2, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    invoke-virtual {v0, p1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89067
    :catch_0
    move-exception v0

    .line 89068
    sget-object v2, LX/0ca;->a:Ljava/lang/Class;

    const-string v3, "Error parsing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 89069
    goto :goto_0
.end method

.method private declared-synchronized f()V
    .locals 2

    .prologue
    .line 89060
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89061
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Clear memory cache in process ("

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/0ca;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00G;

    invoke-virtual {v0}, LX/00G;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89062
    :cond_0
    monitor-exit p0

    return-void

    .line 89063
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 89058
    iget-object v0, p0, LX/0ca;->c:LX/0cc;

    invoke-virtual {v0, p1}, LX/0cc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89059
    invoke-direct {p0, v0}, LX/0ca;->d(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0cb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89095
    iget-object v0, p0, LX/0ca;->b:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method

.method public final a(LX/0Yu;)V
    .locals 1

    .prologue
    .line 89056
    iget-object v0, p0, LX/0ca;->i:LX/0cg;

    invoke-virtual {v0, p1}, LX/0cg;->a(LX/0Yu;)V

    .line 89057
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 89048
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89049
    const/4 v2, 0x0

    .line 89050
    new-instance v0, LX/2WQ;

    invoke-direct {v0}, LX/2WQ;-><init>()V

    invoke-virtual {v0, p1}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    const-string v1, "local_default_group"

    invoke-virtual {v0, v1}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v0

    iget-object v1, p0, LX/0ca;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {}, LX/0pb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    .line 89051
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 89052
    iput-object v1, v0, LX/2WQ;->g:Ljava/util/Map;

    .line 89053
    move-object v0, v0

    .line 89054
    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    move-object v0, v0

    .line 89055
    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryInterfaces$Configuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89041
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 89042
    iget-object v0, p0, LX/0ca;->c:LX/0cc;

    invoke-virtual {v0}, LX/0cc;->a()Ljava/util/Map;

    move-result-object v0

    .line 89043
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89044
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, LX/0ca;->d(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v1

    .line 89045
    if-eqz v1, :cond_0

    .line 89046
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 89047
    :cond_1
    return-object v2
.end method

.method public final c()LX/0d6;
    .locals 1

    .prologue
    .line 89040
    new-instance v0, LX/0d5;

    invoke-direct {v0, p0}, LX/0d5;-><init>(LX/0ca;)V

    return-object v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 89037
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89038
    const/4 v0, 0x0

    monitor-exit p0

    return-object v0

    .line 89039
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 89034
    invoke-direct {p0}, LX/0ca;->f()V

    .line 89035
    iget-object v0, p0, LX/0ca;->i:LX/0cg;

    invoke-virtual {v0}, LX/0cg;->a()V

    .line 89036
    return-void
.end method
