.class public LX/0ne;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "LX/0ne;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0ne;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _artifactId:Ljava/lang/String;

.field public final _groupId:Ljava/lang/String;

.field public final _majorVersion:I

.field public final _minorVersion:I

.field public final _patchLevel:I

.field public final _snapshotInfo:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 136433
    new-instance v0, LX/0ne;

    move v2, v1

    move v3, v1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, LX/0ne;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0ne;->a:LX/0ne;

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136399
    iput p1, p0, LX/0ne;->_majorVersion:I

    .line 136400
    iput p2, p0, LX/0ne;->_minorVersion:I

    .line 136401
    iput p3, p0, LX/0ne;->_patchLevel:I

    .line 136402
    iput-object p4, p0, LX/0ne;->_snapshotInfo:Ljava/lang/String;

    .line 136403
    if-nez p5, :cond_0

    const-string p5, ""

    :cond_0
    iput-object p5, p0, LX/0ne;->_groupId:Ljava/lang/String;

    .line 136404
    if-nez p6, :cond_1

    const-string p6, ""

    :cond_1
    iput-object p6, p0, LX/0ne;->_artifactId:Ljava/lang/String;

    .line 136405
    return-void
.end method

.method private a(LX/0ne;)I
    .locals 2

    .prologue
    .line 136406
    if-ne p1, p0, :cond_1

    const/4 v0, 0x0

    .line 136407
    :cond_0
    :goto_0
    return v0

    .line 136408
    :cond_1
    iget-object v0, p0, LX/0ne;->_groupId:Ljava/lang/String;

    iget-object v1, p1, LX/0ne;->_groupId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 136409
    if-nez v0, :cond_0

    .line 136410
    iget-object v0, p0, LX/0ne;->_artifactId:Ljava/lang/String;

    iget-object v1, p1, LX/0ne;->_artifactId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 136411
    if-nez v0, :cond_0

    .line 136412
    iget v0, p0, LX/0ne;->_majorVersion:I

    iget v1, p1, LX/0ne;->_majorVersion:I

    sub-int/2addr v0, v1

    .line 136413
    if-nez v0, :cond_0

    .line 136414
    iget v0, p0, LX/0ne;->_minorVersion:I

    iget v1, p1, LX/0ne;->_minorVersion:I

    sub-int/2addr v0, v1

    .line 136415
    if-nez v0, :cond_0

    .line 136416
    iget v0, p0, LX/0ne;->_patchLevel:I

    iget v1, p1, LX/0ne;->_patchLevel:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 136417
    iget-object v0, p0, LX/0ne;->_snapshotInfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0ne;->_snapshotInfo:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 136418
    check-cast p1, LX/0ne;

    invoke-direct {p0, p1}, LX/0ne;->a(LX/0ne;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 136419
    if-ne p1, p0, :cond_1

    .line 136420
    :cond_0
    :goto_0
    return v0

    .line 136421
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 136422
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 136423
    :cond_3
    check-cast p1, LX/0ne;

    .line 136424
    iget v2, p1, LX/0ne;->_majorVersion:I

    iget v3, p0, LX/0ne;->_majorVersion:I

    if-ne v2, v3, :cond_4

    iget v2, p1, LX/0ne;->_minorVersion:I

    iget v3, p0, LX/0ne;->_minorVersion:I

    if-ne v2, v3, :cond_4

    iget v2, p1, LX/0ne;->_patchLevel:I

    iget v3, p0, LX/0ne;->_patchLevel:I

    if-ne v2, v3, :cond_4

    iget-object v2, p1, LX/0ne;->_artifactId:Ljava/lang/String;

    iget-object v3, p0, LX/0ne;->_artifactId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, LX/0ne;->_groupId:Ljava/lang/String;

    iget-object v3, p0, LX/0ne;->_groupId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 136425
    iget-object v0, p0, LX/0ne;->_artifactId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/0ne;->_groupId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    iget v2, p0, LX/0ne;->_majorVersion:I

    add-int/2addr v1, v2

    iget v2, p0, LX/0ne;->_minorVersion:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/0ne;->_patchLevel:I

    add-int/2addr v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x2e

    .line 136426
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136427
    iget v1, p0, LX/0ne;->_majorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136428
    iget v1, p0, LX/0ne;->_minorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136429
    iget v1, p0, LX/0ne;->_patchLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 136430
    invoke-direct {p0}, LX/0ne;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136431
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0ne;->_snapshotInfo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136432
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
