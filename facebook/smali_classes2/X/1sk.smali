.class public LX/1sk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1sk;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0SG;

.field public final c:LX/0lC;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 335191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335192
    iput-object p1, p0, LX/1sk;->a:LX/0Zb;

    .line 335193
    iput-object p2, p0, LX/1sk;->b:LX/0SG;

    .line 335194
    iput-object p3, p0, LX/1sk;->c:LX/0lC;

    .line 335195
    return-void
.end method

.method public static a(LX/0QB;)LX/1sk;
    .locals 6

    .prologue
    .line 335196
    sget-object v0, LX/1sk;->d:LX/1sk;

    if-nez v0, :cond_1

    .line 335197
    const-class v1, LX/1sk;

    monitor-enter v1

    .line 335198
    :try_start_0
    sget-object v0, LX/1sk;->d:LX/1sk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 335199
    if-eqz v2, :cond_0

    .line 335200
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 335201
    new-instance p0, LX/1sk;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {p0, v3, v4, v5}, LX/1sk;-><init>(LX/0Zb;LX/0SG;LX/0lC;)V

    .line 335202
    move-object v0, p0

    .line 335203
    sput-object v0, LX/1sk;->d:LX/1sk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 335205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 335206
    :cond_1
    sget-object v0, LX/1sk;->d:LX/1sk;

    return-object v0

    .line 335207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 335208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
