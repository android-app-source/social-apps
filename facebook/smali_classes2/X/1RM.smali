.class public LX/1RM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1RJ;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1kG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1kG;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 246093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246094
    iput-object p1, p0, LX/1RM;->a:LX/0Ot;

    .line 246095
    iput-object p2, p0, LX/1RM;->b:Landroid/content/Context;

    .line 246096
    return-void
.end method

.method private d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 246091
    iget-object v0, p0, LX/1RM;->b:Landroid/content/Context;

    invoke-virtual {p0, p1, v0}, LX/1RM;->a(LX/1RN;Landroid/content/Context;)LX/AlW;

    move-result-object v0

    .line 246092
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LX/AkL;->g()LX/AkM;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 2

    .prologue
    .line 246076
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246077
    check-cast v0, LX/1kJ;

    .line 246078
    iget-object v1, p0, LX/1RM;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1kG;

    .line 246079
    iget-object p0, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, p0

    .line 246080
    goto :goto_1

    .line 246081
    :goto_0
    move-object v0, p0

    .line 246082
    return-object v0

    .line 246083
    :goto_1
    iget-object p0, v1, LX/1kG;->g:LX/AlX;

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/1kG;->g:LX/AlX;

    .line 246084
    iget-object p1, p0, LX/AlX;->b:Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    invoke-virtual {p1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->getContext()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_2

    instance-of p1, p2, Landroid/app/Activity;

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    :goto_2
    move p0, p1

    .line 246085
    if-eqz p0, :cond_1

    .line 246086
    :cond_0
    new-instance p0, LX/AlX;

    invoke-direct {p0, v1, p2}, LX/AlX;-><init>(LX/1kG;Landroid/content/Context;)V

    iput-object p0, v1, LX/1kG;->g:LX/AlX;

    .line 246087
    :cond_1
    iget-object p0, v1, LX/1kG;->g:LX/AlX;

    .line 246088
    iput-object v0, p0, LX/AlX;->c:LX/1lR;

    .line 246089
    iget-object p1, p0, LX/AlX;->b:Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    invoke-virtual {p1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(LX/1lR;)V

    .line 246090
    iget-object p0, v1, LX/1kG;->g:LX/AlX;

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_2
.end method

.method public final a(LX/1RN;)V
    .locals 0

    .prologue
    .line 246075
    return-void
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 1
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246071
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246072
    instance-of v0, v0, LX/1kJ;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246073
    invoke-direct {p0, p2}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setPromptSession(LX/1RN;)V

    .line 246074
    :cond_0
    return-void
.end method

.method public final a(ZLX/1RN;)V
    .locals 2

    .prologue
    .line 246065
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246066
    if-nez p1, :cond_0

    instance-of v1, v0, LX/1kJ;

    if-eqz v1, :cond_0

    invoke-direct {p0, p2}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 246067
    invoke-direct {p0, p2}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v1

    check-cast v0, LX/1kJ;

    .line 246068
    iget-object p0, v0, LX/1kJ;->a:LX/1lR;

    move-object v0, p0

    .line 246069
    invoke-virtual {v1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(LX/1lR;)V

    .line 246070
    :cond_0
    return-void
.end method

.method public final b(LX/1RN;)V
    .locals 1

    .prologue
    .line 246061
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246062
    instance-of v0, v0, LX/1kJ;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246063
    invoke-direct {p0, p1}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    .line 246064
    :cond_0
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 0
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 246060
    return-void
.end method

.method public final c(LX/1RN;)V
    .locals 1

    .prologue
    .line 246054
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246055
    instance-of v0, v0, LX/1kJ;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 246056
    invoke-direct {p0, p1}, LX/1RM;->d(LX/1RN;)Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    .line 246057
    :cond_0
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 246058
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 246059
    if-eqz v0, :cond_0

    instance-of v0, v0, LX/1kJ;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
