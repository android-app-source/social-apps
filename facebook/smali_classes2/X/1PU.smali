.class public LX/1PU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PV;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/1PY;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final c:LX/1PY;

.field private final d:LX/03V;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5Oj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 244072
    const-class v0, LX/1PU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1PU;->a:Ljava/lang/String;

    .line 244073
    new-instance v0, LX/1PX;

    invoke-direct {v0}, LX/1PX;-><init>()V

    sput-object v0, LX/1PU;->b:LX/1PY;

    return-void
.end method

.method public constructor <init>(LX/1PY;LX/03V;)V
    .locals 3
    .param p1    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244075
    iput-object p2, p0, LX/1PU;->d:LX/03V;

    .line 244076
    if-nez p1, :cond_0

    .line 244077
    iget-object v0, p0, LX/1PU;->d:LX/03V;

    sget-object v1, LX/1PU;->a:Ljava/lang/String;

    const-string v2, "Null delegate not supported. Please use DISABLE_SCROLL_LISTENERS instead for a no-op."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244078
    :cond_0
    iput-object p1, p0, LX/1PU;->c:LX/1PY;

    .line 244079
    return-void
.end method

.method public static a(LX/0g8;)LX/1PY;
    .locals 2

    .prologue
    .line 244080
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 244081
    new-instance v1, LX/1PZ;

    invoke-direct {v1, v0}, LX/1PZ;-><init>(Ljava/lang/ref/WeakReference;)V

    return-object v1
.end method


# virtual methods
.method public final a(LX/5Oj;)V
    .locals 3

    .prologue
    .line 244082
    iget-object v0, p0, LX/1PU;->c:LX/1PY;

    sget-object v1, LX/1PU;->b:LX/1PY;

    if-ne v0, v1, :cond_0

    .line 244083
    iget-object v0, p0, LX/1PU;->d:LX/03V;

    sget-object v1, LX/1PU;->a:Ljava/lang/String;

    const-string v2, "Trying to add a scroll listener when scroll listeners are disabled. Ensure thatthe environment for the fragment that this PartDefintion is currentlybeing rendered in does not have DISABLE_SCROLL_LISTENERS"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 244084
    :cond_0
    iget-object v0, p0, LX/1PU;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 244085
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1PU;->e:Ljava/util/List;

    .line 244086
    iget-object v0, p0, LX/1PU;->c:LX/1PY;

    invoke-interface {v0, p0}, LX/1PY;->a(LX/1PU;)V

    .line 244087
    :cond_1
    iget-object v0, p0, LX/1PU;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244088
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 244089
    iget-object v0, p0, LX/1PU;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 244090
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 244091
    iget-object v0, p0, LX/1PU;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 244092
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 244093
    iget-object v0, p0, LX/1PU;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Oj;

    invoke-interface {v0}, LX/5Oj;->a()V

    .line 244094
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 244095
    :cond_0
    return-void
.end method
