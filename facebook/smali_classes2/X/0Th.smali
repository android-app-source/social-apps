.class public abstract LX/0Th;
.super LX/0Ta;
.source ""

# interfaces
.implements LX/0Tf;


# instance fields
.field private final a:LX/0Tf;


# direct methods
.method public constructor <init>(LX/0Tf;)V
    .locals 0

    .prologue
    .line 63386
    invoke-direct {p0, p1}, LX/0Ta;-><init>(LX/0TD;)V

    .line 63387
    iput-object p1, p0, LX/0Th;->a:LX/0Tf;

    .line 63388
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63385
    iget-object v0, p0, LX/0Th;->a:LX/0Tf;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, LX/0Tf;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63384
    iget-object v0, p0, LX/0Th;->a:LX/0Tf;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 63383
    iget-object v0, p0, LX/0Th;->a:LX/0Tf;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0Tf;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63382
    iget-object v0, p0, LX/0Th;->a:LX/0Tf;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, LX/0Tf;->b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63381
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0Th;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63380
    invoke-virtual {p0, p1, p2, p3, p4}, LX/0Th;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63378
    invoke-virtual/range {p0 .. p6}, LX/0Th;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 63379
    invoke-virtual/range {p0 .. p6}, LX/0Th;->b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method
