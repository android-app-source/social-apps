.class public abstract LX/14l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Xp;

.field private final c:Landroid/content/IntentFilter;

.field private final d:Landroid/content/BroadcastReceiver;

.field private final e:Landroid/os/Looper;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179166
    const-class v0, LX/14l;

    sput-object v0, LX/14l;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/IntentFilter;)V
    .locals 1

    .prologue
    .line 179167
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/14l;-><init>(Landroid/content/Context;Landroid/content/IntentFilter;Landroid/os/Looper;)V

    .line 179168
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/IntentFilter;Landroid/os/Looper;)V
    .locals 1
    .param p3    # Landroid/os/Looper;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 179169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179170
    invoke-static {p1}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    iput-object v0, p0, LX/14l;->b:LX/0Xp;

    .line 179171
    iput-object p2, p0, LX/14l;->c:Landroid/content/IntentFilter;

    .line 179172
    new-instance v0, LX/14m;

    invoke-direct {v0, p0}, LX/14m;-><init>(LX/14l;)V

    iput-object v0, p0, LX/14l;->d:Landroid/content/BroadcastReceiver;

    .line 179173
    iput-object p3, p0, LX/14l;->e:Landroid/os/Looper;

    .line 179174
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 179175
    iget-boolean v0, p0, LX/14l;->f:Z

    if-eqz v0, :cond_0

    .line 179176
    sget-object v0, LX/14l;->a:Ljava/lang/Class;

    const-string v1, "Called registerNotificationReceiver twice."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 179177
    :goto_0
    return-void

    .line 179178
    :cond_0
    iget-object v0, p0, LX/14l;->b:LX/0Xp;

    iget-object v1, p0, LX/14l;->d:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, LX/14l;->c:Landroid/content/IntentFilter;

    iget-object v3, p0, LX/14l;->e:Landroid/os/Looper;

    invoke-virtual {v0, v1, v2, v3}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Landroid/os/Looper;)V

    .line 179179
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14l;->f:Z

    goto :goto_0
.end method

.method public abstract a(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public b()V
    .locals 2

    .prologue
    .line 179180
    iget-boolean v0, p0, LX/14l;->f:Z

    if-eqz v0, :cond_0

    .line 179181
    iget-object v0, p0, LX/14l;->b:LX/0Xp;

    iget-object v1, p0, LX/14l;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;)V

    .line 179182
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/14l;->f:Z

    .line 179183
    :cond_0
    return-void
.end method
