.class public LX/0tR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tS;


# direct methods
.method private constructor <init>(LX/0tS;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 154615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154616
    iput-object p1, p0, LX/0tR;->a:LX/0tS;

    .line 154617
    return-void
.end method

.method public static a(LX/0QB;)LX/0tR;
    .locals 1

    .prologue
    .line 154618
    invoke-static {p0}, LX/0tR;->b(LX/0QB;)LX/0tR;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/0tR;
    .locals 2

    .prologue
    .line 154619
    new-instance v1, LX/0tR;

    invoke-static {p0}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v0

    check-cast v0, LX/0tS;

    invoke-direct {v1, v0}, LX/0tR;-><init>(LX/0tS;)V

    .line 154620
    return-object v1
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 6

    .prologue
    .line 154621
    iget-object v0, p0, LX/0tR;->a:LX/0tS;

    invoke-virtual {v0}, LX/0tS;->b()I

    move-result v0

    .line 154622
    const-string v1, "is_inspiration"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "msqrd_facetracker_model_version"

    const-wide/16 v4, 0x5

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "msqrd_sdk_version"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "msqrd_capabilities"

    invoke-static {}, LX/8Cg;->a()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v1

    const-string v2, "model_type_capabilities"

    const-string v3, "FUSE_BIG"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "thumbnail_height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "thumbnail_width"

    invoke-static {v0}, LX/0tS;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "media_effects"

    const/4 p0, 0x1

    .line 154623
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 154624
    new-instance v3, LX/4Gx;

    invoke-direct {v3}, LX/4Gx;-><init>()V

    const-string v4, "MSQRD_MASK"

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/String;)LX/4Gx;

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/Integer;)LX/4Gx;

    move-result-object v3

    .line 154625
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 154626
    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/util/List;)LX/4Gx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 154627
    new-instance v3, LX/4Gx;

    invoke-direct {v3}, LX/4Gx;-><init>()V

    const-string v4, "FRAME"

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/String;)LX/4Gx;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/Integer;)LX/4Gx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 154628
    new-instance v3, LX/4Gx;

    invoke-direct {v3}, LX/4Gx;-><init>()V

    const-string v4, "PARTICLE_EFFECT"

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/String;)LX/4Gx;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/Integer;)LX/4Gx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 154629
    new-instance v3, LX/4Gx;

    invoke-direct {v3}, LX/4Gx;-><init>()V

    const-string v4, "SHADER_FILTER"

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/String;)LX/4Gx;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/Integer;)LX/4Gx;

    move-result-object v3

    new-instance v4, LX/4Gw;

    invoke-direct {v4}, LX/4Gw;-><init>()V

    const-string v5, "multipass"

    invoke-virtual {v4, v5}, LX/4Gw;->a(Ljava/lang/String;)LX/4Gw;

    move-result-object v4

    const-string v5, "true"

    invoke-virtual {v4, v5}, LX/4Gw;->b(Ljava/lang/String;)LX/4Gw;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/util/List;)LX/4Gx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 154630
    new-instance v3, LX/4Gx;

    invoke-direct {v3}, LX/4Gx;-><init>()V

    const-string v4, "STYLE_TRANSFER"

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/String;)LX/4Gx;

    move-result-object v3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/lang/Integer;)LX/4Gx;

    move-result-object v3

    new-instance v4, LX/4Gw;

    invoke-direct {v4}, LX/4Gw;-><init>()V

    const-string v5, "model_type"

    invoke-virtual {v4, v5}, LX/4Gw;->a(Ljava/lang/String;)LX/4Gw;

    move-result-object v4

    const-string v5, "fuse_big"

    invoke-virtual {v4, v5}, LX/4Gw;->b(Ljava/lang/String;)LX/4Gw;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4Gx;->a(Ljava/util/List;)LX/4Gx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 154631
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 154632
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 154633
    return-void
.end method
