.class public LX/0ue;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/5MG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/5MG;"
        }
    .end annotation

    .prologue
    .line 156748
    const-string v0, "same"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156749
    new-instance v0, LX/5MP;

    invoke-direct {v0, p1, p3}, LX/5MP;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 156750
    :goto_0
    return-object v0

    .line 156751
    :cond_0
    sget-object v0, LX/5MN;->e:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5MM;

    move-object v1, v0

    .line 156752
    if-eqz v1, :cond_1

    .line 156753
    new-instance v0, LX/5MN;

    invoke-direct {v0, p1, v1, p3}, LX/5MN;-><init>(Ljava/lang/String;LX/5MM;Ljava/util/List;)V

    goto :goto_0

    .line 156754
    :cond_1
    sget-object v0, LX/5MS;->e:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5MR;

    move-object v1, v0

    .line 156755
    if-eqz v1, :cond_2

    .line 156756
    new-instance v0, LX/5MS;

    invoke-direct {v0, p1, v1, p3}, LX/5MS;-><init>(Ljava/lang/String;LX/5MR;Ljava/util/List;)V

    goto :goto_0

    .line 156757
    :cond_2
    new-instance v0, LX/5MH;

    const-string v1, "Unknown bucket definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0
.end method
