.class public final LX/0YL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y8;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field private static volatile l:LX/0YL;


# instance fields
.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4l5;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/locks/ReadWriteLock;

.field public j:LX/03R;

.field public k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80977
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_socket_publisher_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0YL;->a:LX/0Tn;

    .line 80978
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_socket_publisher_flush_events"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0YL;->b:LX/0Tn;

    .line 80979
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "perfmarker_socket_publisher_port"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0YL;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 80953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80954
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0YL;->g:Ljava/util/List;

    .line 80955
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0YL;->h:Ljava/util/Set;

    .line 80956
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v0, p0, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 80957
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/0YL;->j:LX/03R;

    .line 80958
    const/16 v0, 0x2328

    iput v0, p0, LX/0YL;->k:I

    .line 80959
    iput-object p3, p0, LX/0YL;->f:Ljava/util/concurrent/ExecutorService;

    .line 80960
    iput-object p2, p0, LX/0YL;->e:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 80961
    iput-object p1, p0, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 80962
    iget-object v0, p0, LX/0YL;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v1, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;

    invoke-direct {v1, p0}, Lcom/facebook/quicklog/module/QPLSocketPublishListener$OnSharedPreferencesInitialized;-><init>(LX/0YL;)V

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 80963
    return-void
.end method

.method public static a(LX/0QB;)LX/0YL;
    .locals 6

    .prologue
    .line 80964
    sget-object v0, LX/0YL;->l:LX/0YL;

    if-nez v0, :cond_1

    .line 80965
    const-class v1, LX/0YL;

    monitor-enter v1

    .line 80966
    :try_start_0
    sget-object v0, LX/0YL;->l:LX/0YL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80967
    if-eqz v2, :cond_0

    .line 80968
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80969
    new-instance p0, LX/0YL;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/0YL;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/common/perftest/PerfTestConfig;Ljava/util/concurrent/ExecutorService;)V

    .line 80970
    move-object v0, p0

    .line 80971
    sput-object v0, LX/0YL;->l:LX/0YL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80972
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80973
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80974
    :cond_1
    sget-object v0, LX/0YL;->l:LX/0YL;

    return-object v0

    .line 80975
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80976
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 80926
    iget-object v1, p0, LX/0YL;->j:LX/03R;

    sget-object v2, LX/03R;->YES:LX/03R;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/0YL;->j:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 80927
    :cond_1
    if-nez v0, :cond_3

    .line 80928
    :cond_2
    :goto_0
    return-void

    .line 80929
    :cond_3
    new-instance v1, LX/4l5;

    .line 80930
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v0, v0

    .line 80931
    if-eqz v0, :cond_4

    .line 80932
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v0, v0

    .line 80933
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->g()Ljava/lang/String;

    move-result-object v2

    .line 80934
    iget v3, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    move v3, v3

    .line 80935
    invoke-direct {v1, v0, v2, v3}, LX/4l5;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80936
    const/4 v0, 0x0

    .line 80937
    iget-object v2, p0, LX/0YL;->g:Ljava/util/List;

    monitor-enter v2

    .line 80938
    :try_start_0
    iget-object v3, p0, LX/0YL;->g:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80939
    iget-object v3, p0, LX/0YL;->j:LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 80940
    monitor-exit v2

    goto :goto_0

    .line 80941
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 80942
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 80943
    :cond_5
    :try_start_1
    iget-object v3, p0, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80944
    :try_start_2
    iget-object v3, p0, LX/0YL;->h:Ljava/util/Set;

    .line 80945
    iget-object v4, v1, LX/4l5;->a:Ljava/lang/String;

    move-object v1, v4

    .line 80946
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 80947
    new-instance v0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;-><init>(LX/0YL;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80948
    :cond_6
    :try_start_3
    iget-object v1, p0, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 80949
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 80950
    if-eqz v0, :cond_2

    .line 80951
    iget-object v1, p0, LX/0YL;->f:Ljava/util/concurrent/ExecutorService;

    const v2, 0x23a9a585

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 80952
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/0YL;->i:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
