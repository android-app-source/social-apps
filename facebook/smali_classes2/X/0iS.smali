.class public LX/0iS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0iS;


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 120837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120838
    iput-object p1, p0, LX/0iS;->a:LX/0Xl;

    .line 120839
    return-void
.end method

.method public static a(LX/0QB;)LX/0iS;
    .locals 4

    .prologue
    .line 120840
    sget-object v0, LX/0iS;->b:LX/0iS;

    if-nez v0, :cond_1

    .line 120841
    const-class v1, LX/0iS;

    monitor-enter v1

    .line 120842
    :try_start_0
    sget-object v0, LX/0iS;->b:LX/0iS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 120843
    if-eqz v2, :cond_0

    .line 120844
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 120845
    new-instance p0, LX/0iS;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/0iS;-><init>(LX/0Xl;)V

    .line 120846
    move-object v0, p0

    .line 120847
    sput-object v0, LX/0iS;->b:LX/0iS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120848
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 120849
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 120850
    :cond_1
    sget-object v0, LX/0iS;->b:LX/0iS;

    return-object v0

    .line 120851
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 120852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 120853
    iget-object v0, p0, LX/0iS;->a:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.facebook.feed.util.NAVIGATE_TO_FEED_INTERACTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 120854
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 120855
    iget-object v0, p0, LX/0iS;->a:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.facebook.feed.util.NEWS_FEED_NEW_STORIES"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "new_story_count"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 120856
    return-void
.end method
