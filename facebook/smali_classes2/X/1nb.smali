.class public final LX/1nb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1na;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public A:Z

.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/text/TextUtils$TruncateAt;

.field public c:Z

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:F

.field public i:I

.field public j:Z

.field public k:I

.field public l:Landroid/content/res/ColorStateList;

.field public m:I

.field public n:I

.field public o:F

.field public p:F

.field public q:I

.field public r:Landroid/graphics/Typeface;

.field public s:Landroid/text/Layout$Alignment;

.field public t:Z

.field public u:Landroid/text/Layout;

.field public v:LX/1nd;

.field public w:Landroid/text/Layout;

.field public x:Ljava/lang/Float;

.field public y:[Landroid/text/style/ClickableSpan;

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x1000000

    .line 316908
    invoke-static {}, LX/1na;->q()LX/1na;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 316909
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nb;->c:Z

    .line 316910
    const/high16 v0, -0x80000000

    iput v0, p0, LX/1nb;->d:I

    .line 316911
    const v0, 0x7fffffff

    iput v0, p0, LX/1nb;->e:I

    .line 316912
    const v0, -0x777778

    iput v0, p0, LX/1nb;->i:I

    .line 316913
    iput v1, p0, LX/1nb;->k:I

    .line 316914
    sget-object v0, LX/1nc;->a:Landroid/content/res/ColorStateList;

    iput-object v0, p0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    .line 316915
    iput v1, p0, LX/1nb;->m:I

    .line 316916
    const/16 v0, 0xd

    iput v0, p0, LX/1nb;->n:I

    .line 316917
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/1nb;->p:F

    .line 316918
    sget v0, LX/1nc;->b:I

    iput v0, p0, LX/1nb;->q:I

    .line 316919
    sget-object v0, LX/1nc;->c:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/1nb;->r:Landroid/graphics/Typeface;

    .line 316920
    sget-object v0, LX/1nc;->e:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    .line 316921
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1nb;->t:Z

    .line 316922
    sget-object v0, LX/1nc;->d:LX/1nd;

    iput-object v0, p0, LX/1nb;->v:LX/1nd;

    .line 316923
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316924
    const-string v0, "Text"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1na;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316925
    check-cast p1, LX/1nb;

    .line 316926
    iget-object v0, p1, LX/1nb;->u:Landroid/text/Layout;

    iput-object v0, p0, LX/1nb;->u:Landroid/text/Layout;

    .line 316927
    iget-object v0, p1, LX/1nb;->w:Landroid/text/Layout;

    iput-object v0, p0, LX/1nb;->w:Landroid/text/Layout;

    .line 316928
    iget-object v0, p1, LX/1nb;->x:Ljava/lang/Float;

    iput-object v0, p0, LX/1nb;->x:Ljava/lang/Float;

    .line 316929
    iget-object v0, p1, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    iput-object v0, p0, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    .line 316930
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 316931
    if-ne p0, p1, :cond_1

    .line 316932
    :cond_0
    :goto_0
    return v0

    .line 316933
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 316934
    goto :goto_0

    .line 316935
    :cond_3
    check-cast p1, LX/1nb;

    .line 316936
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 316937
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 316938
    if-eq v2, v3, :cond_0

    .line 316939
    iget-object v2, p0, LX/1nb;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1nb;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/1nb;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 316940
    goto :goto_0

    .line 316941
    :cond_5
    iget-object v2, p1, LX/1nb;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 316942
    :cond_6
    iget-object v2, p0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    iget-object v3, p1, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/text/TextUtils$TruncateAt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 316943
    goto :goto_0

    .line 316944
    :cond_8
    iget-object v2, p1, LX/1nb;->b:Landroid/text/TextUtils$TruncateAt;

    if-nez v2, :cond_7

    .line 316945
    :cond_9
    iget-boolean v2, p0, LX/1nb;->c:Z

    iget-boolean v3, p1, LX/1nb;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 316946
    goto :goto_0

    .line 316947
    :cond_a
    iget v2, p0, LX/1nb;->d:I

    iget v3, p1, LX/1nb;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 316948
    goto :goto_0

    .line 316949
    :cond_b
    iget v2, p0, LX/1nb;->e:I

    iget v3, p1, LX/1nb;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 316950
    goto :goto_0

    .line 316951
    :cond_c
    iget v2, p0, LX/1nb;->f:F

    iget v3, p1, LX/1nb;->f:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_d

    move v0, v1

    .line 316952
    goto :goto_0

    .line 316953
    :cond_d
    iget v2, p0, LX/1nb;->g:F

    iget v3, p1, LX/1nb;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_e

    move v0, v1

    .line 316954
    goto :goto_0

    .line 316955
    :cond_e
    iget v2, p0, LX/1nb;->h:F

    iget v3, p1, LX/1nb;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_f

    move v0, v1

    .line 316956
    goto :goto_0

    .line 316957
    :cond_f
    iget v2, p0, LX/1nb;->i:I

    iget v3, p1, LX/1nb;->i:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 316958
    goto/16 :goto_0

    .line 316959
    :cond_10
    iget-boolean v2, p0, LX/1nb;->j:Z

    iget-boolean v3, p1, LX/1nb;->j:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 316960
    goto/16 :goto_0

    .line 316961
    :cond_11
    iget v2, p0, LX/1nb;->k:I

    iget v3, p1, LX/1nb;->k:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 316962
    goto/16 :goto_0

    .line 316963
    :cond_12
    iget-object v2, p0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/1nb;->l:Landroid/content/res/ColorStateList;

    iget-object v3, p1, LX/1nb;->l:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 316964
    goto/16 :goto_0

    .line 316965
    :cond_14
    iget-object v2, p1, LX/1nb;->l:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_13

    .line 316966
    :cond_15
    iget v2, p0, LX/1nb;->m:I

    iget v3, p1, LX/1nb;->m:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 316967
    goto/16 :goto_0

    .line 316968
    :cond_16
    iget v2, p0, LX/1nb;->n:I

    iget v3, p1, LX/1nb;->n:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 316969
    goto/16 :goto_0

    .line 316970
    :cond_17
    iget v2, p0, LX/1nb;->o:F

    iget v3, p1, LX/1nb;->o:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_18

    move v0, v1

    .line 316971
    goto/16 :goto_0

    .line 316972
    :cond_18
    iget v2, p0, LX/1nb;->p:F

    iget v3, p1, LX/1nb;->p:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_19

    move v0, v1

    .line 316973
    goto/16 :goto_0

    .line 316974
    :cond_19
    iget v2, p0, LX/1nb;->q:I

    iget v3, p1, LX/1nb;->q:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 316975
    goto/16 :goto_0

    .line 316976
    :cond_1a
    iget-object v2, p0, LX/1nb;->r:Landroid/graphics/Typeface;

    if-eqz v2, :cond_1c

    iget-object v2, p0, LX/1nb;->r:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/1nb;->r:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 316977
    goto/16 :goto_0

    .line 316978
    :cond_1c
    iget-object v2, p1, LX/1nb;->r:Landroid/graphics/Typeface;

    if-nez v2, :cond_1b

    .line 316979
    :cond_1d
    iget-object v2, p0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/1nb;->s:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/1nb;->s:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 316980
    goto/16 :goto_0

    .line 316981
    :cond_1f
    iget-object v2, p1, LX/1nb;->s:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_1e

    .line 316982
    :cond_20
    iget-boolean v2, p0, LX/1nb;->t:Z

    iget-boolean v3, p1, LX/1nb;->t:Z

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 316983
    goto/16 :goto_0

    .line 316984
    :cond_21
    iget-object v2, p0, LX/1nb;->v:LX/1nd;

    if-eqz v2, :cond_23

    iget-object v2, p0, LX/1nb;->v:LX/1nd;

    iget-object v3, p1, LX/1nb;->v:LX/1nd;

    invoke-virtual {v2, v3}, LX/1nd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    .line 316985
    goto/16 :goto_0

    .line 316986
    :cond_23
    iget-object v2, p1, LX/1nb;->v:LX/1nd;

    if-nez v2, :cond_22

    .line 316987
    :cond_24
    iget v2, p0, LX/1nb;->z:I

    iget v3, p1, LX/1nb;->z:I

    if-eq v2, v3, :cond_25

    move v0, v1

    .line 316988
    goto/16 :goto_0

    .line 316989
    :cond_25
    iget-boolean v2, p0, LX/1nb;->A:Z

    iget-boolean v3, p1, LX/1nb;->A:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 316990
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 316991
    const/4 v1, 0x0

    .line 316992
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1nb;

    .line 316993
    iput-object v1, v0, LX/1nb;->u:Landroid/text/Layout;

    .line 316994
    iput-object v1, v0, LX/1nb;->w:Landroid/text/Layout;

    .line 316995
    iput-object v1, v0, LX/1nb;->x:Ljava/lang/Float;

    .line 316996
    iput-object v1, v0, LX/1nb;->y:[Landroid/text/style/ClickableSpan;

    .line 316997
    return-object v0
.end method
