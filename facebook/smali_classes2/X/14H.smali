.class public LX/14H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/14H;


# instance fields
.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4cB;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178572
    const-class v0, LX/14H;

    sput-object v0, LX/14H;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178574
    iput-object p1, p0, LX/14H;->c:LX/0ad;

    .line 178575
    return-void
.end method

.method public static a(LX/0QB;)LX/14H;
    .locals 4

    .prologue
    .line 178576
    sget-object v0, LX/14H;->d:LX/14H;

    if-nez v0, :cond_1

    .line 178577
    const-class v1, LX/14H;

    monitor-enter v1

    .line 178578
    :try_start_0
    sget-object v0, LX/14H;->d:LX/14H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178579
    if-eqz v2, :cond_0

    .line 178580
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 178581
    new-instance p0, LX/14H;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/14H;-><init>(LX/0ad;)V

    .line 178582
    move-object v0, p0

    .line 178583
    sput-object v0, LX/14H;->d:LX/14H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178584
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178585
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178586
    :cond_1
    sget-object v0, LX/14H;->d:LX/14H;

    return-object v0

    .line 178587
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
