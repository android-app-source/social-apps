.class public final LX/15g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private b:LX/15h;

.field public c:LX/15h;

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180980
    new-instance v0, LX/15h;

    invoke-direct {v0}, LX/15h;-><init>()V

    iput-object v0, p0, LX/15g;->b:LX/15h;

    .line 180981
    iget-object v0, p0, LX/15g;->b:LX/15h;

    iput-object v0, p0, LX/15g;->c:LX/15h;

    .line 180982
    iput-boolean v1, p0, LX/15g;->d:Z

    .line 180983
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/15g;->a:Ljava/lang/String;

    .line 180984
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)LX/15g;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 180985
    new-instance v0, LX/15h;

    invoke-direct {v0}, LX/15h;-><init>()V

    .line 180986
    iget-object v1, p0, LX/15g;->c:LX/15h;

    iput-object v0, v1, LX/15h;->c:LX/15h;

    iput-object v0, p0, LX/15g;->c:LX/15h;

    .line 180987
    move-object v1, v0

    .line 180988
    iput-object p2, v1, LX/15h;->b:Ljava/lang/Object;

    .line 180989
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, LX/15h;->a:Ljava/lang/String;

    .line 180990
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)LX/15g;
    .locals 1

    .prologue
    .line 180963
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/15g;->b(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)LX/15g;
    .locals 2

    .prologue
    .line 180978
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/15g;->b(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 180977
    invoke-direct {p0, p1, p2}, LX/15g;->b(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)LX/15g;
    .locals 1

    .prologue
    .line 180976
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/15g;->b(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 180964
    iget-boolean v2, p0, LX/15g;->d:Z

    .line 180965
    const-string v1, ""

    .line 180966
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, LX/15g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 180967
    iget-object v0, p0, LX/15g;->b:LX/15h;

    iget-object v0, v0, LX/15h;->c:LX/15h;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 180968
    if-eqz v2, :cond_0

    iget-object v4, v1, LX/15h;->b:Ljava/lang/Object;

    if-eqz v4, :cond_2

    .line 180969
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180970
    const-string v0, ", "

    .line 180971
    iget-object v4, v1, LX/15h;->a:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 180972
    iget-object v4, v1, LX/15h;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180973
    :cond_1
    iget-object v4, v1, LX/15h;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180974
    :cond_2
    iget-object v1, v1, LX/15h;->c:LX/15h;

    goto :goto_0

    .line 180975
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
