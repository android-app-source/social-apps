.class public final LX/1st;
.super LX/1su;
.source ""


# static fields
.field private static final d:LX/1sv;

.field private static final f:LX/1sw;

.field private static final g:[B


# instance fields
.field public a:[B

.field public b:[B

.field public c:[B

.field private h:LX/1sx;

.field private i:S

.field private j:B

.field private k:LX/1sw;

.field private l:Ljava/lang/Boolean;

.field private m:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 335600
    new-instance v0, LX/1sv;

    const-string v1, ""

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1st;->d:LX/1sv;

    .line 335601
    new-instance v0, LX/1sw;

    const-string v1, ""

    invoke-direct {v0, v1, v2, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1st;->f:LX/1sw;

    .line 335602
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 335603
    sput-object v0, LX/1st;->g:[B

    aput-byte v2, v0, v2

    .line 335604
    sget-object v0, LX/1st;->g:[B

    const/4 v1, 0x2

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 335605
    sget-object v0, LX/1st;->g:[B

    aput-byte v3, v0, v3

    .line 335606
    sget-object v0, LX/1st;->g:[B

    aput-byte v4, v0, v5

    .line 335607
    sget-object v0, LX/1st;->g:[B

    const/4 v1, 0x5

    aput-byte v1, v0, v6

    .line 335608
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xa

    aput-byte v5, v0, v1

    .line 335609
    sget-object v0, LX/1st;->g:[B

    const/4 v1, 0x7

    aput-byte v1, v0, v4

    .line 335610
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xb

    aput-byte v6, v0, v1

    .line 335611
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xf

    const/16 v2, 0x9

    aput-byte v2, v0, v1

    .line 335612
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xe

    const/16 v2, 0xa

    aput-byte v2, v0, v1

    .line 335613
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xd

    const/16 v2, 0xb

    aput-byte v2, v0, v1

    .line 335614
    sget-object v0, LX/1st;->g:[B

    const/16 v1, 0xc

    const/16 v2, 0xc

    aput-byte v2, v0, v1

    .line 335615
    return-void
.end method

.method public constructor <init>(LX/1ss;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 335539
    invoke-direct {p0, p1}, LX/1su;-><init>(LX/1ss;)V

    .line 335540
    new-instance v0, LX/1sx;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, LX/1sx;-><init>(I)V

    iput-object v0, p0, LX/1st;->h:LX/1sx;

    .line 335541
    const/4 v0, 0x0

    iput-short v0, p0, LX/1st;->i:S

    .line 335542
    const/4 v0, 0x2

    iput-byte v0, p0, LX/1st;->j:B

    .line 335543
    iput-object v3, p0, LX/1st;->k:LX/1sw;

    .line 335544
    iput-object v3, p0, LX/1st;->l:Ljava/lang/Boolean;

    .line 335545
    const/4 v0, 0x5

    new-array v0, v0, [B

    iput-object v0, p0, LX/1st;->a:[B

    .line 335546
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, LX/1st;->b:[B

    .line 335547
    new-array v0, v2, [B

    iput-object v0, p0, LX/1st;->m:[B

    .line 335548
    new-array v0, v2, [B

    iput-object v0, p0, LX/1st;->c:[B

    .line 335549
    return-void
.end method

.method private a(LX/1sw;B)V
    .locals 2

    .prologue
    .line 335550
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-byte v0, p1, LX/1sw;->b:B

    invoke-static {v0}, LX/1st;->e(B)B

    move-result p2

    .line 335551
    :cond_0
    iget-short v0, p1, LX/1sw;->c:S

    iget-short v1, p0, LX/1st;->i:S

    if-le v0, v1, :cond_1

    iget-short v0, p1, LX/1sw;->c:S

    iget-short v1, p0, LX/1st;->i:S

    sub-int/2addr v0, v1

    const/16 v1, 0xf

    if-gt v0, v1, :cond_1

    .line 335552
    iget-short v0, p1, LX/1sw;->c:S

    iget-short v1, p0, LX/1st;->i:S

    sub-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x4

    or-int/2addr v0, p2

    invoke-static {p0, v0}, LX/1st;->d(LX/1st;I)V

    .line 335553
    :goto_0
    iget-short v0, p1, LX/1sw;->c:S

    iput-short v0, p0, LX/1st;->i:S

    .line 335554
    return-void

    .line 335555
    :cond_1
    invoke-direct {p0, p2}, LX/1st;->b(B)V

    .line 335556
    iget-short v0, p1, LX/1sw;->c:S

    invoke-virtual {p0, v0}, LX/1su;->a(S)V

    goto :goto_0
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 335557
    invoke-static {p0, p3}, LX/1st;->b(LX/1st;I)V

    .line 335558
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    invoke-virtual {v0, p1, p2, p3}, LX/1ss;->b([BII)V

    .line 335559
    return-void
.end method

.method private b(B)V
    .locals 2

    .prologue
    .line 335560
    iget-object v0, p0, LX/1st;->m:[B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    .line 335561
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/1st;->m:[B

    .line 335562
    const/4 p0, 0x0

    array-length p1, v1

    invoke-virtual {v0, v1, p0, p1}, LX/1ss;->b([BII)V

    .line 335563
    return-void
.end method

.method private b(J)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 335564
    move v0, v1

    .line 335565
    :goto_0
    const-wide/16 v2, -0x80

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 335566
    iget-object v2, p0, LX/1st;->b:[B

    add-int/lit8 v3, v0, 0x1

    long-to-int v4, p1

    int-to-byte v4, v4

    aput-byte v4, v2, v0

    .line 335567
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v2, p0, LX/1st;->b:[B

    invoke-virtual {v0, v2, v1, v3}, LX/1ss;->b([BII)V

    .line 335568
    return-void

    .line 335569
    :cond_0
    iget-object v3, p0, LX/1st;->b:[B

    add-int/lit8 v2, v0, 0x1

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 335570
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    move v0, v2

    goto :goto_0
.end method

.method public static b(LX/1st;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 335571
    move v0, v1

    .line 335572
    :goto_0
    and-int/lit8 v2, p1, -0x80

    if-nez v2, :cond_0

    .line 335573
    iget-object v2, p0, LX/1st;->a:[B

    add-int/lit8 v3, v0, 0x1

    int-to-byte v4, p1

    aput-byte v4, v2, v0

    .line 335574
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v2, p0, LX/1st;->a:[B

    invoke-virtual {v0, v2, v1, v3}, LX/1ss;->b([BII)V

    .line 335575
    return-void

    .line 335576
    :cond_0
    iget-object v3, p0, LX/1st;->a:[B

    add-int/lit8 v2, v0, 0x1

    and-int/lit8 v4, p1, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 335577
    ushr-int/lit8 p1, p1, 0x7

    move v0, v2

    goto :goto_0
.end method

.method private static c(I)I
    .locals 2

    .prologue
    .line 335578
    shl-int/lit8 v0, p0, 0x1

    shr-int/lit8 v1, p0, 0x1f

    xor-int/2addr v0, v1

    return v0
.end method

.method private static d(B)B
    .locals 3

    .prologue
    .line 335579
    and-int/lit8 v0, p0, 0xf

    int-to-byte v0, v0

    packed-switch v0, :pswitch_data_0

    .line 335580
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "don\'t know what type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit8 v2, p0, 0xf

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335581
    :pswitch_0
    const/4 v0, 0x0

    .line 335582
    :goto_0
    return v0

    .line 335583
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 335584
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 335585
    :pswitch_3
    const/4 v0, 0x6

    goto :goto_0

    .line 335586
    :pswitch_4
    const/16 v0, 0x8

    goto :goto_0

    .line 335587
    :pswitch_5
    const/16 v0, 0xa

    goto :goto_0

    .line 335588
    :pswitch_6
    const/4 v0, 0x4

    goto :goto_0

    .line 335589
    :pswitch_7
    const/16 v0, 0xb

    goto :goto_0

    .line 335590
    :pswitch_8
    const/16 v0, 0xf

    goto :goto_0

    .line 335591
    :pswitch_9
    const/16 v0, 0xe

    goto :goto_0

    .line 335592
    :pswitch_a
    const/16 v0, 0xd

    goto :goto_0

    .line 335593
    :pswitch_b
    const/16 v0, 0xc

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static d(LX/1st;I)V
    .locals 1

    .prologue
    .line 335594
    int-to-byte v0, p1

    invoke-direct {p0, v0}, LX/1st;->b(B)V

    .line 335595
    return-void
.end method

.method public static e(B)B
    .locals 1

    .prologue
    .line 335527
    sget-object v0, LX/1st;->g:[B

    aget-byte v0, v0, p0

    return v0
.end method

.method private e(I)[B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 335596
    if-nez p1, :cond_0

    new-array v0, v2, [B

    .line 335597
    :goto_0
    return-object v0

    .line 335598
    :cond_0
    new-array v0, p1, [B

    .line 335599
    iget-object v1, p0, LX/1su;->e:LX/1ss;

    invoke-virtual {v1, v0, v2, p1}, LX/1ss;->c([BII)I

    goto :goto_0
.end method

.method private static f(I)I
    .locals 2

    .prologue
    .line 335647
    ushr-int/lit8 v0, p0, 0x1

    and-int/lit8 v1, p0, 0x1

    neg-int v1, v1

    xor-int/2addr v0, v1

    return v0
.end method

.method private v()I
    .locals 8

    .prologue
    const/16 v7, 0x80

    const/4 v0, 0x0

    .line 335630
    const/4 v1, -0x1

    move v1, v1

    .line 335631
    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    .line 335632
    const/4 v1, 0x0

    move-object v3, v1

    .line 335633
    const/4 v1, 0x0

    move v4, v1

    .line 335634
    move v1, v0

    move v2, v0

    .line 335635
    :goto_0
    add-int v5, v4, v0

    aget-byte v5, v3, v5

    .line 335636
    and-int/lit8 v6, v5, 0x7f

    shl-int/2addr v6, v1

    or-int/2addr v2, v6

    .line 335637
    and-int/lit16 v5, v5, 0x80

    if-ne v5, v7, :cond_1

    .line 335638
    add-int/lit8 v1, v1, 0x7

    .line 335639
    add-int/lit8 v0, v0, 0x1

    .line 335640
    goto :goto_0

    .line 335641
    :goto_1
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v2

    .line 335642
    and-int/lit8 v3, v2, 0x7f

    shl-int/2addr v3, v0

    or-int/2addr v1, v3

    .line 335643
    and-int/lit16 v2, v2, 0x80

    if-ne v2, v7, :cond_0

    .line 335644
    add-int/lit8 v0, v0, 0x7

    .line 335645
    goto :goto_1

    :cond_0
    move v2, v1

    .line 335646
    :cond_1
    return v2

    :cond_2
    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 335627
    iget-object v0, p0, LX/1st;->h:LX/1sx;

    iget-short v1, p0, LX/1st;->i:S

    invoke-virtual {v0, v1}, LX/1sx;->a(S)V

    .line 335628
    const/4 v0, 0x0

    iput-short v0, p0, LX/1st;->i:S

    .line 335629
    return-void
.end method

.method public final a(B)V
    .locals 0

    .prologue
    .line 335625
    invoke-direct {p0, p1}, LX/1st;->b(B)V

    .line 335626
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 335623
    invoke-static {p1}, LX/1st;->c(I)I

    move-result v0

    invoke-static {p0, v0}, LX/1st;->b(LX/1st;I)V

    .line 335624
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 335620
    const/4 v3, 0x1

    shl-long v3, p1, v3

    const/16 v5, 0x3f

    shr-long v5, p1, v5

    xor-long/2addr v3, v5

    move-wide v0, v3

    .line 335621
    invoke-direct {p0, v0, v1}, LX/1st;->b(J)V

    .line 335622
    return-void
.end method

.method public final a(LX/1sw;)V
    .locals 2

    .prologue
    .line 335616
    iget-byte v0, p1, LX/1sw;->b:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 335617
    iput-object p1, p0, LX/1st;->k:LX/1sw;

    .line 335618
    :goto_0
    return-void

    .line 335619
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, LX/1st;->a(LX/1sw;B)V

    goto :goto_0
.end method

.method public final a(LX/1u3;)V
    .locals 3

    .prologue
    .line 335528
    iget-byte v0, p1, LX/1u3;->a:B

    iget v1, p1, LX/1u3;->b:I

    .line 335529
    const/16 v2, 0xe

    if-gt v1, v2, :cond_0

    .line 335530
    shl-int/lit8 v2, v1, 0x4

    invoke-static {v0}, LX/1st;->e(B)B

    move-result p1

    or-int/2addr v2, p1

    invoke-static {p0, v2}, LX/1st;->d(LX/1st;I)V

    .line 335531
    :goto_0
    return-void

    .line 335532
    :cond_0
    invoke-static {v0}, LX/1st;->e(B)B

    move-result v2

    or-int/lit16 v2, v2, 0xf0

    invoke-static {p0, v2}, LX/1st;->d(LX/1st;I)V

    .line 335533
    invoke-static {p0, v1}, LX/1st;->b(LX/1st;I)V

    goto :goto_0
.end method

.method public final a(LX/7H3;)V
    .locals 2

    .prologue
    .line 335534
    iget v0, p1, LX/7H3;->c:I

    if-nez v0, :cond_0

    .line 335535
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1st;->d(LX/1st;I)V

    .line 335536
    :goto_0
    return-void

    .line 335537
    :cond_0
    iget v0, p1, LX/7H3;->c:I

    invoke-static {p0, v0}, LX/1st;->b(LX/1st;I)V

    .line 335538
    iget-byte v0, p1, LX/7H3;->a:B

    invoke-static {v0}, LX/1st;->e(B)B

    move-result v0

    shl-int/lit8 v0, v0, 0x4

    iget-byte v1, p1, LX/7H3;->b:B

    invoke-static {v1}, LX/1st;->e(B)B

    move-result v1

    or-int/2addr v0, v1

    invoke-static {p0, v0}, LX/1st;->d(LX/1st;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 335418
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 335419
    const/4 v1, 0x0

    array-length v2, v0

    invoke-direct {p0, v0, v1, v2}, LX/1st;->a([BII)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335420
    return-void

    .line 335421
    :catch_0
    new-instance v0, LX/7H0;

    const-string v1, "UTF-8 not supported!"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(S)V
    .locals 1

    .prologue
    .line 335422
    invoke-static {p1}, LX/1st;->c(I)I

    move-result v0

    invoke-static {p0, v0}, LX/1st;->b(LX/1st;I)V

    .line 335423
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 335424
    iget-object v2, p0, LX/1st;->k:LX/1sw;

    if-eqz v2, :cond_1

    .line 335425
    iget-object v2, p0, LX/1st;->k:LX/1sw;

    if-eqz p1, :cond_0

    :goto_0
    invoke-direct {p0, v2, v0}, LX/1st;->a(LX/1sw;B)V

    .line 335426
    const/4 v0, 0x0

    iput-object v0, p0, LX/1st;->k:LX/1sw;

    .line 335427
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 335428
    goto :goto_0

    .line 335429
    :cond_1
    if-eqz p1, :cond_2

    :goto_2
    invoke-direct {p0, v0}, LX/1st;->b(B)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final a([B)V
    .locals 2

    .prologue
    .line 335430
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, LX/1st;->a([BII)V

    .line 335431
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 335432
    iget-object v0, p0, LX/1st;->h:LX/1sx;

    invoke-virtual {v0}, LX/1sx;->a()S

    move-result v0

    iput-short v0, p0, LX/1st;->i:S

    .line 335433
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 335434
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1st;->b(B)V

    .line 335435
    return-void
.end method

.method public final d()LX/1sv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1sv;"
        }
    .end annotation

    .prologue
    .line 335436
    iget-object v0, p0, LX/1st;->h:LX/1sx;

    iget-short v1, p0, LX/1st;->i:S

    invoke-virtual {v0, v1}, LX/1sx;->a(S)V

    .line 335437
    const/4 v0, 0x0

    iput-short v0, p0, LX/1st;->i:S

    .line 335438
    sget-object v0, LX/1st;->d:LX/1sv;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 335439
    iget-object v0, p0, LX/1st;->h:LX/1sx;

    invoke-virtual {v0}, LX/1sx;->a()S

    move-result v0

    iput-short v0, p0, LX/1st;->i:S

    .line 335440
    return-void
.end method

.method public final f()LX/1sw;
    .locals 5

    .prologue
    .line 335441
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v2

    .line 335442
    if-nez v2, :cond_0

    .line 335443
    sget-object v0, LX/1st;->f:LX/1sw;

    .line 335444
    :goto_0
    return-object v0

    .line 335445
    :cond_0
    and-int/lit16 v0, v2, 0xf0

    shr-int/lit8 v0, v0, 0x4

    int-to-short v0, v0

    .line 335446
    if-nez v0, :cond_3

    .line 335447
    invoke-virtual {p0}, LX/1su;->l()S

    move-result v0

    .line 335448
    :goto_1
    new-instance v1, LX/1sw;

    const-string v3, ""

    and-int/lit8 v4, v2, 0xf

    int-to-byte v4, v4

    invoke-static {v4}, LX/1st;->d(B)B

    move-result v4

    invoke-direct {v1, v3, v4, v0}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    .line 335449
    const/4 v0, 0x1

    .line 335450
    and-int/lit8 v3, v2, 0xf

    .line 335451
    if-eq v3, v0, :cond_1

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    :cond_1
    :goto_2
    move v0, v0

    .line 335452
    if-eqz v0, :cond_2

    .line 335453
    and-int/lit8 v0, v2, 0xf

    int-to-byte v0, v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_3
    iput-object v0, p0, LX/1st;->l:Ljava/lang/Boolean;

    .line 335454
    :cond_2
    iget-short v0, v1, LX/1sw;->c:S

    iput-short v0, p0, LX/1st;->i:S

    move-object v0, v1

    .line 335455
    goto :goto_0

    .line 335456
    :cond_3
    iget-short v1, p0, LX/1st;->i:S

    add-int/2addr v0, v1

    int-to-short v0, v0

    goto :goto_1

    .line 335457
    :cond_4
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final g()LX/7H3;
    .locals 4

    .prologue
    .line 335458
    invoke-direct {p0}, LX/1st;->v()I

    move-result v1

    .line 335459
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 335460
    :goto_0
    new-instance v2, LX/7H3;

    shr-int/lit8 v3, v0, 0x4

    int-to-byte v3, v3

    invoke-static {v3}, LX/1st;->d(B)B

    move-result v3

    and-int/lit8 v0, v0, 0xf

    int-to-byte v0, v0

    invoke-static {v0}, LX/1st;->d(B)B

    move-result v0

    invoke-direct {v2, v3, v0, v1}, LX/7H3;-><init>(BBI)V

    return-object v2

    .line 335461
    :cond_0
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v0

    goto :goto_0
.end method

.method public final h()LX/1u3;
    .locals 3

    .prologue
    .line 335462
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    .line 335463
    shr-int/lit8 v0, v1, 0x4

    and-int/lit8 v0, v0, 0xf

    .line 335464
    const/16 v2, 0xf

    if-ne v0, v2, :cond_0

    .line 335465
    invoke-direct {p0}, LX/1st;->v()I

    move-result v0

    .line 335466
    :cond_0
    invoke-static {v1}, LX/1st;->d(B)B

    move-result v1

    .line 335467
    new-instance v2, LX/1u3;

    invoke-direct {v2, v1, v0}, LX/1u3;-><init>(BI)V

    return-object v2
.end method

.method public final i()LX/7H5;
    .locals 2

    .prologue
    .line 335468
    new-instance v0, LX/7H5;

    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H5;-><init>(LX/1u3;)V

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 335469
    iget-object v1, p0, LX/1st;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 335470
    iget-object v0, p0, LX/1st;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 335471
    const/4 v1, 0x0

    iput-object v1, p0, LX/1st;->l:Ljava/lang/Boolean;

    .line 335472
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 335473
    const/4 v0, -0x1

    move v0, v0

    .line 335474
    if-lez v0, :cond_0

    .line 335475
    const/4 v0, 0x0

    move-object v0, v0

    .line 335476
    const/4 v1, 0x0

    move v1, v1

    .line 335477
    aget-byte v0, v0, v1

    .line 335478
    :goto_0
    return v0

    .line 335479
    :cond_0
    iget-object v0, p0, LX/1su;->e:LX/1ss;

    iget-object v1, p0, LX/1st;->c:[B

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, LX/1ss;->c([BII)I

    .line 335480
    iget-object v0, p0, LX/1st;->c:[B

    aget-byte v0, v0, v3

    goto :goto_0
.end method

.method public final l()S
    .locals 1

    .prologue
    .line 335481
    invoke-direct {p0}, LX/1st;->v()I

    move-result v0

    invoke-static {v0}, LX/1st;->f(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 335482
    invoke-direct {p0}, LX/1st;->v()I

    move-result v0

    invoke-static {v0}, LX/1st;->f(I)I

    move-result v0

    return v0
.end method

.method public final n()J
    .locals 13

    .prologue
    .line 335483
    const/16 v12, 0x80

    const/4 v2, 0x0

    .line 335484
    const-wide/16 v4, 0x0

    .line 335485
    const/4 v3, -0x1

    move v3, v3

    .line 335486
    const/16 v6, 0xa

    if-lt v3, v6, :cond_0

    .line 335487
    const/4 v3, 0x0

    move-object v6, v3

    .line 335488
    const/4 v3, 0x0

    move v7, v3

    .line 335489
    move v3, v2

    .line 335490
    :goto_0
    add-int v8, v7, v2

    aget-byte v8, v6, v8

    .line 335491
    and-int/lit8 v9, v8, 0x7f

    int-to-long v10, v9

    shl-long/2addr v10, v3

    or-long/2addr v4, v10

    .line 335492
    and-int/lit16 v8, v8, 0x80

    if-ne v8, v12, :cond_1

    .line 335493
    add-int/lit8 v3, v3, 0x7

    .line 335494
    add-int/lit8 v2, v2, 0x1

    .line 335495
    goto :goto_0

    .line 335496
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/1su;->k()B

    move-result v3

    .line 335497
    and-int/lit8 v6, v3, 0x7f

    int-to-long v6, v6

    shl-long/2addr v6, v2

    or-long/2addr v4, v6

    .line 335498
    and-int/lit16 v3, v3, 0x80

    if-ne v3, v12, :cond_1

    .line 335499
    add-int/lit8 v2, v2, 0x7

    .line 335500
    goto :goto_1

    .line 335501
    :cond_1
    move-wide v0, v4

    .line 335502
    const/4 v2, 0x1

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x1

    and-long/2addr v4, v0

    neg-long v4, v4

    xor-long/2addr v2, v4

    move-wide v0, v2

    .line 335503
    return-wide v0
.end method

.method public final o()D
    .locals 12

    .prologue
    const/16 v3, 0x8

    .line 335504
    new-array v0, v3, [B

    .line 335505
    iget-object v1, p0, LX/1su;->e:LX/1ss;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/1ss;->c([BII)I

    .line 335506
    iget-byte v1, p0, LX/1st;->j:B

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    .line 335507
    const-wide/16 v10, 0xff

    .line 335508
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    int-to-long v4, v4

    and-long/2addr v4, v10

    const/16 v6, 0x38

    shl-long/2addr v4, v6

    const/4 v6, 0x1

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x30

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x2

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x28

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x3

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x20

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x4

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x18

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x5

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x10

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x6

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x8

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x7

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    or-long/2addr v4, v6

    move-wide v0, v4

    .line 335509
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0

    .line 335510
    :cond_0
    const-wide/16 v10, 0xff

    .line 335511
    const/4 v4, 0x7

    aget-byte v4, v0, v4

    int-to-long v4, v4

    and-long/2addr v4, v10

    const/16 v6, 0x38

    shl-long/2addr v4, v6

    const/4 v6, 0x6

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x30

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x5

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x28

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x4

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x20

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x3

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x18

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x2

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x10

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x1

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    const/16 v8, 0x8

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/4 v6, 0x0

    aget-byte v6, v0, v6

    int-to-long v6, v6

    and-long/2addr v6, v10

    or-long/2addr v4, v6

    move-wide v0, v4

    .line 335512
    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 5

    .prologue
    .line 335513
    invoke-direct {p0}, LX/1st;->v()I

    move-result v1

    .line 335514
    if-nez v1, :cond_0

    .line 335515
    const-string v0, ""

    .line 335516
    :goto_0
    return-object v0

    .line 335517
    :cond_0
    :try_start_0
    const/4 v0, -0x1

    move v0, v0

    .line 335518
    if-lt v0, v1, :cond_1

    .line 335519
    new-instance v0, Ljava/lang/String;

    .line 335520
    const/4 v2, 0x0

    move-object v2, v2

    .line 335521
    const/4 v3, 0x0

    move v3, v3

    .line 335522
    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 335523
    :catch_0
    new-instance v0, LX/7H0;

    const-string v1, "UTF-8 not supported!"

    invoke-direct {v0, v1}, LX/7H0;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335524
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, v1}, LX/1st;->e(I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final q()[B
    .locals 1

    .prologue
    .line 335525
    invoke-direct {p0}, LX/1st;->v()I

    move-result v0

    .line 335526
    invoke-direct {p0, v0}, LX/1st;->e(I)[B

    move-result-object v0

    return-object v0
.end method
