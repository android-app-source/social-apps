.class public LX/0os;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0ot",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 143235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143236
    iput-object p1, p0, LX/0os;->a:Ljava/lang/Class;

    .line 143237
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 143238
    :try_start_0
    iget-object v0, p0, LX/0os;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 143239
    :goto_0
    return-object v0

    .line 143240
    :catch_0
    move-exception v0

    .line 143241
    sget-object v1, LX/0ou;->a:Ljava/lang/Class;

    const-string v2, "Couldn\'t instantiate object"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143242
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 143243
    :catch_1
    move-exception v0

    .line 143244
    sget-object v1, LX/0ou;->a:Ljava/lang/Class;

    const-string v2, "Couldn\'t instantiate object"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 143245
    return-void
.end method
