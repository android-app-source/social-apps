.class public LX/1gg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1ge;


# direct methods
.method public constructor <init>(LX/1gc;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 294485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294486
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1gc;->a(Ljava/lang/String;)LX/1ge;

    move-result-object v0

    iput-object v0, p0, LX/1gg;->a:LX/1ge;

    .line 294487
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 294488
    iget-object v0, p0, LX/1gg;->a:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->d()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 294489
    iget-object v0, p0, LX/1gg;->a:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->a()V

    .line 294490
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 294491
    iget-object v0, p0, LX/1gg;->a:LX/1ge;

    invoke-virtual {v0}, LX/1ge;->b()V

    .line 294492
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 294493
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/1gg;->a:LX/1ge;

    invoke-virtual {v3}, LX/1ge;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
