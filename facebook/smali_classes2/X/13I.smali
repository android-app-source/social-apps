.class public final LX/13I;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:[C

.field public static final c:[C

.field public static final d:[B

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field private static g:I

.field private static h:I

.field private static i:J

.field private static j:J

.field private static k:J

.field private static l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x2

    const/4 v14, 0x1

    const/16 v13, 0xfa0

    const/16 v12, 0xa

    const/4 v1, 0x0

    .line 176196
    const v0, 0xf4240

    sput v0, LX/13I;->g:I

    .line 176197
    const v0, 0x3b9aca00

    sput v0, LX/13I;->h:I

    .line 176198
    const-wide v2, 0x2540be400L

    sput-wide v2, LX/13I;->i:J

    .line 176199
    const-wide/16 v2, 0x3e8

    sput-wide v2, LX/13I;->j:J

    .line 176200
    const-wide/32 v2, -0x80000000

    sput-wide v2, LX/13I;->k:J

    .line 176201
    const-wide/32 v2, 0x7fffffff

    sput-wide v2, LX/13I;->l:J

    .line 176202
    const-string v0, "-9223372036854775808"

    sput-object v0, LX/13I;->a:Ljava/lang/String;

    .line 176203
    new-array v0, v13, [C

    sput-object v0, LX/13I;->b:[C

    .line 176204
    new-array v0, v13, [C

    sput-object v0, LX/13I;->c:[C

    move v8, v1

    move v5, v1

    .line 176205
    :goto_0
    if-ge v8, v12, :cond_4

    .line 176206
    add-int/lit8 v0, v8, 0x30

    int-to-char v2, v0

    .line 176207
    if-nez v8, :cond_0

    move v0, v1

    :goto_1
    move v7, v1

    .line 176208
    :goto_2
    if-ge v7, v12, :cond_3

    .line 176209
    add-int/lit8 v3, v7, 0x30

    int-to-char v4, v3

    .line 176210
    if-nez v8, :cond_1

    if-nez v7, :cond_1

    move v3, v1

    :goto_3
    move v6, v5

    move v5, v1

    .line 176211
    :goto_4
    if-ge v5, v12, :cond_2

    .line 176212
    add-int/lit8 v9, v5, 0x30

    int-to-char v9, v9

    .line 176213
    sget-object v10, LX/13I;->b:[C

    aput-char v0, v10, v6

    .line 176214
    sget-object v10, LX/13I;->b:[C

    add-int/lit8 v11, v6, 0x1

    aput-char v3, v10, v11

    .line 176215
    sget-object v10, LX/13I;->b:[C

    add-int/lit8 v11, v6, 0x2

    aput-char v9, v10, v11

    .line 176216
    sget-object v10, LX/13I;->c:[C

    aput-char v2, v10, v6

    .line 176217
    sget-object v10, LX/13I;->c:[C

    add-int/lit8 v11, v6, 0x1

    aput-char v4, v10, v11

    .line 176218
    sget-object v10, LX/13I;->c:[C

    add-int/lit8 v11, v6, 0x2

    aput-char v9, v10, v11

    .line 176219
    add-int/lit8 v6, v6, 0x4

    .line 176220
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_0
    move v0, v2

    .line 176221
    goto :goto_1

    :cond_1
    move v3, v4

    .line 176222
    goto :goto_3

    .line 176223
    :cond_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v6

    goto :goto_2

    .line 176224
    :cond_3
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 176225
    :cond_4
    new-array v0, v13, [B

    sput-object v0, LX/13I;->d:[B

    move v0, v1

    .line 176226
    :goto_5
    if-ge v0, v13, :cond_5

    .line 176227
    sget-object v2, LX/13I;->d:[B

    sget-object v3, LX/13I;->c:[C

    aget-char v3, v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 176228
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 176229
    :cond_5
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "0"

    aput-object v2, v0, v1

    const-string v2, "1"

    aput-object v2, v0, v14

    const-string v2, "2"

    aput-object v2, v0, v15

    const/4 v2, 0x3

    const-string v3, "3"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string v3, "4"

    aput-object v3, v0, v2

    const/4 v2, 0x5

    const-string v3, "5"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    const-string v3, "6"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "7"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "8"

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "9"

    aput-object v3, v0, v2

    const-string v2, "10"

    aput-object v2, v0, v12

    sput-object v0, LX/13I;->e:[Ljava/lang/String;

    .line 176230
    new-array v0, v12, [Ljava/lang/String;

    const-string v2, "-1"

    aput-object v2, v0, v1

    const-string v1, "-2"

    aput-object v1, v0, v14

    const-string v1, "-3"

    aput-object v1, v0, v15

    const/4 v1, 0x3

    const-string v2, "-4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "-5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "-6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "-7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "-8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "-9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "-10"

    aput-object v2, v0, v1

    sput-object v0, LX/13I;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I[BI)I
    .locals 5

    .prologue
    .line 176365
    if-gez p0, :cond_1

    .line 176366
    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_0

    .line 176367
    int-to-long v0, p0

    invoke-static {v0, v1, p1, p2}, LX/13I;->a(J[BI)I

    move-result v0

    .line 176368
    :goto_0
    return v0

    .line 176369
    :cond_0
    add-int/lit8 v0, p2, 0x1

    const/16 v1, 0x2d

    aput-byte v1, p1, p2

    .line 176370
    neg-int p0, p0

    move p2, v0

    .line 176371
    :cond_1
    sget v0, LX/13I;->g:I

    if-ge p0, v0, :cond_4

    .line 176372
    const/16 v0, 0x3e8

    if-ge p0, v0, :cond_3

    .line 176373
    const/16 v0, 0xa

    if-ge p0, v0, :cond_2

    .line 176374
    add-int/lit8 v0, p2, 0x1

    add-int/lit8 v1, p0, 0x30

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    goto :goto_0

    .line 176375
    :cond_2
    invoke-static {p0, p1, p2}, LX/13I;->b(I[BI)I

    move-result v0

    goto :goto_0

    .line 176376
    :cond_3
    div-int/lit16 v0, p0, 0x3e8

    .line 176377
    mul-int/lit16 v1, v0, 0x3e8

    sub-int v1, p0, v1

    .line 176378
    invoke-static {v0, p1, p2}, LX/13I;->b(I[BI)I

    move-result v0

    .line 176379
    invoke-static {v1, p1, v0}, LX/13I;->c(I[BI)I

    move-result v0

    goto :goto_0

    .line 176380
    :cond_4
    sget v0, LX/13I;->h:I

    if-lt p0, v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    .line 176381
    :goto_1
    if-eqz v1, :cond_5

    .line 176382
    sget v0, LX/13I;->h:I

    sub-int/2addr p0, v0

    .line 176383
    sget v0, LX/13I;->h:I

    if-lt p0, v0, :cond_7

    .line 176384
    sget v0, LX/13I;->h:I

    sub-int/2addr p0, v0

    .line 176385
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x32

    aput-byte v2, p1, p2

    move p2, v0

    .line 176386
    :cond_5
    :goto_2
    div-int/lit16 v0, p0, 0x3e8

    .line 176387
    mul-int/lit16 v2, v0, 0x3e8

    sub-int v2, p0, v2

    .line 176388
    div-int/lit16 v3, v0, 0x3e8

    .line 176389
    mul-int/lit16 v4, v3, 0x3e8

    sub-int v4, v0, v4

    .line 176390
    if-eqz v1, :cond_8

    .line 176391
    invoke-static {v3, p1, p2}, LX/13I;->c(I[BI)I

    move-result v0

    .line 176392
    :goto_3
    invoke-static {v4, p1, v0}, LX/13I;->c(I[BI)I

    move-result v0

    .line 176393
    invoke-static {v2, p1, v0}, LX/13I;->c(I[BI)I

    move-result v0

    goto :goto_0

    .line 176394
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 176395
    :cond_7
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x31

    aput-byte v2, p1, p2

    move p2, v0

    goto :goto_2

    .line 176396
    :cond_8
    invoke-static {v3, p1, p2}, LX/13I;->b(I[BI)I

    move-result v0

    goto :goto_3
.end method

.method public static a(I[CI)I
    .locals 5

    .prologue
    .line 176333
    if-gez p0, :cond_1

    .line 176334
    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_0

    .line 176335
    int-to-long v0, p0

    invoke-static {v0, v1, p1, p2}, LX/13I;->a(J[CI)I

    move-result v0

    .line 176336
    :goto_0
    return v0

    .line 176337
    :cond_0
    add-int/lit8 v0, p2, 0x1

    const/16 v1, 0x2d

    aput-char v1, p1, p2

    .line 176338
    neg-int p0, p0

    move p2, v0

    .line 176339
    :cond_1
    sget v0, LX/13I;->g:I

    if-ge p0, v0, :cond_4

    .line 176340
    const/16 v0, 0x3e8

    if-ge p0, v0, :cond_3

    .line 176341
    const/16 v0, 0xa

    if-ge p0, v0, :cond_2

    .line 176342
    add-int/lit8 v0, p2, 0x1

    add-int/lit8 v1, p0, 0x30

    int-to-char v1, v1

    aput-char v1, p1, p2

    goto :goto_0

    .line 176343
    :cond_2
    invoke-static {p0, p1, p2}, LX/13I;->b(I[CI)I

    move-result v0

    goto :goto_0

    .line 176344
    :cond_3
    div-int/lit16 v0, p0, 0x3e8

    .line 176345
    mul-int/lit16 v1, v0, 0x3e8

    sub-int v1, p0, v1

    .line 176346
    invoke-static {v0, p1, p2}, LX/13I;->b(I[CI)I

    move-result v0

    .line 176347
    invoke-static {v1, p1, v0}, LX/13I;->c(I[CI)I

    move-result v0

    goto :goto_0

    .line 176348
    :cond_4
    sget v0, LX/13I;->h:I

    if-lt p0, v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    .line 176349
    :goto_1
    if-eqz v1, :cond_5

    .line 176350
    sget v0, LX/13I;->h:I

    sub-int/2addr p0, v0

    .line 176351
    sget v0, LX/13I;->h:I

    if-lt p0, v0, :cond_7

    .line 176352
    sget v0, LX/13I;->h:I

    sub-int/2addr p0, v0

    .line 176353
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x32

    aput-char v2, p1, p2

    move p2, v0

    .line 176354
    :cond_5
    :goto_2
    div-int/lit16 v0, p0, 0x3e8

    .line 176355
    mul-int/lit16 v2, v0, 0x3e8

    sub-int v2, p0, v2

    .line 176356
    div-int/lit16 v3, v0, 0x3e8

    .line 176357
    mul-int/lit16 v4, v3, 0x3e8

    sub-int v4, v0, v4

    .line 176358
    if-eqz v1, :cond_8

    .line 176359
    invoke-static {v3, p1, p2}, LX/13I;->c(I[CI)I

    move-result v0

    .line 176360
    :goto_3
    invoke-static {v4, p1, v0}, LX/13I;->c(I[CI)I

    move-result v0

    .line 176361
    invoke-static {v2, p1, v0}, LX/13I;->c(I[CI)I

    move-result v0

    goto :goto_0

    .line 176362
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 176363
    :cond_7
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x31

    aput-char v2, p1, p2

    move p2, v0

    goto :goto_2

    .line 176364
    :cond_8
    invoke-static {v3, p1, p2}, LX/13I;->b(I[CI)I

    move-result v0

    goto :goto_3
.end method

.method public static a(J[BI)I
    .locals 8

    .prologue
    .line 176305
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_4

    .line 176306
    sget-wide v0, LX/13I;->k:J

    cmp-long v0, p0, v0

    if-lez v0, :cond_1

    .line 176307
    long-to-int v0, p0

    invoke-static {v0, p2, p3}, LX/13I;->a(I[BI)I

    move-result v1

    .line 176308
    :cond_0
    :goto_0
    return v1

    .line 176309
    :cond_1
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_2

    .line 176310
    sget-object v0, LX/13I;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 176311
    const/4 v0, 0x0

    move v1, p3

    :goto_1
    if-ge v0, v2, :cond_0

    .line 176312
    add-int/lit8 p3, v1, 0x1

    sget-object v3, LX/13I;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, p2, v1

    .line 176313
    add-int/lit8 v0, v0, 0x1

    move v1, p3

    goto :goto_1

    .line 176314
    :cond_2
    add-int/lit8 v0, p3, 0x1

    const/16 v1, 0x2d

    aput-byte v1, p2, p3

    .line 176315
    neg-long p0, p0

    move p3, v0

    .line 176316
    :cond_3
    invoke-static {p0, p1}, LX/13I;->b(J)I

    move-result v0

    add-int v1, p3, v0

    move v0, v1

    .line 176317
    :goto_2
    sget-wide v2, LX/13I;->l:J

    cmp-long v2, p0, v2

    if-lez v2, :cond_5

    .line 176318
    add-int/lit8 v0, v0, -0x3

    .line 176319
    sget-wide v2, LX/13I;->j:J

    div-long v2, p0, v2

    .line 176320
    sget-wide v4, LX/13I;->j:J

    mul-long/2addr v4, v2

    sub-long v4, p0, v4

    long-to-int v4, v4

    .line 176321
    invoke-static {v4, p2, v0}, LX/13I;->c(I[BI)I

    move-wide p0, v2

    .line 176322
    goto :goto_2

    .line 176323
    :cond_4
    sget-wide v0, LX/13I;->l:J

    cmp-long v0, p0, v0

    if-gtz v0, :cond_3

    .line 176324
    long-to-int v0, p0

    invoke-static {v0, p2, p3}, LX/13I;->a(I[BI)I

    move-result v1

    goto :goto_0

    .line 176325
    :cond_5
    long-to-int v2, p0

    move v6, v2

    move v2, v0

    move v0, v6

    .line 176326
    :goto_3
    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_6

    .line 176327
    add-int/lit8 v3, v2, -0x3

    .line 176328
    div-int/lit16 v2, v0, 0x3e8

    .line 176329
    mul-int/lit16 v4, v2, 0x3e8

    sub-int/2addr v0, v4

    .line 176330
    invoke-static {v0, p2, v3}, LX/13I;->c(I[BI)I

    move v0, v2

    move v2, v3

    .line 176331
    goto :goto_3

    .line 176332
    :cond_6
    invoke-static {v0, p2, p3}, LX/13I;->b(I[BI)I

    goto :goto_0
.end method

.method public static a(J[CI)I
    .locals 8

    .prologue
    .line 176278
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_3

    .line 176279
    sget-wide v0, LX/13I;->k:J

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    .line 176280
    long-to-int v0, p0

    invoke-static {v0, p2, p3}, LX/13I;->a(I[CI)I

    move-result v1

    .line 176281
    :goto_0
    return v1

    .line 176282
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 176283
    sget-object v0, LX/13I;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 176284
    sget-object v1, LX/13I;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0, p2, p3}, Ljava/lang/String;->getChars(II[CI)V

    .line 176285
    add-int v1, p3, v0

    goto :goto_0

    .line 176286
    :cond_1
    add-int/lit8 v0, p3, 0x1

    const/16 v1, 0x2d

    aput-char v1, p2, p3

    .line 176287
    neg-long p0, p0

    move p3, v0

    .line 176288
    :cond_2
    invoke-static {p0, p1}, LX/13I;->b(J)I

    move-result v0

    add-int v1, p3, v0

    move v0, v1

    .line 176289
    :goto_1
    sget-wide v2, LX/13I;->l:J

    cmp-long v2, p0, v2

    if-lez v2, :cond_4

    .line 176290
    add-int/lit8 v0, v0, -0x3

    .line 176291
    sget-wide v2, LX/13I;->j:J

    div-long v2, p0, v2

    .line 176292
    sget-wide v4, LX/13I;->j:J

    mul-long/2addr v4, v2

    sub-long v4, p0, v4

    long-to-int v4, v4

    .line 176293
    invoke-static {v4, p2, v0}, LX/13I;->c(I[CI)I

    move-wide p0, v2

    .line 176294
    goto :goto_1

    .line 176295
    :cond_3
    sget-wide v0, LX/13I;->l:J

    cmp-long v0, p0, v0

    if-gtz v0, :cond_2

    .line 176296
    long-to-int v0, p0

    invoke-static {v0, p2, p3}, LX/13I;->a(I[CI)I

    move-result v1

    goto :goto_0

    .line 176297
    :cond_4
    long-to-int v2, p0

    move v6, v2

    move v2, v0

    move v0, v6

    .line 176298
    :goto_2
    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_5

    .line 176299
    add-int/lit8 v3, v2, -0x3

    .line 176300
    div-int/lit16 v2, v0, 0x3e8

    .line 176301
    mul-int/lit16 v4, v2, 0x3e8

    sub-int/2addr v0, v4

    .line 176302
    invoke-static {v0, p2, v3}, LX/13I;->c(I[CI)I

    move v0, v2

    move v2, v3

    .line 176303
    goto :goto_2

    .line 176304
    :cond_5
    invoke-static {v0, p2, p3}, LX/13I;->b(I[CI)I

    goto :goto_0
.end method

.method public static a(D)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176277
    invoke-static {p0, p1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176269
    sget-object v0, LX/13I;->e:[Ljava/lang/String;

    array-length v0, v0

    if-ge p0, v0, :cond_1

    .line 176270
    if-ltz p0, :cond_0

    .line 176271
    sget-object v0, LX/13I;->e:[Ljava/lang/String;

    aget-object v0, v0, p0

    .line 176272
    :goto_0
    return-object v0

    .line 176273
    :cond_0
    neg-int v0, p0

    add-int/lit8 v0, v0, -0x1

    .line 176274
    sget-object v1, LX/13I;->f:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 176275
    sget-object v1, LX/13I;->f:[Ljava/lang/String;

    aget-object v0, v1, v0

    goto :goto_0

    .line 176276
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176266
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 176267
    long-to-int v0, p0

    invoke-static {v0}, LX/13I;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 176268
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(I[BI)I
    .locals 3

    .prologue
    .line 176257
    shl-int/lit8 v0, p0, 0x2

    .line 176258
    sget-object v1, LX/13I;->b:[C

    add-int/lit8 v2, v0, 0x1

    aget-char v1, v1, v0

    .line 176259
    if-eqz v1, :cond_0

    .line 176260
    add-int/lit8 v0, p2, 0x1

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    move p2, v0

    .line 176261
    :cond_0
    sget-object v0, LX/13I;->b:[C

    add-int/lit8 v1, v2, 0x1

    aget-char v2, v0, v2

    .line 176262
    if-eqz v2, :cond_1

    .line 176263
    add-int/lit8 v0, p2, 0x1

    int-to-byte v2, v2

    aput-byte v2, p1, p2

    move p2, v0

    .line 176264
    :cond_1
    add-int/lit8 v0, p2, 0x1

    sget-object v2, LX/13I;->b:[C

    aget-char v1, v2, v1

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    .line 176265
    return v0
.end method

.method private static b(I[CI)I
    .locals 3

    .prologue
    .line 176248
    shl-int/lit8 v0, p0, 0x2

    .line 176249
    sget-object v1, LX/13I;->b:[C

    add-int/lit8 v2, v0, 0x1

    aget-char v1, v1, v0

    .line 176250
    if-eqz v1, :cond_0

    .line 176251
    add-int/lit8 v0, p2, 0x1

    aput-char v1, p1, p2

    move p2, v0

    .line 176252
    :cond_0
    sget-object v0, LX/13I;->b:[C

    add-int/lit8 v1, v2, 0x1

    aget-char v2, v0, v2

    .line 176253
    if-eqz v2, :cond_1

    .line 176254
    add-int/lit8 v0, p2, 0x1

    aput-char v2, p1, p2

    move p2, v0

    .line 176255
    :cond_1
    add-int/lit8 v0, p2, 0x1

    sget-object v2, LX/13I;->b:[C

    aget-char v1, v2, v1

    aput-char v1, p1, p2

    .line 176256
    return v0
.end method

.method private static b(J)I
    .locals 6

    .prologue
    .line 176241
    const/16 v2, 0xa

    .line 176242
    sget-wide v0, LX/13I;->i:J

    .line 176243
    :goto_0
    cmp-long v3, p0, v0

    if-ltz v3, :cond_0

    .line 176244
    const/16 v3, 0x13

    if-eq v2, v3, :cond_0

    .line 176245
    add-int/lit8 v2, v2, 0x1

    .line 176246
    const/4 v3, 0x3

    shl-long v4, v0, v3

    const/4 v3, 0x1

    shl-long/2addr v0, v3

    add-long/2addr v0, v4

    goto :goto_0

    .line 176247
    :cond_0
    return v2
.end method

.method private static c(I[BI)I
    .locals 5

    .prologue
    .line 176236
    shl-int/lit8 v0, p0, 0x2

    .line 176237
    add-int/lit8 v1, p2, 0x1

    sget-object v2, LX/13I;->d:[B

    add-int/lit8 v3, v0, 0x1

    aget-byte v0, v2, v0

    aput-byte v0, p1, p2

    .line 176238
    add-int/lit8 v0, v1, 0x1

    sget-object v2, LX/13I;->d:[B

    add-int/lit8 v4, v3, 0x1

    aget-byte v2, v2, v3

    aput-byte v2, p1, v1

    .line 176239
    add-int/lit8 v1, v0, 0x1

    sget-object v2, LX/13I;->d:[B

    aget-byte v2, v2, v4

    aput-byte v2, p1, v0

    .line 176240
    return v1
.end method

.method private static c(I[CI)I
    .locals 5

    .prologue
    .line 176231
    shl-int/lit8 v0, p0, 0x2

    .line 176232
    add-int/lit8 v1, p2, 0x1

    sget-object v2, LX/13I;->c:[C

    add-int/lit8 v3, v0, 0x1

    aget-char v0, v2, v0

    aput-char v0, p1, p2

    .line 176233
    add-int/lit8 v0, v1, 0x1

    sget-object v2, LX/13I;->c:[C

    add-int/lit8 v4, v3, 0x1

    aget-char v2, v2, v3

    aput-char v2, p1, v1

    .line 176234
    add-int/lit8 v1, v0, 0x1

    sget-object v2, LX/13I;->c:[C

    aget-char v2, v2, v4

    aput-char v2, p1, v0

    .line 176235
    return v1
.end method
