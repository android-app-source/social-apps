.class public LX/1mO;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313755
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 313756
    return-void
.end method

.method public static a(LX/0Or;LX/0Or;)LX/1mC;
    .locals 1
    .param p0    # LX/0Or;
        .annotation runtime Lcom/facebook/video/settings/IsVideoDefaultAutoplaySettingOff;
        .end annotation
    .end param
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/video/settings/IsVideoDefaultAutoplaySettingWifiOnly;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/video/settings/DefaultAutoPlaySettingsFromServer;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/1mC;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 313757
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313758
    sget-object v0, LX/1mC;->OFF:LX/1mC;

    .line 313759
    :goto_0
    return-object v0

    .line 313760
    :cond_0
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313761
    sget-object v0, LX/1mC;->WIFI_ONLY:LX/1mC;

    goto :goto_0

    .line 313762
    :cond_1
    sget-object v0, LX/1mC;->ON:LX/1mC;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 313763
    return-void
.end method
