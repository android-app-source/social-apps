.class public abstract LX/0Vt;
.super Landroid/content/res/Resources;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0Vu;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Vu;)V
    .locals 3

    .prologue
    .line 74935
    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 74936
    iput-object p1, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    .line 74937
    iput-object p2, p0, LX/0Vt;->b:LX/0Vu;

    .line 74938
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 74913
    iget-object v0, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 74914
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    .line 74915
    iget v2, p0, LX/0Vt;->c:I

    if-eq v1, v2, :cond_0

    .line 74916
    iput v1, p0, LX/0Vt;->c:I

    .line 74917
    iget-object v1, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 74918
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 74932
    iget-object v0, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 74933
    iget-object v1, p0, LX/0Vt;->b:LX/0Vu;

    invoke-virtual {v1, p1, p2, v0}, LX/0Vu;->a(JLjava/util/Locale;)V

    .line 74934
    return-void
.end method

.method public a(Ljava/util/Locale;)V
    .locals 3

    .prologue
    .line 74926
    iget-object v0, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 74927
    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74928
    :goto_0
    return-void

    .line 74929
    :cond_0
    iput-object p1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 74930
    iget-object v1, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    iget-object v2, p0, LX/0Vt;->a:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 74931
    invoke-virtual {p0}, LX/0Vt;->a()V

    goto :goto_0
.end method

.method public abstract b()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public getQuantityText(II)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74924
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74925
    invoke-super {p0, p1, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getText(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74922
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74923
    invoke-super {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 74921
    invoke-super {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextArray(I)[Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 74919
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, LX/0Vt;->a(J)V

    .line 74920
    invoke-super {p0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
