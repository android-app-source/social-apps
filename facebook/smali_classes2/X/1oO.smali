.class public final LX/1oO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/nio/ByteBuffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/25f;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/facebook/flatbuffers/Extra;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/15i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 319074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319075
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1oO;->e:Ljava/lang/Object;

    .line 319076
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 319197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319198
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1oO;->e:Ljava/lang/Object;

    .line 319199
    invoke-direct {p0, p1}, LX/1oO;->a(Ljava/nio/ByteBuffer;)V

    .line 319200
    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 319176
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    .line 319177
    iput-object v1, p0, LX/1oO;->d:LX/15i;

    .line 319178
    iget-object v0, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_3

    .line 319179
    iget-object v0, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move v0, v2

    .line 319180
    :goto_1
    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    .line 319181
    iget-object v1, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const-string v3, "EXTR"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    if-eq v1, v3, :cond_1

    .line 319182
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Extra buffer header is invalid"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    .line 319183
    goto :goto_0

    .line 319184
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 319185
    :cond_2
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    .line 319186
    iget-object v1, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    .line 319187
    add-int/lit8 v0, v0, 0x4

    move v1, v0

    move v0, v2

    .line 319188
    :goto_2
    if-ge v0, v3, :cond_4

    .line 319189
    iget-object v2, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    .line 319190
    add-int/lit8 v1, v1, 0x4

    .line 319191
    new-instance v4, LX/25f;

    iget-object v5, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    invoke-direct {v4, v5, v1}, LX/25f;-><init>(Ljava/nio/ByteBuffer;I)V

    .line 319192
    add-int/lit8 v1, v1, 0x10

    .line 319193
    iget-object v5, p0, LX/1oO;->b:Landroid/util/SparseArray;

    invoke-virtual {v5, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319194
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 319195
    :cond_3
    iput-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    .line 319196
    :cond_4
    return-void
.end method

.method private static d(LX/1oO;)LX/15i;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 319173
    iget-object v0, p0, LX/1oO;->d:LX/15i;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 319174
    new-instance v0, LX/15i;

    iget-object v1, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    iput-object v0, p0, LX/1oO;->d:LX/15i;

    .line 319175
    :cond_0
    iget-object v0, p0, LX/1oO;->d:LX/15i;

    return-object v0
.end method


# virtual methods
.method public final a(ILcom/facebook/flatbuffers/Flattenable;Ljava/lang/Class;)Lcom/facebook/graphql/model/extras/BaseExtra;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Extra;",
            ">(I",
            "Lcom/facebook/flatbuffers/Flattenable;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 319150
    iget-object v4, p0, LX/1oO;->e:Ljava/lang/Object;

    monitor-enter v4

    .line 319151
    :try_start_0
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    .line 319152
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    .line 319153
    if-ltz v1, :cond_1

    .line 319154
    iget-object v2, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/extras/BaseExtra;

    monitor-exit v4

    .line 319155
    :goto_0
    return-object v1

    .line 319156
    :cond_0
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319157
    :cond_1
    :try_start_1
    invoke-virtual {p3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/extras/BaseExtra;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319158
    :try_start_2
    iget-object v2, p0, LX/1oO;->b:Landroid/util/SparseArray;

    if-eqz v2, :cond_3

    .line 319159
    iget-object v2, p0, LX/1oO;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/25f;

    .line 319160
    if-eqz v2, :cond_3

    iget v3, v2, LX/25f;->c:I

    if-eqz v3, :cond_3

    .line 319161
    invoke-static {p0}, LX/1oO;->d(LX/1oO;)LX/15i;

    move-result-object v3

    if-nez v3, :cond_2

    .line 319162
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "mByteBuffer should not be null."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 319163
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 319164
    :catch_0
    move-exception v1

    .line 319165
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Not able to create object"

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 319166
    :catch_1
    move-exception v1

    .line 319167
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Access to constructor denied"

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 319168
    :cond_2
    instance-of v3, v1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v3, :cond_4

    .line 319169
    move-object v0, v1

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    invoke-static {p0}, LX/1oO;->d(LX/1oO;)LX/15i;

    move-result-object v5

    iget v2, v2, LX/25f;->c:I

    invoke-interface {v3, v5, v2}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/15i;I)V

    .line 319170
    :cond_3
    iget-object v2, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319171
    monitor-exit v4

    goto :goto_0

    .line 319172
    :cond_4
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "object should implement Flattenable to save persistent states."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a()Ljava/nio/ByteBuffer;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 319147
    iget-object v1, p0, LX/1oO;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 319148
    :try_start_0
    iget-object v0, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    monitor-exit v1

    return-object v0

    .line 319149
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319133
    iget-object v4, p0, LX/1oO;->e:Ljava/lang/Object;

    monitor-enter v4

    .line 319134
    :try_start_0
    iget-object v0, p0, LX/1oO;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 319135
    :cond_0
    monitor-exit v4

    move v0, v2

    .line 319136
    :goto_0
    return v0

    :cond_1
    move v3, v2

    .line 319137
    :goto_1
    iget-object v0, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 319138
    iget-object v0, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/extras/BaseExtra;

    .line 319139
    if-nez v0, :cond_2

    .line 319140
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 319141
    :cond_2
    iget-boolean v5, v0, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z

    move v5, v5

    .line 319142
    if-eqz v5, :cond_3

    instance-of v0, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v0, :cond_3

    .line 319143
    monitor-exit v4

    move v0, v1

    goto :goto_0

    .line 319144
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 319145
    :cond_4
    monitor-exit v4

    move v0, v2

    goto :goto_0

    .line 319146
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 319077
    iget-object v5, p0, LX/1oO;->e:Ljava/lang/Object;

    monitor-enter v5

    .line 319078
    :try_start_0
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 319079
    :cond_0
    monitor-exit v5

    move v1, v3

    .line 319080
    :goto_0
    return v1

    .line 319081
    :cond_1
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 319082
    new-instance v7, LX/186;

    const/16 v1, 0x100

    invoke-direct {v7, v1}, LX/186;-><init>(I)V

    move v4, v3

    .line 319083
    :goto_1
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    .line 319084
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    .line 319085
    iget-object v1, p0, LX/1oO;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/extras/BaseExtra;

    .line 319086
    if-nez v1, :cond_3

    .line 319087
    new-instance v1, LX/25f;

    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v1, v2, v9, v10, v11}, LX/25f;-><init>(IIII)V

    invoke-virtual {v6, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319088
    :cond_2
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 319089
    :cond_3
    iget-boolean v0, v1, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z

    move v2, v0

    .line 319090
    if-eqz v2, :cond_2

    instance-of v2, v1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v2, :cond_2

    .line 319091
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, v7, LX/186;->h:[I

    .line 319092
    const/4 v0, 0x0

    iput v0, v7, LX/186;->i:I

    .line 319093
    invoke-virtual {v7}, LX/186;->b()I

    move-result v9

    .line 319094
    move-object v0, v1

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v0

    invoke-virtual {v7, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 319095
    invoke-virtual {v7}, LX/186;->b()I

    move-result v10

    sub-int v9, v10, v9

    .line 319096
    new-instance v10, LX/25f;

    invoke-virtual {v7}, LX/186;->b()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct {v10, v11, v9, v2, v12}, LX/25f;-><init>(IIII)V

    invoke-virtual {v6, v8, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319097
    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/facebook/graphql/model/extras/BaseExtra;->a:Z

    .line 319098
    goto :goto_2

    .line 319099
    :catchall_0
    move-exception v1

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 319100
    :cond_4
    :try_start_1
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_5

    .line 319101
    monitor-exit v5

    move v1, v3

    goto :goto_0

    .line 319102
    :cond_5
    iget-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    if-eqz v1, :cond_9

    .line 319103
    iget-object v1, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_6

    .line 319104
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "mByteBuffer for ExtraBuffer should not be null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move v2, v3

    .line 319105
    :goto_3
    iget-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v2, v1, :cond_9

    .line 319106
    iget-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 319107
    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-gez v1, :cond_7

    .line 319108
    iget-object v1, p0, LX/1oO;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/25f;

    .line 319109
    iget v8, v1, LX/25f;->c:I

    if-nez v8, :cond_8

    .line 319110
    invoke-virtual {v6, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319111
    :cond_7
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 319112
    :cond_8
    invoke-virtual {v1}, LX/25f;->a()I

    move-result v8

    iget v9, v1, LX/25f;->b:I

    invoke-virtual {v7, v8, v9}, LX/186;->a(II)V

    .line 319113
    iget-object v8, p0, LX/1oO;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    iget v9, v1, LX/25f;->a:I

    iget v10, v1, LX/25f;->b:I

    invoke-virtual {v7, v8, v9, v10}, LX/186;->a([BII)V

    .line 319114
    invoke-virtual {v7}, LX/186;->b()I

    move-result v8

    .line 319115
    new-instance v9, LX/25f;

    iget v10, v1, LX/25f;->b:I

    iget v11, v1, LX/25f;->c:I

    iget v12, v1, LX/25f;->a:I

    sub-int/2addr v11, v12

    sub-int v11, v8, v11

    iget v1, v1, LX/25f;->d:I

    invoke-direct {v9, v8, v10, v11, v1}, LX/25f;-><init>(IIII)V

    invoke-virtual {v6, v4, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_4

    .line 319116
    :cond_9
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0x4

    .line 319117
    const/4 v2, 0x4

    const/4 v4, 0x0

    invoke-virtual {v7, v2, v4}, LX/186;->a(II)V

    .line 319118
    iget v0, v7, LX/186;->d:I

    move v2, v0

    .line 319119
    invoke-virtual {v7, v2, v1}, LX/186;->a(II)V

    .line 319120
    :goto_5
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v3, v1, :cond_a

    .line 319121
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 319122
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/25f;

    .line 319123
    invoke-virtual {v1, v7}, LX/25f;->a(LX/186;)V

    .line 319124
    invoke-virtual {v7, v2}, LX/186;->a(I)V

    .line 319125
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 319126
    :cond_a
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-virtual {v7, v1}, LX/186;->a(I)V

    .line 319127
    const/4 v1, 0x3

    :goto_6
    if-ltz v1, :cond_b

    .line 319128
    const-string v2, "EXTR"

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v7, v2}, LX/186;->a(B)V

    .line 319129
    add-int/lit8 v1, v1, -0x1

    goto :goto_6

    .line 319130
    :cond_b
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 319131
    invoke-direct {p0, v1}, LX/1oO;->a(Ljava/nio/ByteBuffer;)V

    .line 319132
    const/4 v1, 0x1

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
