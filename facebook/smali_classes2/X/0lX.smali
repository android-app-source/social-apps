.class public final enum LX/0lX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0lX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0lX;

.field public static final enum ANY:LX/0lX;

.field public static final enum DEFAULT:LX/0lX;

.field public static final enum NONE:LX/0lX;

.field public static final enum NON_PRIVATE:LX/0lX;

.field public static final enum PROTECTED_AND_PUBLIC:LX/0lX;

.field public static final enum PUBLIC_ONLY:LX/0lX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 129970
    new-instance v0, LX/0lX;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v3}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->ANY:LX/0lX;

    .line 129971
    new-instance v0, LX/0lX;

    const-string v1, "NON_PRIVATE"

    invoke-direct {v0, v1, v4}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->NON_PRIVATE:LX/0lX;

    .line 129972
    new-instance v0, LX/0lX;

    const-string v1, "PROTECTED_AND_PUBLIC"

    invoke-direct {v0, v1, v5}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->PROTECTED_AND_PUBLIC:LX/0lX;

    .line 129973
    new-instance v0, LX/0lX;

    const-string v1, "PUBLIC_ONLY"

    invoke-direct {v0, v1, v6}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->PUBLIC_ONLY:LX/0lX;

    .line 129974
    new-instance v0, LX/0lX;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v7}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->NONE:LX/0lX;

    .line 129975
    new-instance v0, LX/0lX;

    const-string v1, "DEFAULT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0lX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0lX;->DEFAULT:LX/0lX;

    .line 129976
    const/4 v0, 0x6

    new-array v0, v0, [LX/0lX;

    sget-object v1, LX/0lX;->ANY:LX/0lX;

    aput-object v1, v0, v3

    sget-object v1, LX/0lX;->NON_PRIVATE:LX/0lX;

    aput-object v1, v0, v4

    sget-object v1, LX/0lX;->PROTECTED_AND_PUBLIC:LX/0lX;

    aput-object v1, v0, v5

    sget-object v1, LX/0lX;->PUBLIC_ONLY:LX/0lX;

    aput-object v1, v0, v6

    sget-object v1, LX/0lX;->NONE:LX/0lX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0lX;->DEFAULT:LX/0lX;

    aput-object v2, v0, v1

    sput-object v0, LX/0lX;->$VALUES:[LX/0lX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 129961
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0lX;
    .locals 1

    .prologue
    .line 129969
    const-class v0, LX/0lX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0lX;

    return-object v0
.end method

.method public static values()[LX/0lX;
    .locals 1

    .prologue
    .line 129968
    sget-object v0, LX/0lX;->$VALUES:[LX/0lX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0lX;

    return-object v0
.end method


# virtual methods
.method public final isVisible(Ljava/lang/reflect/Member;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129962
    sget-object v2, LX/2Ao;->a:[I

    invoke-virtual {p0}, LX/0lX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 129963
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 129964
    goto :goto_0

    .line 129965
    :pswitch_2
    invoke-interface {p1}, Ljava/lang/reflect/Member;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 129966
    :pswitch_3
    invoke-interface {p1}, Ljava/lang/reflect/Member;->getModifiers()I

    move-result v1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isProtected(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129967
    :pswitch_4
    invoke-interface {p1}, Ljava/lang/reflect/Member;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
