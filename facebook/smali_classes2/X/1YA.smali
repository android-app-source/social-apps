.class public final LX/1YA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# static fields
.field private static final a:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 273342
    const/4 v0, 0x0

    .line 273343
    const/16 v1, 0x80

    new-array v2, v1, [B

    .line 273344
    const/4 v1, -0x1

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([BB)V

    move v1, v0

    .line 273345
    :goto_0
    const/16 v3, 0x9

    if-gt v1, v3, :cond_0

    .line 273346
    add-int/lit8 v3, v1, 0x30

    int-to-byte v4, v1

    aput-byte v4, v2, v3

    .line 273347
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273348
    :cond_0
    :goto_1
    const/16 v1, 0x1a

    if-gt v0, v1, :cond_1

    .line 273349
    add-int/lit8 v1, v0, 0x41

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 273350
    add-int/lit8 v1, v0, 0x61

    add-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 273351
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 273352
    :cond_1
    move-object v0, v2

    .line 273353
    sput-object v0, LX/1YA;->a:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 273341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(C)I
    .locals 1

    .prologue
    .line 273340
    const/16 v0, 0x80

    if-ge p0, v0, :cond_0

    sget-object v0, LX/1YA;->a:[B

    aget-byte v0, v0, p0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(JJ)I
    .locals 2

    .prologue
    .line 273354
    cmp-long v0, p0, p2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, p0, p2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/Long;
    .locals 12
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation build Ljavax/annotation/CheckForNull;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const-wide/high16 v10, -0x8000000000000000L

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 273312
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v4

    .line 273313
    :goto_0
    return-object v0

    .line 273314
    :cond_0
    const/4 v0, 0x2

    if-lt p1, v0, :cond_1

    const/16 v0, 0x24

    if-le p1, v0, :cond_2

    .line 273315
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "radix must be between MIN_RADIX and MAX_RADIX but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273316
    :cond_2
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_3

    move v5, v1

    .line 273317
    :goto_1
    if-eqz v5, :cond_4

    move v0, v1

    .line 273318
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_5

    move-object v0, v4

    .line 273319
    goto :goto_0

    :cond_3
    move v5, v2

    .line 273320
    goto :goto_1

    :cond_4
    move v0, v2

    .line 273321
    goto :goto_2

    .line 273322
    :cond_5
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, LX/1YA;->a(C)I

    move-result v0

    .line 273323
    if-ltz v0, :cond_6

    if-lt v0, p1, :cond_7

    :cond_6
    move-object v0, v4

    .line 273324
    goto :goto_0

    .line 273325
    :cond_7
    neg-int v0, v0

    int-to-long v0, v0

    .line 273326
    int-to-long v6, p1

    div-long v6, v10, v6

    .line 273327
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_b

    .line 273328
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, LX/1YA;->a(C)I

    move-result v2

    .line 273329
    if-ltz v2, :cond_8

    if-ge v2, p1, :cond_8

    cmp-long v8, v0, v6

    if-gez v8, :cond_9

    :cond_8
    move-object v0, v4

    .line 273330
    goto :goto_0

    .line 273331
    :cond_9
    int-to-long v8, p1

    mul-long/2addr v0, v8

    .line 273332
    int-to-long v8, v2

    add-long/2addr v8, v10

    cmp-long v8, v0, v8

    if-gez v8, :cond_a

    move-object v0, v4

    .line 273333
    goto :goto_0

    .line 273334
    :cond_a
    int-to-long v8, v2

    sub-long/2addr v0, v8

    move v2, v3

    goto :goto_3

    .line 273335
    :cond_b
    if-eqz v5, :cond_c

    .line 273336
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 273337
    :cond_c
    cmp-long v2, v0, v10

    if-nez v2, :cond_d

    move-object v0, v4

    .line 273338
    goto :goto_0

    .line 273339
    :cond_d
    neg-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static varargs a([J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273309
    array-length v0, p0

    if-nez v0, :cond_0

    .line 273310
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 273311
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/52A;

    invoke-direct {v0, p0}, LX/52A;-><init>([J)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/Number;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 273290
    instance-of v0, p0, LX/52A;

    if-eqz v0, :cond_0

    .line 273291
    check-cast p0, LX/52A;

    .line 273292
    invoke-virtual {p0}, LX/52A;->size()I

    move-result v0

    .line 273293
    new-array v1, v0, [J

    .line 273294
    iget-object v2, p0, LX/52A;->array:[J

    iget v3, p0, LX/52A;->start:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273295
    move-object v0, v1

    .line 273296
    :goto_0
    return-object v0

    .line 273297
    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 273298
    array-length v4, v3

    .line 273299
    new-array v1, v4, [J

    .line 273300
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 273301
    aget-object v0, v3, v2

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    aput-wide v6, v1, v2

    .line 273302
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 273303
    goto :goto_0
.end method

.method public static c([JJII)I
    .locals 5

    .prologue
    .line 273304
    move v0, p3

    :goto_0
    if-ge v0, p4, :cond_1

    .line 273305
    aget-wide v2, p0, v0

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 273306
    :goto_1
    return v0

    .line 273307
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273308
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
