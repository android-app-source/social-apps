.class public abstract enum LX/0Qo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Qo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Qo;

.field public static final enum STRONG:LX/0Qo;

.field public static final enum STRONG_ACCESS:LX/0Qo;

.field public static final enum STRONG_ACCESS_WRITE:LX/0Qo;

.field public static final enum STRONG_WRITE:LX/0Qo;

.field public static final enum WEAK:LX/0Qo;

.field public static final enum WEAK_ACCESS:LX/0Qo;

.field public static final enum WEAK_ACCESS_WRITE:LX/0Qo;

.field public static final enum WEAK_WRITE:LX/0Qo;

.field public static final factories:[LX/0Qo;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58506
    new-instance v0, LX/0Qp;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, LX/0Qp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->STRONG:LX/0Qo;

    .line 58507
    new-instance v0, LX/0Qq;

    const-string v1, "STRONG_ACCESS"

    invoke-direct {v0, v1, v4}, LX/0Qq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->STRONG_ACCESS:LX/0Qo;

    .line 58508
    new-instance v0, LX/0Qr;

    const-string v1, "STRONG_WRITE"

    invoke-direct {v0, v1, v5}, LX/0Qr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->STRONG_WRITE:LX/0Qo;

    .line 58509
    new-instance v0, LX/0Qs;

    const-string v1, "STRONG_ACCESS_WRITE"

    invoke-direct {v0, v1, v6}, LX/0Qs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->STRONG_ACCESS_WRITE:LX/0Qo;

    .line 58510
    new-instance v0, LX/0Qt;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, LX/0Qt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->WEAK:LX/0Qo;

    .line 58511
    new-instance v0, LX/0Qu;

    const-string v1, "WEAK_ACCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0Qu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->WEAK_ACCESS:LX/0Qo;

    .line 58512
    new-instance v0, LX/0Qv;

    const-string v1, "WEAK_WRITE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/0Qv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->WEAK_WRITE:LX/0Qo;

    .line 58513
    new-instance v0, LX/0Qw;

    const-string v1, "WEAK_ACCESS_WRITE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/0Qw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Qo;->WEAK_ACCESS_WRITE:LX/0Qo;

    .line 58514
    const/16 v0, 0x8

    new-array v0, v0, [LX/0Qo;

    sget-object v1, LX/0Qo;->STRONG:LX/0Qo;

    aput-object v1, v0, v3

    sget-object v1, LX/0Qo;->STRONG_ACCESS:LX/0Qo;

    aput-object v1, v0, v4

    sget-object v1, LX/0Qo;->STRONG_WRITE:LX/0Qo;

    aput-object v1, v0, v5

    sget-object v1, LX/0Qo;->STRONG_ACCESS_WRITE:LX/0Qo;

    aput-object v1, v0, v6

    sget-object v1, LX/0Qo;->WEAK:LX/0Qo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0Qo;->WEAK_ACCESS:LX/0Qo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0Qo;->WEAK_WRITE:LX/0Qo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0Qo;->WEAK_ACCESS_WRITE:LX/0Qo;

    aput-object v2, v0, v1

    sput-object v0, LX/0Qo;->$VALUES:[LX/0Qo;

    .line 58515
    const/16 v0, 0x8

    new-array v0, v0, [LX/0Qo;

    sget-object v1, LX/0Qo;->STRONG:LX/0Qo;

    aput-object v1, v0, v3

    sget-object v1, LX/0Qo;->STRONG_ACCESS:LX/0Qo;

    aput-object v1, v0, v4

    sget-object v1, LX/0Qo;->STRONG_WRITE:LX/0Qo;

    aput-object v1, v0, v5

    sget-object v1, LX/0Qo;->STRONG_ACCESS_WRITE:LX/0Qo;

    aput-object v1, v0, v6

    sget-object v1, LX/0Qo;->WEAK:LX/0Qo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0Qo;->WEAK_ACCESS:LX/0Qo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0Qo;->WEAK_WRITE:LX/0Qo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0Qo;->WEAK_ACCESS_WRITE:LX/0Qo;

    aput-object v2, v0, v1

    sput-object v0, LX/0Qo;->factories:[LX/0Qo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58505
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFactory(LX/0QX;ZZ)LX/0Qo;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58502
    sget-object v1, LX/0QX;->WEAK:LX/0QX;

    if-ne p0, v1, :cond_1

    const/4 v1, 0x4

    move v2, v1

    :goto_0
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v1, v2

    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 58503
    sget-object v1, LX/0Qo;->factories:[LX/0Qo;

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v2, v0

    .line 58504
    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Qo;
    .locals 1

    .prologue
    .line 58501
    const-class v0, LX/0Qo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Qo;

    return-object v0
.end method

.method public static values()[LX/0Qo;
    .locals 1

    .prologue
    .line 58516
    sget-object v0, LX/0Qo;->$VALUES:[LX/0Qo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Qo;

    return-object v0
.end method


# virtual methods
.method public copyAccessEntry(LX/0R1;LX/0R1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58496
    invoke-interface {p1}, LX/0R1;->getAccessTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LX/0R1;->setAccessTime(J)V

    .line 58497
    invoke-interface {p1}, LX/0R1;->getPreviousInAccessQueue()LX/0R1;

    move-result-object v0

    invoke-static {v0, p2}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 58498
    invoke-interface {p1}, LX/0R1;->getNextInAccessQueue()LX/0R1;

    move-result-object v0

    invoke-static {p2, v0}, LX/0Qd;->a(LX/0R1;LX/0R1;)V

    .line 58499
    invoke-static {p1}, LX/0Qd;->b(LX/0R1;)V

    .line 58500
    return-void
.end method

.method public copyEntry(LX/0Qx;LX/0R1;LX/0R1;)LX/0R1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 58490
    invoke-interface {p2}, LX/0R1;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, LX/0R1;->getHash()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, LX/0Qo;->newEntry(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;

    move-result-object v0

    return-object v0
.end method

.method public copyWriteEntry(LX/0R1;LX/0R1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0R1",
            "<TK;TV;>;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 58491
    invoke-interface {p1}, LX/0R1;->getWriteTime()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, LX/0R1;->setWriteTime(J)V

    .line 58492
    invoke-interface {p1}, LX/0R1;->getPreviousInWriteQueue()LX/0R1;

    move-result-object v0

    invoke-static {v0, p2}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 58493
    invoke-interface {p1}, LX/0R1;->getNextInWriteQueue()LX/0R1;

    move-result-object v0

    invoke-static {p2, v0}, LX/0Qd;->b(LX/0R1;LX/0R1;)V

    .line 58494
    invoke-static {p1}, LX/0Qd;->c(LX/0R1;)V

    .line 58495
    return-void
.end method

.method public abstract newEntry(LX/0Qx;Ljava/lang/Object;ILX/0R1;)LX/0R1;
    .param p4    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Qx",
            "<TK;TV;>;TK;I",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end method
