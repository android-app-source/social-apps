.class public final LX/1Bw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Bx;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1Bt;

.field public final synthetic c:LX/1Be;


# direct methods
.method public constructor <init>(LX/1Be;Ljava/lang/String;LX/1Bt;)V
    .locals 0

    .prologue
    .line 214538
    iput-object p1, p0, LX/1Bw;->c:LX/1Be;

    iput-object p2, p0, LX/1Bw;->a:Ljava/lang/String;

    iput-object p3, p0, LX/1Bw;->b:LX/1Bt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 214539
    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 214540
    iget-object v8, p0, LX/1Bw;->a:Ljava/lang/String;

    .line 214541
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object v12, v11

    .line 214542
    :goto_0
    const/16 v0, 0x1e

    if-gt v4, v0, :cond_0

    if-eqz v8, :cond_0

    :try_start_0
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, LX/1Be;->a$redex0(LX/1Be;Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 214543
    :cond_0
    iget-object v0, p0, LX/1Bw;->b:LX/1Bt;

    iget-boolean v0, v0, LX/1Bt;->c:Z

    if-eqz v0, :cond_1

    .line 214544
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v1, p0, LX/1Bw;->a:Ljava/lang/String;

    sget-object v5, LX/1Bu;->ERROR_RESPONSE:LX/1Bu;

    invoke-virtual {v0, v1, v5}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 214545
    :cond_1
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7hw;

    iget-object v1, p0, LX/1Bw;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    const/4 v5, 0x0

    iget-object v6, p0, LX/1Bw;->b:LX/1Bt;

    iget v6, v6, LX/1Bt;->b:I

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, LX/7hw;->a(Ljava/lang/String;JIIILX/7i0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 214546
    if-eqz v12, :cond_2

    .line 214547
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v12}, LX/1Bp;->b(LX/3nA;)V

    :cond_2
    move-object v0, v11

    :goto_1
    return-object v0

    .line 214548
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1Bw;->b:LX/1Bt;

    invoke-virtual {v0, v8}, LX/1Bt;->a(Ljava/lang/String;)Z

    move-result v0

    .line 214549
    iget-object v1, p0, LX/1Bw;->c:LX/1Be;

    const/4 v5, 0x0

    invoke-static {v1, v8, v5, v0}, LX/1Be;->a$redex0(LX/1Be;Ljava/lang/String;ZZ)Ljava/util/Map;

    move-result-object v10

    .line 214550
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v5, v0, LX/1Be;->y:LX/1Bp;

    iget-object v6, p0, LX/1Bw;->b:LX/1Bt;

    iget-object v7, p0, LX/1Bw;->a:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {v5 .. v10}, LX/1Bp;->a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)LX/3nA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 214551
    :try_start_2
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v10}, LX/1Bp;->a(LX/3nA;)LX/1Bx;

    move-result-object v9

    .line 214552
    if-eqz v9, :cond_6

    iget-object v0, v9, LX/1Bx;->a:LX/1By;

    if-eqz v0, :cond_6

    .line 214553
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7hw;

    iget-object v1, p0, LX/1Bw;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    iget-object v5, v9, LX/1Bx;->a:LX/1By;

    iget-object v6, v9, LX/1Bx;->c:LX/7i0;

    iget-object v7, v9, LX/1Bx;->g:Ljava/lang/String;

    iget-object v8, v9, LX/1Bx;->h:Ljava/lang/String;

    .line 214554
    new-instance v11, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v12, "inapp_browser_prefetch_timing"

    invoke-direct {v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 214555
    const-string v12, "url"

    invoke-virtual {v11, v12, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214556
    const-string v12, "duration"

    invoke-virtual {v11, v12, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214557
    const-string v12, "redirects"

    invoke-virtual {v11, v12, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214558
    const-string v12, "status"

    const/16 v13, 0xc8

    invoke-virtual {v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214559
    const-string v12, "file_size"

    .line 214560
    iget v13, v5, LX/1By;->b:I

    move v13, v13

    .line 214561
    invoke-virtual {v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214562
    const-string v12, "link_context"

    .line 214563
    iget v13, v5, LX/1By;->f:I

    move v13, v13

    .line 214564
    invoke-virtual {v11, v12, v13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214565
    const-string v12, "rsp_body_size"

    invoke-virtual {v11, v12, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214566
    const-string v12, "rsp_body_cmp_size"

    invoke-virtual {v11, v12, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 214567
    if-eqz v6, :cond_4

    .line 214568
    invoke-static {v11, v6}, LX/7hw;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7i0;)V

    .line 214569
    :cond_4
    iget-object v12, v0, LX/7hw;->a:LX/0Zb;

    invoke-interface {v12, v11}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214570
    if-eqz v10, :cond_5

    .line 214571
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v10}, LX/1Bp;->b(LX/3nA;)V

    :cond_5
    move-object v0, v9

    goto/16 :goto_1

    .line 214572
    :cond_6
    if-eqz v9, :cond_7

    :try_start_3
    iget-object v0, v9, LX/1Bx;->d:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 214573
    add-int/lit8 v4, v4, 0x1

    .line 214574
    iget-object v0, v9, LX/1Bx;->d:Ljava/lang/String;

    invoke-static {v8, v0}, LX/1Be;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v12, v10

    goto/16 :goto_0

    .line 214575
    :cond_7
    if-eqz v9, :cond_a

    iget-object v0, v9, LX/1Bx;->f:LX/1Bu;

    if-eqz v0, :cond_a

    .line 214576
    iget-object v0, p0, LX/1Bw;->b:LX/1Bt;

    iget-boolean v0, v0, LX/1Bt;->c:Z

    if-eqz v0, :cond_8

    .line 214577
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v1, p0, LX/1Bw;->a:Ljava/lang/String;

    iget-object v5, v9, LX/1Bx;->f:LX/1Bu;

    invoke-virtual {v0, v1, v5}, LX/1Be;->a(Ljava/lang/String;LX/1Bu;)V

    .line 214578
    :cond_8
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7hw;

    iget-object v1, p0, LX/1Bw;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    iget v5, v9, LX/1Bx;->e:I

    iget-object v6, p0, LX/1Bw;->b:LX/1Bt;

    iget v6, v6, LX/1Bt;->b:I

    iget-object v7, v9, LX/1Bx;->c:LX/7i0;

    invoke-virtual/range {v0 .. v7}, LX/7hw;->a(Ljava/lang/String;JIIILX/7i0;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 214579
    if-eqz v10, :cond_9

    .line 214580
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v10}, LX/1Bp;->b(LX/3nA;)V

    :cond_9
    move-object v0, v9

    goto/16 :goto_1

    .line 214581
    :cond_a
    if-eqz v10, :cond_b

    .line 214582
    iget-object v0, p0, LX/1Bw;->c:LX/1Be;

    iget-object v0, v0, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v0, v10}, LX/1Bp;->b(LX/3nA;)V

    :cond_b
    move-object v0, v11

    goto/16 :goto_1

    .line 214583
    :catchall_0
    move-exception v0

    move-object v1, v10

    :goto_2
    if-eqz v1, :cond_c

    .line 214584
    iget-object v2, p0, LX/1Bw;->c:LX/1Be;

    iget-object v2, v2, LX/1Be;->y:LX/1Bp;

    invoke-virtual {v2, v1}, LX/1Bp;->b(LX/3nA;)V

    :cond_c
    throw v0

    .line 214585
    :catchall_1
    move-exception v0

    move-object v1, v12

    goto :goto_2
.end method
