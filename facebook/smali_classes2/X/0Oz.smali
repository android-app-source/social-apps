.class public final enum LX/0Oz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Oz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Oz;

.field public static final enum CONTACTS_CONTENT:LX/0Oz;

.field public static final enum CONTACT_ID:LX/0Oz;

.field public static final enum FRIENDS_BIRTHDAYS:LX/0Oz;

.field public static final enum FRIENDS_CONTENT:LX/0Oz;

.field public static final enum FRIENDS_PREFIX_SEARCH:LX/0Oz;

.field public static final enum FRIEND_UID:LX/0Oz;

.field public static final enum PAGES_CONTENT:LX/0Oz;

.field public static final enum PAGES_SEARCH:LX/0Oz;

.field public static final enum PAGE_ID:LX/0Oz;


# instance fields
.field public final category:Ljava/lang/String;

.field public final uriMatcherSuffix:Ljava/lang/String;

.field public final uriSuffix:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 54773
    new-instance v0, LX/0Oz;

    const-string v1, "CONTACTS_CONTENT"

    const-string v3, "all_contacts"

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0Oz;->CONTACTS_CONTENT:LX/0Oz;

    .line 54774
    new-instance v3, LX/0Oz;

    const-string v4, "CONTACT_ID"

    const-string v6, "all_contacts"

    const-string v7, "/id"

    const-string v8, "/#"

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->CONTACT_ID:LX/0Oz;

    .line 54775
    new-instance v3, LX/0Oz;

    const-string v4, "FRIENDS_CONTENT"

    const-string v6, "friends"

    const-string v7, ""

    const-string v8, ""

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->FRIENDS_CONTENT:LX/0Oz;

    .line 54776
    new-instance v3, LX/0Oz;

    const-string v4, "FRIEND_UID"

    const-string v6, "friends"

    const-string v7, "/uid"

    const-string v8, "/#"

    move v5, v11

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->FRIEND_UID:LX/0Oz;

    .line 54777
    new-instance v3, LX/0Oz;

    const-string v4, "FRIENDS_PREFIX_SEARCH"

    const-string v6, "friends"

    const-string v7, "/search"

    const-string v8, "/*"

    move v5, v12

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->FRIENDS_PREFIX_SEARCH:LX/0Oz;

    .line 54778
    new-instance v3, LX/0Oz;

    const-string v4, "FRIENDS_BIRTHDAYS"

    const/4 v5, 0x5

    const-string v6, "friends"

    const-string v7, "/birthdays"

    const-string v8, ""

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->FRIENDS_BIRTHDAYS:LX/0Oz;

    .line 54779
    new-instance v3, LX/0Oz;

    const-string v4, "PAGES_CONTENT"

    const/4 v5, 0x6

    const-string v6, "pages"

    const-string v7, ""

    const-string v8, ""

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->PAGES_CONTENT:LX/0Oz;

    .line 54780
    new-instance v3, LX/0Oz;

    const-string v4, "PAGE_ID"

    const/4 v5, 0x7

    const-string v6, "pages"

    const-string v7, "/id"

    const-string v8, "/#"

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->PAGE_ID:LX/0Oz;

    .line 54781
    new-instance v3, LX/0Oz;

    const-string v4, "PAGES_SEARCH"

    const/16 v5, 0x8

    const-string v6, "pages"

    const-string v7, "/search"

    const-string v8, "/*"

    invoke-direct/range {v3 .. v8}, LX/0Oz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, LX/0Oz;->PAGES_SEARCH:LX/0Oz;

    .line 54782
    const/16 v0, 0x9

    new-array v0, v0, [LX/0Oz;

    sget-object v1, LX/0Oz;->CONTACTS_CONTENT:LX/0Oz;

    aput-object v1, v0, v2

    sget-object v1, LX/0Oz;->CONTACT_ID:LX/0Oz;

    aput-object v1, v0, v9

    sget-object v1, LX/0Oz;->FRIENDS_CONTENT:LX/0Oz;

    aput-object v1, v0, v10

    sget-object v1, LX/0Oz;->FRIEND_UID:LX/0Oz;

    aput-object v1, v0, v11

    sget-object v1, LX/0Oz;->FRIENDS_PREFIX_SEARCH:LX/0Oz;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LX/0Oz;->FRIENDS_BIRTHDAYS:LX/0Oz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0Oz;->PAGES_CONTENT:LX/0Oz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0Oz;->PAGE_ID:LX/0Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0Oz;->PAGES_SEARCH:LX/0Oz;

    aput-object v2, v0, v1

    sput-object v0, LX/0Oz;->$VALUES:[LX/0Oz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54765
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54766
    iput-object p3, p0, LX/0Oz;->category:Ljava/lang/String;

    .line 54767
    iput-object p4, p0, LX/0Oz;->uriSuffix:Ljava/lang/String;

    .line 54768
    iput-object p5, p0, LX/0Oz;->uriMatcherSuffix:Ljava/lang/String;

    .line 54769
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Oz;
    .locals 1

    .prologue
    .line 54772
    const-class v0, LX/0Oz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Oz;

    return-object v0
.end method

.method public static values()[LX/0Oz;
    .locals 1

    .prologue
    .line 54783
    sget-object v0, LX/0Oz;->$VALUES:[LX/0Oz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Oz;

    return-object v0
.end method


# virtual methods
.method public final getFullUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 54771
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0P0;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Oz;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Oz;->uriSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final getMatcherPartialUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54770
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/0Oz;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Oz;->uriSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/0Oz;->uriMatcherSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final uriMatcherIndex()I
    .locals 1

    .prologue
    .line 54764
    invoke-virtual {p0}, LX/0Oz;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
