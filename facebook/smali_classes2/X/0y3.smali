.class public LX/0y3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/0y3;


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:Landroid/location/LocationManager;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 163963
    const-class v0, LX/0y3;

    sput-object v0, LX/0y3;->a:Ljava/lang/Class;

    .line 163964
    const-string v0, "gps"

    const-string v1, "network"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0y3;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/location/LocationManager;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163959
    iput-object p1, p0, LX/0y3;->c:Landroid/content/Context;

    .line 163960
    iput-object p2, p0, LX/0y3;->d:Landroid/location/LocationManager;

    .line 163961
    iput-object p3, p0, LX/0y3;->e:LX/0Ot;

    .line 163962
    return-void
.end method

.method public static a(LX/0QB;)LX/0y3;
    .locals 6

    .prologue
    .line 163945
    sget-object v0, LX/0y3;->f:LX/0y3;

    if-nez v0, :cond_1

    .line 163946
    const-class v1, LX/0y3;

    monitor-enter v1

    .line 163947
    :try_start_0
    sget-object v0, LX/0y3;->f:LX/0y3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 163948
    if-eqz v2, :cond_0

    .line 163949
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 163950
    new-instance v5, LX/0y3;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0y4;->b(LX/0QB;)Landroid/location/LocationManager;

    move-result-object v4

    check-cast v4, Landroid/location/LocationManager;

    const/16 p0, 0x13ba

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/0y3;-><init>(Landroid/content/Context;Landroid/location/LocationManager;LX/0Ot;)V

    .line 163951
    move-object v0, v5

    .line 163952
    sput-object v0, LX/0y3;->f:LX/0y3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163953
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 163954
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 163955
    :cond_1
    sget-object v0, LX/0y3;->f:LX/0y3;

    return-object v0

    .line 163956
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 163957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0y3;LX/0yF;LX/0cA;LX/0cA;)LX/0yG;
    .locals 6
    .param p1    # LX/0yF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0cA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yF;",
            "LX/0cA",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0cA",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0yG;"
        }
    .end annotation

    .prologue
    .line 163911
    const/4 v0, 0x0

    .line 163912
    sget-object v1, LX/0y3;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 163913
    :try_start_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163914
    iget-object v3, p0, LX/0y3;->d:Landroid/location/LocationManager;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163915
    :try_start_1
    iget-object v3, p0, LX/0y3;->d:Landroid/location/LocationManager;

    invoke-virtual {v3, v0}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v3

    .line 163916
    :goto_1
    move-object v3, v3

    .line 163917
    if-nez v3, :cond_3

    .line 163918
    sget-object v3, LX/0yG;->LOCATION_UNSUPPORTED:LX/0yG;
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0

    .line 163919
    :goto_2
    move-object v3, v3

    .line 163920
    sget-object v4, LX/0yG;->OKAY:LX/0yG;

    if-ne v3, v4, :cond_1

    .line 163921
    if-eqz p2, :cond_0

    .line 163922
    invoke-virtual {p2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 163923
    :cond_0
    :goto_3
    if-nez v1, :cond_7

    .line 163924
    :goto_4
    move-object v0, v3

    .line 163925
    move-object v1, v0

    .line 163926
    goto :goto_0

    .line 163927
    :cond_1
    sget-object v4, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v3, v4, :cond_0

    .line 163928
    if-eqz p3, :cond_0

    .line 163929
    invoke-virtual {p3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_3

    .line 163930
    :cond_2
    return-object v1

    .line 163931
    :cond_3
    :try_start_3
    invoke-virtual {v3}, Landroid/location/LocationProvider;->getPowerRequirement()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    sget-object v4, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    if-eq p1, v4, :cond_4

    .line 163932
    sget-object v3, LX/0yG;->LOCATION_UNSUPPORTED:LX/0yG;

    goto :goto_2

    .line 163933
    :cond_4
    invoke-virtual {v3}, Landroid/location/LocationProvider;->hasMonetaryCost()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/0y3;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/0y3;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0yH;

    sget-object v4, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {v3, v4}, LX/0yH;->a(LX/0yY;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 163934
    sget-object v3, LX/0yG;->LOCATION_UNSUPPORTED:LX/0yG;

    goto :goto_2

    .line 163935
    :cond_5
    iget-object v3, p0, LX/0y3;->d:Landroid/location/LocationManager;

    invoke-virtual {v3, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 163936
    sget-object v3, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    goto :goto_2

    .line 163937
    :cond_6
    sget-object v3, LX/0yG;->OKAY:LX/0yG;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 163938
    :catch_0
    sget-object v3, LX/0yG;->PERMISSION_DENIED:LX/0yG;

    goto :goto_2

    .line 163939
    :catch_1
    :goto_5
    :try_start_4
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 163940
    const/4 v3, 0x0

    goto :goto_1

    .line 163941
    :catch_2
    goto :goto_5

    :catch_3
    goto :goto_5
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0

    .line 163942
    :cond_7
    if-nez v3, :cond_8

    move-object v3, v1

    .line 163943
    goto :goto_4

    .line 163944
    :cond_8
    const/4 v0, 0x1

    new-array v0, v0, [LX/0yG;

    const/4 v4, 0x0

    aput-object v3, v0, v4

    invoke-static {v1, v0}, LX/0yq;->a(Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, LX/0yG;

    move-object v3, v0

    goto :goto_4
.end method

.method public static b(LX/0y3;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163909
    :try_start_0
    iget-object v1, p0, LX/0y3;->c:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 163910
    :cond_0
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0yG;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163908
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {p0, v0, v1, v1}, LX/0y3;->a(LX/0y3;LX/0yF;LX/0cA;LX/0cA;)LX/0yG;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1rv;
    .locals 1

    .prologue
    .line 163907
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-virtual {p0, v0}, LX/0y3;->b(LX/0yF;)LX/1rv;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0yF;)LX/1rv;
    .locals 4

    .prologue
    .line 163896
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {p0, v0}, LX/0y3;->b(LX/0y3;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v0}, LX/0y3;->b(LX/0y3;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 163897
    if-nez v0, :cond_1

    .line 163898
    new-instance v0, LX/1rv;

    sget-object v1, LX/0yG;->PERMISSION_DENIED:LX/0yG;

    .line 163899
    sget-object v2, LX/0Re;->a:LX/0Re;

    move-object v2, v2

    .line 163900
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 163901
    invoke-direct {v0, v1, v2, v3}, LX/1rv;-><init>(LX/0yG;LX/0Rf;LX/0Rf;)V

    .line 163902
    :goto_1
    return-object v0

    .line 163903
    :cond_1
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 163904
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 163905
    invoke-static {p0, p1, v1, v2}, LX/0y3;->a(LX/0y3;LX/0yF;LX/0cA;LX/0cA;)LX/0yG;

    move-result-object v3

    .line 163906
    new-instance v0, LX/1rv;

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, LX/1rv;-><init>(LX/0yG;LX/0Rf;LX/0Rf;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
