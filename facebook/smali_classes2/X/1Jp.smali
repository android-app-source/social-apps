.class public LX/1Jp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Jr;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final d:LX/1Ju;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ju",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            "LX/9Ae",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/1Ju;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ju",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            "LX/9Af",
            "<",
            "LX/1Rq;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/1Jq;

.field public g:LX/9Af;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9Af",
            "<",
            "LX/1Rq;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/A8f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/A8f",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:LX/9Ac;

.field public k:LX/1Jt;


# direct methods
.method private constructor <init>(LX/1Jq;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 230556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230557
    new-instance v0, LX/1Jr;

    invoke-direct {v0, p0}, LX/1Jr;-><init>(LX/1Jp;)V

    iput-object v0, p0, LX/1Jp;->a:LX/1Jr;

    .line 230558
    iput-boolean v1, p0, LX/1Jp;->i:Z

    .line 230559
    new-instance v0, LX/1Js;

    invoke-direct {v0}, LX/1Js;-><init>()V

    iput-object v0, p0, LX/1Jp;->k:LX/1Jt;

    .line 230560
    new-instance v0, LX/1Ju;

    new-instance v1, LX/1Jv;

    invoke-direct {v1}, LX/1Jv;-><init>()V

    invoke-direct {v0, v1}, LX/1Ju;-><init>(LX/1Jv;)V

    iput-object v0, p0, LX/1Jp;->d:LX/1Ju;

    .line 230561
    new-instance v0, LX/1Ju;

    new-instance v1, LX/1Jv;

    invoke-direct {v1}, LX/1Jv;-><init>()V

    invoke-direct {v0, v1}, LX/1Ju;-><init>(LX/1Jv;)V

    iput-object v0, p0, LX/1Jp;->e:LX/1Ju;

    .line 230562
    iput-object p1, p0, LX/1Jp;->f:LX/1Jq;

    .line 230563
    return-void
.end method

.method public static a(LX/0QB;)LX/1Jp;
    .locals 1

    .prologue
    .line 230564
    invoke-static {p0}, LX/1Jp;->b(LX/0QB;)LX/1Jp;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1Jp;
    .locals 3

    .prologue
    .line 230565
    new-instance v2, LX/1Jp;

    const-class v0, LX/1Jq;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/1Jq;

    invoke-direct {v2, v0}, LX/1Jp;-><init>(LX/1Jq;)V

    .line 230566
    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    .line 230567
    iput-object v0, v2, LX/1Jp;->b:LX/0Sh;

    iput-object v1, v2, LX/1Jp;->c:LX/0ad;

    .line 230568
    return-object v2
.end method


# virtual methods
.method public final a(LX/62M;LX/1Rq;Z)V
    .locals 3

    .prologue
    .line 230569
    new-instance v0, LX/9Af;

    invoke-direct {v0, p2}, LX/9Af;-><init>(LX/1OO;)V

    iput-object v0, p0, LX/1Jp;->g:LX/9Af;

    .line 230570
    new-instance v0, LX/A8f;

    new-instance v1, LX/9AY;

    invoke-direct {v1, p0}, LX/9AY;-><init>(LX/1Jp;)V

    invoke-direct {v0, p1, v1}, LX/A8f;-><init>(LX/62M;LX/0QK;)V

    iput-object v0, p0, LX/1Jp;->h:LX/A8f;

    .line 230571
    iget-object v0, p0, LX/1Jp;->a:LX/1Jr;

    .line 230572
    new-instance v1, LX/9Ac;

    invoke-direct {v1, p1, p2, p3, v0}, LX/9Ac;-><init>(LX/62M;LX/1Rq;ZLX/1Jr;)V

    .line 230573
    move-object v0, v1

    .line 230574
    iput-object v0, p0, LX/1Jp;->j:LX/9Ac;

    .line 230575
    iget-object v0, p0, LX/1Jp;->j:LX/9Ac;

    .line 230576
    iget-object v1, v0, LX/9Ac;->a:LX/62M;

    iget-object v2, v0, LX/9Ac;->e:LX/9Ab;

    .line 230577
    iget-object p1, v1, LX/62M;->b:LX/01J;

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v2, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230578
    iget-boolean v1, v0, LX/9Ac;->c:Z

    if-eqz v1, :cond_0

    .line 230579
    iget-object v1, v0, LX/9Ac;->b:LX/1Rq;

    iget-object v2, v0, LX/9Ac;->g:LX/9Aa;

    invoke-interface {v1, v2}, LX/1Rr;->a(LX/1KR;)V

    .line 230580
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Jp;->i:Z

    .line 230581
    return-void

    .line 230582
    :cond_0
    iget-object v1, v0, LX/9Ac;->b:LX/1Rq;

    iget-object v2, v0, LX/9Ac;->f:LX/9AZ;

    invoke-interface {v1, v2}, LX/1OQ;->a(LX/1OD;)V

    goto :goto_0
.end method
