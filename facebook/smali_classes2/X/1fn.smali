.class public final LX/1fn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1fl;


# direct methods
.method public constructor <init>(LX/1fl;)V
    .locals 0

    .prologue
    .line 292328
    iput-object p1, p0, LX/1fn;->a:LX/1fl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292335
    invoke-interface {p1}, LX/1ca;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292336
    iget-object v0, p0, LX/1fn;->a:LX/1fl;

    .line 292337
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v1

    invoke-static {v0, p1, v1}, LX/1fl;->a(LX/1fl;LX/1ca;Z)V

    .line 292338
    invoke-static {v0}, LX/1fl;->j(LX/1fl;)LX/1ca;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 292339
    const/4 v1, 0x0

    invoke-interface {p1}, LX/1ca;->b()Z

    move-result p0

    invoke-virtual {v0, v1, p0}, LX/1cZ;->a(Ljava/lang/Object;Z)Z

    .line 292340
    :cond_0
    :goto_0
    return-void

    .line 292341
    :cond_1
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292342
    iget-object v0, p0, LX/1fn;->a:LX/1fl;

    invoke-static {v0, p1}, LX/1fl;->c(LX/1fl;LX/1ca;)V

    goto :goto_0
.end method

.method public final b(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292333
    iget-object v0, p0, LX/1fn;->a:LX/1fl;

    invoke-static {v0, p1}, LX/1fl;->c(LX/1fl;LX/1ca;)V

    .line 292334
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292332
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292329
    iget-object v0, p0, LX/1fn;->a:LX/1fl;

    invoke-virtual {v0}, LX/1cZ;->f()F

    move-result v0

    .line 292330
    iget-object v1, p0, LX/1fn;->a:LX/1fl;

    invoke-interface {p1}, LX/1ca;->f()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {v1, v0}, LX/1cZ;->a(F)Z

    .line 292331
    return-void
.end method
