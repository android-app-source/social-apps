.class public final enum LX/0Pw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0Pw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0Pw;

.field public static final enum BROWSER:LX/0Pw;

.field public static final enum DASH:LX/0Pw;

.field public static final enum DASH_SERVICE:LX/0Pw;

.field public static final enum MAIN:LX/0Pw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57451
    new-instance v0, LX/0Pw;

    const-string v1, "MAIN"

    invoke-direct {v0, v1, v2}, LX/0Pw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Pw;->MAIN:LX/0Pw;

    .line 57452
    new-instance v0, LX/0Pw;

    const-string v1, "DASH"

    invoke-direct {v0, v1, v3}, LX/0Pw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Pw;->DASH:LX/0Pw;

    .line 57453
    new-instance v0, LX/0Pw;

    const-string v1, "DASH_SERVICE"

    invoke-direct {v0, v1, v4}, LX/0Pw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Pw;->DASH_SERVICE:LX/0Pw;

    .line 57454
    new-instance v0, LX/0Pw;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v5}, LX/0Pw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0Pw;->BROWSER:LX/0Pw;

    .line 57455
    const/4 v0, 0x4

    new-array v0, v0, [LX/0Pw;

    sget-object v1, LX/0Pw;->MAIN:LX/0Pw;

    aput-object v1, v0, v2

    sget-object v1, LX/0Pw;->DASH:LX/0Pw;

    aput-object v1, v0, v3

    sget-object v1, LX/0Pw;->DASH_SERVICE:LX/0Pw;

    aput-object v1, v0, v4

    sget-object v1, LX/0Pw;->BROWSER:LX/0Pw;

    aput-object v1, v0, v5

    sput-object v0, LX/0Pw;->$VALUES:[LX/0Pw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57450
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convertProcessNameToProcessEnum(LX/00G;)LX/0Pw;
    .locals 2

    .prologue
    .line 57446
    invoke-virtual {p0}, LX/00G;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57447
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid process name: unknown."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57448
    :cond_0
    iget-object v0, p0, LX/00G;->c:LX/014;

    move-object v0, v0

    .line 57449
    invoke-static {v0}, LX/0Pw;->convertProcessNameToProcessEnum(LX/014;)LX/0Pw;

    move-result-object v0

    return-object v0
.end method

.method public static convertProcessNameToProcessEnum(LX/014;)LX/0Pw;
    .locals 4

    .prologue
    .line 57429
    sget-object v0, LX/014;->a:LX/014;

    invoke-virtual {v0, p0}, LX/014;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 57430
    if-eqz v0, :cond_0

    .line 57431
    sget-object v0, LX/0Pw;->MAIN:LX/0Pw;

    .line 57432
    :goto_0
    return-object v0

    .line 57433
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/014;->b:Ljava/lang/String;

    move-object v0, v0

    .line 57434
    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57435
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Pw;->valueOf(Ljava/lang/String;)LX/0Pw;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 57436
    :catch_0
    move-exception v0

    .line 57437
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized process name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 57438
    iget-object v3, p0, LX/014;->b:Ljava/lang/String;

    move-object v3, v3

    .line 57439
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)LX/0Pw;
    .locals 1

    .prologue
    .line 57445
    const-class v0, LX/0Pw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0Pw;

    return-object v0
.end method

.method public static values()[LX/0Pw;
    .locals 1

    .prologue
    .line 57444
    sget-object v0, LX/0Pw;->$VALUES:[LX/0Pw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0Pw;

    return-object v0
.end method


# virtual methods
.method public final getProcessName()LX/00G;
    .locals 3

    .prologue
    .line 57440
    sget-object v0, LX/0Pw;->MAIN:LX/0Pw;

    invoke-virtual {p0, v0}, LX/0Pw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57441
    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v0

    .line 57442
    :goto_0
    invoke-static {v0}, LX/00G;->a(Ljava/lang/String;)LX/00G;

    move-result-object v0

    return-object v0

    .line 57443
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/0Pw;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
