.class public LX/0S7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0R6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60776
    iput-object p1, p0, LX/0S7;->a:Landroid/content/Context;

    .line 60777
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0S7;->b:Ljava/util/List;

    .line 60778
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0S7;->c:Ljava/util/List;

    .line 60779
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 60773
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    iget-object v1, p0, LX/0S7;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60774
    return-void
.end method

.method public final a(LX/0R6;)V
    .locals 1

    .prologue
    .line 60771
    iget-object v0, p0, LX/0S7;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60772
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60769
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60770
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 60765
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 60766
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    iget-object v1, p0, LX/0S7;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 60767
    return-void

    .line 60768
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 60759
    iget-object v0, p0, LX/0S7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 60760
    iget-object v0, p0, LX/0S7;->c:Ljava/util/List;

    iget-object v1, p0, LX/0S7;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 60761
    return-void

    .line 60762
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/content/Context;
    .locals 2

    .prologue
    .line 60764
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0S7;->a:Landroid/content/Context;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0S7;->b:Ljava/util/List;

    iget-object v1, p0, LX/0S7;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    goto :goto_0
.end method

.method public final e()LX/0R6;
    .locals 2

    .prologue
    .line 60763
    iget-object v0, p0, LX/0S7;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0S7;->c:Ljava/util/List;

    iget-object v1, p0, LX/0S7;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0R6;

    goto :goto_0
.end method
