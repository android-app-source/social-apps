.class public LX/0ij;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ik;
.implements LX/0il;
.implements LX/0im;
.implements LX/0in;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0ik",
        "<",
        "Lcom/facebook/composer/system/api/ComposerDerivedData;",
        ">;",
        "LX/0il",
        "<",
        "Lcom/facebook/composer/system/api/ComposerModel;",
        ">;",
        "LX/0im",
        "<",
        "Lcom/facebook/composer/system/api/ComposerMutation;",
        ">;",
        "LX/0in",
        "<",
        "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
        "<",
        "Lcom/facebook/composer/system/api/ComposerModel;",
        "Lcom/facebook/composer/system/api/ComposerDerivedData;",
        "Lcom/facebook/composer/system/api/ComposerMutation;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/Ar1;


# direct methods
.method public constructor <init>(LX/Ar1;)V
    .locals 0

    .prologue
    .line 122093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122094
    iput-object p1, p0, LX/0ij;->a:LX/Ar1;

    .line 122095
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122096
    iget-object v0, p0, LX/0ij;->a:LX/Ar1;

    invoke-virtual {v0}, LX/Ar1;->a()LX/HvN;

    move-result-object v0

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    return-object v0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122097
    invoke-virtual {p0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0jJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
            "<",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122098
    iget-object v0, p0, LX/0ij;->a:LX/Ar1;

    invoke-virtual {v0}, LX/Ar1;->a()LX/HvN;

    move-result-object v0

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122099
    iget-object v0, p0, LX/0ij;->a:LX/Ar1;

    invoke-virtual {v0}, LX/Ar1;->a()LX/HvN;

    move-result-object v0

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    return-object v0
.end method

.method public final e()Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 1

    .prologue
    .line 122100
    iget-object v0, p0, LX/0ij;->a:LX/Ar1;

    invoke-virtual {v0}, LX/Ar1;->a()LX/HvN;

    move-result-object v0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    return-object v0
.end method
