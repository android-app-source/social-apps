.class public final LX/1XO;
.super LX/1OD;
.source ""


# instance fields
.field public final synthetic a:LX/1UR;


# direct methods
.method public constructor <init>(LX/1UR;)V
    .locals 0

    .prologue
    .line 271142
    iput-object p1, p0, LX/1XO;->a:LX/1UR;

    invoke-direct {p0}, LX/1OD;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 271118
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271119
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271120
    return-void

    .line 271121
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Don\'t call notify* directly on the multi-adapter, call only on sub-adapters"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 271130
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-boolean v0, v0, LX/1UR;->d:Z

    if-eqz v0, :cond_0

    .line 271131
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    .line 271132
    :goto_0
    return-void

    .line 271133
    :cond_0
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    const/4 v1, 0x1

    .line 271134
    iput-boolean v1, v0, LX/1UR;->d:Z

    .line 271135
    move v1, v2

    .line 271136
    :goto_1
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 271137
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 271138
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 271139
    :cond_1
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    .line 271140
    iput-boolean v2, v0, LX/1UR;->d:Z

    .line 271141
    iget-object v0, p0, LX/1XO;->a:LX/1UR;

    iget-object v0, v0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->a()V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 271128
    invoke-direct {p0}, LX/1XO;->b()V

    .line 271129
    return-void
.end method

.method public final a(III)V
    .locals 0

    .prologue
    .line 271126
    invoke-direct {p0}, LX/1XO;->b()V

    .line 271127
    return-void
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 271124
    invoke-direct {p0}, LX/1XO;->b()V

    .line 271125
    return-void
.end method

.method public final c(II)V
    .locals 0

    .prologue
    .line 271122
    invoke-direct {p0}, LX/1XO;->b()V

    .line 271123
    return-void
.end method
