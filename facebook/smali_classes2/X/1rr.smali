.class public LX/1rr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        "LX/3DK;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1rt;


# direct methods
.method public constructor <init>(LX/1rt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333427
    iput-object p1, p0, LX/1rr;->a:LX/1rt;

    .line 333428
    return-void
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333421
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 333422
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 333423
    new-instance v4, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-direct {v4, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333424
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333425
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333416
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 333417
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 333418
    new-instance v4, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-direct {v4, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333419
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333420
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1rr;
    .locals 2

    .prologue
    .line 333414
    new-instance v1, LX/1rr;

    invoke-static {p0}, LX/1rt;->b(LX/0QB;)LX/1rt;

    move-result-object v0

    check-cast v0, LX/1rt;

    invoke-direct {v1, v0}, LX/1rr;-><init>(LX/1rt;)V

    .line 333415
    return-object v1
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 333332
    check-cast p2, LX/3DK;

    .line 333333
    iget-object v0, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v0, v0

    .line 333334
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 333335
    iget-object v0, p2, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v0, v0

    .line 333336
    if-eqz v0, :cond_0

    .line 333337
    iget-object v0, p2, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v0, v0

    .line 333338
    iget-object v1, v0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->g:Ljava/lang/String;

    move-object v0, v1

    .line 333339
    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    invoke-virtual {v1}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 333340
    :goto_0
    iget-object v1, p0, LX/1rr;->a:LX/1rt;

    .line 333341
    iget-object v2, p2, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v2, v2

    .line 333342
    invoke-virtual {v1, v2, v0}, LX/1rt;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Z)LX/0gW;

    move-result-object v1

    .line 333343
    if-eqz v0, :cond_1

    .line 333344
    const-string v0, "before_notification_stories"

    .line 333345
    iget-object v2, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v2, v2

    .line 333346
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333347
    const-string v0, "last_notification_stories"

    .line 333348
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 333349
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 333350
    :goto_1
    return-object v1

    .line 333351
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 333352
    :cond_1
    const-string v0, "after_notification_stories"

    .line 333353
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 333354
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 333355
    const-string v0, "first_notification_stories"

    .line 333356
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 333357
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 333358
    const/4 v3, 0x0

    .line 333359
    if-eqz p1, :cond_0

    .line 333360
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333361
    if-nez v0, :cond_1

    .line 333362
    :cond_0
    sget-object v0, LX/5Mb;->a:LX/5Mb;

    .line 333363
    :goto_0
    return-object v0

    .line 333364
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333365
    instance-of v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    if-eqz v0, :cond_4

    .line 333366
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333367
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 333368
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333369
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    .line 333370
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;->j()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/1rr;->a(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 333371
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;->k()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_3

    .line 333372
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 333373
    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    :goto_1
    move v4, v0

    move v5, v1

    move-object v1, v3

    .line 333374
    :goto_2
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 333375
    :cond_2
    sget-object v0, LX/5Mb;->a:LX/5Mb;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 333376
    goto :goto_1

    .line 333377
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333378
    instance-of v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;

    if-eqz v0, :cond_6

    .line 333379
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333380
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 333381
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333382
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;

    .line 333383
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/1rr;->b(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 333384
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    move v4, v0

    move v5, v1

    move-object v1, v3

    .line 333385
    goto :goto_2

    :cond_5
    move v0, v2

    .line 333386
    goto :goto_3

    .line 333387
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333388
    instance-of v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;

    if-eqz v0, :cond_8

    .line 333389
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333390
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 333391
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333392
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;

    .line 333393
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/1rr;->a(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 333394
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_7

    .line 333395
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$FirstNotificationsQueryModel$NotificationStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 333396
    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    :goto_4
    move v4, v1

    move v5, v0

    move-object v1, v3

    .line 333397
    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 333398
    goto :goto_4

    .line 333399
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333400
    instance-of v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;

    if-eqz v0, :cond_a

    .line 333401
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333402
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 333403
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 333404
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;

    .line 333405
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/1rr;->b(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 333406
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    move v4, v1

    move v5, v0

    move-object v1, v3

    .line 333407
    goto/16 :goto_2

    :cond_9
    move v0, v2

    .line 333408
    goto :goto_5

    .line 333409
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Result should come from delta or first notifications query only"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333410
    :cond_b
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    .line 333411
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->ia_()Ljava/lang/String;

    move-result-object v2

    .line 333412
    add-int/lit8 v0, v3, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->ia_()Ljava/lang/String;

    move-result-object v3

    .line 333413
    new-instance v0, LX/5Mb;

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_c
    move v4, v1

    move v5, v1

    move-object v1, v3

    goto/16 :goto_2
.end method
