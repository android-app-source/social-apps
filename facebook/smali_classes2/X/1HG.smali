.class public LX/1HG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1Gd",
        "<",
        "LX/1HR;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 226643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 226644
    const v2, 0x7fffffff

    .line 226645
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v6

    const-wide/32 v8, 0x7fffffff

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v6, v6

    .line 226646
    const/high16 v7, 0x1000000

    if-ge v6, v7, :cond_0

    .line 226647
    const/high16 v6, 0x100000

    .line 226648
    :goto_0
    move v1, v6

    .line 226649
    div-int/lit8 v5, v1, 0x8

    .line 226650
    new-instance v0, LX/1HR;

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v5}, LX/1HR;-><init>(IIIII)V

    return-object v0

    .line 226651
    :cond_0
    const/high16 v7, 0x2000000

    if-ge v6, v7, :cond_1

    .line 226652
    const/high16 v6, 0x200000

    goto :goto_0

    .line 226653
    :cond_1
    const/high16 v6, 0x400000

    goto :goto_0
.end method
