.class public LX/15M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/15M;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6c4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/01T;

.field public d:Landroid/app/Activity;


# direct methods
.method public constructor <init>(LX/0Or;LX/01T;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/ipc/annotations/IsChatHeadDebugShowInsideAppEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 180522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180523
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 180524
    iput-object v0, p0, LX/15M;->a:LX/0Ot;

    .line 180525
    iput-object p1, p0, LX/15M;->b:LX/0Or;

    .line 180526
    iput-object p2, p0, LX/15M;->c:LX/01T;

    .line 180527
    return-void
.end method

.method public static a(LX/0QB;)LX/15M;
    .locals 5

    .prologue
    .line 180528
    sget-object v0, LX/15M;->e:LX/15M;

    if-nez v0, :cond_1

    .line 180529
    const-class v1, LX/15M;

    monitor-enter v1

    .line 180530
    :try_start_0
    sget-object v0, LX/15M;->e:LX/15M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 180531
    if-eqz v2, :cond_0

    .line 180532
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 180533
    new-instance v4, LX/15M;

    const/16 v3, 0x150b

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-direct {v4, p0, v3}, LX/15M;-><init>(LX/0Or;LX/01T;)V

    .line 180534
    const/16 v3, 0x26f4

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 180535
    iput-object v3, v4, LX/15M;->a:LX/0Ot;

    .line 180536
    move-object v0, v4

    .line 180537
    sput-object v0, LX/15M;->e:LX/15M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180538
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 180539
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 180540
    :cond_1
    sget-object v0, LX/15M;->e:LX/15M;

    return-object v0

    .line 180541
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 180542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/15M;)V
    .locals 1

    .prologue
    .line 180543
    const/4 v0, 0x0

    iput-object v0, p0, LX/15M;->d:Landroid/app/Activity;

    .line 180544
    iget-object v0, p0, LX/15M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6c4;

    invoke-virtual {v0}, LX/6c4;->a()V

    .line 180545
    return-void
.end method
