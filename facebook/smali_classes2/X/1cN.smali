.class public abstract LX/1cN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T::",
        "Ljava/io/Closeable;",
        ">",
        "Ljava/lang/Object;",
        "LX/1cF",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "LX/1cN",
            "<TK;TT;>.Multiplexer;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final b:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1cF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 281964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281965
    iput-object p1, p0, LX/1cN;->b:LX/1cF;

    .line 281966
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1cN;->a:Ljava/util/Map;

    .line 281967
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/1cN;Ljava/lang/Object;)LX/1eL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/1cN",
            "<TK;TT;>.Multiplexer;"
        }
    .end annotation

    .prologue
    .line 281959
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cN;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eL;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/1cN;Ljava/lang/Object;LX/1eL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/1cN",
            "<TK;TT;>.Multiplexer;)V"
        }
    .end annotation

    .prologue
    .line 281955
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1cN;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 281956
    iget-object v0, p0, LX/1cN;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281957
    :cond_0
    monitor-exit p0

    return-void

    .line 281958
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/Object;)LX/1eL;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LX/1cN",
            "<TK;TT;>.Multiplexer;"
        }
    .end annotation

    .prologue
    .line 281960
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/1eL;

    invoke-direct {v0, p0, p1}, LX/1eL;-><init>(LX/1cN;Ljava/lang/Object;)V

    .line 281961
    iget-object v1, p0, LX/1cN;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281962
    monitor-exit p0

    return-object v0

    .line 281963
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public abstract a(Ljava/io/Closeable;)Ljava/io/Closeable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation
.end method

.method public abstract a(LX/1cW;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")TK;"
        }
    .end annotation
.end method

.method public final a(LX/1cd;LX/1cW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281942
    invoke-virtual {p0, p2}, LX/1cN;->a(LX/1cW;)Ljava/lang/Object;

    move-result-object v2

    .line 281943
    :cond_0
    const/4 v1, 0x0

    .line 281944
    monitor-enter p0

    .line 281945
    :try_start_0
    invoke-static {p0, v2}, LX/1cN;->a$redex0(LX/1cN;Ljava/lang/Object;)LX/1eL;

    move-result-object v0

    .line 281946
    if-nez v0, :cond_1

    .line 281947
    invoke-direct {p0, v2}, LX/1cN;->b(Ljava/lang/Object;)LX/1eL;

    move-result-object v0

    .line 281948
    const/4 v1, 0x1

    .line 281949
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281950
    invoke-virtual {v0, p1, p2}, LX/1eL;->a(LX/1cd;LX/1cW;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 281951
    if-eqz v1, :cond_2

    .line 281952
    invoke-static {v0}, LX/1eL;->a$redex0(LX/1eL;)V

    .line 281953
    :cond_2
    return-void

    .line 281954
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
