.class public LX/1fP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:Ljava/util/Random;


# direct methods
.method public constructor <init>(Ljava/util/Random;LX/0Zb;)V
    .locals 0
    .param p1    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 291557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291558
    iput-object p2, p0, LX/1fP;->a:LX/0Zb;

    .line 291559
    iput-object p1, p0, LX/1fP;->b:Ljava/util/Random;

    .line 291560
    return-void
.end method

.method public static b(LX/0QB;)LX/1fP;
    .locals 3

    .prologue
    .line 291561
    new-instance v2, LX/1fP;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {v2, v0, v1}, LX/1fP;-><init>(Ljava/util/Random;LX/0Zb;)V

    .line 291562
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 291563
    iget-object v0, p0, LX/1fP;->a:LX/0Zb;

    const-string v1, "feed_omnistore_invalidation"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 291564
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291565
    const-string v1, "status"

    const-string v2, "failure"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291566
    const-string v1, "dedup_key"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291567
    const-string v1, "graphql_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291568
    const-string v1, "reason"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291569
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 291570
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 3

    .prologue
    .line 291571
    iget-object v0, p0, LX/1fP;->b:Ljava/util/Random;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 291572
    :cond_0
    :goto_0
    return-void

    .line 291573
    :cond_1
    iget-object v0, p0, LX/1fP;->a:LX/0Zb;

    const-string v1, "feed_omnistore_save_mod_50"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 291574
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291575
    const-string v1, "dedup_key"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291576
    const-string v1, "sort_key"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291577
    const-string v1, "graphql_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 291578
    const-string v1, "valid"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 291579
    const-string v1, "old_like_count"

    invoke-virtual {v0, v1, p5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 291580
    const-string v1, "old_comment_count"

    invoke-virtual {v0, v1, p6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 291581
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
