.class public LX/1WU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile c:LX/1WU;


# instance fields
.field public final b:LX/1WV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268651
    const-class v0, LX/1WU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1WU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1WV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 268652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268653
    iput-object p1, p0, LX/1WU;->b:LX/1WV;

    .line 268654
    return-void
.end method

.method public static a(LX/0QB;)LX/1WU;
    .locals 6

    .prologue
    .line 268655
    sget-object v0, LX/1WU;->c:LX/1WU;

    if-nez v0, :cond_1

    .line 268656
    const-class v1, LX/1WU;

    monitor-enter v1

    .line 268657
    :try_start_0
    sget-object v0, LX/1WU;->c:LX/1WU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 268658
    if-eqz v2, :cond_0

    .line 268659
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 268660
    new-instance v4, LX/1WU;

    .line 268661
    new-instance p0, LX/1WV;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v5}, LX/1WV;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 268662
    move-object v3, p0

    .line 268663
    check-cast v3, LX/1WV;

    invoke-direct {v4, v3}, LX/1WU;-><init>(LX/1WV;)V

    .line 268664
    move-object v0, v4

    .line 268665
    sput-object v0, LX/1WU;->c:LX/1WU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268666
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 268667
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 268668
    :cond_1
    sget-object v0, LX/1WU;->c:LX/1WU;

    return-object v0

    .line 268669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 268670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
