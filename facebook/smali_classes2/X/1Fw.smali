.class public LX/1Fw;
.super Ljava/util/concurrent/AbstractExecutorService;
.source ""

# interfaces
.implements LX/1Fx;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V
    .locals 7

    .prologue
    .line 224756
    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    .line 224757
    new-instance v0, LX/1Fy;

    invoke-direct {v0, p0}, LX/1Fy;-><init>(LX/1Fw;)V

    iput-object v0, p0, LX/1Fw;->a:Ljava/util/concurrent/Executor;

    .line 224758
    new-instance v0, LX/1Fz;

    invoke-direct {v0, p0}, LX/1Fz;-><init>(LX/1Fw;)V

    iput-object v0, p0, LX/1Fw;->b:Ljava/util/concurrent/Executor;

    .line 224759
    new-instance v0, LX/1G0;

    invoke-direct {v0, p0}, LX/1G0;-><init>(LX/1Fw;)V

    iput-object v0, p0, LX/1Fw;->c:Ljava/util/concurrent/Executor;

    .line 224760
    new-instance v0, LX/0TS;

    .line 224761
    new-instance v1, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v2, 0xb

    new-instance v3, LX/1G1;

    invoke-direct {v3}, LX/1G1;-><init>()V

    invoke-direct {v1, v2, v3}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    move-object v4, v1

    .line 224762
    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/0TS;-><init>(Ljava/lang/String;ILjava/util/concurrent/Executor;Ljava/util/concurrent/BlockingQueue;LX/0Sj;LX/0TR;)V

    iput-object v0, p0, LX/1Fw;->d:Ljava/util/concurrent/Executor;

    .line 224763
    return-void
.end method


# virtual methods
.method public final a(LX/1Fu;Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fu;",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 224752
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224753
    new-instance v0, LX/1em;

    invoke-direct {v0, p2, p1}, LX/1em;-><init>(Ljava/lang/Runnable;LX/1Fu;)V

    move-object v0, v0

    .line 224754
    const v1, -0x24594bc6

    invoke-static {p0, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 224755
    return-object v0
.end method

.method public final a(LX/1Fu;)Ljava/util/concurrent/Executor;
    .locals 2

    .prologue
    .line 224748
    sget-object v0, LX/1G7;->a:[I

    invoke-virtual {p1}, LX/1Fu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 224749
    iget-object v0, p0, LX/1Fw;->b:Ljava/util/concurrent/Executor;

    :goto_0
    return-object v0

    .line 224750
    :pswitch_0
    iget-object v0, p0, LX/1Fw;->a:Ljava/util/concurrent/Executor;

    goto :goto_0

    .line 224751
    :pswitch_1
    iget-object v0, p0, LX/1Fw;->c:Ljava/util/concurrent/Executor;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 224747
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 224741
    iget-object v0, p0, LX/1Fw;->d:Ljava/util/concurrent/Executor;

    const v1, -0x153a9f81

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 224742
    return-void
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 224746
    const/4 v0, 0x0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 224745
    const/4 v0, 0x0

    return v0
.end method

.method public final shutdown()V
    .locals 1

    .prologue
    .line 224744
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224743
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
