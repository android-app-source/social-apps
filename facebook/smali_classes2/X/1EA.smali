.class public LX/1EA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1EF;

.field public c:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public d:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public e:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public f:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public g:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public i:I
    .annotation build Landroid/support/annotation/Dimension;
    .end annotation
.end field

.field public j:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Z

.field private r:Z

.field public s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

.field public t:Z

.field public u:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 219411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219412
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 219413
    iput-object v0, p0, LX/1EA;->a:LX/0Px;

    .line 219414
    const/4 v0, -0x1

    iput v0, p0, LX/1EA;->i:I

    .line 219415
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->l:Ljava/lang/String;

    .line 219416
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->m:Ljava/lang/String;

    .line 219417
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->n:Ljava/lang/String;

    .line 219418
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->o:Ljava/lang/String;

    .line 219419
    iput-boolean v1, p0, LX/1EA;->p:Z

    .line 219420
    iput-boolean v1, p0, LX/1EA;->q:Z

    .line 219421
    iput-boolean v1, p0, LX/1EA;->r:Z

    .line 219422
    return-void
.end method

.method public constructor <init>(LX/1EE;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 219357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219358
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 219359
    iput-object v0, p0, LX/1EA;->a:LX/0Px;

    .line 219360
    const/4 v0, -0x1

    iput v0, p0, LX/1EA;->i:I

    .line 219361
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->l:Ljava/lang/String;

    .line 219362
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->m:Ljava/lang/String;

    .line 219363
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->n:Ljava/lang/String;

    .line 219364
    const-string v0, ""

    iput-object v0, p0, LX/1EA;->o:Ljava/lang/String;

    .line 219365
    iput-boolean v1, p0, LX/1EA;->p:Z

    .line 219366
    iput-boolean v1, p0, LX/1EA;->q:Z

    .line 219367
    iput-boolean v1, p0, LX/1EA;->r:Z

    .line 219368
    iget-object v0, p1, LX/1EE;->a:LX/0Px;

    move-object v0, v0

    .line 219369
    iput-object v0, p0, LX/1EA;->a:LX/0Px;

    .line 219370
    iget-boolean v0, p1, LX/1EE;->b:Z

    move v0, v0

    .line 219371
    iput-boolean v0, p0, LX/1EA;->p:Z

    .line 219372
    iget-boolean v0, p1, LX/1EE;->c:Z

    move v0, v0

    .line 219373
    iput-boolean v0, p0, LX/1EA;->q:Z

    .line 219374
    iget-object v0, p1, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    move-object v0, v0

    .line 219375
    iput-object v0, p0, LX/1EA;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219376
    iget-boolean v0, p1, LX/1EE;->d:Z

    move v0, v0

    .line 219377
    iput-boolean v0, p0, LX/1EA;->r:Z

    .line 219378
    iget-object v0, p1, LX/1EE;->e:LX/1EF;

    move-object v0, v0

    .line 219379
    iput-object v0, p0, LX/1EA;->b:LX/1EF;

    .line 219380
    iget v0, p1, LX/1EE;->j:I

    move v0, v0

    .line 219381
    iput v0, p0, LX/1EA;->c:I

    .line 219382
    iget v0, p1, LX/1EE;->k:I

    move v0, v0

    .line 219383
    iput v0, p0, LX/1EA;->d:I

    .line 219384
    iget v0, p1, LX/1EE;->l:I

    move v0, v0

    .line 219385
    iput v0, p0, LX/1EA;->e:I

    .line 219386
    iget-boolean v0, p1, LX/1EE;->u:Z

    move v0, v0

    .line 219387
    iput-boolean v0, p0, LX/1EA;->u:Z

    .line 219388
    iget v0, p1, LX/1EE;->m:I

    move v0, v0

    .line 219389
    iput v0, p0, LX/1EA;->f:I

    .line 219390
    iget v0, p1, LX/1EE;->n:I

    move v0, v0

    .line 219391
    iput v0, p0, LX/1EA;->g:I

    .line 219392
    iget v0, p1, LX/1EE;->o:I

    move v0, v0

    .line 219393
    iput v0, p0, LX/1EA;->h:I

    .line 219394
    iget v0, p1, LX/1EE;->p:I

    move v0, v0

    .line 219395
    iput v0, p0, LX/1EA;->i:I

    .line 219396
    iget-object v0, p1, LX/1EE;->r:Ljava/lang/Integer;

    move-object v0, v0

    .line 219397
    iput-object v0, p0, LX/1EA;->k:Ljava/lang/Integer;

    .line 219398
    iget-object v0, p1, LX/1EE;->f:Ljava/lang/String;

    move-object v0, v0

    .line 219399
    iput-object v0, p0, LX/1EA;->l:Ljava/lang/String;

    .line 219400
    iget-object v0, p1, LX/1EE;->g:Ljava/lang/String;

    move-object v0, v0

    .line 219401
    iput-object v0, p0, LX/1EA;->m:Ljava/lang/String;

    .line 219402
    iget-object v0, p1, LX/1EE;->h:Ljava/lang/String;

    move-object v0, v0

    .line 219403
    iput-object v0, p0, LX/1EA;->n:Ljava/lang/String;

    .line 219404
    iget-object v0, p1, LX/1EE;->i:Ljava/lang/String;

    move-object v0, v0

    .line 219405
    iput-object v0, p0, LX/1EA;->o:Ljava/lang/String;

    .line 219406
    iget-object v0, p1, LX/1EE;->q:Ljava/lang/Integer;

    move-object v0, v0

    .line 219407
    iput-object v0, p0, LX/1EA;->j:Ljava/lang/Integer;

    .line 219408
    iget-boolean v0, p1, LX/1EE;->t:Z

    move v0, v0

    .line 219409
    iput-boolean v0, p0, LX/1EA;->t:Z

    .line 219410
    return-void
.end method


# virtual methods
.method public final a()LX/1EE;
    .locals 23

    .prologue
    .line 219356
    new-instance v1, LX/1EE;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1EA;->a:LX/0Px;

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/1EA;->p:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/1EA;->q:Z

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1EA;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/1EA;->r:Z

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1EA;->b:LX/1EF;

    move-object/from16 v0, p0

    iget v8, v0, LX/1EA;->c:I

    move-object/from16 v0, p0

    iget v9, v0, LX/1EA;->d:I

    move-object/from16 v0, p0

    iget v10, v0, LX/1EA;->e:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/1EA;->u:Z

    move-object/from16 v0, p0

    iget v12, v0, LX/1EA;->f:I

    move-object/from16 v0, p0

    iget v13, v0, LX/1EA;->g:I

    move-object/from16 v0, p0

    iget v14, v0, LX/1EA;->h:I

    move-object/from16 v0, p0

    iget v15, v0, LX/1EA;->i:I

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->k:Ljava/lang/Integer;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->l:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->m:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->n:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/1EA;->t:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1EA;->j:Ljava/lang/Integer;

    move-object/from16 v22, v0

    invoke-direct/range {v1 .. v22}, LX/1EE;-><init>(LX/0Px;ZZLcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;ZLX/1EF;IIIZIIIILjava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;)V

    return-object v1
.end method
