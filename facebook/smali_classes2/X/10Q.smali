.class public final enum LX/10Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/10Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/10Q;

.field public static final enum HIDDEN:LX/10Q;

.field public static final enum SHOWN_MORE:LX/10Q;

.field public static final enum SHOWN_TABS:LX/10Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169135
    new-instance v0, LX/10Q;

    const-string v1, "SHOWN_TABS"

    invoke-direct {v0, v1, v2}, LX/10Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10Q;->SHOWN_TABS:LX/10Q;

    .line 169136
    new-instance v0, LX/10Q;

    const-string v1, "SHOWN_MORE"

    invoke-direct {v0, v1, v3}, LX/10Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10Q;->SHOWN_MORE:LX/10Q;

    .line 169137
    new-instance v0, LX/10Q;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4}, LX/10Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/10Q;->HIDDEN:LX/10Q;

    .line 169138
    const/4 v0, 0x3

    new-array v0, v0, [LX/10Q;

    sget-object v1, LX/10Q;->SHOWN_TABS:LX/10Q;

    aput-object v1, v0, v2

    sget-object v1, LX/10Q;->SHOWN_MORE:LX/10Q;

    aput-object v1, v0, v3

    sget-object v1, LX/10Q;->HIDDEN:LX/10Q;

    aput-object v1, v0, v4

    sput-object v0, LX/10Q;->$VALUES:[LX/10Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 169139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/10Q;
    .locals 1

    .prologue
    .line 169140
    const-class v0, LX/10Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/10Q;

    return-object v0
.end method

.method public static values()[LX/10Q;
    .locals 1

    .prologue
    .line 169141
    sget-object v0, LX/10Q;->$VALUES:[LX/10Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/10Q;

    return-object v0
.end method
