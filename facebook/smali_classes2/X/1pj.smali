.class public final LX/1pj;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/1pk;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1pj;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1pk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329608
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 329609
    return-void
.end method

.method public static a(LX/0QB;)LX/1pj;
    .locals 4

    .prologue
    .line 329610
    sget-object v0, LX/1pj;->a:LX/1pj;

    if-nez v0, :cond_1

    .line 329611
    const-class v1, LX/1pj;

    monitor-enter v1

    .line 329612
    :try_start_0
    sget-object v0, LX/1pj;->a:LX/1pj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 329613
    if-eqz v2, :cond_0

    .line 329614
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 329615
    new-instance v3, LX/1pj;

    const/16 p0, 0x4ef

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1pj;-><init>(LX/0Ot;)V

    .line 329616
    move-object v0, v3

    .line 329617
    sput-object v0, LX/1pj;->a:LX/1pj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 329618
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 329619
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 329620
    :cond_1
    sget-object v0, LX/1pj;->a:LX/1pj;

    return-object v0

    .line 329621
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 329622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 329623
    check-cast p3, LX/1pk;

    .line 329624
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 329625
    const-string v1, "com.facebook.zero.ZERO_RATING_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329626
    iget-object v0, p3, LX/1pk;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/1pk;->e:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 329627
    :cond_0
    :goto_0
    return-void

    .line 329628
    :cond_1
    iget-object v0, p3, LX/1pk;->f:LX/1pm;

    .line 329629
    invoke-static {}, LX/49l;->a()LX/49k;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 329630
    iget-object p0, v0, LX/1pm;->a:LX/0tX;

    invoke-virtual {p0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    move-object v0, v1

    .line 329631
    new-instance v1, LX/5NQ;

    invoke-direct {v1, p3}, LX/5NQ;-><init>(LX/1pk;)V

    .line 329632
    iget-object p0, p3, LX/1pk;->h:LX/1Ck;

    const/4 p1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 329633
    goto :goto_0
.end method
