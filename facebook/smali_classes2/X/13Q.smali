.class public LX/13Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/13Q;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/13X;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "itself"
    .end annotation
.end field

.field private final c:LX/13S;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176818
    const-class v0, LX/13Q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/13Q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/13S;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176757
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    .line 176758
    iput-object p1, p0, LX/13Q;->c:LX/13S;

    .line 176759
    return-void
.end method

.method public static a(LX/0QB;)LX/13Q;
    .locals 4

    .prologue
    .line 176805
    sget-object v0, LX/13Q;->d:LX/13Q;

    if-nez v0, :cond_1

    .line 176806
    const-class v1, LX/13Q;

    monitor-enter v1

    .line 176807
    :try_start_0
    sget-object v0, LX/13Q;->d:LX/13Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 176808
    if-eqz v2, :cond_0

    .line 176809
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 176810
    new-instance p0, LX/13Q;

    invoke-static {v0}, LX/13R;->a(LX/0QB;)LX/13S;

    move-result-object v3

    check-cast v3, LX/13S;

    invoke-direct {p0, v3}, LX/13Q;-><init>(LX/13S;)V

    .line 176811
    move-object v0, p0

    .line 176812
    sput-object v0, LX/13Q;->d:LX/13Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176813
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 176814
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176815
    :cond_1
    sget-object v0, LX/13Q;->d:LX/13Q;

    return-object v0

    .line 176816
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 176817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 176802
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/13Q;->c:LX/13S;

    const-string v1, "data"

    new-instance v2, LX/13V;

    invoke-direct {v2, p0, p1}, LX/13V;-><init>(LX/13Q;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1, v2}, LX/13S;->a(Ljava/lang/String;Ljava/lang/String;LX/13W;)LX/13S;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176803
    monitor-exit p0

    return-void

    .line 176804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 176800
    const-wide/16 v0, 0x1

    invoke-virtual {p0, p1, v0, v1}, LX/13Q;->a(Ljava/lang/String;J)V

    .line 176801
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 176798
    const-string v0, "counters"

    invoke-virtual {p0, p1, p2, p3, v0}, LX/13Q;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 176799
    return-void
.end method

.method public final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 176785
    iget-object v1, p0, LX/13Q;->b:Ljava/util/Map;

    monitor-enter v1

    .line 176786
    :try_start_0
    iget-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176787
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 176788
    iget-object v2, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v2, p4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176789
    invoke-direct {p0, p4}, LX/13Q;->d(Ljava/lang/String;)V

    .line 176790
    :cond_0
    iget-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 176791
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 176792
    new-instance v2, LX/13X;

    invoke-direct {v2}, LX/13X;-><init>()V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176793
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13X;

    .line 176794
    iget v4, v0, LX/13X;->a:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/13X;->a:I

    .line 176795
    iget-wide v4, v0, LX/13X;->b:J

    add-long/2addr v4, p2

    iput-wide v4, v0, LX/13X;->b:J

    .line 176796
    iget-wide v4, v0, LX/13X;->c:J

    mul-long v6, p2, p2

    add-long/2addr v4, v6

    iput-wide v4, v0, LX/13X;->c:J

    .line 176797
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/String;)LX/0lF;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 176764
    iget-object v2, p0, LX/13Q;->b:Ljava/util/Map;

    monitor-enter v2

    .line 176765
    :try_start_0
    iget-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 176766
    if-nez v0, :cond_0

    .line 176767
    monitor-exit v2

    move-object v0, v1

    .line 176768
    :goto_0
    return-object v0

    .line 176769
    :cond_0
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    .line 176770
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 176771
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176772
    invoke-virtual {v3}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 176773
    goto :goto_0

    .line 176774
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 176775
    :cond_1
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 176776
    invoke-virtual {v3}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176777
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13X;

    .line 176778
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 176779
    const-string v5, "count"

    iget v6, v0, LX/13X;->a:I

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 176780
    const-string v5, "sum"

    iget-wide v6, v0, LX/13X;->b:J

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 176781
    const-string v5, "s_sum"

    iget-wide v6, v0, LX/13X;->c:J

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 176782
    move-object v0, v4

    .line 176783
    invoke-virtual {v2, v1, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 176784
    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 176760
    iget-object v1, p0, LX/13Q;->b:Ljava/util/Map;

    monitor-enter v1

    .line 176761
    :try_start_0
    iget-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176762
    iget-object v0, p0, LX/13Q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 176763
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
