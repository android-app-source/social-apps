.class public LX/1EQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/1BK;

.field private final c:LX/0kb;

.field private final d:LX/13x;

.field private final e:LX/0SG;

.field private final f:LX/0Zh;


# direct methods
.method public constructor <init>(LX/0Zb;LX/1BK;LX/0kb;LX/0SG;LX/13x;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219877
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    iput-object v0, p0, LX/1EQ;->f:LX/0Zh;

    .line 219878
    iput-object p1, p0, LX/1EQ;->a:LX/0Zb;

    .line 219879
    iput-object p2, p0, LX/1EQ;->b:LX/1BK;

    .line 219880
    iput-object p3, p0, LX/1EQ;->c:LX/0kb;

    .line 219881
    iput-object p4, p0, LX/1EQ;->e:LX/0SG;

    .line 219882
    iput-object p5, p0, LX/1EQ;->d:LX/13x;

    .line 219883
    return-void
.end method

.method public static a(LX/0QB;)LX/1EQ;
    .locals 1

    .prologue
    .line 219801
    invoke-static {p0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 219864
    iget-object v0, p0, LX/1EQ;->f:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->c()LX/0zL;

    move-result-object v1

    .line 219865
    :try_start_0
    iget-object v0, p0, LX/1EQ;->d:LX/13x;

    invoke-virtual {v0, v1}, LX/13x;->a(LX/0zL;)V

    .line 219866
    invoke-virtual {v1}, LX/0zL;->j()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 219867
    invoke-virtual {v1}, LX/0nA;->a()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 219868
    :cond_0
    :try_start_1
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0nA;->a(LX/0nD;)V

    .line 219869
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 219870
    invoke-virtual {v1, v0}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 219871
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 219872
    invoke-virtual {v1}, LX/0nA;->a()V

    goto :goto_0

    .line 219873
    :catch_0
    move-exception v0

    .line 219874
    :try_start_2
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219875
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method

.method public static b(LX/0QB;)LX/1EQ;
    .locals 6

    .prologue
    .line 219862
    new-instance v0, LX/1EQ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/1BK;->a(LX/0QB;)LX/1BK;

    move-result-object v2

    check-cast v2, LX/1BK;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/13x;->a(LX/0QB;)LX/13x;

    move-result-object v5

    check-cast v5, LX/13x;

    invoke-direct/range {v0 .. v5}, LX/1EQ;-><init>(LX/0Zb;LX/1BK;LX/0kb;LX/0SG;LX/13x;)V

    .line 219863
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;ILjava/lang/String;JI)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 219852
    const-string v0, "image_loading_state"

    invoke-virtual {p1, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 219853
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    .line 219854
    const-string v0, "time_since_fetched"

    invoke-virtual {p1, v0, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 219855
    :cond_0
    if-eqz p3, :cond_1

    .line 219856
    const-string v0, "client_viewstate_position"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 219857
    const-string v0, "client_bump_state"

    invoke-virtual {p1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 219858
    :cond_1
    invoke-direct {p0}, LX/1EQ;->a()Ljava/lang/String;

    move-result-object v0

    .line 219859
    if-eqz v0, :cond_2

    .line 219860
    const-string v1, "enabled_features"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 219861
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 219841
    const/4 v6, 0x0

    .line 219842
    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219843
    iget-object v0, p0, LX/1EQ;->b:LX/1BK;

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v6

    .line 219844
    :cond_0
    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 219845
    iget-object v0, p0, LX/1EQ;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-interface {p2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v2

    sub-long v4, v0, v2

    .line 219846
    :cond_1
    const/4 v2, -0x1

    .line 219847
    invoke-static {p2}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    .line 219848
    if-eqz v3, :cond_2

    .line 219849
    invoke-static {p2}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v2

    :cond_2
    move-object v0, p0

    move-object v1, p1

    .line 219850
    invoke-virtual/range {v0 .. v6}, LX/1EQ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ILjava/lang/String;JI)V

    .line 219851
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V
    .locals 6

    .prologue
    .line 219812
    iget-object v0, p0, LX/1EQ;->f:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 219813
    :try_start_0
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0nA;->a(LX/0nD;)V

    .line 219814
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219815
    const-string v0, "image_loading_state"

    iget-object v2, p0, LX/1EQ;->b:LX/1BK;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 219816
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219817
    :cond_0
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 219818
    const-string v0, "time_since_fetched"

    iget-object v2, p0, LX/1EQ;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 219819
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219820
    :cond_1
    const-string v0, "radio_type"

    iget-object v2, p0, LX/1EQ;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->m()Ljava/lang/String;

    move-result-object v2

    .line 219821
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219822
    invoke-static {p1}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    .line 219823
    if-eqz v0, :cond_2

    .line 219824
    const-string v2, "client_viewstate_position"

    invoke-static {p1}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 219825
    invoke-static {v1, v2, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219826
    const-string v2, "client_bump_state"

    .line 219827
    invoke-static {v1, v2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219828
    :cond_2
    invoke-direct {p0}, LX/1EQ;->a()Ljava/lang/String;

    move-result-object v0

    .line 219829
    if-eqz v0, :cond_3

    .line 219830
    const-string v2, "enabled_features"

    .line 219831
    invoke-static {v1, v2, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 219832
    :cond_3
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 219833
    invoke-virtual {v1, v0}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 219834
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 219835
    iput-object v0, p2, LX/21A;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219836
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 219837
    return-void

    .line 219838
    :catch_0
    move-exception v0

    .line 219839
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219840
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/0nA;->a()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 219807
    iget-object v0, p0, LX/1EQ;->a:LX/0Zb;

    const-string v1, "share_clicked"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 219808
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219809
    invoke-virtual {v0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "user_id"

    invoke-virtual {v1, v2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "shareable_id"

    invoke-virtual {v1, v2, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 219810
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 219811
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 219802
    iget-object v0, p0, LX/1EQ;->a:LX/0Zb;

    const-string v1, "feed_share_action"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 219803
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219804
    invoke-virtual {v0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "share_type"

    const-string v3, "share_button_tapped"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "user_id"

    invoke-virtual {v1, v2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "shareable_id"

    invoke-virtual {v1, v2, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 219805
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 219806
    :cond_0
    return-void
.end method
