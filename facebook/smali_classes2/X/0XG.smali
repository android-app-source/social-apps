.class public final enum LX/0XG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0XG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0XG;

.field public static final enum ADDRESS_BOOK:LX/0XG;

.field public static final enum EMAIL:LX/0XG;

.field public static final enum FACEBOOK:LX/0XG;

.field public static final enum FACEBOOK_CONTACT:LX/0XG;

.field public static final enum FACEBOOK_OBJECT:LX/0XG;

.field public static final enum PHONE_NUMBER:LX/0XG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77707
    new-instance v0, LX/0XG;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v3}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->FACEBOOK:LX/0XG;

    .line 77708
    new-instance v0, LX/0XG;

    const-string v1, "ADDRESS_BOOK"

    invoke-direct {v0, v1, v4}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    .line 77709
    new-instance v0, LX/0XG;

    const-string v1, "PHONE_NUMBER"

    invoke-direct {v0, v1, v5}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->PHONE_NUMBER:LX/0XG;

    .line 77710
    new-instance v0, LX/0XG;

    const-string v1, "FACEBOOK_OBJECT"

    invoke-direct {v0, v1, v6}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->FACEBOOK_OBJECT:LX/0XG;

    .line 77711
    new-instance v0, LX/0XG;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v7}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->EMAIL:LX/0XG;

    .line 77712
    new-instance v0, LX/0XG;

    const-string v1, "FACEBOOK_CONTACT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0XG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    .line 77713
    const/4 v0, 0x6

    new-array v0, v0, [LX/0XG;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    aput-object v1, v0, v3

    sget-object v1, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    aput-object v1, v0, v4

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    aput-object v1, v0, v5

    sget-object v1, LX/0XG;->FACEBOOK_OBJECT:LX/0XG;

    aput-object v1, v0, v6

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    aput-object v2, v0, v1

    sput-object v0, LX/0XG;->$VALUES:[LX/0XG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77714
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0XG;
    .locals 1

    .prologue
    .line 77715
    const-class v0, LX/0XG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0XG;

    return-object v0
.end method

.method public static values()[LX/0XG;
    .locals 1

    .prologue
    .line 77716
    sget-object v0, LX/0XG;->$VALUES:[LX/0XG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0XG;

    return-object v0
.end method


# virtual methods
.method public final isPhoneContact()Z
    .locals 1

    .prologue
    .line 77717
    sget-object v0, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0XG;->EMAIL:LX/0XG;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
