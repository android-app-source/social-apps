.class public LX/1UT;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Ya;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a2;",
        ">;",
        "LX/0Ya;",
        "LX/1OO",
        "<",
        "LX/1a2;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/1Cw;

.field public d:Landroid/support/v7/widget/RecyclerView;

.field private e:Z

.field public f:Z

.field private g:I

.field private final h:Landroid/database/DataSetObserver;

.field private final i:LX/1OD;


# direct methods
.method public constructor <init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 256067
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/1UT;-><init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;Z)V

    .line 256068
    return-void
.end method

.method private constructor <init>(LX/1Cw;Landroid/support/v7/widget/RecyclerView;Z)V
    .locals 2

    .prologue
    .line 256058
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 256059
    new-instance v0, LX/1UU;

    invoke-direct {v0, p0}, LX/1UU;-><init>(LX/1UT;)V

    iput-object v0, p0, LX/1UT;->h:Landroid/database/DataSetObserver;

    .line 256060
    new-instance v0, LX/1UV;

    invoke-direct {v0, p0}, LX/1UV;-><init>(LX/1UT;)V

    iput-object v0, p0, LX/1UT;->i:LX/1OD;

    .line 256061
    const-class v0, LX/1UT;

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/1UT;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 256062
    invoke-virtual {p0, p3}, LX/1OM;->a(Z)V

    .line 256063
    iput-object p1, p0, LX/1UT;->c:LX/1Cw;

    .line 256064
    iput-object p2, p0, LX/1UT;->d:Landroid/support/v7/widget/RecyclerView;

    .line 256065
    iget-object v0, p0, LX/1UT;->i:LX/1OD;

    invoke-super {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 256066
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/1UT;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object v0, p1, LX/1UT;->a:LX/0Sh;

    iput-object p0, p1, LX/1UT;->b:LX/03V;

    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 256057
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 256044
    new-instance v0, LX/1a2;

    iget-object v1, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v1, p2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1a2;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1OD;)V
    .locals 2

    .prologue
    .line 256051
    iget v0, p0, LX/1UT;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1UT;->g:I

    .line 256052
    invoke-super {p0, p1}, LX/1OM;->a(LX/1OD;)V

    .line 256053
    iget-boolean v0, p0, LX/1UT;->e:Z

    if-nez v0, :cond_0

    .line 256054
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    iget-object v1, p0, LX/1UT;->h:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 256055
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1UT;->e:Z

    .line 256056
    :cond_0
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 256069
    check-cast p1, LX/1a2;

    .line 256070
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v2, p0, LX/1UT;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-interface {v0, p2, v1, v2}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 256071
    return-void
.end method

.method public final b(LX/1OD;)V
    .locals 2

    .prologue
    .line 256045
    iget v0, p0, LX/1UT;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1UT;->g:I

    .line 256046
    invoke-super {p0, p1}, LX/1OM;->b(LX/1OD;)V

    .line 256047
    iget-boolean v0, p0, LX/1UT;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/1UT;->g:I

    if-nez v0, :cond_0

    .line 256048
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    iget-object v1, p0, LX/1UT;->h:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 256049
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1UT;->e:Z

    .line 256050
    :cond_0
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 256043
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 256042
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public ii_()I
    .locals 1

    .prologue
    .line 256041
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 256040
    iget-object v0, p0, LX/1UT;->c:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    return v0
.end method
