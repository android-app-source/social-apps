.class public LX/1HB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1HC;


# instance fields
.field private final a:LX/1HC;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2ux;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/03V;


# direct methods
.method public constructor <init>(LX/1HC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HC;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2ux;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 226535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226536
    iput-object p1, p0, LX/1HB;->a:LX/1HC;

    .line 226537
    iput-object p2, p0, LX/1HB;->b:LX/0Ot;

    .line 226538
    iput-object p3, p0, LX/1HB;->c:LX/0Ot;

    .line 226539
    iput-object p4, p0, LX/1HB;->d:LX/0Ot;

    .line 226540
    iput-object p5, p0, LX/1HB;->e:LX/0Ot;

    .line 226541
    iput-object p6, p0, LX/1HB;->f:LX/0Or;

    .line 226542
    iput-object p7, p0, LX/1HB;->g:LX/03V;

    .line 226543
    return-void
.end method


# virtual methods
.method public final a(LX/1Gf;)LX/1Ha;
    .locals 8

    .prologue
    .line 226544
    new-instance v0, LX/1HZ;

    iget-object v1, p0, LX/1HB;->a:LX/1HC;

    invoke-interface {v1, p1}, LX/1HC;->a(LX/1Gf;)LX/1Ha;

    move-result-object v1

    iget-object v2, p0, LX/1HB;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Hz;

    iget-object v3, p0, LX/1HB;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Hz;

    iget-object v4, p0, LX/1HB;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Hz;

    iget-object v5, p0, LX/1HB;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1IW;

    iget-object v6, p0, LX/1HB;->f:LX/0Or;

    iget-object v7, p0, LX/1HB;->g:LX/03V;

    invoke-direct/range {v0 .. v7}, LX/1HZ;-><init>(LX/1Ha;LX/1Hz;LX/1Hz;LX/1Hz;LX/1IW;LX/0Or;LX/03V;)V

    return-object v0
.end method
