.class public final LX/0bp;
.super LX/0bi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0bi",
        "<",
        "LX/2KA;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0bp;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2KA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 87324
    const/16 v0, 0x232

    invoke-direct {p0, p1, v0}, LX/0bi;-><init>(LX/0Ot;I)V

    .line 87325
    return-void
.end method

.method public static a(LX/0QB;)LX/0bp;
    .locals 4

    .prologue
    .line 87308
    sget-object v0, LX/0bp;->b:LX/0bp;

    if-nez v0, :cond_1

    .line 87309
    const-class v1, LX/0bp;

    monitor-enter v1

    .line 87310
    :try_start_0
    sget-object v0, LX/0bp;->b:LX/0bp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87311
    if-eqz v2, :cond_0

    .line 87312
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87313
    new-instance v3, LX/0bp;

    const/16 p0, 0x4af

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0bp;-><init>(LX/0Ot;)V

    .line 87314
    move-object v0, v3

    .line 87315
    sput-object v0, LX/0bp;->b:LX/0bp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87316
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87317
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87318
    :cond_1
    sget-object v0, LX/0bp;->b:LX/0bp;

    return-object v0

    .line 87319
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Uh;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 87321
    check-cast p3, LX/2KA;

    .line 87322
    invoke-static {p3}, LX/2KA;->a$redex0(LX/2KA;)V

    .line 87323
    return-void
.end method
