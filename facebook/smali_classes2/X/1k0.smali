.class public final LX/1k0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 301145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I
    .locals 3
    .param p1    # Lcom/facebook/flatbuffers/MutableFlattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 301135
    if-nez p1, :cond_0

    .line 301136
    const/4 v0, 0x0

    .line 301137
    :goto_0
    return v0

    .line 301138
    :cond_0
    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301139
    :cond_1
    invoke-virtual {p0, p1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    goto :goto_0

    .line 301140
    :cond_2
    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 301141
    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 301142
    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v2

    if-eq v2, v1, :cond_3

    .line 301143
    invoke-virtual {p0, p1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    goto :goto_0

    .line 301144
    :cond_3
    invoke-virtual {p0, v0, v1}, LX/186;->a(Ljava/nio/ByteBuffer;I)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/186;Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ">(",
            "LX/186;",
            "Ljava/util/List",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 301096
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 301097
    :cond_0
    :goto_0
    return v0

    .line 301098
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v2, v1, [I

    move v1, v0

    .line 301099
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 301100
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    aput v0, v2, v1

    .line 301101
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 301102
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0Px;LX/1jy;)LX/0Pz;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")",
            "LX/0Pz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 301120
    const/4 v1, 0x0

    .line 301121
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    move-object v4, v1

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jT;

    .line 301122
    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v6

    .line 301123
    add-int/lit8 v3, v2, 0x1

    const/4 v7, 0x0

    .line 301124
    if-eq v0, v6, :cond_0

    if-nez v4, :cond_0

    .line 301125
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 301126
    invoke-virtual {p0, v7, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v8

    .line 301127
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    :goto_1
    if-ge v7, v9, :cond_0

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    .line 301128
    invoke-virtual {v4, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 301129
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 301130
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v6, :cond_1

    .line 301131
    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 301132
    :cond_1
    move-object v2, v4

    .line 301133
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v4, v2

    move v2, v3

    goto :goto_0

    .line 301134
    :cond_2
    return-object v4
.end method

.method public static a(LX/0jT;LX/0jT;)LX/0jT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 301117
    if-nez p0, :cond_0

    .line 301118
    invoke-interface {p1}, LX/0jT;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jT;

    .line 301119
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;
    .locals 4

    .prologue
    .line 301109
    invoke-static {p2, p3, p4, p5}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p7, :cond_0

    .line 301110
    new-instance p7, LX/3Si;

    invoke-direct {p7}, LX/3Si;-><init>()V

    .line 301111
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, LX/2uF;->a(II)LX/2uF;

    move-result-object v0

    .line 301112
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v3, v1, LX/1vs;->b:I

    iget v1, v1, LX/1vs;->c:I

    .line 301113
    invoke-virtual {p7, v2, v3, v1}, LX/3Si;->c(LX/15i;II)LX/3Si;

    goto :goto_0

    .line 301114
    :cond_0
    if-eqz p7, :cond_1

    if-eqz p5, :cond_1

    .line 301115
    invoke-virtual {p7, p4, p5, p6}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 301116
    :cond_1
    return-object p7
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 301103
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301104
    instance-of v0, p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    if-eqz v0, :cond_0

    .line 301105
    check-cast p0, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 301106
    if-eqz v0, :cond_0

    .line 301107
    invoke-virtual {v0, p1}, LX/15i;->a(Ljava/lang/String;)V

    .line 301108
    :cond_0
    return-void
.end method
