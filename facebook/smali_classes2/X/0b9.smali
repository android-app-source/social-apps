.class public final LX/0b9;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b1",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        "LX/BaK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0b3;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/BaK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 86690
    invoke-direct {p0, p1, p2}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86691
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86692
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 86693
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    check-cast p2, LX/BaK;

    .line 86694
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86695
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v0, v1

    .line 86696
    if-nez v0, :cond_1

    .line 86697
    :cond_0
    :goto_0
    return-void

    .line 86698
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 86699
    iget-object v1, p2, LX/BaK;->a:LX/1EY;

    .line 86700
    iget-object v2, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->d:Ljava/lang/String;

    move-object v2, v2

    .line 86701
    iget-object v3, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v3, v3

    .line 86702
    invoke-virtual {v1, v2, v3}, LX/1EY;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 86703
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 86704
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86705
    iget-object v1, p2, LX/BaK;->b:LX/1EW;

    invoke-virtual {v0}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 86706
    iget v2, v1, LX/1EW;->e:I

    sub-int/2addr v2, v0

    iput v2, v1, LX/1EW;->e:I

    .line 86707
    iget-object v2, v1, LX/1EW;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0131

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, p1

    invoke-virtual {v2, v3, v0, p0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/1EW;->a(LX/1EW;Ljava/lang/String;)Z

    .line 86708
    goto :goto_0
.end method
