.class public LX/1UR;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Vf;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/0Vf;",
        "LX/1OO",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1UW;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1OO;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1XN;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>(LX/0Px;Z)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1OO;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 256001
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 256002
    iput-object p1, p0, LX/1UR;->b:LX/0Px;

    .line 256003
    new-instance v0, LX/1UW;

    invoke-direct {v0, p1, p2}, LX/1UW;-><init>(Ljava/util/List;Z)V

    iput-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 256004
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1UR;->c:Ljava/util/List;

    move v1, v2

    .line 256005
    :goto_0
    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 256006
    new-instance v3, LX/1XN;

    invoke-direct {v3, p0, v1}, LX/1XN;-><init>(LX/1UR;I)V

    .line 256007
    iget-object v0, p0, LX/1UR;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256008
    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    invoke-interface {v0, v3}, LX/1OQ;->a(LX/1OD;)V

    .line 256009
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 256010
    :cond_0
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    const/4 v2, 0x0

    .line 256011
    iget-object v1, v0, LX/1UW;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    move v3, v2

    :goto_1
    if-ge v3, p1, :cond_2

    iget-object v1, v0, LX/1UW;->a:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1OO;

    .line 256012
    invoke-interface {v1}, LX/1OQ;->eC_()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 256013
    :goto_2
    move v0, v1

    .line 256014
    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 256015
    new-instance v0, LX/1XO;

    invoke-direct {v0, p0}, LX/1XO;-><init>(LX/1UR;)V

    invoke-virtual {p0, v0}, LX/1OM;->a(LX/1OD;)V

    .line 256016
    return-void

    .line 256017
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 256018
    :cond_2
    const/4 v1, 0x1

    goto :goto_2
.end method

.method private g(I)V
    .locals 4

    .prologue
    .line 256019
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    .line 256020
    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 256021
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "position: %d, total count: %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 256022
    :cond_1
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 6

    .prologue
    .line 255945
    invoke-direct {p0, p1}, LX/1UR;->g(I)V

    .line 255946
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(I)V

    .line 255947
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255948
    iget-object v2, v0, LX/1UW;->a:LX/0Px;

    iget v3, v0, LX/1UW;->e:I

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1OO;

    iget v3, v0, LX/1UW;->g:I

    invoke-interface {v2, v3}, LX/1OQ;->C_(I)J

    move-result-wide v4

    .line 255949
    const-wide/32 v2, 0x186a0

    cmp-long v2, v4, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 255950
    const v2, 0x186a0

    iget v3, v0, LX/1UW;->e:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v2, v4

    move-wide v0, v2

    .line 255951
    return-wide v0

    .line 255952
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/1OO;)I
    .locals 1

    .prologue
    .line 255997
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(LX/1OO;)V

    .line 255998
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255999
    iget p0, v0, LX/1UW;->f:I

    move v0, p0

    .line 256000
    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 255993
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p2}, LX/1UW;->c(I)V

    .line 255994
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255995
    invoke-virtual {v0}, LX/1UW;->b()I

    move-result v1

    sub-int v1, p2, v1

    move v0, v1

    .line 255996
    iget-object v1, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v1}, LX/1UW;->h()LX/1OO;

    move-result-object v1

    invoke-interface {v1, p1, v0}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 255987
    invoke-direct {p0, p2}, LX/1UR;->g(I)V

    .line 255988
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p2}, LX/1UW;->a(I)V

    .line 255989
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255990
    iget v1, v0, LX/1UW;->g:I

    move v0, v1

    .line 255991
    iget-object v1, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v1}, LX/1UW;->h()LX/1OO;

    move-result-object v1

    invoke-interface {v1, p1, v0}, LX/1OQ;->a(LX/1a1;I)V

    .line 255992
    return-void
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 255983
    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    .line 255984
    invoke-interface {v0, p1}, LX/1OQ;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 255985
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255986
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 255979
    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    .line 255980
    invoke-interface {v0, p1}, LX/1OQ;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 255981
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 255982
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 3

    .prologue
    .line 256023
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/1UR;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 256024
    iget-object v0, p0, LX/1UR;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OO;

    iget-object v1, p0, LX/1UR;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1OD;

    invoke-interface {v0, v1}, LX/1OQ;->b(LX/1OD;)V

    .line 256025
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 256026
    :cond_0
    iget-object v0, p0, LX/1UR;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 256027
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->dispose()V

    .line 256028
    return-void
.end method

.method public final e(I)LX/1OO;
    .locals 1

    .prologue
    .line 255976
    invoke-direct {p0, p1}, LX/1UR;->g(I)V

    .line 255977
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(I)V

    .line 255978
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->h()LX/1OO;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)I
    .locals 1

    .prologue
    .line 255971
    invoke-direct {p0, p1}, LX/1UR;->g(I)V

    .line 255972
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(I)V

    .line 255973
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255974
    iget p0, v0, LX/1UW;->g:I

    move v0, p0

    .line 255975
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 255966
    invoke-direct {p0, p1}, LX/1UR;->g(I)V

    .line 255967
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(I)V

    .line 255968
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->h()LX/1OO;

    move-result-object v0

    iget-object v1, p0, LX/1UR;->a:LX/1UW;

    .line 255969
    iget p0, v1, LX/1UW;->g:I

    move v1, p0

    .line 255970
    invoke-interface {v0, v1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 255961
    invoke-direct {p0, p1}, LX/1UR;->g(I)V

    .line 255962
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0, p1}, LX/1UW;->a(I)V

    .line 255963
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->h()LX/1OO;

    move-result-object v0

    iget-object v1, p0, LX/1UR;->a:LX/1UW;

    .line 255964
    iget p1, v1, LX/1UW;->g:I

    move v1, p1

    .line 255965
    invoke-interface {v0, v1}, LX/1OP;->getItemViewType(I)I

    move-result v0

    iget-object v1, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v1}, LX/1UW;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 255957
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255958
    iget-boolean p0, v0, LX/1UW;->d:Z

    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 255959
    iget p0, v0, LX/1UW;->i:I

    move v0, p0

    .line 255960
    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 255954
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    .line 255955
    iget p0, v0, LX/1UW;->h:I

    move v0, p0

    .line 255956
    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 255953
    iget-object v0, p0, LX/1UR;->a:LX/1UW;

    invoke-virtual {v0}, LX/1UW;->isDisposed()Z

    move-result v0

    return v0
.end method
