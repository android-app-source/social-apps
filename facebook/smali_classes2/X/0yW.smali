.class public LX/0yW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile C:LX/0yW;

.field public static a:LX/0Tn;

.field public static b:LX/0Tn;

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/0Uh;

.field public B:LX/0ad;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field public i:J

.field private j:J

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:J

.field public p:Z

.field public q:Z

.field public final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/12K;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0SG;

.field public final t:Ljava/util/concurrent/ExecutorService;

.field private u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0yX;

.field private w:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/00H;

.field public z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 164924
    const-class v0, LX/0yW;

    sput-object v0, LX/0yW;->c:Ljava/lang/Class;

    .line 164925
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "user_data_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0yW;->a:LX/0Tn;

    .line 164926
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "last_state_change_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0yW;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0yX;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/00H;LX/0Ot;LX/0Uh;LX/0ad;LX/0SG;)V
    .locals 5
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/datacheck/annotations/IsInAndroidBalanceDetection;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0yX;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/00H;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x3

    const-wide/16 v2, 0x0

    .line 164927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164928
    iput v1, p0, LX/0yW;->d:I

    .line 164929
    iput v1, p0, LX/0yW;->e:I

    .line 164930
    const/16 v0, 0x8

    iput v0, p0, LX/0yW;->f:I

    .line 164931
    const/4 v0, 0x4

    iput v0, p0, LX/0yW;->g:I

    .line 164932
    iput v1, p0, LX/0yW;->h:I

    .line 164933
    iput-wide v2, p0, LX/0yW;->i:J

    .line 164934
    iput-wide v2, p0, LX/0yW;->j:J

    .line 164935
    iput-wide v2, p0, LX/0yW;->k:J

    .line 164936
    iput-wide v2, p0, LX/0yW;->l:J

    .line 164937
    iput-wide v2, p0, LX/0yW;->m:J

    .line 164938
    iput-wide v2, p0, LX/0yW;->n:J

    .line 164939
    iput-wide v2, p0, LX/0yW;->o:J

    .line 164940
    iput-boolean v4, p0, LX/0yW;->p:Z

    .line 164941
    iput-boolean v4, p0, LX/0yW;->q:Z

    .line 164942
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/0yW;->r:Ljava/util/Set;

    .line 164943
    iput-object p1, p0, LX/0yW;->t:Ljava/util/concurrent/ExecutorService;

    .line 164944
    iput-object p2, p0, LX/0yW;->u:LX/0Or;

    .line 164945
    iput-object p3, p0, LX/0yW;->v:LX/0yX;

    .line 164946
    iput-object p4, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 164947
    iput-object p5, p0, LX/0yW;->x:LX/0Ot;

    .line 164948
    iput-object p6, p0, LX/0yW;->y:LX/00H;

    .line 164949
    iput-object p7, p0, LX/0yW;->z:LX/0Ot;

    .line 164950
    iput-object p8, p0, LX/0yW;->A:LX/0Uh;

    .line 164951
    iput-object p9, p0, LX/0yW;->B:LX/0ad;

    .line 164952
    iput-object p10, p0, LX/0yW;->s:LX/0SG;

    .line 164953
    return-void
.end method

.method public static a(LX/0QB;)LX/0yW;
    .locals 14

    .prologue
    .line 164954
    sget-object v0, LX/0yW;->C:LX/0yW;

    if-nez v0, :cond_1

    .line 164955
    const-class v1, LX/0yW;

    monitor-enter v1

    .line 164956
    :try_start_0
    sget-object v0, LX/0yW;->C:LX/0yW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164957
    if-eqz v2, :cond_0

    .line 164958
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164959
    new-instance v3, LX/0yW;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v5, 0x15b1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 164960
    new-instance v8, LX/0yX;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v7, 0x13c1

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v8, v6, v9, v7}, LX/0yX;-><init>(LX/0Zb;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 164961
    move-object v6, v8

    .line 164962
    check-cast v6, LX/0yX;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, LX/00H;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/00H;

    const/16 v10, 0x38f0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v13

    check-cast v13, LX/0SG;

    invoke-direct/range {v3 .. v13}, LX/0yW;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0yX;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/00H;LX/0Ot;LX/0Uh;LX/0ad;LX/0SG;)V

    .line 164963
    move-object v0, v3

    .line 164964
    sput-object v0, LX/0yW;->C:LX/0yW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164965
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164966
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164967
    :cond_1
    sget-object v0, LX/0yW;->C:LX/0yW;

    return-object v0

    .line 164968
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0yW;LX/2XZ;)V
    .locals 3

    .prologue
    .line 164970
    iget-object v0, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164971
    :goto_0
    return-void

    .line 164972
    :cond_0
    iget-object v0, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0yW;->a:LX/0Tn;

    invoke-virtual {p1}, LX/2XZ;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public static a(LX/0yW;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 164973
    iget-object v0, p0, LX/0yW;->v:LX/0yX;

    iget-wide v2, p0, LX/0yW;->l:J

    iget-wide v4, p0, LX/0yW;->k:J

    iget-wide v6, p0, LX/0yW;->n:J

    iget-wide v8, p0, LX/0yW;->m:J

    move-object v1, p1

    .line 164974
    iget-object p0, v0, LX/0yX;->e:LX/0Zb;

    const/4 p1, 0x0

    invoke-interface {p0, v1, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 164975
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 164976
    const-string p1, "free_failure_pings"

    invoke-virtual {p0, p1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 164977
    const-string p1, "free_success_pings"

    invoke-virtual {p0, p1, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 164978
    const-string p1, "standard_failure_pings"

    invoke-virtual {p0, p1, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 164979
    const-string p1, "standard_success_pings"

    invoke-virtual {p0, p1, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 164980
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 164981
    :cond_0
    return-void
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 164982
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_0

    const/16 v0, 0x131

    if-le p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x1f4

    if-lt p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/0yW;LX/2XZ;ILjava/lang/String;)V
    .locals 12

    .prologue
    .line 164983
    iget-object v0, p0, LX/0yW;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0yW;->y:LX/00H;

    .line 164984
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 164985
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/0yW;->y:LX/00H;

    .line 164986
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 164987
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-eq v0, v1, :cond_1

    .line 164988
    :cond_0
    return-void

    .line 164989
    :cond_1
    invoke-virtual {p0}, LX/0yW;->a()LX/2XZ;

    move-result-object v2

    .line 164990
    iget-object v1, p0, LX/0yW;->v:LX/0yX;

    iget-wide v6, p0, LX/0yW;->i:J

    iget-wide v8, p0, LX/0yW;->j:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0yW;->a(Z)J

    move-result-wide v10

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v1 .. v11}, LX/0yX;->a(LX/2XZ;LX/2XZ;ILjava/lang/String;JJJ)V

    .line 164991
    invoke-static {p0, p1}, LX/0yW;->a(LX/0yW;LX/2XZ;)V

    .line 164992
    iget-object v0, p0, LX/0yW;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12K;

    .line 164993
    invoke-interface {v0, p1}, LX/12K;->a(LX/2XZ;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/0yW;ZLX/1lF;)V
    .locals 2

    .prologue
    .line 164994
    iget-object v0, p0, LX/0yW;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/12K;

    .line 164995
    invoke-interface {v0, p1, p2}, LX/12K;->a(ZLX/1lF;)V

    goto :goto_0

    .line 164996
    :cond_0
    return-void
.end method

.method private b(LX/1lF;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 164997
    invoke-virtual {p1}, LX/1lF;->name()Ljava/lang/String;

    .line 164998
    iget-object v1, p0, LX/0yW;->v:LX/0yX;

    const-string v2, "zb_redirect_initiated"

    invoke-virtual {v1, v2}, LX/0yX;->a(Ljava/lang/String;)V

    .line 164999
    sget-object v1, LX/1lF;->UPSELL_FLOW_STARTING:LX/1lF;

    if-eq p1, v1, :cond_0

    sget-object v1, LX/1lF;->UPSELL_FLOW_FINISHING:LX/1lF;

    if-ne p1, v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 165000
    :goto_0
    iget-object v2, p0, LX/0yW;->B:LX/0ad;

    sget-char v3, LX/49i;->i:C

    const-string v4, ""

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 165001
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 165002
    sget-object v2, LX/0yW;->c:Ljava/lang/Class;

    const-string v3, "Redirect ping failed because QE did not return a valid URL"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 165003
    if-eqz v1, :cond_1

    .line 165004
    invoke-static {p0, v0, p1}, LX/0yW;->a$redex0(LX/0yW;ZLX/1lF;)V

    .line 165005
    :cond_1
    iput-boolean v0, p0, LX/0yW;->q:Z

    .line 165006
    :goto_1
    return-void

    :cond_2
    move v1, v0

    .line 165007
    goto :goto_0

    .line 165008
    :cond_3
    iget-object v0, p0, LX/0yW;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    .line 165009
    iget-object v3, v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->d:LX/0TD;

    new-instance v4, LX/643;

    invoke-direct {v4, v0, v2, v1}, LX/643;-><init>(Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;Ljava/lang/String;Z)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 165010
    new-instance v2, LX/646;

    invoke-direct {v2, p0, p1, v1}, LX/646;-><init>(LX/0yW;LX/1lF;Z)V

    iget-object v1, p0, LX/0yW;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method private e()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x3

    .line 164911
    iget-wide v0, p0, LX/0yW;->j:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/0yW;->i:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized j(LX/0yW;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 164912
    monitor-enter p0

    :try_start_0
    const-string v1, "confirm_free_tier"

    invoke-static {p0, v1}, LX/0yW;->a(LX/0yW;Ljava/lang/String;)V

    .line 164913
    invoke-static {p0}, LX/0yW;->n(LX/0yW;)V

    move v1, v0

    .line 164914
    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 164915
    iget-object v2, p0, LX/0yW;->z:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    invoke-virtual {v2}, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 164916
    new-instance v3, LX/648;

    invoke-direct {v3, p0}, LX/648;-><init>(LX/0yW;)V

    iget-object v4, p0, LX/0yW;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 164917
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164918
    :cond_0
    :goto_1
    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 164919
    iget-object v1, p0, LX/0yW;->z:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    invoke-virtual {v1}, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 164920
    new-instance v2, LX/649;

    invoke-direct {v2, p0}, LX/649;-><init>(LX/0yW;)V

    iget-object v3, p0, LX/0yW;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164921
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 164922
    :cond_1
    monitor-exit p0

    return-void

    .line 164923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized k(LX/0yW;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x8

    const-wide/16 v2, 0x1

    .line 164833
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/0yW;->k:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/0yW;->n:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_2

    .line 164834
    :cond_0
    iget-wide v0, p0, LX/0yW;->k:J

    iget-wide v2, p0, LX/0yW;->l:J

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/0yW;->n:J

    add-long/2addr v0, v2

    iget-wide v2, p0, LX/0yW;->m:J

    add-long/2addr v0, v2

    const-wide/16 v2, 0xc

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 164835
    const-string v0, "confirm_free_tier_failure"

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;Ljava/lang/String;)V

    .line 164836
    invoke-static {p0}, LX/0yW;->m(LX/0yW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164837
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 164838
    :cond_2
    :try_start_1
    invoke-virtual {p0}, LX/0yW;->a()LX/2XZ;

    move-result-object v0

    sget-object v1, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    if-eq v0, v1, :cond_1

    .line 164839
    iget-wide v0, p0, LX/0yW;->k:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    iget-wide v0, p0, LX/0yW;->n:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_3

    .line 164840
    const-string v0, "confirm_free_tier_success"

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;Ljava/lang/String;)V

    .line 164841
    sget-object v0, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/0yW;->a$redex0(LX/0yW;LX/2XZ;ILjava/lang/String;)V

    .line 164842
    :goto_1
    invoke-static {p0}, LX/0yW;->m(LX/0yW;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164843
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164844
    :cond_3
    :try_start_2
    const-string v0, "confirm_free_tier_failure"

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static declared-synchronized m(LX/0yW;)V
    .locals 2

    .prologue
    .line 164845
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, LX/0yW;->i:J

    .line 164846
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->j:J

    .line 164847
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0yW;->p:Z

    .line 164848
    invoke-static {p0}, LX/0yW;->n(LX/0yW;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164849
    monitor-exit p0

    return-void

    .line 164850
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized n(LX/0yW;)V
    .locals 2

    .prologue
    .line 164905
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, LX/0yW;->l:J

    .line 164906
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->k:J

    .line 164907
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->n:J

    .line 164908
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164909
    monitor-exit p0

    return-void

    .line 164910
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Z)J
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 164851
    iget-object v2, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 164852
    :cond_0
    :goto_0
    return-wide v0

    .line 164853
    :cond_1
    iget-object v2, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yW;->b:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 164854
    iget-object v4, p0, LX/0yW;->s:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 164855
    if-eqz p1, :cond_2

    .line 164856
    iget-object v6, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/0yW;->b:LX/0Tn;

    invoke-interface {v6, v7, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 164857
    :cond_2
    cmp-long v6, v2, v0

    if-lez v6, :cond_0

    sub-long v0, v4, v2

    goto :goto_0
.end method

.method public final a()LX/2XZ;
    .locals 4

    .prologue
    .line 164858
    iget-object v0, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164859
    sget-object v0, LX/2XZ;->NOT_CONNECTED:LX/2XZ;

    .line 164860
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/2XZ;->values()[LX/2XZ;

    move-result-object v0

    iget-object v1, p0, LX/0yW;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0yW;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 164861
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0yW;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 164862
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164863
    :cond_1
    :try_start_1
    invoke-static {p1}, LX/0yW;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164864
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164865
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164866
    :cond_2
    :try_start_2
    iget-wide v0, p0, LX/0yW;->j:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0yW;->j:J

    .line 164867
    invoke-direct {p0}, LX/0yW;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164868
    sget-object v0, LX/2XZ;->NOT_CONNECTED:LX/2XZ;

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;LX/2XZ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(LX/12K;)V
    .locals 1
    .param p1    # LX/12K;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 164869
    if-eqz p1, :cond_0

    .line 164870
    iget-object v0, p0, LX/0yW;->r:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 164871
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(LX/1lF;)V
    .locals 3

    .prologue
    .line 164872
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/1lG;->a:[I

    invoke-virtual {p1}, LX/1lF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 164873
    :goto_0
    iget-boolean v0, p0, LX/0yW;->q:Z

    if-nez v0, :cond_0

    .line 164874
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yW;->q:Z

    .line 164875
    invoke-direct {p0, p1}, LX/0yW;->b(LX/1lF;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164876
    :cond_0
    monitor-exit p0

    return-void

    .line 164877
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/0yW;->B:LX/0ad;

    sget-short v1, LX/1lH;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164878
    iget-object v0, p0, LX/0yW;->v:LX/0yX;

    const-string v1, "zb_load_fail_triggered"

    invoke-virtual {v0, v1}, LX/0yX;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164879
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164880
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/0yW;->B:LX/0ad;

    sget-short v1, LX/1lH;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164881
    iget-object v0, p0, LX/0yW;->v:LX/0yX;

    const-string v1, "zb_load_start_triggered"

    invoke-virtual {v0, v1}, LX/0yX;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 164882
    :pswitch_2
    iget-object v0, p0, LX/0yW;->B:LX/0ad;

    sget-short v1, LX/1lH;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164883
    iget-object v0, p0, LX/0yW;->v:LX/0yX;

    const-string v1, "zb_app_foreground_triggered"

    invoke-virtual {v0, v1}, LX/0yX;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized b(ILjava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3

    const-wide/16 v4, 0x0

    .line 164884
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0yW;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 164885
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 164886
    :cond_1
    :try_start_1
    invoke-static {p1}, LX/0yW;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164887
    sget-object v0, LX/2XZ;->CONNECTED:LX/2XZ;

    invoke-static {p0, v0, p1, p2}, LX/0yW;->a$redex0(LX/0yW;LX/2XZ;ILjava/lang/String;)V

    .line 164888
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->i:J

    .line 164889
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0yW;->o:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164890
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 164891
    :cond_2
    :try_start_2
    iget-wide v0, p0, LX/0yW;->i:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0yW;->i:J

    .line 164892
    iget-wide v0, p0, LX/0yW;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    iget-wide v0, p0, LX/0yW;->i:J

    const-wide/16 v2, 0x3

    rem-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    invoke-virtual {p0}, LX/0yW;->a()LX/2XZ;

    move-result-object v0

    sget-object v1, LX/2XZ;->FREE_TIER_ONLY:LX/2XZ;

    if-eq v0, v1, :cond_4

    .line 164893
    iget-object v0, p0, LX/0yW;->A:LX/0Uh;

    const/16 v1, 0x46e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v0, p0, LX/0yW;->o:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_3

    .line 164894
    iget-boolean v0, p0, LX/0yW;->q:Z

    if-nez v0, :cond_0

    .line 164895
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yW;->q:Z

    .line 164896
    sget-object v0, LX/1lF;->REQUEST_FAILED:LX/1lF;

    invoke-direct {p0, v0}, LX/0yW;->b(LX/1lF;)V

    goto :goto_0

    .line 164897
    :cond_3
    const-string v0, "confirm_free_tier_initial_check"

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;Ljava/lang/String;)V

    .line 164898
    invoke-static {p0}, LX/0yW;->m(LX/0yW;)V

    .line 164899
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0yW;->p:Z

    .line 164900
    iget-object v0, p0, LX/0yW;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;

    invoke-virtual {v0}, Lcom/facebook/zero/datacheck/ZeroDataCheckerRequestMaker;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 164901
    new-instance v1, LX/645;

    invoke-direct {v1, p0}, LX/645;-><init>(LX/0yW;)V

    iget-object v2, p0, LX/0yW;->t:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 164902
    goto :goto_0

    .line 164903
    :cond_4
    invoke-direct {p0}, LX/0yW;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164904
    sget-object v0, LX/2XZ;->NOT_CONNECTED:LX/2XZ;

    invoke-static {p0, v0}, LX/0yW;->a(LX/0yW;LX/2XZ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 164829
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0yW;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/0yW;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NullUriResponse"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164830
    iget-wide v0, p0, LX/0yW;->i:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/0yW;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164831
    monitor-exit p0

    return-void

    .line 164832
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
