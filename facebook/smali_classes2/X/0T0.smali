.class public LX/0T0;
.super LX/0T1;
.source ""

# interfaces
.implements LX/0T3;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62526
    invoke-direct {p0}, LX/0T1;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62528
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62527
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 62525
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 62524
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 62523
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 62522
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62521
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 62520
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 62529
    return-void
.end method

.method public a(ILandroid/app/Dialog;)Z
    .locals 1

    .prologue
    .line 62519
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 62518
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62517
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62516
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 62515
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 62514
    const/4 v0, 0x0

    return v0
.end method

.method public final g(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 62511
    return-void
.end method

.method public h(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 62513
    return-void
.end method

.method public i(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 62512
    const/4 v0, 0x0

    return v0
.end method
