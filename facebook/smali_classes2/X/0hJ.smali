.class public LX/0hJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 115671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115672
    return-void
.end method

.method public static a(LX/0QB;)LX/0hJ;
    .locals 3

    .prologue
    .line 115673
    const-class v1, LX/0hJ;

    monitor-enter v1

    .line 115674
    :try_start_0
    sget-object v0, LX/0hJ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 115675
    sput-object v2, LX/0hJ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115676
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115677
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 115678
    new-instance v0, LX/0hJ;

    invoke-direct {v0}, LX/0hJ;-><init>()V

    .line 115679
    move-object v0, v0

    .line 115680
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 115681
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0hJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115682
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 115683
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
