.class public LX/11d;
.super LX/11e;
.source ""

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Xs",
        "<TK;TV;>.WrappedCollection;",
        "Ljava/util/List",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic g:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/11e;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/List",
            "<TV;>;",
            "LX/0Xs",
            "<TK;TV;>.WrappedCollection;)V"
        }
    .end annotation

    .prologue
    .line 172731
    iput-object p1, p0, LX/11d;->g:LX/0Xs;

    .line 172732
    invoke-direct {p0, p1, p2, p3, p4}, LX/11e;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Collection;LX/11e;)V

    .line 172733
    return-void
.end method


# virtual methods
.method public final add(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)V"
        }
    .end annotation

    .prologue
    .line 172723
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172724
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 172725
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    .line 172726
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 172727
    iget-object v1, p0, LX/11d;->g:LX/0Xs;

    invoke-static {v1}, LX/0Xs;->c(LX/0Xs;)I

    .line 172728
    if-eqz v0, :cond_0

    .line 172729
    invoke-virtual {p0}, LX/11e;->d()V

    .line 172730
    :cond_0
    return-void
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 172712
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172713
    const/4 v0, 0x0

    .line 172714
    :cond_0
    :goto_0
    return v0

    .line 172715
    :cond_1
    invoke-virtual {p0}, LX/11d;->size()I

    move-result v1

    .line 172716
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 172717
    if-eqz v0, :cond_0

    .line 172718
    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    move-object v2, v2

    .line 172719
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 172720
    iget-object v3, p0, LX/11d;->g:LX/0Xs;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, LX/0Xs;->a(LX/0Xs;I)I

    .line 172721
    if-nez v1, :cond_0

    .line 172722
    invoke-virtual {p0}, LX/11e;->d()V

    goto :goto_0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 172710
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 172711
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 172708
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172709
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 172706
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172707
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 172704
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172705
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 172702
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172703
    new-instance v0, LX/4wv;

    invoke-direct {v0, p0}, LX/4wv;-><init>(LX/11d;)V

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 172700
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172701
    new-instance v0, LX/4wv;

    invoke-direct {v0, p0, p1}, LX/4wv;-><init>(LX/11d;I)V

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 172685
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172686
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 172687
    iget-object v1, p0, LX/11d;->g:LX/0Xs;

    invoke-static {v1}, LX/0Xs;->b(LX/0Xs;)I

    .line 172688
    invoke-virtual {p0}, LX/11e;->b()V

    .line 172689
    return-object v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)TV;"
        }
    .end annotation

    .prologue
    .line 172698
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172699
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subList(II)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 172690
    invoke-virtual {p0}, LX/11e;->a()V

    .line 172691
    iget-object v0, p0, LX/11d;->g:LX/0Xs;

    .line 172692
    iget-object v1, p0, LX/11e;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 172693
    invoke-virtual {p0}, LX/11d;->g()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 172694
    iget-object v3, p0, LX/11e;->d:LX/11e;

    move-object v3, v3

    .line 172695
    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0, v1, v2, p0}, LX/0Xs;->a$redex0(LX/0Xs;Ljava/lang/Object;Ljava/util/List;LX/11e;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 172696
    :cond_0
    iget-object v3, p0, LX/11e;->d:LX/11e;

    move-object p0, v3

    .line 172697
    goto :goto_0
.end method
