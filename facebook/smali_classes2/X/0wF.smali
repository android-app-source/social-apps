.class public final enum LX/0wF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wF;

.field public static final enum IMAGEGIF:LX/0wF;

.field public static final enum IMAGEJPEG:LX/0wF;

.field public static final enum IMAGEPNG:LX/0wF;

.field public static final enum IMAGEWEBP:LX/0wF;

.field public static final enum IMAGEXAUTO:LX/0wF;

.field public static final enum IMAGEXBEST:LX/0wF;

.field public static final enum IMAGEXFBA:LX/0wF;

.field public static final enum IMAGEXPNG2JPG:LX/0wF;

.field public static final enum VIDEOXAPNG:LX/0wF;


# instance fields
.field public final serverValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159541
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEGIF"

    const-string v2, "image/gif"

    invoke-direct {v0, v1, v4, v2}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEGIF:LX/0wF;

    .line 159542
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEWEBP"

    const-string v2, "image/webp"

    invoke-direct {v0, v1, v5, v2}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEWEBP:LX/0wF;

    .line 159543
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEJPEG"

    const-string v2, "image/jpeg"

    invoke-direct {v0, v1, v6, v2}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEJPEG:LX/0wF;

    .line 159544
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEPNG"

    const-string v2, "image/png"

    invoke-direct {v0, v1, v7, v2}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEPNG:LX/0wF;

    .line 159545
    new-instance v0, LX/0wF;

    const-string v1, "VIDEOXAPNG"

    const-string v2, "video/x-apng"

    invoke-direct {v0, v1, v8, v2}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->VIDEOXAPNG:LX/0wF;

    .line 159546
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEXFBA"

    const/4 v2, 0x5

    const-string v3, "image/x-fba"

    invoke-direct {v0, v1, v2, v3}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEXFBA:LX/0wF;

    .line 159547
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEXAUTO"

    const/4 v2, 0x6

    const-string v3, "image/x-auto"

    invoke-direct {v0, v1, v2, v3}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEXAUTO:LX/0wF;

    .line 159548
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEXBEST"

    const/4 v2, 0x7

    const-string v3, "image/x-best"

    invoke-direct {v0, v1, v2, v3}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEXBEST:LX/0wF;

    .line 159549
    new-instance v0, LX/0wF;

    const-string v1, "IMAGEXPNG2JPG"

    const/16 v2, 0x8

    const-string v3, "image/x-png2jpg"

    invoke-direct {v0, v1, v2, v3}, LX/0wF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wF;->IMAGEXPNG2JPG:LX/0wF;

    .line 159550
    const/16 v0, 0x9

    new-array v0, v0, [LX/0wF;

    sget-object v1, LX/0wF;->IMAGEGIF:LX/0wF;

    aput-object v1, v0, v4

    sget-object v1, LX/0wF;->IMAGEWEBP:LX/0wF;

    aput-object v1, v0, v5

    sget-object v1, LX/0wF;->IMAGEJPEG:LX/0wF;

    aput-object v1, v0, v6

    sget-object v1, LX/0wF;->IMAGEPNG:LX/0wF;

    aput-object v1, v0, v7

    sget-object v1, LX/0wF;->VIDEOXAPNG:LX/0wF;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0wF;->IMAGEXFBA:LX/0wF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0wF;->IMAGEXAUTO:LX/0wF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0wF;->IMAGEXBEST:LX/0wF;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0wF;->IMAGEXPNG2JPG:LX/0wF;

    aput-object v2, v0, v1

    sput-object v0, LX/0wF;->$VALUES:[LX/0wF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159551
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 159552
    iput-object p3, p0, LX/0wF;->serverValue:Ljava/lang/String;

    .line 159553
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wF;
    .locals 1

    .prologue
    .line 159554
    const-class v0, LX/0wF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wF;

    return-object v0
.end method

.method public static values()[LX/0wF;
    .locals 1

    .prologue
    .line 159555
    sget-object v0, LX/0wF;->$VALUES:[LX/0wF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wF;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159556
    iget-object v0, p0, LX/0wF;->serverValue:Ljava/lang/String;

    return-object v0
.end method
