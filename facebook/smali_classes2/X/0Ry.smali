.class public abstract LX/0Ry;
.super LX/0Rb;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Rb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field private b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 60544
    invoke-direct {p0}, LX/0Rb;-><init>()V

    .line 60545
    invoke-static {p2, p1}, LX/0PB;->checkPositionIndex(II)I

    .line 60546
    iput p1, p0, LX/0Ry;->a:I

    .line 60547
    iput p2, p0, LX/0Ry;->b:I

    .line 60548
    return-void
.end method


# virtual methods
.method public abstract a(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 60534
    iget v0, p0, LX/0Ry;->b:I

    iget v1, p0, LX/0Ry;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 60535
    iget v0, p0, LX/0Ry;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 60536
    invoke-virtual {p0}, LX/0Ry;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60537
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 60538
    :cond_0
    iget v0, p0, LX/0Ry;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/0Ry;->b:I

    invoke-virtual {p0, v0}, LX/0Ry;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 60539
    iget v0, p0, LX/0Ry;->b:I

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 60540
    invoke-virtual {p0}, LX/0Ry;->hasPrevious()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60541
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 60542
    :cond_0
    iget v0, p0, LX/0Ry;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0Ry;->b:I

    invoke-virtual {p0, v0}, LX/0Ry;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 60543
    iget v0, p0, LX/0Ry;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
