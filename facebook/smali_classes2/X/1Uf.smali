.class public LX/1Uf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile I:LX/1Uf;

.field public static final a:I

.field public static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1eF;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLInterfaces$LinkableUtilApplyAggregatedLinksGraphQL$AggregatedRanges;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/1y5;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/0id;

.field public final B:LX/17Q;

.field private final C:LX/1Uk;

.field private final D:LX/0Uh;

.field private final E:LX/0W3;

.field private final F:LX/1Ul;

.field private final G:LX/1Ar;

.field public final H:LX/1Um;

.field private final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field public final j:Landroid/content/Context;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Zb;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/17U;

.field private final p:LX/1Uj;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8qT;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/ufiservices/UriIntentGenerator;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final w:I

.field private final x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifySpanFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256467
    const v0, 0x7f0a0428

    sput v0, LX/1Uf;->a:I

    .line 256468
    new-instance v0, LX/1Ug;

    invoke-direct {v0}, LX/1Ug;-><init>()V

    sput-object v0, LX/1Uf;->b:Ljava/util/Comparator;

    .line 256469
    new-instance v0, LX/1Uh;

    invoke-direct {v0}, LX/1Uh;-><init>()V

    sput-object v0, LX/1Uf;->c:Ljava/util/Comparator;

    .line 256470
    new-instance v0, LX/1Ui;

    invoke-direct {v0}, LX/1Ui;-><init>()V

    sput-object v0, LX/1Uf;->d:LX/0QK;

    .line 256471
    const v0, -0x36bd2417

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1Uf;->e:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Zb;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/17U;LX/1Uj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0id;LX/17Q;LX/1Uk;LX/0Uh;LX/0W3;LX/1Ul;LX/1Ar;LX/1Um;)V
    .locals 3
    .param p12    # LX/0Ot;
        .annotation runtime Lcom/facebook/translation/annotations/IsCommentTranslationEnabled;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8qT;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17U;",
            "Lcom/facebook/text/CustomFontUtil;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/ufiservices/UriIntentGenerator;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/ufiservices/util/LinkifySpanFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1zK;",
            ">;",
            "LX/0id;",
            "LX/17Q;",
            "LX/1Uk;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/1Ul;",
            "LX/1Ar;",
            "LX/1Um;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 256472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256473
    iput-object p1, p0, LX/1Uf;->j:Landroid/content/Context;

    .line 256474
    iput-object p2, p0, LX/1Uf;->k:LX/0Ot;

    .line 256475
    iput-object p3, p0, LX/1Uf;->l:LX/0Zb;

    .line 256476
    iput-object p4, p0, LX/1Uf;->m:LX/0Ot;

    .line 256477
    iput-object p5, p0, LX/1Uf;->q:LX/0Ot;

    .line 256478
    iput-object p6, p0, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    .line 256479
    iput-object p7, p0, LX/1Uf;->o:LX/17U;

    .line 256480
    iput-object p8, p0, LX/1Uf;->p:LX/1Uj;

    .line 256481
    iput-object p9, p0, LX/1Uf;->r:LX/0Ot;

    .line 256482
    iput-object p10, p0, LX/1Uf;->s:LX/0Ot;

    .line 256483
    iput-object p11, p0, LX/1Uf;->t:LX/0Ot;

    .line 256484
    iput-object p12, p0, LX/1Uf;->v:LX/0Ot;

    .line 256485
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1Uf;->u:LX/0Ot;

    .line 256486
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1Uf;->x:LX/0Or;

    .line 256487
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1Uf;->y:LX/0Ot;

    .line 256488
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1Uf;->z:LX/0Ot;

    .line 256489
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1Uf;->A:LX/0id;

    .line 256490
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1Uf;->B:LX/17Q;

    .line 256491
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1Uf;->C:LX/1Uk;

    .line 256492
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1Uf;->D:LX/0Uh;

    .line 256493
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1Uf;->E:LX/0W3;

    .line 256494
    move-object/from16 v0, p22

    iput-object v0, p0, LX/1Uf;->F:LX/1Ul;

    .line 256495
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1Uf;->G:LX/1Ar;

    .line 256496
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1Uf;->H:LX/1Um;

    .line 256497
    const v1, 0x7f0a0427

    iput v1, p0, LX/1Uf;->w:I

    .line 256498
    iget-object v1, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1Uf;->f:Ljava/lang/String;

    .line 256499
    iget-object v1, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1Uf;->g:Ljava/lang/String;

    .line 256500
    iget-object v1, p0, LX/1Uf;->j:Landroid/content/Context;

    const v2, 0x7f081035

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1Uf;->h:Ljava/lang/String;

    .line 256501
    iget-object v1, p0, LX/1Uf;->j:Landroid/content/Context;

    const v2, 0x7f081111

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/1Uf;->i:Ljava/lang/String;

    .line 256502
    return-void
.end method

.method public static a(LX/0QB;)LX/1Uf;
    .locals 3

    .prologue
    .line 256503
    sget-object v0, LX/1Uf;->I:LX/1Uf;

    if-nez v0, :cond_1

    .line 256504
    const-class v1, LX/1Uf;

    monitor-enter v1

    .line 256505
    :try_start_0
    sget-object v0, LX/1Uf;->I:LX/1Uf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 256506
    if-eqz v2, :cond_0

    .line 256507
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/1Uf;->b(LX/0QB;)LX/1Uf;

    move-result-object v0

    sput-object v0, LX/1Uf;->I:LX/1Uf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256508
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 256509
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 256510
    :cond_1
    sget-object v0, LX/1Uf;->I:LX/1Uf;

    return-object v0

    .line 256511
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 256512
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Uf;Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/1eK;Landroid/text/Spannable;LX/0lF;I)Landroid/text/SpannableStringBuilder;
    .locals 8
    .param p1    # Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 256513
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, v6

    invoke-static/range {v0 .. v7}, LX/1Uf;->a(LX/1Uf;Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/1eK;Landroid/text/Spannable;LX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1Uf;Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/1eK;Landroid/text/Spannable;LX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;
    .locals 10
    .param p1    # Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256514
    if-nez p3, :cond_0

    .line 256515
    const/4 v3, 0x0

    .line 256516
    :goto_0
    return-object v3

    .line 256517
    :cond_0
    invoke-static {p3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 256518
    const/4 v4, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v0 .. v9}, LX/1Uf;->a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;LX/0P1;)V

    .line 256519
    invoke-direct {p0, p1, v3, p4, p5}, LX/1Uf;->a(LX/1y8;Landroid/text/Spannable;LX/0lF;I)V

    goto :goto_0
.end method

.method private static a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;
    .locals 1

    .prologue
    .line 256520
    iget-object v0, p0, LX/1Uf;->p:LX/1Uj;

    invoke-virtual {v0}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/text/Spannable;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 256521
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    const-string v2, " "

    aput-object v2, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;I)V
    .locals 10
    .param p1    # LX/1eE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256522
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move/from16 v6, p6

    invoke-static/range {v0 .. v9}, LX/1Uf;->a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;LX/0P1;)V

    .line 256523
    return-void
.end method

.method private static a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;LX/0P1;)V
    .locals 28
    .param p1    # LX/1eE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eE;",
            "LX/1eK;",
            "Landroid/text/Spannable;",
            "Z",
            "LX/0lF;",
            "I",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/1PT;",
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256524
    if-nez p1, :cond_1

    .line 256525
    :cond_0
    return-void

    .line 256526
    :cond_1
    if-nez p9, :cond_2

    .line 256527
    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object p9

    .line 256528
    :cond_2
    invoke-virtual/range {p9 .. p9}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 256529
    sget-object v4, LX/1Uf;->e:LX/0Rf;

    invoke-virtual {v4, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GraphQLObjectType does not support custom click handlers: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    .line 256530
    :cond_3
    if-eqz p6, :cond_5

    .line 256531
    :goto_1
    const v26, 0x7f0a010c

    .line 256532
    invoke-interface/range {p1 .. p1}, LX/1eE;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 256533
    sget-object v3, LX/1Uf;->b:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 256534
    invoke-static/range {p2 .. p2}, LX/1Uf;->a(LX/1eK;)Z

    move-result v25

    .line 256535
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    move/from16 v12, v25

    :cond_4
    :goto_2
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1eF;

    .line 256536
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 256537
    :try_start_0
    invoke-interface/range {p1 .. p1}, LX/1eE;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/1yL;

    invoke-interface {v3}, LX/1eF;->c()I

    move-result v5

    invoke-interface {v3}, LX/1eF;->b()I

    move-result v6

    invoke-direct {v4, v5, v6}, LX/1yL;-><init>(II)V

    invoke-static {v2, v4}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 256538
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    .line 256539
    move/from16 v0, v25

    invoke-static {v3, v5, v0}, LX/1Uf;->a(LX/1eJ;IZ)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 256540
    const/4 v7, 0x0

    .line 256541
    if-eqz v25, :cond_6

    move/from16 v2, v26

    :goto_3
    move v6, v2

    .line 256542
    :goto_4
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1Uf;->l:LX/0Zb;

    invoke-static {v2, v8}, LX/1Uf;->a(LX/1yJ;LX/0Zb;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 256543
    invoke-interface/range {p1 .. p1}, LX/1eE;->a()Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, LX/1Uf;->a(LX/1eG;LX/1yN;Landroid/text/Spannable;IZLX/0lF;)V

    .line 256544
    :goto_5
    if-eqz v12, :cond_4

    .line 256545
    const/4 v12, 0x0

    goto :goto_2

    .line 256546
    :cond_5
    const p6, 0x7f0a010c

    goto/16 :goto_1

    .line 256547
    :catch_0
    move-exception v2

    .line 256548
    const-string v3, "LinkifyUtil"

    invoke-virtual {v2}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 256549
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, LX/1Uf;->w:I

    goto :goto_3

    .line 256550
    :cond_7
    const v2, -0x7333ac54

    if-ne v5, v2, :cond_8

    .line 256551
    invoke-interface/range {p1 .. p1}, LX/1eE;->a()Ljava/lang/String;

    move-object/from16 v8, p0

    move-object v9, v3

    move-object v10, v4

    move-object/from16 v11, p3

    move v13, v6

    move v14, v7

    move-object/from16 v15, p5

    invoke-direct/range {v8 .. v15}, LX/1Uf;->a(LX/1eI;LX/1yN;Landroid/text/Spannable;ZIZLX/0lF;)V

    goto :goto_5

    .line 256552
    :cond_8
    const v2, 0x64687ce

    if-ne v5, v2, :cond_9

    .line 256553
    invoke-interface/range {p1 .. p1}, LX/1eE;->a()Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, LX/1Uf;->a(LX/1eH;LX/1yN;Landroid/text/Spannable;ILX/0lF;)V

    goto :goto_5

    .line 256554
    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LX/1Uf;->a(LX/1eF;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 256555
    const v2, -0x36bd2417

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v4, v1, v2}, LX/1Uf;->a(LX/1eF;LX/1yN;Landroid/text/Spannable;Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 256556
    :cond_a
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->s()LX/1yO;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 256557
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->s()LX/1yO;

    move-result-object v22

    .line 256558
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1Uf;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nG;

    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v14

    .line 256559
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->c()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 256560
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v2

    invoke-interface {v2}, LX/1y9;->c()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v14, v2

    .line 256561
    :cond_b
    invoke-static {v14}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 256562
    invoke-virtual {v4}, LX/1yN;->a()I

    move-result v15

    invoke-virtual {v4}, LX/1yN;->c()I

    move-result v16

    move-object/from16 v13, p0

    move-object/from16 v17, p3

    move/from16 v18, v12

    move/from16 v19, v6

    move/from16 v20, v7

    move-object/from16 v21, p5

    move-object/from16 v23, p7

    move-object/from16 v24, p8

    invoke-direct/range {v13 .. v24}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;Lcom/facebook/graphql/model/FeedUnit;LX/1PT;)V

    goto/16 :goto_5

    .line 256563
    :cond_c
    invoke-interface {v3}, LX/1eF;->d()LX/1y9;

    move-result-object v22

    goto :goto_6

    :cond_d
    move/from16 v7, p4

    move/from16 v6, p6

    goto/16 :goto_4
.end method

.method private a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;I)V
    .locals 7
    .param p2    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256564
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, LX/1Uf;->a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;I)V

    .line 256565
    return-void
.end method

.method private a(LX/1eF;LX/1yN;Landroid/text/Spannable;Landroid/view/View$OnClickListener;)V
    .locals 10
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256583
    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v0

    invoke-interface {v0}, LX/1y9;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    if-nez v0, :cond_1

    .line 256584
    :cond_0
    :goto_0
    return-void

    .line 256585
    :cond_1
    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v0

    invoke-interface {v0}, LX/1y9;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->NEW_YEARS:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256586
    iget v0, p2, LX/1yN;->a:I

    move v0, v0

    .line 256587
    invoke-virtual {p2}, LX/1yN;->c()I

    move-result v1

    .line 256588
    iget-object v2, p0, LX/1Uf;->F:LX/1Ul;

    .line 256589
    new-instance v4, LX/7mx;

    invoke-static {v2}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v2}, Lcom/facebook/delights/floating/DelightsFireworks;->a(LX/0QB;)Lcom/facebook/delights/floating/DelightsFireworks;

    move-result-object v6

    check-cast v6, Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v2}, LX/1Ar;->b(LX/0QB;)LX/1Ar;

    move-result-object v8

    check-cast v8, LX/1Ar;

    move-object v9, p4

    invoke-direct/range {v4 .. v9}, LX/7mx;-><init>(LX/0W3;Lcom/facebook/delights/floating/DelightsFireworks;LX/0Zb;LX/1Ar;Landroid/view/View$OnClickListener;)V

    .line 256590
    move-object v2, v4

    .line 256591
    const/16 v3, 0x21

    invoke-interface {p3, v2, v0, v1, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private a(LX/1eG;LX/1yN;Landroid/text/Spannable;IZLX/0lF;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/16 v8, 0x21

    .line 256566
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 256567
    :cond_0
    :goto_0
    return-void

    .line 256568
    :cond_1
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x57fc1342

    if-ne v0, v1, :cond_2

    .line 256569
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->j()Ljava/lang/String;

    move-result-object v2

    .line 256570
    :goto_1
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256571
    new-instance v0, LX/1yS;

    iget-object v3, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v1

    invoke-interface {v1}, LX/1yC;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v6

    move-object v1, p0

    move-object v4, p6

    invoke-direct/range {v0 .. v7}, LX/1yS;-><init>(LX/1Uf;Ljava/lang/String;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 256572
    iput p4, v0, LX/1yS;->f:I

    .line 256573
    iget v1, p2, LX/1yN;->a:I

    move v1, v1

    .line 256574
    invoke-virtual {p2}, LX/1yN;->c()I

    move-result v2

    invoke-interface {p3, v0, v1, v2, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 256575
    if-eqz p5, :cond_0

    .line 256576
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v0

    .line 256577
    iget v1, p2, LX/1yN;->a:I

    move v1, v1

    .line 256578
    invoke-virtual {p2}, LX/1yN;->c()I

    move-result v2

    invoke-interface {p3, v0, v1, v2, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 256579
    :cond_2
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 256580
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->c()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    goto :goto_1

    .line 256581
    :cond_3
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 256582
    invoke-interface {p1}, LX/1eG;->e()LX/1yC;

    move-result-object v0

    invoke-interface {v0}, LX/1yC;->j()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v2, v7

    goto :goto_1
.end method

.method private a(LX/1eH;LX/1yN;Landroid/text/Spannable;ILX/0lF;)V
    .locals 10

    .prologue
    const/16 v9, 0x21

    .line 256649
    invoke-interface {p1}, LX/1eH;->eL_()LX/1yH;

    move-result-object v7

    .line 256650
    if-eqz v7, :cond_0

    invoke-interface {v7}, LX/1yH;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 256651
    :cond_0
    :goto_0
    return-void

    .line 256652
    :cond_1
    iget-object v0, p0, LX/1Uf;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-virtual {v0, v7}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v2

    .line 256653
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256654
    new-instance v0, LX/1yR;

    iget-object v3, p0, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/1Uf;->j:Landroid/content/Context;

    const v6, 0x64687ce

    invoke-interface {v7}, LX/1yH;->e()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, LX/1yR;-><init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 256655
    iget v1, p2, LX/1yN;->a:I

    move v1, v1

    .line 256656
    invoke-virtual {p2}, LX/1yN;->c()I

    move-result v2

    .line 256657
    iput p4, v0, LX/1yS;->f:I

    .line 256658
    invoke-interface {p3, v0, v1, v2, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 256659
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v0

    invoke-interface {p3, v0, v1, v2, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private a(LX/1eI;LX/1yN;Landroid/text/Spannable;ZIZLX/0lF;)V
    .locals 9

    .prologue
    .line 256633
    invoke-interface {p1}, LX/1eI;->eM_()LX/1yI;

    move-result-object v7

    .line 256634
    if-eqz v7, :cond_0

    invoke-interface {v7}, LX/1yI;->w_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 256635
    :cond_0
    :goto_0
    return-void

    .line 256636
    :cond_1
    const/4 v0, 0x0

    .line 256637
    invoke-interface {v7}, LX/1yI;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, LX/1yI;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 256638
    invoke-interface {v7}, LX/1yI;->c()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256639
    :cond_2
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 256640
    iget-object v0, p0, LX/1Uf;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-interface {p1}, LX/1eI;->eM_()LX/1yI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v2

    .line 256641
    :goto_1
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256642
    new-instance v0, LX/1yR;

    iget-object v3, p0, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/1Uf;->j:Landroid/content/Context;

    const v6, -0x7333ac54

    const/4 v8, 0x0

    move-object v1, p0

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v8}, LX/1yR;-><init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 256643
    invoke-virtual {p2}, LX/1yN;->a()I

    move-result v1

    .line 256644
    invoke-virtual {p2}, LX/1yN;->c()I

    move-result v2

    .line 256645
    invoke-virtual {v0, p5}, LX/1yS;->a(I)V

    .line 256646
    const/16 v3, 0x21

    invoke-interface {p3, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 256647
    if-nez p4, :cond_3

    if-eqz p6, :cond_0

    .line 256648
    :cond_3
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    const/16 v3, 0x21

    invoke-interface {p3, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method

.method private a(LX/1y8;Landroid/text/Spannable;LX/0lF;I)V
    .locals 10

    .prologue
    .line 256615
    const/4 v1, 0x0

    .line 256616
    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel$SampleEntitiesModel;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 256617
    if-nez v0, :cond_1

    .line 256618
    :cond_0
    return-void

    .line 256619
    :cond_1
    invoke-interface {p1}, LX/1y8;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 256620
    sget-object v1, LX/1Uf;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 256621
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;

    .line 256622
    :try_start_0
    invoke-interface {p1}, LX/1y8;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/1yL;

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->a()I

    move-result v4

    invoke-direct {v2, v3, v4}, LX/1yL;-><init>(II)V

    invoke-static {v1, v2}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v3

    .line 256623
    invoke-virtual {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$AggregatedRangesModel;->c()LX/0Px;

    move-result-object v1

    .line 256624
    iget v0, v3, LX/1yN;->a:I

    move v2, v0

    .line 256625
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    .line 256626
    if-eqz v6, :cond_3

    .line 256627
    :goto_2
    new-instance v8, LX/36P;

    invoke-direct {v8, v0, v1, v5, v6}, LX/36P;-><init>(LX/1Uf;LX/0Px;LX/0lF;I)V

    .line 256628
    const/16 v9, 0x21

    invoke-interface {v4, v8, v2, v3, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 256629
    goto :goto_1

    .line 256630
    :catch_0
    move-exception v0

    .line 256631
    const-string v1, "LinkifyUtil"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    .line 256632
    :cond_3
    const v6, 0x7f0a010c

    goto :goto_2
.end method

.method public static varargs a(Landroid/text/Spannable;LX/1nV;[I)V
    .locals 10
    .param p1    # LX/1nV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 256660
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, LX/1yS;

    invoke-interface {p0, v3, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1yS;

    .line 256661
    array-length v5, v0

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_4

    aget-object v6, v0, v4

    .line 256662
    iget v1, v6, LX/1yS;->g:I

    move v7, v1

    .line 256663
    if-eqz p2, :cond_2

    array-length v1, p2

    if-eqz v1, :cond_2

    .line 256664
    array-length v8, p2

    move v1, v3

    :goto_1
    if-ge v1, v8, :cond_1

    aget v9, p2, v1

    .line 256665
    if-ne v9, v7, :cond_0

    move v1, v2

    .line 256666
    :goto_2
    if-eqz v1, :cond_3

    move-object v1, p1

    :goto_3
    invoke-virtual {v6, v1}, LX/1yS;->a(LX/1nV;)V

    .line 256667
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 256668
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :cond_2
    move v1, v2

    .line 256669
    goto :goto_2

    .line 256670
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    .line 256671
    :cond_4
    return-void
.end method

.method private a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;Lcom/facebook/graphql/model/FeedUnit;LX/1PT;)V
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256602
    if-nez p1, :cond_1

    .line 256603
    :cond_0
    :goto_0
    return-void

    .line 256604
    :cond_1
    if-eqz p9, :cond_4

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz p11, :cond_4

    invoke-interface/range {p11 .. p11}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface/range {p11 .. p11}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->GROUPS:LX/1Qt;

    if-eq v1, v2, :cond_2

    invoke-interface/range {p11 .. p11}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->GROUPS_MEMBER_INFO:LX/1Qt;

    if-ne v1, v2, :cond_4

    .line 256605
    :cond_2
    new-instance v1, LX/8sF;

    iget-object v4, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const/4 v7, 0x0

    invoke-interface/range {p9 .. p9}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p9 .. p9}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v10, p10

    check-cast v10, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v5, p8

    invoke-direct/range {v1 .. v10}, LX/8sF;-><init>(LX/1Uf;Ljava/lang/String;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 256606
    :goto_1
    move/from16 v0, p6

    invoke-virtual {v1, v0}, LX/1yS;->a(I)V

    .line 256607
    const/16 v2, 0x21

    move-object/from16 v0, p4

    invoke-interface {v0, v1, p2, p3, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 256608
    if-nez p5, :cond_3

    if-eqz p7, :cond_0

    .line 256609
    :cond_3
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v1

    const/16 v2, 0x21

    move-object/from16 v0, p4

    invoke-interface {v0, v1, p2, p3, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 256610
    :cond_4
    if-eqz p9, :cond_5

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x1eaef984

    if-eq v1, v2, :cond_5

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x4dba1a9d

    if-eq v1, v2, :cond_5

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x5fcedbf5

    if-eq v1, v2, :cond_5

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0xa7c5482

    if-eq v1, v2, :cond_5

    .line 256611
    iget-object v1, p0, LX/1Uf;->C:LX/1Uk;

    invoke-virtual {v1}, LX/1Uk;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x252412

    if-ne v1, v2, :cond_6

    .line 256612
    sget-object v1, LX/0ax;->bC:Ljava/lang/String;

    invoke-interface/range {p9 .. p9}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    move-object v3, p1

    .line 256613
    :goto_2
    new-instance v1, LX/1yQ;

    iget-object v4, p0, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-interface/range {p9 .. p9}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const/4 v8, 0x0

    invoke-interface/range {p9 .. p9}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v9

    sget-object v10, LX/1Uf;->d:LX/0QK;

    move-object v2, p0

    move-object/from16 v6, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, LX/1yQ;-><init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;LX/0QK;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 256614
    :cond_5
    new-instance v1, LX/2xy;

    iget-object v4, p0, LX/1Uf;->o:LX/17U;

    iget-object v5, p0, LX/1Uf;->j:Landroid/content/Context;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v6, p8

    invoke-direct/range {v1 .. v6}, LX/2xy;-><init>(LX/1Uf;Ljava/lang/String;LX/17U;Landroid/content/Context;LX/0lF;)V

    goto/16 :goto_1

    :cond_6
    move-object v3, p1

    goto :goto_2
.end method

.method private a(LX/1eF;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 256597
    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v1

    invoke-interface {v1}, LX/1y9;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 256598
    :cond_0
    :goto_0
    return v0

    .line 256599
    :cond_1
    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v1

    invoke-interface {v1}, LX/1y9;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x36bd2417

    if-ne v1, v2, :cond_0

    .line 256600
    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v1

    invoke-interface {v1}, LX/1y9;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/1eF;->d()LX/1y9;

    move-result-object v1

    invoke-interface {v1}, LX/1y9;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->NEW_YEARS:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256601
    iget-object v0, p0, LX/1Uf;->G:LX/1Ar;

    invoke-virtual {v0}, LX/1Ar;->d()Z

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/1eJ;IZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 256592
    const v2, 0x5fcedbf5

    if-ne p1, v2, :cond_1

    .line 256593
    :cond_0
    :goto_0
    return v0

    .line 256594
    :cond_1
    const v2, 0x1eaef984

    if-eq p1, v2, :cond_2

    move v0, v1

    .line 256595
    goto :goto_0

    .line 256596
    :cond_2
    if-eqz p2, :cond_0

    invoke-interface {p0}, LX/1eJ;->j()LX/1yK;

    move-result-object v2

    invoke-interface {v2}, LX/1yK;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1H1;->d(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static a(LX/1eK;)Z
    .locals 1
    .param p0    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256455
    sget-object v0, LX/1eK;->TITLE:LX/1eK;

    invoke-virtual {v0, p0}, LX/1eK;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static a(LX/1yJ;LX/0Zb;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 256456
    invoke-interface {p0}, LX/1yJ;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1yJ;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/1yJ;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/1yJ;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "fbrpc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 256457
    :goto_0
    return v0

    .line 256458
    :cond_0
    invoke-interface {p0}, LX/1yJ;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, LX/1yJ;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v3, -0xe9bddb6

    if-ne v0, v3, :cond_2

    .line 256459
    const-string v0, "instagram_tag_interactions"

    invoke-interface {p1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 256460
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 256461
    const-string v2, "event"

    const-string v3, "android_view"

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 256462
    invoke-virtual {v0}, LX/0oG;->d()V

    :cond_1
    move v0, v1

    .line 256463
    goto :goto_0

    .line 256464
    :cond_2
    invoke-interface {p0}, LX/1yJ;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, LX/1yJ;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v3, -0x57fc1342

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 256465
    goto :goto_0

    .line 256466
    :cond_3
    invoke-interface {p0}, LX/1yJ;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p0}, LX/1yJ;->j()Ljava/lang/String;

    move-result-object v0

    const-string v3, "fbrpc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1Uf;
    .locals 27

    .prologue
    .line 256361
    new-instance v2, LX/1Uf;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x8b

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 v6, 0x97

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3748

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/17U;->a(LX/0QB;)LX/17U;

    move-result-object v9

    check-cast v9, LX/17U;

    invoke-static/range {p0 .. p0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v10

    check-cast v10, LX/1Uj;

    const/16 v11, 0x255e

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2eb

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1589

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xb19

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x129a

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x129e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v19

    check-cast v19, LX/0id;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v20

    check-cast v20, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/1Uk;->a(LX/0QB;)LX/1Uk;

    move-result-object v21

    check-cast v21, LX/1Uk;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v23

    check-cast v23, LX/0W3;

    const-class v24, LX/1Ul;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/1Ul;

    invoke-static/range {p0 .. p0}, LX/1Ar;->a(LX/0QB;)LX/1Ar;

    move-result-object v25

    check-cast v25, LX/1Ar;

    invoke-static/range {p0 .. p0}, LX/1Um;->a(LX/0QB;)LX/1Um;

    move-result-object v26

    check-cast v26, LX/1Um;

    invoke-direct/range {v2 .. v26}, LX/1Uf;-><init>(Landroid/content/Context;LX/0Ot;LX/0Zb;LX/0Ot;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/17U;LX/1Uj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0id;LX/17Q;LX/1Uk;LX/0Uh;LX/0W3;LX/1Ul;LX/1Ar;LX/1Um;)V

    .line 256362
    return-object v2
.end method


# virtual methods
.method public final a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;
    .locals 1
    .param p1    # LX/1eE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256363
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/1Uf;->a(LX/1eE;ZLX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/8sH;)Landroid/text/Spannable;
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "boldLinksOnly"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 256364
    if-eqz p1, :cond_1

    invoke-interface {p1}, LX/8sG;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/8sG;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 256365
    invoke-interface {p1}, LX/8sG;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 256366
    invoke-interface {p1}, LX/8sG;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8sJ;

    .line 256367
    invoke-interface {v0}, LX/8sJ;->a()LX/8sI;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, LX/8sJ;->a()LX/8sI;

    move-result-object v5

    invoke-interface {v5}, LX/8sI;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 256368
    :try_start_0
    invoke-interface {p1}, LX/8sG;->a()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/1yL;

    invoke-interface {v0}, LX/8sJ;->c()I

    move-result v7

    invoke-interface {v0}, LX/8sJ;->b()I

    move-result v0

    invoke-direct {v6, v7, v0}, LX/1yL;-><init>(II)V

    invoke-static {v5, v6}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 256369
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v5

    .line 256370
    iget v6, v0, LX/1yN;->a:I

    move v6, v6

    .line 256371
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    const/16 v7, 0x11

    invoke-interface {v2, v5, v6, v0, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 256372
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 256373
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 256374
    :catch_0
    move-exception v0

    .line 256375
    const-string v5, "LinkifyUtil"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 256376
    :cond_2
    invoke-interface {p1}, LX/8sH;->c()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 256377
    invoke-interface {p1}, LX/8sH;->c()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 256378
    :try_start_1
    invoke-interface {p1}, LX/8sG;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v5, LX/1yL;

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v6}, LX/15i;->j(II)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, LX/15i;->j(II)I

    move-result v3

    invoke-direct {v5, v6, v3}, LX/1yL;-><init>(II)V

    invoke-static {v0, v5}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v0

    .line 256379
    invoke-static {p0}, LX/1Uf;->a(LX/1Uf;)Landroid/text/style/MetricAffectingSpan;

    move-result-object v3

    .line 256380
    iget v4, v0, LX/1yN;->a:I

    move v4, v4

    .line 256381
    invoke-virtual {v0}, LX/1yN;->c()I

    move-result v0

    const/16 v5, 0x11

    invoke-interface {v2, v3, v4, v0, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 256382
    :catch_1
    move-exception v0

    .line 256383
    const-string v3, "LinkifyUtil"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 256384
    :cond_3
    return-object v2
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;ZLX/0lF;)Landroid/text/Spannable;
    .locals 1
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "addLikeSentenceLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 256385
    if-nez p1, :cond_0

    .line 256386
    const/4 v0, 0x0

    .line 256387
    :goto_0
    return-object v0

    .line 256388
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;->c()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256389
    invoke-virtual {p0, p1}, LX/1Uf;->a(LX/8sH;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    .line 256390
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/0lF;)Landroid/text/Spannable;
    .locals 13

    .prologue
    const/16 v6, 0x21

    const/4 v5, 0x0

    .line 256391
    if-nez p1, :cond_0

    .line 256392
    const/4 v0, 0x0

    .line 256393
    :goto_0
    return-object v0

    .line 256394
    :cond_0
    iget-object v0, p0, LX/1Uf;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 256395
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 256396
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, 0x7f0a015d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    invoke-interface {v0, v2, v5, v3, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 256397
    sget-object v2, LX/1eK;->SUFFIX:LX/1eK;

    .line 256398
    const/4 v12, 0x0

    move-object v7, p0

    move-object v8, p1

    move-object v9, v2

    move-object v10, v0

    move-object v11, p2

    invoke-static/range {v7 .. v12}, LX/1Uf;->a(LX/1Uf;Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/1eK;Landroid/text/Spannable;LX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    move-object v2, v7

    .line 256399
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 256400
    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 256401
    const v3, 0x7f081074

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 256402
    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 256403
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    const v4, 0x7f0a015d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v3, v5, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 256404
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 256405
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final a(Ljava/lang/CharSequence;ILandroid/text/style/CharacterStyle;ILjava/lang/String;)Landroid/text/Spannable;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 256406
    invoke-static {p5}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    .line 256407
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x21

    invoke-interface {v3, p3, v1, v0, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    move v0, v1

    move v2, v1

    .line 256408
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 256409
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    .line 256410
    add-int/lit8 v2, v2, 0x1

    .line 256411
    :cond_0
    const/4 v4, 0x7

    if-lt v2, v4, :cond_1

    .line 256412
    invoke-static {v0, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 256413
    new-array v2, v9, [Ljava/lang/CharSequence;

    invoke-static {p1, v1, v0}, LX/33Q;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v1

    iget-object v0, p0, LX/1Uf;->f:Ljava/lang/String;

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    aput-object v3, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 256414
    :goto_1
    return-object v0

    .line 256415
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256416
    :cond_2
    if-le p2, p4, :cond_4

    .line 256417
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1, p4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 256418
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    if-lt v2, p2, :cond_3

    .line 256419
    const/4 v0, 0x0

    goto :goto_1

    .line 256420
    :cond_3
    new-array v2, v9, [Ljava/lang/CharSequence;

    invoke-static {p1, v1, v0}, LX/33Q;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v1

    iget-object v0, p0, LX/1Uf;->f:Ljava/lang/String;

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    aput-object v3, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_1

    .line 256421
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 256422
    iget-object v0, p0, LX/1Uf;->g:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;ILjava/lang/String;)Landroid/text/Spannable;
    .locals 6

    .prologue
    .line 256423
    iget-object v0, p0, LX/1Uf;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zL;

    invoke-virtual {v0, p2}, LX/1zL;->a(Landroid/view/View$OnClickListener;)Landroid/text/style/CharacterStyle;

    move-result-object v3

    .line 256424
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(Ljava/lang/CharSequence;ILandroid/text/style/CharacterStyle;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 6

    .prologue
    .line 256425
    iget-object v0, p0, LX/1Uf;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zL;

    invoke-virtual {v0, p2}, LX/1zL;->a(Landroid/view/View$OnClickListener;)Landroid/text/style/CharacterStyle;

    move-result-object v3

    .line 256426
    iget-object v0, p0, LX/1Uf;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zK;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/1zK;->a(Ljava/lang/CharSequence;Z)I

    move-result v4

    .line 256427
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(Ljava/lang/CharSequence;ILandroid/text/style/CharacterStyle;ILjava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1eE;ZLX/0lF;I)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p1    # LX/1eE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256428
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(LX/1eE;ZLX/0lF;ILX/0P1;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1eE;ZLX/0lF;ILX/0P1;)Landroid/text/SpannableStringBuilder;
    .locals 10
    .param p1    # LX/1eE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eE;",
            "Z",
            "LX/0lF;",
            "I",
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 256429
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1eE;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1eE;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, v2

    move-object v8, v2

    move-object v9, p5

    .line 256430
    invoke-static/range {v0 .. v9}, LX/1Uf;->a(LX/1Uf;LX/1eE;LX/1eK;Landroid/text/Spannable;ZLX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;LX/0P1;)V

    .line 256431
    return-object v3

    .line 256432
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(LX/1y5;LX/0lF;IZ)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 256433
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/1Uf;->a(LX/1y5;LX/0lF;IZLcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1y5;LX/0lF;IZLcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;
    .locals 12
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256434
    if-nez p1, :cond_0

    .line 256435
    const/4 v4, 0x0

    .line 256436
    :goto_0
    return-object v4

    .line 256437
    :cond_0
    invoke-interface {p1}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 256438
    :goto_1
    iget-object v0, p0, LX/1Uf;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-interface {p1}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-interface {p1}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 256439
    invoke-static {v3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 256440
    const/4 v2, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v5, 0x0

    move-object v0, p0

    move v6, p3

    move/from16 v7, p4

    move-object v8, p2

    move-object v9, p1

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v0 .. v11}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;Lcom/facebook/graphql/model/FeedUnit;LX/1PT;)V

    goto :goto_0

    .line 256441
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256442
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;I)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 256443
    if-nez p1, :cond_0

    .line 256444
    :goto_0
    return-object v6

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;ILX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;ILX/1PT;)Landroid/text/SpannableStringBuilder;
    .locals 8
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256445
    if-nez p1, :cond_0

    .line 256446
    const/4 v0, 0x0

    .line 256447
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p2

    move-object v7, p6

    invoke-static/range {v0 .. v7}, LX/1Uf;->a(LX/1Uf;Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/1eK;Landroid/text/Spannable;LX/0lF;ILcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final a(LX/1eE;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 256448
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    invoke-static {v0}, LX/1Uf;->a(Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;)V
    .locals 6
    .param p2    # LX/1eK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256449
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/1Uf;->a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;I)V

    .line 256450
    return-void
.end method

.method public final a(LX/1y8;Landroid/text/Spannable;LX/0lF;)V
    .locals 1

    .prologue
    .line 256451
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/1Uf;->a(LX/1y8;Landroid/text/Spannable;LX/0lF;I)V

    .line 256452
    return-void
.end method

.method public final a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;)V
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256453
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v11}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;Lcom/facebook/graphql/model/FeedUnit;LX/1PT;)V

    .line 256454
    return-void
.end method
