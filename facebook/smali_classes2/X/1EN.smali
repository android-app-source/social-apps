.class public LX/1EN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/1EO;

.field private static n:LX/0Xm;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/1EP;

.field private final d:LX/0gh;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1EQ;

.field private final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9EG;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/03V;

.field private final l:LX/0tF;

.field private final m:LX/0pf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 219687
    sget-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    sput-object v0, LX/1EN;->a:LX/1EO;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1EP;LX/0gh;LX/0Ot;LX/1EQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/0tF;LX/0pf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/1EP;",
            "LX/0gh;",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;",
            "LX/1EQ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9EG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tF;",
            "LX/0pf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 219737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219738
    iput-object p1, p0, LX/1EN;->b:LX/0Zb;

    .line 219739
    iput-object p2, p0, LX/1EN;->c:LX/1EP;

    .line 219740
    iput-object p3, p0, LX/1EN;->d:LX/0gh;

    .line 219741
    iput-object p4, p0, LX/1EN;->e:LX/0Ot;

    .line 219742
    iput-object p5, p0, LX/1EN;->f:LX/1EQ;

    .line 219743
    iput-object p6, p0, LX/1EN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 219744
    iput-object p7, p0, LX/1EN;->i:LX/0Ot;

    .line 219745
    iput-object p9, p0, LX/1EN;->h:LX/0Ot;

    .line 219746
    iput-object p8, p0, LX/1EN;->j:LX/0Ot;

    .line 219747
    iput-object p10, p0, LX/1EN;->k:LX/03V;

    .line 219748
    iput-object p11, p0, LX/1EN;->l:LX/0tF;

    .line 219749
    iput-object p12, p0, LX/1EN;->m:LX/0pf;

    .line 219750
    return-void
.end method

.method public static a(LX/0QB;)LX/1EN;
    .locals 3

    .prologue
    .line 219751
    const-class v1, LX/1EN;

    monitor-enter v1

    .line 219752
    :try_start_0
    sget-object v0, LX/1EN;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 219753
    sput-object v2, LX/1EN;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 219754
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219755
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1EN;->b(LX/0QB;)LX/1EN;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 219756
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1EN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219757
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 219758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/1EN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V
    .locals 12
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "LX/1Qt;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Landroid/view/View;",
            "LX/An0;",
            "LX/1EO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219688
    if-nez p4, :cond_0

    .line 219689
    iget-object v2, p0, LX/1EN;->k:LX/03V;

    const-string v3, "flyoutLauncherNull"

    const-string v4, "null feedback flyout requested"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219690
    :goto_0
    return-void

    .line 219691
    :cond_0
    iget-object v2, p0, LX/1EN;->m:LX/0pf;

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v2

    .line 219692
    if-eqz v2, :cond_c

    invoke-virtual {v2}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 219693
    invoke-virtual {v2}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 219694
    invoke-static/range {p4 .. p4}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v3

    new-instance v4, LX/4XY;

    invoke-direct {v4}, LX/4XY;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4XY;->d(Ljava/lang/String;)LX/4XY;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4XY;->f(Ljava/lang/String;)LX/4XY;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/4XY;->c(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XY;

    move-result-object v2

    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/3dM;

    move-result-object v2

    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p4

    move-object/from16 v3, p4

    .line 219695
    :goto_1
    if-nez p5, :cond_b

    .line 219696
    iget-object v2, p0, LX/1EN;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9EG;

    invoke-virtual {v2, p1, p2}, LX/9EG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)LX/9EF;

    move-result-object v4

    .line 219697
    iget-boolean v2, v4, LX/9EF;->a:Z

    if-eqz v2, :cond_b

    .line 219698
    iget-object v2, v4, LX/9EF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 219699
    if-eqz v2, :cond_b

    .line 219700
    iget-object p1, v4, LX/9EF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v8, v2

    .line 219701
    :goto_2
    iget-object v2, p0, LX/1EN;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0pP;->b:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219702
    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, p1, v2}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    goto :goto_0

    .line 219703
    :cond_1
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    .line 219704
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p8

    iget-object v7, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    invoke-static/range {v2 .. v7}, LX/1EP;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 219705
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 219706
    invoke-static {v3}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 219707
    move-object/from16 v0, p6

    invoke-static {v3, v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 219708
    :cond_2
    iget-object v4, p0, LX/1EN;->b:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 219709
    iget-object v3, p0, LX/1EN;->d:LX/0gh;

    invoke-virtual {v3}, LX/0gh;->b()Ljava/lang/String;

    move-result-object v3

    .line 219710
    invoke-static {v3}, LX/5H3;->a(Ljava/lang/String;)LX/21C;

    move-result-object v9

    .line 219711
    iget-object v3, p0, LX/1EN;->d:LX/0gh;

    move-object/from16 v0, p7

    iget-object v4, v0, LX/An0;->navigationTapPoint:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 219712
    sget-object v3, LX/An0;->BLINGBAR:LX/An0;

    move-object/from16 v0, p7

    if-ne v0, v3, :cond_7

    invoke-static {v8}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    .line 219713
    :goto_3
    const/4 v4, 0x0

    .line 219714
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    if-eqz v5, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v7, 0x41e065f

    if-ne v5, v7, :cond_a

    .line 219715
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v7, v4

    .line 219716
    :goto_4
    if-eqz p5, :cond_8

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v4

    .line 219717
    :goto_5
    iget-object v5, p0, LX/1EN;->l:LX/0tF;

    invoke-virtual {v5}, LX/0tF;->f()Z

    move-result v5

    if-eqz v5, :cond_9

    if-eqz p5, :cond_9

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    .line 219718
    :goto_6
    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p8

    invoke-static {v10, v0}, LX/82k;->a(Landroid/content/Context;LX/1EO;)Ljava/lang/String;

    move-result-object v10

    .line 219719
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v11

    invoke-virtual {v11, v6}, LX/21A;->a(LX/162;)LX/21A;

    move-result-object v6

    move-object/from16 v0, p7

    iget-object v11, v0, LX/An0;->nectarModule:Ljava/lang/String;

    invoke-virtual {v6, v11}, LX/21A;->a(Ljava/lang/String;)LX/21A;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/21A;->a(LX/21C;)LX/21A;

    move-result-object v6

    invoke-virtual {v6, v10}, LX/21A;->b(Ljava/lang/String;)LX/21A;

    move-result-object v6

    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v9

    invoke-virtual {v6, v9}, LX/21A;->a(Z)LX/21A;

    move-result-object v6

    if-eqz p2, :cond_3

    invoke-interface {p2}, LX/1PT;->a()LX/1Qt;

    move-result-object p3

    :cond_3
    invoke-static {p3}, LX/9Ir;->a(LX/1Qt;)LX/21D;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/21A;->a(LX/21D;)LX/21A;

    move-result-object v6

    .line 219720
    iget-object v9, p0, LX/1EN;->f:LX/1EQ;

    invoke-virtual {v9, v2, v6}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 219721
    new-instance v9, LX/8qL;

    invoke-direct {v9}, LX/8qL;-><init>()V

    invoke-virtual {v9, v8}, LX/8qL;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/8qL;

    move-result-object v9

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/8qL;->a(Ljava/lang/String;)LX/8qL;

    move-result-object v9

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/8qL;->b(Ljava/lang/String;)LX/8qL;

    move-result-object v9

    invoke-virtual {v6}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/8qL;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/8qL;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/8qL;->a(Ljava/lang/Long;)LX/8qL;

    move-result-object v6

    move-object/from16 v0, p7

    iget-boolean v7, v0, LX/An0;->showKeyboardOnFirstLoad:Z

    invoke-virtual {v6, v7}, LX/8qL;->b(Z)LX/8qL;

    move-result-object v6

    move-object/from16 v0, p7

    iget-boolean v7, v0, LX/An0;->scrollToBottomOnFirstLoad:Z

    invoke-virtual {v6, v7}, LX/8qL;->a(Z)LX/8qL;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/8qL;->c(Z)LX/8qL;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/8qL;->d(Ljava/lang/String;)LX/8qL;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/8qL;->e(Ljava/lang/String;)LX/8qL;

    move-result-object v3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8qL;->a(LX/21y;)LX/8qL;

    move-result-object v3

    invoke-static {v8}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/8qL;->d(Z)LX/8qL;

    move-result-object v3

    invoke-static {v2}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    invoke-virtual {v3, v4}, LX/8qL;->e(Z)LX/8qL;

    move-result-object v3

    invoke-static {v2}, LX/14w;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    invoke-virtual {v3, v2}, LX/8qL;->f(Z)LX/8qL;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/8qL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/8qL;

    move-result-object v2

    invoke-virtual {v2}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v4

    .line 219722
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    .line 219723
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 219724
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->o()Lcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;

    move-result-object v7

    .line 219725
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v2}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v6

    .line 219726
    const/4 v2, -0x1

    if-ne v6, v2, :cond_5

    .line 219727
    iget-object v2, p0, LX/1EN;->k:LX/03V;

    const-string v3, "flyoutLauncherStoryIndex"

    const-string v5, "story index -1 in flyout launch"

    invoke-virtual {v2, v3, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219728
    :cond_5
    iget-object v2, p0, LX/1EN;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1nI;

    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface/range {v2 .. v7}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;ZILcom/facebook/graphql/enums/GraphQLFeedbackReadLikelihood;)V

    goto/16 :goto_0

    .line 219729
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 219730
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 219731
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_6

    :cond_a
    move-object v7, v4

    goto/16 :goto_4

    :cond_b
    move-object v8, v3

    goto/16 :goto_2

    :cond_c
    move-object/from16 v3, p4

    goto/16 :goto_1
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219732
    iget-object v0, p0, LX/1EN;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 219733
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 219734
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v0, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 219735
    iget-object v0, p0, LX/1EN;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 219736
    return-void
.end method

.method private static b(LX/0QB;)LX/1EN;
    .locals 13

    .prologue
    .line 219676
    new-instance v0, LX/1EN;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/1EP;->a(LX/0QB;)LX/1EP;

    move-result-object v2

    check-cast v2, LX/1EP;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v3

    check-cast v3, LX/0gh;

    const/16 v4, 0x7bc

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v5

    check-cast v5, LX/1EQ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v7, 0x455

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1da3

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbc6

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {p0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v11

    check-cast v11, LX/0tF;

    invoke-static {p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v12

    check-cast v12, LX/0pf;

    invoke-direct/range {v0 .. v12}, LX/1EN;-><init>(LX/0Zb;LX/1EP;LX/0gh;LX/0Ot;LX/1EQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/03V;LX/0tF;LX/0pf;)V

    .line 219677
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/view/View;LX/An0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "Landroid/view/View;",
            "LX/An0;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 219678
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 219679
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    sget-object v8, LX/1EN;->a:LX/1EO;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v3

    move-object v6, p3

    move-object v7, p4

    invoke-static/range {v0 .. v8}, LX/1EN;->a(LX/1EN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    .line 219680
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Qt;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Landroid/view/View;",
            "LX/An0;",
            "LX/1EO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219681
    const/4 v2, 0x0

    .line 219682
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 219683
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-static/range {v0 .. v8}, LX/1EN;->a(LX/1EN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    .line 219684
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;LX/1EO;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1Qt;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Landroid/view/View;",
            "LX/An0;",
            "LX/1EO;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 219685
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v2

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-static/range {v0 .. v8}, LX/1EN;->a(LX/1EN;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Landroid/view/View;LX/An0;LX/1EO;)V

    .line 219686
    return-void
.end method
