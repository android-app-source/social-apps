.class public LX/13t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/13t;


# instance fields
.field private final a:LX/0Sg;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/13u;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Yb;

.field public i:Landroid/content/BroadcastReceiver;

.field public j:Z

.field public k:Landroid/net/NetworkInfo;

.field public l:LX/0p3;

.field public m:LX/0p3;

.field public n:LX/0p3;


# direct methods
.method public constructor <init>(LX/0Sg;LX/0Ot;LX/0Ot;LX/0Or;LX/13u;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation build Lcom/facebook/analytics/config/IsQualityChangeLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/13u;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177530
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/13t;->j:Z

    .line 177531
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/13t;->l:LX/0p3;

    .line 177532
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/13t;->m:LX/0p3;

    .line 177533
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    iput-object v0, p0, LX/13t;->n:LX/0p3;

    .line 177534
    iput-object p1, p0, LX/13t;->a:LX/0Sg;

    .line 177535
    iput-object p2, p0, LX/13t;->b:LX/0Ot;

    .line 177536
    iput-object p3, p0, LX/13t;->c:LX/0Ot;

    .line 177537
    iput-object p4, p0, LX/13t;->d:LX/0Or;

    .line 177538
    iput-object p5, p0, LX/13t;->e:LX/13u;

    .line 177539
    iput-object p6, p0, LX/13t;->f:LX/0Ot;

    .line 177540
    iput-object p7, p0, LX/13t;->g:LX/0Ot;

    .line 177541
    return-void
.end method

.method public static a(LX/0QB;)LX/13t;
    .locals 11

    .prologue
    .line 177542
    sget-object v0, LX/13t;->o:LX/13t;

    if-nez v0, :cond_1

    .line 177543
    const-class v1, LX/13t;

    monitor-enter v1

    .line 177544
    :try_start_0
    sget-object v0, LX/13t;->o:LX/13t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177545
    if-eqz v2, :cond_0

    .line 177546
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177547
    new-instance v3, LX/13t;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v4

    check-cast v4, LX/0Sg;

    const/16 v5, 0x24e

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1ce

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x143f

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/13u;->a(LX/0QB;)LX/13u;

    move-result-object v8

    check-cast v8, LX/13u;

    const/16 v9, 0xbc

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x97

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/13t;-><init>(LX/0Sg;LX/0Ot;LX/0Ot;LX/0Or;LX/13u;LX/0Ot;LX/0Ot;)V

    .line 177548
    move-object v0, v3

    .line 177549
    sput-object v0, LX/13t;->o:LX/13t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177552
    :cond_1
    sget-object v0, LX/13t;->o:LX/13t;

    return-object v0

    .line 177553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/13t;LX/0p3;Z)V
    .locals 6

    .prologue
    .line 177555
    iget-object v0, p0, LX/13t;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0oz;

    .line 177556
    iput-object p1, p0, LX/13t;->n:LX/0p3;

    .line 177557
    invoke-virtual {v0}, LX/0oz;->b()LX/0p3;

    move-result-object v1

    iput-object v1, p0, LX/13t;->l:LX/0p3;

    .line 177558
    invoke-virtual {v0}, LX/0oz;->e()LX/0p3;

    move-result-object v1

    iput-object v1, p0, LX/13t;->m:LX/0p3;

    .line 177559
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "quality_change"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 177560
    const-string v2, "device"

    .line 177561
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 177562
    iget-object v2, p0, LX/13t;->e:LX/13u;

    invoke-virtual {v2, v1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 177563
    const-string v2, "quality"

    iget-object v3, p0, LX/13t;->n:LX/0p3;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177564
    const-string v2, "bandwidth"

    iget-object v3, p0, LX/13t;->l:LX/0p3;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177565
    const-string v2, "latency"

    iget-object v3, p0, LX/13t;->m:LX/0p3;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177566
    if-eqz p2, :cond_0

    .line 177567
    const-string v2, "raw_kbps"

    invoke-virtual {v0}, LX/0oz;->f()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177568
    const-string v2, "raw_rtt"

    invoke-virtual {v0}, LX/0oz;->k()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177569
    :cond_0
    iget-object v0, p0, LX/13t;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 177570
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/13t;Landroid/net/NetworkInfo;)V
    .locals 4

    .prologue
    .line 177571
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "connection_change"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 177572
    const-string v0, "device"

    .line 177573
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 177574
    iget-object v0, p0, LX/13t;->e:LX/13u;

    invoke-virtual {v0, v1}, LX/13u;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 177575
    iget-object v0, p0, LX/13t;->e:LX/13u;

    iget-object v2, p0, LX/13t;->k:Landroid/net/NetworkInfo;

    .line 177576
    const-string v3, "previous_"

    invoke-static {v0, v1, v3, v2}, LX/13u;->a(LX/13u;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Landroid/net/NetworkInfo;)V

    .line 177577
    iget-object v0, p0, LX/13t;->e:LX/13u;

    invoke-virtual {v0, v1}, LX/13u;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 177578
    if-eqz p1, :cond_0

    .line 177579
    const-string v0, "state"

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo$State;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 177580
    :cond_0
    iget-object v0, p0, LX/13t;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177581
    monitor-exit p0

    return-void

    .line 177582
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 177583
    iget-object v0, p0, LX/13t;->k:Landroid/net/NetworkInfo;

    move-object v0, v0

    .line 177584
    const/16 p0, 0x5f

    .line 177585
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177586
    :cond_0
    const-string v1, "unknown"

    .line 177587
    :goto_0
    move-object v0, v1

    .line 177588
    return-object v0

    .line 177589
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 177590
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 177591
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2e

    invoke-virtual {v2, v3, p0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3, p0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 177592
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 177593
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    move-object v0, v0

    .line 177594
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    sget-object v2, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v2, :cond_0

    .line 177595
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/13t;->j:Z

    .line 177596
    :cond_0
    iget-boolean v1, p0, LX/13t;->j:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/13t;->k:Landroid/net/NetworkInfo;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 177597
    if-nez v1, :cond_5

    .line 177598
    if-nez v0, :cond_4

    .line 177599
    :cond_1
    :goto_0
    move v1, v2

    .line 177600
    if-nez v1, :cond_3

    .line 177601
    :cond_2
    iget-object v1, p0, LX/13t;->a:LX/0Sg;

    const-string v2, "Report Connection Changed"

    new-instance v3, Lcom/facebook/analytics/ConnectionStatusLogger$1;

    invoke-direct {v3, p0, v0}, Lcom/facebook/analytics/ConnectionStatusLogger$1;-><init>(LX/13t;Landroid/net/NetworkInfo;)V

    sget-object v4, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v5, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 177602
    iput-object v0, p0, LX/13t;->k:Landroid/net/NetworkInfo;

    .line 177603
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/13t;->j:Z

    .line 177604
    :cond_3
    return-void

    :cond_4
    move v2, v3

    .line 177605
    goto :goto_0

    .line 177606
    :cond_5
    if-nez v0, :cond_6

    move v2, v3

    .line 177607
    goto :goto_0

    .line 177608
    :cond_6
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v4, v5, :cond_7

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v5

    if-eq v4, v5, :cond_1

    :cond_7
    move v2, v3

    goto :goto_0
.end method
