.class public LX/0ql;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148369
    new-instance v0, LX/0ql;

    invoke-direct {v0}, LX/0ql;-><init>()V

    sput-object v0, LX/0ql;->a:Ljava/util/Comparator;

    .line 148370
    const/16 v0, 0x463

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ql;->b:Ljava/lang/String;

    .line 148371
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ql;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 148368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 148360
    if-eqz p0, :cond_1

    .line 148361
    if-eqz p1, :cond_0

    .line 148362
    invoke-virtual {p1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 148363
    :goto_0
    return v0

    .line 148364
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 148365
    :cond_1
    if-nez p1, :cond_2

    .line 148366
    const/4 v0, 0x0

    goto :goto_0

    .line 148367
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 148356
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 148357
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v0

    .line 148358
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v1

    .line 148359
    invoke-static {v0, v1}, LX/0ql;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
