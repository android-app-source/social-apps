.class public LX/1Yk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile j:LX/1Yk;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7K3;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Yl;

.field public final e:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;

.field public final g:LX/0Uh;

.field public final h:LX/0wq;

.field public i:LX/3FK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273960
    const-class v0, LX/1Yk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Yk;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0wq;LX/0Ot;LX/1Yl;LX/0ad;LX/0Uh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wq;",
            "LX/0Ot",
            "<",
            "LX/7K3;",
            ">;",
            "LX/1Yl;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 274001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274002
    new-instance v0, LX/0aq;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1Yk;->c:LX/0aq;

    .line 274003
    iput-object p2, p0, LX/1Yk;->b:LX/0Ot;

    .line 274004
    iput-object p3, p0, LX/1Yk;->d:LX/1Yl;

    .line 274005
    iput-object p4, p0, LX/1Yk;->f:LX/0ad;

    .line 274006
    iput-object p5, p0, LX/1Yk;->g:LX/0Uh;

    .line 274007
    iput-object p1, p0, LX/1Yk;->h:LX/0wq;

    .line 274008
    new-instance v0, LX/0aq;

    iget v1, p1, LX/0wq;->x:I

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1Yk;->e:LX/0aq;

    .line 274009
    return-void
.end method

.method public static a(LX/0QB;)LX/1Yk;
    .locals 9

    .prologue
    .line 273988
    sget-object v0, LX/1Yk;->j:LX/1Yk;

    if-nez v0, :cond_1

    .line 273989
    const-class v1, LX/1Yk;

    monitor-enter v1

    .line 273990
    :try_start_0
    sget-object v0, LX/1Yk;->j:LX/1Yk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 273991
    if-eqz v2, :cond_0

    .line 273992
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 273993
    new-instance v3, LX/1Yk;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v4

    check-cast v4, LX/0wq;

    const/16 v5, 0x37ef

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1Yl;->a(LX/0QB;)LX/1Yl;

    move-result-object v6

    check-cast v6, LX/1Yl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/1Yk;-><init>(LX/0wq;LX/0Ot;LX/1Yl;LX/0ad;LX/0Uh;)V

    .line 273994
    move-object v0, v3

    .line 273995
    sput-object v0, LX/1Yk;->j:LX/1Yk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273996
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 273997
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 273998
    :cond_1
    sget-object v0, LX/1Yk;->j:LX/1Yk;

    return-object v0

    .line 273999
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 274000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/video/engine/VideoPlayerParams;)Lcom/facebook/video/engine/VideoDataSource;
    .locals 2
    .param p0    # Lcom/facebook/video/engine/VideoPlayerParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 273984
    if-nez p0, :cond_1

    .line 273985
    :cond_0
    :goto_0
    return-object v0

    .line 273986
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273987
    iget-object v0, p0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/36s;Z)LX/7Jq;
    .locals 8

    .prologue
    .line 273963
    iget-object v0, p0, LX/1Yk;->i:LX/3FK;

    if-nez v0, :cond_0

    .line 273964
    new-instance v0, LX/3FK;

    iget-object v1, p0, LX/1Yk;->f:LX/0ad;

    iget-object v2, p0, LX/1Yk;->g:LX/0Uh;

    invoke-direct {v0, v1, v2}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    iput-object v0, p0, LX/1Yk;->i:LX/3FK;

    .line 273965
    :cond_0
    iget-object v0, p0, LX/1Yk;->i:LX/3FK;

    move-object v0, v0

    .line 273966
    iget-boolean v1, v0, LX/3FK;->a:Z

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    iget-boolean v1, v0, LX/3FK;->k:Z

    if-nez v1, :cond_1

    const-wide/16 v6, 0x0

    .line 273967
    iget-object v4, p2, LX/36s;->d:Landroid/net/Uri;

    if-eqz v4, :cond_3

    iget-wide v4, p2, LX/36s;->e:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    iget-wide v4, p2, LX/36s;->f:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    iget-wide v4, p2, LX/36s;->g:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x1

    :goto_0
    move v1, v4

    .line 273968
    if-eqz v1, :cond_2

    .line 273969
    :cond_1
    iget-wide v4, p2, LX/36s;->g:J

    move-wide v2, v4

    .line 273970
    iget-wide v0, v0, LX/3FK;->d:J

    cmp-long v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, LX/1Yk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7K3;

    .line 273971
    iget-object v1, v0, LX/7K3;->f:LX/7K2;

    invoke-virtual {v1, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7K5;

    .line 273972
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/7K5;->c()LX/2qD;

    move-result-object v1

    sget-object v2, LX/2qD;->STATE_PREPARED:LX/2qD;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 273973
    if-nez v0, :cond_2

    .line 273974
    iget-object v0, p0, LX/1Yk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7K3;

    invoke-virtual {v0, p1, p2}, LX/7K3;->a(Landroid/net/Uri;LX/36s;)LX/7Jr;

    move-result-object v0

    .line 273975
    :goto_2
    return-object v0

    .line 273976
    :cond_2
    iget-object v0, p0, LX/1Yk;->e:LX/0aq;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273977
    iget-object v0, p0, LX/1Yk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7K3;

    .line 273978
    iget-object v1, v0, LX/7K3;->f:LX/7K2;

    invoke-virtual {v1, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7K5;

    .line 273979
    iget-object v2, v0, LX/7K3;->f:LX/7K2;

    invoke-virtual {v2, p1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273980
    if-eqz v1, :cond_5

    invoke-virtual {v1}, LX/7K5;->c()LX/2qD;

    move-result-object v2

    sget-object v3, LX/2qD;->STATE_ERROR:LX/2qD;

    if-eq v2, v3, :cond_5

    .line 273981
    :goto_3
    move-object v0, v1

    .line 273982
    move-object v0, v0

    .line 273983
    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    new-instance v1, LX/7K5;

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iget-object v3, v0, LX/7K3;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object p0, v0, LX/7K3;->c:LX/0So;

    invoke-direct {v1, v2, v3, p1, p0}, LX/7K5;-><init>(Landroid/media/MediaPlayer;Landroid/content/Context;Landroid/net/Uri;LX/0So;)V

    goto :goto_3
.end method

.method public final a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;
    .locals 6

    .prologue
    .line 273961
    iget-object v0, p0, LX/1Yk;->e:LX/0aq;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273962
    iget-object v0, p0, LX/1Yk;->d:LX/1Yl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Yl;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Z)LX/7K1;

    move-result-object v0

    return-object v0
.end method
