.class public LX/0xk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:[[Landroid/graphics/ColorFilter;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:LX/0xl;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0gw;LX/0W3;LX/0Or;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0gw;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 163406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163407
    invoke-static {}, LX/0xl;->values()[LX/0xl;

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0x1a

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Landroid/graphics/ColorFilter;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Landroid/graphics/ColorFilter;

    iput-object v0, p0, LX/0xk;->a:[[Landroid/graphics/ColorFilter;

    .line 163408
    sget-object v0, LX/0xl;->LIGHT:LX/0xl;

    iput-object v0, p0, LX/0xk;->d:LX/0xl;

    .line 163409
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 163410
    sget-wide v2, LX/0X5;->ce:J

    const/4 v1, 0x0

    invoke-interface {p3, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v1

    .line 163411
    sparse-switch v1, :sswitch_data_0

    .line 163412
    const v1, 0x7f010273

    const v2, 0x7f0a0346

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    .line 163413
    :goto_0
    const v2, 0x7f010272

    const v3, 0x7f0a00d1

    invoke-static {v0, v2, v3}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v2

    .line 163414
    const v3, 0x7f010275

    const v4, 0x7f0a0347

    invoke-static {v0, v3, v4}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    .line 163415
    const v4, 0x7f010274

    const v5, 0x7f0a00d5

    invoke-static {v0, v4, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    .line 163416
    invoke-virtual {p2}, LX/0gw;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 163417
    sget-object v1, LX/0xl;->LIGHT:LX/0xl;

    const v2, 0x7f0a0345

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v4, 0x7f0a0344

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {p0, v1, v2, v4}, LX/0xk;->a(LX/0xl;II)V

    .line 163418
    :goto_1
    sget-object v1, LX/0xl;->DARK:LX/0xl;

    invoke-direct {p0, v1, v3, v0}, LX/0xk;->a(LX/0xl;II)V

    .line 163419
    const v0, 0x7f080a25

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0xk;->b:Ljava/lang/String;

    .line 163420
    const v0, 0x7f080a26

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0xk;->c:Ljava/lang/String;

    .line 163421
    return-void

    .line 163422
    :sswitch_0
    const v1, 0x7f0a00a3

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 163423
    :sswitch_1
    const v1, 0x7f0a00a7

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    .line 163424
    :cond_0
    sget-object v4, LX/0xl;->LIGHT:LX/0xl;

    invoke-direct {p0, v4, v1, v2}, LX/0xk;->a(LX/0xl;II)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1e -> :sswitch_0
        0x32 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/0QB;)LX/0xk;
    .locals 7

    .prologue
    .line 163425
    const-class v1, LX/0xk;

    monitor-enter v1

    .line 163426
    :try_start_0
    sget-object v0, LX/0xk;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 163427
    sput-object v2, LX/0xk;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 163428
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163429
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 163430
    new-instance v6, LX/0xk;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v4

    check-cast v4, LX/0gw;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    const/16 p0, 0x1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/0xk;-><init>(Landroid/content/res/Resources;LX/0gw;LX/0W3;LX/0Or;)V

    .line 163431
    move-object v0, v6

    .line 163432
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 163433
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0xk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163434
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 163435
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/0xl;II)V
    .locals 13

    .prologue
    .line 163436
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    .line 163437
    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 163438
    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v3

    .line 163439
    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    .line 163440
    invoke-static/range {p3 .. p3}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    .line 163441
    invoke-static/range {p3 .. p3}, Landroid/graphics/Color;->red(I)I

    move-result v6

    .line 163442
    invoke-static/range {p3 .. p3}, Landroid/graphics/Color;->green(I)I

    move-result v7

    .line 163443
    invoke-static/range {p3 .. p3}, Landroid/graphics/Color;->blue(I)I

    move-result v8

    .line 163444
    const/4 v0, 0x0

    :goto_0
    const/16 v9, 0x19

    if-gt v0, v9, :cond_0

    .line 163445
    int-to-float v9, v0

    const/high16 v10, 0x41c80000    # 25.0f

    div-float/2addr v9, v10

    .line 163446
    sub-int v10, v5, v1

    int-to-float v10, v10

    mul-float/2addr v10, v9

    float-to-int v10, v10

    add-int/2addr v10, v1

    shl-int/lit8 v10, v10, 0x18

    sub-int v11, v6, v2

    int-to-float v11, v11

    mul-float/2addr v11, v9

    float-to-int v11, v11

    add-int/2addr v11, v2

    shl-int/lit8 v11, v11, 0x10

    or-int/2addr v10, v11

    sub-int v11, v7, v3

    int-to-float v11, v11

    mul-float/2addr v11, v9

    float-to-int v11, v11

    add-int/2addr v11, v3

    shl-int/lit8 v11, v11, 0x8

    or-int/2addr v10, v11

    sub-int v11, v8, v4

    int-to-float v11, v11

    mul-float/2addr v9, v11

    float-to-int v9, v9

    add-int/2addr v9, v4

    or-int/2addr v9, v10

    .line 163447
    iget-object v10, p0, LX/0xk;->a:[[Landroid/graphics/ColorFilter;

    invoke-virtual {p1}, LX/0xl;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    new-instance v11, Landroid/graphics/PorterDuffColorFilter;

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v11, v9, v12}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    aput-object v11, v10, v0

    .line 163448
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163449
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(D)Landroid/graphics/ColorFilter;
    .locals 7

    .prologue
    .line 163450
    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    .line 163451
    const-wide/high16 v2, 0x4039000000000000L    # 25.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 163452
    iget-object v1, p0, LX/0xk;->a:[[Landroid/graphics/ColorFilter;

    iget-object v2, p0, LX/0xk;->d:LX/0xl;

    invoke-virtual {v2}, LX/0xl;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0
.end method
