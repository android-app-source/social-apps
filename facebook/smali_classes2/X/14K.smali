.class public LX/14K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/14B;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/14K;


# instance fields
.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final c:LX/0Xl;

.field private d:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178630
    const-string v0, "login_screen"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/14K;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Xl;)V
    .locals 3
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 178607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178608
    iput-object p1, p0, LX/14K;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 178609
    iput-object p2, p0, LX/14K;->c:LX/0Xl;

    .line 178610
    iget-object v0, p0, LX/14K;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/14L;

    invoke-direct {v2, p0}, LX/14L;-><init>(LX/14K;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    .line 178611
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/14K;->d:LX/0Yb;

    .line 178612
    iget-object v0, p0, LX/14K;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 178613
    return-void
.end method

.method public static a(LX/0QB;)LX/14K;
    .locals 5

    .prologue
    .line 178617
    sget-object v0, LX/14K;->e:LX/14K;

    if-nez v0, :cond_1

    .line 178618
    const-class v1, LX/14K;

    monitor-enter v1

    .line 178619
    :try_start_0
    sget-object v0, LX/14K;->e:LX/14K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 178620
    if-eqz v2, :cond_0

    .line 178621
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 178622
    new-instance p0, LX/14K;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-direct {p0, v3, v4}, LX/14K;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;LX/0Xl;)V

    .line 178623
    move-object v0, p0

    .line 178624
    sput-object v0, LX/14K;->e:LX/14K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 178626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178627
    :cond_1
    sget-object v0, LX/14K;->e:LX/14K;

    return-object v0

    .line 178628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 178629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 178614
    if-eqz p2, :cond_0

    const-string v0, "unknown"

    if-eq p2, v0, :cond_0

    sget-object v0, LX/14K;->a:LX/0Rf;

    invoke-virtual {v0, p2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178615
    iget-object v0, p0, LX/14K;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(Ljava/lang/String;)V

    .line 178616
    :cond_0
    return-void
.end method
