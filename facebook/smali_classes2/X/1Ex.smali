.class public LX/1Ex;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zm;

.field private final b:LX/0SG;

.field private c:J

.field private d:LX/03R;


# direct methods
.method public constructor <init>(LX/0Zm;LX/0SG;)V
    .locals 1

    .prologue
    .line 221459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221460
    iput-object p1, p0, LX/1Ex;->a:LX/0Zm;

    .line 221461
    iput-object p2, p0, LX/1Ex;->b:LX/0SG;

    .line 221462
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/1Ex;->d:LX/03R;

    .line 221463
    return-void
.end method

.method private declared-synchronized b()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 221464
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1Ex;->d:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 221465
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, LX/1Ex;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/1Ex;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 221466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/03R;
    .locals 2

    .prologue
    .line 221467
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1Ex;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221468
    iget-object v0, p0, LX/1Ex;->a:LX/0Zm;

    const-string v1, "image_pipeline_counters"

    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    .line 221469
    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, LX/1Ex;->d:LX/03R;

    .line 221470
    iget-object v0, p0, LX/1Ex;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/1Ex;->c:J

    .line 221471
    :cond_0
    iget-object v0, p0, LX/1Ex;->d:LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 221472
    :cond_1
    :try_start_1
    sget-object v0, LX/03R;->NO:LX/03R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221473
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
