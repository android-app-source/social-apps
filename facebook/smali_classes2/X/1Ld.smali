.class public LX/1Ld;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1K5;
.implements LX/1DD;
.implements LX/0hk;
.implements LX/0fs;


# instance fields
.field public final a:LX/0wq;

.field public final b:LX/1Le;

.field public final c:LX/1Lg;

.field private d:LX/1Yh;

.field private e:LX/1Yh;

.field public f:LX/1M7;


# direct methods
.method public constructor <init>(LX/0wq;LX/1Le;LX/1Lg;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 234016
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 234017
    iput-object p1, p0, LX/1Ld;->a:LX/0wq;

    .line 234018
    iput-object p2, p0, LX/1Ld;->b:LX/1Le;

    .line 234019
    iput-object p3, p0, LX/1Ld;->c:LX/1Lg;

    .line 234020
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 1

    .prologue
    .line 234021
    check-cast p1, LX/1OP;

    .line 234022
    iget-object v0, p0, LX/1Ld;->a:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Ld;->a:LX/0wq;

    iget-boolean v0, v0, LX/0wq;->r:Z

    if-eqz v0, :cond_1

    .line 234023
    :cond_0
    iget-object v0, p0, LX/1Ld;->b:LX/1Le;

    invoke-virtual {v0, p2, p1}, LX/1Le;->a(LX/0g8;LX/1OP;)LX/1Yg;

    move-result-object v0

    iput-object v0, p0, LX/1Ld;->d:LX/1Yh;

    .line 234024
    :cond_1
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 234025
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 234026
    iget-object v0, p0, LX/1Ld;->f:LX/1M7;

    invoke-interface {v0}, LX/1M7;->b()V

    .line 234027
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ld;->f:LX/1M7;

    .line 234028
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 234029
    iget-object v0, p0, LX/1Ld;->f:LX/1M7;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1M7;->a(Z)V

    .line 234030
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 234031
    iget-object v0, p0, LX/1Ld;->f:LX/1M7;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1M7;->a(Z)V

    .line 234032
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234033
    iget-object v0, p0, LX/1Ld;->d:LX/1Yh;

    if-eqz v0, :cond_0

    .line 234034
    iget-object v0, p0, LX/1Ld;->d:LX/1Yh;

    invoke-virtual {v0}, LX/1Yh;->a()V

    .line 234035
    iput-object v1, p0, LX/1Ld;->d:LX/1Yh;

    .line 234036
    :cond_0
    iget-object v0, p0, LX/1Ld;->e:LX/1Yh;

    if-eqz v0, :cond_1

    .line 234037
    iget-object v0, p0, LX/1Ld;->e:LX/1Yh;

    invoke-virtual {v0}, LX/1Yh;->a()V

    .line 234038
    iput-object v1, p0, LX/1Ld;->e:LX/1Yh;

    .line 234039
    :cond_1
    return-void
.end method
