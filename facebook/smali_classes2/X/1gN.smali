.class public LX/1gN;
.super LX/1gO;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/1Fk;

.field public b:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;",
            ">;"
        }
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>(LX/1Fk;)V
    .locals 2

    .prologue
    .line 294034
    iget-object v0, p1, LX/1Fk;->g:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    move v0, v0

    .line 294035
    invoke-direct {p0, p1, v0}, LX/1gN;-><init>(LX/1Fk;I)V

    .line 294036
    return-void
.end method

.method public constructor <init>(LX/1Fk;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 294027
    invoke-direct {p0}, LX/1gO;-><init>()V

    .line 294028
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 294029
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fk;

    iput-object v0, p0, LX/1gN;->a:LX/1Fk;

    .line 294030
    iput v1, p0, LX/1gN;->c:I

    .line 294031
    iget-object v0, p0, LX/1gN;->a:LX/1Fk;

    invoke-virtual {v0, p2}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/1gN;->a:LX/1Fk;

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/1gN;->b:LX/1FJ;

    .line 294032
    return-void

    :cond_0
    move v0, v1

    .line 294033
    goto :goto_0
.end method

.method public static d(LX/1gN;)V
    .locals 1

    .prologue
    .line 294024
    iget-object v0, p0, LX/1gN;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294025
    new-instance v0, LX/4eN;

    invoke-direct {v0}, LX/4eN;-><init>()V

    throw v0

    .line 294026
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()LX/1FK;
    .locals 1

    .prologue
    .line 294023
    invoke-virtual {p0}, LX/1gN;->c()LX/1FK;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 294022
    iget v0, p0, LX/1gN;->c:I

    return v0
.end method

.method public final c()LX/1FK;
    .locals 3

    .prologue
    .line 294020
    invoke-static {p0}, LX/1gN;->d(LX/1gN;)V

    .line 294021
    new-instance v0, LX/1FK;

    iget-object v1, p0, LX/1gN;->b:LX/1FJ;

    iget v2, p0, LX/1gN;->c:I

    invoke-direct {v0, v1, v2}, LX/1FK;-><init>(LX/1FJ;I)V

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 294015
    iget-object v0, p0, LX/1gN;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 294016
    const/4 v0, 0x0

    iput-object v0, p0, LX/1gN;->b:LX/1FJ;

    .line 294017
    const/4 v0, -0x1

    iput v0, p0, LX/1gN;->c:I

    .line 294018
    invoke-super {p0}, LX/1gO;->close()V

    .line 294019
    return-void
.end method

.method public final write(I)V
    .locals 3

    .prologue
    .line 293996
    const/4 v0, 0x1

    new-array v0, v0, [B

    .line 293997
    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 293998
    invoke-virtual {p0, v0}, LX/1gN;->write([B)V

    .line 293999
    return-void
.end method

.method public final write([BII)V
    .locals 5

    .prologue
    .line 294000
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 294001
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294002
    :cond_1
    invoke-static {p0}, LX/1gN;->d(LX/1gN;)V

    .line 294003
    iget v0, p0, LX/1gN;->c:I

    add-int/2addr v0, p3

    const/4 v4, 0x0

    .line 294004
    invoke-static {p0}, LX/1gN;->d(LX/1gN;)V

    .line 294005
    iget-object v1, p0, LX/1gN;->b:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 294006
    iget v2, v1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->b:I

    move v1, v2

    .line 294007
    if-gt v0, v1, :cond_2

    .line 294008
    :goto_0
    iget-object v0, p0, LX/1gN;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    iget v1, p0, LX/1gN;->c:I

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->a(I[BII)I

    .line 294009
    iget v0, p0, LX/1gN;->c:I

    add-int/2addr v0, p3

    iput v0, p0, LX/1gN;->c:I

    .line 294010
    return-void

    .line 294011
    :cond_2
    iget-object v1, p0, LX/1gN;->a:LX/1Fk;

    invoke-virtual {v1, v0}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    .line 294012
    iget-object v2, p0, LX/1gN;->b:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;

    iget v3, p0, LX/1gN;->c:I

    invoke-virtual {v2, v4, v1, v4, v3}, Lcom/facebook/imagepipeline/memory/NativeMemoryChunk;->a(ILcom/facebook/imagepipeline/memory/NativeMemoryChunk;II)V

    .line 294013
    iget-object v2, p0, LX/1gN;->b:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 294014
    iget-object v2, p0, LX/1gN;->a:LX/1Fk;

    invoke-static {v1, v2}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v1

    iput-object v1, p0, LX/1gN;->b:LX/1FJ;

    goto :goto_0
.end method
