.class public LX/13L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13H;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;

.field private c:Ljava/lang/StringBuilder;

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176553
    const-class v0, LX/13L;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/13L;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 176549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176550
    iput-object p1, p0, LX/13L;->b:LX/03V;

    .line 176551
    iput-object p2, p0, LX/13L;->d:LX/0Ot;

    .line 176552
    return-void
.end method

.method public static a(LX/13L;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176545
    iget-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 176546
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    .line 176547
    :cond_0
    iget-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176548
    return-void
.end method

.method public static b(LX/0QB;)LX/13L;
    .locals 3

    .prologue
    .line 176543
    new-instance v1, LX/13L;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    const/16 v2, 0x104c

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/13L;-><init>(LX/03V;LX/0Ot;)V

    .line 176544
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/1Y7;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 176498
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176499
    iget-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    .line 176500
    iget-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    iget-object v1, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 176501
    :cond_0
    iget-object v0, p0, LX/13L;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13P;

    .line 176502
    const-string v1, "client_definition_validator_content"

    invoke-virtual {v0, p1, v1}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176503
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176504
    iget-boolean v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->isExposureHoldout:Z

    if-eqz v3, :cond_6

    .line 176505
    :cond_1
    :goto_0
    move v1, v1

    .line 176506
    if-eqz v1, :cond_5

    .line 176507
    const-string v1, "client_definition_validator_conditions"

    invoke-virtual {v0, p1, v1}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;)V

    .line 176508
    const/4 v1, 0x0

    .line 176509
    const/4 v0, 0x1

    .line 176510
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176511
    const-string v0, "Trigger list is empty\n"

    invoke-static {p0, v0}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v0, v1

    .line 176512
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;

    .line 176513
    iget-object p2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_11

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->b()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_11

    .line 176514
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string p2, "Filter "

    invoke-direct {v2, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->a()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter$Type;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has null/empty data\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v0, v1

    :goto_2
    move v2, v0

    .line 176515
    goto :goto_1

    .line 176516
    :cond_4
    move v0, v2

    .line 176517
    if-eqz v0, :cond_5

    .line 176518
    sget-object v0, LX/1Y7;->a:LX/1Y7;

    .line 176519
    :goto_3
    return-object v0

    .line 176520
    :cond_5
    iget-object v0, p0, LX/13L;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176521
    iget-object v1, p0, LX/13L;->b:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/13L;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_invalid_promotion_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 176522
    new-instance v1, LX/1Y8;

    invoke-direct {v1, v4}, LX/1Y8;-><init>(Z)V

    .line 176523
    iput-object v0, v1, LX/1Y8;->e:Ljava/lang/String;

    .line 176524
    move-object v0, v1

    .line 176525
    invoke-virtual {v0}, LX/1Y8;->a()LX/1Y7;

    move-result-object v0

    goto :goto_3

    .line 176526
    :cond_6
    invoke-static {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 176527
    const-string v1, "Title is null/empty\n"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v1, v2

    .line 176528
    :cond_7
    invoke-static {p1}, LX/13K;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 176529
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_8

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-boolean v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-nez v3, :cond_a

    :cond_8
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_9

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-boolean v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-nez v3, :cond_a

    :cond_9
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_10

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-boolean v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-eqz v3, :cond_10

    :cond_a
    const/4 v3, 0x1

    :goto_4
    move v3, v3

    .line 176530
    if-nez v3, :cond_b

    invoke-static {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 176531
    const-string v1, "No button dismisses promotion"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v1, v2

    .line 176532
    :cond_b
    invoke-static {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 176533
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_1

    .line 176534
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-boolean v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-nez v3, :cond_1

    .line 176535
    const-string v1, "Primary Action url is null/empty\n"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_0

    .line 176536
    :cond_c
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-nez v3, :cond_d

    .line 176537
    const-string v1, "Primary Action is null\n"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    :goto_5
    move v1, v2

    .line 176538
    goto/16 :goto_0

    .line 176539
    :cond_d
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->e()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    move-result-object v3

    sget-object p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;->TOAST_FOOTER:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$TemplateType;

    if-eq v3, p2, :cond_e

    .line 176540
    const-string v1, "Primary Action title is null/empty\n"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    move v1, v2

    .line 176541
    :cond_e
    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v3, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-boolean v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-nez v3, :cond_f

    .line 176542
    const-string v1, "Primary Action url is null/empty\n"

    invoke-static {p0, v1}, LX/13L;->a(LX/13L;Ljava/lang/String;)V

    goto :goto_5

    :cond_f
    move v2, v1

    goto :goto_5

    :cond_10
    const/4 v3, 0x0

    goto :goto_4

    :cond_11
    move v0, v2

    goto/16 :goto_2
.end method
