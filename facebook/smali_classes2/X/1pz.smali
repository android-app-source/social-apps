.class public LX/1pz;
.super LX/1q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1q0",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final transient a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 329930
    iput-object p1, p0, LX/1pz;->b:LX/0Xs;

    invoke-direct {p0}, LX/1q0;-><init>()V

    .line 329931
    iput-object p2, p0, LX/1pz;->a:Ljava/util/Map;

    .line 329932
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 329929
    new-instance v0, LX/2QH;

    invoke-direct {v0, p0}, LX/2QH;-><init>(LX/1pz;)V

    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 329925
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    iget-object v1, p0, LX/1pz;->b:LX/0Xs;

    iget-object v1, v1, LX/0Xs;->a:Ljava/util/Map;

    if-ne v0, v1, :cond_0

    .line 329926
    iget-object v0, p0, LX/1pz;->b:LX/0Xs;

    invoke-virtual {v0}, LX/0Xs;->g()V

    .line 329927
    :goto_0
    return-void

    .line 329928
    :cond_0
    new-instance v0, LX/2QJ;

    invoke-direct {v0, p0}, LX/2QJ;-><init>(LX/1pz;)V

    invoke-static {v0}, LX/0RZ;->h(Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 329924
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-static {v0, p1}, LX/0PM;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 329906
    if-eq p0, p1, :cond_0

    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 329920
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 329921
    if-nez v0, :cond_0

    .line 329922
    const/4 v0, 0x0

    .line 329923
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/1pz;->b:LX/0Xs;

    invoke-virtual {v1, p1, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 329919
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 329918
    iget-object v0, p0, LX/1pz;->b:LX/0Xs;

    invoke-virtual {v0}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 329909
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 329910
    if-nez v0, :cond_0

    .line 329911
    const/4 v0, 0x0

    .line 329912
    :goto_0
    return-object v0

    .line 329913
    :cond_0
    iget-object v1, p0, LX/1pz;->b:LX/0Xs;

    invoke-virtual {v1}, LX/0Xs;->c()Ljava/util/Collection;

    move-result-object v1

    .line 329914
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 329915
    iget-object v2, p0, LX/1pz;->b:LX/0Xs;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v2, v3}, LX/0Xs;->b(LX/0Xs;I)I

    .line 329916
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    move-object v0, v1

    .line 329917
    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 329908
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329907
    iget-object v0, p0, LX/1pz;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
