.class public abstract LX/1Qj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pf;
.implements LX/02k;
.implements LX/1Px;


# instance fields
.field public a:LX/1Q8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1QF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Q1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Q2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Pz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Q4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1QI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Q0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1QC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1QD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1Q3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1QK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Q5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:Landroid/content/Context;

.field private final o:LX/1QG;

.field private final p:LX/1PU;

.field private final q:LX/1QL;

.field public final r:LX/1QR;

.field private final s:LX/1QM;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V
    .locals 2

    .prologue
    .line 245031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245032
    iput-object p1, p0, LX/1Qj;->n:Landroid/content/Context;

    .line 245033
    new-instance v0, LX/1QG;

    invoke-direct {v0}, LX/1QG;-><init>()V

    iput-object v0, p0, LX/1Qj;->o:LX/1QG;

    .line 245034
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/1Qj;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 245035
    invoke-static {p2}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v0

    iput-object v0, p0, LX/1Qj;->r:LX/1QR;

    .line 245036
    iget-object v0, p0, LX/1Qj;->g:LX/1QI;

    invoke-virtual {v0, p3}, LX/1QI;->a(LX/1PY;)LX/1PU;

    move-result-object v0

    iput-object v0, p0, LX/1Qj;->p:LX/1PU;

    .line 245037
    iget-object v0, p0, LX/1Qj;->c:LX/1Q1;

    invoke-virtual {v0, p0}, LX/1Q1;->a(LX/1Po;)LX/1QL;

    move-result-object v0

    iput-object v0, p0, LX/1Qj;->q:LX/1QL;

    .line 245038
    iget-object v0, p0, LX/1Qj;->m:LX/1Q5;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v0

    iput-object v0, p0, LX/1Qj;->s:LX/1QM;

    .line 245039
    return-void
.end method

.method private static a(LX/1Qj;LX/1Q8;LX/1QF;LX/1Q1;LX/1Q2;LX/1Pz;LX/1Q4;LX/1QI;LX/1Q0;LX/1QC;LX/1QD;LX/1Q3;LX/1QK;LX/1Q5;)V
    .locals 0

    .prologue
    .line 245040
    iput-object p1, p0, LX/1Qj;->a:LX/1Q8;

    iput-object p2, p0, LX/1Qj;->b:LX/1QF;

    iput-object p3, p0, LX/1Qj;->c:LX/1Q1;

    iput-object p4, p0, LX/1Qj;->d:LX/1Q2;

    iput-object p5, p0, LX/1Qj;->e:LX/1Pz;

    iput-object p6, p0, LX/1Qj;->f:LX/1Q4;

    iput-object p7, p0, LX/1Qj;->g:LX/1QI;

    iput-object p8, p0, LX/1Qj;->h:LX/1Q0;

    iput-object p9, p0, LX/1Qj;->i:LX/1QC;

    iput-object p10, p0, LX/1Qj;->j:LX/1QD;

    iput-object p11, p0, LX/1Qj;->k:LX/1Q3;

    iput-object p12, p0, LX/1Qj;->l:LX/1QK;

    iput-object p13, p0, LX/1Qj;->m:LX/1Q5;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, LX/1Qj;

    invoke-static {v13}, LX/1Q8;->a(LX/0QB;)LX/1Q8;

    move-result-object v1

    check-cast v1, LX/1Q8;

    invoke-static {v13}, LX/1QF;->b(LX/0QB;)LX/1QF;

    move-result-object v2

    check-cast v2, LX/1QF;

    const-class v3, LX/1Q1;

    invoke-interface {v13, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Q1;

    invoke-static {v13}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object v4

    check-cast v4, LX/1Q2;

    invoke-static {v13}, LX/1Pz;->b(LX/0QB;)LX/1Pz;

    move-result-object v5

    check-cast v5, LX/1Pz;

    invoke-static {v13}, LX/1Q4;->a(LX/0QB;)LX/1Q4;

    move-result-object v6

    check-cast v6, LX/1Q4;

    const-class v7, LX/1QI;

    invoke-interface {v13, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1QI;

    invoke-static {v13}, LX/1Q0;->b(LX/0QB;)LX/1Q0;

    move-result-object v8

    check-cast v8, LX/1Q0;

    const-class v9, LX/1QC;

    invoke-interface {v13, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1QC;

    invoke-static {v13}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v10

    check-cast v10, LX/1QD;

    invoke-static {v13}, LX/1Q3;->b(LX/0QB;)LX/1Q3;

    move-result-object v11

    check-cast v11, LX/1Q3;

    invoke-static {v13}, LX/1QK;->a(LX/0QB;)LX/1QK;

    move-result-object v12

    check-cast v12, LX/1QK;

    const-class v14, LX/1Q5;

    invoke-interface {v13, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1Q5;

    invoke-static/range {v0 .. v13}, LX/1Qj;->a(LX/1Qj;LX/1Q8;LX/1QF;LX/1Q1;LX/1Q2;LX/1Pz;LX/1Q4;LX/1QI;LX/1Q0;LX/1QC;LX/1QD;LX/1Q3;LX/1QK;LX/1Q5;)V

    return-void
.end method


# virtual methods
.method public a()LX/1Q9;
    .locals 1

    .prologue
    .line 245041
    iget-object v0, p0, LX/1Qj;->a:LX/1Q8;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6
    .param p5    # LX/5P5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245042
    iget-object v0, p0, LX/1Qj;->h:LX/1Q0;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Q0;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 245043
    iget-object v0, p0, LX/1Qj;->b:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 245044
    iget-object v0, p0, LX/1Qj;->b:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1
    .param p1    # LX/1R6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 245045
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 245046
    return-void
.end method

.method public a(LX/1Rb;)V
    .locals 0

    .prologue
    .line 245047
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 245048
    return-void
.end method

.method public a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 245049
    return-void
.end method

.method public a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 245050
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 245053
    iget-object v0, p0, LX/1Qj;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/22C;)V

    .line 245054
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 245051
    iget-object v0, p0, LX/1Qj;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/34p;)V

    .line 245052
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 245073
    iget-object v0, p0, LX/1Qj;->p:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->a(LX/5Oj;)V

    .line 245074
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 245071
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 245072
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245069
    iget-object v0, p0, LX/1Qj;->q:LX/1QL;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245070
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 245067
    iget-object v0, p0, LX/1Qj;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 245068
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 245065
    iget-object v0, p0, LX/1Qj;->d:LX/1Q2;

    invoke-virtual {v0, p1, p2}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 245066
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 245064
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245062
    iget-object v0, p0, LX/1Qj;->e:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245063
    return-void
.end method

.method public final varargs a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 245060
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 245061
    return-void
.end method

.method public final varargs a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 245058
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 245059
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 245057
    iget-object v0, p0, LX/1Qj;->b:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 245029
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 245030
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 245055
    iget-object v0, p0, LX/1Qj;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->b(LX/22C;)V

    .line 245056
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 244996
    iget-object v0, p0, LX/1Qj;->p:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->b(LX/5Oj;)V

    .line 244997
    return-void
.end method

.method public b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 244999
    iget-object v0, p0, LX/1Qj;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 245000
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245001
    iget-object v0, p0, LX/1Qj;->b:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 245002
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245003
    iget-object v0, p0, LX/1Qj;->e:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 245004
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 244998
    return-void
.end method

.method public c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 245005
    iget-object v0, p0, LX/1Qj;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 245006
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
    .end annotation

    .prologue
    .line 245007
    iget-object v0, p0, LX/1Qj;->s:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 245008
    iget-object v0, p0, LX/1Qj;->n:Landroid/content/Context;

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
    .end annotation

    .prologue
    .line 245009
    iget-object v0, p0, LX/1Qj;->s:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
    .end annotation

    .prologue
    .line 245010
    iget-object v0, p0, LX/1Qj;->s:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
    .end annotation

    .prologue
    .line 245011
    iget-object v0, p0, LX/1Qj;->s:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 245012
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245013
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 245014
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 245015
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 245016
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 245017
    iget-object v0, p0, LX/1Qj;->j:LX/1QD;

    invoke-virtual {v0}, LX/1QD;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245018
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 245019
    iget-object v0, p0, LX/1Qj;->o:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->k()V

    .line 245020
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 245021
    const/4 v0, 0x0

    return v0
.end method

.method public m()LX/1f9;
    .locals 1

    .prologue
    .line 245022
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 245023
    iget-object v0, p0, LX/1Qj;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 245024
    return-void
.end method

.method public o()LX/1Rb;
    .locals 1

    .prologue
    .line 245025
    const/4 v0, 0x0

    return-object v0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 245026
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 245027
    const/4 v0, 0x0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 245028
    iget-object v0, p0, LX/1Qj;->l:LX/1QK;

    invoke-virtual {v0}, LX/1QK;->r()Z

    move-result v0

    return v0
.end method
