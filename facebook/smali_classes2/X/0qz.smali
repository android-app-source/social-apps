.class public LX/0qz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Lcom/facebook/user/model/User;

.field public final b:LX/0SG;

.field private final c:LX/0kb;

.field public final d:LX/0Uh;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rW;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/user/model/User;LX/0SG;LX/0kb;LX/0Uh;)V
    .locals 1
    .param p1    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 148844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148845
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 148846
    iput-object v0, p0, LX/0qz;->e:LX/0Ot;

    .line 148847
    iput-object p1, p0, LX/0qz;->a:Lcom/facebook/user/model/User;

    .line 148848
    iput-object p2, p0, LX/0qz;->b:LX/0SG;

    .line 148849
    iput-object p3, p0, LX/0qz;->c:LX/0kb;

    .line 148850
    iput-object p4, p0, LX/0qz;->d:LX/0Uh;

    .line 148851
    return-void
.end method

.method public static a(LX/0QB;)LX/0qz;
    .locals 1

    .prologue
    .line 148843
    invoke-static {p0}, LX/0qz;->b(LX/0QB;)LX/0qz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/api/feedtype/FeedType;ILX/0gf;J)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 4

    .prologue
    .line 148717
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 148718
    iput-object v1, v0, LX/0rT;->a:LX/0rS;

    .line 148719
    move-object v0, v0

    .line 148720
    iput-object p0, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 148721
    move-object v0, v0

    .line 148722
    iput p1, v0, LX/0rT;->c:I

    .line 148723
    move-object v0, v0

    .line 148724
    invoke-virtual {v0, p2}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    const/4 v1, 0x0

    .line 148725
    iput-boolean v1, v0, LX/0rT;->j:Z

    .line 148726
    move-object v0, v0

    .line 148727
    sget-object v1, LX/0rU;->HEAD:LX/0rU;

    .line 148728
    iput-object v1, v0, LX/0rT;->k:LX/0rU;

    .line 148729
    move-object v0, v0

    .line 148730
    iput-wide p3, v0, LX/0rT;->e:J

    .line 148731
    move-object v0, v0

    .line 148732
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0qz;LX/0rT;)V
    .locals 0

    .prologue
    .line 148840
    invoke-static {p0, p1}, LX/0qz;->b(LX/0qz;LX/0rT;)V

    .line 148841
    invoke-static {p0, p1}, LX/0qz;->d(LX/0qz;LX/0rT;)V

    .line 148842
    return-void
.end method

.method public static b(LX/0QB;)LX/0qz;
    .locals 5

    .prologue
    .line 148836
    new-instance v4, LX/0qz;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/0qz;-><init>(Lcom/facebook/user/model/User;LX/0SG;LX/0kb;LX/0Uh;)V

    .line 148837
    const/16 v0, 0x68b

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    .line 148838
    iput-object v0, v4, LX/0qz;->e:LX/0Ot;

    .line 148839
    return-object v4
.end method

.method public static b(LX/0qz;LX/0rT;)V
    .locals 1

    .prologue
    .line 148828
    iget-object v0, p0, LX/0qz;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rW;

    .line 148829
    iget-boolean p0, v0, LX/0rW;->a:Z

    if-eqz p0, :cond_0

    .line 148830
    iget-object p0, v0, LX/0rW;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1vR;

    invoke-virtual {p0}, LX/0rY;->a()LX/0Px;

    move-result-object p0

    .line 148831
    iput-object p0, p1, LX/0rT;->o:LX/0Px;

    .line 148832
    :goto_0
    return-void

    .line 148833
    :cond_0
    iget-object p0, v0, LX/0rW;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0rX;

    invoke-virtual {p0}, LX/0rY;->a()LX/0Px;

    move-result-object p0

    .line 148834
    iput-object p0, p1, LX/0rT;->n:LX/0Px;

    .line 148835
    goto :goto_0
.end method

.method public static d(LX/0qz;LX/0rT;)V
    .locals 2

    .prologue
    .line 148820
    iget-object v0, p0, LX/0qz;->a:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0qz;->a:Lcom/facebook/user/model/User;

    .line 148821
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 148822
    :goto_0
    iget-object v1, p0, LX/0qz;->b:LX/0SG;

    invoke-static {v0, v1}, LX/0rh;->a(Ljava/lang/String;LX/0SG;)Ljava/lang/String;

    move-result-object v0

    .line 148823
    iput-object v0, p1, LX/0rT;->h:Ljava/lang/String;

    .line 148824
    iget-object v0, p0, LX/0qz;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 148825
    iput-object v0, p1, LX/0rT;->m:Lcom/facebook/common/callercontext/CallerContext;

    .line 148826
    return-void

    .line 148827
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 2

    .prologue
    .line 148803
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    .line 148804
    iput-object p5, v0, LX/0rT;->a:LX/0rS;

    .line 148805
    move-object v0, v0

    .line 148806
    iput-object p1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 148807
    move-object v0, v0

    .line 148808
    iput p3, v0, LX/0rT;->c:I

    .line 148809
    move-object v0, v0

    .line 148810
    iput-object p2, v0, LX/0rT;->g:Ljava/lang/String;

    .line 148811
    move-object v0, v0

    .line 148812
    invoke-virtual {v0, p6}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    .line 148813
    iput-boolean p4, v0, LX/0rT;->r:Z

    .line 148814
    move-object v0, v0

    .line 148815
    sget-object v1, LX/0rU;->HEAD:LX/0rU;

    .line 148816
    iput-object v1, v0, LX/0rT;->k:LX/0rU;

    .line 148817
    move-object v0, v0

    .line 148818
    invoke-static {p0, v0}, LX/0qz;->a(LX/0qz;LX/0rT;)V

    .line 148819
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZLX/0rS;LX/0gf;ZZJ)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 4

    .prologue
    .line 148775
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    .line 148776
    iput-object p5, v0, LX/0rT;->a:LX/0rS;

    .line 148777
    move-object v0, v0

    .line 148778
    iput-object p1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 148779
    move-object v0, v0

    .line 148780
    iput p3, v0, LX/0rT;->c:I

    .line 148781
    move-object v0, v0

    .line 148782
    iput-object p2, v0, LX/0rT;->f:Ljava/lang/String;

    .line 148783
    move-object v0, v0

    .line 148784
    invoke-virtual {v0, p6}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    .line 148785
    iput-boolean p4, v0, LX/0rT;->j:Z

    .line 148786
    move-object v0, v0

    .line 148787
    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    .line 148788
    iput-object v1, v0, LX/0rT;->k:LX/0rU;

    .line 148789
    move-object v0, v0

    .line 148790
    iput-boolean p7, v0, LX/0rT;->p:Z

    .line 148791
    move-object v1, v0

    .line 148792
    if-eqz p8, :cond_1

    iget-object v0, p0, LX/0qz;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 148793
    :goto_0
    iput-boolean v0, v1, LX/0rT;->q:Z

    .line 148794
    move-object v0, v1

    .line 148795
    iput-wide p9, v0, LX/0rT;->e:J

    .line 148796
    move-object v0, v0

    .line 148797
    iget-object v1, p0, LX/0qz;->d:LX/0Uh;

    const/16 v2, 0x61c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 148798
    if-eqz v1, :cond_0

    .line 148799
    invoke-static {p0, v0}, LX/0qz;->b(LX/0qz;LX/0rT;)V

    .line 148800
    :cond_0
    invoke-static {p0, v0}, LX/0qz;->d(LX/0qz;LX/0rT;)V

    .line 148801
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    return-object v0

    .line 148802
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 2

    .prologue
    .line 148751
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v1, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v1, :cond_0

    .line 148752
    const-string p2, "cold_start_cursor"

    .line 148753
    :cond_0
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    .line 148754
    iput-object p6, v0, LX/0rT;->a:LX/0rS;

    .line 148755
    move-object v0, v0

    .line 148756
    iput-object p1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 148757
    move-object v0, v0

    .line 148758
    iput p3, v0, LX/0rT;->c:I

    .line 148759
    move-object v0, v0

    .line 148760
    iput-object p2, v0, LX/0rT;->g:Ljava/lang/String;

    .line 148761
    move-object v0, v0

    .line 148762
    invoke-virtual {v0, p7}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    .line 148763
    iput-boolean p4, v0, LX/0rT;->j:Z

    .line 148764
    move-object v0, v0

    .line 148765
    const/4 v1, 0x1

    .line 148766
    iput-boolean v1, v0, LX/0rT;->q:Z

    .line 148767
    move-object v0, v0

    .line 148768
    iput-boolean p5, v0, LX/0rT;->r:Z

    .line 148769
    move-object v0, v0

    .line 148770
    sget-object v1, LX/0rU;->HEAD:LX/0rU;

    .line 148771
    iput-object v1, v0, LX/0rT;->k:LX/0rU;

    .line 148772
    move-object v0, v0

    .line 148773
    invoke-static {p0, v0}, LX/0qz;->a(LX/0qz;LX/0rT;)V

    .line 148774
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;Ljava/lang/String;ILX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 2

    .prologue
    .line 148735
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    .line 148736
    iput-object p5, v0, LX/0rT;->a:LX/0rS;

    .line 148737
    move-object v0, v0

    .line 148738
    iput-object p1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 148739
    move-object v0, v0

    .line 148740
    iput p4, v0, LX/0rT;->c:I

    .line 148741
    move-object v0, v0

    .line 148742
    iput-object p2, v0, LX/0rT;->f:Ljava/lang/String;

    .line 148743
    move-object v0, v0

    .line 148744
    invoke-virtual {v0, p6}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    .line 148745
    iput-object v1, v0, LX/0rT;->k:LX/0rU;

    .line 148746
    move-object v0, v0

    .line 148747
    if-eqz p3, :cond_0

    .line 148748
    iput-object p3, v0, LX/0rT;->g:Ljava/lang/String;

    .line 148749
    :cond_0
    invoke-static {p0, v0}, LX/0qz;->a(LX/0qz;LX/0rT;)V

    .line 148750
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 148733
    iput-object p1, p0, LX/0qz;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 148734
    return-void
.end method
