.class public LX/0cZ;
.super LX/0Uq;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0cZ;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 88997
    const-string v0, "FbSharedPreferences Init Lock Held"

    invoke-direct {p0, p1, v0}, LX/0Uq;-><init>(LX/0Sh;Ljava/lang/String;)V

    .line 88998
    return-void
.end method

.method public static b(LX/0QB;)LX/0cZ;
    .locals 4

    .prologue
    .line 88999
    sget-object v0, LX/0cZ;->a:LX/0cZ;

    if-nez v0, :cond_1

    .line 89000
    const-class v1, LX/0cZ;

    monitor-enter v1

    .line 89001
    :try_start_0
    sget-object v0, LX/0cZ;->a:LX/0cZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 89002
    if-eqz v2, :cond_0

    .line 89003
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 89004
    new-instance p0, LX/0cZ;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/0cZ;-><init>(LX/0Sh;)V

    .line 89005
    move-object v0, p0

    .line 89006
    sput-object v0, LX/0cZ;->a:LX/0cZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89007
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 89008
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 89009
    :cond_1
    sget-object v0, LX/0cZ;->a:LX/0cZ;

    return-object v0

    .line 89010
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 89011
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
