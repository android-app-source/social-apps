.class public LX/0wo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0wo;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 160797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160798
    return-void
.end method

.method public static a(LX/0QB;)LX/0wo;
    .locals 3

    .prologue
    .line 160799
    sget-object v0, LX/0wo;->a:LX/0wo;

    if-nez v0, :cond_1

    .line 160800
    const-class v1, LX/0wo;

    monitor-enter v1

    .line 160801
    :try_start_0
    sget-object v0, LX/0wo;->a:LX/0wo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 160802
    if-eqz v2, :cond_0

    .line 160803
    :try_start_1
    new-instance v0, LX/0wo;

    invoke-direct {v0}, LX/0wo;-><init>()V

    .line 160804
    move-object v0, v0

    .line 160805
    sput-object v0, LX/0wo;->a:LX/0wo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160806
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 160807
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 160808
    :cond_1
    sget-object v0, LX/0wo;->a:LX/0wo;

    return-object v0

    .line 160809
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 160810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0gW;)V
    .locals 2

    .prologue
    .line 160811
    const-string v0, "in_social_search_experiment"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 160812
    return-void
.end method
