.class public LX/1Gs;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/imagepipeline/decoder/ProgressiveJpegConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Gv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 226128
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Gv;
    .locals 3

    .prologue
    .line 226129
    sget-object v0, LX/1Gs;->a:LX/1Gv;

    if-nez v0, :cond_1

    .line 226130
    const-class v1, LX/1Gs;

    monitor-enter v1

    .line 226131
    :try_start_0
    sget-object v0, LX/1Gs;->a:LX/1Gv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 226132
    if-eqz v2, :cond_0

    .line 226133
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 226134
    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-static {p0}, LX/1Aq;->b(LX/0W3;)LX/1Gv;

    move-result-object p0

    move-object v0, p0

    .line 226135
    sput-object v0, LX/1Gs;->a:LX/1Gv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226136
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 226137
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226138
    :cond_1
    sget-object v0, LX/1Gs;->a:LX/1Gv;

    return-object v0

    .line 226139
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 226140
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226141
    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-static {v0}, LX/1Aq;->b(LX/0W3;)LX/1Gv;

    move-result-object v0

    return-object v0
.end method
