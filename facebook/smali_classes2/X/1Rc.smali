.class public LX/1Rc;
.super LX/1Ra;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "LX/1Ra",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private b:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private c:LX/1aC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aC",
            "<TP;TS;TE;TV;>;"
        }
    .end annotation
.end field

.field private d:LX/1Nt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nt",
            "<TP;TS;TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/1Nt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "LX/1Nt",
            "<TP;TS;TE;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 246536
    invoke-direct {p0}, LX/1Ra;-><init>()V

    .line 246537
    iput-object p1, p0, LX/1Rc;->a:Ljava/lang/Object;

    .line 246538
    iput-object p2, p0, LX/1Rc;->d:LX/1Nt;

    .line 246539
    return-void
.end method

.method private a(LX/1aC;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            "V2:",
            "Landroid/view/View;",
            ">(",
            "LX/1aC",
            "<TP2;*TE;TV2;>;)V"
        }
    .end annotation

    .prologue
    .line 246528
    :try_start_0
    iget-object v0, p0, LX/1Rc;->b:LX/1PW;

    .line 246529
    iget-object v1, p1, LX/1aC;->a:LX/1Nt;

    iget-object v2, p1, LX/1aC;->b:Ljava/lang/Object;

    invoke-interface {v1, p1, v2, v0}, LX/1Nt;->a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p1, LX/1aC;->h:Ljava/lang/Object;

    .line 246530
    iget-object v0, p1, LX/1aC;->d:LX/1aC;

    :goto_0
    if-eqz v0, :cond_0

    .line 246531
    invoke-direct {p0, v0}, LX/1Rc;->a(LX/1aC;)V

    .line 246532
    iget-object v0, v0, LX/1aC;->g:LX/1aC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246533
    :catch_0
    move-exception v0

    .line 246534
    const-string v1, "preparing"

    invoke-static {p1, v0, v1}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 246535
    :cond_0
    return-void
.end method

.method private a(LX/1aC;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            "V1:",
            "Landroid/view/View;",
            "V2:",
            "Landroid/view/View;",
            ">(",
            "LX/1aC",
            "<TP2;*TE;TV1;>;TV1;)V"
        }
    .end annotation

    .prologue
    .line 246479
    :try_start_0
    iget-object v0, p0, LX/1Rc;->b:LX/1PW;

    .line 246480
    iget-object v1, p1, LX/1aC;->a:LX/1Nt;

    iget-object v2, p1, LX/1aC;->b:Ljava/lang/Object;

    iget-object v3, p1, LX/1aC;->h:Ljava/lang/Object;

    invoke-interface {v1, v2, v3, v0, p2}, LX/1Nt;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 246481
    iget-object v0, p1, LX/1aC;->d:LX/1aC;

    :goto_0
    if-eqz v0, :cond_0

    .line 246482
    iget v1, v0, LX/1aC;->c:I

    invoke-static {p2, v1}, LX/1aP;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 246483
    invoke-direct {p0, v0, v1}, LX/1Rc;->a(LX/1aC;Landroid/view/View;)V

    .line 246484
    iget-object v0, v0, LX/1aC;->g:LX/1aC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246485
    :catch_0
    move-exception v0

    .line 246486
    const-string v1, "binding"

    invoke-static {p1, v0, v1}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 246487
    :cond_0
    return-void
.end method

.method private static a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 246521
    if-nez p0, :cond_0

    .line 246522
    const-string v0, "Null partholder"

    .line 246523
    :goto_0
    invoke-static {v0, p2, p1}, LX/5Oo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246524
    return-void

    .line 246525
    :cond_0
    iget-object v0, p0, LX/1aC;->a:LX/1Nt;

    if-nez v0, :cond_1

    .line 246526
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 246527
    :cond_1
    iget-object v0, p0, LX/1aC;->a:LX/1Nt;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/1aC;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            "V2:",
            "Landroid/view/View;",
            ">(",
            "LX/1aC",
            "<TP2;*TE;TV2;>;)V"
        }
    .end annotation

    .prologue
    .line 246512
    :try_start_0
    iget-object v0, p0, LX/1Rc;->b:LX/1PW;

    .line 246513
    iget-object v1, p1, LX/1aC;->a:LX/1Nt;

    iget-object v2, p1, LX/1aC;->b:Ljava/lang/Object;

    iget-object v3, p1, LX/1aC;->h:Ljava/lang/Object;

    invoke-interface {v1, v2, v3, v0}, LX/1Nt;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V

    .line 246514
    const/4 v1, 0x0

    iput-object v1, p1, LX/1aC;->h:Ljava/lang/Object;

    .line 246515
    iget-object v0, p1, LX/1aC;->d:LX/1aC;

    :goto_0
    if-eqz v0, :cond_0

    .line 246516
    invoke-direct {p0, v0}, LX/1Rc;->b(LX/1aC;)V

    .line 246517
    iget-object v0, v0, LX/1aC;->g:LX/1aC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246518
    :catch_0
    move-exception v0

    .line 246519
    const-string v1, "releasing"

    invoke-static {p1, v0, v1}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 246520
    :cond_0
    return-void
.end method

.method private b(LX/1aC;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P2:",
            "Ljava/lang/Object;",
            "V1:",
            "Landroid/view/View;",
            "V2:",
            "Landroid/view/View;",
            ">(",
            "LX/1aC",
            "<TP2;*TE;TV1;>;TV1;)V"
        }
    .end annotation

    .prologue
    .line 246502
    :try_start_0
    iget-object v0, p1, LX/1aC;->e:LX/1aC;

    .line 246503
    :goto_0
    if-eqz v0, :cond_0

    .line 246504
    iget v1, v0, LX/1aC;->c:I

    invoke-static {p2, v1}, LX/1aP;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 246505
    invoke-direct {p0, v0, v1}, LX/1Rc;->b(LX/1aC;Landroid/view/View;)V

    .line 246506
    iget-object v0, v0, LX/1aC;->f:LX/1aC;

    goto :goto_0

    .line 246507
    :cond_0
    iget-object v0, p0, LX/1Rc;->b:LX/1PW;

    .line 246508
    iget-object v1, p1, LX/1aC;->a:LX/1Nt;

    iget-object v2, p1, LX/1aC;->b:Ljava/lang/Object;

    iget-object p0, p1, LX/1aC;->h:Ljava/lang/Object;

    invoke-interface {v1, v2, p0, v0, p2}, LX/1Nt;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246509
    :goto_1
    return-void

    .line 246510
    :catch_0
    move-exception v0

    .line 246511
    const-string v1, "unbinding"

    invoke-static {p1, v0, v1}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1PW;)V
    .locals 4

    .prologue
    .line 246496
    :try_start_0
    iput-object p1, p0, LX/1Rc;->b:LX/1PW;

    .line 246497
    new-instance v0, LX/1aC;

    iget-object v1, p0, LX/1Rc;->d:LX/1Nt;

    const/4 v2, -0x1

    iget-object v3, p0, LX/1Rc;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, v3}, LX/1aC;-><init>(LX/1Nt;ILjava/lang/Object;)V

    iput-object v0, p0, LX/1Rc;->c:LX/1aC;

    .line 246498
    iget-object v0, p0, LX/1Rc;->c:LX/1aC;

    invoke-direct {p0, v0}, LX/1Rc;->a(LX/1aC;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246499
    :goto_0
    return-void

    .line 246500
    :catch_0
    move-exception v0

    .line 246501
    iget-object v1, p0, LX/1Rc;->c:LX/1aC;

    const-string v2, "preparing"

    invoke-static {v1, v0, v2}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 246494
    iget-object v0, p0, LX/1Rc;->c:LX/1aC;

    invoke-direct {p0, v0, p1}, LX/1Rc;->a(LX/1aC;Landroid/view/View;)V

    .line 246495
    return-void
.end method

.method public final b(LX/1PW;)V
    .locals 3

    .prologue
    .line 246490
    :try_start_0
    iget-object v0, p0, LX/1Rc;->c:LX/1aC;

    invoke-direct {p0, v0}, LX/1Rc;->b(LX/1aC;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246491
    :goto_0
    return-void

    .line 246492
    :catch_0
    move-exception v0

    .line 246493
    iget-object v1, p0, LX/1Rc;->c:LX/1aC;

    const-string v2, "releasing"

    invoke-static {v1, v0, v2}, LX/1Rc;->a(LX/1aC;Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 246488
    iget-object v0, p0, LX/1Rc;->c:LX/1aC;

    invoke-direct {p0, v0, p1}, LX/1Rc;->b(LX/1aC;Landroid/view/View;)V

    .line 246489
    return-void
.end method
