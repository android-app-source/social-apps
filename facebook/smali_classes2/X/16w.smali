.class public LX/16w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/16x;

.field private static volatile f:LX/16w;


# instance fields
.field public b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public c:Lcom/facebook/performancelogger/PerformanceLogger;

.field public d:LX/11i;

.field private e:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 187771
    new-instance v0, LX/16x;

    invoke-direct {v0}, LX/16x;-><init>()V

    sput-object v0, LX/16w;->a:LX/16x;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;Lcom/facebook/performancelogger/PerformanceLogger;LX/11i;Ljava/util/Random;)V
    .locals 0
    .param p4    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 187772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187773
    iput-object p1, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 187774
    iput-object p2, p0, LX/16w;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 187775
    iput-object p3, p0, LX/16w;->d:LX/11i;

    .line 187776
    iput-object p4, p0, LX/16w;->e:Ljava/util/Random;

    .line 187777
    return-void
.end method

.method public static a(LX/0QB;)LX/16w;
    .locals 7

    .prologue
    .line 187778
    sget-object v0, LX/16w;->f:LX/16w;

    if-nez v0, :cond_1

    .line 187779
    const-class v1, LX/16w;

    monitor-enter v1

    .line 187780
    :try_start_0
    sget-object v0, LX/16w;->f:LX/16w;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 187781
    if-eqz v2, :cond_0

    .line 187782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 187783
    new-instance p0, LX/16w;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v5

    check-cast v5, LX/11i;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    invoke-direct {p0, v3, v4, v5, v6}, LX/16w;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;Lcom/facebook/performancelogger/PerformanceLogger;LX/11i;Ljava/util/Random;)V

    .line 187784
    move-object v0, p0

    .line 187785
    sput-object v0, LX/16w;->f:LX/16w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187786
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 187787
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 187788
    :cond_1
    sget-object v0, LX/16w;->f:LX/16w;

    return-object v0

    .line 187789
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 187790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 187791
    const-wide/16 v0, 0x4

    invoke-static {v0, v1}, LX/00k;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187792
    :goto_0
    return-void

    .line 187793
    :cond_0
    iget-object v0, p0, LX/16w;->e:Ljava/util/Random;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 187794
    :pswitch_0
    const v3, 0x30003

    const/4 v5, 0x2

    const/4 v4, 0x0

    const v6, 0x30001

    .line 187795
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 187796
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 187797
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v10

    .line 187798
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 187799
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 187800
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v3, v4, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 187801
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v10, :cond_1

    :goto_1
    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 187802
    goto :goto_0

    .line 187803
    :pswitch_1
    const v3, 0x30005

    const/4 v4, 0x0

    const v7, 0x30002

    .line 187804
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 187805
    iget-object v2, p0, LX/16w;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v5, "PerfLogger"

    invoke-interface {v2, v7, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 187806
    iget-object v2, p0, LX/16w;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v5, "PerfLogger"

    invoke-interface {v2, v7, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->g(ILjava/lang/String;)Z

    move-result v5

    .line 187807
    iget-object v2, p0, LX/16w;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v6, "PerfLogger"

    invoke-interface {v2, v7, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 187808
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 187809
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v3, v4, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 187810
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    :goto_2
    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 187811
    goto :goto_0

    .line 187812
    :pswitch_2
    const v3, 0x30004

    const/4 v4, 0x0

    .line 187813
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 187814
    iget-object v2, p0, LX/16w;->d:LX/11i;

    sget-object v5, LX/16w;->a:LX/16x;

    invoke-interface {v2, v5}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 187815
    iget-object v2, p0, LX/16w;->d:LX/11i;

    sget-object v5, LX/16w;->a:LX/16x;

    invoke-interface {v2, v5}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v5

    .line 187816
    if-eqz v5, :cond_4

    .line 187817
    const-string v2, "PerfLogger"

    const v6, 0x1d9f679b

    invoke-static {v5, v2, v6}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 187818
    const-string v2, "PerfLogger"

    invoke-interface {v5, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    .line 187819
    const-string v6, "PerfLogger"

    const v7, 0x73bbf04d

    invoke-static {v5, v6, v7}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 187820
    iget-object v5, p0, LX/16w;->d:LX/11i;

    sget-object v6, LX/16w;->a:LX/16x;

    invoke-interface {v5, v6}, LX/11i;->b(LX/0Pq;)V

    move v5, v2

    .line 187821
    :goto_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 187822
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v3, v4, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 187823
    iget-object v2, p0, LX/16w;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v5, :cond_3

    const/4 v5, 0x2

    :goto_4
    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 187824
    goto/16 :goto_0

    .line 187825
    :cond_1
    const/4 v5, 0x3

    goto/16 :goto_1

    .line 187826
    :cond_2
    const/4 v5, 0x3

    goto :goto_2

    .line 187827
    :cond_3
    const/4 v5, 0x3

    goto :goto_4

    :cond_4
    move v5, v4

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
