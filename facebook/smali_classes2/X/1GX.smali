.class public final LX/1GX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GE;


# instance fields
.field private final a:LX/0pk;


# direct methods
.method public constructor <init>(LX/0pk;)V
    .locals 0

    .prologue
    .line 225699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225700
    iput-object p1, p0, LX/1GX;->a:LX/0pk;

    .line 225701
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 225698
    return-void
.end method

.method public final a(LX/1gC;)V
    .locals 1

    .prologue
    .line 225696
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->b()V

    .line 225697
    return-void
.end method

.method public final b(LX/1gC;)V
    .locals 1

    .prologue
    .line 225685
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    invoke-virtual {v0}, LX/0pk;->c()V

    .line 225686
    return-void
.end method

.method public final c(LX/1gC;)V
    .locals 5

    .prologue
    .line 225702
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    .line 225703
    sget-object v1, LX/17N;->WRITE_ATTEMPTS:LX/17N;

    const-wide/16 v3, 0x1

    invoke-static {v0, v1, v3, v4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 225704
    return-void
.end method

.method public final d(LX/1gC;)V
    .locals 0

    .prologue
    .line 225695
    return-void
.end method

.method public final e(LX/1gC;)V
    .locals 5

    .prologue
    .line 225692
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    .line 225693
    sget-object v1, LX/17N;->READ_EXCEPTION_COUNT:LX/17N;

    const-wide/16 v3, 0x1

    invoke-static {v0, v1, v3, v4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 225694
    return-void
.end method

.method public final f(LX/1gC;)V
    .locals 5

    .prologue
    .line 225689
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    .line 225690
    sget-object v1, LX/17N;->WRITE_EXCEPTION_COUNT:LX/17N;

    const-wide/16 v3, 0x1

    invoke-static {v0, v1, v3, v4}, LX/0pk;->a(LX/0pk;LX/17N;J)V

    .line 225691
    return-void
.end method

.method public final g(LX/1gC;)V
    .locals 6

    .prologue
    .line 225687
    iget-object v0, p0, LX/1GX;->a:LX/0pk;

    invoke-interface {p1}, LX/1gC;->g()LX/37E;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1}, LX/1gC;->c()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, LX/0pk;->a(LX/37E;IJ)V

    .line 225688
    return-void
.end method
