.class public LX/1oY;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1oY;


# instance fields
.field public final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/1oY;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)LX/1oY;
    .locals 2

    invoke-static {p0}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v1, LX/1oY;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1oY;->a:LX/1oY;

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1om;->a(Landroid/content/Context;)V

    new-instance v0, LX/1oY;

    invoke-direct {v0, p0}, LX/1oY;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/1oY;->a:LX/1oY;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, LX/1oY;->a:LX/1oY;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static varargs a(Landroid/content/pm/PackageInfo;[LX/1oa;)LX/1oa;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const-string v0, "GoogleSignatureVerifier"

    const-string v2, "Package has more than one signature."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, LX/1oq;

    iget-object v3, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-direct {v2, v3}, LX/1oq;-><init>([B)V

    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_3

    aget-object v3, p1, v0

    invoke-virtual {v3, v2}, LX/1oa;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-object v0, p1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/pm/PackageInfo;Z)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_1

    iget-object v2, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_0

    sget-object v2, LX/1oZ;->a:[LX/1oa;

    invoke-static {p0, v2}, LX/1oY;->a(Landroid/content/pm/PackageInfo;[LX/1oa;)LX/1oa;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    new-array v2, v0, [LX/1oa;

    sget-object v3, LX/1oZ;->a:[LX/1oa;

    aget-object v3, v3, v1

    aput-object v3, v2, v1

    invoke-static {p0, v2}, LX/1oY;->a(Landroid/content/pm/PackageInfo;[LX/1oa;)LX/1oa;

    move-result-object v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/content/pm/PackageInfo;Z)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v0, v0

    if-eq v0, v2, :cond_0

    const-string v0, "GoogleSignatureVerifier"

    const-string v2, "Package has more than one signature."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    new-instance v3, LX/1oq;

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-direct {v3, v0}, LX/1oq;-><init>([B)V

    if-eqz p1, :cond_2

    invoke-static {}, LX/1om;->a()Ljava/util/Set;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oc;

    invoke-virtual {v3, v0}, LX/1oa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-static {}, LX/1om;->b()Ljava/util/Set;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 2

    const/16 v0, 0x40

    :try_start_0
    invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const/4 p2, 0x1

    const/4 v1, 0x0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object p1, p0, LX/1oY;->b:Landroid/content/Context;

    invoke-static {p1}, LX/1oW;->c(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {v0, p2}, LX/1oY;->b(Landroid/content/pm/PackageInfo;Z)Z

    move-result v1

    goto :goto_0

    :cond_2
    invoke-static {v0, v1}, LX/1oY;->b(Landroid/content/pm/PackageInfo;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0, p2}, LX/1oY;->b(Landroid/content/pm/PackageInfo;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "GoogleSignatureVerifier"

    const-string p2, "Test-keys aren\'t accepted on this build."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
