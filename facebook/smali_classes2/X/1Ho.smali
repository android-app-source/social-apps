.class public abstract LX/1Ho;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Hq;

.field public final b:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>(LX/1Hq;Ljava/security/SecureRandom;)V
    .locals 0

    .prologue
    .line 227714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227715
    iput-object p1, p0, LX/1Ho;->a:LX/1Hq;

    .line 227716
    iput-object p2, p0, LX/1Ho;->b:Ljava/security/SecureRandom;

    .line 227717
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;
    .locals 1

    .prologue
    .line 227711
    invoke-virtual {p0, p1}, LX/1Ho;->c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;
    .locals 3

    .prologue
    .line 227713
    new-instance v0, LX/1Hz;

    iget-object v1, p0, LX/1Ho;->a:LX/1Hq;

    sget-object v2, LX/1Hy;->KEY_128:LX/1Hy;

    invoke-direct {v0, p1, v1, v2}, LX/1Hz;-><init>(Lcom/facebook/crypto/keychain/KeyChain;LX/1Hq;LX/1Hy;)V

    return-object v0
.end method

.method public final c(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;
    .locals 3

    .prologue
    .line 227712
    new-instance v0, LX/1Hz;

    iget-object v1, p0, LX/1Ho;->a:LX/1Hq;

    sget-object v2, LX/1Hy;->KEY_256:LX/1Hy;

    invoke-direct {v0, p1, v1, v2}, LX/1Hz;-><init>(Lcom/facebook/crypto/keychain/KeyChain;LX/1Hq;LX/1Hy;)V

    return-object v0
.end method
