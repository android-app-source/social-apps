.class public LX/1B1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0b2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211570
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1B1;->a:Ljava/util/List;

    .line 211571
    return-void
.end method

.method public static a(LX/0QB;)LX/1B1;
    .locals 1

    .prologue
    .line 211572
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    .line 211573
    move-object v0, v0

    .line 211574
    return-object v0
.end method


# virtual methods
.method public final a(LX/0b4;)V
    .locals 3

    .prologue
    .line 211575
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211576
    const/4 v0, 0x0

    iget-object v1, p0, LX/1B1;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 211577
    iget-object v0, p0, LX/1B1;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b2;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 211578
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 211579
    :cond_0
    return-void
.end method

.method public final varargs a([LX/0b2;)V
    .locals 3

    .prologue
    .line 211580
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 211581
    invoke-virtual {p0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 211582
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211583
    :cond_0
    return-void
.end method

.method public final a(LX/0b2;)Z
    .locals 1

    .prologue
    .line 211584
    if-nez p1, :cond_0

    .line 211585
    const/4 v0, 0x0

    .line 211586
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1B1;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/0b4;)V
    .locals 3

    .prologue
    .line 211587
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 211588
    const/4 v0, 0x0

    iget-object v1, p0, LX/1B1;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 211589
    iget-object v0, p0, LX/1B1;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b2;

    invoke-virtual {p1, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 211590
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 211591
    :cond_0
    return-void
.end method
