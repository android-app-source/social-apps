.class public LX/1tQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1tQ;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336478
    iput-object p1, p0, LX/1tQ;->a:LX/0Uh;

    .line 336479
    return-void
.end method

.method public static a(LX/0QB;)LX/1tQ;
    .locals 4

    .prologue
    .line 336480
    sget-object v0, LX/1tQ;->b:LX/1tQ;

    if-nez v0, :cond_1

    .line 336481
    const-class v1, LX/1tQ;

    monitor-enter v1

    .line 336482
    :try_start_0
    sget-object v0, LX/1tQ;->b:LX/1tQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 336483
    if-eqz v2, :cond_0

    .line 336484
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 336485
    new-instance p0, LX/1tQ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/1tQ;-><init>(LX/0Uh;)V

    .line 336486
    move-object v0, p0

    .line 336487
    sput-object v0, LX/1tQ;->b:LX/1tQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 336488
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 336489
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 336490
    :cond_1
    sget-object v0, LX/1tQ;->b:LX/1tQ;

    return-object v0

    .line 336491
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 336492
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
