.class public final enum LX/1lo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1lo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1lo;

.field public static final enum ALWAYS:LX/1lo;

.field public static final enum CACHED_ONLY:LX/1lo;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312392
    new-instance v0, LX/1lo;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, LX/1lo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lo;->ALWAYS:LX/1lo;

    .line 312393
    new-instance v0, LX/1lo;

    const-string v1, "CACHED_ONLY"

    invoke-direct {v0, v1, v3}, LX/1lo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lo;->CACHED_ONLY:LX/1lo;

    .line 312394
    const/4 v0, 0x2

    new-array v0, v0, [LX/1lo;

    sget-object v1, LX/1lo;->ALWAYS:LX/1lo;

    aput-object v1, v0, v2

    sget-object v1, LX/1lo;->CACHED_ONLY:LX/1lo;

    aput-object v1, v0, v3

    sput-object v0, LX/1lo;->$VALUES:[LX/1lo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 312395
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1lo;
    .locals 1

    .prologue
    .line 312396
    const-class v0, LX/1lo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1lo;

    return-object v0
.end method

.method public static values()[LX/1lo;
    .locals 1

    .prologue
    .line 312397
    sget-object v0, LX/1lo;->$VALUES:[LX/1lo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1lo;

    return-object v0
.end method
