.class public LX/19r;
.super LX/041;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/video/exoserviceclient/NonPlayerSessionListener$ServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/19l;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/19l;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/19l;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208817
    invoke-direct {p0}, LX/041;-><init>()V

    .line 208818
    const-class v0, LX/19r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/19r;->a:Ljava/lang/String;

    .line 208819
    iput-object p1, p0, LX/19r;->c:LX/19l;

    .line 208820
    iput-object p2, p0, LX/19r;->d:LX/0Ot;

    .line 208821
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/19r;->b:Ljava/util/Set;

    .line 208822
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 208816
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 208815
    return-void
.end method

.method public final a(IIF)V
    .locals 0

    .prologue
    .line 208814
    return-void
.end method

.method public final a(IJ)V
    .locals 0

    .prologue
    .line 208810
    return-void
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerServiceEvent;)V
    .locals 5

    .prologue
    .line 208791
    invoke-static {p1}, LX/0H9;->fromVal(I)LX/0H9;

    move-result-object v0

    .line 208792
    sget-object v1, LX/7KX;->a:[I

    invoke-virtual {v0}, LX/0H9;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 208793
    iget-object v1, p0, LX/19r;->a:Ljava/lang/String;

    const-string v2, "Unknown event type callback: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208794
    :goto_0
    return-void

    .line 208795
    :pswitch_0
    check-cast p2, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;

    .line 208796
    new-instance v0, LX/7KV;

    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/facebook/exoplayer/ipc/VpsPrefetchCompleteEvent;->b:Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    invoke-direct {v0, v1, v2}, LX/7KV;-><init>(Ljava/lang/String;Lcom/facebook/exoplayer/ipc/VideoCacheStatus;)V

    .line 208797
    iget-object v1, p0, LX/19r;->c:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    invoke-virtual {v1, v0}, LX/16V;->a(LX/1AD;)V

    goto :goto_0

    .line 208798
    :pswitch_1
    check-cast p2, Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;

    .line 208799
    new-instance v0, LX/2oT;

    invoke-direct {v0, p2}, LX/2oT;-><init>(Lcom/facebook/exoplayer/ipc/VpsManifestFetchEndEvent;)V

    .line 208800
    iget-object v1, p0, LX/19r;->c:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    invoke-virtual {v1, v0}, LX/16V;->a(LX/1AD;)V

    goto :goto_0

    .line 208801
    :pswitch_2
    check-cast p2, Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;

    .line 208802
    new-instance v0, LX/2WE;

    invoke-direct {v0, p2}, LX/2WE;-><init>(Lcom/facebook/exoplayer/ipc/VpsManifestMisalignedEvent;)V

    .line 208803
    iget-object v1, p0, LX/19r;->c:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    invoke-virtual {v1, v0}, LX/16V;->a(LX/1AD;)V

    goto :goto_0

    .line 208804
    :pswitch_3
    check-cast p2, Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;

    .line 208805
    new-instance v0, LX/2WD;

    invoke-direct {v0, p2}, LX/2WD;-><init>(Lcom/facebook/exoplayer/ipc/VpsHttpTransferEndEvent;)V

    .line 208806
    iget-object v1, p0, LX/19r;->c:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    invoke-virtual {v1, v0}, LX/16V;->a(LX/1AD;)V

    goto :goto_0

    .line 208807
    :pswitch_4
    check-cast p2, Lcom/facebook/exoplayer/ipc/VpsPrefetchCacheEvictEvent;

    .line 208808
    new-instance v0, LX/7KU;

    iget-object v1, p2, Lcom/facebook/exoplayer/ipc/VpsPrefetchCacheEvictEvent;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/7KU;-><init>(Ljava/lang/String;)V

    .line 208809
    iget-object v1, p0, LX/19r;->c:LX/19l;

    iget-object v1, v1, LX/19l;->a:LX/16V;

    invoke-virtual {v1, v0}, LX/16V;->a(LX/1AD;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(ILcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;IJJ)V
    .locals 0

    .prologue
    .line 208790
    return-void
.end method

.method public final declared-synchronized a(LX/0A6;)V
    .locals 1

    .prologue
    .line 208811
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19r;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208812
    monitor-exit p0

    return-void

    .line 208813
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/MediaRenderer;Lcom/facebook/exoplayer/ipc/RendererContext;)V
    .locals 0

    .prologue
    .line 208789
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/VideoPlayerSession;ZII)V
    .locals 0

    .prologue
    .line 208788
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208786
    iget-object v0, p0, LX/19r;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "VideoPlayerService"

    const-string v2, "Ipc failed at %s: %s.\n Call stack: %s"

    invoke-static {v2, p1, p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208787
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 208782
    return-void
.end method

.method public final a(Ljava/util/List;J[Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerMediaChunk;",
            ">;J[",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamFormat;",
            "Lcom/facebook/exoplayer/ipc/VideoPlayerStreamEvaluation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 208785
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 208784
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 208783
    return-void
.end method
