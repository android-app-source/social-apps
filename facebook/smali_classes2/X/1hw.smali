.class public LX/1hw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hr;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1oy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/IsZeroRatingAvailable;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/UseSessionlessBackupRewriteRules;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1oy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297360
    iput-object p1, p0, LX/1hw;->a:LX/0Or;

    .line 297361
    iput-object p2, p0, LX/1hw;->b:LX/0Or;

    .line 297362
    iput-object p3, p0, LX/1hw;->c:LX/0Ot;

    .line 297363
    return-void
.end method

.method private b(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 297357
    iget-object v0, p0, LX/1hw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oy;

    invoke-virtual {p1}, Lorg/apache/http/impl/client/RequestWrapper;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1oy;->a(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V

    .line 297358
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 297356
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 2

    .prologue
    .line 297353
    iget-object v0, p0, LX/1hw;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/1hw;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297354
    :cond_0
    invoke-direct {p0, p1}, LX/1hw;->b(Lorg/apache/http/impl/client/RequestWrapper;)V

    .line 297355
    :cond_1
    return-void
.end method
