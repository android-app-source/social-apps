.class public LX/1mp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1mr;

.field private b:Z


# direct methods
.method public constructor <init>(LX/1mr;)V
    .locals 0

    .prologue
    .line 314188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314189
    iput-object p1, p0, LX/1mp;->a:LX/1mr;

    .line 314190
    return-void
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Canvas;
    .locals 3

    .prologue
    .line 314191
    iget-boolean v0, p0, LX/1mp;->b:Z

    if-eqz v0, :cond_0

    .line 314192
    new-instance v0, LX/32E;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t start a DisplayList that is already started"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/32E;-><init>(Ljava/lang/Exception;)V

    throw v0

    .line 314193
    :cond_0
    iget-object v0, p0, LX/1mp;->a:LX/1mr;

    invoke-interface {v0, p1, p2}, LX/1mr;->a(II)Landroid/graphics/Canvas;

    move-result-object v0

    .line 314194
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/1mp;->b:Z

    .line 314195
    return-object v0
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 314196
    iget-object v0, p0, LX/1mp;->a:LX/1mr;

    invoke-interface {v0, p1, p2, p3, p4}, LX/1mr;->a(IIII)V

    .line 314197
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 314198
    iget-boolean v0, p0, LX/1mp;->b:Z

    if-nez v0, :cond_0

    .line 314199
    new-instance v0, LX/32E;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Can\'t end a DisplayList that is not started"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/32E;-><init>(Ljava/lang/Exception;)V

    throw v0

    .line 314200
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1mp;->b:Z

    .line 314201
    iget-object v0, p0, LX/1mp;->a:LX/1mr;

    invoke-interface {v0, p1}, LX/1mr;->a(Landroid/graphics/Canvas;)V

    .line 314202
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 314203
    iget-object v0, p0, LX/1mp;->a:LX/1mr;

    invoke-interface {v0}, LX/1mr;->a()Z

    move-result v0

    return v0
.end method
