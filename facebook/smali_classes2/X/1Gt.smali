.class public final LX/1Gt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Gu;


# instance fields
.field public final synthetic a:LX/0W3;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0

    .prologue
    .line 226142
    iput-object p1, p0, LX/1Gt;->a:LX/0W3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226143
    iget-object v0, p0, LX/1Gt;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 226144
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, LX/1Gt;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1Gt;->b:Ljava/util/List;

    .line 226145
    :cond_0
    iget-object v0, p0, LX/1Gt;->b:Ljava/util/List;

    return-object v0
.end method

.method public final declared-synchronized b()I
    .locals 4

    .prologue
    .line 226146
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1Gt;->d:Z

    if-nez v0, :cond_0

    .line 226147
    iget-object v0, p0, LX/1Gt;->a:LX/0W3;

    sget-wide v2, LX/0X5;->fm:J

    const/4 v1, 0x5

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1Gt;->c:I

    .line 226148
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Gt;->d:Z

    .line 226149
    :cond_0
    iget v0, p0, LX/1Gt;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 226150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
