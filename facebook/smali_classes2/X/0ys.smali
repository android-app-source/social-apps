.class public LX/0ys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yn;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0ys;


# instance fields
.field private a:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166271
    return-void
.end method

.method public static a(LX/0QB;)LX/0ys;
    .locals 3

    .prologue
    .line 166258
    sget-object v0, LX/0ys;->c:LX/0ys;

    if-nez v0, :cond_1

    .line 166259
    const-class v1, LX/0ys;

    monitor-enter v1

    .line 166260
    :try_start_0
    sget-object v0, LX/0ys;->c:LX/0ys;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 166261
    if-eqz v2, :cond_0

    .line 166262
    :try_start_1
    new-instance v0, LX/0ys;

    invoke-direct {v0}, LX/0ys;-><init>()V

    .line 166263
    move-object v0, v0

    .line 166264
    sput-object v0, LX/0ys;->c:LX/0ys;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166265
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 166266
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166267
    :cond_1
    sget-object v0, LX/0ys;->c:LX/0ys;

    return-object v0

    .line 166268
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 166269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 166257
    return-void
.end method

.method public final declared-synchronized a(LX/0y8;)V
    .locals 1

    .prologue
    .line 166272
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0ys;->a:Z

    invoke-virtual {p1, v0}, LX/0y8;->c(Z)V

    .line 166273
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0ys;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166274
    monitor-exit p0

    return-void

    .line 166275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0yl;)V
    .locals 0

    .prologue
    .line 166256
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 166251
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0ys;->a:Z

    if-eq p1, v0, :cond_0

    .line 166252
    iput-boolean p1, p0, LX/0ys;->a:Z

    .line 166253
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0ys;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166254
    :cond_0
    monitor-exit p0

    return-void

    .line 166255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 166250
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0ys;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
