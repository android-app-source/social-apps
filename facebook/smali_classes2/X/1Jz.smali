.class public LX/1Jz;
.super LX/1K0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1K0",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        "LX/9Ae",
        "<",
        "Lcom/facebook/graphql/model/FeedEdge;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:LX/0bH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 230782
    invoke-direct {p0}, LX/1K0;-><init>()V

    .line 230783
    return-void
.end method

.method public static a(LX/0QB;)LX/1Jz;
    .locals 2

    .prologue
    .line 230784
    new-instance v1, LX/1Jz;

    invoke-direct {v1}, LX/1Jz;-><init>()V

    .line 230785
    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    .line 230786
    iput-object v0, v1, LX/1Jz;->a:LX/0bH;

    .line 230787
    move-object v0, v1

    .line 230788
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;ILX/9Ad;)V
    .locals 7

    .prologue
    .line 230789
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    const/4 v2, 0x0

    .line 230790
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 230791
    instance-of v0, v1, Lcom/facebook/graphql/model/HideableUnit;

    if-nez v0, :cond_1

    .line 230792
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, v1

    .line 230793
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 230794
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v3, :cond_0

    .line 230795
    iget-object v6, p0, LX/1Jz;->a:LX/0bH;

    new-instance v0, LX/1Nd;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x1

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 230796
    iget-object v0, p0, LX/1Jz;->a:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
