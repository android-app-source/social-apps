.class public final LX/10L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# instance fields
.field public final synthetic a:LX/0gg;


# direct methods
.method public constructor <init>(LX/0gg;)V
    .locals 0

    .prologue
    .line 168726
    iput-object p1, p0, LX/10L;->a:LX/0gg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 5

    .prologue
    .line 168727
    sget-object v0, LX/32G;->OnAppBackgrounded:LX/32G;

    if-ne p1, v0, :cond_0

    .line 168728
    :goto_0
    return-void

    .line 168729
    :cond_0
    new-instance v1, Lcom/facebook/ui/mainview/ViewPagerController$5$1;

    invoke-direct {v1, p0}, Lcom/facebook/ui/mainview/ViewPagerController$5$1;-><init>(LX/10L;)V

    .line 168730
    iget-object v0, p0, LX/10L;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sg;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Killing inactive tabs due to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/32G;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v0, v2, v1, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    goto :goto_0
.end method
