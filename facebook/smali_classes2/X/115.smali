.class public LX/115;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 169788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169789
    const/4 v0, 0x0

    iput-object v0, p0, LX/115;->a:Landroid/content/ContentResolver;

    .line 169790
    const-string v0, ""

    iput-object v0, p0, LX/115;->b:Ljava/lang/String;

    .line 169791
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, LX/115;->c:Landroid/content/IntentFilter;

    .line 169792
    return-void
.end method

.method private constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/content/IntentFilter;)V
    .locals 2

    .prologue
    .line 169793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169794
    iput-object p1, p0, LX/115;->a:Landroid/content/ContentResolver;

    .line 169795
    iput-object p2, p0, LX/115;->b:Ljava/lang/String;

    .line 169796
    iput-object p3, p0, LX/115;->c:Landroid/content/IntentFilter;

    .line 169797
    iget-object v0, p0, LX/115;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/115;->c:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 169798
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You need to specify at least one: Activity/IntentFilter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169799
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)LX/115;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 169800
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169801
    new-instance v0, LX/115;

    invoke-direct {v0}, LX/115;-><init>()V

    .line 169802
    :goto_0
    return-object v0

    .line 169803
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 169804
    sparse-switch v0, :sswitch_data_0

    .line 169805
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Criteria specification is not valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169806
    :sswitch_0
    const/4 v1, 0x0

    .line 169807
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 169808
    :goto_1
    invoke-static {v0}, LX/115;->a(Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v2

    .line 169809
    new-instance v0, LX/115;

    invoke-direct {v0, p0, v1, v2}, LX/115;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/content/IntentFilter;)V

    goto :goto_0

    .line 169810
    :sswitch_1
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 169811
    if-gez v0, :cond_1

    .line 169812
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Criteria specification is not valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169813
    :cond_1
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 169814
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_1
        0x2a -> :sswitch_0
        0x3a -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(Ljava/lang/String;)Landroid/content/IntentFilter;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 169815
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169816
    const/4 v0, 0x0

    .line 169817
    :goto_0
    return-object v0

    .line 169818
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 169819
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 169820
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 169821
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 169822
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 169823
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->readFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 169824
    :catch_0
    move-exception v0

    .line 169825
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Something went wrong with the parser"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 169826
    iget-object v0, p0, LX/115;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/115;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 169827
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 169828
    iget-object v2, p0, LX/115;->c:Landroid/content/IntentFilter;

    if-nez v2, :cond_4

    .line 169829
    :cond_1
    :goto_1
    move v0, v0

    .line 169830
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 169831
    :cond_4
    iget-object v2, p0, LX/115;->c:Landroid/content/IntentFilter;

    iget-object v3, p0, LX/115;->a:Landroid/content/ContentResolver;

    const-string p1, "TAG"

    invoke-virtual {v2, v3, p2, v1, p1}, Landroid/content/IntentFilter;->match(Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I

    move-result v2

    .line 169832
    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_1
.end method
