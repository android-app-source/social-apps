.class public LX/1fm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:J

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;IJLX/0am;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "IJ",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 292310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292311
    iput-object p1, p0, LX/1fm;->a:Landroid/net/Uri;

    .line 292312
    iput p2, p0, LX/1fm;->b:I

    .line 292313
    iput-wide p3, p0, LX/1fm;->c:J

    .line 292314
    iput-object p5, p0, LX/1fm;->d:LX/0am;

    .line 292315
    iput-boolean p6, p0, LX/1fm;->e:Z

    .line 292316
    iput-boolean p7, p0, LX/1fm;->f:Z

    .line 292317
    iput-object p8, p0, LX/1fm;->g:Ljava/lang/String;

    .line 292318
    iput-object p9, p0, LX/1fm;->h:Ljava/lang/String;

    .line 292319
    iput-object p10, p0, LX/1fm;->i:Ljava/lang/String;

    .line 292320
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 292321
    if-ne p0, p1, :cond_1

    .line 292322
    :cond_0
    :goto_0
    return v0

    .line 292323
    :cond_1
    instance-of v2, p1, LX/1fm;

    if-nez v2, :cond_2

    move v0, v1

    .line 292324
    goto :goto_0

    .line 292325
    :cond_2
    check-cast p1, LX/1fm;

    .line 292326
    iget-object v2, p0, LX/1fm;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/1fm;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LX/1fm;->b:I

    iget v3, p1, LX/1fm;->b:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, LX/1fm;->c:J

    iget-wide v4, p1, LX/1fm;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, LX/1fm;->d:LX/0am;

    iget-object v3, p1, LX/1fm;->d:LX/0am;

    invoke-virtual {v2, v3}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/1fm;->e:Z

    iget-boolean v3, p1, LX/1fm;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, LX/1fm;->f:Z

    iget-boolean v3, p1, LX/1fm;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/1fm;->g:Ljava/lang/String;

    iget-object v3, p1, LX/1fm;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1fm;->h:Ljava/lang/String;

    iget-object v3, p1, LX/1fm;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/1fm;->i:Ljava/lang/String;

    iget-object v3, p1, LX/1fm;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 292327
    iget-object v0, p0, LX/1fm;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method
