.class public final LX/12n;
.super LX/12o;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/12o",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:LX/0P1;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final transient b:[Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0P1;[Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<TK;TV;>;[",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 175743
    invoke-direct {p0}, LX/12o;-><init>()V

    .line 175744
    iput-object p1, p0, LX/12n;->a:LX/0P1;

    .line 175745
    iput-object p2, p0, LX/12n;->b:[Ljava/util/Map$Entry;

    .line 175746
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 175747
    iget-object v0, p0, LX/12n;->a:LX/0P1;

    return-object v0
.end method

.method public final createAsList()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 175748
    new-instance v0, LX/12p;

    iget-object v1, p0, LX/12n;->b:[Ljava/util/Map$Entry;

    invoke-direct {v0, p0, v1}, LX/12p;-><init>(LX/0Py;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 175749
    invoke-virtual {p0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 175750
    invoke-virtual {p0}, LX/12n;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method
