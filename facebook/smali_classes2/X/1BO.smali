.class public LX/1BO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BP;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/1BO;


# instance fields
.field private final a:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

.field private final b:Ljava/lang/Boolean;

.field private final c:Ljava/lang/Boolean;

.field public final d:LX/1BS;

.field private final e:Ljava/util/Random;

.field private final f:LX/1BW;

.field private final g:J

.field public final h:J


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;LX/1BQ;LX/1BS;Ljava/util/Random;LX/1BW;)V
    .locals 4
    .param p4    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213079
    iput-object p1, p0, LX/1BO;->a:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    .line 213080
    invoke-virtual {p2}, LX/1BQ;->a()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1BO;->b:Ljava/lang/Boolean;

    .line 213081
    invoke-virtual {p2}, LX/1BQ;->b()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1BO;->c:Ljava/lang/Boolean;

    .line 213082
    iput-object p3, p0, LX/1BO;->d:LX/1BS;

    .line 213083
    iput-object p4, p0, LX/1BO;->e:Ljava/util/Random;

    .line 213084
    iput-object p5, p0, LX/1BO;->f:LX/1BW;

    .line 213085
    iget-wide v2, p2, LX/1BQ;->b:J

    move-wide v0, v2

    .line 213086
    iput-wide v0, p0, LX/1BO;->g:J

    .line 213087
    iget-wide v2, p2, LX/1BQ;->c:J

    move-wide v0, v2

    .line 213088
    iput-wide v0, p0, LX/1BO;->h:J

    .line 213089
    return-void
.end method

.method public static a(LX/0QB;)LX/1BO;
    .locals 9

    .prologue
    .line 213065
    sget-object v0, LX/1BO;->i:LX/1BO;

    if-nez v0, :cond_1

    .line 213066
    const-class v1, LX/1BO;

    monitor-enter v1

    .line 213067
    :try_start_0
    sget-object v0, LX/1BO;->i:LX/1BO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213068
    if-eqz v2, :cond_0

    .line 213069
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213070
    new-instance v3, LX/1BO;

    invoke-static {v0}, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->b(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    invoke-static {v0}, LX/1BQ;->b(LX/0QB;)LX/1BQ;

    move-result-object v5

    check-cast v5, LX/1BQ;

    invoke-static {v0}, LX/1BR;->a(LX/0QB;)LX/1BS;

    move-result-object v6

    check-cast v6, LX/1BS;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v7

    check-cast v7, Ljava/util/Random;

    invoke-static {v0}, LX/1BW;->a(LX/0QB;)LX/1BW;

    move-result-object v8

    check-cast v8, LX/1BW;

    invoke-direct/range {v3 .. v8}, LX/1BO;-><init>(Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;LX/1BQ;LX/1BS;Ljava/util/Random;LX/1BW;)V

    .line 213071
    move-object v0, v3

    .line 213072
    sput-object v0, LX/1BO;->i:LX/1BO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213073
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213074
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213075
    :cond_1
    sget-object v0, LX/1BO;->i:LX/1BO;

    return-object v0

    .line 213076
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2xo;)V
    .locals 4

    .prologue
    .line 213061
    iget-object v0, p0, LX/1BO;->f:LX/1BW;

    invoke-virtual {v0, p1}, LX/1BW;->a(LX/2xo;)V

    .line 213062
    iget-object v0, p0, LX/1BO;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1BO;->e:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, LX/1BO;->g:J

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 213063
    iget-object v0, p0, LX/1BO;->a:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    invoke-virtual {v0, p1}, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->a(LX/2xo;)V

    .line 213064
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 213060
    iget-object v0, p0, LX/1BO;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1BO;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
