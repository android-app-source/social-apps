.class public LX/0bX;
.super LX/0bY;
.source ""

# interfaces
.implements LX/0bZ;


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87049
    invoke-direct {p0}, LX/0bY;-><init>()V

    .line 87050
    iput-object p1, p0, LX/0bX;->a:LX/0am;

    .line 87051
    iput-object p2, p0, LX/0bX;->b:LX/0am;

    .line 87052
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 87053
    const-string v2, "PulsarPresenceChanged\n\tfrom: %s\n\tto: %s"

    iget-object v0, p0, LX/0bX;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0bX;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, LX/0bX;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0bX;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "null"

    move-object v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "null"

    goto :goto_1
.end method

.method public final d()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87054
    iget-object v0, p0, LX/0bX;->b:LX/0am;

    return-object v0
.end method
