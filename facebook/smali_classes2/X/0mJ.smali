.class public LX/0mJ;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0mI;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0mI;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132359
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0mI;
    .locals 3

    .prologue
    .line 132360
    sget-object v0, LX/0mJ;->a:LX/0mI;

    if-nez v0, :cond_1

    .line 132361
    const-class v1, LX/0mJ;

    monitor-enter v1

    .line 132362
    :try_start_0
    sget-object v0, LX/0mJ;->a:LX/0mI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 132363
    if-eqz v2, :cond_0

    .line 132364
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 132365
    const/16 p0, 0x9b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/analytics/AnalyticsClientModule;->c(LX/0Ot;)LX/0mI;

    move-result-object p0

    move-object v0, p0

    .line 132366
    sput-object v0, LX/0mJ;->a:LX/0mI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132367
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 132368
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 132369
    :cond_1
    sget-object v0, LX/0mJ;->a:LX/0mI;

    return-object v0

    .line 132370
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 132371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132372
    const/16 v0, 0x9b

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/analytics/AnalyticsClientModule;->c(LX/0Ot;)LX/0mI;

    move-result-object v0

    return-object v0
.end method
