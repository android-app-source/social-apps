.class public LX/1A9;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1AA;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1AA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 210101
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1AA;
    .locals 3

    .prologue
    .line 210102
    sget-object v0, LX/1A9;->a:LX/1AA;

    if-nez v0, :cond_1

    .line 210103
    const-class v1, LX/1A9;

    monitor-enter v1

    .line 210104
    :try_start_0
    sget-object v0, LX/1A9;->a:LX/1AA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 210105
    if-eqz v2, :cond_0

    .line 210106
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 210107
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/19Y;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1AA;

    move-result-object p0

    move-object v0, p0

    .line 210108
    sput-object v0, LX/1A9;->a:LX/1AA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210109
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 210110
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 210111
    :cond_1
    sget-object v0, LX/1A9;->a:LX/1AA;

    return-object v0

    .line 210112
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 210113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 210114
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/19Y;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/1AA;

    move-result-object v0

    return-object v0
.end method
