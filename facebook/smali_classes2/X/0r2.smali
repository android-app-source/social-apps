.class public LX/0r2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g3;


# instance fields
.field private final a:LX/0r3;


# direct methods
.method public constructor <init>(LX/0r3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 148909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148910
    iput-object p1, p0, LX/0r2;->a:LX/0r3;

    .line 148911
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 148908
    return-void
.end method

.method public final a(ILX/1lq;ILX/0ta;)V
    .locals 0

    .prologue
    .line 148907
    return-void
.end method

.method public final a(LX/0gf;)V
    .locals 2

    .prologue
    .line 148903
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {p1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0r3;->b(Ljava/lang/String;)V

    .line 148904
    invoke-virtual {p1}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148905
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {v0}, LX/0r3;->a()V

    .line 148906
    :cond_0
    return-void
.end method

.method public final a(LX/0gf;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 148902
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 148901
    return-void
.end method

.method public final a(ZILX/0ta;)V
    .locals 0

    .prologue
    .line 148870
    return-void
.end method

.method public final a(ZLcom/facebook/api/feed/FetchFeedResult;LX/18G;Ljava/lang/String;ILX/0qw;LX/18O;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 148875
    if-nez p2, :cond_0

    .line 148876
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    const-string v1, "fetch_null_result"

    invoke-virtual {v0, v1}, LX/0r3;->a(Ljava/lang/String;)V

    .line 148877
    :goto_0
    return-void

    .line 148878
    :cond_0
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v1, v0

    .line 148879
    if-eqz v1, :cond_1

    .line 148880
    iget-object v0, v1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 148881
    if-nez v0, :cond_2

    .line 148882
    :cond_1
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    const-string v1, "fetch_null_feed_params"

    invoke-virtual {v0, v1}, LX/0r3;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 148883
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 148884
    :goto_1
    iget-object v3, v1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v3, v3

    .line 148885
    iget-object v4, v1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v4, v4

    .line 148886
    if-eqz v4, :cond_6

    .line 148887
    iget-object v4, v1, Lcom/facebook/api/feed/FetchFeedParams;->a:LX/0rS;

    move-object v1, v4

    .line 148888
    invoke-virtual {v1}, LX/0rS;->name()Ljava/lang/String;

    move-result-object v1

    .line 148889
    :goto_2
    iget-object v4, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 148890
    if-eqz v4, :cond_3

    .line 148891
    iget-object v2, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 148892
    invoke-virtual {v2}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v2

    .line 148893
    :cond_3
    if-nez v0, :cond_4

    .line 148894
    iget-object v4, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {v4}, LX/0r3;->b()V

    .line 148895
    :cond_4
    iget-object v4, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {v3}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v1, v2, v0}, LX/0r3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 148896
    sget-object v0, LX/18P;->a:[I

    invoke-virtual {p3}, LX/18G;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 148897
    :pswitch_0
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {v0, p4}, LX/0r3;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 148898
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    move-object v1, v2

    .line 148899
    goto :goto_2

    .line 148900
    :pswitch_1
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {v0}, LX/0r3;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/0gf;)V
    .locals 2

    .prologue
    .line 148873
    iget-object v0, p0, LX/0r2;->a:LX/0r3;

    invoke-virtual {p1}, LX/0gf;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0r3;->b(Ljava/lang/String;)V

    .line 148874
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 148872
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 148871
    return-void
.end method
