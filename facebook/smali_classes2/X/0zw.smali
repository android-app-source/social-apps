.class public LX/0zw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/ViewStub;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/0zy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zy",
            "<TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;)V
    .locals 1
    .param p1    # Landroid/view/ViewStub;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 168096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168097
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/0zw;->a:Landroid/view/ViewStub;

    .line 168098
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewStub;LX/0zy;)V
    .locals 1
    .param p1    # Landroid/view/ViewStub;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # LX/0zy;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewStub;",
            "LX/0zy",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 168099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168100
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/0zw;->a:Landroid/view/ViewStub;

    .line 168101
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zy;

    iput-object v0, p0, LX/0zw;->b:LX/0zy;

    .line 168102
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 168103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0zw;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 168104
    iget-object v0, p0, LX/0zw;->a:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 168105
    iget-object v1, p0, LX/0zw;->b:LX/0zy;

    if-eqz v1, :cond_0

    .line 168106
    iget-object v1, p0, LX/0zw;->b:LX/0zy;

    invoke-interface {v1, v0}, LX/0zy;->a(Landroid/view/View;)V

    .line 168107
    :cond_0
    iput-object v0, p0, LX/0zw;->c:Landroid/view/View;

    .line 168108
    const/4 v0, 0x0

    iput-object v0, p0, LX/0zw;->b:LX/0zy;

    .line 168109
    const/4 v0, 0x0

    iput-object v0, p0, LX/0zw;->a:Landroid/view/ViewStub;

    .line 168110
    :cond_1
    iget-object v0, p0, LX/0zw;->c:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 168111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 168112
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0zw;->c:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 168113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0zw;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 168114
    iget-object v0, p0, LX/0zw;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168115
    :cond_0
    monitor-exit p0

    return-void

    .line 168116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
