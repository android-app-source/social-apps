.class public final LX/0Wf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 76612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static memoize(LX/0QR;)LX/0QR;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QR",
            "<TT;>;)",
            "LX/0QR",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76613
    instance-of v0, p0, LX/0Wg;

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v1, LX/0Wg;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QR;

    invoke-direct {v1, v0}, LX/0Wg;-><init>(LX/0QR;)V

    move-object p0, v1

    goto :goto_0
.end method
