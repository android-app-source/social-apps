.class public LX/0eo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0en;

.field public final c:LX/0em;

.field public final d:LX/0WV;

.field public final e:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 104125
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "i18n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/0eo;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0eo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0WV;LX/0en;LX/0em;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 104126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104127
    iput-object p1, p0, LX/0eo;->d:LX/0WV;

    .line 104128
    iput-object p2, p0, LX/0eo;->b:LX/0en;

    .line 104129
    iput-object p3, p0, LX/0eo;->c:LX/0em;

    .line 104130
    iput-object p4, p0, LX/0eo;->e:Landroid/content/Context;

    .line 104131
    return-void
.end method
