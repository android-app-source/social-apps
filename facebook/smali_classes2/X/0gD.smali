.class public abstract LX/0gD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gC;
.implements LX/02k;


# static fields
.field public static final l:I

.field private static final s:Ljava/lang/String;


# instance fields
.field public a:LX/0oy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0fz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0qy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0pV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0qz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0r0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18Q;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/feed/feedtype/FeedTypeDataItem;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Yn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0r2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0r3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0qk;

.field public n:Lcom/facebook/api/feedtype/FeedType;

.field public o:LX/0rB;

.field private final p:Landroid/content/Context;

.field public q:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Z

.field private u:Z

.field private v:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 111615
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0gD;->s:Ljava/lang/String;

    .line 111616
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v2, 0x83d60

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LX/0gD;->l:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111618
    new-instance v0, LX/0qk;

    invoke-direct {v0}, LX/0qk;-><init>()V

    iput-object v0, p0, LX/0gD;->m:LX/0qk;

    .line 111619
    iput-boolean v1, p0, LX/0gD;->t:Z

    .line 111620
    iput-boolean v1, p0, LX/0gD;->u:Z

    .line 111621
    const/4 v0, 0x0

    iput-object v0, p0, LX/0gD;->v:Ljava/lang/String;

    .line 111622
    iput-object p1, p0, LX/0gD;->p:Landroid/content/Context;

    .line 111623
    const-class v0, LX/0gD;

    invoke-static {v0, p0}, LX/0gD;->a(Ljava/lang/Class;LX/02k;)V

    .line 111624
    iget-object v0, p0, LX/0gD;->e:LX/0qz;

    invoke-virtual {p0}, LX/0gD;->F()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 111625
    iput-object v1, v0, LX/0qz;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 111626
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    iget-object v1, p0, LX/0gD;->j:LX/0r2;

    invoke-virtual {v0, v1}, LX/0qk;->a(LX/0g3;)Z

    .line 111627
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 111628
    iget-object v0, p0, LX/0gD;->v:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 111629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/0gD;->F()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 111630
    iget-object v2, v1, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v1, v2

    .line 111631
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0gD;->v:Ljava/lang/String;

    .line 111632
    :cond_0
    iget-object v0, p0, LX/0gD;->v:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, LX/0gD;

    invoke-static {p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v2

    check-cast v2, LX/0oy;

    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v3

    check-cast v3, LX/0fz;

    new-instance v5, LX/0qy;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v4}, LX/0qy;-><init>(LX/0SG;)V

    move-object v4, v5

    check-cast v4, LX/0qy;

    invoke-static {p0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v5

    check-cast v5, LX/0pV;

    invoke-static {p0}, LX/0qz;->b(LX/0QB;)LX/0qz;

    move-result-object v6

    check-cast v6, LX/0qz;

    new-instance v7, LX/0r0;

    invoke-direct {v7}, LX/0r0;-><init>()V

    move-object v7, v7

    move-object v7, v7

    check-cast v7, LX/0r0;

    const/16 v8, 0xcbb

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0oa;->a(LX/0QB;)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v11, 0x2d2

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/0Yn;->a(LX/0QB;)LX/0Yn;

    move-result-object v12

    check-cast v12, LX/0Yn;

    new-instance p1, LX/0r2;

    invoke-static {p0}, LX/0r3;->a(LX/0QB;)LX/0r3;

    move-result-object v13

    check-cast v13, LX/0r3;

    invoke-direct {p1, v13}, LX/0r2;-><init>(LX/0r3;)V

    move-object v13, p1

    check-cast v13, LX/0r2;

    invoke-static {p0}, LX/0r3;->a(LX/0QB;)LX/0r3;

    move-result-object p0

    check-cast p0, LX/0r3;

    iput-object v2, v1, LX/0gD;->a:LX/0oy;

    iput-object v3, v1, LX/0gD;->b:LX/0fz;

    iput-object v4, v1, LX/0gD;->c:LX/0qy;

    iput-object v5, v1, LX/0gD;->d:LX/0pV;

    iput-object v6, v1, LX/0gD;->e:LX/0qz;

    iput-object v7, v1, LX/0gD;->f:LX/0r0;

    iput-object v8, v1, LX/0gD;->g:LX/0Ot;

    iput-object v9, v1, LX/0gD;->h:LX/0Ot;

    iput-object v10, v1, LX/0gD;->q:LX/0Uh;

    iput-object v11, v1, LX/0gD;->r:LX/0Ot;

    iput-object v12, v1, LX/0gD;->i:LX/0Yn;

    iput-object v13, v1, LX/0gD;->j:LX/0r2;

    iput-object p0, v1, LX/0gD;->k:LX/0r3;

    return-void
.end method


# virtual methods
.method public D()V
    .locals 0

    .prologue
    .line 111633
    return-void
.end method

.method public final E()J
    .locals 4

    .prologue
    .line 111634
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/0gD;->a:LX/0oy;

    invoke-virtual {v1}, LX/0oy;->i()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public abstract F()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public abstract G()V
.end method

.method public final H()V
    .locals 7

    .prologue
    .line 111635
    iget-boolean v0, p0, LX/0gD;->t:Z

    if-nez v0, :cond_1

    .line 111636
    iget-object v0, p0, LX/0gD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18Q;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {p0}, LX/0gD;->F()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 111637
    iget-object v3, v0, LX/18Q;->h:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 111638
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0gD;->t:Z

    .line 111639
    :cond_1
    return-void

    .line 111640
    :cond_2
    iget-object v3, v0, LX/18Q;->f:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/18R;

    .line 111641
    if-eqz v3, :cond_3

    iget-object v4, v0, LX/18Q;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v3, v3, LX/18R;->b:J

    cmp-long v3, v5, v3

    if-ltz v3, :cond_0

    .line 111642
    :cond_3
    iget-object v3, v0, LX/18Q;->h:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 111643
    iget-object v3, v0, LX/18Q;->d:LX/18S;

    .line 111644
    iget-object v4, v3, LX/18S;->c:LX/0Wd;

    new-instance v5, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;

    invoke-direct {v5, v3, v1, v2}, Lcom/facebook/megaphone/fetcher/MegaphoneFetcher$1;-><init>(LX/18S;Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v3, v4

    .line 111645
    new-instance v4, LX/18g;

    invoke-direct {v4, v0, v1}, LX/18g;-><init>(LX/18Q;Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public final I()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111604
    iget-object v1, p0, LX/0gD;->q:LX/0Uh;

    const/16 v2, 0x414

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111605
    :cond_0
    :goto_0
    return v0

    .line 111606
    :cond_1
    iget-object v1, p0, LX/0gD;->o:LX/0rB;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0gD;->o:LX/0rB;

    .line 111607
    iget-boolean v2, v1, LX/0rB;->d:Z

    move v1, v2

    .line 111608
    if-eqz v1, :cond_0

    .line 111609
    iget-object v1, p0, LX/0gD;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0gD;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 111610
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->d:Z

    move v1, v1

    .line 111611
    if-nez v1, :cond_0

    .line 111612
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final J()Lcom/facebook/common/perftest/PerfTestConfig;
    .locals 1

    .prologue
    .line 111646
    iget-object v0, p0, LX/0gD;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/perftest/PerfTestConfig;

    return-object v0
.end method

.method public final K()LX/0Uh;
    .locals 1

    .prologue
    .line 111647
    iget-object v0, p0, LX/0gD;->q:LX/0Uh;

    return-object v0
.end method

.method public final L()Z
    .locals 1

    .prologue
    .line 111648
    invoke-virtual {p0}, LX/0gD;->B()LX/1EM;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0gD;->B()LX/1EM;

    move-result-object v0

    invoke-virtual {v0}, LX/1EM;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0gD;->B()LX/1EM;

    move-result-object v0

    invoke-virtual {v0}, LX/1EM;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final M()J
    .locals 2

    .prologue
    .line 111649
    invoke-virtual {p0}, LX/0gD;->B()LX/1EM;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, LX/0gD;->B()LX/1EM;

    move-result-object v0

    invoke-virtual {v0}, LX/1EM;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(LX/0g3;)V
    .locals 1

    .prologue
    .line 111613
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0, p1}, LX/0qk;->a(LX/0g3;)Z

    .line 111614
    return-void
.end method

.method public final a(LX/0rj;)V
    .locals 2

    .prologue
    .line 111650
    iget-object v0, p0, LX/0gD;->d:LX/0pV;

    invoke-direct {p0}, LX/0gD;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0pV;->a(Ljava/lang/String;LX/0rj;)V

    .line 111651
    return-void
.end method

.method public final a(LX/0rj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 111560
    iget-object v0, p0, LX/0gD;->d:LX/0pV;

    invoke-direct {p0}, LX/0gD;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 111561
    return-void
.end method

.method public a(Lcom/facebook/api/feedtype/FeedType;)V
    .locals 4

    .prologue
    .line 111562
    iget-object v0, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 111563
    iput-object p1, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    .line 111564
    iget-object v0, p0, LX/0gD;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rB;

    .line 111565
    iget-object v2, v0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v2

    .line 111566
    invoke-virtual {p0}, LX/0gD;->s()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    .line 111567
    iget-object p1, v3, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v3, p1

    .line 111568
    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111569
    iput-object v0, p0, LX/0gD;->o:LX/0rB;

    .line 111570
    :cond_1
    return-void

    .line 111571
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 111572
    iput-boolean p1, p0, LX/0gD;->u:Z

    .line 111573
    return-void
.end method

.method public b(LX/0g3;)V
    .locals 1

    .prologue
    .line 111574
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    .line 111575
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111576
    iget-object p0, v0, LX/0qk;->a:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 111577
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111578
    sget-object v0, LX/0gD;->s:Ljava/lang/String;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 111579
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    .line 111580
    iget-object v1, v0, LX/0fz;->d:LX/0qm;

    move-object v0, v1

    .line 111581
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 111582
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    .line 111583
    iget-object v1, v0, LX/0fz;->d:LX/0qm;

    move-object v0, v1

    .line 111584
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 111585
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 111586
    iget-object v0, p0, LX/0gD;->p:Landroid/content/Context;

    return-object v0
.end method

.method public final h()LX/0fz;
    .locals 1

    .prologue
    .line 111587
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    return-object v0
.end method

.method public final hU_()V
    .locals 1

    .prologue
    .line 111588
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111589
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    .line 111590
    const/4 p0, 0x0

    iput-object p0, v0, LX/0fz;->p:LX/22x;

    .line 111591
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 111592
    invoke-virtual {p0}, LX/0gD;->G()V

    .line 111593
    iget-object v0, p0, LX/0gD;->i:LX/0Yn;

    invoke-virtual {v0}, LX/0Yn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111594
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 111595
    :cond_0
    return-void
.end method

.method public final s()Lcom/facebook/api/feedtype/FeedType;
    .locals 1

    .prologue
    .line 111596
    iget-object v0, p0, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    return-object v0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 111597
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0qk;->a(Z)V

    .line 111598
    iget-object v0, p0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->m()V

    .line 111599
    iget-object v0, p0, LX/0gD;->m:LX/0qk;

    invoke-virtual {v0}, LX/0qk;->d()V

    .line 111600
    iget-object v0, p0, LX/0gD;->c:LX/0qy;

    invoke-virtual {v0}, LX/0qy;->d()V

    .line 111601
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 111602
    iget-object v0, p0, LX/0gD;->f:LX/0r0;

    invoke-virtual {v0}, LX/0r0;->a()Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 111603
    iget-boolean v0, p0, LX/0gD;->u:Z

    return v0
.end method
