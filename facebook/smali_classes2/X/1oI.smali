.class public LX/1oI;
.super LX/1ah;
.source ""

# interfaces
.implements LX/1d1;
.implements LX/1cu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "LX/1aY;",
        ">",
        "LX/1ah;",
        "LX/1d1;",
        "LX/1cu;"
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final c:LX/1oK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1oK",
            "<TDH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1aY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TDH;)V"
        }
    .end annotation

    .prologue
    .line 318941
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 318942
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    iput-object v0, p0, LX/1oI;->a:Landroid/graphics/drawable/Drawable;

    .line 318943
    iget-object v0, p0, LX/1oI;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 318944
    new-instance v0, LX/1oK;

    invoke-direct {v0, p2}, LX/1oK;-><init>(LX/1aY;)V

    .line 318945
    move-object v0, v0

    .line 318946
    iput-object v0, p0, LX/1oI;->c:LX/1oK;

    .line 318947
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    const/4 v1, 0x1

    .line 318948
    iput-boolean v1, v0, LX/1oK;->c:Z

    .line 318949
    return-void
.end method


# virtual methods
.method public final a(LX/1aZ;)V
    .locals 1

    .prologue
    .line 318939
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 318940
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 318938
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 318937
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0, p1}, LX/1aX;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 318950
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1ah;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 318951
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 318952
    return-void
.end method

.method public final bH_()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318936
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 318932
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1oI;->a(LX/1aZ;)V

    .line 318933
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 318934
    iget-object v0, p0, LX/1oI;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, LX/1ah;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 318935
    return-void
.end method

.method public final d()LX/1aY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDH;"
        }
    .end annotation

    .prologue
    .line 318917
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    return-object v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 318929
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    invoke-virtual {v0}, LX/1aX;->c()V

    .line 318930
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 318931
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 318918
    iget-object v0, p0, LX/1oI;->c:LX/1oK;

    const-wide/16 v2, 0x50

    .line 318919
    iget-boolean v1, v0, LX/1oK;->d:Z

    if-eqz v1, :cond_0

    .line 318920
    :goto_0
    return-void

    .line 318921
    :cond_0
    iget-object v1, v0, LX/1aX;->g:LX/1ab;

    move-object v1, v1

    .line 318922
    sget-object v4, LX/1at;->ON_SCHEDULE_CLEAR_CONTROLLER:LX/1at;

    invoke-virtual {v1, v4}, LX/1ab;->a(LX/1at;)V

    .line 318923
    sget-object v1, LX/1oK;->a:Landroid/os/Handler;

    .line 318924
    iget-object v4, v0, LX/1oK;->b:Ljava/lang/Runnable;

    if-nez v4, :cond_1

    .line 318925
    new-instance v4, Lcom/facebook/drawee/view/FbDraweeHolder$1;

    invoke-direct {v4, v0}, Lcom/facebook/drawee/view/FbDraweeHolder$1;-><init>(LX/1oK;)V

    iput-object v4, v0, LX/1oK;->b:Ljava/lang/Runnable;

    .line 318926
    :cond_1
    iget-object v4, v0, LX/1oK;->b:Ljava/lang/Runnable;

    move-object v4, v4

    .line 318927
    const p0, 0x6e6d81b9

    invoke-static {v1, v4, v2, v3, p0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 318928
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1oK;->d:Z

    goto :goto_0
.end method
