.class public LX/11v;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/11w;

.field public final d:LX/0yH;

.field public final e:LX/11x;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field private final h:LX/0Xl;

.field public final i:LX/0Uh;

.field private final j:LX/0Yb;

.field public final k:LX/0Zb;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/lang/String;

.field public n:Landroid/view/ViewStub;

.field public o:LX/12Q;

.field public p:LX/12S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/0yY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173537
    const-class v0, LX/11v;

    sput-object v0, LX/11v;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/11w;LX/0yH;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Xl;LX/0Zb;LX/0Or;)V
    .locals 3
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/11w;",
            "LX/0yH;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Xl;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 173523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173524
    iput-object p1, p0, LX/11v;->b:Landroid/content/Context;

    .line 173525
    iput-object p2, p0, LX/11v;->c:LX/11w;

    .line 173526
    iput-object p6, p0, LX/11v;->i:LX/0Uh;

    .line 173527
    iput-object p3, p0, LX/11v;->d:LX/0yH;

    .line 173528
    iput-object p4, p0, LX/11v;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 173529
    iput-object p5, p0, LX/11v;->g:Lcom/facebook/content/SecureContextHelper;

    .line 173530
    iput-object p7, p0, LX/11v;->h:LX/0Xl;

    .line 173531
    iput-object p8, p0, LX/11v;->k:LX/0Zb;

    .line 173532
    iput-object p9, p0, LX/11v;->l:LX/0Or;

    .line 173533
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/11v;->m:Ljava/lang/String;

    .line 173534
    new-instance v0, LX/11x;

    invoke-direct {v0, p0}, LX/11x;-><init>(LX/11v;)V

    iput-object v0, p0, LX/11v;->e:LX/11x;

    .line 173535
    iget-object v0, p0, LX/11v;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.zero.ZERO_RATING_INDICATOR_DATA_CHANGED"

    new-instance v2, LX/11y;

    invoke-direct {v2, p0}, LX/11y;-><init>(LX/11v;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.zero.ZERO_RATING_STATE_CHANGED"

    new-instance v2, LX/11z;

    invoke-direct {v2, p0}, LX/11z;-><init>(LX/11v;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/11v;->j:LX/0Yb;

    .line 173536
    return-void
.end method

.method public static a(LX/0QB;)LX/11v;
    .locals 1

    .prologue
    .line 173483
    invoke-static {p0}, LX/11v;->b(LX/0QB;)LX/11v;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/11v;
    .locals 10

    .prologue
    .line 173521
    new-instance v0, LX/11v;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/11w;->a(LX/0QB;)LX/11w;

    move-result-object v2

    check-cast v2, LX/11w;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v3

    check-cast v3, LX/0yH;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    const/16 v9, 0x14d1

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/11v;-><init>(Landroid/content/Context;LX/11w;LX/0yH;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Xl;LX/0Zb;LX/0Or;)V

    .line 173522
    return-object v0
.end method

.method public static f(LX/11v;)V
    .locals 1

    .prologue
    .line 173516
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/11v;->o:LX/12Q;

    invoke-interface {v0}, LX/12Q;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173517
    :cond_0
    :goto_0
    return-void

    .line 173518
    :cond_1
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    invoke-interface {v0}, LX/12Q;->b()V

    .line 173519
    iget-object v0, p0, LX/11v;->p:LX/12S;

    if-eqz v0, :cond_0

    .line 173520
    iget-object v0, p0, LX/11v;->p:LX/12S;

    invoke-interface {v0}, LX/12S;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 173513
    iget-object v0, p0, LX/11v;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 173514
    invoke-virtual {p0}, LX/11v;->c()V

    .line 173515
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 173511
    iget-object v0, p0, LX/11v;->j:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 173512
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 173484
    iget-object v0, p0, LX/11v;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173485
    iget-object v0, p0, LX/11v;->d:LX/0yH;

    iget-object v1, p0, LX/11v;->q:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 173486
    invoke-static {p0}, LX/11v;->f(LX/11v;)V

    .line 173487
    :cond_0
    :goto_0
    return-void

    .line 173488
    :cond_1
    invoke-static {p0}, LX/11v;->f(LX/11v;)V

    goto :goto_0

    .line 173489
    :cond_2
    iget-object v0, p0, LX/11v;->c:LX/11w;

    invoke-virtual {v0}, LX/11w;->c()Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    move-result-object v1

    .line 173490
    if-nez v1, :cond_3

    .line 173491
    invoke-static {p0}, LX/11v;->f(LX/11v;)V

    goto :goto_0

    .line 173492
    :cond_3
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    if-nez v0, :cond_4

    iget-object v0, p0, LX/11v;->n:Landroid/view/ViewStub;

    if-nez v0, :cond_4

    .line 173493
    sget-object v0, LX/11v;->a:Ljava/lang/Class;

    const-string v1, "We don\'t have a stub when we need to display banner"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 173494
    :cond_4
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    if-nez v0, :cond_5

    .line 173495
    iget-object v0, p0, LX/11v;->n:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/12Q;

    iput-object v0, p0, LX/11v;->o:LX/12Q;

    .line 173496
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    iget-object v2, p0, LX/11v;->e:LX/11x;

    invoke-interface {v0, v2}, LX/12Q;->setListener(LX/11x;)V

    .line 173497
    :goto_1
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    invoke-interface {v0, v1}, LX/12Q;->setIndicatorData(Lcom/facebook/zero/sdk/request/ZeroIndicatorData;)V

    .line 173498
    iget-object v0, p0, LX/11v;->k:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "view"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/11v;->m:Ljava/lang/String;

    .line 173499
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 173500
    move-object v1, v1

    .line 173501
    const-string v2, "zero_indicator"

    .line 173502
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 173503
    move-object v1, v1

    .line 173504
    const-string v2, "zero_indicator_show"

    .line 173505
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 173506
    move-object v1, v1

    .line 173507
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 173508
    iget-object v0, p0, LX/11v;->p:LX/12S;

    if-eqz v0, :cond_0

    .line 173509
    iget-object v0, p0, LX/11v;->p:LX/12S;

    invoke-interface {v0}, LX/12S;->a()V

    goto :goto_0

    .line 173510
    :cond_5
    iget-object v0, p0, LX/11v;->o:LX/12Q;

    invoke-interface {v0}, LX/12Q;->a()V

    goto :goto_1
.end method
