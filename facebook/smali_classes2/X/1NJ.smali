.class public LX/1NJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1EU;

.field private final c:LX/0fz;

.field private final d:LX/0Zb;

.field private final e:LX/0pV;

.field private final f:LX/1EM;

.field public g:LX/1Z9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/1UQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0g5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 237255
    const-class v0, LX/1NJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1NJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0fz;LX/1EM;LX/1EU;LX/0Zb;LX/0pV;)V
    .locals 1
    .param p1    # LX/0fz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1EM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 237256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237257
    const/4 v0, -0x1

    iput v0, p0, LX/1NJ;->k:I

    .line 237258
    iput-object p1, p0, LX/1NJ;->c:LX/0fz;

    .line 237259
    iput-object p2, p0, LX/1NJ;->f:LX/1EM;

    .line 237260
    iput-object p3, p0, LX/1NJ;->b:LX/1EU;

    .line 237261
    iput-object p4, p0, LX/1NJ;->d:LX/0Zb;

    .line 237262
    iput-object p5, p0, LX/1NJ;->e:LX/0pV;

    .line 237263
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1NJ;->j:Ljava/util/Set;

    .line 237264
    return-void
.end method

.method public static a(LX/1NJ;LX/0g8;)I
    .locals 3

    .prologue
    .line 237265
    iget-object v0, p0, LX/1NJ;->h:LX/1UQ;

    invoke-virtual {v0}, LX/1UQ;->b()I

    move-result v0

    invoke-interface {p1}, LX/0g8;->r()I

    move-result v1

    iget-object v2, p0, LX/1NJ;->h:LX/1UQ;

    invoke-virtual {v2}, LX/1UQ;->c()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(LX/1NJ;LX/0rj;)V
    .locals 4

    .prologue
    .line 237266
    iget-object v0, p0, LX/1NJ;->e:LX/0pV;

    sget-object v1, LX/1NJ;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ", Stories:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/1NJ;->j:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 237267
    return-void
.end method

.method public static a(LX/1NJ;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 237268
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "is_in_top_snapshot"

    invoke-virtual {p0}, LX/1NJ;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "num_stories_fetched_in_last_result"

    iget-object v2, p0, LX/1NJ;->j:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "tracking"

    iget-object v2, p0, LX/1NJ;->i:LX/0g5;

    iget-object v3, p0, LX/1NJ;->c:LX/0fz;

    iget-object v4, p0, LX/1NJ;->h:LX/1UQ;

    invoke-static {v2, v3, v4}, LX/Am9;->a(LX/0g5;LX/0fz;LX/1UQ;)LX/162;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "android_pull_to_refresh"

    .line 237269
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 237270
    move-object v0, v0

    .line 237271
    iget-object v1, p0, LX/1NJ;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 237272
    return-void
.end method


# virtual methods
.method public final a(LX/1lq;)V
    .locals 5

    .prologue
    .line 237273
    iget-object v0, p0, LX/1NJ;->b:LX/1EU;

    invoke-virtual {v0}, LX/1EU;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1NJ;->g:LX/1Z9;

    if-nez v0, :cond_1

    .line 237274
    :cond_0
    :goto_0
    return-void

    .line 237275
    :cond_1
    sget-object v0, LX/Am7;->a:[I

    invoke-virtual {p1}, LX/1lq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 237276
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal enum for handleNewStoriesBelowPill"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237277
    :pswitch_0
    iget-object v0, p0, LX/1NJ;->c:LX/0fz;

    const/4 v3, -0x1

    .line 237278
    iget-object v1, p0, LX/1NJ;->h:LX/1UQ;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1NJ;->i:LX/0g5;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1NJ;->h:LX/1UQ;

    if-nez v1, :cond_5

    :cond_2
    move v2, v3

    .line 237279
    :cond_3
    :goto_1
    move v0, v2

    .line 237280
    iput v0, p0, LX/1NJ;->k:I

    .line 237281
    iget v0, p0, LX/1NJ;->k:I

    if-lez v0, :cond_4

    .line 237282
    iget-object v0, p0, LX/1NJ;->g:LX/1Z9;

    sget-object v1, LX/1lq;->ShowNSBPFullyLoadedText:LX/1lq;

    invoke-virtual {v0, v1}, LX/1Z9;->a(LX/1lq;)V

    .line 237283
    const-string v0, "fresh_feed_new_stories_below_pill_fully_loaded_shown"

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 237284
    sget-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_FULLY_LOADED_TEXT_SHOWN:LX/0rj;

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 237285
    goto :goto_0

    .line 237286
    :cond_4
    const-string v0, "fresh_feed_new_stories_below_pill_failed_to_show"

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 237287
    sget-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_FAILED_TO_SHOW:LX/0rj;

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 237288
    sget-object v0, LX/1lq;->HideNSBP:LX/1lq;

    invoke-virtual {p0, v0}, LX/1NJ;->a(LX/1lq;)V

    goto :goto_0

    .line 237289
    :pswitch_1
    const-string v0, "fresh_feed_new_stories_below_pill_hidden"

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 237290
    sget-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN:LX/0rj;

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 237291
    iget-object v0, p0, LX/1NJ;->g:LX/1Z9;

    invoke-virtual {v0, p1}, LX/1Z9;->a(LX/1lq;)V

    .line 237292
    iget-object v0, p0, LX/1NJ;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 237293
    const/4 v0, -0x1

    iput v0, p0, LX/1NJ;->k:I

    .line 237294
    goto :goto_0

    .line 237295
    :pswitch_2
    const-string v0, "fresh_feed_new_stories_below_pill_hide_if_not_fully_loaded"

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 237296
    sget-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_HIDDEN_IF_NOT_FULLY_LOADED:LX/0rj;

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 237297
    iget-object v0, p0, LX/1NJ;->g:LX/1Z9;

    invoke-virtual {v0, p1}, LX/1Z9;->a(LX/1lq;)V

    goto :goto_0

    .line 237298
    :pswitch_3
    const-string v0, "fresh_feed_new_stories_below_pill_spinner_shown"

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;Ljava/lang/String;)V

    .line 237299
    sget-object v0, LX/0rj;->NEW_STORIES_BELOW_PILL_SPINNER_SHOWN:LX/0rj;

    invoke-static {p0, v0}, LX/1NJ;->a(LX/1NJ;LX/0rj;)V

    .line 237300
    iget-object v0, p0, LX/1NJ;->g:LX/1Z9;

    invoke-virtual {v0, p1}, LX/1Z9;->a(LX/1lq;)V

    goto/16 :goto_0

    .line 237301
    :cond_5
    iget-object v1, p0, LX/1NJ;->i:LX/0g5;

    invoke-static {p0, v1}, LX/1NJ;->a(LX/1NJ;LX/0g8;)I

    move-result v1

    .line 237302
    const/4 v2, 0x0

    iget-object v4, p0, LX/1NJ;->h:LX/1UQ;

    invoke-interface {v4, v1}, LX/1Qr;->h_(I)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v2, v1

    .line 237303
    :goto_2
    invoke-virtual {v0}, LX/0fz;->v()I

    move-result v1

    if-ge v2, v1, :cond_7

    .line 237304
    invoke-virtual {v0, v2}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 237305
    instance-of v4, v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-eqz v4, :cond_6

    .line 237306
    check-cast v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 237307
    iget-boolean v4, v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->B:Z

    move v4, v4

    .line 237308
    if-nez v4, :cond_8

    iget-object v4, p0, LX/1NJ;->j:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_3
    move v1, v4

    .line 237309
    if-nez v1, :cond_3

    .line 237310
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_7
    move v2, v3

    .line 237311
    goto/16 :goto_1

    :cond_8
    const/4 v4, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 237312
    iget-object v0, p0, LX/1NJ;->h:LX/1UQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1NJ;->i:LX/0g5;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 237313
    :goto_0
    return v0

    .line 237314
    :cond_1
    iget-object v0, p0, LX/1NJ;->h:LX/1UQ;

    iget-object v2, p0, LX/1NJ;->i:LX/0g5;

    invoke-static {p0, v2}, LX/1NJ;->a(LX/1NJ;LX/0g8;)I

    move-result v2

    invoke-interface {v0, v2}, LX/1Qr;->h_(I)I

    move-result v0

    .line 237315
    if-ltz v0, :cond_2

    iget-object v2, p0, LX/1NJ;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->v()I

    move-result v2

    if-lt v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 237316
    goto :goto_0

    .line 237317
    :cond_3
    iget-object v2, p0, LX/1NJ;->c:LX/0fz;

    invoke-virtual {v2, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 237318
    instance-of v2, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-eqz v2, :cond_4

    .line 237319
    iget-object v2, p0, LX/1NJ;->f:LX/1EM;

    invoke-virtual {v2}, LX/1EM;->e()J

    .line 237320
    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 237321
    iget-wide v6, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->D:J

    move-wide v2, v6

    .line 237322
    iget-object v0, p0, LX/1NJ;->f:LX/1EM;

    invoke-virtual {v0}, LX/1EM;->e()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 237323
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 237324
    goto :goto_0
.end method
