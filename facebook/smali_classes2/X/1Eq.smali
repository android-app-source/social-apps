.class public final enum LX/1Eq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Eq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Eq;

.field public static final enum CACHE_HIT:LX/1Eq;

.field public static final enum CACHE_MISS:LX/1Eq;

.field public static final enum CACHE_MISS_PREFETCH_IN_FLIGHT:LX/1Eq;

.field public static final enum CACHE_STALE_HIT:LX/1Eq;

.field public static final enum COULD_PREFETCH:LX/1Eq;

.field public static final enum PREFETCH_FAILED:LX/1Eq;

.field public static final enum PREFETCH_SUCCEEDED:LX/1Eq;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 221287
    new-instance v0, LX/1Eq;

    const-string v1, "COULD_PREFETCH"

    const-string v2, "could_prefetch"

    invoke-direct {v0, v1, v4, v2}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->COULD_PREFETCH:LX/1Eq;

    .line 221288
    new-instance v0, LX/1Eq;

    const-string v1, "PREFETCH_SUCCEEDED"

    const-string v2, "prefetch_succeeded"

    invoke-direct {v0, v1, v5, v2}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->PREFETCH_SUCCEEDED:LX/1Eq;

    .line 221289
    new-instance v0, LX/1Eq;

    const-string v1, "PREFETCH_FAILED"

    const-string v2, "prefetch_failed"

    invoke-direct {v0, v1, v6, v2}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->PREFETCH_FAILED:LX/1Eq;

    .line 221290
    new-instance v0, LX/1Eq;

    const-string v1, "CACHE_HIT"

    const-string v2, "cache_hit"

    invoke-direct {v0, v1, v7, v2}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->CACHE_HIT:LX/1Eq;

    .line 221291
    new-instance v0, LX/1Eq;

    const-string v1, "CACHE_STALE_HIT"

    const-string v2, "cache_stale_hit"

    invoke-direct {v0, v1, v8, v2}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->CACHE_STALE_HIT:LX/1Eq;

    .line 221292
    new-instance v0, LX/1Eq;

    const-string v1, "CACHE_MISS"

    const/4 v2, 0x5

    const-string v3, "cache_miss"

    invoke-direct {v0, v1, v2, v3}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->CACHE_MISS:LX/1Eq;

    .line 221293
    new-instance v0, LX/1Eq;

    const-string v1, "CACHE_MISS_PREFETCH_IN_FLIGHT"

    const/4 v2, 0x6

    const-string v3, "cache_miss_prefetch_in_flight"

    invoke-direct {v0, v1, v2, v3}, LX/1Eq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1Eq;->CACHE_MISS_PREFETCH_IN_FLIGHT:LX/1Eq;

    .line 221294
    const/4 v0, 0x7

    new-array v0, v0, [LX/1Eq;

    sget-object v1, LX/1Eq;->COULD_PREFETCH:LX/1Eq;

    aput-object v1, v0, v4

    sget-object v1, LX/1Eq;->PREFETCH_SUCCEEDED:LX/1Eq;

    aput-object v1, v0, v5

    sget-object v1, LX/1Eq;->PREFETCH_FAILED:LX/1Eq;

    aput-object v1, v0, v6

    sget-object v1, LX/1Eq;->CACHE_HIT:LX/1Eq;

    aput-object v1, v0, v7

    sget-object v1, LX/1Eq;->CACHE_STALE_HIT:LX/1Eq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/1Eq;->CACHE_MISS:LX/1Eq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1Eq;->CACHE_MISS_PREFETCH_IN_FLIGHT:LX/1Eq;

    aput-object v2, v0, v1

    sput-object v0, LX/1Eq;->$VALUES:[LX/1Eq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 221295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 221296
    iput-object p3, p0, LX/1Eq;->mText:Ljava/lang/String;

    .line 221297
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Eq;
    .locals 1

    .prologue
    .line 221298
    const-class v0, LX/1Eq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Eq;

    return-object v0
.end method

.method public static values()[LX/1Eq;
    .locals 1

    .prologue
    .line 221299
    sget-object v0, LX/1Eq;->$VALUES:[LX/1Eq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Eq;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221300
    iget-object v0, p0, LX/1Eq;->mText:Ljava/lang/String;

    return-object v0
.end method
