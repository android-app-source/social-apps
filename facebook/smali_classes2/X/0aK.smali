.class public LX/0aK;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84469
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 84470
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0ZT;LX/0ZU;LX/0ZW;LX/0VT;LX/5og;LX/03V;LX/0aJ;LX/0Ot;)LX/0ac;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ZT;",
            "LX/0ZU;",
            "LX/0ZW;",
            "Lcom/facebook/common/process/ProcessUtil;",
            "LX/5og;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0aJ;",
            "LX/0Ot",
            "<",
            "LX/33I;",
            ">;)",
            "LX/0ac;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84471
    invoke-virtual {p4}, LX/0VT;->a()LX/00G;

    move-result-object v0

    invoke-virtual {v0}, LX/00G;->e()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 84472
    :goto_0
    new-instance v3, LX/0aL;

    invoke-direct {v3}, LX/0aL;-><init>()V

    new-instance v4, LX/0aM;

    invoke-direct {v4, p0}, LX/0aM;-><init>(Landroid/content/Context;)V

    .line 84473
    iput-object v4, v3, LX/0aL;->g:LX/0aM;

    .line 84474
    move-object v3, v3

    .line 84475
    iput-object p1, v3, LX/0aL;->f:LX/0ZT;

    .line 84476
    move-object v3, v3

    .line 84477
    iput-object p2, v3, LX/0aL;->h:LX/0ZU;

    .line 84478
    move-object v3, v3

    .line 84479
    iput-object p3, v3, LX/0aL;->e:LX/0ZW;

    .line 84480
    move-object v3, v3

    .line 84481
    new-instance v4, LX/0aN;

    const-string v5, "qe_sessioned"

    invoke-virtual {p0, v5, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, v0}, LX/0aN;-><init>(Ljava/io/File;Z)V

    .line 84482
    iput-object v4, v3, LX/0aL;->a:LX/0aN;

    .line 84483
    move-object v3, v3

    .line 84484
    new-instance v4, LX/0aN;

    const-string v5, "qe_sessionless"

    invoke-virtual {p0, v5, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v4, v2, v0}, LX/0aN;-><init>(Ljava/io/File;Z)V

    .line 84485
    iput-object v4, v3, LX/0aL;->b:LX/0aN;

    .line 84486
    move-object v2, v3

    .line 84487
    invoke-static {p0}, LX/0aO;->b(Landroid/content/Context;)LX/0aa;

    move-result-object v3

    .line 84488
    iput-object v3, v2, LX/0aL;->c:LX/0aa;

    .line 84489
    move-object v2, v2

    .line 84490
    invoke-static {p0}, LX/0aO;->a(Landroid/content/Context;)LX/0aa;

    move-result-object v3

    .line 84491
    iput-object v3, v2, LX/0aL;->d:LX/0aa;

    .line 84492
    move-object v2, v2

    .line 84493
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, LX/0aL;->i:Ljava/lang/Boolean;

    .line 84494
    move-object v0, v2

    .line 84495
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/0aL;->j:Ljava/lang/Boolean;

    .line 84496
    move-object v0, v0

    .line 84497
    iput-object p5, v0, LX/0aL;->k:LX/5og;

    .line 84498
    move-object v0, v0

    .line 84499
    iput-object p6, v0, LX/0aL;->l:LX/03V;

    .line 84500
    move-object v1, v0

    .line 84501
    const/4 v0, 0x0

    .line 84502
    iget-object v2, p7, LX/0aJ;->a:LX/0Uh;

    const/16 v3, 0xd

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 84503
    iget-object v3, p7, LX/0aJ;->a:LX/0Uh;

    const/16 v4, 0xc

    invoke-virtual {v3, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 84504
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 84505
    if-eqz v0, :cond_1

    .line 84506
    invoke-interface {p8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33I;

    .line 84507
    iput-object v0, v1, LX/0aL;->m:LX/33I;

    .line 84508
    :cond_1
    invoke-virtual {v1}, LX/0aL;->a()LX/0ac;

    move-result-object v0

    .line 84509
    return-object v0

    :cond_2
    move v0, v2

    .line 84510
    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 84511
    return-void
.end method
