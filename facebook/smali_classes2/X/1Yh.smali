.class public abstract LX/1Yh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Yi;

.field public static final b:LX/1Yj;


# instance fields
.field public final c:LX/0fx;

.field private final d:LX/0g8;

.field private final e:LX/0Sy;

.field private final f:LX/0Vq;

.field private final g:Z

.field public final h:LX/1Yp;

.field private final i:LX/1Yi;

.field private final j:LX/1Yj;

.field public k:Z

.field public l:Z

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273943
    sget-object v0, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    sput-object v0, LX/1Yh;->a:LX/1Yi;

    .line 273944
    sget-object v0, LX/1Yj;->ALL_OFFSCREEN:LX/1Yj;

    sput-object v0, LX/1Yh;->b:LX/1Yj;

    return-void
.end method

.method public constructor <init>(LX/0g8;I)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 273941
    sget-object v3, LX/1Yh;->a:LX/1Yi;

    sget-object v4, LX/1Yh;->b:LX/1Yj;

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v7, v5

    invoke-direct/range {v0 .. v8}, LX/1Yh;-><init>(LX/0g8;ILX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V

    .line 273942
    return-void
.end method

.method public constructor <init>(LX/0g8;ILX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V
    .locals 9
    .param p7    # LX/1AP;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 273939
    new-instance v2, LX/1Yo;

    invoke-direct {v2, p2}, LX/1Yo;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/1Yh;-><init>(LX/0g8;LX/1Yp;LX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V

    .line 273940
    return-void
.end method

.method public constructor <init>(LX/0g8;LX/1Yp;LX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V
    .locals 2
    .param p7    # LX/1AP;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 273910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273911
    iput-boolean v0, p0, LX/1Yh;->k:Z

    .line 273912
    iput-boolean v0, p0, LX/1Yh;->l:Z

    .line 273913
    iput v1, p0, LX/1Yh;->m:I

    .line 273914
    iput v1, p0, LX/1Yh;->n:I

    .line 273915
    iput v1, p0, LX/1Yh;->o:I

    .line 273916
    iput v1, p0, LX/1Yh;->p:I

    .line 273917
    iput v1, p0, LX/1Yh;->q:I

    .line 273918
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273919
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273920
    if-nez p6, :cond_0

    if-eqz p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 273921
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273922
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273923
    iput-object p1, p0, LX/1Yh;->d:LX/0g8;

    .line 273924
    iput-object p2, p0, LX/1Yh;->h:LX/1Yp;

    .line 273925
    iput-object p3, p0, LX/1Yh;->i:LX/1Yi;

    .line 273926
    iput-object p4, p0, LX/1Yh;->j:LX/1Yj;

    .line 273927
    new-instance v0, LX/1Yr;

    invoke-direct {v0, p0, p8}, LX/1Yr;-><init>(LX/1Yh;I)V

    .line 273928
    if-eqz p7, :cond_2

    .line 273929
    invoke-virtual {p7, v0}, LX/1AP;->b(LX/0fx;)LX/1Ks;

    move-result-object v1

    move-object v0, v1

    .line 273930
    :cond_2
    iput-object v0, p0, LX/1Yh;->c:LX/0fx;

    .line 273931
    iget-object v0, p0, LX/1Yh;->d:LX/0g8;

    iget-object v1, p0, LX/1Yh;->c:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 273932
    iput-object p5, p0, LX/1Yh;->e:LX/0Sy;

    .line 273933
    iput-boolean p6, p0, LX/1Yh;->g:Z

    .line 273934
    iget-boolean v0, p0, LX/1Yh;->g:Z

    if-nez v0, :cond_3

    .line 273935
    new-instance v0, LX/624;

    invoke-direct {v0, p0}, LX/624;-><init>(LX/1Yh;)V

    iput-object v0, p0, LX/1Yh;->f:LX/0Vq;

    .line 273936
    iget-object v0, p0, LX/1Yh;->e:LX/0Sy;

    iget-object v1, p0, LX/1Yh;->f:LX/0Vq;

    invoke-virtual {v0, v1}, LX/0Sy;->a(LX/0Vq;)V

    .line 273937
    :goto_0
    return-void

    .line 273938
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Yh;->f:LX/0Vq;

    goto :goto_0
.end method

.method private a(ILX/1fq;)V
    .locals 1

    .prologue
    .line 273945
    invoke-virtual {p0, p1}, LX/1Yh;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273946
    invoke-virtual {p0, p1}, LX/1Yh;->a(I)V

    .line 273947
    :cond_0
    return-void
.end method

.method public static b(LX/1Yh;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273889
    iget-boolean v0, p0, LX/1Yh;->l:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/1Yh;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/1Yh;->g:Z

    if-eqz v0, :cond_6

    .line 273890
    :cond_0
    iget-object v0, p0, LX/1Yh;->j:LX/1Yj;

    sget-object v3, LX/1Yj;->ALL_ONSCREEN_AND_OFFSCREEN:LX/1Yj;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, LX/1Yh;->i:LX/1Yi;

    sget-object v3, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    if-ne v0, v3, :cond_1

    .line 273891
    iget v0, p0, LX/1Yh;->n:I

    :goto_0
    iget v3, p0, LX/1Yh;->o:I

    if-gt v0, v3, :cond_1

    .line 273892
    sget-object v3, LX/1fq;->ONSCREEN:LX/1fq;

    invoke-direct {p0, v0, v3}, LX/1Yh;->a(ILX/1fq;)V

    .line 273893
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273894
    :cond_1
    iget v0, p0, LX/1Yh;->n:I

    iget v3, p0, LX/1Yh;->m:I

    sub-int/2addr v0, v3

    iget v3, p0, LX/1Yh;->p:I

    iget v4, p0, LX/1Yh;->o:I

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 273895
    add-int/lit8 v0, v3, 0x1

    .line 273896
    iget-object v4, p0, LX/1Yh;->i:LX/1Yi;

    sget-object v5, LX/1Yi;->FURTHEST_FIRST:LX/1Yi;

    if-ne v4, v5, :cond_7

    .line 273897
    const/4 v0, -0x1

    move v1, v2

    .line 273898
    :goto_1
    if-eq v3, v1, :cond_4

    .line 273899
    iget v4, p0, LX/1Yh;->n:I

    sub-int/2addr v4, v3

    iget v5, p0, LX/1Yh;->m:I

    if-lt v4, v5, :cond_2

    .line 273900
    iget v4, p0, LX/1Yh;->n:I

    sub-int/2addr v4, v3

    sget-object v5, LX/1fq;->OFFSCREEN:LX/1fq;

    invoke-direct {p0, v4, v5}, LX/1Yh;->a(ILX/1fq;)V

    .line 273901
    :cond_2
    iget v4, p0, LX/1Yh;->o:I

    add-int/2addr v4, v3

    iget v5, p0, LX/1Yh;->p:I

    if-gt v4, v5, :cond_3

    .line 273902
    iget v4, p0, LX/1Yh;->o:I

    add-int/2addr v4, v3

    sget-object v5, LX/1fq;->OFFSCREEN:LX/1fq;

    invoke-direct {p0, v4, v5}, LX/1Yh;->a(ILX/1fq;)V

    .line 273903
    :cond_3
    add-int/2addr v3, v0

    goto :goto_1

    .line 273904
    :cond_4
    iget-object v0, p0, LX/1Yh;->j:LX/1Yj;

    sget-object v1, LX/1Yj;->ALL_ONSCREEN_AND_OFFSCREEN:LX/1Yj;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, LX/1Yh;->i:LX/1Yi;

    sget-object v1, LX/1Yi;->FURTHEST_FIRST:LX/1Yi;

    if-ne v0, v1, :cond_5

    .line 273905
    iget v0, p0, LX/1Yh;->n:I

    :goto_2
    iget v1, p0, LX/1Yh;->o:I

    if-gt v0, v1, :cond_5

    .line 273906
    sget-object v1, LX/1fq;->ONSCREEN:LX/1fq;

    invoke-direct {p0, v0, v1}, LX/1Yh;->a(ILX/1fq;)V

    .line 273907
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 273908
    :cond_5
    iput-boolean v2, p0, LX/1Yh;->l:Z

    .line 273909
    :cond_6
    return-void

    :cond_7
    move v3, v1

    move v6, v0

    move v0, v1

    move v1, v6

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 273885
    iget-object v0, p0, LX/1Yh;->d:LX/0g8;

    iget-object v1, p0, LX/1Yh;->c:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    .line 273886
    iget-object v0, p0, LX/1Yh;->e:LX/0Sy;

    if-eqz v0, :cond_0

    .line 273887
    iget-object v0, p0, LX/1Yh;->e:LX/0Sy;

    iget-object v1, p0, LX/1Yh;->f:LX/0Vq;

    invoke-virtual {v0, v1}, LX/0Sy;->b(LX/0Vq;)V

    .line 273888
    :cond_0
    return-void
.end method

.method public abstract a(I)V
.end method

.method public abstract b(I)Z
.end method

.method public abstract c(I)V
.end method
