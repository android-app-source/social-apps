.class public LX/1gA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 293830
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1gA;->a:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 293807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293808
    return-void
.end method

.method private static a(ILandroid/view/View;)V
    .locals 7

    .prologue
    .line 293815
    instance-of v0, p1, LX/1a6;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/1a6;

    invoke-interface {v0}, LX/1a6;->getWrappedView()Landroid/view/View;

    move-result-object v1

    .line 293816
    :goto_0
    instance-of v0, v1, Lcom/facebook/components/ComponentView;

    if-nez v0, :cond_2

    .line 293817
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v1, p1

    .line 293818
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 293819
    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 293820
    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293821
    if-eq p1, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eq v2, v1, :cond_3

    .line 293822
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewDiagnosticsWrapper must be the same height as the underlying view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293823
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 293824
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 293825
    if-lez v1, :cond_4

    if-ge v2, p0, :cond_4

    .line 293826
    iget-object v3, v0, Lcom/facebook/components/ComponentView;->d:Landroid/graphics/Rect;

    move-object v3, v3

    .line 293827
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getHeight()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 293828
    :cond_4
    sget-object v3, LX/1gA;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getLeft()I

    move-result v4

    const/4 v5, 0x0

    neg-int v6, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getRight()I

    move-result v6

    invoke-static {v2, p0}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int v1, v2, v1

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 293829
    sget-object v1, LX/1gA;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->a(Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 293809
    invoke-static {}, LX/1dS;->b()V

    .line 293810
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    .line 293811
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 293812
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v2}, LX/1gA;->a(ILandroid/view/View;)V

    .line 293813
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 293814
    :cond_0
    return-void
.end method
