.class public final LX/1Ch;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;",
        ">;",
        "LX/3j3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Cg;


# direct methods
.method public constructor <init>(LX/1Cg;)V
    .locals 0

    .prologue
    .line 216688
    iput-object p1, p0, LX/1Ch;->a:LX/1Cg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 216689
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 216690
    if-eqz p1, :cond_0

    .line 216691
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 216692
    if-eqz v0, :cond_0

    .line 216693
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 216694
    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 216695
    :cond_0
    const/4 v0, 0x0

    .line 216696
    :goto_0
    return-object v0

    .line 216697
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 216698
    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;

    invoke-static {v0}, LX/3j0;->a(Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;)Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLViewer;->k()Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    move-result-object v0

    .line 216699
    new-instance v1, LX/1u8;

    invoke-direct {v1}, LX/1u8;-><init>()V

    .line 216700
    iput-object v0, v1, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 216701
    move-object v1, v1

    .line 216702
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->a()Ljava/lang/String;

    move-result-object v0

    .line 216703
    iput-object v0, v1, LX/1u8;->d:Ljava/lang/String;

    .line 216704
    move-object v0, v1

    .line 216705
    invoke-virtual {v0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 216706
    new-instance v1, LX/3j3;

    iget-object v3, p0, LX/1Ch;->a:LX/1Cg;

    .line 216707
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 216708
    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialoguePinnedStoryQueryModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/FetchDailyDialoguePinnedUnitsGraphQLModels$DailyDialogueCustomizedStoryModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v2, v0}, LX/3j3;-><init>(LX/1Cg;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method
