.class public LX/0P2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation
.end field

.field public b:[LX/0P3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0P3",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54902
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/0P2;-><init>(I)V

    .line 54903
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54905
    new-array v0, p1, [LX/0P3;

    iput-object v0, p0, LX/0P2;->b:[LX/0P3;

    .line 54906
    iput v1, p0, LX/0P2;->c:I

    .line 54907
    iput-boolean v1, p0, LX/0P2;->d:Z

    .line 54908
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 54909
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 54910
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    array-length v1, v1

    invoke-static {v1, p1}, LX/0P7;->a(II)I

    move-result v1

    invoke-static {v0, v1}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0P3;

    iput-object v0, p0, LX/0P2;->b:[LX/0P3;

    .line 54911
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0P2;->d:Z

    .line 54912
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)LX/0P2;
    .locals 2
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;>;)",
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54878
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 54879
    iget v1, p0, LX/0P2;->c:I

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/0P2;->a(I)V

    .line 54880
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 54881
    invoke-virtual {p0, v0}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    goto :goto_0

    .line 54882
    :cond_1
    return-object p0
.end method

.method public a(Ljava/util/Map$Entry;)LX/0P2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<+TK;+TV;>;)",
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54901
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Map;)LX/0P2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)",
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54900
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0P2;->a(Ljava/lang/Iterable;)LX/0P2;

    move-result-object v0

    return-object v0
.end method

.method public b()LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 54887
    iget v0, p0, LX/0P2;->c:I

    packed-switch v0, :pswitch_data_0

    .line 54888
    iget-object v0, p0, LX/0P2;->a:Ljava/util/Comparator;

    if-eqz v0, :cond_1

    .line 54889
    iget-boolean v0, p0, LX/0P2;->d:Z

    if-eqz v0, :cond_0

    .line 54890
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    invoke-static {v0, v2}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0P3;

    iput-object v0, p0, LX/0P2;->b:[LX/0P3;

    .line 54891
    :cond_0
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    iget-object v3, p0, LX/0P2;->a:Ljava/util/Comparator;

    invoke-static {v3}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v3

    .line 54892
    sget-object v4, LX/2zy;->VALUE:LX/2zy;

    move-object v4, v4

    .line 54893
    invoke-virtual {v3, v4}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 54894
    :cond_1
    iget v0, p0, LX/0P2;->c:I

    iget-object v2, p0, LX/0P2;->b:[LX/0P3;

    array-length v2, v2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0P2;->d:Z

    .line 54895
    iget v0, p0, LX/0P2;->c:I

    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    invoke-static {v0, v1}, LX/0PA;->a(I[Ljava/util/Map$Entry;)LX/0PA;

    move-result-object v0

    :goto_1
    return-object v0

    .line 54896
    :pswitch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 54897
    goto :goto_1

    .line 54898
    :pswitch_1
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LX/0P2;->b:[LX/0P3;

    aget-object v1, v2, v1

    invoke-virtual {v1}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 54899
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 54883
    iget v0, p0, LX/0P2;->c:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/0P2;->a(I)V

    .line 54884
    invoke-static {p1, p2}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v0

    .line 54885
    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/0P2;->c:I

    aput-object v0, v1, v2

    .line 54886
    return-object p0
.end method
