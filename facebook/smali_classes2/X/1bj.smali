.class public final enum LX/1bj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1bj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1bj;

.field public static final enum BITMAP_MEMORY_CACHE:LX/1bj;

.field public static final enum DISK_CACHE:LX/1bj;

.field public static final enum FULL_FETCH:LX/1bj;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280885
    new-instance v0, LX/1bj;

    const-string v1, "FULL_FETCH"

    invoke-direct {v0, v1, v2}, LX/1bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bj;->FULL_FETCH:LX/1bj;

    .line 280886
    new-instance v0, LX/1bj;

    const-string v1, "DISK_CACHE"

    invoke-direct {v0, v1, v3}, LX/1bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bj;->DISK_CACHE:LX/1bj;

    .line 280887
    new-instance v0, LX/1bj;

    const-string v1, "BITMAP_MEMORY_CACHE"

    invoke-direct {v0, v1, v4}, LX/1bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1bj;->BITMAP_MEMORY_CACHE:LX/1bj;

    .line 280888
    const/4 v0, 0x3

    new-array v0, v0, [LX/1bj;

    sget-object v1, LX/1bj;->FULL_FETCH:LX/1bj;

    aput-object v1, v0, v2

    sget-object v1, LX/1bj;->DISK_CACHE:LX/1bj;

    aput-object v1, v0, v3

    sget-object v1, LX/1bj;->BITMAP_MEMORY_CACHE:LX/1bj;

    aput-object v1, v0, v4

    sput-object v0, LX/1bj;->$VALUES:[LX/1bj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 280889
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1bj;
    .locals 1

    .prologue
    .line 280890
    const-class v0, LX/1bj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1bj;

    return-object v0
.end method

.method public static values()[LX/1bj;
    .locals 1

    .prologue
    .line 280891
    sget-object v0, LX/1bj;->$VALUES:[LX/1bj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1bj;

    return-object v0
.end method
