.class public LX/1SW;
.super LX/1SX;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/lang/Runnable;

.field private final e:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247943
    new-instance v0, LX/1SY;

    invoke-direct {v0}, LX/1SY;-><init>()V

    sput-object v0, LX/1SW;->a:LX/0Or;

    .line 247944
    new-instance v0, LX/1SZ;

    invoke-direct {v0}, LX/1SZ;-><init>()V

    sput-object v0, LX/1SW;->b:LX/0Or;

    .line 247945
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/1SW;->c:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Zb;LX/17Q;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V
    .locals 42
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p24    # LX/1Qj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/1Qj;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 247946
    sget-object v16, LX/1SW;->a:LX/0Or;

    sget-object v17, LX/1SW;->b:LX/0Or;

    sget-object v18, LX/1SW;->a:LX/0Or;

    const/16 v21, 0x0

    sget-object v25, LX/1SW;->a:LX/0Or;

    const/16 v34, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v19, p15

    move-object/from16 v20, p16

    move-object/from16 v22, p17

    move-object/from16 v23, p18

    move-object/from16 v24, p19

    move-object/from16 v26, p20

    move-object/from16 v27, p21

    move-object/from16 v28, p22

    move-object/from16 v29, p23

    move-object/from16 v30, p24

    move-object/from16 v31, p25

    move-object/from16 v32, p26

    move-object/from16 v33, p27

    move-object/from16 v35, p28

    move-object/from16 v36, p29

    move-object/from16 v37, p30

    move-object/from16 v41, p31

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 247947
    new-instance v2, LX/1So;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/1So;-><init>(LX/1SW;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/1SW;->e:LX/1Sp;

    .line 247948
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/1SW;->d:Ljava/lang/Runnable;

    .line 247949
    const-string v2, "daily_dialogue"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 247950
    const-string v2, "native_good_morning"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 247951
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1SW;->e:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 247952
    return-void
.end method


# virtual methods
.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 247942
    new-instance v0, LX/DIW;

    invoke-direct {v0, p0}, LX/DIW;-><init>(LX/1SW;)V

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 247941
    const/4 v0, 0x1

    return v0
.end method
