.class public LX/1XT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;

.field public final b:LX/0g8;

.field private final c:LX/1UQ;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/view/ViewGroup$LayoutParams;

.field private final f:LX/1Db;

.field public g:LX/1XS;


# direct methods
.method public constructor <init>(LX/0g8;LX/1UQ;LX/1Db;LX/03V;)V
    .locals 3
    .param p1    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1UQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 271283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271284
    iput-object p1, p0, LX/1XT;->b:LX/0g8;

    .line 271285
    iput-object p2, p0, LX/1XT;->c:LX/1UQ;

    .line 271286
    iput-object p4, p0, LX/1XT;->a:LX/03V;

    .line 271287
    iput-object p3, p0, LX/1XT;->f:LX/1Db;

    .line 271288
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v0, p0, LX/1XT;->e:Landroid/view/ViewGroup$LayoutParams;

    .line 271289
    new-instance v0, LX/0S8;

    invoke-direct {v0}, LX/0S8;-><init>()V

    invoke-virtual {v0}, LX/0S8;->g()LX/0S8;

    move-result-object v0

    invoke-virtual {v0}, LX/0S8;->l()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LX/1XT;->d:Ljava/util/Map;

    .line 271290
    return-void
.end method

.method private static a(LX/1XT;LX/1Rq;LX/1a1;II)LX/1a1;
    .locals 1
    .param p1    # LX/1Rq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 271279
    if-nez p2, :cond_0

    .line 271280
    iget-object v0, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v0}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-interface {p1, v0, p4}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object p2

    .line 271281
    :cond_0
    invoke-interface {p1, p2, p3}, LX/1OQ;->a(LX/1a1;I)V

    .line 271282
    return-object p2
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;LX/1a1;II)LX/1a1;
    .locals 8
    .param p2    # LX/1a1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 271261
    invoke-static {p0, p3}, LX/1XT;->b(LX/1XT;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271262
    iget-object v0, p0, LX/1XT;->a:LX/03V;

    const-string v2, "RowHeightMeasurer_IllegalIndex"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to measure index "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " when adapter item count is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/1XT;->c:LX/1UQ;

    invoke-interface {v4}, LX/1OP;->ij_()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271263
    :goto_0
    return-object v1

    .line 271264
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    instance-of v0, v0, LX/1Qq;

    if-eqz v0, :cond_1

    .line 271265
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    check-cast v0, LX/1Qq;

    .line 271266
    if-eqz p2, :cond_4

    iget-object v2, p2, LX/1a1;->a:Landroid/view/View;

    .line 271267
    :goto_1
    iget-object v3, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v3}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-interface {v0, p3, v2, v3}, LX/1Qq;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 271268
    new-instance v3, LX/AkU;

    invoke-direct {v3, p0, v2}, LX/AkU;-><init>(LX/1XT;Landroid/view/View;)V

    move-object v1, v3

    .line 271269
    goto :goto_0

    .line 271270
    :cond_1
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    instance-of v0, v0, LX/1Rq;

    if-eqz v0, :cond_2

    .line 271271
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    check-cast v0, LX/1Rq;

    invoke-static {p0, v0, p2, p3, p4}, LX/1XT;->a(LX/1XT;LX/1Rq;LX/1a1;II)LX/1a1;

    move-result-object v1

    goto :goto_0

    .line 271272
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown Feed Adapter type"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271273
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 271274
    iget-object v3, p0, LX/1XT;->a:LX/03V;

    const-string v4, "RowHeightMeasurer"

    const-string v5, "Problem measuring %s. Zombie: %s"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez p2, :cond_3

    move-object v0, v1

    :goto_2
    aput-object v0, v6, v7

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 271275
    iput-object v2, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 271276
    move-object v0, v0

    .line 271277
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    :cond_3
    iget-object v0, p2, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 271278
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static b(LX/1XT;Lcom/facebook/graphql/model/FeedUnit;I)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 271230
    invoke-static {p0, p2}, LX/1XT;->b(LX/1XT;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271231
    iget-object v0, p0, LX/1XT;->a:LX/03V;

    const-string v2, "RowHeightMeasurer_IllegalIndex"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to get height for index="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " when adapter.getCount()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LX/1XT;->c:LX/1UQ;

    invoke-interface {v4}, LX/1OP;->ij_()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 271232
    :goto_0
    return v0

    .line 271233
    :cond_0
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    invoke-interface {v0, p2}, LX/1OP;->getItemViewType(I)I

    move-result v2

    .line 271234
    iget-object v0, p0, LX/1XT;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 271235
    invoke-direct {p0, p1, v0, p2, v2}, LX/1XT;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1a1;II)LX/1a1;

    move-result-object v3

    .line 271236
    if-nez v3, :cond_1

    move v0, v1

    .line 271237
    goto :goto_0

    .line 271238
    :cond_1
    iget-object v1, v3, LX/1a1;->a:Landroid/view/View;

    .line 271239
    const/4 p2, 0x0

    .line 271240
    iget-object v4, p0, LX/1XT;->e:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271241
    iget-object v4, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v4}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    const/high16 p1, 0x40000000    # 2.0f

    invoke-static {v4, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {p2, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v1, v4, p1}, Landroid/view/View;->measure(II)V

    .line 271242
    if-nez v0, :cond_2

    .line 271243
    iget-object v0, p0, LX/1XT;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271244
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 271245
    invoke-static {v1}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    goto :goto_0
.end method

.method private static b(LX/1XT;I)Z
    .locals 1

    .prologue
    .line 271260
    iget-object v0, p0, LX/1XT;->c:LX/1UQ;

    invoke-interface {v0, p1}, LX/1Rs;->b(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;III)V
    .locals 3

    .prologue
    .line 271246
    iget-object v0, p0, LX/1XT;->g:LX/1XS;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271247
    iget-object v0, p0, LX/1XT;->g:LX/1XS;

    invoke-virtual {v0, p1, p4, p2}, LX/1XS;->b(Lcom/facebook/graphql/model/FeedUnit;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271248
    :cond_0
    :goto_0
    return-void

    .line 271249
    :cond_1
    iget-object v0, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    sub-int v0, p3, v0

    .line 271250
    if-ltz v0, :cond_2

    iget-object v1, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v1}, LX/0g8;->p()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 271251
    :cond_2
    invoke-static {p0, p1, p3}, LX/1XT;->b(LX/1XT;Lcom/facebook/graphql/model/FeedUnit;I)I

    move-result v0

    .line 271252
    :cond_3
    :goto_1
    move v0, v0

    .line 271253
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 271254
    iget-object v1, p0, LX/1XT;->g:LX/1XS;

    invoke-virtual {v1, p1, p4, v0, p2}, LX/1XS;->a(Lcom/facebook/graphql/model/FeedUnit;III)V

    goto :goto_0

    .line 271255
    :cond_4
    iget-object v1, p0, LX/1XT;->b:LX/0g8;

    invoke-interface {v1, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    .line 271256
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-eqz v2, :cond_5

    .line 271257
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 271258
    :goto_2
    move v0, v1

    .line 271259
    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    invoke-static {p0, p1, p3}, LX/1XT;->b(LX/1XT;Lcom/facebook/graphql/model/FeedUnit;I)I

    move-result v0

    goto :goto_1

    :cond_5
    const/4 v1, -0x1

    goto :goto_2
.end method
