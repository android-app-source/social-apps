.class public LX/0t3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0t3;


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153515
    iput-object p1, p0, LX/0t3;->a:LX/0lC;

    .line 153516
    return-void
.end method

.method public static a(LX/0QB;)LX/0t3;
    .locals 4

    .prologue
    .line 153501
    sget-object v0, LX/0t3;->b:LX/0t3;

    if-nez v0, :cond_1

    .line 153502
    const-class v1, LX/0t3;

    monitor-enter v1

    .line 153503
    :try_start_0
    sget-object v0, LX/0t3;->b:LX/0t3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 153504
    if-eqz v2, :cond_0

    .line 153505
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 153506
    new-instance p0, LX/0t3;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/0t3;-><init>(LX/0lC;)V

    .line 153507
    move-object v0, p0

    .line 153508
    sput-object v0, LX/0t3;->b:LX/0t3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153509
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 153510
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153511
    :cond_1
    sget-object v0, LX/0t3;->b:LX/0t3;

    return-object v0

    .line 153512
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 153513
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Enum;
    .locals 2

    .prologue
    .line 153493
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153494
    if-nez p0, :cond_0

    .line 153495
    const-string p0, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    .line 153496
    :cond_0
    :try_start_1
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 153497
    :goto_0
    return-object v0

    .line 153498
    :catch_0
    move-exception v0

    .line 153499
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 153500
    :catch_1
    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 153481
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 153482
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 153483
    :cond_0
    :goto_0
    return-object p1

    .line 153484
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 153485
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    goto :goto_0

    .line 153486
    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 153487
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    .line 153488
    :cond_3
    const/4 v0, 0x4

    if-ne p0, v0, :cond_4

    .line 153489
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_0

    .line 153490
    :cond_4
    const/4 v0, 0x6

    if-ne p0, v0, :cond_0

    .line 153491
    const-string v0, "Missing enum class name"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153492
    invoke-static {p1, p2}, LX/0t3;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p1

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 153428
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 153429
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153430
    const/4 v0, -0x1

    .line 153431
    :goto_0
    return v0

    .line 153432
    :cond_0
    check-cast p0, Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .line 153433
    :cond_1
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 153434
    const/4 v0, 0x1

    goto :goto_0

    .line 153435
    :cond_2
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 153436
    const/4 v0, 0x2

    goto :goto_0

    .line 153437
    :cond_3
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 153438
    const/4 v0, 0x3

    goto :goto_0

    .line 153439
    :cond_4
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 153440
    const/4 v0, 0x4

    goto :goto_0

    .line 153441
    :cond_5
    instance-of v0, p0, Ljava/lang/Enum;

    if-eqz v0, :cond_6

    .line 153442
    const/4 v0, 0x6

    goto :goto_0

    .line 153443
    :cond_6
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 153444
    const/4 v0, 0x5

    goto :goto_0

    .line 153445
    :cond_7
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public static c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153478
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153479
    check-cast p0, Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 153480
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b(ILjava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 153452
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 153453
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 153454
    :goto_0
    return-object v0

    .line 153455
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 153456
    :try_start_0
    const-class v0, Ljava/lang/Integer;

    .line 153457
    :goto_1
    iget-object v1, p0, LX/0t3;->a:LX/0lC;

    invoke-virtual {v1}, LX/0lD;->b()LX/0lp;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 153458
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 153459
    invoke-virtual {v1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_7

    .line 153460
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 153461
    iget-object v2, p0, LX/0t3;->a:LX/0lC;

    invoke-virtual {v2, v1, v0}, LX/0lC;->c(LX/15w;Ljava/lang/Class;)LX/4pr;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 153462
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 153463
    const-class v0, Ljava/lang/Double;

    goto :goto_1

    .line 153464
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 153465
    const-class v0, Ljava/lang/Long;

    goto :goto_1

    .line 153466
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 153467
    const-class v0, Ljava/lang/Boolean;

    goto :goto_1

    .line 153468
    :cond_4
    const/4 v0, 0x6

    if-ne p1, v0, :cond_5

    .line 153469
    invoke-static {p3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_1

    .line 153470
    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    .line 153471
    const-class v0, Ljava/lang/String;

    goto :goto_1

    .line 153472
    :cond_6
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported type number "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 153473
    :catch_0
    move-exception v0

    .line 153474
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 153475
    :cond_7
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to parse array, expecting start array but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 153476
    :catch_1
    move-exception v0

    .line 153477
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153446
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 153447
    :try_start_0
    iget-object v0, p0, LX/0t3;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153448
    :goto_0
    return-object v0

    .line 153449
    :catch_0
    move-exception v0

    .line 153450
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 153451
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
