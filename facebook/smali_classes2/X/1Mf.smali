.class public LX/1Mf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0oz;

.field private final c:LX/0kb;

.field private final d:LX/0So;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6XZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/0kb;LX/0So;)V
    .locals 1

    .prologue
    .line 235839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235840
    iput-object p1, p0, LX/1Mf;->a:LX/0Zb;

    .line 235841
    iput-object p2, p0, LX/1Mf;->b:LX/0oz;

    .line 235842
    iput-object p3, p0, LX/1Mf;->c:LX/0kb;

    .line 235843
    iput-object p4, p0, LX/1Mf;->d:LX/0So;

    .line 235844
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1Mf;->e:Ljava/util/List;

    .line 235845
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/4iP;LX/1iW;)V
    .locals 8

    .prologue
    .line 235846
    new-instance v1, LX/6XZ;

    invoke-direct {v1}, LX/6XZ;-><init>()V

    .line 235847
    iget-object v0, p0, LX/1Mf;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v1, LX/6XZ;->a:J

    .line 235848
    iput-object p1, v1, LX/6XZ;->b:Ljava/lang/String;

    .line 235849
    iput-object p2, v1, LX/6XZ;->c:Ljava/lang/String;

    .line 235850
    iget-object v0, p0, LX/1Mf;->c:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 235851
    if-eqz v0, :cond_0

    .line 235852
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    iput v2, v1, LX/6XZ;->d:I

    .line 235853
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    iput v0, v1, LX/6XZ;->e:I

    .line 235854
    :cond_0
    if-eqz p3, :cond_2

    .line 235855
    iget-object v0, p3, LX/4iP;->mServerAddr:Ljava/net/InetAddress;

    move-object v0, v0

    .line 235856
    if-eqz v0, :cond_1

    .line 235857
    iget-object v0, p3, LX/4iP;->mServerAddr:Ljava/net/InetAddress;

    move-object v0, v0

    .line 235858
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/6XZ;->f:Ljava/lang/String;

    .line 235859
    :cond_1
    iget v0, p3, LX/4iP;->mReqHeaderCompBytes:I

    move v0, v0

    .line 235860
    iput v0, v1, LX/6XZ;->g:I

    .line 235861
    iget v0, p3, LX/4iP;->mReqBodyBytes:I

    move v0, v0

    .line 235862
    iput v0, v1, LX/6XZ;->h:I

    .line 235863
    iget v0, p3, LX/4iP;->mRspHeaderCompBytes:I

    move v0, v0

    .line 235864
    iput v0, v1, LX/6XZ;->i:I

    .line 235865
    iget v0, p3, LX/4iP;->mRspBodyCompBytes:I

    move v0, v0

    .line 235866
    iput v0, v1, LX/6XZ;->j:I

    .line 235867
    iget-wide v4, p3, LX/4iP;->mRtt:J

    move-wide v2, v4

    .line 235868
    iput-wide v2, v1, LX/6XZ;->k:J

    .line 235869
    iget-wide v4, p3, LX/4iP;->mTimeToFirstByte:J

    move-wide v2, v4

    .line 235870
    iput-wide v2, v1, LX/6XZ;->l:J

    .line 235871
    iget-wide v4, p3, LX/4iP;->mTimeToLastByte:J

    move-wide v2, v4

    .line 235872
    iput-wide v2, v1, LX/6XZ;->m:J

    .line 235873
    iget-wide v4, p3, LX/4iP;->mTotalConnect:J

    move-wide v2, v4

    .line 235874
    iput-wide v2, v1, LX/6XZ;->n:J

    .line 235875
    iget-wide v4, p3, LX/4iP;->mServerRtt:J

    move-wide v2, v4

    .line 235876
    iput-wide v2, v1, LX/6XZ;->q:J

    .line 235877
    iget v0, p3, LX/4iP;->mLocalPort:I

    move v0, v0

    .line 235878
    iput v0, v1, LX/6XZ;->r:I

    .line 235879
    iget-wide v4, p3, LX/4iP;->mServerRtx:J

    move-wide v2, v4

    .line 235880
    iput-wide v2, v1, LX/6XZ;->s:J

    .line 235881
    iget-wide v4, p3, LX/4iP;->mServerCwnd:J

    move-wide v2, v4

    .line 235882
    iput-wide v2, v1, LX/6XZ;->t:J

    .line 235883
    iget-wide v4, p3, LX/4iP;->mServerMss:J

    move-wide v2, v4

    .line 235884
    iput-wide v2, v1, LX/6XZ;->u:J

    .line 235885
    iget-wide v4, p3, LX/4iP;->mServerTotalBytesWritten:J

    move-wide v2, v4

    .line 235886
    iput-wide v2, v1, LX/6XZ;->v:J

    .line 235887
    :cond_2
    const-string v0, "done"

    .line 235888
    iget-object v2, p4, LX/1iW;->i:Ljava/lang/String;

    move-object v2, v2

    .line 235889
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, LX/6XZ;->o:Z

    .line 235890
    iget-object v0, p0, LX/1Mf;->b:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->f()D

    move-result-wide v2

    double-to-long v2, v2

    iput-wide v2, v1, LX/6XZ;->p:J

    .line 235891
    iget-object v0, p0, LX/1Mf;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235892
    iget-object v0, p0, LX/1Mf;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_5

    .line 235893
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 235894
    iget-object v0, p0, LX/1Mf;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6XZ;

    .line 235895
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 235896
    iget-wide v6, v0, LX/6XZ;->a:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235897
    iget-object v4, v0, LX/6XZ;->b:Ljava/lang/String;

    invoke-static {v4}, LX/6XZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 235898
    iget-object v4, v0, LX/6XZ;->c:Ljava/lang/String;

    invoke-static {v4}, LX/6XZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 235899
    iget v4, v0, LX/6XZ;->d:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235900
    iget v4, v0, LX/6XZ;->e:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235901
    iget-object v4, v0, LX/6XZ;->f:Ljava/lang/String;

    invoke-static {v4}, LX/6XZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 235902
    iget v4, v0, LX/6XZ;->g:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235903
    iget v4, v0, LX/6XZ;->h:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235904
    iget v4, v0, LX/6XZ;->i:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235905
    iget v4, v0, LX/6XZ;->j:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235906
    iget-wide v6, v0, LX/6XZ;->k:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235907
    iget-wide v6, v0, LX/6XZ;->l:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235908
    iget-wide v6, v0, LX/6XZ;->m:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235909
    iget-wide v6, v0, LX/6XZ;->n:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235910
    iget-boolean v4, v0, LX/6XZ;->o:Z

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235911
    iget-wide v6, v0, LX/6XZ;->p:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235912
    iget-wide v6, v0, LX/6XZ;->q:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235913
    iget v4, v0, LX/6XZ;->r:I

    invoke-virtual {v5, v4}, LX/162;->c(I)LX/162;

    .line 235914
    iget-wide v6, v0, LX/6XZ;->s:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235915
    iget-wide v6, v0, LX/6XZ;->t:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235916
    iget-wide v6, v0, LX/6XZ;->u:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235917
    iget-wide v6, v0, LX/6XZ;->v:J

    invoke-virtual {v5, v6, v7}, LX/162;->b(J)LX/162;

    .line 235918
    move-object v0, v5

    .line 235919
    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto/16 :goto_1

    .line 235920
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 235921
    :cond_4
    iget-object v0, p0, LX/1Mf;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 235922
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "mobile_requests_batch"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 235923
    const-string v2, "requests_batch"

    .line 235924
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 235925
    const-string v2, "data"

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 235926
    iget-object v1, p0, LX/1Mf;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 235927
    :cond_5
    return-void

    .line 235928
    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method
