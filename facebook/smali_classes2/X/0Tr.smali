.class public LX/0Tr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Landroid/database/sqlite/SQLiteDatabase;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Tt;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/0Tw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field public f:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63754
    const-class v0, LX/0Tr;

    sput-object v0, LX/0Tr;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Tt;",
            "LX/0Px",
            "<+",
            "LX/0Tw;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63756
    iput-object p1, p0, LX/0Tr;->b:Landroid/content/Context;

    .line 63757
    iput-object p2, p0, LX/0Tr;->c:LX/0Tt;

    .line 63758
    iput-object p3, p0, LX/0Tr;->d:LX/0Px;

    .line 63759
    iput-object p4, p0, LX/0Tr;->e:Ljava/lang/String;

    .line 63760
    return-void
.end method

.method private declared-synchronized m()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 63761
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 63762
    :goto_0
    monitor-exit p0

    return-void

    .line 63763
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    .line 63764
    const-string v0, "ensureDatabase(%s)"

    iget-object v2, p0, LX/0Tr;->e:Ljava/lang/String;

    const v3, 0x586643ca

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63765
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    .line 63766
    :goto_1
    const/16 v0, 0xa

    if-gt v1, v0, :cond_2

    .line 63767
    const/4 v0, 0x5

    if-le v1, v0, :cond_1

    .line 63768
    :try_start_2
    iget-object v0, p0, LX/0Tr;->b:Landroid/content/Context;

    iget-object v3, p0, LX/0Tr;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 63769
    :cond_1
    invoke-static {p0}, LX/0Tr;->n(LX/0Tr;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 63770
    :cond_2
    if-lez v1, :cond_3

    .line 63771
    :try_start_3
    iget-object v0, p0, LX/0Tr;->b:Landroid/content/Context;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 63772
    const-string v3, "AbstractDatabaseSupplier_RETRIES"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " attempts for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, LX/0Tr;->e:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 63773
    :cond_3
    const v0, -0x7e81e749

    :try_start_4
    invoke-static {v0}, LX/02m;->a(I)V

    .line 63774
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_4

    .line 63775
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 63776
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 63777
    :catch_0
    move-exception v0

    .line 63778
    const-wide/16 v2, 0x1e

    :try_start_5
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 63779
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_1

    .line 63780
    :catch_1
    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 63781
    :catchall_1
    move-exception v0

    const v1, 0x514b6314

    :try_start_7
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 63782
    :cond_4
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, LX/0bc;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0
.end method

.method private static n(LX/0Tr;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 63783
    new-instance v0, LX/0Yq;

    iget-object v1, p0, LX/0Tr;->b:Landroid/content/Context;

    iget-object v2, p0, LX/0Tr;->e:Ljava/lang/String;

    iget-object v3, p0, LX/0Tr;->d:LX/0Px;

    invoke-virtual {p0}, LX/0Tr;->d()I

    move-result v4

    invoke-virtual {p0}, LX/0Tr;->j()Landroid/database/DatabaseErrorHandler;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/0Yq;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;ILandroid/database/DatabaseErrorHandler;)V

    .line 63784
    :try_start_0
    invoke-virtual {v0}, LX/0Yq;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 63785
    invoke-virtual {p0}, LX/0Tr;->c()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 63786
    invoke-virtual {p0}, LX/0Tr;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->setMaximumSize(J)J

    .line 63787
    :cond_0
    :goto_0
    return-object v0

    .line 63788
    :catch_0
    sget-object v0, LX/0Tr;->a:Ljava/lang/Class;

    const-string v1, "Database %s corrupt and repair overflowed; deleting"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/0Tr;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63789
    invoke-virtual {p0}, LX/0Tr;->h()V

    .line 63790
    invoke-static {p0}, LX/0Tr;->n(LX/0Tr;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized p()V
    .locals 2

    .prologue
    .line 63791
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0Tr;->i()V

    .line 63792
    iget-object v0, p0, LX/0Tr;->b:Landroid/content/Context;

    iget-object v1, p0, LX/0Tr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63793
    monitor-exit p0

    return-void

    .line 63794
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 63795
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Tr;->c:LX/0Tt;

    invoke-interface {v0}, LX/0Tt;->a()V

    .line 63796
    invoke-direct {p0}, LX/0Tr;->m()V

    .line 63797
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, LX/0bc;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63798
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 63799
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 63800
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 63729
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 63753
    const v0, 0xc800

    return v0
.end method

.method public final e()J
    .locals 6

    .prologue
    .line 63748
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    .line 63749
    iget-object v2, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "page_count"

    invoke-static {v2, v3}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    .line 63750
    iget-object v4, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "page_size"

    invoke-static {v4, v5}, LX/0Yr;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    .line 63751
    mul-long/2addr v2, v4

    move-wide v0, v2

    .line 63752
    return-wide v0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 63745
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    .line 63746
    invoke-virtual {p0}, LX/0Tr;->g()V

    .line 63747
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 63741
    iget-object v0, p0, LX/0Tr;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/0Tr;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tw;

    .line 63742
    iget-object v3, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v3}, LX/0Tw;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63743
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63744
    :cond_0
    return-void
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63740
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized h()V
    .locals 1

    .prologue
    .line 63737
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/0Tr;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63738
    monitor-exit p0

    return-void

    .line 63739
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 63731
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 63732
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 63733
    iget-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, LX/0bc;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 63734
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Tr;->f:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63735
    :cond_0
    monitor-exit p0

    return-void

    .line 63736
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Landroid/database/DatabaseErrorHandler;
    .locals 1

    .prologue
    .line 63730
    new-instance v0, Landroid/database/DefaultDatabaseErrorHandler;

    invoke-direct {v0}, Landroid/database/DefaultDatabaseErrorHandler;-><init>()V

    return-object v0
.end method
