.class public LX/1Br;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile n:LX/1Br;


# instance fields
.field public b:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "LX/7hu;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/regex/Pattern;

.field public e:Ljava/util/regex/Pattern;

.field public f:[Ljava/lang/String;

.field public final g:LX/0ad;

.field public final h:LX/2n4;

.field private final i:LX/0kb;

.field public final j:LX/2nA;

.field public final k:LX/0W3;

.field private final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 214233
    const-class v0, LX/1Br;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Br;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/2n4;LX/0kb;LX/2nA;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/2n4;",
            "LX/0kb;",
            "LX/2nA;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 214234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214235
    iput-object p1, p0, LX/1Br;->g:LX/0ad;

    .line 214236
    iput-object p2, p0, LX/1Br;->h:LX/2n4;

    .line 214237
    iput-object p3, p0, LX/1Br;->i:LX/0kb;

    .line 214238
    iput-object p4, p0, LX/1Br;->j:LX/2nA;

    .line 214239
    iput-object p5, p0, LX/1Br;->k:LX/0W3;

    .line 214240
    iput-object p6, p0, LX/1Br;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 214241
    iput-object p7, p0, LX/1Br;->m:LX/0Or;

    .line 214242
    return-void
.end method

.method public static a(LX/0QB;)LX/1Br;
    .locals 11

    .prologue
    .line 214243
    sget-object v0, LX/1Br;->n:LX/1Br;

    if-nez v0, :cond_1

    .line 214244
    const-class v1, LX/1Br;

    monitor-enter v1

    .line 214245
    :try_start_0
    sget-object v0, LX/1Br;->n:LX/1Br;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 214246
    if-eqz v2, :cond_0

    .line 214247
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 214248
    new-instance v3, LX/1Br;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/2n4;->a(LX/0QB;)LX/2n4;

    move-result-object v5

    check-cast v5, LX/2n4;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    .line 214249
    new-instance v8, LX/2nA;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct {v8, v7}, LX/2nA;-><init>(LX/0Zb;)V

    .line 214250
    move-object v7, v8

    .line 214251
    check-cast v7, LX/2nA;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v10, 0x2fd

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/1Br;-><init>(LX/0ad;LX/2n4;LX/0kb;LX/2nA;LX/0W3;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 214252
    move-object v0, v3

    .line 214253
    sput-object v0, LX/1Br;->n:LX/1Br;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 214255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 214256
    :cond_1
    sget-object v0, LX/1Br;->n:LX/1Br;

    return-object v0

    .line 214257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 214258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/File;Ljava/io/ByteArrayOutputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 214259
    const/4 v2, 0x0

    .line 214260
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214261
    const/16 v0, 0x400

    :try_start_1
    new-array v0, v0, [B

    .line 214262
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 214263
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 214264
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 214265
    :catch_0
    move-exception v0

    .line 214266
    :goto_1
    :try_start_2
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 214267
    if-eqz v1, :cond_0

    .line 214268
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 214269
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 214270
    :goto_2
    return-void

    .line 214271
    :cond_1
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 214272
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 214273
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 214274
    :catch_1
    move-exception v0

    .line 214275
    sget-object v1, LX/1Br;->a:Ljava/lang/String;

    const-string v2, "Failed close stream"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 214276
    :catch_2
    move-exception v0

    .line 214277
    sget-object v1, LX/1Br;->a:Ljava/lang/String;

    const-string v2, "Failed close stream"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 214278
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 214279
    :goto_3
    if-eqz v1, :cond_2

    .line 214280
    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 214281
    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 214282
    :goto_4
    throw v0

    .line 214283
    :catch_3
    move-exception v1

    .line 214284
    sget-object v2, LX/1Br;->a:Ljava/lang/String;

    const-string v3, "Failed close stream"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 214285
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 214286
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static a(LX/1Br;LX/1Bt;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 214287
    iget-object v2, p0, LX/1Br;->g:LX/0ad;

    sget-short v3, LX/1Bm;->u:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 214288
    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p1, LX/1Bt;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static c(LX/1Br;)Z
    .locals 3

    .prologue
    .line 214289
    iget-object v0, p0, LX/1Br;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1C0;->h:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static d(LX/1Br;)I
    .locals 8

    .prologue
    const/4 v0, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v4, 0x0

    .line 214290
    iget-object v5, p0, LX/1Br;->i:LX/0kb;

    invoke-virtual {v5}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v5

    .line 214291
    iget-object v6, p0, LX/1Br;->i:LX/0kb;

    invoke-virtual {v6}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v6

    .line 214292
    const-string v7, "wifi"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 214293
    :goto_0
    return v0

    .line 214294
    :cond_0
    const-string v7, "mobile"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 214295
    const/4 v5, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_1
    move v0, v5

    :goto_1
    packed-switch v0, :pswitch_data_0

    move v0, v4

    .line 214296
    goto :goto_0

    .line 214297
    :sswitch_0
    const-string v0, "cdma"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    goto :goto_1

    :sswitch_1
    const-string v0, "cdma - 1xrtt"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "edge"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_1

    :sswitch_3
    const-string v0, "gprs"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :sswitch_4
    const-string v0, "iden"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    goto :goto_1

    :sswitch_5
    const-string v7, "cdma - evdo rev. 0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :sswitch_6
    const-string v0, "cdma - evdo rev. a"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v0, "hspa"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v0, "umts"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v0, "cdma - ehrpd"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_a
    const-string v0, "hsdpa"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_b
    const-string v0, "hsupa"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    goto :goto_1

    :sswitch_c
    const-string v0, "cdma - evdo rev. b"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "hspa+"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "lte"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xe

    goto/16 :goto_1

    :pswitch_0
    move v0, v1

    .line 214298
    goto/16 :goto_0

    :pswitch_1
    move v0, v2

    .line 214299
    goto/16 :goto_0

    :pswitch_2
    move v0, v3

    .line 214300
    goto/16 :goto_0

    :cond_2
    move v0, v4

    .line 214301
    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x29a01e73 -> :sswitch_1
        -0x26ca9ebb -> :sswitch_9
        0x1a3dd -> :sswitch_e
        0x2e85b5 -> :sswitch_0
        0x2f6dbd -> :sswitch_2
        0x3084ea -> :sswitch_3
        0x31043c -> :sswitch_7
        0x313f04 -> :sswitch_4
        0x36d717 -> :sswitch_8
        0x5ef586a -> :sswitch_a
        0x5ef836f -> :sswitch_d
        0x5ef983b -> :sswitch_b
        0x6f81de41 -> :sswitch_5
        0x6f81de72 -> :sswitch_6
        0x6f81de73 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 214302
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "utf-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 214303
    iget-object v1, p0, LX/1Br;->f:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 214304
    iget-object v2, p0, LX/1Br;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 214305
    if-eqz p1, :cond_1

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 214306
    const/4 v0, 0x1

    .line 214307
    :cond_0
    return v0

    .line 214308
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
