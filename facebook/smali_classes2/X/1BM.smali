.class public LX/1BM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BN;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1BM;


# instance fields
.field public final a:LX/1BF;


# direct methods
.method public constructor <init>(LX/1BF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 213028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213029
    iput-object p1, p0, LX/1BM;->a:LX/1BF;

    .line 213030
    return-void
.end method

.method public static a(LX/0QB;)LX/1BM;
    .locals 4

    .prologue
    .line 213031
    sget-object v0, LX/1BM;->b:LX/1BM;

    if-nez v0, :cond_1

    .line 213032
    const-class v1, LX/1BM;

    monitor-enter v1

    .line 213033
    :try_start_0
    sget-object v0, LX/1BM;->b:LX/1BM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 213034
    if-eqz v2, :cond_0

    .line 213035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 213036
    new-instance p0, LX/1BM;

    invoke-static {v0}, LX/1BF;->b(LX/0QB;)LX/1BF;

    move-result-object v3

    check-cast v3, LX/1BF;

    invoke-direct {p0, v3}, LX/1BM;-><init>(LX/1BF;)V

    .line 213037
    move-object v0, p0

    .line 213038
    sput-object v0, LX/1BM;->b:LX/1BM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 213040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213041
    :cond_1
    sget-object v0, LX/1BM;->b:LX/1BM;

    return-object v0

    .line 213042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 213043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/photos/prefetch/PrefetchParams;Z)V
    .locals 1

    .prologue
    .line 213044
    monitor-enter p0

    .line 213045
    :try_start_0
    iget-object v0, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    .line 213046
    iget-object p1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, p1

    .line 213047
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 213048
    invoke-virtual {p0, v0, p2}, LX/1BM;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213049
    monitor-exit p0

    return-void

    .line 213050
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 213051
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid feed unit id"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 213052
    const-string v0, "Image url cannot be null"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213053
    iget-object v0, p0, LX/1BM;->a:LX/1BF;

    invoke-virtual {v0, p1, p2}, LX/1BF;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213054
    monitor-exit p0

    return-void

    .line 213055
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 213056
    monitor-enter p0

    .line 213057
    :try_start_0
    iget-object v0, p0, LX/1BM;->a:LX/1BF;

    invoke-virtual {v0, p1, p2}, LX/1BF;->b(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213058
    monitor-exit p0

    return-void

    .line 213059
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
