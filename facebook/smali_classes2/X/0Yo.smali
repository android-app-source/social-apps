.class public LX/0Yo;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0ac;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0ac;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82471
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0ac;
    .locals 12

    .prologue
    .line 82472
    sget-object v0, LX/0Yo;->a:LX/0ac;

    if-nez v0, :cond_1

    .line 82473
    const-class v1, LX/0Yo;

    monitor-enter v1

    .line 82474
    :try_start_0
    sget-object v0, LX/0Yo;->a:LX/0ac;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82475
    if-eqz v2, :cond_0

    .line 82476
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 82477
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0ZT;->b(LX/0QB;)LX/0ZT;

    move-result-object v4

    check-cast v4, LX/0ZT;

    invoke-static {v0}, LX/0ZU;->b(LX/0QB;)LX/0ZU;

    move-result-object v5

    check-cast v5, LX/0ZU;

    invoke-static {v0}, LX/0ZW;->b(LX/0QB;)LX/0ZW;

    move-result-object v6

    check-cast v6, LX/0ZW;

    invoke-static {v0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v7

    check-cast v7, LX/0VT;

    invoke-static {v0}, LX/0aI;->a(LX/0QB;)LX/5og;

    move-result-object v8

    check-cast v8, LX/5og;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0aJ;->a(LX/0QB;)LX/0aJ;

    move-result-object v10

    check-cast v10, LX/0aJ;

    const/16 v11, 0x1034

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v3 .. v11}, LX/0aK;->a(Landroid/content/Context;LX/0ZT;LX/0ZU;LX/0ZW;LX/0VT;LX/5og;LX/03V;LX/0aJ;LX/0Ot;)LX/0ac;

    move-result-object v3

    move-object v0, v3

    .line 82478
    sput-object v0, LX/0Yo;->a:LX/0ac;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82479
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82480
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82481
    :cond_1
    sget-object v0, LX/0Yo;->a:LX/0ac;

    return-object v0

    .line 82482
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 82484
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0ZT;->b(LX/0QB;)LX/0ZT;

    move-result-object v1

    check-cast v1, LX/0ZT;

    invoke-static {p0}, LX/0ZU;->b(LX/0QB;)LX/0ZU;

    move-result-object v2

    check-cast v2, LX/0ZU;

    invoke-static {p0}, LX/0ZW;->b(LX/0QB;)LX/0ZW;

    move-result-object v3

    check-cast v3, LX/0ZW;

    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v4

    check-cast v4, LX/0VT;

    invoke-static {p0}, LX/0aI;->a(LX/0QB;)LX/5og;

    move-result-object v5

    check-cast v5, LX/5og;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0aJ;->a(LX/0QB;)LX/0aJ;

    move-result-object v7

    check-cast v7, LX/0aJ;

    const/16 v8, 0x1034

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/0aK;->a(Landroid/content/Context;LX/0ZT;LX/0ZU;LX/0ZW;LX/0VT;LX/5og;LX/03V;LX/0aJ;LX/0Ot;)LX/0ac;

    move-result-object v0

    return-object v0
.end method
