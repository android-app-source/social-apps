.class public final LX/0dK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0dM;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0dM;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 90214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90215
    iput-object p1, p0, LX/0dK;->a:LX/0QB;

    .line 90216
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 90217
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0dK;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 90218
    packed-switch p2, :pswitch_data_0

    .line 90219
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90220
    :pswitch_0
    invoke-static {p1}, LX/0dL;->a(LX/0QB;)LX/0dL;

    move-result-object v0

    .line 90221
    :goto_0
    return-object v0

    .line 90222
    :pswitch_1
    new-instance v0, LX/0dT;

    const/16 v1, 0xb43

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0dT;-><init>(LX/0Ot;)V

    .line 90223
    move-object v0, v0

    .line 90224
    goto :goto_0

    .line 90225
    :pswitch_2
    new-instance v0, LX/0dZ;

    const/16 v1, 0x2516

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0dZ;-><init>(LX/0Ot;)V

    .line 90226
    move-object v0, v0

    .line 90227
    goto :goto_0

    .line 90228
    :pswitch_3
    invoke-static {p1}, LX/0da;->a(LX/0QB;)LX/0da;

    move-result-object v0

    goto :goto_0

    .line 90229
    :pswitch_4
    new-instance v0, LX/0de;

    const/16 v1, 0x13ba

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0de;-><init>(LX/0Ot;)V

    .line 90230
    move-object v0, v0

    .line 90231
    goto :goto_0

    .line 90232
    :pswitch_5
    invoke-static {p1}, LX/0dg;->a(LX/0QB;)LX/0dg;

    move-result-object v0

    goto :goto_0

    .line 90233
    :pswitch_6
    invoke-static {p1}, LX/0di;->b(LX/0QB;)LX/0di;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 90234
    const/4 v0, 0x7

    return v0
.end method
