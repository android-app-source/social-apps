.class public LX/18H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 206223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206224
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 206219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 206220
    invoke-static {v1}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206221
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 206222
    :cond_0
    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 206216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->v()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 206217
    :cond_0
    const/4 v0, 0x0

    .line 206218
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;)Z
    .locals 1

    .prologue
    .line 206215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 206209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 206210
    invoke-static {v2}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 206211
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 206212
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 206213
    goto :goto_0

    .line 206214
    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;)Z
    .locals 3

    .prologue
    .line 206207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 206208
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)Z
    .locals 1

    .prologue
    .line 206206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLocation;)Z
    .locals 4

    .prologue
    .line 206205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4056800000000000L    # 90.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 206198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 206199
    :goto_0
    return v0

    .line 206200
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;

    .line 206201
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 206202
    goto :goto_0

    .line 206203
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 206204
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 206195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->y()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 206196
    :cond_0
    const/4 v0, 0x0

    .line 206197
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageInfo;)Z
    .locals 1

    .prologue
    .line 206194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 206187
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 206188
    :goto_0
    return v0

    .line 206189
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;

    .line 206190
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 206191
    goto :goto_0

    .line 206192
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 206193
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;)Z
    .locals 2

    .prologue
    .line 206182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 206183
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 206225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206226
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 206227
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 206228
    goto :goto_0

    .line 206229
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    .line 206230
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 206231
    goto :goto_0

    .line 206232
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 206233
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)Z
    .locals 1

    .prologue
    .line 206184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 206185
    :cond_0
    const/4 v0, 0x0

    .line 206186
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;)Z
    .locals 1

    .prologue
    .line 206181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLUser;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;)Z
    .locals 2

    .prologue
    .line 206179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;->k()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 206180
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)Z
    .locals 1

    .prologue
    .line 206178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLUser;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z
    .locals 1

    .prologue
    .line 206177
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 206169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 206170
    :goto_0
    return v0

    .line 206171
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    .line 206172
    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 206173
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 206174
    goto :goto_0

    .line 206175
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 206176
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)Z
    .locals 2

    .prologue
    .line 206165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v0

    .line 206166
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->T()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 206167
    :cond_0
    const/4 v0, 0x0

    .line 206168
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z
    .locals 1

    .prologue
    .line 206160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    .line 206161
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->l()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->k()Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object p0

    if-nez p0, :cond_2

    .line 206162
    :cond_0
    const/4 p0, 0x0

    .line 206163
    :goto_0
    move v0, p0

    .line 206164
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;)Z
    .locals 2

    .prologue
    .line 206158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    .line 206159
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 206156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 206157
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSponsoredData;)Z
    .locals 1

    .prologue
    .line 206155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)Z
    .locals 4

    .prologue
    .line 206143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v0

    const/4 v2, 0x0

    .line 206144
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->j()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    .line 206145
    :goto_0
    move v0, v1

    .line 206146
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 206147
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStructuredSurvey;->l()Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestionsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v3

    .line 206148
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 206149
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;

    .line 206150
    if-eqz v1, :cond_5

    .line 206151
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->k()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->l()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object p0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-eq p0, v0, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->l()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object p0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne p0, v0, :cond_7

    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStructuredSurveyQuestion;->m()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    const/4 v0, 0x5

    if-gt p0, v0, :cond_7

    const/4 p0, 0x1

    :goto_2
    move v1, p0

    .line 206152
    if-nez v1, :cond_3

    :cond_5
    move v1, v2

    .line 206153
    goto :goto_0

    .line 206154
    :cond_6
    const/4 v1, 0x1

    goto :goto_0

    :cond_7
    const/4 p0, 0x0

    goto :goto_2
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLUser;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 206137
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 206138
    :goto_0
    return v0

    .line 206139
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206140
    :catch_0
    move-exception v2

    .line 206141
    const-string v3, "IsValidUtil"

    const-string v4, "User id not a long: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v1

    invoke-static {v3, v2, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 206142
    goto :goto_0
.end method
