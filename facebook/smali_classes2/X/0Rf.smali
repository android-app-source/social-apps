.class public abstract LX/0Rf;
.super LX/0Py;
.source ""

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Py",
        "<TE;>;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final CUTOFF:I = 0x2ccccccc

.field private static final DESIRED_LOAD_FACTOR:D = 0.7

.field public static final MAX_TABLE_SIZE:I = 0x40000000


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60277
    invoke-direct {p0}, LX/0Py;-><init>()V

    return-void
.end method

.method public static synthetic access$000(I[Ljava/lang/Object;)LX/0Rf;
    .locals 1

    .prologue
    .line 60278
    invoke-static {p0, p1}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static builder()LX/0cA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0cA",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60279
    new-instance v0, LX/0cA;

    invoke-direct {v0}, LX/0cA;-><init>()V

    return-object v0
.end method

.method public static chooseTableSize(I)I
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 60280
    const v0, 0x2ccccccc

    if-ge p0, v0, :cond_0

    .line 60281
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 60282
    :goto_0
    int-to-double v2, v0

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    int-to-double v4, p0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 60283
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60284
    :cond_0
    if-ge p0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v2, "collection too large"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move v0, v1

    .line 60285
    :cond_1
    return v0

    .line 60286
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static varargs construct(I[Ljava/lang/Object;)LX/0Rf;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I[",
            "Ljava/lang/Object;",
            ")",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 60287
    packed-switch p0, :pswitch_data_0

    .line 60288
    invoke-static {p0}, LX/0Rf;->chooseTableSize(I)I

    move-result v5

    .line 60289
    new-array v6, v5, [Ljava/lang/Object;

    .line 60290
    add-int/lit8 v7, v5, -0x1

    move v3, v4

    move v1, v4

    move v2, v4

    .line 60291
    :goto_0
    if-ge v3, p0, :cond_1

    .line 60292
    aget-object v0, p1, v3

    invoke-static {v0, v3}, LX/0P8;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v8

    .line 60293
    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v9

    .line 60294
    invoke-static {v9}, LX/0PC;->a(I)I

    move-result v0

    .line 60295
    :goto_1
    and-int v10, v0, v7

    .line 60296
    aget-object v11, v6, v10

    .line 60297
    if-nez v11, :cond_0

    .line 60298
    add-int/lit8 v0, v1, 0x1

    aput-object v8, p1, v1

    .line 60299
    aput-object v8, v6, v10

    .line 60300
    add-int v1, v2, v9

    .line 60301
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 60302
    :pswitch_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 60303
    :goto_3
    return-object v0

    .line 60304
    :pswitch_1
    aget-object v0, p1, v4

    .line 60305
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_3

    .line 60306
    :cond_0
    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 60307
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 60308
    :cond_1
    const/4 v0, 0x0

    invoke-static {p1, v1, p0, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 60309
    const/4 v0, 0x1

    if-ne v1, v0, :cond_2

    .line 60310
    aget-object v1, p1, v4

    .line 60311
    new-instance v0, LX/0dV;

    invoke-direct {v0, v1, v2}, LX/0dV;-><init>(Ljava/lang/Object;I)V

    goto :goto_3

    .line 60312
    :cond_2
    invoke-static {v1}, LX/0Rf;->chooseTableSize(I)I

    move-result v0

    if-eq v5, v0, :cond_3

    .line 60313
    invoke-static {v1, p1}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_3

    .line 60314
    :cond_3
    array-length v0, p1

    if-ge v1, v0, :cond_4

    invoke-static {p1, v1}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 60315
    :cond_4
    new-instance v0, LX/0Re;

    invoke-direct {v0, p1, v2, v6, v7}, LX/0Re;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V

    goto :goto_3

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static copyOf(Ljava/lang/Iterable;)LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60316
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Iterator;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf(Ljava/util/Collection;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60317
    instance-of v0, p0, LX/0Rf;

    if-eqz v0, :cond_0

    instance-of v0, p0, LX/0dW;

    if-nez v0, :cond_0

    move-object v0, p0

    .line 60318
    check-cast v0, LX/0Rf;

    .line 60319
    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v1

    if-nez v1, :cond_1

    .line 60320
    :goto_0
    return-object v0

    .line 60321
    :cond_0
    instance-of v0, p0, Ljava/util/EnumSet;

    if-eqz v0, :cond_1

    .line 60322
    check-cast p0, Ljava/util/EnumSet;

    invoke-static {p0}, LX/0Rf;->copyOfEnumSet(Ljava/util/EnumSet;)LX/0Rf;

    move-result-object v0

    goto :goto_0

    .line 60323
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 60324
    array-length v1, v0

    invoke-static {v1, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf(Ljava/util/Iterator;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60325
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60326
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 60327
    :goto_0
    return-object v0

    .line 60328
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 60329
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 60330
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0

    .line 60331
    :cond_1
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyOf([Ljava/lang/Object;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60332
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 60333
    array-length v1, p0

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    :goto_0
    return-object v0

    .line 60334
    :pswitch_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 60335
    goto :goto_0

    .line 60336
    :pswitch_1
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static copyOfEnumSet(Ljava/util/EnumSet;)LX/0Rf;
    .locals 1

    .prologue
    .line 60337
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, LX/1Y6;->a(Ljava/util/EnumSet;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static of()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60275
    sget-object v0, LX/0Re;->a:LX/0Re;

    return-object v0
.end method

.method public static of(Ljava/lang/Object;)LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 60276
    new-instance v0, LX/0dV;

    invoke-direct {v0, p0}, LX/0dV;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 60252
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v2, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 60253
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v2, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 60254
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v2, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x5

    .line 60255
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v2, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static varargs of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;[TE;)",
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 60256
    array-length v0, p6

    add-int/lit8 v0, v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 60257
    aput-object p0, v0, v3

    .line 60258
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 60259
    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 60260
    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 60261
    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 60262
    const/4 v1, 0x5

    aput-object p5, v0, v1

    .line 60263
    const/4 v1, 0x6

    array-length v2, p6

    invoke-static {p6, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60264
    array-length v1, v0

    invoke-static {v1, v0}, LX/0Rf;->construct(I[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 60265
    if-ne p1, p0, :cond_0

    .line 60266
    const/4 v0, 0x1

    .line 60267
    :goto_0
    return v0

    .line 60268
    :cond_0
    instance-of v0, p1, LX/0Rf;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0Rf;->isHashCodeFast()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isHashCodeFast()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/0Rf;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 60269
    const/4 v0, 0x0

    goto :goto_0

    .line 60270
    :cond_1
    invoke-static {p0, p1}, LX/0RA;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 60271
    invoke-static {p0}, LX/0RA;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public isHashCodeFast()Z
    .locals 1

    .prologue
    .line 60272
    const/4 v0, 0x0

    return v0
.end method

.method public abstract iterator()LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 60273
    invoke-virtual {p0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 60274
    new-instance v0, LX/4yb;

    invoke-virtual {p0}, LX/0Rf;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4yb;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method
