.class public LX/0sM;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151648
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 151649
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0Or;LX/AnM;LX/0fJ;)Landroid/content/Intent;
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/feed/annotations/OpenAppToFeedIntent;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/AnM;",
            "LX/0fJ;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 151650
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {p2, p0, v0}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 151651
    const-string v1, "target_tab_name"

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v2}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151652
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 151653
    const-string v2, "tabbar_target_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 151654
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 151655
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 151656
    invoke-interface {p3, v1}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 151657
    return-object v1
.end method

.method public static a()Ljava/lang/Integer;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/widget/titlebar/TitleBarResourceId;
    .end annotation

    .prologue
    .line 151658
    const v0, 0x7f030606

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/00G;Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/katana/annotations/ForUserAgentOptions;
    .end annotation

    .prologue
    .line 151659
    new-instance v0, LX/0sN;

    sget-object v1, LX/0Pw;->DASH:LX/0Pw;

    invoke-virtual {v1}, LX/0Pw;->getProcessName()LX/00G;

    move-result-object v1

    invoke-direct {v0, p1, v1, p2, p0}, LX/0sN;-><init>(LX/00G;LX/00G;Landroid/content/pm/ApplicationInfo;Landroid/content/Context;)V

    .line 151660
    invoke-virtual {v0}, LX/0sN;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 151661
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/katana/activity/ImmersiveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 151662
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/katana/activity/react/ImmersiveReactActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static d(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 151663
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 151664
    return-void
.end method
