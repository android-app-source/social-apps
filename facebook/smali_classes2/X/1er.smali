.class public LX/1er;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1er;


# instance fields
.field private final a:LX/1LD;

.field private final b:LX/1L1;

.field public c:I


# direct methods
.method public constructor <init>(LX/1L1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289208
    const/4 v0, -0x1

    iput v0, p0, LX/1er;->c:I

    .line 289209
    new-instance v0, LX/1es;

    invoke-direct {v0, p0}, LX/1es;-><init>(LX/1er;)V

    iput-object v0, p0, LX/1er;->a:LX/1LD;

    .line 289210
    iput-object p1, p0, LX/1er;->b:LX/1L1;

    .line 289211
    return-void
.end method

.method public static a(LX/0QB;)LX/1er;
    .locals 4

    .prologue
    .line 289212
    sget-object v0, LX/1er;->d:LX/1er;

    if-nez v0, :cond_1

    .line 289213
    const-class v1, LX/1er;

    monitor-enter v1

    .line 289214
    :try_start_0
    sget-object v0, LX/1er;->d:LX/1er;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 289215
    if-eqz v2, :cond_0

    .line 289216
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 289217
    new-instance p0, LX/1er;

    invoke-static {v0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v3

    check-cast v3, LX/1L1;

    invoke-direct {p0, v3}, LX/1er;-><init>(LX/1L1;)V

    .line 289218
    move-object v0, p0

    .line 289219
    sput-object v0, LX/1er;->d:LX/1er;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289220
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 289221
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 289222
    :cond_1
    sget-object v0, LX/1er;->d:LX/1er;

    return-object v0

    .line 289223
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 289224
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 289225
    iget-object v0, p0, LX/1er;->b:LX/1L1;

    iget-object v1, p0, LX/1er;->a:LX/1LD;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 289226
    return-void
.end method
