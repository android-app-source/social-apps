.class public LX/1nq;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0aq;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/text/Layout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/1nr;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private c:Landroid/text/Layout;

.field public d:LX/1WA;

.field public e:Z

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 317485
    new-instance v0, LX/0aq;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    sput-object v0, LX/1nq;->a:LX/0aq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 317475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317476
    new-instance v0, LX/1nr;

    invoke-direct {v0}, LX/1nr;-><init>()V

    iput-object v0, p0, LX/1nq;->b:LX/1nr;

    .line 317477
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317478
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nq;->e:Z

    .line 317479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1nq;->f:Z

    .line 317480
    return-void
.end method


# virtual methods
.method public final a(F)LX/1nq;
    .locals 1

    .prologue
    .line 317467
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget v0, v0, LX/1nr;->g:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 317468
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput p1, v0, LX/1nr;->g:F

    .line 317469
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317470
    :cond_0
    return-object p0
.end method

.method public final a(FFFI)LX/1nq;
    .locals 1
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 317481
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317482
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 317483
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317484
    return-object p0
.end method

.method public final a(I)LX/1nq;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317486
    if-gtz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/1nq;->a(II)LX/1nq;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(II)LX/1nq;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 317487
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget v0, v0, LX/1nr;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget v0, v0, LX/1nr;->c:I

    if-eq v0, p2, :cond_1

    .line 317488
    :cond_0
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput p1, v0, LX/1nr;->b:I

    .line 317489
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput p2, v0, LX/1nr;->c:I

    .line 317490
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317491
    :cond_1
    return-object p0
.end method

.method public final a(LX/0zr;)LX/1nq;
    .locals 1

    .prologue
    .line 317492
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->m:LX/0zr;

    if-eq v0, p1, :cond_0

    .line 317493
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object p1, v0, LX/1nr;->m:LX/0zr;

    .line 317494
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317495
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/content/res/ColorStateList;)LX/1nq;
    .locals 2

    .prologue
    .line 317496
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317497
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object p1, v0, LX/1nr;->e:Landroid/content/res/ColorStateList;

    .line 317498
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v1, v0, LX/1nr;->a:Landroid/text/TextPaint;

    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->e:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 317499
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317500
    return-object p0

    .line 317501
    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Typeface;)LX/1nq;
    .locals 1

    .prologue
    .line 317502
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 317503
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317504
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 317505
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317506
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/text/Layout$Alignment;)LX/1nq;
    .locals 1

    .prologue
    .line 317507
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->l:Landroid/text/Layout$Alignment;

    if-eq v0, p1, :cond_0

    .line 317508
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object p1, v0, LX/1nr;->l:Landroid/text/Layout$Alignment;

    .line 317509
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317510
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;
    .locals 1

    .prologue
    .line 317511
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    if-eq v0, p1, :cond_0

    .line 317512
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object p1, v0, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    .line 317513
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317514
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/1nq;
    .locals 1

    .prologue
    .line 317471
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->d:Ljava/lang/CharSequence;

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->d:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317472
    :cond_0
    :goto_0
    return-object p0

    .line 317473
    :cond_1
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object p1, v0, LX/1nr;->d:Ljava/lang/CharSequence;

    .line 317474
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    goto :goto_0
.end method

.method public final a(Z)LX/1nq;
    .locals 1

    .prologue
    .line 317392
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-boolean v0, v0, LX/1nr;->h:Z

    if-eq v0, p1, :cond_0

    .line 317393
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-boolean p1, v0, LX/1nr;->h:Z

    .line 317394
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317395
    :cond_0
    return-object p0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 317396
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 317397
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    return v0
.end method

.method public final b(F)LX/1nq;
    .locals 1

    .prologue
    .line 317398
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget v0, v0, LX/1nr;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 317399
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput p1, v0, LX/1nr;->f:F

    .line 317400
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317401
    :cond_0
    return-object p0
.end method

.method public final b(I)LX/1nq;
    .locals 2

    .prologue
    .line 317402
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    int-to-float v1, p1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 317403
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317404
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 317405
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317406
    :cond_0
    return-object p0
.end method

.method public final b(Z)LX/1nq;
    .locals 1

    .prologue
    .line 317407
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-boolean v0, v0, LX/1nr;->j:Z

    if-eq v0, p1, :cond_0

    .line 317408
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-boolean p1, v0, LX/1nr;->j:Z

    .line 317409
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317410
    :cond_0
    return-object p0
.end method

.method public final c(I)LX/1nq;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 317411
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317412
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput-object v1, v0, LX/1nr;->e:Landroid/content/res/ColorStateList;

    .line 317413
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 317414
    iput-object v1, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317415
    return-object p0
.end method

.method public final c(Z)LX/1nq;
    .locals 0

    .prologue
    .line 317416
    iput-boolean p1, p0, LX/1nq;->e:Z

    .line 317417
    return-object p0
.end method

.method public final c()Landroid/text/Layout;
    .locals 20

    .prologue
    .line 317428
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/1nq;->e:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->c:Landroid/text/Layout;

    if-eqz v2, :cond_1

    .line 317429
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->c:Landroid/text/Layout;

    .line 317430
    :cond_0
    :goto_0
    return-object v2

    .line 317431
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 317432
    const/4 v2, 0x0

    goto :goto_0

    .line 317433
    :cond_2
    const/4 v2, 0x0

    .line 317434
    const/4 v3, -0x1

    .line 317435
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/1nq;->e:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1nq;->b:LX/1nr;

    iget-object v4, v4, LX/1nr;->d:Ljava/lang/CharSequence;

    instance-of v4, v4, Landroid/text/Spannable;

    if-eqz v4, :cond_a

    .line 317436
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    check-cast v2, Landroid/text/Spannable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1nq;->b:LX/1nr;

    iget-object v5, v5, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    const-class v6, Landroid/text/style/ClickableSpan;

    invoke-interface {v2, v4, v5, v6}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/ClickableSpan;

    .line 317437
    array-length v2, v2

    if-lez v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move/from16 v18, v2

    .line 317438
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/1nq;->e:Z

    if-eqz v2, :cond_3

    if-nez v18, :cond_3

    .line 317439
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v2}, LX/1nr;->hashCode()I

    move-result v3

    .line 317440
    sget-object v2, LX/1nq;->a:LX/0aq;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/text/Layout;

    .line 317441
    if-nez v2, :cond_0

    :cond_3
    move/from16 v19, v3

    .line 317442
    const/4 v8, 0x0

    .line 317443
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-boolean v2, v2, LX/1nr;->j:Z

    if-eqz v2, :cond_6

    const/16 v16, 0x1

    .line 317444
    :goto_3
    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_4

    .line 317445
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-static {v2, v3}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;

    move-result-object v8

    .line 317446
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget v2, v2, LX/1nr;->c:I

    packed-switch v2, :pswitch_data_0

    .line 317447
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected measure mode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1nq;->b:LX/1nr;

    iget v4, v4, LX/1nr;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 317448
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 317449
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget v0, v2, LX/1nr;->k:I

    move/from16 v16, v0

    goto :goto_3

    .line 317450
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-static {v2, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v4, v2

    .line 317451
    :goto_4
    if-eqz v8, :cond_8

    .line 317452
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->a:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1nq;->b:LX/1nr;

    iget-object v5, v5, LX/1nr;->l:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1nq;->b:LX/1nr;

    iget v6, v6, LX/1nr;->f:F

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1nq;->b:LX/1nr;

    iget v7, v7, LX/1nr;->g:F

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1nq;->b:LX/1nr;

    iget-boolean v9, v9, LX/1nr;->h:Z

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1nq;->b:LX/1nr;

    iget-object v10, v10, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    move v11, v4

    invoke-static/range {v2 .. v11}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;

    move-result-object v2

    .line 317453
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/1nq;->e:Z

    if-eqz v3, :cond_7

    if-nez v18, :cond_7

    .line 317454
    move-object/from16 v0, p0

    iput-object v2, v0, LX/1nq;->c:Landroid/text/Layout;

    .line 317455
    sget-object v3, LX/1nq;->a:LX/0aq;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317456
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    const/4 v4, 0x1

    iput-boolean v4, v3, LX/1nr;->n:Z

    .line 317457
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/1nq;->f:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->d:LX/1WA;

    if-eqz v3, :cond_0

    .line 317458
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->d:LX/1WA;

    invoke-interface {v3, v2}, LX/1WA;->a(Landroid/text/Layout;)V

    goto/16 :goto_0

    .line 317459
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget v4, v2, LX/1nr;->b:I

    goto :goto_4

    .line 317460
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->a:Landroid/text/TextPaint;

    invoke-static {v2, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget v3, v3, LX/1nr;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto/16 :goto_4

    .line 317461
    :catch_0
    move-exception v2

    .line 317462
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->d:Ljava/lang/CharSequence;

    instance-of v3, v3, Ljava/lang/String;

    if-nez v3, :cond_9

    .line 317463
    const-string v3, "TextLayoutBuilder"

    const-string v5, "Hit bug #35412, retrying with Spannables removed"

    invoke-static {v3, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 317464
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1nq;->b:LX/1nr;

    iget-object v3, v3, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    .line 317465
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v5, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v2, v2, LX/1nr;->d:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v8, v2, LX/1nr;->a:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v10, v2, LX/1nr;->l:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget v11, v2, LX/1nr;->f:F

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget v12, v2, LX/1nr;->g:F

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-boolean v13, v2, LX/1nr;->h:Z

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v14, v2, LX/1nr;->i:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1nq;->b:LX/1nr;

    iget-object v0, v2, LX/1nr;->m:LX/0zr;

    move-object/from16 v17, v0

    move v9, v4

    move v15, v4

    invoke-static/range {v5 .. v17}, LX/1o6;->a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;IILX/0zr;)Landroid/text/StaticLayout;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto/16 :goto_5

    .line 317466
    :cond_9
    throw v2

    :cond_a
    move/from16 v18, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final d(I)LX/1nq;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 317418
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    iget v0, v0, Landroid/text/TextPaint;->linkColor:I

    if-eq v0, p1, :cond_0

    .line 317419
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    invoke-virtual {v0}, LX/1nr;->a()V

    .line 317420
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget-object v0, v0, LX/1nr;->a:Landroid/text/TextPaint;

    iput p1, v0, Landroid/text/TextPaint;->linkColor:I

    .line 317421
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317422
    :cond_0
    return-object p0
.end method

.method public final e(I)LX/1nq;
    .locals 1

    .prologue
    .line 317423
    invoke-static {p1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1nq;->a(Landroid/graphics/Typeface;)LX/1nq;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)LX/1nq;
    .locals 1

    .prologue
    .line 317424
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iget v0, v0, LX/1nr;->k:I

    if-eq v0, p1, :cond_0

    .line 317425
    iget-object v0, p0, LX/1nq;->b:LX/1nr;

    iput p1, v0, LX/1nr;->k:I

    .line 317426
    const/4 v0, 0x0

    iput-object v0, p0, LX/1nq;->c:Landroid/text/Layout;

    .line 317427
    :cond_0
    return-object p0
.end method
