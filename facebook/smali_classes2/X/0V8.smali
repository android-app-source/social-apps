.class public LX/0V8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static a:LX/0V8;

.field private static final b:J


# instance fields
.field public volatile c:Landroid/os/StatFs;

.field private volatile d:Ljava/io/File;

.field public volatile e:Landroid/os/StatFs;

.field private volatile f:Ljava/io/File;

.field private g:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field

.field private final h:Ljava/util/concurrent/locks/Lock;

.field private volatile i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 67462
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/0V8;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67457
    iput-object v0, p0, LX/0V8;->c:Landroid/os/StatFs;

    .line 67458
    iput-object v0, p0, LX/0V8;->e:Landroid/os/StatFs;

    .line 67459
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0V8;->i:Z

    .line 67460
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    .line 67461
    return-void
.end method

.method public static declared-synchronized a()LX/0V8;
    .locals 2

    .prologue
    .line 67452
    const-class v1, LX/0V8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/0V8;->a:LX/0V8;

    if-nez v0, :cond_0

    .line 67453
    new-instance v0, LX/0V8;

    invoke-direct {v0}, LX/0V8;-><init>()V

    sput-object v0, LX/0V8;->a:LX/0V8;

    .line 67454
    :cond_0
    sget-object v0, LX/0V8;->a:LX/0V8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67455
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/os/StatFs;Ljava/io/File;)Landroid/os/StatFs;
    .locals 2
    .param p0    # Landroid/os/StatFs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 67441
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object p0, v0

    .line 67442
    :goto_0
    return-object p0

    .line 67443
    :cond_1
    if-nez p0, :cond_2

    .line 67444
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 67445
    new-instance p0, Landroid/os/StatFs;

    invoke-direct {p0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    move-object p0, p0

    .line 67446
    goto :goto_0

    .line 67447
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 67448
    :catch_0
    move-object p0, v0

    .line 67449
    goto :goto_0

    .line 67450
    :catch_1
    move-exception v0

    .line 67451
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static c(LX/0V8;)V
    .locals 2

    .prologue
    .line 67463
    iget-boolean v0, p0, LX/0V8;->i:Z

    if-nez v0, :cond_1

    .line 67464
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 67465
    :try_start_0
    iget-boolean v0, p0, LX/0V8;->i:Z

    if-nez v0, :cond_0

    .line 67466
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0V8;->d:Ljava/io/File;

    .line 67467
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LX/0V8;->f:Ljava/io/File;

    .line 67468
    invoke-direct {p0}, LX/0V8;->e()V

    .line 67469
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0V8;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67470
    :cond_0
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 67471
    :cond_1
    return-void

    .line 67472
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static d(LX/0V8;)V
    .locals 4

    .prologue
    .line 67435
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67436
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/0V8;->g:J

    sub-long/2addr v0, v2

    sget-wide v2, LX/0V8;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 67437
    invoke-direct {p0}, LX/0V8;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67438
    :cond_0
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 67439
    :cond_1
    return-void

    .line 67440
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private e()V
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 67431
    iget-object v0, p0, LX/0V8;->c:Landroid/os/StatFs;

    iget-object v1, p0, LX/0V8;->d:Ljava/io/File;

    invoke-static {v0, v1}, LX/0V8;->a(Landroid/os/StatFs;Ljava/io/File;)Landroid/os/StatFs;

    move-result-object v0

    iput-object v0, p0, LX/0V8;->c:Landroid/os/StatFs;

    .line 67432
    iget-object v0, p0, LX/0V8;->e:Landroid/os/StatFs;

    iget-object v1, p0, LX/0V8;->f:Ljava/io/File;

    invoke-static {v0, v1}, LX/0V8;->a(Landroid/os/StatFs;Ljava/io/File;)Landroid/os/StatFs;

    move-result-object v0

    iput-object v0, p0, LX/0V8;->e:Landroid/os/StatFs;

    .line 67433
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/0V8;->g:J

    .line 67434
    return-void
.end method


# virtual methods
.method public final a(LX/0VA;J)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 67406
    invoke-static {p0}, LX/0V8;->c(LX/0V8;)V

    .line 67407
    invoke-virtual {p0, p1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    .line 67408
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 67409
    cmp-long v1, v2, p2

    if-gez v1, :cond_1

    .line 67410
    :cond_0
    :goto_0
    return v0

    .line 67411
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67425
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67426
    :try_start_0
    invoke-static {p0}, LX/0V8;->c(LX/0V8;)V

    .line 67427
    invoke-direct {p0}, LX/0V8;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67428
    iget-object v0, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 67429
    :cond_0
    return-void

    .line 67430
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0V8;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final c(LX/0VA;)J
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .prologue
    .line 67412
    invoke-static {p0}, LX/0V8;->c(LX/0V8;)V

    .line 67413
    invoke-static {p0}, LX/0V8;->d(LX/0V8;)V

    .line 67414
    sget-object v0, LX/0VA;->INTERNAL:LX/0VA;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/0V8;->c:Landroid/os/StatFs;

    .line 67415
    :goto_0
    if-eqz v0, :cond_2

    .line 67416
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 67417
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 67418
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 67419
    :goto_1
    mul-long/2addr v0, v2

    .line 67420
    :goto_2
    return-wide v0

    .line 67421
    :cond_0
    iget-object v0, p0, LX/0V8;->e:Landroid/os/StatFs;

    goto :goto_0

    .line 67422
    :cond_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v2, v1

    .line 67423
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    goto :goto_1

    .line 67424
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_2
.end method
