.class public LX/1W4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BtB;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1W4",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BtB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 267326
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 267327
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1W4;->b:LX/0Zi;

    .line 267328
    iput-object p1, p0, LX/1W4;->a:LX/0Ot;

    .line 267329
    return-void
.end method

.method public static a(LX/0QB;)LX/1W4;
    .locals 4

    .prologue
    .line 267354
    const-class v1, LX/1W4;

    monitor-enter v1

    .line 267355
    :try_start_0
    sget-object v0, LX/1W4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267356
    sput-object v2, LX/1W4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267357
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267358
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267359
    new-instance v3, LX/1W4;

    const/16 p0, 0x1d45

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1W4;-><init>(LX/0Ot;)V

    .line 267360
    move-object v0, v3

    .line 267361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1W4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 267353
    const v0, -0x64e0243e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 267344
    check-cast p2, LX/BtA;

    .line 267345
    iget-object v0, p0, LX/1W4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BtB;

    iget-object v1, p2, LX/BtA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BtA;->b:LX/1Pn;

    .line 267346
    iget-object p0, v0, LX/BtB;->a:LX/1WE;

    invoke-virtual {p0, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object p0

    .line 267347
    iget-object p2, v0, LX/BtB;->d:LX/1VI;

    invoke-virtual {p2}, LX/1VI;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 267348
    const p2, 0x7f0a00aa

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1XG;->a(Ljava/lang/Integer;)LX/1XG;

    .line 267349
    :cond_0
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 267350
    const p2, -0x64e0243e

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 267351
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 267352
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 267330
    invoke-static {}, LX/1dS;->b()V

    .line 267331
    iget v0, p1, LX/1dQ;->b:I

    .line 267332
    packed-switch v0, :pswitch_data_0

    .line 267333
    :goto_0
    return-object v2

    .line 267334
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 267335
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 267336
    check-cast v1, LX/BtA;

    .line 267337
    iget-object v3, p0, LX/1W4;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BtB;

    iget-object p1, v1, LX/BtA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 267338
    iget-object p2, v3, LX/BtB;->c:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Bt8;

    .line 267339
    iget-object p0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 267340
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/text/InstantArticleClickTextPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p0

    .line 267341
    iput-object p0, p2, LX/Bt8;->b:Ljava/lang/String;

    .line 267342
    invoke-virtual {p2, v0, v1, v1}, LX/Bt8;->onClick(Landroid/view/View;FF)V

    .line 267343
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x64e0243e
        :pswitch_0
    .end packed-switch
.end method
