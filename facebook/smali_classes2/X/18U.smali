.class public LX/18U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/megaphone/api/FetchMegaphoneParams;",
        "Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 206552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206553
    iput-object p1, p0, LX/18U;->a:LX/0SG;

    .line 206554
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 206555
    check-cast p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;

    .line 206556
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 206557
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "megaphone_location"

    iget-object v2, p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->a:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206558
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "img_size"

    iget v2, p1, Lcom/facebook/megaphone/api/FetchMegaphoneParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206559
    new-instance v0, LX/14N;

    const-string v1, "xml_megaphone"

    const-string v2, "GET"

    const-string v3, "megaphoneandroidlayouts"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 206560
    const/4 v6, 0x0

    .line 206561
    iget v0, p2, LX/1pN;->b:I

    move v2, v0

    .line 206562
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 206563
    new-instance v2, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;

    sget-object v3, LX/0ta;->NO_DATA:LX/0ta;

    iget-object v4, p0, LX/18U;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    move-object v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;-><init>(LX/0ta;JLjava/lang/String;Ljava/lang/String;)V

    .line 206564
    :goto_0
    return-object v2

    .line 206565
    :cond_0
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v2

    .line 206566
    :try_start_0
    const-class v3, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;

    invoke-virtual {v2, v3}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206567
    new-instance v2, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v4, p0, LX/18U;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    if-nez v7, :cond_1

    move-object v8, v6

    :goto_1
    if-nez v7, :cond_2

    move-object v7, v6

    :goto_2
    move-object v6, v8

    invoke-direct/range {v2 .. v7}, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;-><init>(LX/0ta;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206568
    :catch_0
    new-instance v2, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;

    sget-object v3, LX/0ta;->NO_DATA:LX/0ta;

    iget-object v4, p0, LX/18U;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    move-object v7, v6

    invoke-direct/range {v2 .. v7}, Lcom/facebook/megaphone/api/FetchMegaphoneLayoutResult;-><init>(LX/0ta;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206569
    :cond_1
    iget-object v8, v7, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;->cacheId:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v7, v7, Lcom/facebook/megaphone/model/MegaphoneLayoutResponse;->layout:Ljava/lang/String;

    goto :goto_2
.end method
