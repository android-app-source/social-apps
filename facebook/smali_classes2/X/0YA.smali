.class public final enum LX/0YA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0YA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0YA;

.field public static final enum DONE:LX/0YA;

.field public static final enum READY:LX/0YA;

.field public static final enum TRACING:LX/0YA;

.field public static final enum WAIT:LX/0YA;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80692
    new-instance v0, LX/0YA;

    const-string v1, "WAIT"

    invoke-direct {v0, v1, v2}, LX/0YA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0YA;->WAIT:LX/0YA;

    new-instance v0, LX/0YA;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, LX/0YA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0YA;->READY:LX/0YA;

    new-instance v0, LX/0YA;

    const-string v1, "TRACING"

    invoke-direct {v0, v1, v4}, LX/0YA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0YA;->TRACING:LX/0YA;

    new-instance v0, LX/0YA;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, LX/0YA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0YA;->DONE:LX/0YA;

    .line 80693
    const/4 v0, 0x4

    new-array v0, v0, [LX/0YA;

    sget-object v1, LX/0YA;->WAIT:LX/0YA;

    aput-object v1, v0, v2

    sget-object v1, LX/0YA;->READY:LX/0YA;

    aput-object v1, v0, v3

    sget-object v1, LX/0YA;->TRACING:LX/0YA;

    aput-object v1, v0, v4

    sget-object v1, LX/0YA;->DONE:LX/0YA;

    aput-object v1, v0, v5

    sput-object v0, LX/0YA;->$VALUES:[LX/0YA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80696
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0YA;
    .locals 1

    .prologue
    .line 80695
    const-class v0, LX/0YA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0YA;

    return-object v0
.end method

.method public static values()[LX/0YA;
    .locals 1

    .prologue
    .line 80694
    sget-object v0, LX/0YA;->$VALUES:[LX/0YA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0YA;

    return-object v0
.end method
