.class public LX/0fW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0fW;


# instance fields
.field public final a:LX/0WP;

.field private final b:LX/0Uh;

.field private final c:LX/10M;

.field public final d:LX/0lC;

.field public final e:LX/03V;

.field public final f:LX/0SG;

.field public final g:LX/0Xl;

.field public h:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0WP;LX/0Uh;LX/10M;LX/0lC;LX/03V;LX/0SG;LX/0Xl;)V
    .locals 0
    .param p2    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 108261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108262
    iput-object p1, p0, LX/0fW;->a:LX/0WP;

    .line 108263
    iput-object p2, p0, LX/0fW;->b:LX/0Uh;

    .line 108264
    iput-object p3, p0, LX/0fW;->c:LX/10M;

    .line 108265
    iput-object p4, p0, LX/0fW;->d:LX/0lC;

    .line 108266
    iput-object p5, p0, LX/0fW;->e:LX/03V;

    .line 108267
    iput-object p6, p0, LX/0fW;->f:LX/0SG;

    .line 108268
    iput-object p7, p0, LX/0fW;->g:LX/0Xl;

    .line 108269
    return-void
.end method

.method public static a(LX/0QB;)LX/0fW;
    .locals 11

    .prologue
    .line 108248
    sget-object v0, LX/0fW;->i:LX/0fW;

    if-nez v0, :cond_1

    .line 108249
    const-class v1, LX/0fW;

    monitor-enter v1

    .line 108250
    :try_start_0
    sget-object v0, LX/0fW;->i:LX/0fW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 108251
    if-eqz v2, :cond_0

    .line 108252
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 108253
    new-instance v3, LX/0fW;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v4

    check-cast v4, LX/0WP;

    invoke-static {v0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v6

    check-cast v6, LX/10M;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-direct/range {v3 .. v10}, LX/0fW;-><init>(LX/0WP;LX/0Uh;LX/10M;LX/0lC;LX/03V;LX/0SG;LX/0Xl;)V

    .line 108254
    move-object v0, v3

    .line 108255
    sput-object v0, LX/0fW;->i:LX/0fW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108256
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 108257
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 108258
    :cond_1
    sget-object v0, LX/0fW;->i:LX/0fW;

    return-object v0

    .line 108259
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 108260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0fW;Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;)V
    .locals 4

    .prologue
    .line 108241
    iget-object v0, p0, LX/0fW;->a:LX/0WP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dbl_local_auth_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->uid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    .line 108242
    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    .line 108243
    invoke-interface {v0}, LX/1gW;->a()LX/1gW;

    .line 108244
    :try_start_0
    const-string v1, "credentials"

    iget-object v2, p0, LX/0fW;->d:LX/0lC;

    invoke-virtual {v2, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    move-result-object v0

    const-string v1, "persisted_ts"

    iget-object v2, p0, LX/0fW;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/1gW;->a(Ljava/lang/String;J)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108245
    :goto_0
    return-void

    .line 108246
    :catch_0
    move-exception v0

    .line 108247
    iget-object v1, p0, LX/0fW;->e:LX/03V;

    const-string v2, "DeviceBasedLoginSessionPersister"

    const-string v3, "Unable to save localauth credentials"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(LX/0fW;)Z
    .locals 3

    .prologue
    .line 108240
    iget-object v0, p0, LX/0fW;->b:LX/0Uh;

    const/16 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private g(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 108270
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108271
    :cond_0
    :goto_0
    return v0

    .line 108272
    :cond_1
    iget-object v1, p0, LX/0fW;->a:LX/0WP;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dbl_local_auth_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v1

    .line 108273
    const-string v2, "credentials"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108274
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 108239
    invoke-static {p0}, LX/0fW;->b(LX/0fW;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fW;->c:LX/10M;

    invoke-virtual {v0, p1}, LX/10M;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0fW;->c:LX/10M;

    invoke-virtual {v0, p1}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LX/0fW;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 108218
    invoke-static {p0}, LX/0fW;->b(LX/0fW;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LX/0fW;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 108235
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108236
    :goto_0
    return-void

    .line 108237
    :cond_0
    iget-object v0, p0, LX/0fW;->a:LX/0WP;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dbl_local_auth_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    .line 108238
    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v0

    const-string v1, "credentials"

    invoke-interface {v0, v1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    const-string v1, "persisted_ts"

    invoke-interface {v0, v1}, LX/1gW;->a(Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->b()Z

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lcom/facebook/auth/component/AuthenticationResult;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 108219
    invoke-static {p0}, LX/0fW;->b(LX/0fW;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v1, v8

    .line 108220
    :goto_0
    return-object v1

    .line 108221
    :cond_0
    const/4 v1, 0x0

    .line 108222
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108223
    :goto_1
    move-object v7, v1

    .line 108224
    if-nez v7, :cond_1

    move-object v1, v8

    .line 108225
    goto :goto_0

    .line 108226
    :cond_1
    new-instance v0, Lcom/facebook/auth/credentials/FacebookCredentials;

    iget-object v2, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->accessToken:Ljava/lang/String;

    iget-object v3, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->sessionCookieString:Ljava/lang/String;

    iget-object v4, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->secret:Ljava/lang/String;

    iget-object v5, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->sessionKey:Ljava/lang/String;

    iget-object v6, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->username:Ljava/lang/String;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/auth/credentials/FacebookCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108227
    new-instance v1, Lcom/facebook/auth/protocol/AuthenticationResultImpl;

    iget-object v4, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->machineId:Ljava/lang/String;

    iget v2, v7, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;->confirmationStatus:I

    invoke-static {v2}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v5

    move-object v2, p1

    move-object v3, v0

    move-object v6, v8

    move-object v7, v8

    invoke-direct/range {v1 .. v7}, Lcom/facebook/auth/protocol/AuthenticationResultImpl;-><init>(Ljava/lang/String;Lcom/facebook/auth/credentials/FacebookCredentials;Ljava/lang/String;LX/03R;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108228
    :cond_2
    iget-object v0, p0, LX/0fW;->a:LX/0WP;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dbl_local_auth_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    .line 108229
    const-string v2, "credentials"

    invoke-virtual {v0, v2, v1}, LX/0WS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108230
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 108231
    :try_start_0
    iget-object v2, p0, LX/0fW;->d:LX/0lC;

    const-class v3, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;

    invoke-virtual {v2, v0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v1, v0

    .line 108232
    goto :goto_1

    .line 108233
    :catch_0
    move-exception v0

    .line 108234
    iget-object v2, p0, LX/0fW;->e:LX/03V;

    const-string v3, "DeviceBasedLoginSessionPersister"

    const-string v4, "Unable to read localauth credentials from preferences"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
