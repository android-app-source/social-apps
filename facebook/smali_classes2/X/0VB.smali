.class public LX/0VB;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/03U;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/03U;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67488
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/03U;
    .locals 9

    .prologue
    .line 67490
    sget-object v0, LX/0VB;->a:LX/03U;

    if-nez v0, :cond_1

    .line 67491
    const-class v1, LX/0VB;

    monitor-enter v1

    .line 67492
    :try_start_0
    sget-object v0, LX/0VB;->a:LX/03U;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67493
    if-eqz v2, :cond_0

    .line 67494
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67495
    const/16 v3, 0x2fd

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x1466

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0VC;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    const-class v7, Landroid/content/Context;

    const-class v8, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v7, v8}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static/range {v3 .. v8}, LX/0VE;->a(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;Landroid/content/Context;LX/0Uh;)LX/03U;

    move-result-object v3

    move-object v0, v3

    .line 67496
    sput-object v0, LX/0VB;->a:LX/03U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67497
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67498
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67499
    :cond_1
    sget-object v0, LX/0VB;->a:LX/03U;

    return-object v0

    .line 67500
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 67489
    const/16 v0, 0x2fd

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    const/16 v1, 0x1466

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0VC;->a(LX/0QB;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v3

    check-cast v3, Ljava/util/Random;

    const-class v4, Landroid/content/Context;

    const-class v5, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v4, v5}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static/range {v0 .. v5}, LX/0VE;->a(LX/0Or;LX/0Or;Ljava/util/concurrent/ExecutorService;Ljava/util/Random;Landroid/content/Context;LX/0Uh;)LX/03U;

    move-result-object v0

    return-object v0
.end method
