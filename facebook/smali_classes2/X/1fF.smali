.class public final LX/1fF;
.super LX/11e;
.source ""

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Xs",
        "<TK;TV;>.WrappedCollection;",
        "Ljava/util/Set",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Set;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Set",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 291120
    iput-object p1, p0, LX/1fF;->a:LX/0Xs;

    .line 291121
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/11e;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Collection;LX/11e;)V

    .line 291122
    return-void
.end method


# virtual methods
.method public final removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 291123
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291124
    const/4 v0, 0x0

    .line 291125
    :cond_0
    :goto_0
    return v0

    .line 291126
    :cond_1
    invoke-virtual {p0}, LX/1fF;->size()I

    move-result v1

    .line 291127
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    check-cast v0, Ljava/util/Set;

    invoke-static {v0, p1}, LX/0RA;->a(Ljava/util/Set;Ljava/util/Collection;)Z

    move-result v0

    .line 291128
    if-eqz v0, :cond_0

    .line 291129
    iget-object v2, p0, LX/11e;->c:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 291130
    iget-object v3, p0, LX/1fF;->a:LX/0Xs;

    sub-int v1, v2, v1

    invoke-static {v3, v1}, LX/0Xs;->a(LX/0Xs;I)I

    .line 291131
    invoke-virtual {p0}, LX/11e;->b()V

    goto :goto_0
.end method
