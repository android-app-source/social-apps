.class public final LX/1XA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1V1;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/1X9;

.field public c:I

.field public d:LX/1X6;

.field public e:Z

.field public final synthetic f:LX/1V1;


# direct methods
.method public constructor <init>(LX/1V1;)V
    .locals 1

    .prologue
    .line 270592
    iput-object p1, p0, LX/1XA;->f:LX/1V1;

    .line 270593
    move-object v0, p1

    .line 270594
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 270595
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270596
    const-string v0, "FeedBackgroundStylerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270597
    if-ne p0, p1, :cond_1

    .line 270598
    :cond_0
    :goto_0
    return v0

    .line 270599
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 270600
    goto :goto_0

    .line 270601
    :cond_3
    check-cast p1, LX/1XA;

    .line 270602
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 270603
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 270604
    if-eq v2, v3, :cond_0

    .line 270605
    iget-object v2, p0, LX/1XA;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1XA;->a:LX/1X1;

    iget-object v3, p1, LX/1XA;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 270606
    goto :goto_0

    .line 270607
    :cond_5
    iget-object v2, p1, LX/1XA;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 270608
    :cond_6
    iget-object v2, p0, LX/1XA;->b:LX/1X9;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1XA;->b:LX/1X9;

    iget-object v3, p1, LX/1XA;->b:LX/1X9;

    invoke-virtual {v2, v3}, LX/1X9;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 270609
    goto :goto_0

    .line 270610
    :cond_8
    iget-object v2, p1, LX/1XA;->b:LX/1X9;

    if-nez v2, :cond_7

    .line 270611
    :cond_9
    iget v2, p0, LX/1XA;->c:I

    iget v3, p1, LX/1XA;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 270612
    goto :goto_0

    .line 270613
    :cond_a
    iget-object v2, p0, LX/1XA;->d:LX/1X6;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/1XA;->d:LX/1X6;

    iget-object v3, p1, LX/1XA;->d:LX/1X6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 270614
    goto :goto_0

    .line 270615
    :cond_c
    iget-object v2, p1, LX/1XA;->d:LX/1X6;

    if-nez v2, :cond_b

    .line 270616
    :cond_d
    iget-boolean v2, p0, LX/1XA;->e:Z

    iget-boolean v3, p1, LX/1XA;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 270617
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 270618
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1XA;

    .line 270619
    iget-object v1, v0, LX/1XA;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1XA;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/1XA;->a:LX/1X1;

    .line 270620
    return-object v0

    .line 270621
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
