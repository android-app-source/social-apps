.class public abstract LX/1P5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1OR;

.field public b:I


# direct methods
.method public constructor <init>(LX/1OR;)V
    .locals 1

    .prologue
    .line 243600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243601
    const/high16 v0, -0x80000000

    iput v0, p0, LX/1P5;->b:I

    .line 243602
    iput-object p1, p0, LX/1P5;->a:LX/1OR;

    .line 243603
    return-void
.end method

.method public static a(LX/1OR;I)LX/1P5;
    .locals 2

    .prologue
    .line 243604
    packed-switch p1, :pswitch_data_0

    .line 243605
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243606
    :pswitch_0
    new-instance v0, LX/25a;

    invoke-direct {v0, p0}, LX/25a;-><init>(LX/1OR;)V

    move-object v0, v0

    .line 243607
    :goto_0
    return-object v0

    .line 243608
    :pswitch_1
    new-instance v0, LX/1P6;

    invoke-direct {v0, p0}, LX/1P6;-><init>(LX/1OR;)V

    move-object v0, v0

    .line 243609
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)I
.end method

.method public abstract a(I)V
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 243610
    const/high16 v0, -0x80000000

    iget v1, p0, LX/1P5;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/1P5;->f()I

    move-result v0

    iget v1, p0, LX/1P5;->b:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public abstract b(Landroid/view/View;)I
.end method

.method public abstract c()I
.end method

.method public abstract c(Landroid/view/View;)I
.end method

.method public abstract d()I
.end method

.method public abstract d(Landroid/view/View;)I
.end method

.method public abstract e()I
.end method

.method public abstract f()I
.end method

.method public abstract g()I
.end method
