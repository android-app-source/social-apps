.class public LX/1qb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static f:I

.field public static g:I

.field private static volatile h:LX/1qb;


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/1DR;

.field private c:LX/17R;

.field private d:LX/1Uj;

.field public e:LX/0hB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1DR;LX/17R;LX/1Uj;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 331226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331227
    iput-object p1, p0, LX/1qb;->a:Landroid/content/Context;

    .line 331228
    iput-object p2, p0, LX/1qb;->b:LX/1DR;

    .line 331229
    iput-object p3, p0, LX/1qb;->c:LX/17R;

    .line 331230
    iput-object p4, p0, LX/1qb;->d:LX/1Uj;

    .line 331231
    iput-object p5, p0, LX/1qb;->e:LX/0hB;

    .line 331232
    return-void
.end method

.method public static a(LX/0QB;)LX/1qb;
    .locals 9

    .prologue
    .line 331213
    sget-object v0, LX/1qb;->h:LX/1qb;

    if-nez v0, :cond_1

    .line 331214
    const-class v1, LX/1qb;

    monitor-enter v1

    .line 331215
    :try_start_0
    sget-object v0, LX/1qb;->h:LX/1qb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 331216
    if-eqz v2, :cond_0

    .line 331217
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 331218
    new-instance v3, LX/1qb;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v6

    check-cast v6, LX/17R;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v7

    check-cast v7, LX/1Uj;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v8

    check-cast v8, LX/0hB;

    invoke-direct/range {v3 .. v8}, LX/1qb;-><init>(Landroid/content/Context;LX/1DR;LX/17R;LX/1Uj;LX/0hB;)V

    .line 331219
    move-object v0, v3

    .line 331220
    sput-object v0, LX/1qb;->h:LX/1qb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 331221
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 331222
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331223
    :cond_1
    sget-object v0, LX/1qb;->h:LX/1qb;

    return-object v0

    .line 331224
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 331225
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 331210
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 331211
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 331212
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/text/SpannableStringBuilder;)V
    .locals 2

    .prologue
    .line 331203
    invoke-static {p0}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 331204
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 331205
    :goto_0
    if-eqz v0, :cond_0

    .line 331206
    invoke-static {p1}, LX/1qb;->a(Landroid/text/SpannableStringBuilder;)V

    .line 331207
    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 331208
    :cond_0
    return-void

    .line 331209
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;
    .locals 2

    .prologue
    .line 331199
    invoke-static {p0}, LX/1qb;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 331200
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331201
    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 331202
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 331193
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 331194
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331195
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 331196
    :goto_0
    return-object v0

    .line 331197
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 331198
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 331187
    invoke-static {p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 331188
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331189
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 331190
    :goto_0
    return-object v0

    .line 331191
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 331192
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331151
    invoke-static {p0}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 331152
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331153
    invoke-static {p0}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 331154
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 331155
    :goto_0
    if-eqz v1, :cond_0

    .line 331156
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 331157
    :cond_0
    return-object v0

    .line 331158
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLImage;F)Z
    .locals 8
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const v3, 0x3f0ccccd    # 0.55f

    .line 331168
    if-nez p1, :cond_1

    .line 331169
    :cond_0
    :goto_0
    return v0

    .line 331170
    :cond_1
    sget v1, LX/1qb;->f:I

    if-eqz v1, :cond_2

    sget v1, LX/1qb;->g:I

    if-nez v1, :cond_4

    .line 331171
    :cond_2
    iget-object v1, p0, LX/1qb;->b:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    .line 331172
    iget-object v2, p0, LX/1qb;->e:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    .line 331173
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 331174
    iget-object v6, p0, LX/1qb;->a:Landroid/content/Context;

    const/4 v7, 0x1

    .line 331175
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 331176
    sget p0, LX/1DR;->a:I

    const/4 v6, -0x1

    if-ne p0, v6, :cond_3

    .line 331177
    const p0, 0x7f0b0917

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    sput p0, LX/1DR;->a:I

    .line 331178
    :cond_3
    add-int/lit8 v5, v7, 0x1

    sget p0, LX/1DR;->a:I

    mul-int/2addr v5, p0

    move v5, v5

    .line 331179
    mul-int/lit8 v5, v5, 0x2

    .line 331180
    sub-int/2addr v4, v5

    sput v4, LX/1qb;->f:I

    .line 331181
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 331182
    sub-int/2addr v1, v5

    sput v1, LX/1qb;->g:I

    .line 331183
    :cond_4
    sget v1, LX/1qb;->f:I

    int-to-float v1, v1

    div-float/2addr v1, p2

    .line 331184
    sget v2, LX/1qb;->f:I

    int-to-float v2, v2

    mul-float/2addr v2, v3

    .line 331185
    mul-float/2addr v1, v3

    .line 331186
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 331165
    invoke-virtual {p0, p1}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 331166
    invoke-static {p1, v0}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/text/SpannableStringBuilder;)V

    .line 331167
    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 331159
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 331160
    invoke-static {p1}, LX/1qb;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v1

    .line 331161
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 331162
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 331163
    iget-object v2, p0, LX/1qb;->d:LX/1Uj;

    invoke-virtual {v2}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x11

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 331164
    :cond_0
    return-object v0
.end method
