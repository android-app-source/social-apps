.class public LX/1nA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static w:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final d:LX/0Zb;

.field public final e:LX/0gh;

.field public final f:LX/1nG;

.field public final g:LX/17Q;

.field public final h:LX/17V;

.field public final i:LX/1Ay;

.field public final j:LX/0bH;

.field public final k:LX/0kL;

.field public final l:LX/03V;

.field public final m:LX/1nI;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/82I;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/1nM;

.field public final p:LX/17d;

.field private final q:LX/1nP;

.field public final r:LX/1nS;

.field public final s:LX/1nV;

.field public final t:Landroid/view/View$OnClickListener;

.field private final u:Landroid/view/View$OnClickListener;

.field private final v:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314755
    const-class v0, LX/1nA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1nA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/1nG;LX/17Q;LX/17V;LX/1Ay;LX/0bH;LX/0kL;LX/0gh;LX/03V;LX/1nI;LX/0Ot;LX/1nM;LX/17d;LX/1nP;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Zb;",
            "LX/1nG;",
            "LX/17Q;",
            "LX/17V;",
            "LX/1Ay;",
            "LX/0bH;",
            "LX/0kL;",
            "LX/0gh;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1nI;",
            "LX/0Ot",
            "<",
            "LX/82I;",
            ">;",
            "LX/1nM;",
            "LX/17d;",
            "LX/1nP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 314731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314732
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314733
    iput-object p1, p0, LX/1nA;->b:Landroid/content/Context;

    .line 314734
    iput-object p2, p0, LX/1nA;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 314735
    iput-object p3, p0, LX/1nA;->d:LX/0Zb;

    .line 314736
    iput-object p7, p0, LX/1nA;->i:LX/1Ay;

    .line 314737
    iput-object p4, p0, LX/1nA;->f:LX/1nG;

    .line 314738
    iput-object p5, p0, LX/1nA;->g:LX/17Q;

    .line 314739
    iput-object p6, p0, LX/1nA;->h:LX/17V;

    .line 314740
    iput-object p8, p0, LX/1nA;->j:LX/0bH;

    .line 314741
    iput-object p9, p0, LX/1nA;->k:LX/0kL;

    .line 314742
    iput-object p10, p0, LX/1nA;->e:LX/0gh;

    .line 314743
    iput-object p11, p0, LX/1nA;->l:LX/03V;

    .line 314744
    iput-object p12, p0, LX/1nA;->m:LX/1nI;

    .line 314745
    iput-object p13, p0, LX/1nA;->n:LX/0Ot;

    .line 314746
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1nA;->o:LX/1nM;

    .line 314747
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1nA;->p:LX/17d;

    .line 314748
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1nA;->q:LX/1nP;

    .line 314749
    new-instance v1, LX/1nS;

    invoke-direct {v1, p0}, LX/1nS;-><init>(LX/1nA;)V

    iput-object v1, p0, LX/1nA;->r:LX/1nS;

    .line 314750
    new-instance v1, LX/1nT;

    invoke-direct {v1, p0}, LX/1nT;-><init>(LX/1nA;)V

    iput-object v1, p0, LX/1nA;->t:Landroid/view/View$OnClickListener;

    .line 314751
    new-instance v1, LX/1nU;

    invoke-direct {v1, p0}, LX/1nU;-><init>(LX/1nA;)V

    iput-object v1, p0, LX/1nA;->s:LX/1nV;

    .line 314752
    new-instance v1, LX/1nW;

    invoke-direct {v1, p0}, LX/1nW;-><init>(LX/1nA;)V

    iput-object v1, p0, LX/1nA;->u:Landroid/view/View$OnClickListener;

    .line 314753
    new-instance v1, LX/1nX;

    invoke-direct {v1, p0}, LX/1nX;-><init>(LX/1nA;)V

    iput-object v1, p0, LX/1nA;->v:Landroid/view/View$OnClickListener;

    .line 314754
    return-void
.end method

.method public static a(LX/0QB;)LX/1nA;
    .locals 3

    .prologue
    .line 314723
    const-class v1, LX/1nA;

    monitor-enter v1

    .line 314724
    :try_start_0
    sget-object v0, LX/1nA;->w:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 314725
    sput-object v2, LX/1nA;->w:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 314726
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314727
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1nA;->b(LX/0QB;)LX/1nA;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 314728
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1nA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314729
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 314730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/1nA;Landroid/view/View;Ljava/lang/String;ZLX/162;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8

    .prologue
    .line 314712
    const v1, 0x7f0d0083

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 314713
    const v1, 0x7f0d007d

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/Integer;

    .line 314714
    if-nez v2, :cond_1

    .line 314715
    const-string v5, "native_newsfeed"

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 314716
    :cond_0
    :goto_0
    move-object v0, v1

    .line 314717
    return-object v0

    .line 314718
    :cond_1
    iget-object v1, p0, LX/1nA;->h:LX/17V;

    const-string v5, "native_newsfeed"

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v1 .. v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 314719
    const v2, 0x7f0d007e

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    .line 314720
    :goto_1
    if-eqz v2, :cond_0

    .line 314721
    const-string v2, "cta_click"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 314722
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/1nA;
    .locals 18

    .prologue
    .line 314710
    new-instance v1, LX/1nA;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v5

    check-cast v5, LX/1nG;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v6

    check-cast v6, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/17V;->a(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static/range {p0 .. p0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v8

    check-cast v8, LX/1Ay;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v11

    check-cast v11, LX/0gh;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v13

    check-cast v13, LX/1nI;

    const/16 v14, 0x1cc8

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/1nM;->a(LX/0QB;)LX/1nM;

    move-result-object v15

    check-cast v15, LX/1nM;

    invoke-static/range {p0 .. p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v16

    check-cast v16, LX/17d;

    invoke-static/range {p0 .. p0}, LX/1nP;->a(LX/0QB;)LX/1nP;

    move-result-object v17

    check-cast v17, LX/1nP;

    invoke-direct/range {v1 .. v17}, LX/1nA;-><init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/1nG;LX/17Q;LX/17V;LX/1Ay;LX/0bH;LX/0kL;LX/0gh;LX/03V;LX/1nI;LX/0Ot;LX/1nM;LX/17d;LX/1nP;)V

    .line 314711
    return-object v1
.end method

.method public static c(LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 314703
    invoke-static {p1}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-nez v0, :cond_0

    .line 314704
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 314705
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 314706
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v1

    .line 314707
    :goto_0
    iget-object v2, p0, LX/1nA;->l:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATTACHMENT_MISSING_PARENT_STORY_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dedupe key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314708
    :cond_0
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    return-object v0

    .line 314709
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Ljava/lang/String;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 314693
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 314694
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 314695
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 314696
    sget-object v5, LX/5P2;->NEWSFEED:LX/5P2;

    move-object v9, v1

    move-object v7, v1

    .line 314697
    :goto_0
    new-instance v0, LX/3i6;

    invoke-virtual {p0, p3, p2, v1}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v1

    const v2, 0x7f0d0082

    invoke-static {p0, p1}, LX/1nA;->c(LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const v4, 0x7f0d0084

    const v6, 0x7f0d0085

    const v8, 0x7f0d0087

    invoke-direct/range {v0 .. v9}, LX/3i6;-><init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    return-object v0

    .line 314698
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314699
    sget-object v7, Lcom/facebook/events/common/ActionSource;->NEWSFEED:Lcom/facebook/events/common/ActionSource;

    .line 314700
    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object v5, v1

    goto :goto_0

    .line 314701
    :cond_1
    invoke-static {p3}, LX/047;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314702
    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object v7, v1

    move-object v5, v1

    goto :goto_0

    :cond_2
    move-object v9, v1

    move-object v7, v1

    move-object v5, v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 314756
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314757
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314758
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 314759
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 314760
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 314761
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BUY_TICKETS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bG()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 314762
    iget-object v0, p0, LX/1nA;->q:LX/1nP;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-static {}, LX/1nP;->a()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1nP;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 314763
    :cond_0
    :goto_0
    return-object v6

    .line 314764
    :cond_1
    invoke-virtual {p0, v5, p2}, LX/1nA;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v4

    .line 314765
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314766
    new-instance v0, LX/3i5;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/3i5;-><init>(LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 314767
    if-nez v2, :cond_2

    move-object v1, v6

    .line 314768
    :goto_1
    invoke-static {v5, v1, v4}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v6

    .line 314769
    new-instance v1, LX/3i6;

    const v3, 0x7f0d007f

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v4

    const v5, 0x7f0d0080

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, LX/3i6;-><init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;)V

    move-object v6, v1

    goto :goto_0

    .line 314770
    :cond_2
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 314771
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 314692
    new-instance v0, LX/82G;

    invoke-direct {v0, p0, p1, p2}, LX/82G;-><init>(LX/1nA;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 314688
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 314689
    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 314690
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 314691
    :cond_2
    new-instance v0, LX/3i6;

    iget-object v1, p0, LX/1nA;->u:Landroid/view/View$OnClickListener;

    const v2, 0x7f0d006a

    const v4, 0x7f0d007a

    const v6, 0x7f0d0073

    move-object v3, p2

    move-object v5, p3

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/3i6;-><init>(Landroid/view/View$OnClickListener;ILjava/lang/Object;ILjava/lang/Object;ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;IZ)Ljava/lang/CharSequence;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IZ)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 314599
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 314600
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 314601
    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 314602
    if-nez v2, :cond_0

    .line 314603
    iget-object v1, p0, LX/1nA;->l:LX/03V;

    const-class v2, LX/1nA;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Story has null message: %s"

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314604
    const-string v0, ""

    .line 314605
    :goto_0
    return-object v0

    .line 314606
    :cond_0
    const/4 v3, 0x0

    .line 314607
    if-eqz p3, :cond_4

    .line 314608
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 314609
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 314610
    iget-object v1, p0, LX/1nA;->o:LX/1nM;

    .line 314611
    const/4 v3, -0x1

    invoke-static {v1, p1, p2, v3}, LX/1nM;->a(LX/1nM;Lcom/facebook/feed/rows/core/props/FeedProps;II)Landroid/text/Spannable;

    move-result-object v3

    move-object v1, v3

    .line 314612
    :goto_1
    if-nez v1, :cond_a

    .line 314613
    iget-object v3, p0, LX/1nA;->o:LX/1nM;

    invoke-virtual {v3, p1, p2}, LX/1nM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/text/Spannable;

    move-result-object v4

    .line 314614
    :goto_2
    if-nez v4, :cond_5

    .line 314615
    const/4 v4, 0x0

    .line 314616
    :cond_1
    :goto_3
    move-object v1, v4

    .line 314617
    move-object v1, v1

    .line 314618
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 314619
    goto :goto_0

    .line 314620
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 314621
    if-nez v1, :cond_3

    .line 314622
    iget-object v1, p0, LX/1nA;->l:LX/03V;

    const-class v2, LX/1nA;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Story has null message text: %s"

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314623
    const-string v0, ""

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 314624
    goto :goto_0

    :cond_4
    move-object v1, v3

    goto :goto_1

    .line 314625
    :cond_5
    instance-of v3, v4, Landroid/text/Spannable;

    if-eqz v3, :cond_1

    move-object v3, v4

    .line 314626
    check-cast v3, Landroid/text/Spannable;

    const/4 v7, 0x0

    .line 314627
    iget-object v5, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 314628
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 314629
    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v6

    const-class p3, LX/1yS;

    invoke-interface {v3, v7, v6, p3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [LX/1yS;

    .line 314630
    array-length p2, v6

    move v1, v7

    :goto_4
    if-ge v1, p2, :cond_9

    aget-object v7, v6, v1

    .line 314631
    iget-object p3, v7, LX/1yS;->c:Ljava/lang/String;

    move-object p3, p3

    .line 314632
    invoke-static {p3}, LX/17d;->a(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_7

    .line 314633
    iget-object p3, p0, LX/1nA;->s:LX/1nV;

    invoke-virtual {v7, p3}, LX/1yS;->a(LX/1nV;)V

    .line 314634
    :cond_6
    :goto_5
    add-int/lit8 v7, v1, 0x1

    move v1, v7

    goto :goto_4

    .line 314635
    :cond_7
    instance-of p3, v7, LX/2xy;

    if-eqz p3, :cond_6

    .line 314636
    check-cast v7, LX/2xy;

    .line 314637
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p3

    .line 314638
    iput-object p3, v7, LX/2xy;->f:Ljava/lang/String;

    .line 314639
    invoke-static {p1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p3

    .line 314640
    if-eqz p3, :cond_8

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p3

    .line 314641
    :goto_6
    iput-object p3, v7, LX/2xy;->g:Ljava/lang/String;

    .line 314642
    iget-object p3, p0, LX/1nA;->r:LX/1nS;

    .line 314643
    if-eqz p3, :cond_b

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_7
    iput-object v3, v7, LX/2xy;->c:Ljava/lang/ref/WeakReference;

    .line 314644
    goto :goto_5

    .line 314645
    :cond_8
    const/4 p3, 0x0

    goto :goto_6

    .line 314646
    :cond_9
    goto :goto_3

    :cond_a
    move-object v4, v1

    goto :goto_2

    .line 314647
    :cond_b
    const/4 v3, 0x0

    goto :goto_7
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 314687
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;IZ)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 314679
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314680
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314681
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    .line 314682
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 314683
    iget-object v0, p0, LX/1nA;->f:LX/1nG;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;)Ljava/lang/String;

    move-result-object v0

    .line 314684
    :cond_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 314685
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    .line 314686
    :cond_1
    return-object v0
.end method

.method public final a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 314677
    const v0, 0x7f0d006a

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const v0, 0x7f0d0070

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const v0, 0x7f0d0071

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, LX/1nA;->a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 314678
    return-void
.end method

.method public final a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 314656
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314657
    iget-object v0, p0, LX/1nA;->f:LX/1nG;

    invoke-interface {p2}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-interface {p2}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 314658
    if-nez v2, :cond_0

    .line 314659
    :goto_0
    return-void

    .line 314660
    :cond_0
    invoke-static {p3}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 314661
    invoke-static {p3, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 314662
    :cond_1
    iget-object v0, p0, LX/1nA;->d:LX/0Zb;

    invoke-interface {v0, p3}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 314663
    iget-object v0, p0, LX/1nA;->e:LX/0gh;

    .line 314664
    iput-object p6, v0, LX/0gh;->y:Ljava/lang/String;

    .line 314665
    if-eqz p3, :cond_3

    .line 314666
    iget-boolean v0, p3, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v0

    .line 314667
    if-eqz v0, :cond_3

    .line 314668
    iget-object v0, p0, LX/1nA;->e:LX/0gh;

    const-string v3, "tap_profile_pic_sponsored"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 314669
    :goto_1
    if-nez p4, :cond_2

    .line 314670
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    .line 314671
    :cond_2
    invoke-interface {p2}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    invoke-interface {p2}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    invoke-interface {v0}, LX/3Bf;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {p2}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v5

    invoke-static {p4, v3, v4, v0, v5}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 314672
    iget-object v0, p0, LX/1nA;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v2, p4, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto :goto_0

    .line 314673
    :cond_3
    if-eqz p5, :cond_4

    .line 314674
    iget-object v0, p0, LX/1nA;->e:LX/0gh;

    invoke-virtual {v0, p5}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_1

    .line 314675
    :cond_4
    iget-object v0, p0, LX/1nA;->e:LX/0gh;

    const-string v3, "tap_profile_pic"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 314676
    goto :goto_2
.end method

.method public final b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p3    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 314648
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 314649
    const v0, 0x7f0d006a

    invoke-virtual {p1, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 314650
    const v0, 0x7f0d0068

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 314651
    const v0, 0x7f0d0069

    invoke-virtual {p1, v0, p4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 314652
    const v0, 0x7f0d0070

    invoke-virtual {p1, v0, p5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 314653
    const v0, 0x7f0d0071

    invoke-virtual {p1, v0, p6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 314654
    iget-object v0, p0, LX/1nA;->v:Landroid/view/View$OnClickListener;

    .line 314655
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
