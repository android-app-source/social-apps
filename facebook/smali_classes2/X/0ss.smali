.class public final LX/0ss;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 153289
    sget-object v0, LX/0st;->a:LX/0U1;

    sget-object v1, LX/0st;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/0ss;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 153301
    const-string v0, "tags"

    sget-object v1, LX/0ss;->a:LX/0Px;

    new-instance v2, LX/0su;

    sget-object v3, LX/0st;->a:LX/0U1;

    sget-object v4, LX/0st;->b:LX/0U1;

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0su;-><init>(LX/0Px;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 153302
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 153297
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 153298
    const-string v0, "tags"

    const-string v1, "tag_table_tags_index"

    sget-object v2, LX/0st;->b:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x218159f9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x48d05984

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153299
    const-string v0, "tags"

    const-string v1, "tag_table_rowid_ref_index"

    sget-object v2, LX/0st;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x3d8cf085

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x886ee37

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153300
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 153294
    const/16 v0, 0x3e

    if-lt p2, v0, :cond_0

    .line 153295
    :goto_0
    return-void

    .line 153296
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0
.end method

.method public final d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 153290
    invoke-super {p0, p1}, LX/0Tz;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 153291
    const-string v0, "PRAGMA foreign_keys = ON"

    const v1, -0x2c33e7a7

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x49726588

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153292
    const-string v0, "PRAGMA synchronous = OFF"

    const v1, 0x3a4b84f5

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x3ef7a120

    invoke-static {v0}, LX/03h;->a(I)V

    .line 153293
    return-void
.end method
