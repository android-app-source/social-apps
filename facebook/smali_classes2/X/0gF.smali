.class public abstract LX/0gF;
.super LX/0gG;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0gc;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment$SavedState;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0hH;

.field private e:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111786
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 111787
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0gF;->a:Ljava/util/ArrayList;

    .line 111788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    .line 111789
    iput-object v1, p0, LX/0gF;->d:LX/0hH;

    .line 111790
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/0gF;->e:LX/0YU;

    .line 111791
    iput-object v1, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    .line 111792
    iput-object p1, p0, LX/0gF;->b:LX/0gc;

    .line 111793
    return-void
.end method

.method private static a(Landroid/support/v4/app/Fragment;Z)V
    .locals 0

    .prologue
    .line 111783
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 111784
    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 111785
    return-void
.end method

.method private static a(Ljava/util/ArrayList;I)V
    .locals 1

    .prologue
    .line 111780
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 111781
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111782
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 111760
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 111761
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 111762
    if-eqz v0, :cond_0

    .line 111763
    :goto_0
    return-object v0

    .line 111764
    :cond_0
    iget-object v0, p0, LX/0gF;->d:LX/0hH;

    if-nez v0, :cond_1

    .line 111765
    iget-object v0, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iput-object v0, p0, LX/0gF;->d:LX/0hH;

    .line 111766
    :cond_1
    invoke-virtual {p0, p2}, LX/0gF;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 111767
    invoke-virtual {p0, p2}, LX/0gF;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 111768
    iget-object v0, p0, LX/0gF;->a:Ljava/util/ArrayList;

    add-int/lit8 v3, p2, 0x1

    invoke-static {v0, v3}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111769
    iget-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_2

    .line 111770
    iget-object v0, p0, LX/0gF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111771
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111772
    iget-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    .line 111773
    if-eqz v0, :cond_2

    .line 111774
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 111775
    :cond_2
    iget-object v0, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    if-eq v1, v0, :cond_3

    .line 111776
    const/4 v0, 0x0

    invoke-static {v1, v0}, LX/0gF;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 111777
    :cond_3
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, p2, v1}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 111778
    iget-object v0, p0, LX/0gF;->d:LX/0hH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {p0, p2}, LX/0gF;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-object v0, v1

    .line 111779
    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 111725
    if-eqz p1, :cond_3

    .line 111726
    check-cast p1, Landroid/os/Bundle;

    .line 111727
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 111728
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 111729
    const-string v0, "states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 111730
    iget-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 111731
    if-eqz v3, :cond_0

    .line 111732
    iget-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    array-length v1, v3

    invoke-static {v0, v1}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111733
    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 111734
    iget-object v5, p0, LX/0gF;->c:Ljava/util/ArrayList;

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111736
    :cond_0
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 111737
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111738
    const-string v3, "f"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111739
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 111740
    iget-object v4, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v4, p1, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 111741
    if-eqz v4, :cond_2

    .line 111742
    invoke-static {v4, v2}, LX/0gF;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 111743
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, v3, v4}, LX/0YU;->a(ILjava/lang/Object;)V

    goto :goto_1

    .line 111744
    :cond_2
    const-string v3, "FSPA"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad fragment at key "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 111745
    :cond_3
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 111724
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 111746
    move-object v0, p3

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 111747
    iget-object v1, p0, LX/0gF;->d:LX/0hH;

    if-nez v1, :cond_0

    .line 111748
    iget-object v1, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iput-object v1, p0, LX/0gF;->d:LX/0hH;

    .line 111749
    :cond_0
    invoke-virtual {p0, p3}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_1

    .line 111750
    :goto_0
    iget-object v1, p0, LX/0gF;->c:Ljava/util/ArrayList;

    add-int/lit8 v2, p2, 0x1

    invoke-static {v1, v2}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111751
    iget-object v1, p0, LX/0gF;->a:Ljava/util/ArrayList;

    add-int/lit8 v2, p2, 0x1

    invoke-static {v1, v2}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111752
    iget-object v1, p0, LX/0gF;->c:Ljava/util/ArrayList;

    iget-object v2, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v2, v0}, LX/0gc;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 111753
    iget-object v1, p0, LX/0gF;->a:Ljava/util/ArrayList;

    .line 111754
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    move-object v2, v2

    .line 111755
    invoke-virtual {v1, p2, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 111756
    iget-object v1, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v1, p2}, LX/0YU;->b(I)V

    .line 111757
    iget-object v1, p0, LX/0gF;->d:LX/0hH;

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 111758
    return-void

    .line 111759
    :cond_1
    invoke-virtual {p0, p3}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result p2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 111721
    check-cast p2, Landroid/support/v4/app/Fragment;

    .line 111722
    iget-object v0, p2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 111723
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111720
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 111671
    iget-object v0, p0, LX/0gF;->d:LX/0hH;

    if-eqz v0, :cond_0

    .line 111672
    iget-object v0, p0, LX/0gF;->d:LX/0hH;

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 111673
    const/4 v0, 0x0

    iput-object v0, p0, LX/0gF;->d:LX/0hH;

    .line 111674
    iget-object v0, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 111675
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 111712
    check-cast p3, Landroid/support/v4/app/Fragment;

    .line 111713
    iget-object v0, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 111714
    iget-object v0, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 111715
    iget-object v0, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0gF;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 111716
    :cond_0
    if-eqz p3, :cond_1

    .line 111717
    const/4 v0, 0x1

    invoke-static {p3, v0}, LX/0gF;->a(Landroid/support/v4/app/Fragment;Z)V

    .line 111718
    :cond_1
    iput-object p3, p0, LX/0gF;->f:Landroid/support/v4/app/Fragment;

    .line 111719
    :cond_2
    return-void
.end method

.method public final e(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 111711
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final kV_()V
    .locals 8

    .prologue
    .line 111691
    new-instance v5, LX/0YU;

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    invoke-direct {v5, v0}, LX/0YU;-><init>(I)V

    .line 111692
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 111693
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    invoke-static {v6, v0}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111694
    iget-object v0, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v1

    invoke-static {v0, v1}, LX/0gF;->a(Ljava/util/ArrayList;I)V

    .line 111695
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 111696
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->e(I)I

    move-result v4

    .line 111697
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 111698
    invoke-virtual {p0, v0}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result v3

    .line 111699
    iget-object v1, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment$SavedState;

    .line 111700
    const/4 v7, -0x2

    if-eq v3, v7, :cond_1

    .line 111701
    if-ltz v3, :cond_0

    .line 111702
    :goto_1
    invoke-virtual {v5, v3, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 111703
    invoke-virtual {v6, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 111704
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v3, v4

    .line 111705
    goto :goto_1

    .line 111706
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v6, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 111707
    :cond_2
    iput-object v5, p0, LX/0gF;->e:LX/0YU;

    .line 111708
    iput-object v6, p0, LX/0gF;->c:Ljava/util/ArrayList;

    .line 111709
    invoke-super {p0}, LX/0gG;->kV_()V

    .line 111710
    return-void
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 111676
    const/4 v0, 0x0

    .line 111677
    iget-object v1, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 111678
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111679
    iget-object v1, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/support/v4/app/Fragment$SavedState;

    .line 111680
    iget-object v2, p0, LX/0gF;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 111681
    const-string v2, "states"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 111682
    :cond_0
    const/4 v1, 0x0

    move-object v2, v0

    :goto_0
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 111683
    iget-object v0, p0, LX/0gF;->e:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 111684
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 111685
    if-nez v2, :cond_1

    .line 111686
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 111687
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "f"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 111688
    iget-object v4, p0, LX/0gF;->b:LX/0gc;

    invoke-virtual {v4, v2, v3, v0}, LX/0gc;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 111689
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111690
    :cond_3
    return-object v2
.end method
