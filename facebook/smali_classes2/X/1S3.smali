.class public abstract LX/1S3;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final c:Lcom/facebook/csslayout/YogaMeasureFunction;


# instance fields
.field public b:Z

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 247476
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, LX/1S3;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 247477
    new-instance v0, LX/1S4;

    invoke-direct {v0}, LX/1S4;-><init>()V

    sput-object v0, LX/1S3;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 247478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247479
    sget-object v0, LX/1S3;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, LX/1S3;->d:I

    .line 247480
    return-void
.end method

.method public static a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1De;",
            "I[",
            "Ljava/lang/Object;",
            ")",
            "LX/1dQ",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 247481
    new-instance v0, LX/1dQ;

    iget-object v1, p0, LX/1De;->f:LX/1X1;

    invoke-direct {v0, v1, p1, p2}, LX/1dQ;-><init>(LX/1X1;I[Ljava/lang/Object;)V

    move-object v0, v0

    .line 247482
    return-object v0
.end method

.method public static a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1X1",
            "<*>;I[",
            "Ljava/lang/Object;",
            ")",
            "LX/1dQ",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 247483
    new-instance v0, LX/1dQ;

    invoke-direct {v0, p0, p1, p2}, LX/1dQ;-><init>(LX/1X1;I[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)",
            "LX/3lz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 247484
    sget-object v0, LX/1cy;->r:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lz;

    .line 247485
    if-nez v0, :cond_0

    .line 247486
    new-instance v0, LX/3lz;

    invoke-direct {v0}, LX/3lz;-><init>()V

    .line 247487
    :cond_0
    move-object v0, v0

    .line 247488
    iput-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    .line 247489
    iput-object p1, v0, LX/3lz;->b:Ljava/lang/Object;

    .line 247490
    return-object v0
.end method

.method public static a(LX/1np;)V
    .locals 0

    .prologue
    .line 247491
    invoke-static {p0}, LX/1cy;->a(LX/1np;)V

    .line 247492
    return-void
.end method

.method public static o()LX/1mx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1mx;"
        }
    .end annotation

    .prologue
    .line 247493
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public a(IILX/1X1;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LX/1X1",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 247494
    const/high16 v0, -0x80000000

    return v0
.end method

.method public a(LX/1X1;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 247495
    const/4 v0, 0x0

    return v0
.end method

.method public a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 247496
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;LX/1X1;Z)LX/1Dg;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 247509
    invoke-static {p2}, LX/1X1;->f(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    move v1, v2

    .line 247510
    :goto_0
    iget-object v0, p1, LX/1De;->f:LX/1X1;

    move-object v4, v0

    .line 247511
    iput-object p2, p1, LX/1De;->f:LX/1X1;

    .line 247512
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "createLayout:"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1X1;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1mm;->a(Ljava/lang/String;)V

    .line 247513
    if-eqz v1, :cond_2

    .line 247514
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, LX/1cy;->a(LX/1De;Landroid/content/res/Resources;)LX/1Dg;

    move-result-object v0

    .line 247515
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/1Dg;->o:Z

    .line 247516
    :goto_1
    invoke-static {}, LX/1mm;->a()V

    .line 247517
    iput-object v4, p1, LX/1De;->f:LX/1X1;

    .line 247518
    if-nez v0, :cond_4

    .line 247519
    sget-object v0, LX/1De;->a:LX/1Dg;

    .line 247520
    :cond_0
    :goto_2
    return-object v0

    :cond_1
    move v1, v3

    .line 247521
    goto :goto_0

    .line 247522
    :cond_2
    invoke-static {p2}, LX/1X1;->e(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247523
    iget v0, p1, LX/1De;->i:I

    move v0, v0

    .line 247524
    iget v5, p1, LX/1De;->j:I

    move v5, v5

    .line 247525
    invoke-virtual {p0, p1, v0, v5, p2}, LX/1S3;->b(LX/1De;IILX/1X1;)LX/1Dg;

    move-result-object v0

    check-cast v0, LX/1Dg;

    goto :goto_1

    .line 247526
    :cond_3
    invoke-virtual {p0, p1, p2}, LX/1S3;->a(LX/1De;LX/1X1;)LX/1Dg;

    move-result-object v0

    check-cast v0, LX/1Dg;

    goto :goto_1

    .line 247527
    :cond_4
    iget-object v4, v0, LX/1Dg;->l:LX/1X1;

    move-object v4, v4

    .line 247528
    if-nez v4, :cond_6

    .line 247529
    iput-object p2, v0, LX/1Dg;->l:LX/1X1;

    .line 247530
    invoke-virtual {p0}, LX/1S3;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {p2}, LX/1X1;->c(LX/1X1;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 247531
    :goto_3
    if-nez v2, :cond_5

    if-eqz v1, :cond_6

    .line 247532
    :cond_5
    sget-object v2, LX/1S3;->c:Lcom/facebook/csslayout/YogaMeasureFunction;

    .line 247533
    iget-object v3, v0, LX/1Dg;->a:LX/1mn;

    invoke-interface {v3, v2}, LX/1mn;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 247534
    :cond_6
    if-nez v1, :cond_0

    .line 247535
    invoke-virtual {p0, p1, p2}, LX/1S3;->b(LX/1De;LX/1X1;)V

    goto :goto_2

    :cond_7
    move v2, v3

    .line 247536
    goto :goto_3
.end method

.method public final a(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247497
    invoke-virtual {p0, p1}, LX/1S3;->b(LX/1De;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 247474
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/components/ComponentLayout;",
            "II",
            "LX/1no;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/16 v0, 0x1e

    const v1, 0x13eb9d44

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 247498
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "You must override onMeasure() if you return true in canMeasure(), ComponentLifecycle is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 247499
    iget-object v3, p6, LX/1X1;->e:LX/1S3;

    move-object v3, v3

    .line 247500
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x1f

    const v3, 0x2a303512

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1
.end method

.method public a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/components/ComponentLayout;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247501
    return-void
.end method

.method public a(LX/1X1;LX/1X1;)V
    .locals 0

    .prologue
    .line 247502
    return-void
.end method

.method public a(LX/3sp;IIILX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3sp;",
            "III",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247503
    return-void
.end method

.method public a(LX/3sp;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3sp;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247504
    return-void
.end method

.method public b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 247505
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/1De;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 247506
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to mount a MountSpec that doesn\'t implement @OnCreateMountContext"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(LX/1De;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247475
    return-void
.end method

.method public final b(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1c05267c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 247507
    invoke-virtual {p0, p1, p2, p3}, LX/1S3;->g(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 247508
    const/16 v1, 0x1f

    const v2, 0x3a7590a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1X1;LX/1X1;)Z
    .locals 1

    .prologue
    .line 247452
    invoke-virtual {p0}, LX/1S3;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247453
    invoke-virtual {p0, p1, p2}, LX/1S3;->c(LX/1X1;LX/1X1;)Z

    move-result v0

    .line 247454
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(LX/1De;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247463
    return-void
.end method

.method public final c(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6936fd9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 247461
    invoke-virtual {p0, p1, p2, p3}, LX/1S3;->h(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 247462
    const/16 v1, 0x1f

    const v2, 0x421aeca7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 247460
    const/4 v0, 0x0

    return v0
.end method

.method public c(LX/1X1;LX/1X1;)Z
    .locals 1

    .prologue
    .line 247459
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(LX/1De;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247458
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 247457
    const/4 v0, 0x0

    return v0
.end method

.method public e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247456
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 247455
    const/4 v0, 0x0

    return v0
.end method

.method public f()LX/1mv;
    .locals 1

    .prologue
    .line 247451
    sget-object v0, LX/1mv;->NONE:LX/1mv;

    return-object v0
.end method

.method public f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247464
    return-void
.end method

.method public g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247465
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 247466
    const/4 v0, 0x0

    return v0
.end method

.method public h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/Object;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 247467
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 247468
    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 247469
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 247470
    const/4 v0, 0x0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 247471
    const/4 v0, 0x0

    return v0
.end method

.method public n()I
    .locals 1

    .prologue
    .line 247472
    const/16 v0, 0xf

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 247473
    const/4 v0, 0x0

    return v0
.end method
