.class public LX/0t9;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:LX/0tA;

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 154006
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/0t9;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 153799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153800
    iput-object p1, p0, LX/0t9;->b:LX/0tA;

    .line 153801
    iput-object p2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 153802
    sget-object v0, LX/0t9;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, LX/0t9;->d:I

    .line 153803
    return-void
.end method

.method public static a(LX/0QB;)LX/0t9;
    .locals 1

    .prologue
    .line 154005
    invoke-static {p0}, LX/0t9;->b(LX/0QB;)LX/0t9;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/flatbuffers/MutableFlattenable;LX/3Bq;Ljava/lang/String;)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const v6, 0x850013

    .line 153986
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v6, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153987
    :try_start_0
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850013

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153988
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850013

    iget v2, p0, LX/0t9;->d:I

    const-string v3, "model_row_type"

    invoke-interface {v0, v1, v2, v3, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153989
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850013

    iget v2, p0, LX/0t9;->d:I

    const-string v3, "model_class_name"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153990
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850013

    iget v2, p0, LX/0t9;->d:I

    const-string v3, "visitor_name"

    invoke-static {p2}, LX/0t9;->a(LX/3Bq;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153991
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p2, p1, v0}, LX/3Bq;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 153992
    if-eqz v0, :cond_1

    .line 153993
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 153994
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 153995
    if-eq v1, v2, :cond_1

    .line 153996
    new-instance v0, LX/4KX;

    invoke-static {p2}, LX/0t9;->a(LX/3Bq;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v1, v2}, LX/4KX;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153997
    :catch_0
    move-exception v0

    .line 153998
    :try_start_1
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850013

    iget v3, p0, LX/0t9;->d:I

    const/4 v4, 0x3

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153999
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154000
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v6, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0

    .line 154001
    :cond_1
    if-eqz v0, :cond_2

    :try_start_2
    const-string v1, "confirmed"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x850013

    iget v3, p0, LX/0t9;->d:I

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154002
    invoke-static {p1, v0}, LX/0t9;->a(Lcom/facebook/flatbuffers/MutableFlattenable;Lcom/facebook/flatbuffers/MutableFlattenable;)Ljava/lang/String;

    move-result-object v1

    .line 154003
    iget-object v2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850013

    iget v4, p0, LX/0t9;->d:I

    const-string v5, "model_update"

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154004
    :cond_2
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v6, v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    return-object v0
.end method

.method private static a(LX/3Bq;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 153983
    if-nez p0, :cond_0

    .line 153984
    const-string v0, "null"

    .line 153985
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/3Bq;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CacheVisitor %s has no analytics name"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/flatbuffers/MutableFlattenable;Lcom/facebook/flatbuffers/MutableFlattenable;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$RowUpdateMethod;
    .end annotation

    .prologue
    .line 153976
    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    .line 153977
    if-eqz v0, :cond_0

    if-eq p1, p0, :cond_1

    .line 153978
    :cond_0
    const-string v0, "reflatten"

    .line 153979
    :goto_0
    return-object v0

    .line 153980
    :cond_1
    invoke-virtual {v0}, LX/15i;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, LX/15i;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153981
    :cond_2
    const-string v0, "delta"

    goto :goto_0

    .line 153982
    :cond_3
    const-string v0, "not_updated"

    goto :goto_0
.end method

.method private a(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;TITERATOR;",
            "Ljava/lang/String;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const v5, 0x85000c

    .line 153965
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153966
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "store"

    invoke-interface {p1}, LX/2vL;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v5, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153967
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "model_row_type"

    invoke-interface {v0, v5, v1, v2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153968
    :try_start_0
    invoke-interface {p1, p2, p3, p4}, LX/2vL;->a(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153969
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v5, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153970
    return-void

    .line 153971
    :catch_0
    move-exception v0

    .line 153972
    :try_start_1
    const-string v1, "ConsistentModelWriter"

    const-string v2, "Unable to re-flatten"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153973
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85000c

    iget v3, p0, LX/0t9;->d:I

    const/4 v4, 0x3

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153974
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153975
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v5, v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0
.end method

.method private a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 6
    .param p3    # LX/3Bq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const v4, 0x850011

    .line 153949
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153950
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "store"

    invoke-interface {p1}, LX/2vL;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153951
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "reason"

    invoke-interface {v0, v4, v1, v2, p5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153952
    invoke-interface {p1}, LX/2vL;->a()V

    .line 153953
    :try_start_0
    invoke-direct/range {p0 .. p5}, LX/0t9;->b(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V

    .line 153954
    invoke-interface {p1}, LX/2vL;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153955
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153956
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153957
    :goto_0
    return-void

    .line 153958
    :catch_0
    move-exception v0

    .line 153959
    :try_start_1
    invoke-static {v0}, LX/0t9;->a(Ljava/lang/Exception;)V

    .line 153960
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x850011

    iget v2, p0, LX/0t9;->d:I

    const/4 v3, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153961
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153962
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 153963
    :catchall_0
    move-exception v0

    invoke-interface {p1}, LX/2vL;->c()V

    .line 153964
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v4, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0
.end method

.method private static a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 153934
    instance-of v0, p0, LX/4VR;

    if-eqz v0, :cond_0

    .line 153935
    :goto_0
    return-void

    .line 153936
    :cond_0
    instance-of v0, p0, LX/4Bu;

    if-eqz v0, :cond_2

    .line 153937
    const-string v0, "FlatBufferCorruption"

    .line 153938
    :cond_1
    :goto_1
    move-object v0, v0

    .line 153939
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConsistentModelWriter_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, p0, v0, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 153940
    :cond_2
    instance-of v0, p0, Landroid/database/sqlite/SQLiteDatabaseCorruptException;

    if-eqz v0, :cond_3

    .line 153941
    const-string v0, "SQLiteCorrupt"

    goto :goto_1

    .line 153942
    :cond_3
    instance-of v0, p0, Landroid/database/sqlite/SQLiteException;

    if-eqz v0, :cond_4

    .line 153943
    const-string v0, "SQLiteException"

    goto :goto_1

    .line 153944
    :cond_4
    instance-of v0, p0, LX/4KX;

    if-eqz v0, :cond_5

    .line 153945
    const-string v0, "ModelTypeChanged"

    goto :goto_1

    .line 153946
    :cond_5
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 153947
    if-nez v0, :cond_1

    .line 153948
    const-string v0, "UNKNOWN"

    goto :goto_1
.end method

.method private static a(LX/0t9;LX/2vL;Ljava/lang/Object;LX/3Bq;Ljava/util/Collection;)Z
    .locals 10
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;TITERATOR;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const v9, 0x850010

    const/4 v6, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 153902
    invoke-interface {p1, p2}, LX/2vL;->d(Ljava/lang/Object;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    .line 153903
    if-eqz p3, :cond_8

    .line 153904
    const-string v0, "confirmed"

    invoke-direct {p0, v1, p3, v0}, LX/0t9;->a(Lcom/facebook/flatbuffers/MutableFlattenable;LX/3Bq;Ljava/lang/String;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 153905
    :goto_0
    if-nez v0, :cond_0

    .line 153906
    invoke-interface {p1, p2}, LX/2vL;->f(Ljava/lang/Object;)V

    .line 153907
    :goto_1
    return v2

    .line 153908
    :cond_0
    invoke-static {v1, v0}, LX/0t9;->a(Lcom/facebook/flatbuffers/MutableFlattenable;Lcom/facebook/flatbuffers/MutableFlattenable;)Ljava/lang/String;

    move-result-object v5

    .line 153909
    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    move v1, v6

    :goto_2
    packed-switch v1, :pswitch_data_0

    move v1, v3

    move-object v4, v0

    .line 153910
    :goto_3
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v7, p0, LX/0t9;->d:I

    const-string v8, "confirmed_model_update"

    invoke-interface {v0, v9, v7, v8, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153911
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v5, v4

    :cond_2
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Bq;

    .line 153912
    invoke-interface {v0}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v8

    invoke-interface {p1, p2, v8}, LX/2vL;->a(Ljava/lang/Object;Ljava/util/Set;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 153913
    const-string v8, "optimistic"

    invoke-direct {p0, v5, v0, v8}, LX/0t9;->a(Lcom/facebook/flatbuffers/MutableFlattenable;LX/3Bq;Ljava/lang/String;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 153914
    if-eqz v0, :cond_4

    move-object v5, v0

    .line 153915
    goto :goto_4

    .line 153916
    :sswitch_0
    const-string v1, "reflatten"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    goto :goto_2

    :sswitch_1
    const-string v1, "delta"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_2

    .line 153917
    :pswitch_0
    invoke-interface {p1, p2, v0}, LX/2vL;->a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 153918
    const-string v1, "confirmed"

    invoke-direct {p0, p1, p2, v1, v0}, LX/0t9;->a(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    move v1, v2

    move-object v4, v0

    .line 153919
    goto :goto_3

    .line 153920
    :pswitch_1
    const-string v1, "confirmed"

    invoke-direct {p0, p1, p2, v1, v0}, LX/0t9;->b(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    .line 153921
    invoke-interface {p1, p2, v0}, LX/2vL;->b(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    move v1, v2

    move-object v4, v0

    .line 153922
    goto :goto_3

    :cond_3
    move-object v0, v5

    .line 153923
    :cond_4
    if-nez v0, :cond_5

    .line 153924
    invoke-interface {p1, p2}, LX/2vL;->g(Ljava/lang/Object;)V

    goto :goto_1

    .line 153925
    :cond_5
    invoke-static {v4, v0}, LX/0t9;->a(Lcom/facebook/flatbuffers/MutableFlattenable;Lcom/facebook/flatbuffers/MutableFlattenable;)Ljava/lang/String;

    move-result-object v4

    .line 153926
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_1

    :cond_6
    move v3, v6

    :goto_5
    packed-switch v3, :pswitch_data_1

    .line 153927
    invoke-interface {p1, p2}, LX/2vL;->e(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 153928
    invoke-interface {p1, p2}, LX/2vL;->g(Ljava/lang/Object;)V

    .line 153929
    :goto_6
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v3, "optimistic_model_update"

    invoke-interface {v0, v9, v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 153930
    :sswitch_2
    const-string v5, "reflatten"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_5

    :sswitch_3
    const-string v3, "delta"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v2

    goto :goto_5

    .line 153931
    :pswitch_2
    invoke-interface {p1, p2, v0}, LX/2vL;->a(Ljava/lang/Object;Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v0

    .line 153932
    const-string v1, "optimistic"

    invoke-direct {p0, p1, p2, v1, v0}, LX/0t9;->a(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    goto :goto_6

    .line 153933
    :pswitch_3
    const-string v1, "optimistic"

    invoke-direct {p0, p1, p2, v1, v0}, LX/0t9;->b(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V

    goto :goto_6

    :cond_7
    move v2, v1

    goto :goto_6

    :cond_8
    move-object v0, v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2c1b7b8f -> :sswitch_0
        0x5b0bbb8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x2c1b7b8f -> :sswitch_2
        0x5b0bbb8 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/0t9;
    .locals 3

    .prologue
    .line 153900
    new-instance v2, LX/0t9;

    invoke-static {p0}, LX/0tA;->a(LX/0QB;)LX/0tA;

    move-result-object v0

    check-cast v0, LX/0tA;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v2, v0, v1}, LX/0t9;-><init>(LX/0tA;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 153901
    return-object v2
.end method

.method private b(LX/2vL;Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/consistency/db/ConsistentModelWriter$ModelRowType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;TITERATOR;",
            "Ljava/lang/String;",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const v5, 0x85000d

    .line 153889
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153890
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "store"

    invoke-interface {p1}, LX/2vL;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v5, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153891
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "model_row_type"

    invoke-interface {v0, v5, v1, v2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153892
    :try_start_0
    invoke-interface {p1, p2, p3, p4}, LX/2vL;->b(Ljava/lang/Object;Ljava/lang/String;Lcom/facebook/flatbuffers/MutableFlattenable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153893
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v5, v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153894
    return-void

    .line 153895
    :catch_0
    move-exception v0

    .line 153896
    :try_start_1
    const-string v1, "ConsistentModelWriter"

    const-string v2, "Unable to update delta buffer"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153897
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x85000d

    iget v3, p0, LX/0t9;->d:I

    const/4 v4, 0x3

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153898
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153899
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v5, v2, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0
.end method

.method private b(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 12
    .param p3    # LX/3Bq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 153843
    const/4 v4, 0x0

    .line 153844
    const/4 v2, 0x0

    .line 153845
    :try_start_0
    invoke-interface {p1, p2}, LX/2vL;->a(Ljava/util/Collection;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v3

    .line 153846
    :try_start_1
    invoke-interface {p1, v3}, LX/2vL;->a(Ljava/lang/Object;)I

    move-result v5

    .line 153847
    const/4 v1, 0x0

    move v2, v4

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_3

    .line 153848
    invoke-interface {p1, v3, v4}, LX/2vL;->a(Ljava/lang/Object;I)V

    .line 153849
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    invoke-interface {v1, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153850
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    const-string v8, "store"

    invoke-interface {p1}, LX/2vL;->d()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v6, v7, v8, v9}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153851
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    const-string v8, "reason"

    move-object/from16 v0, p5

    invoke-interface {v1, v6, v7, v8, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153852
    invoke-interface {p1}, LX/2vL;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153853
    :try_start_2
    move-object/from16 v0, p4

    invoke-static {p0, p1, v3, p3, v0}, LX/0t9;->a(LX/0t9;LX/2vL;Ljava/lang/Object;LX/3Bq;Ljava/util/Collection;)Z

    move-result v1

    .line 153854
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    invoke-interface {v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 153855
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    const-string v9, "confirmed_visitor_name"

    invoke-static {p3}, LX/0t9;->a(LX/3Bq;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153856
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    const-string v9, "optimistic_visitors_count"

    invoke-interface/range {p4 .. p4}, Ljava/util/Collection;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153857
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    const-string v9, "model_class_name"

    invoke-interface {p1, v3}, LX/2vL;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153858
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    const-string v9, "row_tags_count"

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153859
    iget-object v6, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v7, 0x850010

    iget v8, p0, LX/0t9;->d:I

    const-string v9, "row_was_updated"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v7, v8, v9, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153860
    :cond_0
    if-eqz v1, :cond_1

    .line 153861
    add-int/lit8 v2, v2, 0x1

    .line 153862
    invoke-interface {p1, v3}, LX/2vL;->h(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    move v1, v2

    .line 153863
    :try_start_3
    invoke-interface {p1}, LX/2vL;->b()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 153864
    :try_start_4
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153865
    iget-object v2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    const/4 v8, 0x2

    invoke-interface {v2, v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 153866
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto/16 :goto_0

    .line 153867
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v2

    move-object v2, v11

    .line 153868
    :goto_2
    :try_start_5
    invoke-static {v2}, LX/0t9;->a(Ljava/lang/Exception;)V

    .line 153869
    iget-object v2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    const/4 v8, 0x3

    invoke-interface {v2, v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 153870
    :try_start_6
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153871
    iget-object v2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x850010

    iget v7, p0, LX/0t9;->d:I

    const/4 v8, 0x2

    invoke-interface {v2, v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 153872
    :catchall_0
    move-exception v1

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_2

    .line 153873
    :try_start_7
    invoke-interface {p1, v2}, LX/2vL;->b(Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 153874
    :cond_2
    :goto_4
    throw v1

    .line 153875
    :catchall_1
    move-exception v1

    :try_start_8
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153876
    iget-object v2, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x850010

    iget v5, p0, LX/0t9;->d:I

    const/4 v6, 0x2

    invoke-interface {v2, v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 153877
    :cond_3
    if-eqz v3, :cond_4

    .line 153878
    :try_start_9
    invoke-interface {p1, v3}, LX/2vL;->b(Ljava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 153879
    :cond_4
    :goto_5
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 153880
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    const-string v6, "confirmed_visitor_name"

    invoke-static {p3}, LX/0t9;->a(LX/3Bq;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153881
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    const-string v6, "optimistic_visitors_count"

    invoke-interface/range {p4 .. p4}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153882
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    const-string v6, "tags_to_visit_count"

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153883
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    const-string v6, "rows_visited_count"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v3, v4, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153884
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x850011

    iget v4, p0, LX/0t9;->d:I

    const-string v5, "rows_updated_count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v4, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153885
    :cond_5
    return-void

    .line 153886
    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    .line 153887
    :catchall_2
    move-exception v1

    goto/16 :goto_3

    .line 153888
    :catch_3
    move-exception v2

    goto/16 :goto_2
.end method

.method private b(LX/2vL;[J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;[J)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 153825
    array-length v0, p2

    if-nez v0, :cond_1

    .line 153826
    :cond_0
    :goto_0
    return-void

    .line 153827
    :cond_1
    iget-object v0, p0, LX/0t9;->b:LX/0tA;

    invoke-virtual {v0}, LX/0tA;->b()Ljava/util/Collection;

    move-result-object v2

    .line 153828
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153829
    :try_start_0
    invoke-interface {p1, p2}, LX/2vL;->a([J)Ljava/lang/Object;

    move-result-object v1

    .line 153830
    invoke-interface {p1, v1}, LX/2vL;->a(Ljava/lang/Object;)I

    move-result v3

    .line 153831
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    .line 153832
    invoke-interface {p1, v1, v0}, LX/2vL;->a(Ljava/lang/Object;I)V

    .line 153833
    const/4 v4, 0x0

    invoke-static {p0, p1, v1, v4, v2}, LX/0t9;->a(LX/0t9;LX/2vL;Ljava/lang/Object;LX/3Bq;Ljava/util/Collection;)Z

    move-result v4

    .line 153834
    if-eqz v4, :cond_2

    .line 153835
    invoke-interface {p1, v1}, LX/2vL;->h(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153836
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 153837
    :cond_3
    if-eqz v1, :cond_0

    .line 153838
    :try_start_1
    invoke-interface {p1, v1}, LX/2vL;->b(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 153839
    :catch_0
    goto :goto_0

    .line 153840
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 153841
    :try_start_2
    invoke-interface {p1, v1}, LX/2vL;->b(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 153842
    :cond_4
    :goto_2
    throw v0

    :catch_1
    goto :goto_2
.end method


# virtual methods
.method public final a(LX/2vL;Ljava/util/Collection;LX/3Bq;)V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vL;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 153823
    iget-object v0, p0, LX/0t9;->b:LX/0tA;

    invoke-virtual {v0}, LX/0tA;->b()Ljava/util/Collection;

    move-result-object v4

    const-string v5, "visitAllInCache"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V

    .line 153824
    return-void
.end method

.method public final a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;)V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vL;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3Bq;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153821
    const-string v5, "applyConfirmed"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V

    .line 153822
    return-void
.end method

.method public final a(LX/2vL;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ITERATOR:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2vL",
            "<TITERATOR;>;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/3Bq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153819
    const/4 v3, 0x0

    const-string v5, "applyOptimistic"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/0t9;->a(LX/2vL;Ljava/util/Collection;LX/3Bq;Ljava/util/Collection;Ljava/lang/String;)V

    .line 153820
    return-void
.end method

.method public final a(LX/2vL;[J)V
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const v4, 0x85001d

    .line 153804
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 153805
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    const-string v2, "store"

    invoke-interface {p1}, LX/2vL;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 153806
    invoke-interface {p1}, LX/2vL;->a()V

    .line 153807
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/0t9;->b(LX/2vL;[J)V

    .line 153808
    invoke-interface {p1}, LX/2vL;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153809
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153810
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 153811
    :goto_0
    return-void

    .line 153812
    :catch_0
    move-exception v0

    .line 153813
    :try_start_1
    invoke-static {v0}, LX/0t9;->a(Ljava/lang/Exception;)V

    .line 153814
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x85001d

    iget v2, p0, LX/0t9;->d:I

    const/4 v3, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153815
    invoke-interface {p1}, LX/2vL;->c()V

    .line 153816
    iget-object v0, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/0t9;->d:I

    invoke-interface {v0, v4, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    goto :goto_0

    .line 153817
    :catchall_0
    move-exception v0

    invoke-interface {p1}, LX/2vL;->c()V

    .line 153818
    iget-object v1, p0, LX/0t9;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, LX/0t9;->d:I

    invoke-interface {v1, v4, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v0
.end method
