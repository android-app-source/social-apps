.class public LX/10H;
.super Ljava/io/FilterWriter;
.source ""


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 0

    .prologue
    .line 168630
    invoke-direct {p0, p1}, Ljava/io/FilterWriter;-><init>(Ljava/io/Writer;)V

    .line 168631
    return-void
.end method

.method public static a(Ljava/io/Writer;C)V
    .locals 4

    .prologue
    const/16 v0, 0x5c

    .line 168643
    sparse-switch p1, :sswitch_data_0

    .line 168644
    const/16 v0, 0x1f

    if-le p1, v0, :cond_0

    const/16 v0, 0x2028

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2029

    if-ne p1, v0, :cond_1

    .line 168645
    :cond_0
    const-string v0, "\\u%04x"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 168646
    :goto_0
    return-void

    .line 168647
    :sswitch_0
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168648
    :cond_1
    invoke-virtual {p0, p1}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    .line 168649
    :sswitch_1
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168650
    const/16 v0, 0x74

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    .line 168651
    :sswitch_2
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168652
    const/16 v0, 0x62

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    .line 168653
    :sswitch_3
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168654
    const-string v0, "n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 168655
    :sswitch_4
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168656
    const/16 v0, 0x72

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    .line 168657
    :sswitch_5
    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    .line 168658
    const/16 v0, 0x66

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0x9 -> :sswitch_1
        0xa -> :sswitch_3
        0xc -> :sswitch_5
        0xd -> :sswitch_4
        0x22 -> :sswitch_0
        0x5c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final write(I)V
    .locals 3

    .prologue
    .line 168640
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 168641
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    int-to-char v2, p1

    invoke-static {v0, v2}, LX/10H;->a(Ljava/io/Writer;C)V

    .line 168642
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 168636
    move v0, p2

    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 168637
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LX/10H;->write(I)V

    .line 168638
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168639
    :cond_0
    return-void
.end method

.method public final write([CII)V
    .locals 2

    .prologue
    .line 168632
    move v0, p2

    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 168633
    aget-char v1, p1, v0

    invoke-virtual {p0, v1}, LX/10H;->write(I)V

    .line 168634
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168635
    :cond_0
    return-void
.end method
