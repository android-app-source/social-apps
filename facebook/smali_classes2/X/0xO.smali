.class public final LX/0xO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:Ljava/util/ArrayList;

.field public final synthetic d:LX/0xK;

.field public final synthetic e:Z

.field public final synthetic f:Landroid/support/v4/app/Fragment;

.field public final synthetic g:Landroid/support/v4/app/Fragment;

.field public final synthetic h:Landroid/support/v4/app/BackStackRecord;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/BackStackRecord;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;LX/0xK;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 163058
    iput-object p1, p0, LX/0xO;->h:Landroid/support/v4/app/BackStackRecord;

    iput-object p2, p0, LX/0xO;->a:Landroid/view/View;

    iput-object p3, p0, LX/0xO;->b:Ljava/lang/Object;

    iput-object p4, p0, LX/0xO;->c:Ljava/util/ArrayList;

    iput-object p5, p0, LX/0xO;->d:LX/0xK;

    iput-boolean p6, p0, LX/0xO;->e:Z

    iput-object p7, p0, LX/0xO;->f:Landroid/support/v4/app/Fragment;

    iput-object p8, p0, LX/0xO;->g:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 8

    .prologue
    .line 163059
    iget-object v0, p0, LX/0xO;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 163060
    iget-object v0, p0, LX/0xO;->b:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 163061
    iget-object v0, p0, LX/0xO;->b:Ljava/lang/Object;

    iget-object v1, p0, LX/0xO;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1}, LX/0xL;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 163062
    iget-object v0, p0, LX/0xO;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 163063
    iget-object v0, p0, LX/0xO;->h:Landroid/support/v4/app/BackStackRecord;

    iget-object v1, p0, LX/0xO;->d:LX/0xK;

    iget-boolean v2, p0, LX/0xO;->e:Z

    iget-object v3, p0, LX/0xO;->f:Landroid/support/v4/app/Fragment;

    const/4 v6, 0x1

    .line 163064
    new-instance v4, LX/026;

    invoke-direct {v4}, LX/026;-><init>()V

    .line 163065
    iget-object v5, v3, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v5, v5

    .line 163066
    if-eqz v5, :cond_0

    .line 163067
    iget-object v7, v0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    .line 163068
    invoke-static {v4, v5}, LX/0xL;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 163069
    if-eqz v2, :cond_6

    .line 163070
    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->t:Ljava/util/ArrayList;

    iget-object v7, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-static {v5, v7, v4}, Landroid/support/v4/app/BackStackRecord;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;LX/026;)LX/026;

    move-result-object v4

    .line 163071
    :cond_0
    :goto_0
    move-object v4, v4

    .line 163072
    if-eqz v2, :cond_4

    .line 163073
    iget-object v5, v3, Landroid/support/v4/app/Fragment;->mExitTransitionCallback:LX/0xM;

    if-eqz v5, :cond_1

    .line 163074
    :cond_1
    invoke-static {v0, v1, v4, v6}, Landroid/support/v4/app/BackStackRecord;->a(Landroid/support/v4/app/BackStackRecord;LX/0xK;LX/026;Z)V

    .line 163075
    :goto_1
    move-object v5, v4

    .line 163076
    iget-object v0, p0, LX/0xO;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LX/0xO;->d:LX/0xK;

    iget-object v1, v1, LX/0xK;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163077
    iget-object v0, p0, LX/0xO;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, LX/026;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 163078
    iget-object v0, p0, LX/0xO;->b:Ljava/lang/Object;

    iget-object v1, p0, LX/0xO;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1}, LX/0xL;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 163079
    iget-object v0, p0, LX/0xO;->h:Landroid/support/v4/app/BackStackRecord;

    iget-object v1, p0, LX/0xO;->d:LX/0xK;

    .line 163080
    iget-object v2, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    invoke-virtual {v5}, LX/01J;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 163081
    iget-object v2, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 163082
    if-eqz v2, :cond_2

    .line 163083
    iget-object v3, v1, LX/0xK;->c:LX/0xS;

    iput-object v2, v3, LX/0xS;->a:Landroid/view/View;

    .line 163084
    :cond_2
    iget-object v2, p0, LX/0xO;->f:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, LX/0xO;->g:Landroid/support/v4/app/Fragment;

    iget-boolean v4, p0, LX/0xO;->e:Z

    .line 163085
    if-eqz v4, :cond_7

    iget-object v0, v3, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    .line 163086
    :goto_2
    if-eqz v0, :cond_3

    .line 163087
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v5}, LX/026;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 163088
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v5}, LX/026;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 163089
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 163090
    :cond_4
    iget-object v5, v3, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    if-eqz v5, :cond_5

    .line 163091
    :cond_5
    invoke-static {v1, v4, v6}, Landroid/support/v4/app/BackStackRecord;->b(LX/0xK;LX/026;Z)V

    goto :goto_1

    .line 163092
    :cond_6
    iget-object v5, v0, Landroid/support/v4/app/BackStackRecord;->u:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, LX/026;->a(Ljava/util/Collection;)Z

    goto :goto_0

    .line 163093
    :cond_7
    iget-object v0, v2, Landroid/support/v4/app/Fragment;->mEnterTransitionCallback:LX/0xM;

    goto :goto_2
.end method
