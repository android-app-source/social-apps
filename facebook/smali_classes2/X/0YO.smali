.class public LX/0YO;
.super LX/0Uq;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0YO;


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 81116
    const-string v0, "Gatekeeper Init Lock Held"

    invoke-direct {p0, p1, v0}, LX/0Uq;-><init>(LX/0Sh;Ljava/lang/String;)V

    .line 81117
    return-void
.end method

.method public static b(LX/0QB;)LX/0YO;
    .locals 4

    .prologue
    .line 81118
    sget-object v0, LX/0YO;->a:LX/0YO;

    if-nez v0, :cond_1

    .line 81119
    const-class v1, LX/0YO;

    monitor-enter v1

    .line 81120
    :try_start_0
    sget-object v0, LX/0YO;->a:LX/0YO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 81121
    if-eqz v2, :cond_0

    .line 81122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 81123
    new-instance p0, LX/0YO;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/0YO;-><init>(LX/0Sh;)V

    .line 81124
    move-object v0, p0

    .line 81125
    sput-object v0, LX/0YO;->a:LX/0YO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81126
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 81127
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 81128
    :cond_1
    sget-object v0, LX/0YO;->a:LX/0YO;

    return-object v0

    .line 81129
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 81130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
