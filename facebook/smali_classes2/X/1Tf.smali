.class public LX/1Tf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 253580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253581
    iput-object p1, p0, LX/1Tf;->a:LX/0Ot;

    .line 253582
    iput-object p2, p0, LX/1Tf;->b:LX/0Ot;

    .line 253583
    return-void
.end method

.method public static a(LX/0QB;)LX/1Tf;
    .locals 5

    .prologue
    .line 253569
    const-class v1, LX/1Tf;

    monitor-enter v1

    .line 253570
    :try_start_0
    sget-object v0, LX/1Tf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 253571
    sput-object v2, LX/1Tf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 253572
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253573
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 253574
    new-instance v3, LX/1Tf;

    const/16 v4, 0x9e1

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2137

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1Tf;-><init>(LX/0Ot;LX/0Ot;)V

    .line 253575
    move-object v0, v3

    .line 253576
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 253577
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Tf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253578
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 253579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 253560
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionCreativeContentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253561
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionLargeImageCreativeContentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253562
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedBackgroundColoredImageCreativeContentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253563
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionBrandedVideoCreativeContentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253564
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253565
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionTwoButtonWithDrawableFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253566
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionOneButtonFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253567
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/ContentBGColoredEmptyFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 253568
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 253557
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    iget-object v1, p0, LX/1Tf;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253558
    const-class v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    iget-object v1, p0, LX/1Tf;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 253559
    return-void
.end method
