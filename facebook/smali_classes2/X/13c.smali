.class public final LX/13c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/0jP;


# direct methods
.method public constructor <init>(LX/0jP;)V
    .locals 0

    .prologue
    .line 177030
    iput-object p1, p0, LX/13c;->a:LX/0jP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/13Z;LX/16C;)V
    .locals 12

    .prologue
    .line 177031
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13a;

    .line 177032
    iget-object v1, v0, LX/13a;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    .line 177033
    invoke-virtual {v1}, LX/0tK;->q()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0wm;->J:S

    const/4 v0, 0x1

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    :goto_0
    move v1, v2

    .line 177034
    move v0, v1

    .line 177035
    if-nez v0, :cond_0

    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x47f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177036
    :cond_0
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, LX/13c;->a:LX/0jP;

    iget-wide v2, v2, LX/0jP;->g:J

    sub-long v2, v0, v2

    .line 177037
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->h:LX/13Z;

    if-eq v0, p1, :cond_4

    sget-object v0, LX/16C;->AUTO_UPDATE:LX/16C;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13a;

    .line 177038
    iget-object v4, v0, LX/13a;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tK;

    const-wide/32 v10, 0xea60

    .line 177039
    invoke-virtual {v4}, LX/0tK;->q()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, v4, LX/0tK;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    sget-wide v8, LX/0wm;->K:J

    invoke-interface {v6, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v6

    :goto_1
    move-wide v4, v6

    .line 177040
    move-wide v0, v4

    .line 177041
    cmp-long v0, v2, v0

    if-gtz v0, :cond_1

    sget-object v0, LX/13Z;->DSM_INDICATOR_ENABLED_ON_STATE:LX/13Z;

    if-ne p1, v0, :cond_4

    .line 177042
    :cond_1
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->i:LX/0f7;

    invoke-interface {v0}, LX/0f7;->q()Z

    move-result v0

    if-nez v0, :cond_2

    .line 177043
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->i:LX/0f7;

    invoke-interface {v0}, LX/0f7;->r()V

    .line 177044
    :cond_2
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->i:LX/0f7;

    invoke-interface {v0}, LX/0f7;->t()V

    .line 177045
    sget-object v0, LX/16C;->AUTO_UPDATE:LX/16C;

    if-ne p2, v0, :cond_3

    .line 177046
    iget-object v1, p0, LX/13c;->a:LX/0jP;

    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 177047
    iput-wide v2, v1, LX/0jP;->g:J

    .line 177048
    :cond_3
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    .line 177049
    iput-object p1, v0, LX/0jP;->h:LX/13Z;

    .line 177050
    :cond_4
    iget-object v0, p0, LX/13c;->a:LX/0jP;

    iget-object v0, v0, LX/0jP;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x8;

    invoke-virtual {v0, p1}, LX/0x8;->a(LX/13Z;)V

    .line 177051
    return-void

    :cond_5
    iget-object v2, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0wm;->p:S

    const/4 v0, 0x0

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    goto/16 :goto_0

    :cond_6
    iget-object v6, v4, LX/0tK;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    sget-wide v8, LX/0wm;->q:J

    invoke-interface {v6, v8, v9, v10, v11}, LX/0ad;->a(JJ)J

    move-result-wide v6

    goto :goto_1
.end method
