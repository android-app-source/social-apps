.class public LX/1u5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;


# instance fields
.field public final appSpecificInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final clientIdentifier:Ljava/lang/String;

.field public final clientInfo:LX/1u6;

.field public final combinedPublishes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mL;",
            ">;"
        }
    .end annotation
.end field

.field public final getDiffsRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field public final password:Ljava/lang/String;

.field public final proxygenInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6mZ;",
            ">;"
        }
    .end annotation
.end field

.field public final willMessage:Ljava/lang/String;

.field public final willTopic:Ljava/lang/String;

.field public final zeroRatingTokenHash:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0xf

    const/16 v4, 0xb

    .line 337020
    new-instance v0, LX/1sv;

    const-string v1, "ConnectMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1u5;->b:LX/1sv;

    .line 337021
    new-instance v0, LX/1sw;

    const-string v1, "clientIdentifier"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->c:LX/1sw;

    .line 337022
    new-instance v0, LX/1sw;

    const-string v1, "willTopic"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->d:LX/1sw;

    .line 337023
    new-instance v0, LX/1sw;

    const-string v1, "willMessage"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->e:LX/1sw;

    .line 337024
    new-instance v0, LX/1sw;

    const-string v1, "clientInfo"

    const/16 v2, 0xc

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->f:LX/1sw;

    .line 337025
    new-instance v0, LX/1sw;

    const-string v1, "password"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->g:LX/1sw;

    .line 337026
    new-instance v0, LX/1sw;

    const-string v1, "getDiffsRequests"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->h:LX/1sw;

    .line 337027
    new-instance v0, LX/1sw;

    const-string v1, "proxygenInfo"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->i:LX/1sw;

    .line 337028
    new-instance v0, LX/1sw;

    const-string v1, "combinedPublishes"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->j:LX/1sw;

    .line 337029
    new-instance v0, LX/1sw;

    const-string v1, "zeroRatingTokenHash"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->k:LX/1sw;

    .line 337030
    new-instance v0, LX/1sw;

    const-string v1, "appSpecificInfo"

    const/16 v2, 0xd

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/1u5;->l:LX/1sw;

    .line 337031
    sput-boolean v6, LX/1u5;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1u6;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/1u6;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/util/List",
            "<",
            "LX/6mZ;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/6mL;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 337032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 337033
    iput-object p1, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    .line 337034
    iput-object p2, p0, LX/1u5;->willTopic:Ljava/lang/String;

    .line 337035
    iput-object p3, p0, LX/1u5;->willMessage:Ljava/lang/String;

    .line 337036
    iput-object p4, p0, LX/1u5;->clientInfo:LX/1u6;

    .line 337037
    iput-object p5, p0, LX/1u5;->password:Ljava/lang/String;

    .line 337038
    iput-object p6, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    .line 337039
    iput-object p7, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    .line 337040
    iput-object p8, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    .line 337041
    iput-object p9, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    .line 337042
    iput-object p10, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    .line 337043
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 337044
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 337045
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v1, v0

    .line 337046
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 337047
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ConnectMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 337048
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337049
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337050
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337051
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337052
    const-string v4, "clientIdentifier"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337053
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337054
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337055
    iget-object v4, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    if-nez v4, :cond_b

    .line 337056
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337057
    :goto_3
    iget-object v4, p0, LX/1u5;->willTopic:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 337058
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337059
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337060
    const-string v4, "willTopic"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337061
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337062
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337063
    iget-object v4, p0, LX/1u5;->willTopic:Ljava/lang/String;

    if-nez v4, :cond_c

    .line 337064
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337065
    :cond_0
    :goto_4
    iget-object v4, p0, LX/1u5;->willMessage:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 337066
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337067
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337068
    const-string v4, "willMessage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337069
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337070
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337071
    iget-object v4, p0, LX/1u5;->willMessage:Ljava/lang/String;

    if-nez v4, :cond_d

    .line 337072
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337073
    :cond_1
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337074
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337075
    const-string v4, "clientInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337076
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337077
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337078
    iget-object v4, p0, LX/1u5;->clientInfo:LX/1u6;

    if-nez v4, :cond_e

    .line 337079
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337080
    :goto_6
    iget-object v4, p0, LX/1u5;->password:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 337081
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337082
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337083
    const-string v4, "password"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337084
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337085
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337086
    iget-object v4, p0, LX/1u5;->password:Ljava/lang/String;

    if-nez v4, :cond_f

    .line 337087
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337088
    :cond_2
    :goto_7
    iget-object v4, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 337089
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337090
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337091
    const-string v4, "getDiffsRequests"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337092
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337093
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337094
    iget-object v4, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-nez v4, :cond_10

    .line 337095
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337096
    :cond_3
    :goto_8
    iget-object v4, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-eqz v4, :cond_4

    .line 337097
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337098
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337099
    const-string v4, "proxygenInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337100
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337101
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337102
    iget-object v4, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-nez v4, :cond_11

    .line 337103
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337104
    :cond_4
    :goto_9
    iget-object v4, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-eqz v4, :cond_5

    .line 337105
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337106
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337107
    const-string v4, "combinedPublishes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337108
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337109
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337110
    iget-object v4, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-nez v4, :cond_12

    .line 337111
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337112
    :cond_5
    :goto_a
    iget-object v4, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 337113
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337114
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337115
    const-string v4, "zeroRatingTokenHash"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337116
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337117
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337118
    iget-object v4, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-nez v4, :cond_13

    .line 337119
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337120
    :cond_6
    :goto_b
    iget-object v4, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-eqz v4, :cond_7

    .line 337121
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337122
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337123
    const-string v4, "appSpecificInfo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337124
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337125
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337126
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-nez v0, :cond_14

    .line 337127
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337128
    :cond_7
    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337129
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337130
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 337131
    :cond_8
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 337132
    :cond_9
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 337133
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 337134
    :cond_b
    iget-object v4, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 337135
    :cond_c
    iget-object v4, p0, LX/1u5;->willTopic:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 337136
    :cond_d
    iget-object v4, p0, LX/1u5;->willMessage:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 337137
    :cond_e
    iget-object v4, p0, LX/1u5;->clientInfo:LX/1u6;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 337138
    :cond_f
    iget-object v4, p0, LX/1u5;->password:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 337139
    :cond_10
    iget-object v4, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 337140
    :cond_11
    iget-object v4, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 337141
    :cond_12
    iget-object v4, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 337142
    :cond_13
    iget-object v4, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 337143
    :cond_14
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 337144
    invoke-virtual {p1}, LX/1su;->a()V

    .line 337145
    iget-object v0, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 337146
    sget-object v0, LX/1u5;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337147
    iget-object v0, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337148
    :cond_0
    iget-object v0, p0, LX/1u5;->willTopic:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 337149
    iget-object v0, p0, LX/1u5;->willTopic:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 337150
    sget-object v0, LX/1u5;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337151
    iget-object v0, p0, LX/1u5;->willTopic:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337152
    :cond_1
    iget-object v0, p0, LX/1u5;->willMessage:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 337153
    iget-object v0, p0, LX/1u5;->willMessage:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 337154
    sget-object v0, LX/1u5;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337155
    iget-object v0, p0, LX/1u5;->willMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337156
    :cond_2
    iget-object v0, p0, LX/1u5;->clientInfo:LX/1u6;

    if-eqz v0, :cond_3

    .line 337157
    sget-object v0, LX/1u5;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337158
    iget-object v0, p0, LX/1u5;->clientInfo:LX/1u6;

    invoke-virtual {v0, p1}, LX/1u6;->a(LX/1su;)V

    .line 337159
    :cond_3
    iget-object v0, p0, LX/1u5;->password:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 337160
    iget-object v0, p0, LX/1u5;->password:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 337161
    sget-object v0, LX/1u5;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337162
    iget-object v0, p0, LX/1u5;->password:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337163
    :cond_4
    iget-object v0, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 337164
    iget-object v0, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 337165
    sget-object v0, LX/1u5;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337166
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v2, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 337167
    iget-object v0, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 337168
    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    goto :goto_0

    .line 337169
    :cond_5
    iget-object v0, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 337170
    iget-object v0, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 337171
    sget-object v0, LX/1u5;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337172
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 337173
    iget-object v0, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mZ;

    .line 337174
    invoke-virtual {v0, p1}, LX/6mZ;->a(LX/1su;)V

    goto :goto_1

    .line 337175
    :cond_6
    iget-object v0, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 337176
    iget-object v0, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 337177
    sget-object v0, LX/1u5;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337178
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 337179
    iget-object v0, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6mL;

    .line 337180
    invoke-virtual {v0, p1}, LX/6mL;->a(LX/1su;)V

    goto :goto_2

    .line 337181
    :cond_7
    iget-object v0, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 337182
    iget-object v0, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 337183
    sget-object v0, LX/1u5;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337184
    iget-object v0, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 337185
    :cond_8
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-eqz v0, :cond_9

    .line 337186
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-eqz v0, :cond_9

    .line 337187
    sget-object v0, LX/1u5;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 337188
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 337189
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 337190
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 337191
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 337192
    :cond_9
    invoke-virtual {p1}, LX/1su;->c()V

    .line 337193
    invoke-virtual {p1}, LX/1su;->b()V

    .line 337194
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 337195
    if-nez p1, :cond_1

    .line 337196
    :cond_0
    :goto_0
    return v0

    .line 337197
    :cond_1
    instance-of v1, p1, LX/1u5;

    if-eqz v1, :cond_0

    .line 337198
    check-cast p1, LX/1u5;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 337199
    if-nez p1, :cond_3

    .line 337200
    :cond_2
    :goto_1
    move v0, v2

    .line 337201
    goto :goto_0

    .line 337202
    :cond_3
    iget-object v0, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 337203
    :goto_2
    iget-object v3, p1, LX/1u5;->clientIdentifier:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 337204
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 337205
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337206
    iget-object v0, p0, LX/1u5;->clientIdentifier:Ljava/lang/String;

    iget-object v3, p1, LX/1u5;->clientIdentifier:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337207
    :cond_5
    iget-object v0, p0, LX/1u5;->willTopic:Ljava/lang/String;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 337208
    :goto_4
    iget-object v3, p1, LX/1u5;->willTopic:Ljava/lang/String;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 337209
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 337210
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337211
    iget-object v0, p0, LX/1u5;->willTopic:Ljava/lang/String;

    iget-object v3, p1, LX/1u5;->willTopic:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337212
    :cond_7
    iget-object v0, p0, LX/1u5;->willMessage:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 337213
    :goto_6
    iget-object v3, p1, LX/1u5;->willMessage:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 337214
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 337215
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337216
    iget-object v0, p0, LX/1u5;->willMessage:Ljava/lang/String;

    iget-object v3, p1, LX/1u5;->willMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337217
    :cond_9
    iget-object v0, p0, LX/1u5;->clientInfo:LX/1u6;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 337218
    :goto_8
    iget-object v3, p1, LX/1u5;->clientInfo:LX/1u6;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 337219
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 337220
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337221
    iget-object v0, p0, LX/1u5;->clientInfo:LX/1u6;

    iget-object v3, p1, LX/1u5;->clientInfo:LX/1u6;

    invoke-virtual {v0, v3}, LX/1u6;->a(LX/1u6;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337222
    :cond_b
    iget-object v0, p0, LX/1u5;->password:Ljava/lang/String;

    if-eqz v0, :cond_20

    move v0, v1

    .line 337223
    :goto_a
    iget-object v3, p1, LX/1u5;->password:Ljava/lang/String;

    if-eqz v3, :cond_21

    move v3, v1

    .line 337224
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 337225
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337226
    iget-object v0, p0, LX/1u5;->password:Ljava/lang/String;

    iget-object v3, p1, LX/1u5;->password:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337227
    :cond_d
    iget-object v0, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-eqz v0, :cond_22

    move v0, v1

    .line 337228
    :goto_c
    iget-object v3, p1, LX/1u5;->getDiffsRequests:Ljava/util/List;

    if-eqz v3, :cond_23

    move v3, v1

    .line 337229
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 337230
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337231
    iget-object v0, p0, LX/1u5;->getDiffsRequests:Ljava/util/List;

    iget-object v3, p1, LX/1u5;->getDiffsRequests:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337232
    :cond_f
    iget-object v0, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-eqz v0, :cond_24

    move v0, v1

    .line 337233
    :goto_e
    iget-object v3, p1, LX/1u5;->proxygenInfo:Ljava/util/List;

    if-eqz v3, :cond_25

    move v3, v1

    .line 337234
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 337235
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337236
    iget-object v0, p0, LX/1u5;->proxygenInfo:Ljava/util/List;

    iget-object v3, p1, LX/1u5;->proxygenInfo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337237
    :cond_11
    iget-object v0, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-eqz v0, :cond_26

    move v0, v1

    .line 337238
    :goto_10
    iget-object v3, p1, LX/1u5;->combinedPublishes:Ljava/util/List;

    if-eqz v3, :cond_27

    move v3, v1

    .line 337239
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 337240
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337241
    iget-object v0, p0, LX/1u5;->combinedPublishes:Ljava/util/List;

    iget-object v3, p1, LX/1u5;->combinedPublishes:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337242
    :cond_13
    iget-object v0, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-eqz v0, :cond_28

    move v0, v1

    .line 337243
    :goto_12
    iget-object v3, p1, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    if-eqz v3, :cond_29

    move v3, v1

    .line 337244
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 337245
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337246
    iget-object v0, p0, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    iget-object v3, p1, LX/1u5;->zeroRatingTokenHash:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337247
    :cond_15
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 337248
    :goto_14
    iget-object v3, p1, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 337249
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 337250
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 337251
    iget-object v0, p0, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    iget-object v3, p1, LX/1u5;->appSpecificInfo:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_17
    move v2, v1

    .line 337252
    goto/16 :goto_1

    :cond_18
    move v0, v2

    .line 337253
    goto/16 :goto_2

    :cond_19
    move v3, v2

    .line 337254
    goto/16 :goto_3

    :cond_1a
    move v0, v2

    .line 337255
    goto/16 :goto_4

    :cond_1b
    move v3, v2

    .line 337256
    goto/16 :goto_5

    :cond_1c
    move v0, v2

    .line 337257
    goto/16 :goto_6

    :cond_1d
    move v3, v2

    .line 337258
    goto/16 :goto_7

    :cond_1e
    move v0, v2

    .line 337259
    goto/16 :goto_8

    :cond_1f
    move v3, v2

    .line 337260
    goto/16 :goto_9

    :cond_20
    move v0, v2

    .line 337261
    goto/16 :goto_a

    :cond_21
    move v3, v2

    .line 337262
    goto/16 :goto_b

    :cond_22
    move v0, v2

    .line 337263
    goto/16 :goto_c

    :cond_23
    move v3, v2

    .line 337264
    goto/16 :goto_d

    :cond_24
    move v0, v2

    .line 337265
    goto/16 :goto_e

    :cond_25
    move v3, v2

    .line 337266
    goto/16 :goto_f

    :cond_26
    move v0, v2

    .line 337267
    goto :goto_10

    :cond_27
    move v3, v2

    .line 337268
    goto :goto_11

    :cond_28
    move v0, v2

    .line 337269
    goto :goto_12

    :cond_29
    move v3, v2

    .line 337270
    goto :goto_13

    :cond_2a
    move v0, v2

    .line 337271
    goto :goto_14

    :cond_2b
    move v3, v2

    .line 337272
    goto :goto_15
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 337273
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 337274
    sget-boolean v0, LX/1u5;->a:Z

    .line 337275
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/1u5;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 337276
    return-object v0
.end method
