.class public LX/1fz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 292669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/162;
    .locals 2

    .prologue
    .line 292668
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    return-object v0
.end method

.method public static a(LX/16g;)LX/162;
    .locals 1

    .prologue
    .line 292670
    invoke-static {p0}, LX/0x1;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 292671
    if-nez v0, :cond_0

    .line 292672
    invoke-interface {p0}, LX/16g;->b()LX/162;

    move-result-object v0

    .line 292673
    invoke-static {p0, v0}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 292674
    :cond_0
    return-object v0
.end method

.method public static a(LX/16h;)LX/162;
    .locals 2

    .prologue
    .line 292646
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292647
    invoke-interface {p0}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    .line 292648
    if-eqz v1, :cond_0

    .line 292649
    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292650
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;
    .locals 1

    .prologue
    .line 292651
    instance-of v0, p0, LX/16g;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 292652
    check-cast v0, LX/16g;

    invoke-static {v0}, LX/0x1;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 292653
    if-eqz v0, :cond_0

    .line 292654
    :goto_0
    return-object v0

    .line 292655
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 292656
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/1fz;->b(LX/16h;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292657
    :cond_1
    invoke-static {p0}, LX/1fz;->a(Ljava/lang/Object;)LX/162;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 292658
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 292659
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->r()LX/0Px;

    move-result-object v0

    .line 292661
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 292662
    :goto_0
    return-object v0

    .line 292663
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;

    .line 292664
    if-nez v0, :cond_2

    move-object v0, v1

    .line 292665
    goto :goto_0

    .line 292666
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    move-object v0, v2

    .line 292667
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)LX/162;
    .locals 2

    .prologue
    .line 292638
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292640
    invoke-static {p0}, LX/2nL;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    move-result-object v1

    .line 292641
    if-eqz v1, :cond_0

    .line 292642
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292643
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LX/162;
    .locals 3

    .prologue
    .line 292568
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 292569
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use TrackableFeedProps.getTrackingCodes to get tracking codes for a Story"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292570
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_1

    .line 292571
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use TrackableFeedProps.getTrackingCodes to get tracking codes for an Attachment"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292572
    :cond_1
    instance-of v0, p0, LX/16g;

    if-eqz v0, :cond_2

    .line 292573
    check-cast p0, LX/16g;

    invoke-static {p0}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 292574
    :goto_0
    return-object v0

    .line 292575
    :cond_2
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_3

    .line 292576
    invoke-static {}, LX/1fz;->a()LX/162;

    move-result-object v0

    goto :goto_0

    .line 292577
    :cond_3
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    if-eqz v0, :cond_4

    .line 292578
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-static {p0}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292579
    :cond_4
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_5

    .line 292580
    check-cast p0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    invoke-static {p0}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292581
    :cond_5
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    if-eqz v0, :cond_6

    .line 292582
    check-cast p0, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;

    .line 292583
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292585
    move-object v0, v0

    .line 292586
    goto :goto_0

    .line 292587
    :cond_6
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    if-eqz v0, :cond_7

    .line 292588
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;

    .line 292589
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292590
    if-eqz p0, :cond_12

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 292591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292592
    :goto_1
    move-object v0, v0

    .line 292593
    goto :goto_0

    .line 292594
    :cond_7
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_8

    .line 292595
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-static {p0}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292596
    :cond_8
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v0, :cond_9

    .line 292597
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-static {p0}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292598
    :cond_9
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    if-eqz v0, :cond_a

    .line 292599
    check-cast p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;

    invoke-static {p0}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;)LX/162;

    move-result-object v0

    goto :goto_0

    .line 292600
    :cond_a
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    if-eqz v0, :cond_c

    .line 292601
    check-cast p0, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    .line 292602
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292604
    invoke-static {p0}, LX/2nL;->a(Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;)Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;

    move-result-object v1

    .line 292605
    if-eqz v1, :cond_b

    .line 292606
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnitItem;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292607
    :cond_b
    move-object v0, v0

    .line 292608
    goto/16 :goto_0

    .line 292609
    :cond_c
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    if-eqz v0, :cond_d

    .line 292610
    check-cast p0, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;

    .line 292611
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292613
    move-object v0, v0

    .line 292614
    goto/16 :goto_0

    .line 292615
    :cond_d
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v0, :cond_e

    .line 292616
    check-cast p0, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    invoke-static {p0}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    goto/16 :goto_0

    .line 292617
    :cond_e
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    if-eqz v0, :cond_f

    .line 292618
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292619
    move-object v0, v0

    .line 292620
    goto/16 :goto_0

    .line 292621
    :cond_f
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_11

    .line 292622
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 292623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->v()Z

    move-result v0

    if-nez v0, :cond_13

    .line 292624
    invoke-static {p0}, LX/1fz;->b(LX/16h;)LX/162;

    move-result-object v0

    .line 292625
    :cond_10
    :goto_2
    move-object v0, v0

    .line 292626
    goto/16 :goto_0

    .line 292627
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 292628
    :cond_13
    invoke-static {p0}, LX/39w;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 292629
    if-nez v1, :cond_14

    .line 292630
    const/4 v0, 0x0

    goto :goto_2

    .line 292631
    :cond_14
    invoke-static {v1}, LX/0x1;->a(LX/16g;)LX/162;

    move-result-object v0

    .line 292632
    if-nez v0, :cond_10

    .line 292633
    new-instance v0, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/162;-><init>(LX/0mC;)V

    .line 292634
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_15

    .line 292635
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292636
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySet;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292637
    invoke-static {v1, v0}, LX/0x1;->a(LX/16g;LX/162;)V

    goto :goto_2
.end method

.method public static b(LX/16h;)LX/162;
    .locals 2

    .prologue
    .line 292565
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 292566
    invoke-interface {p0}, LX/16h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 292567
    return-object v0
.end method

.method public static c()LX/162;
    .locals 2

    .prologue
    .line 292644
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use TrackableFeedProps.getTrackingCodes"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d()LX/162;
    .locals 2

    .prologue
    .line 292645
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use TrackableFeedProps.getTrackingCodes"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
