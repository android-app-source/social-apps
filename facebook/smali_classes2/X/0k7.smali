.class public final LX/0k7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0k8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0k8",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Landroid/os/Bundle;

.field public c:LX/1jv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1jv",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0k9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0k9",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/Object;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:LX/0k7;

.field public final synthetic o:LX/0k4;


# direct methods
.method public constructor <init>(LX/0k4;ILandroid/os/Bundle;LX/1jv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            "LX/1jv",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126329
    iput-object p1, p0, LX/0k7;->o:LX/0k4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126330
    iput p2, p0, LX/0k7;->a:I

    .line 126331
    iput-object p3, p0, LX/0k7;->b:Landroid/os/Bundle;

    .line 126332
    iput-object p4, p0, LX/0k7;->c:LX/1jv;

    .line 126333
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 126309
    iget-boolean v0, p0, LX/0k7;->i:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0k7;->j:Z

    if-eqz v0, :cond_1

    .line 126310
    iput-boolean v2, p0, LX/0k7;->h:Z

    .line 126311
    :cond_0
    :goto_0
    return-void

    .line 126312
    :cond_1
    iget-boolean v0, p0, LX/0k7;->h:Z

    if-nez v0, :cond_0

    .line 126313
    iput-boolean v2, p0, LX/0k7;->h:Z

    .line 126314
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Starting: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126315
    :cond_2
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    if-eqz v0, :cond_3

    .line 126316
    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    iget v1, p0, LX/0k7;->a:I

    invoke-interface {v0, v1}, LX/1jv;->a(I)LX/0k9;

    move-result-object v0

    iput-object v0, p0, LX/0k7;->d:LX/0k9;

    .line 126317
    :cond_3
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    if-eqz v0, :cond_0

    .line 126318
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 126319
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126320
    :cond_4
    iget-boolean v0, p0, LX/0k7;->m:Z

    if-nez v0, :cond_5

    .line 126321
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    iget v1, p0, LX/0k7;->a:I

    invoke-virtual {v0, v1, p0}, LX/0k9;->a(ILX/0k8;)V

    .line 126322
    iput-boolean v2, p0, LX/0k7;->m:Z

    .line 126323
    :cond_5
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    const/4 v2, 0x0

    .line 126324
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0k9;->p:Z

    .line 126325
    iput-boolean v2, v0, LX/0k9;->r:Z

    .line 126326
    iput-boolean v2, v0, LX/0k9;->q:Z

    .line 126327
    invoke-virtual {v0}, LX/0k9;->g()V

    .line 126328
    goto :goto_0
.end method

.method public final a(LX/0k9;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0k9",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 126285
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLoadComplete: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126286
    :cond_0
    iget-boolean v0, p0, LX/0k7;->l:Z

    if-eqz v0, :cond_2

    .line 126287
    :cond_1
    :goto_0
    return-void

    .line 126288
    :cond_2
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->b:LX/0YU;

    iget v1, p0, LX/0k7;->a:I

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 126289
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    .line 126290
    if-eqz v0, :cond_4

    .line 126291
    sget-boolean v1, LX/0k4;->a:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Switching to pending loader: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126292
    :cond_3
    iput-object v3, p0, LX/0k7;->n:LX/0k7;

    .line 126293
    iget-object v1, p0, LX/0k7;->o:LX/0k4;

    iget-object v1, v1, LX/0k4;->b:LX/0YU;

    iget v2, p0, LX/0k7;->a:I

    invoke-virtual {v1, v2, v3}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 126294
    invoke-virtual {p0}, LX/0k7;->f()V

    .line 126295
    iget-object v1, p0, LX/0k7;->o:LX/0k4;

    invoke-virtual {v1, v0}, LX/0k4;->a(LX/0k7;)V

    goto :goto_0

    .line 126296
    :cond_4
    iget-object v0, p0, LX/0k7;->g:Ljava/lang/Object;

    if-ne v0, p2, :cond_5

    iget-boolean v0, p0, LX/0k7;->e:Z

    if-nez v0, :cond_6

    .line 126297
    :cond_5
    iput-object p2, p0, LX/0k7;->g:Ljava/lang/Object;

    .line 126298
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k7;->e:Z

    .line 126299
    iget-boolean v0, p0, LX/0k7;->h:Z

    if-eqz v0, :cond_6

    .line 126300
    invoke-virtual {p0, p1, p2}, LX/0k7;->b(LX/0k9;Ljava/lang/Object;)V

    .line 126301
    :cond_6
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->c:LX/0YU;

    iget v1, p0, LX/0k7;->a:I

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0k7;

    .line 126302
    if-eqz v0, :cond_7

    if-eq v0, p0, :cond_7

    .line 126303
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0k7;->f:Z

    .line 126304
    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126305
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->c:LX/0YU;

    iget v1, p0, LX/0k7;->a:I

    .line 126306
    invoke-virtual {v0, v1}, LX/0YU;->b(I)V

    .line 126307
    :cond_7
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    invoke-virtual {v0}, LX/0k4;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126308
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    invoke-virtual {v0}, LX/0jz;->h()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 126264
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, LX/0k7;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 126265
    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0k7;->b:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 126266
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 126267
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 126268
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    if-eqz v0, :cond_0

    .line 126269
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/0k9;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126270
    :cond_0
    iget-boolean v0, p0, LX/0k7;->e:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/0k7;->f:Z

    if-eqz v0, :cond_2

    .line 126271
    :cond_1
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126272
    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->f:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 126273
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/0k7;->g:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 126274
    :cond_2
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126275
    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->k:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126276
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 126277
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126278
    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 126279
    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k7;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 126280
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    if-eqz v0, :cond_3

    .line 126281
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126282
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 126283
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/0k7;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 126284
    :cond_3
    return-void
.end method

.method public final b(LX/0k9;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0k9",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 126246
    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    if-eqz v0, :cond_2

    .line 126247
    const/4 v0, 0x0

    .line 126248
    iget-object v1, p0, LX/0k7;->o:LX/0k4;

    iget-object v1, v1, LX/0k4;->e:LX/0k3;

    if-eqz v1, :cond_4

    .line 126249
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    iget-object v0, v0, LX/0jz;->t:Ljava/lang/String;

    .line 126250
    iget-object v1, p0, LX/0k7;->o:LX/0k4;

    iget-object v1, v1, LX/0k4;->e:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->o()LX/0jz;

    move-result-object v1

    const-string v2, "onLoadFinished"

    iput-object v2, v1, LX/0jz;->t:Ljava/lang/String;

    move-object v1, v0

    .line 126251
    :goto_0
    :try_start_0
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  onLoadFinished in "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 126252
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 126253
    invoke-static {p2, v2}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 126254
    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126255
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 126256
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126257
    :cond_0
    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    invoke-interface {v0, p1, p2}, LX/1jv;->a(LX/0k9;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126258
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    if-eqz v0, :cond_1

    .line 126259
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    iput-object v1, v0, LX/0jz;->t:Ljava/lang/String;

    .line 126260
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k7;->f:Z

    .line 126261
    :cond_2
    return-void

    .line 126262
    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/0k7;->o:LX/0k4;

    iget-object v2, v2, LX/0k4;->e:LX/0k3;

    if-eqz v2, :cond_3

    .line 126263
    iget-object v2, p0, LX/0k7;->o:LX/0k4;

    iget-object v2, v2, LX/0k4;->e:LX/0k3;

    invoke-virtual {v2}, LX/0k3;->o()LX/0jz;

    move-result-object v2

    iput-object v1, v2, LX/0jz;->t:Ljava/lang/String;

    :cond_3
    throw v0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126236
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Stopping: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126237
    :cond_0
    iput-boolean v2, p0, LX/0k7;->h:Z

    .line 126238
    iget-boolean v0, p0, LX/0k7;->i:Z

    if-nez v0, :cond_1

    .line 126239
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/0k7;->m:Z

    if-eqz v0, :cond_1

    .line 126240
    iput-boolean v2, p0, LX/0k7;->m:Z

    .line 126241
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {v0, p0}, LX/0k9;->a(LX/0k8;)V

    .line 126242
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    .line 126243
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0k9;->p:Z

    .line 126244
    invoke-virtual {v0}, LX/0k9;->h()V

    .line 126245
    :cond_1
    return-void
.end method

.method public final f()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 126196
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Destroying: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126197
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0k7;->l:Z

    .line 126198
    iget-boolean v0, p0, LX/0k7;->f:Z

    .line 126199
    iput-boolean v4, p0, LX/0k7;->f:Z

    .line 126200
    iget-object v1, p0, LX/0k7;->c:LX/1jv;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/0k7;->d:LX/0k9;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/0k7;->e:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 126201
    sget-boolean v0, LX/0k4;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Reseting: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126202
    :cond_1
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    if-eqz v0, :cond_7

    .line 126203
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    iget-object v0, v0, LX/0jz;->t:Ljava/lang/String;

    .line 126204
    iget-object v1, p0, LX/0k7;->o:LX/0k4;

    iget-object v1, v1, LX/0k4;->e:LX/0k3;

    invoke-virtual {v1}, LX/0k3;->o()LX/0jz;

    move-result-object v1

    const-string v3, "onLoaderReset"

    iput-object v3, v1, LX/0jz;->t:Ljava/lang/String;

    move-object v1, v0

    .line 126205
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/0k7;->c:LX/1jv;

    invoke-interface {v0}, LX/1jv;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126206
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    if-eqz v0, :cond_2

    .line 126207
    iget-object v0, p0, LX/0k7;->o:LX/0k4;

    iget-object v0, v0, LX/0k4;->e:LX/0k3;

    invoke-virtual {v0}, LX/0k3;->o()LX/0jz;

    move-result-object v0

    iput-object v1, v0, LX/0jz;->t:Ljava/lang/String;

    .line 126208
    :cond_2
    iput-object v2, p0, LX/0k7;->c:LX/1jv;

    .line 126209
    iput-object v2, p0, LX/0k7;->g:Ljava/lang/Object;

    .line 126210
    iput-boolean v4, p0, LX/0k7;->e:Z

    .line 126211
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    if-eqz v0, :cond_4

    .line 126212
    iget-boolean v0, p0, LX/0k7;->m:Z

    if-eqz v0, :cond_3

    .line 126213
    iput-boolean v4, p0, LX/0k7;->m:Z

    .line 126214
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    invoke-virtual {v0, p0}, LX/0k9;->a(LX/0k8;)V

    .line 126215
    :cond_3
    iget-object v0, p0, LX/0k7;->d:LX/0k9;

    const/4 v2, 0x0

    .line 126216
    invoke-virtual {v0}, LX/0k9;->i()V

    .line 126217
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0k9;->r:Z

    .line 126218
    iput-boolean v2, v0, LX/0k9;->p:Z

    .line 126219
    iput-boolean v2, v0, LX/0k9;->q:Z

    .line 126220
    iput-boolean v2, v0, LX/0k9;->s:Z

    .line 126221
    iput-boolean v2, v0, LX/0k9;->t:Z

    .line 126222
    :cond_4
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    if-eqz v0, :cond_5

    .line 126223
    iget-object v0, p0, LX/0k7;->n:LX/0k7;

    invoke-virtual {v0}, LX/0k7;->f()V

    .line 126224
    :cond_5
    return-void

    .line 126225
    :catchall_0
    move-exception v0

    iget-object v2, p0, LX/0k7;->o:LX/0k4;

    iget-object v2, v2, LX/0k4;->e:LX/0k3;

    if-eqz v2, :cond_6

    .line 126226
    iget-object v2, p0, LX/0k7;->o:LX/0k4;

    iget-object v2, v2, LX/0k4;->e:LX/0k3;

    invoke-virtual {v2}, LX/0k3;->o()LX/0jz;

    move-result-object v2

    iput-object v1, v2, LX/0jz;->t:Ljava/lang/String;

    :cond_6
    throw v0

    :cond_7
    move-object v1, v2

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126227
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 126228
    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126229
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126230
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126231
    iget v1, p0, LX/0k7;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 126232
    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126233
    iget-object v1, p0, LX/0k7;->d:LX/0k9;

    invoke-static {v1, v0}, LX/18p;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 126234
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
