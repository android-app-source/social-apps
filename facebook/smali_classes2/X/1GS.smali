.class public LX/1GS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1GS;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 225497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225498
    iput-object p1, p0, LX/1GS;->a:LX/0Zb;

    .line 225499
    return-void
.end method

.method public static a(LX/0QB;)LX/1GS;
    .locals 4

    .prologue
    .line 225500
    sget-object v0, LX/1GS;->b:LX/1GS;

    if-nez v0, :cond_1

    .line 225501
    const-class v1, LX/1GS;

    monitor-enter v1

    .line 225502
    :try_start_0
    sget-object v0, LX/1GS;->b:LX/1GS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 225503
    if-eqz v2, :cond_0

    .line 225504
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 225505
    new-instance p0, LX/1GS;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/1GS;-><init>(LX/0Zb;)V

    .line 225506
    move-object v0, p0

    .line 225507
    sput-object v0, LX/1GS;->b:LX/1GS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225508
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 225509
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 225510
    :cond_1
    sget-object v0, LX/1GS;->b:LX/1GS;

    return-object v0

    .line 225511
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 225512
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
