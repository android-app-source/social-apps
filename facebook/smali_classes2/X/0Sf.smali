.class public LX/0Sf;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Se;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field public final e:LX/0Sg;

.field public final f:LX/0Sg;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0YO;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0cZ;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/concurrent/Executor;

.field public final k:LX/0TE;

.field private final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2W8;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0So;

.field private final t:LX/0UR;

.field private final u:LX/0TL;

.field public final v:LX/0Uh;

.field private final w:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mHighPriTimings"
    .end annotation
.end field

.field private final y:Ljava/util/concurrent/locks/ReentrantLock;

.field public volatile z:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFbSharedPrefsLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61635
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    :goto_0
    move v0, v0

    .line 61636
    sput v0, LX/0Sf;->a:I

    .line 61637
    const-class v0, LX/0Sf;

    sput-object v0, LX/0Sf;->b:Ljava/lang/Class;

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Sg;LX/0Sg;Ljava/util/concurrent/Executor;LX/0TE;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/0Ot;LX/0UR;LX/0TL;LX/0Uh;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p10    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/init/NeedsHighPriorityInitOnBackgroundThread;
        .end annotation
    .end param
    .param p11    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/init/NeedsHighPriorityInitOnBackgroundThreadWithoutSharedPref;
        .end annotation
    .end param
    .param p13    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/init/NeedsLowPriorityInitOnBackgroundThread;
        .end annotation
    .end param
    .param p14    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/init/NeedsModuleInit;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "Lcom/facebook/common/appchoreographer/AppChoreographerController;",
            "Ljava/util/concurrent/Executor;",
            "LX/0TE;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0YO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0cZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/2W8;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "LX/0Se;",
            ">;",
            "LX/0UR;",
            "LX/0TL;",
            "Lcom/facebook/gk/store/GatekeeperStoreManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 61638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61639
    new-instance v1, LX/0Uk;

    invoke-direct {v1}, LX/0Uk;-><init>()V

    iput-object v1, p0, LX/0Sf;->w:Ljava/util/Comparator;

    .line 61640
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/0Sf;->x:Ljava/util/Map;

    .line 61641
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, LX/0Sf;->y:Ljava/util/concurrent/locks/ReentrantLock;

    .line 61642
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/0Sf;->z:Z

    .line 61643
    iput-object p1, p0, LX/0Sf;->d:Landroid/content/Context;

    .line 61644
    iput-object p2, p0, LX/0Sf;->e:LX/0Sg;

    .line 61645
    iput-object p3, p0, LX/0Sf;->f:LX/0Sg;

    .line 61646
    iput-object p4, p0, LX/0Sf;->j:Ljava/util/concurrent/Executor;

    .line 61647
    iput-object p5, p0, LX/0Sf;->k:LX/0TE;

    .line 61648
    iput-object p6, p0, LX/0Sf;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 61649
    iput-object p9, p0, LX/0Sf;->i:LX/0Ot;

    .line 61650
    iput-object p7, p0, LX/0Sf;->g:LX/0Ot;

    .line 61651
    iput-object p8, p0, LX/0Sf;->h:LX/0Ot;

    .line 61652
    iput-object p10, p0, LX/0Sf;->m:LX/0Ot;

    .line 61653
    iput-object p11, p0, LX/0Sf;->n:LX/0Ot;

    .line 61654
    iput-object p12, p0, LX/0Sf;->o:LX/0Ot;

    .line 61655
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0Sf;->p:LX/0Ot;

    .line 61656
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0Sf;->q:LX/0Ot;

    .line 61657
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0Sf;->r:LX/0Ot;

    .line 61658
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0Sf;->s:LX/0So;

    .line 61659
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0Sf;->c:LX/0Ot;

    .line 61660
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0Sf;->t:LX/0UR;

    .line 61661
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0Sf;->u:LX/0TL;

    .line 61662
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0Sf;->v:LX/0Uh;

    .line 61663
    return-void
.end method

.method private static a(LX/0Sf;Ljava/util/Collection;LX/0TD;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/0Up;",
            ">;",
            "LX/0TD;",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/concurrent/Future",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 61664
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 61665
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 61666
    new-instance v2, Lcom/facebook/common/init/impl/FbAppInitializerInternal$5;

    invoke-direct {v2, p0, v1}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$5;-><init>(LX/0Sf;Ljava/util/Iterator;)V

    invoke-interface {p2, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 61667
    invoke-interface {p3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 61668
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61669
    :cond_0
    return-void
.end method

.method public static a(LX/0Sf;Ljava/util/Iterator;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "LX/0Up;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 61670
    iget-object v0, p0, LX/0Sf;->e:LX/0Sg;

    const-string v1, "FbAppInitializer-LowPriWorkerThread:"

    new-instance v2, Lcom/facebook/common/init/impl/FbAppInitializerInternal$8;

    invoke-direct {v2, p0, p2, p1}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$8;-><init>(LX/0Sf;ZLjava/util/Iterator;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v0

    .line 61671
    iget-object v1, p0, LX/0Sf;->k:LX/0TE;

    iget-object v2, p0, LX/0Sf;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 61672
    return-void
.end method

.method private a(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 61673
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 61674
    if-nez v0, :cond_1

    .line 61675
    :cond_0
    return-void

    .line 61676
    :cond_1
    iget-object v1, p0, LX/0Sf;->x:Ljava/util/Map;

    monitor-enter v1

    .line 61677
    :try_start_0
    iget-object v0, p0, LX/0Sf;->x:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 61678
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61679
    iget-object v1, p0, LX/0Sf;->w:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 61680
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 61681
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 61682
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    goto :goto_0

    .line 61683
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61684
    const-string v0, "HiPri-execute-tasks-synch"

    const v1, -0x3bd703fe

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61685
    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    .line 61686
    if-eqz v0, :cond_0

    .line 61687
    const-string v2, "INeedInit.HighPriorityInitOnBackgroundThread"

    invoke-virtual {p0, v0, v2}, LX/0Sf;->a(LX/0Up;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 61688
    :catchall_0
    move-exception v0

    const v1, 0x4240e97f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, -0x7721898a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61689
    return-void
.end method

.method private static a(Ljava/util/concurrent/Future;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 61690
    invoke-interface {p0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61691
    :try_start_0
    invoke-static {p0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61692
    :cond_0
    :goto_0
    return-void

    .line 61693
    :catch_0
    move-exception v0

    .line 61694
    invoke-static {v0}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    .line 61695
    if-eqz v1, :cond_1

    .line 61696
    invoke-static {v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;)V

    .line 61697
    :cond_1
    sget-object v1, LX/0Sf;->b:Ljava/lang/Class;

    const-string v2, "HighPri init failed because of an exception"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/0Sf;LX/0TD;)V
    .locals 8

    .prologue
    const v7, 0x3d0001

    .line 61698
    iget-object v0, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 61699
    const-string v0, "FbAppInitializer-HiPri"

    const v1, 0x5fc6d1fd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61700
    :try_start_0
    const-string v0, "initializeGatekeeperStore"

    const v1, -0x1e79652c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 61701
    :try_start_1
    new-instance v0, Lcom/facebook/common/init/impl/FbAppInitializerInternal$4;

    invoke-direct {v0, p0}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$4;-><init>(LX/0Sf;)V

    invoke-interface {p1, v0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 61702
    iget-object v1, p0, LX/0Sf;->k:LX/0TE;

    iget-object v4, p0, LX/0Sf;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 61703
    const v0, -0x175cc599

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V

    .line 61704
    invoke-static {p0}, LX/0Sf;->k(LX/0Sf;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61705
    const-string v0, "HiPri-execute-tasks-parallel"

    const v1, -0x16178739

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 61706
    :try_start_3
    invoke-static {p0}, LX/0Sf;->f(LX/0Sf;)Ljava/util/Set;

    move-result-object v0

    .line 61707
    invoke-static {p0}, LX/0Sf;->e(LX/0Sf;)Ljava/util/Set;

    move-result-object v1

    .line 61708
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 61709
    new-instance v5, Lcom/facebook/common/init/impl/FbAppInitializerInternal$3;

    invoke-direct {v5, p0}, Lcom/facebook/common/init/impl/FbAppInitializerInternal$3;-><init>(LX/0Sf;)V

    invoke-interface {p1, v5}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 61710
    invoke-static {p0, v0, p1, v4}, LX/0Sf;->a(LX/0Sf;Ljava/util/Collection;LX/0TD;Ljava/util/Collection;)V

    .line 61711
    invoke-static {v5}, LX/0Sf;->a(Ljava/util/concurrent/Future;)V

    .line 61712
    invoke-static {p0, v1, p1, v4}, LX/0Sf;->a(LX/0Sf;Ljava/util/Collection;LX/0TD;Ljava/util/Collection;)V

    .line 61713
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 61714
    invoke-static {v0}, LX/0Sf;->a(Ljava/util/concurrent/Future;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 61715
    :catchall_0
    move-exception v0

    const v1, 0x657bcb74

    :try_start_4
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 61716
    :catchall_1
    move-exception v0

    const v1, 0x6a79f5c3

    invoke-static {v1}, LX/02m;->b(I)J

    throw v0

    .line 61717
    :catchall_2
    move-exception v0

    const v1, 0x28cb5ee3

    :try_start_5
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 61718
    :cond_0
    const v0, -0x79c66233

    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 61719
    :goto_1
    const v0, -0x2c2cb50d

    invoke-static {v0}, LX/02m;->b(I)J

    .line 61720
    iget-object v0, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v2, v0, v2

    .line 61721
    iget-object v0, p0, LX/0Sf;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 61722
    const/4 v1, 0x5

    invoke-interface {v0, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(II)V

    .line 61723
    iget-object v1, p0, LX/0Sf;->x:Ljava/util/Map;

    monitor-enter v1

    .line 61724
    const v4, 0x3d0001

    const/4 v5, 0x2

    long-to-int v2, v2

    :try_start_6
    iget-object v3, p0, LX/0Sf;->x:Ljava/util/Map;

    invoke-interface {v0, v4, v5, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISILjava/util/Map;)V

    .line 61725
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 61726
    sget-object v0, LX/0Sf;->b:Ljava/lang/Class;

    invoke-static {v0}, LX/0PR;->a(Ljava/lang/Class;)V

    .line 61727
    sget-object v0, LX/0Sf;->b:Ljava/lang/Class;

    invoke-direct {p0, v0}, LX/0Sf;->a(Ljava/lang/Class;)V

    .line 61728
    invoke-direct {p0}, LX/0Sf;->j()V

    .line 61729
    return-void

    .line 61730
    :cond_1
    :try_start_7
    invoke-static {p0}, LX/0Sf;->g(LX/0Sf;)V

    .line 61731
    invoke-static {p0}, LX/0Sf;->f(LX/0Sf;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Sf;->a(Ljava/util/Set;)V

    .line 61732
    invoke-static {p0}, LX/0Sf;->e(LX/0Sf;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0Sf;->a(Ljava/util/Set;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    .line 61733
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0
.end method

.method public static b(LX/0QB;)LX/0Sf;
    .locals 23

    .prologue
    .line 61734
    new-instance v2, LX/0Sf;

    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    move-object/from16 v0, p0

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v4

    check-cast v4, LX/0Sg;

    invoke-static/range {p0 .. p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v5

    check-cast v5, LX/0Sg;

    invoke-static/range {p0 .. p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0TE;->a(LX/0QB;)LX/0TE;

    move-result-object v7

    check-cast v7, LX/0TE;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v9, 0x2a4

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2a3

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x29f

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0UL;->a(LX/0QB;)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0UM;->a(LX/0QB;)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2ab

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0UP;->a(LX/0QB;)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0UQ;->a(LX/0QB;)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x103d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v18

    check-cast v18, LX/0So;

    const/16 v19, 0x2ac

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object v20

    check-cast v20, LX/0UR;

    invoke-static/range {p0 .. p0}, LX/0TK;->a(LX/0QB;)LX/0TL;

    move-result-object v21

    check-cast v21, LX/0TL;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-direct/range {v2 .. v22}, LX/0Sf;-><init>(Landroid/content/Context;LX/0Sg;LX/0Sg;Ljava/util/concurrent/Executor;LX/0TE;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/0Ot;LX/0UR;LX/0TL;LX/0Uh;)V

    .line 61735
    return-object v2
.end method

.method public static b(LX/0Sf;Ljava/util/Iterator;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "LX/0Up;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61621
    iget-object v0, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 61622
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61623
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    .line 61624
    if-eqz v0, :cond_0

    .line 61625
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v4, -0x97d9528

    invoke-static {v1, v4}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61626
    :try_start_0
    invoke-interface {v0}, LX/0Up;->init()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61627
    const v0, -0x7a52cda4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61628
    iget-object v0, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long/2addr v0, v2

    const-wide/16 v4, 0x5

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 61629
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/0Sf;->a(LX/0Sf;Ljava/util/Iterator;Z)V

    .line 61630
    :goto_0
    return-void

    .line 61631
    :catchall_0
    move-exception v0

    const v1, -0x8b4cbd3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 61632
    :cond_1
    iget-object v0, p0, LX/0Sf;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Se;

    .line 61633
    const/4 v1, 0x0

    iput-object v1, v0, LX/0Se;->a:LX/0Sf;

    .line 61634
    const/4 v0, 0x0

    iput-object v0, p0, LX/0Sf;->c:LX/0Ot;

    goto :goto_0
.end method

.method public static c$redex0(LX/0Sf;)V
    .locals 5

    .prologue
    .line 61736
    const-string v0, "FbAppInitializer-ModuleInit"

    const v1, 0x28301fca

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61737
    :try_start_0
    iget-object v0, p0, LX/0Sf;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Up;

    .line 61738
    const-string v2, "#%s"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    const v4, 0xe22e510

    invoke-static {v2, v3, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61739
    :try_start_1
    invoke-interface {v0}, LX/0Up;->init()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 61740
    const v0, 0x317f8f0b

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 61741
    :catchall_0
    move-exception v0

    const v1, 0x2204bdbb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 61742
    :catchall_1
    move-exception v0

    const v1, 0x1524ca3c

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61743
    :cond_0
    const v0, -0x68593fcb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61744
    return-void
.end method

.method public static d(LX/0Sf;)LX/0TD;
    .locals 12

    .prologue
    .line 61616
    invoke-static {p0}, LX/0Sf;->k(LX/0Sf;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, LX/0Sf;->a:I

    add-int/lit8 v0, v0, 0x1

    .line 61617
    :goto_0
    iget-object v1, p0, LX/0Sf;->t:LX/0UR;

    const-string v2, "HPINeedInit"

    const/16 v3, 0x100

    const/4 v4, 0x0

    .line 61618
    iget-object v11, v1, LX/0UR;->c:LX/0Or;

    invoke-static {v2, v0, v4}, LX/0TJ;->a(Ljava/lang/String;II)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v8

    iget-object v9, v1, LX/0UR;->b:LX/0Sj;

    iget-object v10, v1, LX/0UR;->d:LX/0TR;

    move-object v5, v2

    move v6, v0

    move v7, v3

    invoke-static/range {v5 .. v10}, LX/0TS;->a(Ljava/lang/String;IILjava/util/concurrent/Executor;LX/0Sj;LX/0TR;)LX/0TS;

    move-result-object v5

    invoke-static {v11, v5}, LX/0TW;->a(LX/0Or;LX/0TU;)LX/0TU;

    move-result-object v5

    move-object v0, v5

    .line 61619
    return-object v0

    .line 61620
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static e(LX/0Sf;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61551
    const-string v0, "HiPri-Setup"

    const v1, -0xc15791

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61552
    :try_start_0
    iget-object v0, p0, LX/0Sf;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61553
    const v1, -0x21492837

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x513a1f2b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static f(LX/0Sf;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61613
    const-string v0, "HiPriWithoutSharedPrefs-Setup"

    const v1, -0x26d7d1f4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61614
    :try_start_0
    iget-object v0, p0, LX/0Sf;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61615
    const v1, -0x251b21a3

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x6cd13939

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static g(LX/0Sf;)V
    .locals 2

    .prologue
    .line 61606
    const-string v0, "initializeSharedPrefs"

    const v1, 0x42c5b15b    # 98.8464f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61607
    :try_start_0
    iget-boolean v0, p0, LX/0Sf;->z:Z

    if-nez v0, :cond_0

    .line 61608
    invoke-static {p0}, LX/0Sf;->i(LX/0Sf;)V

    .line 61609
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Sf;->z:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61610
    const v0, 0x51e6e27f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61611
    return-void

    .line 61612
    :catchall_0
    move-exception v0

    const v1, -0x6b0ee0b7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static i(LX/0Sf;)V
    .locals 3

    .prologue
    .line 61601
    const-string v0, "HiPri-init-call-shared-prefs"

    const v1, -0x4537f56c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61602
    :try_start_0
    new-instance v0, LX/0Yp;

    iget-object v1, p0, LX/0Sf;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/0Sf;->h:LX/0Ot;

    invoke-direct {v0, p0, v1, v2}, LX/0Yp;-><init>(LX/0Sf;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V

    const-string v1, "INeedInit.HighPriorityInitOnBackgroundThread"

    invoke-virtual {p0, v0, v1}, LX/0Sf;->a(LX/0Up;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61603
    const v0, 0x3c03e2a3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61604
    return-void

    .line 61605
    :catchall_0
    move-exception v0

    const v1, -0x45e65a61

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 61596
    const-string v0, "HiPri-Completed-Setup"

    const v1, -0x7da12a7e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61597
    :try_start_0
    iget-object v0, p0, LX/0Sf;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61598
    const v0, -0x739952eb

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61599
    return-void

    .line 61600
    :catchall_0
    move-exception v0

    const v1, -0x529c063b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static k(LX/0Sf;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 61594
    iget-object v1, p0, LX/0Sf;->u:LX/0TL;

    invoke-virtual {v1}, LX/0TL;->b()I

    move-result v1

    .line 61595
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/0Sf;)V
    .locals 3

    .prologue
    .line 61586
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v1

    .line 61587
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61588
    const-string v0, "LowPriUIThread-Setup"

    const v2, 0x7dc7b017

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61589
    :try_start_0
    iget-object v0, p0, LX/0Sf;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2W8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61590
    const v2, 0x4de01510    # 4.69934592E8f

    invoke-static {v2}, LX/02m;->a(I)V

    .line 61591
    new-instance v2, LX/2Br;

    invoke-direct {v2, p0, v0}, LX/2Br;-><init>(LX/0Sf;LX/2W8;)V

    invoke-virtual {v1, v2}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 61592
    return-void

    .line 61593
    :catchall_0
    move-exception v0

    const v1, 0x7c902dca

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static m(LX/0Sf;)V
    .locals 9

    .prologue
    .line 61566
    const-string v0, "onColdStartDone"

    const v1, 0x5d83368

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 61567
    :try_start_0
    iget-object v0, p0, LX/0Sf;->d:Landroid/content/Context;

    .line 61568
    if-nez v0, :cond_1

    .line 61569
    :cond_0
    :goto_0
    sget-object v2, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mInstalledClassLoader:Lcom/facebook/common/dextricks/MultiDexClassLoader;

    .line 61570
    if-nez v2, :cond_2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61571
    :goto_1
    const v0, -0x5df3cca3    # -1.900073E-18f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 61572
    return-void

    .line 61573
    :catchall_0
    move-exception v0

    const v1, 0x73117b69

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 61574
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 61575
    if-eqz v1, :cond_0

    instance-of p0, v1, LX/003;

    if-eqz p0, :cond_0

    .line 61576
    check-cast v1, LX/003;

    .line 61577
    invoke-interface {v1}, LX/003;->i_()V

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61578
    :cond_2
    const/4 v3, 0x0

    .line 61579
    invoke-virtual {v2}, Lcom/facebook/common/dextricks/MultiDexClassLoader;->onColdstartDone()V

    .line 61580
    iget-object v4, v2, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mConfig:LX/02f;

    move-object v4, v4

    .line 61581
    iget-boolean v5, v2, Lcom/facebook/common/dextricks/MultiDexClassLoader;->mHasArtLocked:Z

    if-eqz v5, :cond_3

    .line 61582
    iget v6, v4, LX/02f;->coldstartSetSize:I

    .line 61583
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "notifyColdstartDone: Art unlock: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61584
    const/4 v8, 0x1

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-static/range {v3 .. v8}, Lcom/facebook/common/dextricks/DalvikInternals;->mstarOatFile(ZZZIZZ)V

    .line 61585
    :cond_3
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Up;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 61554
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 61555
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61556
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 61557
    :cond_0
    iget-object v1, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 61558
    const-string v1, "#%s"

    const v4, -0x7619cc7c

    invoke-static {v1, v0, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 61559
    :try_start_0
    invoke-interface {p1}, LX/0Up;->init()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61560
    const v1, -0x62940b1b

    invoke-static {v1}, LX/02m;->a(I)V

    .line 61561
    iget-object v1, p0, LX/0Sf;->x:Ljava/util/Map;

    monitor-enter v1

    .line 61562
    :try_start_1
    iget-object v4, p0, LX/0Sf;->x:Ljava/util/Map;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, LX/0Sf;->s:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61563
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 61564
    :catchall_0
    move-exception v0

    const v1, -0x5a314c0b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 61565
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
