.class public LX/1iV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field private final d:J

.field private final e:I

.field public final f:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JILcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V
    .locals 3
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298431
    iput p5, p0, LX/1iV;->e:I

    .line 298432
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1iV;->a:Ljava/lang/String;

    .line 298433
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/1iV;->b:Ljava/lang/String;

    .line 298434
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 298435
    iput-wide p3, p0, LX/1iV;->c:J

    .line 298436
    iput-object p6, p0, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 298437
    iput-object p7, p0, LX/1iV;->g:Ljava/lang/String;

    .line 298438
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/1iV;->d:J

    .line 298439
    return-void

    .line 298440
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lorg/apache/http/protocol/HttpContext;)LX/1iV;
    .locals 1

    .prologue
    .line 298441
    const-string v0, "fb_http_request_context"

    invoke-interface {p0, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iV;

    .line 298442
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iV;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298443
    iget-object v0, p0, LX/1iV;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lorg/apache/http/protocol/HttpContext;)V
    .locals 2

    .prologue
    .line 298444
    const-string v0, "fb_http_request_context"

    invoke-interface {p1, v0}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298445
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The HttpContext instance already has an RequestContext object attached to it."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298446
    :cond_0
    const-string v0, "fb_http_request_context"

    invoke-interface {p1, v0, p0}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 298447
    return-void
.end method

.method public final d()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 298448
    iget-object v0, p0, LX/1iV;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method
