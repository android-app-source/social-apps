.class public LX/1Ke;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Kf;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1Ke;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/1Kg;

.field private final c:LX/1Ki;

.field private final d:LX/1Kj;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/1Kg;LX/1Ki;LX/1Kj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 232963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232964
    iput-object p1, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    .line 232965
    iput-object p2, p0, LX/1Ke;->b:LX/1Kg;

    .line 232966
    iput-object p3, p0, LX/1Ke;->c:LX/1Ki;

    .line 232967
    iput-object p4, p0, LX/1Ke;->d:LX/1Kj;

    .line 232968
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ke;
    .locals 7

    .prologue
    .line 232950
    sget-object v0, LX/1Ke;->e:LX/1Ke;

    if-nez v0, :cond_1

    .line 232951
    const-class v1, LX/1Ke;

    monitor-enter v1

    .line 232952
    :try_start_0
    sget-object v0, LX/1Ke;->e:LX/1Ke;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 232953
    if-eqz v2, :cond_0

    .line 232954
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 232955
    new-instance p0, LX/1Ke;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Kg;->a(LX/0QB;)LX/1Kg;

    move-result-object v4

    check-cast v4, LX/1Kg;

    invoke-static {v0}, LX/1Kh;->b(LX/0QB;)LX/1Kh;

    move-result-object v5

    check-cast v5, LX/1Ki;

    invoke-static {v0}, LX/1Kj;->a(LX/0QB;)LX/1Kj;

    move-result-object v6

    check-cast v6, LX/1Kj;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Ke;-><init>(Lcom/facebook/content/SecureContextHelper;LX/1Kg;LX/1Ki;LX/1Kj;)V

    .line 232956
    move-object v0, p0

    .line 232957
    sput-object v0, LX/1Ke;->e:LX/1Ke;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 232959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 232960
    :cond_1
    sget-object v0, LX/1Ke;->e:LX/1Ke;

    return-object v0

    .line 232961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 232962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/app/Activity;)V
    .locals 4

    .prologue
    .line 232945
    iget-object v0, p0, LX/1Ke;->c:LX/1Ki;

    invoke-interface {v0}, LX/1Ki;->a()Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v0

    .line 232946
    if-nez v0, :cond_0

    .line 232947
    :goto_0
    return-void

    .line 232948
    :cond_0
    iget-object v1, p0, LX/1Ke;->d:LX/1Kj;

    invoke-virtual {p2}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ComposerLauncherImpl"

    invoke-virtual {v1, v2, v3}, LX/1Kj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232949
    iget-object v1, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0, p2}, LX/1Kg;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(ILandroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 232940
    iget-object v0, p0, LX/1Ke;->c:LX/1Ki;

    invoke-interface {v0}, LX/1Ki;->a()Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v0

    .line 232941
    if-nez v0, :cond_0

    .line 232942
    :goto_0
    return-void

    .line 232943
    :cond_0
    iget-object v1, p0, LX/1Ke;->d:LX/1Kj;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ComposerLauncherImpl"

    invoke-virtual {v1, v2, v3}, LX/1Kj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232944
    iget-object v1, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, LX/1Kg;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 232930
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232931
    iget-object v0, p0, LX/1Ke;->d:LX/1Kj;

    invoke-virtual {p4}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ComposerLauncherImpl"

    invoke-virtual {v0, v1, v2}, LX/1Kj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232932
    iget-object v0, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/1Ke;->c:LX/1Ki;

    invoke-interface {v1, p2, p1}, LX/1Ki;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v1

    invoke-static {v1, p4}, LX/1Kg;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p3, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 232933
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 232937
    iget-object v0, p0, LX/1Ke;->d:LX/1Kj;

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ComposerLauncherImpl"

    invoke-virtual {v0, v1, v2}, LX/1Kj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232938
    iget-object v0, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/1Ke;->c:LX/1Ki;

    invoke-interface {v1, p2, p1}, LX/1Ki;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v1

    invoke-virtual {p4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, LX/1Kg;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p3, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 232939
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 232934
    iget-object v0, p0, LX/1Ke;->d:LX/1Kj;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ComposerLauncherImpl"

    invoke-virtual {v0, v1, v2}, LX/1Kj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232935
    iget-object v0, p0, LX/1Ke;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/1Ke;->c:LX/1Ki;

    invoke-interface {v1, p2, p1}, LX/1Ki;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;)Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-result-object v1

    invoke-static {v1, p3}, LX/1Kg;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 232936
    return-void
.end method
