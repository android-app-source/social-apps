.class public final LX/123;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/124;


# instance fields
.field public final synthetic a:LX/120;


# direct methods
.method public constructor <init>(LX/120;)V
    .locals 0

    .prologue
    .line 173839
    iput-object p1, p0, LX/123;->a:LX/120;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/CompoundButton;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 173840
    iget-object v0, p0, LX/123;->a:LX/120;

    invoke-static {v0}, LX/120;->h(LX/120;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0yY;->DIALTONE_TOGGLE_INTERSTITIAL:LX/0yY;

    .line 173841
    :goto_0
    iget-object v1, p0, LX/123;->a:LX/120;

    iget-object v1, v1, LX/120;->m:LX/121;

    new-instance v2, LX/Bhl;

    invoke-direct {v2, p0, p3}, LX/Bhl;-><init>(LX/123;Landroid/widget/CompoundButton;)V

    invoke-virtual {v1, v0, p1, p2, v2}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 173842
    iget-object v1, p0, LX/123;->a:LX/120;

    iget-object v1, v1, LX/120;->m:LX/121;

    check-cast p4, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p4}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 173843
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->i()V

    .line 173844
    iget-object v0, p0, LX/123;->a:LX/120;

    const-string v1, "dialtone_switcher_enter_full_fb_dialog_impression"

    .line 173845
    invoke-static {v0, v1}, LX/120;->a$redex0(LX/120;Ljava/lang/String;)V

    .line 173846
    return-void

    .line 173847
    :cond_0
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 173848
    iget-object v0, p0, LX/123;->a:LX/120;

    invoke-virtual {v0}, LX/120;->d()Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    move-result-object v0

    .line 173849
    invoke-virtual {v0}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g()V

    .line 173850
    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->a(J)V

    .line 173851
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    const-string v1, "dialtone_manual_switcher"

    invoke-virtual {v0, v1}, LX/0yc;->a(Ljava/lang/String;)Z

    .line 173852
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0, p1}, LX/0yc;->a(Landroid/content/Context;)V

    .line 173853
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->f:LX/0Xl;

    const-string v1, "com.facebook.zero.ACTION_ZERO_INTERSTITIAL_REFRESH"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 173854
    iget-object v0, p0, LX/123;->a:LX/120;

    const-string v1, "dialtone_explicitly_entered"

    const-string v2, "dialtone_manual_switcher"

    .line 173855
    invoke-static {v0, v1, v2}, LX/120;->a$redex0(LX/120;Ljava/lang/String;Ljava/lang/String;)V

    .line 173856
    iget-object v0, p0, LX/123;->a:LX/120;

    invoke-virtual {v0}, LX/120;->g()V

    .line 173857
    return-void
.end method

.method private b(Landroid/widget/CompoundButton;Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 173858
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->o()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/123;->a:LX/120;

    invoke-static {v0}, LX/120;->h(LX/120;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173859
    :cond_1
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    const-string v1, "flex_toggle_upgrade_without_interstitial"

    invoke-virtual {v0, v1}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 173860
    iget-object v0, p0, LX/123;->a:LX/120;

    const-string v1, "flex_toggle_upgrade_without_interstitial"

    .line 173861
    invoke-static {v0, v1}, LX/120;->a$redex0(LX/120;Ljava/lang/String;)V

    .line 173862
    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 173863
    iget-object v0, p0, LX/123;->a:LX/120;

    invoke-virtual {v0}, LX/120;->g()V

    .line 173864
    :goto_0
    return-void

    .line 173865
    :cond_2
    invoke-virtual {p1, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 173866
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08062c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 173867
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0805f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 173868
    :goto_1
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yc;

    invoke-virtual {v0}, LX/0yc;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0805f0

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173869
    :goto_2
    invoke-direct {p0, v0, v1, p1, p2}, LX/123;->a(Ljava/lang/String;Ljava/lang/String;Landroid/widget/CompoundButton;Landroid/content/Context;)V

    goto :goto_0

    .line 173870
    :cond_3
    const v2, 0x7f0805f1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0df;->j:LX/0Tn;

    invoke-interface {v0, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 173871
    :cond_4
    const v0, 0x7f0805ef

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 173872
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-boolean v0, v0, LX/120;->a:Z

    if-eqz v0, :cond_0

    .line 173873
    :goto_0
    return-void

    .line 173874
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 173875
    sget-object v0, LX/0ax;->cK:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 173876
    iget-object v0, p0, LX/123;->a:LX/120;

    iget-object v0, v0, LX/120;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 173877
    iget-object v0, p0, LX/123;->a:LX/120;

    const-string v1, "dialtone_switcher_info_button_click"

    .line 173878
    invoke-static {v0, v1}, LX/120;->a$redex0(LX/120;Ljava/lang/String;)V

    .line 173879
    goto :goto_0
.end method

.method public final a(Landroid/widget/CompoundButton;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 173880
    iget-object v0, p0, LX/123;->a:LX/120;

    const-string v1, "dialtone_switcher_button_click"

    .line 173881
    invoke-static {v0, v1}, LX/120;->a$redex0(LX/120;Ljava/lang/String;)V

    .line 173882
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173883
    invoke-direct {p0, p1, p2}, LX/123;->b(Landroid/widget/CompoundButton;Landroid/content/Context;)V

    .line 173884
    :goto_0
    return-void

    .line 173885
    :cond_0
    invoke-direct {p0, p2}, LX/123;->b(Landroid/content/Context;)V

    goto :goto_0
.end method
