.class public LX/0r8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0r9;

.field private final b:LX/0pV;

.field private final c:J

.field public d:LX/230;

.field public e:J

.field private f:Z

.field private g:LX/0qw;


# direct methods
.method public constructor <init>(LX/0pV;J)V
    .locals 2

    .prologue
    .line 149102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149103
    sget-object v0, LX/0r9;->NOT_LOADING:LX/0r9;

    iput-object v0, p0, LX/0r8;->a:LX/0r9;

    .line 149104
    sget-object v0, LX/0qw;->FULL:LX/0qw;

    iput-object v0, p0, LX/0r8;->g:LX/0qw;

    .line 149105
    iput-object p1, p0, LX/0r8;->b:LX/0pV;

    .line 149106
    iput-wide p2, p0, LX/0r8;->c:J

    .line 149107
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0r8;->e:J

    .line 149108
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0r8;->f:Z

    .line 149109
    return-void
.end method

.method private a(LX/0r9;)V
    .locals 4

    .prologue
    .line 149097
    iget-object v0, p0, LX/0r8;->b:LX/0pV;

    const-string v1, "HeadLoaderStatus"

    sget-object v2, LX/0rj;->STATUS_CHANGED:LX/0rj;

    invoke-virtual {p1}, LX/0r9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;LX/0rj;Ljava/lang/String;)V

    .line 149098
    iput-object p1, p0, LX/0r8;->a:LX/0r9;

    .line 149099
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, LX/0r8;->e:J

    .line 149100
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0r8;->f:Z

    .line 149101
    return-void
.end method


# virtual methods
.method public final a(LX/0qw;)V
    .locals 0

    .prologue
    .line 149095
    iput-object p1, p0, LX/0r8;->g:LX/0qw;

    .line 149096
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 149092
    sget-object v0, LX/0r9;->NOT_LOADING:LX/0r9;

    invoke-direct {p0, v0}, LX/0r8;->a(LX/0r9;)V

    .line 149093
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0r8;->f:Z

    .line 149094
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 149087
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->LOADING_FOR_UI:LX/0r9;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 149088
    sget-object v0, LX/0r9;->LOADING_FOR_UI:LX/0r9;

    invoke-direct {p0, v0}, LX/0r8;->a(LX/0r9;)V

    .line 149089
    sget-object v0, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    iput-object v0, p0, LX/0r8;->g:LX/0qw;

    .line 149090
    return-void

    .line 149091
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 149110
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->NOT_LOADING:LX/0r9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 149111
    sget-object v0, LX/0r9;->LOADING_FOR_BACKGROUND:LX/0r9;

    invoke-direct {p0, v0}, LX/0r8;->a(LX/0r9;)V

    .line 149112
    sget-object v0, LX/0qw;->FULL:LX/0qw;

    iput-object v0, p0, LX/0r8;->g:LX/0qw;

    .line 149113
    return-void

    .line 149114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 149086
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->LOADING_FOR_BACKGROUND:LX/0r9;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 10

    .prologue
    .line 149080
    iget-wide v0, p0, LX/0r8;->c:J

    .line 149081
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, LX/0r8;->e:J

    add-long/2addr v7, v0

    cmp-long v5, v5, v7

    if-gez v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    move v0, v5

    .line 149082
    if-nez v0, :cond_0

    .line 149083
    iget-object v0, p0, LX/0r8;->b:LX/0pV;

    const-string v1, "HeadLoaderStatus"

    sget-object v2, LX/7zq;->STATUS_STUCK:LX/7zq;

    const-string v3, "StuckLoadingType"

    iget-object v4, p0, LX/0r8;->a:LX/0r9;

    invoke-virtual {v4}, LX/0r9;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 149084
    invoke-virtual {p0}, LX/0r8;->b()V

    .line 149085
    :cond_0
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->NOT_LOADING:LX/0r9;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 149076
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->NOT_LOADING:LX/0r9;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/0r8;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 149077
    sget-object v0, LX/0r9;->NOT_LOADING:LX/0r9;

    invoke-direct {p0, v0}, LX/0r8;->a(LX/0r9;)V

    .line 149078
    return-void

    .line 149079
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 149071
    iget-object v0, p0, LX/0r8;->a:LX/0r9;

    sget-object v1, LX/0r9;->NOT_LOADING:LX/0r9;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/0r8;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 149072
    sget-object v0, LX/0r9;->NOT_LOADING:LX/0r9;

    invoke-direct {p0, v0}, LX/0r8;->a(LX/0r9;)V

    .line 149073
    return-void

    .line 149074
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()LX/0qw;
    .locals 1

    .prologue
    .line 149075
    iget-object v0, p0, LX/0r8;->g:LX/0qw;

    return-object v0
.end method
