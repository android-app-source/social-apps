.class public final LX/15i;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile b:Z


# instance fields
.field public volatile a:LX/49M;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/nio/ByteBuffer;

.field private volatile e:LX/16b;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile f:LX/1oO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field private final h:LX/15j;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:[I

.field public volatile l:LX/1wJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181384
    const/4 v0, 0x0

    sput-boolean v0, LX/15i;->b:Z

    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V
    .locals 2
    .param p2    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 181368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181369
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/15i;->c:Ljava/lang/Object;

    .line 181370
    const/4 v0, 0x0

    iput-object v0, p0, LX/15i;->k:[I

    .line 181371
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    .line 181372
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 181373
    iput-boolean p4, p0, LX/15i;->g:Z

    .line 181374
    iput-object p5, p0, LX/15i;->h:LX/15j;

    .line 181375
    iget-boolean v0, p0, LX/15i;->g:Z

    if-eqz v0, :cond_0

    .line 181376
    if-eqz p2, :cond_1

    .line 181377
    :try_start_0
    new-instance v0, LX/16b;

    invoke-direct {v0, p2}, LX/16b;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v0, p0, LX/15i;->e:LX/16b;

    .line 181378
    :goto_0
    if-eqz p3, :cond_0

    .line 181379
    new-instance v0, LX/1oO;

    invoke-direct {v0, p3}, LX/1oO;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v0, p0, LX/15i;->f:LX/1oO;

    .line 181380
    :cond_0
    return-void

    .line 181381
    :cond_1
    new-instance v0, LX/16b;

    iget-object v1, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-direct {v0, v1}, LX/16b;-><init>(I)V

    iput-object v0, p0, LX/15i;->e:LX/16b;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181382
    :catch_0
    move-exception v0

    .line 181383
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method private a(IILjava/lang/Class;I)LX/22e;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;I)",
            "LX/22e",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 181337
    invoke-virtual {p0, p1, p2}, LX/15i;->p(II)I

    move-result v1

    .line 181338
    if-nez v1, :cond_1

    .line 181339
    :cond_0
    :goto_0
    return-object v0

    .line 181340
    :cond_1
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22e;

    .line 181341
    if-nez v0, :cond_0

    .line 181342
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 181343
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v2

    .line 181344
    iget-object v0, v2, LX/25g;->c:Ljava/nio/ByteBuffer;

    .line 181345
    invoke-virtual {v2, p1}, LX/25g;->b(I)I

    move-result p1

    .line 181346
    :goto_1
    packed-switch p4, :pswitch_data_0

    .line 181347
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181348
    :catch_0
    move-exception v0

    .line 181349
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0

    .line 181350
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 181351
    :pswitch_0
    sget-object v2, LX/4Bs;->a:LX/4Bs;

    sget-object p3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v0, p1, p2, v2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;

    move-result-object v2

    move-object v0, v2
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 181352
    :goto_2
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22e;

    goto :goto_0

    .line 181353
    :pswitch_1
    :try_start_2
    sget-object v2, LX/4Bt;->a:LX/4Bt;

    sget-object p3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v0, p1, p2, v2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;

    move-result-object v2

    move-object v0, v2

    .line 181354
    goto :goto_2

    .line 181355
    :pswitch_2
    sget-object v2, LX/1VT;->a:LX/1VT;

    sget-object p3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v0, p1, p2, v2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;

    move-result-object v2

    move-object v0, v2

    .line 181356
    goto :goto_2

    .line 181357
    :pswitch_3
    sget-object v2, LX/1VP;->a:LX/1VP;

    .line 181358
    invoke-static {v0, p1, p2, v2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;

    move-result-object v2

    move-object v0, v2

    .line 181359
    goto :goto_2
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    .line 181360
    :pswitch_4
    :try_start_3
    const-string v2, "fromString"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class p4, Ljava/lang/String;

    aput-object p4, v3, v4

    invoke-virtual {p3, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    move-result-object v2

    .line 181361
    :goto_3
    new-instance v3, LX/3DE;

    invoke-direct {v3, v2}, LX/3DE;-><init>(Ljava/lang/reflect/Method;)V

    invoke-static {v0, p1, p2, v3, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;

    move-result-object v2

    move-object v0, v2

    .line 181362
    goto :goto_2

    .line 181363
    :pswitch_5
    iget-object v2, p0, LX/15i;->l:LX/1wJ;

    if-nez v2, :cond_3

    .line 181364
    new-instance v2, LX/1wI;

    invoke-direct {v2, p0}, LX/1wI;-><init>(LX/15i;)V

    iput-object v2, p0, LX/15i;->l:LX/1wJ;

    .line 181365
    :cond_3
    iget-object v2, p0, LX/15i;->l:LX/1wJ;

    move-object v2, v2

    .line 181366
    invoke-static {v0, p1, p2, v2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)LX/22d;
    :try_end_4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    goto :goto_2

    .line 181367
    :catch_1
    const/4 v2, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(LX/15i;Ljava/lang/Exception;)LX/4Bu;
    .locals 2

    .prologue
    .line 181334
    iget-object v0, p0, LX/15i;->h:LX/15j;

    if-eqz v0, :cond_0

    .line 181335
    iget-object v0, p0, LX/15i;->h:LX/15j;

    invoke-virtual {v0}, LX/15j;->a()V

    .line 181336
    :cond_0
    new-instance v0, LX/4Bu;

    invoke-direct {p0}, LX/15i;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4Bu;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(Ljava/nio/ByteBuffer;LX/16a;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 12
    .param p2    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 181330
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v5, p2

    .line 181331
    new-instance v6, LX/15i;

    move-object v7, v0

    move-object v8, v2

    move-object v9, v3

    move v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 181332
    invoke-virtual {v6, v1}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    move-object v0, v6

    .line 181333
    return-object v0
.end method

.method public static a(Ljava/nio/ByteBuffer;Ljava/lang/Class;LX/15j;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 12
    .param p2    # LX/15j;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/nio/ByteBuffer;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/facebook/flatbuffers/MutableFlatBuffer$FlatBufferCorruptionHandler;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 181326
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v5, p2

    .line 181327
    new-instance v6, LX/15i;

    move-object v7, v0

    move-object v8, v2

    move-object v9, v3

    move v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 181328
    invoke-virtual {v6, v1}, LX/15i;->a(Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    move-object v0, v6

    .line 181329
    return-object v0
.end method

.method private static a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181320
    if-nez p0, :cond_0

    .line 181321
    const/4 v0, 0x0

    .line 181322
    :goto_0
    return-object v0

    .line 181323
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 181324
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 181325
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static a(LX/15i;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 181317
    sget-boolean v0, LX/15i;->b:Z

    if-nez v0, :cond_0

    .line 181318
    :goto_0
    return-void

    .line 181319
    :cond_0
    const-string v0, "Set %s, position (%d,%d)"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/15i;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(ITT;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181203
    if-nez p1, :cond_0

    .line 181204
    if-eqz p2, :cond_1

    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_1

    .line 181205
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot memoize non-null non-String at 0 offset"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181206
    :cond_0
    monitor-enter p0

    .line 181207
    :try_start_0
    iget-object v1, p0, LX/15i;->j:Landroid/util/SparseArray;

    .line 181208
    if-eqz v1, :cond_2

    .line 181209
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 181210
    if-eqz v0, :cond_2

    .line 181211
    monitor-exit p0

    move-object p2, v0

    .line 181212
    :cond_1
    :goto_0
    return-object p2

    .line 181213
    :cond_2
    if-eqz p2, :cond_3

    .line 181214
    if-nez v1, :cond_4

    .line 181215
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 181216
    iput-object v0, p0, LX/15i;->j:Landroid/util/SparseArray;

    .line 181217
    :goto_1
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 181218
    monitor-exit p0

    goto :goto_0

    .line 181219
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181220
    const/4 p2, 0x0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 181299
    sget-boolean v0, LX/15i;->b:Z

    if-nez v0, :cond_0

    .line 181300
    :goto_0
    return-void

    .line 181301
    :cond_0
    iget-object v1, p0, LX/15i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 181302
    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 181303
    instance-of v2, v0, Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 181304
    check-cast v0, Ljava/util/ArrayList;

    .line 181305
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_1

    .line 181306
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 181307
    const-string v2, "TRUNCATED"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181308
    :cond_1
    const-string v2, "[%s @ %d] %s"

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181309
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 181310
    :cond_2
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 181311
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v0}, LX/15i;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public static e(LX/15i;I)I
    .locals 2

    .prologue
    .line 181293
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 181294
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181295
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    .line 181296
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181297
    :catch_0
    move-exception v0

    .line 181298
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method private g()Ljava/lang/String;
    .locals 5

    .prologue
    .line 181276
    sget-boolean v0, LX/15i;->b:Z

    if-nez v0, :cond_0

    .line 181277
    const-string v0, ""

    .line 181278
    :goto_0
    return-object v0

    .line 181279
    :cond_0
    iget-object v1, p0, LX/15i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 181280
    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    .line 181281
    instance-of v0, v2, Ljava/lang/String;

    if-nez v0, :cond_1

    .line 181282
    const-string v0, ""

    monitor-exit v1

    goto :goto_0

    .line 181283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 181284
    :cond_1
    const/4 v0, 0x6

    :try_start_1
    invoke-virtual {p0, v0}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 181285
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 181286
    const-string v4, "Source: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181287
    instance-of v2, v0, Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 181288
    const-string v2, "\nHistory:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181289
    check-cast v0, Ljava/util/ArrayList;

    .line 181290
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181291
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 181292
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized i(LX/15i;)LX/16b;
    .locals 2

    .prologue
    .line 181270
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/15i;->g:Z

    if-nez v0, :cond_0

    .line 181271
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Mutation support is not turned on."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 181273
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-nez v0, :cond_1

    .line 181274
    new-instance v0, LX/16b;

    iget-object v1, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-direct {v0, v1}, LX/16b;-><init>(I)V

    iput-object v0, p0, LX/15i;->e:LX/16b;

    .line 181275
    :cond_1
    iget-object v0, p0, LX/15i;->e:LX/16b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method private static r(LX/15i;II)V
    .locals 2

    .prologue
    .line 181265
    iget-object v0, p0, LX/15i;->a:LX/49M;

    .line 181266
    if-nez v0, :cond_1

    .line 181267
    :cond_0
    :goto_0
    return-void

    .line 181268
    :cond_1
    iget-object v1, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 181269
    invoke-virtual {v0, p1, p2}, LX/49M;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIB)B
    .locals 2

    .prologue
    .line 181254
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181255
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181256
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181257
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->g(II)B

    move-result v0

    .line 181258
    :goto_0
    return v0

    .line 181259
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181260
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181261
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIB)B

    move-result v0

    goto :goto_0

    .line 181262
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIB)B
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181263
    :catch_0
    move-exception v0

    .line 181264
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(IID)D
    .locals 3

    .prologue
    .line 181243
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181244
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181245
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181246
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->j(II)D

    move-result-wide v0

    .line 181247
    :goto_0
    return-wide v0

    .line 181248
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181249
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181250
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3, p4}, LX/0ah;->a(Ljava/nio/ByteBuffer;IID)D

    move-result-wide v0

    goto :goto_0

    .line 181251
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3, p4}, LX/0ah;->a(Ljava/nio/ByteBuffer;IID)D
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 181252
    :catch_0
    move-exception v0

    .line 181253
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(IIF)F
    .locals 2

    .prologue
    .line 181232
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181233
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181234
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181235
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->i(II)F

    move-result v0

    .line 181236
    :goto_0
    return v0

    .line 181237
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181238
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181239
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIF)F

    move-result v0

    goto :goto_0

    .line 181240
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIF)F
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181241
    :catch_0
    move-exception v0

    .line 181242
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(III)I
    .locals 2

    .prologue
    .line 181221
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181222
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181223
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181224
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->b(II)I

    move-result v0

    .line 181225
    :goto_0
    return v0

    .line 181226
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181227
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181228
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I

    move-result v0

    goto :goto_0

    .line 181229
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;III)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181230
    :catch_0
    move-exception v0

    .line 181231
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(IIJ)J
    .locals 3

    .prologue
    .line 181491
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181492
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181493
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181494
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->d(II)J

    move-result-wide v0

    .line 181495
    :goto_0
    return-wide v0

    .line 181496
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181497
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181498
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3, p4}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIJ)J

    move-result-wide v0

    goto :goto_0

    .line 181499
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3, p4}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIJ)J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 181500
    :catch_0
    move-exception v0

    .line 181501
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(ILX/16a;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 181385
    if-gtz p1, :cond_1

    .line 181386
    :cond_0
    :goto_0
    return-object v0

    .line 181387
    :cond_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v1

    .line 181388
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 181389
    invoke-interface {p2, v1}, LX/16a;->a(S)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 181390
    if-eqz v1, :cond_0

    .line 181391
    :try_start_0
    invoke-interface {v1, p0, v2}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/15i;I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 181392
    goto :goto_0

    .line 181393
    :catch_0
    move-exception v0

    .line 181394
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181482
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 181483
    :try_start_1
    invoke-interface {v0, p0, p1}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/15i;I)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    .line 181484
    return-object v0

    .line 181485
    :catch_0
    move-exception v0

    .line 181486
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal access for root object:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 181487
    :catch_1
    move-exception v0

    .line 181488
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not able to create root object:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 181489
    :catch_2
    move-exception v0

    .line 181490
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181478
    :try_start_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 181479
    invoke-virtual {p0, v0, p1}, LX/15i;->a(ILX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0

    .line 181480
    :catch_0
    move-exception v0

    .line 181481
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181471
    :try_start_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 181472
    if-gtz v0, :cond_0

    .line 181473
    const/4 v0, 0x0

    .line 181474
    :goto_0
    return-object v0

    .line 181475
    :catch_0
    move-exception v0

    .line 181476
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0

    .line 181477
    :cond_0
    invoke-virtual {p0, v0, p1}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILcom/facebook/flatbuffers/Flattenable;Ljava/lang/Class;)Lcom/facebook/graphql/model/extras/BaseExtra;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Extra;",
            ">(I",
            "Lcom/facebook/flatbuffers/Flattenable;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 181464
    iget-boolean v0, p0, LX/15i;->g:Z

    if-nez v0, :cond_0

    .line 181465
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getExtra called when mMutationSupported = false"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181466
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    if-nez v0, :cond_1

    .line 181467
    new-instance v0, LX/1oO;

    invoke-direct {v0}, LX/1oO;-><init>()V

    iput-object v0, p0, LX/15i;->f:LX/1oO;

    .line 181468
    :cond_1
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    invoke-virtual {v0, p1, p2, p3}, LX/1oO;->a(ILcom/facebook/flatbuffers/Flattenable;Ljava/lang/Class;)Lcom/facebook/graphql/model/extras/BaseExtra;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 181469
    :catch_0
    move-exception v0

    .line 181470
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(IILjava/lang/Class;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181450
    if-nez p1, :cond_0

    .line 181451
    const/4 v0, 0x0

    .line 181452
    :goto_0
    return-object v0

    .line 181453
    :cond_0
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181454
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_2

    .line 181455
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181456
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->b(II)I

    move-result v0

    .line 181457
    int-to-short v0, v0

    invoke-static {v0, p3}, LX/0ah;->a(SLjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0

    .line 181458
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 181459
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181460
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0

    .line 181461
    :cond_2
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181462
    :catch_0
    move-exception v0

    .line 181463
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(II",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181440
    invoke-virtual {p0, p1, p2}, LX/15i;->p(II)I

    move-result v1

    .line 181441
    if-nez v1, :cond_1

    .line 181442
    :cond_0
    :goto_0
    return-object p4

    .line 181443
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 181444
    if-nez v0, :cond_2

    .line 181445
    invoke-virtual {p0, p1, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    .line 181446
    if-eqz v0, :cond_0

    .line 181447
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    .line 181448
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    :cond_2
    move-object p4, v0

    .line 181449
    goto :goto_0
.end method

.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 181439
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/15i;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIS)S
    .locals 2

    .prologue
    .line 181502
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181503
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181504
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181505
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->h(II)S

    move-result v0

    .line 181506
    :goto_0
    return v0

    .line 181507
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181508
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181509
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIS)S

    move-result v0

    goto :goto_0

    .line 181510
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->a(Ljava/nio/ByteBuffer;IIS)S
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181511
    :catch_0
    move-exception v0

    .line 181512
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 181436
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 181437
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/15i;->g:Z

    .line 181438
    return-void
.end method

.method public final a(IILcom/facebook/flatbuffers/Flattenable;)V
    .locals 2
    .param p3    # Lcom/facebook/flatbuffers/Flattenable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 181428
    if-eqz p3, :cond_0

    .line 181429
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 181430
    invoke-interface {p3, v0}, Lcom/facebook/flatbuffers/Flattenable;->a(LX/186;)I

    move-result v1

    .line 181431
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 181432
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    move-object v0, v0

    .line 181433
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(II[B)V

    .line 181434
    return-void

    .line 181435
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IILjava/lang/Enum;)V
    .locals 2

    .prologue
    .line 181423
    if-nez p3, :cond_0

    .line 181424
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value for setEnum call should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181425
    :cond_0
    const-string v0, "Enum"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181426
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, LX/16b;->a(III)V

    .line 181427
    return-void
.end method

.method public final a(IILjava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 181419
    const-string v0, "String"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181420
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    .line 181421
    invoke-static {v0, p1, p2, p3}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 181422
    return-void
.end method

.method public final a(IILjava/util/List;)V
    .locals 2
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 181411
    if-eqz p3, :cond_0

    .line 181412
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 181413
    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, LX/186;->a(Ljava/util/List;Z)I

    move-result v1

    .line 181414
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 181415
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    move-object v0, v0

    .line 181416
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(II[B)V

    .line 181417
    return-void

    .line 181418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIZ)V
    .locals 1

    .prologue
    .line 181407
    const-string v0, "Boolean"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181408
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    .line 181409
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-static {v0, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 181410
    return-void
.end method

.method public final a(II[B)V
    .locals 1
    .param p3    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 181401
    :try_start_0
    const-string v0, "Extension"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181402
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/16b;->a(II[B)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181403
    return-void

    .line 181404
    :catch_0
    move-exception v0

    .line 181405
    :goto_0
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0

    .line 181406
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final declared-synchronized a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 181312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 181313
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;

    .line 181314
    :cond_0
    iget-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181315
    monitor-exit p0

    return-void

    .line 181316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/15i;)V
    .locals 3

    .prologue
    .line 181395
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 181396
    :cond_0
    monitor-exit p0

    return-void

    .line 181397
    :cond_1
    const/4 v0, 0x0

    :goto_0
    :try_start_1
    iget-object v1, p0, LX/15i;->i:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 181398
    iget-object v1, p0, LX/15i;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    iget-object v2, p0, LX/15i;->i:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/15i;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181399
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 180995
    sget-boolean v0, LX/15i;->b:Z

    if-nez v0, :cond_0

    .line 180996
    :goto_0
    return-void

    .line 180997
    :cond_0
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180998
    iget-object v1, p0, LX/15i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 180999
    const/4 v0, 0x5

    :try_start_0
    const-string v2, "[%s @ %d] %s"

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, LX/15i;->a(ILjava/lang/Object;)V

    .line 181000
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V
    .locals 3

    .prologue
    .line 181086
    sget-boolean v0, LX/15i;->b:Z

    if-nez v0, :cond_0

    .line 181087
    :goto_0
    return-void

    .line 181088
    :cond_0
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181089
    iget-object v1, p0, LX/15i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 181090
    :try_start_0
    invoke-virtual {p0, p1}, LX/15i;->a(Ljava/lang/String;)V

    .line 181091
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Converted from "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/15i;->b(Ljava/lang/String;)V

    .line 181092
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181077
    :try_start_0
    iget-object v2, p0, LX/15i;->e:LX/16b;

    if-eqz v2, :cond_2

    .line 181078
    iget-object v2, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-lt p1, v2, :cond_2

    .line 181079
    iget-object v2, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v2, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v2

    .line 181080
    iget-object v3, v2, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, p1}, LX/25g;->b(I)I

    move-result v2

    invoke-static {v3, v2, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I

    move-result v2

    if-eqz v2, :cond_1

    .line 181081
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 181082
    goto :goto_0

    .line 181083
    :cond_2
    iget-object v2, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v2, p1, p2}, LX/0ah;->k(Ljava/nio/ByteBuffer;II)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 181084
    :catch_0
    move-exception v0

    .line 181085
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final declared-synchronized b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181093
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 181094
    const/4 v0, 0x0

    .line 181095
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/15i;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 181096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(IILX/16a;)Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "LX/16a;",
            ")",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181071
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v0

    .line 181072
    if-eqz v0, :cond_0

    .line 181073
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v1

    .line 181074
    add-int/lit8 v2, v0, 0x4

    move v2, v2

    .line 181075
    new-instance v0, LX/17F;

    invoke-direct {v0, p0, v2, v1, p3}, LX/17F;-><init>(LX/15i;IILX/16a;)V

    .line 181076
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(IILjava/lang/Class;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181064
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181065
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 181066
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181067
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->c(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    .line 181068
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->c(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181069
    :catch_0
    move-exception v0

    .line 181070
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final b(III)V
    .locals 1

    .prologue
    .line 181061
    const-string v0, "Integer"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181062
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LX/16b;->a(III)V

    .line 181063
    return-void
.end method

.method public final b(IIJ)V
    .locals 1

    .prologue
    .line 181057
    const-string v0, "Long"

    invoke-static {p0, v0, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 181058
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v0

    .line 181059
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-static {v0, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 181060
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 181054
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_0

    .line 181055
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0}, LX/16b;->a()Z

    move-result v0

    .line 181056
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 2

    .prologue
    .line 181041
    if-nez p1, :cond_0

    .line 181042
    const/4 v0, 0x0

    .line 181043
    :goto_0
    return v0

    .line 181044
    :cond_0
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181045
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_2

    .line 181046
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181047
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->c(II)Z

    move-result v0

    goto :goto_0

    .line 181048
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 181049
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181050
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2}, LX/0ah;->a(Ljava/nio/ByteBuffer;II)Z

    move-result v0

    goto :goto_0

    .line 181051
    :cond_2
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->a(Ljava/nio/ByteBuffer;II)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181052
    :catch_0
    move-exception v0

    .line 181053
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 181032
    if-nez p1, :cond_0

    .line 181033
    const/4 v0, 0x0

    .line 181034
    :goto_0
    return v0

    .line 181035
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181036
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181037
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I

    move-result v0

    goto :goto_0

    .line 181038
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1}, LX/0ah;->d(Ljava/nio/ByteBuffer;I)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181039
    :catch_0
    move-exception v0

    .line 181040
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final c(II)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181031
    invoke-virtual {p0, p1, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/nio/ByteBuffer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181025
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_0

    .line 181026
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0}, LX/16b;->b()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/15i;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181027
    :goto_0
    return-object v0

    .line 181028
    :catch_0
    move-exception v0

    .line 181029
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0

    .line 181030
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(IILjava/lang/Class;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181019
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 181020
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181021
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2, p3}, LX/0ah;->e(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    .line 181022
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2, p3}, LX/0ah;->e(Ljava/nio/ByteBuffer;IILjava/lang/Class;)Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181023
    :catch_0
    move-exception v0

    .line 181024
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 181016
    if-nez p1, :cond_0

    .line 181017
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181018
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v0

    return v0
.end method

.method public final d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181012
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v0

    .line 181013
    if-eqz v0, :cond_0

    .line 181014
    invoke-virtual {p0, v0, p3}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 181015
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(II)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181001
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181002
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    .line 181003
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181004
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->e(II)Ljava/lang/String;

    move-result-object v0

    .line 181005
    :goto_0
    return-object v0

    .line 181006
    :cond_0
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181007
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181008
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 181009
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181010
    :catch_0
    move-exception v0

    .line 181011
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 180992
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    if-eqz v0, :cond_0

    .line 180993
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    invoke-virtual {v0}, LX/1oO;->b()Z

    move-result v0

    .line 180994
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/nio/ByteBuffer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181131
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    if-eqz v0, :cond_0

    .line 181132
    :try_start_0
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    invoke-virtual {v0}, LX/1oO;->c()Z

    .line 181133
    iget-object v0, p0, LX/15i;->f:LX/1oO;

    invoke-virtual {v0}, LX/1oO;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/15i;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181134
    :goto_0
    return-object v0

    .line 181135
    :catch_0
    move-exception v0

    .line 181136
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0

    .line 181137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(II)Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181186
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181187
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_2

    .line 181188
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181189
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->f(II)I

    move-result v0

    .line 181190
    if-nez v0, :cond_0

    .line 181191
    const/4 v0, 0x0

    .line 181192
    :goto_0
    return-object v0

    .line 181193
    :cond_0
    iget-object v1, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v1, v0}, LX/16b;->a(I)LX/25g;

    move-result-object v1

    .line 181194
    iget-object v2, v1, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, LX/25g;->b(I)I

    move-result v0

    .line 181195
    sget-object v1, LX/4Bs;->a:LX/4Bs;

    sget-object p1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0, v1, p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;ILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v1

    move-object v0, v1

    .line 181196
    goto :goto_0

    .line 181197
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 181198
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181199
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2}, LX/0ah;->d(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 181200
    :cond_2
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->d(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181201
    :catch_0
    move-exception v0

    .line 181202
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final e(IILjava/lang/Class;)Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181180
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v0

    .line 181181
    if-eqz v0, :cond_0

    .line 181182
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v1

    .line 181183
    add-int/lit8 v2, v0, 0x4

    move v2, v2

    .line 181184
    new-instance v0, LX/17F;

    invoke-direct {v0, p0, v2, v1, p3}, LX/17F;-><init>(LX/15i;IILjava/lang/Class;)V

    .line 181185
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 181173
    invoke-virtual {p0, p1, p2}, LX/15i;->p(II)I

    move-result v1

    .line 181174
    if-nez v1, :cond_1

    .line 181175
    :cond_0
    :goto_0
    return-object v0

    .line 181176
    :cond_1
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 181177
    if-nez v0, :cond_0

    .line 181178
    invoke-virtual {p0, v1, p3}, LX/15i;->a(ILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 181179
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    goto :goto_0
.end method

.method public final f(II)Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181156
    :try_start_0
    invoke-static {p0, p1, p2}, LX/15i;->r(LX/15i;II)V

    .line 181157
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_2

    .line 181158
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181159
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->f(II)I

    move-result v0

    .line 181160
    if-nez v0, :cond_0

    .line 181161
    const/4 v0, 0x0

    .line 181162
    :goto_0
    return-object v0

    .line 181163
    :cond_0
    iget-object v1, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v1, v0}, LX/16b;->a(I)LX/25g;

    move-result-object v1

    .line 181164
    iget-object v2, v1, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, LX/25g;->b(I)I

    move-result v0

    .line 181165
    sget-object v1, LX/1VT;->a:LX/1VT;

    sget-object p1, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0, v1, p1}, LX/0ah;->a(Ljava/nio/ByteBuffer;ILX/1VQ;Ljava/lang/Object;)LX/1VR;

    move-result-object v1

    move-object v0, v1

    .line 181166
    goto :goto_0

    .line 181167
    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 181168
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181169
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v0

    invoke-static {v1, v0, p2}, LX/0ah;->g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0

    .line 181170
    :cond_2
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->g(Ljava/nio/ByteBuffer;II)Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 181171
    :catch_0
    move-exception v0

    .line 181172
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final g(II)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 181143
    if-nez p1, :cond_1

    .line 181144
    :cond_0
    :goto_0
    return v0

    .line 181145
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/15i;->e:LX/16b;

    if-eqz v1, :cond_3

    .line 181146
    iget-object v1, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v1, p1, p2}, LX/16b;->a(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 181147
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1, p2}, LX/16b;->f(II)I

    move-result v0

    goto :goto_0

    .line 181148
    :cond_2
    iget-object v1, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-lt p1, v1, :cond_3

    .line 181149
    iget-object v1, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v1, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v1

    .line 181150
    iget-object v2, v1, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, LX/25g;->b(I)I

    move-result v3

    invoke-static {v2, v3, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I

    move-result v2

    .line 181151
    if-eqz v2, :cond_0

    .line 181152
    invoke-virtual {v1, v2}, LX/25g;->c(I)I

    move-result v0

    goto :goto_0

    .line 181153
    :cond_3
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->j(Ljava/nio/ByteBuffer;II)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181154
    :catch_0
    move-exception v0

    .line 181155
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method

.method public final g(IILjava/lang/Class;)LX/22e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/22e",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181142
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, p3, v0}, LX/15i;->a(IILjava/lang/Class;I)LX/22e;

    move-result-object v0

    return-object v0
.end method

.method public final h(IILjava/lang/Class;)LX/22e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LX/22e",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181141
    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, p3, v0}, LX/15i;->a(IILjava/lang/Class;I)LX/22e;

    move-result-object v0

    return-object v0
.end method

.method public final h(II)Z
    .locals 1

    .prologue
    .line 181138
    if-nez p1, :cond_0

    .line 181139
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181140
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/15i;->b(II)Z

    move-result v0

    return v0
.end method

.method public final i(II)S
    .locals 1

    .prologue
    .line 181097
    if-nez p1, :cond_0

    .line 181098
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181099
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IIS)S

    move-result v0

    return v0
.end method

.method public final j(II)I
    .locals 1

    .prologue
    .line 181128
    if-nez p1, :cond_0

    .line 181129
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181130
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(III)I

    move-result v0

    return v0
.end method

.method public final k(II)J
    .locals 2

    .prologue
    .line 181125
    if-nez p1, :cond_0

    .line 181126
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181127
    :cond_0
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final l(II)D
    .locals 2

    .prologue
    .line 181122
    if-nez p1, :cond_0

    .line 181123
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181124
    :cond_0
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/15i;->a(IID)D

    move-result-wide v0

    return-wide v0
.end method

.method public final m(II)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181114
    if-nez p1, :cond_0

    .line 181115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181116
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v1

    .line 181117
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181118
    if-nez v0, :cond_1

    .line 181119
    invoke-virtual {p0, p1, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 181120
    invoke-direct {p0, v1, v0}, LX/15i;->b(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181121
    :cond_1
    return-object v0
.end method

.method public final n(II)LX/22e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/22e",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181113
    const-class v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/15i;->a(IILjava/lang/Class;I)LX/22e;

    move-result-object v0

    return-object v0
.end method

.method public final o(II)LX/22e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/22e",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 181112
    const-class v0, Ljava/lang/String;

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, v0, v1}, LX/15i;->a(IILjava/lang/Class;I)LX/22e;

    move-result-object v0

    return-object v0
.end method

.method public final p(II)I
    .locals 1

    .prologue
    .line 181109
    if-nez p1, :cond_0

    .line 181110
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181111
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/15i;->g(II)I

    move-result v0

    return v0
.end method

.method public final q(II)I
    .locals 3

    .prologue
    .line 181100
    if-nez p1, :cond_0

    .line 181101
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181102
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/15i;->e:LX/16b;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 181103
    iget-object v0, p0, LX/15i;->e:LX/16b;

    invoke-virtual {v0, p1}, LX/16b;->a(I)LX/25g;

    move-result-object v0

    .line 181104
    iget-object v1, v0, LX/25g;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, LX/25g;->b(I)I

    move-result v2

    invoke-static {v1, v2, p2}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I

    move-result v1

    .line 181105
    invoke-virtual {v0, v1}, LX/25g;->c(I)I

    move-result v0

    .line 181106
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/15i;->d:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1, p2}, LX/0ah;->i(Ljava/nio/ByteBuffer;II)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181107
    :catch_0
    move-exception v0

    .line 181108
    invoke-static {p0, v0}, LX/15i;->a(LX/15i;Ljava/lang/Exception;)LX/4Bu;

    move-result-object v0

    throw v0
.end method
