.class public LX/1kv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0sT;

.field public final c:LX/0sg;

.field public final d:LX/1kw;

.field public final e:LX/0t8;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0sT;LX/0sg;LX/1kw;LX/0t8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/4Ud;",
            ">;",
            "LX/0sT;",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "LX/1kw;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 310483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310484
    iput-object p1, p0, LX/1kv;->a:LX/0Ot;

    .line 310485
    iput-object p2, p0, LX/1kv;->b:LX/0sT;

    .line 310486
    iput-object p3, p0, LX/1kv;->c:LX/0sg;

    .line 310487
    iput-object p4, p0, LX/1kv;->d:LX/1kw;

    .line 310488
    iput-object p5, p0, LX/1kv;->e:LX/0t8;

    .line 310489
    return-void
.end method

.method public static a(LX/0QB;)LX/1kv;
    .locals 1

    .prologue
    .line 310490
    invoke-static {p0}, LX/1kv;->b(LX/0QB;)LX/1kv;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1kv;
    .locals 6

    .prologue
    .line 310491
    new-instance v0, LX/1kv;

    const/16 v1, 0x235e

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v2

    check-cast v2, LX/0sT;

    invoke-static {p0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v3

    check-cast v3, LX/0sg;

    const-class v4, LX/1kw;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1kw;

    .line 310492
    sget-object v5, LX/0t7;->a:LX/0t8;

    move-object v5, v5

    .line 310493
    check-cast v5, LX/0t8;

    invoke-direct/range {v0 .. v5}, LX/1kv;-><init>(LX/0Ot;LX/0sT;LX/0sg;LX/1kw;LX/0t8;)V

    .line 310494
    return-object v0
.end method


# virtual methods
.method public final a(LX/0v6;Ljava/util/Map;)LX/2lk;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "Ljava/util/Map",
            "<",
            "LX/0zO;",
            "LX/3U1;",
            ">;)",
            "LX/2lk;"
        }
    .end annotation

    .prologue
    .line 310495
    iget-boolean v0, p1, LX/0v6;->l:Z

    move v0, v0

    .line 310496
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1kv;->b:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 310497
    const/4 v8, 0x0

    .line 310498
    new-instance v10, LX/4VM;

    invoke-direct {v10}, LX/4VM;-><init>()V

    .line 310499
    new-instance v7, LX/4Ug;

    invoke-direct {v7, v8, v10}, LX/4Ug;-><init>(Ljava/util/Collection;LX/4VM;)V

    .line 310500
    new-instance v4, LX/4Ue;

    iget-object v1, p0, LX/1kv;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4Ud;

    invoke-direct {v4, v1}, LX/4Ue;-><init>(LX/4Ud;)V

    .line 310501
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 310502
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, LX/0zO;

    .line 310503
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3U1;

    .line 310504
    invoke-virtual {v9}, LX/0zO;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310505
    iget-object v1, v1, LX/3U1;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 310506
    invoke-static {v1}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v1

    .line 310507
    iget-object v2, v9, LX/0zO;->m:LX/0gW;

    move-object v3, v2

    .line 310508
    if-eqz v1, :cond_1

    .line 310509
    invoke-virtual {v3}, LX/0gW;->s()I

    move-result v2

    .line 310510
    iget-boolean v5, v3, LX/0gW;->m:Z

    move v3, v5

    .line 310511
    iget-object v5, p0, LX/1kv;->e:LX/0t8;

    const/4 v6, 0x1

    invoke-static/range {v1 .. v8}, LX/4V0;->a(LX/15i;IZLX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    goto :goto_0

    .line 310512
    :cond_2
    iget-object v1, p0, LX/1kv;->d:LX/1kw;

    invoke-virtual {v1, v10}, LX/1kw;->a(LX/4VM;)LX/4VN;

    move-result-object v1

    move-object v0, v1

    .line 310513
    :goto_1
    return-object v0

    .line 310514
    :cond_3
    iget-object v0, p0, LX/1kv;->c:LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object v2

    .line 310515
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 310516
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0zO;

    .line 310517
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3U1;

    .line 310518
    invoke-virtual {v1}, LX/0zO;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 310519
    iget-object v0, v0, LX/3U1;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 310520
    invoke-static {v0, v2}, LX/1l8;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/2lk;)V

    goto :goto_2

    .line 310521
    :cond_5
    move-object v0, v2

    .line 310522
    goto :goto_1
.end method

.method public final a(Lcom/facebook/flatbuffers/MutableFlattenable;)LX/2lk;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 310523
    instance-of v0, p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1kv;->b:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 310524
    iget-object v0, p0, LX/1kv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Ud;

    move-object v1, p1

    check-cast v1, Lcom/facebook/graphql/modelutil/FragmentModel;

    invoke-interface {v1}, Lcom/facebook/graphql/modelutil/FragmentModel;->d_()I

    move-result v1

    invoke-interface {v0, v1}, LX/4Ud;->a(I)[[I

    move-result-object v1

    .line 310525
    :goto_0
    if-eqz v1, :cond_1

    .line 310526
    new-instance v7, LX/4VM;

    invoke-direct {v7}, LX/4VM;-><init>()V

    .line 310527
    iget-object v0, p0, LX/1kv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4Ud;

    iget-object v3, p0, LX/1kv;->e:LX/0t8;

    const/4 v4, 0x0

    new-instance v5, LX/4Ug;

    invoke-direct {v5, v6, v7}, LX/4Ug;-><init>(Ljava/util/Collection;LX/4VM;)V

    move-object v0, p1

    invoke-static/range {v0 .. v6}, LX/4V0;->a(Lcom/facebook/flatbuffers/MutableFlattenable;[[ILX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    .line 310528
    iget-object v0, p0, LX/1kv;->d:LX/1kw;

    invoke-virtual {v0, v7}, LX/1kw;->a(LX/4VM;)LX/4VN;

    move-result-object v0

    .line 310529
    :cond_0
    :goto_1
    return-object v0

    .line 310530
    :cond_1
    iget-object v0, p0, LX/1kv;->c:LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object v0

    .line 310531
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/0jT;

    if-eqz v1, :cond_0

    .line 310532
    check-cast p1, LX/0jT;

    invoke-interface {v0, p1}, LX/2lk;->a(LX/0jT;)Z

    goto :goto_1

    :cond_2
    move-object v1, v6

    goto :goto_0
.end method

.method public final a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/util/Collection;)LX/4VN;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4VN;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 310533
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 310534
    instance-of v0, v0, LX/0gW;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 310535
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v2, v0

    .line 310536
    invoke-virtual {v2}, LX/0gW;->u()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 310537
    invoke-static {p2}, LX/1lN;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/15i;

    move-result-object v0

    .line 310538
    if-eqz v0, :cond_0

    .line 310539
    new-instance v8, LX/4VM;

    invoke-direct {v8}, LX/4VM;-><init>()V

    .line 310540
    invoke-virtual {v2}, LX/0gW;->s()I

    move-result v1

    .line 310541
    iget-boolean v3, v2, LX/0gW;->m:Z

    move v2, v3

    .line 310542
    iget-object v3, p0, LX/1kv;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4Ud;

    iget-object v4, p0, LX/1kv;->e:LX/0t8;

    const/4 v5, 0x1

    new-instance v6, LX/4Ug;

    invoke-direct {v6, v7, v8}, LX/4Ug;-><init>(Ljava/util/Collection;LX/4VM;)V

    move-object v7, p3

    invoke-static/range {v0 .. v7}, LX/4V0;->a(LX/15i;IZLX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    move-object v0, v8

    .line 310543
    :goto_0
    iget-object v1, p0, LX/1kv;->d:LX/1kw;

    invoke-virtual {v1, v0}, LX/1kw;->a(LX/4VM;)LX/4VN;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v7

    goto :goto_0
.end method
