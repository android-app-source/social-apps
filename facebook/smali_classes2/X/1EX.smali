.class public LX/1EX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/1EY;

.field public final d:LX/1El;

.field public final e:LX/0SG;

.field public final f:LX/1Em;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:LX/1Fm;

.field private final i:LX/1EZ;

.field public final j:LX/0gK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220027
    const-class v0, LX/1EX;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1EX;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/1EY;LX/1El;LX/0SG;LX/1Em;Ljava/util/concurrent/Executor;LX/1Fm;LX/1EZ;LX/0gK;)V
    .locals 0
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 220028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220029
    iput-object p1, p0, LX/1EX;->b:Lcom/facebook/content/SecureContextHelper;

    .line 220030
    iput-object p2, p0, LX/1EX;->c:LX/1EY;

    .line 220031
    iput-object p3, p0, LX/1EX;->d:LX/1El;

    .line 220032
    iput-object p4, p0, LX/1EX;->e:LX/0SG;

    .line 220033
    iput-object p5, p0, LX/1EX;->f:LX/1Em;

    .line 220034
    iput-object p6, p0, LX/1EX;->g:Ljava/util/concurrent/Executor;

    .line 220035
    iput-object p7, p0, LX/1EX;->h:LX/1Fm;

    .line 220036
    iput-object p8, p0, LX/1EX;->i:LX/1EZ;

    .line 220037
    iput-object p9, p0, LX/1EX;->j:LX/0gK;

    .line 220038
    return-void
.end method

.method public static b(LX/0QB;)LX/1EX;
    .locals 10

    .prologue
    .line 220039
    new-instance v0, LX/1EX;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1EY;->a(LX/0QB;)LX/1EY;

    move-result-object v2

    check-cast v2, LX/1EY;

    invoke-static {p0}, LX/1El;->a(LX/0QB;)LX/1El;

    move-result-object v3

    check-cast v3, LX/1El;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/1Em;->a(LX/0QB;)LX/1Em;

    move-result-object v5

    check-cast v5, LX/1Em;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1Fm;->b(LX/0QB;)LX/1Fm;

    move-result-object v7

    check-cast v7, LX/1Fm;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v8

    check-cast v8, LX/1EZ;

    invoke-static {p0}, LX/0gK;->b(LX/0QB;)LX/0gK;

    move-result-object v9

    check-cast v9, LX/0gK;

    invoke-direct/range {v0 .. v9}, LX/1EX;-><init>(Lcom/facebook/content/SecureContextHelper;LX/1EY;LX/1El;LX/0SG;LX/1Em;Ljava/util/concurrent/Executor;LX/1Fm;LX/1EZ;LX/0gK;)V

    .line 220040
    return-object v0
.end method

.method public static varargs b(LX/1EX;Landroid/content/Context;[Lcom/facebook/audience/model/UploadShot;)V
    .locals 6

    .prologue
    .line 220041
    iget-object v0, p0, LX/1EX;->j:LX/0gK;

    invoke-virtual {v0}, LX/0gK;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220042
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p2, v0

    .line 220043
    iget-object v3, p0, LX/1EX;->d:LX/1El;

    invoke-virtual {v2}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/1El;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)V

    .line 220044
    iget-object v3, p0, LX/1EX;->h:LX/1Fm;

    invoke-virtual {v2}, Lcom/facebook/audience/model/UploadShot;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v5

    new-instance p1, LX/BaG;

    invoke-direct {p1, p0, v2}, LX/BaG;-><init>(LX/1EX;Lcom/facebook/audience/model/UploadShot;)V

    invoke-virtual {v3, v4, v5, p1}, LX/1Fm;->a(Ljava/lang/String;Ljava/lang/String;LX/AI9;)V

    .line 220045
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220046
    :cond_0
    :goto_1
    return-void

    .line 220047
    :cond_1
    const/4 v0, 0x0

    .line 220048
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/audience/direct/upload/BackstageUploadService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 220049
    const-string v2, "backstage_save_shot"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 220050
    const-string v2, "shot"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 220051
    const-string v2, "confirmation_toast_enabled"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 220052
    iget-object v2, p0, LX/1EX;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 220053
    goto :goto_1
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 220054
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 220055
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 220056
    iget-object v4, p0, LX/1EX;->d:LX/1El;

    invoke-virtual {v4, v3}, LX/1El;->a(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot;

    move-result-object v3

    .line 220057
    if-eqz v3, :cond_0

    .line 220058
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220059
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220060
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 220061
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/audience/model/UploadShot;

    .line 220062
    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/audience/model/UploadShot;

    check-cast v0, [Lcom/facebook/audience/model/UploadShot;

    invoke-static {p0, p1, v0}, LX/1EX;->b(LX/1EX;Landroid/content/Context;[Lcom/facebook/audience/model/UploadShot;)V

    .line 220063
    :cond_2
    return-void
.end method

.method public final varargs a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 220064
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 220065
    iget-object v3, p0, LX/1EX;->i:LX/1EZ;

    invoke-virtual {v3, v2}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v3

    .line 220066
    if-nez v3, :cond_0

    .line 220067
    iget-object v3, p0, LX/1EX;->c:LX/1EY;

    invoke-virtual {v3, v2}, LX/1EY;->b(Ljava/lang/String;)V

    .line 220068
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220069
    :cond_0
    iget-object v2, p0, LX/1EX;->i:LX/1EZ;

    invoke-virtual {v2, v3}, LX/1EZ;->e(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_1

    .line 220070
    :cond_1
    return-void
.end method
