.class public LX/1g1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/1g2;

.field public final b:Ljava/lang/Object;

.field public c:Lcom/facebook/graphql/model/BaseImpression;

.field public d:LX/3EA;

.field public e:LX/162;

.field public f:J

.field public final g:I

.field public h:J

.field public i:I

.field public j:I

.field public k:Ljava/lang/String;

.field public l:I

.field public m:Z

.field public n:I

.field public o:Ljava/lang/String;

.field public p:I

.field public q:I

.field public r:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1g2;Ljava/lang/Object;LX/162;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 292680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292681
    iput v1, p0, LX/1g1;->q:I

    .line 292682
    iput-object p1, p0, LX/1g1;->a:LX/1g2;

    .line 292683
    iput-object p2, p0, LX/1g1;->b:Ljava/lang/Object;

    .line 292684
    iput-object v3, p0, LX/1g1;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 292685
    iput-wide v4, p0, LX/1g1;->f:J

    .line 292686
    iput-object p3, p0, LX/1g1;->e:LX/162;

    .line 292687
    instance-of v0, p2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {p2}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v0

    :goto_0
    iput v0, p0, LX/1g1;->g:I

    .line 292688
    iput-wide v4, p0, LX/1g1;->h:J

    .line 292689
    iput v2, p0, LX/1g1;->i:I

    .line 292690
    iput v2, p0, LX/1g1;->j:I

    .line 292691
    iput v2, p0, LX/1g1;->l:I

    .line 292692
    iput-boolean v1, p0, LX/1g1;->m:Z

    .line 292693
    const-string v0, "unknown"

    iput-object v0, p0, LX/1g1;->k:Ljava/lang/String;

    .line 292694
    iput v2, p0, LX/1g1;->n:I

    .line 292695
    iput-object v3, p0, LX/1g1;->o:Ljava/lang/String;

    .line 292696
    iput v2, p0, LX/1g1;->p:I

    .line 292697
    return-void

    :cond_0
    move v0, v1

    .line 292698
    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 292699
    iget-object v0, p0, LX/1g1;->e:LX/162;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1g1;->e:LX/162;

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->STRING:LX/0nH;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/1g1;->e:LX/162;

    invoke-virtual {v0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/1g1;->e:LX/162;

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    sget-object v1, LX/0nH;->STRING:LX/0nH;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/1g1;->e:LX/162;

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 292700
    iget-object v0, p0, LX/1g1;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 292701
    iget v0, p0, LX/1g1;->g:I

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "(idx="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/1g1;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292702
    :goto_0
    const-string v2, "%s : %s %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    iget-object v4, p0, LX/1g1;->d:LX/3EA;

    aput-object v4, v3, v1

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 292703
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
