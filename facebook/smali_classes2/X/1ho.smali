.class public LX/1ho;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1ho;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1hr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/0ad;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/1hr;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297110
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 297111
    new-instance v1, LX/1hy;

    invoke-direct {v1}, LX/1hy;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 297112
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1ho;->a:LX/0Px;

    .line 297113
    iput-object p2, p0, LX/1ho;->b:LX/0ad;

    .line 297114
    return-void
.end method

.method public static a(LX/0QB;)LX/1ho;
    .locals 6

    .prologue
    .line 297094
    sget-object v0, LX/1ho;->c:LX/1ho;

    if-nez v0, :cond_1

    .line 297095
    const-class v1, LX/1ho;

    monitor-enter v1

    .line 297096
    :try_start_0
    sget-object v0, LX/1ho;->c:LX/1ho;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297097
    if-eqz v2, :cond_0

    .line 297098
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297099
    new-instance v4, LX/1ho;

    .line 297100
    new-instance v3, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/1hp;

    invoke-direct {p0, v0}, LX/1hp;-><init>(LX/0QB;)V

    invoke-direct {v3, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v3

    .line 297101
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v5, v3}, LX/1ho;-><init>(Ljava/util/Set;LX/0ad;)V

    .line 297102
    move-object v0, v4

    .line 297103
    sput-object v0, LX/1ho;->c:LX/1ho;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297104
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297105
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297106
    :cond_1
    sget-object v0, LX/1ho;->c:LX/1ho;

    return-object v0

    .line 297107
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    .locals 3

    .prologue
    .line 297115
    :try_start_0
    instance-of v1, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v1, :cond_1

    .line 297116
    instance-of v1, p0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    if-eqz v1, :cond_0

    .line 297117
    new-instance v2, LX/1iE;

    move-object v0, p0

    check-cast v0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    move-object v1, v0

    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-direct {v2, v1, p0}, LX/1iE;-><init>(Lorg/apache/http/client/methods/AbortableHttpRequest;Lorg/apache/http/HttpEntityEnclosingRequest;)V

    move-object v1, v2

    .line 297118
    :goto_0
    invoke-virtual {v1}, Lorg/apache/http/impl/client/RequestWrapper;->resetHeaders()V

    .line 297119
    return-object v1

    .line 297120
    :cond_0
    new-instance v1, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;

    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-direct {v1, p0}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;-><init>(Lorg/apache/http/HttpEntityEnclosingRequest;)V
    :try_end_0
    .catch Lorg/apache/http/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297121
    :catch_0
    move-exception v1

    .line 297122
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 297123
    :cond_1
    :try_start_1
    instance-of v1, p0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    if-eqz v1, :cond_2

    .line 297124
    new-instance v2, LX/1v0;

    move-object v0, p0

    check-cast v0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    move-object v1, v0

    invoke-direct {v2, v1, p0}, LX/1v0;-><init>(Lorg/apache/http/client/methods/AbortableHttpRequest;Lorg/apache/http/client/methods/HttpUriRequest;)V

    move-object v1, v2

    goto :goto_0

    .line 297125
    :cond_2
    new-instance v1, Lorg/apache/http/impl/client/RequestWrapper;

    invoke-direct {v1, p0}, Lorg/apache/http/impl/client/RequestWrapper;-><init>(Lorg/apache/http/HttpRequest;)V
    :try_end_1
    .catch Lorg/apache/http/ProtocolException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15D;)Lorg/apache/http/impl/client/RequestWrapper;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 297085
    iget-object v0, p1, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v0, v0

    .line 297086
    invoke-static {v0}, LX/1ho;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object v2

    .line 297087
    iget-object v0, p0, LX/1ho;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/1ho;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hr;

    .line 297088
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const v5, -0x7f3f8b67

    invoke-static {v4, v5}, LX/02m;->a(Ljava/lang/String;I)V

    .line 297089
    :try_start_0
    invoke-interface {v0, v2}, LX/1hr;->a(Lorg/apache/http/impl/client/RequestWrapper;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297090
    const v0, -0x4492dd83

    invoke-static {v0}, LX/02m;->a(I)V

    .line 297091
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 297092
    :catchall_0
    move-exception v0

    const v1, -0x42557f62

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 297093
    :cond_0
    return-object v2
.end method
