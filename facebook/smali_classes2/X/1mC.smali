.class public final enum LX/1mC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1mC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1mC;

.field public static final enum DEFAULT:LX/1mC;

.field public static final enum OFF:LX/1mC;

.field public static final enum ON:LX/1mC;

.field public static final enum WIFI_ONLY:LX/1mC;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 313353
    new-instance v0, LX/1mC;

    const-string v1, "ON"

    invoke-direct {v0, v1, v2, v2}, LX/1mC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1mC;->ON:LX/1mC;

    .line 313354
    new-instance v0, LX/1mC;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3, v3}, LX/1mC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1mC;->OFF:LX/1mC;

    .line 313355
    new-instance v0, LX/1mC;

    const-string v1, "WIFI_ONLY"

    invoke-direct {v0, v1, v4, v4}, LX/1mC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1mC;->WIFI_ONLY:LX/1mC;

    .line 313356
    new-instance v0, LX/1mC;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v5, v5}, LX/1mC;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1mC;->DEFAULT:LX/1mC;

    .line 313357
    const/4 v0, 0x4

    new-array v0, v0, [LX/1mC;

    sget-object v1, LX/1mC;->ON:LX/1mC;

    aput-object v1, v0, v2

    sget-object v1, LX/1mC;->OFF:LX/1mC;

    aput-object v1, v0, v3

    sget-object v1, LX/1mC;->WIFI_ONLY:LX/1mC;

    aput-object v1, v0, v4

    sget-object v1, LX/1mC;->DEFAULT:LX/1mC;

    aput-object v1, v0, v5

    sput-object v0, LX/1mC;->$VALUES:[LX/1mC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 313359
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 313360
    iput p3, p0, LX/1mC;->value:I

    .line 313361
    return-void
.end method

.method public static lookupInstanceByValue(Ljava/lang/String;)LX/1mC;
    .locals 1

    .prologue
    .line 313358
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1mC;->lookupInstanceByValue(Ljava/lang/String;LX/1mC;)LX/1mC;

    move-result-object v0

    return-object v0
.end method

.method public static lookupInstanceByValue(Ljava/lang/String;LX/1mC;)LX/1mC;
    .locals 6
    .param p1    # LX/1mC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 313347
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 313348
    invoke-static {}, LX/1mC;->values()[LX/1mC;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 313349
    iget v5, v0, LX/1mC;->value:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v2, :cond_1

    move-object p1, v0

    .line 313350
    :cond_0
    :goto_1
    return-object p1

    .line 313351
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/1mC;
    .locals 1

    .prologue
    .line 313352
    const-class v0, LX/1mC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1mC;

    return-object v0
.end method

.method public static values()[LX/1mC;
    .locals 1

    .prologue
    .line 313346
    sget-object v0, LX/1mC;->$VALUES:[LX/1mC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1mC;

    return-object v0
.end method


# virtual methods
.method public final isAutoPlayOff()Z
    .locals 1

    .prologue
    .line 313345
    sget-object v0, LX/1mC;->OFF:LX/1mC;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAutoPlayOn()Z
    .locals 1

    .prologue
    .line 313344
    sget-object v0, LX/1mC;->ON:LX/1mC;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDefaultAutoPlay()Z
    .locals 1

    .prologue
    .line 313343
    sget-object v0, LX/1mC;->DEFAULT:LX/1mC;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isWifiOnly()Z
    .locals 1

    .prologue
    .line 313342
    sget-object v0, LX/1mC;->WIFI_ONLY:LX/1mC;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
