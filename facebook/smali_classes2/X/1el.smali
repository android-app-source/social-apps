.class public LX/1el;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResult:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1eg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1eg",
            "<TTResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 288930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288931
    new-instance v0, LX/1eg;

    invoke-direct {v0}, LX/1eg;-><init>()V

    iput-object v0, p0, LX/1el;->a:LX/1eg;

    .line 288932
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 288933
    iget-object v0, p0, LX/1el;->a:LX/1eg;

    invoke-virtual {v0, p1}, LX/1eg;->b(Ljava/lang/Exception;)Z

    move-result v0

    move v0, v0

    .line 288934
    if-nez v0, :cond_0

    .line 288935
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set the error on a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288936
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)V"
        }
    .end annotation

    .prologue
    .line 288937
    iget-object v0, p0, LX/1el;->a:LX/1eg;

    invoke-virtual {v0, p1}, LX/1eg;->b(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 288938
    if-nez v0, :cond_0

    .line 288939
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set the result of a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288940
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 288941
    iget-object v0, p0, LX/1el;->a:LX/1eg;

    invoke-virtual {v0}, LX/1eg;->f()Z

    move-result v0

    move v0, v0

    .line 288942
    if-nez v0, :cond_0

    .line 288943
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot cancel a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288944
    :cond_0
    return-void
.end method
