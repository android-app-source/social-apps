.class public LX/0pf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0pf;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1g0;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 145081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145082
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0pf;->a:Ljava/util/Map;

    .line 145083
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0pf;->b:Ljava/util/Map;

    .line 145084
    return-void
.end method

.method public static a(LX/0QB;)LX/0pf;
    .locals 3

    .prologue
    .line 145069
    sget-object v0, LX/0pf;->c:LX/0pf;

    if-nez v0, :cond_1

    .line 145070
    const-class v1, LX/0pf;

    monitor-enter v1

    .line 145071
    :try_start_0
    sget-object v0, LX/0pf;->c:LX/0pf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 145072
    if-eqz v2, :cond_0

    .line 145073
    :try_start_1
    new-instance v0, LX/0pf;

    invoke-direct {v0}, LX/0pf;-><init>()V

    .line 145074
    move-object v0, v0

    .line 145075
    sput-object v0, LX/0pf;->c:LX/0pf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145076
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 145077
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145078
    :cond_1
    sget-object v0, LX/0pf;->c:LX/0pf;

    return-object v0

    .line 145079
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 145080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;)LX/1g0;
    .locals 2

    .prologue
    .line 145064
    iget-object v0, p0, LX/0pf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1g0;

    .line 145065
    if-nez v0, :cond_0

    .line 145066
    new-instance v0, LX/1g0;

    invoke-direct {v0}, LX/1g0;-><init>()V

    .line 145067
    iget-object v1, p0, LX/0pf;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145068
    :cond_0
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 145048
    invoke-interface {p0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    .line 145049
    if-nez v1, :cond_0

    .line 145050
    const/4 v0, 0x0

    .line 145051
    :goto_0
    return-object v0

    .line 145052
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_1

    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-nez v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 145053
    goto :goto_0

    .line 145054
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-interface {p0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;
    .locals 1

    .prologue
    .line 145061
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145062
    invoke-static {p1}, LX/0pf;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0pf;->b(Ljava/lang/String;)LX/1g0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 145063
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/1g0;
    .locals 1

    .prologue
    .line 145058
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145059
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0pf;->b(Ljava/lang/String;)LX/1g0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 145060
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;)LX/1g0;
    .locals 1

    .prologue
    .line 145055
    iget-object v0, p0, LX/0pf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145056
    iget-object v0, p0, LX/0pf;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, LX/0pf;->b(Ljava/lang/String;)LX/1g0;

    move-result-object v0

    .line 145057
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
