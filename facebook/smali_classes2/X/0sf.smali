.class public LX/0sf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0sf;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0sg;

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/0sT;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0sg;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;",
            "Lcom/facebook/graphql/executor/iface/ConsistencyCacheFactory;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0sT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152415
    iput-object p1, p0, LX/0sf;->a:LX/0Ot;

    .line 152416
    iput-object p2, p0, LX/0sf;->b:LX/0sg;

    .line 152417
    iput-object p3, p0, LX/0sf;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 152418
    iput-object p4, p0, LX/0sf;->d:LX/0sT;

    .line 152419
    return-void
.end method

.method public static a(LX/0QB;)LX/0sf;
    .locals 7

    .prologue
    .line 152420
    sget-object v0, LX/0sf;->e:LX/0sf;

    if-nez v0, :cond_1

    .line 152421
    const-class v1, LX/0sf;

    monitor-enter v1

    .line 152422
    :try_start_0
    sget-object v0, LX/0sf;->e:LX/0sf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152423
    if-eqz v2, :cond_0

    .line 152424
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152425
    new-instance v6, LX/0sf;

    const/16 v3, 0xb0f

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0sg;->b(LX/0QB;)LX/0sg;

    move-result-object v3

    check-cast v3, LX/0sg;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v5

    check-cast v5, LX/0sT;

    invoke-direct {v6, p0, v3, v4, v5}, LX/0sf;-><init>(LX/0Ot;LX/0sg;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V

    .line 152426
    move-object v0, v6

    .line 152427
    sput-object v0, LX/0sf;->e:LX/0sf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152428
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152429
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152430
    :cond_1
    sget-object v0, LX/0sf;->e:LX/0sf;

    return-object v0

    .line 152431
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zO;)LX/1ks;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/1ks",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 152433
    iget-object v0, p1, LX/0zO;->t:LX/0sj;

    move-object v2, v0

    .line 152434
    if-nez v2, :cond_0

    .line 152435
    iget-object v0, p0, LX/0sf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sj;

    move-object v2, v0

    .line 152436
    :cond_0
    new-instance v0, LX/1ks;

    iget-object v3, p0, LX/0sf;->b:LX/0sg;

    iget-object v4, p0, LX/0sf;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v5, p0, LX/0sf;->d:LX/0sT;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/1ks;-><init>(LX/0zO;LX/0sj;LX/0sg;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V

    return-object v0
.end method
