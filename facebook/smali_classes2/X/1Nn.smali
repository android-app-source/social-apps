.class public LX/1Nn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0ad;

.field public final c:Landroid/support/v4/app/FragmentActivity;

.field public final d:LX/0wM;

.field public final e:LX/1Np;

.field public final f:LX/0WJ;

.field private final g:LX/1Nq;

.field public final h:LX/1Ns;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;Landroid/support/v4/app/FragmentActivity;LX/0wM;LX/1Np;LX/0WJ;LX/1Nq;LX/1Ns;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237720
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/1Nn;->a:Landroid/content/res/Resources;

    .line 237721
    iput-object p2, p0, LX/1Nn;->b:LX/0ad;

    .line 237722
    iput-object p3, p0, LX/1Nn;->c:Landroid/support/v4/app/FragmentActivity;

    .line 237723
    iput-object p4, p0, LX/1Nn;->d:LX/0wM;

    .line 237724
    iput-object p5, p0, LX/1Nn;->e:LX/1Np;

    .line 237725
    iput-object p6, p0, LX/1Nn;->f:LX/0WJ;

    .line 237726
    iput-object p7, p0, LX/1Nn;->g:LX/1Nq;

    .line 237727
    iput-object p8, p0, LX/1Nn;->h:LX/1Ns;

    .line 237728
    return-void
.end method

.method public static a(LX/0QB;)LX/1Nn;
    .locals 12

    .prologue
    .line 237729
    const-class v1, LX/1Nn;

    monitor-enter v1

    .line 237730
    :try_start_0
    sget-object v0, LX/1Nn;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 237731
    sput-object v2, LX/1Nn;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 237732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 237734
    new-instance v3, LX/1Nn;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, LX/1Np;->a(LX/0QB;)LX/1Np;

    move-result-object v8

    check-cast v8, LX/1Np;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v9

    check-cast v9, LX/0WJ;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    const-class v11, LX/1Ns;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1Ns;

    invoke-direct/range {v3 .. v11}, LX/1Nn;-><init>(Landroid/content/Context;LX/0ad;Landroid/support/v4/app/FragmentActivity;LX/0wM;LX/1Np;LX/0WJ;LX/1Nq;LX/1Ns;)V

    .line 237735
    move-object v0, v3

    .line 237736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 237737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Nn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 237739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d$redex0(LX/1Nn;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 237740
    iget-object v0, p0, LX/1Nn;->b:LX/0ad;

    sget-short v1, LX/1EB;->y:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1Nn;->g:LX/1Nq;

    invoke-static {}, Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;->c()Lcom/facebook/composer/hint/plugin/ComposerHintExperimentPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
