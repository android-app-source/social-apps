.class public LX/19V;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# static fields
.field public static final a:Z

.field public static final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 208023
    const-string v0, "TigonHttpClientAdapter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/19V;->a:Z

    .line 208024
    const-string v0, "TigonHttpClientAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/19V;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 208025
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 208026
    return-void
.end method

.method public static a(LX/0UR;LX/14J;)LX/0TU;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/tigon/httpclientadapter/ResponseHandlerThreadPool;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208027
    const-string v0, "NetDisp"

    .line 208028
    iget-object v1, p1, LX/14J;->a:LX/0ad;

    sget v2, LX/14p;->e:I

    const/16 v3, 0x14

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    move v1, v1

    .line 208029
    const/16 v2, 0xc8

    const/16 v3, 0xa

    invoke-virtual {p0, v0, v1, v2, v3}, LX/0UR;->a(Ljava/lang/String;III)LX/0TU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0UR;)Ljava/util/concurrent/ExecutorService;
    .locals 4
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/tigon/httpclientadapter/CallbacksHandlerExecutor;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 208030
    const-string v0, "TigonCallbacks"

    const/4 v1, 0x1

    const v2, 0x7fffffff

    const/16 v3, 0xa

    invoke-virtual {p0, v0, v1, v2, v3}, LX/0UR;->a(Ljava/lang/String;III)LX/0TU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 208031
    return-void
.end method
