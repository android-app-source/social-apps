.class public LX/1LV;
.super LX/1Cd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1LV;


# instance fields
.field private a:[LX/1B3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1B3;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1B3;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 233884
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 233885
    iput-object p1, p0, LX/1LV;->b:LX/0Ot;

    .line 233886
    return-void
.end method

.method public static a(LX/0QB;)LX/1LV;
    .locals 4

    .prologue
    .line 233871
    sget-object v0, LX/1LV;->c:LX/1LV;

    if-nez v0, :cond_1

    .line 233872
    const-class v1, LX/1LV;

    monitor-enter v1

    .line 233873
    :try_start_0
    sget-object v0, LX/1LV;->c:LX/1LV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 233874
    if-eqz v2, :cond_0

    .line 233875
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 233876
    new-instance v3, LX/1LV;

    invoke-static {v0}, LX/1LW;->a(LX/0QB;)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1LV;-><init>(LX/0Ot;)V

    .line 233877
    move-object v0, v3

    .line 233878
    sput-object v0, LX/1LV;->c:LX/1LV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233879
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 233880
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233881
    :cond_1
    sget-object v0, LX/1LV;->c:LX/1LV;

    return-object v0

    .line 233882
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 233883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1LV;)[LX/1B3;
    .locals 2

    .prologue
    .line 233863
    iget-object v0, p0, LX/1LV;->a:[LX/1B3;

    if-nez v0, :cond_0

    .line 233864
    const-string v0, "FeedLoggingViewportEventListener#getLoggingHandlers"

    const v1, 0x637ca868

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 233865
    :try_start_0
    iget-object v0, p0, LX/1LV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 233866
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [LX/1B3;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1B3;

    iput-object v0, p0, LX/1LV;->a:[LX/1B3;

    .line 233867
    const/4 v0, 0x0

    iput-object v0, p0, LX/1LV;->b:LX/0Ot;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233868
    const v0, -0x24608352

    invoke-static {v0}, LX/02m;->a(I)V

    .line 233869
    :cond_0
    iget-object v0, p0, LX/1LV;->a:[LX/1B3;

    return-object v0

    .line 233870
    :catchall_0
    move-exception v0

    const v1, -0x5e0f8f69

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(LX/01J;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233820
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233821
    invoke-interface {v3, p1}, LX/1B3;->a(LX/01J;)V

    .line 233822
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233823
    :cond_0
    return-void
.end method

.method public final a(LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233859
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233860
    invoke-interface {v3, p1}, LX/1B3;->a(LX/0P1;)V

    .line 233861
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233862
    :cond_0
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 5

    .prologue
    .line 233848
    invoke-static {p2}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 233849
    if-nez v0, :cond_1

    .line 233850
    :cond_0
    return-void

    .line 233851
    :cond_1
    invoke-interface {p1}, LX/0g8;->d()I

    move-result v0

    .line 233852
    if-lez v0, :cond_0

    .line 233853
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    .line 233854
    if-eqz v0, :cond_0

    .line 233855
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233856
    invoke-interface {v3, p2}, LX/1B3;->d(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 233857
    invoke-interface {v3, p1, p2, p3}, LX/1B3;->a(LX/0g8;Ljava/lang/Object;I)V

    .line 233858
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 233844
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233845
    invoke-interface {v3, p1, p3, p4}, LX/1B3;->a(LX/0g8;II)V

    .line 233846
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233847
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 233836
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 233837
    invoke-interface {v4, p1, p2}, LX/1B3;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 233838
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 233839
    :cond_0
    invoke-static {p1, p2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 233840
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 233841
    invoke-interface {v3, p1, p2}, LX/1B3;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 233842
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 233843
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 233832
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233833
    invoke-interface {v3, p1}, LX/1B3;->b(Ljava/lang/Object;)V

    .line 233834
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233835
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 233828
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233829
    invoke-interface {v3, p1}, LX/1B3;->a(Ljava/lang/String;)V

    .line 233830
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233831
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 233824
    invoke-static {p0}, LX/1LV;->a(LX/1LV;)[LX/1B3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 233825
    invoke-interface {v3, p1}, LX/1B3;->c(Ljava/lang/Object;)V

    .line 233826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233827
    :cond_0
    return-void
.end method
