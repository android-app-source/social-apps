.class public LX/1AV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1AW;LX/1AZ;LX/0gp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 210507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210508
    const/4 v0, 0x1

    invoke-virtual {p3}, LX/0gp;->b()Z

    move-result v1

    invoke-virtual {p2, p1, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, LX/1AV;->a:LX/1Aa;

    .line 210509
    return-void
.end method

.method public static a(LX/0QB;)LX/1AV;
    .locals 6

    .prologue
    .line 210510
    const-class v1, LX/1AV;

    monitor-enter v1

    .line 210511
    :try_start_0
    sget-object v0, LX/1AV;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 210512
    sput-object v2, LX/1AV;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210513
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210514
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 210515
    new-instance p0, LX/1AV;

    .line 210516
    new-instance v4, LX/1AW;

    invoke-static {v0}, LX/1AY;->b(LX/0QB;)LX/1AY;

    move-result-object v3

    check-cast v3, LX/1AY;

    invoke-direct {v4, v3}, LX/1AW;-><init>(LX/1AY;)V

    .line 210517
    move-object v3, v4

    .line 210518
    check-cast v3, LX/1AW;

    const-class v4, LX/1AZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1AZ;

    invoke-static {v0}, LX/0gp;->a(LX/0QB;)LX/0gp;

    move-result-object v5

    check-cast v5, LX/0gp;

    invoke-direct {p0, v3, v4, v5}, LX/1AV;-><init>(LX/1AW;LX/1AZ;LX/0gp;)V

    .line 210519
    move-object v0, p0

    .line 210520
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 210521
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1AV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210522
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 210523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 210524
    iget-object v0, p0, LX/1AV;->a:LX/1Aa;

    invoke-virtual {v0, p1}, LX/1Aa;->a(Landroid/view/View;)V

    .line 210525
    return-void
.end method

.method public final a(Landroid/view/View;LX/2oV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/2oV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 210526
    iget-object v0, p0, LX/1AV;->a:LX/1Aa;

    invoke-virtual {v0, p1, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 210527
    return-void
.end method
