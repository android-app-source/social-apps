.class public final LX/1WK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1WL;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0QA;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 267947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267948
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    iput-object v0, p0, LX/1WK;->a:LX/0QA;

    .line 267949
    return-void
.end method

.method public static a(LX/0QB;)LX/1WK;
    .locals 4

    .prologue
    .line 267950
    const-class v1, LX/1WK;

    monitor-enter v1

    .line 267951
    :try_start_0
    sget-object v0, LX/1WK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 267952
    sput-object v2, LX/1WK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 267953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 267955
    new-instance p0, LX/1WK;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/1WK;-><init>(Landroid/content/Context;)V

    .line 267956
    move-object v0, p0

    .line 267957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 267958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1WK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 267960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)LX/1RB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ")",
            "LX/1RB",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 267961
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 267962
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 267963
    :sswitch_0
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267964
    :sswitch_1
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267965
    :sswitch_2
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267966
    :sswitch_3
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267967
    :sswitch_4
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267968
    :sswitch_5
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267969
    :sswitch_6
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267970
    :sswitch_7
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267971
    :sswitch_8
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267972
    :sswitch_9
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267973
    :sswitch_a
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267974
    :sswitch_b
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267975
    :sswitch_c
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267976
    :sswitch_d
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto :goto_0

    .line 267977
    :sswitch_e
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/CenteredTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/CenteredTextComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267978
    :sswitch_f
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267979
    :sswitch_10
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267980
    :sswitch_11
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267981
    :sswitch_12
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267982
    :sswitch_13
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267983
    :sswitch_14
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267984
    :sswitch_15
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/list/ListAttachmentRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/list/ListAttachmentRootPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267985
    :sswitch_16
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267986
    :sswitch_17
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267987
    :sswitch_18
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267988
    :sswitch_19
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267989
    :sswitch_1a
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267990
    :sswitch_1b
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267991
    :sswitch_1c
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267992
    :sswitch_1d
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentVideoSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentVideoSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267993
    :sswitch_1e
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267994
    :sswitch_1f
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267995
    :sswitch_20
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267996
    :sswitch_21
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267997
    :sswitch_22
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267998
    :sswitch_23
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 267999
    :sswitch_24
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268000
    :sswitch_25
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268001
    :sswitch_26
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268002
    :sswitch_27
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268003
    :sswitch_28
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268004
    :sswitch_29
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCardAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCardAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268005
    :sswitch_2a
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268006
    :sswitch_2b
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268007
    :sswitch_2c
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268008
    :sswitch_2d
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268009
    :sswitch_2e
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareAttachmentSelectorPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268010
    :sswitch_2f
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268011
    :sswitch_30
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268012
    :sswitch_31
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268013
    :sswitch_32
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268014
    :sswitch_33
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268015
    :sswitch_34
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268016
    :sswitch_35
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSelectorPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268017
    :sswitch_36
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/rooms/RoomLinkAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/rooms/RoomLinkAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268018
    :sswitch_37
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268019
    :sswitch_38
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268020
    :sswitch_39
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentGroupPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268021
    :sswitch_3a
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268022
    :sswitch_3b
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268023
    :sswitch_3c
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    .line 268024
    :sswitch_3d
    iget-object v0, p0, LX/1WK;->a:LX/0QA;

    invoke-static {v0}, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;

    move-result-object v0

    check-cast v0, LX/1RB;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_31
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_19
        0x4 -> :sswitch_20
        0x6 -> :sswitch_7
        0x8 -> :sswitch_f
        0xb -> :sswitch_13
        0xe -> :sswitch_1c
        0x12 -> :sswitch_2f
        0x13 -> :sswitch_15
        0x19 -> :sswitch_c
        0x1a -> :sswitch_b
        0x1c -> :sswitch_3b
        0x1d -> :sswitch_10
        0x1f -> :sswitch_10
        0x22 -> :sswitch_16
        0x24 -> :sswitch_1c
        0x25 -> :sswitch_8
        0x26 -> :sswitch_8
        0x27 -> :sswitch_12
        0x29 -> :sswitch_4
        0x2b -> :sswitch_5
        0x2f -> :sswitch_3c
        0x31 -> :sswitch_3c
        0x32 -> :sswitch_1d
        0x33 -> :sswitch_1e
        0x35 -> :sswitch_3c
        0x36 -> :sswitch_3c
        0x38 -> :sswitch_6
        0x41 -> :sswitch_2e
        0x42 -> :sswitch_2e
        0x43 -> :sswitch_2e
        0x45 -> :sswitch_2e
        0x49 -> :sswitch_20
        0x4b -> :sswitch_d
        0x4e -> :sswitch_2b
        0x4f -> :sswitch_2c
        0x50 -> :sswitch_14
        0x51 -> :sswitch_3d
        0x55 -> :sswitch_2a
        0x58 -> :sswitch_a
        0x6c -> :sswitch_1c
        0x79 -> :sswitch_3c
        0x7d -> :sswitch_3
        0x7e -> :sswitch_34
        0x7f -> :sswitch_18
        0x90 -> :sswitch_11
        0x93 -> :sswitch_9
        0x94 -> :sswitch_9
        0x95 -> :sswitch_38
        0x96 -> :sswitch_39
        0x97 -> :sswitch_25
        0x98 -> :sswitch_25
        0x99 -> :sswitch_26
        0x9a -> :sswitch_26
        0x9b -> :sswitch_27
        0x9c -> :sswitch_29
        0x9d -> :sswitch_28
        0xa2 -> :sswitch_23
        0xaf -> :sswitch_1a
        0xb1 -> :sswitch_e
        0xb5 -> :sswitch_1b
        0xb9 -> :sswitch_0
        0xbc -> :sswitch_2d
        0xbd -> :sswitch_2d
        0xc6 -> :sswitch_22
        0xc8 -> :sswitch_30
        0xca -> :sswitch_33
        0xcb -> :sswitch_2
        0xcd -> :sswitch_21
        0xcf -> :sswitch_32
        0xd3 -> :sswitch_36
        0xd8 -> :sswitch_35
        0xde -> :sswitch_1
        0xe3 -> :sswitch_37
        0xee -> :sswitch_17
        0xef -> :sswitch_1f
        0xf6 -> :sswitch_39
        0xf8 -> :sswitch_3a
        0xfa -> :sswitch_24
    .end sparse-switch
.end method
