.class public LX/0dp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90932
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "composer/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 90933
    sput-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "photo_review_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->b:LX/0Tn;

    .line 90934
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "breakfast_club_share_composer_nux_v2"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->c:LX/0Tn;

    .line 90935
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "breakfast_club_tag_expansion_tip"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->d:LX/0Tn;

    .line 90936
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "draft_exists"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->e:LX/0Tn;

    .line 90937
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "has_pending_stories"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->f:LX/0Tn;

    .line 90938
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "publish_mode_selector_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->g:LX/0Tn;

    .line 90939
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "linear_composer_next_button_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->h:LX/0Tn;

    .line 90940
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "auto_enhance_photos"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->i:LX/0Tn;

    .line 90941
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "check_in_nux_for_photo_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->j:LX/0Tn;

    .line 90942
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "creative_editing_stickers_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->k:LX/0Tn;

    .line 90943
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "creative_editing_frames_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->l:LX/0Tn;

    .line 90944
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "creative_editing_filter_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->m:LX/0Tn;

    .line 90945
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "sticker_post_sticker_icon_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->n:LX/0Tn;

    .line 90946
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "last_published_post_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->o:LX/0Tn;

    .line 90947
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "sprouts_music_nux_tapped"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->p:LX/0Tn;

    .line 90948
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "facecast_icon_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->q:LX/0Tn;

    .line 90949
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "facecast_sprouts_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->r:LX/0Tn;

    .line 90950
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "last_published_mmp_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->s:LX/0Tn;

    .line 90951
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "composer_feed_only_post_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->t:LX/0Tn;

    .line 90952
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "branded_content_sprouts_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->u:LX/0Tn;

    .line 90953
    sget-object v0, LX/0dp;->a:LX/0Tn;

    const-string v1, "composer_tag_expansion_pill_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/0dp;->v:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 90929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90930
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90931
    sget-object v0, LX/0dp;->b:LX/0Tn;

    sget-object v1, LX/0dp;->c:LX/0Tn;

    sget-object v2, LX/0dp;->d:LX/0Tn;

    sget-object v3, LX/0dp;->o:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
