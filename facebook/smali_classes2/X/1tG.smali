.class public abstract LX/1tG;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/1tH;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 336352
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 336353
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p0, p0, v0}, LX/1tG;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 336354
    return-void
.end method

.method public static a(Landroid/os/IBinder;)LX/1tH;
    .locals 2

    .prologue
    .line 336240
    if-nez p0, :cond_0

    .line 336241
    const/4 v0, 0x0

    .line 336242
    :goto_0
    return-object v0

    .line 336243
    :cond_0
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 336244
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/1tH;

    if-eqz v1, :cond_1

    .line 336245
    check-cast v0, LX/1tH;

    goto :goto_0

    .line 336246
    :cond_1
    new-instance v0, LX/766;

    invoke-direct {v0, p0}, LX/766;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 336351
    return-object p0
.end method

.method public final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 336247
    sparse-switch p1, :sswitch_data_0

    .line 336248
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 336249
    :sswitch_0
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 336250
    :sswitch_1
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336251
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/1Md;->a(Landroid/os/IBinder;)LX/1Me;

    move-result-object v0

    .line 336252
    invoke-virtual {p0, v0}, LX/1tG;->a(LX/1Me;)V

    .line 336253
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 336254
    :sswitch_2
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336255
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/1Md;->a(Landroid/os/IBinder;)LX/1Me;

    move-result-object v0

    .line 336256
    invoke-virtual {p0, v0}, LX/1tG;->b(LX/1Me;)V

    .line 336257
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 336258
    :sswitch_3
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336259
    invoke-virtual {p0}, LX/1tG;->a()Z

    move-result v0

    .line 336260
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336261
    if-eqz v0, :cond_0

    move v0, v9

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    move v0, v10

    goto :goto_1

    .line 336262
    :sswitch_4
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336263
    invoke-virtual {p0}, LX/1tG;->b()Z

    move-result v0

    .line 336264
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336265
    if-eqz v0, :cond_1

    move v10, v9

    :cond_1
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 336266
    :sswitch_5
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336267
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 336268
    invoke-virtual {p0, v0, v1}, LX/1tG;->a(J)Z

    move-result v0

    .line 336269
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336270
    if-eqz v0, :cond_2

    move v10, v9

    :cond_2
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 336271
    :sswitch_6
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336272
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 336273
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 336274
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 336275
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, LX/76D;->a(Landroid/os/IBinder;)LX/76B;

    move-result-object v3

    .line 336276
    invoke-virtual {p0, v0, v1, v2, v3}, LX/1tG;->a(Ljava/lang/String;[BILX/76B;)I

    move-result v0

    .line 336277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336278
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 336279
    :sswitch_7
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336280
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 336281
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 336282
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 336283
    invoke-virtual {p0, v0, v1, v2}, LX/1tG;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 336284
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336285
    :sswitch_8
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336286
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 336287
    invoke-virtual {p0, v0}, LX/1tG;->a(Ljava/lang/String;)V

    .line 336288
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336289
    :sswitch_9
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336290
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 336291
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 336292
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    .line 336293
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/76D;->a(Landroid/os/IBinder;)LX/76B;

    move-result-object v5

    .line 336294
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    move-object v0, p0

    .line 336295
    invoke-virtual/range {v0 .. v7}, LX/1tG;->a(Ljava/lang/String;[BJLX/76B;J)Z

    move-result v0

    .line 336296
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336297
    if-eqz v0, :cond_3

    move v10, v9

    :cond_3
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 336298
    :sswitch_a
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336299
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 336300
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 336301
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    .line 336302
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LX/76D;->a(Landroid/os/IBinder;)LX/76B;

    move-result-object v5

    .line 336303
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 336304
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    .line 336305
    invoke-virtual/range {v0 .. v8}, LX/1tG;->a(Ljava/lang/String;[BJLX/76B;JLjava/lang/String;)Z

    move-result v0

    .line 336306
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336307
    if-eqz v0, :cond_4

    move v10, v9

    :cond_4
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 336308
    :sswitch_b
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336309
    sget-object v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 336310
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, LX/76A;->a(Landroid/os/IBinder;)LX/768;

    move-result-object v1

    .line 336311
    invoke-virtual {p0, v0, v1}, LX/1tG;->a(Ljava/util/List;LX/768;)V

    .line 336312
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336313
    :sswitch_c
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336314
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 336315
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, LX/76A;->a(Landroid/os/IBinder;)LX/768;

    move-result-object v1

    .line 336316
    invoke-virtual {p0, v0, v1}, LX/1tG;->b(Ljava/util/List;LX/768;)V

    .line 336317
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336318
    :sswitch_d
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336319
    sget-object v0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 336320
    invoke-virtual {p0, v0}, LX/1tG;->a(Ljava/util/List;)V

    .line 336321
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336322
    :sswitch_e
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336323
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v10, v9

    .line 336324
    :cond_5
    invoke-virtual {p0, v10}, LX/1tG;->a(Z)V

    .line 336325
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336326
    :sswitch_f
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336327
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v10, v9

    .line 336328
    :cond_6
    sget-object v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 336329
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 336330
    invoke-virtual {p0, v10, v0, v1}, LX/1tG;->a(ZLjava/util/List;Ljava/util/List;)V

    .line 336331
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 336332
    :sswitch_10
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336333
    invoke-virtual {p0}, LX/1tG;->c()Ljava/lang/String;

    move-result-object v0

    .line 336334
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336335
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 336336
    :sswitch_11
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336337
    invoke-virtual {p0}, LX/1tG;->d()Ljava/lang/String;

    move-result-object v0

    .line 336338
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336339
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 336340
    :sswitch_12
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336341
    invoke-virtual {p0}, LX/1tG;->e()Ljava/lang/String;

    move-result-object v0

    .line 336342
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336343
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 336344
    :sswitch_13
    const-string v0, "com.facebook.push.mqtt.ipc.IMqttPushService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336345
    invoke-virtual {p0}, LX/1tG;->f()Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v0

    .line 336346
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336347
    if-eqz v0, :cond_7

    .line 336348
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 336349
    invoke-virtual {v0, p3, v9}, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 336350
    :cond_7
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
