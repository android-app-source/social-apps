.class public LX/1Sn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/14w;

.field private b:LX/0qn;

.field private c:LX/0ad;

.field public d:LX/1Kf;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/1Pf;


# direct methods
.method public constructor <init>(LX/14w;LX/0qn;LX/0ad;LX/1Kf;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/1Pf;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedPrivacyEditingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14w;",
            "LX/0qn;",
            "LX/0ad;",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/1Pf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249238
    iput-object p1, p0, LX/1Sn;->a:LX/14w;

    .line 249239
    iput-object p2, p0, LX/1Sn;->b:LX/0qn;

    .line 249240
    iput-object p3, p0, LX/1Sn;->c:LX/0ad;

    .line 249241
    iput-object p4, p0, LX/1Sn;->d:LX/1Kf;

    .line 249242
    iput-object p5, p0, LX/1Sn;->e:LX/0Or;

    .line 249243
    iput-object p6, p0, LX/1Sn;->f:LX/0Or;

    .line 249244
    iput-object p7, p0, LX/1Sn;->g:LX/0Or;

    .line 249245
    iput-object p8, p0, LX/1Sn;->h:LX/0Or;

    .line 249246
    iput-object p9, p0, LX/1Sn;->i:LX/1Pf;

    .line 249247
    return-void
.end method

.method public static b(LX/1Sn;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 249248
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1Sn;->b:LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 249249
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    .line 249250
    :cond_0
    :goto_0
    return v0

    .line 249251
    :cond_1
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 249252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/16z;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->bj()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3

    .prologue
    .line 249253
    iget-object v0, p0, LX/1Sn;->i:LX/1Pf;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/9Ir;->b(LX/1PT;)LX/21D;

    move-result-object v1

    .line 249254
    iget-object v0, p0, LX/1Sn;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-string v2, "baseFeedStoryMenuHelper"

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method
