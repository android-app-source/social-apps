.class public final enum LX/1fq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1fq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1fq;

.field public static final enum OFFSCREEN:LX/1fq;

.field public static final enum ONSCREEN:LX/1fq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 292382
    new-instance v0, LX/1fq;

    const-string v1, "ONSCREEN"

    invoke-direct {v0, v1, v2}, LX/1fq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1fq;->ONSCREEN:LX/1fq;

    .line 292383
    new-instance v0, LX/1fq;

    const-string v1, "OFFSCREEN"

    invoke-direct {v0, v1, v3}, LX/1fq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1fq;->OFFSCREEN:LX/1fq;

    .line 292384
    const/4 v0, 0x2

    new-array v0, v0, [LX/1fq;

    sget-object v1, LX/1fq;->ONSCREEN:LX/1fq;

    aput-object v1, v0, v2

    sget-object v1, LX/1fq;->OFFSCREEN:LX/1fq;

    aput-object v1, v0, v3

    sput-object v0, LX/1fq;->$VALUES:[LX/1fq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 292379
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1fq;
    .locals 1

    .prologue
    .line 292381
    const-class v0, LX/1fq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1fq;

    return-object v0
.end method

.method public static values()[LX/1fq;
    .locals 1

    .prologue
    .line 292380
    sget-object v0, LX/1fq;->$VALUES:[LX/1fq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1fq;

    return-object v0
.end method
