.class public abstract LX/0te;
.super Landroid/app/Service;
.source ""

# interfaces
.implements LX/0Xn;


# static fields
.field private static final ERROR_TAG:Ljava/lang/String;


# instance fields
.field private mCalledOnCreate:Z

.field private mCalledOnDestroy:Z

.field private mCalledOnStartCommand:Z

.field public mFbErrorReporter:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public mMultiplexBackgroundWorkObserver:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Sk;",
            ">;"
        }
    .end annotation
.end field

.field private final mPropertyBagHelper:LX/0jb;

.field public mViewerContextManager:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155277
    const-class v0, LX/0te;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0te;->ERROR_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 155264
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 155265
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 155266
    iput-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    .line 155267
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 155268
    iput-object v0, p0, LX/0te;->mViewerContextManager:LX/0Ot;

    .line 155269
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 155270
    iput-object v0, p0, LX/0te;->mFbErrorReporter:LX/0Ot;

    .line 155271
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, LX/0te;->mPropertyBagHelper:LX/0jb;

    .line 155272
    iput-boolean v1, p0, LX/0te;->mCalledOnCreate:Z

    .line 155273
    iput-boolean v1, p0, LX/0te;->mCalledOnStartCommand:Z

    .line 155274
    iput-boolean v1, p0, LX/0te;->mCalledOnDestroy:Z

    return-void
.end method

.method public static STATICDI_COMPONENT$injectImpl(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/0te;

    const/16 v1, 0x1bf

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1a1

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p1, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    iput-object v2, p1, LX/0te;->mViewerContextManager:LX/0Ot;

    iput-object v0, p1, LX/0te;->mFbErrorReporter:LX/0Ot;

    return-void
.end method

.method private innerFbStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 155263
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method private varargs onFail(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 155261
    iget-object v0, p0, LX/0te;->mFbErrorReporter:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/0te;->ERROR_TAG:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155262
    return-void
.end method


# virtual methods
.method public getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155260
    iget-object v0, p0, LX/0te;->mPropertyBagHelper:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 155259
    invoke-virtual {p0}, LX/0te;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public injectMe()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 155275
    const-class v0, LX/0te;

    invoke-static {v0, p0, p0}, LX/0te;->STATICDI_COMPONENT$injectImpl(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 155276
    return-void
.end method

.method public final onCreate()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x24

    const v1, -0x48da910e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 155239
    iget-boolean v0, p0, LX/0te;->mCalledOnCreate:Z

    if-eqz v0, :cond_0

    .line 155240
    const-string v0, "Class %s called onCreate twice. This may be due to calling super.onCreate instead of super.onFbCreate"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {p0, v0, v1}, LX/0te;->onFail(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155241
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 155242
    const/16 v0, 0x25

    const v1, 0x748207f8

    invoke-static {v3, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 155243
    :goto_0
    return-void

    .line 155244
    :cond_0
    invoke-virtual {p0}, LX/0te;->injectMe()V

    .line 155245
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 155246
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    .line 155247
    iget-object v6, v0, LX/0Sk;->a:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    move-wide v0, v6

    .line 155248
    move-wide v2, v0

    .line 155249
    :goto_1
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0te;->mCalledOnCreate:Z

    .line 155250
    invoke-virtual {p0}, LX/0te;->onFbCreate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155251
    iput-boolean v5, p0, LX/0te;->mCalledOnCreate:Z

    .line 155252
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 155253
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/0Sk;->a(JLjava/lang/Class;)V

    .line 155254
    :cond_1
    const v0, 0x542c2b0

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto :goto_0

    .line 155255
    :cond_2
    const-wide/16 v0, -0x1

    move-wide v2, v0

    goto :goto_1

    .line 155256
    :catchall_0
    move-exception v0

    move-object v1, v0

    iput-boolean v5, p0, LX/0te;->mCalledOnCreate:Z

    .line 155257
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 155258
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v5}, LX/0Sk;->a(JLjava/lang/Class;)V

    :cond_3
    const v0, 0x5de17779

    invoke-static {v0, v4}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onDestroy()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x24

    const v1, -0x10049225

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 155187
    iget-boolean v0, p0, LX/0te;->mCalledOnDestroy:Z

    if-eqz v0, :cond_0

    .line 155188
    const-string v0, "Class %s called onDestroy twice. This may be due to calling super.onDestroy instead of super.onFbDestroy"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {p0, v0, v1}, LX/0te;->onFail(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155189
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 155190
    const/16 v0, 0x25

    const v1, 0x3dc48c9e

    invoke-static {v3, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 155191
    :goto_0
    return-void

    .line 155192
    :cond_0
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 155193
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    .line 155194
    iget-object v6, v0, LX/0Sk;->a:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    move-wide v0, v6

    .line 155195
    move-wide v2, v0

    .line 155196
    :goto_1
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0te;->mCalledOnDestroy:Z

    .line 155197
    invoke-virtual {p0}, LX/0te;->onFbDestroy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155198
    iput-boolean v5, p0, LX/0te;->mCalledOnDestroy:Z

    .line 155199
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 155200
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/0Sk;->b(JLjava/lang/Class;)V

    .line 155201
    :cond_1
    const v0, 0x25e9d1cd

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto :goto_0

    .line 155202
    :cond_2
    const-wide/16 v0, -0x1

    move-wide v2, v0

    goto :goto_1

    .line 155203
    :catchall_0
    move-exception v0

    move-object v1, v0

    iput-boolean v5, p0, LX/0te;->mCalledOnDestroy:Z

    .line 155204
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 155205
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v5}, LX/0Sk;->b(JLjava/lang/Class;)V

    :cond_3
    const v0, -0x555db54c

    invoke-static {v0, v4}, LX/02F;->d(II)V

    throw v1
.end method

.method public onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x4685763a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 155237
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 155238
    const/16 v1, 0x25

    const v2, -0x50e3dac9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0xe2d3e8e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 155235
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 155236
    const/16 v1, 0x25

    const v2, 0x46a22196

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFbStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0xb2c9a65

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 155234
    invoke-direct {p0, p1, p2, p3}, LX/0te;->innerFbStartCommand(Landroid/content/Intent;II)I

    move-result v1

    const/16 v2, 0x25

    const v3, 0x67711873

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x24

    const v1, -0x75d5549c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 155213
    iget-boolean v0, p0, LX/0te;->mCalledOnStartCommand:Z

    if-eqz v0, :cond_0

    .line 155214
    const-string v0, "Class %s called onStartCommand twice. This may be due to calling super.onStartCommand instead of super.onFbStartCommand"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-direct {p0, v0, v1}, LX/0te;->onFail(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155215
    invoke-direct {p0, p1, p2, p3}, LX/0te;->innerFbStartCommand(Landroid/content/Intent;II)I

    move-result v0

    const/16 v1, 0x25

    const v2, -0x310ee419

    invoke-static {v5, v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 155216
    :goto_0
    return v0

    .line 155217
    :cond_0
    invoke-virtual {p0, p1}, LX/0te;->pushViewerContext(Landroid/content/Intent;)LX/1mW;

    move-result-object v6

    const/4 v2, 0x0

    .line 155218
    :try_start_0
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 155219
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    .line 155220
    iget-object v9, v0, LX/0Sk;->a:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v9

    move-wide v0, v9
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 155221
    move-wide v4, v0

    .line 155222
    :goto_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/0te;->mCalledOnStartCommand:Z

    .line 155223
    invoke-virtual {p0, p1, p2, p3}, LX/0te;->onFbStartCommand(Landroid/content/Intent;II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 155224
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, LX/0te;->mCalledOnStartCommand:Z

    .line 155225
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 155226
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v0, v4, v5, v7, p1}, LX/0Sk;->a(JLjava/lang/Class;Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 155227
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, LX/1mW;->close()V

    :cond_2
    const v0, 0xaa187f9

    invoke-static {v0, v3}, LX/02F;->d(II)V

    move v0, v1

    goto :goto_0

    .line 155228
    :cond_3
    const-wide/16 v0, -0x1

    move-wide v4, v0

    goto :goto_1

    .line 155229
    :catchall_0
    move-exception v0

    move-object v1, v0

    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, LX/0te;->mCalledOnStartCommand:Z

    .line 155230
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 155231
    iget-object v0, p0, LX/0te;->mMultiplexBackgroundWorkObserver:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sk;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v0, v4, v5, v7, p1}, LX/0Sk;->a(JLjava/lang/Class;Landroid/content/Intent;)V

    :cond_4
    const v0, -0x36e374a5

    invoke-static {v0, v3}, LX/02F;->d(II)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 155232
    :catch_0
    move-exception v0

    const v1, -0x58ed5209

    :try_start_4
    invoke-static {v1, v3}, LX/02F;->d(II)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 155233
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v6, :cond_5

    if-eqz v1, :cond_6

    :try_start_5
    invoke-interface {v6}, LX/1mW;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    :goto_3
    const v1, 0x390dafb

    invoke-static {v1, v3}, LX/02F;->d(II)V

    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-interface {v6}, LX/1mW;->close()V

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public pushViewerContext(Landroid/content/Intent;)LX/1mW;
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 155208
    if-eqz p1, :cond_0

    const-string v0, "overridden_viewer_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155209
    :cond_0
    sget-object v0, LX/1mW;->a:LX/1mW;

    .line 155210
    :goto_0
    return-object v0

    .line 155211
    :cond_1
    const-string v0, "overridden_viewer_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 155212
    iget-object v1, p0, LX/0te;->mViewerContextManager:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-interface {v1, v0}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    move-result-object v0

    goto :goto_0
.end method

.method public setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 155206
    iget-object v0, p0, LX/0te;->mPropertyBagHelper:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155207
    return-void
.end method
