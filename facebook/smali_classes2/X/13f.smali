.class public LX/13f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/13f;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/13g;

.field public final c:LX/13h;

.field public final d:LX/0tK;

.field public final e:LX/0Sh;

.field public f:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/13g;LX/13h;LX/0tK;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177060
    iput-object p1, p0, LX/13f;->a:LX/0ad;

    .line 177061
    iput-object p2, p0, LX/13f;->b:LX/13g;

    .line 177062
    iput-object p3, p0, LX/13f;->c:LX/13h;

    .line 177063
    iput-object p4, p0, LX/13f;->d:LX/0tK;

    .line 177064
    iput-object p5, p0, LX/13f;->e:LX/0Sh;

    .line 177065
    return-void
.end method

.method public static a(LX/0QB;)LX/13f;
    .locals 9

    .prologue
    .line 177066
    sget-object v0, LX/13f;->g:LX/13f;

    if-nez v0, :cond_1

    .line 177067
    const-class v1, LX/13f;

    monitor-enter v1

    .line 177068
    :try_start_0
    sget-object v0, LX/13f;->g:LX/13f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177069
    if-eqz v2, :cond_0

    .line 177070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177071
    new-instance v3, LX/13f;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/13g;->a(LX/0QB;)LX/13g;

    move-result-object v5

    check-cast v5, LX/13g;

    invoke-static {v0}, LX/13h;->a(LX/0QB;)LX/13h;

    move-result-object v6

    check-cast v6, LX/13h;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v7

    check-cast v7, LX/0tK;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-direct/range {v3 .. v8}, LX/13f;-><init>(LX/0ad;LX/13g;LX/13h;LX/0tK;LX/0Sh;)V

    .line 177072
    move-object v0, v3

    .line 177073
    sput-object v0, LX/13f;->g:LX/13f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177076
    :cond_1
    sget-object v0, LX/13f;->g:LX/13f;

    return-object v0

    .line 177077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
