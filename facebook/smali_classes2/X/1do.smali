.class public LX/1do;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1dp;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1X9;",
            "LX/1dq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0fO;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 286564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286565
    invoke-virtual {p2}, LX/0fO;->b()Z

    move-result v0

    .line 286566
    new-instance v2, Ljava/util/EnumMap;

    const-class v1, LX/1X9;

    invoke-direct {v2, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 286567
    sget-object v3, LX/1X9;->TOP:LX/1X9;

    new-instance v4, LX/1dq;

    const v5, 0x7f0102e1

    if-eqz v0, :cond_0

    const v1, 0x7f020a74

    :goto_0
    invoke-static {p1, v5, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    const v5, 0x7f0102e2

    const v6, 0x7f020a6e

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e6

    const p2, 0x7f0219e7

    invoke-direct {v4, v1, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286568
    sget-object v1, LX/1X9;->DIVIDER_TOP:LX/1X9;

    new-instance v3, LX/1dq;

    const v4, 0x7f0102e3

    const v5, 0x7f020a72

    invoke-static {p1, v4, v5}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    const v5, 0x7f0102e3

    const v6, 0x7f020a72

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v3, v4, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v1, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286569
    sget-object v3, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    new-instance v4, LX/1dq;

    const v5, 0x7f0102e4

    if-eqz v0, :cond_1

    const v1, 0x7f020a63

    :goto_1
    invoke-static {p1, v5, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    const v5, 0x7f0102e5

    const v6, 0x7f020aed

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v4, v1, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286570
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    new-instance v3, LX/1dq;

    const v4, 0x7f0102e6

    const v5, 0x7f020aed

    invoke-static {p1, v4, v5}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    const v5, 0x7f0102e6

    const v6, 0x7f020aed

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v3, v4, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v1, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286571
    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    new-instance v3, LX/1dq;

    const v4, 0x7f0102e7

    const v5, 0x7f020a70

    invoke-static {p1, v4, v5}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    const v5, 0x7f0102e8

    const v6, 0x7f020a6d

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v3, v4, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v1, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286572
    sget-object v3, LX/1X9;->BOX:LX/1X9;

    new-instance v4, LX/1dq;

    const v5, 0x7f0102e9

    if-eqz v0, :cond_2

    const v1, 0x7f020a67

    :goto_2
    invoke-static {p1, v5, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0102ea

    if-eqz v0, :cond_3

    const v1, 0x7f020a6c

    :goto_3
    invoke-static {p1, v6, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v4, v5, v1, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286573
    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    new-instance v4, LX/1dq;

    const v5, 0x7f0102eb

    if-eqz v0, :cond_4

    const v1, 0x7f020a65

    :goto_4
    invoke-static {p1, v5, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    const v5, 0x7f0102ec

    const v6, 0x7f020a6a

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e4

    const p2, 0x7f0219e7

    invoke-direct {v4, v1, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286574
    sget-object v1, LX/1X9;->FOLLOW_UP:LX/1X9;

    new-instance v3, LX/1dq;

    const v4, 0x7f0102ed

    const v5, 0x7f020a70

    invoke-static {p1, v4, v5}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    const v5, 0x7f0102ed

    const v6, 0x7f020a70

    invoke-static {p1, v5, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    const v6, 0x7f0219e7

    const p2, 0x7f0219e7

    invoke-direct {v3, v4, v5, v6, p2}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v2, v1, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286575
    move-object v0, v2

    .line 286576
    iput-object v0, p0, LX/1do;->a:Ljava/util/EnumMap;

    .line 286577
    return-void

    .line 286578
    :cond_0
    const v1, 0x7f020a73

    goto/16 :goto_0

    .line 286579
    :cond_1
    const v1, 0x7f020a62

    goto/16 :goto_1

    .line 286580
    :cond_2
    const v1, 0x7f020a66

    goto :goto_2

    :cond_3
    const v1, 0x7f020a6b

    goto :goto_3

    .line 286581
    :cond_4
    const v1, 0x7f020a64

    goto :goto_4
.end method

.method public static a(LX/0QB;)LX/1do;
    .locals 5

    .prologue
    .line 286582
    const-class v1, LX/1do;

    monitor-enter v1

    .line 286583
    :try_start_0
    sget-object v0, LX/1do;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 286584
    sput-object v2, LX/1do;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 286585
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286586
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 286587
    new-instance p0, LX/1do;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v4

    check-cast v4, LX/0fO;

    invoke-direct {p0, v3, v4}, LX/1do;-><init>(Landroid/content/Context;LX/0fO;)V

    .line 286588
    move-object v0, p0

    .line 286589
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 286590
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1do;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286591
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 286592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;LX/1X9;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 286593
    iget-object v0, p0, LX/1do;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dq;

    invoke-virtual {v0, p1, p3, p4}, LX/1dq;->a(Landroid/content/res/Resources;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
