.class public final LX/1Hl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation


# instance fields
.field private a:Z

.field private b:J

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 227650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227651
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Hl;->a:Z

    .line 227652
    iput-wide v2, p0, LX/1Hl;->b:J

    .line 227653
    iput-wide v2, p0, LX/1Hl;->c:J

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(JJ)V
    .locals 1

    .prologue
    .line 227662
    monitor-enter p0

    :try_start_0
    iput-wide p3, p0, LX/1Hl;->c:J

    .line 227663
    iput-wide p1, p0, LX/1Hl;->b:J

    .line 227664
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Hl;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227665
    monitor-exit p0

    return-void

    .line 227666
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 227661
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1Hl;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 227667
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1Hl;->a:Z

    .line 227668
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1Hl;->c:J

    .line 227669
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/1Hl;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227670
    monitor-exit p0

    return-void

    .line 227671
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(JJ)V
    .locals 3

    .prologue
    .line 227656
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1Hl;->a:Z

    if-eqz v0, :cond_0

    .line 227657
    iget-wide v0, p0, LX/1Hl;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/1Hl;->b:J

    .line 227658
    iget-wide v0, p0, LX/1Hl;->c:J

    add-long/2addr v0, p3

    iput-wide v0, p0, LX/1Hl;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227659
    :cond_0
    monitor-exit p0

    return-void

    .line 227660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()J
    .locals 2

    .prologue
    .line 227655
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Hl;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()J
    .locals 2

    .prologue
    .line 227654
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/1Hl;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
