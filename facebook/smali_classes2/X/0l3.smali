.class public LX/0l3;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/14l;

.field private final c:LX/0WJ;

.field private final d:LX/0Uq;

.field public final e:LX/0l4;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Xl;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field private final j:Ljava/util/concurrent/Executor;

.field public k:Ljava/lang/String;

.field public l:LX/0Yb;

.field public m:LX/2EJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 128206
    sget-object v0, LX/0ax;->A:Ljava/lang/String;

    sget-object v1, LX/0ax;->z:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/0l3;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/0Uq;LX/0l4;LX/0Ot;LX/0Or;LX/0Xl;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Uq;",
            "LX/0l4;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 128207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128208
    iput-object p1, p0, LX/0l3;->c:LX/0WJ;

    .line 128209
    iput-object p2, p0, LX/0l3;->d:LX/0Uq;

    .line 128210
    iput-object p3, p0, LX/0l3;->e:LX/0l4;

    .line 128211
    iput-object p4, p0, LX/0l3;->f:LX/0Ot;

    .line 128212
    iput-object p5, p0, LX/0l3;->g:LX/0Or;

    .line 128213
    iput-object p6, p0, LX/0l3;->h:LX/0Xl;

    .line 128214
    iput-object p7, p0, LX/0l3;->i:Ljava/util/concurrent/ExecutorService;

    .line 128215
    iput-object p8, p0, LX/0l3;->j:Ljava/util/concurrent/Executor;

    .line 128216
    return-void
.end method

.method public static d(LX/0l3;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 128217
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128218
    iget-object v0, p0, LX/0l3;->m:LX/2EJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0l3;->m:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 128219
    if-nez v0, :cond_0

    .line 128220
    new-instance v0, LX/31Y;

    invoke-direct {v0, p1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080166

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080167

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080168

    new-instance v2, LX/HlL;

    invoke-direct {v2, p0, p1}, LX/HlL;-><init>(LX/0l3;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    .line 128221
    iget-object v1, p0, LX/0l3;->j:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/auth/activity/AuthenticatedActivityHelper$3;-><init>(LX/0l3;LX/0ju;)V

    const v0, -0x5ff2d9a9

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 128222
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0l3;Landroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 128223
    instance-of v0, p1, LX/0l6;

    if-eqz v0, :cond_0

    move v0, v1

    .line 128224
    :goto_0
    return v0

    .line 128225
    :cond_0
    iget-object v0, p0, LX/0l3;->d:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->c()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 128226
    goto :goto_0

    .line 128227
    :cond_1
    iget-object v0, p0, LX/0l3;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/0l3;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LX/0l3;->k:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 128228
    goto :goto_0

    .line 128229
    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 128230
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 128231
    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 128232
    sget-object v0, LX/0l3;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128233
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 128234
    goto :goto_0

    .line 128235
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
