.class public LX/1cz;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public b:LX/1mp;

.field public c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;LX/1mp;)V
    .locals 0

    .prologue
    .line 283948
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 283949
    invoke-virtual {p0, p1, p2}, LX/1cz;->a(Landroid/graphics/drawable/Drawable;LX/1mp;)V

    .line 283950
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;LX/1mp;)V
    .locals 2

    .prologue
    .line 283951
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 283952
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 283953
    :cond_0
    iput-object p1, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    .line 283954
    if-eqz p1, :cond_1

    .line 283955
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 283956
    :cond_1
    iput-object p2, p0, LX/1cz;->b:LX/1mp;

    .line 283957
    invoke-virtual {p0}, LX/1cz;->invalidateSelf()V

    .line 283958
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 283959
    iget-object v0, p0, LX/1cz;->b:LX/1mp;

    if-nez v0, :cond_0

    .line 283960
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 283961
    :goto_0
    return-void

    .line 283962
    :cond_0
    :try_start_0
    iget-boolean v0, p0, LX/1cz;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1cz;->b:LX/1mp;

    invoke-virtual {v0}, LX/1mp;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 283963
    :cond_1
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 283964
    iget-object v1, p0, LX/1cz;->b:LX/1mp;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/1mp;->a(II)Landroid/graphics/Canvas;

    move-result-object v1

    .line 283965
    iget v2, v0, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 283966
    iget-object v2, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 283967
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 283968
    iget-object v2, p0, LX/1cz;->b:LX/1mp;

    invoke-virtual {v2, v1}, LX/1mp;->a(Landroid/graphics/Canvas;)V

    .line 283969
    iget-object v1, p0, LX/1cz;->b:LX/1mp;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v0}, LX/1mp;->a(IIII)V

    .line 283970
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1cz;->d:Z

    .line 283971
    :cond_2
    iget-object v0, p0, LX/1cz;->b:LX/1mp;

    invoke-virtual {v0}, LX/1mp;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283972
    iget-object v0, p0, LX/1cz;->b:LX/1mp;

    .line 283973
    iget-object v1, v0, LX/1mp;->a:LX/1mr;

    invoke-interface {v1, p1}, LX/1mr;->b(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch LX/32E; {:try_start_0 .. :try_end_0} :catch_0

    .line 283974
    goto :goto_0

    .line 283975
    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/1cz;->b:LX/1mp;

    .line 283976
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 283977
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_1
    .catch LX/32E; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 283978
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    return v0
.end method

.method public final getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 283979
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 283980
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 283981
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getMinimumHeight()I
    .locals 1

    .prologue
    .line 283982
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public final getMinimumWidth()I
    .locals 1

    .prologue
    .line 283983
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 283946
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 283984
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final getState()[I
    .locals 1

    .prologue
    .line 283985
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public final getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    .prologue
    .line 283986
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 283987
    invoke-virtual {p0}, LX/1cz;->invalidateSelf()V

    .line 283988
    iget-boolean v0, p0, LX/1cz;->c:Z

    if-eqz v0, :cond_0

    .line 283989
    :goto_0
    return-void

    .line 283990
    :cond_0
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1cz;->setBounds(Landroid/graphics/Rect;)V

    .line 283991
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1cz;->d:Z

    goto :goto_0
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 283947
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    return v0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 283992
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    .line 283993
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 283994
    if-eq v1, v0, :cond_0

    .line 283995
    iget-object v0, p0, LX/1cz;->b:LX/1mp;

    invoke-virtual {p0, v1, v0}, LX/1cz;->a(Landroid/graphics/drawable/Drawable;LX/1mp;)V

    .line 283996
    :cond_0
    return-object p0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 283921
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 283922
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 283923
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    return v0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 283924
    invoke-virtual {p0, p2, p3, p4}, LX/1cz;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 283925
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 283926
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 283927
    return-void
.end method

.method public final setChangingConfigurations(I)V
    .locals 1

    .prologue
    .line 283928
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 283929
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 283930
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 283931
    return-void
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 283932
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 283933
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 283934
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 283935
    return-void
.end method

.method public final setState([I)Z
    .locals 1

    .prologue
    .line 283936
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    return v0
.end method

.method public final setTint(I)V
    .locals 1

    .prologue
    .line 283937
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1cz;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 283938
    return-void
.end method

.method public final setTintList(Landroid/content/res/ColorStateList;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 283939
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 283940
    return-void
.end method

.method public final setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 283941
    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 283942
    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 283943
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1cz;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 283944
    invoke-virtual {p0, p2}, LX/1cz;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 283945
    return-void
.end method
