.class public final LX/0aR;
.super LX/0aS;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aS",
        "<",
        "Lcom/facebook/contacts/service/ContactChatContextRefresher;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0aR;


# instance fields
.field private final a:LX/0aU;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0aU;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/contacts/service/ContactChatContextRefresher;",
            ">;",
            "LX/0aU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 84763
    sget-object v0, LX/0Zz;->CROSS_APP:LX/0Zz;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "NEARBY_FRIENDS_SETTINGS_CHANGED_ACTION"

    invoke-virtual {p2, v3}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, p1, v1}, LX/0aS;-><init>(LX/0Zz;LX/0Ot;[Ljava/lang/String;)V

    .line 84764
    iput-object p2, p0, LX/0aR;->a:LX/0aU;

    .line 84765
    return-void
.end method

.method public static a(LX/0QB;)LX/0aR;
    .locals 5

    .prologue
    .line 84773
    sget-object v0, LX/0aR;->b:LX/0aR;

    if-nez v0, :cond_1

    .line 84774
    const-class v1, LX/0aR;

    monitor-enter v1

    .line 84775
    :try_start_0
    sget-object v0, LX/0aR;->b:LX/0aR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 84776
    if-eqz v2, :cond_0

    .line 84777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 84778
    new-instance v4, LX/0aR;

    const/16 v3, 0x1a26

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v3

    check-cast v3, LX/0aU;

    invoke-direct {v4, p0, v3}, LX/0aR;-><init>(LX/0Ot;LX/0aU;)V

    .line 84779
    move-object v0, v4

    .line 84780
    sput-object v0, LX/0aR;->b:LX/0aR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 84782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 84783
    :cond_1
    sget-object v0, LX/0aR;->b:LX/0aR;

    return-object v0

    .line 84784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 84785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 84766
    check-cast p3, Lcom/facebook/contacts/service/ContactChatContextRefresher;

    .line 84767
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/0aR;->a:LX/0aU;

    const-string v2, "NEARBY_FRIENDS_SETTINGS_CHANGED_ACTION"

    invoke-virtual {v1, v2}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84768
    iget-object v0, p3, Lcom/facebook/contacts/service/ContactChatContextRefresher;->d:LX/0Uh;

    const/16 v1, 0x534

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 84769
    :goto_0
    iget-object v3, p3, Lcom/facebook/contacts/service/ContactChatContextRefresher;->d:LX/0Uh;

    const/16 v4, 0x534

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_2

    .line 84770
    :cond_0
    :goto_1
    return-void

    .line 84771
    :cond_1
    iget-object v0, p3, Lcom/facebook/contacts/service/ContactChatContextRefresher;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3LK;

    invoke-virtual {v0}, LX/3LK;->b()V

    goto :goto_0

    .line 84772
    :cond_2
    iget-object v3, p3, Lcom/facebook/contacts/service/ContactChatContextRefresher;->c:LX/0aG;

    const-string v4, "sync_chat_context"

    sget-object v5, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v7, Lcom/facebook/contacts/service/ContactChatContextRefresher;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v8, -0x2912aa11

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    goto :goto_1
.end method
