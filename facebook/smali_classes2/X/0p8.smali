.class public LX/0p8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/0p8;


# instance fields
.field public final a:LX/0W3;

.field private b:Z

.field private c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0p1;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/0p3;",
            ">;"
        }
    .end annotation
.end field

.field public final g:[D

.field public h:LX/1pb;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 143801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143802
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p8;->b:Z

    .line 143803
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0p8;->d:Ljava/util/List;

    .line 143804
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143805
    const/4 v0, 0x6

    new-array v0, v0, [D

    iput-object v0, p0, LX/0p8;->g:[D

    .line 143806
    iput-object p1, p0, LX/0p8;->a:LX/0W3;

    .line 143807
    return-void
.end method

.method public static a(LX/0QB;)LX/0p8;
    .locals 4

    .prologue
    .line 143724
    sget-object v0, LX/0p8;->i:LX/0p8;

    if-nez v0, :cond_1

    .line 143725
    const-class v1, LX/0p8;

    monitor-enter v1

    .line 143726
    :try_start_0
    sget-object v0, LX/0p8;->i:LX/0p8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 143727
    if-eqz v2, :cond_0

    .line 143728
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 143729
    new-instance p0, LX/0p8;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/0p8;-><init>(LX/0W3;)V

    .line 143730
    move-object v0, p0

    .line 143731
    sput-object v0, LX/0p8;->i:LX/0p8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143732
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 143733
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143734
    :cond_1
    sget-object v0, LX/0p8;->i:LX/0p8;

    return-object v0

    .line 143735
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 143736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static d(LX/0p8;)LX/0p3;
    .locals 11

    .prologue
    .line 143771
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    if-nez v0, :cond_0

    .line 143772
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    .line 143773
    :goto_0
    return-object v0

    .line 143774
    :cond_0
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    const/4 v10, 0x0

    .line 143775
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_1

    .line 143776
    iget-object v4, p0, LX/0p8;->g:[D

    iget-object v5, p0, LX/0p8;->a:LX/0W3;

    sget-wide v6, LX/0X5;->bI:J

    const-wide/32 v8, 0x186a0

    invoke-interface {v5, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v6

    long-to-double v6, v6

    aput-wide v6, v4, v10

    .line 143777
    :cond_1
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    move-wide v2, v4

    .line 143778
    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 143779
    sget-object v0, LX/0p3;->DEGRADED:LX/0p3;

    goto :goto_0

    .line 143780
    :cond_2
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    const/4 v10, 0x1

    .line 143781
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_3

    .line 143782
    iget-object v4, p0, LX/0p8;->g:[D

    iget-object v5, p0, LX/0p8;->a:LX/0W3;

    sget-wide v6, LX/0X5;->bE:J

    const-wide/16 v8, 0x3e8

    invoke-interface {v5, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v6

    long-to-double v6, v6

    aput-wide v6, v4, v10

    .line 143783
    :cond_3
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    move-wide v2, v4

    .line 143784
    cmpl-double v0, v0, v2

    if-lez v0, :cond_4

    .line 143785
    sget-object v0, LX/0p3;->POOR:LX/0p3;

    goto :goto_0

    .line 143786
    :cond_4
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    const/4 v10, 0x2

    .line 143787
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_5

    .line 143788
    iget-object v4, p0, LX/0p8;->g:[D

    iget-object v5, p0, LX/0p8;->a:LX/0W3;

    sget-wide v6, LX/0X5;->bF:J

    const-wide/16 v8, 0x1f4

    invoke-interface {v5, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v6

    long-to-double v6, v6

    aput-wide v6, v4, v10

    .line 143789
    :cond_5
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    move-wide v2, v4

    .line 143790
    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    .line 143791
    sget-object v0, LX/0p3;->MODERATE:LX/0p3;

    goto/16 :goto_0

    .line 143792
    :cond_6
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    const/4 v10, 0x3

    .line 143793
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_7

    .line 143794
    iget-object v4, p0, LX/0p8;->g:[D

    iget-object v5, p0, LX/0p8;->a:LX/0W3;

    sget-wide v6, LX/0X5;->bG:J

    const-wide/16 v8, 0xfa

    invoke-interface {v5, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v6

    long-to-double v6, v6

    aput-wide v6, v4, v10

    .line 143795
    :cond_7
    iget-object v4, p0, LX/0p8;->g:[D

    aget-wide v4, v4, v10

    move-wide v2, v4

    .line 143796
    cmpl-double v0, v0, v2

    if-lez v0, :cond_8

    .line 143797
    sget-object v0, LX/0p3;->GOOD:LX/0p3;

    goto/16 :goto_0

    .line 143798
    :cond_8
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->a()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_9

    .line 143799
    sget-object v0, LX/0p3;->EXCELLENT:LX/0p3;

    goto/16 :goto_0

    .line 143800
    :cond_9
    sget-object v0, LX/0p3;->UNKNOWN:LX/0p3;

    goto/16 :goto_0
.end method

.method private static e(LX/0p8;)D
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 143808
    iget-object v0, p0, LX/0p8;->g:[D

    aget-wide v0, v0, v8

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 143809
    iget-object v0, p0, LX/0p8;->g:[D

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    iget-object v1, p0, LX/0p8;->a:LX/0W3;

    sget-wide v4, LX/0X5;->bA:J

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v5, v6, v7}, LX/0W4;->a(JJ)J

    move-result-wide v4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    aput-wide v2, v0, v8

    .line 143810
    :cond_0
    iget-object v0, p0, LX/0p8;->g:[D

    aget-wide v0, v0, v8

    return-wide v0
.end method


# virtual methods
.method public final a(LX/0p1;)LX/0p3;
    .locals 1

    .prologue
    .line 143769
    iget-object v0, p0, LX/0p8;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143770
    iget-object v0, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p3;

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 143764
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    if-eqz v0, :cond_0

    .line 143765
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0}, LX/1pb;->b()V

    .line 143766
    :cond_0
    iget-object v0, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143767
    monitor-exit p0

    return-void

    .line 143768
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(D)V
    .locals 12

    .prologue
    .line 143737
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 143738
    :goto_0
    return-void

    .line 143739
    :cond_0
    monitor-enter p0

    .line 143740
    :try_start_0
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    if-nez v0, :cond_1

    .line 143741
    new-instance v0, LX/1pa;

    invoke-static {p0}, LX/0p8;->e(LX/0p8;)D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LX/1pa;-><init>(D)V

    iput-object v0, p0, LX/0p8;->h:LX/1pb;

    .line 143742
    :cond_1
    iget-object v0, p0, LX/0p8;->h:LX/1pb;

    invoke-interface {v0, p1, p2}, LX/1pb;->a(D)V

    .line 143743
    iget-boolean v0, p0, LX/0p8;->b:Z

    if-eqz v0, :cond_5

    .line 143744
    iget v0, p0, LX/0p8;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0p8;->c:I

    .line 143745
    invoke-static {p0}, LX/0p8;->d(LX/0p8;)LX/0p3;

    move-result-object v0

    iget-object v1, p0, LX/0p8;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 143746
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p8;->b:Z

    .line 143747
    const/4 v0, 0x1

    iput v0, p0, LX/0p8;->c:I

    .line 143748
    :cond_2
    iget v0, p0, LX/0p8;->c:I

    const/4 v11, 0x5

    .line 143749
    iget-object v5, p0, LX/0p8;->g:[D

    aget-wide v5, v5, v11

    const-wide/16 v7, 0x0

    cmpl-double v5, v5, v7

    if-nez v5, :cond_3

    .line 143750
    iget-object v5, p0, LX/0p8;->g:[D

    iget-object v6, p0, LX/0p8;->a:LX/0W3;

    sget-wide v7, LX/0X5;->by:J

    const-wide/16 v9, 0xf

    invoke-interface {v6, v7, v8, v9, v10}, LX/0W4;->a(JJ)J

    move-result-wide v7

    long-to-double v7, v7

    aput-wide v7, v5, v11

    .line 143751
    :cond_3
    iget-object v5, p0, LX/0p8;->g:[D

    aget-wide v5, v5, v11

    double-to-int v5, v5

    move v1, v5

    .line 143752
    if-lt v0, v1, :cond_4

    .line 143753
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0p8;->b:Z

    .line 143754
    const/4 v0, 0x1

    iput v0, p0, LX/0p8;->c:I

    .line 143755
    iget-object v0, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, LX/0p8;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 143756
    iget-object v0, p0, LX/0p8;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0p1;

    .line 143757
    iget-object v1, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0p3;

    invoke-interface {v0, v1}, LX/0p1;->b(LX/0p3;)V

    goto :goto_1

    .line 143758
    :cond_4
    monitor-exit p0

    goto/16 :goto_0

    .line 143759
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 143760
    :cond_5
    :try_start_1
    iget-object v0, p0, LX/0p8;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0}, LX/0p8;->d(LX/0p8;)LX/0p3;

    move-result-object v1

    if-eq v0, v1, :cond_6

    .line 143761
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0p8;->b:Z

    .line 143762
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p0}, LX/0p8;->d(LX/0p8;)LX/0p3;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/0p8;->f:Ljava/util/concurrent/atomic/AtomicReference;

    .line 143763
    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
