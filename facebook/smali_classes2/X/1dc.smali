.class public abstract LX/1dc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/1n4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1n4",
            "<T",
            "L;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1n4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n4",
            "<T",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286486
    iput-object p1, p0, LX/1dc;->a:LX/1n4;

    .line 286487
    return-void
.end method

.method public static a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1De;",
            "LX/1dc",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 286488
    iget-object v0, p1, LX/1dc;->a:LX/1n4;

    invoke-virtual {v0, p0, p1}, LX/1n4;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1De;",
            "TT;",
            "LX/1dc",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 286489
    iget-object v0, p2, LX/1dc;->a:LX/1n4;

    invoke-virtual {v0, p0, p1, p2}, LX/1n4;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 286490
    return-void
.end method

.method public static a(LX/1dc;LX/1dc;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1dc",
            "<TT;>;",
            "LX/1dc",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 286491
    if-eqz p0, :cond_1

    .line 286492
    const/4 v0, 0x1

    .line 286493
    if-nez p0, :cond_4

    .line 286494
    if-eqz p1, :cond_3

    .line 286495
    :cond_0
    :goto_0
    move v0, v0

    .line 286496
    :goto_1
    return v0

    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 286497
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 286498
    :cond_4
    if-eqz p1, :cond_0

    .line 286499
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 286500
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 286501
    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method
