.class public LX/0Xi;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/util/concurrent/ScheduledExecutorService;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79356
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 4

    .prologue
    .line 79344
    sget-object v0, LX/0Xi;->a:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_1

    .line 79345
    const-class v1, LX/0Xi;

    monitor-enter v1

    .line 79346
    :try_start_0
    sget-object v0, LX/0Xi;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79347
    if-eqz v2, :cond_0

    .line 79348
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79349
    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v3

    check-cast v3, LX/0TJ;

    const/16 p0, 0x1a1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3, p0}, LX/0Su;->c(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    move-object v0, v3

    .line 79350
    sput-object v0, LX/0Xi;->a:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79351
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79352
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79353
    :cond_1
    sget-object v0, LX/0Xi;->a:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0

    .line 79354
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79355
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79343
    invoke-static {p0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v0

    check-cast v0, LX/0TJ;

    const/16 v1, 0x1a1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Su;->c(LX/0TJ;LX/0Or;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method
