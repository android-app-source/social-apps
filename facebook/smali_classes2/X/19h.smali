.class public LX/19h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/19h;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/5PX;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/SurfaceTexture;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0rb;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0rb;",
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 208346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208347
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/19h;->e:Ljava/util/List;

    .line 208348
    iput-object p1, p0, LX/19h;->a:LX/0Uh;

    .line 208349
    iput-object p3, p0, LX/19h;->b:LX/0Ot;

    .line 208350
    iput-object p4, p0, LX/19h;->c:LX/0Ot;

    .line 208351
    if-eqz p2, :cond_0

    .line 208352
    invoke-interface {p2, p0}, LX/0rb;->a(LX/0rf;)V

    .line 208353
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/19h;
    .locals 7

    .prologue
    .line 208333
    sget-object v0, LX/19h;->f:LX/19h;

    if-nez v0, :cond_1

    .line 208334
    const-class v1, LX/19h;

    monitor-enter v1

    .line 208335
    :try_start_0
    sget-object v0, LX/19h;->f:LX/19h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 208336
    if-eqz v2, :cond_0

    .line 208337
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 208338
    new-instance v5, LX/19h;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v4

    check-cast v4, LX/0rb;

    const/16 v6, 0x1335

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, LX/19h;-><init>(LX/0Uh;LX/0rb;LX/0Ot;LX/0Ot;)V

    .line 208339
    move-object v0, v5

    .line 208340
    sput-object v0, LX/19h;->f:LX/19h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208341
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 208342
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208343
    :cond_1
    sget-object v0, LX/19h;->f:LX/19h;

    return-object v0

    .line 208344
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 208345
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/19h;Z)Landroid/graphics/SurfaceTexture;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 208281
    invoke-static {}, LX/19h;->g()V

    .line 208282
    if-eqz p1, :cond_0

    .line 208283
    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v2, 0x2cd

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v2, 0x2ce

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 208284
    :goto_0
    return-object v0

    .line 208285
    :cond_0
    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v2, 0x2d0

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v2, 0x2d1

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 208286
    goto :goto_0

    .line 208287
    :cond_1
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    if-eqz v0, :cond_2

    .line 208288
    :try_start_0
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    invoke-virtual {v0}, LX/5PV;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208289
    :goto_1
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 208290
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 208291
    iget-object v1, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1C2;

    sget-object v2, LX/0JO;->SURFACE_TEXTURE_POOL_HIT:LX/0JO;

    iget-object v3, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3, p1}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    goto :goto_0

    .line 208292
    :catch_0
    move-exception v0

    .line 208293
    const-string v2, "getSurfaceTexture"

    const-string v3, "Error encountered in makeCurrent with PbufferSurface"

    invoke-static {p0, v2, v3, v0}, LX/19h;->a(LX/19h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208294
    invoke-static {p0}, LX/19h;->e(LX/19h;)V

    goto :goto_1

    .line 208295
    :cond_2
    const/4 v0, 0x1

    .line 208296
    iget-object v2, p0, LX/19h;->d:LX/5PX;

    if-eqz v2, :cond_5

    .line 208297
    :goto_2
    move v0, v0

    .line 208298
    if-nez v0, :cond_3

    .line 208299
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v2, LX/0JO;->SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

    iget-object v3, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v2, v3, p1}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    move-object v0, v1

    .line 208300
    goto :goto_0

    .line 208301
    :cond_3
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 208302
    new-array v4, v7, [I

    .line 208303
    :try_start_1
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    invoke-virtual {v0}, LX/5PV;->a()V

    .line 208304
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v4, v3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 208305
    const-string v0, "glGenTextures"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 208306
    const/4 v0, 0x0

    aget v0, v4, v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    .line 208307
    const v3, 0x8d65

    :try_start_2
    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 208308
    const-string v3, "glBindTexture"

    invoke-static {v3}, LX/5PP;->a(Ljava/lang/String;)V

    .line 208309
    const v3, 0x8d65

    const/16 v5, 0x2801

    const/high16 v6, 0x46180000    # 9728.0f

    invoke-static {v3, v5, v6}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 208310
    const v3, 0x8d65

    const/16 v5, 0x2800

    const v6, 0x46180400    # 9729.0f

    invoke-static {v3, v5, v6}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 208311
    const v3, 0x8d65

    const/16 v5, 0x2802

    const v6, 0x812f

    invoke-static {v3, v5, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 208312
    const v3, 0x8d65

    const/16 v5, 0x2803

    const v6, 0x812f

    invoke-static {v3, v5, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 208313
    const-string v3, "glTexParameter"

    invoke-static {v3}, LX/5PP;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3

    .line 208314
    :goto_3
    move v2, v0

    .line 208315
    if-nez v2, :cond_4

    .line 208316
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v2, LX/0JO;->SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

    iget-object v3, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v2, v3, p1}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    move-object v0, v1

    .line 208317
    goto/16 :goto_0

    .line 208318
    :cond_4
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_MISSED:LX/0JO;

    iget-object v3, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v1, v3, p1}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    .line 208319
    new-instance v0, Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v2}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 208320
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    goto/16 :goto_0

    .line 208321
    :cond_5
    :try_start_3
    new-instance v2, LX/5PX;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, LX/5PX;-><init>(II)V

    iput-object v2, p0, LX/19h;->d:LX/5PX;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    .line 208322
    :catch_1
    move-exception v0

    .line 208323
    const-string v2, "setupPbufferSurface"

    const-string v3, "Error encountered in creating 1x1 PbufferSurface"

    invoke-static {p0, v2, v3, v0}, LX/19h;->a(LX/19h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208324
    invoke-static {p0}, LX/19h;->e(LX/19h;)V

    .line 208325
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 208326
    :catch_2
    move-exception v0

    move v3, v2

    .line 208327
    :goto_4
    const-string v5, "generateExternalTexture"

    const-string v6, "Error encountered in creating an external texture"

    invoke-static {p0, v5, v6, v0}, LX/19h;->a(LX/19h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208328
    if-eqz v3, :cond_6

    .line 208329
    invoke-static {v7, v4, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 208330
    :cond_6
    invoke-static {p0}, LX/19h;->e(LX/19h;)V

    move v0, v2

    .line 208331
    goto :goto_3

    .line 208332
    :catch_3
    move-exception v3

    move-object v8, v3

    move v3, v0

    move-object v0, v8

    goto :goto_4
.end method

.method public static a(LX/19h;Landroid/graphics/SurfaceTexture;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208262
    invoke-static {}, LX/19h;->g()V

    .line 208263
    if-eqz p2, :cond_0

    .line 208264
    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v1, 0x2ce

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 208265
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 208266
    :goto_0
    return-void

    .line 208267
    :cond_0
    iget-object v0, p0, LX/19h;->a:LX/0Uh;

    const/16 v1, 0x2d1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 208268
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    goto :goto_0

    .line 208269
    :cond_1
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_2

    .line 208270
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 208271
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_OVERFLOWED:LX/0JO;

    iget-object v2, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2, p2}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    goto :goto_0

    .line 208272
    :cond_2
    if-nez p2, :cond_3

    .line 208273
    :try_start_0
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    .line 208274
    iget-object v1, v0, LX/5PV;->b:LX/5PM;

    invoke-virtual {v1, p1}, LX/5PM;->b(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208275
    :cond_3
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208276
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_POOLED:LX/0JO;

    iget-object v2, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2, p2}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    goto :goto_0

    .line 208277
    :catch_0
    move-exception v0

    .line 208278
    const-string v1, "recycleSurfaceTexture"

    const-string v2, "Error encountered in clearing SurfaceTexture"

    invoke-static {p0, v1, v2, v0}, LX/19h;->a(LX/19h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 208279
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 208280
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    sget-object v1, LX/0JO;->SURFACE_TEXTURE_POOL_FAILED:LX/0JO;

    iget-object v2, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2, p2}, LX/1C2;->a(LX/0JO;IZ)LX/1C2;

    goto :goto_0
.end method

.method public static a(LX/19h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 208256
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SurfaceTexturePool."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 208257
    iput-object p3, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 208258
    move-object v0, v0

    .line 208259
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 208260
    iget-object v0, p0, LX/19h;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 208261
    return-void
.end method

.method public static b(LX/19h;LX/32G;)V
    .locals 4

    .prologue
    .line 208249
    invoke-static {}, LX/19h;->g()V

    .line 208250
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208251
    :goto_0
    return-void

    .line 208252
    :cond_0
    iget-object v0, p0, LX/19h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    iget-object v1, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 208253
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0JO;->SURFACE_TEXTURE_POOL_TRIMMED:LX/0JO;

    iget-object v3, v3, LX/0JO;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/0JN;->POOL_COUNT:LX/0JN;

    iget-object v3, v3, LX/0JN;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/0JN;->TRIM_TYPE:LX/0JN;

    iget-object v3, v3, LX/0JN;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 208254
    invoke-static {v0, v2}, LX/1C2;->d(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1C2;

    .line 208255
    invoke-static {p0}, LX/19h;->e(LX/19h;)V

    goto :goto_0
.end method

.method public static e(LX/19h;)V
    .locals 2

    .prologue
    .line 208234
    invoke-static {}, LX/19h;->g()V

    .line 208235
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 208236
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    goto :goto_0

    .line 208237
    :cond_0
    iget-object v0, p0, LX/19h;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 208238
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    if-eqz v0, :cond_1

    .line 208239
    iget-object v0, p0, LX/19h;->d:LX/5PX;

    invoke-virtual {v0}, LX/5PV;->c()V

    .line 208240
    const/4 v0, 0x0

    iput-object v0, p0, LX/19h;->d:LX/5PX;

    .line 208241
    :cond_1
    return-void
.end method

.method private static f()Z
    .locals 2

    .prologue
    .line 208248
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g()V
    .locals 1

    .prologue
    .line 208246
    invoke-static {}, LX/19h;->f()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 208247
    return-void
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 3

    .prologue
    .line 208242
    invoke-static {}, LX/19h;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208243
    invoke-static {p0, p1}, LX/19h;->b(LX/19h;LX/32G;)V

    .line 208244
    :goto_0
    return-void

    .line 208245
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/video/engine/texview/SurfaceTexturePool$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/SurfaceTexturePool$1;-><init>(LX/19h;LX/32G;)V

    const v2, 0xa24f1e0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
