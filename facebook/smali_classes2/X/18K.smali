.class public LX/18K;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/graphql/enums/StoryVisibility;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206259
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    sput-object v0, LX/18K;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 206260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/HideableUnit;)Lcom/facebook/graphql/enums/StoryVisibility;
    .locals 2

    .prologue
    .line 206261
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    .line 206262
    const-string v1, "local_story_visibility"

    invoke-interface {p0, v1, v0}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 206263
    invoke-virtual {v0}, LX/18L;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206264
    sget-object v0, LX/18K;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 206265
    :goto_0
    return-object v0

    .line 206266
    :cond_0
    :try_start_0
    iget-object v0, v0, LX/18L;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/StoryVisibility;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/StoryVisibility;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 206267
    :catch_0
    sget-object v0, LX/18K;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    goto :goto_0
.end method

.method public static a(LX/16d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 206268
    invoke-interface {p0}, LX/16d;->C_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/HideableUnit;)I
    .locals 2

    .prologue
    .line 206269
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    .line 206270
    const-string v1, "local_story_visible_height"

    invoke-interface {p0, v1, v0}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 206271
    invoke-virtual {v0}, LX/18L;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206272
    const/4 v0, 0x0

    .line 206273
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, LX/18L;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
