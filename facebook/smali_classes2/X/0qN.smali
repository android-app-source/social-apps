.class public LX/0qN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0qN;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 147409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147410
    return-void
.end method

.method public static a(LX/0QB;)LX/0qN;
    .locals 3

    .prologue
    .line 147397
    sget-object v0, LX/0qN;->a:LX/0qN;

    if-nez v0, :cond_1

    .line 147398
    const-class v1, LX/0qN;

    monitor-enter v1

    .line 147399
    :try_start_0
    sget-object v0, LX/0qN;->a:LX/0qN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147400
    if-eqz v2, :cond_0

    .line 147401
    :try_start_1
    new-instance v0, LX/0qN;

    invoke-direct {v0}, LX/0qN;-><init>()V

    .line 147402
    move-object v0, v0

    .line 147403
    sput-object v0, LX/0qN;->a:LX/0qN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147404
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147405
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147406
    :cond_1
    sget-object v0, LX/0qN;->a:LX/0qN;

    return-object v0

    .line 147407
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;II)Ljava/nio/ByteBuffer;
    .locals 9
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 147392
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 147393
    :try_start_0
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    int-to-long v2, p1

    int-to-long v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 147394
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    return-object v0

    .line 147395
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147396
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_0
.end method
