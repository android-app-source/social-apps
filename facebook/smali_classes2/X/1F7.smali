.class public LX/1F7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Landroid/util/SparseIntArray;

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method public constructor <init>(IILandroid/util/SparseIntArray;)V
    .locals 7
    .param p3    # Landroid/util/SparseIntArray;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221603
    const/4 v4, 0x0

    const v5, 0x7fffffff

    const/4 v6, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;III)V

    .line 221604
    return-void
.end method

.method public constructor <init>(IILandroid/util/SparseIntArray;III)V
    .locals 1
    .param p3    # Landroid/util/SparseIntArray;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221594
    if-ltz p1, :cond_0

    if-lt p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 221595
    iput p1, p0, LX/1F7;->b:I

    .line 221596
    iput p2, p0, LX/1F7;->a:I

    .line 221597
    iput-object p3, p0, LX/1F7;->c:Landroid/util/SparseIntArray;

    .line 221598
    iput p4, p0, LX/1F7;->d:I

    .line 221599
    iput p5, p0, LX/1F7;->e:I

    .line 221600
    iput p6, p0, LX/1F7;->f:I

    .line 221601
    return-void

    .line 221602
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
