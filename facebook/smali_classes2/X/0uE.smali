.class public LX/0uE;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0uN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0uN;

.field private c:Z

.field private d:J

.field private e:D

.field private f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 156126
    const-string v0, "BOOL"

    sget-object v1, LX/0uN;->BOOLEAN:LX/0uN;

    const-string v2, "INT"

    sget-object v3, LX/0uN;->INTEGER:LX/0uN;

    const-string v4, "FLOAT"

    sget-object v5, LX/0uN;->FLOAT:LX/0uN;

    const-string v6, "STRING"

    sget-object v7, LX/0uN;->STRING:LX/0uN;

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/0uE;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(D)V
    .locals 1

    .prologue
    .line 156187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156188
    sget-object v0, LX/0uN;->FLOAT:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156189
    iput-wide p1, p0, LX/0uE;->e:D

    .line 156190
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    .prologue
    .line 156183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156184
    sget-object v0, LX/0uN;->FLOAT:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156185
    float-to-double v0, p1

    iput-wide v0, p0, LX/0uE;->e:D

    .line 156186
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 156179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156180
    sget-object v0, LX/0uN;->INTEGER:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156181
    int-to-long v0, p1

    iput-wide v0, p0, LX/0uE;->d:J

    .line 156182
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 156175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156176
    sget-object v0, LX/0uN;->INTEGER:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156177
    iput-wide p1, p0, LX/0uE;->d:J

    .line 156178
    return-void
.end method

.method public constructor <init>(LX/0uN;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 156160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156161
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 156162
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Bad context value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156163
    :cond_1
    sget-object v0, LX/1jq;->a:[I

    invoke-virtual {p1}, LX/0uN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 156164
    new-instance v0, LX/5MH;

    const-string v1, "Unsupported context type"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156165
    :pswitch_0
    const-string v0, "true"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "false"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 156166
    :cond_2
    invoke-static {p2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/0uE;->c:Z

    .line 156167
    :goto_0
    iput-object p1, p0, LX/0uE;->b:LX/0uN;

    .line 156168
    return-void

    .line 156169
    :cond_3
    new-instance v0, LX/5MH;

    const-string v1, "Invalid boolean string"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156170
    :pswitch_1
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/0uE;->d:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 156171
    :catch_0
    new-instance v0, LX/5MH;

    const-string v1, "Invalid integer string"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156172
    :pswitch_2
    :try_start_1
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, LX/0uE;->e:D
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 156173
    :catch_1
    new-instance v0, LX/5MH;

    const-string v1, "Invalid float string"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156174
    :pswitch_3
    iput-object p2, p0, LX/0uE;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 156156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156157
    sget-object v0, LX/0uN;->STRING:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156158
    iput-object p1, p0, LX/0uE;->f:Ljava/lang/String;

    .line 156159
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 156152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156153
    sget-object v0, LX/0uN;->BOOLEAN:LX/0uN;

    iput-object v0, p0, LX/0uE;->b:LX/0uN;

    .line 156154
    iput-boolean p1, p0, LX/0uE;->c:Z

    .line 156155
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0uN;
    .locals 1

    .prologue
    .line 156151
    sget-object v0, LX/0uE;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0uN;

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 156148
    iget-object v0, p0, LX/0uE;->b:LX/0uN;

    sget-object v1, LX/0uN;->BOOLEAN:LX/0uN;

    if-eq v0, v1, :cond_0

    .line 156149
    new-instance v0, LX/5MH;

    const-string v1, "Invalid value type"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156150
    :cond_0
    iget-boolean v0, p0, LX/0uE;->c:Z

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 156142
    iget-object v0, p0, LX/0uE;->b:LX/0uN;

    sget-object v1, LX/0uN;->INTEGER:LX/0uN;

    if-ne v0, v1, :cond_0

    .line 156143
    iget-wide v0, p0, LX/0uE;->d:J

    .line 156144
    :goto_0
    return-wide v0

    .line 156145
    :cond_0
    iget-object v0, p0, LX/0uE;->b:LX/0uN;

    sget-object v1, LX/0uN;->FLOAT:LX/0uN;

    if-ne v0, v1, :cond_1

    .line 156146
    iget-wide v0, p0, LX/0uE;->e:D

    double-to-long v0, v0

    goto :goto_0

    .line 156147
    :cond_1
    new-instance v0, LX/5MH;

    const-string v1, "Invalid value type"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 156136
    iget-object v0, p0, LX/0uE;->b:LX/0uN;

    sget-object v1, LX/0uN;->INTEGER:LX/0uN;

    if-ne v0, v1, :cond_0

    .line 156137
    iget-wide v0, p0, LX/0uE;->d:J

    long-to-double v0, v0

    .line 156138
    :goto_0
    return-wide v0

    .line 156139
    :cond_0
    iget-object v0, p0, LX/0uE;->b:LX/0uN;

    sget-object v1, LX/0uN;->FLOAT:LX/0uN;

    if-ne v0, v1, :cond_1

    .line 156140
    iget-wide v0, p0, LX/0uE;->e:D

    goto :goto_0

    .line 156141
    :cond_1
    new-instance v0, LX/5MH;

    const-string v1, "Invalid value type"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 156127
    iget-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 156128
    iget-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    .line 156129
    :goto_0
    return-object v0

    .line 156130
    :cond_0
    sget-object v0, LX/1jq;->a:[I

    iget-object v1, p0, LX/0uE;->b:LX/0uN;

    invoke-virtual {v1}, LX/0uN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 156131
    const-string v0, "n/a"

    iput-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    .line 156132
    :goto_1
    iget-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    goto :goto_0

    .line 156133
    :pswitch_0
    iget-boolean v0, p0, LX/0uE;->c:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    goto :goto_1

    .line 156134
    :pswitch_1
    iget-wide v0, p0, LX/0uE;->d:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    goto :goto_1

    .line 156135
    :pswitch_2
    iget-wide v0, p0, LX/0uE;->e:D

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0uE;->f:Ljava/lang/String;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
