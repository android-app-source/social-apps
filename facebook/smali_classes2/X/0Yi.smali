.class public LX/0Yi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile N:LX/0Yi;

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:[LX/0Yj;

.field public static final c:[LX/0Yj;

.field public static final d:[LX/0Yj;

.field public static final e:[LX/0Yj;

.field public static final f:[I

.field public static final g:[LX/0Yj;


# instance fields
.field public A:Z

.field public B:Z

.field public C:J

.field public D:J

.field public E:J

.field public F:Z

.field public G:J

.field public H:I

.field public final I:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile J:Z

.field private volatile K:Z

.field private L:Z

.field public M:Z

.field public final h:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final i:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final j:LX/0Yl;

.field public final k:LX/0So;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Uo;

.field private final n:LX/0TD;

.field public final o:LX/0Ym;

.field public final p:LX/0Sk;

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public volatile w:J

.field public x:Z

.field public y:Z

.field public z:LX/0ao;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 82015
    const-class v0, LX/0Yi;

    sput-object v0, LX/0Yi;->a:Ljava/lang/Class;

    .line 82016
    new-array v0, v8, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa0047

    const-string v3, "NNFWarm_FragmentCreateToDataFetched"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa0001

    const-string v3, "NNFColdStart"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa0004

    const-string v3, "NNFWarmStart"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    sput-object v0, LX/0Yi;->b:[LX/0Yj;

    .line 82017
    new-array v0, v9, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa0014

    const-string v3, "NNFFreshContentStart"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa000f

    const-string v3, "NNFFreshFetch"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa004d

    const-string v3, "NNFColdStartNetwork"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, LX/0Yj;

    const v2, 0xa0020

    const-string v3, "NNFColdStartAndRenderTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v8

    .line 82018
    sput-object v0, LX/0Yi;->c:[LX/0Yj;

    new-array v1, v9, [LX/0Yj;

    new-instance v2, LX/0Yj;

    const v3, 0xa0013

    const-string v4, "NNFWarmStartTTI"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v5

    new-instance v2, LX/0Yj;

    const v3, 0xa0023

    const-string v4, "NNFWarmStartAndRenderTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v6

    new-instance v2, LX/0Yj;

    const v3, 0xa0024

    const-string v4, "NNFWarmStartAndFreshRenderTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v7

    new-instance v2, LX/0Yj;

    const v3, 0xa0025

    const-string v4, "NNFWarmStartAndCachedRenderTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v8

    const/4 v10, 0x0

    .line 82019
    array-length v2, v0

    array-length v3, v1

    add-int/2addr v2, v3

    new-array v2, v2, [LX/0Yj;

    .line 82020
    array-length v3, v0

    invoke-static {v0, v10, v2, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82021
    array-length v3, v0

    array-length v4, v1

    invoke-static {v1, v10, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82022
    move-object v0, v2

    .line 82023
    sput-object v0, LX/0Yi;->d:[LX/0Yj;

    .line 82024
    new-array v0, v9, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa003e

    const-string v3, "NNFTailFetchTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa0042

    const-string v3, "NNFTailFetchNetworkCallTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa0043

    const-string v3, "NNFTailFetchNotConnectedCallTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, LX/0Yj;

    const v2, 0xa003f

    const-string v3, "NNFTailFetchRenderTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v8

    sput-object v0, LX/0Yi;->e:[LX/0Yj;

    .line 82025
    new-array v0, v6, [I

    const v1, 0xa0032

    aput v1, v0, v5

    sput-object v0, LX/0Yi;->f:[I

    .line 82026
    new-array v0, v9, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa002b

    const-string v3, "NNFHotStartAndRenderTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa002c

    const-string v3, "NNFHotStartAndFreshRenderTime"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa004a

    const-string v3, "NNFHotStartAndFreshRenderTimeNotVisible"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, LX/0Yj;

    const v2, 0xa0038

    const-string v3, "NNFHotStartTTI"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v8

    sput-object v0, LX/0Yi;->g:[LX/0Yj;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Yl;LX/0So;LX/0Ot;LX/0Uo;LX/0TD;LX/0Ym;LX/0Sk;LX/0YP;)V
    .locals 2
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Yl;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0Uo;",
            "LX/0TD;",
            "LX/0Ym;",
            "LX/0Sk;",
            "LX/0YP;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82028
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->q:Z

    .line 82029
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->r:Z

    .line 82030
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->s:Z

    .line 82031
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->t:Z

    .line 82032
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->u:Z

    .line 82033
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->v:Z

    .line 82034
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0Yi;->w:J

    .line 82035
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->x:Z

    .line 82036
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->y:Z

    .line 82037
    sget-object v0, LX/0ao;->NOT_SET:LX/0ao;

    iput-object v0, p0, LX/0Yi;->z:LX/0ao;

    .line 82038
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->A:Z

    .line 82039
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->B:Z

    .line 82040
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0Yi;->C:J

    .line 82041
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->F:Z

    .line 82042
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0Yi;->G:J

    .line 82043
    const/4 v0, -0x1

    iput v0, p0, LX/0Yi;->H:I

    .line 82044
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 82045
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->J:Z

    .line 82046
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->K:Z

    .line 82047
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->L:Z

    .line 82048
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->M:Z

    .line 82049
    iput-object p1, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 82050
    iput-object p2, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 82051
    iput-object p3, p0, LX/0Yi;->j:LX/0Yl;

    .line 82052
    iput-object p4, p0, LX/0Yi;->k:LX/0So;

    .line 82053
    iput-object p5, p0, LX/0Yi;->l:LX/0Ot;

    .line 82054
    iput-object p6, p0, LX/0Yi;->m:LX/0Uo;

    .line 82055
    iput-object p7, p0, LX/0Yi;->n:LX/0TD;

    .line 82056
    iput-object p8, p0, LX/0Yi;->o:LX/0Ym;

    .line 82057
    iput-object p9, p0, LX/0Yi;->p:LX/0Sk;

    .line 82058
    const v0, 0xa0032

    invoke-virtual {p10, v0}, LX/0YP;->a(I)V

    .line 82059
    const v0, 0xa0041

    invoke-virtual {p10, v0}, LX/0YP;->a(I)V

    .line 82060
    const v0, 0xa0016

    invoke-virtual {p10, v0}, LX/0YP;->a(I)V

    .line 82061
    return-void
.end method

.method public static C(LX/0Yi;)V
    .locals 6

    .prologue
    .line 82062
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const/16 v1, 0x8

    new-array v1, v1, [LX/0Yj;

    const/4 v2, 0x0

    new-instance v3, LX/0Yj;

    const v4, 0xa0001

    const-string v5, "NNFColdStart"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, LX/0Yj;

    const v4, 0xa003c

    const-string v5, "NNFColdStartChromeLoadTime"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, LX/0Yj;

    const v4, 0xa0016

    const-string v5, "NNFColdStartTTI"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, LX/0Yj;

    const v4, 0xa004d

    const-string v5, "NNFColdStartNetwork"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, LX/0Yj;

    const v4, 0xa0020

    const-string v5, "NNFColdStartAndRenderTime"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, LX/0Yj;

    const v4, 0xa003a

    const-string v5, "NNFFirstRunColdStart"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, LX/0Yj;

    const v4, 0xa0054

    const-string v5, "NNFColdFreshContentStart"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-instance v3, LX/0Yj;

    const v4, 0xa00a6

    const-string v5, "NNFInspirationCameraColdTTI"

    invoke-direct {v3, v4, v5}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 82063
    invoke-static {v0, v1}, LX/0Yl;->d(LX/0Yl;Ljava/util/List;)V

    .line 82064
    return-void
.end method

.method public static E(LX/0Yi;)Z
    .locals 3

    .prologue
    .line 82065
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0040

    const-string v2, "NNFPullToRefreshNetworkTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static G(LX/0Yi;)Z
    .locals 3

    .prologue
    .line 82066
    invoke-static {p0}, LX/0Yi;->H(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0020

    const-string v2, "NNFColdStartAndRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa003c

    const-string v2, "NNFColdStartChromeLoadTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0013

    const-string v2, "NNFWarmStartTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0023

    const-string v2, "NNFWarmStartAndRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0024

    const-string v2, "NNFWarmStartAndFreshRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0025

    const-string v2, "NNFWarmStartAndCachedRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa002b

    const-string v2, "NNFHotStartAndRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x230013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static H(LX/0Yi;)Z
    .locals 1

    .prologue
    .line 82067
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0}, LX/0Yl;->d()Z

    move-result v0

    return v0
.end method

.method private static I(LX/0Yi;)V
    .locals 8

    .prologue
    const v3, 0xa0048

    const v2, 0xa0016

    .line 82068
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNFColdStartTTI"

    invoke-interface {v0, v2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0Yi;->Q(LX/0Yi;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82069
    const-string v0, "com.facebook.common.classmarkers.IsColdStartTTIRun"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 82070
    :cond_0
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNFCold_DataFetchedToFirstRender"

    invoke-interface {v0, v3, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82071
    const-string v0, "NNFCold_DataFetchedToFirstRender"

    invoke-static {p0, v3, v0}, LX/0Yi;->c(LX/0Yi;ILjava/lang/String;)V

    .line 82072
    :cond_1
    invoke-static {p0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 82073
    :goto_0
    new-instance v0, LX/0Yj;

    const-string v1, "NNFColdStartTTI"

    invoke-direct {v0, v2, v1}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const-string v1, "coldStartFinishReason"

    .line 82074
    sget-object v2, LX/2uP;->a:[I

    iget-object v3, p0, LX/0Yi;->z:LX/0ao;

    invoke-virtual {v3}, LX/0ao;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 82075
    const-string v2, "UK"

    :goto_1
    move-object v2, v2

    .line 82076
    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-static {p0, v0}, LX/0Yi;->a(LX/0Yi;LX/0Yj;)V

    .line 82077
    return-void

    .line 82078
    :cond_2
    new-instance v4, LX/0Yj;

    const v5, 0xa00ad

    const-string v6, "NNFColdFeedUnitAdapterInitTotal"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 82079
    iget-object v5, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v5, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 82080
    iget-object v5, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-wide v6, p0, LX/0Yi;->G:J

    long-to-double v6, v6

    invoke-interface {v5, v4, v6, v7}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;D)V

    .line 82081
    iget-object v5, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v5, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 82082
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/0Yi;->G:J

    goto :goto_0

    .line 82083
    :pswitch_0
    const-string v2, "D_Fresh"

    goto :goto_1

    .line 82084
    :pswitch_1
    const-string v2, "D_Cached"

    goto :goto_1

    .line 82085
    :pswitch_2
    const-string v2, "A_CachedUnseen"

    goto :goto_1

    .line 82086
    :pswitch_3
    const-string v2, "A_FreshSeenCache"

    goto :goto_1

    .line 82087
    :pswitch_4
    const-string v2, "A_FreshNoCache"

    goto :goto_1

    .line 82088
    :pswitch_5
    const-string v2, "A_FreshFastNetwork"

    goto :goto_1

    .line 82089
    :pswitch_6
    const-string v2, "F_Network"

    goto :goto_1

    .line 82090
    :pswitch_7
    const-string v2, "F_Cache"

    goto :goto_1

    .line 82091
    :pswitch_8
    const-string v2, "F_Cache_Unread"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static K(LX/0Yi;)Z
    .locals 3

    .prologue
    .line 82092
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0020

    const-string v2, "NNFColdStartAndRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static L(LX/0Yi;)Z
    .locals 2

    .prologue
    .line 82093
    iget-boolean v0, p0, LX/0Yi;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->m:LX/0Uo;

    .line 82094
    iget-boolean v1, v0, LX/0Uo;->U:Z

    move v0, v1

    .line 82095
    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->m:LX/0Uo;

    .line 82096
    iget-boolean v1, v0, LX/0Uo;->V:Z

    move v0, v1

    .line 82097
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static N(LX/0Yi;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82098
    iget-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82099
    :goto_0
    return-void

    .line 82100
    :cond_0
    iput-boolean v1, p0, LX/0Yi;->J:Z

    .line 82101
    iput-boolean v1, p0, LX/0Yi;->K:Z

    .line 82102
    invoke-static {}, LX/1gM;->loadIsNotColdStartRunMarker()V

    goto :goto_0
.end method

.method public static P(LX/0Yi;)V
    .locals 1

    .prologue
    .line 82103
    iget-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0Yi;->Q(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82104
    :cond_0
    :goto_0
    return-void

    .line 82105
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yi;->K:Z

    .line 82106
    invoke-static {p0}, LX/0Yi;->R(LX/0Yi;)V

    goto :goto_0
.end method

.method public static Q(LX/0Yi;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 82107
    iget-object v1, p0, LX/0Yi;->m:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->k()LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_0

    .line 82108
    const-string v1, "Class List Generation: Cancelling: App Started in background."

    invoke-direct {p0, v1}, LX/0Yi;->a(Ljava/lang/String;)V

    .line 82109
    :goto_0
    return v0

    .line 82110
    :cond_0
    iget-object v1, p0, LX/0Yi;->m:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82111
    const-string v1, "Class List Generation: Cancelling: App is in the background."

    invoke-direct {p0, v1}, LX/0Yi;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 82112
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static R(LX/0Yi;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82113
    iget-boolean v0, p0, LX/0Yi;->J:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/0Yi;->K:Z

    if-nez v0, :cond_1

    .line 82114
    :cond_0
    :goto_0
    return-void

    .line 82115
    :cond_1
    iget-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82116
    const-string v0, "com.facebook.common.classmarkers.IsColdStartRun"

    invoke-static {v0}, LX/1gM;->loadClass(Ljava/lang/String;)V

    .line 82117
    iput-boolean v1, p0, LX/0Yi;->J:Z

    .line 82118
    iput-boolean v1, p0, LX/0Yi;->K:Z

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0Yi;
    .locals 14

    .prologue
    .line 82119
    sget-object v0, LX/0Yi;->N:LX/0Yi;

    if-nez v0, :cond_1

    .line 82120
    const-class v1, LX/0Yi;

    monitor-enter v1

    .line 82121
    :try_start_0
    sget-object v0, LX/0Yi;->N:LX/0Yi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 82122
    if-eqz v2, :cond_0

    .line 82123
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 82124
    new-instance v3, LX/0Yi;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v5

    check-cast v5, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Yl;->b(LX/0QB;)LX/0Yl;

    move-result-object v6

    check-cast v6, LX/0Yl;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    const/16 v8, 0x2ca

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v9

    check-cast v9, LX/0Uo;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    invoke-static {v0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v11

    check-cast v11, LX/0Ym;

    invoke-static {v0}, LX/0Sk;->a(LX/0QB;)LX/0Sk;

    move-result-object v12

    check-cast v12, LX/0Sk;

    invoke-static {v0}, LX/0YP;->a(LX/0QB;)LX/0YP;

    move-result-object v13

    check-cast v13, LX/0YP;

    invoke-direct/range {v3 .. v13}, LX/0Yi;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Yl;LX/0So;LX/0Ot;LX/0Uo;LX/0TD;LX/0Ym;LX/0Sk;LX/0YP;)V

    .line 82125
    move-object v0, v3

    .line 82126
    sput-object v0, LX/0Yi;->N:LX/0Yi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82127
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 82128
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82129
    :cond_1
    sget-object v0, LX/0Yi;->N:LX/0Yi;

    return-object v0

    .line 82130
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 82131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0Yi;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 82132
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 82133
    return-void
.end method

.method public static a(LX/0Yi;ILjava/lang/String;Z)V
    .locals 5

    .prologue
    .line 82134
    const/4 v0, 0x0

    .line 82135
    new-instance v1, LX/0Yj;

    invoke-direct {v1, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {}, LX/0Yi;->v()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 82136
    iput-boolean v0, v1, LX/0Yj;->n:Z

    .line 82137
    move-object v1, v1

    .line 82138
    if-eqz p3, :cond_0

    .line 82139
    invoke-virtual {v1}, LX/0Yj;->b()LX/0Yj;

    move-result-object v1

    .line 82140
    :cond_0
    iget-object v2, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 82141
    return-void
.end method

.method public static a(LX/0Yi;IZ)V
    .locals 7

    .prologue
    .line 82000
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x0

    iget-object v0, p0, LX/0Yi;->k:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move v2, p1

    move v6, p2

    invoke-interface/range {v1 .. v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIJZ)V

    .line 82001
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {}, LX/0Yi;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 82002
    return-void
.end method

.method private static a(LX/0Yi;LX/0Yj;)V
    .locals 1

    .prologue
    .line 82003
    invoke-static {p0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82004
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    .line 82005
    iget-boolean p0, v0, LX/0Yl;->d:Z

    move p0, p0

    .line 82006
    if-nez p0, :cond_1

    .line 82007
    iget-object p0, v0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {p0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 82008
    :goto_0
    return-void

    .line 82009
    :cond_0
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    .line 82010
    iget-object p0, v0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {p0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 82011
    goto :goto_0

    .line 82012
    :cond_1
    invoke-virtual {v0}, LX/0Yl;->d()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 82013
    iget-object p0, v0, LX/0Yl;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {p0, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 82014
    :cond_2
    goto :goto_0
.end method

.method public static a(LX/0Yi;LX/0ao;)V
    .locals 2

    .prologue
    .line 81837
    iget-object v0, p0, LX/0Yi;->z:LX/0ao;

    sget-object v1, LX/0ao;->NOT_SET:LX/0ao;

    if-ne v0, v1, :cond_0

    .line 81838
    iput-object p1, p0, LX/0Yi;->z:LX/0ao;

    .line 81839
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81840
    iget-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81841
    :goto_0
    return-void

    .line 81842
    :cond_0
    invoke-static {}, LX/1gM;->loadIsNotColdStartRunMarker()V

    goto :goto_0
.end method

.method private b(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 81998
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p1, p2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 81999
    return-void
.end method

.method public static b(LX/0Yi;Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)V
    .locals 4

    .prologue
    .line 81843
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81844
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa0074

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NNFCold"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yl;->c(ILjava/lang/String;)LX/0Yl;

    .line 81845
    :goto_0
    return-void

    .line 81846
    :cond_0
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81847
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa0075

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NNFWarm"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Yl;->f(ILjava/lang/String;)LX/0Yl;

    goto :goto_0

    .line 81848
    :cond_1
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0, p2}, LX/0Yi;->g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81849
    iget-object v0, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81850
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFNavigate"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81851
    const v0, 0xa0076

    .line 81852
    :goto_1
    iget-object v2, p0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v2, v0, v1}, LX/0Yl;->i(ILjava/lang/String;)LX/0Yl;

    goto :goto_0

    .line 81853
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFNavigateOther"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81854
    const v0, 0xa0077

    goto :goto_1

    .line 81855
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFOther"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81856
    const v0, 0xa0078

    goto :goto_1
.end method

.method public static c(LX/0Yi;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 81857
    invoke-static {p0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81858
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0, p1, p2}, LX/0Yl;->e(ILjava/lang/String;)LX/0Yl;

    .line 81859
    :goto_0
    return-void

    .line 81860
    :cond_0
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    invoke-virtual {v0, p1, p2}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    goto :goto_0
.end method

.method public static c(LX/0Yi;J)V
    .locals 7

    .prologue
    .line 81861
    const/4 v0, 0x1

    .line 81862
    iput-boolean v0, p0, LX/0Yi;->q:Z

    .line 81863
    iput-boolean v0, p0, LX/0Yi;->r:Z

    .line 81864
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0Yi;->s:Z

    .line 81865
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa009b

    const-string v2, "NNFColdLoadInlineComposerAfterLoggedIn"

    invoke-virtual {v0, v1, v2, p1, p2}, LX/0Yl;->a(ILjava/lang/String;J)LX/0Yl;

    .line 81866
    invoke-static {p0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->m:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->k()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 81867
    invoke-static {p0}, LX/0Yi;->C(LX/0Yi;)V

    .line 81868
    :cond_0
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81869
    invoke-static {}, LX/00q;->a()LX/00q;

    move-result-object v0

    .line 81870
    iget-object v1, p0, LX/0Yi;->k:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/0Yi;->E:J

    .line 81871
    iget-object v1, p0, LX/0Yi;->j:LX/0Yl;

    const/16 v2, 0x8

    new-array v2, v2, [LX/0Yj;

    const/4 v3, 0x0

    new-instance v4, LX/0Yj;

    const v5, 0xa0013

    const-string v6, "NNFWarmStartTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, LX/0Yj;

    const v5, 0xa0023

    const-string v6, "NNFWarmStartAndRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, LX/0Yj;

    const v5, 0xa0024

    const-string v6, "NNFWarmStartAndFreshRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, LX/0Yj;

    const v5, 0xa0025

    const-string v6, "NNFWarmStartAndCachedRenderTime"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, LX/0Yj;

    const v5, 0xa0004

    const-string v6, "NNFWarmStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, LX/0Yj;

    const v5, 0xa0014

    const-string v6, "NNFFreshContentStart"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, LX/0Yj;

    const v5, 0xa009c

    const-string v6, "NNFWarmLoadInlineComposerAfterLoggedIn"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, LX/0Yj;

    const v5, 0xa00a7

    const-string v6, "NNFInspirationCameraWarmTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, p1, p2, v0}, LX/0Yl;->a(Ljava/util/List;JLX/00q;)LX/0Yl;

    .line 81872
    :cond_1
    return-void
.end method

.method private static g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I
    .locals 1

    .prologue
    .line 81873
    iget-object v0, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0xa003d

    :goto_0
    return v0

    :cond_0
    const v0, 0xa0044

    goto :goto_0
.end method

.method public static h(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)V
    .locals 2

    .prologue
    .line 81874
    iget-boolean v0, p0, LX/0Yi;->M:Z

    if-eqz v0, :cond_0

    .line 81875
    :goto_0
    return-void

    .line 81876
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yi;->M:Z

    .line 81877
    invoke-static {p0, p1}, LX/0Yi;->g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/0Yi;->a(LX/0Yi;IZ)V

    goto :goto_0
.end method

.method public static v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81878
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static x(LX/0Yi;)Z
    .locals 2

    .prologue
    .line 81879
    iget-boolean v0, p0, LX/0Yi;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0Yi;->z:LX/0ao;

    sget-object v1, LX/0ao;->ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 81880
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81881
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFCold"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81882
    :goto_0
    return-object v0

    .line 81883
    :cond_0
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81884
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFWarm"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81885
    :cond_1
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0, p2}, LX/0Yi;->g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81886
    iget-object v0, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81887
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFNavigate"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81888
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFNavigateOther"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81889
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NNFOther"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 81890
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    .line 81891
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Yl;->d:Z

    .line 81892
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    .line 81893
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Yl;->e:Z

    .line 81894
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    .line 81895
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/0Yl;->f:Z

    .line 81896
    invoke-static {p0}, LX/0Yi;->N(LX/0Yi;)V

    .line 81897
    return-void
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 3

    .prologue
    .line 81898
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v0, v0

    .line 81899
    iget-object v1, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v1}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    .line 81900
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 81901
    if-ne v1, v2, :cond_0

    sget-object v1, LX/0rU;->CHUNKED_INITIAL:LX/0rU;

    invoke-virtual {v0, v1}, LX/0rU;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81902
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 81903
    invoke-virtual {v0}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81904
    const v0, 0xa0055

    const-string v1, "NNFPullToRefreshBeforeExecuteTime"

    invoke-direct {p0, v0, v1}, LX/0Yi;->b(ILjava/lang/String;)V

    .line 81905
    :cond_0
    :goto_0
    return-void

    .line 81906
    :cond_1
    iget-object v0, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v0

    const-string v1, "TimeToFeedFetchExecuteFromTrigger"

    iget-object v2, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v2}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/0Yi;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/api/feedtype/FeedType;I)Z
    .locals 13

    .prologue
    const v11, 0xa0023

    const/4 v4, 0x0

    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 81907
    iget-object v0, p0, LX/0Yi;->k:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    .line 81908
    iget-object v0, p0, LX/0Yi;->I:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0Yi;->Q(LX/0Yi;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 81909
    :cond_0
    :goto_0
    const-string v0, "FragmentResumeToRender"

    invoke-static {p0, v0, p1}, LX/0Yi;->b(LX/0Yi;Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)V

    .line 81910
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNFWarmStartAndRenderTime"

    invoke-interface {v0, v11, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81911
    iget-object v1, p0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa0046

    const-string v3, "NNFWarm_DataFetchedToFirstRender"

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 81912
    iget-object v1, p0, LX/0Yi;->j:LX/0Yl;

    const v2, 0xa0004

    const-string v3, "NNFWarmStart"

    move-object v5, v4

    invoke-virtual/range {v1 .. v7}, LX/0Yl;->c(ILjava/lang/String;Ljava/lang/String;LX/0P1;J)LX/0Yl;

    .line 81913
    iget-boolean v0, p0, LX/0Yi;->F:Z

    if-nez v0, :cond_5

    .line 81914
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa0013

    const-string v2, "NNFWarmStartTTI"

    invoke-virtual {v0, v1, v2, v6, v7}, LX/0Yl;->c(ILjava/lang/String;J)LX/0Yl;

    .line 81915
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const-string v1, "NNFWarmStartAndRenderTime"

    invoke-virtual {v0, v11, v1, v6, v7}, LX/0Yl;->c(ILjava/lang/String;J)LX/0Yl;

    .line 81916
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa0025

    const-string v2, "NNFWarmStartAndCachedRenderTime"

    invoke-virtual {v0, v1, v2, v6, v7}, LX/0Yl;->c(ILjava/lang/String;J)LX/0Yl;

    .line 81917
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0024

    const-string v2, "NNFWarmStartAndFreshRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    :cond_1
    :goto_1
    move v0, v9

    .line 81918
    :goto_2
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x230013

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/0Yi;->s:Z

    if-nez v1, :cond_2

    if-lez p2, :cond_3

    .line 81919
    :cond_2
    sget-object v0, LX/2uP;->a:[I

    iget-object v1, p0, LX/0Yi;->z:LX/0ao;

    invoke-virtual {v1}, LX/0ao;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v0, v10

    .line 81920
    :goto_3
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x230013

    iget-object v3, p0, LX/0Yi;->z:LX/0ao;

    invoke-virtual {v3}, LX/0ao;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 81921
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x230013

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    move v0, v9

    .line 81922
    :cond_3
    iput-boolean v8, p0, LX/0Yi;->M:Z

    .line 81923
    invoke-static {p0, p1}, LX/0Yi;->g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I

    move-result v1

    .line 81924
    iget-object v2, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 81925
    iget-object v2, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v1, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 81926
    :cond_4
    :goto_4
    if-nez v0, :cond_11

    invoke-static {p0}, LX/0Yi;->G(LX/0Yi;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 81927
    :goto_5
    return v9

    .line 81928
    :cond_5
    iget-boolean v0, p0, LX/0Yi;->s:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, LX/0Yi;->r:Z

    if-eqz v0, :cond_12

    .line 81929
    iput-boolean v8, p0, LX/0Yi;->r:Z

    .line 81930
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const-string v1, "NNFWarmStartAndRenderTime"

    invoke-virtual {v0, v11, v1, v6, v7}, LX/0Yl;->c(ILjava/lang/String;J)LX/0Yl;

    .line 81931
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa0024

    const-string v2, "NNFWarmStartAndFreshRenderTime"

    invoke-virtual {v0, v1, v2, v6, v7}, LX/0Yl;->c(ILjava/lang/String;J)LX/0Yl;

    .line 81932
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0025

    const-string v2, "NNFWarmStartAndCachedRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 81933
    :cond_6
    invoke-static {p0}, LX/0Yi;->K(LX/0Yi;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 81934
    invoke-static {p0}, LX/0Yi;->L(LX/0Yi;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 81935
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa003a

    const-string v2, "NNFFirstRunColdStart"

    invoke-virtual {v0, v1, v2, v6, v7}, LX/0Yl;->b(ILjava/lang/String;J)LX/0Yl;

    .line 81936
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 81937
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0016

    const-string v2, "NNFColdStartTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 81938
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa004d

    const-string v2, "NNFColdStartNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 81939
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0020

    const-string v2, "NNFColdStartAndRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 81940
    :cond_7
    :goto_6
    iget-boolean v0, p0, LX/0Yi;->s:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, LX/0Yi;->r:Z

    if-eqz v0, :cond_12

    if-lez p2, :cond_12

    .line 81941
    iput-boolean v8, p0, LX/0Yi;->r:Z

    .line 81942
    new-instance v0, LX/0Yj;

    const v1, 0xa0020

    const-string v2, "NNFColdStartAndRenderTime"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 81943
    iput-wide v6, v0, LX/0Yj;->h:J

    .line 81944
    move-object v0, v0

    .line 81945
    iget-boolean v1, p0, LX/0Yi;->L:Z

    if-eqz v1, :cond_e

    .line 81946
    const-string v1, "networkFetchIsCritPath"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    .line 81947
    :goto_7
    invoke-static {p0, v0}, LX/0Yi;->a(LX/0Yi;LX/0Yj;)V

    .line 81948
    iget-boolean v0, p0, LX/0Yi;->y:Z

    if-nez v0, :cond_1

    .line 81949
    invoke-static {p0}, LX/0Yi;->I(LX/0Yi;)V

    goto/16 :goto_1

    .line 81950
    :cond_8
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa003a

    const-string v2, "NNFFirstRunColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 81951
    if-lez p2, :cond_9

    .line 81952
    new-instance v0, LX/0Yj;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    .line 81953
    iput-wide v6, v0, LX/0Yj;->h:J

    .line 81954
    move-object v0, v0

    .line 81955
    iget-boolean v1, p0, LX/0Yi;->y:Z

    if-eqz v1, :cond_b

    .line 81956
    const-string v1, "feedFreshWithoutFetch"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    .line 81957
    :goto_8
    invoke-static {p0, v0}, LX/0Yi;->a(LX/0Yi;LX/0Yj;)V

    .line 81958
    :cond_9
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0084

    const-string v2, "NNFCold_Network"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 81959
    iput-boolean v9, p0, LX/0Yi;->L:Z

    .line 81960
    :cond_a
    iget-boolean v0, p0, LX/0Yi;->y:Z

    if-eqz v0, :cond_c

    .line 81961
    sget-object v0, LX/0ao;->DEFAULT_CACHED_FRESH_WITHOUT_NETWORK_FETCH:LX/0ao;

    invoke-static {p0, v0}, LX/0Yi;->a(LX/0Yi;LX/0ao;)V

    .line 81962
    if-lez p2, :cond_7

    .line 81963
    invoke-static {p0}, LX/0Yi;->I(LX/0Yi;)V

    goto :goto_6

    .line 81964
    :cond_b
    const-string v1, "feedFreshWithoutFetch"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    goto :goto_8

    .line 81965
    :cond_c
    iget-object v0, p0, LX/0Yi;->z:LX/0ao;

    sget-object v1, LX/0ao;->ASYNC_CACHE_UNSEEN_STORY:LX/0ao;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, LX/0Yi;->z:LX/0ao;

    sget-object v1, LX/0ao;->FRESH_FEED_CACHE_UNREAD:LX/0ao;

    if-ne v0, v1, :cond_7

    .line 81966
    :cond_d
    if-lez p2, :cond_7

    .line 81967
    invoke-static {p0}, LX/0Yi;->I(LX/0Yi;)V

    goto/16 :goto_6

    .line 81968
    :cond_e
    const-string v1, "networkFetchIsCritPath"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    goto :goto_7

    .line 81969
    :pswitch_0
    const/16 v0, 0x1b

    .line 81970
    goto/16 :goto_3

    .line 81971
    :pswitch_1
    const/16 v0, 0x19

    goto/16 :goto_3

    .line 81972
    :cond_f
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xa0035

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 81973
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa0035

    invoke-interface {v0, v1, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    move v0, v9

    .line 81974
    goto/16 :goto_4

    .line 81975
    :cond_10
    iget-object v1, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xa0036

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 81976
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xa0036

    invoke-interface {v0, v1, v10}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    move v0, v9

    .line 81977
    goto/16 :goto_4

    :cond_11
    move v9, v0

    goto/16 :goto_5

    :cond_12
    move v0, v8

    goto/16 :goto_2

    .line 81978
    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yi;->J:Z

    .line 81979
    invoke-static {p0}, LX/0Yi;->R(LX/0Yi;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81980
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Yi;->M:Z

    .line 81981
    if-eqz p1, :cond_0

    .line 81982
    const v0, 0xa0036

    invoke-static {p0, v0, v1}, LX/0Yi;->a(LX/0Yi;IZ)V

    .line 81983
    :goto_0
    return-void

    .line 81984
    :cond_0
    const v0, 0xa0035

    invoke-static {p0, v0, v1}, LX/0Yi;->a(LX/0Yi;IZ)V

    goto :goto_0
.end method

.method public final f(Lcom/facebook/api/feedtype/FeedType;)I
    .locals 3

    .prologue
    .line 81985
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81986
    const v0, 0xa0074

    .line 81987
    :goto_0
    return v0

    .line 81988
    :cond_0
    iget-object v0, p0, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0004

    const-string v2, "NNFWarmStart"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81989
    const v0, 0xa0075

    goto :goto_0

    .line 81990
    :cond_1
    iget-object v0, p0, LX/0Yi;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0, p1}, LX/0Yi;->g(LX/0Yi;Lcom/facebook/api/feedtype/FeedType;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81991
    iget-object v0, p0, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81992
    const v0, 0xa0076

    goto :goto_0

    .line 81993
    :cond_2
    const v0, 0xa0077

    goto :goto_0

    .line 81994
    :cond_3
    const v0, 0xa0078

    goto :goto_0
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 81995
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa00a6

    const-string v2, "NNFInspirationCameraColdTTI"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 81996
    iget-object v0, p0, LX/0Yi;->j:LX/0Yl;

    const v1, 0xa00a7

    const-string v2, "NNFInspirationCameraWarmTTI"

    invoke-virtual {v0, v1, v2}, LX/0Yl;->j(ILjava/lang/String;)LX/0Yl;

    .line 81997
    return-void
.end method
