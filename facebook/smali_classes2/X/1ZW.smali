.class public final LX/1ZW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field public j:I

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274781
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ZW;->a:Z

    .line 274782
    iput v1, p0, LX/1ZW;->h:I

    .line 274783
    iput-boolean v1, p0, LX/1ZW;->i:Z

    .line 274784
    const/4 v0, 0x0

    iput-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    return-void
.end method

.method public static a(LX/1ZW;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 274763
    iget-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 274764
    const/4 v3, 0x0

    .line 274765
    const v1, 0x7fffffff

    .line 274766
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 274767
    iget-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    .line 274768
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 274769
    if-eq v2, p1, :cond_3

    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v6

    if-nez v6, :cond_3

    .line 274770
    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v0

    iget v6, p0, LX/1ZW;->d:I

    sub-int/2addr v0, v6

    iget v6, p0, LX/1ZW;->e:I

    mul-int/2addr v0, v6

    .line 274771
    if-ltz v0, :cond_3

    .line 274772
    if-ge v0, v1, :cond_3

    .line 274773
    if-eqz v0, :cond_1

    move-object v1, v2

    .line 274774
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v2, v3

    .line 274775
    :cond_1
    move-object v0, v2

    .line 274776
    if-nez v0, :cond_2

    .line 274777
    const/4 v0, -0x1

    iput v0, p0, LX/1ZW;->d:I

    .line 274778
    :goto_2
    return-void

    .line 274779
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v0

    iput v0, p0, LX/1ZW;->d:I

    goto :goto_2

    :cond_3
    move v0, v1

    move-object v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1Od;)Landroid/view/View;
    .locals 4

    .prologue
    .line 274785
    iget-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 274786
    iget-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 274787
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 274788
    iget-object v0, p0, LX/1ZW;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    .line 274789
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 274790
    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result p1

    if-nez p1, :cond_1

    .line 274791
    iget p1, p0, LX/1ZW;->d:I

    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 274792
    invoke-static {p0, v1}, LX/1ZW;->a(LX/1ZW;Landroid/view/View;)V

    move-object v0, v1

    .line 274793
    :goto_1
    move-object v0, v0

    .line 274794
    :goto_2
    return-object v0

    .line 274795
    :cond_0
    iget v0, p0, LX/1ZW;->d:I

    invoke-virtual {p1, v0}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v0

    .line 274796
    iget v1, p0, LX/1ZW;->d:I

    iget v2, p0, LX/1ZW;->e:I

    add-int/2addr v1, v2

    iput v1, p0, LX/1ZW;->d:I

    goto :goto_2

    .line 274797
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 274798
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 274761
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1ZW;->a(LX/1ZW;Landroid/view/View;)V

    .line 274762
    return-void
.end method

.method public final a(LX/1Ok;)Z
    .locals 2

    .prologue
    .line 274760
    iget v0, p0, LX/1ZW;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/1ZW;->d:I

    invoke-virtual {p1}, LX/1Ok;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
