.class public LX/1Vu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Vu;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266792
    iput-object p1, p0, LX/1Vu;->b:LX/0Uh;

    .line 266793
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1Vu;->a:Ljava/util/Map;

    .line 266794
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vu;
    .locals 4

    .prologue
    .line 266795
    sget-object v0, LX/1Vu;->c:LX/1Vu;

    if-nez v0, :cond_1

    .line 266796
    const-class v1, LX/1Vu;

    monitor-enter v1

    .line 266797
    :try_start_0
    sget-object v0, LX/1Vu;->c:LX/1Vu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 266798
    if-eqz v2, :cond_0

    .line 266799
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 266800
    new-instance p0, LX/1Vu;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/1Vu;-><init>(LX/0Uh;)V

    .line 266801
    move-object v0, p0

    .line 266802
    sput-object v0, LX/1Vu;->c:LX/1Vu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266803
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 266804
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 266805
    :cond_1
    sget-object v0, LX/1Vu;->c:LX/1Vu;

    return-object v0

    .line 266806
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 266807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 266808
    iget-object v1, p0, LX/1Vu;->a:Ljava/util/Map;

    .line 266809
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266810
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266811
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;"
        }
    .end annotation

    .prologue
    .line 266812
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266813
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 266814
    iget-object v1, p0, LX/1Vu;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266815
    iget-object v1, p0, LX/1Vu;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 266816
    :goto_0
    return-object v0

    .line 266817
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 266818
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->bl()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v0

    goto :goto_0
.end method
