.class public LX/0eq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0eq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 104228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104229
    return-void
.end method

.method public static a(Ljava/io/DataInputStream;Landroid/util/SparseArray;II)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInputStream;",
            "Landroid/util/SparseArray",
            "<",
            "LX/25o;",
            ">;II)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 104218
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 104219
    new-array v3, v2, [I

    .line 104220
    new-array v4, v2, [I

    move v1, v0

    .line 104221
    :goto_0
    if-ge v0, v2, :cond_0

    .line 104222
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v5

    aput v5, v3, v0

    .line 104223
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    move-result v5

    aput v5, v4, v0

    .line 104224
    aget v5, v4, v0

    add-int/2addr v1, v5

    .line 104225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104226
    :cond_0
    new-instance v0, LX/25o;

    invoke-direct {v0, p3, v3, v4}, LX/25o;-><init>(I[I[I)V

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 104227
    return v1
.end method

.method public static a(LX/0QB;)LX/0eq;
    .locals 3

    .prologue
    .line 104230
    sget-object v0, LX/0eq;->a:LX/0eq;

    if-nez v0, :cond_1

    .line 104231
    const-class v1, LX/0eq;

    monitor-enter v1

    .line 104232
    :try_start_0
    sget-object v0, LX/0eq;->a:LX/0eq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 104233
    if-eqz v2, :cond_0

    .line 104234
    :try_start_1
    new-instance v0, LX/0eq;

    invoke-direct {v0}, LX/0eq;-><init>()V

    .line 104235
    move-object v0, v0

    .line 104236
    sput-object v0, LX/0eq;->a:LX/0eq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104237
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 104238
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104239
    :cond_1
    sget-object v0, LX/0eq;->a:LX/0eq;

    return-object v0

    .line 104240
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 104241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(Ljava/io/DataInputStream;)LX/25u;
    .locals 15

    .prologue
    const/4 v1, 0x0

    .line 104190
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 104191
    if-nez v5, :cond_0

    .line 104192
    sget-object v0, LX/25u;->a:LX/25u;

    .line 104193
    :goto_0
    return-object v0

    .line 104194
    :cond_0
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6, v5}, Landroid/util/SparseArray;-><init>(I)V

    .line 104195
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    move v3, v1

    move v2, v0

    move v0, v1

    .line 104196
    :goto_1
    if-ge v3, v5, :cond_3

    .line 104197
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    move-result v4

    add-int/2addr v4, v2

    .line 104198
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v7

    .line 104199
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8, v7}, Landroid/util/SparseArray;-><init>(I)V

    move v2, v0

    move v0, v1

    .line 104200
    :goto_2
    if-ge v0, v7, :cond_2

    .line 104201
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v9

    const/4 v10, 0x0

    .line 104202
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v12

    .line 104203
    new-array v13, v12, [I

    move v11, v10

    .line 104204
    :goto_3
    if-ge v10, v12, :cond_1

    .line 104205
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readShort()S

    move-result v14

    aput v14, v13, v10

    .line 104206
    aget v14, v13, v10

    add-int/2addr v11, v14

    .line 104207
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 104208
    :cond_1
    new-instance v10, LX/25s;

    invoke-direct {v10, v2, v13}, LX/25s;-><init>(I[I)V

    invoke-virtual {v8, v9, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 104209
    move v9, v11

    .line 104210
    add-int/2addr v2, v9

    .line 104211
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 104212
    :cond_2
    new-instance v0, LX/25t;

    invoke-direct {v0, v8}, LX/25t;-><init>(Landroid/util/SparseArray;)V

    invoke-virtual {v6, v4, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 104213
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    move v2, v4

    goto :goto_1

    .line 104214
    :cond_3
    new-array v1, v0, [B

    .line 104215
    const-string v2, "string-array"

    invoke-static {p0, v1, v2}, LX/25l;->a(Ljava/io/InputStream;[BLjava/lang/String;)V

    .line 104216
    new-instance v2, LX/25u;

    invoke-direct {v2, v6, v1}, LX/25u;-><init>(Landroid/util/SparseArray;[B)V

    move-object v0, v2

    .line 104217
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;)LX/0lA;
    .locals 13

    .prologue
    .line 104139
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    const/high16 v2, 0x10000

    invoke-direct {v1, p1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 104140
    :try_start_0
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 104141
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 104142
    new-instance v0, LX/313;

    invoke-direct {v0, v1}, LX/313;-><init>(I)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104143
    :catch_0
    move-exception v0

    .line 104144
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 104145
    :cond_0
    :try_start_1
    new-instance v1, LX/0lA;

    new-instance v2, LX/25j;

    const/4 v4, 0x0

    .line 104146
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    .line 104147
    if-nez v9, :cond_1

    .line 104148
    sget-object v3, LX/25m;->a:LX/25m;

    .line 104149
    :goto_0
    move-object v3, v3

    .line 104150
    const/4 v5, 0x0

    .line 104151
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    .line 104152
    if-nez v9, :cond_4

    .line 104153
    sget-object v4, LX/25q;->a:LX/25q;

    .line 104154
    :goto_1
    move-object v4, v4

    .line 104155
    invoke-static {v0}, LX/0eq;->c(Ljava/io/DataInputStream;)LX/25u;

    move-result-object v0

    invoke-direct {v2, v3, v4, v0}, LX/25j;-><init>(LX/25m;LX/25q;LX/25u;)V

    invoke-direct {v1, v2}, LX/0lA;-><init>(LX/0hV;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1

    .line 104156
    :cond_1
    :try_start_2
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10, v9}, Landroid/util/SparseArray;-><init>(I)V

    .line 104157
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move v6, v4

    move v7, v4

    .line 104158
    :goto_2
    if-ge v6, v9, :cond_3

    .line 104159
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readShort()S

    move-result v5

    add-int v8, v3, v5

    .line 104160
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result v11

    .line 104161
    new-instance v12, Landroid/util/SparseIntArray;

    invoke-direct {v12, v11}, Landroid/util/SparseIntArray;-><init>(I)V

    move v3, v4

    move v5, v4

    .line 104162
    :goto_3
    if-ge v3, v11, :cond_2

    .line 104163
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result p0

    .line 104164
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readShort()S

    move-result p1

    .line 104165
    invoke-virtual {v12, p0, p1}, Landroid/util/SparseIntArray;->append(II)V

    .line 104166
    add-int/2addr v5, p1

    .line 104167
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 104168
    :cond_2
    new-instance v3, LX/25k;

    invoke-direct {v3, v7, v12}, LX/25k;-><init>(ILandroid/util/SparseIntArray;)V

    invoke-virtual {v10, v8, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 104169
    add-int/2addr v5, v7

    .line 104170
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v7, v5

    move v3, v8

    goto :goto_2

    .line 104171
    :cond_3
    new-array v3, v7, [B

    .line 104172
    const-string v4, "string"

    invoke-static {v0, v3, v4}, LX/25l;->a(Ljava/io/InputStream;[BLjava/lang/String;)V

    .line 104173
    new-instance v4, LX/25m;

    invoke-direct {v4, v10, v3}, LX/25m;-><init>(Landroid/util/SparseArray;[B)V

    move-object v3, v4

    .line 104174
    goto :goto_0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 104175
    :cond_4
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10, v9}, Landroid/util/SparseArray;-><init>(I)V

    .line 104176
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    move v7, v5

    move v6, v4

    move v4, v5

    .line 104177
    :goto_4
    if-ge v7, v9, :cond_6

    .line 104178
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    add-int/2addr v8, v6

    .line 104179
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result v11

    .line 104180
    new-instance v12, Landroid/util/SparseArray;

    invoke-direct {v12, v11}, Landroid/util/SparseArray;-><init>(I)V

    move v6, v4

    move v4, v5

    .line 104181
    :goto_5
    if-ge v4, v11, :cond_5

    .line 104182
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result p0

    invoke-static {v0, v12, p0, v6}, LX/0eq;->a(Ljava/io/DataInputStream;Landroid/util/SparseArray;II)I

    move-result p0

    add-int/2addr v6, p0

    .line 104183
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 104184
    :cond_5
    new-instance v4, LX/25p;

    invoke-direct {v4, v12}, LX/25p;-><init>(Landroid/util/SparseArray;)V

    invoke-virtual {v10, v8, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 104185
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v4, v6

    move v6, v8

    goto :goto_4

    .line 104186
    :cond_6
    new-array v5, v4, [B

    .line 104187
    const-string v6, "plural"

    invoke-static {v0, v5, v6}, LX/25l;->a(Ljava/io/InputStream;[BLjava/lang/String;)V

    .line 104188
    new-instance v6, LX/25q;

    invoke-direct {v6, v10, v5}, LX/25q;-><init>(Landroid/util/SparseArray;[B)V

    move-object v4, v6

    .line 104189
    goto/16 :goto_1
.end method
