.class public LX/0tN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154487
    iput-object p1, p0, LX/0tN;->a:LX/0ad;

    .line 154488
    return-void
.end method

.method public static a(LX/0QB;)LX/0tN;
    .locals 4

    .prologue
    .line 154475
    const-class v1, LX/0tN;

    monitor-enter v1

    .line 154476
    :try_start_0
    sget-object v0, LX/0tN;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 154477
    sput-object v2, LX/0tN;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 154478
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154479
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 154480
    new-instance p0, LX/0tN;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/0tN;-><init>(LX/0ad;)V

    .line 154481
    move-object v0, p0

    .line 154482
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 154483
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0tN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154484
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 154485
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 154472
    iget-object v0, p0, LX/0tN;->a:LX/0ad;

    sget-short v1, LX/0wl;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 154474
    iget-object v0, p0, LX/0tN;->a:LX/0ad;

    sget-short v1, LX/0wl;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 154473
    iget-object v0, p0, LX/0tN;->a:LX/0ad;

    sget-short v1, LX/0wl;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
