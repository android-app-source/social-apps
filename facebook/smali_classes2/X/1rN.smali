.class public final enum LX/1rN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1rN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1rN;

.field public static final enum OFF:LX/1rN;

.field public static final enum ON:LX/1rN;

.field public static final enum UNKNOWN:LX/1rN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 332169
    new-instance v0, LX/1rN;

    const-string v1, "ON"

    invoke-direct {v0, v1, v2}, LX/1rN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rN;->ON:LX/1rN;

    .line 332170
    new-instance v0, LX/1rN;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, LX/1rN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rN;->OFF:LX/1rN;

    .line 332171
    new-instance v0, LX/1rN;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/1rN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1rN;->UNKNOWN:LX/1rN;

    .line 332172
    const/4 v0, 0x3

    new-array v0, v0, [LX/1rN;

    sget-object v1, LX/1rN;->ON:LX/1rN;

    aput-object v1, v0, v2

    sget-object v1, LX/1rN;->OFF:LX/1rN;

    aput-object v1, v0, v3

    sget-object v1, LX/1rN;->UNKNOWN:LX/1rN;

    aput-object v1, v0, v4

    sput-object v0, LX/1rN;->$VALUES:[LX/1rN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 332173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1rN;
    .locals 1

    .prologue
    .line 332174
    const-class v0, LX/1rN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1rN;

    return-object v0
.end method

.method public static values()[LX/1rN;
    .locals 1

    .prologue
    .line 332175
    sget-object v0, LX/1rN;->$VALUES:[LX/1rN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1rN;

    return-object v0
.end method
