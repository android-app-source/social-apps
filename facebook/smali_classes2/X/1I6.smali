.class public abstract LX/1I6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# static fields
.field private static final a:LX/1I6;

.field public static final b:LX/1I6;

.field public static final c:LX/1I6;

.field private static final d:LX/1I6;

.field public static final e:LX/1I6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x3d

    .line 228295
    new-instance v0, LX/1I7;

    const-string v1, "base64()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1I7;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, LX/1I6;->a:LX/1I6;

    .line 228296
    new-instance v0, LX/1I7;

    const-string v1, "base64Url()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1I7;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, LX/1I6;->b:LX/1I6;

    .line 228297
    new-instance v0, LX/1I8;

    const-string v1, "base32()"

    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1I8;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, LX/1I6;->c:LX/1I6;

    .line 228298
    new-instance v0, LX/1I8;

    const-string v1, "base32Hex()"

    const-string v2, "0123456789ABCDEFGHIJKLMNOPQRSTUV"

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/1I8;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V

    sput-object v0, LX/1I6;->d:LX/1I6;

    .line 228299
    new-instance v0, LX/1IU;

    const-string v1, "base16()"

    const-string v2, "0123456789ABCDEF"

    invoke-direct {v0, v1, v2}, LX/1IU;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1I6;->e:LX/1I6;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 228294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([BII)Ljava/lang/String;
    .locals 2

    .prologue
    .line 228288
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    .line 228289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0, p3}, LX/1I6;->a(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 228290
    :try_start_0
    invoke-virtual {p0, v0, p1, p2, p3}, LX/1I6;->a(Ljava/lang/Appendable;[BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228291
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 228292
    :catch_0
    move-exception v0

    .line 228293
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public abstract a(I)I
.end method

.method public abstract a([BLjava/lang/CharSequence;)I
.end method

.method public abstract a()LX/1IA;
.end method

.method public final a([B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 228287
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, LX/1I6;->a([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Appendable;[BII)V
.end method

.method public final a(Ljava/lang/CharSequence;)[B
    .locals 2

    .prologue
    .line 228274
    :try_start_0
    invoke-virtual {p0}, LX/1I6;->a()LX/1IA;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1IA;->trimTrailingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 228275
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, v1}, LX/1I6;->b(I)I

    move-result v1

    new-array v1, v1, [B

    .line 228276
    invoke-virtual {p0, v1, v0}, LX/1I6;->a([BLjava/lang/CharSequence;)I

    move-result v0

    .line 228277
    const/4 p1, 0x0

    .line 228278
    array-length p0, v1

    if-ne v0, p0, :cond_0

    .line 228279
    :goto_0
    move-object v0, v1

    .line 228280
    move-object v0, v0
    :try_end_0
    .catch LX/51z; {:try_start_0 .. :try_end_0} :catch_0

    .line 228281
    return-object v0

    .line 228282
    :catch_0
    move-exception v0

    .line 228283
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 228284
    :cond_0
    new-array p0, v0, [B

    .line 228285
    invoke-static {v1, p1, p0, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v1, p0

    .line 228286
    goto :goto_0
.end method

.method public abstract b(I)I
.end method

.method public abstract b()LX/1I6;
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method

.method public abstract c()LX/1I6;
    .annotation build Ljavax/annotation/CheckReturnValue;
    .end annotation
.end method
