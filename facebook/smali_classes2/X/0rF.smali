.class public LX/0rF;
.super LX/0rB;
.source ""


# instance fields
.field private final a:LX/0Ym;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ym;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0rm;",
            ">;",
            "LX/0Ym;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149165
    sget-object v0, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-direct {p0, v0, p1}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;)V

    .line 149166
    iput-object p2, p0, LX/0rF;->a:LX/0Ym;

    .line 149167
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
    .locals 1

    .prologue
    .line 149152
    const-string v0, "feed_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149153
    if-nez v0, :cond_1

    .line 149154
    iget-object v0, p0, LX/0rF;->a:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    .line 149155
    :cond_0
    :goto_0
    return-object v0

    .line 149156
    :cond_1
    sget-object p0, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 149157
    sget-object p0, Lcom/facebook/api/feedtype/FeedType;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 149158
    :goto_1
    move-object v0, p0

    .line 149159
    if-nez v0, :cond_0

    .line 149160
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_0

    .line 149161
    :cond_2
    sget-object p0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {p0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 149162
    sget-object p0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    goto :goto_1

    .line 149163
    :cond_3
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149164
    const/4 v0, 0x0

    return-object v0
.end method
