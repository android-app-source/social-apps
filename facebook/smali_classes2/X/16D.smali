.class public LX/16D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/16D;


# instance fields
.field private final a:LX/16F;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/16F;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 184660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184661
    iput-object p1, p0, LX/16D;->a:LX/16F;

    .line 184662
    iput-object p2, p0, LX/16D;->b:LX/0ad;

    .line 184663
    return-void
.end method

.method public static a(LX/0QB;)LX/16D;
    .locals 5

    .prologue
    .line 184636
    sget-object v0, LX/16D;->c:LX/16D;

    if-nez v0, :cond_1

    .line 184637
    const-class v1, LX/16D;

    monitor-enter v1

    .line 184638
    :try_start_0
    sget-object v0, LX/16D;->c:LX/16D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 184639
    if-eqz v2, :cond_0

    .line 184640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 184641
    new-instance p0, LX/16D;

    invoke-static {v0}, LX/16F;->a(LX/0QB;)LX/16F;

    move-result-object v3

    check-cast v3, LX/16F;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/16D;-><init>(LX/16F;LX/0ad;)V

    .line 184642
    move-object v0, p0

    .line 184643
    sput-object v0, LX/16D;->c:LX/16D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184644
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 184645
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184646
    :cond_1
    sget-object v0, LX/16D;->c:LX/16D;

    return-object v0

    .line 184647
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 184648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 184658
    const-wide/32 v2, 0x240c8400

    move-wide v0, v2

    .line 184659
    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 184657
    iget-object v0, p0, LX/16D;->a:LX/16F;

    invoke-virtual {v0, p1}, LX/16F;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 184664
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 184652
    iget-object v0, p0, LX/16D;->b:LX/0ad;

    sget-char v1, LX/34q;->e:C

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184653
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184654
    const v0, 0x7f080d34

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 184655
    :cond_0
    iget-object v1, p0, LX/16D;->a:LX/16F;

    invoke-virtual {v1, p1, v0, v2}, LX/16F;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 184656
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 184651
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184650
    const-string v0, "3907"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184649
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_MAIN_TAB_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
