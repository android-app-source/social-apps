.class public LX/0cC;
.super LX/0cD;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 87890
    sget-object v0, LX/0cB;->e:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, LX/0cD;-><init>(Landroid/net/Uri;I)V

    .line 87891
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 87888
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0cC;->b:Z

    .line 87889
    return-void
.end method

.method public final a(Landroid/net/Uri;LX/4gt;)V
    .locals 1

    .prologue
    .line 87883
    sget-object v0, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87884
    iget-boolean v0, p0, LX/0cC;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/4gt;->a:Ljava/lang/Object;

    .line 87885
    iget-boolean v0, p0, LX/0cC;->b:Z

    if-eqz v0, :cond_0

    .line 87886
    const/4 v0, 0x1

    iput-boolean v0, p2, LX/4gt;->b:Z

    .line 87887
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87881
    sget-object v0, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, LX/0cC;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 87882
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/Object;)Z
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 87876
    sget-object v1, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87877
    iget-boolean v1, p0, LX/0cC;->b:Z

    .line 87878
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, LX/0cC;->b:Z

    .line 87879
    iget-boolean v2, p0, LX/0cC;->b:Z

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 87880
    :cond_0
    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87874
    sget-object v0, LX/0cB;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/0cC;->b:Z

    .line 87875
    return-void
.end method
