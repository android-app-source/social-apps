.class public final LX/0kN;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:LX/0kK;


# direct methods
.method public constructor <init>(LX/0kK;)V
    .locals 0

    .prologue
    .line 127094
    iput-object p1, p0, LX/0kN;->a:LX/0kK;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 127102
    instance-of v0, p1, LX/0ew;

    if-eqz v0, :cond_3

    .line 127103
    iget-object v1, p0, LX/0kN;->a:LX/0kK;

    move-object v0, p1

    check-cast v0, LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 127104
    iput-object p1, v1, LX/0kK;->g:Landroid/content/Context;

    .line 127105
    iput-object v0, v1, LX/0kK;->h:LX/0gc;

    .line 127106
    iget-boolean v2, v1, LX/0kK;->e:Z

    if-nez v2, :cond_3

    iget-object v2, v1, LX/0kK;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 127107
    iget-object v2, v1, LX/0kK;->i:LX/6G9;

    if-nez v2, :cond_0

    .line 127108
    new-instance v2, LX/6GA;

    invoke-direct {v2, v1}, LX/6GA;-><init>(LX/0kK;)V

    iput-object v2, v1, LX/0kK;->i:LX/6G9;

    .line 127109
    :cond_0
    iget-object v2, v1, LX/0kK;->j:LX/6GC;

    if-nez v2, :cond_1

    .line 127110
    new-instance v3, LX/6GC;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v2, v1, LX/0kK;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fX;

    iget-object p0, v1, LX/0kK;->i:LX/6G9;

    invoke-direct {v3, v4, v2, p0}, LX/6GC;-><init>(Landroid/content/Context;LX/1fX;Landroid/hardware/SensorEventListener;)V

    iput-object v3, v1, LX/0kK;->j:LX/6GC;

    .line 127111
    :cond_1
    iget-object v2, v1, LX/0kK;->j:LX/6GC;

    .line 127112
    iget-object v3, v2, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    if-nez v3, :cond_2

    .line 127113
    invoke-static {v2}, LX/6GC;->d(LX/6GC;)V

    .line 127114
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/0kK;->e:Z

    .line 127115
    :cond_3
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 127095
    iget-object v0, p0, LX/0kN;->a:LX/0kK;

    const/4 p1, 0x0

    .line 127096
    iget-object p0, v0, LX/0kK;->i:LX/6G9;

    if-eqz p0, :cond_0

    iget-boolean p0, v0, LX/0kK;->e:Z

    if-eqz p0, :cond_0

    .line 127097
    iget-object p0, v0, LX/0kK;->j:LX/6GC;

    invoke-virtual {p0}, LX/6GC;->b()V

    .line 127098
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/0kK;->e:Z

    .line 127099
    :cond_0
    iput-object p1, v0, LX/0kK;->g:Landroid/content/Context;

    .line 127100
    iput-object p1, v0, LX/0kK;->h:LX/0gc;

    .line 127101
    return-void
.end method
