.class public LX/1TB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 249726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249727
    iput-object p1, p0, LX/1TB;->a:LX/0Ot;

    .line 249728
    return-void
.end method

.method public static a(LX/0QB;)LX/1TB;
    .locals 4

    .prologue
    .line 249729
    const-class v1, LX/1TB;

    monitor-enter v1

    .line 249730
    :try_start_0
    sget-object v0, LX/1TB;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 249731
    sput-object v2, LX/1TB;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 249732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 249734
    new-instance v3, LX/1TB;

    const/16 p0, 0x6f6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1TB;-><init>(LX/0Ot;)V

    .line 249735
    move-object v0, v3

    .line 249736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 249737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1TB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 249739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 249740
    const-class v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    iget-object v1, p0, LX/1TB;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 249741
    return-void
.end method
