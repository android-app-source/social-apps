.class public LX/0mj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Z

.field public b:LX/0mh;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:LX/0mq;

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final q:Z

.field public r:LX/0n9;

.field public s:LX/0n9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile t:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 132952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132953
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/0mj;->k:J

    .line 132954
    iput-boolean p1, p0, LX/0mj;->q:Z

    .line 132955
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 132956
    iget-boolean v0, p0, LX/0mj;->a:Z

    if-nez v0, :cond_0

    .line 132957
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "isSampled was not invoked, how can you have known?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132958
    :cond_0
    iget-boolean v0, p0, LX/0mj;->q:Z

    if-nez v0, :cond_1

    .line 132959
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Event is not sampled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132960
    :cond_1
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 132961
    invoke-static {p0}, LX/0mj;->j(LX/0mj;)V

    .line 132962
    iget-object v0, p0, LX/0mj;->r:LX/0n9;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    if-eqz v0, :cond_1

    .line 132963
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call ejectBaseParameters before release"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132964
    :cond_1
    iget-object v0, p0, LX/0mj;->b:LX/0mh;

    iget-object v0, v0, LX/0mh;->a:LX/0Zk;

    .line 132965
    iput-object v1, p0, LX/0mj;->c:Ljava/lang/String;

    .line 132966
    iput-object v1, p0, LX/0mj;->d:Ljava/lang/String;

    .line 132967
    iput-object v1, p0, LX/0mj;->e:LX/0mq;

    .line 132968
    iput-boolean v4, p0, LX/0mj;->f:Z

    .line 132969
    iput-object v1, p0, LX/0mj;->h:Ljava/lang/String;

    .line 132970
    iput-object v1, p0, LX/0mj;->j:Ljava/lang/Boolean;

    .line 132971
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/0mj;->k:J

    .line 132972
    iput-object v1, p0, LX/0mj;->l:Ljava/lang/String;

    .line 132973
    iput-object v1, p0, LX/0mj;->m:Ljava/lang/String;

    .line 132974
    iput-object v1, p0, LX/0mj;->n:Ljava/lang/String;

    .line 132975
    iput-object v1, p0, LX/0mj;->o:Ljava/lang/String;

    .line 132976
    iput-object v1, p0, LX/0mj;->p:Ljava/lang/String;

    .line 132977
    iput-object v1, p0, LX/0mj;->b:LX/0mh;

    .line 132978
    iput-object v1, p0, LX/0mj;->i:Ljava/lang/String;

    .line 132979
    iput-boolean v4, p0, LX/0mj;->a:Z

    .line 132980
    iput-boolean v4, p0, LX/0mj;->g:Z

    .line 132981
    invoke-interface {v0, p0}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 132982
    return-void
.end method

.method public static j(LX/0mj;)V
    .locals 2

    .prologue
    .line 132983
    iget-boolean v0, p0, LX/0mj;->t:Z

    if-eqz v0, :cond_0

    .line 132984
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected immutability"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132985
    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 132986
    iget-boolean v0, p0, LX/0mj;->t:Z

    if-nez v0, :cond_0

    .line 132987
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected mutability"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132988
    :cond_0
    return-void
.end method

.method private static l(LX/0mj;)V
    .locals 0

    .prologue
    .line 132989
    invoke-direct {p0}, LX/0mj;->f()V

    .line 132990
    invoke-direct {p0}, LX/0mj;->k()V

    .line 132991
    return-void
.end method

.method private n()LX/0n9;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132992
    iget-object v0, p0, LX/0mj;->r:LX/0n9;

    .line 132993
    invoke-virtual {v0}, LX/0nA;->f()V

    .line 132994
    iget-object v1, p0, LX/0mj;->s:LX/0n9;

    if-eqz v1, :cond_0

    .line 132995
    iget-object v1, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v1}, LX/0nA;->f()V

    .line 132996
    :cond_0
    iput-object v2, p0, LX/0mj;->s:LX/0n9;

    .line 132997
    iput-object v2, p0, LX/0mj;->r:LX/0n9;

    .line 132998
    return-object v0
.end method

.method private o()LX/0pE;
    .locals 1

    .prologue
    .line 132999
    iget-boolean v0, p0, LX/0mj;->f:Z

    if-eqz v0, :cond_0

    .line 133000
    iget-object v0, p0, LX/0mj;->b:LX/0mh;

    iget-object v0, v0, LX/0mh;->f:LX/0mk;

    invoke-virtual {v0}, LX/0mk;->d()LX/0pE;

    move-result-object v0

    .line 133001
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0mj;->b:LX/0mh;

    iget-object v0, v0, LX/0mh;->f:LX/0mk;

    invoke-virtual {v0}, LX/0mk;->b()LX/0pE;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)LX/0mj;
    .locals 1

    .prologue
    .line 133002
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 133003
    iput-wide p1, p0, LX/0mj;->k:J

    .line 133004
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/0mj;
    .locals 2

    .prologue
    .line 132947
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132948
    if-nez p1, :cond_0

    .line 132949
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "processName cannot be null if specified explicitly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132950
    :cond_0
    iput-object p1, p0, LX/0mj;->h:Ljava/lang/String;

    .line 132951
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0mj;
    .locals 1

    .prologue
    .line 132943
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132944
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    .line 132945
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132946
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Number;)LX/0mj;
    .locals 1

    .prologue
    .line 132824
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132825
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    .line 132826
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132827
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0mj;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132939
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132940
    iput-object p1, p0, LX/0mj;->l:Ljava/lang/String;

    .line 132941
    iput-object p2, p0, LX/0mj;->m:Ljava/lang/String;

    .line 132942
    return-object p0
.end method

.method public final a(LX/0n9;)V
    .locals 3

    .prologue
    .line 132932
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132933
    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    if-eqz v0, :cond_0

    .line 132934
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mExtras has already been set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132935
    :cond_0
    if-eqz p1, :cond_1

    .line 132936
    iput-object p1, p0, LX/0mj;->s:LX/0n9;

    .line 132937
    iget-object v0, p0, LX/0mj;->r:LX/0n9;

    iget-object v1, p0, LX/0mj;->e:LX/0mq;

    invoke-virtual {v1}, LX/0mq;->getExtraJsonKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v0, v1, v2}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 132938
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 132930
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0mj;->a:Z

    .line 132931
    iget-boolean v0, p0, LX/0mj;->q:Z

    return v0
.end method

.method public final b(Ljava/lang/String;)LX/0mj;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132927
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132928
    iput-object p1, p0, LX/0mj;->n:Ljava/lang/String;

    .line 132929
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/0mj;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132923
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132924
    iput-object p1, p0, LX/0mj;->o:Ljava/lang/String;

    .line 132925
    iput-object p2, p0, LX/0mj;->p:Ljava/lang/String;

    .line 132926
    return-object p0
.end method

.method public final c()LX/0n9;
    .locals 3

    .prologue
    .line 132918
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132919
    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    if-nez v0, :cond_0

    .line 132920
    iget-object v0, p0, LX/0mj;->b:LX/0mh;

    iget-object v0, v0, LX/0mh;->g:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    iput-object v0, p0, LX/0mj;->s:LX/0n9;

    .line 132921
    iget-object v0, p0, LX/0mj;->r:LX/0n9;

    iget-object v1, p0, LX/0mj;->e:LX/0mq;

    invoke-virtual {v1}, LX/0mq;->getExtraJsonKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v0, v1, v2}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 132922
    :cond_0
    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    return-object v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)LX/0mj;
    .locals 1

    .prologue
    .line 132914
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132915
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v0

    .line 132916
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132917
    return-object p0
.end method

.method public final e()V
    .locals 9

    .prologue
    .line 132828
    invoke-static {p0}, LX/0mj;->l(LX/0mj;)V

    .line 132829
    iget-object v3, p0, LX/0mj;->h:Ljava/lang/String;

    if-nez v3, :cond_0

    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->e:LX/0mO;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->e:LX/0mO;

    const/4 v4, 0x0

    .line 132830
    iget-object v5, v3, LX/0mO;->a:LX/00G;

    invoke-virtual {v5}, LX/00G;->c()Ljava/lang/String;

    move-result-object v6

    .line 132831
    if-nez v6, :cond_11

    .line 132832
    :goto_0
    move v3, v4

    .line 132833
    if-eqz v3, :cond_0

    .line 132834
    invoke-static {}, LX/0WQ;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/0mj;->h:Ljava/lang/String;

    .line 132835
    :cond_0
    iget-object v3, p0, LX/0mj;->j:Ljava/lang/Boolean;

    if-nez v3, :cond_1

    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->b:LX/0ma;

    if-eqz v3, :cond_1

    .line 132836
    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->b:LX/0ma;

    invoke-virtual {v3}, LX/0ma;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, LX/0mj;->j:Ljava/lang/Boolean;

    .line 132837
    :cond_1
    iget-wide v3, p0, LX/0mj;->k:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    .line 132838
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, LX/0mj;->k:J

    .line 132839
    :cond_2
    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->c:LX/0mc;

    if-eqz v3, :cond_3

    .line 132840
    iget-object v3, p0, LX/0mj;->b:LX/0mh;

    iget-object v3, v3, LX/0mh;->c:LX/0mc;

    .line 132841
    iget-object v4, v3, LX/0mc;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-virtual {v4}, LX/0kb;->m()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 132842
    iput-object v3, p0, LX/0mj;->i:Ljava/lang/String;

    .line 132843
    :cond_3
    invoke-direct {p0}, LX/0mj;->o()LX/0pE;

    move-result-object v0

    iget-object v1, p0, LX/0mj;->d:Ljava/lang/String;

    .line 132844
    iget-object v2, v0, LX/0pE;->g:LX/40D;

    if-eqz v2, :cond_13

    iget-object v2, v0, LX/0pE;->g:LX/40D;

    .line 132845
    iget-object v0, v2, LX/40D;->a:LX/4lQ;

    invoke-virtual {v0, v1}, LX/4lQ;->a(Ljava/lang/String;)Z

    move-result v0

    move v2, v0

    .line 132846
    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 132847
    if-eqz v0, :cond_5

    .line 132848
    invoke-direct {p0}, LX/0mj;->o()LX/0pE;

    move-result-object v0

    .line 132849
    iget-object v1, p0, LX/0mj;->s:LX/0n9;

    if-eqz v1, :cond_14

    .line 132850
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 132851
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, LX/0mj;->s:LX/0n9;

    .line 132852
    iget v4, v3, LX/0n9;->c:I

    move v3, v4

    .line 132853
    if-ge v1, v3, :cond_4

    .line 132854
    iget-object v3, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v3, v1}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v4, v1}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132855
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 132856
    :goto_3
    move-object v1, v1

    .line 132857
    iget-object v2, p0, LX/0mj;->d:Ljava/lang/String;

    .line 132858
    new-instance v3, LX/40B;

    invoke-direct {v3, v2, v1}, LX/40B;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 132859
    iget-object v4, v0, LX/0pE;->a:LX/0q7;

    .line 132860
    const/4 v0, 0x5

    invoke-virtual {v4, v0, v3}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0q7;->sendMessage(Landroid/os/Message;)Z

    .line 132861
    :cond_5
    iget-object v3, p0, LX/0mj;->h:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 132862
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v3

    const-string v4, "process"

    iget-object v5, p0, LX/0mj;->h:Ljava/lang/String;

    .line 132863
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132864
    :cond_6
    iget-object v3, p0, LX/0mj;->i:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 132865
    invoke-virtual {p0}, LX/0mj;->c()LX/0n9;

    move-result-object v3

    const-string v4, "radio_type"

    iget-object v5, p0, LX/0mj;->i:Ljava/lang/String;

    .line 132866
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132867
    :cond_7
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "log_type"

    iget-object v5, p0, LX/0mj;->e:LX/0mq;

    invoke-virtual {v5}, LX/0mq;->getProtocolValue()Ljava/lang/String;

    move-result-object v5

    .line 132868
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132869
    iget-object v3, p0, LX/0mj;->j:Ljava/lang/Boolean;

    if-eqz v3, :cond_8

    .line 132870
    iget-object v4, p0, LX/0mj;->r:LX/0n9;

    const-string v5, "bg"

    iget-object v3, p0, LX/0mj;->j:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_15

    const-string v3, "true"

    .line 132871
    :goto_4
    invoke-static {v4, v5, v3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132872
    :cond_8
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "time"

    iget-wide v5, p0, LX/0mj;->k:J

    long-to-double v5, v5

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 132873
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132874
    iget-object v3, p0, LX/0mj;->c:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 132875
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "module"

    iget-object v5, p0, LX/0mj;->c:Ljava/lang/String;

    .line 132876
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132877
    :cond_9
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "name"

    iget-object v5, p0, LX/0mj;->d:Ljava/lang/String;

    .line 132878
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132879
    iget-object v3, p0, LX/0mj;->l:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 132880
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "obj_type"

    iget-object v5, p0, LX/0mj;->l:Ljava/lang/String;

    .line 132881
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132882
    :cond_a
    iget-object v3, p0, LX/0mj;->m:Ljava/lang/String;

    if-eqz v3, :cond_b

    .line 132883
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "obj_id"

    iget-object v5, p0, LX/0mj;->m:Ljava/lang/String;

    .line 132884
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132885
    :cond_b
    iget-object v3, p0, LX/0mj;->n:Ljava/lang/String;

    if-eqz v3, :cond_c

    .line 132886
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "uuid"

    iget-object v5, p0, LX/0mj;->n:Ljava/lang/String;

    .line 132887
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132888
    :cond_c
    iget-object v3, p0, LX/0mj;->o:Ljava/lang/String;

    if-eqz v3, :cond_d

    .line 132889
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "interface"

    iget-object v5, p0, LX/0mj;->o:Ljava/lang/String;

    .line 132890
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132891
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "src_interface"

    iget-object v5, p0, LX/0mj;->o:Ljava/lang/String;

    .line 132892
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132893
    :cond_d
    iget-object v3, p0, LX/0mj;->p:Ljava/lang/String;

    if-eqz v3, :cond_e

    .line 132894
    iget-object v3, p0, LX/0mj;->r:LX/0n9;

    const-string v4, "dst_interface"

    iget-object v5, p0, LX/0mj;->p:Ljava/lang/String;

    .line 132895
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 132896
    :cond_e
    iget-object v0, p0, LX/0mj;->r:LX/0n9;

    invoke-virtual {v0}, LX/0nA;->f()V

    .line 132897
    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    if-eqz v0, :cond_f

    .line 132898
    iget-object v0, p0, LX/0mj;->s:LX/0n9;

    invoke-virtual {v0}, LX/0nA;->f()V

    .line 132899
    :cond_f
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0mj;->t:Z

    .line 132900
    iget-boolean v0, p0, LX/0mj;->g:Z

    if-eqz v0, :cond_10

    .line 132901
    invoke-direct {p0}, LX/0mj;->o()LX/0pE;

    move-result-object v0

    invoke-direct {p0}, LX/0mj;->n()LX/0n9;

    move-result-object v1

    .line 132902
    iget-object v2, v0, LX/0pE;->a:LX/0q7;

    .line 132903
    const/4 v0, 0x1

    invoke-virtual {v2, v0, v1}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0q7;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 132904
    :goto_5
    invoke-direct {p0}, LX/0mj;->g()V

    .line 132905
    return-void

    .line 132906
    :cond_10
    invoke-direct {p0}, LX/0mj;->o()LX/0pE;

    move-result-object v0

    invoke-direct {p0}, LX/0mj;->n()LX/0n9;

    move-result-object v1

    .line 132907
    iget-object v2, v0, LX/0pE;->a:LX/0q7;

    .line 132908
    const/4 v0, 0x1

    invoke-virtual {v2, v0, v1}, LX/0q7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0q7;->sendMessage(Landroid/os/Message;)Z

    .line 132909
    goto :goto_5

    .line 132910
    :cond_11
    const/4 v5, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :cond_12
    :goto_6
    packed-switch v5, :pswitch_data_1

    goto/16 :goto_0

    .line 132911
    :pswitch_0
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 132912
    :pswitch_1
    const-string v7, "dash"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    move v5, v4

    goto :goto_6

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 132913
    :cond_15
    const-string v3, "false"

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2eef92
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
