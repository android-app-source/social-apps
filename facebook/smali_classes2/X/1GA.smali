.class public LX/1GA;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/1GC;

.field public static b:LX/1GC;


# instance fields
.field public final c:LX/1G9;

.field private final d:LX/1FZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 224926
    sput-object v0, LX/1GA;->a:LX/1GC;

    .line 224927
    sput-object v0, LX/1GA;->b:LX/1GC;

    .line 224928
    const-string v0, "com.facebook.animated.gif.GifImage"

    invoke-static {v0}, LX/1GA;->a(Ljava/lang/String;)LX/1GC;

    move-result-object v0

    sput-object v0, LX/1GA;->a:LX/1GC;

    .line 224929
    const-string v0, "com.facebook.animated.webp.WebPImage"

    invoke-static {v0}, LX/1GA;->a(Ljava/lang/String;)LX/1GC;

    move-result-object v0

    sput-object v0, LX/1GA;->b:LX/1GC;

    .line 224930
    return-void
.end method

.method public constructor <init>(LX/1G9;LX/1FZ;)V
    .locals 0

    .prologue
    .line 224922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224923
    iput-object p1, p0, LX/1GA;->c:LX/1G9;

    .line 224924
    iput-object p2, p0, LX/1GA;->d:LX/1FZ;

    .line 224925
    return-void
.end method

.method public static a(LX/1GA;IILandroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224917
    iget-object v0, p0, LX/1GA;->d:LX/1FZ;

    invoke-virtual {v0, p1, p2, p3}, LX/1FZ;->b(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 224918
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 224919
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v0, v2, :cond_0

    .line 224920
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 224921
    :cond_0
    return-object v1
.end method

.method private a(LX/1GB;Landroid/graphics/Bitmap$Config;I)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1GB;",
            "Landroid/graphics/Bitmap$Config;",
            "I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224911
    invoke-interface {p1}, LX/1GB;->a()I

    move-result v0

    invoke-interface {p1}, LX/1GB;->b()I

    move-result v1

    invoke-static {p0, v0, v1, p2}, LX/1GA;->a(LX/1GA;IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v1

    .line 224912
    invoke-static {p1}, LX/4dO;->a(LX/1GB;)LX/4dO;

    move-result-object v0

    .line 224913
    iget-object v2, p0, LX/1GA;->c:LX/1G9;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, LX/1G9;->a(LX/4dO;Landroid/graphics/Rect;)LX/4dG;

    move-result-object v0

    .line 224914
    new-instance v2, LX/4dh;

    new-instance v3, LX/4dS;

    invoke-direct {v3, p0}, LX/4dS;-><init>(LX/1GA;)V

    invoke-direct {v2, v0, v3}, LX/4dh;-><init>(LX/4dG;LX/4CF;)V

    .line 224915
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, p3, v0}, LX/4dh;->a(ILandroid/graphics/Bitmap;)V

    .line 224916
    return-object v1
.end method

.method private static a(Ljava/lang/String;)LX/1GC;
    .locals 1

    .prologue
    .line 224908
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 224909
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GC;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 224910
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/1bZ;LX/1GB;Landroid/graphics/Bitmap$Config;)LX/1ln;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 224858
    :try_start_0
    iget-boolean v2, p1, LX/1bZ;->c:Z

    if-eqz v2, :cond_0

    invoke-interface {p2}, LX/1GB;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 224859
    :goto_0
    iget-boolean v0, p1, LX/1bZ;->e:Z

    if-eqz v0, :cond_1

    .line 224860
    new-instance v0, LX/1ll;

    invoke-direct {p0, p2, p3, v3}, LX/1GA;->a(LX/1GB;Landroid/graphics/Bitmap$Config;I)LX/1FJ;

    move-result-object v2

    sget-object v3, LX/1lk;->a:LX/1lk;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224861
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 224862
    invoke-static {v1}, LX/1FJ;->a(Ljava/lang/Iterable;)V

    :goto_1
    return-object v0

    :cond_0
    move v3, v0

    .line 224863
    goto :goto_0

    .line 224864
    :cond_1
    :try_start_1
    iget-boolean v0, p1, LX/1bZ;->d:Z

    if-eqz v0, :cond_4

    .line 224865
    invoke-static {p2}, LX/4dO;->a(LX/1GB;)LX/4dO;

    move-result-object v0

    .line 224866
    iget-object v2, p0, LX/1GA;->c:LX/1G9;

    const/4 v4, 0x0

    invoke-interface {v2, v0, v4}, LX/1G9;->a(LX/4dO;Landroid/graphics/Rect;)LX/4dG;

    move-result-object v4

    .line 224867
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, LX/4dG;->c()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 224868
    new-instance v6, LX/4dh;

    new-instance v0, LX/4dT;

    invoke-direct {v0, p0, v5}, LX/4dT;-><init>(LX/1GA;Ljava/util/List;)V

    invoke-direct {v6, v4, v0}, LX/4dh;-><init>(LX/4dG;LX/4CF;)V

    .line 224869
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    invoke-interface {v4}, LX/4dG;->c()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 224870
    invoke-interface {v4}, LX/4dG;->e()I

    move-result v0

    invoke-interface {v4}, LX/4dG;->f()I

    move-result v7

    invoke-static {p0, v0, v7, p3}, LX/1GA;->a(LX/1GA;IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v7

    .line 224871
    invoke-virtual {v7}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v2, v0}, LX/4dh;->a(ILandroid/graphics/Bitmap;)V

    .line 224872
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224873
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 224874
    :cond_2
    move-object v2, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224875
    :try_start_2
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 224876
    :goto_3
    iget-boolean v0, p1, LX/1bZ;->b:Z

    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 224877
    invoke-direct {p0, p2, p3, v3}, LX/1GA;->a(LX/1GB;Landroid/graphics/Bitmap$Config;I)LX/1FJ;

    move-result-object v1

    .line 224878
    :cond_3
    new-instance v0, LX/4dP;

    invoke-direct {v0, p2}, LX/4dP;-><init>(LX/1GB;)V

    move-object v0, v0

    .line 224879
    invoke-static {v1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v4

    iput-object v4, v0, LX/4dP;->b:LX/1FJ;

    .line 224880
    move-object v0, v0

    .line 224881
    iput v3, v0, LX/4dP;->d:I

    .line 224882
    move-object v0, v0

    .line 224883
    invoke-static {v2}, LX/1FJ;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v0, LX/4dP;->c:Ljava/util/List;

    .line 224884
    move-object v0, v0

    .line 224885
    invoke-virtual {v0}, LX/4dP;->e()LX/4dO;

    move-result-object v3

    .line 224886
    new-instance v0, LX/4e7;

    invoke-direct {v0, v3}, LX/4e7;-><init>(LX/4dO;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 224887
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 224888
    invoke-static {v2}, LX/1FJ;->a(Ljava/lang/Iterable;)V

    goto/16 :goto_1

    .line 224889
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 224890
    invoke-static {v2}, LX/1FJ;->a(Ljava/lang/Iterable;)V

    throw v0

    .line 224891
    :catchall_1
    move-exception v0

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/1FL;LX/1bZ;Landroid/graphics/Bitmap$Config;)LX/1ln;
    .locals 6

    .prologue
    .line 224900
    sget-object v0, LX/1GA;->a:LX/1GC;

    if-nez v0, :cond_0

    .line 224901
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "To encode animated gif please add the dependency to the animated-gif module"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224902
    :cond_0
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 224903
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224904
    :try_start_0
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 224905
    sget-object v2, LX/1GA;->a:LX/1GC;

    invoke-virtual {v0}, LX/1FK;->b()J

    move-result-wide v4

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    invoke-interface {v2, v4, v5, v0}, LX/1GC;->a(JI)LX/1GB;

    move-result-object v0

    .line 224906
    invoke-direct {p0, p2, v0, p3}, LX/1GA;->a(LX/1bZ;LX/1GB;Landroid/graphics/Bitmap$Config;)LX/1ln;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 224907
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final b(LX/1FL;LX/1bZ;Landroid/graphics/Bitmap$Config;)LX/1ln;
    .locals 6

    .prologue
    .line 224892
    sget-object v0, LX/1GA;->b:LX/1GC;

    if-nez v0, :cond_0

    .line 224893
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "To encode animated webp please add the dependency to the animated-webp module"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224894
    :cond_0
    invoke-virtual {p1}, LX/1FL;->a()LX/1FJ;

    move-result-object v1

    .line 224895
    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224896
    :try_start_0
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 224897
    sget-object v2, LX/1GA;->b:LX/1GC;

    invoke-virtual {v0}, LX/1FK;->b()J

    move-result-wide v4

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v0

    invoke-interface {v2, v4, v5, v0}, LX/1GC;->a(JI)LX/1GB;

    move-result-object v0

    .line 224898
    invoke-direct {p0, p2, v0, p3}, LX/1GA;->a(LX/1bZ;LX/1GB;Landroid/graphics/Bitmap$Config;)LX/1ln;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 224899
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
