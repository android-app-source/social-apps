.class public LX/0h3;
.super Lcom/facebook/ui/titlebar/Fb4aTitleBar;
.source ""


# instance fields
.field public n:LX/0hs;

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 114824
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0h3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 114825
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 114822
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0h3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114823
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 114819
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114820
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    new-instance v1, LX/0wR;

    invoke-direct {v1, p0, p1}, LX/0wR;-><init>(LX/0h3;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114821
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 114812
    invoke-super/range {p0 .. p5}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->onLayout(ZIIII)V

    .line 114813
    iget-object v0, p0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->e:Landroid/widget/TextView;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    iput-boolean v0, p0, LX/0h3;->o:Z

    .line 114814
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114815
    invoke-super {p0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 114816
    iget-object v0, p0, LX/0h3;->n:LX/0hs;

    if-eqz v0, :cond_0

    .line 114817
    iget-object v0, p0, LX/0h3;->n:LX/0hs;

    invoke-virtual {v0, p1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 114818
    :cond_0
    return-void
.end method
