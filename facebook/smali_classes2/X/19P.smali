.class public LX/19P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/19Q;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.String.length"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile ae:LX/19P;


# instance fields
.field public A:LX/2qU;

.field public B:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;"
        }
    .end annotation
.end field

.field private C:I

.field private D:Z

.field public E:Z

.field private F:LX/0So;

.field private G:LX/19Z;

.field private H:Ljava/lang/Boolean;

.field private final I:LX/19d;

.field private J:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;"
        }
    .end annotation
.end field

.field private final L:Landroid/os/Looper;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:LX/19s;

.field public N:LX/04j;

.field private final O:LX/16V;

.field private final P:LX/0ka;

.field private final Q:LX/0oz;

.field public final R:LX/0Uh;

.field private final S:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final T:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2qA;",
            ">;"
        }
    .end annotation
.end field

.field private final U:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2qC;",
            ">;"
        }
    .end annotation
.end field

.field private final V:LX/0YR;

.field private final W:LX/19v;

.field private final X:LX/0tQ;

.field private final Y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field private final Z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/04J;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;>;"
        }
    .end annotation
.end field

.field private final aa:LX/1AA;

.field private ab:J

.field private ac:J

.field private ad:J

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2qU;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2p8;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3FI;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/19R;

.field private final h:LX/0Sh;

.field public final i:LX/19S;

.field private final j:Landroid/media/AudioManager;

.field private final k:LX/1AB;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/11i;

.field public final o:LX/0wq;

.field private final p:LX/0wp;

.field private final q:Ljava/util/concurrent/ScheduledExecutorService;

.field private final r:LX/0TD;

.field private final s:LX/0ad;

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7I8;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2q3;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/13m;

.field private final w:LX/19j;

.field public final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/19a;

.field private z:LX/16V;


# direct methods
.method public constructor <init>(LX/0Ot;LX/19R;LX/0Sh;LX/19S;Landroid/media/AudioManager;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;LX/0So;LX/0Ot;LX/19Z;LX/11i;LX/0Or;LX/0ad;LX/0Ot;LX/19a;Ljava/lang/Boolean;LX/15Z;LX/19d;LX/0Or;LX/13m;LX/0Ot;LX/0Ot;LX/0Or;LX/0ka;LX/0oz;LX/0Ot;LX/0Uh;LX/19s;LX/19j;LX/0Ot;LX/0Ot;LX/0YR;LX/19v;LX/0tQ;LX/0Ot;LX/0Ot;LX/1AA;)V
    .locals 5
    .param p6    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param
    .param p8    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .param p16    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/engine/IsPausedBitmapEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3FI;",
            ">;",
            "LX/19R;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/video/engine/VideoPlayerLimitsProvider;",
            "Landroid/media/AudioManager;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0TD;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "LX/1m0;",
            ">;",
            "LX/19Z;",
            "LX/11i;",
            "LX/0Or",
            "<",
            "LX/0wq;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/7I8;",
            ">;",
            "LX/19a;",
            "Ljava/lang/Boolean;",
            "LX/15Z;",
            "Lcom/facebook/video/engine/VideoPlayerViewProvider;",
            "LX/0Or",
            "<",
            "LX/2q3;",
            ">;",
            "LX/13m;",
            "LX/0Ot",
            "<",
            "LX/1Yk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0ka;",
            "LX/0oz;",
            "LX/0Ot",
            "<",
            "LX/2qA;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/19s;",
            "LX/19j;",
            "LX/0Ot",
            "<",
            "LX/2qC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0YR;",
            "LX/19v;",
            "LX/0tQ;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/04J;",
            ">;",
            "LX/1AA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 207825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207826
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/19P;->a:Ljava/util/List;

    .line 207827
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/19P;->b:Ljava/util/List;

    .line 207828
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/19P;->c:Ljava/util/List;

    .line 207829
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/19P;->d:Ljava/util/List;

    .line 207830
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/19P;->e:Ljava/util/List;

    .line 207831
    const/4 v2, 0x0

    iput-object v2, p0, LX/19P;->A:LX/2qU;

    .line 207832
    const/4 v2, 0x0

    iput v2, p0, LX/19P;->C:I

    .line 207833
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/19P;->D:Z

    .line 207834
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/19P;->E:Z

    .line 207835
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/19P;->J:Z

    .line 207836
    const/4 v2, 0x0

    iput-object v2, p0, LX/19P;->N:LX/04j;

    .line 207837
    new-instance v2, LX/0aq;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, LX/0aq;-><init>(I)V

    iput-object v2, p0, LX/19P;->S:LX/0aq;

    .line 207838
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/19P;->ab:J

    .line 207839
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/19P;->ac:J

    .line 207840
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/19P;->ad:J

    .line 207841
    iput-object p1, p0, LX/19P;->f:LX/0Ot;

    .line 207842
    iput-object p2, p0, LX/19P;->g:LX/19R;

    .line 207843
    iput-object p3, p0, LX/19P;->h:LX/0Sh;

    .line 207844
    iput-object p4, p0, LX/19P;->i:LX/19S;

    .line 207845
    iput-object p5, p0, LX/19P;->j:Landroid/media/AudioManager;

    .line 207846
    move-object/from16 v0, p21

    iput-object v0, p0, LX/19P;->K:LX/0Ot;

    .line 207847
    move-object/from16 v0, p26

    iput-object v0, p0, LX/19P;->T:LX/0Ot;

    .line 207848
    new-instance v2, LX/1AB;

    invoke-direct {v2, p0, p0}, LX/1AB;-><init>(LX/19P;LX/19P;)V

    iput-object v2, p0, LX/19P;->k:LX/1AB;

    .line 207849
    iput-object p6, p0, LX/19P;->q:Ljava/util/concurrent/ScheduledExecutorService;

    .line 207850
    iput-object p7, p0, LX/19P;->r:LX/0TD;

    .line 207851
    invoke-interface/range {p12 .. p12}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0wq;

    iput-object v2, p0, LX/19P;->o:LX/0wq;

    .line 207852
    invoke-interface/range {p23 .. p23}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0wp;

    iput-object v2, p0, LX/19P;->p:LX/0wp;

    .line 207853
    move-object/from16 v0, p24

    iput-object v0, p0, LX/19P;->P:LX/0ka;

    .line 207854
    move-object/from16 v0, p25

    iput-object v0, p0, LX/19P;->Q:LX/0oz;

    .line 207855
    move-object/from16 v0, p13

    iput-object v0, p0, LX/19P;->s:LX/0ad;

    .line 207856
    move-object/from16 v0, p14

    iput-object v0, p0, LX/19P;->t:LX/0Ot;

    .line 207857
    move-object/from16 v0, p19

    iput-object v0, p0, LX/19P;->u:LX/0Or;

    .line 207858
    move-object/from16 v0, p15

    iput-object v0, p0, LX/19P;->y:LX/19a;

    .line 207859
    move-object/from16 v0, p32

    iput-object v0, p0, LX/19P;->V:LX/0YR;

    .line 207860
    move-object/from16 v0, p16

    iput-object v0, p0, LX/19P;->H:Ljava/lang/Boolean;

    .line 207861
    iput-object p8, p0, LX/19P;->F:LX/0So;

    .line 207862
    iput-object p9, p0, LX/19P;->l:LX/0Ot;

    .line 207863
    move-object/from16 v0, p22

    iput-object v0, p0, LX/19P;->m:LX/0Ot;

    .line 207864
    iput-object p10, p0, LX/19P;->G:LX/19Z;

    .line 207865
    move-object/from16 v0, p11

    iput-object v0, p0, LX/19P;->n:LX/11i;

    .line 207866
    move-object/from16 v0, p20

    iput-object v0, p0, LX/19P;->v:LX/13m;

    .line 207867
    move-object/from16 v0, p27

    iput-object v0, p0, LX/19P;->R:LX/0Uh;

    .line 207868
    invoke-virtual/range {p17 .. p17}, LX/15Z;->a()Z

    move-result v2

    iput-boolean v2, p0, LX/19P;->E:Z

    .line 207869
    new-instance v2, LX/1AC;

    invoke-direct {v2, p0}, LX/1AC;-><init>(LX/19P;)V

    move-object/from16 v0, p17

    invoke-virtual {v0, v2}, LX/15Z;->a(LX/0T2;)V

    .line 207870
    move-object/from16 v0, p18

    iput-object v0, p0, LX/19P;->I:LX/19d;

    .line 207871
    move-object/from16 v0, p33

    iput-object v0, p0, LX/19P;->W:LX/19v;

    .line 207872
    move-object/from16 v0, p34

    iput-object v0, p0, LX/19P;->X:LX/0tQ;

    .line 207873
    move-object/from16 v0, p35

    iput-object v0, p0, LX/19P;->Y:LX/0Ot;

    .line 207874
    move-object/from16 v0, p36

    iput-object v0, p0, LX/19P;->Z:LX/0Ot;

    .line 207875
    move-object/from16 v0, p37

    iput-object v0, p0, LX/19P;->aa:LX/1AA;

    .line 207876
    move-object/from16 v0, p28

    iput-object v0, p0, LX/19P;->M:LX/19s;

    .line 207877
    iget-object v2, p0, LX/19P;->M:LX/19s;

    invoke-virtual {v2}, LX/19s;->b()LX/04j;

    move-result-object v2

    iput-object v2, p0, LX/19P;->N:LX/04j;

    .line 207878
    new-instance v2, LX/16V;

    invoke-direct {v2}, LX/16V;-><init>()V

    iput-object v2, p0, LX/19P;->O:LX/16V;

    .line 207879
    iget-object v2, p0, LX/19P;->O:LX/16V;

    const-class v3, LX/044;

    new-instance v4, LX/1AE;

    invoke-direct {v4, p0}, LX/1AE;-><init>(LX/19P;)V

    invoke-virtual {v2, v3, v4}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 207880
    iget-object v2, p0, LX/19P;->M:LX/19s;

    invoke-virtual {v2, p0}, LX/19s;->a(LX/19Q;)V

    .line 207881
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "VideoPlayerServiceThread"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 207882
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 207883
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iput-object v2, p0, LX/19P;->L:Landroid/os/Looper;

    .line 207884
    move-object/from16 v0, p29

    iput-object v0, p0, LX/19P;->w:LX/19j;

    .line 207885
    move-object/from16 v0, p30

    iput-object v0, p0, LX/19P;->U:LX/0Ot;

    .line 207886
    move-object/from16 v0, p31

    iput-object v0, p0, LX/19P;->x:LX/0Ot;

    .line 207887
    return-void
.end method

.method public static a(LX/0QB;)LX/19P;
    .locals 3

    .prologue
    .line 207802
    sget-object v0, LX/19P;->ae:LX/19P;

    if-nez v0, :cond_1

    .line 207803
    const-class v1, LX/19P;

    monitor-enter v1

    .line 207804
    :try_start_0
    sget-object v0, LX/19P;->ae:LX/19P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 207805
    if-eqz v2, :cond_0

    .line 207806
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/19P;->b(LX/0QB;)LX/19P;

    move-result-object v0

    sput-object v0, LX/19P;->ae:LX/19P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207807
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 207808
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207809
    :cond_1
    sget-object v0, LX/19P;->ae:LX/19P;

    return-object v0

    .line 207810
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 207811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/19P;
    .locals 40

    .prologue
    .line 207888
    new-instance v2, LX/19P;

    const/16 v3, 0x1338

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/19R;->a(LX/0QB;)LX/19R;

    move-result-object v4

    check-cast v4, LX/19R;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/19S;->a(LX/0QB;)LX/19S;

    move-result-object v6

    check-cast v6, LX/19S;

    invoke-static/range {p0 .. p0}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    invoke-static/range {p0 .. p0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/19U;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    const/16 v11, 0x1358

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/19X;->a(LX/0QB;)LX/19Z;

    move-result-object v12

    check-cast v12, LX/19Z;

    invoke-static/range {p0 .. p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v13

    check-cast v13, LX/11i;

    const/16 v14, 0x12e5

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    const/16 v16, 0x37b0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/19a;->a(LX/0QB;)LX/19a;

    move-result-object v17

    check-cast v17, LX/19a;

    invoke-static/range {p0 .. p0}, LX/19b;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v18

    check-cast v18, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/15Z;->a(LX/0QB;)LX/15Z;

    move-result-object v19

    check-cast v19, LX/15Z;

    invoke-static/range {p0 .. p0}, LX/19d;->a(LX/0QB;)LX/19d;

    move-result-object v20

    check-cast v20, LX/19d;

    const/16 v21, 0x1354

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/13m;->a(LX/0QB;)LX/13m;

    move-result-object v22

    check-cast v22, LX/13m;

    const/16 v23, 0x1333

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x135d

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v26

    check-cast v26, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v27

    check-cast v27, LX/0oz;

    const/16 v28, 0x1336

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v28

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v29

    check-cast v29, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v30

    check-cast v30, LX/19s;

    invoke-static/range {p0 .. p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v31

    check-cast v31, LX/19j;

    const/16 v32, 0x1353

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v33

    invoke-static/range {p0 .. p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v34

    check-cast v34, LX/0YR;

    invoke-static/range {p0 .. p0}, LX/19v;->a(LX/0QB;)LX/19v;

    move-result-object v35

    check-cast v35, LX/19v;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v36

    check-cast v36, LX/0tQ;

    const/16 v37, 0x132e

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v37

    const/16 v38, 0x12f3

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v38

    invoke-static/range {p0 .. p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v39

    check-cast v39, LX/1AA;

    invoke-direct/range {v2 .. v39}, LX/19P;-><init>(LX/0Ot;LX/19R;LX/0Sh;LX/19S;Landroid/media/AudioManager;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;LX/0So;LX/0Ot;LX/19Z;LX/11i;LX/0Or;LX/0ad;LX/0Ot;LX/19a;Ljava/lang/Boolean;LX/15Z;LX/19d;LX/0Or;LX/13m;LX/0Ot;LX/0Ot;LX/0Or;LX/0ka;LX/0oz;LX/0Ot;LX/0Uh;LX/19s;LX/19j;LX/0Ot;LX/0Ot;LX/0YR;LX/19v;LX/0tQ;LX/0Ot;LX/0Ot;LX/1AA;)V

    .line 207889
    return-object v2
.end method

.method public static b(LX/19P;LX/04g;)V
    .locals 1

    .prologue
    .line 207890
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eqz v0, :cond_0

    .line 207891
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    invoke-static {p0, v0, p1}, LX/19P;->c(LX/19P;LX/2qU;LX/04g;)V

    .line 207892
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    invoke-virtual {v0, p1}, LX/2qU;->g(LX/04g;)V

    .line 207893
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/19P;->A:LX/2qU;

    .line 207894
    return-void
.end method

.method private static c(LX/19P;LX/2qU;LX/04g;)V
    .locals 2

    .prologue
    .line 207895
    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_BOOKMARK:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_DIALOG:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_DIVEBAR:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_MANAGER:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, LX/04g;->value:Ljava/lang/String;

    sget-object v1, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    iget-object v1, v1, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 207896
    if-eqz v0, :cond_1

    .line 207897
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/19P;->B:Ljava/lang/ref/WeakReference;

    .line 207898
    :goto_1
    return-void

    .line 207899
    :cond_1
    invoke-static {p0}, LX/19P;->j(LX/19P;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized f()LX/16V;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 207900
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19P;->z:LX/16V;

    if-nez v0, :cond_0

    .line 207901
    iget-object v0, p0, LX/19P;->l:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/19P;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1m0;

    .line 207902
    :goto_0
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, p0, LX/19P;->z:LX/16V;

    .line 207903
    :cond_0
    iget-object v0, p0, LX/19P;->z:LX/16V;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    move-object v0, v1

    .line 207904
    goto :goto_0

    .line 207905
    :cond_2
    :try_start_1
    iget-object v1, v0, LX/1m0;->n:LX/16V;

    move-object v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207906
    goto :goto_1

    .line 207907
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static g(LX/19P;LX/2qU;)I
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 207908
    iget v0, p1, LX/2qU;->c:I

    move v2, v0

    .line 207909
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 207910
    iget-object v0, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qU;

    .line 207911
    if-eqz v0, :cond_0

    .line 207912
    iget p1, v0, LX/2qU;->c:I

    move v0, p1

    .line 207913
    if-ne v0, v2, :cond_0

    .line 207914
    :goto_1
    return v1

    .line 207915
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 207916
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static g(LX/19P;)V
    .locals 1

    .prologue
    .line 207917
    iget-object v0, p0, LX/19P;->o:LX/0wq;

    invoke-virtual {v0}, LX/0wq;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/19P;->o:LX/0wq;

    invoke-virtual {v0}, LX/0wq;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207918
    :cond_0
    iget-object v0, p0, LX/19P;->M:LX/19s;

    invoke-virtual {v0}, LX/19s;->a()V

    .line 207919
    :cond_1
    return-void
.end method

.method private h()I
    .locals 2

    .prologue
    .line 207920
    iget v0, p0, LX/19P;->C:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/19P;->C:I

    return v0
.end method

.method public static h(LX/19P;LX/2qU;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 207921
    iget v0, p1, LX/2qU;->c:I

    move v3, v0

    .line 207922
    move v1, v2

    .line 207923
    :goto_0
    iget-object v0, p0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 207924
    iget-object v0, p0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qU;

    .line 207925
    if-eqz v0, :cond_1

    .line 207926
    iget p1, v0, LX/2qU;->c:I

    move v0, p1

    .line 207927
    if-ne v0, v3, :cond_1

    .line 207928
    const/4 v2, 0x1

    .line 207929
    :cond_0
    return v2

    .line 207930
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static j(LX/19P;)V
    .locals 1

    .prologue
    .line 207684
    const/4 v0, 0x0

    iput-object v0, p0, LX/19P;->B:Ljava/lang/ref/WeakReference;

    .line 207685
    return-void
.end method

.method public static j(LX/19P;LX/2qU;)V
    .locals 1

    .prologue
    .line 207931
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-ne v0, p1, :cond_0

    .line 207932
    const/4 v0, 0x0

    iput-object v0, p0, LX/19P;->A:LX/2qU;

    .line 207933
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;
    .locals 44

    .prologue
    .line 207934
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/19P;->l:LX/0Ot;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207935
    move-object/from16 v0, p0

    iget-object v2, v0, LX/19P;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1m0;

    .line 207936
    invoke-static/range {p0 .. p0}, LX/19P;->g(LX/19P;)V

    .line 207937
    sget-object v3, LX/04D;->BACKSTAGE_VIDEOS:LX/04D;

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, LX/04D;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 207938
    if-eqz v2, :cond_6

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->R:LX/0Uh;

    sget v4, LX/19n;->s:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->o:LX/0wq;

    invoke-virtual {v3}, LX/0wq;->g()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz p9, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->o:LX/0wq;

    invoke-virtual {v3}, LX/0wq;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_1
    const/4 v4, 0x1

    .line 207939
    :goto_0
    const/4 v3, 0x0

    .line 207940
    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, LX/19P;->N:LX/04j;

    if-nez v5, :cond_a

    .line 207941
    invoke-virtual/range {p6 .. p6}, LX/1C2;->b()LX/1C2;

    .line 207942
    const/4 v4, 0x0

    .line 207943
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->o:LX/0wq;

    invoke-virtual {v3}, LX/0wq;->h()Z

    move-result v3

    move v5, v3

    move v6, v4

    .line 207944
    :goto_1
    const-string v3, "VideoPlayerManager.createVideoPlayer.initSharedParams"

    const v4, -0x4a2df844

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 207945
    :try_start_1
    new-instance v7, LX/2pu;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v7, v0, v1}, LX/2pu;-><init>(LX/19P;LX/2pf;)V

    .line 207946
    new-instance v17, LX/2pv;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, LX/2pv;-><init>(LX/19P;)V

    .line 207947
    new-instance v19, LX/2px;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/19P;->n:LX/11i;

    invoke-direct/range {p0 .. p0}, LX/19P;->f()LX/16V;

    move-result-object v9

    if-nez v5, :cond_2

    if-eqz v6, :cond_7

    :cond_2
    const-string v3, "old_api_exo"

    move-object v4, v3

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->m:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Lv;

    move-object/from16 v0, v19

    invoke-direct {v0, v8, v9, v4, v3}, LX/2px;-><init>(LX/11i;LX/16V;Ljava/lang/String;LX/1Lv;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207948
    const v3, -0x2e6868f7

    :try_start_2
    invoke-static {v3}, LX/02m;->a(I)V

    .line 207949
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->R:LX/0Uh;

    sget v4, LX/19n;->b:I

    invoke-virtual {v3, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v25

    .line 207950
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->R:LX/0Uh;

    sget v4, LX/19n;->a:I

    invoke-virtual {v3, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v32

    .line 207951
    const/4 v3, 0x0

    .line 207952
    if-eqz v6, :cond_8

    .line 207953
    const-string v3, "ExoplayerServiceApi.setVideoServerBaseUri"

    const v4, -0x88d588c

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 207954
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->N:LX/04j;

    invoke-virtual {v2}, LX/1m0;->b()Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, LX/04j;->a(Landroid/net/Uri;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 207955
    const v3, -0x1a20aade

    :try_start_4
    invoke-static {v3}, LX/02m;->a(I)V

    .line 207956
    :goto_3
    const-string v3, "ExoVideoPlayerClient.init"

    const v4, 0x15efac97

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 207957
    :try_start_5
    const-string v3, "ExoVideoPlayerClient.initArgs"

    const v4, 0x1ebc8fb5

    invoke-static {v3, v4}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 207958
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->u:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, LX/2q3;

    .line 207959
    invoke-virtual {v2}, LX/1m0;->c()LX/04m;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-object v28

    .line 207960
    const v2, 0x262086b9

    :try_start_7
    invoke-static {v2}, LX/02m;->a(I)V

    .line 207961
    new-instance v43, LX/2q5;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/19P;->q:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, v43

    invoke-direct {v0, v2}, LX/2q5;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 207962
    new-instance v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/19P;->I:LX/19d;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/19P;->r:LX/0TD;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/19P;->H:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/19P;->h:LX/0Sh;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/19P;->G:LX/19Z;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/19P;->F:LX/0So;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/19P;->o:LX/0wq;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->g:LX/19R;

    invoke-virtual {v3}, LX/19R;->a()LX/2q9;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->v:LX/13m;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->K:LX/0Ot;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->M:LX/19s;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->O:LX/16V;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->p:LX/0wp;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->P:LX/0ka;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->Q:LX/0oz;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->s:LX/0ad;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->R:LX/0Uh;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->T:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, LX/2qA;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->L:Landroid/os/Looper;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->w:LX/19j;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->U:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, LX/2qC;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->x:LX/0Ot;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->V:LX/0YR;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->aa:LX/1AA;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->y:LX/19a;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->Y:LX/0Ot;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->Z:LX/0Ot;

    move-object/from16 v42, v0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v8, p6

    move/from16 v11, p7

    move-object/from16 v18, p5

    invoke-direct/range {v2 .. v43}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/2pf;LX/1C2;LX/0TD;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/0wq;LX/2q9;LX/2pw;LX/2qI;LX/2px;LX/13m;LX/2q3;LX/0Ot;LX/0Or;LX/16V;ZLX/0wp;LX/0ka;LX/04m;LX/0oz;LX/0ad;LX/0Uh;ZLX/2qA;Landroid/os/Looper;LX/19j;LX/2qC;LX/0Ot;LX/0YR;LX/1AA;LX/19a;LX/0Ot;LX/0Ot;LX/2q5;)V

    .line 207963
    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->o:LX/0wq;

    iget-boolean v3, v3, LX/0wq;->ab:Z

    if-eqz v3, :cond_3

    .line 207964
    new-instance v3, LX/7KM;

    move-object/from16 v0, v43

    invoke-direct {v3, v2, v0}, LX/7KM;-><init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2q5;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    move-object v2, v3

    .line 207965
    :cond_3
    const v3, -0x4495c575

    :try_start_8
    invoke-static {v3}, LX/02m;->a(I)V

    .line 207966
    :goto_4
    if-nez v2, :cond_4

    .line 207967
    move-object/from16 v0, p0

    iget-object v2, v0, LX/19P;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3FI;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/19P;->g:LX/19R;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->h:LX/0Sh;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/19P;->u:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, LX/2q3;

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move/from16 v11, p3

    move-object v12, v7

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    move/from16 v18, p7

    invoke-virtual/range {v8 .. v20}, LX/3FI;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;LX/19R;LX/0Sh;LX/2pw;ZLX/2px;LX/2q3;)LX/2q7;

    move-result-object v2

    .line 207968
    :cond_4
    new-instance v3, LX/2qU;

    new-instance v4, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, LX/19P;->h()I

    move-result v5

    invoke-direct {v3, v4, v2, v7, v5}, LX/2qU;-><init>(Ljava/lang/ref/WeakReference;LX/2q7;LX/2pu;I)V

    .line 207969
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, LX/2pv;->a(LX/2qU;)V

    .line 207970
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 207971
    if-eqz p8, :cond_5

    .line 207972
    move-object/from16 v0, p0

    iget-object v4, v0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 207973
    :cond_5
    monitor-exit p0

    return-object v3

    .line 207974
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 207975
    :cond_7
    :try_start_9
    const-string v3, "old_api"
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-object v4, v3

    goto/16 :goto_2

    .line 207976
    :catchall_0
    move-exception v2

    const v3, 0x2496640a

    :try_start_a
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 207977
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 207978
    :catch_0
    const v3, 0x7e6d8ffa

    :try_start_b
    invoke-static {v3}, LX/02m;->a(I)V

    goto/16 :goto_3

    :catchall_2
    move-exception v2

    const v3, 0x5b8a36ef

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 207979
    :catchall_3
    move-exception v2

    const v3, -0x456b466d

    :try_start_c
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 207980
    :catchall_4
    move-exception v2

    const v3, -0x36b823c9

    :try_start_d
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 207981
    :cond_8
    if-eqz v5, :cond_9

    .line 207982
    new-instance v2, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/19P;->I:LX/19d;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/19P;->q:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/19P;->H:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/19P;->h:LX/0Sh;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/19P;->G:LX/19Z;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/19P;->F:LX/0So;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/19P;->o:LX/0wq;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->g:LX/19R;

    invoke-virtual {v3}, LX/19R;->a()LX/2q9;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->v:LX/13m;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/19P;->u:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, LX/2q3;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->K:LX/0Ot;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->s:LX/0ad;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->R:LX/0Uh;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->w:LX/19j;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->y:LX/19a;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/19P;->x:LX/0Ot;

    move-object/from16 v29, v0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v8, p6

    move/from16 v11, p7

    move-object/from16 v18, p5

    move/from16 v26, v32

    invoke-direct/range {v2 .. v29}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILX/19d;LX/2pf;LX/1C2;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Boolean;ZLX/0Sh;LX/19Z;LX/0So;LX/0wq;LX/2q9;LX/2pw;LX/2qI;LX/2px;LX/13m;LX/2q3;LX/0Ot;LX/0ad;LX/0Uh;ZZLX/19j;LX/19a;LX/0Ot;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_4

    :cond_9
    move-object v2, v3

    goto/16 :goto_4

    :cond_a
    move v5, v3

    move v6, v4

    goto/16 :goto_1
.end method

.method public final declared-synchronized a()V
    .locals 9

    .prologue
    const-wide/16 v2, -0x1

    .line 207812
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/19P;->N:LX/04j;

    .line 207813
    iget-wide v0, p0, LX/19P;->ab:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 207814
    iget-object v0, p0, LX/19P;->F:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/19P;->ab:J

    sub-long v2, v0, v2

    .line 207815
    iget-wide v0, p0, LX/19P;->ad:J

    iget-wide v4, p0, LX/19P;->ac:J

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    iget-wide v4, p0, LX/19P;->ac:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    div-long/2addr v0, v4

    iput-wide v0, p0, LX/19P;->ad:J

    .line 207816
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/19P;->ab:J

    .line 207817
    :cond_0
    new-instance v1, LX/0Je;

    invoke-direct {v1}, LX/0Je;-><init>()V

    .line 207818
    iget-wide v4, p0, LX/19P;->ad:J

    iget-wide v6, p0, LX/19P;->ac:J

    .line 207819
    iput-wide v2, v1, LX/0Je;->b:J

    .line 207820
    iput-wide v4, v1, LX/0Je;->c:J

    .line 207821
    iput-wide v6, v1, LX/0Je;->d:J

    .line 207822
    iget-object v0, p0, LX/19P;->O:LX/16V;

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207823
    monitor-exit p0

    return-void

    .line 207824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/04j;)V
    .locals 4

    .prologue
    .line 207677
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/19P;->N:LX/04j;

    .line 207678
    iget-wide v0, p0, LX/19P;->ab:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 207679
    iget-object v0, p0, LX/19P;->F:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/19P;->ab:J

    .line 207680
    :cond_0
    iget-wide v0, p0, LX/19P;->ac:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/19P;->ac:J

    .line 207681
    iget-object v0, p0, LX/19P;->O:LX/16V;

    new-instance v1, LX/04k;

    iget-object v2, p0, LX/19P;->N:LX/04j;

    invoke-direct {v1, v2}, LX/04k;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207682
    monitor-exit p0

    return-void

    .line 207683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2qU;)V
    .locals 3

    .prologue
    .line 207686
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207687
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 207688
    iget v2, p1, LX/2qU;->c:I

    move v2, v2

    .line 207689
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 207690
    sget-object v0, LX/04g;->BY_MANAGER:LX/04g;

    invoke-virtual {p1, v0}, LX/2qU;->f(LX/04g;)V

    .line 207691
    iget-object v0, p0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 207692
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207693
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 207694
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 207695
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 207696
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 207697
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207698
    :goto_1
    monitor-exit p0

    return-void

    .line 207699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    goto :goto_1
.end method

.method public final declared-synchronized a(LX/2qU;LX/04g;)V
    .locals 1

    .prologue
    .line 207700
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-ne v0, p1, :cond_0

    .line 207701
    invoke-static {p0, p1, p2}, LX/19P;->c(LX/19P;LX/2qU;LX/04g;)V

    .line 207702
    :cond_0
    invoke-static {p0, p1}, LX/19P;->j(LX/19P;LX/2qU;)V

    .line 207703
    invoke-virtual {p1, p2}, LX/2qU;->f(LX/04g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207704
    monitor-exit p0

    return-void

    .line 207705
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2qU;LX/04g;I)V
    .locals 1

    .prologue
    .line 207706
    monitor-enter p0

    .line 207707
    :try_start_0
    iget-object v0, p1, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207708
    iget-object v0, p1, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p3, p2}, LX/2q7;->a(ILX/04g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207709
    monitor-exit p0

    return-void

    .line 207710
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2qU;LX/04g;LX/7K4;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207711
    monitor-enter p0

    .line 207712
    :try_start_0
    iget-object v0, p0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 207713
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 207714
    const/4 v0, 0x1

    .line 207715
    :goto_0
    move v0, v0

    .line 207716
    if-eqz v0, :cond_6

    .line 207717
    iget-boolean v0, p0, LX/19P;->E:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/19P;->J:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/19P;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7I8;

    iget-boolean v0, v0, LX/7I8;->a:Z

    if-eqz v0, :cond_2

    .line 207718
    :cond_1
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-ne p2, v0, :cond_8

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    invoke-virtual {v0}, LX/2qU;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 207719
    if-nez v0, :cond_2

    .line 207720
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    invoke-virtual {v0}, LX/2qU;->h()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 207721
    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 207722
    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eqz v0, :cond_5

    .line 207723
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-ne v0, p1, :cond_4

    move v0, v1

    .line 207724
    :goto_4
    iget-object v1, p1, LX/2qU;->b:LX/2pu;

    invoke-virtual {v1, p2, v0}, LX/2pu;->a(LX/04g;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207725
    :goto_5
    monitor-exit p0

    return-void

    :cond_3
    move v0, v2

    .line 207726
    goto :goto_3

    :cond_4
    move v0, v2

    .line 207727
    goto :goto_4

    .line 207728
    :cond_5
    :try_start_1
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eq p1, v0, :cond_6

    .line 207729
    sget-object v0, LX/04g;->BY_MANAGER:LX/04g;

    invoke-static {p0, v0}, LX/19P;->b(LX/19P;LX/04g;)V

    .line 207730
    iput-object p1, p0, LX/19P;->A:LX/2qU;

    .line 207731
    invoke-static {p0}, LX/19P;->j(LX/19P;)V

    .line 207732
    :cond_6
    iget-object v0, p1, LX/2qU;->d:LX/2q7;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207733
    iget-object v0, p1, LX/2qU;->d:LX/2q7;

    invoke-interface {v0, p2, p3}, LX/2q7;->a(LX/04g;LX/7K4;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207734
    goto :goto_5

    .line 207735
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_2
    const/4 v0, 0x0

    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    :try_start_3
    const/4 v0, 0x0

    goto :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 207736
    iget-object v0, p0, LX/19P;->N:LX/04j;

    if-nez v0, :cond_1

    .line 207737
    invoke-static {p0}, LX/19P;->g(LX/19P;)V

    .line 207738
    :cond_0
    :goto_0
    return-void

    .line 207739
    :cond_1
    iget-object v0, p0, LX/19P;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yk;

    .line 207740
    iget-object v1, v0, LX/1Yk;->c:LX/0aq;

    invoke-virtual {v1, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-static {v1}, LX/1Yk;->b(Lcom/facebook/video/engine/VideoPlayerParams;)Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v1

    move-object v4, v1

    .line 207741
    if-eqz v4, :cond_0

    .line 207742
    iget-object v0, p0, LX/19P;->S:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 207743
    iget-object v0, p0, LX/19P;->S:LX/0aq;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207744
    iget-object v0, p0, LX/19P;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Yk;

    .line 207745
    iget-object v1, v0, LX/1Yk;->c:LX/0aq;

    invoke-virtual {v1, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v0, v1

    .line 207746
    if-nez v0, :cond_3

    .line 207747
    :goto_1
    iget-object v0, v4, Lcom/facebook/video/engine/VideoDataSource;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 207748
    :cond_2
    new-instance v0, Lcom/facebook/video/engine/VideoPlayerManager$2;

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/engine/VideoPlayerManager$2;-><init>(LX/19P;Ljava/lang/String;ZLcom/facebook/video/engine/VideoDataSource;Z)V

    .line 207749
    iget-object v1, p0, LX/19P;->q:Ljava/util/concurrent/ScheduledExecutorService;

    const v2, -0x2fc57132

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 207750
    :cond_3
    iget-boolean v3, v0, Lcom/facebook/video/engine/VideoPlayerParams;->n:Z

    goto :goto_1
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 207751
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/19P;->J:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207752
    monitor-exit p0

    return-void

    .line 207753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 207754
    iget-object v0, p0, LX/19P;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 207755
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2p8;

    .line 207756
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/2p8;->b()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 207757
    invoke-virtual {v0}, LX/2p8;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207758
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 207759
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 207760
    goto :goto_0

    .line 207761
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "num_players="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/19P;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\nnum_allocated_players="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\nnum_paused_frames="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, LX/19P;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\nnum_texture_views="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, LX/19P;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\nnum_360_video_surface_targets="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\nnum_regular_video_surface_targets="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nhas_active_player="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/19P;->A:LX/2qU;

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nin_fullscreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/19P;->D:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napi_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/19P;->A:LX/2qU;

    invoke-virtual {v0}, LX/2qU;->r()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    const-string v0, "unknown"

    goto :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_1
.end method

.method public final declared-synchronized b(LX/2qU;)V
    .locals 6

    .prologue
    .line 207762
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 207763
    iget v2, p1, LX/2qU;->c:I

    move v2, v2

    .line 207764
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 207765
    invoke-static {p0, p1}, LX/19P;->g(LX/19P;LX/2qU;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 207766
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 207767
    :goto_0
    monitor-exit p0

    return-void

    .line 207768
    :cond_0
    :try_start_1
    const-wide/16 v4, 0x3

    move-wide v0, v4

    .line 207769
    iget-object v2, p0, LX/19P;->b:Ljava/util/List;

    invoke-static {v2}, LX/13m;->a(Ljava/util/Collection;)V

    .line 207770
    iget-object v2, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v2, v0

    if-ltz v0, :cond_2

    .line 207771
    const/4 v0, 0x0

    .line 207772
    iget-object v1, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2qU;

    .line 207773
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 207774
    iget v4, v1, LX/2qU;->c:I

    move v4, v4

    .line 207775
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 207776
    iget-object v2, p0, LX/19P;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 207777
    sget-object v2, LX/04g;->BY_MANAGER:LX/04g;

    invoke-virtual {v1, v2}, LX/2qU;->b(LX/04g;)V

    .line 207778
    invoke-static {p0, v1}, LX/19P;->j(LX/19P;LX/2qU;)V

    .line 207779
    iget-object v2, p0, LX/19P;->B:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/19P;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v1, :cond_1

    .line 207780
    invoke-static {p0}, LX/19P;->j(LX/19P;)V

    .line 207781
    :cond_1
    iget-object v2, v1, LX/2qU;->d:LX/2q7;

    if-eqz v2, :cond_2

    .line 207782
    iget-object v2, v1, LX/2qU;->d:LX/2q7;

    invoke-interface {v2}, LX/2q7;->f()V

    .line 207783
    :cond_2
    iget-object v0, p0, LX/19P;->b:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 207784
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/2qU;LX/04g;)V
    .locals 1

    .prologue
    .line 207785
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/19P;->A:LX/2qU;

    if-ne v0, p1, :cond_0

    .line 207786
    invoke-static {p0, p1, p2}, LX/19P;->c(LX/19P;LX/2qU;LX/04g;)V

    .line 207787
    :cond_0
    invoke-static {p0, p1}, LX/19P;->j(LX/19P;LX/2qU;)V

    .line 207788
    invoke-virtual {p1, p2}, LX/2qU;->g(LX/04g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207789
    monitor-exit p0

    return-void

    .line 207790
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 207791
    iget-object v0, p0, LX/19P;->j:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 207792
    :goto_0
    return-void

    .line 207793
    :cond_0
    iget-object v1, p0, LX/19P;->j:Landroid/media/AudioManager;

    monitor-enter v1

    .line 207794
    :try_start_0
    iget-object v0, p0, LX/19P;->j:Landroid/media/AudioManager;

    iget-object v2, p0, LX/19P;->k:LX/1AB;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 207795
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d(LX/2qU;)I
    .locals 1

    .prologue
    .line 207796
    monitor-enter p0

    if-nez p1, :cond_0

    .line 207797
    const/4 v0, 0x0

    .line 207798
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, LX/2qU;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 207799
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 207800
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/19P;->E:Z

    .line 207801
    return-void
.end method
