.class public LX/0xv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public static final b:[Ljava/lang/String;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private static final c:Ljava/lang/String;

.field private static final d:[D

.field private static final e:[D


# instance fields
.field private final f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

.field private final g:LX/0SG;

.field private final h:LX/0y5;

.field private final i:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0y6;

.field private final k:LX/0ad;

.field private final l:LX/0yj;

.field private final m:LX/0yk;

.field public n:Z

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v1, 0x17

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 163790
    const-class v0, LX/0xv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0xv;->c:Ljava/lang/String;

    .line 163791
    new-array v0, v3, [D

    sput-object v0, LX/0xv;->d:[D

    .line 163792
    new-array v0, v1, [D

    sput-object v0, LX/0xv;->e:[D

    .line 163793
    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "client_has_seen"

    aput-object v1, v0, v4

    const-string v1, "cur_client_story_age_ms"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "image_cache_state"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "live_video_ended"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "connection_quality"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "is_offline"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_taken_in_last_n_minutes"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "video_cache_state"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "story_has_video"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "waiting_for_new_stories"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "story_has_downloaded_video"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "num_images_loaded"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "num_images_expected"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "attachment_text_is_loaded"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "has_attachment_text"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "attachment_media_cache_state"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "attachment_media_loaded"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "attachment_media_expected"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "attachment_link_cache_state"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "reaction_count"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "video_play_count"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "video_play_secs"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "live_comment_age_ms"

    aput-object v2, v0, v1

    sput-object v0, LX/0xv;->a:[Ljava/lang/String;

    .line 163794
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "weight_final"

    aput-object v1, v0, v4

    sput-object v0, LX/0xv;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0lB;LX/0SG;LX/0y5;LX/0y9;LX/0ad;LX/0yA;LX/0yB;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 163721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163722
    iput-boolean v0, p0, LX/0xv;->n:Z

    .line 163723
    iput-boolean v0, p0, LX/0xv;->o:Z

    .line 163724
    const-string v0, "default"

    sget-object v1, LX/0xv;->a:[Ljava/lang/String;

    sget-object v2, LX/0xv;->b:[Ljava/lang/String;

    .line 163725
    new-instance v3, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-direct {v3, p1, v0, v1, v2}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 163726
    move-object v0, v3

    .line 163727
    iput-object v0, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    .line 163728
    iput-object p4, p0, LX/0xv;->g:LX/0SG;

    .line 163729
    iput-object p5, p0, LX/0xv;->h:LX/0y5;

    .line 163730
    iget-object v0, p5, LX/0y5;->a:LX/0y6;

    move-object v0, v0

    .line 163731
    iput-object v0, p0, LX/0xv;->j:LX/0y6;

    .line 163732
    iput-object p7, p0, LX/0xv;->k:LX/0ad;

    .line 163733
    iget-object v0, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    .line 163734
    new-instance v3, LX/0yj;

    invoke-static {p8}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p8}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v2

    check-cast v2, LX/0lp;

    invoke-direct {v3, v0, v1, v2}, LX/0yj;-><init>(Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;LX/0SG;LX/0lp;)V

    .line 163735
    move-object v0, v3

    .line 163736
    iput-object v0, p0, LX/0xv;->l:LX/0yj;

    .line 163737
    iget-object v0, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    .line 163738
    new-instance v1, LX/0yk;

    invoke-direct {v1, v0}, LX/0yk;-><init>(Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;)V

    .line 163739
    move-object v0, v1

    .line 163740
    iput-object v0, p0, LX/0xv;->m:LX/0yk;

    .line 163741
    iget-object v0, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v0}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->shouldUseVectorInput()Z

    move-result v0

    iput-boolean v0, p0, LX/0xv;->o:Z

    .line 163742
    iget-boolean v0, p0, LX/0xv;->o:Z

    if-eqz v0, :cond_3

    .line 163743
    iget-object v0, p0, LX/0xv;->l:LX/0yj;

    .line 163744
    iget-object v1, v0, LX/0yj;->b:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v1}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->getRequiredFeatures()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0yj;->e:[Ljava/lang/String;

    .line 163745
    iget-object v1, v0, LX/0yj;->b:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v1}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->getClientFeatures()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2Ch;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, v0, LX/0yj;->f:Ljava/util/Set;

    .line 163746
    iget-object v1, v0, LX/0yj;->e:[Ljava/lang/String;

    iget-object v2, v0, LX/0yj;->f:Ljava/util/Set;

    .line 163747
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v0, LX/0yj;->g:Ljava/util/Map;

    .line 163748
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v0, LX/0yj;->h:Ljava/util/Map;

    .line 163749
    const/4 v3, 0x0

    :goto_0
    array-length p1, v1

    if-ge v3, p1, :cond_1

    .line 163750
    aget-object p1, v1, v3

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 163751
    aget-object p1, v1, v3

    invoke-static {p1}, LX/0yo;->getEnum(Ljava/lang/String;)LX/0yo;

    move-result-object p1

    .line 163752
    iget-object p4, v0, LX/0yj;->g:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-interface {p4, p1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163753
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 163754
    :cond_0
    iget-object p1, v0, LX/0yj;->h:Ljava/util/Map;

    aget-object p4, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-interface {p1, p4, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 163755
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, LX/0yj;->i:Ljava/util/Map;

    .line 163756
    iget-object v0, p0, LX/0xv;->m:LX/0yk;

    .line 163757
    iget-object v1, v0, LX/0yk;->a:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v1}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->getRequiredEvents()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/0yk;->b:[Ljava/lang/String;

    .line 163758
    iget-object v1, v0, LX/0yk;->b:[Ljava/lang/String;

    .line 163759
    new-instance v3, Ljava/util/HashMap;

    array-length v2, v1

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 163760
    const/4 v2, 0x0

    :goto_2
    array-length p1, v1

    if-ge v2, p1, :cond_2

    .line 163761
    aget-object p1, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-interface {v3, p1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163762
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 163763
    :cond_2
    move-object v1, v3

    .line 163764
    iput-object v1, v0, LX/0yk;->c:Ljava/util/Map;

    .line 163765
    iget-object v1, v0, LX/0yk;->b:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [D

    iput-object v1, v0, LX/0yk;->d:[D

    .line 163766
    :cond_3
    iget-object v0, p0, LX/0xv;->h:LX/0y5;

    iget-object v1, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v1}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->getClientFeatures()[Ljava/lang/String;

    move-result-object v1

    .line 163767
    :try_start_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 163768
    const-class v2, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    invoke-virtual {p3, p2, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163769
    :goto_3
    move-object v2, v2

    .line 163770
    iget-boolean v3, p0, LX/0xv;->o:Z

    const/4 p2, 0x0

    .line 163771
    iget-object p3, v0, LX/0y5;->a:LX/0y6;

    new-instance p4, LX/0yl;

    if-nez v3, :cond_5

    const/4 p1, 0x1

    :goto_4
    invoke-direct {p4, p1, v1, v2}, LX/0yl;-><init>(Z[Ljava/lang/String;Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;)V

    .line 163772
    iget-boolean p1, p3, LX/0y6;->c:Z

    if-eqz p1, :cond_8

    .line 163773
    :goto_5
    array-length p3, v1

    :goto_6
    if-ge p2, p3, :cond_6

    aget-object p1, v1, p2

    .line 163774
    invoke-static {p1}, LX/0yo;->getEnum(Ljava/lang/String;)LX/0yo;

    move-result-object p1

    .line 163775
    sget-object p4, LX/0yo;->LIVE_VIDEO_ENDED:LX/0yo;

    invoke-static {p1, p4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 163776
    iget-object p1, v0, LX/0y5;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/14u;

    new-instance p4, LX/6VM;

    invoke-direct {p4, v0}, LX/6VM;-><init>(LX/0y5;)V

    .line 163777
    iget-boolean v2, p1, LX/14u;->g:Z

    if-eqz v2, :cond_a

    .line 163778
    :cond_4
    :goto_7
    add-int/lit8 p2, p2, 0x1

    goto :goto_6

    :cond_5
    move p1, p2

    .line 163779
    goto :goto_4

    .line 163780
    :cond_6
    new-instance v0, LX/0aq;

    const/16 v1, 0xfa

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/0xv;->i:LX/0aq;

    .line 163781
    return-void

    .line 163782
    :catch_0
    move-exception v2

    .line 163783
    sget-object v3, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->a:Ljava/lang/Class;

    const-string p1, "Failed to parse config: %s"

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    const/4 p5, 0x0

    aput-object p2, p4, p5

    invoke-static {v3, v2, p1, p4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163784
    :cond_7
    new-instance v2, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    invoke-direct {v2}, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;-><init>()V

    goto :goto_3

    .line 163785
    :cond_8
    iget-object p1, p3, LX/0y6;->a:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0yn;

    .line 163786
    invoke-interface {p1, p4}, LX/0yn;->a(LX/0yl;)V

    goto :goto_8

    .line 163787
    :cond_9
    const/4 p1, 0x1

    iput-boolean p1, p3, LX/0y6;->c:Z

    goto :goto_5

    .line 163788
    :cond_a
    const/4 v2, 0x1

    iput-boolean v2, p1, LX/14u;->g:Z

    .line 163789
    iput-object p4, p1, LX/14u;->f:LX/6VM;

    goto :goto_7
.end method

.method public static a(LX/0xv;Lcom/facebook/feed/model/ClientFeedUnitEdge;ZLX/14r;)D
    .locals 12
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163629
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v2

    .line 163630
    iget-object v0, p0, LX/0xv;->h:LX/0y5;

    invoke-virtual {v0, v2}, LX/0y5;->b(Ljava/lang/String;)LX/14s;

    move-result-object v0

    .line 163631
    if-nez v0, :cond_0

    .line 163632
    iget-object v0, p0, LX/0xv;->h:LX/0y5;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, LX/0y5;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;I)LX/14s;

    move-result-object v0

    .line 163633
    if-nez v0, :cond_0

    .line 163634
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t create ClientRankingSignal for edge: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v1, v0

    .line 163635
    iget-object v0, p0, LX/0xv;->j:LX/0y6;

    .line 163636
    iget-object v3, v0, LX/0y6;->b:LX/0y8;

    move-object v4, v3

    .line 163637
    iget-object v0, p0, LX/0xv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 163638
    iget-wide v10, v1, LX/14s;->mFetchedAt:J

    move-wide v8, v10

    .line 163639
    sub-long/2addr v6, v8

    .line 163640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/32 v2, 0xea60

    div-long v2, v6, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 163641
    iget-object v0, p0, LX/0xv;->i:LX/0aq;

    invoke-virtual {v0, v5}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 163642
    iget-boolean v2, v1, LX/14s;->a:Z

    move v2, v2

    .line 163643
    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    if-eqz p2, :cond_2

    .line 163644
    :cond_1
    sget-object v0, LX/0xv;->d:[D

    const/4 v2, 0x0

    .line 163645
    iget-wide v10, v1, LX/14s;->mRankingWeight:D

    move-wide v8, v10

    .line 163646
    aput-wide v8, v0, v2

    .line 163647
    sget-object v0, LX/0xv;->e:[D

    const/4 v8, 0x0

    .line 163648
    iget v2, v1, LX/14s;->mSeenState:I

    move v2, v2

    .line 163649
    if-nez v2, :cond_3

    const-wide/16 v2, 0x0

    :goto_0
    aput-wide v2, v0, v8

    .line 163650
    sget-object v0, LX/0xv;->e:[D

    const/4 v2, 0x1

    long-to-double v6, v6

    aput-wide v6, v0, v2

    .line 163651
    sget-object v0, LX/0xv;->e:[D

    const/4 v2, 0x2

    .line 163652
    iget v3, v1, LX/14s;->mImageCacheState:I

    move v3, v3

    .line 163653
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163654
    sget-object v0, LX/0xv;->e:[D

    const/4 v6, 0x3

    .line 163655
    iget v2, v1, LX/14s;->mLiveVideoState:I

    move v2, v2

    .line 163656
    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_1
    aput-wide v2, v0, v6

    .line 163657
    sget-object v0, LX/0xv;->e:[D

    const/4 v2, 0x4

    invoke-virtual {v4}, LX/0y8;->a()LX/0p3;

    move-result-object v3

    invoke-virtual {v3}, LX/0p3;->ordinal()I

    move-result v3

    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163658
    sget-object v0, LX/0xv;->e:[D

    const/4 v6, 0x5

    invoke-virtual {v4}, LX/0y8;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    const-wide/16 v2, 0x0

    :goto_2
    aput-wide v2, v0, v6

    .line 163659
    sget-object v0, LX/0xv;->e:[D

    const/4 v6, 0x6

    invoke-virtual {v4}, LX/0y8;->c()Z

    move-result v2

    if-eqz v2, :cond_6

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_3
    aput-wide v2, v0, v6

    .line 163660
    sget-object v0, LX/0xv;->e:[D

    const/16 v6, 0x9

    invoke-virtual {v4}, LX/0y8;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_4
    aput-wide v2, v0, v6

    .line 163661
    sget-object v0, LX/0xv;->e:[D

    const/4 v2, 0x7

    .line 163662
    iget v3, v1, LX/14s;->mVideoCacheState:I

    move v3, v3

    .line 163663
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163664
    sget-object v0, LX/0xv;->e:[D

    const/16 v6, 0x8

    .line 163665
    iget-boolean v2, v1, LX/14s;->mStoryHasVideo:Z

    move v2, v2

    .line 163666
    if-eqz v2, :cond_8

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_5
    aput-wide v2, v0, v6

    .line 163667
    sget-object v0, LX/0xv;->e:[D

    const/16 v6, 0xa

    .line 163668
    iget-boolean v2, v1, LX/14s;->mStoryHasDownloadedVideo:Z

    move v2, v2

    .line 163669
    if-eqz v2, :cond_9

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_6
    aput-wide v2, v0, v6

    .line 163670
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0xb

    .line 163671
    iget v3, v1, LX/14s;->mImagesLoaded:I

    move v3, v3

    .line 163672
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163673
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0xc

    .line 163674
    iget v3, v1, LX/14s;->mImagesExpected:I

    move v3, v3

    .line 163675
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163676
    sget-object v0, LX/0xv;->e:[D

    const/16 v6, 0xd

    .line 163677
    iget-boolean v2, v1, LX/14s;->mIsAttachmentTextLoaded:Z

    move v2, v2

    .line 163678
    if-eqz v2, :cond_a

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_7
    aput-wide v2, v0, v6

    .line 163679
    sget-object v0, LX/0xv;->e:[D

    const/16 v6, 0xe

    .line 163680
    iget-boolean v2, v1, LX/14s;->mHasAttachmentText:Z

    move v2, v2

    .line 163681
    if-eqz v2, :cond_b

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_8
    aput-wide v2, v0, v6

    .line 163682
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0xf

    .line 163683
    iget v3, v1, LX/14s;->mAttachmentMediaCacheState:I

    move v3, v3

    .line 163684
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163685
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x11

    .line 163686
    iget v3, v1, LX/14s;->mAttachmentMediaExpected:I

    move v3, v3

    .line 163687
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163688
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x10

    .line 163689
    iget v3, v1, LX/14s;->mAttachmentMediaLoaded:I

    move v3, v3

    .line 163690
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163691
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x12

    .line 163692
    iget v3, v1, LX/14s;->mLinkCacheState:I

    move v3, v3

    .line 163693
    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163694
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x13

    invoke-virtual {v4}, LX/0y8;->e()I

    move-result v3

    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163695
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x14

    invoke-virtual {v4}, LX/0y8;->f()I

    move-result v3

    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163696
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x15

    invoke-virtual {v4}, LX/0y8;->g()I

    move-result v3

    int-to-double v6, v3

    aput-wide v6, v0, v2

    .line 163697
    sget-object v0, LX/0xv;->e:[D

    const/16 v2, 0x16

    .line 163698
    iget-wide v10, v1, LX/14s;->mLiveCommentAgeMs:J

    move-wide v6, v10

    .line 163699
    long-to-double v6, v6

    aput-wide v6, v0, v2

    .line 163700
    iget-object v2, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    sget-object v3, LX/0xv;->d:[D

    sget-object v4, LX/0xv;->e:[D

    .line 163701
    iget-object v0, v1, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    move-object v0, v0

    .line 163702
    if-nez v0, :cond_c

    const-string v0, ""

    :goto_9
    invoke-virtual {v2, v3, v4, v0}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->a([D[DLjava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 163703
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 163704
    iput-wide v2, v1, LX/14s;->mClientWeight:D

    .line 163705
    invoke-virtual {v1}, LX/14s;->i()V

    .line 163706
    iget-object v1, p0, LX/0xv;->i:LX/0aq;

    invoke-virtual {v1, v5, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163707
    if-eqz p3, :cond_2

    .line 163708
    const/4 v1, 0x1

    iput v1, p3, LX/14r;->a:I

    .line 163709
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0

    .line 163710
    :cond_3
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/16 :goto_0

    .line 163711
    :cond_4
    const-wide/16 v2, 0x0

    goto/16 :goto_1

    .line 163712
    :cond_5
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/16 :goto_2

    .line 163713
    :cond_6
    const-wide/16 v2, 0x0

    goto/16 :goto_3

    .line 163714
    :cond_7
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    .line 163715
    :cond_8
    const-wide/16 v2, 0x0

    goto/16 :goto_5

    .line 163716
    :cond_9
    const-wide/16 v2, 0x0

    goto/16 :goto_6

    .line 163717
    :cond_a
    const-wide/16 v2, 0x0

    goto/16 :goto_7

    .line 163718
    :cond_b
    const-wide/16 v2, 0x0

    goto/16 :goto_8

    .line 163719
    :cond_c
    iget-object v0, v1, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    move-object v0, v0

    .line 163720
    goto :goto_9
.end method

.method public static a(LX/0xv;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 163795
    iget-object v1, p0, LX/0xv;->j:LX/0y6;

    .line 163796
    iget-boolean v2, v1, LX/0y6;->c:Z

    if-nez v2, :cond_3

    .line 163797
    :cond_0
    iget-boolean v1, p0, LX/0xv;->n:Z

    if-nez v1, :cond_1

    iget-object v1, p0, LX/0xv;->j:LX/0y6;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 163798
    iget-boolean v2, v1, LX/0y6;->c:Z

    if-nez v2, :cond_4

    move v2, v3

    .line 163799
    :goto_0
    move v1, v2

    .line 163800
    if-eqz v1, :cond_2

    .line 163801
    :cond_1
    iget-object v1, p0, LX/0xv;->j:LX/0y6;

    .line 163802
    iget-boolean v2, v1, LX/0y6;->c:Z

    if-nez v2, :cond_8

    .line 163803
    :goto_1
    iput-boolean v0, p0, LX/0xv;->n:Z

    .line 163804
    const/4 v0, 0x1

    .line 163805
    :cond_2
    return v0

    .line 163806
    :cond_3
    iget-object v2, v1, LX/0y6;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yn;

    .line 163807
    invoke-interface {v2}, LX/0yn;->a()V

    goto :goto_2

    .line 163808
    :cond_4
    iget-boolean v2, v1, LX/0y6;->d:Z

    if-nez v2, :cond_5

    move v2, v4

    .line 163809
    goto :goto_0

    .line 163810
    :cond_5
    iget-object v2, v1, LX/0y6;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yn;

    .line 163811
    invoke-interface {v2}, LX/0yn;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v4

    .line 163812
    goto :goto_0

    :cond_7
    move v2, v3

    .line 163813
    goto :goto_0

    .line 163814
    :cond_8
    iget-object v2, v1, LX/0y6;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yn;

    .line 163815
    iget-object v4, v1, LX/0y6;->b:LX/0y8;

    invoke-interface {v2, v4}, LX/0yn;->a(LX/0y8;)V

    goto :goto_3

    .line 163816
    :cond_9
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/0y6;->d:Z

    goto :goto_1
.end method

.method public static b(LX/0xv;Lcom/facebook/feed/model/ClientFeedUnitEdge;ZLX/14r;)D
    .locals 10
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163599
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v1

    .line 163600
    iget-object v0, p0, LX/0xv;->h:LX/0y5;

    invoke-virtual {v0, p1}, LX/0y5;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/14s;

    move-result-object v2

    .line 163601
    iget-object v0, p0, LX/0xv;->j:LX/0y6;

    .line 163602
    iget-object v3, v0, LX/0y6;->b:LX/0y8;

    move-object v3, v3

    .line 163603
    iget-object v0, p0, LX/0xv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 163604
    iget-wide v8, v2, LX/14s;->mFetchedAt:J

    move-wide v6, v8

    .line 163605
    sub-long/2addr v4, v6

    .line 163606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 163607
    iget-object v0, p0, LX/0xv;->i:LX/0aq;

    invoke-virtual {v0, v4}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 163608
    iget-boolean v5, v2, LX/14s;->a:Z

    move v5, v5

    .line 163609
    if-nez v5, :cond_0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    .line 163610
    :cond_0
    iget-object v0, p0, LX/0xv;->l:LX/0yj;

    .line 163611
    iget-object v5, v0, LX/0yj;->i:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 163612
    iget-object v5, v0, LX/0yj;->i:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [D

    .line 163613
    invoke-static {v0, v5, v2, v3}, LX/0yj;->a(LX/0yj;[DLX/14s;LX/0y8;)V

    .line 163614
    :goto_0
    move-object v0, v5

    .line 163615
    iget-object v1, p0, LX/0xv;->m:LX/0yk;

    invoke-virtual {v1, v2}, LX/0yk;->a(LX/14s;)[D

    move-result-object v1

    .line 163616
    iget-object v3, p0, LX/0xv;->f:Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;

    invoke-virtual {v3, v1, v0}, Lcom/facebook/multifeed/ranking/core/value_model/ClientValueModelHolder;->a([D[D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 163617
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 163618
    iput-wide v6, v2, LX/14s;->mClientWeight:D

    .line 163619
    invoke-virtual {v2}, LX/14s;->i()V

    .line 163620
    iget-object v1, p0, LX/0xv;->i:LX/0aq;

    invoke-virtual {v1, v4, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163621
    if-eqz p3, :cond_1

    .line 163622
    const/4 v1, 0x1

    iput v1, p3, LX/14r;->a:I

    .line 163623
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0

    .line 163624
    :cond_2
    iget-object v5, v0, LX/0yj;->e:[Ljava/lang/String;

    array-length v5, v5

    new-array v5, v5, [D

    .line 163625
    invoke-static {v0, v5, v2, v3}, LX/0yj;->a(LX/0yj;[DLX/14s;LX/0y8;)V

    .line 163626
    iget-object v6, v2, LX/14s;->mFeaturesMeta:Ljava/lang/String;

    move-object v6, v6

    .line 163627
    invoke-static {v0, v5, v6}, LX/0yj;->a(LX/0yj;[DLjava/lang/String;)V

    .line 163628
    iget-object v6, v0, LX/0yj;->i:Ljava/util/Map;

    invoke-interface {v6, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private declared-synchronized b(Ljava/util/List;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 163585
    monitor-enter p0

    :try_start_0
    const/4 v3, 0x0

    .line 163586
    invoke-static {p0}, LX/0xv;->a(LX/0xv;)Z

    move-result v4

    .line 163587
    new-instance v5, LX/14r;

    invoke-direct {v5, v3}, LX/14r;-><init>(I)V

    .line 163588
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 163589
    iput v3, v5, LX/14r;->a:I

    .line 163590
    invoke-static {p0, v1, v4, v5}, LX/0xv;->a(LX/0xv;Lcom/facebook/feed/model/ClientFeedUnitEdge;ZLX/14r;)D

    move-result-wide v7

    .line 163591
    iget v9, v5, LX/14r;->a:I

    if-eqz v9, :cond_0

    .line 163592
    add-int/lit8 v2, v2, 0x1

    .line 163593
    :cond_0
    iput-wide v7, v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F:D

    .line 163594
    goto :goto_0

    .line 163595
    :cond_1
    move v0, v2

    .line 163596
    invoke-static {p1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163597
    monitor-exit p0

    return v0

    .line 163598
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/List;)I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 163567
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0xv;->o:Z

    if-nez v0, :cond_0

    .line 163568
    invoke-direct {p0, p1}, LX/0xv;->b(Ljava/util/List;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 163569
    :goto_0
    monitor-exit p0

    return v0

    .line 163570
    :cond_0
    :try_start_1
    const/4 v4, 0x0

    .line 163571
    invoke-static {p0}, LX/0xv;->a(LX/0xv;)Z

    move-result v5

    .line 163572
    new-instance v6, LX/14r;

    invoke-direct {v6, v4}, LX/14r;-><init>(I)V

    .line 163573
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 163574
    iput v4, v6, LX/14r;->a:I

    .line 163575
    invoke-static {p0, v2, v5, v6}, LX/0xv;->b(LX/0xv;Lcom/facebook/feed/model/ClientFeedUnitEdge;ZLX/14r;)D

    move-result-wide v8

    .line 163576
    iget v10, v6, LX/14r;->a:I

    if-eqz v10, :cond_1

    .line 163577
    add-int/lit8 v3, v3, 0x1

    .line 163578
    :cond_1
    iput-wide v8, v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F:D

    .line 163579
    goto :goto_1

    .line 163580
    :cond_2
    move v1, v3

    .line 163581
    invoke-static {p1, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 163582
    move v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163583
    goto :goto_0

    .line 163584
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 163555
    check-cast p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    check-cast p2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 163556
    iget-wide v4, p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F:D

    move-wide v0, v4

    .line 163557
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 163558
    iget-wide v4, p2, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F:D

    move-wide v2, v4

    .line 163559
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 163560
    if-nez v0, :cond_0

    .line 163561
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Memoized Ranking score was null for edge: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163562
    :cond_0
    if-nez v1, :cond_1

    .line 163563
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Memoized Ranking score was null for edge: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163564
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 163565
    const/4 v0, 0x0

    .line 163566
    :goto_0
    return v0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpg-double v0, v2, v0

    if-gez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method
