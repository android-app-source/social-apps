.class public abstract LX/0bL;
.super LX/0b1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/0b5;",
        ">",
        "LX/0b1",
        "<TT;",
        "LX/GwY;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/0b4;LX/0Ot;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/0b4;",
            "LX/0Ot",
            "<",
            "LX/GwY;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 86807
    invoke-direct {p0, p2, p3}, LX/0b1;-><init>(LX/0b4;LX/0Ot;)V

    .line 86808
    iput-object p1, p0, LX/0bL;->a:Ljava/lang/Class;

    .line 86809
    iput-boolean p4, p0, LX/0bL;->b:Z

    .line 86810
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86811
    iget-object v0, p0, LX/0bL;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public final a(LX/0b7;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 86812
    check-cast p1, LX/0b5;

    check-cast p2, LX/GwY;

    .line 86813
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 86814
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v1

    .line 86815
    iget-boolean v1, p0, LX/0bL;->b:Z

    .line 86816
    new-instance v2, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/facebook/katana/platform/handler/GetPendingAppCallForMediaUploadOperation$Params;-><init>(Ljava/lang/String;Z)V

    .line 86817
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 86818
    const-string p0, "platform_get_app_call_for_pending_upload_params"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86819
    iget-object v2, p2, LX/GwY;->b:LX/0aG;

    const-string p0, "platform_get_app_call_for_pending_upload"

    const p1, -0x18ecea26

    invoke-static {v2, p0, v3, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 86820
    move-object v2, v2

    .line 86821
    new-instance v3, LX/GwX;

    invoke-direct {v3, p2, v1}, LX/GwX;-><init>(LX/GwY;Z)V

    iget-object p0, p2, LX/GwY;->f:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 86822
    return-void
.end method
