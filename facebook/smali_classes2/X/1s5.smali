.class public LX/1s5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1s5;


# instance fields
.field public final a:LX/1s6;

.field public final b:LX/1s9;


# direct methods
.method public constructor <init>(LX/1s6;LX/1s9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 333695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333696
    iput-object p1, p0, LX/1s5;->a:LX/1s6;

    .line 333697
    iput-object p2, p0, LX/1s5;->b:LX/1s9;

    .line 333698
    return-void
.end method

.method public static a(LX/0QB;)LX/1s5;
    .locals 5

    .prologue
    .line 333699
    sget-object v0, LX/1s5;->c:LX/1s5;

    if-nez v0, :cond_1

    .line 333700
    const-class v1, LX/1s5;

    monitor-enter v1

    .line 333701
    :try_start_0
    sget-object v0, LX/1s5;->c:LX/1s5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 333702
    if-eqz v2, :cond_0

    .line 333703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 333704
    new-instance p0, LX/1s5;

    invoke-static {v0}, LX/1s6;->a(LX/0QB;)LX/1s6;

    move-result-object v3

    check-cast v3, LX/1s6;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v4

    check-cast v4, LX/1s9;

    invoke-direct {p0, v3, v4}, LX/1s5;-><init>(LX/1s6;LX/1s9;)V

    .line 333705
    move-object v0, p0

    .line 333706
    sput-object v0, LX/1s5;->c:LX/1s5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333707
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 333708
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 333709
    :cond_1
    sget-object v0, LX/1s5;->c:LX/1s5;

    return-object v0

    .line 333710
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 333711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0m9;Ljava/lang/String;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m9;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333712
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 333713
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333714
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 333715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 333716
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 333717
    return-void
.end method
