.class public LX/1tu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 336867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1tu;
    .locals 1

    .prologue
    .line 336868
    new-instance v0, LX/1tu;

    invoke-direct {v0}, LX/1tu;-><init>()V

    .line 336869
    move-object v0, v0

    .line 336870
    return-object v0
.end method


# virtual methods
.method public final a(J)Z
    .locals 5

    .prologue
    .line 336871
    sget-object v1, LX/1tp;->VOIP:LX/1tp;

    invoke-static {v1}, LX/1tz;->a(Ljava/lang/Enum;)J

    move-result-wide v1

    and-long/2addr v1, p1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 336872
    if-nez v0, :cond_0

    .line 336873
    sget-object v1, LX/1tp;->VOIP_WEB:LX/1tp;

    invoke-static {v1}, LX/1tz;->a(Ljava/lang/Enum;)J

    move-result-wide v1

    and-long/2addr v1, p1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 336874
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
