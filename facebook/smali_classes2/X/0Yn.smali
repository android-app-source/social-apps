.class public LX/0Yn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0ak;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0ak;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 82428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82429
    iput-object p1, p0, LX/0Yn;->a:LX/0ad;

    .line 82430
    new-instance v0, LX/0aj;

    invoke-direct {v0}, LX/0aj;-><init>()V

    move-object v0, v0

    .line 82431
    new-instance p1, LX/0al;

    invoke-direct {p1}, LX/0al;-><init>()V

    move-object p1, p1

    .line 82432
    invoke-static {v0, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->b:Ljava/util/List;

    .line 82433
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->c:LX/0am;

    .line 82434
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->d:LX/0am;

    .line 82435
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->e:LX/0am;

    .line 82436
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->f:LX/0am;

    .line 82437
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->g:LX/0am;

    .line 82438
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->h:LX/0am;

    .line 82439
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->i:LX/0am;

    .line 82440
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->j:LX/0am;

    .line 82441
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->k:LX/0am;

    .line 82442
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->l:LX/0am;

    .line 82443
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->m:LX/0am;

    .line 82444
    return-void
.end method

.method public static a(LX/0QB;)LX/0Yn;
    .locals 4

    .prologue
    .line 82445
    const-class v1, LX/0Yn;

    monitor-enter v1

    .line 82446
    :try_start_0
    sget-object v0, LX/0Yn;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 82447
    sput-object v2, LX/0Yn;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 82448
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82449
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 82450
    new-instance p0, LX/0Yn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/0Yn;-><init>(LX/0ad;)V

    .line 82451
    move-object v0, p0

    .line 82452
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 82453
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/0Yn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82454
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 82455
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static e(LX/0Yn;)LX/0ak;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 82456
    iget-object v0, p0, LX/0Yn;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82457
    iget-object v0, p0, LX/0Yn;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ak;

    .line 82458
    :goto_0
    return-object v0

    .line 82459
    :cond_0
    iget-object v0, p0, LX/0Yn;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ak;

    .line 82460
    iget-object v2, p0, LX/0Yn;->a:LX/0ad;

    invoke-interface {v0}, LX/0ak;->a()S

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82461
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->c:LX/0am;

    .line 82462
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/0Yn;->d:LX/0am;

    goto :goto_1

    .line 82463
    :cond_2
    iget-object v0, p0, LX/0Yn;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ak;

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 82464
    iget-object v0, p0, LX/0Yn;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82465
    invoke-static {p0}, LX/0Yn;->e(LX/0Yn;)LX/0ak;

    .line 82466
    :cond_0
    iget-object v0, p0, LX/0Yn;->d:LX/0am;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 82467
    const-string v0, "NULL"

    .line 82468
    iget-object v1, p0, LX/0Yn;->g:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82469
    iget-object v1, p0, LX/0Yn;->a:LX/0ad;

    invoke-static {p0}, LX/0Yn;->e(LX/0Yn;)LX/0ak;

    move-result-object v2

    invoke-interface {v2}, LX/0ak;->b()C

    move-result v2

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/0Yn;->g:LX/0am;

    .line 82470
    :cond_0
    iget-object v1, p0, LX/0Yn;->g:LX/0am;

    invoke-virtual {v1, v0}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
