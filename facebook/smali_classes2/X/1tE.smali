.class public final enum LX/1tE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1tE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1tE;

.field public static final enum NO:LX/1tE;

.field public static final enum UNSURE:LX/1tE;

.field public static final enum YES:LX/1tE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 336108
    new-instance v0, LX/1tE;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2}, LX/1tE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tE;->YES:LX/1tE;

    .line 336109
    new-instance v0, LX/1tE;

    const-string v1, "NO"

    invoke-direct {v0, v1, v3}, LX/1tE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tE;->NO:LX/1tE;

    .line 336110
    new-instance v0, LX/1tE;

    const-string v1, "UNSURE"

    invoke-direct {v0, v1, v4}, LX/1tE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1tE;->UNSURE:LX/1tE;

    .line 336111
    const/4 v0, 0x3

    new-array v0, v0, [LX/1tE;

    sget-object v1, LX/1tE;->YES:LX/1tE;

    aput-object v1, v0, v2

    sget-object v1, LX/1tE;->NO:LX/1tE;

    aput-object v1, v0, v3

    sget-object v1, LX/1tE;->UNSURE:LX/1tE;

    aput-object v1, v0, v4

    sput-object v0, LX/1tE;->$VALUES:[LX/1tE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 336114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1tE;
    .locals 1

    .prologue
    .line 336113
    const-class v0, LX/1tE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1tE;

    return-object v0
.end method

.method public static values()[LX/1tE;
    .locals 1

    .prologue
    .line 336112
    sget-object v0, LX/1tE;->$VALUES:[LX/1tE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1tE;

    return-object v0
.end method
