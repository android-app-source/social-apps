.class public abstract LX/0lS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0lJ;


# direct methods
.method public constructor <init>(LX/0lJ;)V
    .locals 0

    .prologue
    .line 129497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129498
    iput-object p1, p0, LX/0lS;->a:LX/0lJ;

    .line 129499
    return-void
.end method


# virtual methods
.method public final a()LX/0lJ;
    .locals 1

    .prologue
    .line 129500
    iget-object v0, p0, LX/0lS;->a:LX/0lJ;

    return-object v0
.end method

.method public abstract a(Ljava/lang/reflect/Type;)LX/0lJ;
.end method

.method public abstract a(LX/0nr;)LX/0nr;
.end method

.method public abstract a(Ljava/lang/String;[Ljava/lang/Class;)LX/2At;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/2At;"
        }
    .end annotation
.end method

.method public abstract a(LX/4pN;)LX/4pN;
.end method

.method public abstract a(Z)Ljava/lang/Object;
.end method

.method public varargs abstract a([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 129501
    iget-object v0, p0, LX/0lS;->a:LX/0lJ;

    .line 129502
    iget-object p0, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, p0

    .line 129503
    return-object v0
.end method

.method public varargs abstract b([Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation
.end method

.method public abstract c()LX/0lN;
.end method

.method public abstract d()LX/4qt;
.end method

.method public abstract e()Z
.end method

.method public abstract f()LX/1Y3;
.end method

.method public abstract g()LX/0lQ;
.end method

.method public abstract h()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Aq;",
            ">;"
        }
    .end annotation
.end method

.method public abstract i()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2An;",
            ">;"
        }
    .end annotation
.end method

.method public abstract j()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract k()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2Vc;",
            ">;"
        }
    .end annotation
.end method

.method public abstract l()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2At;",
            ">;"
        }
    .end annotation
.end method

.method public abstract m()LX/2Vc;
.end method

.method public abstract n()LX/2An;
.end method

.method public abstract o()LX/2At;
.end method

.method public abstract p()LX/2At;
.end method

.method public abstract q()LX/1Xr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract r()LX/1Xr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract s()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LX/2An;",
            ">;"
        }
    .end annotation
.end method

.method public abstract t()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract u()LX/2zN;
.end method
