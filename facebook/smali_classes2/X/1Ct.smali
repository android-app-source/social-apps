.class public LX/1Ct;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1Ce;
.implements LX/0hk;
.implements LX/0yL;
.implements LX/0fs;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public a:LX/0yc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1Cu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/18Q;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/1Pq;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 217008
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 217009
    return-void
.end method

.method public static b(LX/0QB;)LX/1Ct;
    .locals 14

    .prologue
    .line 217010
    new-instance v4, LX/1Ct;

    invoke-direct {v4}, LX/1Ct;-><init>()V

    .line 217011
    invoke-static {p0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v0

    check-cast v0, LX/0yc;

    .line 217012
    new-instance v5, LX/1Cu;

    const/16 v6, 0x3039

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xcbb

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/1Cx;->a(LX/0QB;)LX/1Cx;

    move-result-object v8

    check-cast v8, LX/1Cx;

    invoke-static {p0}, LX/0qb;->a(LX/0QB;)LX/0qb;

    move-result-object v9

    check-cast v9, LX/0qb;

    invoke-static {p0}, LX/14y;->a(LX/0QB;)LX/14z;

    move-result-object v10

    check-cast v10, LX/14z;

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v11

    check-cast v11, LX/0Wd;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    const/16 v13, 0x3038

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v5 .. v13}, LX/1Cu;-><init>(LX/0Ot;LX/0Ot;LX/1Cx;LX/0qb;LX/14z;LX/0Wd;LX/0Sh;LX/0Ot;)V

    .line 217013
    move-object v1, v5

    .line 217014
    check-cast v1, LX/1Cu;

    invoke-static {p0}, LX/18Q;->a(LX/0QB;)LX/18Q;

    move-result-object v2

    check-cast v2, LX/18Q;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 217015
    iput-object v0, v4, LX/1Ct;->a:LX/0yc;

    iput-object v1, v4, LX/1Ct;->b:LX/1Cu;

    iput-object v2, v4, LX/1Ct;->c:LX/18Q;

    iput-object v3, v4, LX/1Ct;->d:LX/0ad;

    .line 217016
    return-object v4
.end method


# virtual methods
.method public final a(LX/01J;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01J",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217017
    return-void
.end method

.method public final a(LX/0g8;)V
    .locals 0

    .prologue
    .line 217018
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 217019
    return-void
.end method

.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 0

    .prologue
    .line 217020
    return-void
.end method

.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 2

    .prologue
    .line 217003
    iput-object p3, p0, LX/1Ct;->e:LX/1Pq;

    .line 217004
    iget-object v0, p0, LX/1Ct;->a:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 217005
    iget-object v0, p0, LX/1Ct;->c:LX/18Q;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 217006
    iget-object p1, v0, LX/18Q;->g:LX/0Xu;

    invoke-interface {p1, v1, p0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 217007
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 217021
    iget-object v0, p0, LX/1Ct;->b:LX/1Cu;

    iget-object v0, v0, LX/1Cu;->a:LX/1Cz;

    if-eq p1, v0, :cond_1

    .line 217022
    :cond_0
    :goto_0
    return-void

    .line 217023
    :cond_1
    iget-object v0, p0, LX/1Ct;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 217024
    iget-object v0, p0, LX/1Ct;->d:LX/0ad;

    sget-short v1, LX/0rI;->h:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1Ct;->f:Ljava/lang/Boolean;

    .line 217025
    :cond_2
    iget-object v0, p0, LX/1Ct;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 217026
    if-eqz v0, :cond_0

    .line 217027
    iget-object v0, p0, LX/1Ct;->b:LX/1Cu;

    .line 217028
    iget-boolean v1, v0, LX/1Cu;->u:Z

    if-eqz v1, :cond_4

    .line 217029
    :cond_3
    :goto_1
    goto :goto_0

    .line 217030
    :cond_4
    invoke-static {v0}, LX/1Cu;->g(LX/1Cu;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 217031
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1Cu;->u:Z

    .line 217032
    iget-object v1, v0, LX/1Cu;->p:LX/0Wd;

    new-instance p0, Lcom/facebook/feed/megaphone/FeedMegaphoneAdapter$11;

    invoke-direct {p0, v0}, Lcom/facebook/feed/megaphone/FeedMegaphoneAdapter$11;-><init>(LX/1Cu;)V

    invoke-interface {v1, p0}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method

.method public final b(LX/0g8;)V
    .locals 0

    .prologue
    .line 217002
    return-void
.end method

.method public final b(LX/0g8;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 217001
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 216979
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 216990
    iget-object v0, p0, LX/1Ct;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 216991
    iget-object v0, p0, LX/1Ct;->d:LX/0ad;

    sget-short v1, LX/0rI;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1Ct;->g:Ljava/lang/Boolean;

    .line 216992
    :cond_0
    iget-object v0, p0, LX/1Ct;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 216993
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1Ct;->b:LX/1Cu;

    .line 216994
    iget-object v1, v0, LX/1Cu;->t:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/1Cu;->m:LX/0qb;

    iget-object v2, v0, LX/1Cu;->l:LX/1Cx;

    sget-object v3, LX/0rH;->NEWS_FEED:LX/0rH;

    invoke-virtual {v1, v2, v3}, LX/0qb;->b(LX/0qg;LX/0rH;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 216995
    if-nez v0, :cond_1

    .line 216996
    iget-object v0, p0, LX/1Ct;->b:LX/1Cu;

    .line 216997
    iget-boolean v1, v0, LX/1Cu;->u:Z

    if-eqz v1, :cond_3

    .line 216998
    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 216999
    :cond_3
    invoke-virtual {v0}, LX/1Cu;->c()V

    .line 217000
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1Cu;->u:Z

    goto :goto_1
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 216989
    return-void
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 216986
    iget-object v0, p0, LX/1Ct;->b:LX/1Cu;

    invoke-virtual {v0}, LX/1Cu;->c()V

    .line 216987
    iget-object v0, p0, LX/1Ct;->e:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 216988
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 216985
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 216980
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Ct;->e:LX/1Pq;

    .line 216981
    iget-object v0, p0, LX/1Ct;->a:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->b(LX/0yL;)V

    .line 216982
    iget-object v0, p0, LX/1Ct;->c:LX/18Q;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->NEWSFEED:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 216983
    iget-object v2, v0, LX/18Q;->g:LX/0Xu;

    invoke-interface {v2, v1, p0}, LX/0Xu;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 216984
    return-void
.end method
