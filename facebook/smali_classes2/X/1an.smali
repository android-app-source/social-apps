.class public LX/1an;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 278419
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1ai;)LX/1ai;
    .locals 2

    .prologue
    .line 278368
    :goto_0
    invoke-interface {p0}, LX/1ai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 278369
    if-eq v0, p0, :cond_0

    instance-of v1, v0, LX/1ai;

    if-eqz v1, :cond_0

    .line 278370
    check-cast v0, LX/1ai;

    move-object p0, v0

    .line 278371
    goto :goto_0

    .line 278372
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;LX/1Up;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 278417
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/1an;->a(Landroid/graphics/drawable/Drawable;LX/1Up;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;LX/1Up;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 278411
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 278412
    :cond_0
    :goto_0
    return-object p0

    .line 278413
    :cond_1
    new-instance v0, LX/1ao;

    invoke-direct {v0, p0, p1}, LX/1ao;-><init>(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 278414
    if-eqz p2, :cond_2

    .line 278415
    invoke-virtual {v0, p2}, LX/1ao;->a(Landroid/graphics/PointF;)V

    :cond_2
    move-object p0, v0

    .line 278416
    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;LX/4Ab;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/4Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278402
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 278403
    iget-object v0, p1, LX/4Ab;->a:LX/4Aa;

    move-object v0, v0

    .line 278404
    sget-object v1, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    if-eq v0, v1, :cond_1

    .line 278405
    :cond_0
    :goto_0
    return-object p0

    .line 278406
    :cond_1
    new-instance v0, LX/1oL;

    invoke-direct {v0, p0}, LX/1oL;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 278407
    invoke-static {v0, p1}, LX/1an;->a(LX/1oM;LX/4Ab;)V

    .line 278408
    iget v1, p1, LX/4Ab;->d:I

    move v1, v1

    .line 278409
    invoke-virtual {v0, v1}, LX/1oL;->a(I)V

    move-object p0, v0

    .line 278410
    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/4Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 278392
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 278393
    iget-object v0, p1, LX/4Ab;->a:LX/4Aa;

    move-object v0, v0

    .line 278394
    sget-object v1, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    if-eq v0, v1, :cond_1

    .line 278395
    :cond_0
    :goto_0
    return-object p0

    .line 278396
    :cond_1
    instance-of v0, p0, LX/1ah;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 278397
    check-cast v0, LX/1ah;

    invoke-static {v0}, LX/1an;->a(LX/1ai;)LX/1ai;

    move-result-object v0

    .line 278398
    sget-object v1, LX/1an;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 278399
    invoke-static {v1, p1, p2}, LX/1an;->b(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 278400
    invoke-interface {v0, v1}, LX/1ai;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 278401
    :cond_2
    invoke-static {p0, p1, p2}, LX/1an;->b(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/1oM;LX/4Ab;)V
    .locals 2

    .prologue
    .line 278382
    iget-boolean v0, p1, LX/4Ab;->b:Z

    move v0, v0

    .line 278383
    invoke-interface {p0, v0}, LX/1oM;->a(Z)V

    .line 278384
    iget-object v0, p1, LX/4Ab;->c:[F

    move-object v0, v0

    .line 278385
    invoke-interface {p0, v0}, LX/1oM;->a([F)V

    .line 278386
    iget v0, p1, LX/4Ab;->f:I

    move v0, v0

    .line 278387
    iget v1, p1, LX/4Ab;->e:F

    move v1, v1

    .line 278388
    invoke-interface {p0, v0, v1}, LX/1oM;->a(IF)V

    .line 278389
    iget v0, p1, LX/4Ab;->g:F

    move v0, v0

    .line 278390
    invoke-interface {p0, v0}, LX/1oM;->b(F)V

    .line 278391
    return-void
.end method

.method public static b(Landroid/graphics/drawable/Drawable;LX/4Ab;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 278373
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 278374
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    .line 278375
    new-instance v0, LX/4AV;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2}, LX/4AV;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 278376
    invoke-static {v0, p1}, LX/1an;->a(LX/1oM;LX/4Ab;)V

    move-object p0, v0

    .line 278377
    :cond_0
    :goto_0
    return-object p0

    .line 278378
    :cond_1
    instance-of v0, p0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 278379
    check-cast p0, Landroid/graphics/drawable/ColorDrawable;

    .line 278380
    new-instance v0, LX/4AW;

    invoke-virtual {p0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v1

    invoke-direct {v0, v1}, LX/4AW;-><init>(I)V

    move-object p0, v0

    .line 278381
    invoke-static {p0, p1}, LX/1an;->a(LX/1oM;LX/4Ab;)V

    goto :goto_0
.end method
