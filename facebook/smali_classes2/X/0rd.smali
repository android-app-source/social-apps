.class public final LX/0rd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0pi;

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0rb;


# direct methods
.method public constructor <init>(LX/0pi;LX/0Ot;LX/0rb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0pi;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0rb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149894
    const-string v0, "CacheTracker.Factory cannot be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pi;

    iput-object v0, p0, LX/0rd;->a:LX/0pi;

    .line 149895
    const-string v0, "FbErrorReporter cannot be null"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    iput-object v0, p0, LX/0rd;->b:LX/0Ot;

    .line 149896
    const-string v0, "MemoryTrimmableRegistry cannot be null"

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rb;

    iput-object v0, p0, LX/0rd;->c:LX/0rb;

    .line 149897
    return-void
.end method

.method public static a(LX/0rd;IILjava/lang/String;LX/3du;LX/0rg;)LX/0re;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/String;",
            "Lcom/facebook/cache/LruCacheListener",
            "<TK;TV;>;",
            "LX/0rg;",
            ")",
            "LX/0re",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 149891
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149892
    new-instance v0, LX/3dv;

    const/4 v4, 0x0

    iget-object v1, p0, LX/0rd;->a:LX/0pi;

    invoke-virtual {v1, p3}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v5

    iget-object v6, p0, LX/0rd;->b:LX/0Ot;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v7, p5

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, LX/3dv;-><init>(LX/0rd;IIILX/0pk;LX/0Ot;LX/0rg;LX/3du;)V

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)LX/0re;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/String;",
            ")",
            "LX/0re",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 149887
    new-instance v0, LX/0re;

    iget-object v1, p0, LX/0rd;->a:LX/0pi;

    invoke-virtual {v1, p2}, LX/0pi;->b(Ljava/lang/String;)LX/0pk;

    move-result-object v1

    iget-object v2, p0, LX/0rd;->b:LX/0Ot;

    sget-object v3, LX/0rg;->COUNT:LX/0rg;

    invoke-direct {v0, p1, v1, v2, v3}, LX/0re;-><init>(ILX/0pk;LX/0Ot;LX/0rg;)V

    return-object v0
.end method

.method public final b(ILjava/lang/String;)LX/0re;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/String;",
            ")",
            "LX/0re",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 149888
    invoke-virtual {p0, p1, p2}, LX/0rd;->a(ILjava/lang/String;)LX/0re;

    move-result-object v0

    .line 149889
    iget-object v1, p0, LX/0rd;->c:LX/0rb;

    invoke-interface {v1, v0}, LX/0rb;->a(LX/0rf;)V

    .line 149890
    return-object v0
.end method
