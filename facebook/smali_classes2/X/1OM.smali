.class public abstract LX/1OM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/1P8;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 240545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240546
    new-instance v0, LX/1P8;

    invoke-direct {v0}, LX/1P8;-><init>()V

    iput-object v0, p0, LX/1OM;->a:LX/1P8;

    .line 240547
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1OM;->b:Z

    return-void
.end method


# virtual methods
.method public C_(I)J
    .locals 2

    .prologue
    .line 240548
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public abstract a(Landroid/view/ViewGroup;I)LX/1a1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 240549
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1, p2}, LX/1P8;->a(II)V

    .line 240550
    return-void
.end method

.method public final a(IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 240551
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1, p2, p3}, LX/1P8;->a(IILjava/lang/Object;)V

    .line 240552
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 240553
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, LX/1P8;->a(IILjava/lang/Object;)V

    .line 240554
    return-void
.end method

.method public a(LX/1OD;)V
    .locals 1

    .prologue
    .line 240564
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1}, LX/1P8;->registerObserver(Ljava/lang/Object;)V

    .line 240565
    return-void
.end method

.method public a(LX/1a1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)V"
        }
    .end annotation

    .prologue
    .line 240555
    return-void
.end method

.method public abstract a(LX/1a1;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation
.end method

.method public a(LX/1a1;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240556
    invoke-virtual {p0, p1, p2}, LX/1OM;->a(LX/1a1;I)V

    .line 240557
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 240558
    invoke-virtual {p0}, LX/1OM;->ab_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240559
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot change whether this adapter has stable IDs while the adapter has registered observers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240560
    :cond_0
    iput-boolean p1, p0, LX/1OM;->b:Z

    .line 240561
    return-void
.end method

.method public a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 240562
    return-void
.end method

.method public final ab_()Z
    .locals 1

    .prologue
    .line 240563
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0}, LX/1P8;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation

    .prologue
    .line 240538
    const-string v0, "RV CreateView"

    const v1, -0x73edc44a

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 240539
    invoke-virtual {p0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 240540
    iput p2, v0, LX/1a1;->e:I

    .line 240541
    const v1, -0x7e06622

    invoke-static {v1}, LX/03q;->a(I)V

    .line 240542
    return-object v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 240543
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1, p2}, LX/1P8;->d(II)V

    .line 240544
    return-void
.end method

.method public b(LX/1OD;)V
    .locals 1

    .prologue
    .line 240510
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1}, LX/1P8;->unregisterObserver(Ljava/lang/Object;)V

    .line 240511
    return-void
.end method

.method public final b(LX/1a1;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation

    .prologue
    .line 240512
    iput p2, p1, LX/1a1;->b:I

    .line 240513
    invoke-virtual {p0}, LX/1OM;->eC_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240514
    invoke-virtual {p0, p2}, LX/1OM;->C_(I)J

    move-result-wide v0

    iput-wide v0, p1, LX/1a1;->d:J

    .line 240515
    :cond_0
    const/4 v0, 0x1

    const/16 v1, 0x207

    invoke-virtual {p1, v0, v1}, LX/1a1;->a(II)V

    .line 240516
    const-string v0, "RV OnBindView"

    const v1, -0x650bfd75

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 240517
    invoke-virtual {p1}, LX/1a1;->t()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/1OM;->a(LX/1a1;ILjava/util/List;)V

    .line 240518
    invoke-virtual {p1}, LX/1a1;->s()V

    .line 240519
    const v0, 0x61175067

    invoke-static {v0}, LX/03q;->a(I)V

    .line 240520
    return-void
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 240521
    return-void
.end method

.method public b(LX/1a1;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)Z"
        }
    .end annotation

    .prologue
    .line 240522
    const/4 v0, 0x0

    return v0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 240523
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1, p2}, LX/1P8;->b(II)V

    .line 240524
    return-void
.end method

.method public c(LX/1a1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)V"
        }
    .end annotation

    .prologue
    .line 240525
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 240526
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/1P8;->c(II)V

    .line 240527
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 240528
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0, p1, p2}, LX/1P8;->c(II)V

    .line 240529
    return-void
.end method

.method public d(LX/1a1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;)V"
        }
    .end annotation

    .prologue
    .line 240537
    return-void
.end method

.method public final eC_()Z
    .locals 1

    .prologue
    .line 240509
    iget-boolean v0, p0, LX/1OM;->b:Z

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 240530
    const/4 v0, 0x0

    return v0
.end method

.method public final i_(I)V
    .locals 2

    .prologue
    .line 240531
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/1P8;->a(II)V

    .line 240532
    return-void
.end method

.method public abstract ij_()I
.end method

.method public final j_(I)V
    .locals 2

    .prologue
    .line 240533
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/1P8;->b(II)V

    .line 240534
    return-void
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 240535
    iget-object v0, p0, LX/1OM;->a:LX/1P8;

    invoke-virtual {v0}, LX/1P8;->b()V

    .line 240536
    return-void
.end method
