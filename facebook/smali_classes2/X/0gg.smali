.class public LX/0gg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rb;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fU;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2r2;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0fd;

.field public final p:Lcom/facebook/widget/CustomViewPager;

.field public final q:LX/0gc;

.field public r:LX/0hP;

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:I


# direct methods
.method public constructor <init>(LX/0fd;Landroid/view/View;LX/0gc;)V
    .locals 1
    .param p1    # LX/0fd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 112937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112938
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112939
    iput-object v0, p0, LX/0gg;->g:LX/0Ot;

    .line 112940
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112941
    iput-object v0, p0, LX/0gg;->h:LX/0Ot;

    .line 112942
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112943
    iput-object v0, p0, LX/0gg;->i:LX/0Ot;

    .line 112944
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112945
    iput-object v0, p0, LX/0gg;->j:LX/0Ot;

    .line 112946
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112947
    iput-object v0, p0, LX/0gg;->k:LX/0Ot;

    .line 112948
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112949
    iput-object v0, p0, LX/0gg;->l:LX/0Ot;

    .line 112950
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112951
    iput-object v0, p0, LX/0gg;->m:LX/0Ot;

    .line 112952
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 112953
    iput-object v0, p0, LX/0gg;->n:LX/0Ot;

    .line 112954
    iput-object p1, p0, LX/0gg;->o:LX/0fd;

    .line 112955
    check-cast p2, Lcom/facebook/widget/CustomViewPager;

    iput-object p2, p0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    .line 112956
    iput-object p3, p0, LX/0gg;->q:LX/0gc;

    .line 112957
    return-void
.end method

.method public static a(LX/0gg;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gg;",
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0rb;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0fU;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0i0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2r2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112936
    iput-object p1, p0, LX/0gg;->a:LX/0Or;

    iput-object p2, p0, LX/0gg;->b:LX/0Or;

    iput-object p3, p0, LX/0gg;->c:LX/0Or;

    iput-object p4, p0, LX/0gg;->d:LX/0Or;

    iput-object p5, p0, LX/0gg;->e:LX/0Or;

    iput-object p6, p0, LX/0gg;->f:Landroid/content/Context;

    iput-object p7, p0, LX/0gg;->g:LX/0Ot;

    iput-object p8, p0, LX/0gg;->h:LX/0Ot;

    iput-object p9, p0, LX/0gg;->i:LX/0Ot;

    iput-object p10, p0, LX/0gg;->j:LX/0Ot;

    iput-object p11, p0, LX/0gg;->k:LX/0Ot;

    iput-object p12, p0, LX/0gg;->l:LX/0Ot;

    iput-object p13, p0, LX/0gg;->m:LX/0Ot;

    iput-object p14, p0, LX/0gg;->n:LX/0Ot;

    return-void
.end method

.method public static a$redex0(LX/0gg;Landroid/app/Activity;I)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 112918
    iget-object v0, p0, LX/0gg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, LX/0gh;

    .line 112919
    iget-boolean v0, p0, LX/0gg;->s:Z

    if-eqz v0, :cond_1

    .line 112920
    const-string v0, "swipe"

    invoke-virtual {v10, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 112921
    const-string v9, "swipe"

    .line 112922
    :goto_0
    iput-boolean v1, p0, LX/0gg;->s:Z

    .line 112923
    iput-boolean v1, p0, LX/0gg;->t:Z

    .line 112924
    iget-object v0, p0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v7

    .line 112925
    iget-object v0, p0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->b()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v5

    .line 112926
    invoke-virtual {p0, v7}, LX/0gg;->a(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;

    move-result-object v0

    .line 112927
    if-nez v0, :cond_3

    .line 112928
    :cond_0
    :goto_1
    return-void

    .line 112929
    :cond_1
    iget-boolean v0, p0, LX/0gg;->t:Z

    if-eqz v0, :cond_2

    .line 112930
    const-string v9, "tap"

    goto :goto_0

    .line 112931
    :cond_2
    const-string v9, "other"

    goto :goto_0

    .line 112932
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/katana/fragment/FbChromeFragment;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 112933
    if-eqz v0, :cond_0

    .line 112934
    const-string v0, "tab_src_position"

    invoke-virtual {p0}, LX/0gg;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tab_dest_position"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "tab_src_name"

    const-string v6, "tab_dest_name"

    const-string v8, "tab_change_action"

    invoke-static/range {v0 .. v9}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 112935
    invoke-virtual {v10, p1, v0}, LX/0gh;->a(Landroid/app/Activity;Ljava/util/Map;)V

    goto :goto_1
.end method

.method public static g(LX/0gg;)V
    .locals 2

    .prologue
    .line 112911
    const-string v0, "FbMainTabActivity.onCreateSetupMemoryFragmentTimmer"

    const v1, -0x7508bc0

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 112912
    :try_start_0
    new-instance v1, LX/10L;

    invoke-direct {v1, p0}, LX/10L;-><init>(LX/0gg;)V

    .line 112913
    iget-object v0, p0, LX/0gg;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rb;

    .line 112914
    invoke-interface {v0, v1}, LX/0rb;->a(LX/0rf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112915
    const v0, 0x347849f2

    invoke-static {v0}, LX/02m;->a(I)V

    .line 112916
    return-void

    .line 112917
    :catchall_0
    move-exception v0

    const v1, -0x1d1cdebe

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/katana/fragment/FbChromeFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 112887
    iget-object v0, p0, LX/0gg;->r:LX/0hP;

    if-eqz v0, :cond_0

    .line 112888
    iget-object v0, p0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0, p1}, LX/0fd;->d(Ljava/lang/String;)I

    move-result v0

    .line 112889
    iget-object v1, p0, LX/0gg;->r:LX/0hP;

    invoke-virtual {v1, v0}, LX/0hQ;->b(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/fragment/FbChromeFragment;

    .line 112890
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 112909
    iget-object v0, p0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 112910
    return-void
.end method

.method public final a(Lcom/facebook/katana/fragment/FbChromeFragment;I)V
    .locals 2

    .prologue
    .line 112895
    invoke-virtual {p0}, LX/0gg;->d()I

    move-result v0

    .line 112896
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 112897
    if-ne p2, v0, :cond_1

    .line 112898
    const/4 v0, 0x2

    .line 112899
    :goto_0
    iget v1, p1, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    if-ne v1, v0, :cond_4

    .line 112900
    :cond_0
    :goto_1
    return-void

    .line 112901
    :cond_1
    add-int/lit8 v1, v0, -0x1

    if-eq p2, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    if-ne p2, v0, :cond_3

    .line 112902
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 112903
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 112904
    :cond_4
    iput v0, p1, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    .line 112905
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112906
    iget v1, p1, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    const/4 p0, 0x2

    if-eq v1, p0, :cond_5

    iget v1, p1, Lcom/facebook/katana/fragment/FbChromeFragment;->j:I

    const/4 p0, 0x1

    if-ne v1, p0, :cond_0

    .line 112907
    :cond_5
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/katana/fragment/FbChromeFragment;->a(Z)V

    .line 112908
    invoke-static {p1}, Lcom/facebook/katana/fragment/FbChromeFragment;->p(Lcom/facebook/katana/fragment/FbChromeFragment;)V

    goto :goto_1
.end method

.method public final b()Lcom/facebook/katana/fragment/FbChromeFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 112892
    iget-object v0, p0, LX/0gg;->r:LX/0hP;

    if-eqz v0, :cond_0

    .line 112893
    iget-object v0, p0, LX/0gg;->r:LX/0hP;

    invoke-virtual {p0}, LX/0gg;->d()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0hQ;->b(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/fragment/FbChromeFragment;

    .line 112894
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 112891
    iget-object v0, p0, LX/0gg;->p:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method
