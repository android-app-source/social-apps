.class public final LX/1LC;
.super LX/1LD;
.source ""


# instance fields
.field public final synthetic a:LX/1LB;


# direct methods
.method public constructor <init>(LX/1LB;)V
    .locals 0

    .prologue
    .line 233658
    iput-object p1, p0, LX/1LC;->a:LX/1LB;

    invoke-direct {p0}, LX/1LD;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 13

    .prologue
    .line 233659
    check-cast p1, LX/1ep;

    .line 233660
    iget-boolean v0, p1, LX/1ep;->a:Z

    if-nez v0, :cond_0

    .line 233661
    iget-object v0, p0, LX/1LC;->a:LX/1LB;

    .line 233662
    invoke-static {v0}, LX/1LB;->d(LX/1LB;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/1LB;->e:LX/1Zd;

    if-nez v1, :cond_1

    .line 233663
    :cond_0
    :goto_0
    return-void

    .line 233664
    :cond_1
    iget-object v1, v0, LX/1LB;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1YW;

    .line 233665
    iget-object v3, v0, LX/1LB;->e:LX/1Zd;

    iget-object v4, v0, LX/1LB;->d:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v5

    iget-wide v7, v0, LX/1LB;->f:J

    sub-long/2addr v5, v7

    .line 233666
    iget-object v4, v1, LX/1YW;->g:LX/1Pi;

    .line 233667
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v7

    .line 233668
    iget-object v8, v3, LX/1Zd;->a:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 233669
    iget-object v8, v3, LX/1Zd;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 233670
    :cond_2
    iget-object v8, v3, LX/1Zd;->b:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 233671
    iget-object v8, v3, LX/1Zd;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 233672
    :cond_3
    iget-object v8, v1, LX/1YW;->f:LX/0ad;

    sget-char v9, LX/34q;->g:C

    const v10, 0x7f080d33

    iget-object v11, v1, LX/1YW;->b:Landroid/content/res/Resources;

    invoke-interface {v8, v9, v10, v11}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v8

    .line 233673
    iget-object v9, v1, LX/1YW;->e:LX/1YX;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7}, LX/0cA;->b()LX/0Rf;

    move-result-object v7

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->PHOTO_WELCOME:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v9, v8, v10, v7, v11}, LX/1YX;->a(Ljava/lang/String;Ljava/lang/Long;LX/0Rf;Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;)LX/34n;

    move-result-object v7

    move-object v7, v7

    .line 233674
    invoke-interface {v4, v7}, LX/1Pi;->a(LX/34p;)V

    .line 233675
    goto :goto_1

    .line 233676
    :cond_4
    const/4 v1, 0x0

    iput-object v1, v0, LX/1LB;->e:LX/1Zd;

    .line 233677
    const-wide/16 v1, 0x0

    iput-wide v1, v0, LX/1LB;->f:J

    goto :goto_0
.end method
