.class public LX/14O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:LX/14P;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Z

.field public C:LX/4cx;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0n9;

.field public i:Z

.field public j:Z

.field public k:LX/14S;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/Object;

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:LX/14Q;

.field public w:LX/14R;

.field public x:LX/14P;

.field public volatile y:LX/0zW;

.field private z:Lcom/facebook/http/interfaces/RequestPriority;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178795
    sget-object v0, LX/14P;->CONSERVATIVE:LX/14P;

    sput-object v0, LX/14O;->a:LX/14P;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178835
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14O;->u:Z

    .line 178836
    sget-object v0, LX/14Q;->FALLBACK_NOT_REQUIRED:LX/14Q;

    iput-object v0, p0, LX/14O;->v:LX/14Q;

    .line 178837
    sget-object v0, LX/14R;->AUTO:LX/14R;

    iput-object v0, p0, LX/14O;->w:LX/14R;

    .line 178838
    sget-object v0, LX/14O;->a:LX/14P;

    iput-object v0, p0, LX/14O;->x:LX/14P;

    .line 178839
    return-void
.end method

.method public static D(LX/14O;)LX/0zW;
    .locals 2

    .prologue
    .line 178826
    iget-object v0, p0, LX/14O;->y:LX/0zW;

    if-nez v0, :cond_1

    .line 178827
    monitor-enter p0

    .line 178828
    :try_start_0
    iget-object v0, p0, LX/14O;->y:LX/0zW;

    if-nez v0, :cond_0

    .line 178829
    iget-object v0, p0, LX/14O;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/14O;->b:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, LX/14O;->z:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {v0, v1}, LX/14T;->a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zW;

    move-result-object v0

    iput-object v0, p0, LX/14O;->y:LX/0zW;

    .line 178830
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178831
    :cond_1
    iget-object v0, p0, LX/14O;->y:LX/0zW;

    return-object v0

    .line 178832
    :cond_2
    :try_start_1
    const-string v0, "unknown"

    goto :goto_0

    .line 178833
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178824
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178825
    return-void
.end method


# virtual methods
.method public final C()LX/14N;
    .locals 1

    .prologue
    .line 178822
    invoke-static {p0}, LX/14O;->D(LX/14O;)LX/0zW;

    .line 178823
    new-instance v0, LX/14N;

    invoke-direct {v0, p0}, LX/14N;-><init>(LX/14O;)V

    return-object v0
.end method

.method public final a(LX/0Px;)LX/14O;
    .locals 6
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;)",
            "LX/14O;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 178816
    if-eqz p1, :cond_0

    .line 178817
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 178818
    const-string v4, "X-"

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x2

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 178819
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 178820
    :cond_0
    iput-object p1, p0, LX/14O;->f:LX/0Px;

    .line 178821
    return-object p0
.end method

.method public final a(LX/14P;)LX/14O;
    .locals 1

    .prologue
    .line 178814
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14P;

    iput-object v0, p0, LX/14O;->x:LX/14P;

    .line 178815
    return-object p0
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;
    .locals 2

    .prologue
    .line 178810
    iput-object p1, p0, LX/14O;->z:Lcom/facebook/http/interfaces/RequestPriority;

    .line 178811
    iget-object v0, p0, LX/14O;->y:LX/0zW;

    if-eqz v0, :cond_0

    .line 178812
    iget-object v0, p0, LX/14O;->y:LX/0zW;

    iget-object v1, p0, LX/14O;->z:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zW;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 178813
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/io/File;II)LX/14O;
    .locals 1

    .prologue
    .line 178808
    new-instance v0, LX/4cx;

    invoke-direct {v0, p1, p2, p3}, LX/4cx;-><init>(Ljava/io/File;II)V

    iput-object v0, p0, LX/14O;->C:LX/4cx;

    .line 178809
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/14O;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "LX/14O;"
        }
    .end annotation

    .prologue
    .line 178806
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 178807
    invoke-virtual {p0, p1, v0}, LX/14O;->a(Ljava/util/Map;Ljava/util/Map;)LX/14O;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;Ljava/util/Map;)LX/14O;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "LX/0am",
            "<*>;>;)",
            "LX/14O;"
        }
    .end annotation

    .prologue
    .line 178796
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 178797
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 178798
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v1, v0}, LX/14O;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 178799
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 178800
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    .line 178801
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 178802
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v0, v1}, LX/14O;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 178803
    :cond_2
    iput-object v2, p0, LX/14O;->g:Ljava/util/List;

    .line 178804
    move-object v0, p0

    .line 178805
    return-object v0
.end method
