.class public LX/0c4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/0Up;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/0c4;


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:LX/0c6;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0cK;",
            "Ljava/util/Set",
            "<",
            "LX/0cD;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/SortedSet",
            "<",
            "LX/0cD;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/WeakHashMap",
            "<",
            "LX/2aX;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0cD;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0cJ;

.field public final h:Ljava/lang/ClassLoader;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0c6;LX/0c7;LX/0c8;LX/00H;LX/0Xl;LX/0Ot;)V
    .locals 5
    .param p1    # LX/0c6;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multiprocess/peer/state/PeerStateRoleFactory;",
            "LX/0c7;",
            "LX/0c8;",
            "LX/00H;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87757
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->a:Ljava/lang/Class;

    .line 87758
    const-class v0, LX/0c9;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->h:Ljava/lang/ClassLoader;

    .line 87759
    iput-object p1, p0, LX/0c4;->b:LX/0c6;

    .line 87760
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 87761
    sget-object v1, LX/0c7;->Fscam:LX/0c7;

    if-ne p2, v1, :cond_1

    .line 87762
    sget-object v1, LX/0cB;->e:Landroid/net/Uri;

    invoke-virtual {p1, v1, v3}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 87763
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->f:LX/0Rf;

    .line 87764
    iput-object p6, p0, LX/0c4;->i:LX/0Ot;

    .line 87765
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->c:Ljava/util/Map;

    .line 87766
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->d:Ljava/util/Map;

    .line 87767
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->e:Ljava/util/Map;

    .line 87768
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.messages.ipc.peers"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 87769
    iget-object v1, p4, LX/00H;->k:LX/01U;

    move-object v1, v1

    .line 87770
    invoke-virtual {v1}, LX/01U;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87771
    invoke-virtual {p3, v0, p5, v4}, LX/0c8;->a(Ljava/lang/String;LX/0Xl;Z)LX/0cJ;

    move-result-object v0

    iput-object v0, p0, LX/0c4;->g:LX/0cJ;

    .line 87772
    new-instance v0, LX/0cL;

    invoke-direct {v0, p0}, LX/0cL;-><init>(LX/0c4;)V

    .line 87773
    iget-object v1, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v1, v0}, LX/0cJ;->a(LX/0cN;)V

    .line 87774
    iget-object v1, p0, LX/0c4;->g:LX/0cJ;

    const v2, 0x3b9aca00

    invoke-interface {v1, v2, v0}, LX/0cJ;->a(ILX/0cM;)V

    .line 87775
    iget-object v1, p0, LX/0c4;->g:LX/0cJ;

    const v2, 0x3b9aca01

    invoke-interface {v1, v2, v0}, LX/0cJ;->a(ILX/0cM;)V

    .line 87776
    iget-object v0, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v0}, LX/0cJ;->a()LX/0cK;

    move-result-object v0

    iget-object v1, p0, LX/0c4;->f:LX/0Rf;

    invoke-direct {p0, v0, v1}, LX/0c4;->a(LX/0cK;Ljava/util/Set;)V

    .line 87777
    return-void

    .line 87778
    :cond_1
    sget-object v1, LX/0c7;->Messenger:LX/0c7;

    if-ne p2, v1, :cond_2

    .line 87779
    sget-object v1, LX/0cB;->a:Landroid/net/Uri;

    const/16 v2, 0x1f3

    invoke-virtual {p1, v1, v2}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 87780
    sget-object v1, LX/0cB;->e:Landroid/net/Uri;

    invoke-virtual {p1, v1, v3}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 87781
    sget-object v1, LX/0cB;->g:Landroid/net/Uri;

    invoke-virtual {p1, v1, v3}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 87782
    :cond_2
    sget-object v1, LX/0c7;->Fb4a:LX/0c7;

    if-ne p2, v1, :cond_3

    .line 87783
    sget-object v1, LX/0cB;->e:Landroid/net/Uri;

    invoke-virtual {p1, v1, v4}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 87784
    sget-object v1, LX/0cB;->g:Landroid/net/Uri;

    invoke-virtual {p1, v1, v4}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto/16 :goto_0

    .line 87785
    :cond_3
    sget-object v1, LX/0c7;->PMA:LX/0c7;

    if-ne p2, v1, :cond_0

    .line 87786
    sget-object v1, LX/0cB;->e:Landroid/net/Uri;

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/0c4;
    .locals 10

    .prologue
    .line 87740
    sget-object v0, LX/0c4;->j:LX/0c4;

    if-nez v0, :cond_1

    .line 87741
    const-class v1, LX/0c4;

    monitor-enter v1

    .line 87742
    :try_start_0
    sget-object v0, LX/0c4;->j:LX/0c4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 87743
    if-eqz v2, :cond_0

    .line 87744
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 87745
    new-instance v3, LX/0c4;

    invoke-static {v0}, LX/0c6;->a(LX/0QB;)LX/0c6;

    move-result-object v4

    check-cast v4, LX/0c6;

    .line 87746
    sget-object v5, LX/0c7;->Fb4a:LX/0c7;

    move-object v5, v5

    .line 87747
    move-object v5, v5

    .line 87748
    check-cast v5, LX/0c7;

    invoke-static {v0}, LX/0c8;->a(LX/0QB;)LX/0c8;

    move-result-object v6

    check-cast v6, LX/0c8;

    const-class v7, LX/00H;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/00H;

    invoke-static {v0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v8

    check-cast v8, LX/0Xl;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/0c4;-><init>(LX/0c6;LX/0c7;LX/0c8;LX/00H;LX/0Xl;LX/0Ot;)V

    .line 87749
    move-object v0, v3

    .line 87750
    sput-object v0, LX/0c4;->j:LX/0c4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87751
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 87752
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 87753
    :cond_1
    sget-object v0, LX/0c4;->j:LX/0c4;

    return-object v0

    .line 87754
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 87755
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(LX/0c4;)Landroid/os/Message;
    .locals 8

    .prologue
    .line 87722
    monitor-enter p0

    const/4 v0, 0x0

    const v1, 0x3b9aca00

    :try_start_0
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 87723
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 87724
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 87725
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 87726
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 87727
    iget-object v0, p0, LX/0c4;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 87728
    iget-object v7, v0, LX/0cD;->b:Landroid/net/Uri;

    move-object v7, v7

    .line 87729
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87730
    iget v7, v0, LX/0cD;->c:I

    move v7, v7

    .line 87731
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87732
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 87733
    invoke-virtual {v0, v7}, LX/0cD;->a(Landroid/os/Bundle;)V

    .line 87734
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87735
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87736
    :cond_0
    :try_start_1
    const-string v0, "__BASE_URIS__"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 87737
    const-string v0, "__PRIORITIES__"

    invoke-virtual {v2, v0, v5}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 87738
    const-string v0, "__ROLES_DATA__"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87739
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized a(LX/0cK;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cK;",
            "Ljava/util/Set",
            "<",
            "LX/0cD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87709
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0c4;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87710
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 87711
    iget-object v1, p0, LX/0c4;->d:Ljava/util/Map;

    .line 87712
    iget-object v3, v0, LX/0cD;->b:Landroid/net/Uri;

    move-object v3, v3

    .line 87713
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedSet;

    .line 87714
    if-nez v1, :cond_0

    .line 87715
    sget-object v1, LX/0cD;->a:Ljava/util/Comparator;

    invoke-static {v1}, LX/0RA;->a(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v1

    .line 87716
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 87717
    iget-object v3, p0, LX/0c4;->d:Ljava/util/Map;

    .line 87718
    iget-object p1, v0, LX/0cD;->b:Landroid/net/Uri;

    move-object v0, p1

    .line 87719
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87721
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private static a(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 87700
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 87701
    :cond_0
    :goto_0
    return v2

    .line 87702
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 87703
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 87704
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    move v1, v2

    .line 87705
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 87706
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 87708
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static a$redex0(LX/0c4;LX/0cK;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 87685
    const-string v0, "__STATE_URI__"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 87686
    monitor-enter p0

    .line 87687
    :try_start_0
    iget-object v1, p0, LX/0c4;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 87688
    if-nez v1, :cond_1

    .line 87689
    monitor-exit p0

    .line 87690
    :cond_0
    :goto_0
    return-void

    .line 87691
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0cD;

    .line 87692
    iget-object v4, v1, LX/0cD;->b:Landroid/net/Uri;

    move-object v4, v4

    .line 87693
    invoke-static {v0, v4}, LX/0c4;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 87694
    invoke-virtual {v1, p2}, LX/0cD;->b(Landroid/os/Bundle;)V

    .line 87695
    const/4 v1, 0x1

    .line 87696
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87697
    if-eqz v1, :cond_0

    .line 87698
    invoke-static {p0, v0, v2}, LX/0c4;->a$redex0(LX/0c4;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 87699
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public static a$redex0(LX/0c4;Landroid/net/Uri;Z)V
    .locals 4

    .prologue
    .line 87676
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 87677
    monitor-enter p0

    .line 87678
    :try_start_0
    iget-object v0, p0, LX/0c4;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87679
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-static {p1, v1}, LX/0c4;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87680
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 87681
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87682
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2aX;

    .line 87683
    invoke-interface {v0, p1, p2}, LX/2aX;->a(Landroid/net/Uri;Z)V

    goto :goto_1

    .line 87684
    :cond_2
    return-void
.end method

.method public static declared-synchronized b$redex0(LX/0c4;LX/0cK;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 87613
    monitor-enter p0

    :try_start_0
    const-string v0, "__BASE_URIS__"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 87614
    const-string v0, "__PRIORITIES__"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 87615
    const-string v0, "__ROLES_DATA__"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 87616
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v6

    .line 87617
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 87618
    iget-object v7, p0, LX/0c4;->b:LX/0c6;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v7, v0, v1}, LX/0c6;->a(Landroid/net/Uri;I)LX/0cD;

    move-result-object v1

    .line 87619
    if-nez v1, :cond_0

    .line 87620
    iget-object v0, p0, LX/0c4;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/0c4;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Can not create PeerStateRole for base uri "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " with priority "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " in process "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v8}, LX/0cJ;->a()LX/0cK;

    move-result-object v8

    iget-object v8, v8, LX/0cK;->c:LX/00G;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87621
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 87622
    :cond_0
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, LX/0cD;->b(Landroid/os/Bundle;)V

    .line 87623
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 87624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87625
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, v6}, LX/0c4;->a(LX/0cK;Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87626
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 87665
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0c4;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 87666
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-static {p1, v1}, LX/0c4;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87667
    new-instance v1, LX/4gt;

    invoke-direct {v1}, LX/4gt;-><init>()V

    .line 87668
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 87669
    invoke-virtual {v0, p1, v1}, LX/0cD;->a(Landroid/net/Uri;LX/4gt;)V

    .line 87670
    iget-boolean v0, v1, LX/4gt;->b:Z

    if-eqz v0, :cond_1

    .line 87671
    iget-object v0, v1, LX/4gt;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87672
    :goto_0
    monitor-exit p0

    return-object v0

    .line 87673
    :cond_2
    :try_start_1
    iget-object v0, v1, LX/4gt;->a:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87674
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 87675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;LX/2aX;)V
    .locals 2

    .prologue
    .line 87658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0c4;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/WeakHashMap;

    .line 87659
    if-nez v0, :cond_0

    .line 87660
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 87661
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87662
    iget-object v1, p0, LX/0c4;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87663
    monitor-exit p0

    return-void

    .line 87664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 87639
    iget-object v0, p0, LX/0c4;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 87640
    iget-object v0, p0, LX/0c4;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 87641
    iget-object v3, v0, LX/0cD;->b:Landroid/net/Uri;

    move-object v3, v3

    .line 87642
    invoke-static {p1, v3}, LX/0c4;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87643
    :goto_0
    if-eqz v0, :cond_2

    .line 87644
    const v2, 0x3b9aca01

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 87645
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 87646
    monitor-enter p0

    .line 87647
    :try_start_0
    invoke-virtual {v0, p1, p2}, LX/0cD;->a(Landroid/net/Uri;Ljava/lang/Object;)Z

    move-result v3

    .line 87648
    if-eqz v3, :cond_1

    .line 87649
    invoke-virtual {v0, v2}, LX/0cD;->a(Landroid/os/Bundle;)V

    .line 87650
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87651
    if-eqz v3, :cond_2

    .line 87652
    const-string v0, "__STATE_URI__"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 87653
    iget-object v0, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v0, v1}, LX/0cJ;->a(Landroid/os/Message;)V

    .line 87654
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/0c4;->a$redex0(LX/0c4;Landroid/net/Uri;Z)V

    .line 87655
    :cond_2
    return-void

    .line 87656
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 87657
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current process "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v2}, LX/0cJ;->a()LX/0cK;

    move-result-object v2

    iget-object v2, v2, LX/0cK;->c:LX/00G;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a stateful peer."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 87629
    monitor-enter p0

    .line 87630
    :try_start_0
    iget-object v0, p0, LX/0c4;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 87631
    iget-object v0, p0, LX/0c4;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 87632
    iget-object v0, p0, LX/0c4;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 87633
    invoke-virtual {v0}, LX/0cD;->a()V

    goto :goto_0

    .line 87634
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 87635
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v0}, LX/0cJ;->a()LX/0cK;

    move-result-object v0

    iget-object v1, p0, LX/0c4;->f:LX/0Rf;

    invoke-direct {p0, v0, v1}, LX/0c4;->a(LX/0cK;Ljava/util/Set;)V

    .line 87636
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87637
    iget-object v0, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v0}, LX/0c5;->clearUserData()V

    .line 87638
    return-void
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 87627
    iget-object v0, p0, LX/0c4;->g:LX/0cJ;

    invoke-interface {v0}, LX/0Up;->init()V

    .line 87628
    return-void
.end method
