.class public final enum LX/0jt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0jt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0jt;

.field public static final enum ALWAYS_SHOW:LX/0jt;

.field public static final enum NEVER_SHOW:LX/0jt;

.field public static final enum ONLY_SHOW_FOR_SETTINGS:LX/0jt;


# instance fields
.field public final shouldShowForDialogStep:Z

.field public final shouldShowForSettingsStep:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124696
    new-instance v0, LX/0jt;

    const-string v1, "NEVER_SHOW"

    invoke-direct {v0, v1, v2, v2, v2}, LX/0jt;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/0jt;->NEVER_SHOW:LX/0jt;

    .line 124697
    new-instance v0, LX/0jt;

    const-string v1, "ONLY_SHOW_FOR_SETTINGS"

    invoke-direct {v0, v1, v3, v2, v3}, LX/0jt;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/0jt;->ONLY_SHOW_FOR_SETTINGS:LX/0jt;

    .line 124698
    new-instance v0, LX/0jt;

    const-string v1, "ALWAYS_SHOW"

    invoke-direct {v0, v1, v4, v3, v3}, LX/0jt;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    .line 124699
    const/4 v0, 0x3

    new-array v0, v0, [LX/0jt;

    sget-object v1, LX/0jt;->NEVER_SHOW:LX/0jt;

    aput-object v1, v0, v2

    sget-object v1, LX/0jt;->ONLY_SHOW_FOR_SETTINGS:LX/0jt;

    aput-object v1, v0, v3

    sget-object v1, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    aput-object v1, v0, v4

    sput-object v0, LX/0jt;->$VALUES:[LX/0jt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 124692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 124693
    iput-boolean p3, p0, LX/0jt;->shouldShowForDialogStep:Z

    .line 124694
    iput-boolean p4, p0, LX/0jt;->shouldShowForSettingsStep:Z

    .line 124695
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0jt;
    .locals 1

    .prologue
    .line 124691
    const-class v0, LX/0jt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0jt;

    return-object v0
.end method

.method public static values()[LX/0jt;
    .locals 1

    .prologue
    .line 124690
    sget-object v0, LX/0jt;->$VALUES:[LX/0jt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0jt;

    return-object v0
.end method
