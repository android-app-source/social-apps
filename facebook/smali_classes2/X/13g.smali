.class public LX/13g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/13g;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0tK;


# direct methods
.method public constructor <init>(LX/0ad;LX/0tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 177079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177080
    iput-object p1, p0, LX/13g;->a:LX/0ad;

    .line 177081
    iput-object p2, p0, LX/13g;->b:LX/0tK;

    .line 177082
    return-void
.end method

.method public static a(LX/0QB;)LX/13g;
    .locals 5

    .prologue
    .line 177083
    sget-object v0, LX/13g;->c:LX/13g;

    if-nez v0, :cond_1

    .line 177084
    const-class v1, LX/13g;

    monitor-enter v1

    .line 177085
    :try_start_0
    sget-object v0, LX/13g;->c:LX/13g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 177086
    if-eqz v2, :cond_0

    .line 177087
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 177088
    new-instance p0, LX/13g;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v4

    check-cast v4, LX/0tK;

    invoke-direct {p0, v3, v4}, LX/13g;-><init>(LX/0ad;LX/0tK;)V

    .line 177089
    move-object v0, p0

    .line 177090
    sput-object v0, LX/13g;->c:LX/13g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 177092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177093
    :cond_1
    sget-object v0, LX/13g;->c:LX/13g;

    return-object v0

    .line 177094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 177095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
