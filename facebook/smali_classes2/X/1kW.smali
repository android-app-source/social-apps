.class public LX/1kW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kK;


# instance fields
.field public final a:Lcom/facebook/productionprompts/model/ProductionPrompt;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/model/ProductionPrompt;)V
    .locals 0

    .prologue
    .line 309932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309933
    iput-object p1, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 309934
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309931
    iget-object v0, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309918
    iget-object v0, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/5ob;
    .locals 1

    .prologue
    .line 309930
    sget-object v0, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    return-object v0
.end method

.method public final e()Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 1

    .prologue
    .line 309929
    iget-object v0, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 309920
    if-ne p0, p1, :cond_0

    .line 309921
    const/4 v0, 0x1

    .line 309922
    :goto_0
    return v0

    .line 309923
    :cond_0
    instance-of v0, p1, LX/1kW;

    if-nez v0, :cond_1

    .line 309924
    const/4 v0, 0x0

    goto :goto_0

    .line 309925
    :cond_1
    check-cast p1, LX/1kW;

    .line 309926
    iget-object v0, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 309927
    iget-object v1, p1, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, v1

    .line 309928
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 309919
    iget-object v0, p0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->hashCode()I

    move-result v0

    return v0
.end method
