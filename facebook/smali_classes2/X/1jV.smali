.class public LX/1jV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0SG;

.field private final d:LX/0qR;


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0qR;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 300137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300138
    iput-object p1, p0, LX/1jV;->a:LX/0Zb;

    .line 300139
    iput-object p2, p0, LX/1jV;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 300140
    iput-object p3, p0, LX/1jV;->c:LX/0SG;

    .line 300141
    iput-object p4, p0, LX/1jV;->d:LX/0qR;

    .line 300142
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 300143
    iget-object v0, p0, LX/1jV;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->e:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/1jV;LX/0oG;LX/0Px;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300144
    const/4 v10, 0x0

    .line 300145
    const/4 v9, 0x0

    .line 300146
    const/4 v8, 0x0

    .line 300147
    const/4 v3, 0x0

    .line 300148
    const-wide/16 v6, 0x0

    .line 300149
    const-wide/16 v4, 0x0

    .line 300150
    invoke-virtual/range {p2 .. p2}, LX/0Px;->size()I

    move-result v12

    const/4 v2, 0x0

    move v11, v2

    :goto_0
    if-ge v11, v12, :cond_3

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 300151
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 300152
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v13

    invoke-static {v13}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 300153
    add-int/lit8 v2, v9, 0x1

    move v9, v10

    move-wide/from16 v18, v6

    move v6, v3

    move v7, v8

    move v8, v2

    move-wide v2, v4

    move-wide/from16 v4, v18

    .line 300154
    :goto_1
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move/from16 v18, v6

    move-wide v6, v4

    move-wide v4, v2

    move/from16 v3, v18

    goto :goto_0

    .line 300155
    :cond_0
    add-int/lit8 v10, v10, 0x1

    .line 300156
    invoke-virtual {v2}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->F()Z

    move-result v13

    if-nez v13, :cond_5

    .line 300157
    add-int/lit8 v8, v8, 0x1

    .line 300158
    move-object/from16 v0, p0

    iget-object v13, v0, LX/1jV;->d:LX/0qR;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, LX/0qR;->c(Ljava/lang/String;)LX/14t;

    move-result-object v13

    .line 300159
    const-string v14, "PHOTO"

    invoke-virtual {v13, v14}, LX/14t;->a(Ljava/lang/String;)I

    move-result v14

    if-gtz v14, :cond_1

    const-string v14, "VIDEO"

    invoke-virtual {v13, v14}, LX/14t;->a(Ljava/lang/String;)I

    move-result v13

    if-lez v13, :cond_2

    .line 300160
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 300161
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, LX/1jV;->c:LX/0SG;

    invoke-interface {v13}, LX/0SG;->a()J

    move-result-wide v14

    invoke-virtual {v2}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->x()J

    move-result-wide v16

    sub-long v14, v14, v16

    add-long/2addr v6, v14

    .line 300162
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v14

    add-double/2addr v4, v14

    move-wide/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move v7, v8

    move-wide/from16 v2, v18

    move v8, v9

    move v9, v10

    goto :goto_1

    .line 300163
    :cond_3
    const-string v2, "time_since_last_fetch"

    move-object/from16 v0, p0

    iget-object v11, v0, LX/1jV;->c:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v12

    invoke-direct/range {p0 .. p0}, LX/1jV;->a()J

    move-result-wide v14

    sub-long/2addr v12, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v13}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 300164
    const-string v2, "total_stories"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 300165
    const-string v2, "total_ads"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 300166
    const-string v2, "total_unread"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 300167
    const-string v2, "total_unread_with_media"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 300168
    if-lez v8, :cond_4

    .line 300169
    const-string v2, "avg_age_of_unread_stories"

    int-to-long v10, v8

    div-long/2addr v6, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 300170
    const-string v2, "avg_weight_of_unread_stories"

    int-to-double v6, v8

    div-double/2addr v4, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 300171
    :cond_4
    invoke-virtual/range {p1 .. p1}, LX/0oG;->d()V

    .line 300172
    return-void

    :cond_5
    move-wide/from16 v18, v4

    move-wide v4, v6

    move v6, v3

    move v7, v8

    move-wide/from16 v2, v18

    move v8, v9

    move v9, v10

    goto/16 :goto_1
.end method
