.class public abstract LX/118;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public b:LX/2ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/118",
            "<TK;TV;>.EntrySet;"
        }
    .end annotation
.end field

.field public c:LX/2wD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/118",
            "<TK;TV;>.KeySet;"
        }
    .end annotation
.end field

.field public d:LX/2wB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/118",
            "<TK;TV;>.ValuesCollection;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 170425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170426
    return-void
.end method

.method public static a(Ljava/util/Set;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 170417
    if-ne p0, p1, :cond_1

    .line 170418
    :cond_0
    :goto_0
    return v0

    .line 170419
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_3

    .line 170420
    check-cast p1, Ljava/util/Set;

    .line 170421
    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 170422
    :catch_0
    move v0, v1

    goto :goto_0

    .line 170423
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 170424
    goto :goto_0
.end method

.method public static c(Ljava/util/Map;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 170411
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    .line 170412
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 170413
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170414
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 170415
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 170416
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Ljava/lang/Object;)I
.end method

.method public abstract a(II)Ljava/lang/Object;
.end method

.method public abstract a(ILjava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITV;)TV;"
        }
    .end annotation
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation
.end method

.method public final a([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;I)[TT;"
        }
    .end annotation

    .prologue
    .line 170387
    invoke-virtual {p0}, LX/118;->a()I

    move-result v2

    .line 170388
    array-length v0, p1

    if-ge v0, v2, :cond_2

    .line 170389
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 170390
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 170391
    invoke-virtual {p0, v1, p2}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 170392
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 170393
    :cond_0
    array-length v1, v0

    if-le v1, v2, :cond_1

    .line 170394
    const/4 v1, 0x0

    aput-object v1, v0, v2

    .line 170395
    :cond_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)I
.end method

.method public abstract b()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public final b(I)[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 170405
    invoke-virtual {p0}, LX/118;->a()I

    move-result v1

    .line 170406
    new-array v2, v1, [Ljava/lang/Object;

    .line 170407
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 170408
    invoke-virtual {p0, v0, p1}, LX/118;->a(II)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 170409
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170410
    :cond_0
    return-object v2
.end method

.method public abstract c()V
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 170402
    iget-object v0, p0, LX/118;->b:LX/2ln;

    if-nez v0, :cond_0

    .line 170403
    new-instance v0, LX/2ln;

    invoke-direct {v0, p0}, LX/2ln;-><init>(LX/118;)V

    iput-object v0, p0, LX/118;->b:LX/2ln;

    .line 170404
    :cond_0
    iget-object v0, p0, LX/118;->b:LX/2ln;

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 170399
    iget-object v0, p0, LX/118;->c:LX/2wD;

    if-nez v0, :cond_0

    .line 170400
    new-instance v0, LX/2wD;

    invoke-direct {v0, p0}, LX/2wD;-><init>(LX/118;)V

    iput-object v0, p0, LX/118;->c:LX/2wD;

    .line 170401
    :cond_0
    iget-object v0, p0, LX/118;->c:LX/2wD;

    return-object v0
.end method

.method public final f()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 170396
    iget-object v0, p0, LX/118;->d:LX/2wB;

    if-nez v0, :cond_0

    .line 170397
    new-instance v0, LX/2wB;

    invoke-direct {v0, p0}, LX/2wB;-><init>(LX/118;)V

    iput-object v0, p0, LX/118;->d:LX/2wB;

    .line 170398
    :cond_0
    iget-object v0, p0, LX/118;->d:LX/2wB;

    return-object v0
.end method
