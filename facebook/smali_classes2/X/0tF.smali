.class public LX/0tF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0tF;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 154204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154205
    iput-object p1, p0, LX/0tF;->a:LX/0ad;

    .line 154206
    return-void
.end method

.method public static a(LX/0QB;)LX/0tF;
    .locals 4

    .prologue
    .line 154207
    sget-object v0, LX/0tF;->b:LX/0tF;

    if-nez v0, :cond_1

    .line 154208
    const-class v1, LX/0tF;

    monitor-enter v1

    .line 154209
    :try_start_0
    sget-object v0, LX/0tF;->b:LX/0tF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 154210
    if-eqz v2, :cond_0

    .line 154211
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 154212
    new-instance p0, LX/0tF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/0tF;-><init>(LX/0ad;)V

    .line 154213
    move-object v0, p0

    .line 154214
    sput-object v0, LX/0tF;->b:LX/0tF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154215
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 154216
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154217
    :cond_1
    sget-object v0, LX/0tF;->b:LX/0tF;

    return-object v0

    .line 154218
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 154219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 154220
    iget-object v0, p0, LX/0tF;->a:LX/0ad;

    sget-short v1, LX/0wn;->ae:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154221
    invoke-virtual {p0}, LX/0tF;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tF;->a:LX/0ad;

    sget-short v2, LX/0wn;->ad:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154222
    invoke-virtual {p0}, LX/0tF;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tF;->a:LX/0ad;

    sget-short v2, LX/0wn;->ac:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154223
    invoke-virtual {p0}, LX/0tF;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tF;->a:LX/0ad;

    sget-short v2, LX/0wn;->ah:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 154224
    invoke-virtual {p0}, LX/0tF;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0tF;->a:LX/0ad;

    sget-short v2, LX/0wn;->ab:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
