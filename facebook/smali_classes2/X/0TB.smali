.class public final LX/0TB;
.super LX/0TC;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "TODO"
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field

.field private c:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62628
    invoke-direct {p0}, LX/0TC;-><init>()V

    .line 62629
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0TB;->a:Ljava/lang/Object;

    .line 62630
    iput v1, p0, LX/0TB;->b:I

    .line 62631
    iput-boolean v1, p0, LX/0TB;->c:Z

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 62622
    iget-object v1, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 62623
    :try_start_0
    iget-boolean v0, p0, LX/0TB;->c:Z

    if-eqz v0, :cond_0

    .line 62624
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    const-string v2, "Executor already shutdown"

    invoke-direct {v0, v2}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62625
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 62626
    :cond_0
    :try_start_1
    iget v0, p0, LX/0TB;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0TB;->b:I

    .line 62627
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 62587
    iget-object v1, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 62588
    :try_start_0
    iget v0, p0, LX/0TB;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/0TB;->b:I

    .line 62589
    if-nez v0, :cond_0

    .line 62590
    iget-object v0, p0, LX/0TB;->a:Ljava/lang/Object;

    const v2, 0x59a2797

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 62591
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 9

    .prologue
    .line 62610
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 62611
    iget-object v2, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 62612
    :goto_0
    :try_start_0
    iget-boolean v3, p0, LX/0TB;->c:Z

    if-eqz v3, :cond_0

    iget v3, p0, LX/0TB;->b:I

    if-nez v3, :cond_0

    .line 62613
    const/4 v0, 0x1

    monitor-exit v2

    .line 62614
    :goto_1
    return v0

    .line 62615
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gtz v3, :cond_1

    .line 62616
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_1

    .line 62617
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 62618
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 62619
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, LX/0TB;->a:Ljava/lang/Object;

    invoke-virtual {v3, v6, v0, v1}, Ljava/util/concurrent/TimeUnit;->timedWait(Ljava/lang/Object;J)V

    .line 62620
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v6

    sub-long v4, v6, v4

    sub-long/2addr v0, v4

    .line 62621
    goto :goto_0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 62605
    invoke-direct {p0}, LX/0TB;->a()V

    .line 62606
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62607
    invoke-direct {p0}, LX/0TB;->b()V

    .line 62608
    return-void

    .line 62609
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/0TB;->b()V

    throw v0
.end method

.method public final isShutdown()Z
    .locals 2

    .prologue
    .line 62602
    iget-object v1, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 62603
    :try_start_0
    iget-boolean v0, p0, LX/0TB;->c:Z

    monitor-exit v1

    return v0

    .line 62604
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isTerminated()Z
    .locals 2

    .prologue
    .line 62599
    iget-object v1, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 62600
    :try_start_0
    iget-boolean v0, p0, LX/0TB;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/0TB;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62601
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final shutdown()V
    .locals 3

    .prologue
    .line 62594
    iget-object v1, p0, LX/0TB;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 62595
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/0TB;->c:Z

    .line 62596
    iget v0, p0, LX/0TB;->b:I

    if-nez v0, :cond_0

    .line 62597
    iget-object v0, p0, LX/0TB;->a:Ljava/lang/Object;

    const v2, -0x10c867e7

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 62598
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62592
    invoke-virtual {p0}, LX/0TB;->shutdown()V

    .line 62593
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
