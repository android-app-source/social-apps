.class public abstract LX/0UO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:B

.field private final b:LX/0QB;

.field private volatile c:LX/0R7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 2

    .prologue
    .line 64523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64524
    iput-object p1, p0, LX/0UO;->b:LX/0QB;

    .line 64525
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v0

    .line 64526
    iget-byte v1, v0, LX/0SD;->a:B

    move v0, v1

    .line 64527
    iput-byte v0, p0, LX/0UO;->a:B

    .line 64528
    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    iput-object v0, p0, LX/0UO;->c:LX/0R7;

    .line 64529
    return-void
.end method


# virtual methods
.method public abstract a(LX/0QB;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")TT;"
        }
    .end annotation
.end method

.method public final get()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 64530
    iget-object v0, p0, LX/0UO;->c:LX/0R7;

    if-eqz v0, :cond_1

    .line 64531
    monitor-enter p0

    .line 64532
    :try_start_0
    iget-object v0, p0, LX/0UO;->c:LX/0R7;

    if-eqz v0, :cond_0

    .line 64533
    iget-object v1, p0, LX/0UO;->c:LX/0R7;

    .line 64534
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 64535
    iget-byte v0, p0, LX/0UO;->a:B

    invoke-virtual {v2, v0}, LX/0SD;->b(B)B

    move-result v3

    .line 64536
    invoke-interface {v1}, LX/0R7;->a()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 64537
    :try_start_1
    iget-object v0, p0, LX/0UO;->b:LX/0QB;

    invoke-interface {v0}, LX/0QB;->getScopeUnawareInjector()LX/0QD;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0UO;->a(LX/0QB;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0UO;->d:Ljava/lang/Object;

    .line 64538
    const/4 v0, 0x0

    iput-object v0, p0, LX/0UO;->c:LX/0R7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64539
    :try_start_2
    invoke-interface {v1, v4}, LX/0R7;->a(Ljava/lang/Object;)V

    .line 64540
    iput-byte v3, v2, LX/0SD;->a:B

    .line 64541
    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 64542
    :cond_1
    iget-object v0, p0, LX/0UO;->d:Ljava/lang/Object;

    return-object v0

    .line 64543
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v1, v4}, LX/0R7;->a(Ljava/lang/Object;)V

    .line 64544
    iput-byte v3, v2, LX/0SD;->a:B

    .line 64545
    throw v0

    .line 64546
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
