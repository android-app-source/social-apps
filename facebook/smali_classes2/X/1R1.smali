.class public LX/1R1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R2",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1R0;

.field public final b:LX/0g1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0g1",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:LX/1DZ;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Rb;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1PW;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1R4;

.field private final h:LX/1R6;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Rk;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1R7;

.field public k:[I


# direct methods
.method public constructor <init>(LX/1R0;LX/0g1;LX/1PW;LX/1DZ;)V
    .locals 2
    .param p1    # LX/1R0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0g1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1PW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1DZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 245504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245505
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/1R1;->d:Ljava/util/Map;

    .line 245506
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1R1;->f:Ljava/util/Set;

    .line 245507
    new-instance v0, LX/1R3;

    invoke-direct {v0, p0}, LX/1R3;-><init>(LX/1R1;)V

    iput-object v0, p0, LX/1R1;->g:LX/1R4;

    .line 245508
    new-instance v0, LX/1R5;

    invoke-direct {v0, p0}, LX/1R5;-><init>(LX/1R1;)V

    iput-object v0, p0, LX/1R1;->h:LX/1R6;

    .line 245509
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1R1;->i:Ljava/util/List;

    .line 245510
    const/16 v0, 0xa

    invoke-static {v0}, LX/1R7;->a(I)LX/1R7;

    move-result-object v0

    iput-object v0, p0, LX/1R1;->j:LX/1R7;

    .line 245511
    iput-object p1, p0, LX/1R1;->a:LX/1R0;

    .line 245512
    iput-object p2, p0, LX/1R1;->b:LX/0g1;

    .line 245513
    iput-object p4, p0, LX/1R1;->c:LX/1DZ;

    .line 245514
    iput-object p3, p0, LX/1R1;->e:LX/1PW;

    .line 245515
    iget-object v0, p0, LX/1R1;->e:LX/1PW;

    instance-of v0, v0, LX/1Px;

    if-eqz v0, :cond_0

    .line 245516
    iget-object v0, p0, LX/1R1;->e:LX/1PW;

    check-cast v0, LX/1Px;

    .line 245517
    iget-object v1, p0, LX/1R1;->h:LX/1R6;

    invoke-interface {v0, v1}, LX/1Px;->a(LX/1R6;)V

    .line 245518
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Px;->m_(Z)V

    .line 245519
    :cond_0
    return-void
.end method

.method private static a(LX/1Rk;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245499
    iget-object v0, p0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_0

    .line 245500
    iget-object v0, p0, LX/1Rk;->d:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 245501
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 245502
    iget-object p0, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 245503
    goto :goto_0
.end method

.method public static a(LX/1R1;Ljava/lang/Object;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 245491
    iget-object v0, p0, LX/1R1;->a:LX/1R0;

    iget-object v1, p0, LX/1R1;->e:LX/1PW;

    invoke-interface {v0, p1, v1}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v1

    .line 245492
    iget-object v0, p0, LX/1R1;->k:[I

    iget-object v2, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    aput v2, v0, p2

    .line 245493
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, LX/1RA;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 245494
    iget-object v2, p0, LX/1R1;->j:LX/1R7;

    invoke-virtual {v2, p2}, LX/1R7;->b(I)V

    .line 245495
    iget-object v2, p0, LX/1R1;->i:Ljava/util/List;

    invoke-virtual {v1, v0}, LX/1RA;->g(I)LX/1Rk;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245496
    iget-object v2, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p0, v2}, LX/1R1;->g(LX/1R1;I)V

    .line 245497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245498
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1Rk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245484
    const/4 v1, 0x0

    .line 245485
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 245486
    iget-object v3, v0, LX/1Rk;->a:LX/1RA;

    if-eq v3, v1, :cond_1

    .line 245487
    iget-object v0, v0, LX/1Rk;->a:LX/1RA;

    .line 245488
    invoke-virtual {v0}, LX/1RA;->c()V

    :goto_1
    move-object v1, v0

    .line 245489
    goto :goto_0

    .line 245490
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static f(LX/1R1;I)LX/1Rb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 245481
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 245482
    :cond_0
    const/4 v0, 0x0

    .line 245483
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    invoke-virtual {v0}, LX/1Rk;->e()LX/1Rb;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(LX/1R1;I)V
    .locals 3

    .prologue
    .line 245479
    iget-object v0, p0, LX/1R1;->d:Ljava/util/Map;

    invoke-static {p0, p1}, LX/1R1;->f(LX/1R1;I)LX/1Rb;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245480
    return-void
.end method


# virtual methods
.method public final a(I)LX/1Rk;
    .locals 1

    .prologue
    .line 245392
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 245470
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-static {v0}, LX/1R1;->a(Ljava/util/List;)V

    .line 245471
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    .line 245472
    const/4 v2, 0x0

    .line 245473
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Rk;

    .line 245474
    iget-object p0, v1, LX/1Rk;->a:LX/1RA;

    if-eq p0, v2, :cond_1

    .line 245475
    iget-object v1, v1, LX/1Rk;->a:LX/1RA;

    .line 245476
    invoke-virtual {v1}, LX/1RA;->d()V

    :goto_1
    move-object v2, v1

    .line 245477
    goto :goto_0

    .line 245478
    :cond_0
    return-void

    :cond_1
    move-object v1, v2

    goto :goto_1
.end method

.method public final a(LX/5Mj;)V
    .locals 7

    .prologue
    .line 245460
    iget-object v0, p1, LX/5Mj;->f:LX/5Mk;

    move-object v2, v0

    .line 245461
    iget-object v0, p1, LX/5Mj;->e:Ljava/io/PrintWriter;

    move-object v3, v0

    .line 245462
    const/4 v0, 0x0

    .line 245463
    new-instance v1, LX/6Vc;

    invoke-direct {v1, p0}, LX/6Vc;-><init>(LX/1R1;)V

    move-object v1, v1

    .line 245464
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RA;

    .line 245465
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "story "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, LX/1R1;->b:LX/0g1;

    invoke-interface {v6}, LX/0g1;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245466
    invoke-virtual {v2, v0, p1}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 245467
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 245468
    goto :goto_0

    .line 245469
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 245459
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 245458
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 245449
    iget-object v1, p0, LX/1R1;->j:LX/1R7;

    .line 245450
    iget v2, v1, LX/1R7;->b:I

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 245451
    if-eqz v1, :cond_0

    .line 245452
    :goto_1
    return v0

    .line 245453
    :cond_0
    iget-object v1, p0, LX/1R1;->j:LX/1R7;

    .line 245454
    iget v2, v1, LX/1R7;->b:I

    move v1, v2

    .line 245455
    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 245456
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 245457
    iget-object v1, p0, LX/1R1;->j:LX/1R7;

    invoke-virtual {v1, v0}, LX/1R7;->c(I)I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 245444
    iget-object v1, p0, LX/1R1;->k:[I

    array-length v1, v1

    if-nez v1, :cond_0

    .line 245445
    :goto_0
    return v0

    .line 245446
    :cond_0
    iget-object v1, p0, LX/1R1;->k:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 245447
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 245448
    iget-object v1, p0, LX/1R1;->k:[I

    aget v0, v1, v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 245441
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-static {v0}, LX/1R1;->a(Ljava/util/List;)V

    .line 245442
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1R1;->i:Ljava/util/List;

    .line 245443
    return-void
.end method

.method public final d(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 245433
    iget-object v1, p0, LX/1R1;->k:[I

    array-length v1, v1

    if-nez v1, :cond_1

    .line 245434
    :cond_0
    :goto_0
    return v0

    .line 245435
    :cond_1
    iget-object v1, p0, LX/1R1;->k:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 245436
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 245437
    iget-object v2, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, LX/1R1;->k:[I

    aget v3, v3, v1

    if-le v2, v3, :cond_0

    .line 245438
    iget-object v0, p0, LX/1R1;->k:[I

    aget v2, v0, v1

    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    iget-object v3, p0, LX/1R1;->k:[I

    aget v1, v3, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 245439
    iget-object v1, v0, LX/1Rk;->a:LX/1RA;

    move-object v0, v1

    .line 245440
    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method public final d()LX/1R4;
    .locals 1

    .prologue
    .line 245432
    iget-object v0, p0, LX/1R1;->g:LX/1R4;

    return-object v0
.end method

.method public final e()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 245396
    const-string v0, "AdaptersCollection.regenerateInternalAdapters"

    const v1, -0x791ef995

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 245397
    iget-object v0, p0, LX/1R1;->e:LX/1PW;

    instance-of v0, v0, LX/1Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1R1;->e:LX/1PW;

    check-cast v0, LX/1Px;

    move-object v1, v0

    .line 245398
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/1R1;->j:LX/1R7;

    invoke-virtual {v0}, LX/1R7;->c()V

    .line 245399
    iget-object v0, p0, LX/1R1;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 245400
    iget-object v0, p0, LX/1R1;->b:LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/1R1;->k:[I

    .line 245401
    iget-object v4, p0, LX/1R1;->i:Ljava/util/List;

    .line 245402
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/1R1;->i:Ljava/util/List;

    move v3, v2

    .line 245403
    :goto_1
    iget-object v0, p0, LX/1R1;->b:LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 245404
    iget-object v0, p0, LX/1R1;->b:LX/0g1;

    invoke-interface {v0, v3}, LX/0g1;->a(I)Ljava/lang/Object;

    move-result-object v5

    .line 245405
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_1

    .line 245406
    invoke-static {p0, v5, v3}, LX/1R1;->a(LX/1R1;Ljava/lang/Object;I)V

    move v0, v2

    .line 245407
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 245408
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 245409
    :cond_1
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    .line 245410
    iget-object v6, v0, LX/1Rk;->a:LX/1RA;

    invoke-virtual {v6}, LX/1RA;->a()I

    move-result v6

    .line 245411
    if-eqz v1, :cond_6

    invoke-static {v5}, LX/1YB;->b(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, LX/1R1;->f:Ljava/util/Set;

    invoke-static {v5}, LX/1YB;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    :goto_3
    move v7, v7

    .line 245412
    if-nez v7, :cond_4

    .line 245413
    iget-object v7, p0, LX/1R1;->c:LX/1DZ;

    invoke-static {v0}, LX/1R1;->a(LX/1Rk;)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7, v5, v8}, LX/1DZ;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 245414
    invoke-static {p0, v5, v3}, LX/1R1;->a(LX/1R1;Ljava/lang/Object;I)V

    move v0, v2

    .line 245415
    goto :goto_2

    .line 245416
    :cond_2
    iget-object v7, p0, LX/1R1;->c:LX/1DZ;

    invoke-static {v0}, LX/1R1;->a(LX/1Rk;)Ljava/lang/Object;

    move-result-object v8

    iget-wide v10, v0, LX/1Rk;->c:J

    invoke-interface {v7, v5, v8, v10, v11}, LX/1DZ;->a(Ljava/lang/Object;Ljava/lang/Object;J)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 245417
    iget-object v0, p0, LX/1R1;->k:[I

    iget-object v5, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    aput v5, v0, v3

    .line 245418
    const/4 v0, 0x0

    move v5, v0

    :goto_4
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    iget-object v0, v0, LX/1Rk;->a:LX/1RA;

    invoke-virtual {v0}, LX/1RA;->a()I

    move-result v0

    if-ge v5, v0, :cond_3

    .line 245419
    iget-object v0, p0, LX/1R1;->j:LX/1R7;

    invoke-virtual {v0, v3}, LX/1R7;->b(I)V

    .line 245420
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    add-int v7, v2, v5

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245421
    iget-object v0, p0, LX/1R1;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, LX/1R1;->g(LX/1R1;I)V

    .line 245422
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_4

    .line 245423
    :cond_3
    :goto_5
    add-int v0, v2, v6

    goto/16 :goto_2

    .line 245424
    :cond_4
    invoke-static {p0, v5, v3}, LX/1R1;->a(LX/1R1;Ljava/lang/Object;I)V

    .line 245425
    iget-object v7, v0, LX/1Rk;->a:LX/1RA;

    invoke-virtual {v7}, LX/1RA;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245426
    goto :goto_5

    .line 245427
    :catchall_0
    move-exception v0

    const v1, -0x75785bf0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 245428
    :cond_5
    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/1R1;->a(Ljava/util/List;)V

    .line 245429
    iget-object v0, p0, LX/1R1;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245430
    const v0, 0x2482f29b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 245431
    return-void

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_3
.end method

.method public final e(I)Z
    .locals 1

    .prologue
    .line 245395
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/1R1;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 245394
    iget-object v0, p0, LX/1R1;->b:LX/0g1;

    invoke-interface {v0}, LX/0g1;->size()I

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 245393
    return-void
.end method
