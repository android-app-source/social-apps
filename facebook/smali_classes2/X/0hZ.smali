.class public final LX/0hZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ha;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/0gg;


# direct methods
.method public constructor <init>(LX/0gg;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 117838
    iput-object p1, p0, LX/0hZ;->b:LX/0gg;

    iput-object p2, p0, LX/0hZ;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 117839
    iget-object v0, p0, LX/0hZ;->b:LX/0gg;

    const/4 v1, 0x1

    .line 117840
    iput-boolean v1, v0, LX/0gg;->u:Z

    .line 117841
    iget-object v0, p0, LX/0hZ;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_tab_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117842
    iget-object v0, p0, LX/0hZ;->b:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    iget-object v1, p0, LX/0hZ;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0fd;->a(Landroid/content/Intent;)V

    .line 117843
    :cond_0
    iget-object v0, p0, LX/0hZ;->b:LX/0gg;

    iget-object v0, v0, LX/0gg;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    iget-object v1, p0, LX/0hZ;->a:Landroid/app/Activity;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_ATTACHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v0, v1, v2}, LX/15W;->a(Landroid/app/Activity;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    .line 117844
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 117845
    iget-object v0, p0, LX/0hZ;->b:LX/0gg;

    const/4 v1, 0x0

    .line 117846
    iput-boolean v1, v0, LX/0gg;->u:Z

    .line 117847
    return-void
.end method
