.class public abstract LX/0uk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)LX/0zO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 156909
    const/4 v0, 0x1

    return v0
.end method

.method public a(LX/0uh;)Z
    .locals 1

    .prologue
    .line 156903
    sget-object v0, LX/0uh;->HIGH:LX/0uh;

    invoke-virtual {p1, v0}, LX/0uh;->isAtLeast(LX/0uh;)Z

    move-result v0

    return v0
.end method

.method public abstract a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)Z"
        }
    .end annotation
.end method

.method public b(Ljava/lang/String;)LX/0zO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 156908
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()I
    .locals 4

    .prologue
    .line 156907
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 156906
    const/4 v0, 0x0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 156905
    const/16 v0, 0xa

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 156904
    const/4 v0, 0x0

    return v0
.end method
