.class public LX/0ZL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83226
    iput v0, p0, LX/0ZL;->b:I

    .line 83227
    iput v0, p0, LX/0ZL;->c:I

    .line 83228
    iput v0, p0, LX/0ZL;->d:I

    .line 83229
    new-array v0, p1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/0ZL;->a:[Ljava/lang/Object;

    .line 83230
    return-void
.end method

.method private declared-synchronized f()Z
    .locals 1

    .prologue
    .line 83224
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ZL;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 83231
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ZL;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Underflow"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 83232
    iget-object v0, p0, LX/0ZL;->a:[Ljava/lang/Object;

    iget v1, p0, LX/0ZL;->c:I

    aget-object v0, v0, v1

    .line 83233
    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    iget v2, p0, LX/0ZL;->c:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 83234
    iget v1, p0, LX/0ZL;->c:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, p0, LX/0ZL;->c:I

    .line 83235
    iget v1, p0, LX/0ZL;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/0ZL;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83236
    monitor-exit p0

    return-object v0

    .line 83237
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 83209
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ZL;->b:I

    if-gt v0, p1, :cond_0

    .line 83210
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 83212
    :cond_0
    :try_start_1
    iget v0, p0, LX/0ZL;->c:I

    add-int/2addr v0, p1

    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    .line 83213
    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    aget-object v0, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 83214
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 83215
    :goto_0
    monitor-exit p0

    return-void

    .line 83216
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/0ZL;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/0ZL;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 83217
    invoke-virtual {p0}, LX/0ZL;->a()Ljava/lang/Object;

    .line 83218
    :cond_1
    iget v0, p0, LX/0ZL;->b:I

    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Overflow"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 83219
    iget-object v0, p0, LX/0ZL;->a:[Ljava/lang/Object;

    iget v1, p0, LX/0ZL;->d:I

    aput-object p1, v0, v1

    .line 83220
    iget v0, p0, LX/0ZL;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, LX/0ZL;->d:I

    .line 83221
    iget v0, p0, LX/0ZL;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/0ZL;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 83223
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 83203
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/0ZL;->d()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 83204
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LX/0ZL;->b:I

    if-ge v0, v2, :cond_0

    .line 83205
    invoke-virtual {p0, v0}, LX/0ZL;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83207
    :cond_0
    monitor-exit p0

    return-object v1

    .line 83208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    .prologue
    .line 83193
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ZL;->b:I

    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()I
    .locals 1

    .prologue
    .line 83202
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0ZL;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 83194
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, LX/0ZL;->b:I

    .line 83195
    const/4 v1, 0x0

    iput v1, p0, LX/0ZL;->c:I

    .line 83196
    const/4 v1, 0x0

    iput v1, p0, LX/0ZL;->d:I

    .line 83197
    :goto_0
    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 83198
    iget-object v1, p0, LX/0ZL;->a:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83200
    :cond_0
    monitor-exit p0

    return-void

    .line 83201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
