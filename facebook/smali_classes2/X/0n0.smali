.class public final LX/0n0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/2Ak;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:LX/2Ai;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 133619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133620
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    .line 133621
    const/4 v0, 0x0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133622
    return-void
.end method


# virtual methods
.method public final a()LX/2Ai;
    .locals 1

    .prologue
    .line 133596
    monitor-enter p0

    .line 133597
    :try_start_0
    iget-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133598
    if-nez v0, :cond_0

    .line 133599
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    invoke-static {v0}, LX/2Ai;->a(Ljava/util/HashMap;)LX/2Ai;

    move-result-object v0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133600
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133601
    invoke-virtual {v0}, LX/2Ai;->a()LX/2Ai;

    move-result-object v0

    return-object v0

    .line 133602
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133610
    monitor-enter p0

    .line 133611
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(LX/0lJ;Z)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    monitor-exit p0

    return-object v0

    .line 133612
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133607
    monitor-enter p0

    .line 133608
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    monitor-exit p0

    return-object v0

    .line 133609
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133603
    monitor-enter p0

    .line 133604
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(LX/0lJ;Z)V

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 133605
    const/4 v0, 0x0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133606
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0my;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133613
    monitor-enter p0

    .line 133614
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(LX/0lJ;Z)V

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 133615
    const/4 v0, 0x0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133616
    :cond_0
    instance-of v0, p2, LX/2B9;

    if-eqz v0, :cond_1

    .line 133617
    check-cast p2, LX/2B9;

    invoke-interface {p2, p3}, LX/2B9;->a(LX/0my;)V

    .line 133618
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133580
    monitor-enter p0

    .line 133581
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 133582
    const/4 v0, 0x0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133583
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/0my;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133584
    monitor-enter p0

    .line 133585
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 133586
    const/4 v0, 0x0

    iput-object v0, p0, LX/0n0;->b:LX/2Ai;

    .line 133587
    :cond_0
    instance-of v0, p2, LX/2B9;

    if-eqz v0, :cond_1

    .line 133588
    check-cast p2, LX/2B9;

    invoke-interface {p2, p3}, LX/2B9;->a(LX/0my;)V

    .line 133589
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133590
    monitor-enter p0

    .line 133591
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(LX/0lJ;Z)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    monitor-exit p0

    return-object v0

    .line 133592
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133593
    monitor-enter p0

    .line 133594
    :try_start_0
    iget-object v0, p0, LX/0n0;->a:Ljava/util/HashMap;

    new-instance v1, LX/2Ak;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LX/2Ak;-><init>(Ljava/lang/Class;Z)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    monitor-exit p0

    return-object v0

    .line 133595
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
