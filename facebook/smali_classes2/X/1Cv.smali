.class public abstract LX/1Cv;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/1Cw;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 217207
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 217208
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1Cv;->a:Z

    return-void
.end method


# virtual methods
.method public abstract a(ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 217197
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, LX/1Cv;->a:Z

    .line 217198
    invoke-virtual {p0, p1}, LX/1Cv;->getItemViewType(I)I

    move-result v4

    .line 217199
    if-nez p2, :cond_1

    .line 217200
    invoke-virtual {p0, v4, p3}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    .line 217201
    if-eqz v3, :cond_0

    :goto_0
    const-string v1, "createDropDownView() shall not return null value!"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 217202
    :goto_1
    invoke-virtual {p0, p1}, LX/1Cv;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    .line 217203
    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217204
    iput-boolean v6, p0, LX/1Cv;->a:Z

    return-object v3

    :cond_0
    move v0, v6

    .line 217205
    goto :goto_0

    .line 217206
    :catchall_0
    move-exception v0

    iput-boolean v6, p0, LX/1Cv;->a:Z

    throw v0

    :cond_1
    move-object v3, p2

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 217188
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, LX/1Cv;->a:Z

    .line 217189
    invoke-virtual {p0, p1}, LX/1Cv;->getItemViewType(I)I

    move-result v4

    .line 217190
    if-nez p2, :cond_1

    .line 217191
    invoke-virtual {p0, v4, p3}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 217192
    if-eqz v3, :cond_0

    :goto_0
    const-string v1, "createView() shall not return null value!"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 217193
    :goto_1
    invoke-virtual {p0, p1}, LX/1Cv;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217194
    iput-boolean v6, p0, LX/1Cv;->a:Z

    return-object v3

    :cond_0
    move v0, v6

    .line 217195
    goto :goto_0

    .line 217196
    :catchall_0
    move-exception v0

    iput-boolean v6, p0, LX/1Cv;->a:Z

    throw v0

    :cond_1
    move-object v3, p2

    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 217184
    iget-boolean v0, p0, LX/1Cv;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Call to notifyDataSetChanged while the adapter is getting a view!"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 217185
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 217186
    return-void

    .line 217187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
