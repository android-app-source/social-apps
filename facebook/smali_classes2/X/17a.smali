.class public final LX/17a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Elu;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Elu;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 198944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198945
    iput-object p1, p0, LX/17a;->a:LX/0QB;

    .line 198946
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/Elu;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 198947
    new-instance v0, LX/17a;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/17a;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 198948
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 198949
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/17a;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 198950
    packed-switch p2, :pswitch_data_0

    .line 198951
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198952
    :pswitch_0
    new-instance p0, LX/Elv;

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v1

    check-cast v1, LX/Elm;

    invoke-direct {p0, v0, v1}, LX/Elv;-><init>(LX/0W3;LX/Elm;)V

    .line 198953
    move-object v0, p0

    .line 198954
    :goto_0
    return-object v0

    .line 198955
    :pswitch_1
    new-instance v1, LX/Elw;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Elw;-><init>(LX/Elm;)V

    .line 198956
    move-object v0, v1

    .line 198957
    goto :goto_0

    .line 198958
    :pswitch_2
    new-instance v1, LX/Elx;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Elx;-><init>(LX/Elm;)V

    .line 198959
    move-object v0, v1

    .line 198960
    goto :goto_0

    .line 198961
    :pswitch_3
    new-instance v1, LX/Ely;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Ely;-><init>(LX/Elm;)V

    .line 198962
    move-object v0, v1

    .line 198963
    goto :goto_0

    .line 198964
    :pswitch_4
    new-instance v1, LX/Elz;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Elz;-><init>(LX/Elm;)V

    .line 198965
    move-object v0, v1

    .line 198966
    goto :goto_0

    .line 198967
    :pswitch_5
    new-instance v2, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;

    invoke-static {p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v5

    check-cast v5, LX/Elm;

    invoke-static {p1}, LX/Em6;->b(LX/0QB;)LX/Em6;

    move-result-object v6

    check-cast v6, LX/Em6;

    invoke-static {p1}, LX/17a;->a(LX/0QB;)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/03V;LX/Elm;LX/Em6;LX/0Ot;)V

    .line 198968
    move-object v0, v2

    .line 198969
    goto :goto_0

    .line 198970
    :pswitch_6
    new-instance v1, LX/Em1;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Em1;-><init>(LX/Elm;)V

    .line 198971
    move-object v0, v1

    .line 198972
    goto :goto_0

    .line 198973
    :pswitch_7
    new-instance v1, LX/Em2;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Em2;-><init>(LX/Elm;)V

    .line 198974
    move-object v0, v1

    .line 198975
    goto :goto_0

    .line 198976
    :pswitch_8
    new-instance v0, LX/Em3;

    invoke-direct {v0}, LX/Em3;-><init>()V

    .line 198977
    move-object v0, v0

    .line 198978
    move-object v0, v0

    .line 198979
    goto :goto_0

    .line 198980
    :pswitch_9
    new-instance v1, LX/Em4;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Em4;-><init>(LX/Elm;)V

    .line 198981
    move-object v0, v1

    .line 198982
    goto/16 :goto_0

    .line 198983
    :pswitch_a
    new-instance v1, LX/Em5;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v0

    check-cast v0, LX/Elm;

    invoke-direct {v1, v0}, LX/Em5;-><init>(LX/Elm;)V

    .line 198984
    move-object v0, v1

    .line 198985
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 198986
    const/16 v0, 0xb

    return v0
.end method
