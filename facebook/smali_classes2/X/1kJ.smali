.class public LX/1kJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kK;


# instance fields
.field public final a:LX/1lR;


# direct methods
.method public constructor <init>(LX/1lR;)V
    .locals 0

    .prologue
    .line 309399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309400
    iput-object p1, p0, LX/1kJ;->a:LX/1lR;

    .line 309401
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309402
    iget-object v0, p0, LX/1kJ;->a:LX/1lR;

    invoke-virtual {v0}, LX/1lR;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309403
    iget-object v0, p0, LX/1kJ;->a:LX/1lR;

    invoke-virtual {v0}, LX/1lR;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/5ob;
    .locals 1

    .prologue
    .line 309404
    sget-object v0, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 309405
    if-ne p0, p1, :cond_0

    .line 309406
    const/4 v0, 0x1

    .line 309407
    :goto_0
    return v0

    .line 309408
    :cond_0
    instance-of v0, p1, LX/1kJ;

    if-nez v0, :cond_1

    .line 309409
    const/4 v0, 0x0

    goto :goto_0

    .line 309410
    :cond_1
    check-cast p1, LX/1kJ;

    .line 309411
    iget-object v0, p0, LX/1kJ;->a:LX/1lR;

    .line 309412
    iget-object v1, p1, LX/1kJ;->a:LX/1lR;

    move-object v1, v1

    .line 309413
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 309414
    iget-object v0, p0, LX/1kJ;->a:LX/1lR;

    invoke-virtual {v0}, LX/1lR;->hashCode()I

    move-result v0

    return v0
.end method
