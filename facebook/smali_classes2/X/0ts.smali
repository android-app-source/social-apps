.class public LX/0ts;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/contextual/ContextualResolver;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0uf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155759
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0uf;
    .locals 3

    .prologue
    .line 155760
    sget-object v0, LX/0ts;->a:LX/0uf;

    if-nez v0, :cond_1

    .line 155761
    const-class v1, LX/0ts;

    monitor-enter v1

    .line 155762
    :try_start_0
    sget-object v0, LX/0ts;->a:LX/0uf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 155763
    if-eqz v2, :cond_0

    .line 155764
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 155765
    invoke-static {v0}, LX/0tt;->a(LX/0QB;)LX/0uf;

    move-result-object p0

    check-cast p0, LX/0uf;

    .line 155766
    move-object p0, p0

    .line 155767
    move-object v0, p0

    .line 155768
    sput-object v0, LX/0ts;->a:LX/0uf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155769
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 155770
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155771
    :cond_1
    sget-object v0, LX/0ts;->a:LX/0uf;

    return-object v0

    .line 155772
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 155773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 155774
    invoke-static {p0}, LX/0tt;->a(LX/0QB;)LX/0uf;

    move-result-object v0

    check-cast v0, LX/0uf;

    .line 155775
    move-object v0, v0

    .line 155776
    return-object v0
.end method
