.class public LX/0kb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0kc;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile K:LX/0kb;


# instance fields
.field private final A:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final B:Ljava/lang/Object;

.field public C:Landroid/net/NetworkInfo;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private D:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private G:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private H:LX/0kg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCurrentNetworkInfoLock"
    .end annotation
.end field

.field private volatile I:Z

.field private final J:Ljava/lang/Runnable;

.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/PowerManager;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/telephony/TelephonyManager;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/net/wifi/WifiManager;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0So;

.field private final f:LX/0Xl;

.field private final g:LX/0Xl;

.field private final h:LX/03V;

.field private final i:LX/0dx;

.field public final j:LX/0Zb;

.field public final k:LX/0Uh;

.field private final l:LX/0W3;

.field private final m:Landroid/content/Context;

.field private final n:LX/0kd;

.field public final o:LX/0Wd;

.field private final p:LX/0ad;

.field private final q:LX/0ZL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ZL",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/lang/Object;

.field public final s:Ljava/util/concurrent/ExecutorService;

.field public t:LX/0kh;

.field private u:J

.field private v:J

.field public volatile w:J

.field public volatile x:J

.field private volatile y:Ljava/lang/Boolean;

.field public final z:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/telephony/TelephonyManager;LX/0Ot;LX/0Ot;LX/0So;LX/0Xl;LX/0Xl;LX/03V;LX/0dx;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0W3;LX/0Uh;Landroid/content/Context;LX/0kd;LX/0Wd;LX/0ad;)V
    .locals 4
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p16    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/net/ConnectivityManager;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0Ot",
            "<",
            "Landroid/net/wifi/WifiManager;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/os/PowerManager;",
            ">;",
            "LX/0So;",
            "LX/0Xl;",
            "LX/0Xl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0dx;",
            "LX/0Zb;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Landroid/content/Context;",
            "LX/0kd;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 127529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127530
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, LX/0kb;->r:Ljava/lang/Object;

    .line 127531
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, LX/0kb;->u:J

    .line 127532
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, LX/0kb;->v:J

    .line 127533
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, LX/0kb;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 127534
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, LX/0kb;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 127535
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, LX/0kb;->B:Ljava/lang/Object;

    .line 127536
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/0kb;->G:J

    .line 127537
    new-instance v2, Lcom/facebook/common/network/FbNetworkManager$1;

    invoke-direct {v2, p0}, Lcom/facebook/common/network/FbNetworkManager$1;-><init>(LX/0kb;)V

    iput-object v2, p0, LX/0kb;->J:Ljava/lang/Runnable;

    .line 127538
    iput-object p1, p0, LX/0kb;->a:LX/0Ot;

    .line 127539
    iput-object p2, p0, LX/0kb;->c:Landroid/telephony/TelephonyManager;

    .line 127540
    iput-object p3, p0, LX/0kb;->d:LX/0Ot;

    .line 127541
    iput-object p4, p0, LX/0kb;->b:LX/0Ot;

    .line 127542
    iput-object p5, p0, LX/0kb;->e:LX/0So;

    .line 127543
    iput-object p6, p0, LX/0kb;->f:LX/0Xl;

    .line 127544
    iput-object p7, p0, LX/0kb;->g:LX/0Xl;

    .line 127545
    iput-object p8, p0, LX/0kb;->h:LX/03V;

    .line 127546
    iput-object p9, p0, LX/0kb;->i:LX/0dx;

    .line 127547
    iput-object p10, p0, LX/0kb;->j:LX/0Zb;

    .line 127548
    iput-object p11, p0, LX/0kb;->s:Ljava/util/concurrent/ExecutorService;

    .line 127549
    move-object/from16 v0, p12

    iput-object v0, p0, LX/0kb;->l:LX/0W3;

    .line 127550
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0kb;->k:LX/0Uh;

    .line 127551
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0kb;->m:Landroid/content/Context;

    .line 127552
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0kb;->n:LX/0kd;

    .line 127553
    move-object/from16 v0, p16

    iput-object v0, p0, LX/0kb;->o:LX/0Wd;

    .line 127554
    move-object/from16 v0, p17

    iput-object v0, p0, LX/0kb;->p:LX/0ad;

    .line 127555
    new-instance v2, LX/0ZL;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, LX/0ZL;-><init>(I)V

    iput-object v2, p0, LX/0kb;->q:LX/0ZL;

    .line 127556
    return-void
.end method

.method public static A(LX/0kb;)V
    .locals 3

    .prologue
    .line 127557
    invoke-static {p0}, LX/0kb;->B(LX/0kb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127558
    invoke-virtual {p0}, LX/0kb;->d()Z

    move-result v0

    invoke-static {p0, v0}, LX/0kb;->b(LX/0kb;Z)V

    .line 127559
    :goto_0
    return-void

    .line 127560
    :cond_0
    iget-object v0, p0, LX/0kb;->o:LX/0Wd;

    iget-object v1, p0, LX/0kb;->J:Ljava/lang/Runnable;

    const v2, 0x540205b1

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static B(LX/0kb;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 127561
    iget-object v1, p0, LX/0kb;->n:LX/0kd;

    invoke-virtual {v1}, LX/0kd;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0kb;->p:LX/0ad;

    sget-short v2, LX/0ke;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static C(LX/0kb;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 127562
    iget-object v1, p0, LX/0kb;->n:LX/0kd;

    invoke-virtual {v1}, LX/0kd;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/0kb;->p:LX/0ad;

    sget-short v2, LX/0ke;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static D(LX/0kb;)Z
    .locals 2

    .prologue
    .line 127563
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/0kb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isDeviceIdleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 127564
    :try_start_0
    iget-object v0, p0, LX/0kb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v0}, LX/0kf;->a(Landroid/net/ConnectivityManager;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 127565
    :goto_0
    return v0

    .line 127566
    :catch_0
    move-exception v0

    .line 127567
    invoke-static {p0, v0}, LX/0kb;->a(LX/0kb;Ljava/lang/SecurityException;)V

    .line 127568
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private G()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 127569
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    .line 127570
    :cond_0
    :goto_0
    return-void

    .line 127571
    :cond_1
    iget-object v2, p0, LX/0kb;->m:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Application;

    if-eqz v2, :cond_2

    .line 127572
    iget-object v0, p0, LX/0kb;->m:Landroid/content/Context;

    check-cast v0, Landroid/app/Application;

    move-object v2, v0

    .line 127573
    :goto_1
    if-eqz v2, :cond_0

    .line 127574
    iget-object v3, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v3

    .line 127575
    :try_start_0
    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    if-nez v0, :cond_3

    .line 127576
    monitor-exit v3

    goto :goto_0

    .line 127577
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 127578
    :cond_2
    iget-object v2, p0, LX/0kb;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Application;

    if-eqz v2, :cond_7

    .line 127579
    iget-object v0, p0, LX/0kb;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    move-object v2, v0

    goto :goto_1

    .line 127580
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v4, :cond_5

    const/4 v0, 0x1

    .line 127581
    :goto_2
    if-eqz v0, :cond_6

    iget-object v1, p0, LX/0kb;->H:LX/0kg;

    if-nez v1, :cond_6

    iget-object v1, p0, LX/0kb;->p:LX/0ad;

    sget-short v4, LX/0ke;->d:S

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 127582
    new-instance v0, LX/0kg;

    invoke-direct {v0, p0}, LX/0kg;-><init>(LX/0kb;)V

    iput-object v0, p0, LX/0kb;->H:LX/0kg;

    .line 127583
    iget-object v0, p0, LX/0kb;->H:LX/0kg;

    invoke-virtual {v2, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 127584
    :cond_4
    :goto_3
    monitor-exit v3

    goto :goto_0

    :cond_5
    move v0, v1

    .line 127585
    goto :goto_2

    .line 127586
    :cond_6
    if-nez v0, :cond_4

    iget-object v0, p0, LX/0kb;->H:LX/0kg;

    if-eqz v0, :cond_4

    .line 127587
    iget-object v0, p0, LX/0kb;->H:LX/0kg;

    invoke-virtual {v2, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 127588
    const/4 v0, 0x0

    iput-object v0, p0, LX/0kb;->H:LX/0kg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_7
    move-object v2, v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/0kb;
    .locals 3

    .prologue
    .line 127589
    sget-object v0, LX/0kb;->K:LX/0kb;

    if-nez v0, :cond_1

    .line 127590
    const-class v1, LX/0kb;

    monitor-enter v1

    .line 127591
    :try_start_0
    sget-object v0, LX/0kb;->K:LX/0kb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 127592
    if-eqz v2, :cond_0

    .line 127593
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/0kb;->b(LX/0QB;)LX/0kb;

    move-result-object v0

    sput-object v0, LX/0kb;->K:LX/0kb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127594
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 127595
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 127596
    :cond_1
    sget-object v0, LX/0kb;->K:LX/0kb;

    return-object v0

    .line 127597
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 127598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0kb;Z)Landroid/net/NetworkInfo;
    .locals 4

    .prologue
    .line 127599
    iget-object v1, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 127600
    :try_start_0
    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    .line 127601
    invoke-direct {p0}, LX/0kb;->y()Landroid/net/NetworkInfo;

    .line 127602
    const/4 v2, 0x0

    iput-object v2, p0, LX/0kb;->D:Ljava/lang/String;

    .line 127603
    const/4 v2, 0x0

    iput-object v2, p0, LX/0kb;->E:Ljava/lang/String;

    .line 127604
    const/4 v2, 0x0

    iput-object v2, p0, LX/0kb;->F:Ljava/lang/String;

    .line 127605
    iget-object v2, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/0kb;->G:J

    .line 127606
    iget-object v2, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    invoke-virtual {p0, v2}, LX/0kb;->a(Landroid/net/NetworkInfo;)V

    .line 127607
    iget-object v2, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    .line 127608
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127609
    invoke-direct {p0}, LX/0kb;->F()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/0kb;->y:Ljava/lang/Boolean;

    .line 127610
    if-eqz p1, :cond_1

    if-eq v0, v2, :cond_1

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, LX/0kb;->d(Landroid/net/NetworkInfo;)Z

    move-result v1

    invoke-direct {p0, v2}, LX/0kb;->d(Landroid/net/NetworkInfo;)Z

    move-result v3

    if-ne v1, v3, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 127611
    :cond_0
    invoke-static {p0}, LX/0kb;->A(LX/0kb;)V

    .line 127612
    :cond_1
    return-object v2

    .line 127613
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/0kb;Ljava/lang/SecurityException;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 127614
    iget-object v0, p0, LX/0kb;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 127615
    rem-int/lit8 v1, v0, 0x40

    if-ne v1, v3, :cond_0

    .line 127616
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "success: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0kb;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failures: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127617
    const-string v1, "FbNetworkManager"

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 127618
    iput-object p1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 127619
    move-object v0, v0

    .line 127620
    iput-boolean v3, v0, LX/0VK;->d:Z

    .line 127621
    move-object v0, v0

    .line 127622
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 127623
    iget-object v1, p0, LX/0kb;->h:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 127624
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/0kb;Landroid/net/NetworkInfo;I)V
    .locals 3

    .prologue
    .line 127625
    new-instance v1, LX/0kh;

    iget-object v0, p0, LX/0kb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-direct {v1, p1, p2, v0}, LX/0kh;-><init>(Landroid/net/NetworkInfo;ILandroid/net/ConnectivityManager;)V

    .line 127626
    iget-object v0, p0, LX/0kb;->t:LX/0kh;

    const/4 v2, 0x0

    .line 127627
    if-nez v0, :cond_2

    .line 127628
    :cond_0
    :goto_0
    move v0, v2

    .line 127629
    if-nez v0, :cond_1

    .line 127630
    iput-object v1, p0, LX/0kb;->t:LX/0kh;

    .line 127631
    iget-object v0, p0, LX/0kb;->q:LX/0ZL;

    invoke-virtual {v0, v1}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 127632
    :cond_1
    return-void

    :cond_2
    iget-object p1, v1, LX/0kh;->a:LX/0kj;

    iget-object p2, v0, LX/0kh;->a:LX/0kj;

    invoke-static {p1, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, v1, LX/0kh;->c:I

    iget p2, v0, LX/0kh;->c:I

    if-ne p1, p2, :cond_0

    iget-boolean p1, v1, LX/0kh;->d:Z

    iget-boolean p2, v0, LX/0kh;->d:Z

    if-ne p1, p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static a$redex0(LX/0kb;Landroid/net/NetworkInfo;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 127633
    iget-object v0, p0, LX/0kb;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/common/network/FbNetworkManager$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/common/network/FbNetworkManager$4;-><init>(LX/0kb;Landroid/net/NetworkInfo;Ljava/lang/String;)V

    const v2, 0x1de1eac4

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 127634
    return-void
.end method

.method private static b(LX/0QB;)LX/0kb;
    .locals 19

    .prologue
    .line 127635
    new-instance v1, LX/0kb;

    const/16 v2, 0x23

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    const/16 v4, 0x26

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x33

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0dt;->a(LX/0QB;)LX/0dx;

    move-result-object v10

    check-cast v10, LX/0dx;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v11

    check-cast v11, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v13

    check-cast v13, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    const-class v15, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0kd;->a(LX/0QB;)LX/0kd;

    move-result-object v16

    check-cast v16, LX/0kd;

    invoke-static/range {p0 .. p0}, LX/0kl;->a(LX/0QB;)LX/0Wd;

    move-result-object v17

    check-cast v17, LX/0Wd;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-direct/range {v1 .. v18}, LX/0kb;-><init>(LX/0Ot;Landroid/telephony/TelephonyManager;LX/0Ot;LX/0Ot;LX/0So;LX/0Xl;LX/0Xl;LX/03V;LX/0dx;LX/0Zb;Ljava/util/concurrent/ExecutorService;LX/0W3;LX/0Uh;Landroid/content/Context;LX/0kd;LX/0Wd;LX/0ad;)V

    .line 127636
    return-object v1
.end method

.method public static b(LX/0kb;Z)V
    .locals 3

    .prologue
    .line 127637
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 127638
    iget-object v0, p0, LX/0kb;->g:LX/0Xl;

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 127639
    iget-object v2, p0, LX/0kb;->r:Ljava/lang/Object;

    monitor-enter v2

    .line 127640
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, LX/0kb;->u:J

    .line 127641
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/0kb;->v:J

    .line 127642
    iget-object v0, p0, LX/0kb;->r:Ljava/lang/Object;

    const v1, 0x1afca7be

    invoke-static {v0, v1}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 127643
    monitor-exit v2

    return-void

    .line 127644
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0

    .line 127645
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b$redex0(LX/0kb;I)V
    .locals 2

    .prologue
    .line 127646
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.common.hardware.ACTION_INET_CONDITION_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "INET_CONDITION"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 127647
    iget-object v1, p0, LX/0kb;->g:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 127648
    return-void
.end method

.method private d(Landroid/net/NetworkInfo;)Z
    .locals 1

    .prologue
    .line 127649
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/0kb;->D(LX/0kb;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127650
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "none"

    goto :goto_0
.end method

.method public static f(Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127651
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "none"

    goto :goto_0
.end method

.method public static x(LX/0kb;)Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 127652
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/0kb;->a(LX/0kb;Z)Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method private y()Landroid/net/NetworkInfo;
    .locals 3

    .prologue
    .line 127518
    iget-object v1, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 127519
    :try_start_0
    iget-object v0, p0, LX/0kb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 127520
    iget-object v2, p0, LX/0kb;->z:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127521
    :goto_0
    :try_start_1
    move-object v0, v0

    .line 127522
    iput-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    .line 127523
    invoke-direct {p0}, LX/0kb;->G()V

    .line 127524
    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    monitor-exit v1

    return-object v0

    .line 127525
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 127526
    :catch_0
    move-exception v0

    .line 127527
    invoke-static {p0, v0}, LX/0kb;->a(LX/0kb;Ljava/lang/SecurityException;)V

    .line 127528
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0ki;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127653
    iget-object v0, p0, LX/0kb;->q:LX/0ZL;

    invoke-virtual {v0}, LX/0ZL;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 9

    .prologue
    .line 127404
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    add-long v2, v0, p1

    .line 127405
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 127406
    iget-object v4, p0, LX/0kb;->r:Ljava/lang/Object;

    monitor-enter v4

    .line 127407
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-lez v5, :cond_0

    :try_start_0
    invoke-virtual {p0}, LX/0kb;->d()Z

    move-result v5

    if-nez v5, :cond_0

    .line 127408
    iget-object v5, p0, LX/0kb;->r:Ljava/lang/Object;

    const v6, -0x17608f95

    invoke-static {v5, v0, v1, v6}, LX/02L;->a(Ljava/lang/Object;JI)V

    .line 127409
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v0, v2, v0

    goto :goto_0

    .line 127410
    :cond_0
    monitor-exit v4

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/net/NetworkInfo;)V
    .locals 4
    .param p1    # Landroid/net/NetworkInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x0

    .line 127411
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127412
    iget-wide v0, p0, LX/0kb;->x:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 127413
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/0kb;->x:J

    .line 127414
    :cond_0
    :goto_0
    return-void

    .line 127415
    :cond_1
    iput-wide v2, p0, LX/0kb;->x:J

    goto :goto_0
.end method

.method public final b()Landroid/net/NetworkInfo;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127416
    const/4 v1, 0x0

    .line 127417
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 127418
    iget-object v4, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v4

    .line 127419
    :try_start_0
    iget-boolean v0, p0, LX/0kb;->I:Z

    if-eqz v0, :cond_1

    .line 127420
    const/4 v0, 0x0

    monitor-exit v4

    .line 127421
    :cond_0
    :goto_0
    return-object v0

    .line 127422
    :cond_1
    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/0kb;->C:Landroid/net/NetworkInfo;

    .line 127423
    :goto_1
    iget-wide v6, p0, LX/0kb;->G:J

    sub-long v6, v2, v6

    iget-object v5, p0, LX/0kb;->l:LX/0W3;

    .line 127424
    sget-wide v10, LX/0X5;->hS:J

    const/16 v12, 0x12c

    invoke-interface {v5, v10, v11, v12}, LX/0W4;->a(JI)I

    move-result v10

    mul-int/lit16 v10, v10, 0x3e8

    move v5, v10

    .line 127425
    int-to-long v8, v5

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 127426
    iput-wide v2, p0, LX/0kb;->G:J

    .line 127427
    const/4 v1, 0x1

    .line 127428
    :cond_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127429
    if-eqz v1, :cond_0

    .line 127430
    const-string v1, "invalidateCache"

    invoke-static {p0, v0, v1}, LX/0kb;->a$redex0(LX/0kb;Landroid/net/NetworkInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 127431
    :cond_3
    :try_start_1
    invoke-direct {p0}, LX/0kb;->y()Landroid/net/NetworkInfo;

    move-result-object v0

    goto :goto_1

    .line 127432
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 127433
    invoke-virtual {p0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0kb;->d(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127434
    iget-object v1, p0, LX/0kb;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 127435
    :try_start_0
    iget-wide v2, p0, LX/0kb;->u:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 127436
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    monitor-exit v1

    .line 127437
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/0kb;->u:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 127438
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final f()LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127439
    iget-object v1, p0, LX/0kb;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 127440
    :try_start_0
    iget-wide v2, p0, LX/0kb;->v:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 127441
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    monitor-exit v1

    .line 127442
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0kb;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/0kb;->v:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 127443
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 127444
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 127445
    if-eqz v0, :cond_0

    .line 127446
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v1, v0}, LX/0km;->a(II)Z

    move-result v0

    .line 127447
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 127448
    iget-object v0, p0, LX/0kb;->y:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0kb;->y:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    invoke-direct {p0}, LX/0kb;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0kb;->y:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final i()Landroid/net/NetworkInfo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127449
    invoke-virtual {p0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 127450
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/0kb;->D(LX/0kb;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127451
    :cond_0
    const/4 v0, 0x0

    .line 127452
    :cond_1
    return-object v0
.end method

.method public final init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127453
    invoke-virtual {p0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0kb;->a(Landroid/net/NetworkInfo;)V

    .line 127454
    new-instance v0, LX/0kn;

    invoke-direct {v0, p0}, LX/0kn;-><init>(LX/0kb;)V

    .line 127455
    iget-object v1, p0, LX/0kb;->f:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "android.net.conn.INET_CONDITION_ACTION"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    .line 127456
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    iget-object v1, p0, LX/0kb;->k:LX/0Uh;

    sget v2, LX/0ko;->a:I

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127457
    const-string v1, "android.os.action.DEVICE_IDLE_MODE_CHANGED"

    new-instance v2, LX/0kp;

    invoke-direct {v2, p0}, LX/0kp;-><init>(LX/0kb;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 127458
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    iget-object v1, p0, LX/0kb;->k:LX/0Uh;

    sget v2, LX/0ko;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127459
    iget-object v1, p0, LX/0kb;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    new-instance v2, LX/46L;

    invoke-direct {v2, p0}, LX/46L;-><init>(LX/0kb;)V

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->addDefaultNetworkActiveListener(Landroid/net/ConnectivityManager$OnNetworkActiveListener;)V

    .line 127460
    :cond_1
    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 127461
    iget-object v0, p0, LX/0kb;->i:LX/0dx;

    new-instance v1, LX/0kq;

    invoke-direct {v1, p0}, LX/0kq;-><init>(LX/0kb;)V

    invoke-interface {v0, v1}, LX/0dx;->a(LX/0kr;)V

    .line 127462
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127463
    const/4 v0, 0x0

    .line 127464
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 127465
    if-eqz v1, :cond_0

    .line 127466
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 127467
    if-eqz v1, :cond_0

    .line 127468
    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->name()Ljava/lang/String;

    move-result-object v0

    .line 127469
    :cond_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127470
    const-string v0, "none"

    .line 127471
    :cond_1
    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127472
    iget-object v1, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 127473
    :try_start_0
    iget-object v0, p0, LX/0kb;->D:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 127474
    iget-object v0, p0, LX/0kb;->D:Ljava/lang/String;

    monitor-exit v1

    .line 127475
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, LX/0kb;->e(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0kb;->D:Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 127476
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127477
    iget-object v1, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 127478
    :try_start_0
    iget-object v0, p0, LX/0kb;->E:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 127479
    iget-object v0, p0, LX/0kb;->E:Ljava/lang/String;

    monitor-exit v1

    .line 127480
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, LX/0kb;->f(Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0kb;->E:Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 127481
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final m()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127482
    iget-object v1, p0, LX/0kb;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 127483
    :try_start_0
    iget-object v0, p0, LX/0kb;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 127484
    iget-object v0, p0, LX/0kb;->F:Ljava/lang/String;

    monitor-exit v1

    .line 127485
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0kb;->F:Ljava/lang/String;

    monitor-exit v1

    goto :goto_0

    .line 127486
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final o()Landroid/net/wifi/WifiInfo;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 127487
    invoke-virtual {p0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127488
    :try_start_0
    iget-object v0, p0, LX/0kb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 127489
    :goto_0
    return-object v0

    .line 127490
    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 127491
    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 127492
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 127493
    if-eqz v0, :cond_0

    .line 127494
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127495
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "disconnected"

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 127496
    iget-object v0, p0, LX/0kb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 127497
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 127498
    iget-object v0, p0, LX/0kb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p0}, LX/0kb;->r()I

    move-result v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    return v0
.end method

.method public final t()J
    .locals 9

    .prologue
    .line 127499
    invoke-virtual {p0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v1

    iget-object v0, p0, LX/0kb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    const/4 v5, 0x0

    .line 127500
    const-string v2, ""

    .line 127501
    sget-object v3, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    .line 127502
    if-eqz v1, :cond_1

    .line 127503
    if-eqz v0, :cond_0

    .line 127504
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    .line 127505
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    .line 127506
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    .line 127507
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 127508
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    .line 127509
    :goto_0
    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v5

    const/4 v5, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v5

    const/4 v4, 0x2

    aput-object v2, v7, v4

    const/4 v2, 0x3

    aput-object v3, v7, v2

    invoke-static {v7}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v2

    int-to-long v2, v2

    move-wide v0, v2

    .line 127510
    return-wide v0

    :cond_1
    move v4, v5

    move v6, v5

    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    goto :goto_0
.end method

.method public final v()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127511
    invoke-virtual {p0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 127512
    if-eqz v2, :cond_1

    .line 127513
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    .line 127514
    if-ne v2, v0, :cond_0

    .line 127515
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 127516
    goto :goto_0

    :cond_1
    move v0, v1

    .line 127517
    goto :goto_0
.end method
