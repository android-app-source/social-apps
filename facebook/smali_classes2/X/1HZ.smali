.class public LX/1HZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ha;


# static fields
.field public static final a:LX/1Hb;

.field public static final j:[B

.field public static final k:[B

.field public static final l:[B

.field public static final m:[B

.field public static final n:I


# instance fields
.field private final b:LX/1Ha;

.field public final c:LX/1Hz;

.field public final d:LX/1Hz;

.field public final e:LX/1Hz;

.field public final f:LX/1IW;

.field public final g:LX/1FQ;

.field private final h:LX/03V;

.field public volatile i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2ux;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 227234
    new-instance v0, LX/1Hb;

    const-string v1, "some-fixed-value-for-now"

    invoke-direct {v0, v1}, LX/1Hb;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1HZ;->a:LX/1Hb;

    .line 227235
    new-array v0, v2, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    sput-object v0, LX/1HZ;->j:[B

    .line 227236
    const-string v0, "CRPT1"

    invoke-static {v0}, LX/1Hc;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1HZ;->k:[B

    .line 227237
    const-string v0, "CRPT2"

    invoke-static {v0}, LX/1Hc;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1HZ;->l:[B

    .line 227238
    const-string v0, "CRPT3"

    invoke-static {v0}, LX/1Hc;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1HZ;->m:[B

    .line 227239
    sget-object v0, LX/1HZ;->k:[B

    array-length v0, v0

    sput v0, LX/1HZ;->n:I

    return-void
.end method

.method public constructor <init>(LX/1Ha;LX/1Hz;LX/1Hz;LX/1Hz;LX/1IW;LX/0Or;LX/03V;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ha;",
            "LX/1Hz;",
            "LX/1Hz;",
            "LX/1Hz;",
            "LX/1IW;",
            "LX/0Or",
            "<",
            "LX/2ux;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 227222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227223
    iput-object p1, p0, LX/1HZ;->b:LX/1Ha;

    .line 227224
    iput-object p2, p0, LX/1HZ;->c:LX/1Hz;

    .line 227225
    iput-object p3, p0, LX/1HZ;->d:LX/1Hz;

    .line 227226
    iput-object p4, p0, LX/1HZ;->e:LX/1Hz;

    .line 227227
    iput-object p5, p0, LX/1HZ;->f:LX/1IW;

    .line 227228
    iput-object p6, p0, LX/1HZ;->i:LX/0Or;

    .line 227229
    iput-object p7, p0, LX/1HZ;->h:LX/03V;

    .line 227230
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 227231
    const/16 v1, 0x2000

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 227232
    new-instance v1, LX/1FQ;

    invoke-static {}, LX/1IX;->a()LX/1IX;

    move-result-object v2

    new-instance v3, LX/1F7;

    const/16 v4, 0x4000

    const v5, 0x7fffffff

    invoke-direct {v3, v4, v5, v0}, LX/1F7;-><init>(IILandroid/util/SparseIntArray;)V

    invoke-static {}, LX/1FT;->a()LX/1FT;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/1FQ;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    iput-object v1, p0, LX/1HZ;->g:LX/1FQ;

    .line 227233
    return-void
.end method

.method public static b([B[B)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 227216
    array-length v0, p0

    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 227217
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 227218
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 227219
    aget-byte v2, p0, v0

    aget-byte v3, p1, v0

    if-ne v2, v3, :cond_0

    .line 227220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 227221
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final U_()V
    .locals 1

    .prologue
    .line 227214
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0}, LX/0po;->U_()V

    .line 227215
    return-void
.end method

.method public final a(J)J
    .locals 3

    .prologue
    .line 227213
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0, p1, p2}, LX/1Ha;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/1bh;)LX/1gI;
    .locals 2

    .prologue
    .line 227211
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->a(LX/1bh;)LX/1gI;

    move-result-object v1

    .line 227212
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1gJ;

    invoke-direct {v0, p0, v1}, LX/1gJ;-><init>(LX/1HZ;LX/1gI;)V

    goto :goto_0
.end method

.method public final a(LX/1bh;LX/2uu;)LX/1gI;
    .locals 2

    .prologue
    .line 227201
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    new-instance v1, LX/2uv;

    invoke-direct {v1, p0, p2}, LX/2uv;-><init>(LX/1HZ;LX/2uu;)V

    invoke-interface {v0, p1, v1}, LX/1Ha;->a(LX/1bh;LX/2uu;)LX/1gI;

    move-result-object v1

    .line 227202
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1gJ;

    invoke-direct {v0, p0, v1}, LX/1gJ;-><init>(LX/1HZ;LX/1gI;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 227209
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0}, LX/0po;->b()V

    .line 227210
    return-void
.end method

.method public final b(LX/1bh;)Z
    .locals 1

    .prologue
    .line 227208
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->b(LX/1bh;)Z

    move-result v0

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 227207
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0}, LX/1Ha;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(LX/1bh;)Z
    .locals 1

    .prologue
    .line 227206
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->c(LX/1bh;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 227204
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0}, LX/1Ha;->d()V

    .line 227205
    return-void
.end method

.method public final d(LX/1bh;)Z
    .locals 1

    .prologue
    .line 227203
    iget-object v0, p0, LX/1HZ;->b:LX/1Ha;

    invoke-interface {v0, p1}, LX/1Ha;->d(LX/1bh;)Z

    move-result v0

    return v0
.end method
