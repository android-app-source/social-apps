.class public LX/0xI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/katana/login/WorkOnboardingFlowComponent;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hgt;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 162344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162345
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162346
    iput-object v0, p0, LX/0xI;->b:LX/0Ot;

    .line 162347
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162348
    iput-object v0, p0, LX/0xI;->c:LX/0Ot;

    .line 162349
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162350
    iput-object v0, p0, LX/0xI;->d:LX/0Ot;

    .line 162351
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 162352
    iput-object v0, p0, LX/0xI;->e:LX/0Ot;

    .line 162353
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 162342
    iget-object v0, p0, LX/0xI;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, LX/0xI;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 162354
    iget-object v0, p0, LX/0xI;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hgt;

    iget-object v1, p0, LX/0xI;->f:Landroid/app/Activity;

    .line 162355
    sget-object p0, LX/Hgs;->NEWSFEED:LX/Hgs;

    invoke-static {v0, v1, p0}, LX/Hgt;->a(LX/Hgt;Landroid/content/Context;LX/Hgs;)Z

    .line 162356
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 162343
    return-void
.end method
