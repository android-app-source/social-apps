.class public LX/0Sh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0Sh;


# instance fields
.field private volatile a:Z

.field private final b:LX/0Sj;

.field private c:Landroid/os/Handler;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 62039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62040
    iput-object p1, p0, LX/0Sh;->b:LX/0Sj;

    .line 62041
    return-void
.end method

.method public static a(LX/0QB;)LX/0Sh;
    .locals 4

    .prologue
    .line 62026
    sget-object v0, LX/0Sh;->d:LX/0Sh;

    if-nez v0, :cond_1

    .line 62027
    const-class v1, LX/0Sh;

    monitor-enter v1

    .line 62028
    :try_start_0
    sget-object v0, LX/0Sh;->d:LX/0Sh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62029
    if-eqz v2, :cond_0

    .line 62030
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62031
    new-instance p0, LX/0Sh;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v3

    check-cast v3, LX/0Sj;

    invoke-direct {p0, v3}, LX/0Sh;-><init>(LX/0Sj;)V

    .line 62032
    move-object v0, p0

    .line 62033
    sput-object v0, LX/0Sh;->d:LX/0Sh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62034
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62035
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62036
    :cond_1
    sget-object v0, LX/0Sh;->d:LX/0Sh;

    return-object v0

    .line 62037
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 62020
    monitor-enter p0

    .line 62021
    :try_start_0
    iget-object v0, p0, LX/0Sh;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 62022
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0Sh;->c:Landroid/os/Handler;

    .line 62023
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62024
    iget-object v0, p0, LX/0Sh;->c:Landroid/os/Handler;

    return-object v0

    .line 62025
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final varargs a(LX/3nE;[Ljava/lang/Object;)LX/3nE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Params:",
            "Ljava/lang/Object;",
            "Progress:",
            "Ljava/lang/Object;",
            "Result:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3nE",
            "<TParams;TProgress;TResult;>;[TParams;)",
            "LX/3nE",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 61987
    iget-object v0, p0, LX/0Sh;->b:LX/0Sj;

    invoke-virtual {p1, v0, p2}, LX/3nE;->a(LX/0Sj;[Ljava/lang/Object;)LX/3nE;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 62018
    const-string v0, "This operation must be run on UI thread."

    invoke-virtual {p0, v0}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 62019
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;",
            "LX/0TF",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 62011
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62012
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62013
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 62014
    const-string v1, "Must be called on a handler thread"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62015
    new-instance v0, LX/0Td;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, LX/0Td;-><init>(Landroid/os/Handler;)V

    .line 62016
    invoke-static {p1, p2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 62017
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 62007
    invoke-virtual {p0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62008
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 62009
    :goto_0
    return-void

    .line 62010
    :cond_0
    invoke-direct {p0}, LX/0Sh;->d()Landroid/os/Handler;

    move-result-object v0

    const v1, 0x12aab90f

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 62005
    invoke-direct {p0}, LX/0Sh;->d()Landroid/os/Handler;

    move-result-object v0

    const v1, 0x2b0e01ab

    invoke-static {v0, p1, p2, p3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 62006
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62002
    iget-boolean v0, p0, LX/0Sh;->a:Z

    if-nez v0, :cond_0

    .line 62003
    invoke-virtual {p0}, LX/0Sh;->c()Z

    move-result v0

    invoke-static {v0, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 62004
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 62000
    const-string v0, "This operation can\'t be run on UI thread."

    invoke-virtual {p0, v0}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 62001
    return-void
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 61998
    invoke-direct {p0}, LX/0Sh;->d()Landroid/os/Handler;

    move-result-object v0

    const v1, -0x474dc263

    invoke-static {v0, p1, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 61999
    return-void
.end method

.method public final b(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 61995
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 61996
    const v1, -0x57b8fac0

    invoke-static {v0, p1, p2, p3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 61997
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 61991
    iget-boolean v0, p0, LX/0Sh;->a:Z

    if-nez v0, :cond_0

    .line 61992
    invoke-virtual {p0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 61993
    :cond_0
    return-void

    .line 61994
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 61989
    invoke-direct {p0}, LX/0Sh;->d()Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 61990
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 61988
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
