.class public LX/0hO;
.super Lcom/facebook/apptab/glyph/BadgableGlyphView;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public h:LX/0xk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 115908
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0hO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 115909
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115894
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/0hO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115895
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115889
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/apptab/glyph/BadgableGlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115890
    const-class v0, LX/0hO;

    invoke-static {v0, p0}, LX/0hO;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 115891
    const v0, 0x7f020208

    invoke-virtual {p0, v0}, LX/0hO;->setBackgroundResource(I)V

    .line 115892
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setBadgeOutlineColor(I)V

    .line 115893
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/0hO;

    invoke-static {p0}, LX/0xk;->a(LX/0QB;)LX/0xk;

    move-result-object p0

    check-cast p0, LX/0xk;

    iput-object p0, p1, LX/0hO;->h:LX/0xk;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 6

    .prologue
    const/16 v1, 0x9

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 115896
    if-eqz p2, :cond_3

    .line 115897
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-le v0, v1, :cond_1

    .line 115898
    invoke-virtual {p0}, LX/0hO;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a36

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 115899
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115900
    return-void

    .line 115901
    :cond_1
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-lez v0, :cond_2

    .line 115902
    invoke-virtual {p0}, LX/0hO;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f003b

    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 115903
    :cond_2
    invoke-virtual {p0}, LX/0hO;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a34

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 115904
    :cond_3
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-le v0, v1, :cond_4

    .line 115905
    invoke-virtual {p0}, LX/0hO;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a35

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 115906
    :cond_4
    iget v0, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    if-lez v0, :cond_0

    .line 115907
    invoke-virtual {p0}, LX/0hO;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f003a

    iget v2, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    iget v4, p0, Lcom/facebook/apptab/glyph/BadgableGlyphView;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 115887
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0hO;->a(Ljava/lang/CharSequence;Z)V

    .line 115888
    return-void
.end method

.method public setGlyphImage(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 115884
    iget-object v0, p0, LX/0hO;->h:LX/0xk;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0xk;->a(D)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 115885
    invoke-super {p0, p1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    .line 115886
    return-void
.end method

.method public setTabIconImageResource(I)V
    .locals 4

    .prologue
    .line 115880
    invoke-virtual {p0}, LX/0hO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 115881
    iget-object v1, p0, LX/0hO;->h:LX/0xk;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0xk;->a(D)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 115882
    invoke-super {p0, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setGlyphImage(Landroid/graphics/drawable/Drawable;)V

    .line 115883
    return-void
.end method

.method public setUnreadCount(I)V
    .locals 1

    .prologue
    .line 115877
    invoke-super {p0, p1}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setUnreadCount(I)V

    .line 115878
    iget-object v0, p0, LX/0hO;->i:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, LX/0hO;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115879
    return-void
.end method
