.class public final LX/1fk;
.super LX/1cZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1cZ",
        "<TT;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final synthetic a:LX/1fC;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1ca",
            "<TT;>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "IncreasingQualityDataSource.this"
    .end annotation
.end field

.field private c:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "IncreasingQualityDataSource.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1fC;)V
    .locals 5

    .prologue
    .line 292223
    iput-object p1, p0, LX/1fk;->a:LX/1fC;

    invoke-direct {p0}, LX/1cZ;-><init>()V

    .line 292224
    iget-object v0, p1, LX/1fC;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 292225
    iput v2, p0, LX/1fk;->c:I

    .line 292226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1fk;->b:Ljava/util/ArrayList;

    .line 292227
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 292228
    iget-object v0, p1, LX/1fC;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;

    .line 292229
    iget-object v3, p0, LX/1fk;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292230
    new-instance v3, LX/1fp;

    invoke-direct {v3, p0, v1}, LX/1fp;-><init>(LX/1fk;I)V

    .line 292231
    sget-object v4, LX/1fo;->a:LX/1fo;

    move-object v4, v4

    .line 292232
    invoke-interface {v0, v3, v4}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 292233
    invoke-interface {v0}, LX/1ca;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 292235
    :cond_0
    return-void
.end method

.method private static declared-synchronized a(LX/1fk;I)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1fk;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1fk;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/1fk;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292219
    if-eqz p0, :cond_0

    .line 292220
    invoke-interface {p0}, LX/1ca;->g()Z

    .line 292221
    :cond_0
    return-void
.end method

.method public static a(LX/1fk;ILX/1ca;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1ca",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 292175
    monitor-enter p0

    .line 292176
    :try_start_0
    iget v0, p0, LX/1fk;->c:I

    .line 292177
    iget v1, p0, LX/1fk;->c:I

    .line 292178
    invoke-static {p0, p1}, LX/1fk;->a(LX/1fk;I)LX/1ca;

    move-result-object v2

    if-ne p2, v2, :cond_0

    iget v2, p0, LX/1fk;->c:I

    if-ne p1, v2, :cond_2

    .line 292179
    :cond_0
    monitor-exit p0

    .line 292180
    :cond_1
    return-void

    .line 292181
    :cond_2
    invoke-static {p0}, LX/1fk;->h(LX/1fk;)LX/1ca;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz p3, :cond_4

    iget v2, p0, LX/1fk;->c:I

    if-ge p1, v2, :cond_4

    .line 292182
    :cond_3
    iput p1, p0, LX/1fk;->c:I

    .line 292183
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292184
    :goto_1
    if-le v0, p1, :cond_1

    .line 292185
    invoke-static {p0, v0}, LX/1fk;->b(LX/1fk;I)LX/1ca;

    move-result-object v1

    invoke-static {v1}, LX/1fk;->a(LX/1ca;)V

    .line 292186
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 292187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move p1, v1

    goto :goto_0
.end method

.method private static declared-synchronized b(LX/1fk;I)LX/1ca;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 292218
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1fk;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1fk;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v0, p0, LX/1fk;->b:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b$redex0(LX/1fk;ILX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1ca",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292214
    invoke-direct {p0, p1, p2}, LX/1fk;->c(ILX/1ca;)LX/1ca;

    move-result-object v0

    invoke-static {v0}, LX/1fk;->a(LX/1ca;)V

    .line 292215
    if-nez p1, :cond_0

    .line 292216
    invoke-interface {p2}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    .line 292217
    :cond_0
    return-void
.end method

.method private declared-synchronized c(ILX/1ca;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1ca",
            "<TT;>;)",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292208
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1fk;->h(LX/1fk;)LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-ne p2, v0, :cond_1

    .line 292209
    const/4 p2, 0x0

    .line 292210
    :cond_0
    :goto_0
    monitor-exit p0

    return-object p2

    .line 292211
    :cond_1
    :try_start_1
    invoke-static {p0, p1}, LX/1fk;->a(LX/1fk;I)LX/1ca;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 292212
    invoke-static {p0, p1}, LX/1fk;->b(LX/1fk;I)LX/1ca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p2

    goto :goto_0

    .line 292213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h(LX/1fk;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1ca",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292207
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1fk;->c:I

    invoke-static {p0, v0}, LX/1fk;->a(LX/1fk;I)LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 292204
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1fk;->h(LX/1fk;)LX/1ca;

    move-result-object v0

    .line 292205
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1ca;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292201
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/1fk;->h(LX/1fk;)LX/1ca;

    move-result-object v0

    .line 292202
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1ca;->d()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 292203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 292188
    monitor-enter p0

    .line 292189
    :try_start_0
    invoke-super {p0}, LX/1cZ;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 292190
    monitor-exit p0

    .line 292191
    :goto_0
    return v0

    .line 292192
    :cond_0
    iget-object v2, p0, LX/1fk;->b:Ljava/util/ArrayList;

    .line 292193
    const/4 v1, 0x0

    iput-object v1, p0, LX/1fk;->b:Ljava/util/ArrayList;

    .line 292194
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292195
    if-eqz v2, :cond_1

    move v1, v0

    .line 292196
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 292197
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ca;

    invoke-static {v0}, LX/1fk;->a(LX/1ca;)V

    .line 292198
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 292199
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 292200
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
