.class public LX/1a5;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/1a6;
.implements LX/1a7;
.implements LX/1a8;
.implements LX/1a9;


# instance fields
.field public a:LX/1Rn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 276421
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 276422
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, LX/1a5;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 276423
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/1a5;

    new-instance v1, LX/1Rn;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v5

    check-cast v5, LX/0Zm;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-direct/range {v1 .. v7}, LX/1Rn;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0Zb;LX/0Zm;Ljava/util/Random;Landroid/content/Context;)V

    move-object v0, v1

    check-cast v0, LX/1Rn;

    iput-object v0, p0, LX/1a5;->a:LX/1Rn;

    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 276416
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    .line 276417
    invoke-virtual {v0}, LX/1Rn;->b()Z

    move-result p0

    if-nez p0, :cond_0

    .line 276418
    iget-boolean p0, v0, LX/1Rn;->C:Z

    move p0, p0

    .line 276419
    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 276420
    return v0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final addView(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 276406
    invoke-virtual {p0}, LX/1a5;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 276407
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 276408
    if-eqz v0, :cond_2

    .line 276409
    instance-of v1, v0, Landroid/widget/AbsListView$LayoutParams;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/1a3;

    if-nez v1, :cond_0

    instance-of v1, v0, LX/7Uk;

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 276410
    if-nez v0, :cond_2

    .line 276411
    const-string v0, "\nChild view %s with id %d has %s.\nThis will cause a ClassCastException when ViewDiagnostics are disabled.\nChild views should NOT call setLayoutParams() on themselves.\nPlease see intern/wiki/Android/SetLayoutParams."

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 276412
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 276413
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 276414
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->addView(Landroid/view/View;)V

    .line 276415
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 276405
    instance-of v0, p1, LX/7Uk;

    return v0
.end method

.method public final cr_()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 276404
    invoke-virtual {p0}, LX/1a5;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, LX/1a7;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/1a7;

    invoke-interface {v0}, LX/1a7;->cr_()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    .line 276337
    invoke-direct {p0}, LX/1a5;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276338
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 276339
    :goto_0
    return-void

    .line 276340
    :cond_0
    const-string v0, "ViewDiagnosticsWrapper.dispatchDraw"

    const v1, 0x43ab0bf2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 276341
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276342
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    const v1, 0xa296cdc

    invoke-static {v1}, LX/02m;->b(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1Rn;->f(J)V

    .line 276343
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1a5;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 276344
    iget-object v1, p0, LX/1a5;->a:LX/1Rn;

    invoke-virtual {v1, v0}, LX/1Rn;->a(Landroid/view/View;)V

    .line 276345
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    invoke-virtual {v0}, LX/1Rn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276346
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    invoke-virtual {v0, p1}, LX/1Rn;->draw(Landroid/graphics/Canvas;)V

    .line 276347
    :cond_1
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    .line 276348
    iget-boolean v1, v0, LX/1Rn;->C:Z

    move v0, v1

    .line 276349
    if-eqz v0, :cond_2

    .line 276350
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    .line 276351
    invoke-static {v0}, LX/1Rn;->g(LX/1Rn;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 276352
    iget-boolean v4, v0, LX/1Rn;->C:Z

    move v4, v4

    .line 276353
    if-nez v4, :cond_3

    .line 276354
    :cond_2
    :goto_1
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    const-wide/16 v6, -0x1

    .line 276355
    iput-wide v6, v0, LX/1Rn;->p:J

    .line 276356
    iput-wide v6, v0, LX/1Rn;->q:J

    .line 276357
    iput-wide v6, v0, LX/1Rn;->r:J

    .line 276358
    iput-wide v6, v0, LX/1Rn;->s:J

    .line 276359
    iput-wide v6, v0, LX/1Rn;->t:J

    .line 276360
    const-wide/16 v4, 0x0

    iput-wide v4, v0, LX/1Rn;->u:J

    .line 276361
    iput-wide v6, v0, LX/1Rn;->v:J

    .line 276362
    iput-wide v6, v0, LX/1Rn;->w:J

    .line 276363
    const/4 v4, 0x0

    iput-boolean v4, v0, LX/1Rn;->C:Z

    .line 276364
    goto :goto_0

    .line 276365
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1a5;->a:LX/1Rn;

    const v2, -0x601f09

    invoke-static {v2}, LX/02m;->b(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/1Rn;->f(J)V

    throw v0

    .line 276366
    :cond_3
    iget-object v4, v0, LX/1Rn;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/widget/viewdiagnostics/ViewDiagnostics$ReportEventRunnable;

    .line 276367
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "view_scroll_perf"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 276368
    const-string v8, "prepare_time"

    iget-wide v9, v0, LX/1Rn;->p:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276369
    const-string v8, "prepare_async_time"

    iget-wide v9, v0, LX/1Rn;->q:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276370
    const-string v8, "bind_time"

    iget-wide v9, v0, LX/1Rn;->r:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276371
    const-string v8, "measure_time"

    iget-wide v9, v0, LX/1Rn;->t:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276372
    const-string v8, "layout_time"

    iget-wide v9, v0, LX/1Rn;->s:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276373
    const-string v8, "draw_time"

    iget-wide v9, v0, LX/1Rn;->u:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276374
    const-string v8, "class_name"

    iget-object v9, v0, LX/1Rn;->o:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 276375
    move-object v6, v7

    .line 276376
    invoke-direct {v5, v0, v6}, Lcom/facebook/widget/viewdiagnostics/ViewDiagnostics$ReportEventRunnable;-><init>(LX/1Rn;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    const v6, -0x7a278571

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 276424
    new-instance v0, LX/7Uk;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/7Uk;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 276403
    new-instance v0, LX/7Uk;

    invoke-virtual {p0}, LX/1a5;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/7Uk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 276402
    new-instance v0, LX/7Uk;

    invoke-direct {v0, p1}, LX/7Uk;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final getChildAt(I)Landroid/view/View;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 276401
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewDiagnostics()LX/1Rn;
    .locals 1

    .prologue
    .line 276400
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    return-object v0
.end method

.method public getWrappedView()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276397
    invoke-virtual {p0}, LX/1a5;->getChildCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 276398
    invoke-super {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 276399
    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 276387
    invoke-direct {p0}, LX/1a5;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276388
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V

    .line 276389
    :goto_0
    return-void

    .line 276390
    :cond_0
    const-string v0, "ViewDiagnosticsWrapper.onLayout"

    const v1, 0x703b5794

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 276391
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276392
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    const v1, -0x62aec284

    invoke-static {v1}, LX/02m;->b(I)J

    move-result-wide v2

    .line 276393
    iput-wide v2, v0, LX/1Rn;->s:J

    .line 276394
    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1a5;->a:LX/1Rn;

    const v2, -0x50ae82fa

    invoke-static {v2}, LX/02m;->b(I)J

    move-result-wide v2

    .line 276395
    iput-wide v2, v1, LX/1Rn;->s:J

    .line 276396
    throw v0
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    .line 276377
    invoke-direct {p0}, LX/1a5;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276378
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 276379
    :goto_0
    return-void

    .line 276380
    :cond_0
    const-string v0, "ViewDiagnosticsWrapper.onMeasure"

    const v1, -0x3bd47353

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 276381
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276382
    iget-object v0, p0, LX/1a5;->a:LX/1Rn;

    const v1, -0x71b7dcce

    invoke-static {v1}, LX/02m;->b(I)J

    move-result-wide v2

    .line 276383
    iput-wide v2, v0, LX/1Rn;->t:J

    .line 276384
    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/1a5;->a:LX/1Rn;

    const v2, -0x413e2df4

    invoke-static {v2}, LX/02m;->b(I)J

    move-result-wide v2

    .line 276385
    iput-wide v2, v1, LX/1Rn;->t:J

    .line 276386
    throw v0
.end method
