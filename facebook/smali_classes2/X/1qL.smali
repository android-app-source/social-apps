.class public LX/1qL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0lB;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0lB;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1qN;",
            ">;",
            "LX/0lB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330528
    iput-object p1, p0, LX/1qL;->a:LX/0Ot;

    .line 330529
    iput-object p2, p0, LX/1qL;->b:LX/0lB;

    .line 330530
    return-void
.end method

.method public static a(LX/0QB;)LX/1qL;
    .locals 5

    .prologue
    .line 330553
    const-class v1, LX/1qL;

    monitor-enter v1

    .line 330554
    :try_start_0
    sget-object v0, LX/1qL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 330555
    sput-object v2, LX/1qL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 330556
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330557
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 330558
    new-instance v4, LX/1qL;

    const/16 v3, 0x3cd

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-direct {v4, p0, v3}, LX/1qL;-><init>(LX/0Ot;LX/0lB;)V

    .line 330559
    move-object v0, v4

    .line 330560
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 330561
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330562
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 330563
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 330564
    iget-object v0, p0, LX/1qL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    invoke-virtual {v0}, LX/1qN;->c()V

    .line 330565
    return-void
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 330531
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 330532
    const-string v1, "composer_save_session"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330533
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 330534
    const-string v1, "saveSession"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    .line 330535
    if-eqz v0, :cond_0

    .line 330536
    iget-object v1, p0, LX/1qL;->b:LX/0lB;

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 330537
    iget-object v0, p0, LX/1qL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1qN;

    .line 330538
    iget-object v2, v0, LX/1qN;->e:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 330539
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330540
    invoke-virtual {v0}, LX/1qN;->c()V

    .line 330541
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 330542
    sget-object v3, LX/1qP;->a:LX/0U1;

    invoke-virtual {v3}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 330543
    iget-object v3, v0, LX/1qN;->d:LX/1qR;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "drafts"

    const-string p0, ""

    const p1, -0x3f9ce6c

    invoke-static {p1}, LX/03h;->a(I)V

    invoke-virtual {v3, v4, p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x72b18db2

    invoke-static {v2}, LX/03h;->a(I)V

    .line 330544
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 330545
    move-object v0, v0

    .line 330546
    :goto_0
    return-object v0

    .line 330547
    :cond_1
    const-string v1, "composer_delete_session"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 330548
    invoke-virtual {p0}, LX/1qL;->b()V

    .line 330549
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 330550
    move-object v0, v0

    .line 330551
    goto :goto_0

    .line 330552
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
