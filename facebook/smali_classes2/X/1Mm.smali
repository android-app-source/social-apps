.class public final enum LX/1Mm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Mm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Mm;

.field public static final enum CDMA:LX/1Mm;

.field public static final enum GSM:LX/1Mm;

.field public static final enum LTE:LX/1Mm;

.field public static final enum UNKNOWN:LX/1Mm;

.field public static final enum WCDMA:LX/1Mm;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236309
    new-instance v0, LX/1Mm;

    const-string v1, "CDMA"

    invoke-direct {v0, v1, v2}, LX/1Mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Mm;->CDMA:LX/1Mm;

    new-instance v0, LX/1Mm;

    const-string v1, "GSM"

    invoke-direct {v0, v1, v3}, LX/1Mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Mm;->GSM:LX/1Mm;

    new-instance v0, LX/1Mm;

    const-string v1, "LTE"

    invoke-direct {v0, v1, v4}, LX/1Mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Mm;->LTE:LX/1Mm;

    new-instance v0, LX/1Mm;

    const-string v1, "WCDMA"

    invoke-direct {v0, v1, v5}, LX/1Mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Mm;->WCDMA:LX/1Mm;

    new-instance v0, LX/1Mm;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/1Mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1Mm;->UNKNOWN:LX/1Mm;

    .line 236310
    const/4 v0, 0x5

    new-array v0, v0, [LX/1Mm;

    sget-object v1, LX/1Mm;->CDMA:LX/1Mm;

    aput-object v1, v0, v2

    sget-object v1, LX/1Mm;->GSM:LX/1Mm;

    aput-object v1, v0, v3

    sget-object v1, LX/1Mm;->LTE:LX/1Mm;

    aput-object v1, v0, v4

    sget-object v1, LX/1Mm;->WCDMA:LX/1Mm;

    aput-object v1, v0, v5

    sget-object v1, LX/1Mm;->UNKNOWN:LX/1Mm;

    aput-object v1, v0, v6

    sput-object v0, LX/1Mm;->$VALUES:[LX/1Mm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 236308
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Mm;
    .locals 1

    .prologue
    .line 236307
    const-class v0, LX/1Mm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Mm;

    return-object v0
.end method

.method public static values()[LX/1Mm;
    .locals 1

    .prologue
    .line 236306
    sget-object v0, LX/1Mm;->$VALUES:[LX/1Mm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Mm;

    return-object v0
.end method
