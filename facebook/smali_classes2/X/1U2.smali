.class public LX/1U2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1U2;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255081
    iput-object p1, p0, LX/1U2;->a:LX/0Ot;

    .line 255082
    return-void
.end method

.method public static a(LX/0QB;)LX/1U2;
    .locals 4

    .prologue
    .line 255066
    sget-object v0, LX/1U2;->b:LX/1U2;

    if-nez v0, :cond_1

    .line 255067
    const-class v1, LX/1U2;

    monitor-enter v1

    .line 255068
    :try_start_0
    sget-object v0, LX/1U2;->b:LX/1U2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 255069
    if-eqz v2, :cond_0

    .line 255070
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 255071
    new-instance v3, LX/1U2;

    const/16 p0, 0x21f1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1U2;-><init>(LX/0Ot;)V

    .line 255072
    move-object v0, v3

    .line 255073
    sput-object v0, LX/1U2;->b:LX/1U2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 255075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 255076
    :cond_1
    sget-object v0, LX/1U2;->b:LX/1U2;

    return-object v0

    .line 255077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 255078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 255079
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255062
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255063
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255064
    sget-object v0, Lcom/facebook/feedplugins/worktrending/rows/WorkTrendingFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255065
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255060
    const-class v0, Lcom/facebook/graphql/model/GraphQLWorkCommunityTrendingFeedUnit;

    iget-object v1, p0, LX/1U2;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255061
    return-void
.end method
