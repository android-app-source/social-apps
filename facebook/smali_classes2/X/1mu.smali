.class public LX/1mu;
.super LX/1ms;
.source ""


# direct methods
.method public constructor <init>(Landroid/view/DisplayList;)V
    .locals 0

    .prologue
    .line 314258
    invoke-direct {p0, p1}, LX/1ms;-><init>(Landroid/view/DisplayList;)V

    .line 314259
    return-void
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Canvas;
    .locals 1

    .prologue
    .line 314260
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0, p1, p2}, Landroid/view/DisplayList;->start(II)Landroid/view/HardwareCanvas;

    move-result-object v0

    .line 314261
    check-cast v0, Landroid/graphics/Canvas;

    return-object v0
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 314262
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/DisplayList;->setLeftTopRightBottom(IIII)V

    .line 314263
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/DisplayList;->setClipToBounds(Z)V

    .line 314264
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 314265
    iget-object v0, p0, LX/1ms;->a:Landroid/view/DisplayList;

    invoke-virtual {v0}, Landroid/view/DisplayList;->end()V

    .line 314266
    return-void
.end method
