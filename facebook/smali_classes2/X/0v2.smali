.class public LX/0v2;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0v2;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 7
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/api/feed/annotation/IsNewsFeedDbBackCompatEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157438
    const-string v3, "feed"

    const/16 v4, 0x3b

    new-instance v5, LX/0v3;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v5, v0}, LX/0v3;-><init>(Z)V

    new-instance v0, LX/0v4;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_1
    invoke-direct {v0, v1}, LX/0v4;-><init>(Z)V

    invoke-static {v5, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v3, v4, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 157439
    return-void

    :cond_0
    move v0, v2

    .line 157440
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/0v2;
    .locals 6

    .prologue
    .line 157416
    sget-object v0, LX/0v2;->a:LX/0v2;

    if-nez v0, :cond_1

    .line 157417
    const-class v1, LX/0v2;

    monitor-enter v1

    .line 157418
    :try_start_0
    sget-object v0, LX/0v2;->a:LX/0v2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 157419
    if-eqz v2, :cond_0

    .line 157420
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 157421
    new-instance v4, LX/0v2;

    .line 157422
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 v5, 0x494

    const/4 p0, 0x0

    invoke-virtual {v3, v5, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object v3, v3

    .line 157423
    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v3}, LX/0v2;-><init>(Ljava/lang/Boolean;)V

    .line 157424
    move-object v0, v4

    .line 157425
    sput-object v0, LX/0v2;->a:LX/0v2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157426
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 157427
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 157428
    :cond_1
    sget-object v0, LX/0v2;->a:LX/0v2;

    return-object v0

    .line 157429
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 157430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 157431
    invoke-super {p0, p1, p2, p3}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 157432
    const/16 v0, 0x22

    if-gt p2, v0, :cond_0

    .line 157433
    const-string v0, "drop table if exists feed_unit_partial;"

    const v1, 0x34fb7764

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x61edfea8

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157434
    const-string v0, "drop table if exists feedback;"

    const v1, 0x475c1424

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x754e8477

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157435
    const-string v0, "drop table if exists feed_unit_impression;"

    const v1, 0x3b20c6f8

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x38f00fe1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157436
    const-string v0, "drop table if exists feed_unit_low_engagement;"

    const v1, -0x7893718d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x6ed01d6f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 157437
    :cond_0
    return-void
.end method
