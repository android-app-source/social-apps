.class public LX/0qY;
.super LX/0pK;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0qY;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 147876
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/0pK;-><init>(LX/0W3;IIII)V

    .line 147877
    return-void
.end method

.method public static a(LX/0QB;)LX/0qY;
    .locals 4

    .prologue
    .line 147878
    sget-object v0, LX/0qY;->a:LX/0qY;

    if-nez v0, :cond_1

    .line 147879
    const-class v1, LX/0qY;

    monitor-enter v1

    .line 147880
    :try_start_0
    sget-object v0, LX/0qY;->a:LX/0qY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 147881
    if-eqz v2, :cond_0

    .line 147882
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 147883
    new-instance p0, LX/0qY;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/0qY;-><init>(LX/0W3;)V

    .line 147884
    move-object v0, p0

    .line 147885
    sput-object v0, LX/0qY;->a:LX/0qY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147886
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 147887
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 147888
    :cond_1
    sget-object v0, LX/0qY;->a:LX/0qY;

    return-object v0

    .line 147889
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 147890
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
