.class public LX/1FE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static a:LX/1FE;


# instance fields
.field private final b:LX/1HH;

.field private final c:LX/1H6;

.field private d:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/1HY;

.field private i:LX/1Ha;

.field private j:LX/1GL;

.field private k:LX/1HI;

.field private l:LX/1HK;

.field private m:LX/1HJ;

.field private n:LX/1HY;

.field private o:LX/1Ha;

.field private p:LX/1IZ;

.field private q:LX/1FZ;

.field private r:LX/1FG;

.field private s:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221883
    const/4 v0, 0x0

    sput-object v0, LX/1FE;->a:LX/1FE;

    return-void
.end method

.method private constructor <init>(LX/1H6;)V
    .locals 2

    .prologue
    .line 221884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221885
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1H6;

    iput-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221886
    new-instance v0, LX/1HH;

    .line 221887
    iget-object v1, p1, LX/1H6;->i:LX/1Ft;

    move-object v1, v1

    .line 221888
    invoke-interface {v1}, LX/1Ft;->e()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1HH;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, LX/1FE;->b:LX/1HH;

    .line 221889
    return-void
.end method

.method public static a()LX/1FE;
    .locals 2

    .prologue
    .line 221890
    sget-object v0, LX/1FE;->a:LX/1FE;

    const-string v1, "ImagePipelineFactory was not initialized!"

    invoke-static {v0, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FE;

    return-object v0
.end method

.method public static a(LX/1FB;Z)LX/1FG;
    .locals 4

    .prologue
    .line 221891
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 221892
    invoke-virtual {p0}, LX/1FB;->c()I

    move-result v1

    .line 221893
    new-instance v0, LX/4eD;

    invoke-virtual {p0}, LX/1FB;->a()LX/4eM;

    move-result-object v2

    new-instance v3, LX/0Zi;

    invoke-direct {v3, v1}, LX/0Zi;-><init>(I)V

    invoke-direct {v0, v2, v1, v3}, LX/4eD;-><init>(LX/4eM;ILX/0Zi;)V

    .line 221894
    :goto_0
    return-object v0

    .line 221895
    :cond_0
    if-eqz p1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 221896
    new-instance v0, LX/4el;

    invoke-direct {v0}, LX/4el;-><init>()V

    goto :goto_0

    .line 221897
    :cond_1
    new-instance v0, LX/1FF;

    invoke-virtual {p0}, LX/1FB;->b()LX/1FO;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1FF;-><init>(LX/1FO;)V

    goto :goto_0
.end method

.method public static a(LX/1FB;LX/1FG;)LX/1FZ;
    .locals 3

    .prologue
    .line 221898
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 221899
    new-instance v0, LX/4dk;

    invoke-virtual {p0}, LX/1FB;->a()LX/4eM;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4dk;-><init>(LX/4eM;)V

    .line 221900
    :goto_0
    return-object v0

    .line 221901
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 221902
    new-instance v0, LX/1FY;

    new-instance v1, LX/1Fi;

    invoke-virtual {p0}, LX/1FB;->e()LX/1Fj;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Fi;-><init>(LX/1Fj;)V

    invoke-direct {v0, v1, p1}, LX/1FY;-><init>(LX/1Fi;LX/1FG;)V

    goto :goto_0

    .line 221903
    :cond_1
    new-instance v0, LX/4dl;

    invoke-direct {v0}, LX/4dl;-><init>()V

    goto :goto_0
.end method

.method public static a(LX/1H6;)V
    .locals 1

    .prologue
    .line 221904
    new-instance v0, LX/1FE;

    invoke-direct {v0, p0}, LX/1FE;-><init>(LX/1H6;)V

    sput-object v0, LX/1FE;->a:LX/1FE;

    .line 221905
    return-void
.end method

.method private static j(LX/1FE;)LX/1GL;
    .locals 6

    .prologue
    .line 221906
    iget-object v0, p0, LX/1FE;->j:LX/1GL;

    if-nez v0, :cond_0

    .line 221907
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221908
    iget-object v1, v0, LX/1H6;->k:LX/1GL;

    move-object v0, v1

    .line 221909
    if-eqz v0, :cond_1

    .line 221910
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221911
    iget-object v1, v0, LX/1H6;->k:LX/1GL;

    move-object v0, v1

    .line 221912
    iput-object v0, p0, LX/1FE;->j:LX/1GL;

    .line 221913
    :cond_0
    :goto_0
    iget-object v0, p0, LX/1FE;->j:LX/1GL;

    return-object v0

    .line 221914
    :cond_1
    invoke-virtual {p0}, LX/1FE;->b()Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    move-result-object v0

    .line 221915
    if-eqz v0, :cond_2

    .line 221916
    invoke-virtual {p0}, LX/1FE;->b()Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;->a()LX/1GA;

    move-result-object v0

    .line 221917
    :goto_1
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221918
    iget-object v2, v1, LX/1H6;->v:LX/4e5;

    move-object v1, v2

    .line 221919
    if-nez v1, :cond_3

    .line 221920
    new-instance v1, LX/1GK;

    invoke-static {p0}, LX/1FE;->m(LX/1FE;)LX/1FG;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221921
    iget-object v4, v3, LX/1H6;->b:Landroid/graphics/Bitmap$Config;

    move-object v3, v4

    .line 221922
    invoke-direct {v1, v0, v2, v3}, LX/1GK;-><init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;)V

    iput-object v1, p0, LX/1FE;->j:LX/1GL;

    goto :goto_0

    .line 221923
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 221924
    :cond_3
    new-instance v1, LX/1GK;

    invoke-static {p0}, LX/1FE;->m(LX/1FE;)LX/1FG;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221925
    iget-object v4, v3, LX/1H6;->b:Landroid/graphics/Bitmap$Config;

    move-object v3, v4

    .line 221926
    iget-object v4, p0, LX/1FE;->c:LX/1H6;

    .line 221927
    iget-object v5, v4, LX/1H6;->v:LX/4e5;

    move-object v4, v5

    .line 221928
    iget-object v5, v4, LX/4e5;->a:Ljava/util/Map;

    move-object v4, v5

    .line 221929
    invoke-direct {v1, v0, v2, v3, v4}, LX/1GK;-><init>(LX/1GA;LX/1FG;Landroid/graphics/Bitmap$Config;Ljava/util/Map;)V

    iput-object v1, p0, LX/1FE;->j:LX/1GL;

    .line 221930
    invoke-static {}, LX/1la;->a()LX/1la;

    move-result-object v0

    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221931
    iget-object v2, v1, LX/1H6;->v:LX/4e5;

    move-object v1, v2

    .line 221932
    iget-object v2, v1, LX/4e5;->b:Ljava/util/List;

    move-object v1, v2

    .line 221933
    iput-object v1, v0, LX/1la;->c:Ljava/util/List;

    .line 221934
    invoke-static {v0}, LX/1la;->b(LX/1la;)V

    .line 221935
    goto :goto_0
.end method

.method private static k(LX/1FE;)LX/1HY;
    .locals 8

    .prologue
    .line 221936
    iget-object v0, p0, LX/1FE;->h:LX/1HY;

    if-nez v0, :cond_0

    .line 221937
    new-instance v0, LX/1HY;

    invoke-virtual {p0}, LX/1FE;->g()LX/1Ha;

    move-result-object v1

    iget-object v2, p0, LX/1FE;->c:LX/1H6;

    .line 221938
    iget-object v3, v2, LX/1H6;->q:LX/1FB;

    move-object v2, v3

    .line 221939
    invoke-virtual {v2}, LX/1FB;->e()LX/1Fj;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221940
    iget-object v4, v3, LX/1H6;->q:LX/1FB;

    move-object v3, v4

    .line 221941
    invoke-virtual {v3}, LX/1FB;->f()LX/1Fl;

    move-result-object v3

    iget-object v4, p0, LX/1FE;->c:LX/1H6;

    .line 221942
    iget-object v5, v4, LX/1H6;->i:LX/1Ft;

    move-object v4, v5

    .line 221943
    invoke-interface {v4}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, LX/1FE;->c:LX/1H6;

    .line 221944
    iget-object v6, v5, LX/1H6;->i:LX/1Ft;

    move-object v5, v6

    .line 221945
    invoke-interface {v5}, LX/1Ft;->b()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, p0, LX/1FE;->c:LX/1H6;

    .line 221946
    iget-object v7, v6, LX/1H6;->j:LX/1GF;

    move-object v6, v7

    .line 221947
    invoke-direct/range {v0 .. v6}, LX/1HY;-><init>(LX/1Ha;LX/1Fj;LX/1Fl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/1GF;)V

    iput-object v0, p0, LX/1FE;->h:LX/1HY;

    .line 221948
    :cond_0
    iget-object v0, p0, LX/1FE;->h:LX/1HY;

    return-object v0
.end method

.method private static l(LX/1FE;)LX/1FZ;
    .locals 2

    .prologue
    .line 221949
    iget-object v0, p0, LX/1FE;->q:LX/1FZ;

    if-nez v0, :cond_0

    .line 221950
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221951
    iget-object v1, v0, LX/1H6;->q:LX/1FB;

    move-object v0, v1

    .line 221952
    invoke-static {p0}, LX/1FE;->m(LX/1FE;)LX/1FG;

    move-result-object v1

    invoke-static {v0, v1}, LX/1FE;->a(LX/1FB;LX/1FG;)LX/1FZ;

    move-result-object v0

    iput-object v0, p0, LX/1FE;->q:LX/1FZ;

    .line 221953
    :cond_0
    iget-object v0, p0, LX/1FE;->q:LX/1FZ;

    return-object v0
.end method

.method private static m(LX/1FE;)LX/1FG;
    .locals 3

    .prologue
    .line 221875
    iget-object v0, p0, LX/1FE;->r:LX/1FG;

    if-nez v0, :cond_0

    .line 221876
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221877
    iget-object v1, v0, LX/1H6;->q:LX/1FB;

    move-object v0, v1

    .line 221878
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221879
    iget-object v2, v1, LX/1H6;->w:LX/1HF;

    move-object v1, v2

    .line 221880
    iget-boolean v2, v1, LX/1HF;->b:Z

    move v1, v2

    .line 221881
    invoke-static {v0, v1}, LX/1FE;->a(LX/1FB;Z)LX/1FG;

    move-result-object v0

    iput-object v0, p0, LX/1FE;->r:LX/1FG;

    .line 221882
    :cond_0
    iget-object v0, p0, LX/1FE;->r:LX/1FG;

    return-object v0
.end method

.method private static n(LX/1FE;)LX/1HK;
    .locals 20

    .prologue
    .line 221758
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1FE;->l:LX/1HK;

    if-nez v1, :cond_0

    .line 221759
    new-instance v1, LX/1HK;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v2}, LX/1H6;->d()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v3}, LX/1H6;->p()LX/1FB;

    move-result-object v3

    invoke-virtual {v3}, LX/1FB;->h()LX/1FQ;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/1FE;->j(LX/1FE;)LX/1GL;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v5}, LX/1H6;->q()LX/1Gv;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v6}, LX/1H6;->g()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v7}, LX/1H6;->s()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v8}, LX/1H6;->v()LX/1HF;

    move-result-object v8

    invoke-virtual {v8}, LX/1HF;->g()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v9}, LX/1H6;->i()LX/1Ft;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1FE;->c:LX/1H6;

    invoke-virtual {v10}, LX/1H6;->p()LX/1FB;

    move-result-object v10

    invoke-virtual {v10}, LX/1FB;->e()LX/1Fj;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, LX/1FE;->d()LX/1Fh;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, LX/1FE;->f()LX/1Fh;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/1FE;->k(LX/1FE;)LX/1HY;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/1FE;->p(LX/1FE;)LX/1HY;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/1FE;->q(LX/1FE;)LX/1IZ;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1FE;->c:LX/1H6;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, LX/1H6;->c()LX/1Ao;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/1FE;->l(LX/1FE;)LX/1FZ;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1FE;->c:LX/1H6;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, LX/1H6;->v()LX/1HF;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/1HF;->a()Z

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1FE;->c:LX/1H6;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, LX/1H6;->v()LX/1HF;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, LX/1HF;->c()I

    move-result v19

    invoke-direct/range {v1 .. v19}, LX/1HK;-><init>(Landroid/content/Context;LX/1FQ;LX/1GL;LX/1Gv;ZZZLX/1Ft;LX/1Fj;LX/1Fh;LX/1Fh;LX/1HY;LX/1HY;LX/1IZ;LX/1Ao;LX/1FZ;ZI)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/1FE;->l:LX/1HK;

    .line 221760
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1FE;->l:LX/1HK;

    return-object v1
.end method

.method private o()LX/1HJ;
    .locals 8

    .prologue
    .line 221954
    iget-object v0, p0, LX/1FE;->m:LX/1HJ;

    if-nez v0, :cond_0

    .line 221955
    new-instance v0, LX/1HJ;

    invoke-static {p0}, LX/1FE;->n(LX/1FE;)LX/1HK;

    move-result-object v1

    iget-object v2, p0, LX/1FE;->c:LX/1H6;

    .line 221956
    iget-object v3, v2, LX/1H6;->o:LX/1Gj;

    move-object v2, v3

    .line 221957
    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221958
    iget-boolean v4, v3, LX/1H6;->t:Z

    move v3, v4

    .line 221959
    iget-object v4, p0, LX/1FE;->c:LX/1H6;

    .line 221960
    iget-object v5, v4, LX/1H6;->w:LX/1HF;

    move-object v4, v5

    .line 221961
    iget-boolean v5, v4, LX/1HF;->b:Z

    move v4, v5

    .line 221962
    iget-object v5, p0, LX/1FE;->b:LX/1HH;

    iget-object v6, p0, LX/1FE;->c:LX/1H6;

    .line 221963
    iget-object v7, v6, LX/1H6;->w:LX/1HF;

    move-object v6, v7

    .line 221964
    iget-boolean v7, v6, LX/1HF;->j:Z

    move v6, v7

    .line 221965
    invoke-direct/range {v0 .. v6}, LX/1HJ;-><init>(LX/1HK;LX/1Gj;ZZLX/1HH;Z)V

    iput-object v0, p0, LX/1FE;->m:LX/1HJ;

    .line 221966
    :cond_0
    iget-object v0, p0, LX/1FE;->m:LX/1HJ;

    return-object v0
.end method

.method private static p(LX/1FE;)LX/1HY;
    .locals 8

    .prologue
    .line 221761
    iget-object v0, p0, LX/1FE;->n:LX/1HY;

    if-nez v0, :cond_0

    .line 221762
    new-instance v0, LX/1HY;

    invoke-virtual {p0}, LX/1FE;->i()LX/1Ha;

    move-result-object v1

    iget-object v2, p0, LX/1FE;->c:LX/1H6;

    .line 221763
    iget-object v3, v2, LX/1H6;->q:LX/1FB;

    move-object v2, v3

    .line 221764
    invoke-virtual {v2}, LX/1FB;->e()LX/1Fj;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221765
    iget-object v4, v3, LX/1H6;->q:LX/1FB;

    move-object v3, v4

    .line 221766
    invoke-virtual {v3}, LX/1FB;->f()LX/1Fl;

    move-result-object v3

    iget-object v4, p0, LX/1FE;->c:LX/1H6;

    .line 221767
    iget-object v5, v4, LX/1H6;->i:LX/1Ft;

    move-object v4, v5

    .line 221768
    invoke-interface {v4}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, LX/1FE;->c:LX/1H6;

    .line 221769
    iget-object v6, v5, LX/1H6;->i:LX/1Ft;

    move-object v5, v6

    .line 221770
    invoke-interface {v5}, LX/1Ft;->b()Ljava/util/concurrent/Executor;

    move-result-object v5

    iget-object v6, p0, LX/1FE;->c:LX/1H6;

    .line 221771
    iget-object v7, v6, LX/1H6;->j:LX/1GF;

    move-object v6, v7

    .line 221772
    invoke-direct/range {v0 .. v6}, LX/1HY;-><init>(LX/1Ha;LX/1Fj;LX/1Fl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/1GF;)V

    iput-object v0, p0, LX/1FE;->n:LX/1HY;

    .line 221773
    :cond_0
    iget-object v0, p0, LX/1FE;->n:LX/1HY;

    return-object v0
.end method

.method private static q(LX/1FE;)LX/1IZ;
    .locals 5

    .prologue
    .line 221774
    iget-object v0, p0, LX/1FE;->p:LX/1IZ;

    if-nez v0, :cond_0

    .line 221775
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221776
    iget-object v1, v0, LX/1H6;->w:LX/1HF;

    move-object v0, v1

    .line 221777
    iget-object v1, v0, LX/1HF;->e:LX/1Gd;

    invoke-interface {v1}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 221778
    if-eqz v0, :cond_1

    new-instance v0, LX/23L;

    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221779
    iget-object v2, v1, LX/1H6;->e:Landroid/content/Context;

    move-object v1, v2

    .line 221780
    iget-object v2, p0, LX/1FE;->c:LX/1H6;

    .line 221781
    iget-object v3, v2, LX/1H6;->i:LX/1Ft;

    move-object v2, v3

    .line 221782
    invoke-interface {v2}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221783
    iget-object v4, v3, LX/1H6;->i:LX/1Ft;

    move-object v3, v4

    .line 221784
    invoke-interface {v3}, LX/1Ft;->b()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/23L;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    :goto_0
    iput-object v0, p0, LX/1FE;->p:LX/1IZ;

    .line 221785
    :cond_0
    iget-object v0, p0, LX/1FE;->p:LX/1IZ;

    return-object v0

    .line 221786
    :cond_1
    new-instance v0, LX/1uD;

    invoke-direct {v0}, LX/1uD;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final b()Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;
    .locals 7

    .prologue
    .line 221787
    iget-object v0, p0, LX/1FE;->s:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    if-nez v0, :cond_0

    .line 221788
    invoke-static {p0}, LX/1FE;->l(LX/1FE;)LX/1FZ;

    move-result-object v0

    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221789
    iget-object v2, v1, LX/1H6;->i:LX/1Ft;

    move-object v1, v2

    .line 221790
    const/4 v6, 0x1

    .line 221791
    sget-boolean v2, LX/4dR;->a:Z

    if-nez v2, :cond_2

    .line 221792
    :try_start_0
    const-string v2, "com.facebook.imagepipeline.animated.factory.AnimatedFactoryImplSupport"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 221793
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/1FZ;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, LX/1Ft;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 221794
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    sput-object v2, LX/4dR;->b:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 221795
    :goto_0
    sget-object v2, LX/4dR;->b:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    if-eqz v2, :cond_1

    .line 221796
    sput-boolean v6, LX/4dR;->a:Z

    .line 221797
    sget-object v2, LX/4dR;->b:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    .line 221798
    :goto_1
    move-object v0, v2

    .line 221799
    iput-object v0, p0, LX/1FE;->s:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    .line 221800
    :cond_0
    iget-object v0, p0, LX/1FE;->s:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    return-object v0

    .line 221801
    :cond_1
    :try_start_1
    const-string v2, "com.facebook.imagepipeline.animated.factory.AnimatedFactoryImpl"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 221802
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/1FZ;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, LX/1Ft;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 221803
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    sput-object v2, LX/4dR;->b:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 221804
    :goto_2
    sput-boolean v6, LX/4dR;->a:Z

    .line 221805
    :cond_2
    sget-object v2, LX/4dR;->b:Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    goto :goto_1

    :catch_0
    goto :goto_2

    :catch_1
    goto :goto_0
.end method

.method public final c()LX/1Fg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221806
    iget-object v0, p0, LX/1FE;->d:LX/1Fg;

    if-nez v0, :cond_0

    .line 221807
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221808
    iget-object v1, v0, LX/1H6;->c:LX/1Gd;

    move-object v0, v1

    .line 221809
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221810
    iget-object v2, v1, LX/1H6;->n:LX/0rb;

    move-object v1, v2

    .line 221811
    invoke-static {p0}, LX/1FE;->l(LX/1FE;)LX/1FZ;

    move-result-object v2

    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221812
    iget-object v4, v3, LX/1H6;->w:LX/1HF;

    move-object v3, v4

    .line 221813
    iget-boolean v4, v3, LX/1HF;->d:Z

    move v3, v4

    .line 221814
    new-instance v5, LX/1HL;

    invoke-direct {v5}, LX/1HL;-><init>()V

    .line 221815
    new-instance v6, LX/1HN;

    invoke-direct {v6}, LX/1HN;-><init>()V

    .line 221816
    new-instance v4, LX/1Fg;

    move-object v7, v0

    move-object v8, v2

    move v9, v3

    invoke-direct/range {v4 .. v9}, LX/1Fg;-><init>(LX/1HM;LX/1HO;LX/1Gd;LX/1FZ;Z)V

    .line 221817
    invoke-interface {v1, v4}, LX/0rb;->a(LX/0rf;)V

    .line 221818
    move-object v0, v4

    .line 221819
    iput-object v0, p0, LX/1FE;->d:LX/1Fg;

    .line 221820
    :cond_0
    iget-object v0, p0, LX/1FE;->d:LX/1Fg;

    return-object v0
.end method

.method public final d()LX/1Fh;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221821
    iget-object v0, p0, LX/1FE;->e:LX/1Fh;

    if-nez v0, :cond_0

    .line 221822
    invoke-virtual {p0}, LX/1FE;->c()LX/1Fg;

    move-result-object v0

    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221823
    iget-object v2, v1, LX/1H6;->j:LX/1GF;

    move-object v1, v2

    .line 221824
    invoke-interface {v1, v0}, LX/1GF;->a(LX/1Fg;)V

    .line 221825
    new-instance v2, LX/1HS;

    invoke-direct {v2, v1}, LX/1HS;-><init>(LX/1GF;)V

    .line 221826
    new-instance v3, LX/1HU;

    invoke-direct {v3, v0, v2}, LX/1HU;-><init>(LX/1Fh;LX/1HT;)V

    move-object v0, v3

    .line 221827
    iput-object v0, p0, LX/1FE;->e:LX/1Fh;

    .line 221828
    :cond_0
    iget-object v0, p0, LX/1FE;->e:LX/1Fh;

    return-object v0
.end method

.method public final e()LX/1Fg;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Fg",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221829
    iget-object v0, p0, LX/1FE;->f:LX/1Fg;

    if-nez v0, :cond_0

    .line 221830
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221831
    iget-object v1, v0, LX/1H6;->h:LX/1Gd;

    move-object v0, v1

    .line 221832
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221833
    iget-object v2, v1, LX/1H6;->n:LX/0rb;

    move-object v1, v2

    .line 221834
    invoke-static {p0}, LX/1FE;->l(LX/1FE;)LX/1FZ;

    move-result-object v2

    .line 221835
    new-instance v4, LX/1HV;

    invoke-direct {v4}, LX/1HV;-><init>()V

    .line 221836
    new-instance v5, LX/1HW;

    invoke-direct {v5}, LX/1HW;-><init>()V

    .line 221837
    new-instance v3, LX/1Fg;

    const/4 v8, 0x0

    move-object v6, v0

    move-object v7, v2

    invoke-direct/range {v3 .. v8}, LX/1Fg;-><init>(LX/1HM;LX/1HO;LX/1Gd;LX/1FZ;Z)V

    .line 221838
    invoke-interface {v1, v3}, LX/0rb;->a(LX/0rf;)V

    .line 221839
    move-object v0, v3

    .line 221840
    iput-object v0, p0, LX/1FE;->f:LX/1Fg;

    .line 221841
    :cond_0
    iget-object v0, p0, LX/1FE;->f:LX/1Fg;

    return-object v0
.end method

.method public final f()LX/1Fh;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221842
    iget-object v0, p0, LX/1FE;->g:LX/1Fh;

    if-nez v0, :cond_0

    .line 221843
    invoke-virtual {p0}, LX/1FE;->e()LX/1Fg;

    move-result-object v0

    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221844
    iget-object v2, v1, LX/1H6;->j:LX/1GF;

    move-object v1, v2

    .line 221845
    invoke-interface {v1, v0}, LX/1GF;->b(LX/1Fg;)V

    .line 221846
    new-instance v2, LX/1HX;

    invoke-direct {v2, v1}, LX/1HX;-><init>(LX/1GF;)V

    .line 221847
    new-instance v3, LX/1HU;

    invoke-direct {v3, v0, v2}, LX/1HU;-><init>(LX/1Fh;LX/1HT;)V

    move-object v0, v3

    .line 221848
    iput-object v0, p0, LX/1FE;->g:LX/1Fh;

    .line 221849
    :cond_0
    iget-object v0, p0, LX/1FE;->g:LX/1Fh;

    return-object v0
.end method

.method public final g()LX/1Ha;
    .locals 3

    .prologue
    .line 221850
    iget-object v0, p0, LX/1FE;->i:LX/1Ha;

    if-nez v0, :cond_0

    .line 221851
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221852
    iget-object v1, v0, LX/1H6;->m:LX/1Gf;

    move-object v0, v1

    .line 221853
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221854
    iget-object v2, v1, LX/1H6;->g:LX/1HC;

    move-object v1, v2

    .line 221855
    invoke-interface {v1, v0}, LX/1HC;->a(LX/1Gf;)LX/1Ha;

    move-result-object v0

    iput-object v0, p0, LX/1FE;->i:LX/1Ha;

    .line 221856
    :cond_0
    iget-object v0, p0, LX/1FE;->i:LX/1Ha;

    return-object v0
.end method

.method public final h()LX/1HI;
    .locals 12

    .prologue
    .line 221857
    iget-object v0, p0, LX/1FE;->k:LX/1HI;

    if-nez v0, :cond_0

    .line 221858
    new-instance v0, LX/1HI;

    invoke-direct {p0}, LX/1FE;->o()LX/1HJ;

    move-result-object v1

    iget-object v2, p0, LX/1FE;->c:LX/1H6;

    .line 221859
    iget-object v3, v2, LX/1H6;->s:Ljava/util/Set;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    move-object v2, v3

    .line 221860
    iget-object v3, p0, LX/1FE;->c:LX/1H6;

    .line 221861
    iget-object v4, v3, LX/1H6;->l:LX/1Gd;

    move-object v3, v4

    .line 221862
    invoke-virtual {p0}, LX/1FE;->d()LX/1Fh;

    move-result-object v4

    invoke-virtual {p0}, LX/1FE;->f()LX/1Fh;

    move-result-object v5

    invoke-static {p0}, LX/1FE;->k(LX/1FE;)LX/1HY;

    move-result-object v6

    invoke-static {p0}, LX/1FE;->p(LX/1FE;)LX/1HY;

    move-result-object v7

    iget-object v8, p0, LX/1FE;->c:LX/1H6;

    .line 221863
    iget-object v9, v8, LX/1H6;->d:LX/1Ao;

    move-object v8, v9

    .line 221864
    iget-object v9, p0, LX/1FE;->b:LX/1HH;

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 221865
    new-instance v11, LX/1Ic;

    invoke-direct {v11, v10}, LX/1Ic;-><init>(Ljava/lang/Object;)V

    move-object v10, v11

    .line 221866
    invoke-direct/range {v0 .. v10}, LX/1HI;-><init>(LX/1HJ;Ljava/util/Set;LX/1Gd;LX/1Fh;LX/1Fh;LX/1HY;LX/1HY;LX/1Ao;LX/1HH;LX/1Gd;)V

    iput-object v0, p0, LX/1FE;->k:LX/1HI;

    .line 221867
    :cond_0
    iget-object v0, p0, LX/1FE;->k:LX/1HI;

    return-object v0
.end method

.method public final i()LX/1Ha;
    .locals 3

    .prologue
    .line 221868
    iget-object v0, p0, LX/1FE;->o:LX/1Ha;

    if-nez v0, :cond_0

    .line 221869
    iget-object v0, p0, LX/1FE;->c:LX/1H6;

    .line 221870
    iget-object v1, v0, LX/1H6;->u:LX/1Gf;

    move-object v0, v1

    .line 221871
    iget-object v1, p0, LX/1FE;->c:LX/1H6;

    .line 221872
    iget-object v2, v1, LX/1H6;->g:LX/1HC;

    move-object v1, v2

    .line 221873
    invoke-interface {v1, v0}, LX/1HC;->a(LX/1Gf;)LX/1Ha;

    move-result-object v0

    iput-object v0, p0, LX/1FE;->o:LX/1Ha;

    .line 221874
    :cond_0
    iget-object v0, p0, LX/1FE;->o:LX/1Ha;

    return-object v0
.end method
