.class public LX/0x0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:J

.field public a:Ljava/lang/String;

.field public b:D

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLBumpReason;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:I

.field public l:Z

.field public m:Ljava/nio/ByteBuffer;

.field public n:Ljava/nio/ByteBuffer;

.field private o:Ljava/lang/String;

.field public p:J

.field public q:I

.field private r:I

.field private s:LX/15j;

.field private t:LX/0qN;

.field public u:Ljava/lang/String;

.field private v:I

.field public w:Ljava/lang/String;

.field public x:Z

.field public y:Z

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 161423
    new-instance v2, LX/0x0;

    invoke-direct {v2}, LX/0x0;-><init>()V

    .line 161424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->K_()Ljava/lang/String;

    move-result-object v0

    .line 161425
    iput-object v0, v2, LX/0x0;->f:Ljava/lang/String;

    .line 161426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->J_()Ljava/lang/String;

    move-result-object v0

    .line 161427
    iput-object v0, v2, LX/0x0;->e:Ljava/lang/String;

    .line 161428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 161429
    iput-object v0, v2, LX/0x0;->c:Ljava/lang/String;

    .line 161430
    invoke-static {p0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v0

    .line 161431
    iput-object v0, v2, LX/0x0;->g:Ljava/lang/String;

    .line 161432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->n()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v0

    .line 161433
    iput-object v0, v2, LX/0x0;->d:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->e()D

    move-result-wide v4

    .line 161435
    iput-wide v4, v2, LX/0x0;->b:D

    .line 161436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    .line 161437
    iput-object v0, v2, LX/0x0;->a:Ljava/lang/String;

    .line 161438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->l()Ljava/lang/String;

    move-result-object v0

    .line 161439
    iput-object v0, v2, LX/0x0;->w:Ljava/lang/String;

    .line 161440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->m()Z

    move-result v0

    .line 161441
    iput-boolean v0, v2, LX/0x0;->x:Z

    .line 161442
    iput-boolean v1, v2, LX/0x0;->y:Z

    .line 161443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 161444
    if-eqz v0, :cond_0

    .line 161445
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    .line 161446
    iput-wide v4, v2, LX/0x0;->p:J

    .line 161447
    invoke-static {p0}, LX/1uc;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v3

    .line 161448
    iput-object v3, v2, LX/0x0;->u:Ljava/lang/String;

    .line 161449
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    .line 161450
    iput-object v3, v2, LX/0x0;->z:Ljava/lang/String;

    .line 161451
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_0

    .line 161452
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 161453
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aM()J

    move-result-wide v4

    .line 161454
    iput-wide v4, v2, LX/0x0;->A:J

    .line 161455
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 161456
    :goto_0
    iput v0, v2, LX/0x0;->q:I

    .line 161457
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 161458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 161459
    iput-object v0, v2, LX/0x0;->n:Ljava/nio/ByteBuffer;

    .line 161460
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 161461
    iput-object v0, v2, LX/0x0;->m:Ljava/nio/ByteBuffer;

    .line 161462
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    .line 161463
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 161464
    iput-boolean v0, v2, LX/0x0;->l:Z

    .line 161465
    :cond_1
    invoke-virtual {v2}, LX/0x0;->a()Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v0

    .line 161466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->o()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 161467
    return-object v0

    :cond_2
    move v0, v1

    .line 161468
    goto :goto_0
.end method


# virtual methods
.method public final a(D)LX/0x0;
    .locals 1

    .prologue
    .line 161469
    iput-wide p1, p0, LX/0x0;->b:D

    .line 161470
    return-object p0
.end method

.method public final a(I)LX/0x0;
    .locals 0

    .prologue
    .line 161471
    iput p1, p0, LX/0x0;->v:I

    .line 161472
    return-object p0
.end method

.method public final a(J)LX/0x0;
    .locals 1

    .prologue
    .line 161473
    iput-wide p1, p0, LX/0x0;->p:J

    .line 161474
    return-object p0
.end method

.method public final a(LX/0qN;)LX/0x0;
    .locals 0

    .prologue
    .line 161477
    iput-object p1, p0, LX/0x0;->t:LX/0qN;

    .line 161478
    return-object p0
.end method

.method public final a(LX/15j;)LX/0x0;
    .locals 0

    .prologue
    .line 161490
    iput-object p1, p0, LX/0x0;->s:LX/15j;

    .line 161491
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/0x0;
    .locals 0

    .prologue
    .line 161479
    iput-object p1, p0, LX/0x0;->d:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    .line 161480
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161481
    iput-object p1, p0, LX/0x0;->a:Ljava/lang/String;

    .line 161482
    return-object p0
.end method

.method public final a(Ljava/nio/ByteBuffer;)LX/0x0;
    .locals 0

    .prologue
    .line 161483
    iput-object p1, p0, LX/0x0;->m:Ljava/nio/ByteBuffer;

    .line 161484
    return-object p0
.end method

.method public final a(Z)LX/0x0;
    .locals 0

    .prologue
    .line 161485
    iput-boolean p1, p0, LX/0x0;->l:Z

    .line 161486
    return-object p0
.end method

.method public final a()Lcom/facebook/feed/model/ClientFeedUnitEdge;
    .locals 34

    .prologue
    .line 161487
    new-instance v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/0x0;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v5, v0, LX/0x0;->b:D

    move-object/from16 v0, p0

    iget-object v7, v0, LX/0x0;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/0x0;->d:Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0x0;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0x0;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0x0;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/0x0;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, LX/0x0;->i:I

    move-object/from16 v0, p0

    iget v14, v0, LX/0x0;->j:I

    move-object/from16 v0, p0

    iget v15, v0, LX/0x0;->k:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0x0;->l:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->m:Ljava/nio/ByteBuffer;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->n:Ljava/nio/ByteBuffer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->o:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0x0;->p:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0x0;->q:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0x0;->r:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/0x0;->v:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->s:LX/15j;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->t:LX/0qN;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->u:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->w:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0x0;->x:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/0x0;->y:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/0x0;->z:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/0x0;->A:J

    move-wide/from16 v32, v0

    invoke-direct/range {v3 .. v33}, Lcom/facebook/feed/model/ClientFeedUnitEdge;-><init>(Ljava/lang/String;DLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLBumpReason;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZLjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/lang/String;JIIILX/15j;LX/0qN;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;J)V

    return-object v3
.end method

.method public final b(I)LX/0x0;
    .locals 0

    .prologue
    .line 161488
    iput p1, p0, LX/0x0;->i:I

    .line 161489
    return-object p0
.end method

.method public final b(J)LX/0x0;
    .locals 1

    .prologue
    .line 161420
    iput-wide p1, p0, LX/0x0;->A:J

    .line 161421
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161475
    iput-object p1, p0, LX/0x0;->c:Ljava/lang/String;

    .line 161476
    return-object p0
.end method

.method public final b(Ljava/nio/ByteBuffer;)LX/0x0;
    .locals 0

    .prologue
    .line 161390
    iput-object p1, p0, LX/0x0;->n:Ljava/nio/ByteBuffer;

    .line 161391
    return-object p0
.end method

.method public final b(Z)LX/0x0;
    .locals 0

    .prologue
    .line 161394
    iput-boolean p1, p0, LX/0x0;->x:Z

    .line 161395
    return-object p0
.end method

.method public final c(I)LX/0x0;
    .locals 0

    .prologue
    .line 161396
    iput p1, p0, LX/0x0;->k:I

    .line 161397
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161398
    iput-object p1, p0, LX/0x0;->e:Ljava/lang/String;

    .line 161399
    return-object p0
.end method

.method public final c(Z)LX/0x0;
    .locals 0

    .prologue
    .line 161400
    iput-boolean p1, p0, LX/0x0;->y:Z

    .line 161401
    return-object p0
.end method

.method public final d(I)LX/0x0;
    .locals 0

    .prologue
    .line 161402
    iput p1, p0, LX/0x0;->j:I

    .line 161403
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161404
    iput-object p1, p0, LX/0x0;->f:Ljava/lang/String;

    .line 161405
    return-object p0
.end method

.method public final e(I)LX/0x0;
    .locals 0

    .prologue
    .line 161406
    iput p1, p0, LX/0x0;->q:I

    .line 161407
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161408
    iput-object p1, p0, LX/0x0;->g:Ljava/lang/String;

    .line 161409
    return-object p0
.end method

.method public final f(I)LX/0x0;
    .locals 0

    .prologue
    .line 161410
    iput p1, p0, LX/0x0;->r:I

    .line 161411
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161392
    iput-object p1, p0, LX/0x0;->h:Ljava/lang/String;

    .line 161393
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161412
    iput-object p1, p0, LX/0x0;->o:Ljava/lang/String;

    .line 161413
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161414
    iput-object p1, p0, LX/0x0;->u:Ljava/lang/String;

    .line 161415
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161416
    iput-object p1, p0, LX/0x0;->w:Ljava/lang/String;

    .line 161417
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/0x0;
    .locals 0

    .prologue
    .line 161418
    iput-object p1, p0, LX/0x0;->z:Ljava/lang/String;

    .line 161419
    return-object p0
.end method
