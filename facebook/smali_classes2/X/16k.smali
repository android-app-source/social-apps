.class public LX/16k;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/util/concurrent/ExecutorService;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 186450
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/concurrent/ExecutorService;
    .locals 3

    .prologue
    .line 186451
    sget-object v0, LX/16k;->a:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 186452
    const-class v1, LX/16k;

    monitor-enter v1

    .line 186453
    :try_start_0
    sget-object v0, LX/16k;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 186454
    if-eqz v2, :cond_0

    .line 186455
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 186456
    invoke-static {v0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object p0

    check-cast p0, LX/0UR;

    invoke-static {p0}, LX/19V;->a(LX/0UR;)Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    move-object v0, p0

    .line 186457
    sput-object v0, LX/16k;->a:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186458
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 186459
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 186460
    :cond_1
    sget-object v0, LX/16k;->a:Ljava/util/concurrent/ExecutorService;

    return-object v0

    .line 186461
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 186462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186463
    invoke-static {p0}, LX/0UR;->a(LX/0QB;)LX/0UR;

    move-result-object v0

    check-cast v0, LX/0UR;

    invoke-static {v0}, LX/19V;->a(LX/0UR;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method
