.class public LX/0i5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i6;


# instance fields
.field private final b:I

.field private final c:I

.field public d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

.field public e:Z

.field public f:Landroid/app/Activity;

.field public g:LX/6Zx;

.field private h:LX/0js;

.field public i:LX/1Ml;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/0s6;LX/1Ml;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 119494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119495
    iput v1, p0, LX/0i5;->b:I

    .line 119496
    const/16 v0, 0x6f

    iput v0, p0, LX/0i5;->c:I

    .line 119497
    iput-object p1, p0, LX/0i5;->f:Landroid/app/Activity;

    .line 119498
    iput-object p3, p0, LX/0i5;->i:LX/1Ml;

    .line 119499
    new-instance v0, LX/0js;

    invoke-direct {v0, p0}, LX/0js;-><init>(LX/0i5;)V

    iput-object v0, p0, LX/0i5;->h:LX/0js;

    .line 119500
    invoke-direct {p0, p2}, LX/0i5;->a(LX/0s6;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/0i5;->j:Ljava/lang/String;

    .line 119501
    return-void
.end method

.method private a(LX/0s6;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 119490
    iget-object v0, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 119491
    iget-object v1, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 119492
    invoke-static {v1}, LX/01H;->g(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {p1, v1, v2}, LX/01H;->c(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    :goto_0
    move-object v1, p0

    .line 119493
    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    const-string v0, "(unknown)"

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1, v2}, LX/01H;->f(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    goto :goto_0
.end method

.method public static b(LX/0i5;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 119475
    invoke-static {p0, p1}, LX/0i5;->i(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 119476
    iget-object v1, p0, LX/0i5;->f:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    .line 119477
    return-void
.end method

.method public static d([Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 119534
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 119535
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 119536
    invoke-static {v3}, LX/2rM;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 119537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119538
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method public static f(LX/0i5;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 119502
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 119503
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 119504
    invoke-static {v4}, LX/2rM;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 119505
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119506
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const-string v0, ""

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 119507
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 119508
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-static {v0}, LX/2rM;->c(Ljava/lang/String;)I

    move-result v5

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, LX/0i5;->j:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 119509
    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 119510
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    sub-int v4, v6, v4

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v3, v5, v4, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 119511
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 119512
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    const/4 v5, 0x0

    .line 119513
    const/4 v6, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_1
    :goto_2
    packed-switch v6, :pswitch_data_0

    .line 119514
    :goto_3
    move v0, v5

    .line 119515
    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, LX/0i5;->j:Ljava/lang/String;

    aput-object v6, v5, v1

    invoke-virtual {v4, v0, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 119516
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 119517
    :cond_2
    return-object v3

    .line 119518
    :sswitch_0
    const-string v7, "android.permission-group.CALENDAR"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v5

    goto :goto_2

    :sswitch_1
    const-string v7, "android.permission-group.CAMERA"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "android.permission-group.CONTACTS"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x2

    goto :goto_2

    :sswitch_3
    const-string v7, "android.permission-group.LOCATION"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x3

    goto :goto_2

    :sswitch_4
    const-string v7, "android.permission-group.MICROPHONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x4

    goto :goto_2

    :sswitch_5
    const-string v7, "android.permission-group.PHONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x5

    goto :goto_2

    :sswitch_6
    const-string v7, "android.permission-group.SENSORS"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x6

    goto :goto_2

    :sswitch_7
    const-string v7, "android.permission-group.SMS"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v6, 0x7

    goto :goto_2

    :sswitch_8
    const-string v7, "android.permission-group.STORAGE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/16 v6, 0x8

    goto :goto_2

    .line 119519
    :pswitch_0
    const v5, 0x7f08195a

    goto :goto_3

    .line 119520
    :pswitch_1
    const v5, 0x7f08195c

    goto :goto_3

    .line 119521
    :pswitch_2
    const v5, 0x7f08195e

    goto :goto_3

    .line 119522
    :pswitch_3
    const v5, 0x7f081960

    goto :goto_3

    .line 119523
    :pswitch_4
    const v5, 0x7f081962

    goto/16 :goto_3

    .line 119524
    :pswitch_5
    const v5, 0x7f081964

    goto/16 :goto_3

    .line 119525
    :pswitch_6
    const v5, 0x7f081966

    goto/16 :goto_3

    .line 119526
    :pswitch_7
    const v5, 0x7f081968

    goto/16 :goto_3

    .line 119527
    :pswitch_8
    const v5, 0x7f08196a

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x61be3c1f -> :sswitch_2
        -0x540bd380 -> :sswitch_5
        -0x4a8ca134 -> :sswitch_0
        -0x440149cd -> :sswitch_1
        0x1923928b -> :sswitch_6
        0x31640343 -> :sswitch_3
        0x32c9b10d -> :sswitch_8
        0x5e404d38 -> :sswitch_4
        0x6b004ceb -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static i(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 119528
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 119529
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 119530
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 119531
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119532
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119533
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static j(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    .prologue
    .line 119478
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 119479
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 119480
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119481
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119482
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119483
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static k(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    .prologue
    .line 119484
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 119485
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 119486
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->checkSelfPermission(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 119487
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119488
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119489
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6Zx;)V
    .locals 1

    .prologue
    .line 119413
    sget-object v0, LX/0i5;->a:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    invoke-virtual {p0, p1, v0, p2}, LX/0i5;->a(Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 119414
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V
    .locals 2

    .prologue
    .line 119415
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0, p2, p3}, LX/0i5;->a([Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 119416
    return-void
.end method

.method public final a([Ljava/lang/String;LX/6Zx;)V
    .locals 1

    .prologue
    .line 119417
    sget-object v0, LX/0i5;->a:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    invoke-virtual {p0, p1, v0, p2}, LX/0i5;->a([Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 119418
    return-void
.end method

.method public final a([Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V
    .locals 8

    .prologue
    .line 119419
    iput-object p2, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    .line 119420
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    .line 119421
    invoke-interface {p3}, LX/6Zx;->a()V

    .line 119422
    :goto_0
    return-void

    .line 119423
    :cond_0
    iput-object p3, p0, LX/0i5;->g:LX/6Zx;

    .line 119424
    invoke-static {p0, p1}, LX/0i5;->k(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/0i5;->e:Z

    .line 119425
    iget-object v0, p0, LX/0i5;->f:Landroid/app/Activity;

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_1

    .line 119426
    iget-object v0, p0, LX/0i5;->f:Landroid/app/Activity;

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v1, p0, LX/0i5;->h:LX/0js;

    .line 119427
    iput-object v1, v0, Lcom/facebook/base/activity/FbFragmentActivity;->B:LX/0js;

    .line 119428
    :cond_1
    invoke-virtual {p0, p1}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119429
    iget-object v0, p0, LX/0i5;->g:LX/6Zx;

    invoke-interface {v0}, LX/6Zx;->a()V

    goto :goto_0

    .line 119430
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 119431
    :cond_3
    const/4 v0, 0x0

    .line 119432
    array-length v2, p1

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_4

    aget-object p2, p1, v1

    .line 119433
    iget-object p3, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {p3, p2}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 119434
    const/4 v0, 0x1

    .line 119435
    :cond_4
    move v0, v0

    .line 119436
    if-eqz v0, :cond_5

    iget-object v0, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v0, v0, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->c:LX/0jt;

    iget-boolean v0, v0, LX/0jt;->shouldShowForDialogStep:Z

    if-eqz v0, :cond_5

    .line 119437
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 119438
    iget-object v0, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 119439
    const v2, 0x7f031247

    iget-object v0, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 119440
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/0i5;->f:Landroid/app/Activity;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081975

    new-instance v3, LX/79V;

    invoke-direct {v3, p0, p1}, LX/79V;-><init>(LX/0i5;[Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081976

    new-instance v3, LX/79U;

    invoke-direct {v3, p0, p1}, LX/79U;-><init>(LX/0i5;[Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    .line 119441
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 119442
    const v1, 0x7f0d2b00

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 119443
    const v3, 0x7f0d2b01

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 119444
    invoke-static {p0, p1}, LX/0i5;->j(LX/0i5;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 119445
    iget-object v4, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v4, v4, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 119446
    iget-object v4, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v4, v4, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 119447
    :goto_3
    iget-object v1, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v1, v1, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 119448
    iget-object v1, p0, LX/0i5;->d:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    iget-object v1, v1, Lcom/facebook/runtimepermissions/RequestPermissionsConfig;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 119449
    :goto_4
    new-instance v0, LX/79W;

    invoke-direct {v0, p0, v3, p1}, LX/79W;-><init>(LX/0i5;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 119450
    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 119451
    goto/16 :goto_0

    .line 119452
    :cond_5
    invoke-static {p0, p1}, LX/0i5;->b(LX/0i5;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119453
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 119454
    :cond_7
    invoke-static {v3}, LX/0i5;->d([Ljava/lang/String;)I

    move-result v4

    if-le v4, p3, :cond_8

    .line 119455
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    const v5, 0x7f081958

    new-array v6, p3, [Ljava/lang/Object;

    iget-object v7, p0, LX/0i5;->j:Ljava/lang/String;

    aput-object v7, v6, p2

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 119456
    :cond_8
    iget-object v4, p0, LX/0i5;->f:Landroid/app/Activity;

    aget-object v5, v3, p2

    invoke-static {v5}, LX/2rM;->d(Ljava/lang/String;)I

    move-result v5

    new-array v6, p3, [Ljava/lang/Object;

    iget-object v7, p0, LX/0i5;->j:Ljava/lang/String;

    aput-object v7, v6, p2

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 119457
    :cond_9
    invoke-static {v3}, LX/0i5;->d([Ljava/lang/String;)I

    move-result v1

    if-le v1, p3, :cond_a

    .line 119458
    invoke-static {p0, p1}, LX/0i5;->f(LX/0i5;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 119459
    :cond_a
    iget-object v1, p0, LX/0i5;->f:Landroid/app/Activity;

    aget-object v4, v3, p2

    invoke-static {v4}, LX/2rM;->e(Ljava/lang/String;)I

    move-result v4

    new-array v5, p3, [Ljava/lang/Object;

    iget-object v6, p0, LX/0i5;->j:Ljava/lang/String;

    aput-object v6, v5, p2

    invoke-virtual {v1, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public final a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 119460
    new-instance v0, LX/2rN;

    invoke-direct {v0}, LX/2rN;-><init>()V

    .line 119461
    iput-object p2, v0, LX/2rN;->a:Ljava/lang/String;

    .line 119462
    move-object v0, v0

    .line 119463
    iput-object p3, v0, LX/2rN;->b:Ljava/lang/String;

    .line 119464
    move-object v0, v0

    .line 119465
    sget-object v1, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    .line 119466
    iput-object v1, v0, LX/2rN;->c:LX/0jt;

    .line 119467
    move-object v0, v0

    .line 119468
    const/4 v1, 0x0

    .line 119469
    iput-boolean v1, v0, LX/2rN;->d:Z

    .line 119470
    move-object v0, v0

    .line 119471
    invoke-virtual {v0}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v0

    .line 119472
    invoke-virtual {p0, p1, v0, p4}, LX/0i5;->a([Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 119473
    return-void
.end method

.method public final a([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 119474
    iget-object v0, p0, LX/0i5;->i:LX/1Ml;

    invoke-virtual {v0, p1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
