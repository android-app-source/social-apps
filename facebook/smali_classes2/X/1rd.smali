.class public LX/1rd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/telephony/TelephonyManager;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Ml;

.field public final e:LX/1re;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/telephony/SubscriptionManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;LX/0Or;LX/1Ml;LX/1re;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/telephony/annotations/IsMediaTekPhoneNumberJarUsed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Ml;",
            "LX/1re;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332821
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/1rd;->a:Landroid/content/Context;

    .line 332822
    iput-object p2, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    .line 332823
    iput-object p3, p0, LX/1rd;->c:LX/0Or;

    .line 332824
    iput-object p4, p0, LX/1rd;->d:LX/1Ml;

    .line 332825
    iput-object p5, p0, LX/1rd;->e:LX/1re;

    .line 332826
    new-instance v0, LX/1rf;

    invoke-direct {v0, p0}, LX/1rf;-><init>(LX/1rd;)V

    iput-object v0, p0, LX/1rd;->f:LX/0Ot;

    .line 332827
    return-void
.end method

.method public static b(LX/0QB;)LX/1rd;
    .locals 7

    .prologue
    .line 332815
    new-instance v0, LX/1rd;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    const/16 v3, 0x1584

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v4

    check-cast v4, LX/1Ml;

    .line 332816
    new-instance v6, LX/1re;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v6, v5}, LX/1re;-><init>(Landroid/content/Context;)V

    .line 332817
    move-object v5, v6

    .line 332818
    check-cast v5, LX/1re;

    invoke-direct/range {v0 .. v5}, LX/1rd;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;LX/0Or;LX/1Ml;LX/1re;)V

    .line 332819
    return-object v0
.end method

.method public static d(LX/1rd;)Z
    .locals 2

    .prologue
    .line 332814
    iget-object v0, p0, LX/1rd;->d:LX/1Ml;

    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static e(LX/1rd;)Z
    .locals 1

    .prologue
    .line 332811
    iget-object v0, p0, LX/1rd;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332812
    const-string v0, "true"

    const-string p0, "ro.mediatek.gemini_support"

    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 332813
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/1rd;)Z
    .locals 2

    .prologue
    .line 332810
    iget-object v0, p0, LX/1rd;->d:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static q(LX/1rd;I)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 332808
    invoke-static {p0, p1}, LX/1rd;->t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 332809
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getNumber()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/1rd;I)Landroid/telephony/SubscriptionInfo;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 332804
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x16

    if-lt v0, v2, :cond_0

    invoke-static {p0}, LX/1rd;->d(LX/1rd;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 332805
    :goto_0
    return-object v0

    .line 332806
    :cond_1
    iget-object v0, p0, LX/1rd;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionManager;

    .line 332807
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 332828
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    .line 332829
    :try_start_0
    iget-object v0, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I
    :try_end_0
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 332830
    :goto_0
    return v0

    .line 332831
    :catch_0
    move-exception v0

    .line 332832
    const-string v1, "FbTelephonyManager"

    const-string v2, "TelephonyManager.getPhoneCount() failed to be called directly"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332833
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(II)I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 332788
    invoke-virtual {p0}, LX/1rd;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 332789
    iget-object v1, p0, LX/1rd;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    .line 332790
    invoke-virtual {v1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v1

    .line 332791
    if-nez v1, :cond_1

    move p1, v0

    .line 332792
    :cond_0
    :goto_0
    return p1

    .line 332793
    :cond_1
    const/4 v0, 0x0

    .line 332794
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 332795
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    .line 332796
    if-eq v0, p1, :cond_0

    .line 332797
    if-ne v0, p2, :cond_5

    .line 332798
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 332799
    goto :goto_1

    .line 332800
    :cond_2
    if-eqz v1, :cond_3

    move p1, p2

    .line 332801
    goto :goto_0

    .line 332802
    :cond_3
    invoke-static {}, Landroid/telephony/SmsManager;->getDefaultSmsSubscriptionId()I

    move-result p1

    goto :goto_0

    :cond_4
    move p1, v0

    .line 332803
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final b()I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    .line 332749
    invoke-virtual {p0}, LX/1rd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332750
    iget-object v0, p0, LX/1rd;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionManager;

    .line 332751
    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoCount()I

    move-result v0

    .line 332752
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 332779
    invoke-static {p0}, LX/1rd;->e(LX/1rd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332780
    :try_start_0
    new-instance v0, LX/1rg;

    invoke-direct {v0}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->d()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 332781
    :goto_0
    return v0

    .line 332782
    :catch_0
    move-exception v0

    .line 332783
    :goto_1
    const-string v1, "FbTelephonyManager"

    const-string v2, "Error attempting to get network type from MediaTek API."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332784
    :cond_0
    iget-object v0, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 332785
    iget-object v0, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    goto :goto_0

    .line 332786
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 332787
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 332778
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    invoke-static {p0}, LX/1rd;->d(LX/1rd;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 332777
    invoke-virtual {p0, p1}, LX/1rd;->m(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/1rd;->h(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(I)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 332764
    invoke-static {p0}, LX/1rd;->d(LX/1rd;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1rd;->f(LX/1rd;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 332765
    :cond_0
    invoke-static {p0}, LX/1rd;->e(LX/1rd;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332766
    :try_start_0
    new-instance v0, LX/1rg;

    invoke-direct {v0}, LX/1rg;-><init>()V

    invoke-static {}, LX/1rg;->j()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 332767
    :cond_1
    :goto_0
    return-object v0

    .line 332768
    :catch_0
    move-exception v0

    .line 332769
    :goto_1
    const-string v1, "FbTelephonyManager"

    const-string v2, "Error attempting to get phone number from MediaTek API."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332770
    :cond_2
    invoke-static {p0, p1}, LX/1rd;->q(LX/1rd;I)Ljava/lang/String;

    move-result-object v0

    .line 332771
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 332772
    iget-object v0, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 332773
    iget-object v0, p0, LX/1rd;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 332774
    :cond_3
    iget-object v0, p0, LX/1rd;->e:LX/1re;

    invoke-virtual {v0, p1}, LX/1re;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 332775
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 332776
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final m(I)I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    .line 332753
    const/4 v0, -0x1

    .line 332754
    invoke-virtual {p0}, LX/1rd;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 332755
    iget-object v1, p0, LX/1rd;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/SubscriptionManager;

    .line 332756
    const/4 v2, -0x1

    invoke-virtual {p0, p1, v2}, LX/1rd;->a(II)I

    move-result v2

    move v2, v2

    .line 332757
    if-eq v2, p1, :cond_0

    if-ltz v0, :cond_0

    .line 332758
    :goto_0
    move v0, v0

    .line 332759
    return v0

    .line 332760
    :cond_0
    invoke-virtual {v1, v2}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    .line 332761
    if-eqz v1, :cond_1

    .line 332762
    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v0

    goto :goto_0

    .line 332763
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
