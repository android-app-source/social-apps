.class public LX/0iP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0vi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 120806
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 120807
    new-instance v0, LX/0ve;

    invoke-direct {v0}, LX/0ve;-><init>()V

    sput-object v0, LX/0iP;->a:LX/0vi;

    .line 120808
    :goto_0
    return-void

    .line 120809
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 120810
    new-instance v0, LX/0vf;

    invoke-direct {v0}, LX/0vf;-><init>()V

    sput-object v0, LX/0iP;->a:LX/0vi;

    goto :goto_0

    .line 120811
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2

    .line 120812
    new-instance v0, LX/0vg;

    invoke-direct {v0}, LX/0vg;-><init>()V

    sput-object v0, LX/0iP;->a:LX/0vi;

    goto :goto_0

    .line 120813
    :cond_2
    new-instance v0, LX/0vh;

    invoke-direct {v0}, LX/0vh;-><init>()V

    sput-object v0, LX/0iP;->a:LX/0vi;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120815
    return-void
.end method

.method public static a(Landroid/view/ViewConfiguration;)I
    .locals 1

    .prologue
    .line 120816
    sget-object v0, LX/0iP;->a:LX/0vi;

    invoke-interface {v0, p0}, LX/0vi;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/ViewConfiguration;)Z
    .locals 1

    .prologue
    .line 120817
    sget-object v0, LX/0iP;->a:LX/0vi;

    invoke-interface {v0, p0}, LX/0vi;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    return v0
.end method
