.class public LX/1e7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Ck;

.field private final b:LX/0tX;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287333
    iput-object p1, p0, LX/1e7;->a:LX/1Ck;

    .line 287334
    iput-object p2, p0, LX/1e7;->b:LX/0tX;

    .line 287335
    iput-object p3, p0, LX/1e7;->c:Ljava/lang/String;

    .line 287336
    return-void
.end method

.method public static a(LX/0QB;)LX/1e7;
    .locals 1

    .prologue
    .line 287337
    invoke-static {p0}, LX/1e7;->b(LX/0QB;)LX/1e7;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1e7;
    .locals 4

    .prologue
    .line 287338
    new-instance v3, LX/1e7;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v0, v1, v2}, LX/1e7;-><init>(LX/1Ck;LX/0tX;Ljava/lang/String;)V

    .line 287339
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Vd;)V
    .locals 5

    .prologue
    .line 287340
    new-instance v0, LX/A6J;

    invoke-direct {v0}, LX/A6J;-><init>()V

    move-object v0, v0

    .line 287341
    const-string v1, "input"

    new-instance v2, LX/4Ju;

    invoke-direct {v2}, LX/4Ju;-><init>()V

    iget-object v3, p0, LX/1e7;->c:Ljava/lang/String;

    .line 287342
    const-string v4, "actor_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287343
    move-object v2, v2

    .line 287344
    const-string v3, "hide_topic_id"

    invoke-virtual {v2, v3, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287345
    move-object v2, v2

    .line 287346
    const-string v3, "hide_user_id"

    invoke-virtual {v2, v3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287347
    move-object v2, v2

    .line 287348
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 287349
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 287350
    iget-object v1, p0, LX/1e7;->a:LX/1Ck;

    const-string v2, "MUTATE_USER_TOPIC_HIDE_KEY"

    iget-object v3, p0, LX/1e7;->b:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p3}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 287351
    return-void
.end method
