.class public LX/1Np;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1Np;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0gh;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 237745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237746
    iput-object p1, p0, LX/1Np;->a:LX/0Zb;

    .line 237747
    iput-object p2, p0, LX/1Np;->b:LX/0gh;

    .line 237748
    return-void
.end method

.method public static a(LX/0QB;)LX/1Np;
    .locals 5

    .prologue
    .line 237749
    sget-object v0, LX/1Np;->c:LX/1Np;

    if-nez v0, :cond_1

    .line 237750
    const-class v1, LX/1Np;

    monitor-enter v1

    .line 237751
    :try_start_0
    sget-object v0, LX/1Np;->c:LX/1Np;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 237752
    if-eqz v2, :cond_0

    .line 237753
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 237754
    new-instance p0, LX/1Np;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v4

    check-cast v4, LX/0gh;

    invoke-direct {p0, v3, v4}, LX/1Np;-><init>(LX/0Zb;LX/0gh;)V

    .line 237755
    move-object v0, p0

    .line 237756
    sput-object v0, LX/1Np;->c:LX/1Np;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237757
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 237758
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 237759
    :cond_1
    sget-object v0, LX/1Np;->c:LX/1Np;

    return-object v0

    .line 237760
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 237761
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 237762
    iget-object v0, p0, LX/1Np;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 237763
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 237764
    const-string v1, "inline_composer"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 237765
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 237766
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 237767
    const-string v0, "inline_composer_text_box_clicked"

    invoke-direct {p0, v0}, LX/1Np;->b(Ljava/lang/String;)V

    .line 237768
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 237769
    const-string v0, "inline_composer_profile_pic_tapped"

    invoke-direct {p0, v0}, LX/1Np;->b(Ljava/lang/String;)V

    .line 237770
    iget-object v0, p0, LX/1Np;->b:LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 237771
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 237772
    const-string v0, "inline_composer_status_button_clicked"

    invoke-direct {p0, v0}, LX/1Np;->b(Ljava/lang/String;)V

    .line 237773
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 237774
    const-string v0, "inline_composer_photo_button_clicked"

    invoke-direct {p0, v0}, LX/1Np;->b(Ljava/lang/String;)V

    .line 237775
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 237776
    const-string v0, "inline_composer_check_in_button_clicked"

    invoke-direct {p0, v0}, LX/1Np;->b(Ljava/lang/String;)V

    .line 237777
    return-void
.end method
