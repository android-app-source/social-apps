.class public final enum LX/1Li;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1Li;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1Li;

.field public static final enum CHANNEL:LX/1Li;

.field public static final enum COMMERCIAL_BREAK:LX/1Li;

.field public static final enum DIRECT_INBOX:LX/1Li;

.field public static final enum INSTANT_ARTICLE:LX/1Li;

.field public static final enum MISC:LX/1Li;

.field public static final enum NEWSFEED:LX/1Li;

.field public static final enum TIMELINE:LX/1Li;

.field public static final enum VIDEO_HOME:LX/1Li;


# instance fields
.field public final priority:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 234139
    new-instance v0, LX/1Li;

    const-string v1, "COMMERCIAL_BREAK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->COMMERCIAL_BREAK:LX/1Li;

    .line 234140
    new-instance v0, LX/1Li;

    const-string v1, "INSTANT_ARTICLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->INSTANT_ARTICLE:LX/1Li;

    .line 234141
    new-instance v0, LX/1Li;

    const-string v1, "CHANNEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->CHANNEL:LX/1Li;

    .line 234142
    new-instance v0, LX/1Li;

    const-string v1, "DIRECT_INBOX"

    invoke-direct {v0, v1, v6, v7}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->DIRECT_INBOX:LX/1Li;

    .line 234143
    new-instance v0, LX/1Li;

    const-string v1, "VIDEO_HOME"

    invoke-direct {v0, v1, v7, v6}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->VIDEO_HOME:LX/1Li;

    .line 234144
    new-instance v0, LX/1Li;

    const-string v1, "TIMELINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v5}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->TIMELINE:LX/1Li;

    .line 234145
    new-instance v0, LX/1Li;

    const-string v1, "MISC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->MISC:LX/1Li;

    .line 234146
    new-instance v0, LX/1Li;

    const-string v1, "NEWSFEED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1Li;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1Li;->NEWSFEED:LX/1Li;

    .line 234147
    const/16 v0, 0x8

    new-array v0, v0, [LX/1Li;

    sget-object v1, LX/1Li;->COMMERCIAL_BREAK:LX/1Li;

    aput-object v1, v0, v3

    sget-object v1, LX/1Li;->INSTANT_ARTICLE:LX/1Li;

    aput-object v1, v0, v4

    sget-object v1, LX/1Li;->CHANNEL:LX/1Li;

    aput-object v1, v0, v5

    sget-object v1, LX/1Li;->DIRECT_INBOX:LX/1Li;

    aput-object v1, v0, v6

    sget-object v1, LX/1Li;->VIDEO_HOME:LX/1Li;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1Li;->TIMELINE:LX/1Li;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1Li;->MISC:LX/1Li;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1Li;->NEWSFEED:LX/1Li;

    aput-object v2, v0, v1

    sput-object v0, LX/1Li;->$VALUES:[LX/1Li;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 234148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 234149
    iput p3, p0, LX/1Li;->priority:I

    .line 234150
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1Li;
    .locals 1

    .prologue
    .line 234151
    const-class v0, LX/1Li;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1Li;

    return-object v0
.end method

.method public static values()[LX/1Li;
    .locals 1

    .prologue
    .line 234152
    sget-object v0, LX/1Li;->$VALUES:[LX/1Li;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1Li;

    return-object v0
.end method
