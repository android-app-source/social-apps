.class public LX/1KX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1KX;


# instance fields
.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/freshfeed/FreshFeedStoryCollectionStatus$DebugStatusDataLoader;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1jg;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field public k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/FeedUnit;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232788
    new-instance v0, LX/1KX;

    invoke-direct {v0}, LX/1KX;-><init>()V

    sput-object v0, LX/1KX;->a:LX/1KX;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 232785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232786
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1KX;->i:Ljava/util/List;

    .line 232787
    return-void
.end method

.method private static q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;
    .locals 1

    .prologue
    .line 232782
    iget-object v0, p0, LX/1KX;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 232783
    iget-object v0, p0, LX/1KX;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 232784
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 232781
    iget-object v0, p0, LX/1KX;->l:Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 1

    .prologue
    .line 232779
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/1KX;->h:Ljava/lang/ref/WeakReference;

    .line 232780
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 232778
    iget v0, p0, LX/1KX;->b:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 232777
    iget v0, p0, LX/1KX;->c:I

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 232789
    iget v0, p0, LX/1KX;->d:I

    return v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232776
    iget-object v0, p0, LX/1KX;->f:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 232775
    iget-object v0, p0, LX/1KX;->g:Ljava/util/List;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 232759
    invoke-static {p0}, LX/1KX;->q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    move-result-object v0

    .line 232760
    if-eqz v0, :cond_3

    .line 232761
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 232762
    iget-object v2, v1, LX/1gi;->a:LX/0fz;

    .line 232763
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 232764
    const/4 v3, 0x0

    move p0, v3

    :goto_0
    :try_start_0
    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v3

    if-ge p0, v3, :cond_2

    .line 232765
    invoke-virtual {v2, p0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v3

    .line 232766
    instance-of v1, v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    .line 232767
    check-cast v3, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-static {v3}, LX/AjT;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/7zl;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232768
    :cond_0
    :goto_1
    add-int/lit8 v3, p0, 0x1

    move p0, v3

    goto :goto_0

    .line 232769
    :cond_1
    instance-of v1, v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    .line 232770
    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-static {v3}, LX/0x0;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v3

    invoke-static {v3}, LX/AjT;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/7zl;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 232771
    :catch_0
    :cond_2
    move-object v2, v0

    .line 232772
    move-object v1, v2

    .line 232773
    :goto_2
    move-object v0, v1

    .line 232774
    :goto_3
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final j()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 232729
    invoke-static {p0}, LX/1KX;->q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    move-result-object v0

    .line 232730
    if-eqz v0, :cond_2

    .line 232731
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 232732
    iget-object v2, v1, LX/1gi;->j:LX/1gj;

    .line 232733
    iget-object v3, v2, LX/1gj;->a:Ljava/util/List;

    .line 232734
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 232735
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2dg;

    .line 232736
    if-eqz p0, :cond_0

    .line 232737
    iget-object v2, p0, LX/2dg;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-object v2, v2

    .line 232738
    if-eqz v2, :cond_0

    .line 232739
    iget-object v2, p0, LX/2dg;->a:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-object p0, v2

    .line 232740
    invoke-static {p0}, LX/AjT;->a(Lcom/facebook/feed/model/ClientFeedUnitEdge;)LX/7zl;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232741
    :cond_1
    move-object v3, v0

    .line 232742
    move-object v2, v3

    .line 232743
    move-object v1, v2

    .line 232744
    :goto_1
    move-object v0, v1

    .line 232745
    :goto_2
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 232755
    invoke-static {p0}, LX/1KX;->q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    move-result-object v0

    .line 232756
    if-eqz v0, :cond_0

    .line 232757
    iget-object p0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->s:LX/1ge;

    invoke-virtual {p0}, LX/1ge;->d()Z

    move-result p0

    move v0, p0

    .line 232758
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 232751
    invoke-static {p0}, LX/1KX;->q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    move-result-object v0

    .line 232752
    if-eqz v0, :cond_0

    .line 232753
    iget-object p0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {p0}, LX/1gf;->c()Z

    move-result p0

    move v0, p0

    .line 232754
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 232747
    invoke-static {p0}, LX/1KX;->q(LX/1KX;)Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    move-result-object v0

    .line 232748
    if-eqz v0, :cond_0

    .line 232749
    iget-object p0, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->t:LX/1gf;

    invoke-virtual {p0}, LX/1gf;->d()Z

    move-result p0

    move v0, p0

    .line 232750
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232746
    iget-object v0, p0, LX/1KX;->k:Ljava/util/Set;

    return-object v0
.end method
