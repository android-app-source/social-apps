.class public LX/1FH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I

.field private static b:LX/1FI;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 221997
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v1

    const-wide/32 v3, 0x7fffffff

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    .line 221998
    int-to-long v3, v1

    const-wide/32 v5, 0x1000000

    cmp-long v2, v3, v5

    if-lez v2, :cond_0

    .line 221999
    div-int/lit8 v1, v1, 0x4

    mul-int/lit8 v1, v1, 0x3

    .line 222000
    :goto_0
    move v0, v1

    .line 222001
    sput v0, LX/1FH;->a:I

    return-void

    :cond_0
    div-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/1FI;
    .locals 3

    .prologue
    .line 221993
    sget-object v0, LX/1FH;->b:LX/1FI;

    if-nez v0, :cond_0

    .line 221994
    new-instance v0, LX/1FI;

    const/16 v1, 0x180

    sget v2, LX/1FH;->a:I

    invoke-direct {v0, v1, v2}, LX/1FI;-><init>(II)V

    sput-object v0, LX/1FH;->b:LX/1FI;

    .line 221995
    :cond_0
    sget-object v0, LX/1FH;->b:LX/1FI;

    return-object v0
.end method
