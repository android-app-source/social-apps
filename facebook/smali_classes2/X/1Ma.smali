.class public LX/1Ma;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/1Ma;


# instance fields
.field public final b:LX/0kd;

.field public final c:LX/0Xl;

.field public final d:LX/0Uh;

.field public final e:LX/0Wd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235779
    const-class v0, LX/1Ma;

    sput-object v0, LX/1Ma;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0kd;LX/0Xl;LX/0Uh;LX/0Wd;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235781
    iput-object p1, p0, LX/1Ma;->b:LX/0kd;

    .line 235782
    iput-object p2, p0, LX/1Ma;->c:LX/0Xl;

    .line 235783
    iput-object p3, p0, LX/1Ma;->d:LX/0Uh;

    .line 235784
    iput-object p4, p0, LX/1Ma;->e:LX/0Wd;

    .line 235785
    return-void
.end method

.method public static a(LX/0QB;)LX/1Ma;
    .locals 7

    .prologue
    .line 235786
    sget-object v0, LX/1Ma;->f:LX/1Ma;

    if-nez v0, :cond_1

    .line 235787
    const-class v1, LX/1Ma;

    monitor-enter v1

    .line 235788
    :try_start_0
    sget-object v0, LX/1Ma;->f:LX/1Ma;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235789
    if-eqz v2, :cond_0

    .line 235790
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235791
    new-instance p0, LX/1Ma;

    invoke-static {v0}, LX/0kd;->a(LX/0QB;)LX/0kd;

    move-result-object v3

    check-cast v3, LX/0kd;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v6

    check-cast v6, LX/0Wd;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1Ma;-><init>(LX/0kd;LX/0Xl;LX/0Uh;LX/0Wd;)V

    .line 235792
    move-object v0, p0

    .line 235793
    sput-object v0, LX/1Ma;->f:LX/1Ma;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235794
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235795
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235796
    :cond_1
    sget-object v0, LX/1Ma;->f:LX/1Ma;

    return-object v0

    .line 235797
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1Ma;LX/2EU;)V
    .locals 3

    .prologue
    .line 235799
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 235800
    const-string v1, "event"

    invoke-virtual {p1}, LX/2EU;->toValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235801
    const-string v1, "clock_skew_detected"

    invoke-virtual {p1}, LX/2EU;->isClockSkewDetected()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 235802
    iget-object v1, p0, LX/1Ma;->c:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 235803
    return-void
.end method
