.class public LX/1rw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1rx;


# direct methods
.method private constructor <init>(LX/1rx;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 333568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333569
    iput-object p1, p0, LX/1rw;->a:LX/1rx;

    .line 333570
    return-void
.end method

.method public static a(LX/0QB;)LX/1rw;
    .locals 1

    .prologue
    .line 333571
    invoke-static {p0}, LX/1rw;->b(LX/0QB;)LX/1rw;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1rw;
    .locals 2

    .prologue
    .line 333572
    new-instance v1, LX/1rw;

    invoke-static {p0}, LX/1rx;->b(LX/0QB;)LX/1rx;

    move-result-object v0

    check-cast v0, LX/1rx;

    invoke-direct {v1, v0}, LX/1rw;-><init>(LX/1rx;)V

    .line 333573
    return-object v1
.end method

.method public static b()LX/3T1;
    .locals 11

    .prologue
    .line 333574
    new-instance v0, LX/3T1;

    invoke-direct {v0}, LX/3T1;-><init>()V

    .line 333575
    new-instance v1, LX/3T2;

    invoke-direct {v1}, LX/3T2;-><init>()V

    const-string v2, "SETTING_PAGE_SECTION"

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v1

    const-string v2, "BASIC_MENU"

    const-string v3, "PROFILE_IMAGE_OPTION"

    const-string v4, "TEXT_WITH_BUTTON"

    const-string v5, "WASH_TEXTS"

    invoke-static {v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v1

    new-instance v2, LX/3T2;

    invoke-direct {v2}, LX/3T2;-><init>()V

    const-string v3, "MENU_SECTION_WITH_INDEPENDENT_ROWS"

    invoke-virtual {v2, v3}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v2

    const-string v3, "BASIC_MENU"

    const-string v4, "PROFILE_IMAGE_OPTION"

    const-string v5, "TEXT_WITH_BUTTON"

    const-string v6, "WASH_TEXTS"

    invoke-static {v3, v4, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v2

    new-instance v3, LX/3T2;

    invoke-direct {v3}, LX/3T2;-><init>()V

    const-string v4, "TOGGLE"

    invoke-virtual {v3, v4}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v3

    const-string v4, "TOGGLE_ON"

    const-string v5, "TOGGLE_OFF"

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v3

    new-instance v4, LX/3T2;

    invoke-direct {v4}, LX/3T2;-><init>()V

    const-string v5, "SINGLE_SELECTOR"

    invoke-virtual {v4, v5}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v4

    const-string v5, "BLUE_CIRCLE_BUTTON"

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v4

    new-instance v5, LX/3T2;

    invoke-direct {v5}, LX/3T2;-><init>()V

    const-string v6, "MULTI_SELECTOR"

    invoke-virtual {v5, v6}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v5

    const-string v6, "PLAIN_CHECK"

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 333576
    invoke-virtual {v0, v1}, LX/3T1;->a(Ljava/util/List;)LX/3T1;

    .line 333577
    const-string v2, "OPEN_ACTION_SHEET"

    const-string v3, "OPEN_DEVICE_PUSH_SETTINGS"

    const-string v4, "OPEN_EVENT_DASHBOARD"

    const-string v5, "OPEN_EVENT_SETTING"

    const-string v6, "OPEN_GROUP_DASHBOARD"

    const-string v7, "OPEN_GROUP_SETTING"

    const-string v8, "OPEN_SOUNDS_SETTING"

    const-string v9, "OPEN_SUB_PAGE"

    const-string v10, "SERVER_ACTION"

    invoke-static/range {v2 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 333578
    invoke-virtual {v0, v1}, LX/3T1;->b(Ljava/util/List;)LX/3T1;

    .line 333579
    return-object v0
.end method

.method public static d(LX/1rw;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3T2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333580
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 333581
    new-instance v1, LX/3T2;

    invoke-direct {v1}, LX/3T2;-><init>()V

    const-string v2, "LONGPRESS_MENU"

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v1

    const-string v2, "POPUP_MENU_OPTION"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333582
    const-string v1, "ACTION_SHEET_MENU"

    iget-object v2, p0, LX/1rw;->a:LX/1rx;

    .line 333583
    iget-object v3, v2, LX/1rx;->a:LX/0ad;

    sget-char v4, LX/15r;->k:C

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->LONGPRESS_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 333584
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333585
    new-instance v1, LX/3T2;

    invoke-direct {v1}, LX/3T2;-><init>()V

    const-string v2, "ACTION_SHEET_MENU"

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v1

    const-string v2, "POPUP_MENU_OPTION"

    const-string v3, "ACTION_SHEET_OPTION"

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333586
    new-instance v1, LX/3T2;

    invoke-direct {v1}, LX/3T2;-><init>()V

    const-string v2, "SINGLE_SELECTOR"

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/lang/String;)LX/3T2;

    move-result-object v1

    const-string v2, "PLAIN_CHECK"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3T2;->a(Ljava/util/List;)LX/3T2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 333587
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/3T1;
    .locals 10

    .prologue
    .line 333588
    new-instance v0, LX/3T1;

    invoke-direct {v0}, LX/3T1;-><init>()V

    .line 333589
    invoke-static {p0}, LX/1rw;->d(LX/1rw;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3T1;->a(Ljava/util/List;)LX/3T1;

    .line 333590
    const-string v2, "HIDE"

    const-string v3, "MODSUB"

    const-string v4, "OPEN_ACTION_SHEET"

    const-string v5, "SHOW_MORE"

    const-string v6, "UNSUB"

    const-string v7, "SERVER_ACTION"

    const-string v8, "SAVE_ITEM"

    const-string v9, "UNSAVE_ITEM"

    invoke-static/range {v2 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 333591
    invoke-virtual {v0, v1}, LX/3T1;->b(Ljava/util/List;)LX/3T1;

    .line 333592
    return-object v0
.end method
