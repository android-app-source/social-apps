.class public LX/1An;
.super LX/1Ao;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1An;


# instance fields
.field public final a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lcom/facebook/imagepipeline/module/IsMediaVariationsIndexEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 211114
    invoke-direct {p0}, LX/1Ao;-><init>()V

    .line 211115
    iput-object p1, p0, LX/1An;->a:Ljava/lang/Boolean;

    .line 211116
    return-void
.end method

.method public static a(LX/0QB;)LX/1An;
    .locals 4

    .prologue
    .line 211117
    sget-object v0, LX/1An;->b:LX/1An;

    if-nez v0, :cond_1

    .line 211118
    const-class v1, LX/1An;

    monitor-enter v1

    .line 211119
    :try_start_0
    sget-object v0, LX/1An;->b:LX/1An;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 211120
    if-eqz v2, :cond_0

    .line 211121
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 211122
    new-instance p0, LX/1An;

    invoke-static {v0}, LX/1Ap;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {p0, v3}, LX/1An;-><init>(Ljava/lang/Boolean;)V

    .line 211123
    move-object v0, p0

    .line 211124
    sput-object v0, LX/1An;->b:LX/1An;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211125
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 211126
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211127
    :cond_1
    sget-object v0, LX/1An;->b:LX/1An;

    return-object v0

    .line 211128
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 211129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/1bf;Ljava/lang/Object;LX/1bh;Ljava/lang/String;)LX/1bh;
    .locals 8

    .prologue
    .line 211130
    new-instance v0, LX/1bg;

    .line 211131
    iget-object v1, p0, LX/1An;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211132
    iget-object v1, p1, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 211133
    if-eqz v1, :cond_0

    .line 211134
    iget-object v1, p1, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 211135
    iget-object v2, v1, LX/1ny;->a:Ljava/lang/String;

    move-object v1, v2

    .line 211136
    if-eqz v1, :cond_0

    .line 211137
    iget-object v1, p1, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 211138
    iget-object v2, v1, LX/1ny;->a:Ljava/lang/String;

    move-object v1, v2

    .line 211139
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 211140
    iget-object v1, p1, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 211141
    iget-object v2, v1, LX/1ny;->a:Ljava/lang/String;

    move-object v1, v2

    .line 211142
    :goto_0
    move-object v1, v1

    .line 211143
    iget-object v2, p1, LX/1bf;->h:LX/1o9;

    move-object v2, v2

    .line 211144
    iget-object v3, p1, LX/1bf;->i:LX/1bd;

    move-object v3, v3

    .line 211145
    iget-object v4, p1, LX/1bf;->g:LX/1bZ;

    move-object v4, v4

    .line 211146
    move-object v5, p3

    move-object v6, p4

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/1bg;-><init>(Ljava/lang/String;LX/1o9;LX/1bd;LX/1bZ;LX/1bh;Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0

    .line 211147
    :cond_0
    iget-object v1, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 211148
    invoke-virtual {p0, v1}, LX/1An;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;)LX/1bh;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 211149
    invoke-direct {p0, p1, p2, v0, v0}, LX/1An;->a(LX/1bf;Ljava/lang/Object;LX/1bh;Ljava/lang/String;)LX/1bh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/Object;)LX/1bh;
    .locals 2

    .prologue
    .line 211150
    invoke-static {p1}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 211151
    new-instance v1, LX/1ec;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p2}, LX/1ec;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 211152
    invoke-static {p1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211153
    invoke-static {p1}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    .line 211154
    :cond_0
    return-object p1
.end method

.method public final b(LX/1bf;Ljava/lang/Object;)LX/1bh;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211155
    iget-object v0, p1, LX/1bf;->m:LX/33B;

    move-object v0, v0

    .line 211156
    if-eqz v0, :cond_0

    .line 211157
    invoke-interface {v0}, LX/33B;->a()LX/1bh;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v1, v0}, LX/1An;->a(LX/1bf;Ljava/lang/Object;LX/1bh;Ljava/lang/String;)LX/1bh;

    move-result-object v0

    .line 211158
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, v1, v1}, LX/1An;->a(LX/1bf;Ljava/lang/Object;LX/1bh;Ljava/lang/String;)LX/1bh;

    move-result-object v0

    goto :goto_0
.end method
