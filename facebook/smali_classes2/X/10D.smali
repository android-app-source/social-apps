.class public LX/10D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0fO;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0fO;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0fO;",
            "LX/0Ot",
            "<",
            "LX/0gJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 168492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168493
    iput-object p1, p0, LX/10D;->a:LX/0fO;

    .line 168494
    iput-object p2, p0, LX/10D;->b:LX/0Ot;

    .line 168495
    return-void
.end method

.method public static a(LX/0QB;)LX/10D;
    .locals 5

    .prologue
    .line 168496
    const-class v1, LX/10D;

    monitor-enter v1

    .line 168497
    :try_start_0
    sget-object v0, LX/10D;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 168498
    sput-object v2, LX/10D;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 168499
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168500
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 168501
    new-instance v4, LX/10D;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v3

    check-cast v3, LX/0fO;

    const/16 p0, 0x165

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/10D;-><init>(LX/0fO;LX/0Ot;)V

    .line 168502
    move-object v0, v4

    .line 168503
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 168504
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/10D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168505
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 168506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 11

    .prologue
    .line 168507
    iget-object v0, p0, LX/10D;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gJ;

    .line 168508
    iget-object v3, v0, LX/0gJ;->e:LX/0gK;

    invoke-virtual {v3}, LX/0gK;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 168509
    iget-object v3, v0, LX/0gJ;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gL;

    .line 168510
    sget-object v4, LX/0zS;->c:LX/0zS;

    .line 168511
    new-instance v7, LX/AFa;

    invoke-direct {v7}, LX/AFa;-><init>()V

    move-object v7, v7

    .line 168512
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/16 v9, 0xa

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 168513
    iget-object v8, v3, LX/0gL;->c:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    move-object v4, v7

    .line 168514
    new-instance v5, LX/AFZ;

    invoke-direct {v5, v3}, LX/AFZ;-><init>(LX/0gL;)V

    iget-object v6, v3, LX/0gL;->b:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 168515
    :goto_0
    const v0, 0x7f020a0d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 168516
    const v1, 0x7f0a00d5

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 168517
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f082aa7

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 168518
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 168519
    move-object v1, v1

    .line 168520
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/108;->p:Z

    .line 168521
    iput-object v0, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 168522
    move-object v0, v1

    .line 168523
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 168524
    return-object v0

    .line 168525
    :cond_0
    iget-object v3, v0, LX/0gJ;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gN;

    .line 168526
    sget-object v4, LX/0zS;->c:LX/0zS;

    .line 168527
    new-instance v7, LX/AGt;

    invoke-direct {v7}, LX/AGt;-><init>()V

    move-object v7, v7

    .line 168528
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/16 v9, 0xa

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 168529
    iget-object v8, v3, LX/0gN;->b:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    move-object v4, v7

    .line 168530
    new-instance v5, LX/AGs;

    invoke-direct {v5, v3}, LX/AGs;-><init>(LX/0gN;)V

    iget-object v6, v3, LX/0gN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 168531
    goto :goto_0
.end method
