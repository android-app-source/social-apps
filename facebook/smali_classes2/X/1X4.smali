.class public final LX/1X4;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1VD;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1X0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VD",
            "<TE;>.FeedStoryHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1VD;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1VD;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 270447
    iput-object p1, p0, LX/1X4;->b:LX/1VD;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 270448
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1X4;->c:[Ljava/lang/String;

    .line 270449
    iput v3, p0, LX/1X4;->d:I

    .line 270450
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1X4;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1X4;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1X4;LX/1De;IILX/1X0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1VD",
            "<TE;>.FeedStoryHeaderComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 270443
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 270444
    iput-object p4, p0, LX/1X4;->a:LX/1X0;

    .line 270445
    iget-object v0, p0, LX/1X4;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 270446
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/1X4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270440
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-object p1, v0, LX/1X0;->b:LX/1Pb;

    .line 270441
    iget-object v0, p0, LX/1X4;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 270442
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270437
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-object p1, v0, LX/1X0;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 270438
    iget-object v0, p0, LX/1X4;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 270439
    return-object p0
.end method

.method public final a(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270435
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->e:Z

    .line 270436
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 270431
    invoke-super {p0}, LX/1X5;->a()V

    .line 270432
    const/4 v0, 0x0

    iput-object v0, p0, LX/1X4;->a:LX/1X0;

    .line 270433
    iget-object v0, p0, LX/1X4;->b:LX/1VD;

    iget-object v0, v0, LX/1VD;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 270434
    return-void
.end method

.method public final c(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270429
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->g:Z

    .line 270430
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1VD;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 270401
    iget-object v1, p0, LX/1X4;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1X4;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1X4;->d:I

    if-ge v1, v2, :cond_2

    .line 270402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 270403
    :goto_0
    iget v2, p0, LX/1X4;->d:I

    if-ge v0, v2, :cond_1

    .line 270404
    iget-object v2, p0, LX/1X4;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270405
    iget-object v2, p0, LX/1X4;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270406
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270407
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270408
    :cond_2
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    .line 270409
    invoke-virtual {p0}, LX/1X4;->a()V

    .line 270410
    return-object v0
.end method

.method public final d(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270427
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->h:Z

    .line 270428
    return-object p0
.end method

.method public final e(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270425
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->i:Z

    .line 270426
    return-object p0
.end method

.method public final f(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270423
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->j:Z

    .line 270424
    return-object p0
.end method

.method public final g(Z)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270421
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput-boolean p1, v0, LX/1X0;->k:Z

    .line 270422
    return-object p0
.end method

.method public final h(I)LX/1X4;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270419
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput p1, v0, LX/1X0;->c:I

    .line 270420
    return-object p0
.end method

.method public final i(I)LX/1X4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270417
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput p1, v0, LX/1X0;->d:I

    .line 270418
    return-object p0
.end method

.method public final j(I)LX/1X4;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270415
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput p1, v0, LX/1X0;->l:I

    .line 270416
    return-object p0
.end method

.method public final m(I)LX/1X4;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270413
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput p1, v0, LX/1X0;->o:I

    .line 270414
    return-object p0
.end method

.method public final n(I)LX/1X4;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1VD",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 270411
    iget-object v0, p0, LX/1X4;->a:LX/1X0;

    iput p1, v0, LX/1X0;->p:I

    .line 270412
    return-object p0
.end method
