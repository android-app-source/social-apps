.class public final LX/0S8;
.super LX/0S9;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0S9",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:I

.field public d:I

.field public e:I

.field public f:LX/0ci;

.field public g:LX/0ci;

.field public h:J

.field public i:J

.field public j:LX/18j;

.field public k:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0QV;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 60834
    invoke-direct {p0}, LX/0S9;-><init>()V

    .line 60835
    iput v0, p0, LX/0S8;->c:I

    .line 60836
    iput v0, p0, LX/0S8;->d:I

    .line 60837
    iput v0, p0, LX/0S8;->e:I

    .line 60838
    iput-wide v2, p0, LX/0S8;->h:J

    .line 60839
    iput-wide v2, p0, LX/0S8;->i:J

    .line 60840
    return-void
.end method

.method public static c(LX/0S8;JLjava/util/concurrent/TimeUnit;)V
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60827
    iget-wide v4, p0, LX/0S8;->h:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0S8;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60828
    iget-wide v4, p0, LX/0S8;->i:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, LX/0S8;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60829
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60830
    return-void

    :cond_0
    move v0, v2

    .line 60831
    goto :goto_0

    :cond_1
    move v0, v2

    .line 60832
    goto :goto_1

    :cond_2
    move v0, v2

    .line 60833
    goto :goto_2
.end method


# virtual methods
.method public final a(I)LX/0S8;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60821
    iget v0, p0, LX/0S8;->c:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "initial capacity was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, LX/0S8;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60822
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 60823
    iput p1, p0, LX/0S8;->c:I

    .line 60824
    return-object p0

    :cond_0
    move v0, v2

    .line 60825
    goto :goto_0

    :cond_1
    move v1, v2

    .line 60826
    goto :goto_1
.end method

.method public final a(LX/0ci;)LX/0S8;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 60814
    iget-object v0, p0, LX/0S8;->f:LX/0ci;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Key strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/0S8;->f:LX/0ci;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60815
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ci;

    iput-object v0, p0, LX/0S8;->f:LX/0ci;

    .line 60816
    iget-object v0, p0, LX/0S8;->f:LX/0ci;

    sget-object v3, LX/0ci;->SOFT:LX/0ci;

    if-eq v0, v3, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "Soft keys are not supported"

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 60817
    sget-object v0, LX/0ci;->STRONG:LX/0ci;

    if-eq p1, v0, :cond_1

    .line 60818
    iput-boolean v1, p0, LX/0S8;->b:Z

    .line 60819
    :cond_1
    return-object p0

    :cond_2
    move v0, v2

    .line 60820
    goto :goto_0
.end method

.method public final b(LX/0ci;)LX/0S8;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 60808
    iget-object v0, p0, LX/0S8;->g:LX/0ci;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Value strength was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/0S8;->g:LX/0ci;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60809
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ci;

    iput-object v0, p0, LX/0S8;->g:LX/0ci;

    .line 60810
    sget-object v0, LX/0ci;->STRONG:LX/0ci;

    if-eq p1, v0, :cond_0

    .line 60811
    iput-boolean v1, p0, LX/0S8;->b:Z

    .line 60812
    :cond_0
    return-object p0

    :cond_1
    move v0, v2

    .line 60813
    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 60807
    iget v0, p0, LX/0S8;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0S8;->c:I

    goto :goto_0
.end method

.method public final c(I)LX/0S8;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60841
    iget v0, p0, LX/0S8;->d:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "concurrency level was already set to %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, LX/0S8;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 60842
    if-lez p1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 60843
    iput p1, p0, LX/0S8;->d:I

    .line 60844
    return-object p0

    :cond_0
    move v0, v2

    .line 60845
    goto :goto_0

    :cond_1
    move v1, v2

    .line 60846
    goto :goto_1
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 60806
    iget v0, p0, LX/0S8;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/0S8;->d:I

    goto :goto_0
.end method

.method public final e()LX/0S8;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 60805
    sget-object v0, LX/0ci;->WEAK:LX/0ci;

    invoke-virtual {p0, v0}, LX/0S8;->a(LX/0ci;)LX/0S8;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0ci;
    .locals 2

    .prologue
    .line 60804
    iget-object v0, p0, LX/0S8;->f:LX/0ci;

    sget-object v1, LX/0ci;->STRONG:LX/0ci;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ci;

    return-object v0
.end method

.method public final g()LX/0S8;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.lang.ref.WeakReference"
    .end annotation

    .prologue
    .line 60803
    sget-object v0, LX/0ci;->WEAK:LX/0ci;

    invoke-virtual {p0, v0}, LX/0S8;->b(LX/0ci;)LX/0S8;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/concurrent/ConcurrentMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/util/concurrent/ConcurrentMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 60800
    iget-boolean v0, p0, LX/0S8;->b:Z

    if-nez v0, :cond_0

    .line 60801
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, LX/0S8;->c()I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {p0}, LX/0S8;->d()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 60802
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/0S8;->j:LX/18j;

    if-nez v0, :cond_1

    new-instance v0, LX/0cn;

    invoke-direct {v0, p0}, LX/0cn;-><init>(LX/0S8;)V

    :goto_1
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_0

    :cond_1
    new-instance v0, LX/4zL;

    invoke-direct {v0, p0}, LX/4zL;-><init>(LX/0S8;)V

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    .line 60780
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    .line 60781
    iget v1, p0, LX/0S8;->c:I

    if-eq v1, v3, :cond_0

    .line 60782
    const-string v1, "initialCapacity"

    iget v2, p0, LX/0S8;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    .line 60783
    :cond_0
    iget v1, p0, LX/0S8;->d:I

    if-eq v1, v3, :cond_1

    .line 60784
    const-string v1, "concurrencyLevel"

    iget v2, p0, LX/0S8;->d:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    .line 60785
    :cond_1
    iget v1, p0, LX/0S8;->e:I

    if-eq v1, v3, :cond_2

    .line 60786
    const-string v1, "maximumSize"

    iget v2, p0, LX/0S8;->e:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    .line 60787
    :cond_2
    iget-wide v2, p0, LX/0S8;->h:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 60788
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LX/0S8;->h:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 60789
    :cond_3
    iget-wide v2, p0, LX/0S8;->i:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 60790
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, LX/0S8;->i:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 60791
    :cond_4
    iget-object v1, p0, LX/0S8;->f:LX/0ci;

    if-eqz v1, :cond_5

    .line 60792
    const-string v1, "keyStrength"

    iget-object v2, p0, LX/0S8;->f:LX/0ci;

    invoke-virtual {v2}, LX/0ci;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1IV;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 60793
    :cond_5
    iget-object v1, p0, LX/0S8;->g:LX/0ci;

    if-eqz v1, :cond_6

    .line 60794
    const-string v1, "valueStrength"

    iget-object v2, p0, LX/0S8;->g:LX/0ci;

    invoke-virtual {v2}, LX/0ci;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1IV;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    .line 60795
    :cond_6
    iget-object v1, p0, LX/0S8;->k:LX/0Qj;

    if-eqz v1, :cond_7

    .line 60796
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, LX/0zA;->addValue(Ljava/lang/Object;)LX/0zA;

    .line 60797
    :cond_7
    iget-object v1, p0, LX/0S9;->a:LX/0d2;

    if-eqz v1, :cond_8

    .line 60798
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, LX/0zA;->addValue(Ljava/lang/Object;)LX/0zA;

    .line 60799
    :cond_8
    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
