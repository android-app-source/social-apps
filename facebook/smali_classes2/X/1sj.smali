.class public LX/1sj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1sj;


# instance fields
.field private final a:LX/1sk;

.field private final b:LX/03V;

.field private final c:LX/1sl;


# direct methods
.method public constructor <init>(LX/1sk;LX/03V;LX/1sl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 335110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335111
    iput-object p1, p0, LX/1sj;->a:LX/1sk;

    .line 335112
    iput-object p2, p0, LX/1sj;->b:LX/03V;

    .line 335113
    iput-object p3, p0, LX/1sj;->c:LX/1sl;

    .line 335114
    return-void
.end method

.method public static a(LX/0QB;)LX/1sj;
    .locals 9

    .prologue
    .line 335115
    sget-object v0, LX/1sj;->d:LX/1sj;

    if-nez v0, :cond_1

    .line 335116
    const-class v1, LX/1sj;

    monitor-enter v1

    .line 335117
    :try_start_0
    sget-object v0, LX/1sj;->d:LX/1sj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 335118
    if-eqz v2, :cond_0

    .line 335119
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 335120
    new-instance v6, LX/1sj;

    invoke-static {v0}, LX/1sk;->a(LX/0QB;)LX/1sk;

    move-result-object v3

    check-cast v3, LX/1sk;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    .line 335121
    new-instance v8, LX/1sl;

    const/16 v5, 0x2fd

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v7

    check-cast v7, Ljava/util/Random;

    invoke-direct {v8, p0, v5, v7}, LX/1sl;-><init>(LX/0Or;LX/0W3;Ljava/util/Random;)V

    .line 335122
    move-object v5, v8

    .line 335123
    check-cast v5, LX/1sl;

    invoke-direct {v6, v3, v4, v5}, LX/1sj;-><init>(LX/1sk;LX/03V;LX/1sl;)V

    .line 335124
    move-object v0, v6

    .line 335125
    sput-object v0, LX/1sj;->d:LX/1sj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335126
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 335127
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 335128
    :cond_1
    sget-object v0, LX/1sj;->d:LX/1sj;

    return-object v0

    .line 335129
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 335130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;
    .locals 4
    .param p0    # Lcom/facebook/fbtrace/FbTraceNode;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 335131
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne p0, v0, :cond_0

    .line 335132
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 335133
    :goto_0
    return-object v0

    .line 335134
    :cond_0
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {p0, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335135
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    .line 335136
    :goto_1
    move-object v0, v0

    .line 335137
    goto :goto_0

    :cond_1
    new-instance v0, Lcom/facebook/fbtrace/FbTraceNode;

    iget-object v1, p0, Lcom/facebook/fbtrace/FbTraceNode;->b:Ljava/lang/String;

    invoke-static {}, LX/0Ps;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/fbtrace/FbTraceNode;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/fbtrace/FbTraceNode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b()Lcom/facebook/fbtrace/FbTraceNode;
    .locals 1

    .prologue
    .line 335138
    invoke-static {}, LX/0Ps;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbtrace/FbTraceNode;->a(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/fbtrace/FbTraceNode;
    .locals 1

    .prologue
    .line 335139
    const-string v0, "sampling_rate"

    invoke-virtual {p0, v0}, LX/1sj;->a(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 335140
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335141
    iget-object v0, p0, LX/1sj;->c:LX/1sl;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 335142
    iget-object v1, v0, LX/1sl;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v3}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 335143
    :goto_0
    move v0, v1

    .line 335144
    if-eqz v0, :cond_0

    .line 335145
    invoke-static {}, LX/0Ps;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbtrace/FbTraceNode;->a(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    .line 335146
    :goto_1
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_1

    .line 335147
    :cond_1
    const-string v9, "sampling_rate"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 335148
    sget-wide v9, LX/0X5;->gl:J

    .line 335149
    :goto_2
    move-wide v5, v9

    .line 335150
    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-nez v1, :cond_2

    move v1, v3

    .line 335151
    goto :goto_0

    .line 335152
    :cond_2
    iget-object v1, v0, LX/1sl;->b:LX/0W3;

    invoke-interface {v1, v5, v6, v3}, LX/0W4;->a(JI)I

    move-result v1

    .line 335153
    if-lez v1, :cond_3

    iget-object v4, v0, LX/1sl;->c:Ljava/util/Random;

    invoke-virtual {v4, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_0

    .line 335154
    :cond_4
    const-string v9, "voip_sampling_rate"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 335155
    sget-wide v9, LX/0X5;->gm:J

    goto :goto_2

    .line 335156
    :cond_5
    const-wide/16 v9, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    .locals 7
    .param p1    # Lcom/facebook/fbtrace/FbTraceNode;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # LX/2gR;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # LX/2gQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 335157
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne p1, v0, :cond_0

    .line 335158
    :goto_0
    return-void

    .line 335159
    :cond_0
    iget-object v0, p0, LX/1sj;->a:LX/1sk;

    .line 335160
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335161
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335162
    sget-object v1, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne p1, v1, :cond_1

    .line 335163
    :goto_1
    goto :goto_0

    .line 335164
    :cond_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fbtracer_trace_events2"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "trace_id"

    iget-object v3, p1, Lcom/facebook/fbtrace/FbTraceNode;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "self_id"

    iget-object v3, p1, Lcom/facebook/fbtrace/FbTraceNode;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "event"

    iget-object v3, p2, LX/2gR;->encodedName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "timestamp"

    iget-object v3, v0, LX/1sk;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "parent_id"

    iget-object v1, p1, Lcom/facebook/fbtrace/FbTraceNode;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/facebook/fbtrace/FbTraceNode;->d:Ljava/lang/String;

    :goto_2
    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 335165
    iget-object v2, v0, LX/1sk;->c:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->f()LX/162;

    move-result-object v4

    .line 335166
    if-nez p3, :cond_3

    move-object v2, v4

    .line 335167
    :goto_3
    move-object v2, v2

    .line 335168
    const-string v3, "info"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 335169
    iget-object v2, v0, LX/1sk;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    .line 335170
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    .line 335171
    :cond_3
    invoke-interface {p3}, LX/2gQ;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 335172
    iget-object v3, v0, LX/1sk;->c:LX/0lC;

    invoke-virtual {v3}, LX/0lC;->e()LX/0m9;

    move-result-object v6

    .line 335173
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 335174
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_4

    :cond_4
    move-object v2, v4

    .line 335175
    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/fbtrace/FbTraceNode;
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 335176
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335177
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 335178
    :try_start_0
    invoke-static {p1}, LX/0Ps;->a(Ljava/lang/String;)[J

    move-result-object v5

    .line 335179
    const/4 v6, 0x0

    aget-wide v7, v5, v6

    invoke-static {v7, v8}, LX/0Ps;->a(J)Ljava/lang/String;

    move-result-object v6

    .line 335180
    const/4 v7, 0x1

    aget-wide v7, v5, v7

    invoke-static {v7, v8}, LX/0Ps;->a(J)Ljava/lang/String;

    move-result-object v7

    .line 335181
    new-instance v5, Lcom/facebook/fbtrace/FbTraceNode;

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lcom/facebook/fbtrace/FbTraceNode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 335182
    :goto_0
    move-object v0, v5

    .line 335183
    sget-object v1, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne v0, v1, :cond_0

    .line 335184
    iget-object v1, p0, LX/1sj;->b:LX/03V;

    const-string v2, "invalid_fbtrace_metadata"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalide fbtrace metadata: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 335185
    :cond_0
    return-object v0

    .line 335186
    :catch_0
    move-exception v5

    .line 335187
    sget-object v6, Lcom/facebook/fbtrace/FbTraceNode;->e:Ljava/lang/Class;

    const-string v7, "invalid FbTrace metadata: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v9

    invoke-static {v6, v5, v7, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335188
    :goto_1
    sget-object v5, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    goto :goto_0

    .line 335189
    :catch_1
    move-exception v5

    .line 335190
    sget-object v6, Lcom/facebook/fbtrace/FbTraceNode;->e:Ljava/lang/Class;

    const-string v7, "invalid FbTrace metadata: %s"

    new-array v8, v10, [Ljava/lang/Object;

    aput-object p1, v8, v9

    invoke-static {v6, v5, v7, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
