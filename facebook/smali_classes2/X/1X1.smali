.class public abstract LX/1X1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/facebook/components/ComponentLifecycle;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final e:LX/1S3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field public f:LX/1De;

.field public g:Z

.field public h:LX/1Dg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 270337
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/1X1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/1S3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 270329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270330
    sget-object v0, LX/1X1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, LX/1X1;->b:I

    .line 270331
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1X1;->g:Z

    .line 270332
    iput-object p1, p0, LX/1X1;->e:LX/1S3;

    .line 270333
    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    .line 270334
    iget p1, v0, LX/1S3;->d:I

    move v0, p1

    .line 270335
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1X1;->d:Ljava/lang/String;

    .line 270336
    return-void
.end method

.method public static b(LX/1X1;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270328
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    instance-of v0, v0, LX/1dI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1X1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270327
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    invoke-virtual {v0}, LX/1S3;->f()LX/1mv;

    move-result-object v0

    sget-object v1, LX/1mv;->NONE:LX/1mv;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/1X1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270326
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    invoke-virtual {v0}, LX/1S3;->f()LX/1mv;

    move-result-object v0

    sget-object v1, LX/1mv;->VIEW:LX/1mv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/1X1;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270277
    invoke-static {p0}, LX/1X1;->h(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    invoke-virtual {v0}, LX/1S3;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/1X1;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270325
    invoke-static {p0}, LX/1X1;->e(LX/1X1;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, LX/1X1;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h(LX/1X1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 270324
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    invoke-virtual {v0}, LX/1S3;->f()LX/1mv;

    move-result-object v0

    sget-object v1, LX/1mv;->NONE:LX/1mv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final a(LX/1De;IILX/1no;)V
    .locals 7

    .prologue
    .line 270314
    invoke-virtual {p0}, LX/1X1;->k()V

    .line 270315
    const/4 v3, 0x0

    .line 270316
    move-object v1, p1

    move-object v2, p0

    move v4, p2

    move v5, p3

    move-object v6, v3

    invoke-static/range {v1 .. v6}, LX/1dN;->a(LX/1De;LX/1X1;LX/1Dg;IILX/1ml;)LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 270317
    iput-object v0, p0, LX/1X1;->h:LX/1Dg;

    .line 270318
    invoke-static {p0}, LX/1X1;->h(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270319
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    invoke-virtual {v0, p2}, LX/1Dg;->Z(I)V

    .line 270320
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    invoke-virtual {v0, p3}, LX/1Dg;->aa(I)V

    .line 270321
    :cond_0
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    invoke-virtual {v0}, LX/1Dg;->c()I

    move-result v0

    iput v0, p4, LX/1no;->a:I

    .line 270322
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    invoke-virtual {v0}, LX/1Dg;->d()I

    move-result v0

    iput v0, p4, LX/1no;->b:I

    .line 270323
    return-void
.end method

.method public a(LX/1X1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<T",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 270313
    return-void
.end method

.method public final b(LX/1De;)V
    .locals 4

    .prologue
    .line 270299
    iget-object v0, p1, LX/1De;->g:LX/1X1;

    move-object v1, v0

    .line 270300
    iget-object v0, p0, LX/1X1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 270301
    if-nez v1, :cond_1

    .line 270302
    :goto_0
    iput-object v0, p0, LX/1X1;->c:Ljava/lang/String;

    .line 270303
    invoke-static {p1, p0}, LX/1De;->a(LX/1De;LX/1X1;)LX/1De;

    move-result-object v0

    .line 270304
    iput-object v0, p0, LX/1X1;->f:LX/1De;

    .line 270305
    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 270306
    invoke-virtual {v0}, LX/1S3;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270307
    iget-object v0, p1, LX/1De;->e:LX/1mg;

    move-object v0, v0

    .line 270308
    invoke-virtual {v0, p0}, LX/1mg;->a(LX/1X1;)V

    .line 270309
    :cond_0
    return-void

    .line 270310
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 270311
    iget-object v3, v1, LX/1X1;->c:Ljava/lang/String;

    move-object v1, v3

    .line 270312
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 270294
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1X1;->g:Z

    if-eqz v0, :cond_0

    .line 270295
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate layout of a component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 270297
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/1X1;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270298
    monitor-exit p0

    return-void
.end method

.method public final f()LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<T",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270289
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    .line 270290
    const/4 v1, 0x0

    iput-object v1, v0, LX/1X1;->f:LX/1De;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270291
    return-object v0

    .line 270292
    :catch_0
    move-exception v0

    .line 270293
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public g()LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<T",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270284
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    .line 270285
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1X1;->g:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270286
    return-object v0

    .line 270287
    :catch_0
    move-exception v0

    .line 270288
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 270283
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 270279
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    if-eqz v0, :cond_0

    .line 270280
    iget-object v0, p0, LX/1X1;->h:LX/1Dg;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1dN;->a(LX/1Dg;Z)V

    .line 270281
    const/4 v0, 0x0

    iput-object v0, p0, LX/1X1;->h:LX/1Dg;

    .line 270282
    :cond_0
    return-void
.end method

.method public final n()LX/1S3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()T",
            "L;"
        }
    .end annotation

    .prologue
    .line 270278
    iget-object v0, p0, LX/1X1;->e:LX/1S3;

    return-object v0
.end method
