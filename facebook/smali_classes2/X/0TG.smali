.class public LX/0TG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/prefs/shared/FbSharedPreferences;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SharedPreferencesUse"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field private static volatile k:LX/0TG;


# instance fields
.field private final c:I

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0UG;

.field public final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Tj;

.field private volatile j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62688
    const-class v0, LX/0TG;

    sput-object v0, LX/0TG;->a:Ljava/lang/Class;

    .line 62689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0TG;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_NULL_PREF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0TG;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Ot;Ljava/lang/Integer;LX/0Tj;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/prefs/shared/internal/config/DefaultWriteDelay;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/lang/Integer;",
            "LX/0Tj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 62690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62691
    iput-object p2, p0, LX/0TG;->d:Ljava/util/concurrent/ExecutorService;

    .line 62692
    const-string v0, "FbSharedPreferences.ctor"

    const v1, 0x27885b07

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 62693
    :try_start_0
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/0TG;->c:I

    .line 62694
    iput-object p1, p0, LX/0TG;->e:Ljava/util/concurrent/ExecutorService;

    .line 62695
    iput-object p3, p0, LX/0TG;->f:LX/0Ot;

    .line 62696
    new-instance v0, LX/0UG;

    invoke-direct {v0}, LX/0UG;-><init>()V

    iput-object v0, p0, LX/0TG;->g:LX/0UG;

    .line 62697
    invoke-static {}, LX/0UK;->b()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iput-object v0, p0, LX/0TG;->h:Ljava/util/Queue;

    .line 62698
    iput-object p5, p0, LX/0TG;->i:LX/0Tj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62699
    const v0, 0x62706fce

    invoke-static {v0}, LX/02m;->a(I)V

    .line 62700
    return-void

    .line 62701
    :catchall_0
    move-exception v0

    const v1, -0x70b35e41

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(LX/0QB;)LX/0TG;
    .locals 9

    .prologue
    .line 62702
    sget-object v0, LX/0TG;->k:LX/0TG;

    if-nez v0, :cond_1

    .line 62703
    const-class v1, LX/0TG;

    monitor-enter v1

    .line 62704
    :try_start_0
    sget-object v0, LX/0TG;->k:LX/0TG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62705
    if-eqz v2, :cond_0

    .line 62706
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62707
    new-instance v3, LX/0TG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 62708
    invoke-static {}, LX/0Ti;->a()Ljava/lang/Integer;

    move-result-object v7

    move-object v7, v7

    .line 62709
    check-cast v7, Ljava/lang/Integer;

    invoke-static {v0}, LX/0Tj;->a(LX/0QB;)LX/0Tj;

    move-result-object v8

    check-cast v8, LX/0Tj;

    invoke-direct/range {v3 .. v8}, LX/0TG;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Ot;Ljava/lang/Integer;LX/0Tj;)V

    .line 62710
    move-object v0, v3

    .line 62711
    sput-object v0, LX/0TG;->k:LX/0TG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62714
    :cond_1
    sget-object v0, LX/0TG;->k:LX/0TG;

    return-object v0

    .line 62715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private g(LX/0Tn;)Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62717
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->c(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Tn;D)D
    .locals 2

    .prologue
    .line 62718
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 62719
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public final a(LX/0Tn;F)F
    .locals 1

    .prologue
    .line 62720
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 62721
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    :cond_0
    return p2
.end method

.method public final a(LX/0Tn;I)I
    .locals 1

    .prologue
    .line 62722
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 62723
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method public final a(LX/0Tn;J)J
    .locals 2

    .prologue
    .line 62724
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 62725
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public final a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62726
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62727
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public final a(LX/0Tn;LX/0dN;)V
    .locals 1

    .prologue
    .line 62728
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    invoke-virtual {v0, p1, p2}, LX/0UG;->a(LX/0Tn;LX/0dN;)V

    .line 62729
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 62730
    invoke-virtual {p0}, LX/0TG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62731
    iget-object v0, p0, LX/0TG;->e:Ljava/util/concurrent/ExecutorService;

    const v1, -0x3f606678

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 62732
    :goto_0
    return-void

    .line 62733
    :cond_0
    iget-object v0, p0, LX/0TG;->h:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0dN;)V
    .locals 2

    .prologue
    .line 62734
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    .line 62735
    iget-object v1, v0, LX/0UG;->a:LX/0UH;

    new-instance p0, LX/0Tn;

    invoke-direct {p0, p1}, LX/0Tn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p2}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62736
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62681
    invoke-virtual {p0}, LX/0TG;->edit()LX/0hN;

    move-result-object v1

    .line 62682
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 62683
    invoke-interface {v1, v0}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    goto :goto_0

    .line 62684
    :cond_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 62685
    return-void
.end method

.method public final a(Ljava/util/Set;LX/0dN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;",
            "LX/0dN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62686
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    invoke-virtual {v0, p1, p2}, LX/0UG;->a(Ljava/util/Set;LX/0dN;)V

    .line 62687
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 62648
    iget-boolean v0, p0, LX/0TG;->j:Z

    return v0
.end method

.method public final a(LX/0Tn;)Z
    .locals 1

    .prologue
    .line 62649
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->a(LX/0Tn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/0Tn;Z)Z
    .locals 1

    .prologue
    .line 62650
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 62651
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method public final b(LX/0Tn;)LX/03R;
    .locals 1

    .prologue
    .line 62652
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 62653
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/03R;->valueOf(Ljava/lang/Boolean;)LX/03R;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 5

    .prologue
    .line 62654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0}, LX/0Tj;->a()V

    .line 62655
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    .line 62656
    iget-object v1, v0, LX/0Tj;->k:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62657
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    iget v1, p0, LX/0TG;->c:I

    int-to-long v2, v1

    .line 62658
    iput-wide v2, v0, LX/0Tj;->l:J

    .line 62659
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0TG;->j:Z

    .line 62660
    iget-object v0, p0, LX/0TG;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/prefs/shared/FbSharedPreferencesImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferencesImpl$1;-><init>(LX/0TG;)V

    const v2, 0x490d4849

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 62661
    const v0, -0x50b129f1

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62662
    monitor-exit p0

    return-void

    .line 62663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/0Tn;LX/0dN;)V
    .locals 1

    .prologue
    .line 62664
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    invoke-virtual {v0, p1, p2}, LX/0UG;->b(LX/0Tn;LX/0dN;)V

    .line 62665
    return-void
.end method

.method public final b(Ljava/util/Set;LX/0dN;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;",
            "LX/0dN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62666
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    .line 62667
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 62668
    invoke-virtual {v0, v1, p2}, LX/0UG;->b(LX/0Tn;LX/0dN;)V

    goto :goto_0

    .line 62669
    :cond_0
    return-void
.end method

.method public final c(LX/0Tn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62670
    iget-object v0, p0, LX/0TG;->i:LX/0Tj;

    invoke-virtual {v0, p1}, LX/0Tj;->b(LX/0Tn;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 62671
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, LX/0TG;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62672
    const v0, -0x1d179ae4

    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62673
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 62674
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final c(LX/0Tn;LX/0dN;)V
    .locals 1

    .prologue
    .line 62675
    iget-object v0, p0, LX/0TG;->g:LX/0UG;

    .line 62676
    iget-object p0, v0, LX/0UG;->b:LX/0UJ;

    invoke-virtual {p0, p1, p2}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62677
    return-void
.end method

.method public final d(LX/0Tn;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62678
    invoke-direct {p0, p1}, LX/0TG;->g(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/0Tn;)Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/SortedMap",
            "<",
            "LX/0Tn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62679
    invoke-direct {p0, p1}, LX/0TG;->g(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final edit()LX/0hN;
    .locals 1

    .prologue
    .line 62680
    new-instance v0, LX/0uP;

    invoke-direct {v0, p0}, LX/0uP;-><init>(LX/0TG;)V

    return-object v0
.end method
