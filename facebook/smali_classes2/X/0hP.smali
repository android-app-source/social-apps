.class public final LX/0hP;
.super LX/0hQ;
.source ""


# instance fields
.field public final synthetic a:LX/0gg;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/Handler;

.field private final d:Ljava/lang/Runnable;

.field private volatile e:Z


# direct methods
.method public constructor <init>(LX/0gg;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 116133
    iput-object p1, p0, LX/0hP;->a:LX/0gg;

    .line 116134
    iget-object v0, p1, LX/0gg;->q:LX/0gc;

    invoke-direct {p0, v0}, LX/0hQ;-><init>(LX/0gc;)V

    .line 116135
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0hP;->b:Ljava/util/Set;

    .line 116136
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0hP;->e:Z

    .line 116137
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/0hP;->c:Landroid/os/Handler;

    .line 116138
    new-instance v0, Lcom/facebook/ui/mainview/ViewPagerController$MyFragmentPagerAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/ui/mainview/ViewPagerController$MyFragmentPagerAdapter$1;-><init>(LX/0hP;LX/0gg;Landroid/content/Intent;)V

    iput-object v0, p0, LX/0hP;->d:Ljava/lang/Runnable;

    .line 116139
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 116121
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/0hP;->b()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 116122
    invoke-virtual {p0, v1}, LX/0hQ;->b(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 116123
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 116124
    if-ne v2, p1, :cond_1

    .line 116125
    iget-object v1, p0, LX/0hP;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_1
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_1

    .line 116126
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116127
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getItemPosition() called for fragment that was not created by this adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 116104
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 116105
    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    .line 116106
    iget-object v2, v0, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    if-eqz v2, :cond_0

    .line 116107
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 116108
    const-string v3, "target_fragment"

    iget-object v4, v0, Lcom/facebook/apptab/state/TabTag;->b:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116109
    const-string v3, "extra_launch_uri"

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116110
    const/4 v4, 0x1

    .line 116111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116112
    const-string v3, "launched_from_tab"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116113
    const-string v3, "passed_from_tab"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116114
    const-string v3, "current_tab_name_in_focus"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116115
    const-string v3, "tab_root_intent"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 116116
    move-object v0, v0

    .line 116117
    iget-object v1, p0, LX/0hP;->a:LX/0gg;

    iget-object v1, v1, LX/0gg;->f:Landroid/content/Context;

    const-class v2, Lcom/facebook/katana/fragment/FbChromeFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/fragment/FbChromeFragment;

    .line 116118
    iget-object v1, p0, LX/0hP;->a:LX/0gg;

    invoke-virtual {v1, v0, p1}, LX/0gg;->a(Lcom/facebook/katana/fragment/FbChromeFragment;I)V

    .line 116119
    return-object v0

    .line 116120
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tab is not mapped to a fragment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 2

    .prologue
    .line 116096
    if-nez p1, :cond_1

    .line 116097
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 116098
    check-cast v0, Landroid/os/Bundle;

    .line 116099
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 116100
    const-string v1, "tabs"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/NavigationConfig;

    .line 116101
    iget-object v1, p0, LX/0hP;->a:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->g()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v1

    .line 116102
    invoke-virtual {v0, v1}, Lcom/facebook/apptab/state/NavigationConfig;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116103
    invoke-super {p0, p1, p2}, LX/0hQ;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 116128
    iget-boolean v0, p0, LX/0hP;->e:Z

    invoke-virtual {p0, p2, p3, v0}, LX/0hQ;->a(ILjava/lang/Object;Z)V

    .line 116129
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    .line 116130
    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 116131
    iget-object v1, p0, LX/0hP;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 116132
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 116054
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, LX/0hP;->e:Z

    .line 116055
    iget-object v1, p0, LX/0hP;->a:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    .line 116056
    iget-object v2, v1, LX/0fd;->d:LX/0gx;

    .line 116057
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-virtual {v2}, LX/0gx;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_1

    .line 116058
    invoke-virtual {v2}, LX/0gx;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/apptab/state/TabTag;

    .line 116059
    invoke-virtual {v4}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v4

    .line 116060
    invoke-static {p1, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116061
    invoke-virtual {v2, v4}, LX/0gx;->d(Ljava/lang/String;)V

    .line 116062
    :cond_0
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 116063
    :cond_1
    move v1, v0

    .line 116064
    :goto_1
    invoke-virtual {p0}, LX/0hP;->b()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 116065
    invoke-virtual {p0, v1}, LX/0hQ;->b(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 116066
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 116067
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 116068
    iget-object v2, p0, LX/0hP;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116069
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 116070
    :cond_3
    invoke-virtual {p0}, LX/0gG;->kV_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116071
    iput-boolean v3, p0, LX/0hP;->e:Z

    .line 116072
    return-void

    .line 116073
    :catchall_0
    move-exception v0

    iput-boolean v3, p0, LX/0hP;->e:Z

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 116074
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, LX/0hP;->e:Z

    move v1, v0

    .line 116075
    :goto_0
    invoke-virtual {p0}, LX/0hP;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 116076
    invoke-virtual {p0, v1}, LX/0hQ;->b(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116077
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 116078
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116079
    iget-object v2, p0, LX/0hP;->a:LX/0gg;

    iget-object v2, v2, LX/0gg;->o:LX/0fd;

    .line 116080
    iget-object v4, v2, LX/0fd;->d:LX/0gx;

    invoke-virtual {v4, v0}, LX/0gx;->d(Ljava/lang/String;)V

    .line 116081
    iget-object v2, p0, LX/0hP;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116082
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116083
    :cond_1
    invoke-virtual {p0}, LX/0gG;->kV_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116084
    iput-boolean v3, p0, LX/0hP;->e:Z

    .line 116085
    return-void

    .line 116086
    :catchall_0
    move-exception v0

    iput-boolean v3, p0, LX/0hP;->e:Z

    throw v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 116087
    iget-object v0, p0, LX/0hP;->a:LX/0gg;

    iget-object v0, v0, LX/0gg;->o:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->f()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 116088
    invoke-super {p0, p1}, LX/0hQ;->b(Landroid/view/ViewGroup;)V

    .line 116089
    iget-object v0, p0, LX/0hP;->c:Landroid/os/Handler;

    iget-object v1, p0, LX/0hP;->d:Ljava/lang/Runnable;

    const v2, -0x59c135fb

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 116090
    return-void
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 116091
    invoke-super {p0}, LX/0hQ;->lf_()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 116092
    iget-object v1, p0, LX/0hP;->a:LX/0gg;

    iget-object v1, v1, LX/0gg;->o:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->g()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v1

    .line 116093
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 116094
    const-string v2, "tabs"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 116095
    :cond_0
    return-object v0
.end method
