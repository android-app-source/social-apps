.class public LX/0bP;
.super LX/0b5;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;)V
    .locals 6

    .prologue
    .line 86884
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;I)V

    .line 86885
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;I)V
    .locals 2

    .prologue
    .line 86886
    sget-object v0, LX/8Ki;->UPLOADING:LX/8Ki;

    if-ne p4, v0, :cond_0

    sget-object v0, LX/8KZ;->UPLOADING:LX/8KZ;

    :goto_0
    int-to-float v1, p5

    invoke-direct {p0, p1, v0, v1}, LX/0b5;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8KZ;F)V

    .line 86887
    iput p2, p0, LX/0bP;->a:I

    .line 86888
    iput p3, p0, LX/0bP;->b:I

    .line 86889
    return-void

    .line 86890
    :cond_0
    sget-object v0, LX/8KZ;->PUBLISHING:LX/8KZ;

    goto :goto_0
.end method
