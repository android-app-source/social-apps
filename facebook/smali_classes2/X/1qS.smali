.class public abstract LX/1qS;
.super LX/0Tr;
.source ""


# instance fields
.field private final a:LX/1qU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Tt;",
            "LX/1qU;",
            "LX/0Px",
            "<+",
            "LX/0Tw;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 330684
    invoke-direct {p0, p1, p2, p4, p5}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 330685
    iput-object p3, p0, LX/1qS;->a:LX/1qU;

    .line 330686
    return-void
.end method

.method private declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 330675
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1qS;->a:LX/1qU;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1qU;->a(Ljava/lang/String;)V
    :try_end_0
    .catch LX/497; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330676
    :goto_0
    monitor-exit p0

    return-void

    .line 330677
    :catch_0
    move-exception v0

    .line 330678
    :try_start_1
    invoke-virtual {p0}, LX/0Tr;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 330679
    :try_start_2
    iget-object v1, p0, LX/1qS;->a:LX/1qU;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1qU;->a(Ljava/lang/String;)V
    :try_end_2
    .catch LX/497; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 330680
    :catch_1
    move-exception v1

    .line 330681
    :try_start_3
    invoke-virtual {v0}, LX/497;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 330682
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot store uid, initial exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 330683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 330660
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 330671
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/1qS;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 330672
    invoke-direct {p0, v0}, LX/1qS;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330673
    monitor-exit p0

    return-object v0

    .line 330674
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 330667
    invoke-direct {p0}, LX/1qS;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 330668
    invoke-virtual {p0}, LX/0Tr;->g()V

    .line 330669
    iget-object v1, p0, LX/1qS;->a:LX/1qU;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1qU;->b(Ljava/lang/String;)V

    .line 330670
    return-void
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 330666
    invoke-virtual {p0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 330661
    invoke-virtual {p0}, LX/0Tr;->b()Ljava/lang/String;

    move-result-object v0

    .line 330662
    if-eqz v0, :cond_0

    .line 330663
    iget-object v1, p0, LX/1qS;->a:LX/1qU;

    invoke-interface {v1, v0}, LX/1qU;->b(Ljava/lang/String;)V

    .line 330664
    :cond_0
    invoke-super {p0}, LX/0Tr;->h()V

    .line 330665
    return-void
.end method
