.class public LX/1iP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonDelayerRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonIdleTimeoutRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonLigerRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonPriorityQueueRequestTypeInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/AndroidRedirectRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonRequestTimeoutRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonRetrierRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonSamplingConfigInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1he",
            "<",
            "Lcom/facebook/tigon/iface/TigonSwitcherRequestInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:[LX/1he;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/1he",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 298368
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->a:LX/1he;

    .line 298369
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->b:LX/1he;

    .line 298370
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->c:LX/1he;

    .line 298371
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->d:LX/1he;

    .line 298372
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->e:LX/1he;

    .line 298373
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->f:LX/1he;

    .line 298374
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->g:LX/1he;

    .line 298375
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->h:LX/1he;

    .line 298376
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->i:LX/1he;

    .line 298377
    new-instance v0, LX/1he;

    invoke-direct {v0}, LX/1he;-><init>()V

    sput-object v0, LX/1iP;->j:LX/1he;

    .line 298378
    const/16 v0, 0xa

    new-array v0, v0, [LX/1he;

    const/4 v1, 0x0

    sget-object v2, LX/1iP;->a:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/1iP;->b:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/1iP;->c:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/1iP;->d:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/1iP;->e:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/1iP;->f:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1iP;->g:LX/1he;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1iP;->h:LX/1he;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1iP;->i:LX/1he;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1iP;->j:LX/1he;

    aput-object v2, v0, v1

    sput-object v0, LX/1iP;->k:[LX/1he;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 298379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298380
    return-void
.end method
