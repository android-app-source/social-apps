.class public LX/1RG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1RF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method private constructor <init>(LX/1RF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 245832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245833
    iput-object p1, p0, LX/1RG;->a:LX/1RF;

    .line 245834
    return-void
.end method

.method public static a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245835
    new-instance v1, LX/1RG;

    invoke-direct {v1, p0}, LX/1RG;-><init>(LX/1RF;)V

    .line 245836
    iget-object v2, v1, LX/1RG;->a:LX/1RF;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v2, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v1, LX/1RG;->b:Z

    .line 245837
    return-object v1
.end method

.method public static a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245838
    new-instance v0, LX/1RG;

    invoke-direct {v0, p0}, LX/1RG;-><init>(LX/1RF;)V

    .line 245839
    iget-object v1, v0, LX/1RG;->a:LX/1RF;

    invoke-virtual {v1, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, LX/1RG;->b:Z

    .line 245840
    return-object v0
.end method

.method public static a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245841
    new-instance v0, LX/1RG;

    invoke-direct {v0, p0}, LX/1RG;-><init>(LX/1RF;)V

    .line 245842
    iget-object v1, v0, LX/1RG;->a:LX/1RF;

    invoke-virtual {v1, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, LX/1RG;->b:Z

    .line 245843
    return-object v0
.end method

.method public static a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;Z",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245844
    new-instance v1, LX/1RG;

    invoke-direct {v1, p0}, LX/1RG;-><init>(LX/1RF;)V

    .line 245845
    if-eqz p1, :cond_0

    .line 245846
    iget-object v2, v1, LX/1RG;->a:LX/1RF;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v2, v0, p3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, v1, LX/1RG;->b:Z

    .line 245847
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Ot;Ljava/lang/Object;)LX/1RG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245848
    iget-boolean v0, p0, LX/1RG;->b:Z

    if-nez v0, :cond_0

    .line 245849
    iget-object v1, p0, LX/1RG;->a:LX/1RF;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/1RG;->b:Z

    .line 245850
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245851
    iget-boolean v0, p0, LX/1RG;->b:Z

    if-nez v0, :cond_0

    .line 245852
    iget-object v0, p0, LX/1RG;->a:LX/1RF;

    invoke-virtual {v0, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/1RG;->b:Z

    .line 245853
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245854
    iget-boolean v0, p0, LX/1RG;->b:Z

    if-nez v0, :cond_0

    .line 245855
    iget-object v0, p0, LX/1RG;->a:LX/1RF;

    invoke-virtual {v0, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/1RG;->b:Z

    .line 245856
    :cond_0
    return-object p0
.end method

.method public final a(ZLX/0Ot;Ljava/lang/Object;)LX/1RG;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(Z",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245857
    iget-boolean v0, p0, LX/1RG;->b:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 245858
    iget-object v1, p0, LX/1RG;->a:LX/1RF;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {v1, v0, p3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/1RG;->b:Z

    .line 245859
    :cond_0
    return-object p0
.end method

.method public final a(ZLcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(Z",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)",
            "LX/1RG",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 245860
    iget-boolean v0, p0, LX/1RG;->b:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 245861
    iget-object v0, p0, LX/1RG;->a:LX/1RF;

    invoke-virtual {v0, p2, p3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/1RG;->b:Z

    .line 245862
    :cond_0
    return-object p0
.end method
