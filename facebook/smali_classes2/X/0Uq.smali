.class public LX/0Uq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0Uq;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Sh;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0V0;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 67033
    const-string v0, "App Init Lock Held"

    invoke-direct {p0, p1, v0}, LX/0Uq;-><init>(LX/0Sh;Ljava/lang/String;)V

    .line 67034
    return-void
.end method

.method public constructor <init>(LX/0Sh;Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 67084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67085
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Uq;->c:Ljava/util/ArrayList;

    .line 67086
    iput-object p1, p0, LX/0Uq;->b:LX/0Sh;

    .line 67087
    iput-object p2, p0, LX/0Uq;->a:Ljava/lang/String;

    .line 67088
    const-wide/16 v0, 0x8

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, v2}, LX/018;->b(JLjava/lang/String;I)V

    .line 67089
    return-void
.end method

.method public static a(LX/0QB;)LX/0Uq;
    .locals 4

    .prologue
    .line 67071
    sget-object v0, LX/0Uq;->e:LX/0Uq;

    if-nez v0, :cond_1

    .line 67072
    const-class v1, LX/0Uq;

    monitor-enter v1

    .line 67073
    :try_start_0
    sget-object v0, LX/0Uq;->e:LX/0Uq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 67074
    if-eqz v2, :cond_0

    .line 67075
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 67076
    new-instance p0, LX/0Uq;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {p0, v3}, LX/0Uq;-><init>(LX/0Sh;)V

    .line 67077
    move-object v0, p0

    .line 67078
    sput-object v0, LX/0Uq;->e:LX/0Uq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67079
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 67080
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 67081
    :cond_1
    sget-object v0, LX/0Uq;->e:LX/0Uq;

    return-object v0

    .line 67082
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 67083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 67069
    iget-object v0, p0, LX/0Uq;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/common/init/AppInitLock$1;

    invoke-direct {v1, p0}, Lcom/facebook/common/init/AppInitLock$1;-><init>(LX/0Uq;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 67070
    return-void
.end method

.method public static e(LX/0Uq;)V
    .locals 4

    .prologue
    .line 67060
    monitor-enter p0

    .line 67061
    :try_start_0
    iget-object v2, p0, LX/0Uq;->c:Ljava/util/ArrayList;

    .line 67062
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/0Uq;->c:Ljava/util/ArrayList;

    .line 67063
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67064
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0V0;

    .line 67065
    invoke-virtual {v0}, LX/0V0;->a()V

    .line 67066
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67067
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 67068
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 67051
    monitor-enter p0

    .line 67052
    :try_start_0
    iget-boolean v0, p0, LX/0Uq;->d:Z

    if-nez v0, :cond_0

    .line 67053
    const-wide/16 v0, 0x8

    iget-object v2, p0, LX/0Uq;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/018;->c(JLjava/lang/String;I)V

    .line 67054
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0Uq;->d:Z

    .line 67055
    const v0, -0x17089ca9

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 67056
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67057
    invoke-direct {p0}, LX/0Uq;->d()V

    .line 67058
    return-void

    .line 67059
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/0V0;)V
    .locals 1

    .prologue
    .line 67042
    monitor-enter p0

    .line 67043
    :try_start_0
    iget-object v0, p0, LX/0Uq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67044
    iget-object v0, p0, LX/0Uq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67045
    :cond_0
    iget-boolean v0, p0, LX/0Uq;->d:Z

    .line 67046
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67047
    if-eqz v0, :cond_1

    .line 67048
    invoke-direct {p0}, LX/0Uq;->d()V

    .line 67049
    :cond_1
    return-void

    .line 67050
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 67036
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, LX/0Uq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 67037
    const v0, -0xb6204b1

    :try_start_1
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 67038
    :catch_0
    move-exception v0

    .line 67039
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 67040
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67041
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 67035
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/0Uq;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
