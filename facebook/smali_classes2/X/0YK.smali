.class public LX/0YK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Y8;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0YK;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 80900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80901
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 80902
    iput-object v0, p0, LX/0YK;->a:LX/0Ot;

    .line 80903
    return-void
.end method

.method public static a(LX/0QB;)LX/0YK;
    .locals 4

    .prologue
    .line 80904
    sget-object v0, LX/0YK;->b:LX/0YK;

    if-nez v0, :cond_1

    .line 80905
    const-class v1, LX/0YK;

    monitor-enter v1

    .line 80906
    :try_start_0
    sget-object v0, LX/0YK;->b:LX/0YK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 80907
    if-eqz v2, :cond_0

    .line 80908
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 80909
    new-instance v3, LX/0YK;

    invoke-direct {v3}, LX/0YK;-><init>()V

    .line 80910
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object p0

    .line 80911
    iput-object p0, v3, LX/0YK;->a:LX/0Ot;

    .line 80912
    move-object v0, v3

    .line 80913
    sput-object v0, LX/0YK;->b:LX/0YK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80914
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 80915
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80916
    :cond_1
    sget-object v0, LX/0YK;->b:LX/0YK;

    return-object v0

    .line 80917
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 80918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 1

    .prologue
    .line 80919
    sget-boolean v0, LX/008;->deoptTaint:Z

    if-eqz v0, :cond_0

    .line 80920
    const-string v0, "dex_unopt"

    invoke-virtual {p1, v0}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/lang/String;)V

    .line 80921
    :cond_0
    invoke-static {}, LX/008;->getMainDexStoreLoadInformation()LX/02X;

    move-result-object v0

    .line 80922
    iget-object v0, v0, LX/02X;->odexSchemeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/lang/String;)V

    .line 80923
    iget-object v0, p0, LX/0YK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/03w;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80924
    const-string v0, "no_verifier"

    invoke-virtual {p1, v0}, Lcom/facebook/quicklog/PerformanceLoggingEvent;->a(Ljava/lang/String;)V

    .line 80925
    :cond_1
    return-void
.end method
