.class public LX/0rm;
.super LX/0rn;
.source ""


# static fields
.field private static final B:LX/0Yj;

.field private static final C:LX/0Yj;

.field private static final D:LX/0Yj;

.field private static final E:LX/0Yj;

.field private static final F:LX/0Yj;

.field private static final G:LX/0Yj;

.field private static final H:LX/0Yj;


# instance fields
.field private final A:LX/0sX;

.field public I:Ljava/lang/String;

.field public J:[I

.field private final K:LX/0sU;

.field private final L:LX/0tK;

.field private final M:LX/0tM;

.field private final N:LX/0tN;

.field public final O:LX/0rW;

.field private final P:LX/0tO;

.field private final Q:LX/0tQ;

.field private final R:Z

.field private final S:LX/0tR;

.field private final T:LX/0fO;

.field private final U:LX/0tT;

.field private final d:LX/0ad;

.field private final e:LX/0Uh;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sa;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0rq;

.field public final h:LX/0rz;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0w9;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/325;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/debug/feed/DebugFeedConfig;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0pX;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tI;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wo;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0zM;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0oy;

.field public final y:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 150531
    new-instance v0, LX/0Yj;

    const v1, 0xa0084

    const-string v2, "NNFCold_Network"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->B:LX/0Yj;

    .line 150532
    new-instance v0, LX/0Yj;

    const v1, 0xa0085

    const-string v2, "NNFWarm_Network"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->C:LX/0Yj;

    .line 150533
    new-instance v0, LX/0Yj;

    const v1, 0xa0086

    const-string v2, "NNFNetworktimeHead"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->D:LX/0Yj;

    .line 150534
    new-instance v0, LX/0Yj;

    const v1, 0xa0087

    const-string v2, "NNFNetworkTime"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->E:LX/0Yj;

    .line 150535
    new-instance v0, LX/0Yj;

    const v1, 0xa0088

    const-string v2, "NNFNetworkTimeChunkedRemainder"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->F:LX/0Yj;

    .line 150536
    new-instance v0, LX/0Yj;

    const v1, 0xa0089

    const-string v2, "NNFNetworkTimeTail"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->G:LX/0Yj;

    .line 150537
    new-instance v0, LX/0Yj;

    const v1, 0xa008a

    const-string v2, "NNFNetworktimeUnset"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/0rm;->H:LX/0Yj;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0Uh;LX/0Ot;LX/0rq;LX/0rz;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0sO;LX/03V;LX/0SG;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0So;LX/0Yl;LX/0Ot;LX/0Or;LX/0pX;LX/0Ot;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0oy;LX/0Or;LX/0sU;LX/0sX;LX/0sZ;LX/0tK;LX/0tM;LX/0tN;LX/0rW;LX/0tO;LX/0tQ;Ljava/lang/Boolean;LX/0tR;LX/0fO;LX/0tT;)V
    .locals 9
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p19    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsFlatBufferFromServerEnabled;
        .end annotation
    .end param
    .param p28    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsFetchFeedWithViewerCoordinatesEnabled;
        .end annotation
    .end param
    .param p38    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0sa;",
            ">;",
            "LX/0rq;",
            "LX/0rz;",
            "LX/0Ot",
            "<",
            "LX/0w9;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "LX/0sO;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/325;",
            ">;",
            "LX/0So;",
            "LX/0Yl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/debug/feed/DebugFeedConfig;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0pX;",
            "LX/0Ot",
            "<",
            "LX/0Yi;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0tI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0zM;",
            ">;",
            "LX/0oy;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0sU;",
            "LX/0sX;",
            "LX/0sZ;",
            "LX/0tK;",
            "LX/0tM;",
            "LX/0tN;",
            "LX/0rW;",
            "LX/0tO;",
            "LX/0tQ;",
            "Ljava/lang/Boolean;",
            "LX/0tR;",
            "LX/0fO;",
            "LX/0tT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 150538
    move-object v1, p0

    move-object/from16 v2, p10

    move-object/from16 v3, p11

    move-object/from16 v4, p17

    move-object/from16 v5, p12

    move-object/from16 v6, p16

    move-object v7, p1

    move-object/from16 v8, p31

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 150539
    const-string v1, ""

    iput-object v1, p0, LX/0rm;->I:Ljava/lang/String;

    .line 150540
    const/4 v1, 0x0

    iput-object v1, p0, LX/0rm;->J:[I

    .line 150541
    iput-object p1, p0, LX/0rm;->d:LX/0ad;

    .line 150542
    iput-object p2, p0, LX/0rm;->e:LX/0Uh;

    .line 150543
    iput-object p3, p0, LX/0rm;->f:LX/0Ot;

    .line 150544
    iput-object p4, p0, LX/0rm;->g:LX/0rq;

    .line 150545
    iput-object p5, p0, LX/0rm;->h:LX/0rz;

    .line 150546
    iput-object p6, p0, LX/0rm;->i:LX/0Ot;

    .line 150547
    move-object/from16 v0, p7

    iput-object v0, p0, LX/0rm;->n:LX/0Ot;

    .line 150548
    move-object/from16 v0, p9

    iput-object v0, p0, LX/0rm;->r:LX/0Ot;

    .line 150549
    move-object/from16 v0, p8

    iput-object v0, p0, LX/0rm;->o:LX/0Ot;

    .line 150550
    move-object/from16 v0, p13

    iput-object v0, p0, LX/0rm;->j:LX/0Or;

    .line 150551
    move-object/from16 v0, p15

    iput-object v0, p0, LX/0rm;->k:LX/0Ot;

    .line 150552
    move-object/from16 v0, p14

    iput-object v0, p0, LX/0rm;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 150553
    move-object/from16 v0, p18

    iput-object v0, p0, LX/0rm;->m:LX/0Ot;

    .line 150554
    move-object/from16 v0, p20

    iput-object v0, p0, LX/0rm;->q:LX/0pX;

    .line 150555
    move-object/from16 v0, p22

    iput-object v0, p0, LX/0rm;->s:LX/0Or;

    .line 150556
    move-object/from16 v0, p21

    iput-object v0, p0, LX/0rm;->p:LX/0Ot;

    .line 150557
    move-object/from16 v0, p23

    iput-object v0, p0, LX/0rm;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 150558
    move-object/from16 v0, p29

    iput-object v0, p0, LX/0rm;->K:LX/0sU;

    .line 150559
    move-object/from16 v0, p24

    iput-object v0, p0, LX/0rm;->u:LX/0Ot;

    .line 150560
    move-object/from16 v0, p25

    iput-object v0, p0, LX/0rm;->v:LX/0Ot;

    .line 150561
    move-object/from16 v0, p26

    iput-object v0, p0, LX/0rm;->w:LX/0Ot;

    .line 150562
    move-object/from16 v0, p27

    iput-object v0, p0, LX/0rm;->x:LX/0oy;

    .line 150563
    move-object/from16 v0, p19

    iput-object v0, p0, LX/0rm;->y:LX/0Or;

    .line 150564
    move-object/from16 v0, p28

    iput-object v0, p0, LX/0rm;->z:LX/0Or;

    .line 150565
    move-object/from16 v0, p30

    iput-object v0, p0, LX/0rm;->A:LX/0sX;

    .line 150566
    move-object/from16 v0, p32

    iput-object v0, p0, LX/0rm;->L:LX/0tK;

    .line 150567
    move-object/from16 v0, p33

    iput-object v0, p0, LX/0rm;->M:LX/0tM;

    .line 150568
    move-object/from16 v0, p34

    iput-object v0, p0, LX/0rm;->N:LX/0tN;

    .line 150569
    move-object/from16 v0, p35

    iput-object v0, p0, LX/0rm;->O:LX/0rW;

    .line 150570
    move-object/from16 v0, p36

    iput-object v0, p0, LX/0rm;->P:LX/0tO;

    .line 150571
    move-object/from16 v0, p37

    iput-object v0, p0, LX/0rm;->Q:LX/0tQ;

    .line 150572
    invoke-virtual/range {p38 .. p38}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/0rm;->R:Z

    .line 150573
    move-object/from16 v0, p39

    iput-object v0, p0, LX/0rm;->S:LX/0tR;

    .line 150574
    move-object/from16 v0, p40

    iput-object v0, p0, LX/0rm;->T:LX/0fO;

    .line 150575
    move-object/from16 v0, p41

    iput-object v0, p0, LX/0rm;->U:LX/0tT;

    .line 150576
    return-void
.end method

.method public static a(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/api/feed/FetchFeedParams;)LX/0Yj;
    .locals 3

    .prologue
    .line 150577
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v0, v0

    .line 150578
    sget-object v1, LX/0rU;->HEAD:LX/0rU;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0rU;->CHUNKED_INITIAL:LX/0rU;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    if-ne v0, v1, :cond_2

    .line 150579
    :cond_0
    const v1, 0xa0001

    const-string v2, "NNFColdStart"

    invoke-interface {p0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150580
    sget-object v0, LX/0rm;->B:LX/0Yj;

    .line 150581
    :goto_0
    return-object v0

    .line 150582
    :cond_1
    const v1, 0xa0014

    const-string v2, "NNFFreshContentStart"

    invoke-interface {p0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 150583
    sget-object v0, LX/0rm;->C:LX/0Yj;

    goto :goto_0

    .line 150584
    :cond_2
    sget-object v1, LX/2XC;->b:[I

    invoke-virtual {v0}, LX/0rU;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 150585
    sget-object v0, LX/0rm;->H:LX/0Yj;

    goto :goto_0

    .line 150586
    :pswitch_0
    sget-object v0, LX/0rm;->D:LX/0Yj;

    goto :goto_0

    .line 150587
    :pswitch_1
    sget-object v0, LX/0rm;->E:LX/0Yj;

    goto :goto_0

    .line 150588
    :pswitch_2
    sget-object v0, LX/0rm;->F:LX/0Yj;

    goto :goto_0

    .line 150589
    :pswitch_3
    sget-object v0, LX/0rm;->G:LX/0Yj;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;II)LX/0v6;
    .locals 9
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            "Ljava/lang/String;",
            "LX/0tW",
            "<",
            "Lcom/facebook/graphql/model/GraphQLViewer;",
            ">;II)",
            "LX/0v6;"
        }
    .end annotation

    .prologue
    .line 150590
    new-instance v1, LX/0v6;

    invoke-direct {v1, p2}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 150591
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    invoke-virtual {v0, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v0

    .line 150592
    iput p4, v0, LX/0rT;->c:I

    .line 150593
    move-object v0, v0

    .line 150594
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 150595
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p5, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 150596
    iget-object v3, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v3, v0, v2}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)Z

    .line 150597
    invoke-virtual {p0, v0}, LX/0rm;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v2

    .line 150598
    const-string v0, "param"

    const-string v3, "after_home_story_param"

    invoke-virtual {v2, v0, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150599
    const-string v0, "import"

    const-string v3, "end_cursor"

    invoke-virtual {v2, v0, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150600
    const-string v0, "max_runs"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150601
    const-string v3, "storyset_first_fetch_size"

    iget-object v0, p0, LX/0rm;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zM;

    .line 150602
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    .line 150603
    iget-boolean v5, v4, LX/0zN;->b:Z

    if-nez v5, :cond_0

    .line 150604
    iget-object v5, v4, LX/0zN;->a:LX/0W3;

    sget-wide v7, LX/0X5;->cO:J

    const/4 v6, 0x0

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JZ)Z

    move-result v5

    iput-boolean v5, v4, LX/0zN;->e:Z

    .line 150605
    const/4 v5, 0x1

    iput-boolean v5, v4, LX/0zN;->b:Z

    .line 150606
    :cond_0
    iget-boolean v5, v4, LX/0zN;->e:Z

    move v4, v5

    .line 150607
    if-eqz v4, :cond_5

    .line 150608
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    .line 150609
    iget v5, v4, LX/0zN;->f:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    .line 150610
    iget-object v5, v4, LX/0zN;->a:LX/0W3;

    sget-wide v7, LX/0X5;->cP:J

    invoke-virtual {v4}, LX/0zN;->a()I

    move-result v6

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JI)I

    move-result v5

    iput v5, v4, LX/0zN;->f:I

    .line 150611
    :cond_1
    iget v5, v4, LX/0zN;->f:I

    move v4, v5

    .line 150612
    :goto_0
    move v0, v4

    .line 150613
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150614
    const-string v3, "pyml_first_fetch_size"

    iget-object v0, p0, LX/0rm;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zM;

    .line 150615
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    .line 150616
    iget-boolean v5, v4, LX/0zN;->c:Z

    if-nez v5, :cond_2

    .line 150617
    iget-object v5, v4, LX/0zN;->a:LX/0W3;

    sget-wide v7, LX/0X5;->cQ:J

    const/4 v6, 0x0

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JZ)Z

    move-result v5

    iput-boolean v5, v4, LX/0zN;->g:Z

    .line 150618
    const/4 v5, 0x1

    iput-boolean v5, v4, LX/0zN;->c:Z

    .line 150619
    :cond_2
    iget-boolean v5, v4, LX/0zN;->g:Z

    move v4, v5

    .line 150620
    if-eqz v4, :cond_6

    .line 150621
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    .line 150622
    iget v5, v4, LX/0zN;->h:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3

    .line 150623
    iget-object v5, v4, LX/0zN;->a:LX/0W3;

    sget-wide v7, LX/0X5;->cR:J

    invoke-virtual {v4}, LX/0zN;->a()I

    move-result v6

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JI)I

    move-result v5

    iput v5, v4, LX/0zN;->h:I

    .line 150624
    :cond_3
    iget v5, v4, LX/0zN;->h:I

    move v4, v5

    .line 150625
    :goto_1
    move v0, v4

    .line 150626
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150627
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v2, 0x0

    .line 150628
    iput-boolean v2, v0, LX/0zO;->p:Z

    .line 150629
    move-object v2, v0

    .line 150630
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 150631
    if-eqz v0, :cond_4

    .line 150632
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 150633
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 150634
    :cond_4
    invoke-virtual {v1, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 150635
    const-string v3, "feed_subscriber"

    invoke-interface {p3, v3}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 150636
    iget-object v0, p0, LX/0rm;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 150637
    iput-boolean v0, v1, LX/0v6;->g:Z

    .line 150638
    invoke-virtual {p0, v1, v2, p3}, LX/0rn;->a(LX/0v6;LX/0zO;LX/0tW;)V

    .line 150639
    return-object v1

    :cond_5
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    invoke-virtual {v4}, LX/0zN;->a()I

    move-result v4

    goto/16 :goto_0

    :cond_6
    iget-object v4, v0, LX/0zM;->a:LX/0zN;

    invoke-virtual {v4}, LX/0zN;->a()I

    move-result v4

    goto :goto_1
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLViewer;Z)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 18

    .prologue
    .line 150640
    const/4 v6, 0x0

    .line 150641
    const/4 v5, 0x0

    .line 150642
    if-eqz p2, :cond_2

    .line 150643
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v2

    if-nez v2, :cond_0

    .line 150644
    const/4 v2, 0x0

    .line 150645
    :goto_0
    return-object v2

    .line 150646
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;->a()LX/0Px;

    move-result-object v4

    .line 150647
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->l()Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLDebugFeedConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    .line 150648
    const/4 v2, 0x0

    move-object v7, v5

    move v8, v6

    move-object v6, v4

    move-object v5, v3

    move-object v4, v2

    .line 150649
    :goto_1
    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    .line 150650
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0rn;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v12

    .line 150651
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v14

    const/4 v2, 0x0

    move v10, v2

    :goto_2
    if-ge v10, v14, :cond_6

    invoke-virtual {v6, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1pT;

    .line 150652
    invoke-interface {v2}, LX/1pT;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v9

    .line 150653
    if-eqz v9, :cond_7

    .line 150654
    invoke-static {v9, v12, v13}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 150655
    invoke-interface {v9}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 150656
    invoke-interface {v9}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v15

    .line 150657
    const/4 v3, 0x4

    invoke-virtual {v15, v3}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v3

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    move-object/from16 v0, v16

    if-eq v3, v0, :cond_1

    move-object v3, v2

    .line 150658
    check-cast v3, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v3}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v3

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-virtual {v15, v3, v0}, LX/15i;->g(II)I

    move-result v3

    .line 150659
    invoke-virtual {v15, v3}, LX/15i;->a(I)V

    :cond_1
    move-object v3, v9

    .line 150660
    :goto_3
    new-instance v9, LX/1u8;

    invoke-direct {v9}, LX/1u8;-><init>()V

    invoke-virtual {v9, v3}, LX/1u8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/1u8;->b(Ljava/lang/String;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->p()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/1u8;->d(Ljava/lang/String;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/1u8;->a(Ljava/lang/String;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->o()D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, LX/1u8;->a(D)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->a()Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/1u8;->a(Lcom/facebook/graphql/enums/GraphQLBumpReason;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/1u8;->c(Ljava/lang/String;)LX/1u8;

    move-result-object v3

    invoke-interface {v2}, LX/1pT;->l()Z

    move-result v2

    invoke-virtual {v3, v2}, LX/1u8;->a(Z)LX/1u8;

    move-result-object v2

    invoke-virtual {v2}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    .line 150661
    invoke-virtual {v11, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 150662
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto/16 :goto_2

    .line 150663
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    if-nez v2, :cond_3

    .line 150664
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 150665
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v4

    .line 150666
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->k()Z

    move-result v6

    .line 150667
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->m()Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;

    move-result-object v5

    .line 150668
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 150669
    if-nez v2, :cond_4

    .line 150670
    new-instance v2, Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;-><init>()V

    .line 150671
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->a()Ljava/lang/String;

    move-result-object v3

    move-object v7, v5

    move v8, v6

    move-object v6, v4

    move-object v5, v2

    move-object v4, v3

    goto/16 :goto_1

    .line 150672
    :cond_5
    invoke-interface {v2}, LX/1pT;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-static {v3}, LX/4XL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    goto/16 :goto_3

    .line 150673
    :cond_6
    new-instance v2, LX/0uq;

    invoke-direct {v2}, LX/0uq;-><init>()V

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0uq;->a(LX/0Px;)LX/0uq;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0uq;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/0uq;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/0uq;->a(Z)LX/0uq;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0uq;->a(Lcom/facebook/graphql/model/GraphQLPromotionUnitAtTop;)LX/0uq;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0uq;->a(Ljava/lang/String;)LX/0uq;

    move-result-object v2

    invoke-virtual {v2}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    goto/16 :goto_0

    :cond_7
    move-object v3, v9

    goto/16 :goto_3
.end method

.method private static a(LX/0rm;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 12

    .prologue
    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 150674
    const-string v1, "PRODUCTION"

    .line 150675
    iget-object v0, p0, LX/0rm;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 150676
    if-eqz v0, :cond_b

    .line 150677
    iget-boolean v2, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, v2

    .line 150678
    if-eqz v0, :cond_b

    const/4 v0, 0x0

    .line 150679
    iget-object v2, p0, LX/0rm;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/0rm;->t:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0eJ;->g:LX/0Tn;

    invoke-interface {v2, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 150680
    if-nez v0, :cond_b

    .line 150681
    const-string v0, "DEBUG"

    move-object v2, v0

    .line 150682
    :goto_0
    iget-object v0, p0, LX/0rm;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0w9;

    .line 150683
    invoke-static {p1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 150684
    iget-object v1, p0, LX/0rm;->r:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-static {p1, v1}, LX/0w9;->a(LX/0gW;LX/0dC;)LX/0gW;

    .line 150685
    invoke-virtual {v0, p1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 150686
    const-string v1, "cached_stories_range"

    invoke-virtual {v0, p1, p2, v1}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)LX/0gW;

    .line 150687
    invoke-virtual {v0, p1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 150688
    invoke-static {p1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 150689
    const/4 v1, 0x0

    .line 150690
    iget-object v3, v0, LX/0w9;->h:LX/0ad;

    sget-short v4, LX/0fe;->u:S

    invoke-interface {v3, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 150691
    const-string v4, "include_feedback_to_prefetch"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 150692
    const-string v4, "include_read_likelihood"

    if-nez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 150693
    invoke-virtual {v0, p1}, LX/0w9;->e(LX/0gW;)LX/0gW;

    .line 150694
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v1

    .line 150695
    iget-object v0, p0, LX/0rm;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sa;

    .line 150696
    const-string v3, "creative_low_img_size"

    invoke-virtual {v0}, LX/0sa;->B()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "creative_med_img_size"

    invoke-virtual {v0}, LX/0sa;->C()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "creative_high_img_size"

    invoke-virtual {v0}, LX/0sa;->D()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "pymk_size_param"

    invoke-virtual {v0}, LX/0sa;->f()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "saved_item_pic_width"

    invoke-virtual {v0}, LX/0sa;->i()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "saved_item_pic_height"

    invoke-virtual {v0}, LX/0sa;->j()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "pyml_size_param"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "pysf_size_param"

    .line 150697
    iget-object v5, v0, LX/0sa;->e:Landroid/content/res/Resources;

    const v8, 0x7f0b0931

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 150698
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v5, v5

    .line 150699
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "profile_pic_swipe_size_param"

    invoke-virtual {v0}, LX/0sa;->m()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gpymi_size_param"

    invoke-virtual {v0}, LX/0sa;->h()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gysc_member_profile_size"

    invoke-static {}, LX/0sa;->q()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gysj_size_param"

    invoke-virtual {v0}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gysj_facepile_size_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gysj_cover_photo_width_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit8 v5, v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "gysj_facepile_count_param"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "sgny_size_param"

    invoke-virtual {v0}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "sgny_facepile_size_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "sgny_cover_photo_width_param"

    invoke-static {}, LX/0sa;->p()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    mul-int/lit8 v5, v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "sgny_facepile_count_param"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "pdfy_size_param"

    invoke-virtual {v0}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "celebrations_profile_pic_size_param"

    invoke-static {}, LX/0sa;->r()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "multi_share_item_image_size_param"

    invoke-virtual {v0}, LX/0sa;->v()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "friends_locations_profile_pic_size_param"

    invoke-virtual {v0}, LX/0sa;->I()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "quick_promotion_image_size_param"

    invoke-virtual {v0}, LX/0sa;->J()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "quick_promotion_large_image_size_param"

    invoke-virtual {v0}, LX/0sa;->K()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "quick_promotion_branding_image_size_param"

    .line 150700
    iget-object v5, v0, LX/0sa;->e:Landroid/content/res/Resources;

    const v6, 0x7f0b095b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 150701
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v5, v5

    .line 150702
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "first_home_story_param"

    .line 150703
    iget v5, p2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v5, v5

    .line 150704
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "discovery_image_size"

    invoke-virtual {v0}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "debug_mode"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "ad_media_type"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->a()LX/0wF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "num_faceboxes_and_tags"

    .line 150705
    iget-object v4, v0, LX/0sa;->b:Ljava/lang/Integer;

    move-object v4, v4

    .line 150706
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "include_replies_in_total_count"

    iget-object v4, p0, LX/0rm;->d:LX/0ad;

    sget-short v5, LX/0wg;->i:S

    invoke-interface {v4, v5, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "greeting_card_image_size_large"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "greeting_card_image_size_medium"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->g()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "num_media_question_options"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "media_question_photo_size"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "enable_download"

    iget-object v4, p0, LX/0rm;->Q:LX/0tQ;

    invoke-virtual {v4}, LX/0tQ;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "enable_hd"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "goodwill_full_width_image"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->e()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "goodwill_small_accent_image"

    iget-object v4, p0, LX/0rm;->g:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "friends_birthday_profile_image_size"

    .line 150707
    invoke-static {}, LX/0wB;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v4, v4

    .line 150708
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "action_links_location"

    const-string v4, "homepage_stream"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "place_review_img_width"

    .line 150709
    iget-object v4, v0, LX/0sa;->e:Landroid/content/res/Resources;

    const v5, 0x7f0b0a2a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 150710
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v4, v4

    .line 150711
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "place_review_img_height"

    .line 150712
    iget-object v4, v0, LX/0sa;->e:Landroid/content/res/Resources;

    const v5, 0x7f0b0a2b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 150713
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v0, v4

    .line 150714
    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/0rm;->A:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "inline_comments_for_aggregated_stories"

    iget-object v3, p0, LX/0rm;->d:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short v6, LX/0wi;->a:S

    invoke-interface {v3, v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "skip_sample_entities_fields"

    iget-object v3, p0, LX/0rm;->d:LX/0ad;

    sget-short v4, LX/0fe;->a:S

    invoke-interface {v3, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "allow_pinned_dummy_stories"

    .line 150715
    iget-boolean v3, p2, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v3, v3

    .line 150716
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v2, "include_hot_conversation_info"

    iget-object v3, p0, LX/0rm;->d:LX/0ad;

    sget-short v4, LX/0fe;->A:S

    invoke-interface {v3, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "feed_story_render_location"

    const-string v3, "feed_mobile"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "inline_comments_interaction_likelihood"

    iget-object v3, p0, LX/0rm;->M:LX/0tM;

    invoke-virtual {v3}, LX/0tM;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "rich_text_posts_enabled"

    iget-object v3, p0, LX/0rm;->P:LX/0tO;

    invoke-virtual {v3}, LX/0tO;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "default_image_scale"

    if-nez v1, :cond_7

    sget-object v0, LX/0wB;->a:LX/0wC;

    :goto_1
    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "include_page_is_verified"

    iget-object v2, p0, LX/0rm;->e:LX/0Uh;

    const/16 v3, 0x579

    invoke-virtual {v2, v3, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "include_full_reply_parent"

    iget-object v2, p0, LX/0rm;->N:LX/0tN;

    invoke-virtual {v2}, LX/0tN;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "is_work_build"

    iget-boolean v2, p0, LX/0rm;->R:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "topic_following_enabled"

    iget-object v2, p0, LX/0rm;->U:LX/0tT;

    .line 150717
    iget-object v8, v2, LX/0tT;->a:LX/0W3;

    sget-wide v10, LX/0X5;->hx:J

    invoke-interface {v8, v10, v11}, LX/0W4;->b(J)Z

    move-result v8

    move v2, v8

    .line 150718
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 150719
    iget-object v0, p0, LX/0rm;->T:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150720
    iget-object v0, p0, LX/0rm;->S:LX/0tR;

    invoke-virtual {v0, p1}, LX/0tR;->a(LX/0gW;)V

    .line 150721
    :cond_2
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 150722
    iget-object v1, v0, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v1, v1

    .line 150723
    iget-object v2, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v2, v2

    .line 150724
    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v2, v3}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 150725
    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v0

    .line 150726
    :goto_2
    const-string v2, "orderby_home_story_param"

    invoke-virtual {p1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150727
    instance-of v0, v1, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    if-eqz v0, :cond_3

    .line 150728
    const-string v0, "feed_style"

    check-cast v1, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    .line 150729
    iget-object v2, v1, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->a:Ljava/lang/String;

    move-object v1, v2

    .line 150730
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150731
    :cond_3
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 150732
    if-nez v0, :cond_4

    .line 150733
    const-string v0, "refresh_mode_param"

    .line 150734
    iget-object v1, p2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v1, v1

    .line 150735
    invoke-virtual {v1}, LX/0gf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150736
    :cond_4
    iget-object v0, p0, LX/0rm;->L:LX/0tK;

    invoke-virtual {v0, v7}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 150737
    const-string v0, "data_savings_mode"

    const-string v1, "active"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150738
    :cond_5
    iget-object v0, p0, LX/0rm;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tI;

    invoke-virtual {v0, p1}, LX/0tI;->a(LX/0gW;)V

    .line 150739
    iget-object v0, p0, LX/0rm;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/0wo;->a(LX/0gW;)V

    .line 150740
    iget-object v0, p0, LX/0rm;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 150741
    const/4 v0, 0x1

    move v0, v0

    .line 150742
    if-eqz v0, :cond_a

    .line 150743
    const-string v0, "scrubbing"

    const-string v1, "MPEG_DASH"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150744
    :cond_6
    :goto_3
    iget-object v0, p0, LX/0rm;->K:LX/0sU;

    const-string v1, "NEWSFEED"

    invoke-virtual {v0, p1, v1}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 150745
    return-void

    :cond_7
    move-object v0, v1

    .line 150746
    goto/16 :goto_1

    .line 150747
    :cond_8
    instance-of v2, v1, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    if-eqz v2, :cond_9

    move-object v0, v1

    .line 150748
    check-cast v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    .line 150749
    iget-object v2, v0, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;->b:Ljava/lang/String;

    move-object v0, v2

    .line 150750
    goto :goto_2

    .line 150751
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 150752
    :cond_a
    iget-object v0, p0, LX/0rm;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wp;

    invoke-virtual {v0}, LX/0wp;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 150753
    const-string v0, "scrubbing"

    const-string v1, "WEBM_DASH"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_3

    :cond_b
    move-object v2, v1

    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 1

    .prologue
    .line 150754
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150755
    iget-object v0, p0, LX/0rm;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/325;

    invoke-virtual {v0, p1}, LX/325;->b(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 150756
    :goto_0
    return-void

    .line 150757
    :cond_0
    iget-object v0, p0, LX/0rm;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/325;

    invoke-virtual {v0, p1}, LX/325;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/0rm;
    .locals 44

    .prologue
    .line 150758
    new-instance v2, LX/0rm;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x1223

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v6

    check-cast v6, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/0rz;->a(LX/0QB;)LX/0rz;

    move-result-object v7

    check-cast v7, LX/0rz;

    const/16 v8, 0x6b3

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x292

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x24e

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x4cd

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v12

    check-cast v12, LX/0sO;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v14

    check-cast v14, LX/0SG;

    const/16 v15, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v16

    check-cast v16, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v17, 0x5e2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v18

    check-cast v18, LX/0So;

    invoke-static/range {p0 .. p0}, LX/0Yl;->a(LX/0QB;)LX/0Yl;

    move-result-object v19

    check-cast v19, LX/0Yl;

    const/16 v20, 0x49e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x149a

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {p0 .. p0}, LX/0pX;->a(LX/0QB;)LX/0pX;

    move-result-object v22

    check-cast v22, LX/0pX;

    const/16 v23, 0x6a1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v25

    check-cast v25, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v26, 0x122

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x222

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x6b6

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    invoke-static/range {p0 .. p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v29

    check-cast v29, LX/0oy;

    const/16 v30, 0x1499

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v31

    check-cast v31, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v32

    check-cast v32, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/0sZ;->a(LX/0QB;)LX/0sZ;

    move-result-object v33

    check-cast v33, LX/0sZ;

    invoke-static/range {p0 .. p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v34

    check-cast v34, LX/0tK;

    invoke-static/range {p0 .. p0}, LX/0tM;->a(LX/0QB;)LX/0tM;

    move-result-object v35

    check-cast v35, LX/0tM;

    invoke-static/range {p0 .. p0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v36

    check-cast v36, LX/0tN;

    invoke-static/range {p0 .. p0}, LX/0rW;->a(LX/0QB;)LX/0rW;

    move-result-object v37

    check-cast v37, LX/0rW;

    invoke-static/range {p0 .. p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v38

    check-cast v38, LX/0tO;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v39

    check-cast v39, LX/0tQ;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v40

    check-cast v40, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0tR;->a(LX/0QB;)LX/0tR;

    move-result-object v41

    check-cast v41, LX/0tR;

    invoke-static/range {p0 .. p0}, LX/0fO;->a(LX/0QB;)LX/0fO;

    move-result-object v42

    check-cast v42, LX/0fO;

    invoke-static/range {p0 .. p0}, LX/0tT;->a(LX/0QB;)LX/0tT;

    move-result-object v43

    check-cast v43, LX/0tT;

    invoke-direct/range {v2 .. v43}, LX/0rm;-><init>(LX/0ad;LX/0Uh;LX/0Ot;LX/0rq;LX/0rz;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0sO;LX/03V;LX/0SG;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0So;LX/0Yl;LX/0Ot;LX/0Or;LX/0pX;LX/0Ot;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0oy;LX/0Or;LX/0sU;LX/0sX;LX/0sZ;LX/0tK;LX/0tM;LX/0tN;LX/0rW;LX/0tO;LX/0tQ;Ljava/lang/Boolean;LX/0tR;LX/0fO;LX/0tT;)V

    .line 150759
    return-object v2
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 150760
    iget-object v0, p0, LX/0rm;->h:LX/0rz;

    .line 150761
    iget-object p0, v0, LX/0rz;->b:Ljava/lang/String;

    move-object v0, p0

    .line 150762
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static f(LX/0rm;Lcom/facebook/api/feed/FetchFeedParams;)Z
    .locals 2

    .prologue
    .line 150763
    iget-object v0, p0, LX/0rm;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-virtual {v0}, Lcom/facebook/debug/feed/DebugFeedConfig;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 150764
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v1, v1

    .line 150765
    invoke-virtual {v0, v1}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0rU;->HEAD:LX/0rU;

    .line 150766
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v1

    .line 150767
    if-ne v0, v1, :cond_0

    .line 150768
    const/4 v0, 0x1

    .line 150769
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/0rm;Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 150770
    iget-object v0, p0, LX/0rm;->x:LX/0oy;

    invoke-virtual {v0}, LX/0oy;->n()I

    move-result v0

    .line 150771
    iget-object v1, p0, LX/0rm;->x:LX/0oy;

    invoke-virtual {v1}, LX/0oy;->o()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 150772
    new-instance v2, LX/0rT;

    invoke-direct {v2}, LX/0rT;-><init>()V

    invoke-virtual {v2, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v2

    .line 150773
    iput v0, v2, LX/0rT;->c:I

    .line 150774
    move-object v0, v2

    .line 150775
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 150776
    invoke-static {}, LX/0vY;->a()LX/0w4;

    move-result-object v2

    .line 150777
    invoke-static {p0, v2, v0}, LX/0rm;->a(LX/0rm;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 150778
    const-string v0, "before_home_story_param"

    const-string v3, "CURSOR_BOOKMARK"

    invoke-virtual {v2, v0, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "client_query_id"

    const-string v4, "CLIENT_QUERY_ID_BOOKMARK"

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150779
    iget-object v0, v2, LX/0gW;->e:LX/0w7;

    move-object v0, v0

    .line 150780
    invoke-virtual {v0}, LX/0w7;->a()LX/0Zh;

    move-result-object v3

    invoke-virtual {v3}, LX/0Zh;->b()LX/0n9;

    move-result-object v3

    .line 150781
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0nA;->a(LX/0nD;)V

    .line 150782
    const-string v4, "param"

    const-string v5, "after_home_story_param"

    .line 150783
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150784
    const-string v4, "import"

    const-string v5, "end_cursor"

    .line 150785
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150786
    const-string v4, "max_runs"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 150787
    invoke-static {v3, v4, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150788
    invoke-virtual {v0}, LX/0w7;->a()LX/0Zh;

    move-result-object v1

    invoke-virtual {v1}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 150789
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0nA;->a(LX/0nD;)V

    .line 150790
    const-string v4, "query_id"

    .line 150791
    iget-object v5, v2, LX/0gW;->h:Ljava/lang/String;

    move-object v5, v5

    .line 150792
    invoke-static {v1, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150793
    const-string v4, "query_params"

    invoke-virtual {v0}, LX/0w7;->d()LX/0n9;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 150794
    const-string v4, "rerun_param"

    invoke-virtual {v1, v4, v3}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 150795
    invoke-virtual {v0}, LX/0w7;->a()LX/0Zh;

    move-result-object v3

    invoke-virtual {v3}, LX/0Zh;->b()LX/0n9;

    move-result-object v3

    .line 150796
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0nA;->a(LX/0nD;)V

    .line 150797
    iget-object v4, v2, LX/0gW;->f:Ljava/lang/String;

    move-object v2, v4

    .line 150798
    invoke-virtual {v3, v2, v1}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 150799
    invoke-virtual {v0}, LX/0w7;->a()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v1

    .line 150800
    const-string v0, "queries"

    invoke-virtual {v1, v0, v3}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 150801
    const-string v0, "client_query_id"

    const-string v2, "CLIENT_QUERY_ID_BOOKMARK"

    .line 150802
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150803
    const-string v0, "batch_name"

    const-string v2, "udp"

    .line 150804
    invoke-static {v1, v0, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150805
    iget-object v0, p0, LX/0rm;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150806
    sget-object v0, LX/0sO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/message/BasicNameValuePair;

    .line 150807
    invoke-virtual {v0}, Lorg/apache/http/message/BasicNameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/http/message/BasicNameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 150808
    invoke-static {v1, v3, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150809
    goto :goto_0

    .line 150810
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 150811
    invoke-static {}, LX/10F;->a()LX/10F;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/10F;->a(Ljava/io/Writer;LX/0nA;)V

    .line 150812
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 150813
    :catch_0
    move-exception v0

    .line 150814
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 150397
    sget-object v1, LX/00a;->a:LX/00a;

    move-object v3, v1

    .line 150398
    invoke-static {p0, p1}, LX/0rm;->f(LX/0rm;Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v3}, LX/00a;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 150399
    iget-object v1, v3, LX/00a;->d:Ljava/lang/String;

    move-object v1, v1

    .line 150400
    const/4 v5, 0x1

    .line 150401
    const/4 v6, 0x0

    .line 150402
    iget-object v4, p0, LX/0rm;->y:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 150403
    if-eqz v1, :cond_12

    const-string v4, "response_format=flatbuffer"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "flatbuffer_schema_id=10155272039501729"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    move v4, v5

    .line 150404
    :goto_0
    move v1, v4

    .line 150405
    if-eqz v1, :cond_7

    .line 150406
    iget-object v1, v3, LX/00a;->i:Ljava/lang/String;

    move-object v1, v1

    .line 150407
    invoke-direct {p0, v1}, LX/0rm;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v0

    .line 150408
    :goto_1
    if-eqz v1, :cond_8

    .line 150409
    new-instance v1, LX/0rT;

    invoke-direct {v1}, LX/0rT;-><init>()V

    invoke-virtual {v1, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v1

    .line 150410
    iget-object v4, v3, LX/00a;->k:Ljava/lang/String;

    move-object v3, v4

    .line 150411
    iput-object v3, v1, LX/0rT;->h:Ljava/lang/String;

    .line 150412
    move-object v1, v1

    .line 150413
    invoke-virtual {v1}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object p1

    move-object v1, p1

    .line 150414
    :goto_2
    iget-object v3, p0, LX/0rm;->d:LX/0ad;

    sget-char v4, LX/0fe;->au:C

    const-string v5, ""

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 150415
    iget-object v4, p0, LX/0rm;->I:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 150416
    iget-object v4, p0, LX/0rm;->J:[I

    .line 150417
    :goto_3
    move-object v3, v4

    .line 150418
    iget-object v4, v1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v4, v4

    .line 150419
    sget-object v5, LX/0rU;->HEAD:LX/0rU;

    if-ne v4, v5, :cond_a

    .line 150420
    :goto_4
    if-eqz v3, :cond_0

    if-nez v0, :cond_e

    .line 150421
    :cond_0
    if-eqz v0, :cond_b

    iget-object v2, p0, LX/0rm;->x:LX/0oy;

    invoke-virtual {v2}, LX/0oy;->n()I

    move-result v4

    .line 150422
    :goto_5
    if-eqz v0, :cond_c

    iget-object v0, p0, LX/0rm;->x:LX/0oy;

    invoke-virtual {v0}, LX/0oy;->o()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    :goto_6
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    .line 150423
    invoke-direct/range {v0 .. v5}, LX/0rm;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;II)LX/0v6;

    move-result-object v0

    .line 150424
    :goto_7
    const/4 v5, 0x1

    .line 150425
    invoke-static {p0, v1}, LX/0rm;->f(LX/0rm;Lcom/facebook/api/feed/FetchFeedParams;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 150426
    iput-boolean v5, v0, LX/0v6;->i:Z

    .line 150427
    iget-object v2, p0, LX/0rm;->h:LX/0rz;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150428
    sget-object v6, LX/00a;->a:LX/00a;

    move-object v6, v6

    .line 150429
    invoke-static {v2}, LX/0rz;->e(LX/0rz;)Z

    move-result v7

    invoke-static {v2}, LX/0rz;->f(LX/0rz;)Z

    move-result v8

    .line 150430
    iget-boolean p1, v6, LX/00a;->l:Z

    if-ne p1, v7, :cond_1

    iget-boolean p1, v6, LX/00a;->m:Z

    if-eq p1, v8, :cond_17

    :cond_1
    const/4 p1, 0x1

    .line 150431
    :goto_8
    move v7, p1

    .line 150432
    invoke-virtual {v2}, LX/0rz;->a()Z

    move-result v8

    if-eqz v8, :cond_16

    const/4 v8, 0x1

    const/4 p1, 0x0

    .line 150433
    iget-boolean v2, v6, LX/00a;->n:Z

    if-eqz v2, :cond_18

    iget-object v2, v6, LX/00a;->d:Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v2, v6, LX/00a;->i:Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v2, v6, LX/00a;->j:Ljava/lang/String;

    if-eqz v2, :cond_18

    move v2, v8

    .line 150434
    :goto_9
    if-nez v2, :cond_19

    :goto_a
    move v6, v8

    .line 150435
    if-eqz v6, :cond_16

    move v6, v4

    .line 150436
    :goto_b
    if-nez v7, :cond_2

    if-eqz v6, :cond_3

    :cond_2
    move v3, v4

    :cond_3
    move v2, v3

    .line 150437
    if-eqz v2, :cond_4

    .line 150438
    iget-object v2, p0, LX/0rm;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 150439
    if-eqz v2, :cond_4

    .line 150440
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 150441
    invoke-static {p0, v1}, LX/0rm;->g(LX/0rm;Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;

    move-result-object v3

    .line 150442
    iget-object v4, p0, LX/0rm;->h:LX/0rz;

    .line 150443
    if-eqz v3, :cond_4

    if-nez v2, :cond_1a

    .line 150444
    :cond_4
    :goto_c
    sget-object v2, LX/00a;->a:LX/00a;

    move-object v2, v2

    .line 150445
    iget-object v3, v2, LX/00a;->o:LX/00b;

    move-object v3, v3

    .line 150446
    sget-object v4, LX/00b;->NONE:LX/00b;

    if-eq v3, v4, :cond_6

    .line 150447
    iget-object v4, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v4, v1, v3}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/00b;)Z

    .line 150448
    invoke-virtual {v2}, LX/00a;->e()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 150449
    iput-boolean v5, v0, LX/0v6;->h:Z

    .line 150450
    :cond_5
    sget-object v3, LX/00b;->NONE:LX/00b;

    .line 150451
    iput-object v3, v2, LX/00a;->o:LX/00b;

    .line 150452
    :cond_6
    return-object v0

    :cond_7
    move v1, v2

    .line 150453
    goto/16 :goto_1

    .line 150454
    :cond_8
    sget-object v1, LX/00b;->ERROR_MISMATCH:LX/00b;

    .line 150455
    iput-object v1, v3, LX/00a;->o:LX/00b;

    .line 150456
    :cond_9
    move-object v1, p1

    goto/16 :goto_2

    :cond_a
    move v0, v2

    .line 150457
    goto/16 :goto_4

    .line 150458
    :cond_b
    iget-object v2, p0, LX/0rm;->x:LX/0oy;

    invoke-virtual {v2}, LX/0oy;->p()I

    move-result v4

    goto/16 :goto_5

    .line 150459
    :cond_c
    iget-object v0, p0, LX/0rm;->x:LX/0oy;

    .line 150460
    iget-boolean v6, v0, LX/0oy;->C:Z

    if-eqz v6, :cond_1b

    .line 150461
    :try_start_0
    iget-object v6, v0, LX/0oy;->B:Lorg/json/JSONObject;

    if-nez v6, :cond_d

    .line 150462
    sget-wide v6, LX/0X5;->gB:J

    invoke-static {v0, v6, v7}, LX/0oy;->a(LX/0oy;J)Lorg/json/JSONObject;

    move-result-object v6

    iput-object v6, v0, LX/0oy;->B:Lorg/json/JSONObject;

    .line 150463
    :cond_d
    iget-object v6, v0, LX/0oy;->B:Lorg/json/JSONObject;

    invoke-static {v0, v6}, LX/0oy;->a(LX/0oy;Lorg/json/JSONObject;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    .line 150464
    :goto_d
    move v0, v6

    .line 150465
    add-int/lit8 v5, v0, -0x1

    goto/16 :goto_6

    .line 150466
    :cond_e
    new-instance v4, LX/0v6;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_batch"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 150467
    const/4 v0, 0x0

    aget v0, v3, v0

    .line 150468
    new-instance v2, LX/0rT;

    invoke-direct {v2}, LX/0rT;-><init>()V

    invoke-virtual {v2, v1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v2

    .line 150469
    iput v0, v2, LX/0rT;->c:I

    .line 150470
    move-object v0, v2

    .line 150471
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 150472
    iget-object v2, p0, LX/0rm;->q:LX/0pX;

    iget-object v5, p0, LX/0rm;->I:Ljava/lang/String;

    invoke-virtual {v2, v0, v5}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)Z

    .line 150473
    invoke-virtual {p0, v0}, LX/0rm;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v0

    .line 150474
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 150475
    invoke-virtual {v4, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 150476
    const-string v5, "feed_subscriber"

    invoke-interface {p3, v5}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 150477
    invoke-virtual {p0, v4, v2, p3}, LX/0rn;->a(LX/0v6;LX/0zO;LX/0tW;)V

    .line 150478
    const/4 v0, 0x1

    :goto_e
    array-length v5, v3

    if-ge v0, v5, :cond_10

    .line 150479
    aget v5, v3, v0

    .line 150480
    new-instance v6, LX/0rT;

    invoke-direct {v6}, LX/0rT;-><init>()V

    invoke-virtual {v6, v1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v6

    .line 150481
    iput v5, v6, LX/0rT;->c:I

    .line 150482
    move-object v5, v6

    .line 150483
    invoke-virtual {v5}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v5

    .line 150484
    invoke-virtual {p0, v5}, LX/0rm;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v5

    .line 150485
    const-string v6, "after_home_story_param"

    const-string v7, "end_cursor"

    sget-object v8, LX/4Zz;->FIRST:LX/4Zz;

    sget-object p1, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {v2, v7, v8, p1}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 150486
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 150487
    iget-object v5, v1, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v5, v5

    .line 150488
    if-eqz v5, :cond_f

    .line 150489
    iget-object v5, v1, Lcom/facebook/api/feed/FetchFeedParams;->q:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v5, v5

    .line 150490
    invoke-virtual {v2, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 150491
    :cond_f
    invoke-virtual {v4, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    .line 150492
    const-string v6, "feed_subscriber"

    invoke-interface {p3, v6}, LX/0tW;->a(Ljava/lang/String;)LX/0rl;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 150493
    invoke-virtual {p0, v4, v2, p3}, LX/0rn;->a(LX/0v6;LX/0zO;LX/0tW;)V

    .line 150494
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 150495
    :cond_10
    iget-object v0, p0, LX/0rm;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 150496
    iput-boolean v0, v4, LX/0v6;->g:Z

    .line 150497
    move-object v0, v4

    .line 150498
    goto/16 :goto_7

    .line 150499
    :cond_11
    if-eqz v1, :cond_12

    const-string v4, "response_format=flatbuffer"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_12

    move v4, v5

    .line 150500
    goto/16 :goto_0

    :cond_12
    move v4, v6

    goto/16 :goto_0

    .line 150501
    :cond_13
    iput-object v3, p0, LX/0rm;->I:Ljava/lang/String;

    .line 150502
    const/4 v5, 0x0

    .line 150503
    :try_start_1
    const-string v4, "\\+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 150504
    array-length v8, v7

    .line 150505
    if-lez v8, :cond_15

    .line 150506
    new-array v4, v8, [I

    .line 150507
    const/4 v6, 0x0

    :goto_f
    if-ge v6, v8, :cond_14

    .line 150508
    aget-object p1, v7, v6

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    aput p1, v4, v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 150509
    add-int/lit8 v6, v6, 0x1

    goto :goto_f

    .line 150510
    :catch_0
    move-object v4, v5

    .line 150511
    :cond_14
    :goto_10
    move-object v4, v4

    .line 150512
    iput-object v4, p0, LX/0rm;->J:[I

    .line 150513
    iget-object v4, p0, LX/0rm;->J:[I

    goto/16 :goto_3

    :cond_15
    move-object v4, v5

    goto :goto_10

    :cond_16
    move v6, v3

    .line 150514
    goto/16 :goto_b

    .line 150515
    :cond_17
    const/4 p1, 0x0

    goto/16 :goto_8

    :cond_18
    move v2, p1

    .line 150516
    goto/16 :goto_9

    :cond_19
    move v8, p1

    .line 150517
    goto/16 :goto_a

    .line 150518
    :cond_1a
    iget-object v6, v4, LX/0rz;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/http/protocol/UDPPrimingHelper$2;

    invoke-direct {v7, v4, v3, v2}, Lcom/facebook/http/protocol/UDPPrimingHelper$2;-><init>(LX/0rz;Ljava/lang/String;Ljava/lang/String;)V

    const v8, 0x2cccdb48

    invoke-static {v6, v7, v8}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_c

    .line 150519
    :catch_1
    const/4 v6, 0x0

    iput-boolean v6, v0, LX/0oy;->C:Z

    .line 150520
    invoke-virtual {v0}, LX/0oy;->o()I

    move-result v6

    goto/16 :goto_d

    .line 150521
    :cond_1b
    invoke-virtual {v0}, LX/0oy;->o()I

    move-result v6

    goto/16 :goto_d
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 3

    .prologue
    .line 150522
    invoke-super {p0, p1, p2, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 150523
    sget-object v1, LX/2XC;->a:[I

    invoke-static {p1}, LX/2XD;->getQueryType(Lcom/facebook/api/feed/FetchFeedParams;)LX/2XD;

    move-result-object v2

    invoke-virtual {v2}, LX/2XD;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 150524
    :goto_0
    return-object v0

    .line 150525
    :pswitch_0
    invoke-direct {p0, v0}, LX/0rm;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 150526
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150527
    iget-object v1, p0, LX/0rm;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/325;

    invoke-virtual {v1, v0}, LX/325;->d(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 150528
    :goto_1
    goto :goto_0

    .line 150529
    :pswitch_2
    invoke-direct {p0, v0}, LX/0rm;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_0

    .line 150530
    :cond_0
    iget-object v1, p0, LX/0rm;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/325;

    invoke-virtual {v1, v0}, LX/325;->c(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;
    .locals 3

    .prologue
    .line 150393
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 150394
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLViewer;

    if-eqz v1, :cond_0

    .line 150395
    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    iget-object v1, p0, LX/0rm;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-virtual {v1}, Lcom/facebook/debug/feed/DebugFeedConfig;->a()Z

    move-result v1

    invoke-direct {p0, v0, v1}, LX/0rm;->a(Lcom/facebook/graphql/model/GraphQLViewer;Z)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    return-object v0

    .line 150396
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized GraphQLResult:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150381
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    .line 150382
    invoke-static {p2}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    .line 150383
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    if-ne v1, v2, :cond_3

    .line 150384
    :cond_0
    instance-of v1, p2, LX/4cm;

    if-eqz v1, :cond_2

    .line 150385
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v1

    .line 150386
    sget-object v2, LX/0rU;->CHUNKED_REMAINDER:LX/0rU;

    if-ne v1, v2, :cond_1

    .line 150387
    iget-object v1, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v1, p1}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;)Z

    .line 150388
    :cond_1
    :goto_0
    return-object v0

    .line 150389
    :cond_2
    iget-object v1, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->b(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 150390
    :cond_3
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_4

    .line 150391
    iget-object v1, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 150392
    :cond_4
    iget-object v1, p0, LX/0rm;->q:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->c(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150380
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 150373
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 150374
    :try_start_0
    invoke-super {p0, p1, p2}, LX/0rn;->a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 150375
    :catch_0
    move-exception v0

    .line 150376
    iget-object v1, p0, LX/0rm;->q:LX/0pX;

    .line 150377
    iget-boolean v2, p2, LX/1pN;->f:Z

    move v2, v2

    .line 150378
    invoke-virtual {v1, p1, v2, v0}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;ZLjava/lang/Exception;)Z

    .line 150379
    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150372
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 1

    .prologue
    .line 150369
    invoke-super {p0, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 150370
    iget-object v0, p0, LX/0rm;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yi;

    invoke-virtual {v0, p1}, LX/0Yi;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 150371
    return-void
.end method

.method public final synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 150368
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 150367
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150366
    const-string v0, "fetch_news_feed"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150363
    iget-object v0, p0, LX/0rm;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0, p1}, LX/0rm;->a(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/api/feed/FetchFeedParams;)LX/0Yj;

    move-result-object v0

    .line 150364
    iget-object p0, v0, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, p0

    .line 150365
    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 150360
    iget-object v0, p0, LX/0rm;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0, p1}, LX/0rm;->a(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/api/feed/FetchFeedParams;)LX/0Yj;

    move-result-object v0

    .line 150361
    iget p0, v0, LX/0Yj;->a:I

    move v0, p0

    .line 150362
    return v0
.end method

.method public final e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feed/FetchFeedParams;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/graphql/model/GraphQLViewer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150325
    iget-object v0, p0, LX/0rm;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-virtual {v0}, Lcom/facebook/debug/feed/DebugFeedConfig;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150326
    new-instance v0, LX/2XE;

    invoke-direct {v0}, LX/2XE;-><init>()V

    move-object v0, v0

    .line 150327
    :goto_0
    const/4 v1, 0x1

    .line 150328
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 150329
    invoke-static {p0, v0, p1}, LX/0rm;->a(LX/0rm;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 150330
    iget-object v2, p0, LX/0rm;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0w9;

    .line 150331
    iget-object v3, p0, LX/0rm;->n:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0u7;

    invoke-static {v0, v3}, LX/0w9;->a(LX/0gW;LX/0u7;)LX/0gW;

    .line 150332
    iget-object v3, p0, LX/0rm;->o:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-static {v0, v3}, LX/0w9;->a(LX/0gW;LX/0oz;)LX/0gW;

    .line 150333
    const-string v3, "before_home_story_param"

    const-string v4, "after_home_story_param"

    invoke-static {v0, p1, v3, v4}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150334
    iget-object v3, p0, LX/0rm;->z:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150335
    iget-object v8, v2, LX/0w9;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0Uo;

    invoke-virtual {v8}, LX/0Uo;->l()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 150336
    iget-object v8, v2, LX/0w9;->f:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0y2;

    invoke-virtual {v8}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v9

    .line 150337
    if-eqz v9, :cond_0

    .line 150338
    const-string v8, "location_latitude"

    invoke-virtual {v9}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150339
    const-string v8, "location_longitude"

    invoke-virtual {v9}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150340
    const-string v10, "location_accuracy_meters"

    invoke-virtual {v9}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v8

    invoke-virtual {v8}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v0, v10, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150341
    const-string v10, "location_age_seconds"

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, v2, LX/0w9;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0yD;

    invoke-virtual {v8, v9}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v8

    invoke-virtual {v11, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v10, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 150342
    :cond_0
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v2

    .line 150343
    sget-object v3, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    if-ne v2, v3, :cond_1

    .line 150344
    new-instance v3, LX/4Iw;

    invoke-direct {v3}, LX/4Iw;-><init>()V

    .line 150345
    const-string v4, "object_id"

    invoke-virtual {v2, v4}, LX/0gf;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 150346
    const-string v5, "o"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150347
    const-string v4, "href"

    invoke-virtual {v2, v4}, LX/0gf;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 150348
    const-string v5, "href"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150349
    const-string v4, "user_id"

    invoke-virtual {v2, v4}, LX/0gf;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 150350
    const-string v5, "uid"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150351
    const-string v4, "type"

    invoke-virtual {v2, v4}, LX/0gf;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150352
    const-string v4, "type"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150353
    const-string v2, "push_notification"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 150354
    :cond_1
    const-string v2, "client_query_id"

    .line 150355
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 150356
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 150357
    iget-object v2, p0, LX/0rm;->O:LX/0rW;

    const-string v4, "recent_vpvs"

    const-string v5, "recent_vpvs_v2"

    const-string v6, "recent_vpvs_v2_enabled"

    move-object v3, v0

    move-object v7, p1

    invoke-virtual/range {v2 .. v7}, LX/0rW;->a(LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 150358
    return-object v0

    .line 150359
    :cond_2
    invoke-static {}, LX/0vY;->a()LX/0w4;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)LX/0gW;
    .locals 1

    .prologue
    .line 150324
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rm;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
