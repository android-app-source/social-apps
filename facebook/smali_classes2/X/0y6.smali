.class public LX/0y6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/0y6;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0yn;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0y8;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/0yn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164092
    iput-object p1, p0, LX/0y6;->a:Ljava/util/Set;

    .line 164093
    new-instance v0, LX/0y8;

    invoke-direct {v0}, LX/0y8;-><init>()V

    iput-object v0, p0, LX/0y6;->b:LX/0y8;

    .line 164094
    return-void
.end method

.method public static a(LX/0QB;)LX/0y6;
    .locals 6

    .prologue
    .line 164095
    sget-object v0, LX/0y6;->e:LX/0y6;

    if-nez v0, :cond_1

    .line 164096
    const-class v1, LX/0y6;

    monitor-enter v1

    .line 164097
    :try_start_0
    sget-object v0, LX/0y6;->e:LX/0y6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164098
    if-eqz v2, :cond_0

    .line 164099
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164100
    new-instance v3, LX/0y6;

    .line 164101
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/0y7;

    invoke-direct {p0, v0}, LX/0y7;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 164102
    invoke-direct {v3, v4}, LX/0y6;-><init>(Ljava/util/Set;)V

    .line 164103
    move-object v0, v3

    .line 164104
    sput-object v0, LX/0y6;->e:LX/0y6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164105
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164106
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164107
    :cond_1
    sget-object v0, LX/0y6;->e:LX/0y6;

    return-object v0

    .line 164108
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164109
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
