.class public LX/1KB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1KB;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Cz;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "LX/1Cz;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:LX/0Sh;

.field public final e:LX/03V;

.field public f:LX/0Ot;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1TE;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;Landroid/content/Context;LX/0Sh;LX/03V;Ljava/lang/Boolean;)V
    .locals 1
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsInternalBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1TE;",
            ">;>;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 231139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1KB;->a:Ljava/util/List;

    .line 231141
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/1KB;->b:LX/01J;

    .line 231142
    iput-object p1, p0, LX/1KB;->f:LX/0Ot;

    .line 231143
    iput-object p2, p0, LX/1KB;->c:Landroid/content/Context;

    .line 231144
    iput-object p3, p0, LX/1KB;->d:LX/0Sh;

    .line 231145
    iput-object p4, p0, LX/1KB;->e:LX/03V;

    .line 231146
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231147
    invoke-static {p0}, LX/1KB;->b(LX/1KB;)V

    .line 231148
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/1KB;
    .locals 9

    .prologue
    .line 231107
    sget-object v0, LX/1KB;->g:LX/1KB;

    if-nez v0, :cond_1

    .line 231108
    const-class v1, LX/1KB;

    monitor-enter v1

    .line 231109
    :try_start_0
    sget-object v0, LX/1KB;->g:LX/1KB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 231110
    if-eqz v2, :cond_0

    .line 231111
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 231112
    new-instance v3, LX/1KB;

    .line 231113
    new-instance v4, LX/1KC;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-direct {v4, v5}, LX/1KC;-><init>(LX/0QB;)V

    move-object v4, v4

    .line 231114
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v4

    move-object v4, v4

    .line 231115
    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0XR;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v8}, LX/1KB;-><init>(LX/0Ot;Landroid/content/Context;LX/0Sh;LX/03V;Ljava/lang/Boolean;)V

    .line 231116
    move-object v0, v3

    .line 231117
    sput-object v0, LX/1KB;->g:LX/1KB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 231119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 231120
    :cond_1
    sget-object v0, LX/1KB;->g:LX/1KB;

    return-object v0

    .line 231121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 231122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1KB;)V
    .locals 3

    .prologue
    .line 231132
    iget-object v0, p0, LX/1KB;->f:LX/0Ot;

    if-nez v0, :cond_0

    .line 231133
    :goto_0
    return-void

    .line 231134
    :cond_0
    iget-object v0, p0, LX/1KB;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 231135
    iget-object v0, p0, LX/1KB;->e:LX/03V;

    const-string v1, "BG_ROWTYPE_REGISTRATION"

    const-string v2, "registerAllViewTypes() called from outside the UI thread"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 231136
    :cond_1
    iget-object v0, p0, LX/1KB;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1TE;

    .line 231137
    invoke-interface {v0, p0}, LX/1TE;->a(LX/1KB;)V

    goto :goto_1

    .line 231138
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/1KB;->f:LX/0Ot;

    goto :goto_0
.end method

.method public static c(LX/1KB;LX/1Cz;)I
    .locals 3

    .prologue
    .line 231127
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231128
    iget-object v0, p0, LX/1KB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 231129
    iget-object v1, p0, LX/1KB;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231130
    iget-object v1, p0, LX/1KB;->b:LX/01J;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231131
    return v0
.end method


# virtual methods
.method public final a(I)LX/1Cz;
    .locals 1

    .prologue
    .line 231126
    iget-object v0, p0, LX/1KB;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1Cz;)V
    .locals 1

    .prologue
    .line 231123
    iget-object v0, p0, LX/1KB;->b:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231124
    :goto_0
    return-void

    .line 231125
    :cond_0
    invoke-static {p0, p1}, LX/1KB;->c(LX/1KB;LX/1Cz;)I

    goto :goto_0
.end method
