.class public abstract LX/1Cz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 217267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217268
    return-void
.end method

.method public static a(I)LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(I)",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 217269
    new-instance v0, LX/1Un;

    invoke-direct {v0, p0}, LX/1Un;-><init>(I)V

    return-object v0
.end method

.method public static c(ILandroid/content/Context;)Landroid/util/AttributeSet;
    .locals 2

    .prologue
    .line 217270
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    .line 217271
    :cond_0
    :try_start_0
    const/4 v1, 0x1

    .line 217272
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result p0

    .line 217273
    const/4 p1, 0x2

    if-eq p0, p1, :cond_1

    if-eq p0, v1, :cond_1

    :goto_0
    move v1, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 217274
    if-nez v1, :cond_0

    .line 217275
    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    :goto_1
    return-object v0

    .line 217276
    :catch_0
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    :catch_1
    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TV;"
        }
    .end annotation
.end method
