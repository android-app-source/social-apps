.class public LX/1MO;
.super LX/0te;
.source ""


# static fields
.field private static final TAG:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public mBlueServiceLogicLazy:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbservice/service/BlueServiceLogic;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235223
    const-class v0, LX/1MO;

    sput-object v0, LX/1MO;->TAG:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 235222
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method public static STATICDI_COMPONENT$injectImpl(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/1MO;

    const/16 v1, 0x5c0

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p1, LX/1MO;->mBlueServiceLogicLazy:LX/0Ot;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 235221
    iget-object v0, p0, LX/1MO;->mBlueServiceLogicLazy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/BlueServiceLogic;

    invoke-virtual {v0}, LX/1mK;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onFbCreate()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x24530280

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 235224
    const-string v0, "BlueService.onCreate"

    const v2, -0x1eeb8cc9

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 235225
    :try_start_0
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 235226
    const-class v0, LX/1MO;

    invoke-static {v0, p0, p0}, LX/1MO;->STATICDI_COMPONENT$injectImpl(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235227
    const v0, 0x2c18784c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 235228
    const v0, -0x452f513d

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 235229
    :catchall_0
    move-exception v0

    const v2, -0x1459ba39

    invoke-static {v2}, LX/02m;->a(I)V

    const v2, 0x30fa2a2

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0
.end method

.method public onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x7320a4a4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 235218
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 235219
    iget-object v0, p0, LX/1MO;->mBlueServiceLogicLazy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/BlueServiceLogic;

    invoke-virtual {v0}, Lcom/facebook/fbservice/service/BlueServiceLogic;->stopQueues()V

    .line 235220
    const/16 v0, 0x25

    const v2, -0x4dbf04

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x13c1ab3e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 235205
    if-nez p1, :cond_0

    .line 235206
    const/16 v0, 0x25

    const v2, -0x57774ede

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 235207
    :goto_0
    return v3

    .line 235208
    :cond_0
    monitor-enter p0

    .line 235209
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 235210
    const-string v2, "Orca.START"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 235211
    const-string v2, "Orca.STOP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235212
    invoke-virtual {p0, p3}, LX/1MO;->stopSelf(I)V

    .line 235213
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235214
    const v0, -0x3f4652d1

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 235215
    :cond_2
    :try_start_1
    const-string v2, "Orca.DRAIN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235216
    iget-object v0, p0, LX/1MO;->mBlueServiceLogicLazy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/BlueServiceLogic;

    invoke-virtual {v0}, Lcom/facebook/fbservice/service/BlueServiceLogic;->drainQueue()V

    goto :goto_1

    .line 235217
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, 0x421cb151

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x4349b835

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 235203
    invoke-super {p0, p1}, LX/0te;->onRebind(Landroid/content/Intent;)V

    .line 235204
    const/16 v1, 0x25

    const v2, -0x7b14bd2f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
