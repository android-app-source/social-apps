.class public LX/0er;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/0er;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 104242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104243
    return-void
.end method

.method public static a(LX/0QB;)LX/0er;
    .locals 3

    .prologue
    .line 104244
    sget-object v0, LX/0er;->a:LX/0er;

    if-nez v0, :cond_1

    .line 104245
    const-class v1, LX/0er;

    monitor-enter v1

    .line 104246
    :try_start_0
    sget-object v0, LX/0er;->a:LX/0er;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 104247
    if-eqz v2, :cond_0

    .line 104248
    :try_start_1
    new-instance v0, LX/0er;

    invoke-direct {v0}, LX/0er;-><init>()V

    .line 104249
    move-object v0, v0

    .line 104250
    sput-object v0, LX/0er;->a:LX/0er;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104251
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 104252
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104253
    :cond_1
    sget-object v0, LX/0er;->a:LX/0er;

    return-object v0

    .line 104254
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 104255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/io/InputStream;)LX/0lA;
    .locals 5

    .prologue
    .line 104256
    new-instance v0, LX/0hU;

    invoke-static {p0}, LX/0hW;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 104257
    new-instance v2, LX/0l9;

    invoke-direct {v2}, LX/0l9;-><init>()V

    .line 104258
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v3, v4

    .line 104259
    iput v3, v2, LX/0l9;->a:I

    .line 104260
    iput-object v1, v2, LX/0l9;->b:Ljava/nio/ByteBuffer;

    .line 104261
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v2, v4}, LX/0eW;->b(I)I

    move-result v4

    iput v4, v2, LX/0l9;->c:I

    .line 104262
    const/16 v4, 0xe

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v2, v4}, LX/0eW;->b(I)I

    move-result v4

    iput v4, v2, LX/0l9;->d:I

    .line 104263
    const/16 v4, 0x14

    invoke-virtual {v2, v4}, LX/0eW;->a(I)I

    move-result v4

    iget p0, v2, LX/0eW;->a:I

    add-int/2addr v4, p0

    invoke-virtual {v2, v4}, LX/0eW;->b(I)I

    move-result v4

    iput v4, v2, LX/0l9;->e:I

    .line 104264
    move-object v3, v2

    .line 104265
    move-object v2, v3

    .line 104266
    move-object v1, v2

    .line 104267
    invoke-direct {v0, v1}, LX/0hU;-><init>(LX/0l9;)V

    .line 104268
    new-instance v1, LX/0lA;

    invoke-direct {v1, v0}, LX/0lA;-><init>(LX/0hV;)V

    return-object v1
.end method
