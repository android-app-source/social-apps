.class public final LX/1O4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public final synthetic a:LX/1O1;


# direct methods
.method public constructor <init>(LX/1O1;)V
    .locals 0

    .prologue
    .line 238624
    iput-object p1, p0, LX/1O4;->a:LX/1O1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 238622
    iget-object v0, p0, LX/1O4;->a:LX/1O1;

    invoke-virtual {v0}, LX/1O1;->invalidateSelf()V

    .line 238623
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 238620
    iget-object v0, p0, LX/1O4;->a:LX/1O1;

    invoke-virtual {v0, p2, p3, p4}, LX/1O1;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 238621
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 238618
    iget-object v0, p0, LX/1O4;->a:LX/1O1;

    invoke-virtual {v0, p2}, LX/1O1;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 238619
    return-void
.end method
