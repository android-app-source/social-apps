.class public LX/1fj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1fj;


# instance fields
.field public final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 292158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292159
    new-instance v0, LX/0aq;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1fj;->a:LX/0aq;

    .line 292160
    return-void
.end method

.method public static a(LX/0QB;)LX/1fj;
    .locals 3

    .prologue
    .line 292161
    sget-object v0, LX/1fj;->b:LX/1fj;

    if-nez v0, :cond_1

    .line 292162
    const-class v1, LX/1fj;

    monitor-enter v1

    .line 292163
    :try_start_0
    sget-object v0, LX/1fj;->b:LX/1fj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 292164
    if-eqz v2, :cond_0

    .line 292165
    :try_start_1
    new-instance v0, LX/1fj;

    invoke-direct {v0}, LX/1fj;-><init>()V

    .line 292166
    move-object v0, v0

    .line 292167
    sput-object v0, LX/1fj;->b:LX/1fj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292168
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 292169
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292170
    :cond_1
    sget-object v0, LX/1fj;->b:LX/1fj;

    return-object v0

    .line 292171
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 292172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 292173
    iget-object v0, p0, LX/1fj;->a:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V

    .line 292174
    return-void
.end method
