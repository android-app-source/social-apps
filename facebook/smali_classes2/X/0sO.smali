.class public LX/0sO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/message/BasicNameValuePair;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile l:LX/0sO;


# instance fields
.field public final c:LX/0lC;

.field private final d:LX/03V;

.field private final e:LX/0lp;

.field private final f:Ljava/lang/String;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/34r;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0rz;

.field public final i:LX/0sQ;

.field public final j:LX/0sS;

.field public final k:LX/0sT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 151812
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/http/message/BasicNameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "response_format"

    const-string v4, "flatbuffer"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "flatbuffer_version"

    const-string v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "flatbuffer_schema_id"

    const-string v4, "10155272039501729"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, LX/0sO;->a:Ljava/util/List;

    .line 151813
    new-instance v0, LX/0sP;

    invoke-direct {v0}, LX/0sP;-><init>()V

    sput-object v0, LX/0sO;->b:Ljava/util/concurrent/Callable;

    return-void
.end method

.method public constructor <init>(LX/0lC;LX/03V;LX/0lp;Ljava/lang/String;LX/0Ot;LX/0rz;LX/0sQ;LX/0sS;LX/0sT;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/graphql/protocol/CustomGraphQLSchemaName;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0lp;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/34r;",
            ">;",
            "LX/0rz;",
            "LX/0sQ;",
            "LX/0sS;",
            "LX/0sT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 151814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151815
    iput-object p1, p0, LX/0sO;->c:LX/0lC;

    .line 151816
    iput-object p2, p0, LX/0sO;->d:LX/03V;

    .line 151817
    iput-object p3, p0, LX/0sO;->e:LX/0lp;

    .line 151818
    iput-object p4, p0, LX/0sO;->f:Ljava/lang/String;

    .line 151819
    iput-object p5, p0, LX/0sO;->g:LX/0Ot;

    .line 151820
    iput-object p6, p0, LX/0sO;->h:LX/0rz;

    .line 151821
    iput-object p7, p0, LX/0sO;->i:LX/0sQ;

    .line 151822
    iput-object p8, p0, LX/0sO;->j:LX/0sS;

    .line 151823
    iput-object p9, p0, LX/0sO;->k:LX/0sT;

    .line 151824
    return-void
.end method

.method private static a(LX/0sO;LX/0w7;Ljava/lang/String;ZZLX/14S;)LX/0n9;
    .locals 3

    .prologue
    .line 151825
    if-eqz p1, :cond_6

    .line 151826
    invoke-virtual {p1}, LX/0w7;->a()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    .line 151827
    invoke-virtual {p1}, LX/0w7;->d()LX/0n9;

    move-result-object v1

    .line 151828
    if-eqz v1, :cond_0

    .line 151829
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0nA;->a(LX/0nD;)V

    .line 151830
    if-eqz p3, :cond_5

    .line 151831
    const-string v2, "query_params"

    invoke-virtual {v0, v2, v1}, LX/0n9;->b(Ljava/lang/String;LX/0nA;)V

    .line 151832
    :cond_0
    :goto_0
    const-string v1, "method"

    .line 151833
    invoke-static {v0, v1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151834
    iget-object v1, p0, LX/0sO;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 151835
    const-string v1, "custom_schema"

    iget-object v2, p0, LX/0sO;->f:Ljava/lang/String;

    .line 151836
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151837
    :cond_1
    if-eqz p4, :cond_2

    .line 151838
    const-string v1, "is_offline"

    const-string v2, "true"

    .line 151839
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151840
    :cond_2
    sget-object v1, LX/14S;->FLATBUFFER:LX/14S;

    if-ne p5, v1, :cond_3

    .line 151841
    const-string v1, "response_format"

    const-string v2, "flatbuffer"

    .line 151842
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151843
    const-string v1, "flatbuffer_version"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 151844
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151845
    :cond_3
    iget-object v1, p0, LX/0sO;->k:LX/0sT;

    invoke-virtual {v1}, LX/0sT;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 151846
    const-string v1, "oss_response_format"

    const-string v2, "true"

    .line 151847
    invoke-static {v0, v1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151848
    :cond_4
    return-object v0

    .line 151849
    :cond_5
    const-string v2, "query_params"

    invoke-virtual {v0, v2, v1}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    goto :goto_0

    .line 151850
    :cond_6
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/0sO;
    .locals 13

    .prologue
    .line 151890
    sget-object v0, LX/0sO;->l:LX/0sO;

    if-nez v0, :cond_1

    .line 151891
    const-class v1, LX/0sO;

    monitor-enter v1

    .line 151892
    :try_start_0
    sget-object v0, LX/0sO;->l:LX/0sO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151893
    if-eqz v2, :cond_0

    .line 151894
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 151895
    new-instance v3, LX/0sO;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v6

    check-cast v6, LX/0lp;

    .line 151896
    const/4 v7, 0x0

    move-object v7, v7

    .line 151897
    move-object v7, v7

    .line 151898
    check-cast v7, Ljava/lang/String;

    const/16 v8, 0x2e9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0rz;->a(LX/0QB;)LX/0rz;

    move-result-object v9

    check-cast v9, LX/0rz;

    invoke-static {v0}, LX/0sQ;->b(LX/0QB;)LX/0sQ;

    move-result-object v10

    check-cast v10, LX/0sQ;

    invoke-static {v0}, LX/0sS;->a(LX/0QB;)LX/0sS;

    move-result-object v11

    check-cast v11, LX/0sS;

    invoke-static {v0}, LX/0sT;->a(LX/0QB;)LX/0sT;

    move-result-object v12

    check-cast v12, LX/0sT;

    invoke-direct/range {v3 .. v12}, LX/0sO;-><init>(LX/0lC;LX/03V;LX/0lp;Ljava/lang/String;LX/0Ot;LX/0rz;LX/0sQ;LX/0sS;LX/0sT;)V

    .line 151899
    move-object v0, v3

    .line 151900
    sput-object v0, LX/0sO;->l:LX/0sO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151901
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 151902
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151903
    :cond_1
    sget-object v0, LX/0sO;->l:LX/0sO;

    return-object v0

    .line 151904
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 151905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0sO;Ljava/lang/String;LX/0n9;LX/0Px;LX/0Px;LX/14S;LX/14P;LX/0zW;)LX/14N;
    .locals 3
    .param p2    # LX/0n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0n9;",
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;",
            "LX/14S;",
            "LX/14P;",
            "Lcom/facebook/http/interfaces/RequestState;",
            ")",
            "LX/14N;"
        }
    .end annotation

    .prologue
    .line 151851
    iget-object v0, p0, LX/0sO;->h:LX/0rz;

    .line 151852
    invoke-virtual {v0}, LX/0rz;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151853
    sget-object v1, LX/00c;->a:LX/00c;

    move-object v1, v1

    .line 151854
    iget-object v2, v0, LX/0rz;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/00c;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 151855
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0sO;->g:LX/0Ot;

    if-eqz v0, :cond_0

    .line 151856
    iget-object v0, p0, LX/0sO;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34r;

    .line 151857
    sget-object v1, LX/34r;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    .line 151858
    if-eqz v1, :cond_0

    .line 151859
    iget-object v1, v0, LX/34r;->d:LX/0Wd;

    iget-object v2, v0, LX/34r;->e:Ljava/lang/Runnable;

    const p0, -0x4322d170    # -0.026999742f

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 151860
    :cond_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    .line 151861
    iput-object p1, v0, LX/14O;->b:Ljava/lang/String;

    .line 151862
    move-object v0, v0

    .line 151863
    const-string v1, "POST"

    .line 151864
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 151865
    move-object v0, v0

    .line 151866
    const-string v1, "graphql"

    .line 151867
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 151868
    move-object v0, v0

    .line 151869
    iput-object p2, v0, LX/14O;->h:LX/0n9;

    .line 151870
    move-object v0, v0

    .line 151871
    invoke-virtual {v0, p3}, LX/14O;->a(LX/0Px;)LX/14O;

    move-result-object v0

    .line 151872
    iput-object p5, v0, LX/14O;->k:LX/14S;

    .line 151873
    move-object v0, v0

    .line 151874
    invoke-virtual {v0, p6}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    .line 151875
    iput-object p4, v0, LX/14O;->l:Ljava/util/List;

    .line 151876
    move-object v0, v0

    .line 151877
    iput-object p7, v0, LX/14O;->y:LX/0zW;

    .line 151878
    move-object v0, v0

    .line 151879
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0w7;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 151880
    if-nez p0, :cond_1

    .line 151881
    :cond_0
    :goto_0
    return-object v0

    .line 151882
    :cond_1
    invoke-virtual {p0}, LX/0w7;->d()LX/0n9;

    move-result-object v1

    .line 151883
    if-eqz v1, :cond_0

    .line 151884
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0nA;->a(LX/0nD;)V

    .line 151885
    new-instance v0, Ljava/io/StringWriter;

    .line 151886
    iget v2, v1, LX/0n9;->c:I

    move v2, v2

    .line 151887
    mul-int/lit8 v2, v2, 0x32

    invoke-direct {v0, v2}, Ljava/io/StringWriter;-><init>(I)V

    .line 151888
    invoke-virtual {v1, v0}, LX/0nA;->a(Ljava/io/Writer;)V

    .line 151889
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0w7;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0w7;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 151784
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151785
    if-nez p0, :cond_1

    .line 151786
    :cond_0
    :goto_0
    return-void

    .line 151787
    :cond_1
    invoke-virtual {p0}, LX/0w7;->d()LX/0n9;

    move-result-object v2

    .line 151788
    if-eqz v2, :cond_0

    move v0, v1

    .line 151789
    :goto_1
    iget v3, v2, LX/0n9;->c:I

    move v3, v3

    .line 151790
    if-ge v0, v3, :cond_0

    .line 151791
    invoke-virtual {v2, v0}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 151792
    invoke-virtual {v2, v0}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 151793
    instance-of v2, v0, LX/0n9;

    if-eqz v2, :cond_0

    .line 151794
    check-cast v0, LX/0n9;

    move v2, v1

    .line 151795
    :goto_2
    iget v3, v0, LX/0n9;->c:I

    move v3, v3

    .line 151796
    if-ge v2, v3, :cond_2

    .line 151797
    invoke-virtual {v0, v2}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 151798
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 151799
    const/4 v1, 0x1

    .line 151800
    :cond_2
    if-nez v1, :cond_0

    .line 151801
    :try_start_0
    invoke-interface {p3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151802
    if-eqz v1, :cond_0

    .line 151803
    invoke-static {v0, p2, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 151804
    goto :goto_0

    .line 151805
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 151806
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151807
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0zP;Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zP;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151808
    iget-object v0, p0, LX/0gW;->e:LX/0w7;

    move-object v0, v0

    .line 151809
    iget-object v1, p0, LX/0zP;->f:Ljava/lang/String;

    move-object v1, v1

    .line 151810
    invoke-static {v0, v1, p1, p2}, LX/0sO;->a(LX/0w7;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 151811
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/14S;LX/0w7;Ljava/lang/String;LX/14P;LX/0zW;ZZLX/0Px;LX/0Px;ZZ)LX/14N;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/14S;",
            "LX/0w7;",
            "Ljava/lang/String;",
            "LX/14P;",
            "Lcom/facebook/http/interfaces/RequestState;",
            "ZZ",
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4cQ;",
            ">;ZZ)",
            "LX/14N;"
        }
    .end annotation

    .prologue
    .line 151771
    const-string v0, "%s.getParameterizedApiRequest"

    const v1, -0x6f5c658f

    invoke-static {v0, p1, v1}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    move-object v0, p0

    move-object v1, p5

    move-object v2, p6

    move/from16 v3, p13

    move/from16 v4, p14

    move-object v5, p4

    .line 151772
    :try_start_0
    invoke-static/range {v0 .. v5}, LX/0sO;->a(LX/0sO;LX/0w7;Ljava/lang/String;ZZLX/14S;)LX/0n9;

    move-result-object v2

    .line 151773
    const-string v0, "query_id"

    invoke-virtual {v2, v0, p2}, LX/0n9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151774
    const-string v0, "query_name"

    invoke-virtual {v2, v0, p1}, LX/0n9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151775
    iget-object v0, p0, LX/0sO;->k:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 151776
    const-string v0, "flat_buffer_idl"

    invoke-virtual {v2, v0, p3}, LX/0n9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151777
    sget-object p4, LX/14S;->FLATBUFFER_IDL:LX/14S;

    move-object v5, p4

    .line 151778
    :goto_0
    if-eqz p9, :cond_0

    .line 151779
    const-string v0, "strip_defaults"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, LX/0n9;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151780
    :cond_0
    if-eqz p10, :cond_1

    .line 151781
    const-string v0, "strip_nulls"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, LX/0n9;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object/from16 v3, p11

    move-object/from16 v4, p12

    move-object v6, p7

    move-object/from16 v7, p8

    .line 151782
    invoke-static/range {v0 .. v7}, LX/0sO;->a(LX/0sO;Ljava/lang/String;LX/0n9;LX/0Px;LX/0Px;LX/14S;LX/14P;LX/0zW;)LX/14N;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 151783
    const v1, -0xead30d9

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x49d73e9e    # 1763283.8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    move-object v5, p4

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILX/15w;)LX/15w;
    .locals 2

    .prologue
    .line 151768
    const-string v0, "%s.getResponse"

    const v1, -0x54dfb06

    invoke-static {v0, p1, v1}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 151769
    :try_start_0
    iget-object v0, p0, LX/0sO;->c:LX/0lC;

    invoke-static {p2, p3, v0}, LX/261;->a(ILX/15w;LX/0lC;)LX/15w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 151770
    const v1, -0x13a98126

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x904445b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/15w;)LX/2Oo;
    .locals 2

    .prologue
    .line 151767
    invoke-virtual {p0, p1}, LX/0sO;->b(LX/15w;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oo;

    return-object v0
.end method

.method public final a(LX/15w;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/15w;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 151757
    :try_start_0
    invoke-virtual {p1, p2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 151758
    if-nez v0, :cond_0

    .line 151759
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Null result after successful parsing"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151760
    :catch_0
    move-exception v0

    .line 151761
    iget-object v1, p0, LX/0sO;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_parse_error_json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151762
    throw v0

    .line 151763
    :catch_1
    move-exception v0

    .line 151764
    iget-object v1, p0, LX/0sO;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_parse_error_io"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151765
    throw v0

    .line 151766
    :cond_0
    return-object v0
.end method

.method public final a(LX/0w5;LX/15w;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0w5",
            "<TT;>;",
            "LX/15w;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 151756
    iget-object v0, p0, LX/0sO;->c:LX/0lC;

    iget-object v1, p0, LX/0sO;->k:LX/0sT;

    invoke-virtual {v1}, LX/0sT;->j()Z

    move-result v1

    invoke-static {p1, p2, v0, v1}, LX/261;->a(LX/0w5;LX/15w;LX/0lC;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            ")",
            "LX/0Px",
            "<",
            "LX/2Oo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151752
    iget-object v0, p0, LX/0sO;->c:LX/0lC;

    invoke-static {p1, v0}, LX/261;->a(LX/15w;LX/0lC;)LX/0Px;

    move-result-object v0

    .line 151753
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 151754
    return-object v0

    .line 151755
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Given a parser to map an error, but no error was found."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/0w7;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 151716
    if-nez p1, :cond_1

    .line 151717
    :cond_0
    :goto_0
    return-object v0

    .line 151718
    :cond_1
    invoke-virtual {p1}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v1

    .line 151719
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 151720
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 151721
    iget-object v0, p0, LX/0sO;->e:LX/0lp;

    invoke-virtual {v0, v2}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v3

    .line 151722
    :try_start_0
    invoke-virtual {v3}, LX/0nX;->f()V

    .line 151723
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 151724
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 151725
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4a1;

    .line 151726
    invoke-virtual {v3, v1}, LX/0nX;->g(Ljava/lang/String;)V

    .line 151727
    const-string v1, "query"

    iget-object v5, v0, LX/4a1;->a:LX/0zO;

    invoke-virtual {v5}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151728
    const-string v1, "import"

    iget-object v5, v0, LX/4a1;->b:Ljava/lang/String;

    invoke-virtual {v3, v1, v5}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151729
    iget-object v1, v0, LX/4a1;->c:LX/4Zz;

    sget-object v5, LX/4Zz;->NO_FAN_OUT:LX/4Zz;

    if-eq v1, v5, :cond_2

    .line 151730
    const-string v1, "plural"

    iget-object v5, v0, LX/4a1;->c:LX/4Zz;

    .line 151731
    sget-object p0, LX/4Zy;->a:[I

    invoke-virtual {v5}, LX/4Zz;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 151732
    const/4 p0, 0x0

    :goto_2
    move-object v5, p0

    .line 151733
    invoke-virtual {v3, v1, v5}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151734
    :cond_2
    iget-object v1, v0, LX/4a1;->d:LX/4a0;

    sget-object v5, LX/4a0;->NOT_SET:LX/4a0;

    if-eq v1, v5, :cond_3

    .line 151735
    const-string v1, "fallback"

    iget-object v0, v0, LX/4a1;->d:LX/4a0;

    .line 151736
    sget-object v5, LX/4Zy;->b:[I

    invoke-virtual {v0}, LX/4a0;->ordinal()I

    move-result p0

    aget v5, v5, p0

    packed-switch v5, :pswitch_data_1

    .line 151737
    const/4 v5, 0x0

    :goto_3
    move-object v0, v5

    .line 151738
    invoke-virtual {v3, v1, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151739
    :cond_3
    invoke-virtual {v3}, LX/0nX;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 151740
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/0nX;->close()V

    throw v0

    .line 151741
    :cond_4
    :try_start_1
    invoke-virtual {v3}, LX/0nX;->g()V

    .line 151742
    invoke-virtual {v3}, LX/0nX;->flush()V

    .line 151743
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 151744
    invoke-virtual {v3}, LX/0nX;->close()V

    goto/16 :goto_0

    .line 151745
    :pswitch_0
    :try_start_2
    const-string p0, "each"

    goto :goto_2

    .line 151746
    :pswitch_1
    const-string p0, "all"

    goto :goto_2

    .line 151747
    :pswitch_2
    const-string p0, "first"

    goto :goto_2

    .line 151748
    :pswitch_3
    const-string p0, "last"

    goto :goto_2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151749
    :pswitch_4
    const-string v5, "error"

    goto :goto_3

    .line 151750
    :pswitch_5
    const-string v5, "skip"

    goto :goto_3

    .line 151751
    :pswitch_6
    const-string v5, "allow"

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final c(LX/0w7;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 151688
    if-nez p1, :cond_1

    .line 151689
    :cond_0
    :goto_0
    return-object v0

    .line 151690
    :cond_1
    invoke-virtual {p1}, LX/0w7;->c()Ljava/util/Map;

    move-result-object v1

    .line 151691
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 151692
    new-instance v3, Ljava/io/StringWriter;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x32

    invoke-direct {v3, v2}, Ljava/io/StringWriter;-><init>(I)V

    .line 151693
    :try_start_0
    iget-object v2, p0, LX/0sO;->e:LX/0lp;

    invoke-virtual {v2, v3}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 151694
    :try_start_1
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 151695
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 151696
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 151697
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 151698
    if-eqz v0, :cond_2

    .line 151699
    instance-of v5, v0, Ljava/lang/Integer;

    if-eqz v5, :cond_4

    .line 151700
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/0nX;->a(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 151701
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_3

    .line 151702
    invoke-virtual {v1}, LX/0nX;->close()V

    :cond_3
    throw v0

    .line 151703
    :cond_4
    :try_start_2
    instance-of v5, v0, Ljava/lang/Double;

    if-eqz v5, :cond_5

    .line 151704
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v2, v1, v6, v7}, LX/0nX;->a(Ljava/lang/String;D)V

    goto :goto_1

    .line 151705
    :cond_5
    instance-of v5, v0, Ljava/lang/Float;

    if-eqz v5, :cond_6

    .line 151706
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/0nX;->a(Ljava/lang/String;F)V

    goto :goto_1

    .line 151707
    :cond_6
    instance-of v5, v0, Ljava/lang/Long;

    if-eqz v5, :cond_7

    .line 151708
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v1, v6, v7}, LX/0nX;->a(Ljava/lang/String;J)V

    goto :goto_1

    .line 151709
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 151710
    :cond_8
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 151711
    invoke-virtual {v2}, LX/0nX;->flush()V

    .line 151712
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 151713
    if-eqz v2, :cond_0

    .line 151714
    invoke-virtual {v2}, LX/0nX;->close()V

    goto/16 :goto_0

    .line 151715
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2
.end method
