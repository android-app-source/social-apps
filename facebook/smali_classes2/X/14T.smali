.class public final LX/14T;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/facebook/http/interfaces/RequestState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 178870
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/14T;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 178871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178872
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zW;
    .locals 1

    .prologue
    .line 178873
    sget-object v0, LX/14T;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zW;

    .line 178874
    if-nez v0, :cond_0

    .line 178875
    new-instance v0, LX/0zW;

    invoke-direct {v0, p0, p1}, LX/0zW;-><init>(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 178876
    :goto_0
    return-object v0

    .line 178877
    :cond_0
    invoke-virtual {v0, p1}, LX/0zW;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    goto :goto_0
.end method
