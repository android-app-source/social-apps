.class public LX/0Y7;
.super Landroid/database/Observable;
.source ""

# interfaces
.implements LX/0Y8;
.implements LX/0Y4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/database/Observable",
        "<",
        "Lcom/facebook/quicklog/QPLGenericListener;",
        ">;",
        "LX/0Y8;",
        "LX/0Y4;"
    }
.end annotation


# static fields
.field private static b:LX/0Y7;


# instance fields
.field public final a:LX/0Xh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80466
    const/4 v0, 0x0

    sput-object v0, LX/0Y7;->b:LX/0Y7;

    return-void
.end method

.method private constructor <init>(LX/0Xh;)V
    .locals 0

    .prologue
    .line 80563
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    .line 80564
    iput-object p1, p0, LX/0Y7;->a:LX/0Xh;

    .line 80565
    return-void
.end method

.method public static b(LX/0Y7;Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 3

    .prologue
    .line 80556
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 80557
    :cond_0
    const-class v0, LX/0Y7;

    const-string v1, "performanceLoggingEvent/mObservers cannot be null."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 80558
    :goto_0
    return-void

    .line 80559
    :cond_1
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80560
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4im;

    .line 80561
    invoke-interface {v0, p1}, Lcom/facebook/quicklog/QuickPerformanceLoggerEventListener;->onPerformanceLoggingEvent(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    goto :goto_1

    .line 80562
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static create(LX/0Xh;)LX/0Y7;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 80553
    sget-object v0, LX/0Y7;->b:LX/0Y7;

    if-nez v0, :cond_0

    .line 80554
    new-instance v0, LX/0Y7;

    invoke-direct {v0, p0}, LX/0Y7;-><init>(LX/0Xh;)V

    sput-object v0, LX/0Y7;->b:LX/0Y7;

    .line 80555
    :cond_0
    sget-object v0, LX/0Y7;->b:LX/0Y7;

    return-object v0
.end method

.method public static getInstance()LX/0Y7;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 80552
    sget-object v0, LX/0Y7;->b:LX/0Y7;

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Pn;)V
    .locals 5

    .prologue
    .line 80543
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 80544
    :cond_0
    :goto_0
    return-void

    .line 80545
    :cond_1
    const-string v0, "QuickPerformanceLoggerImpl"

    const-string v1, "QPL markerStart - %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 80546
    iget v4, p1, LX/0Pn;->g:I

    move v4, v4

    .line 80547
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80548
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80549
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4im;

    .line 80550
    invoke-interface {v0, p1}, LX/0Y4;->a(LX/0Pn;)V

    goto :goto_1

    .line 80551
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/quicklog/PerformanceLoggingEvent;)V
    .locals 11

    .prologue
    .line 80488
    iget-object v0, p0, LX/0Y7;->a:LX/0Xh;

    invoke-virtual {v0}, LX/0Xh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80489
    :cond_0
    :goto_0
    return-void

    .line 80490
    :cond_1
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->y:Ljava/lang/String;

    move-object v2, v0

    .line 80491
    if-nez v2, :cond_2

    .line 80492
    iget v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->k:I

    move v0, v0

    .line 80493
    invoke-static {v0}, LX/2BW;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 80494
    :cond_2
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 80495
    const/4 v1, 0x0

    .line 80496
    const/4 v0, 0x0

    .line 80497
    iget-object v3, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->w:Ljava/util/ArrayList;

    move-object v3, v3

    .line 80498
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v1

    move-object v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80499
    add-int/lit8 v3, v3, 0x1

    .line 80500
    rem-int/lit8 v6, v3, 0x2

    if-nez v6, :cond_3

    .line 80501
    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 80502
    goto :goto_1

    .line 80503
    :cond_4
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->x:Ljava/util/ArrayList;

    move-object v0, v0

    .line 80504
    if-eqz v0, :cond_5

    .line 80505
    const-string v1, "trace_tags"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80506
    :cond_5
    iget-object v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->t:LX/00q;

    move-object v0, v0

    .line 80507
    if-eqz v0, :cond_6

    .line 80508
    iget-boolean v1, v0, LX/00q;->c:Z

    move v1, v1

    .line 80509
    if-eqz v1, :cond_6

    .line 80510
    const-string v1, "class_load_attempts"

    invoke-virtual {v0}, LX/00q;->j()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80511
    const-string v1, "dex_queries"

    invoke-virtual {v0}, LX/00q;->k()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80512
    const-string v1, "start_pri"

    .line 80513
    iget v3, v0, LX/00q;->f:I

    move v3, v3

    .line 80514
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80515
    const-string v1, "stop_pri"

    .line 80516
    iget v3, v0, LX/00q;->g:I

    move v3, v3

    .line 80517
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80518
    const-string v1, "ps_cpu_ms"

    .line 80519
    iget-wide v9, v0, LX/00q;->h:J

    move-wide v6, v9

    .line 80520
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80521
    const-string v1, "ps_flt"

    .line 80522
    iget-wide v9, v0, LX/00q;->j:J

    move-wide v6, v9

    .line 80523
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80524
    invoke-virtual {v0}, LX/00q;->m()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 80525
    const-string v1, "th_cpu_ms"

    .line 80526
    iget-wide v9, v0, LX/00q;->i:J

    move-wide v6, v9

    .line 80527
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80528
    const-string v1, "th_flt"

    .line 80529
    iget-wide v9, v0, LX/00q;->k:J

    move-wide v6, v9

    .line 80530
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80531
    :cond_6
    iget v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    move v0, v0

    .line 80532
    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    .line 80533
    const-string v0, "gc_ms"

    .line 80534
    iget v1, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->j:I

    move v1, v1

    .line 80535
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80536
    :cond_7
    const-string v1, "QuickPerformanceLoggerImpl"

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 80537
    iget-wide v9, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->h:J

    move-wide v4, v9

    .line 80538
    iget v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->i:I

    move v0, v0

    .line 80539
    int-to-long v6, v0

    .line 80540
    iget-short v0, p1, Lcom/facebook/quicklog/PerformanceLoggingEvent;->o:S

    move v0, v0

    .line 80541
    invoke-static {v0}, LX/2BY;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v1 .. v8}, LX/2BZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 80542
    invoke-static {p0, p1}, LX/0Y7;->b(LX/0Y7;Lcom/facebook/quicklog/PerformanceLoggingEvent;)V

    goto/16 :goto_0
.end method

.method public final b(LX/0Pn;)V
    .locals 3

    .prologue
    .line 80482
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 80483
    :cond_0
    :goto_0
    return-void

    .line 80484
    :cond_1
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80485
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4im;

    .line 80486
    invoke-interface {v0, p1}, LX/0Y4;->b(LX/0Pn;)V

    goto :goto_1

    .line 80487
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final c(LX/0Pn;)V
    .locals 3

    .prologue
    .line 80476
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 80477
    :cond_0
    :goto_0
    return-void

    .line 80478
    :cond_1
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80479
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4im;

    .line 80480
    invoke-interface {v0, p1}, LX/0Y4;->c(LX/0Pn;)V

    goto :goto_1

    .line 80481
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final d(LX/0Pn;)V
    .locals 3

    .prologue
    .line 80470
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 80471
    :cond_0
    :goto_0
    return-void

    .line 80472
    :cond_1
    iget-object v1, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80473
    :try_start_0
    iget-object v0, p0, Landroid/database/Observable;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4im;

    .line 80474
    invoke-interface {v0, p1}, LX/0Y4;->d(LX/0Pn;)V

    goto :goto_1

    .line 80475
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public dummy()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 80468
    new-instance v0, LX/4im;

    invoke-direct {v0, p0}, LX/4im;-><init>(LX/0Y7;)V

    invoke-virtual {p0, v0}, LX/0Y7;->registerObserver(Ljava/lang/Object;)V

    .line 80469
    return-void
.end method

.method public final e(LX/0Pn;)V
    .locals 0

    .prologue
    .line 80467
    return-void
.end method
