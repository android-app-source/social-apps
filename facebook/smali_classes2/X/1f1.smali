.class public LX/1f1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289464
    return-void
.end method

.method public static a(LX/0QB;)LX/1f1;
    .locals 1

    .prologue
    .line 289443
    new-instance v0, LX/1f1;

    invoke-direct {v0}, LX/1f1;-><init>()V

    .line 289444
    move-object v0, v0

    .line 289445
    return-object v0
.end method

.method private static a(ILcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 289451
    if-nez p1, :cond_1

    .line 289452
    :cond_0
    :goto_0
    return-object p2

    .line 289453
    :cond_1
    if-nez p2, :cond_2

    move-object p2, p1

    .line 289454
    goto :goto_0

    .line 289455
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p0, :cond_3

    .line 289456
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    if-ge v0, p0, :cond_5

    :goto_1
    move-object p2, p1

    .line 289457
    goto :goto_0

    .line 289458
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lt v0, p0, :cond_4

    move-object p2, p1

    .line 289459
    goto :goto_0

    .line 289460
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-ge v0, p0, :cond_0

    .line 289461
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    if-le v0, p0, :cond_6

    :goto_2
    move-object p2, p1

    .line 289462
    goto :goto_0

    :cond_5
    move-object p1, p2

    goto :goto_1

    :cond_6
    move-object p1, p2

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 289446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 289447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1f1;->a(ILcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 289448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1f1;->a(ILcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 289449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1f1;->a(ILcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 289450
    return-object v0
.end method
