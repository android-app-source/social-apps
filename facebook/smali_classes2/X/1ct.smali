.class public LX/1ct;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 283585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0YU;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0YU",
            "<",
            "LX/1cv;",
            ">;)",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 283573
    invoke-virtual {p0}, LX/0YU;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 283574
    invoke-virtual {p0, v0}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 283575
    iget-object v1, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 283576
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 283577
    :goto_0
    return-object v0

    .line 283578
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 283579
    :goto_1
    invoke-virtual {p0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 283580
    invoke-virtual {p0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 283581
    iget-object v3, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v3

    .line 283582
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283583
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 283584
    goto :goto_0
.end method

.method public static a(IILX/0YU;LX/0YU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "LX/0YU",
            "<TT;>;",
            "LX/0YU",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 283565
    invoke-static {p0, p3}, LX/1ct;->a(ILX/0YU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283566
    invoke-virtual {p3, p0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 283567
    invoke-virtual {p3, p0}, LX/0YU;->b(I)V

    .line 283568
    :goto_0
    invoke-virtual {p2, p1, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283569
    return-void

    .line 283570
    :cond_0
    invoke-virtual {p2, p0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 283571
    invoke-virtual {p2, p0}, LX/0YU;->b(I)V

    .line 283572
    goto :goto_0
.end method

.method public static a(ILX/0YU;LX/0YU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "LX/0YU",
            "<TT;>;",
            "LX/0YU",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 283561
    invoke-virtual {p1, p0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 283562
    if-eqz v0, :cond_0

    .line 283563
    invoke-virtual {p2, p0, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 283564
    :cond_0
    return-void
.end method

.method public static a(LX/1cv;)V
    .locals 1

    .prologue
    .line 283540
    invoke-virtual {p0}, LX/1cv;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283541
    iget-object v0, p0, LX/1cv;->c:LX/1cn;

    move-object v0, v0

    .line 283542
    invoke-virtual {v0}, LX/1cn;->b()V

    .line 283543
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V
    .locals 2

    .prologue
    .line 283551
    and-int/lit8 v0, p2, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 283552
    if-nez v0, :cond_0

    .line 283553
    and-int/lit8 v0, p2, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 283554
    if-nez v0, :cond_0

    .line 283555
    and-int/lit8 v0, p2, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 283556
    if-nez v0, :cond_0

    invoke-static {p2}, LX/1cv;->d(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 283557
    :goto_3
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283558
    invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 283559
    :cond_1
    return-void

    .line 283560
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 283545
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 283546
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 283547
    invoke-static {p0, p1, p3}, LX/1ct;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    .line 283548
    invoke-virtual {p0, p2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 283549
    return-void

    :cond_0
    move v0, v1

    .line 283550
    goto :goto_0
.end method

.method public static a(ILX/0YU;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "LX/0YU",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 283544
    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
