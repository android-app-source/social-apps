.class public LX/0Wc;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76593
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 76594
    return-void
.end method

.method public static a(LX/0Wb;Ljava/util/concurrent/ExecutorService;)LX/0Wd;
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76592
    invoke-virtual {p0, p1}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Wd;LX/0Sj;LX/0TR;)LX/1fX;
    .locals 1
    .param p0    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ExplicitSimpleProvider"
        }
    .end annotation

    .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76588
    new-instance v0, LX/1fW;

    invoke-direct {v0, p0, p1, p2}, LX/1fW;-><init>(Ljava/util/concurrent/Executor;LX/0Sj;LX/0TR;)V

    return-object v0
.end method

.method public static b(LX/0Wb;Ljava/util/concurrent/ExecutorService;)LX/0Wd;
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 76591
    invoke-virtual {p0, p1}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0Wb;Ljava/util/concurrent/ExecutorService;)LX/0Wd;
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 76590
    invoke-virtual {p0, p1}, LX/0Wb;->a(Ljava/util/concurrent/ExecutorService;)LX/0Wd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 76589
    return-void
.end method
