.class public LX/1JZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Jd;

.field public final b:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final e:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1HI;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:LX/1Jc;

.field public final j:I


# direct methods
.method public constructor <init>(LX/1HI;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/1Ja;LX/1Jb;LX/0ad;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 230120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230121
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    .line 230122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1JZ;->c:Ljava/util/List;

    .line 230123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1JZ;->d:Ljava/util/HashMap;

    .line 230124
    new-instance v0, LX/0aq;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/1JZ;->e:LX/0aq;

    .line 230125
    iput-object p1, p0, LX/1JZ;->f:LX/1HI;

    .line 230126
    iput-object p2, p0, LX/1JZ;->g:Ljava/util/concurrent/ExecutorService;

    .line 230127
    iget-object v0, p0, LX/1JZ;->g:Ljava/util/concurrent/ExecutorService;

    .line 230128
    new-instance v1, LX/1Jc;

    invoke-direct {v1, v0, p0}, LX/1Jc;-><init>(Ljava/util/concurrent/ExecutorService;LX/1JZ;)V

    .line 230129
    move-object v0, v1

    .line 230130
    iput-object v0, p0, LX/1JZ;->i:LX/1Jc;

    .line 230131
    sget v0, LX/1JW;->c:I

    const/4 v1, 0x2

    invoke-interface {p6, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/1JZ;->j:I

    .line 230132
    sget-short v0, LX/1JW;->f:S

    const/4 v1, 0x0

    invoke-interface {p6, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 230133
    if-eqz v0, :cond_0

    :goto_0
    iput-object p3, p0, LX/1JZ;->h:Ljava/util/concurrent/ExecutorService;

    .line 230134
    iget-object v0, p0, LX/1JZ;->h:Ljava/util/concurrent/ExecutorService;

    .line 230135
    new-instance v1, LX/1Jd;

    .line 230136
    new-instance p1, LX/0U8;

    invoke-interface {p5}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p2

    new-instance p3, LX/1Je;

    invoke-direct {p3, p5}, LX/1Je;-><init>(LX/0QB;)V

    invoke-direct {p1, p2, p3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object p1, p1

    .line 230137
    invoke-direct {v1, v0, p1}, LX/1Jd;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/Set;)V

    .line 230138
    move-object v0, v1

    .line 230139
    iput-object v0, p0, LX/1JZ;->a:LX/1Jd;

    .line 230140
    return-void

    :cond_0
    move-object p3, p2

    .line 230141
    goto :goto_0
.end method

.method public static a(LX/1JZ;)V
    .locals 4

    .prologue
    .line 230156
    :goto_0
    iget-object v0, p0, LX/1JZ;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, LX/1JZ;->j:I

    if-ge v0, v1, :cond_0

    .line 230157
    iget-object v0, p0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 230158
    iget-object v0, p0, LX/1JZ;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 230159
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/prefetch/PrefetchParams;

    .line 230160
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 230161
    iget-object v1, p0, LX/1JZ;->f:LX/1HI;

    .line 230162
    iget-object v2, v0, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    move-object v2, v2

    .line 230163
    iget-object v3, v0, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 230164
    invoke-virtual {v1, v2, v3}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 230165
    invoke-static {v1}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v1

    move-object v1, v1

    .line 230166
    new-instance v2, LX/24u;

    invoke-direct {v2, p0, v0}, LX/24u;-><init>(LX/1JZ;Lcom/facebook/photos/prefetch/PrefetchParams;)V

    iget-object v3, p0, LX/1JZ;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 230167
    iget-object v2, p0, LX/1JZ;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230168
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/1JZ;
    .locals 7

    .prologue
    .line 230169
    new-instance v0, LX/1JZ;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    const-class v4, LX/1Ja;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1Ja;

    const-class v5, LX/1Jb;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1Jb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct/range {v0 .. v6}, LX/1JZ;-><init>(LX/1HI;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/1Ja;LX/1Jb;LX/0ad;)V

    .line 230170
    return-object v0
.end method

.method public static b(LX/1JZ;Lcom/facebook/photos/prefetch/PrefetchParams;)V
    .locals 3

    .prologue
    .line 230144
    iget-object v0, p0, LX/1JZ;->f:LX/1HI;

    .line 230145
    iget-object v1, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    move-object v1, v1

    .line 230146
    invoke-virtual {v0, v1}, LX/1HI;->a(LX/1bf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230147
    :goto_0
    return-void

    .line 230148
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->a:LX/1bf;

    move-object v0, v0

    .line 230149
    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 230150
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 230151
    move-object v0, v0

    .line 230152
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 230153
    iget-object v1, p0, LX/1JZ;->f:LX/1HI;

    .line 230154
    iget-object v2, p1, Lcom/facebook/photos/prefetch/PrefetchParams;->b:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 230155
    invoke-virtual {v1, v0, v2}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    goto :goto_0
.end method


# virtual methods
.method public final b(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230142
    iget-object v0, p0, LX/1JZ;->i:LX/1Jc;

    invoke-virtual {v0, p1, p2}, LX/1Jc;->a(Ljava/util/List;Ljava/util/List;)V

    .line 230143
    return-void
.end method
