.class public LX/1HI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/CancellationException;


# instance fields
.field private final b:LX/1HJ;

.field private final c:LX/1BU;

.field private final d:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1HY;

.field public final h:LX/1HY;

.field public final i:LX/1Ao;

.field private final j:LX/1HH;

.field private final k:LX/1Gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 226751
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Prefetching is not enabled"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/1HI;->a:Ljava/util/concurrent/CancellationException;

    return-void
.end method

.method public constructor <init>(LX/1HJ;Ljava/util/Set;LX/1Gd;LX/1Fh;LX/1Fh;LX/1HY;LX/1HY;LX/1Ao;LX/1HH;LX/1Gd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1HJ;",
            "Ljava/util/Set",
            "<",
            "LX/1BU;",
            ">;",
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "LX/1HY;",
            "LX/1HY;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1HH;",
            "LX/1Gd",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226753
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, LX/1HI;->l:Ljava/util/concurrent/atomic/AtomicLong;

    .line 226754
    iput-object p1, p0, LX/1HI;->b:LX/1HJ;

    .line 226755
    new-instance v0, LX/1Id;

    invoke-direct {v0, p2}, LX/1Id;-><init>(Ljava/util/Set;)V

    iput-object v0, p0, LX/1HI;->c:LX/1BU;

    .line 226756
    iput-object p3, p0, LX/1HI;->d:LX/1Gd;

    .line 226757
    iput-object p4, p0, LX/1HI;->e:LX/1Fh;

    .line 226758
    iput-object p5, p0, LX/1HI;->f:LX/1Fh;

    .line 226759
    iput-object p6, p0, LX/1HI;->g:LX/1HY;

    .line 226760
    iput-object p7, p0, LX/1HI;->h:LX/1HY;

    .line 226761
    iput-object p8, p0, LX/1HI;->i:LX/1Ao;

    .line 226762
    iput-object p9, p0, LX/1HI;->j:LX/1HH;

    .line 226763
    iput-object p10, p0, LX/1HI;->k:LX/1Gd;

    .line 226764
    return-void
.end method

.method private a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;)LX/1ca;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<TT;>;>;",
            "LX/1bf;",
            "LX/1bY;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 226765
    invoke-direct {p0, p2}, LX/1HI;->d(LX/1bf;)LX/1BU;

    move-result-object v3

    .line 226766
    :try_start_0
    iget-object v0, p2, LX/1bf;->k:LX/1bY;

    move-object v0, v0

    .line 226767
    invoke-static {v0, p3}, LX/1bY;->getMax(LX/1bY;LX/1bY;)LX/1bY;

    move-result-object v5

    .line 226768
    new-instance v0, LX/1cV;

    invoke-direct {p0}, LX/1HI;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    .line 226769
    iget-boolean v1, p2, LX/1bf;->e:Z

    move v1, v1

    .line 226770
    if-nez v1, :cond_0

    .line 226771
    iget-object v1, p2, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 226772
    if-nez v1, :cond_0

    .line 226773
    iget-object v1, p2, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 226774
    invoke-static {v1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v7, 0x1

    .line 226775
    :cond_1
    iget-object v1, p2, LX/1bf;->j:LX/1bc;

    move-object v8, v1

    .line 226776
    move-object v1, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, LX/1cV;-><init>(LX/1bf;Ljava/lang/String;LX/1BV;Ljava/lang/Object;LX/1bY;ZZLX/1bc;)V

    .line 226777
    new-instance v1, LX/1cX;

    invoke-direct {v1, p1, v0, v3}, LX/1cX;-><init>(LX/1cF;LX/1cV;LX/1BU;)V

    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226778
    :goto_0
    return-object v0

    .line 226779
    :catch_0
    move-exception v0

    .line 226780
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;LX/1bc;)LX/1ca;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "LX/1bf;",
            "LX/1bY;",
            "Ljava/lang/Object;",
            "LX/1bc;",
            ")",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226781
    invoke-direct {p0, p2}, LX/1HI;->d(LX/1bf;)LX/1BU;

    move-result-object v3

    .line 226782
    :try_start_0
    iget-object v0, p2, LX/1bf;->k:LX/1bY;

    move-object v0, v0

    .line 226783
    invoke-static {v0, p3}, LX/1bY;->getMax(LX/1bY;LX/1bY;)LX/1bY;

    move-result-object v5

    .line 226784
    new-instance v0, LX/1cV;

    invoke-direct {p0}, LX/1HI;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p2

    move-object v4, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, LX/1cV;-><init>(LX/1bf;Ljava/lang/String;LX/1BV;Ljava/lang/Object;LX/1bY;ZZLX/1bc;)V

    .line 226785
    new-instance v1, LX/24p;

    invoke-direct {v1, p1, v0, v3}, LX/24p;-><init>(LX/1cF;LX/1cV;LX/1BU;)V

    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226786
    :goto_0
    return-object v0

    .line 226787
    :catch_0
    move-exception v0

    .line 226788
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;LX/1bb;)Z
    .locals 2

    .prologue
    .line 226685
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 226686
    iput-object p2, v0, LX/1bX;->f:LX/1bb;

    .line 226687
    move-object v0, v0

    .line 226688
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 226689
    iget-object v1, p0, LX/1HI;->i:LX/1Ao;

    const/4 p1, 0x0

    invoke-virtual {v1, v0, p1}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v1

    .line 226690
    iget-object p1, v0, LX/1bf;->a:LX/1bb;

    move-object p1, p1

    .line 226691
    sget-object p2, LX/3CU;->a:[I

    invoke-virtual {p1}, LX/1bb;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    .line 226692
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 226693
    return v0

    .line 226694
    :pswitch_0
    iget-object p1, p0, LX/1HI;->g:LX/1HY;

    invoke-virtual {p1, v1}, LX/1HY;->c(LX/1bh;)Z

    move-result v1

    goto :goto_0

    .line 226695
    :pswitch_1
    iget-object p1, p0, LX/1HI;->h:LX/1HY;

    invoke-virtual {p1, v1}, LX/1HY;->c(LX/1bh;)Z

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(LX/1bf;)LX/1BU;
    .locals 4

    .prologue
    .line 226789
    iget-object v0, p1, LX/1bf;->n:LX/1BU;

    move-object v0, v0

    .line 226790
    if-nez v0, :cond_0

    .line 226791
    iget-object v0, p0, LX/1HI;->c:LX/1BU;

    .line 226792
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/1Id;

    const/4 v1, 0x2

    new-array v1, v1, [LX/1BU;

    const/4 v2, 0x0

    iget-object v3, p0, LX/1HI;->c:LX/1BU;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 226793
    iget-object v3, p1, LX/1bf;->n:LX/1BU;

    move-object v3, v3

    .line 226794
    aput-object v3, v1, v2

    invoke-direct {v0, v1}, LX/1Id;-><init>([LX/1BU;)V

    goto :goto_0
.end method

.method private e(Landroid/net/Uri;)Lcom/android/internal/util/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/android/internal/util/Predicate",
            "<",
            "LX/1bh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226795
    new-instance v0, LX/4dx;

    invoke-direct {v0, p0, p1}, LX/4dx;-><init>(LX/1HI;Landroid/net/Uri;)V

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226796
    iget-object v0, p0, LX/1HI;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1Gd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "LX/1bY;",
            ")",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 226797
    new-instance v0, LX/1bm;

    invoke-direct {v0, p0, p1, p2, p3}, LX/1bm;-><init>(LX/1HI;LX/1bf;Ljava/lang/Object;LX/1bY;)V

    return-object v0
.end method

.method public final a(LX/1bf;Ljava/lang/Object;Z)LX/1Gd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "Z)",
            "LX/1Gd",
            "<",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 226798
    if-eqz p3, :cond_0

    sget-object v0, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    .line 226799
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1Gd;

    move-result-object v0

    return-object v0

    .line 226800
    :cond_0
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    goto :goto_0
.end method

.method public final a(LX/1bf;Ljava/lang/Object;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226801
    sget-object v0, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    invoke-virtual {p0, p1, p2, v0}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1ca;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1bf;Ljava/lang/Object;LX/1bc;)LX/1ca;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "LX/1bc;",
            ")",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226740
    iget-object v0, p0, LX/1HI;->d:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226741
    sget-object v0, LX/1HI;->a:Ljava/util/concurrent/CancellationException;

    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    .line 226742
    :goto_0
    return-object v0

    .line 226743
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1HI;->b:LX/1HJ;

    invoke-virtual {v0, p1}, LX/1HJ;->b(LX/1bf;)LX/1cF;

    move-result-object v1

    .line 226744
    sget-object v3, LX/1bY;->FULL_FETCH:LX/1bY;

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/1HI;->a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;LX/1bc;)LX/1ca;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 226745
    :catch_0
    move-exception v0

    .line 226746
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 226747
    new-instance v0, LX/4dv;

    invoke-direct {v0, p0}, LX/4dv;-><init>(LX/1HI;)V

    .line 226748
    iget-object v1, p0, LX/1HI;->e:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->a(Lcom/android/internal/util/Predicate;)I

    .line 226749
    iget-object v1, p0, LX/1HI;->f:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->a(Lcom/android/internal/util/Predicate;)I

    .line 226750
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 226736
    invoke-direct {p0, p1}, LX/1HI;->e(Landroid/net/Uri;)Lcom/android/internal/util/Predicate;

    move-result-object v0

    .line 226737
    iget-object v1, p0, LX/1HI;->e:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->a(Lcom/android/internal/util/Predicate;)I

    .line 226738
    iget-object v1, p0, LX/1HI;->f:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->a(Lcom/android/internal/util/Predicate;)I

    .line 226739
    return-void
.end method

.method public final a(LX/1bf;)Z
    .locals 2

    .prologue
    .line 226729
    if-nez p1, :cond_0

    .line 226730
    const/4 v0, 0x0

    .line 226731
    :goto_0
    return v0

    .line 226732
    :cond_0
    iget-object v0, p0, LX/1HI;->i:LX/1Ao;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v0

    .line 226733
    iget-object v1, p0, LX/1HI;->e:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v1

    .line 226734
    :try_start_0
    invoke-static {v1}, LX/1FJ;->a(LX/1FJ;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 226735
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final b(LX/1bf;Ljava/lang/Object;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226728
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    invoke-virtual {p0, p1, p2, v0}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1ca;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1bf;Ljava/lang/Object;LX/1bY;)LX/1ca;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            "LX/1bY;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226718
    :try_start_0
    iget-object v0, p0, LX/1HI;->b:LX/1HJ;

    .line 226719
    invoke-static {v0, p1}, LX/1HJ;->f(LX/1HJ;LX/1bf;)LX/1cF;

    move-result-object v1

    .line 226720
    iget-object v2, p1, LX/1bf;->m:LX/33B;

    move-object v2, v2

    .line 226721
    if-eqz v2, :cond_0

    .line 226722
    invoke-static {v0, v1}, LX/1HJ;->f(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v1

    .line 226723
    :cond_0
    move-object v0, v1

    .line 226724
    invoke-direct {p0, v0, p1, p3, p2}, LX/1HI;->a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;)LX/1ca;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 226725
    :goto_0
    return-object v0

    .line 226726
    :catch_0
    move-exception v0

    .line 226727
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 226714
    invoke-virtual {p0}, LX/1HI;->a()V

    .line 226715
    iget-object v0, p0, LX/1HI;->g:LX/1HY;

    invoke-virtual {v0}, LX/1HY;->a()LX/1eg;

    .line 226716
    iget-object v0, p0, LX/1HI;->h:LX/1HY;

    invoke-virtual {v0}, LX/1HY;->a()LX/1eg;

    .line 226717
    return-void
.end method

.method public final b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 226709
    if-nez p1, :cond_0

    .line 226710
    const/4 v0, 0x0

    .line 226711
    :goto_0
    return v0

    .line 226712
    :cond_0
    invoke-direct {p0, p1}, LX/1HI;->e(Landroid/net/Uri;)Lcom/android/internal/util/Predicate;

    move-result-object v0

    .line 226713
    iget-object v1, p0, LX/1HI;->e:LX/1Fh;

    invoke-interface {v1, v0}, LX/1Fh;->b(Lcom/android/internal/util/Predicate;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(LX/1bf;Ljava/lang/Object;)LX/1ca;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 226696
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 226697
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226698
    :try_start_0
    iget-object v0, p0, LX/1HI;->b:LX/1HJ;

    invoke-virtual {v0, p1}, LX/1HJ;->a(LX/1bf;)LX/1cF;

    move-result-object v0

    .line 226699
    iget-object v1, p1, LX/1bf;->h:LX/1o9;

    move-object v1, v1

    .line 226700
    if-eqz v1, :cond_0

    .line 226701
    invoke-static {p1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v1

    const/4 v2, 0x0

    .line 226702
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 226703
    move-object v1, v1

    .line 226704
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object p1

    .line 226705
    :cond_0
    sget-object v1, LX/1bY;->FULL_FETCH:LX/1bY;

    invoke-direct {p0, v0, p1, v1, p2}, LX/1HI;->a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;)LX/1ca;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 226706
    :goto_0
    return-object v0

    .line 226707
    :catch_0
    move-exception v0

    .line 226708
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 226684
    sget-object v0, LX/1bb;->SMALL:LX/1bb;

    invoke-direct {p0, p1, v0}, LX/1HI;->a(Landroid/net/Uri;LX/1bb;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/1bb;->DEFAULT:LX/1bb;

    invoke-direct {p0, p1, v0}, LX/1HI;->a(Landroid/net/Uri;LX/1bb;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(LX/1bf;Ljava/lang/Object;)LX/1ca;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226674
    iget-object v0, p0, LX/1HI;->d:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226675
    sget-object v0, LX/1HI;->a:Ljava/util/concurrent/CancellationException;

    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    .line 226676
    :goto_0
    return-object v0

    .line 226677
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1HI;->k:LX/1Gd;

    invoke-interface {v0}, LX/1Gd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1HI;->b:LX/1HJ;

    invoke-virtual {v0, p1}, LX/1HJ;->b(LX/1bf;)LX/1cF;

    move-result-object v1

    .line 226678
    :goto_1
    sget-object v3, LX/1bY;->FULL_FETCH:LX/1bY;

    sget-object v5, LX/1bc;->MEDIUM:LX/1bc;

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LX/1HI;->a(LX/1cF;LX/1bf;LX/1bY;Ljava/lang/Object;LX/1bc;)LX/1ca;

    move-result-object v0

    goto :goto_0

    .line 226679
    :cond_1
    iget-object v0, p0, LX/1HI;->b:LX/1HJ;

    .line 226680
    invoke-static {v0, p1}, LX/1HJ;->f(LX/1HJ;LX/1bf;)LX/1cF;

    move-result-object v1

    invoke-static {v0, v1}, LX/1HJ;->g(LX/1HJ;LX/1cF;)LX/1cF;

    move-result-object v1

    move-object v1, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226681
    goto :goto_1

    .line 226682
    :catch_0
    move-exception v0

    .line 226683
    invoke-static {v0}, LX/49D;->a(Ljava/lang/Throwable;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Landroid/net/Uri;)LX/1ca;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/1ca",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226668
    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 226669
    iget-object v1, p0, LX/1HI;->i:LX/1Ao;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/1Ao;->c(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v1

    .line 226670
    invoke-static {}, LX/49E;->h()LX/49E;

    move-result-object v2

    .line 226671
    iget-object v3, p0, LX/1HI;->g:LX/1HY;

    invoke-virtual {v3, v1}, LX/1HY;->b(LX/1bh;)LX/1eg;

    move-result-object v3

    new-instance p1, LX/3Fi;

    invoke-direct {p1, p0, v1}, LX/3Fi;-><init>(LX/1HI;LX/1bh;)V

    invoke-virtual {v3, p1}, LX/1eg;->b(LX/1ex;)LX/1eg;

    move-result-object v1

    new-instance v3, LX/4dw;

    invoke-direct {v3, p0, v2}, LX/4dw;-><init>(LX/1HI;LX/49E;)V

    invoke-virtual {v1, v3}, LX/1eg;->a(LX/1ex;)LX/1eg;

    .line 226672
    move-object v0, v2

    .line 226673
    return-object v0
.end method

.method public final e(LX/1bf;Ljava/lang/Object;)LX/1ca;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "Ljava/lang/Object;",
            ")",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226667
    sget-object v0, LX/1bc;->MEDIUM:LX/1bc;

    invoke-virtual {p0, p1, p2, v0}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;LX/1bc;)LX/1ca;

    move-result-object v0

    return-object v0
.end method
