.class public LX/0ZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0VI;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2C1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2C1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83239
    iput-object p1, p0, LX/0ZM;->a:LX/0Ot;

    .line 83240
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83241
    const-string v0, "mqtt_client_nonsticky_subscription_data"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 83242
    iget-object v0, p0, LX/0ZM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2C1;

    invoke-virtual {v0}, LX/2C1;->b()Ljava/util/Set;

    move-result-object v0

    .line 83243
    if-nez v0, :cond_0

    .line 83244
    const-string v0, "No currently active non-sticky topics"

    .line 83245
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "Currently active non-sticky topics : %s"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
