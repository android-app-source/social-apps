.class public LX/0Yd;
.super LX/0Ye;
.source ""


# instance fields
.field private final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/0YZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0YZ;)V
    .locals 3

    .prologue
    .line 81792
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0Ye;-><init>(LX/0Sk;)V

    .line 81793
    new-instance v0, LX/01J;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/0Yd;->a:LX/01J;

    .line 81794
    iget-object v0, p0, LX/0Yd;->a:LX/01J;

    const-string v1, "Action is null"

    invoke-static {p1, v1}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "Receiver is null"

    invoke-static {p2, v2}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81795
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0Yd;->b:Ljava/util/Set;

    .line 81796
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "LX/0YZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81790
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/0Yd;-><init>(Ljava/util/Map;LX/0Sk;)V

    .line 81791
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;LX/0Sk;)V
    .locals 4
    .param p2    # LX/0Sk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "LX/0YZ;",
            ">;",
            "LX/0Sk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81767
    invoke-direct {p0, p2}, LX/0Ye;-><init>(LX/0Sk;)V

    .line 81768
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81769
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must include an entry for at least one action"

    invoke-static {v0, v1}, LX/03g;->b(ZLjava/lang/Object;)V

    .line 81770
    new-instance v0, LX/01J;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/0Yd;->a:LX/01J;

    .line 81771
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81772
    iget-object v2, p0, LX/0Yd;->a:LX/01J;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 81773
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81774
    :cond_1
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/0Yd;->b:Ljava/util/Set;

    .line 81775
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Landroid/content/Intent;)LX/0YZ;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 81784
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 81785
    const/4 v0, 0x0

    .line 81786
    if-eqz v1, :cond_0

    .line 81787
    iget-object v0, p0, LX/0Yd;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0YZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81788
    :cond_0
    monitor-exit p0

    return-object v0

    .line 81789
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Landroid/content/IntentFilter;
    .locals 4

    .prologue
    .line 81777
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Yd;->c:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 81778
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, LX/0Yd;->c:Landroid/content/IntentFilter;

    .line 81779
    const/4 v0, 0x0

    iget-object v1, p0, LX/0Yd;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 81780
    iget-object v3, p0, LX/0Yd;->c:Landroid/content/IntentFilter;

    iget-object v0, p0, LX/0Yd;->a:LX/01J;

    invoke-virtual {v0, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81782
    :cond_0
    iget-object v0, p0, LX/0Yd;->c:Landroid/content/IntentFilter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 81783
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 81776
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Yd;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
