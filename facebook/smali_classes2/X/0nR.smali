.class public abstract LX/0nR;
.super LX/0nS;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;>;>;"
        }
    .end annotation
.end field


# instance fields
.field public final _factoryConfig:LX/0nY;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 135691
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0nR;->a:Ljava/util/HashMap;

    .line 135692
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/0nR;->b:Ljava/util/HashMap;

    .line 135693
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    const-class v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/StringSerializer;

    invoke-direct {v2}, Lcom/fasterxml/jackson/databind/ser/std/StringSerializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135694
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;

    .line 135695
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135696
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135697
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/lang/Character;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135698
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    sget-object v2, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135699
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    invoke-static {v0}, LX/0nT;->a(Ljava/util/Map;)V

    .line 135700
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135701
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/fasterxml/jackson/databind/ser/std/BooleanSerializer;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135702
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$NumberSerializer;

    invoke-direct {v0}, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$NumberSerializer;-><init>()V

    .line 135703
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/math/BigInteger;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135704
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135705
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    const-class v1, Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135706
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/DateSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/DateSerializer;

    .line 135707
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135708
    sget-object v1, LX/0nR;->a:Ljava/util/HashMap;

    const-class v2, Ljava/sql/Timestamp;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135709
    sget-object v0, LX/0nR;->b:Ljava/util/HashMap;

    const-class v1, Ljava/sql/Date;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/fasterxml/jackson/databind/ser/std/SqlDateSerializer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135710
    sget-object v0, LX/0nR;->b:Ljava/util/HashMap;

    const-class v1, Ljava/sql/Time;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/fasterxml/jackson/databind/ser/std/SqlTimeSerializer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135711
    invoke-static {}, LX/0nV;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 135712
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 135713
    instance-of v3, v1, Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v3, :cond_0

    .line 135714
    sget-object v3, LX/0nR;->a:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 135715
    :cond_0
    instance-of v3, v1, Ljava/lang/Class;

    if-eqz v3, :cond_1

    .line 135716
    check-cast v1, Ljava/lang/Class;

    .line 135717
    sget-object v3, LX/0nR;->b:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 135718
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Internal error: unrecognized value of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135719
    :cond_2
    sget-object v0, LX/0nR;->b:Ljava/util/HashMap;

    const-class v1, LX/0nW;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/fasterxml/jackson/databind/ser/std/TokenBufferSerializer;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135720
    return-void
.end method

.method public constructor <init>(LX/0nY;)V
    .locals 0

    .prologue
    .line 135721
    invoke-direct {p0}, LX/0nS;-><init>()V

    .line 135722
    if-nez p1, :cond_0

    new-instance p1, LX/0nY;

    invoke-direct {p1}, LX/0nY;-><init>()V

    :cond_0
    iput-object p1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    .line 135723
    return-void
.end method

.method public static a(LX/0m2;LX/0lO;LX/0lJ;)LX/0lJ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lJ;",
            ">(",
            "LX/0m2;",
            "LX/0lO;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 135724
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0lU;->i(LX/0lO;)Ljava/lang/Class;

    move-result-object v0

    .line 135725
    if-eqz v0, :cond_0

    .line 135726
    :try_start_0
    invoke-virtual {p2, v0}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 135727
    :cond_0
    invoke-static {p0, p1, p2}, LX/0nR;->b(LX/0m2;LX/0lO;LX/0lJ;)LX/0lJ;

    move-result-object v0

    return-object v0

    .line 135728
    :catch_0
    move-exception v1

    .line 135729
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to widen type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with concrete-type annotation (value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "), method \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\': "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135730
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135731
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 135732
    sget-object v0, LX/0nR;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 135733
    if-nez v0, :cond_0

    .line 135734
    sget-object v2, LX/0nR;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 135735
    if-eqz v1, :cond_0

    .line 135736
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135737
    :cond_0
    return-object v0

    .line 135738
    :catch_0
    move-exception v0

    .line 135739
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to instantiate standard serializer (of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135740
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    .line 135741
    if-nez v0, :cond_0

    .line 135742
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    .line 135743
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v1

    .line 135744
    invoke-static {p1, p3, v1}, LX/0nR;->a(LX/0m2;LX/0lS;LX/4qz;)Z

    move-result v2

    invoke-static {v0, v2, v1}, LX/4rZ;->a(LX/0lJ;ZLX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0m2;LX/1Xn;LX/0lS;ZLcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/1Xn;",
            "LX/0lS;",
            "Z",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 135745
    invoke-virtual {p0}, LX/0nR;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v1

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nZ;

    .line 135746
    invoke-interface {v0}, LX/0nZ;->d()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135747
    if-eqz v0, :cond_0

    .line 135748
    :cond_1
    if-nez v0, :cond_2

    .line 135749
    const-class v0, Ljava/util/EnumMap;

    .line 135750
    iget-object v2, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 135751
    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135752
    invoke-virtual {p2}, LX/0lJ;->q()LX/0lJ;

    move-result-object v0

    .line 135753
    invoke-virtual {v0}, LX/0lJ;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 135754
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 135755
    invoke-virtual {p1}, LX/0m4;->a()LX/0lU;

    move-result-object v1

    invoke-static {v0, v1}, LX/2BV;->b(Ljava/lang/Class;LX/0lU;)LX/2BV;

    move-result-object v3

    .line 135756
    :goto_0
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;

    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    move v2, p4

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/std/EnumMapSerializer;-><init>(LX/0lJ;ZLX/2BV;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 135757
    :cond_2
    :goto_1
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 135758
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135759
    move-object v0, v0

    .line 135760
    goto :goto_2

    .line 135761
    :cond_3
    invoke-virtual {p1}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    invoke-virtual {p3}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lU;->b(LX/0lO;)[Ljava/lang/String;

    move-result-object v0

    move-object v1, p2

    move v2, p4

    move-object v3, p6

    move-object v4, p5

    move-object v5, p7

    invoke-static/range {v0 .. v5}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a([Ljava/lang/String;LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    move-result-object v0

    goto :goto_1

    .line 135762
    :cond_4
    return-object v0

    :cond_5
    move-object v3, v1

    goto :goto_0
.end method

.method private a(LX/0m2;LX/267;LX/0lS;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/267;",
            "LX/0lS;",
            "Z",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 135763
    invoke-virtual {p0}, LX/0nR;->a()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v1

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nZ;

    .line 135764
    invoke-interface {v0}, LX/0nZ;->b()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135765
    if-eqz v0, :cond_0

    .line 135766
    :cond_1
    if-nez v0, :cond_3

    .line 135767
    invoke-virtual {p3, v1}, LX/0lS;->a(LX/4pN;)LX/4pN;

    move-result-object v2

    .line 135768
    if-eqz v2, :cond_2

    .line 135769
    iget-object v3, v2, LX/4pN;->b:LX/2BA;

    move-object v2, v3

    .line 135770
    sget-object v3, LX/2BA;->OBJECT:LX/2BA;

    if-ne v2, v3, :cond_2

    .line 135771
    :goto_0
    return-object v1

    .line 135772
    :cond_2
    iget-object v2, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v2, v2

    .line 135773
    const-class v3, Ljava/util/EnumSet;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 135774
    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    .line 135775
    invoke-virtual {v0}, LX/0lJ;->h()Z

    move-result v2

    if-nez v2, :cond_b

    .line 135776
    :goto_1
    invoke-static {v1}, LX/4rZ;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135777
    :cond_3
    :goto_2
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 135778
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135779
    move-object v0, v0

    .line 135780
    goto :goto_3

    .line 135781
    :cond_4
    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    .line 135782
    iget-object v3, v1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v3

    .line 135783
    invoke-static {v2}, LX/0nR;->a(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 135784
    const-class v2, Ljava/lang/String;

    if-ne v1, v2, :cond_7

    .line 135785
    if-eqz p6, :cond_5

    invoke-static {p6}, LX/1Xw;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135786
    :cond_5
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;->a:Lcom/fasterxml/jackson/databind/ser/impl/IndexedStringListSerializer;

    .line 135787
    :cond_6
    :goto_4
    if-nez v0, :cond_3

    .line 135788
    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-static {v0, p4, p5, p6}, LX/4rZ;->b(LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    move-result-object v0

    goto :goto_2

    .line 135789
    :cond_7
    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v0

    invoke-static {v0, p4, p5, p6}, LX/4rZ;->a(LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    move-result-object v0

    goto :goto_4

    .line 135790
    :cond_8
    const-class v2, Ljava/lang/String;

    if-ne v1, v2, :cond_6

    .line 135791
    if-eqz p6, :cond_9

    invoke-static {p6}, LX/1Xw;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135792
    :cond_9
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/impl/StringCollectionSerializer;->a:Lcom/fasterxml/jackson/databind/ser/impl/StringCollectionSerializer;

    goto :goto_4

    :cond_a
    move-object v1, v0

    .line 135793
    goto :goto_0

    :cond_b
    move-object v1, v0

    goto :goto_1
.end method

.method private a(LX/0m2;LX/4ra;LX/0lS;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/4ra;",
            "LX/0lS;",
            "Z",
            "LX/4qz;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135794
    const/4 v0, 0x0

    .line 135795
    invoke-virtual {p0}, LX/0nR;->a()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nZ;

    .line 135796
    invoke-interface {v0}, LX/0nZ;->a()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135797
    if-eqz v0, :cond_0

    .line 135798
    :cond_1
    if-nez v0, :cond_4

    .line 135799
    iget-object v1, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 135800
    if-eqz p6, :cond_2

    invoke-static {p6}, LX/1Xw;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 135801
    :cond_2
    const-class v0, [Ljava/lang/String;

    if-ne v0, v1, :cond_5

    .line 135802
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;->a:Lcom/fasterxml/jackson/databind/ser/impl/StringArraySerializer;

    .line 135803
    :cond_3
    :goto_0
    if-nez v0, :cond_4

    .line 135804
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;

    invoke-virtual {p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    invoke-direct {v0, v1, p4, p5, p6}, Lcom/fasterxml/jackson/databind/ser/std/ObjectArraySerializer;-><init>(LX/0lJ;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 135805
    :cond_4
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 135806
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135807
    move-object v0, v0

    .line 135808
    goto :goto_1

    .line 135809
    :cond_5
    invoke-static {v1}, LX/4rY;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0

    .line 135810
    :cond_6
    return-object v0
.end method

.method private static a(LX/0my;LX/0lO;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lO;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135811
    invoke-static {p0, p1}, LX/0nR;->b(LX/0my;LX/0lO;)LX/1Xr;

    move-result-object v1

    .line 135812
    if-nez v1, :cond_0

    .line 135813
    :goto_0
    return-object p2

    .line 135814
    :cond_0
    invoke-virtual {p0}, LX/0mz;->c()LX/0li;

    invoke-interface {v1}, LX/1Xr;->c()LX/0lJ;

    move-result-object v2

    .line 135815
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;

    invoke-direct {v0, v1, v2, p2}, Lcom/fasterxml/jackson/databind/ser/std/StdDelegatingSerializer;-><init>(LX/1Xr;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    move-object p2, v0

    goto :goto_0
.end method

.method public static a(LX/0m2;LX/0lS;LX/4qz;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 135816
    if-eqz p2, :cond_1

    .line 135817
    :cond_0
    :goto_0
    return v0

    .line 135818
    :cond_1
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v1

    .line 135819
    invoke-virtual {p1}, LX/0lS;->c()LX/0lN;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lU;->l(LX/0lO;)LX/1Xv;

    move-result-object v1

    .line 135820
    if-eqz v1, :cond_2

    .line 135821
    sget-object v2, LX/1Xv;->STATIC:LX/1Xv;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 135822
    :cond_2
    sget-object v0, LX/0m6;->USE_STATIC_TYPING:LX/0m6;

    invoke-virtual {p0, v0}, LX/0m4;->a(LX/0m6;)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 135823
    const-class v0, Ljava/util/RandomAccess;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public static b(LX/0m2;LX/0lO;LX/0lJ;)LX/0lJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lJ;",
            ">(",
            "LX/0m2;",
            "LX/0lO;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 135824
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v2

    .line 135825
    invoke-virtual {p2}, LX/0lJ;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135826
    invoke-virtual {v2, p1}, LX/0lU;->j(LX/0lO;)Ljava/lang/Class;

    move-result-object v3

    .line 135827
    if-eqz v3, :cond_1

    .line 135828
    instance-of v1, p2, LX/1Xn;

    if-nez v1, :cond_0

    .line 135829
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Illegal key-type annotation: type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a Map type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135830
    :cond_0
    :try_start_0
    move-object v0, p2

    check-cast v0, LX/1Xn;

    move-object v1, v0

    invoke-virtual {v1, v3}, LX/1Xo;->i(Ljava/lang/Class;)LX/0lJ;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 135831
    :cond_1
    invoke-virtual {v2, p1}, LX/0lU;->k(LX/0lO;)Ljava/lang/Class;

    move-result-object v1

    .line 135832
    if-eqz v1, :cond_2

    .line 135833
    :try_start_1
    invoke-virtual {p2, v1}, LX/0lJ;->f(Ljava/lang/Class;)LX/0lJ;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object p2

    .line 135834
    :cond_2
    return-object p2

    .line 135835
    :catch_0
    move-exception v1

    .line 135836
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to narrow key type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with key-type annotation ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 135837
    :catch_1
    move-exception v2

    .line 135838
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to narrow content type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with content-type annotation ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "): "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private static b(LX/0my;LX/0lO;)LX/1Xr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lO;",
            ")",
            "LX/1Xr",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135687
    invoke-virtual {p0}, LX/0my;->e()LX/0lU;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0lU;->m(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135688
    if-nez v0, :cond_0

    .line 135689
    const/4 v0, 0x0

    .line 135690
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0mz;->a(LX/0lO;Ljava/lang/Object;)LX/1Xr;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135547
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v0

    .line 135548
    if-nez v0, :cond_0

    .line 135549
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    .line 135550
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v1

    .line 135551
    invoke-static {p1, p3, v1}, LX/0nR;->a(LX/0m2;LX/0lS;LX/4qz;)Z

    move-result v2

    invoke-static {v0, v2, v1}, LX/4rZ;->b(LX/0lJ;ZLX/4qz;)Lcom/fasterxml/jackson/databind/ser/ContainerSerializer;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135839
    sget-object v0, LX/2Vf;->a:LX/2Vf;

    .line 135840
    iget-object v1, p0, LX/0my;->_config:LX/0m2;

    move-object v1, v1

    .line 135841
    invoke-virtual {v0, v1, p1, p2}, LX/2Vf;->a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    return-object v0
.end method

.method private c(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135552
    invoke-virtual {p3, v0}, LX/0lS;->a(LX/4pN;)LX/4pN;

    move-result-object v1

    .line 135553
    if-eqz v1, :cond_1

    .line 135554
    iget-object v2, v1, LX/4pN;->b:LX/2BA;

    move-object v2, v2

    .line 135555
    sget-object v3, LX/2BA;->OBJECT:LX/2BA;

    if-ne v2, v3, :cond_1

    .line 135556
    check-cast p3, LX/0lR;

    const-string v1, "declaringClass"

    invoke-virtual {p3, v1}, LX/0lR;->a(Ljava/lang/String;)Z

    .line 135557
    :cond_0
    return-object v0

    .line 135558
    :cond_1
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135559
    invoke-static {v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;->a(Ljava/lang/Class;LX/0m2;LX/4pN;)Lcom/fasterxml/jackson/databind/ser/std/EnumSerializer;

    move-result-object v0

    .line 135560
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135561
    iget-object v1, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v1}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135562
    move-object v0, v0

    .line 135563
    goto :goto_0
.end method

.method private static c(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lO;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135564
    invoke-virtual {p0}, LX/0my;->e()LX/0lU;

    move-result-object v0

    .line 135565
    invoke-virtual {v0, p1}, LX/0lU;->g(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135566
    if-eqz v0, :cond_0

    .line 135567
    invoke-virtual {p0, p1, v0}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135568
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lO;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135569
    invoke-virtual {p0}, LX/0my;->e()LX/0lU;

    move-result-object v0

    .line 135570
    invoke-virtual {v0, p1}, LX/0lU;->h(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135571
    if-eqz v0, :cond_0

    .line 135572
    invoke-virtual {p0, p1, v0}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135573
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/0nY;)LX/0nS;
.end method

.method public final a(LX/0nZ;)LX/0nS;
    .locals 1

    .prologue
    .line 135574
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0, p1}, LX/0nY;->a(LX/0nZ;)LX/0nY;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nR;->a(LX/0nY;)LX/0nS;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0na;)LX/0nS;
    .locals 1

    .prologue
    .line 135575
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0, p1}, LX/0nY;->a(LX/0na;)LX/0nY;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nR;->a(LX/0nY;)LX/0nS;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0m2;LX/0lJ;)LX/4qz;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 135576
    iget-object v1, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 135577
    invoke-virtual {p1, v1}, LX/0m4;->c(Ljava/lang/Class;)LX/0lS;

    move-result-object v1

    .line 135578
    invoke-virtual {v1}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    .line 135579
    invoke-virtual {p1}, LX/0m4;->a()LX/0lU;

    move-result-object v3

    .line 135580
    invoke-virtual {v3, p1, v1, p2}, LX/0lU;->a(LX/0m4;LX/0lN;LX/0lJ;)LX/4qy;

    move-result-object v2

    .line 135581
    if-nez v2, :cond_0

    .line 135582
    invoke-virtual {p1}, LX/0m4;->m()LX/4qy;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 135583
    :goto_0
    if-nez v2, :cond_1

    .line 135584
    :goto_1
    return-object v0

    .line 135585
    :cond_0
    iget-object v4, p1, LX/0m3;->_subtypeResolver:LX/0m0;

    move-object v4, v4

    .line 135586
    invoke-virtual {v4, v1, p1, v3}, LX/0m0;->a(LX/0lN;LX/0m4;LX/0lU;)Ljava/util/Collection;

    move-result-object v1

    goto :goto_0

    .line 135587
    :cond_1
    invoke-interface {v2, p1, p2, v1}, LX/4qy;->a(LX/0m2;LX/0lJ;Ljava/util/Collection;)LX/4qz;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/0m2;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "LX/0lS;",
            "Z)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135588
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135589
    const-class v1, Ljava/util/Iterator;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135590
    invoke-direct {p0, p1, p2, p3}, LX/0nR;->a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135591
    :goto_0
    return-object v0

    .line 135592
    :cond_0
    const-class v1, Ljava/lang/Iterable;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135593
    invoke-direct {p0, p1, p2, p3}, LX/0nR;->b(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0

    .line 135594
    :cond_1
    const-class v1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135595
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;

    goto :goto_0

    .line 135596
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0m2;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135597
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135598
    invoke-virtual {p1, v0}, LX/0m4;->c(Ljava/lang/Class;)LX/0lS;

    move-result-object v1

    .line 135599
    const/4 v0, 0x0

    .line 135600
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135601
    iget-object v2, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->d()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nZ;

    .line 135602
    invoke-interface {v0, p1, p2, v1}, LX/0nZ;->a(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135603
    if-eqz v0, :cond_0

    .line 135604
    :cond_1
    if-nez v0, :cond_4

    .line 135605
    if-nez p3, :cond_2

    .line 135606
    invoke-static {p2}, LX/2SF;->a(LX/0lJ;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object p3

    .line 135607
    :cond_2
    :goto_0
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0}, LX/0nY;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135608
    iget-object v0, p0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v0}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135609
    move-object p3, p3

    .line 135610
    goto :goto_1

    .line 135611
    :cond_3
    return-object p3

    :cond_4
    move-object p3, v0

    goto :goto_0
.end method

.method public final a(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135612
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 135613
    const-class v1, LX/0gT;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135614
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/SerializableSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/SerializableSerializer;

    .line 135615
    :goto_0
    return-object v0

    .line 135616
    :cond_0
    invoke-virtual {p3}, LX/0lS;->p()LX/2At;

    move-result-object v0

    .line 135617
    if-eqz v0, :cond_2

    .line 135618
    iget-object v1, v0, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v1, v1

    .line 135619
    invoke-virtual {p1}, LX/0mz;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135620
    invoke-static {v1}, LX/1Xw;->a(Ljava/lang/reflect/Member;)V

    .line 135621
    :cond_1
    invoke-virtual {p0, p1, v0}, LX/0nR;->a(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    .line 135622
    new-instance v0, Lcom/fasterxml/jackson/databind/ser/std/JsonValueSerializer;

    invoke-direct {v0, v1, v2}, Lcom/fasterxml/jackson/databind/ser/std/JsonValueSerializer;-><init>(Ljava/lang/reflect/Method;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    goto :goto_0

    .line 135623
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            "Z)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135624
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v0

    .line 135625
    const-class v0, Ljava/net/InetAddress;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135626
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/InetAddressSerializer;

    .line 135627
    :cond_0
    :goto_0
    return-object v0

    .line 135628
    :cond_1
    const-class v0, Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 135629
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/TimeZoneSerializer;

    goto :goto_0

    .line 135630
    :cond_2
    const-class v0, Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135631
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/ToStringSerializer;

    goto :goto_0

    .line 135632
    :cond_3
    invoke-static {p1, p2, p3}, LX/0nR;->b(LX/0my;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135633
    if-nez v0, :cond_0

    .line 135634
    const-class v0, Ljava/lang/Number;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135635
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$NumberSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/NumberSerializers$NumberSerializer;

    goto :goto_0

    .line 135636
    :cond_4
    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 135637
    iget-object v0, p1, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 135638
    invoke-direct {p0, v0, p2, p3}, LX/0nR;->c(LX/0m2;LX/0lJ;LX/0lS;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0

    .line 135639
    :cond_5
    const-class v0, Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135640
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/CalendarSerializer;

    goto :goto_0

    .line 135641
    :cond_6
    const-class v0, Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 135642
    sget-object v0, Lcom/fasterxml/jackson/databind/ser/std/DateSerializer;->a:Lcom/fasterxml/jackson/databind/ser/std/DateSerializer;

    goto :goto_0

    .line 135643
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lO;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135644
    invoke-virtual {p1}, LX/0my;->e()LX/0lU;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0lU;->f(LX/0lO;)Ljava/lang/Object;

    move-result-object v0

    .line 135645
    if-nez v0, :cond_0

    .line 135646
    const/4 v0, 0x0

    .line 135647
    :goto_0
    return-object v0

    .line 135648
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/0my;->b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 135649
    invoke-static {p1, p2, v0}, LX/0nR;->a(LX/0my;LX/0lO;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract a()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/0nZ;",
            ">;"
        }
    .end annotation
.end method

.method public final b(LX/0my;LX/0lJ;LX/0lS;Z)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0my;",
            "LX/0lJ;",
            "LX/0lS;",
            "Z)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 135650
    invoke-virtual/range {p1 .. p1}, LX/0my;->d()LX/0m2;

    move-result-object v2

    .line 135651
    if-nez p4, :cond_1

    invoke-virtual/range {p2 .. p2}, LX/0lJ;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135652
    invoke-virtual/range {p2 .. p2}, LX/0lJ;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p2 .. p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    invoke-virtual {v1}, LX/0lJ;->c()Ljava/lang/Class;

    move-result-object v1

    const-class v3, Ljava/lang/Object;

    if-eq v1, v3, :cond_1

    .line 135653
    :cond_0
    const/16 p4, 0x1

    .line 135654
    :cond_1
    invoke-virtual/range {p2 .. p2}, LX/0lJ;->r()LX/0lJ;

    move-result-object v1

    .line 135655
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, LX/0nS;->a(LX/0m2;LX/0lJ;)LX/4qz;

    move-result-object v7

    .line 135656
    if-eqz v7, :cond_c

    .line 135657
    const/4 v5, 0x0

    .line 135658
    :goto_0
    invoke-virtual/range {p3 .. p3}, LX/0lS;->c()LX/0lN;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, LX/0nR;->d(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v8

    .line 135659
    invoke-virtual/range {p2 .. p2}, LX/0lJ;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v1, p2

    .line 135660
    check-cast v1, LX/1Xo;

    .line 135661
    invoke-virtual/range {p3 .. p3}, LX/0lS;->c()LX/0lN;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/0nR;->c(LX/0my;LX/0lO;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v6

    .line 135662
    invoke-virtual {v1}, LX/1Xo;->x()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v3, v1

    .line 135663
    check-cast v3, LX/1Xn;

    move-object/from16 v1, p0

    move-object/from16 v4, p3

    invoke-direct/range {v1 .. v8}, LX/0nR;->a(LX/0m2;LX/1Xn;LX/0lS;ZLcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 135664
    :cond_2
    :goto_1
    return-object v1

    .line 135665
    :cond_3
    invoke-virtual/range {p0 .. p0}, LX/0nR;->a()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0nZ;

    move-object/from16 v11, p2

    .line 135666
    check-cast v11, LX/1Xo;

    move-object v10, v2

    move-object/from16 v12, p3

    move-object v13, v6

    move-object v14, v7

    move-object v15, v8

    .line 135667
    invoke-interface/range {v9 .. v15}, LX/0nZ;->a(LX/0m2;LX/1Xo;LX/0lS;Lcom/fasterxml/jackson/databind/JsonSerializer;LX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 135668
    if-eqz v1, :cond_4

    .line 135669
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135670
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135671
    invoke-static {v1}, LX/0na;->f(Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    goto :goto_2

    .line 135672
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 135673
    :cond_6
    invoke-virtual/range {p2 .. p2}, LX/0lJ;->m()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 135674
    check-cast p2, LX/268;

    .line 135675
    invoke-virtual/range {p2 .. p2}, LX/268;->x()Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v11, p2

    .line 135676
    check-cast v11, LX/267;

    move-object/from16 v9, p0

    move-object v10, v2

    move-object/from16 v12, p3

    move v13, v5

    move-object v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, LX/0nR;->a(LX/0m2;LX/267;LX/0lS;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    goto :goto_1

    .line 135677
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/0nR;->a()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0nZ;

    .line 135678
    invoke-interface {v1}, LX/0nZ;->c()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 135679
    if-eqz v1, :cond_8

    .line 135680
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 135681
    move-object/from16 v0, p0

    iget-object v2, v0, LX/0nR;->_factoryConfig:LX/0nY;

    invoke-virtual {v2}, LX/0nY;->e()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 135682
    invoke-static {v1}, LX/0na;->d(Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    goto :goto_3

    .line 135683
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 135684
    :cond_a
    invoke-virtual/range {p2 .. p2}, LX/0lJ;->g()Z

    move-result v1

    if-eqz v1, :cond_b

    move-object/from16 v11, p2

    .line 135685
    check-cast v11, LX/4ra;

    move-object/from16 v9, p0

    move-object v10, v2

    move-object/from16 v12, p3

    move v13, v5

    move-object v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, LX/0nR;->a(LX/0m2;LX/4ra;LX/0lS;ZLX/4qz;Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    goto/16 :goto_1

    .line 135686
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_c
    move/from16 v5, p4

    goto/16 :goto_0
.end method
