.class public LX/1XK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pe;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zX;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1XK",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1zX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 270971
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 270972
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1XK;->b:LX/0Zi;

    .line 270973
    iput-object p1, p0, LX/1XK;->a:LX/0Ot;

    .line 270974
    return-void
.end method

.method public static a(LX/0QB;)LX/1XK;
    .locals 4

    .prologue
    .line 270960
    const-class v1, LX/1XK;

    monitor-enter v1

    .line 270961
    :try_start_0
    sget-object v0, LX/1XK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 270962
    sput-object v2, LX/1XK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 270963
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270964
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 270965
    new-instance v3, LX/1XK;

    const/16 p0, 0x854

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1XK;-><init>(LX/0Ot;)V

    .line 270966
    move-object v0, v3

    .line 270967
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 270968
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1XK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270969
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 270970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270959
    const v0, -0xb02b30c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 270929
    check-cast p2, LX/1XL;

    .line 270930
    iget-object v0, p0, LX/1XK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/1XL;->a:LX/1X1;

    .line 270931
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p0

    .line 270932
    const p2, -0xb02b30c

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 270933
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 270934
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 270943
    invoke-static {}, LX/1dS;->b()V

    .line 270944
    iget v0, p1, LX/1dQ;->b:I

    .line 270945
    packed-switch v0, :pswitch_data_0

    .line 270946
    :goto_0
    return-object v2

    .line 270947
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 270948
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 270949
    check-cast v1, LX/1XL;

    .line 270950
    iget-object v3, p0, LX/1XK;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1zX;

    iget-boolean v4, v1, LX/1XL;->b:Z

    iget-object p1, v1, LX/1XL;->c:LX/1Po;

    iget-object p2, v1, LX/1XL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 270951
    if-nez v4, :cond_0

    .line 270952
    invoke-static {v3, p2, v0, p1}, LX/1zX;->a(LX/1zX;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1Po;)V

    .line 270953
    :goto_1
    goto :goto_0

    .line 270954
    :cond_0
    iget-object p0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 270955
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0}, LX/14w;->q(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 270956
    if-eqz v1, :cond_1

    move-object p0, p1

    check-cast p0, LX/1Pe;

    invoke-interface {p0, v1}, LX/1Pe;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 270957
    :cond_1
    invoke-static {v3, p2, v0, p1}, LX/1zX;->a(LX/1zX;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1Po;)V

    goto :goto_1

    .line 270958
    :cond_2
    check-cast p1, LX/1Pe;

    invoke-interface {p1, v1}, LX/1Pe;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0xb02b30c
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1XM;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1XK",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 270935
    new-instance v1, LX/1XL;

    invoke-direct {v1, p0}, LX/1XL;-><init>(LX/1XK;)V

    .line 270936
    iget-object v2, p0, LX/1XK;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1XM;

    .line 270937
    if-nez v2, :cond_0

    .line 270938
    new-instance v2, LX/1XM;

    invoke-direct {v2, p0}, LX/1XM;-><init>(LX/1XK;)V

    .line 270939
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1XM;->a$redex0(LX/1XM;LX/1De;IILX/1XL;)V

    .line 270940
    move-object v1, v2

    .line 270941
    move-object v0, v1

    .line 270942
    return-object v0
.end method
