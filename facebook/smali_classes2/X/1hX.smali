.class public LX/1hX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/AsyncTCPProbeCallback;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0

    .prologue
    .line 296105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296106
    iput-object p1, p0, LX/1hX;->a:LX/0Zb;

    .line 296107
    return-void
.end method


# virtual methods
.method public final onProbeResults([Lcom/facebook/proxygen/AsyncTCPProbeResult;)V
    .locals 5

    .prologue
    .line 296108
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 296109
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 296110
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "address_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    .line 296111
    iget-object v4, v3, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mHostAndPort:Ljava/lang/String;

    move-object v3, v4

    .line 296112
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296113
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "region_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    .line 296114
    iget-object v4, v3, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRegion:Ljava/lang/String;

    move-object v3, v4

    .line 296115
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296116
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "latency_mean_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    .line 296117
    iget v4, v3, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTInMs:I

    move v3, v4

    .line 296118
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296119
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "latency_stddev_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    .line 296120
    iget v4, v3, Lcom/facebook/proxygen/AsyncTCPProbeResult;->mRTTStdDevInMs:I

    move v3, v4

    .line 296121
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296123
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "async_tcp_probe"

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 296124
    const-string v2, "latency_stats"

    .line 296125
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 296126
    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 296127
    iget-object v1, p0, LX/1hX;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 296128
    return-void
.end method
