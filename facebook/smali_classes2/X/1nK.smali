.class public LX/1nK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1nK;


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0YP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 315939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315940
    iput-boolean v0, p0, LX/1nK;->e:Z

    .line 315941
    iput-boolean v0, p0, LX/1nK;->f:Z

    .line 315942
    iput-object p1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 315943
    iput-object p2, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 315944
    const v0, 0x390008

    invoke-virtual {p3, v0}, LX/0YP;->a(I)V

    .line 315945
    const v0, 0x39001f

    invoke-virtual {p3, v0}, LX/0YP;->a(I)V

    .line 315946
    const v0, 0x390019

    invoke-virtual {p3, v0}, LX/0YP;->a(I)V

    .line 315947
    return-void
.end method

.method private static O(LX/1nK;)V
    .locals 3

    .prologue
    .line 315948
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x390008

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 315949
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nK;->d:Z

    .line 315950
    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 315951
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390016

    const-string v2, "NNF_FlyoutLoadResumeToRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315952
    return-void
.end method

.method private V()V
    .locals 3

    .prologue
    .line 315953
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390018

    const-string v2, "NNF_FlyoutLoadCompleteFlowToRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315954
    return-void
.end method

.method private W()V
    .locals 3

    .prologue
    .line 315955
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390018

    const-string v2, "NNF_FlyoutLoadCompleteFlowToRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315956
    return-void
.end method

.method public static a(LX/0QB;)LX/1nK;
    .locals 6

    .prologue
    .line 315957
    sget-object v0, LX/1nK;->g:LX/1nK;

    if-nez v0, :cond_1

    .line 315958
    const-class v1, LX/1nK;

    monitor-enter v1

    .line 315959
    :try_start_0
    sget-object v0, LX/1nK;->g:LX/1nK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 315960
    if-eqz v2, :cond_0

    .line 315961
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 315962
    new-instance p0, LX/1nK;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0YP;->a(LX/0QB;)LX/0YP;

    move-result-object v5

    check-cast v5, LX/0YP;

    invoke-direct {p0, v3, v4, v5}, LX/1nK;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0YP;)V

    .line 315963
    move-object v0, p0

    .line 315964
    sput-object v0, LX/1nK;->g:LX/1nK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315965
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 315966
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 315967
    :cond_1
    sget-object v0, LX/1nK;->g:LX/1nK;

    return-object v0

    .line 315968
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 315969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final A()V
    .locals 3

    .prologue
    .line 315970
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001b

    const-string v2, "NNF_FlyoutLoadResumeToAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315971
    return-void
.end method

.method public final B()V
    .locals 5

    .prologue
    const v4, 0x39001c

    .line 315972
    iget-boolean v0, p0, LX/1nK;->c:Z

    if-eqz v0, :cond_0

    .line 315973
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationToDataFetchTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315974
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationToDataFetchTime"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;D)V

    .line 315975
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationToDataFetchTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315976
    :goto_0
    return-void

    .line 315977
    :cond_0
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationToDataFetchTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final C()V
    .locals 3

    .prologue
    .line 315978
    invoke-virtual {p0}, LX/1nK;->f()V

    .line 315979
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390003

    const-string v2, "NNF_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315980
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390004

    const-string v2, "NNF_FlyoutLoadNetworkAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315981
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000c

    const-string v2, "Photos_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315982
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000e

    const-string v2, "Dash_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315983
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000f

    const-string v2, "Flyout_NetworkTimeFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315984
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390010

    const-string v2, "Flyout_NetworkTimeExecutorFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315985
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390011

    const-string v2, "Flyout_NetworkTimePhotoId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315986
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390005

    const-string v2, "NNF_FlyoutLoadNetworkNoCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315987
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390006

    const-string v2, "NNF_FlyoutLoadNetworkNoCacheAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315988
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390007

    const-string v2, "NNF_FlyoutLoadCompleteFlow"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315989
    const v2, 0x390008

    .line 315990
    iget-boolean v0, p0, LX/1nK;->d:Z

    if-nez v0, :cond_1

    .line 315991
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x4

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 315992
    :goto_0
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390018

    const-string v2, "NNF_FlyoutLoadCompleteFlowToRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315993
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390009

    const-string v2, "NNF_FlyoutLoadOnCreateTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315994
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390012

    const-string v2, "NNF_FlyoutLoadOnCreateViewTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315995
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390014

    const-string v2, "NNF_FlyoutLoadOnActivityCreatedTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315996
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390015

    const-string v2, "NNF_FlyoutLoadOnResumeTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315997
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000a

    const-string v2, "NNF_FlyoutLoadFragmentCreateTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315998
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390016

    const-string v2, "NNF_FlyoutLoadResumeToRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315999
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390020

    const-string v2, "NNF_FlyoutBgInflatableFeedbackTotalTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 316000
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390021

    const-string v2, "NNF_FlyoutBgInflationTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 316001
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390017

    const-string v2, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 316002
    const v2, 0x39001f

    .line 316003
    iget-boolean v0, p0, LX/1nK;->e:Z

    if-eqz v0, :cond_0

    .line 316004
    iget-boolean v0, p0, LX/1nK;->f:Z

    if-nez v0, :cond_2

    .line 316005
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x4

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 316006
    :cond_0
    :goto_1
    return-void

    .line 316007
    :cond_1
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_0

    .line 316008
    :cond_2
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_1
.end method

.method public final E()V
    .locals 3

    .prologue
    .line 316009
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390019

    const-string v2, "UfiLoadMoreComments"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316010
    return-void
.end method

.method public final F()V
    .locals 3

    .prologue
    .line 316011
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nK;->e:Z

    .line 316012
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390002

    const-string v2, "NNF_FlyoutLoadDBCacheAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316013
    invoke-static {p0}, LX/1nK;->O(LX/1nK;)V

    .line 316014
    invoke-direct {p0}, LX/1nK;->W()V

    .line 316015
    invoke-direct {p0}, LX/1nK;->S()V

    .line 316016
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x39001f

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(I)V

    .line 316017
    return-void
.end method

.method public final G()V
    .locals 3

    .prologue
    .line 316018
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nK;->f:Z

    .line 316019
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390004

    const-string v2, "NNF_FlyoutLoadNetworkAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316020
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390006

    const-string v2, "NNF_FlyoutLoadNetworkNoCacheAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316021
    invoke-static {p0}, LX/1nK;->O(LX/1nK;)V

    .line 316022
    invoke-direct {p0}, LX/1nK;->W()V

    .line 316023
    invoke-direct {p0}, LX/1nK;->S()V

    .line 316024
    iget-boolean v0, p0, LX/1nK;->e:Z

    if-eqz v0, :cond_0

    .line 316025
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x39001f

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 316026
    :cond_0
    return-void
.end method

.method public final H()V
    .locals 3

    .prologue
    .line 316027
    iget-object v0, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x390008

    const-string v2, "comment"

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 316028
    invoke-static {p0}, LX/1nK;->O(LX/1nK;)V

    .line 316029
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001e

    const-string v2, "NNF_OptimisticComentPostTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 316030
    return-void
.end method

.method public final I()V
    .locals 3

    .prologue
    .line 316031
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001e

    const-string v2, "NNF_OptimisticComentPostTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316032
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const v7, 0x39001f

    const v6, 0x390008

    const/4 v5, 0x3

    .line 315900
    const-string v0, "exception_name"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 315901
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x390003

    const-string v4, "NNF_FlyoutLoadNetwork"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315902
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x390005

    const-string v4, "NNF_FlyoutLoadNetworkNoCache"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315903
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x390007

    const-string v4, "NNF_FlyoutLoadCompleteFlow"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315904
    iget-object v1, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "exception_name"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 315905
    iget-object v1, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v6, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 315906
    iget-boolean v1, p0, LX/1nK;->e:Z

    if-eqz v1, :cond_0

    .line 315907
    iget-object v1, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v2, "exception_name"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v7, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 315908
    iget-object v1, p0, LX/1nK;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v7, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 315909
    :cond_0
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x39000c

    const-string v4, "Photos_FlyoutLoadNetwork"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315910
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x39000e

    const-string v4, "Dash_FlyoutLoadNetwork"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315911
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x39000f

    const-string v4, "Flyout_NetworkTimeFeedbackId"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315912
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x390010

    const-string v4, "Flyout_NetworkTimeExecutorFeedbackId"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315913
    iget-object v1, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v2, LX/0Yj;

    const v3, 0x390011

    const-string v4, "Flyout_NetworkTimePhotoId"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 315914
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 316033
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000f

    const-string v2, "Flyout_NetworkTimeFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 316034
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390010

    const-string v2, "Flyout_NetworkTimeExecutorFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 316035
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 315937
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000f

    const-string v2, "Flyout_NetworkTimeFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315938
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 316036
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390001

    const-string v2, "NNF_FlyoutLoadDBCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316037
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000b

    const-string v2, "Photos_FlyoutLoadCached"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316038
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000d

    const-string v2, "Dash_FlyoutLoadCached"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316039
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390007

    const-string v2, "NNF_FlyoutLoadCompleteFlow"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316040
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nK;->c:Z

    .line 316041
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001b

    const-string v2, "NNF_FlyoutLoadResumeToAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316042
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001c

    const-string v2, "NNF_FlyoutLoadAnimationToDataFetchTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316043
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390017

    const-string v2, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 316044
    invoke-direct {p0}, LX/1nK;->V()V

    .line 316045
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 315870
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390001

    const-string v2, "NNF_FlyoutLoadDBCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315871
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390002

    const-string v2, "NNF_FlyoutLoadDBCacheAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315872
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000b

    const-string v2, "Photos_FlyoutLoadCached"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315873
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000d

    const-string v2, "Dash_FlyoutLoadCached"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315874
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 315897
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390005

    const-string v2, "NNF_FlyoutLoadNetworkNoCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315898
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390006

    const-string v2, "NNF_FlyoutLoadNetworkNoCacheAndRender"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 315899
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 315883
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390003

    const-string v2, "NNF_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315884
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000c

    const-string v2, "Photos_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315885
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000e

    const-string v2, "Dash_FlyoutLoadNetwork"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315886
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000f

    const-string v2, "Flyout_NetworkTimeFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315887
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390010

    const-string v2, "Flyout_NetworkTimeExecutorFeedbackId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315888
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390011

    const-string v2, "Flyout_NetworkTimePhotoId"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315889
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390005

    const-string v2, "NNF_FlyoutLoadNetworkNoCache"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315890
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390007

    const-string v2, "NNF_FlyoutLoadCompleteFlow"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315891
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1nK;->c:Z

    .line 315892
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001b

    const-string v2, "NNF_FlyoutLoadResumeToAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315893
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001c

    const-string v2, "NNF_FlyoutLoadAnimationToDataFetchTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315894
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390017

    const-string v2, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315895
    invoke-direct {p0}, LX/1nK;->V()V

    .line 315896
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 315881
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390009

    const-string v2, "NNF_FlyoutLoadOnCreateTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315882
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 315879
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390012

    const-string v2, "NNF_FlyoutLoadOnCreateViewTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315880
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 315877
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390012

    const-string v2, "NNF_FlyoutLoadOnCreateViewTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315878
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 315875
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390013

    const-string v2, "NNF_FlyoutLoadOnViewCreatedTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315876
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 315868
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390013

    const-string v2, "NNF_FlyoutLoadOnViewCreatedTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315869
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 315915
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390014

    const-string v2, "NNF_FlyoutLoadOnActivityCreatedTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315916
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 315917
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390014

    const-string v2, "NNF_FlyoutLoadOnActivityCreatedTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315918
    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 315919
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390015

    const-string v2, "NNF_FlyoutLoadOnResumeTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315920
    return-void
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 315921
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390015

    const-string v2, "NNF_FlyoutLoadOnResumeTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315922
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 315923
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39000a

    const-string v2, "NNF_FlyoutLoadFragmentCreateTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315924
    return-void
.end method

.method public final w()V
    .locals 3

    .prologue
    .line 315925
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390016

    const-string v2, "NNF_FlyoutLoadResumeToRenderTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315926
    return-void
.end method

.method public final x()V
    .locals 5

    .prologue
    const v4, 0x390017

    .line 315927
    iget-boolean v0, p0, LX/1nK;->d:Z

    if-eqz v0, :cond_0

    .line 315928
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315929
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationWaitTime"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v4, v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;D)V

    .line 315930
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315931
    :goto_0
    return-void

    .line 315932
    :cond_0
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v4, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 315933
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x390017

    const-string v2, "NNF_FlyoutLoadAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 315934
    return-void
.end method

.method public final z()V
    .locals 3

    .prologue
    .line 315935
    iget-object v0, p0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x39001b

    const-string v2, "NNF_FlyoutLoadResumeToAnimationWaitTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 315936
    return-void
.end method
