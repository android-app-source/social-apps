.class public abstract enum LX/0rs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rs;

.field public static final enum COLLECTED:LX/0rs;

.field public static final enum EXPIRED:LX/0rs;

.field public static final enum EXPLICIT:LX/0rs;

.field public static final enum REPLACED:LX/0rs;

.field public static final enum SIZE:LX/0rs;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 151012
    new-instance v0, LX/0rt;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, LX/0rt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rs;->EXPLICIT:LX/0rs;

    .line 151013
    new-instance v0, LX/0ru;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, LX/0ru;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rs;->REPLACED:LX/0rs;

    .line 151014
    new-instance v0, LX/0rv;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, LX/0rv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rs;->COLLECTED:LX/0rs;

    .line 151015
    new-instance v0, LX/0rw;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, LX/0rw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rs;->EXPIRED:LX/0rs;

    .line 151016
    new-instance v0, LX/0rx;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, LX/0rx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0rs;->SIZE:LX/0rs;

    .line 151017
    const/4 v0, 0x5

    new-array v0, v0, [LX/0rs;

    sget-object v1, LX/0rs;->EXPLICIT:LX/0rs;

    aput-object v1, v0, v2

    sget-object v1, LX/0rs;->REPLACED:LX/0rs;

    aput-object v1, v0, v3

    sget-object v1, LX/0rs;->COLLECTED:LX/0rs;

    aput-object v1, v0, v4

    sget-object v1, LX/0rs;->EXPIRED:LX/0rs;

    aput-object v1, v0, v5

    sget-object v1, LX/0rs;->SIZE:LX/0rs;

    aput-object v1, v0, v6

    sput-object v0, LX/0rs;->$VALUES:[LX/0rs;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 151011
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rs;
    .locals 1

    .prologue
    .line 151018
    const-class v0, LX/0rs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rs;

    return-object v0
.end method

.method public static values()[LX/0rs;
    .locals 1

    .prologue
    .line 151010
    sget-object v0, LX/0rs;->$VALUES:[LX/0rs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rs;

    return-object v0
.end method


# virtual methods
.method public abstract wasEvicted()Z
.end method
