.class public final enum LX/0gQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0gQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0gQ;

.field public static final enum DELTA:LX/0gQ;

.field public static final enum NEW:LX/0gQ;

.field public static final enum NONE:LX/0gQ;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 111954
    new-instance v0, LX/0gQ;

    const-string v1, "NONE"

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2}, LX/0gQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gQ;->NONE:LX/0gQ;

    .line 111955
    new-instance v0, LX/0gQ;

    const-string v1, "NEW"

    const-string v2, "new"

    invoke-direct {v0, v1, v4, v2}, LX/0gQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gQ;->NEW:LX/0gQ;

    .line 111956
    new-instance v0, LX/0gQ;

    const-string v1, "DELTA"

    const-string v2, "delta"

    invoke-direct {v0, v1, v5, v2}, LX/0gQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gQ;->DELTA:LX/0gQ;

    .line 111957
    const/4 v0, 0x3

    new-array v0, v0, [LX/0gQ;

    sget-object v1, LX/0gQ;->NONE:LX/0gQ;

    aput-object v1, v0, v3

    sget-object v1, LX/0gQ;->NEW:LX/0gQ;

    aput-object v1, v0, v4

    sget-object v1, LX/0gQ;->DELTA:LX/0gQ;

    aput-object v1, v0, v5

    sput-object v0, LX/0gQ;->$VALUES:[LX/0gQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111951
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 111952
    iput-object p3, p0, LX/0gQ;->mValue:Ljava/lang/String;

    .line 111953
    return-void
.end method

.method public static fromAnnotation(Ljava/lang/String;)LX/0gQ;
    .locals 5

    .prologue
    .line 111932
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111933
    sget-object v0, LX/0gQ;->NONE:LX/0gQ;

    .line 111934
    :cond_0
    return-object v0

    .line 111935
    :cond_1
    invoke-static {}, LX/0gQ;->values()[LX/0gQ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 111936
    invoke-virtual {v0}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 111937
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111938
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized language file annotation : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getMatchAnyPattern()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 111942
    const/4 v0, 0x1

    .line 111943
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111944
    invoke-static {}, LX/0gQ;->values()[LX/0gQ;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 111945
    sget-object v7, LX/0gQ;->NONE:LX/0gQ;

    if-eq v6, v7, :cond_0

    .line 111946
    if-eqz v0, :cond_1

    move v0, v1

    .line 111947
    :goto_1
    invoke-virtual {v6}, LX/0gQ;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111948
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111949
    :cond_1
    const-string v7, "|"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 111950
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0gQ;
    .locals 1

    .prologue
    .line 111941
    const-class v0, LX/0gQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0gQ;

    return-object v0
.end method

.method public static values()[LX/0gQ;
    .locals 1

    .prologue
    .line 111940
    sget-object v0, LX/0gQ;->$VALUES:[LX/0gQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0gQ;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111939
    iget-object v0, p0, LX/0gQ;->mValue:Ljava/lang/String;

    return-object v0
.end method
