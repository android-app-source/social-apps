.class public LX/17s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/17f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 199649
    const-string v0, "/business"

    const-string v1, "/pxlcld"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/17s;->b:Ljava/util/Set;

    .line 199650
    const-string v0, "code.facebook.com"

    const-string v1, "research.facebook.com"

    const-string v2, "developers.facebook.com"

    const-string v3, "business.facebook.com"

    const-string v4, "canvas.facebook.com"

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/17s;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/17e;)V
    .locals 1

    .prologue
    .line 199651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199652
    iget-object v0, p1, LX/17e;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 199653
    iput-object v0, p0, LX/17s;->a:LX/0Px;

    .line 199654
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 199655
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199656
    sget-object v2, LX/0ax;->dn:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199657
    const-string v0, "href"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199658
    if-nez v0, :cond_1

    move-object v0, v1

    .line 199659
    :cond_0
    :goto_0
    return-object v0

    .line 199660
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 199661
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 199662
    goto :goto_0

    .line 199663
    :cond_3
    const/4 v0, 0x0

    .line 199664
    sget-object v2, LX/17s;->c:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 199665
    if-nez v2, :cond_4

    const/4 v3, 0x0

    .line 199666
    invoke-static {p1}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v3

    .line 199667
    :goto_1
    move v2, v2

    .line 199668
    if-eqz v2, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    move v0, v0

    .line 199669
    if-eqz v0, :cond_6

    .line 199670
    sget-object v0, LX/0ax;->fd:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 199671
    :cond_6
    iget-object v0, p0, LX/17s;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_8

    iget-object v0, p0, LX/17s;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17f;

    .line 199672
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 199673
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 199674
    if-nez v7, :cond_f

    .line 199675
    :cond_7
    :goto_3
    move-object v0, v5

    .line 199676
    if-nez v0, :cond_0

    .line 199677
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_8
    move-object v0, v1

    .line 199678
    goto :goto_0

    .line 199679
    :cond_9
    if-eqz v0, :cond_a

    .line 199680
    sget-object v2, LX/17s;->b:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_1

    .line 199681
    :cond_a
    sget-object v2, LX/17s;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 199682
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 199683
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v4, 0x2f

    if-ne v2, v4, :cond_d

    :cond_c
    const/4 v2, 0x1

    goto :goto_1

    :cond_d
    move v2, v3

    goto :goto_1

    :cond_e
    move v2, v3

    .line 199684
    goto :goto_1

    .line 199685
    :cond_f
    iget-object v4, v0, LX/17f;->a:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 199686
    iget-object v4, v0, LX/17f;->d:Ljava/util/regex/Pattern;

    if-nez v4, :cond_10

    .line 199687
    iget-object v4, v0, LX/17f;->b:Ljava/lang/String;

    if-eqz v4, :cond_12

    iget-object v4, v0, LX/17f;->b:Ljava/lang/String;

    :goto_4
    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    iput-object v4, v0, LX/17f;->d:Ljava/util/regex/Pattern;

    .line 199688
    iget-object v4, v0, LX/17f;->c:Ljava/lang/String;

    if-eqz v4, :cond_13

    iget-object v4, v0, LX/17f;->c:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    :goto_5
    iput-object v4, v0, LX/17f;->e:Ljava/util/regex/Pattern;

    .line 199689
    :cond_10
    iget-object v4, v0, LX/17f;->d:Ljava/util/regex/Pattern;

    iget-object v8, v0, LX/17f;->a:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 199690
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 199691
    iget-object v4, v0, LX/17f;->e:Ljava/util/regex/Pattern;

    if-eqz v4, :cond_17

    .line 199692
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    .line 199693
    if-nez v4, :cond_11

    .line 199694
    const-string v4, ""

    .line 199695
    :cond_11
    iget-object v7, v0, LX/17f;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 199696
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 199697
    :goto_6
    iget-object v7, v0, LX/17f;->g:LX/17i;

    if-eqz v7, :cond_14

    iget-object v7, v0, LX/17f;->g:LX/17i;

    invoke-interface {v7}, LX/17i;->a()Ljava/lang/String;

    move-result-object v7

    .line 199698
    :goto_7
    if-eqz v7, :cond_7

    move v5, v6

    .line 199699
    :goto_8
    invoke-virtual {v8}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v9

    if-gt v5, v9, :cond_15

    .line 199700
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "<p\\$"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 199701
    invoke-virtual {v8, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 199702
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 199703
    :cond_12
    const-string v4, ""

    goto :goto_4

    :cond_13
    move-object v4, v5

    .line 199704
    goto :goto_5

    .line 199705
    :cond_14
    iget-object v7, v0, LX/17f;->f:Ljava/lang/String;

    goto :goto_7

    .line 199706
    :cond_15
    if-eqz v4, :cond_16

    .line 199707
    :goto_9
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    if-gt v6, v5, :cond_16

    .line 199708
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "<q\\$"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ">"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 199709
    invoke-virtual {v4, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 199710
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    :cond_16
    move-object v5, v7

    .line 199711
    goto/16 :goto_3

    :cond_17
    move-object v4, v5

    goto :goto_6
.end method
