.class public LX/16N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/0lF;D)D
    .locals 1

    .prologue
    .line 184986
    if-nez p0, :cond_1

    .line 184987
    :cond_0
    :goto_0
    return-wide p1

    .line 184988
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184989
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184990
    :try_start_0
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    goto :goto_0

    .line 184991
    :cond_2
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184992
    invoke-virtual {p0}, LX/0lF;->y()D

    move-result-wide p1

    goto :goto_0

    .line 184993
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0lF;F)F
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 184994
    if-nez p0, :cond_1

    .line 184995
    :cond_0
    :goto_0
    return p1

    .line 184996
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184997
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184998
    :try_start_0
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 184999
    :cond_2
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185000
    invoke-virtual {p0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result p1

    goto :goto_0

    .line 185001
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0lF;I)I
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185002
    if-nez p0, :cond_1

    .line 185003
    :cond_0
    :goto_0
    return p1

    .line 185004
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185005
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185006
    :try_start_0
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 185007
    :cond_2
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185008
    invoke-virtual {p0}, LX/0lF;->w()I

    move-result p1

    goto :goto_0

    .line 185009
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0lF;J)J
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185010
    if-nez p0, :cond_1

    .line 185011
    :cond_0
    :goto_0
    return-wide p1

    .line 185012
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185013
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185014
    :try_start_0
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    goto :goto_0

    .line 185015
    :cond_2
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185016
    invoke-virtual {p0}, LX/0lF;->x()J

    move-result-wide p1

    goto :goto_0

    .line 185017
    :catch_0
    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)LX/0lF;
    .locals 1

    .prologue
    .line 185018
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/16N;->a(Ljava/lang/Object;Z)LX/0lF;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Z)LX/0lF;
    .locals 4

    .prologue
    .line 185019
    if-nez p0, :cond_1

    .line 185020
    sget-object v0, LX/2FN;->a:LX/2FN;

    move-object v0, v0

    .line 185021
    :cond_0
    :goto_0
    return-object v0

    .line 185022
    :cond_1
    instance-of v0, p0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    .line 185023
    new-instance v0, LX/0mD;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0mD;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 185024
    :cond_2
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 185025
    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, LX/1Xb;->b(Z)LX/1Xb;

    move-result-object v0

    goto :goto_0

    .line 185026
    :cond_3
    instance-of v0, p0, Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 185027
    check-cast p0, Ljava/lang/Float;

    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, LX/2Cd;->a(F)LX/2Cd;

    move-result-object v0

    goto :goto_0

    .line 185028
    :cond_4
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_5

    .line 185029
    check-cast p0, Ljava/lang/Double;

    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, LX/0rO;->b(D)LX/0rO;

    move-result-object v0

    goto :goto_0

    .line 185030
    :cond_5
    instance-of v0, p0, Ljava/lang/Short;

    if-eqz v0, :cond_6

    .line 185031
    check-cast p0, Ljava/lang/Short;

    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-static {v0}, LX/4rK;->a(S)LX/4rK;

    move-result-object v0

    goto :goto_0

    .line 185032
    :cond_6
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 185033
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/0rQ;->c(I)LX/0rQ;

    move-result-object v0

    goto :goto_0

    .line 185034
    :cond_7
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 185035
    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/10w;->b(J)LX/10w;

    move-result-object v0

    goto :goto_0

    .line 185036
    :cond_8
    instance-of v0, p0, Ljava/math/BigDecimal;

    if-eqz v0, :cond_9

    .line 185037
    check-cast p0, Ljava/math/BigDecimal;

    invoke-static {p0}, LX/2zM;->a(Ljava/math/BigDecimal;)LX/2zM;

    move-result-object v0

    goto :goto_0

    .line 185038
    :cond_9
    instance-of v0, p0, Ljava/math/BigInteger;

    if-eqz v0, :cond_a

    .line 185039
    check-cast p0, Ljava/math/BigInteger;

    invoke-static {p0}, LX/4rG;->a(Ljava/math/BigInteger;)LX/4rG;

    move-result-object v0

    goto :goto_0

    .line 185040
    :cond_a
    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_c

    .line 185041
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 185042
    check-cast p0, Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 185043
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/16N;->a(Ljava/lang/Object;Z)LX/0lF;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    :cond_b
    move-object v0, v1

    .line 185044
    goto/16 :goto_0

    .line 185045
    :cond_c
    instance-of v0, p0, Ljava/lang/Iterable;

    if-eqz v0, :cond_d

    .line 185046
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 185047
    check-cast p0, Ljava/lang/Iterable;

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 185048
    invoke-static {v2, p1}, LX/16N;->a(Ljava/lang/Object;Z)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    .line 185049
    :cond_d
    instance-of v0, p0, [Ljava/lang/Object;

    if-eqz v0, :cond_e

    .line 185050
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 185051
    check-cast p0, [Ljava/lang/Object;

    check-cast p0, [Ljava/lang/Object;

    array-length v2, p0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    .line 185052
    invoke-static {v3, p1}, LX/16N;->a(Ljava/lang/Object;Z)LX/0lF;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/162;->a(LX/0lF;)LX/162;

    .line 185053
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 185054
    :cond_e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;

    .line 185055
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_4
    move v0, v2

    .line 185056
    if-eqz v0, :cond_f

    .line 185057
    new-instance v0, LX/4rJ;

    invoke-direct {v0, p0}, LX/4rJ;-><init>(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 185058
    :cond_f
    if-eqz p1, :cond_10

    .line 185059
    new-instance v0, LX/47o;

    invoke-direct {v0, p0}, LX/47o;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, p1}, LX/16N;->a(Ljava/lang/Object;Z)LX/0lF;

    move-result-object v0

    goto/16 :goto_0

    .line 185060
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to json: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", of type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public static a(Ljava/util/List;)LX/0m9;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0m9;"
        }
    .end annotation

    .prologue
    .line 185061
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 185062
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 185063
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185064
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 185065
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v1, v2, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 185066
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 185067
    :cond_0
    return-object v3
.end method

.method public static a(Ljava/util/Map;)LX/0m9;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0m9;"
        }
    .end annotation

    .prologue
    .line 185068
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 185069
    if-eqz p0, :cond_0

    .line 185070
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 185071
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 185072
    :cond_0
    return-object v2
.end method

.method private static a(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0mA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0mA;",
            ">(",
            "LX/0lF;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 185074
    invoke-static {p0, p1, p2}, LX/16N;->b(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0mA;

    .line 185075
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2}, LX/16N;->a(Ljava/lang/Class;)LX/0mA;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)LX/0mA;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0mA;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 185076
    sget-object v0, LX/0mC;->a:LX/0mC;

    .line 185077
    const-class v1, LX/162;

    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185078
    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v0

    .line 185079
    :goto_0
    return-object v0

    .line 185080
    :cond_0
    const-class v1, LX/0m9;

    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185081
    invoke-virtual {v0}, LX/0mC;->c()LX/0m9;

    move-result-object v0

    goto :goto_0

    .line 185082
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported node type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljava/lang/Iterable;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 185083
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 185084
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 185085
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 185086
    :cond_0
    return-object v0
.end method

.method public static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185087
    if-nez p0, :cond_1

    .line 185088
    :cond_0
    :goto_0
    return-object p1

    .line 185089
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185090
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185091
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 185092
    :cond_2
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185093
    invoke-virtual {p0}, LX/0lF;->v()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONArray;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185094
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 185095
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 185096
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185097
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185098
    :cond_0
    return-object v1
.end method

.method public static a(LX/0lF;)Z
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 185099
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0lF;Z)Z
    .locals 4
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184971
    if-nez p0, :cond_1

    .line 184972
    :cond_0
    :goto_0
    return p1

    .line 184973
    :cond_1
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 184974
    invoke-virtual {p0}, LX/0lF;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 184975
    invoke-virtual {p0}, LX/0lF;->u()Z

    move-result p1

    goto :goto_0

    .line 184976
    :cond_2
    invoke-virtual {p0}, LX/0lF;->o()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 184977
    invoke-virtual {p0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    .line 184978
    const-string v3, "on"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move p1, v1

    goto :goto_0

    :cond_4
    move p1, v0

    goto :goto_0

    .line 184979
    :cond_5
    invoke-virtual {p0}, LX/0lF;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 184980
    invoke-virtual {p0}, LX/0lF;->w()I

    move-result v2

    if-eqz v2, :cond_6

    move p1, v1

    goto :goto_0

    :cond_6
    move p1, v0

    goto :goto_0
.end method

.method public static b(LX/0lF;Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184981
    invoke-static {p0, p1}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v0

    .line 184982
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 184983
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 184984
    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 184985
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lorg/json/JSONArray;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184947
    invoke-static {p0}, LX/16N;->a(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0lF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/0lF;",
            ">(",
            "LX/0lF;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 184948
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 184949
    if-eqz v3, :cond_0

    invoke-virtual {p2, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v4, "Node %s in not an %s in %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v1

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x2

    aput-object p0, v5, v1

    invoke-static {v0, v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 184950
    invoke-virtual {p2, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0

    :cond_1
    move v0, v1

    .line 184951
    goto :goto_0
.end method

.method public static b(Ljava/util/List;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 184952
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 184953
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184954
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 184955
    :cond_0
    return-object v1
.end method

.method public static b(LX/0lF;)Ljava/lang/String;
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 184956
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0lF;)J
    .locals 2

    .prologue
    .line 184957
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "LX/0lF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184958
    const-class v0, LX/162;

    invoke-static {p0, p1, v0}, LX/16N;->b(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0lF;

    move-result-object v0

    .line 184959
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 184960
    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    return-object v0
.end method

.method public static d(LX/0lF;)I
    .locals 1
    .param p0    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 184961
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/16N;->a(LX/0lF;I)I

    move-result v0

    return v0
.end method

.method public static d(LX/0lF;Ljava/lang/String;)LX/162;
    .locals 1

    .prologue
    .line 184970
    const-class v0, LX/162;

    invoke-static {p0, p1, v0}, LX/16N;->a(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0mA;

    move-result-object v0

    check-cast v0, LX/162;

    return-object v0
.end method

.method public static e(LX/0lF;)D
    .locals 2

    .prologue
    .line 184962
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/16N;->a(LX/0lF;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static e(LX/0lF;Ljava/lang/String;)LX/0m9;
    .locals 1

    .prologue
    .line 184963
    const-class v0, LX/0m9;

    invoke-static {p0, p1, v0}, LX/16N;->a(LX/0lF;Ljava/lang/String;Ljava/lang/Class;)LX/0mA;

    move-result-object v0

    check-cast v0, LX/0m9;

    return-object v0
.end method

.method public static f(LX/0lF;)F
    .locals 1

    .prologue
    .line 184964
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/16N;->a(LX/0lF;F)F

    move-result v0

    return v0
.end method

.method public static f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 184965
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 184966
    invoke-static {p0, p1}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/0lF;)Z
    .locals 1

    .prologue
    .line 184967
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/16N;->a(LX/0lF;Z)Z

    move-result v0

    return v0
.end method

.method public static h(LX/0lF;Ljava/lang/String;)LX/0lF;
    .locals 4

    .prologue
    .line 184968
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "No key %s in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method

.method public static i(LX/0lF;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 184969
    invoke-static {p0, p1}, LX/16N;->f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
