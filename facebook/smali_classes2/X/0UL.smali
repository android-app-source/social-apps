.class public final LX/0UL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Up;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Up;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 64459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64460
    iput-object p1, p0, LX/0UL;->a:LX/0QB;

    .line 64461
    return-void
.end method

.method public static a(LX/0QB;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/0Up;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64462
    new-instance v0, LX/0UL;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0UL;-><init>(LX/0QB;)V

    move-object v0, v0

    .line 64463
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 64464
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/0UL;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 64465
    packed-switch p2, :pswitch_data_0

    .line 64466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64467
    :pswitch_0
    invoke-static {p1}, Lcom/facebook/abtest/qe/QuickExperimentINeedInit;->a(LX/0QB;)Lcom/facebook/abtest/qe/QuickExperimentINeedInit;

    move-result-object v0

    .line 64468
    :goto_0
    return-object v0

    .line 64469
    :pswitch_1
    new-instance v2, LX/0dA;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v4, 0x15e7

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p1}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v5

    check-cast v5, LX/0dC;

    const/16 v6, 0x3e5

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 64470
    new-instance v9, LX/0dD;

    .line 64471
    new-instance v11, LX/0dE;

    invoke-static {p1}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v8

    check-cast v8, Landroid/content/pm/PackageManager;

    invoke-static {p1}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v11, v8, v10}, LX/0dE;-><init>(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 64472
    move-object v8, v11

    .line 64473
    check-cast v8, LX/0dE;

    invoke-direct {v9, v8}, LX/0dD;-><init>(LX/0dE;)V

    .line 64474
    move-object v8, v9

    .line 64475
    check-cast v8, LX/0dD;

    invoke-direct/range {v2 .. v8}, LX/0dA;-><init>(LX/03V;LX/0Or;LX/0dC;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dD;)V

    .line 64476
    move-object v0, v2

    .line 64477
    goto :goto_0

    .line 64478
    :pswitch_2
    invoke-static {p1}, LX/0dJ;->a(LX/0QB;)LX/0dJ;

    move-result-object v0

    goto :goto_0

    .line 64479
    :pswitch_3
    invoke-static {p1}, LX/0dj;->a(LX/0QB;)LX/0dj;

    move-result-object v0

    goto :goto_0

    .line 64480
    :pswitch_4
    invoke-static {p1}, LX/0do;->a(LX/0QB;)LX/0do;

    move-result-object v0

    goto :goto_0

    .line 64481
    :pswitch_5
    invoke-static {p1}, LX/0dq;->a(LX/0QB;)LX/0dq;

    move-result-object v0

    goto :goto_0

    .line 64482
    :pswitch_6
    invoke-static {p1}, LX/0dr;->a(LX/0QB;)LX/0dr;

    move-result-object v0

    goto :goto_0

    .line 64483
    :pswitch_7
    invoke-static {p1}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object v0

    goto :goto_0

    .line 64484
    :pswitch_8
    invoke-static {p1}, LX/0dz;->a(LX/0QB;)LX/0dz;

    move-result-object v0

    goto :goto_0

    .line 64485
    :pswitch_9
    invoke-static {p1}, LX/0eI;->a(LX/0QB;)LX/0eI;

    move-result-object v0

    goto :goto_0

    .line 64486
    :pswitch_a
    invoke-static {p1}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(LX/0QB;)Lcom/facebook/mobileconfig/init/MobileConfigInit;

    move-result-object v0

    goto :goto_0

    .line 64487
    :pswitch_b
    invoke-static {p1}, LX/0Vv;->a(LX/0QB;)LX/0Vv;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64488
    const/16 v0, 0xc

    return v0
.end method
