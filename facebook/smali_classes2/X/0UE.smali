.class public LX/0UE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Integer;

.field private static final b:[Ljava/lang/Object;


# instance fields
.field public final c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<TE;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64301
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/0UE;->a:Ljava/lang/Integer;

    .line 64302
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, LX/0UE;->b:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64304
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/0UE;->c:LX/01J;

    .line 64305
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 64306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64307
    new-instance v0, LX/01J;

    invoke-direct {v0, p1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/0UE;->c:LX/01J;

    .line 64308
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 64309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64310
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/0UE;->c:LX/01J;

    .line 64311
    if-eqz p1, :cond_0

    .line 64312
    invoke-virtual {p0, p1}, LX/0UE;->addAll(Ljava/util/Collection;)Z

    .line 64313
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 64314
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 64315
    iget-object v1, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v1, p1}, LX/01J;->d(I)Ljava/lang/Object;

    .line 64316
    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 64317
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    sget-object v1, LX/0UE;->a:Ljava/lang/Integer;

    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 64318
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 64319
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 64320
    iget-object v2, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v2, v1}, LX/01J;->a(I)V

    .line 64321
    instance-of v1, p1, LX/0UE;

    if-eqz v1, :cond_1

    .line 64322
    check-cast p1, LX/0UE;

    .line 64323
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v1

    .line 64324
    iget-object v2, p0, LX/0UE;->c:LX/01J;

    iget-object v3, p1, LX/0UE;->c:LX/01J;

    invoke-virtual {v2, v3}, LX/01J;->a(LX/01J;)V

    .line 64325
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v2

    if-eq v2, v1, :cond_0

    const/4 v0, 0x1

    .line 64326
    :cond_0
    return v0

    .line 64327
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 64328
    invoke-virtual {p0, v2}, LX/0UE;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 64329
    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 64330
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 64331
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 64332
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64300
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 64236
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 64237
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64238
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/0UE;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64239
    const/4 v0, 0x0

    .line 64240
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64333
    if-ne p1, p0, :cond_1

    .line 64334
    :cond_0
    :goto_0
    return v0

    .line 64335
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-eqz v2, :cond_4

    .line 64336
    check-cast p1, Ljava/util/Set;

    .line 64337
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 64338
    goto :goto_0

    .line 64339
    :cond_2
    :try_start_0
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    .line 64340
    invoke-virtual {p0, v2}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v4

    .line 64341
    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    .line 64342
    goto :goto_0

    .line 64343
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64344
    :catch_0
    move v0, v1

    goto :goto_0

    .line 64345
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 64346
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 64241
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 64242
    invoke-virtual {p0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v3

    .line 64243
    if-eqz v3, :cond_0

    .line 64244
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 64245
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64246
    :cond_1
    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 64247
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 64248
    new-instance v0, LX/2IE;

    invoke-direct {v0, p0}, LX/2IE;-><init>(LX/0UE;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64249
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->a(Ljava/lang/Object;)I

    move-result v0

    move v0, v0

    .line 64250
    if-ltz v0, :cond_0

    .line 64251
    invoke-virtual {p0, v0}, LX/0UE;->a(I)Ljava/lang/Object;

    .line 64252
    const/4 v0, 0x1

    .line 64253
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 64254
    const/4 v0, 0x0

    .line 64255
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 64256
    invoke-virtual {p0, v2}, LX/0UE;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 64257
    goto :goto_0

    .line 64258
    :cond_0
    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 64259
    const/4 v1, 0x0

    .line 64260
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ltz v1, :cond_1

    .line 64261
    invoke-virtual {p0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 64262
    invoke-virtual {p0, v1}, LX/0UE;->a(I)Ljava/lang/Object;

    .line 64263
    const/4 v0, 0x1

    .line 64264
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 64265
    :cond_1
    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64266
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 64267
    iget-object v0, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v0}, LX/01J;->size()I

    move-result v2

    .line 64268
    if-nez v2, :cond_1

    .line 64269
    sget-object v0, LX/0UE;->b:[Ljava/lang/Object;

    .line 64270
    :cond_0
    return-object v0

    .line 64271
    :cond_1
    new-array v0, v2, [Ljava/lang/Object;

    .line 64272
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 64273
    iget-object v3, p0, LX/0UE;->c:LX/01J;

    invoke-virtual {v3, v1}, LX/01J;->b(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 64274
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 64275
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v2

    .line 64276
    array-length v0, p1

    if-ge v0, v2, :cond_2

    .line 64277
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 64278
    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 64279
    invoke-virtual {p0, v1}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 64280
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 64281
    :cond_0
    array-length v1, v0

    if-le v1, v2, :cond_1

    .line 64282
    const/4 v1, 0x0

    aput-object v1, v0, v2

    .line 64283
    :cond_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 64284
    invoke-virtual {p0}, LX/0UE;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64285
    const-string v0, "{}"

    .line 64286
    :goto_0
    return-object v0

    .line 64287
    :cond_0
    invoke-virtual {p0}, LX/0UE;->size()I

    move-result v1

    .line 64288
    new-instance v2, Ljava/lang/StringBuilder;

    mul-int/lit8 v0, v1, 0xe

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 64289
    const/16 v0, 0x7b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 64290
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    .line 64291
    if-lez v0, :cond_1

    .line 64292
    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64293
    :cond_1
    invoke-virtual {p0, v0}, LX/0UE;->b(I)Ljava/lang/Object;

    move-result-object v3

    .line 64294
    if-eq v3, p0, :cond_2

    .line 64295
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 64296
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64297
    :cond_2
    const-string v3, "(this Set)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 64298
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 64299
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
