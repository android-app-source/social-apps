.class public LX/0RN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<TT;>;"
        }
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<+TT;>;"
        }
    .end annotation
.end field

.field public f:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 59995
    const/4 v0, 0x1

    .line 59996
    if-eqz p1, :cond_0

    .line 59997
    iget-byte v1, p0, LX/0RN;->f:B

    or-int/2addr v1, v0

    int-to-byte v1, v1

    iput-byte v1, p0, LX/0RN;->f:B

    .line 59998
    :goto_0
    return-void

    .line 59999
    :cond_0
    iget-byte v1, p0, LX/0RN;->f:B

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    int-to-byte v1, v1

    iput-byte v1, p0, LX/0RN;->f:B

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 60000
    const/4 v0, 0x1

    .line 60001
    iget-byte v1, p0, LX/0RN;->f:B

    and-int/2addr v1, v0

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 60002
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 60003
    const-string v0, "%s{declaringModuleName = %s, key = $s, provider = %s, scope = %s, default = %s}"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/0RN;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LX/0RN;->b:LX/0RI;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LX/0RN;->c:LX/0Or;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LX/0RN;->d:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-virtual {p0}, LX/0RN;->e()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
