.class public final LX/15J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/13t;


# direct methods
.method public constructor <init>(LX/13t;)V
    .locals 0

    .prologue
    .line 180394
    iput-object p1, p0, LX/15J;->a:LX/13t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x60daf4b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 180395
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 180396
    iget-object v2, p0, LX/15J;->a:LX/13t;

    .line 180397
    sget-object p0, LX/0oz;->b:Ljava/lang/String;

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 180398
    sget-object p1, LX/0oz;->c:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .line 180399
    iget-object p2, v2, LX/13t;->l:LX/0p3;

    invoke-virtual {p2, p0}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, v2, LX/13t;->m:LX/0p3;

    invoke-virtual {p2, p1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 180400
    :cond_0
    sget-object p2, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {p2, p0}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    sget-object p0, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-virtual {p0, p1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    sget-object p0, LX/0p3;->UNKNOWN:LX/0p3;

    iget-object p1, v2, LX/13t;->l:LX/0p3;

    invoke-virtual {p0, p1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    sget-object p0, LX/0p3;->UNKNOWN:LX/0p3;

    iget-object p1, v2, LX/13t;->m:LX/0p3;

    invoke-virtual {p0, p1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 180401
    :cond_1
    iget-object p0, v2, LX/13t;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0oz;

    invoke-virtual {p0}, LX/0oz;->c()LX/0p3;

    move-result-object p0

    const/4 p1, 0x1

    invoke-static {v2, p0, p1}, LX/13t;->a(LX/13t;LX/0p3;Z)V

    .line 180402
    :cond_2
    :goto_0
    const/16 v1, 0x27

    const v2, 0x3f137d15

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 180403
    :cond_3
    iget-object p0, v2, LX/13t;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0oz;

    invoke-virtual {p0}, LX/0oz;->c()LX/0p3;

    move-result-object p0

    .line 180404
    iget-object p1, v2, LX/13t;->n:LX/0p3;

    invoke-virtual {p0, p1}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 180405
    const/4 p1, 0x0

    invoke-static {v2, p0, p1}, LX/13t;->a(LX/13t;LX/0p3;Z)V

    goto :goto_0
.end method
