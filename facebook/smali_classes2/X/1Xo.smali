.class public LX/1Xo;
.super LX/0lI;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x5c62b5eee45d462L


# instance fields
.field public final _keyType:LX/0lJ;

.field public final _valueType:LX/0lJ;


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "LX/0lJ;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 272485
    invoke-virtual {p2}, LX/0lJ;->hashCode()I

    move-result v0

    invoke-virtual {p3}, LX/0lJ;->hashCode()I

    move-result v1

    xor-int v2, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, LX/0lI;-><init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 272486
    iput-object p2, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272487
    iput-object p3, p0, LX/1Xo;->_valueType:LX/0lJ;

    .line 272488
    return-void
.end method

.method public static a(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0lJ;",
            "LX/0lJ;",
            ")",
            "LX/1Xo;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 272489
    new-instance v0, LX/1Xo;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method


# virtual methods
.method public final a(I)LX/0lJ;
    .locals 1

    .prologue
    .line 272490
    if-nez p1, :cond_0

    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272491
    :goto_0
    return-object v0

    .line 272492
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    goto :goto_0

    .line 272493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272494
    invoke-virtual {p0, p1}, LX/1Xo;->e(Ljava/lang/Object;)LX/1Xo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 272495
    invoke-virtual {p0}, LX/1Xo;->w()LX/1Xo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272496
    invoke-virtual {p0, p1}, LX/1Xo;->f(Ljava/lang/Object;)LX/1Xo;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 272497
    if-nez p1, :cond_0

    const-string v0, "K"

    .line 272498
    :goto_0
    return-object v0

    .line 272499
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const-string v0, "V"

    goto :goto_0

    .line 272500
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272501
    invoke-virtual {p0, p1}, LX/1Xo;->g(Ljava/lang/Object;)LX/1Xo;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272502
    new-instance v0, LX/1Xo;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 272503
    invoke-virtual {p0, p1}, LX/1Xo;->h(Ljava/lang/Object;)LX/1Xo;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272504
    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    .line 272505
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272506
    if-ne p1, v0, :cond_0

    .line 272507
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public e(Ljava/lang/Object;)LX/1Xo;
    .locals 7

    .prologue
    .line 272508
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 272509
    if-ne p1, p0, :cond_1

    .line 272510
    :cond_0
    :goto_0
    return v0

    .line 272511
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 272512
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    .line 272513
    :cond_3
    check-cast p1, LX/1Xo;

    .line 272514
    iget-object v2, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v3, p1, LX/0lJ;->_class:Ljava/lang/Class;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p1, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, v3}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v3, p1, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v2, v3}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public f(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272481
    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    .line 272482
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272483
    if-ne p1, v0, :cond_0

    .line 272484
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public f(Ljava/lang/Object;)LX/1Xo;
    .locals 7

    .prologue
    .line 272515
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public g(Ljava/lang/Object;)LX/1Xo;
    .locals 7

    .prologue
    .line 272480
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public h(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272476
    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272477
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272478
    if-ne p1, v0, :cond_0

    .line 272479
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public h(Ljava/lang/Object;)LX/1Xo;
    .locals 7

    .prologue
    .line 272475
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public i(Ljava/lang/Class;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 272471
    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    .line 272472
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 272473
    if-ne p1, v0, :cond_0

    .line 272474
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public i(Ljava/lang/Object;)LX/1Xo;
    .locals 7

    .prologue
    .line 272470
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v2, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v2

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v6, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 272469
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 272468
    const/4 v0, 0x1

    return v0
.end method

.method public final q()LX/0lJ;
    .locals 1

    .prologue
    .line 272467
    iget-object v0, p0, LX/1Xo;->_keyType:LX/0lJ;

    return-object v0
.end method

.method public final r()LX/0lJ;
    .locals 1

    .prologue
    .line 272466
    iget-object v0, p0, LX/1Xo;->_valueType:LX/0lJ;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 272465
    const/4 v0, 0x2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272464
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[map-like type; class "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272455
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 272456
    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272457
    iget-object v1, p0, LX/1Xo;->_keyType:LX/0lJ;

    if-eqz v1, :cond_0

    .line 272458
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272459
    iget-object v1, p0, LX/1Xo;->_keyType:LX/0lJ;

    invoke-virtual {v1}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272460
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272461
    iget-object v1, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v1}, LX/0lK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272462
    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272463
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()LX/1Xo;
    .locals 7

    .prologue
    .line 272453
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    .line 272454
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/1Xo;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    iget-object v2, p0, LX/1Xo;->_keyType:LX/0lJ;

    iget-object v3, p0, LX/1Xo;->_valueType:LX/0lJ;

    invoke-virtual {v3}, LX/0lJ;->b()LX/0lJ;

    move-result-object v3

    iget-object v4, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v5, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, LX/1Xo;-><init>(Ljava/lang/Class;LX/0lJ;LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 272452
    const-class v0, Ljava/util/Map;

    iget-object v1, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method
