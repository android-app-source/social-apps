.class public LX/0ZA;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/lang/Boolean;

.field public final c:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82976
    const-class v0, LX/0ZA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ZA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 82977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82978
    const/4 v0, 0x0

    iput-object v0, p0, LX/0ZA;->b:Ljava/lang/Boolean;

    .line 82979
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "fb_dialtone_signal"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/0ZA;->c:Ljava/io/File;

    .line 82980
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 82981
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    const-string v2, "enable_dialtone_mode"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 82982
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/0ZA;->b:Ljava/lang/Boolean;

    .line 82983
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 82984
    :try_start_0
    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82985
    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 82986
    :goto_0
    move v1, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82987
    if-nez v1, :cond_0

    .line 82988
    invoke-direct {p0}, LX/0ZA;->d()V

    .line 82989
    :goto_1
    return v0

    .line 82990
    :cond_0
    :try_start_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/0ZA;->c:Ljava/io/File;

    const-string v3, "enable_dialtone_mode"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 82991
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 82992
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Dialtone signal file successfully created at "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82993
    invoke-direct {p0}, LX/0ZA;->d()V

    .line 82994
    const/4 v0, 0x1

    goto :goto_1

    .line 82995
    :catch_0
    move-exception v1

    .line 82996
    :try_start_2
    sget-object v2, LX/0ZA;->a:Ljava/lang/String;

    const-string v3, "Dialtone file could not be created"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 82997
    invoke-direct {p0}, LX/0ZA;->d()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/0ZA;->d()V

    throw v0

    .line 82998
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 82999
    :cond_2
    iget-object v1, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    goto :goto_0
.end method

.method public b()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 83000
    const/4 v0, 0x1

    .line 83001
    iget-object v2, p0, LX/0ZA;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 83002
    if-eqz v3, :cond_1

    .line 83003
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 83004
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_0

    move v0, v1

    .line 83005
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 83006
    :cond_1
    invoke-direct {p0}, LX/0ZA;->d()V

    .line 83007
    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 83008
    iget-object v0, p0, LX/0ZA;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 83009
    invoke-direct {p0}, LX/0ZA;->d()V

    .line 83010
    :cond_0
    iget-object v0, p0, LX/0ZA;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
