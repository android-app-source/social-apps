.class public LX/1oL;
.super LX/1ah;
.source ""

# interfaces
.implements LX/1oM;


# instance fields
.field public a:LX/4AY;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final c:[F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final d:Landroid/graphics/Paint;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final e:[F

.field private f:Z

.field private g:F

.field private h:I

.field private i:I

.field private j:F

.field private final k:Landroid/graphics/Path;

.field private final l:Landroid/graphics/Path;

.field private final m:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 319044
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 319045
    sget-object v0, LX/4AY;->OVERLAY_COLOR:LX/4AY;

    iput-object v0, p0, LX/1oL;->a:LX/4AY;

    .line 319046
    new-array v0, v1, [F

    iput-object v0, p0, LX/1oL;->e:[F

    .line 319047
    new-array v0, v1, [F

    iput-object v0, p0, LX/1oL;->c:[F

    .line 319048
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    .line 319049
    iput-boolean v2, p0, LX/1oL;->f:Z

    .line 319050
    iput v3, p0, LX/1oL;->g:F

    .line 319051
    iput v2, p0, LX/1oL;->h:I

    .line 319052
    iput v2, p0, LX/1oL;->i:I

    .line 319053
    iput v3, p0, LX/1oL;->j:F

    .line 319054
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    .line 319055
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/1oL;->l:Landroid/graphics/Path;

    .line 319056
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    .line 319057
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 319026
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 319027
    iget-object v0, p0, LX/1oL;->l:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 319028
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/1oL;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 319029
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget v1, p0, LX/1oL;->j:F

    iget v2, p0, LX/1oL;->j:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 319030
    iget-boolean v0, p0, LX/1oL;->f:Z

    if-eqz v0, :cond_0

    .line 319031
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    iget-object v1, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    iget-object v3, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    div-float/2addr v3, v5

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 319032
    :goto_0
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget v1, p0, LX/1oL;->j:F

    neg-float v1, v1

    iget v2, p0, LX/1oL;->j:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 319033
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget v1, p0, LX/1oL;->g:F

    div-float/2addr v1, v5

    iget v2, p0, LX/1oL;->g:F

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 319034
    iget-boolean v0, p0, LX/1oL;->f:Z

    if-eqz v0, :cond_1

    .line 319035
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v5

    .line 319036
    iget-object v1, p0, LX/1oL;->l:Landroid/graphics/Path;

    iget-object v2, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, LX/1oL;->m:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 319037
    :goto_1
    iget-object v0, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget v1, p0, LX/1oL;->g:F

    neg-float v1, v1

    div-float/2addr v1, v5

    iget v2, p0, LX/1oL;->g:F

    neg-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 319038
    return-void

    .line 319039
    :cond_0
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    iget-object v1, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget-object v2, p0, LX/1oL;->e:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto :goto_0

    .line 319040
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, LX/1oL;->c:[F

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 319041
    iget-object v1, p0, LX/1oL;->c:[F

    iget-object v2, p0, LX/1oL;->e:[F

    aget v2, v2, v0

    iget v3, p0, LX/1oL;->j:F

    add-float/2addr v2, v3

    iget v3, p0, LX/1oL;->g:F

    div-float/2addr v3, v5

    sub-float/2addr v2, v3

    aput v2, v1, v0

    .line 319042
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 319043
    :cond_2
    iget-object v0, p0, LX/1oL;->l:Landroid/graphics/Path;

    iget-object v1, p0, LX/1oL;->m:Landroid/graphics/RectF;

    iget-object v2, p0, LX/1oL;->c:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 319022
    iget-object v0, p0, LX/1oL;->e:[F

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([FF)V

    .line 319023
    invoke-direct {p0}, LX/1oL;->b()V

    .line 319024
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319025
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 319019
    iput p1, p0, LX/1oL;->i:I

    .line 319020
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319021
    return-void
.end method

.method public final a(IF)V
    .locals 0

    .prologue
    .line 319058
    iput p1, p0, LX/1oL;->h:I

    .line 319059
    iput p2, p0, LX/1oL;->g:F

    .line 319060
    invoke-direct {p0}, LX/1oL;->b()V

    .line 319061
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319062
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 319015
    iput-boolean p1, p0, LX/1oL;->f:Z

    .line 319016
    invoke-direct {p0}, LX/1oL;->b()V

    .line 319017
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319018
    return-void
.end method

.method public final a([F)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 319007
    if-nez p1, :cond_0

    .line 319008
    iget-object v0, p0, LX/1oL;->e:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 319009
    :goto_0
    invoke-direct {p0}, LX/1oL;->b()V

    .line 319010
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319011
    return-void

    .line 319012
    :cond_0
    array-length v0, p1

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v2, "radii should have exactly 8 values"

    invoke-static {v0, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 319013
    iget-object v0, p0, LX/1oL;->e:[F

    invoke-static {p1, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 319014
    goto :goto_1
.end method

.method public final b(F)V
    .locals 0

    .prologue
    .line 319003
    iput p1, p0, LX/1oL;->j:F

    .line 319004
    invoke-direct {p0}, LX/1oL;->b()V

    .line 319005
    invoke-virtual {p0}, LX/1oL;->invalidateSelf()V

    .line 319006
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 318975
    invoke-virtual {p0}, LX/1oL;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 318976
    sget-object v0, LX/4AX;->a:[I

    iget-object v1, p0, LX/1oL;->a:LX/4AY;

    invoke-virtual {v1}, LX/4AY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 318977
    :cond_0
    :goto_0
    iget v0, p0, LX/1oL;->h:I

    if-eqz v0, :cond_1

    .line 318978
    iget-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 318979
    iget-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    iget v1, p0, LX/1oL;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 318980
    iget-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    iget v1, p0, LX/1oL;->g:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 318981
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 318982
    iget-object v0, p0, LX/1oL;->l:Landroid/graphics/Path;

    iget-object v1, p0, LX/1oL;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 318983
    :cond_1
    return-void

    .line 318984
    :pswitch_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 318985
    iget-object v1, p0, LX/1oL;->k:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v1, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 318986
    iget-object v1, p0, LX/1oL;->k:Landroid/graphics/Path;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 318987
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 318988
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 318989
    :pswitch_1
    invoke-super {p0, p1}, LX/1ah;->draw(Landroid/graphics/Canvas;)V

    .line 318990
    iget-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    iget v1, p0, LX/1oL;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 318991
    iget-object v0, p0, LX/1oL;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 318992
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->INVERSE_EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 318993
    iget-object v0, p0, LX/1oL;->k:Landroid/graphics/Path;

    iget-object v1, p0, LX/1oL;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 318994
    iget-boolean v0, p0, LX/1oL;->f:Z

    if-eqz v0, :cond_0

    .line 318995
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/1oL;->g:F

    add-float/2addr v0, v1

    div-float v7, v0, v2

    .line 318996
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/1oL;->g:F

    add-float/2addr v0, v1

    div-float v8, v0, v2

    .line 318997
    cmpl-float v0, v7, v9

    if-lez v0, :cond_2

    .line 318998
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    add-float v3, v0, v7

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/1oL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 318999
    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float v1, v0, v7

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/1oL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 319000
    :cond_2
    cmpl-float v0, v8, v9

    if-lez v0, :cond_0

    .line 319001
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    add-float v4, v0, v8

    iget-object v5, p0, LX/1oL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 319002
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float v2, v0, v8

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/1oL;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 318972
    invoke-super {p0, p1}, LX/1ah;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 318973
    invoke-direct {p0}, LX/1oL;->b()V

    .line 318974
    return-void
.end method
