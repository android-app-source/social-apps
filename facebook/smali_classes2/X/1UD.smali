.class public LX/1UD;
.super LX/1UE;
.source ""

# interfaces
.implements LX/1UG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1UE;",
        "LX/1UG",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1UI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1UI",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(LX/1Rq;LX/0g8;LX/1UH;)V
    .locals 2
    .param p1    # LX/1Rq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255635
    invoke-direct {p0, p1}, LX/1UE;-><init>(LX/1Rq;)V

    .line 255636
    invoke-interface {p2}, LX/0g8;->b()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 255637
    new-instance p2, LX/1UI;

    invoke-static {p3}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object p1

    check-cast p1, LX/0Wd;

    invoke-direct {p2, p0, v0, v1, p1}, LX/1UI;-><init>(LX/1UG;Landroid/view/ViewGroup;Ljava/lang/Integer;LX/0Wd;)V

    .line 255638
    move-object v0, p2

    .line 255639
    iput-object v0, p0, LX/1UD;->a:LX/1UI;

    .line 255640
    new-instance v0, LX/1UJ;

    invoke-direct {v0, p0}, LX/1UJ;-><init>(LX/1UD;)V

    invoke-virtual {p0, v0}, LX/1UE;->a(LX/1KR;)V

    .line 255641
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 255634
    iget-object v0, p0, LX/1UD;->a:LX/1UI;

    invoke-virtual {v0, p2}, LX/1UI;->a(I)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 255631
    iput p2, p0, LX/1UD;->b:I

    .line 255632
    invoke-super {p0, p1, p2}, LX/1UE;->a(LX/1a1;I)V

    .line 255633
    return-void
.end method

.method public final a_(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 255626
    invoke-super {p0, p1, p2}, LX/1UE;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 255630
    iget v0, p0, LX/1UD;->b:I

    return v0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 255627
    iget-object v0, p0, LX/1UD;->a:LX/1UI;

    invoke-virtual {v0}, LX/1UI;->d()V

    .line 255628
    invoke-super {p0}, LX/1UE;->dispose()V

    .line 255629
    return-void
.end method
