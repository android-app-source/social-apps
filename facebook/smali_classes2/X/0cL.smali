.class public final LX/0cL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cM;
.implements LX/0cN;


# instance fields
.field public final synthetic a:LX/0c4;


# direct methods
.method public constructor <init>(LX/0c4;)V
    .locals 0

    .prologue
    .line 88240
    iput-object p1, p0, LX/0cL;->a:LX/0c4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0cK;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 88211
    iget-object v3, p0, LX/0cL;->a:LX/0c4;

    monitor-enter v3

    .line 88212
    :try_start_0
    iget-object v0, p0, LX/0cL;->a:LX/0c4;

    iget-object v0, v0, LX/0c4;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 88213
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 88214
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0cD;

    .line 88215
    invoke-virtual {v1}, LX/0cD;->a()V

    .line 88216
    iget-object v2, p0, LX/0cL;->a:LX/0c4;

    iget-object v2, v2, LX/0c4;->d:Ljava/util/Map;

    .line 88217
    iget-object v5, v1, LX/0cD;->b:Landroid/net/Uri;

    move-object v5, v5

    .line 88218
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/SortedSet;

    .line 88219
    if-nez v2, :cond_1

    .line 88220
    iget-object v2, p0, LX/0cL;->a:LX/0c4;

    iget-object v2, v2, LX/0c4;->a:Ljava/lang/Class;

    const-string v5, "Invalid state: there should be roles for base uri %s when %s disconnected."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 88221
    iget-object v8, v1, LX/0cD;->b:Landroid/net/Uri;

    move-object v8, v8

    .line 88222
    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p1, LX/0cK;->c:LX/00G;

    aput-object v8, v6, v7

    invoke-static {v2, v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88223
    iget-object v2, p0, LX/0cL;->a:LX/0c4;

    iget-object v2, v2, LX/0c4;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    iget-object v5, p0, LX/0cL;->a:LX/0c4;

    iget-object v5, v5, LX/0c4;->a:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid state: there should be roles for base uri "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88224
    iget-object v7, v1, LX/0cD;->b:Landroid/net/Uri;

    move-object v1, v7

    .line 88225
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " when "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p1, LX/0cK;->c:LX/00G;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " disconnected."

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88226
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 88227
    :cond_1
    :try_start_1
    invoke-interface {v2, v1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 88228
    invoke-interface {v2}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88229
    iget-object v2, p0, LX/0cL;->a:LX/0c4;

    iget-object v2, v2, LX/0c4;->d:Ljava/util/Map;

    .line 88230
    iget-object v5, v1, LX/0cD;->b:Landroid/net/Uri;

    move-object v1, v5

    .line 88231
    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 88232
    :cond_2
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    iget-object v1, v1, LX/0c4;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88233
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88234
    if-eqz v0, :cond_3

    .line 88235
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0cD;

    .line 88236
    iget-object v2, p0, LX/0cL;->a:LX/0c4;

    .line 88237
    iget-object v3, v0, LX/0cD;->b:Landroid/net/Uri;

    move-object v0, v3

    .line 88238
    const-string v3, "disconnected"

    invoke-static {v0, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0, v9}, LX/0c4;->a$redex0(LX/0c4;Landroid/net/Uri;Z)V

    goto :goto_1

    .line 88239
    :cond_3
    return-void
.end method

.method public final a(LX/0cK;LX/1gR;)V
    .locals 2

    .prologue
    .line 88241
    iget-object v0, p0, LX/0cL;->a:LX/0c4;

    iget-object v0, v0, LX/0c4;->f:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88242
    iget-object v0, p0, LX/0cL;->a:LX/0c4;

    invoke-static {v0}, LX/0c4;->a(LX/0c4;)Landroid/os/Message;

    move-result-object v0

    .line 88243
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    iget-object v1, v1, LX/0c4;->g:LX/0cJ;

    invoke-interface {v1, p1, v0}, LX/0cJ;->a(LX/0cK;Landroid/os/Message;)V

    .line 88244
    :cond_0
    return-void
.end method

.method public final a(LX/0cK;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 88200
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 88201
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    iget-object v1, v1, LX/0c4;->h:Ljava/lang/ClassLoader;

    if-eqz v1, :cond_0

    .line 88202
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    iget-object v1, v1, LX/0c4;->h:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 88203
    :cond_0
    iget v1, p2, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 88204
    :goto_0
    return-void

    .line 88205
    :pswitch_0
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    .line 88206
    invoke-static {v1, p1, v0}, LX/0c4;->b$redex0(LX/0c4;LX/0cK;Landroid/os/Bundle;)V

    .line 88207
    goto :goto_0

    .line 88208
    :pswitch_1
    iget-object v1, p0, LX/0cL;->a:LX/0c4;

    .line 88209
    invoke-static {v1, p1, v0}, LX/0c4;->a$redex0(LX/0c4;LX/0cK;Landroid/os/Bundle;)V

    .line 88210
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3b9aca00
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
