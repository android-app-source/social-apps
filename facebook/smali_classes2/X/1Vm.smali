.class public LX/1Vm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1Vm",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 266495
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 266496
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1Vm;->b:LX/0Zi;

    .line 266497
    iput-object p1, p0, LX/1Vm;->a:LX/0Ot;

    .line 266498
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vm;
    .locals 4

    .prologue
    .line 266424
    const-class v1, LX/1Vm;

    monitor-enter v1

    .line 266425
    :try_start_0
    sget-object v0, LX/1Vm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266426
    sput-object v2, LX/1Vm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266427
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266428
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266429
    new-instance v3, LX/1Vm;

    const/16 p0, 0x1eab

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1Vm;-><init>(LX/0Ot;)V

    .line 266430
    move-object v0, v3

    .line 266431
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266432
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266433
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 266463
    check-cast p2, LX/C2M;

    .line 266464
    iget-object v0, p0, LX/1Vm;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;

    iget-object v2, p2, LX/C2M;->a:LX/1Pp;

    iget-object v3, p2, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v4, p2, LX/C2M;->c:Landroid/view/View$OnClickListener;

    iget-object v5, p2, LX/C2M;->d:LX/1dc;

    iget-object v6, p2, LX/C2M;->e:Landroid/net/Uri;

    iget-object v7, p2, LX/C2M;->f:Ljava/lang/String;

    iget-object v8, p2, LX/C2M;->g:Ljava/lang/String;

    iget-object v9, p2, LX/C2M;->h:Ljava/lang/String;

    iget-object v10, p2, LX/C2M;->i:Ljava/lang/String;

    move-object v1, p1

    const/4 p2, 0x2

    .line 266465
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v11

    const/4 p0, 0x3

    invoke-interface {v11, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v11

    .line 266466
    invoke-static {v0, v1, v2, v5, v6}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->a(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;LX/1De;LX/1Pp;LX/1dc;Landroid/net/Uri;)LX/1Di;

    move-result-object p0

    .line 266467
    if-eqz p0, :cond_0

    .line 266468
    invoke-interface {v11, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 266469
    :cond_0
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    .line 266470
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 266471
    :goto_0
    move-object p1, v7

    .line 266472
    invoke-virtual {p0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object p0

    sget-object p1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b004e

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p1, 0x1010212

    invoke-virtual {p0, p1}, LX/1ne;->o(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 266473
    const p1, -0x40fdf2b1

    const/4 p2, 0x0

    invoke-static {v1, p1, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 266474
    invoke-interface {p0, p1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {p0, p1}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {v11, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 266475
    iget-object p0, v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->c:LX/C2K;

    const/4 p1, 0x0

    .line 266476
    new-instance p2, LX/C2I;

    invoke-direct {p2, p0}, LX/C2I;-><init>(LX/C2K;)V

    .line 266477
    sget-object v0, LX/C2K;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2J;

    .line 266478
    if-nez v0, :cond_1

    .line 266479
    new-instance v0, LX/C2J;

    invoke-direct {v0}, LX/C2J;-><init>()V

    .line 266480
    :cond_1
    invoke-static {v0, v1, p1, p1, p2}, LX/C2J;->a$redex0(LX/C2J;LX/1De;IILX/C2I;)V

    .line 266481
    move-object p2, v0

    .line 266482
    move-object p1, p2

    .line 266483
    move-object p0, p1

    .line 266484
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 266485
    :goto_1
    move-object p1, v8

    .line 266486
    iget-object p2, p0, LX/C2J;->a:LX/C2I;

    iput-object p1, p2, LX/C2I;->a:Ljava/lang/CharSequence;

    .line 266487
    iget-object p2, p0, LX/C2J;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 266488
    move-object p0, p0

    .line 266489
    iget-object p1, p0, LX/C2J;->a:LX/C2I;

    iput-object v4, p1, LX/C2I;->b:Landroid/view/View$OnClickListener;

    .line 266490
    iget-object p1, p0, LX/C2J;->d:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 266491
    move-object p0, p0

    .line 266492
    invoke-interface {v11, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 266493
    invoke-interface {v11}, LX/1Di;->k()LX/1Dg;

    move-result-object v11

    move-object v0, v11

    .line 266494
    return-object v0

    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    :cond_3
    move-object v7, v9

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_5
    move-object v8, v10

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 266443
    invoke-static {}, LX/1dS;->b()V

    .line 266444
    iget v0, p1, LX/1dQ;->b:I

    .line 266445
    sparse-switch v0, :sswitch_data_0

    .line 266446
    :goto_0
    return-object v2

    .line 266447
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 266448
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 266449
    check-cast v1, LX/C2M;

    .line 266450
    iget-object p1, p0, LX/1Vm;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/C2M;->c:Landroid/view/View$OnClickListener;

    iget-object p2, v1, LX/C2M;->j:Landroid/view/View$OnClickListener;

    .line 266451
    if-eqz p2, :cond_0

    .line 266452
    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 266453
    :goto_1
    goto :goto_0

    .line 266454
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 266455
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 266456
    check-cast v1, LX/C2M;

    .line 266457
    iget-object p1, p0, LX/1Vm;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/C2M;->c:Landroid/view/View$OnClickListener;

    iget-object p2, v1, LX/C2M;->k:Landroid/view/View$OnClickListener;

    .line 266458
    if-eqz p2, :cond_1

    .line 266459
    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 266460
    :goto_2
    goto :goto_0

    .line 266461
    :cond_0
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 266462
    :cond_1
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x40fdf2b1 -> :sswitch_0
        -0x40fde068 -> :sswitch_1
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/C2N;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1Vm",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 266435
    new-instance v1, LX/C2M;

    invoke-direct {v1, p0}, LX/C2M;-><init>(LX/1Vm;)V

    .line 266436
    iget-object v2, p0, LX/1Vm;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C2N;

    .line 266437
    if-nez v2, :cond_0

    .line 266438
    new-instance v2, LX/C2N;

    invoke-direct {v2, p0}, LX/C2N;-><init>(LX/1Vm;)V

    .line 266439
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C2N;->a$redex0(LX/C2N;LX/1De;IILX/C2M;)V

    .line 266440
    move-object v1, v2

    .line 266441
    move-object v0, v1

    .line 266442
    return-object v0
.end method
