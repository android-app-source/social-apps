.class public LX/1Yb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Ya;
.implements LX/1YR;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "LX/1Ya",
        "<",
        "LX/1ZS;",
        ">;",
        "LX/1YR",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field private final c:LX/0iA;

.field private final d:LX/1CN;

.field public final e:LX/1YX;

.field public final f:LX/0ad;

.field private g:LX/1Pi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 273716
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/1Yb;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0iA;LX/1CN;LX/1YX;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 273709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273710
    iput-object p1, p0, LX/1Yb;->b:Landroid/content/res/Resources;

    .line 273711
    iput-object p2, p0, LX/1Yb;->c:LX/0iA;

    .line 273712
    iput-object p3, p0, LX/1Yb;->d:LX/1CN;

    .line 273713
    iput-object p4, p0, LX/1Yb;->e:LX/1YX;

    .line 273714
    iput-object p5, p0, LX/1Yb;->f:LX/0ad;

    .line 273715
    return-void
.end method

.method public static a(LX/0QB;)LX/1Yb;
    .locals 9

    .prologue
    .line 273698
    const-class v1, LX/1Yb;

    monitor-enter v1

    .line 273699
    :try_start_0
    sget-object v0, LX/1Yb;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 273700
    sput-object v2, LX/1Yb;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 273701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 273703
    new-instance v3, LX/1Yb;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-static {v0}, LX/1CN;->a(LX/0QB;)LX/1CN;

    move-result-object v6

    check-cast v6, LX/1CN;

    const-class v7, LX/1YX;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1YX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/1Yb;-><init>(Landroid/content/res/Resources;LX/0iA;LX/1CN;LX/1YX;LX/0ad;)V

    .line 273704
    move-object v0, v3

    .line 273705
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 273706
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Yb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 273707
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 273708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/1ZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 273697
    const-class v0, LX/1ZS;

    return-object v0
.end method

.method public final a(LX/0bJ;J)V
    .locals 7

    .prologue
    .line 273679
    iget-object v0, p0, LX/1Yb;->g:LX/1Pi;

    check-cast p1, LX/1ZS;

    .line 273680
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 273681
    iget-object v2, p1, LX/1ZS;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 273682
    iget-object v2, p1, LX/1ZS;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273683
    :cond_0
    iget-object v2, p1, LX/1ZS;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 273684
    iget-object v2, p1, LX/1ZS;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 273685
    :cond_1
    iget-object v2, p0, LX/1Yb;->f:LX/0ad;

    sget-char v3, LX/34q;->f:C

    const v4, 0x7f080d30

    iget-object v5, p0, LX/1Yb;->b:Landroid/content/res/Resources;

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 273686
    iget-object v3, p0, LX/1Yb;->e:LX/1YX;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->LINK_WELCOME:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    invoke-virtual {v3, v2, v4, v1, v5}, LX/1YX;->a(Ljava/lang/String;Ljava/lang/Long;LX/0Rf;Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;)LX/34n;

    move-result-object v1

    move-object v1, v1

    .line 273687
    invoke-interface {v0, v1}, LX/1Pi;->a(LX/34p;)V

    .line 273688
    return-void
.end method

.method public final a(LX/1PW;)V
    .locals 1

    .prologue
    .line 273693
    check-cast p1, LX/1Pi;

    .line 273694
    iput-object p1, p0, LX/1Yb;->g:LX/1Pi;

    .line 273695
    iget-object v0, p0, LX/1Yb;->d:LX/1CN;

    invoke-virtual {v0, p0}, LX/1CN;->a(LX/1Ya;)V

    .line 273696
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 273690
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Yb;->g:LX/1Pi;

    .line 273691
    iget-object v0, p0, LX/1Yb;->d:LX/1CN;

    invoke-virtual {v0, p0}, LX/1CN;->b(LX/1Ya;)V

    .line 273692
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 273689
    iget-object v0, p0, LX/1Yb;->c:LX/0iA;

    sget-object v1, LX/1Yb;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v0, v1}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    return v0
.end method
