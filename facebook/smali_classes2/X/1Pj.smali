.class public interface abstract LX/1Pj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PW;


# virtual methods
.method public abstract getAnalyticsModule()Ljava/lang/String;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
    .end annotation
.end method

.method public abstract getFontFoundry()LX/1QO;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
    .end annotation
.end method

.method public abstract getNavigator()LX/5KM;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
    .end annotation
.end method

.method public abstract getTraitCollection()LX/1QN;
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
    .end annotation
.end method
