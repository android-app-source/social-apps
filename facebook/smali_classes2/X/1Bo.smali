.class public LX/1Bo;
.super Ljava/util/LinkedHashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private mMaxSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 214204
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 214205
    iput p1, p0, LX/1Bo;->mMaxSize:I

    .line 214206
    return-void
.end method


# virtual methods
.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 214207
    invoke-virtual {p0, p1}, LX/1Bo;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214208
    invoke-super {p0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214209
    const/4 v0, 0x0

    return-object v0
.end method

.method public final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2

    .prologue
    .line 214210
    invoke-virtual {p0}, LX/1Bo;->size()I

    move-result v0

    iget v1, p0, LX/1Bo;->mMaxSize:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
