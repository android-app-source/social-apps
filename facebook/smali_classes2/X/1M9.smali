.class public LX/1M9;
.super LX/0hD;
.source ""

# interfaces
.implements LX/1DD;
.implements LX/0hk;
.implements LX/0fm;


# instance fields
.field public final a:LX/0pV;

.field public b:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(LX/0pV;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 234853
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 234854
    iput-object p1, p0, LX/1M9;->a:LX/0pV;

    .line 234855
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 234856
    iget-object v0, p0, LX/1M9;->a:LX/0pV;

    iget-object v1, p0, LX/1M9;->b:Landroid/support/v4/app/Fragment;

    sget-object v2, LX/0rj;->VIEW_CREATED:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Landroid/support/v4/app/Fragment;LX/0rj;)V

    .line 234857
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 234858
    iget-object v0, p0, LX/1M9;->a:LX/0pV;

    iget-object v1, p0, LX/1M9;->b:Landroid/support/v4/app/Fragment;

    sget-object v2, LX/0rj;->FRAGMENT_DESTROYED:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Landroid/support/v4/app/Fragment;LX/0rj;)V

    .line 234859
    const/4 v0, 0x0

    iput-object v0, p0, LX/1M9;->b:Landroid/support/v4/app/Fragment;

    .line 234860
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 234861
    iget-object v0, p0, LX/1M9;->a:LX/0pV;

    iget-object v1, p0, LX/1M9;->b:Landroid/support/v4/app/Fragment;

    sget-object v2, LX/0rj;->FRAGMENT_RESUMED:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Landroid/support/v4/app/Fragment;LX/0rj;)V

    .line 234862
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 234863
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 234864
    iget-object v0, p0, LX/1M9;->a:LX/0pV;

    iget-object v1, p0, LX/1M9;->b:Landroid/support/v4/app/Fragment;

    sget-object v2, LX/0rj;->VIEW_DESTROYED:LX/0rj;

    invoke-virtual {v0, v1, v2}, LX/0pV;->a(Landroid/support/v4/app/Fragment;LX/0rj;)V

    .line 234865
    return-void
.end method
