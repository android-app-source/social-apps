.class public LX/0z3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/feed/data/freshfeed/uih/SlidingWindowEventQueueRecycler",
        "<",
        "LX/6VO;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/6VO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166548
    const-class v0, LX/0z3;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0z3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 166549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166550
    iput p1, p0, LX/0z3;->b:I

    .line 166551
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/0z3;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 166552
    return-void
.end method


# virtual methods
.method public final a()LX/6VO;
    .locals 1

    .prologue
    .line 166553
    iget-object v0, p0, LX/0z3;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VO;

    .line 166554
    if-nez v0, :cond_0

    .line 166555
    new-instance v0, LX/6VO;

    invoke-direct {v0}, LX/6VO;-><init>()V

    move-object v0, v0

    .line 166556
    :cond_0
    return-object v0
.end method

.method public final a(LX/6VO;)V
    .locals 4

    .prologue
    .line 166557
    check-cast p1, LX/6VO;

    .line 166558
    iget-object v0, p0, LX/0z3;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    iget v1, p0, LX/0z3;->b:I

    if-lt v0, v1, :cond_0

    .line 166559
    :goto_0
    return-void

    .line 166560
    :cond_0
    sget-object v2, LX/14q;->OTHER:LX/14q;

    iput-object v2, p1, LX/6VO;->a:LX/14q;

    .line 166561
    const-wide/16 v2, 0x0

    iput-wide v2, p1, LX/6VO;->b:J

    .line 166562
    const/4 v2, 0x0

    iput v2, p1, LX/6VO;->c:I

    .line 166563
    iget-object v0, p0, LX/0z3;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method
