.class public LX/0Zc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/0VI;
.implements LX/0Ze;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0Zc;


# instance fields
.field public a:LX/0Yw;

.field public b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83704
    new-instance v0, LX/0Yw;

    const v1, 0x7fffffff

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/0Zc;->a:LX/0Yw;

    .line 83705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/0Zc;->b:Ljava/lang/StringBuilder;

    .line 83706
    return-void
.end method

.method public static a(LX/0QB;)LX/0Zc;
    .locals 3

    .prologue
    .line 83719
    sget-object v0, LX/0Zc;->c:LX/0Zc;

    if-nez v0, :cond_1

    .line 83720
    const-class v1, LX/0Zc;

    monitor-enter v1

    .line 83721
    :try_start_0
    sget-object v0, LX/0Zc;->c:LX/0Zc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83722
    if-eqz v2, :cond_0

    .line 83723
    :try_start_1
    new-instance v0, LX/0Zc;

    invoke-direct {v0}, LX/0Zc;-><init>()V

    .line 83724
    move-object v0, v0

    .line 83725
    sput-object v0, LX/0Zc;->c:LX/0Zc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83726
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83727
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83728
    :cond_1
    sget-object v0, LX/0Zc;->c:LX/0Zc;

    return-object v0

    .line 83729
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83731
    const-string v0, "video_events"

    return-object v0
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83710
    instance-of v0, p1, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_0

    .line 83711
    iget-object v0, p0, LX/0Zc;->a:LX/0Yw;

    invoke-virtual {v0}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83712
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 83713
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "videoId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LX/7IM;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 83714
    iget-object p1, p0, LX/0Zc;->b:Ljava/lang/StringBuilder;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 83715
    iget-object p1, p0, LX/0Zc;->b:Ljava/lang/StringBuilder;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "["

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    const-string p3, "] "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, ", "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83716
    iget-object p1, p0, LX/0Zc;->b:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 83717
    iget-object p2, p0, LX/0Zc;->a:LX/0Yw;

    invoke-virtual {p2, p1}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 83718
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83709
    invoke-virtual {p0}, LX/0Zc;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83708
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83707
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "video_events"

    iget-object v2, p0, LX/0Zc;->a:LX/0Yw;

    invoke-virtual {v2}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
