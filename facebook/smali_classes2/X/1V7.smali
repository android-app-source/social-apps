.class public LX/1V7;
.super LX/1V8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final c:LX/1Uc;

.field private static final f:LX/1Ub;

.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/1Ub;

.field public final b:LX/1Ub;

.field private final d:LX/1Ub;

.field private final e:LX/1Ub;

.field private final g:LX/1Ub;

.field public final h:LX/1Ub;

.field public i:LX/1Ub;

.field public j:LX/1VB;

.field public final k:LX/0fO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 257376
    new-instance v0, LX/1Ue;

    invoke-direct {v0, v3, v3}, LX/1Ue;-><init>(FF)V

    sput-object v0, LX/1V7;->c:LX/1Uc;

    .line 257377
    new-instance v0, LX/1Ub;

    const/high16 v1, 0x41400000    # 12.0f

    sget-object v2, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v3, v1, v3, v2}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    sput-object v0, LX/1V7;->f:LX/1Ub;

    return-void
.end method

.method public constructor <init>(LX/14w;LX/1VB;LX/0fO;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 257274
    invoke-direct {p0, p1}, LX/1V8;-><init>(LX/14w;)V

    .line 257275
    iput-object p2, p0, LX/1V7;->j:LX/1VB;

    .line 257276
    new-instance v0, LX/1Ub;

    invoke-static {p0}, LX/1V7;->n(LX/1V7;)F

    move-result v1

    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v2

    sget-object v3, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v1, v2, v5, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->i:LX/1Ub;

    .line 257277
    new-instance v0, LX/1Ub;

    invoke-static {p0}, LX/1V7;->n(LX/1V7;)F

    move-result v1

    sget-object v2, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v1, v5, v5, v2}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->d:LX/1Ub;

    .line 257278
    new-instance v0, LX/1Ub;

    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v1

    .line 257279
    iget-object v2, p0, LX/1V7;->j:LX/1VB;

    invoke-virtual {v2}, LX/1VB;->e()F

    move-result v2

    move v2, v2

    .line 257280
    add-float/2addr v1, v2

    sget-object v2, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v5, v1, v5, v2}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->e:LX/1Ub;

    .line 257281
    new-instance v0, LX/1Ub;

    invoke-virtual {p0}, LX/1V7;->c()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p0}, LX/1V7;->c()F

    move-result v2

    neg-float v2, v2

    new-instance v3, LX/1Ue;

    invoke-virtual {p0}, LX/1V7;->c()F

    move-result v4

    neg-float v4, v4

    invoke-direct {v3, v4, v5}, LX/1Ue;-><init>(FF)V

    invoke-direct {v0, v1, v2, v5, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->a:LX/1Ub;

    .line 257282
    new-instance v0, LX/1Ub;

    invoke-virtual {p0}, LX/1V7;->c()F

    move-result v1

    neg-float v1, v1

    mul-float/2addr v1, v6

    invoke-virtual {p0}, LX/1V7;->c()F

    move-result v2

    neg-float v2, v2

    mul-float/2addr v2, v6

    new-instance v3, LX/1Ue;

    invoke-direct {v3, v5, v5}, LX/1Ue;-><init>(FF)V

    invoke-direct {v0, v1, v2, v5, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->b:LX/1Ub;

    .line 257283
    new-instance v0, LX/1Ub;

    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v1

    sget-object v2, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v1, v5, v5, v2}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->h:LX/1Ub;

    .line 257284
    new-instance v0, LX/1Ub;

    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v1

    sget-object v2, LX/1V7;->c:LX/1Uc;

    invoke-direct {v0, v5, v1, v5, v2}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/1V7;->g:LX/1Ub;

    .line 257285
    iput-object p3, p0, LX/1V7;->k:LX/0fO;

    .line 257286
    return-void
.end method

.method public static a(F)F
    .locals 2

    .prologue
    .line 257375
    const/high16 v0, 0x41400000    # 12.0f

    sub-float v0, p0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(I)LX/1Ub;
    .locals 1

    .prologue
    .line 257372
    if-nez p1, :cond_0

    iget-object v0, p0, LX/1V7;->d:LX/1Ub;

    :goto_0
    return-object v0

    .line 257373
    :cond_0
    sget-object p0, LX/1VB;->b:LX/1Ub;

    move-object v0, p0

    .line 257374
    goto :goto_0
.end method

.method private a(ILX/1Ua;)LX/1Ub;
    .locals 1

    .prologue
    .line 257367
    if-nez p1, :cond_0

    .line 257368
    iget-object v0, p0, LX/1V7;->e:LX/1Ub;

    .line 257369
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, LX/1Ua;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 257370
    iget-object v0, p2, LX/1Ua;->s:LX/1UZ;

    invoke-virtual {v0}, LX/1UZ;->isSharedStoryAttachmentPadding()Z

    move-result v0

    move v0, v0

    .line 257371
    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/1V7;->g:LX/1Ub;

    goto :goto_0

    :cond_2
    sget-object v0, LX/1V7;->f:LX/1Ub;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1V7;
    .locals 6

    .prologue
    .line 257356
    const-class v1, LX/1V7;

    monitor-enter v1

    .line 257357
    :try_start_0
    sget-object v0, LX/1V7;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 257358
    sput-object v2, LX/1V7;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 257361
    new-instance p0, LX/1V7;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, LX/1V9;->b(LX/0QB;)LX/1VB;

    move-result-object v4

    check-cast v4, LX/1VB;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v5

    check-cast v5, LX/0fO;

    invoke-direct {p0, v3, v4, v5}, LX/1V7;-><init>(LX/14w;LX/1VB;LX/0fO;)V

    .line 257362
    move-object v0, p0

    .line 257363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 257364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1V7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 257366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static n(LX/1V7;)F
    .locals 2

    .prologue
    .line 257352
    iget-object v0, p0, LX/1V7;->j:LX/1VB;

    .line 257353
    const/4 v1, 0x0

    move v1, v1

    .line 257354
    invoke-virtual {v0}, LX/1VB;->e()F

    move-result p0

    add-float/2addr v1, p0

    move v0, v1

    .line 257355
    return v0
.end method


# virtual methods
.method public final a(LX/1Ua;LX/1X9;I)LX/1Ub;
    .locals 3

    .prologue
    .line 257330
    if-ltz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 257331
    invoke-virtual {p0}, LX/1V8;->b()Ljava/util/EnumMap;

    move-result-object v0

    .line 257332
    iget-object v1, p1, LX/1Ua;->s:LX/1UZ;

    move-object v1, v1

    .line 257333
    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ub;

    .line 257334
    iget-object v1, p1, LX/1Ua;->t:LX/1Ub;

    move-object v1, v1

    .line 257335
    invoke-virtual {v1, v0}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    .line 257336
    iget-boolean v1, p1, LX/1Ua;->u:Z

    move v1, v1

    .line 257337
    if-eqz v1, :cond_0

    .line 257338
    invoke-virtual {p1}, LX/1Ua;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1V7;->b:LX/1Ub;

    .line 257339
    :goto_1
    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    .line 257340
    :cond_0
    move-object v0, v0

    .line 257341
    sget-object v1, LX/1dn;->a:[I

    invoke-virtual {p2}, LX/1X9;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 257342
    :goto_2
    return-object v0

    .line 257343
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 257344
    :pswitch_0
    invoke-direct {p0, p3}, LX/1V7;->a(I)LX/1Ub;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_2

    .line 257345
    :pswitch_1
    invoke-direct {p0, p3, p1}, LX/1V7;->a(ILX/1Ua;)LX/1Ub;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_2

    .line 257346
    :pswitch_2
    iget-object v1, p0, LX/1V7;->h:LX/1Ub;

    move-object v1, v1

    .line 257347
    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_2

    .line 257348
    :pswitch_3
    iget-object v1, p0, LX/1V7;->i:LX/1Ub;

    move-object v1, v1

    .line 257349
    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_2

    .line 257350
    :pswitch_4
    invoke-direct {p0, p3}, LX/1V7;->a(I)LX/1Ub;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    invoke-direct {p0, p3, p1}, LX/1V7;->a(ILX/1Ua;)LX/1Ub;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_2

    .line 257351
    :cond_2
    iget-object v1, p0, LX/1V7;->a:LX/1Ub;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a()Ljava/util/EnumMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257329
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1Ua;LX/1X9;I)LX/1Ub;
    .locals 6

    .prologue
    .line 257313
    invoke-virtual {p0, p1, p2, p3}, LX/1V8;->a(LX/1Ua;LX/1X9;I)LX/1Ub;

    move-result-object v0

    .line 257314
    new-instance v1, LX/1Ub;

    .line 257315
    sget-object v2, LX/1dn;->a:[I

    invoke-virtual {p2}, LX/1X9;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 257316
    :pswitch_0
    iget v2, v0, LX/1Ub;->a:F

    move v2, v2

    .line 257317
    invoke-static {v2}, LX/1V7;->a(F)F

    move-result v2

    :goto_0
    move v2, v2

    .line 257318
    sget-object v3, LX/1dn;->a:[I

    invoke-virtual {p2}, LX/1X9;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 257319
    :pswitch_1
    iget v3, v0, LX/1Ub;->b:F

    move v3, v3

    .line 257320
    invoke-static {v3}, LX/1V7;->a(F)F

    move-result v3

    :goto_1
    move v3, v3

    .line 257321
    const/4 v4, 0x0

    const/4 p3, 0x0

    .line 257322
    iget-object v5, v0, LX/1Ub;->d:LX/1Uc;

    move-object v5, v5

    .line 257323
    new-instance p0, LX/1Ue;

    invoke-interface {v5, p3}, LX/1Uc;->a(I)F

    move-result p1

    invoke-static {p1}, LX/1V7;->a(F)F

    move-result p1

    const/4 p2, 0x1

    invoke-interface {v5, p2}, LX/1Uc;->a(I)F

    move-result p2

    invoke-interface {v5, p3}, LX/1Uc;->a(I)F

    move-result v5

    sub-float v5, p2, v5

    invoke-direct {p0, p1, v5}, LX/1Ue;-><init>(FF)V

    move-object v0, p0

    .line 257324
    invoke-direct {v1, v2, v3, v4, v0}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    return-object v1

    .line 257325
    :pswitch_2
    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v2

    goto :goto_0

    .line 257326
    :pswitch_3
    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v2

    goto :goto_0

    .line 257327
    :pswitch_4
    invoke-static {p0}, LX/1V7;->n(LX/1V7;)F

    move-result v3

    goto :goto_1

    .line 257328
    :pswitch_5
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/util/EnumMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257312
    iget-object v0, p0, LX/1V7;->j:LX/1VB;

    invoke-virtual {v0}, LX/1VB;->h()Ljava/util/EnumMap;

    move-result-object v0

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 257310
    const/4 p0, 0x0

    move v0, p0

    .line 257311
    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 257308
    const/high16 p0, 0x40c00000    # 6.0f

    move v0, p0

    .line 257309
    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 257306
    const/high16 p0, 0x40c00000    # 6.0f

    move v0, p0

    .line 257307
    return v0
.end method

.method public final h()LX/1Ua;
    .locals 2

    .prologue
    .line 257302
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v1

    neg-float v1, v1

    .line 257303
    iput v1, v0, LX/1UY;->b:F

    .line 257304
    move-object v0, v0

    .line 257305
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/1Ua;
    .locals 2

    .prologue
    .line 257295
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v1

    neg-float v1, v1

    .line 257296
    iput v1, v0, LX/1UY;->b:F

    .line 257297
    move-object v0, v0

    .line 257298
    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v1

    neg-float v1, v1

    .line 257299
    iput v1, v0, LX/1UY;->c:F

    .line 257300
    move-object v0, v0

    .line 257301
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/1Ua;
    .locals 2

    .prologue
    .line 257291
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {p0}, LX/1V7;->d()F

    move-result v1

    neg-float v1, v1

    .line 257292
    iput v1, v0, LX/1UY;->b:F

    .line 257293
    move-object v0, v0

    .line 257294
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1Ua;
    .locals 2

    .prologue
    .line 257287
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v1

    neg-float v1, v1

    .line 257288
    iput v1, v0, LX/1UY;->c:F

    .line 257289
    move-object v0, v0

    .line 257290
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1Ua;
    .locals 2

    .prologue
    .line 257270
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    invoke-virtual {p0}, LX/1V7;->e()F

    move-result v1

    neg-float v1, v1

    .line 257271
    iput v1, v0, LX/1UY;->c:F

    .line 257272
    move-object v0, v0

    .line 257273
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    return-object v0
.end method
