.class public final enum LX/1hO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1hO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1hO;

.field public static final enum G2:LX/1hO;

.field public static final enum G3:LX/1hO;

.field public static final enum G4:LX/1hO;

.field public static final enum NOT_CELLULAR:LX/1hO;

.field public static final enum UNKNOWN:LX/1hO;


# instance fields
.field public value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296039
    new-instance v0, LX/1hO;

    const-string v1, "NOT_CELLULAR"

    invoke-direct {v0, v1, v2, v2}, LX/1hO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hO;->NOT_CELLULAR:LX/1hO;

    new-instance v0, LX/1hO;

    const-string v1, "G2"

    invoke-direct {v0, v1, v3, v3}, LX/1hO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hO;->G2:LX/1hO;

    new-instance v0, LX/1hO;

    const-string v1, "G3"

    invoke-direct {v0, v1, v4, v4}, LX/1hO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hO;->G3:LX/1hO;

    new-instance v0, LX/1hO;

    const-string v1, "G4"

    invoke-direct {v0, v1, v5, v5}, LX/1hO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hO;->G4:LX/1hO;

    new-instance v0, LX/1hO;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v6}, LX/1hO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/1hO;->UNKNOWN:LX/1hO;

    .line 296040
    const/4 v0, 0x5

    new-array v0, v0, [LX/1hO;

    sget-object v1, LX/1hO;->NOT_CELLULAR:LX/1hO;

    aput-object v1, v0, v2

    sget-object v1, LX/1hO;->G2:LX/1hO;

    aput-object v1, v0, v3

    sget-object v1, LX/1hO;->G3:LX/1hO;

    aput-object v1, v0, v4

    sget-object v1, LX/1hO;->G4:LX/1hO;

    aput-object v1, v0, v5

    sget-object v1, LX/1hO;->UNKNOWN:LX/1hO;

    aput-object v1, v0, v6

    sput-object v0, LX/1hO;->$VALUES:[LX/1hO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 296041
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 296042
    iput p3, p0, LX/1hO;->value:I

    .line 296043
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1hO;
    .locals 1

    .prologue
    .line 296044
    const-class v0, LX/1hO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1hO;

    return-object v0
.end method

.method public static values()[LX/1hO;
    .locals 1

    .prologue
    .line 296045
    sget-object v0, LX/1hO;->$VALUES:[LX/1hO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1hO;

    return-object v0
.end method
