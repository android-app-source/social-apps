.class public final enum LX/0lr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0lr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0lr;

.field public static final enum ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER:LX/0lr;

.field public static final enum ALLOW_COMMENTS:LX/0lr;

.field public static final enum ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

.field public static final enum ALLOW_NUMERIC_LEADING_ZEROS:LX/0lr;

.field public static final enum ALLOW_SINGLE_QUOTES:LX/0lr;

.field public static final enum ALLOW_UNQUOTED_CONTROL_CHARS:LX/0lr;

.field public static final enum ALLOW_UNQUOTED_FIELD_NAMES:LX/0lr;

.field public static final enum AUTO_CLOSE_SOURCE:LX/0lr;


# instance fields
.field private final _defaultState:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 130918
    new-instance v0, LX/0lr;

    const-string v1, "AUTO_CLOSE_SOURCE"

    invoke-direct {v0, v1, v3, v4}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->AUTO_CLOSE_SOURCE:LX/0lr;

    .line 130919
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_COMMENTS"

    invoke-direct {v0, v1, v4, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_COMMENTS:LX/0lr;

    .line 130920
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_UNQUOTED_FIELD_NAMES"

    invoke-direct {v0, v1, v5, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_UNQUOTED_FIELD_NAMES:LX/0lr;

    .line 130921
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_SINGLE_QUOTES"

    invoke-direct {v0, v1, v6, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    .line 130922
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_UNQUOTED_CONTROL_CHARS"

    invoke-direct {v0, v1, v7, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_UNQUOTED_CONTROL_CHARS:LX/0lr;

    .line 130923
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER:LX/0lr;

    .line 130924
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_NUMERIC_LEADING_ZEROS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_NUMERIC_LEADING_ZEROS:LX/0lr;

    .line 130925
    new-instance v0, LX/0lr;

    const-string v1, "ALLOW_NON_NUMERIC_NUMBERS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/0lr;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    .line 130926
    const/16 v0, 0x8

    new-array v0, v0, [LX/0lr;

    sget-object v1, LX/0lr;->AUTO_CLOSE_SOURCE:LX/0lr;

    aput-object v1, v0, v3

    sget-object v1, LX/0lr;->ALLOW_COMMENTS:LX/0lr;

    aput-object v1, v0, v4

    sget-object v1, LX/0lr;->ALLOW_UNQUOTED_FIELD_NAMES:LX/0lr;

    aput-object v1, v0, v5

    sget-object v1, LX/0lr;->ALLOW_SINGLE_QUOTES:LX/0lr;

    aput-object v1, v0, v6

    sget-object v1, LX/0lr;->ALLOW_UNQUOTED_CONTROL_CHARS:LX/0lr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0lr;->ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER:LX/0lr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0lr;->ALLOW_NUMERIC_LEADING_ZEROS:LX/0lr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0lr;->ALLOW_NON_NUMERIC_NUMBERS:LX/0lr;

    aput-object v2, v0, v1

    sput-object v0, LX/0lr;->$VALUES:[LX/0lr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 130915
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 130916
    iput-boolean p3, p0, LX/0lr;->_defaultState:Z

    .line 130917
    return-void
.end method

.method public static collectDefaults()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 130910
    invoke-static {}, LX/0lr;->values()[LX/0lr;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 130911
    invoke-virtual {v4}, LX/0lr;->enabledByDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 130912
    invoke-virtual {v4}, LX/0lr;->getMask()I

    move-result v4

    or-int/2addr v0, v4

    .line 130913
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130914
    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/0lr;
    .locals 1

    .prologue
    .line 130927
    const-class v0, LX/0lr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0lr;

    return-object v0
.end method

.method public static values()[LX/0lr;
    .locals 1

    .prologue
    .line 130909
    sget-object v0, LX/0lr;->$VALUES:[LX/0lr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0lr;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 130908
    iget-boolean v0, p0, LX/0lr;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 2

    .prologue
    .line 130907
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0lr;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method
