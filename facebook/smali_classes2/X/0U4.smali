.class public final LX/0U4;
.super LX/0Px;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Px",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final transient a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 64167
    invoke-direct {p0}, LX/0Px;-><init>()V

    .line 64168
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0U4;->a:Ljava/lang/Object;

    .line 64169
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 64165
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 64166
    iget-object v0, p0, LX/0U4;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 64164
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 64163
    iget-object v0, p0, LX/0U4;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/0RZ;->a(Ljava/lang/Object;)LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 64162
    invoke-virtual {p0}, LX/0U4;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 64154
    const/4 v0, 0x1

    return v0
.end method

.method public final subList(II)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 64158
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 64159
    if-ne p1, p2, :cond_0

    .line 64160
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p0, v0

    .line 64161
    :cond_0
    return-object p0
.end method

.method public final bridge synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 64157
    invoke-virtual {p0, p1, p2}, LX/0U4;->subList(II)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64155
    iget-object v0, p0, LX/0U4;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
