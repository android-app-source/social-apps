.class public LX/0rX;
.super LX/0rY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0rY",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0rX;


# direct methods
.method public constructor <init>(LX/0rd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 149765
    invoke-direct {p0, p1}, LX/0rY;-><init>(LX/0rd;)V

    .line 149766
    return-void
.end method

.method public static a(LX/0QB;)LX/0rX;
    .locals 4

    .prologue
    .line 149767
    sget-object v0, LX/0rX;->b:LX/0rX;

    if-nez v0, :cond_1

    .line 149768
    const-class v1, LX/0rX;

    monitor-enter v1

    .line 149769
    :try_start_0
    sget-object v0, LX/0rX;->b:LX/0rX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 149770
    if-eqz v2, :cond_0

    .line 149771
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 149772
    new-instance p0, LX/0rX;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-direct {p0, v3}, LX/0rX;-><init>(LX/0rd;)V

    .line 149773
    move-object v0, p0

    .line 149774
    sput-object v0, LX/0rX;->b:LX/0rX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149775
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 149776
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 149777
    :cond_1
    sget-object v0, LX/0rX;->b:LX/0rX;

    return-object v0

    .line 149778
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 149779
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
