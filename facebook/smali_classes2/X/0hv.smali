.class public LX/0hv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0pG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Yi;

.field private final c:LX/0ow;

.field private final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119204
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/0hv;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Yi;LX/0ow;Ljava/lang/Boolean;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/reducedinstance/bridge/IsNewsFeedDisabledForWork;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0pG;",
            ">;",
            "LX/0Yi;",
            "LX/0ow;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 119205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119206
    iput-object p1, p0, LX/0hv;->a:LX/0Or;

    .line 119207
    iput-object p2, p0, LX/0hv;->b:LX/0Yi;

    .line 119208
    iput-object p3, p0, LX/0hv;->c:LX/0ow;

    .line 119209
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/0hv;->d:Z

    .line 119210
    return-void
.end method

.method public static a(LX/0QB;)LX/0hv;
    .locals 10

    .prologue
    .line 119211
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 119212
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 119213
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 119214
    if-nez v1, :cond_0

    .line 119215
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119216
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 119217
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 119218
    sget-object v1, LX/0hv;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 119219
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 119220
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 119221
    :cond_1
    if-nez v1, :cond_4

    .line 119222
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 119223
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 119224
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 119225
    new-instance v9, LX/0hv;

    const/16 v1, 0x5ec

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v1

    check-cast v1, LX/0Yi;

    invoke-static {v0}, LX/0ow;->a(LX/0QB;)LX/0ow;

    move-result-object v7

    check-cast v7, LX/0ow;

    invoke-static {v0}, LX/0gq;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-direct {v9, p0, v1, v7, v8}, LX/0hv;-><init>(LX/0Or;LX/0Yi;LX/0ow;Ljava/lang/Boolean;)V

    .line 119226
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 119227
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 119228
    if-nez v1, :cond_2

    .line 119229
    sget-object v0, LX/0hv;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 119230
    :goto_1
    if-eqz v0, :cond_3

    .line 119231
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 119232
    :goto_3
    check-cast v0, LX/0hv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 119233
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 119234
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 119235
    :catchall_1
    move-exception v0

    .line 119236
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 119237
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 119238
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 119239
    :cond_2
    :try_start_8
    sget-object v0, LX/0hv;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private c()V
    .locals 9

    .prologue
    .line 119240
    iget-object v0, p0, LX/0hv;->c:LX/0ow;

    const/4 v4, 0x0

    .line 119241
    iget-object v2, v0, LX/0ow;->f:LX/0Uh;

    const/16 v3, 0x3b1

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/0ow;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 119242
    :cond_0
    :goto_0
    iget-object v0, p0, LX/0hv;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pG;

    invoke-virtual {v0}, LX/0pG;->a()LX/0gC;

    move-result-object v0

    .line 119243
    invoke-interface {v0}, LX/0gC;->u()Z

    move-result v1

    if-nez v1, :cond_1

    .line 119244
    invoke-interface {v0}, LX/0gC;->n()Z

    .line 119245
    :cond_1
    return-void

    .line 119246
    :cond_2
    const/4 v3, 0x0

    .line 119247
    :try_start_0
    iget-object v2, v0, LX/0ow;->c:Landroid/content/Context;

    const-string v5, "crash_count"

    invoke-virtual {v2, v5}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    .line 119248
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 119249
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/FileInputStream;->read([B)I

    .line 119250
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 119251
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 119252
    :goto_1
    move v2, v2

    .line 119253
    iget-object v3, v0, LX/0ow;->e:LX/0oy;

    .line 119254
    iget v5, v3, LX/0oy;->o:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3

    .line 119255
    iget-object v5, v3, LX/0oy;->a:LX/0W3;

    sget-wide v7, LX/0X5;->gD:J

    const/4 v6, 0x2

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JI)I

    move-result v5

    iput v5, v3, LX/0oy;->o:I

    .line 119256
    :cond_3
    iget v5, v3, LX/0oy;->o:I

    move v3, v5

    .line 119257
    if-ge v2, v3, :cond_4

    .line 119258
    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v2}, LX/0ow;->a(LX/0ow;I)V

    goto :goto_0

    .line 119259
    :cond_4
    iget-object v2, v0, LX/0ow;->d:LX/0Zb;

    const-string v3, "android_crash_loop_counter_max_reached"

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 119260
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 119261
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 119262
    :cond_5
    iget-object v2, v0, LX/0ow;->b:LX/0ox;

    sget-object v3, LX/0pP;->w:LX/0Tn;

    invoke-virtual {v2, v3}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v2

    invoke-virtual {v2}, LX/15j;->a()V

    .line 119263
    invoke-static {v0, v4}, LX/0ow;->a(LX/0ow;I)V

    goto :goto_0

    .line 119264
    :catch_0
    move v2, v3

    .line 119265
    goto :goto_1

    .line 119266
    :catch_1
    move-exception v2

    move-object v7, v2

    move v2, v3

    move-object v3, v7

    .line 119267
    :goto_2
    sget-object v5, LX/0ow;->a:Ljava/lang/Class;

    const-string v6, "Exception when attempting to read crash count file"

    invoke-static {v5, v6, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 119268
    :catch_2
    move-exception v3

    goto :goto_2
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 119269
    iget-boolean v0, p0, LX/0hv;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 119270
    invoke-direct {p0}, LX/0hv;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119271
    :goto_0
    return-void

    .line 119272
    :cond_0
    const-string v0, "FeedDataLoaderInitializer.onLoginToFeed"

    const v1, -0x3f0d868e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 119273
    :try_start_0
    invoke-direct {p0}, LX/0hv;->c()V

    .line 119274
    iget-object v1, p0, LX/0hv;->b:LX/0Yi;

    iget-object v0, p0, LX/0hv;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pG;

    invoke-virtual {v0}, LX/0pG;->a()LX/0gC;

    move-result-object v0

    invoke-interface {v0}, LX/0gC;->v()Z

    move-result v0

    .line 119275
    if-eqz v0, :cond_1

    .line 119276
    iget-object v2, v1, LX/0Yi;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v3, v1, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {v3}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Yi;->f(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v3

    const-string v4, "TimeToFeedFetchExecuteFromTrigger"

    iget-object p0, v1, LX/0Yi;->o:LX/0Ym;

    invoke-virtual {p0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object p0

    invoke-virtual {v1, v4, p0}, LX/0Yi;->a(Ljava/lang/String;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119277
    :cond_1
    const v0, 0x13644bb0

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x5b314c32    # -8.964286E-17f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 119278
    invoke-direct {p0}, LX/0hv;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119279
    :goto_0
    return-void

    .line 119280
    :cond_0
    invoke-direct {p0}, LX/0hv;->c()V

    goto :goto_0
.end method
