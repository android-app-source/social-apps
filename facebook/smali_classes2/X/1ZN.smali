.class public abstract LX/1ZN;
.super Landroid/app/IntentService;
.source ""

# interfaces
.implements LX/0ez;
.implements LX/0Xn;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0jb;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jf;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Sj;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 274669
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 274670
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, LX/1ZN;->b:LX/0jb;

    .line 274671
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 274672
    iput-object v0, p0, LX/1ZN;->c:LX/0Ot;

    .line 274673
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 274674
    iput-object v0, p0, LX/1ZN;->d:LX/0Ot;

    .line 274675
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 274676
    iput-object v0, p0, LX/1ZN;->e:LX/0Ot;

    .line 274677
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 274678
    iput-object v0, p0, LX/1ZN;->f:LX/0Ot;

    .line 274679
    iput-object p1, p0, LX/1ZN;->a:Ljava/lang/String;

    .line 274680
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/1ZN;

    const/16 v1, 0x257

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x26c

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1a1

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p0, LX/1ZN;->c:LX/0Ot;

    iput-object v2, p0, LX/1ZN;->d:LX/0Ot;

    iput-object v3, p0, LX/1ZN;->e:LX/0Ot;

    iput-object v0, p0, LX/1ZN;->f:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/0je;)V
    .locals 1

    .prologue
    .line 274667
    iget-object v0, p0, LX/1ZN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jf;

    invoke-virtual {v0, p1}, LX/0jf;->a(LX/0je;)V

    .line 274668
    return-void
.end method

.method public abstract a(Landroid/content/Intent;)V
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274666
    iget-object v0, p0, LX/1ZN;->b:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x778fa19a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 274663
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 274664
    invoke-static {p0, p0}, LX/1ZN;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 274665
    const/16 v1, 0x25

    const v2, 0x238c71ed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x57a81096

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 274660
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 274661
    iget-object v0, p0, LX/1ZN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jf;

    invoke-virtual {v0}, LX/0jf;->a()V

    .line 274662
    const/16 v0, 0x25

    const v2, 0x677da70e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 274638
    sget-object v0, LX/0TP;->NORMAL:LX/0TP;

    invoke-virtual {v0}, LX/0TP;->getAndroidThreadPriority()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 274639
    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "overridden_viewer_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 274640
    :cond_0
    sget-object v0, LX/1mW;->a:LX/1mW;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 274641
    :goto_0
    move-object v2, v0

    .line 274642
    const/4 v1, 0x0

    .line 274643
    :try_start_1
    iget-object v0, p0, LX/1ZN;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sj;

    const-string v3, "IntentService"

    iget-object v4, p0, LX/1ZN;->a:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v0

    .line 274644
    if-eqz v0, :cond_1

    .line 274645
    invoke-interface {v0}, LX/0cV;->a()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 274646
    :cond_1
    :try_start_2
    invoke-virtual {p0, p1}, LX/1ZN;->a(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 274647
    if-eqz v0, :cond_2

    .line 274648
    :try_start_3
    invoke-interface {v0}, LX/0cV;->b()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 274649
    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, LX/1mW;->close()V

    .line 274650
    :cond_3
    return-void

    .line 274651
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    .line 274652
    :try_start_4
    invoke-interface {v0}, LX/0cV;->b()V

    :cond_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 274653
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 274654
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_6

    :try_start_6
    invoke-interface {v2}, LX/1mW;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_5
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_6
    invoke-interface {v2}, LX/1mW;->close()V

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1

    .line 274655
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 274656
    iget-object v0, p0, LX/1ZN;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "cant_get_overriden_viewer_context"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274657
    sget-object v0, LX/1mW;->a:LX/1mW;

    goto :goto_0

    .line 274658
    :cond_7
    const-string v0, "overridden_viewer_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 274659
    iget-object v1, p0, LX/1ZN;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-interface {v1, v0}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    move-result-object v0

    goto :goto_0
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 274636
    iget-object v0, p0, LX/1ZN;->b:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 274637
    return-void
.end method
