.class public abstract LX/0rB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/api/feedtype/FeedType$Name;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "LX/0rn;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            "LX/0Ot",
            "<+",
            "LX/0rn;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 149133
    invoke-direct {p0, p1, p2, v0, v0}, LX/0rB;-><init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;ZZ)V

    .line 149134
    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType$Name;LX/0Ot;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/feedtype/FeedType$Name;",
            "LX/0Ot",
            "<+",
            "LX/0rn;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 149126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149127
    iput-object p1, p0, LX/0rB;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    .line 149128
    iput-object p2, p0, LX/0rB;->b:LX/0Ot;

    .line 149129
    iput-boolean p3, p0, LX/0rB;->c:Z

    .line 149130
    iput-boolean p4, p0, LX/0rB;->d:Z

    .line 149131
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Intent;)Lcom/facebook/api/feedtype/FeedType;
.end method

.method public abstract a(Landroid/content/Intent;Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;
.end method

.method public final b()LX/0rn;
    .locals 1

    .prologue
    .line 149132
    iget-object v0, p0, LX/0rB;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rn;

    return-object v0
.end method
