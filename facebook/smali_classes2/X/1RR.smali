.class public final LX/1RR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1RJ;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1RJ;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 246173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246174
    iput-object p1, p0, LX/1RR;->a:LX/0QB;

    .line 246175
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 246176
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1RR;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 246177
    packed-switch p2, :pswitch_data_0

    .line 246178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246179
    :pswitch_0
    new-instance p0, LX/Aqb;

    invoke-static {p1}, LX/AqW;->b(LX/0QB;)LX/AqW;

    move-result-object v0

    check-cast v0, LX/AqW;

    const-class v1, LX/AqZ;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/AqZ;

    invoke-direct {p0, v0, v1}, LX/Aqb;-><init>(LX/AqW;LX/AqZ;)V

    .line 246180
    move-object v0, p0

    .line 246181
    :goto_0
    return-object v0

    .line 246182
    :pswitch_1
    invoke-static {p1}, LX/Ax3;->b(LX/0QB;)LX/Ax3;

    move-result-object v0

    goto :goto_0

    .line 246183
    :pswitch_2
    invoke-static {p1}, LX/AzZ;->b(LX/0QB;)LX/AzZ;

    move-result-object v0

    goto :goto_0

    .line 246184
    :pswitch_3
    new-instance v1, LX/BGV;

    invoke-static {p1}, LX/BGN;->b(LX/0QB;)LX/BGN;

    move-result-object v0

    check-cast v0, LX/BGN;

    invoke-direct {v1, v0}, LX/BGV;-><init>(LX/BGN;)V

    .line 246185
    const-class v0, LX/BGO;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/BGO;

    .line 246186
    iput-object v0, v1, LX/BGV;->a:LX/BGO;

    .line 246187
    move-object v0, v1

    .line 246188
    goto :goto_0

    .line 246189
    :pswitch_4
    new-instance p0, LX/BM6;

    const/16 v0, 0x2ff5

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-static {p1}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v0

    check-cast v0, LX/B5l;

    const-class v1, LX/BMC;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BMC;

    const-class v2, LX/BM9;

    invoke-interface {p1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/BM9;

    invoke-direct {p0, p2, v0, v1, v2}, LX/BM6;-><init>(LX/0Ot;LX/B5l;LX/BMC;LX/BM9;)V

    .line 246190
    move-object v0, p0

    .line 246191
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 246192
    const/4 v0, 0x5

    return v0
.end method
