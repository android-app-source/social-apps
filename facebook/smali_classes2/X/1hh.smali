.class public LX/1hh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/1hi;",
            ">;"
        }
    .end annotation
.end field

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 296436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296437
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    .line 296438
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/1hh;->b:J

    .line 296439
    return-void
.end method

.method private static b(LX/1hh;)V
    .locals 13

    .prologue
    .line 296440
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 296441
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, LX/1hh;->b:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    const-wide/16 v4, 0xa

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 296442
    iput-wide v0, p0, LX/1hh;->b:J

    .line 296443
    const/4 v6, 0x0

    move v7, v6

    :goto_0
    iget-object v6, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v7, v6, :cond_2

    .line 296444
    iget-object v6, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v6, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1hi;

    .line 296445
    sget-boolean v8, LX/19V;->b:Z

    if-eqz v8, :cond_0

    .line 296446
    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, LX/1hi;->h()J

    move-result-wide v10

    sub-long v10, v0, v10

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    .line 296447
    :cond_0
    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, LX/1hi;->h()J

    move-result-wide v10

    sub-long v10, v0, v10

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    const-wide/16 v10, 0xa

    cmp-long v8, v8, v10

    if-ltz v8, :cond_1

    .line 296448
    const-string v8, "Stale state (%d s)"

    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, LX/1hi;->h()J

    move-result-wide v10

    sub-long v10, v0, v10

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 296449
    const/4 v9, 0x0

    const/4 v10, 0x2

    invoke-virtual {v6, v8, v9, v10}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 296450
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 296451
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296452
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 296453
    monitor-enter p0

    .line 296454
    :try_start_0
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 296455
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 296456
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hi;

    .line 296457
    iget-object v4, v0, LX/1hi;->c:Ljava/lang/String;

    invoke-static {v4, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 296458
    iget v4, v0, LX/1hi;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 296459
    :cond_0
    iget-object v4, v0, LX/1hi;->c:Ljava/lang/String;

    invoke-static {v4, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 296460
    iget v4, v0, LX/1hi;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, v0, LX/1hi;->d:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 296461
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 296462
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296463
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 296464
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/15F;Lcom/google/common/util/concurrent/SettableFuture;LX/1AI;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;ZZ)LX/1hi;
    .locals 16

    .prologue
    .line 296465
    monitor-enter p0

    :try_start_0
    invoke-static/range {p0 .. p0}, LX/1hh;->b(LX/1hh;)V

    .line 296466
    new-instance v1, LX/1hi;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v9

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p13

    invoke-direct/range {v1 .. v15}, LX/1hi;-><init>(Ljava/lang/String;ILorg/apache/http/client/ResponseHandler;Lcom/facebook/tigon/httpclientadapter/FlowObserverRequestInfo;LX/15F;Lcom/google/common/util/concurrent/SettableFuture;LX/1AI;ILjava/lang/String;ILjava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;ZZ)V

    .line 296467
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1hh;->a:Landroid/util/SparseArray;

    move/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296468
    monitor-exit p0

    return-object v1

    .line 296469
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final a()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 296470
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 296471
    monitor-enter p0

    move v1, v0

    .line 296472
    :goto_0
    :try_start_0
    iget-object v3, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 296473
    iget-object v3, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 296474
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296475
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296476
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v0

    move v1, v0

    move v2, v0

    move v3, v0

    :goto_1
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hi;

    .line 296477
    invoke-virtual {v0}, LX/1hi;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 296478
    add-int/lit8 v0, v3, 0x1

    move v8, v1

    move v1, v2

    move v2, v0

    move v0, v8

    .line 296479
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 296480
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 296481
    :cond_1
    invoke-virtual {v0}, LX/1hi;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 296482
    add-int/lit8 v0, v2, 0x1

    move v2, v3

    move v8, v0

    move v0, v1

    move v1, v8

    goto :goto_2

    .line 296483
    :cond_2
    invoke-virtual {v0}, LX/1hi;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 296484
    add-int/lit8 v0, v1, 0x1

    move v1, v2

    move v2, v3

    goto :goto_2

    .line 296485
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "TigonRequestStates:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 296486
    const-string v4, " outstanding("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296487
    const-string v4, " waiting("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296488
    const-string v3, " handling("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296489
    const-string v2, " handled("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296490
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296491
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    .prologue
    .line 296492
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/1hh;->c(I)LX/1hi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 296493
    if-nez v0, :cond_1

    .line 296494
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 296495
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 296496
    sget-boolean v0, LX/19V;->b:Z

    if-eqz v0, :cond_0

    .line 296497
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 296498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)LX/1hi;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 296499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hi;

    .line 296500
    if-nez v0, :cond_0

    .line 296501
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Accessing removed state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1hi;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296502
    :cond_0
    monitor-exit p0

    return-object v0

    .line 296503
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(I)LX/1hi;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 296504
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1hh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1hi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296505
    monitor-exit p0

    return-object v0

    .line 296506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
