.class public LX/0WI;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0SI;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0SI;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75724
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0SI;
    .locals 4

    .prologue
    .line 75711
    sget-object v0, LX/0WI;->a:LX/0SI;

    if-nez v0, :cond_1

    .line 75712
    const-class v1, LX/0WI;

    monitor-enter v1

    .line 75713
    :try_start_0
    sget-object v0, LX/0WI;->a:LX/0SI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75714
    if-eqz v2, :cond_0

    .line 75715
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75716
    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {v3, p0}, LX/0X2;->a(LX/0WJ;Landroid/content/Context;)LX/0SI;

    move-result-object v3

    move-object v0, v3

    .line 75717
    sput-object v0, LX/0WI;->a:LX/0SI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75720
    :cond_1
    sget-object v0, LX/0WI;->a:LX/0SI;

    return-object v0

    .line 75721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 75723
    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0, v1}, LX/0X2;->a(LX/0WJ;Landroid/content/Context;)LX/0SI;

    move-result-object v0

    return-object v0
.end method
