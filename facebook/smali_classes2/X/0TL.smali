.class public LX/0TL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0TL;


# instance fields
.field private b:I

.field private c:I

.field public d:I

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62868
    new-instance v0, LX/0TL;

    invoke-direct {v0}, LX/0TL;-><init>()V

    sput-object v0, LX/0TL;->a:LX/0TL;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62865
    const/4 v0, 0x0

    iput v0, p0, LX/0TL;->d:I

    .line 62866
    const v0, 0x7fffffff

    iput v0, p0, LX/0TL;->e:I

    .line 62867
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 62813
    iget v0, p0, LX/0TL;->d:I

    if-le p1, v0, :cond_0

    .line 62814
    iput p1, p0, LX/0TL;->d:I

    .line 62815
    :cond_0
    iget v0, p0, LX/0TL;->e:I

    if-ge p1, v0, :cond_1

    .line 62816
    iput p1, p0, LX/0TL;->e:I

    .line 62817
    :cond_1
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 62856
    const-string v0, "/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq"

    .line 62857
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62858
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62859
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 62860
    :try_start_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, LX/0TL;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62861
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 62862
    :cond_0
    return-void

    .line 62863
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    throw v0
.end method

.method public static g(LX/0TL;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 62836
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, LX/0TL;->b(I)V

    .line 62837
    invoke-virtual {p0}, LX/0TL;->b()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 62838
    invoke-virtual {p0}, LX/0TL;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/0TL;->b(I)V

    .line 62839
    :cond_0
    iget v0, p0, LX/0TL;->d:I

    if-nez v0, :cond_3

    .line 62840
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc/cpuinfo"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62841
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62842
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62843
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 62844
    const-string v2, "cpu MHz"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62845
    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {p0, v0}, LX/0TL;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62846
    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 62847
    :cond_3
    :goto_0
    iget v0, p0, LX/0TL;->d:I

    iget v1, p0, LX/0TL;->e:I

    if-gt v0, v1, :cond_5

    .line 62848
    iget v0, p0, LX/0TL;->d:I

    if-nez v0, :cond_4

    .line 62849
    iput v3, p0, LX/0TL;->d:I

    .line 62850
    :cond_4
    iput v3, p0, LX/0TL;->e:I

    .line 62851
    :cond_5
    return-void

    .line 62852
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 62853
    :catch_0
    move-exception v0

    .line 62854
    const-string v1, "ProcessorInfoUtil"

    const-string v2, "Unable to read a CPU core maximum frequency"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62855
    iput v3, p0, LX/0TL;->d:I

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized b()I
    .locals 3

    .prologue
    .line 62828
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/0TL;->b:I

    if-eqz v0, :cond_0

    .line 62829
    iget v0, p0, LX/0TL;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62830
    :goto_0
    monitor-exit p0

    return v0

    .line 62831
    :cond_0
    :try_start_1
    invoke-static {}, LX/0TM;->a()I

    move-result v0

    iput v0, p0, LX/0TL;->b:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62832
    :goto_1
    :try_start_2
    iget v0, p0, LX/0TL;->b:I

    goto :goto_0

    .line 62833
    :catch_0
    move-exception v0

    .line 62834
    const-string v1, "ProcessorInfoUtil"

    const-string v2, "Unable to get reliable CPU Core count"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 62835
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 62825
    iget v0, p0, LX/0TL;->c:I

    if-nez v0, :cond_0

    .line 62826
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/0TL;->c:I

    .line 62827
    :cond_0
    iget v0, p0, LX/0TL;->c:I

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 62821
    invoke-virtual {p0}, LX/0TL;->b()I

    move-result v0

    .line 62822
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 62823
    invoke-virtual {p0}, LX/0TL;->c()I

    move-result v0

    .line 62824
    :cond_0
    return v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 62818
    iget v0, p0, LX/0TL;->d:I

    if-nez v0, :cond_0

    .line 62819
    invoke-static {p0}, LX/0TL;->g(LX/0TL;)V

    .line 62820
    :cond_0
    iget v0, p0, LX/0TL;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
