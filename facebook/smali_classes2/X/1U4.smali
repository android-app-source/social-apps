.class public LX/1U4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 255359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255360
    iput-object p1, p0, LX/1U4;->a:LX/0Ot;

    .line 255361
    iput-object p2, p0, LX/1U4;->b:LX/0Ot;

    .line 255362
    return-void
.end method

.method public static a(LX/0QB;)LX/1U4;
    .locals 5

    .prologue
    .line 255363
    const-class v1, LX/1U4;

    monitor-enter v1

    .line 255364
    :try_start_0
    sget-object v0, LX/1U4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 255365
    sput-object v2, LX/1U4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 255366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 255368
    new-instance v3, LX/1U4;

    const/16 v4, 0xae8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2348

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1U4;-><init>(LX/0Ot;LX/0Ot;)V

    .line 255369
    move-object v0, v3

    .line 255370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 255371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1U4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 255373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 255374
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255375
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedSectionHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255376
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryHeaderView;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255377
    sget-object v0, LX/3ZK;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255378
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryFriendViewPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255379
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackAnniversaryCampaignHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255380
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedPromotionHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255381
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignPermalinkStoryHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255382
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFeedStorySectionHeaderView;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255383
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFriendViewPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255384
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255385
    sget-object v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListFriendPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 255386
    return-void
.end method

.method public final a(LX/1T8;)V
    .locals 2

    .prologue
    .line 255387
    const-class v0, Lcom/facebook/goodwill/feed/rows/ThrowbackYearMarkerFeedUnitEdge$ThrowbackYearMarkerFeedUnit;

    iget-object v1, p0, LX/1U4;->a:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255388
    const-class v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    iget-object v1, p0, LX/1U4;->b:LX/0Ot;

    invoke-virtual {p1, v0, v1}, LX/1T8;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 255389
    return-void
.end method
