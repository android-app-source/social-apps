.class public LX/0iZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/0iZ;


# instance fields
.field public final a:LX/0if;

.field public final b:LX/0iY;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0if;LX/0iY;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 121191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121192
    iput-object p1, p0, LX/0iZ;->a:LX/0if;

    .line 121193
    iput-object p2, p0, LX/0iZ;->b:LX/0iY;

    .line 121194
    iput-object p3, p0, LX/0iZ;->c:LX/0Zb;

    .line 121195
    return-void
.end method

.method public static a(LX/0QB;)LX/0iZ;
    .locals 6

    .prologue
    .line 121196
    sget-object v0, LX/0iZ;->d:LX/0iZ;

    if-nez v0, :cond_1

    .line 121197
    const-class v1, LX/0iZ;

    monitor-enter v1

    .line 121198
    :try_start_0
    sget-object v0, LX/0iZ;->d:LX/0iZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 121199
    if-eqz v2, :cond_0

    .line 121200
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 121201
    new-instance p0, LX/0iZ;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v4

    check-cast v4, LX/0iY;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {p0, v3, v4, v5}, LX/0iZ;-><init>(LX/0if;LX/0iY;LX/0Zb;)V

    .line 121202
    move-object v0, p0

    .line 121203
    sput-object v0, LX/0iZ;->d:LX/0iZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 121205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 121206
    :cond_1
    sget-object v0, LX/0iZ;->d:LX/0iZ;

    return-object v0

    .line 121207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 121208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ia;)V
    .locals 9

    .prologue
    .line 121209
    iget-object v0, p0, LX/0iZ;->a:LX/0if;

    sget-object v1, LX/0ig;->aM:LX/0ih;

    invoke-virtual {p1}, LX/0ia;->getActionName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 121210
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v7

    const-string v8, "default_setting"

    iget-object v4, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v4}, LX/0iY;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v7, v8, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v7

    const-string v8, "global_setting"

    iget-object v4, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v4}, LX/0iY;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    invoke-virtual {v7, v8, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v7

    const-string v8, "has_background_audio"

    iget-object v4, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v4}, LX/0iY;->e()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_2
    invoke-virtual {v7, v8, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v7

    const-string v8, "has_headphones"

    iget-object v4, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v4}, LX/0iY;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v5

    :goto_3
    invoke-virtual {v7, v8, v4}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    const-string v7, "is_app_in_background"

    iget-object v8, p0, LX/0iZ;->b:LX/0iY;

    .line 121211
    iget-boolean p1, v8, LX/0iY;->t:Z

    move v8, p1

    .line 121212
    invoke-virtual {v4, v7, v8}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v4

    const-string v7, "is_mute_switch_on"

    iget-object v8, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v8}, LX/0iY;->g()Z

    move-result v8

    if-nez v8, :cond_4

    :goto_4
    invoke-virtual {v4, v7, v5}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    const-string v5, "volume"

    iget-object v6, p0, LX/0iZ;->b:LX/0iY;

    invoke-virtual {v6}, LX/0iY;->m()I

    move-result v6

    invoke-virtual {v4, v5, v6}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    move-object v4, v4

    .line 121213
    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 121214
    return-void

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    move v4, v6

    goto :goto_1

    :cond_2
    move v4, v6

    goto :goto_2

    :cond_3
    move v4, v6

    goto :goto_3

    :cond_4
    move v5, v6

    goto :goto_4
.end method
