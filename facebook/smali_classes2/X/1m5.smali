.class public final LX/1m5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/04m;


# instance fields
.field private a:LX/04m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 313054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 313047
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1m5;->a:LX/04m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1m5;->a:LX/04m;

    invoke-interface {v0}, LX/04m;->a()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 313053
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a(LX/04m;)V
    .locals 1

    .prologue
    .line 313050
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/1m5;->a:LX/04m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313051
    monitor-exit p0

    return-void

    .line 313052
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 313049
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 313048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
