.class public LX/0XZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/0XZ;


# instance fields
.field public final a:LX/0WS;

.field public final b:LX/0Xb;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Random;

.field private final e:LX/0Xa;

.field public volatile f:LX/0ap;

.field private g:Z


# direct methods
.method public constructor <init>(LX/0Ot;Ljava/util/Random;LX/0WP;LX/0Xa;)V
    .locals 2
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "Ljava/util/Random;",
            "LX/0WP;",
            "LX/0Xa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 79122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79123
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0XZ;->g:Z

    .line 79124
    iput-object p1, p0, LX/0XZ;->c:LX/0Ot;

    .line 79125
    iput-object p2, p0, LX/0XZ;->d:Ljava/util/Random;

    .line 79126
    const-string v0, "analytics_flexible_sampling_policy"

    invoke-virtual {p3, v0}, LX/0WP;->a(Ljava/lang/String;)LX/0WS;

    move-result-object v0

    iput-object v0, p0, LX/0XZ;->a:LX/0WS;

    .line 79127
    new-instance v0, LX/0Xb;

    iget-object v1, p0, LX/0XZ;->a:LX/0WS;

    invoke-direct {v0, v1}, LX/0Xb;-><init>(LX/0WS;)V

    iput-object v0, p0, LX/0XZ;->b:LX/0Xb;

    .line 79128
    iput-object p4, p0, LX/0XZ;->e:LX/0Xa;

    .line 79129
    return-void
.end method

.method public static a(LX/0QB;)LX/0XZ;
    .locals 7

    .prologue
    .line 79109
    sget-object v0, LX/0XZ;->h:LX/0XZ;

    if-nez v0, :cond_1

    .line 79110
    const-class v1, LX/0XZ;

    monitor-enter v1

    .line 79111
    :try_start_0
    sget-object v0, LX/0XZ;->h:LX/0XZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 79112
    if-eqz v2, :cond_0

    .line 79113
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 79114
    new-instance v6, LX/0XZ;

    const/16 v3, 0x2ba

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v3

    check-cast v3, Ljava/util/Random;

    invoke-static {v0}, LX/0WL;->a(LX/0QB;)LX/0WP;

    move-result-object v4

    check-cast v4, LX/0WP;

    invoke-static {v0}, LX/0Xa;->a(LX/0QB;)LX/0Xa;

    move-result-object v5

    check-cast v5, LX/0Xa;

    invoke-direct {v6, p0, v3, v4, v5}, LX/0XZ;-><init>(LX/0Ot;Ljava/util/Random;LX/0WP;LX/0Xa;)V

    .line 79115
    move-object v0, v6

    .line 79116
    sput-object v0, LX/0XZ;->h:LX/0XZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79117
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 79118
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 79119
    :cond_1
    sget-object v0, LX/0XZ;->h:LX/0XZ;

    return-object v0

    .line 79120
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 79121
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0lF;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79105
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {p0}, LX/0lF;->e()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 79106
    invoke-virtual {p0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 79107
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79108
    :cond_0
    return-object v1
.end method

.method public static a(LX/0XZ;Ljava/lang/String;LX/1gW;LX/0lF;)V
    .locals 4

    .prologue
    .line 79094
    invoke-virtual {p3}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v2

    .line 79095
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79096
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 79097
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "*"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79098
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-interface {p2, p1, v0}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    goto :goto_0

    .line 79099
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79100
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 79101
    invoke-virtual {v0}, LX/0lF;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 79102
    invoke-static {p0, v1, p2, v0}, LX/0XZ;->a(LX/0XZ;Ljava/lang/String;LX/1gW;LX/0lF;)V

    goto :goto_0

    .line 79103
    :cond_1
    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v0

    invoke-interface {p2, v1, v0}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    goto :goto_0

    .line 79104
    :cond_2
    return-void
.end method

.method private declared-synchronized g()Z
    .locals 5

    .prologue
    .line 79082
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {v0}, LX/0Xc;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 79083
    const/4 v0, 0x1

    .line 79084
    :goto_0
    monitor-exit p0

    return v0

    .line 79085
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/0XZ;->e:LX/0Xa;

    .line 79086
    iget-object v1, v0, LX/0Xa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 79087
    :cond_1
    :goto_1
    iget-object v0, p0, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {v0}, LX/0Xc;->b()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 79088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 79089
    :cond_2
    iget-object v1, v0, LX/0Xa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0uQ;->i:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79090
    iget-object v2, v0, LX/0Xa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0uQ;->h:LX/0Tn;

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79091
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 79092
    invoke-virtual {p0, v1, v2}, LX/0XZ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79093
    iget-object v1, v0, LX/0Xa;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0uQ;->i:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0uQ;->h:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79080
    invoke-direct {p0}, LX/0XZ;->g()Z

    .line 79081
    iget-object v0, p0, LX/0XZ;->b:LX/0Xb;

    invoke-virtual {v0, p1, p2, p3}, LX/0Xc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)LX/03R;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79072
    invoke-virtual {p0, p1, v0, v0}, LX/0XZ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 79073
    if-gez v1, :cond_0

    .line 79074
    sget-object v1, LX/03R;->UNSET:LX/03R;

    .line 79075
    :goto_0
    move-object v0, v1

    .line 79076
    return-object v0

    .line 79077
    :cond_0
    if-nez v1, :cond_1

    .line 79078
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 79079
    :cond_1
    iget-object v2, p0, LX/0XZ;->d:Ljava/util/Random;

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/03R;->YES:LX/03R;

    goto :goto_0

    :cond_2
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 79051
    iget-object v0, p0, LX/0XZ;->a:LX/0WS;

    invoke-virtual {v0}, LX/0WS;->b()LX/1gW;

    move-result-object v1

    .line 79052
    invoke-interface {v1}, LX/1gW;->a()LX/1gW;

    .line 79053
    :try_start_0
    iget-object v0, p0, LX/0XZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0, p2}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 79054
    invoke-virtual {v0}, LX/0lF;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79055
    const-string v2, "__fs_policy_blacklisted_events__"

    invoke-static {v0}, LX/0XZ;->a(LX/0lF;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/1gW;->a(Ljava/lang/String;Ljava/util/Set;)LX/1gW;

    .line 79056
    :cond_0
    const-string v0, "__fs_policy_config_checksum__"

    invoke-interface {v1, v0, p1}, LX/1gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/1gW;

    move-result-object v0

    invoke-interface {v0}, LX/1gW;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79057
    :goto_0
    return-void

    .line 79058
    :catch_0
    move-exception v0

    .line 79059
    const-string v1, "AnalyticsLoggingPolicy"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, p2, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 79060
    :cond_1
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v4

    .line 79061
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79062
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 79063
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v5, "blacklist"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79064
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 79065
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/0lF;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79066
    const-string v3, "__fs_policy_blacklisted_events__"

    invoke-static {v2}, LX/0XZ;->a(LX/0lF;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/1gW;->a(Ljava/lang/String;Ljava/util/Set;)LX/1gW;

    goto :goto_1

    .line 79067
    :cond_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 79068
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 79069
    invoke-virtual {v2}, LX/0lF;->i()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 79070
    invoke-static {p0, v3, v1, v2}, LX/0XZ;->a(LX/0XZ;Ljava/lang/String;LX/1gW;LX/0lF;)V

    goto :goto_1

    .line 79071
    :cond_4
    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    invoke-interface {v1, v3, v2}, LX/1gW;->a(Ljava/lang/String;I)LX/1gW;

    goto :goto_1
.end method

.method public final c()LX/0ap;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 79044
    iget-boolean v0, p0, LX/0XZ;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/0XZ;->f:LX/0ap;

    if-eqz v0, :cond_1

    .line 79045
    :cond_0
    iget-object v0, p0, LX/0XZ;->f:LX/0ap;

    .line 79046
    :goto_0
    return-object v0

    .line 79047
    :cond_1
    invoke-direct {p0}, LX/0XZ;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 79048
    const/4 v0, 0x0

    goto :goto_0

    .line 79049
    :cond_2
    new-instance v0, LX/0ap;

    iget-object v1, p0, LX/0XZ;->b:LX/0Xb;

    invoke-direct {v0, v1}, LX/0ap;-><init>(LX/0Xb;)V

    iput-object v0, p0, LX/0XZ;->f:LX/0ap;

    .line 79050
    iget-object v0, p0, LX/0XZ;->f:LX/0ap;

    goto :goto_0
.end method

.method public final d()Ljava/util/Random;
    .locals 1

    .prologue
    .line 79043
    iget-object v0, p0, LX/0XZ;->d:Ljava/util/Random;

    return-object v0
.end method
