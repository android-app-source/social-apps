.class public final enum LX/0gf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0gf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0gf;

.field public static final enum AUTO_REFRESH:LX/0gf;

.field public static final enum BACK_TO_BACK_PTR:LX/0gf;

.field public static final enum ERROR_RETRY:LX/0gf;

.field public static final enum FROM_PUSH_NOTIFICATION:LX/0gf;

.field public static final enum INITIALIZATION:LX/0gf;

.field public static final enum INITIALIZATION_RERANK:LX/0gf;

.field public static final enum OFFLINE_FEED:LX/0gf;

.field public static final enum ONE_WAY_FEED_BACK:LX/0gf;

.field public static final enum POLLING:LX/0gf;

.field public static final enum PREFETCH:LX/0gf;

.field public static final enum PULL_TO_REFRESH:LX/0gf;

.field public static final enum RERANK:LX/0gf;

.field public static final enum SCROLLING:LX/0gf;

.field public static final enum SCROLL_TOP:LX/0gf;

.field public static final enum SCROLL_TOP_MANUAL:LX/0gf;

.field public static final enum SKIP_TAIL_GAP:LX/0gf;

.field public static final enum TAB_CLICK:LX/0gf;

.field public static final enum TAB_CLICK_MANUAL:LX/0gf;

.field public static final enum UNKNOWN:LX/0gf;

.field public static final enum WARM_START:LX/0gf;


# instance fields
.field private extras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 112865
    new-instance v0, LX/0gf;

    const-string v1, "WARM_START"

    const-string v2, "warm"

    invoke-direct {v0, v1, v4, v2}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->WARM_START:LX/0gf;

    .line 112866
    new-instance v0, LX/0gf;

    const-string v1, "POLLING"

    const-string v2, "nsp"

    invoke-direct {v0, v1, v5, v2}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->POLLING:LX/0gf;

    .line 112867
    new-instance v0, LX/0gf;

    const-string v1, "BACK_TO_BACK_PTR"

    const-string v2, "back_to_back_manual"

    invoke-direct {v0, v1, v6, v2}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    .line 112868
    new-instance v0, LX/0gf;

    const-string v1, "PULL_TO_REFRESH"

    const-string v2, "manual"

    invoke-direct {v0, v1, v7, v2}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    .line 112869
    new-instance v0, LX/0gf;

    const-string v1, "ERROR_RETRY"

    const-string v2, "auto"

    invoke-direct {v0, v1, v8, v2}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->ERROR_RETRY:LX/0gf;

    .line 112870
    new-instance v0, LX/0gf;

    const-string v1, "SCROLLING"

    const/4 v2, 0x5

    const-string v3, "auto"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->SCROLLING:LX/0gf;

    .line 112871
    new-instance v0, LX/0gf;

    const-string v1, "INITIALIZATION"

    const/4 v2, 0x6

    const-string v3, "warm"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    .line 112872
    new-instance v0, LX/0gf;

    const-string v1, "AUTO_REFRESH"

    const/4 v2, 0x7

    const-string v3, "auto"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    .line 112873
    new-instance v0, LX/0gf;

    const-string v1, "PREFETCH"

    const/16 v2, 0x8

    const-string v3, "background"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->PREFETCH:LX/0gf;

    .line 112874
    new-instance v0, LX/0gf;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    const-string v3, "auto"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->UNKNOWN:LX/0gf;

    .line 112875
    new-instance v0, LX/0gf;

    const-string v1, "RERANK"

    const/16 v2, 0xa

    const-string v3, "rerank"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->RERANK:LX/0gf;

    .line 112876
    new-instance v0, LX/0gf;

    const-string v1, "INITIALIZATION_RERANK"

    const/16 v2, 0xb

    const-string v3, "rerank"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    .line 112877
    new-instance v0, LX/0gf;

    const-string v1, "SKIP_TAIL_GAP"

    const/16 v2, 0xc

    const-string v3, "skip_tail_gap"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    .line 112878
    new-instance v0, LX/0gf;

    const-string v1, "FROM_PUSH_NOTIFICATION"

    const/16 v2, 0xd

    const-string v3, "auto"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    .line 112879
    new-instance v0, LX/0gf;

    const-string v1, "SCROLL_TOP"

    const/16 v2, 0xe

    const-string v3, "scroll_top"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    .line 112880
    new-instance v0, LX/0gf;

    const-string v1, "SCROLL_TOP_MANUAL"

    const/16 v2, 0xf

    const-string v3, "manual"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    .line 112881
    new-instance v0, LX/0gf;

    const-string v1, "ONE_WAY_FEED_BACK"

    const/16 v2, 0x10

    const-string v3, "oneway_feed_back"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    .line 112882
    new-instance v0, LX/0gf;

    const-string v1, "TAB_CLICK"

    const/16 v2, 0x11

    const-string v3, "tab_click"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    .line 112883
    new-instance v0, LX/0gf;

    const-string v1, "TAB_CLICK_MANUAL"

    const/16 v2, 0x12

    const-string v3, "manual"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    .line 112884
    new-instance v0, LX/0gf;

    const-string v1, "OFFLINE_FEED"

    const/16 v2, 0x13

    const-string v3, "manual"

    invoke-direct {v0, v1, v2, v3}, LX/0gf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0gf;->OFFLINE_FEED:LX/0gf;

    .line 112885
    const/16 v0, 0x14

    new-array v0, v0, [LX/0gf;

    sget-object v1, LX/0gf;->WARM_START:LX/0gf;

    aput-object v1, v0, v4

    sget-object v1, LX/0gf;->POLLING:LX/0gf;

    aput-object v1, v0, v5

    sget-object v1, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    aput-object v1, v0, v6

    sget-object v1, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    aput-object v1, v0, v7

    sget-object v1, LX/0gf;->ERROR_RETRY:LX/0gf;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/0gf;->SCROLLING:LX/0gf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0gf;->INITIALIZATION:LX/0gf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0gf;->AUTO_REFRESH:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0gf;->PREFETCH:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0gf;->UNKNOWN:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0gf;->RERANK:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0gf;->SCROLL_TOP:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/0gf;->ONE_WAY_FEED_BACK:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/0gf;->TAB_CLICK:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/0gf;->OFFLINE_FEED:LX/0gf;

    aput-object v2, v0, v1

    sput-object v0, LX/0gf;->$VALUES:[LX/0gf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112861
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112862
    iput-object p3, p0, LX/0gf;->name:Ljava/lang/String;

    .line 112863
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    .line 112864
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0gf;
    .locals 1

    .prologue
    .line 112860
    const-class v0, LX/0gf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0gf;

    return-object v0
.end method

.method public static values()[LX/0gf;
    .locals 1

    .prologue
    .line 112859
    sget-object v0, LX/0gf;->$VALUES:[LX/0gf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0gf;

    return-object v0
.end method


# virtual methods
.method public final getExtra(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 112858
    iget-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final isAutoRefresh()Z
    .locals 1

    .prologue
    .line 112857
    sget-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->WARM_START:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->POLLING:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFetchAtTopOfFeed()Z
    .locals 1

    .prologue
    .line 112856
    invoke-virtual {p0}, LX/0gf;->isManual()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0gf;->isInitialization()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->WARM_START:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFreshFeedAutoRefresh()Z
    .locals 1

    .prologue
    .line 112855
    sget-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->POLLING:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFreshFeedNewStoriesEligible()Z
    .locals 1

    .prologue
    .line 112886
    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->ERROR_RETRY:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialization()Z
    .locals 1

    .prologue
    .line 112854
    sget-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->INITIALIZATION_RERANK:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isManual()Z
    .locals 1

    .prologue
    .line 112853
    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNewStoriesFetch()Z
    .locals 1

    .prologue
    .line 112852
    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->ERROR_RETRY:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->AUTO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->POLLING:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->WARM_START:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isScrollHeadFetch()Z
    .locals 1

    .prologue
    .line 112851
    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final needsEmptyReranking()Z
    .locals 1

    .prologue
    .line 112850
    invoke-virtual {p0}, LX/0gf;->isAutoRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/0gf;->INITIALIZATION:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final needsReranking()Z
    .locals 1

    .prologue
    .line 112849
    sget-object v0, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->RERANK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->BACK_TO_BACK_PTR:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->FROM_PUSH_NOTIFICATION:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->SCROLL_TOP_MANUAL:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK:LX/0gf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/0gf;->TAB_CLICK_MANUAL:LX/0gf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setExtras(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 112843
    iget-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    const-string v1, "object_id"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112844
    iget-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    const-string v1, "href"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112845
    iget-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    const-string v1, "user_id"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112846
    iget-object v0, p0, LX/0gf;->extras:Ljava/util/HashMap;

    const-string v1, "type"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112847
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112848
    iget-object v0, p0, LX/0gf;->name:Ljava/lang/String;

    return-object v0
.end method
