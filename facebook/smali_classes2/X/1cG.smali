.class public LX/1cG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Z

.field public static final b:Z

.field public static final c:Z

.field public static d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

.field public static e:Z

.field public static f:Z

.field private static final g:[B

.field private static final h:[B

.field public static final i:[B

.field public static final j:[B

.field public static final k:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 281736
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-gt v0, v3, :cond_1

    move v0, v1

    :goto_0
    sput-boolean v0, LX/1cG;->a:Z

    .line 281737
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_2

    :goto_1
    sput-boolean v1, LX/1cG;->b:Z

    .line 281738
    const/16 v4, 0x11

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 281739
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v3, v4, :cond_3

    .line 281740
    :cond_0
    :goto_2
    move v0, v0

    .line 281741
    sput-boolean v0, LX/1cG;->c:Z

    .line 281742
    const/4 v0, 0x0

    sput-object v0, LX/1cG;->d:Lcom/facebook/webpsupport/WebpBitmapFactoryImpl;

    .line 281743
    sput-boolean v2, LX/1cG;->e:Z

    .line 281744
    sput-boolean v2, LX/1cG;->f:Z

    .line 281745
    const-string v0, "RIFF"

    invoke-static {v0}, LX/1cG;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1cG;->g:[B

    .line 281746
    const-string v0, "WEBP"

    invoke-static {v0}, LX/1cG;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1cG;->h:[B

    .line 281747
    const-string v0, "VP8 "

    invoke-static {v0}, LX/1cG;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1cG;->i:[B

    .line 281748
    const-string v0, "VP8L"

    invoke-static {v0}, LX/1cG;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1cG;->j:[B

    .line 281749
    const-string v0, "VP8X"

    invoke-static {v0}, LX/1cG;->a(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, LX/1cG;->k:[B

    return-void

    :cond_1
    move v0, v2

    .line 281750
    goto :goto_0

    :cond_2
    move v1, v2

    .line 281751
    goto :goto_1

    .line 281752
    :cond_3
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v3, v4, :cond_4

    .line 281753
    const-string v3, "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA=="

    invoke-static {v3, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 281754
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 281755
    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 281756
    array-length v5, v3

    invoke-static {v3, v0, v5, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 281757
    iget v3, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v3, v1, :cond_0

    iget v3, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v3, v1, :cond_0

    :cond_4
    move v0, v1

    .line 281758
    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 281759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BI[B)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 281760
    if-eqz p2, :cond_0

    if-nez p0, :cond_1

    .line 281761
    :cond_0
    :goto_0
    return v1

    .line 281762
    :cond_1
    array-length v0, p2

    add-int/2addr v0, p1

    array-length v2, p0

    if-gt v0, v2, :cond_0

    move v0, v1

    .line 281763
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_2

    .line 281764
    add-int v2, v0, p1

    aget-byte v2, p0, v2

    aget-byte v3, p2, v0

    if-ne v2, v3, :cond_0

    .line 281765
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 281766
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 281767
    :try_start_0
    const-string v0, "ASCII"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 281768
    :catch_0
    move-exception v0

    .line 281769
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "ASCII not found!"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static b([BII)Z
    .locals 2

    .prologue
    .line 281770
    const/16 v0, 0x14

    if-lt p2, v0, :cond_0

    sget-object v0, LX/1cG;->g:[B

    invoke-static {p0, p1, v0}, LX/1cG;->a([BI[B)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, p1, 0x8

    sget-object v1, LX/1cG;->h:[B

    invoke-static {p0, v0, v1}, LX/1cG;->a([BI[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
