.class public LX/1hI;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final a:LX/1Mk;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0y2;

.field private final d:J

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 295746
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "network_type"

    aput-object v1, v0, v3

    const-string v1, "phone_type"

    aput-object v1, v0, v4

    const-string v1, "sim_country_iso"

    aput-object v1, v0, v5

    const-string v1, "sim_operator_name"

    aput-object v1, v0, v6

    const-string v1, "network_country_iso"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "network_operator_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_network_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "signal_level"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "signal_asu_level"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "signal_dbm"

    aput-object v2, v0, v1

    sput-object v0, LX/1hI;->f:[Ljava/lang/String;

    .line 295747
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sim_operator_mcc_mnc"

    aput-object v1, v0, v3

    const-string v1, "has_icc_card"

    aput-object v1, v0, v4

    const-string v1, "cdma_base_station_id"

    aput-object v1, v0, v5

    const-string v1, "cdma_base_station_latitude"

    aput-object v1, v0, v6

    const-string v1, "cdma_base_station_longitude"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "cdma_network_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "cdma_system_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "network_operator_mcc_mnc"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "gsm_cid"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "gsm_lac"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "gsm_psc"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "lte_ci"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "lte_mcc"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "lte_mnc"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "lte_pci"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "lte_tac"

    aput-object v2, v0, v1

    sput-object v0, LX/1hI;->g:[Ljava/lang/String;

    .line 295748
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cdma_base_station_id"

    aput-object v1, v0, v3

    const-string v1, "cdma_network_id"

    aput-object v1, v0, v4

    const-string v1, "cdma_system_id"

    aput-object v1, v0, v5

    const-string v1, "gsm_cid"

    aput-object v1, v0, v6

    const-string v1, "gsm_lac"

    aput-object v1, v0, v7

    sput-object v0, LX/1hI;->h:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Mk;LX/0Or;LX/0y2;LX/0ad;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Mk;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0y2;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295741
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mk;

    iput-object v0, p0, LX/1hI;->a:LX/1Mk;

    .line 295742
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    iput-object v0, p0, LX/1hI;->b:LX/0Or;

    .line 295743
    iput-object p3, p0, LX/1hI;->c:LX/0y2;

    .line 295744
    sget-wide v0, LX/0by;->v:J

    const-wide/16 v2, -0x1

    invoke-interface {p4, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/1hI;->d:J

    .line 295745
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295734
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 295735
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 295736
    if-eqz v3, :cond_0

    .line 295737
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295738
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 295739
    :cond_1
    return-void
.end method

.method private a(Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 295702
    iget-object v0, p0, LX/1hI;->a:LX/1Mk;

    invoke-virtual {v0}, LX/1Mk;->b()Ljava/util/Map;

    move-result-object v2

    .line 295703
    if-eqz v2, :cond_0

    .line 295704
    if-eqz p2, :cond_1

    .line 295705
    invoke-static {v2}, LX/1Mk;->a(Ljava/util/Map;)I

    move-result v0

    iput v0, p0, LX/1hI;->e:I

    .line 295706
    :cond_0
    :goto_0
    return-void

    .line 295707
    :cond_1
    sget-object v0, LX/1hI;->f:[Ljava/lang/String;

    invoke-static {p1, v2, v0}, LX/1hI;->a(Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;)V

    .line 295708
    iget-object v0, p0, LX/1hI;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295709
    iget-object v0, p0, LX/1hI;->c:LX/0y2;

    .line 295710
    iget-object v3, v0, LX/0y2;->c:Landroid/location/LocationManager;

    move-object v0, v3

    .line 295711
    const-string v3, "gps"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/1hI;->c:LX/0y2;

    .line 295712
    iget-object v3, v0, LX/0y2;->c:Landroid/location/LocationManager;

    move-object v0, v3

    .line 295713
    const-string v3, "network"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 295714
    if-eqz v0, :cond_3

    .line 295715
    sget-object v0, LX/1hI;->g:[Ljava/lang/String;

    invoke-static {p1, v2, v0}, LX/1hI;->a(Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;)V

    .line 295716
    :cond_3
    const-string v3, "tower_changed"

    iget v0, p0, LX/1hI;->e:I

    invoke-static {v2}, LX/1Mk;->a(Ljava/util/Map;)I

    move-result v2

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 295727
    sget-object v3, LX/1hI;->h:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 295728
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 295729
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 295730
    const/4 v0, 0x1

    .line 295731
    :goto_1
    return v0

    .line 295732
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 295733
    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295725
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1hI;->a(Ljava/util/Map;Z)V

    .line 295726
    return-void
.end method

.method public final b(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295723
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1hI;->a(Ljava/util/Map;Z)V

    .line 295724
    return-void
.end method

.method public final c(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295717
    iget-wide v0, p0, LX/1hI;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 295718
    :cond_0
    :goto_0
    return-void

    .line 295719
    :cond_1
    iget-object v0, p0, LX/1hI;->c:LX/0y2;

    iget-wide v2, p0, LX/1hI;->d:J

    invoke-virtual {v0, v2, v3}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 295720
    if-eqz v0, :cond_0

    .line 295721
    const-string v1, "device_lat"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295722
    const-string v1, "device_long"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
