.class public LX/1aX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "LX/1aY;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/memory/MemoryUiTrimmable;",
        "Lcom/facebook/drawee/drawable/VisibilityCallback;"
    }
.end annotation


# instance fields
.field private a:Z

.field public b:Z

.field private c:Z

.field public d:Z

.field public e:LX/1aY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDH;"
        }
    .end annotation
.end field

.field public f:LX/1aZ;

.field public final g:LX/1ab;


# direct methods
.method public constructor <init>(LX/1aY;)V
    .locals 2
    .param p1    # LX/1aY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 277829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 277830
    iput-boolean v1, p0, LX/1aX;->a:Z

    .line 277831
    iput-boolean v1, p0, LX/1aX;->b:Z

    .line 277832
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1aX;->c:Z

    .line 277833
    iput-boolean v1, p0, LX/1aX;->d:Z

    .line 277834
    const/4 v0, 0x0

    iput-object v0, p0, LX/1aX;->f:LX/1aZ;

    .line 277835
    invoke-static {}, LX/1ab;->a()LX/1ab;

    move-result-object v0

    iput-object v0, p0, LX/1aX;->g:LX/1ab;

    .line 277836
    if-eqz p1, :cond_0

    .line 277837
    invoke-virtual {p0, p1}, LX/1aX;->a(LX/1aY;)V

    .line 277838
    :cond_0
    return-void
.end method

.method public static a(LX/1aY;Landroid/content/Context;)LX/1aX;
    .locals 1
    .param p0    # LX/1aY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<DH::",
            "LX/1aY;",
            ">(TDH;",
            "Landroid/content/Context;",
            ")",
            "LX/1aX",
            "<TDH;>;"
        }
    .end annotation

    .prologue
    .line 277826
    new-instance v0, LX/1aX;

    invoke-direct {v0, p0}, LX/1aX;-><init>(LX/1aY;)V

    .line 277827
    sget-object p0, LX/1ac;->a:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 277828
    return-object v0
.end method

.method private a(LX/1aX;)V
    .locals 2
    .param p1    # LX/1aX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277822
    invoke-virtual {p0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 277823
    instance-of v1, v0, LX/1as;

    if-eqz v1, :cond_0

    .line 277824
    check-cast v0, LX/1as;

    invoke-interface {v0, p1}, LX/1as;->a(LX/1aX;)V

    .line 277825
    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 277752
    iget-boolean v0, p0, LX/1aX;->a:Z

    if-eqz v0, :cond_1

    .line 277753
    :cond_0
    :goto_0
    return-void

    .line 277754
    :cond_1
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_ATTACH_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 277755
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1aX;->a:Z

    .line 277756
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    invoke-interface {v0}, LX/1aZ;->c()LX/1aY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 277757
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    invoke-interface {v0}, LX/1aZ;->d()V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 277816
    iget-boolean v0, p0, LX/1aX;->a:Z

    if-nez v0, :cond_1

    .line 277817
    :cond_0
    :goto_0
    return-void

    .line 277818
    :cond_1
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_DETACH_CONTROLLER:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 277819
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1aX;->a:Z

    .line 277820
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    if-eqz v0, :cond_0

    .line 277821
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    invoke-interface {v0}, LX/1aZ;->e()V

    goto :goto_0
.end method

.method public static n(LX/1aX;)V
    .locals 1

    .prologue
    .line 277812
    iget-boolean v0, p0, LX/1aX;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/1aX;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/1aX;->d:Z

    if-nez v0, :cond_0

    .line 277813
    invoke-direct {p0}, LX/1aX;->l()V

    .line 277814
    :goto_0
    return-void

    .line 277815
    :cond_0
    invoke-direct {p0}, LX/1aX;->m()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    .prologue
    .line 277802
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_SET_HIERARCHY:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 277803
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/1aX;->a(LX/1aX;)V

    .line 277804
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aY;

    iput-object v0, p0, LX/1aX;->e:LX/1aY;

    .line 277805
    iget-object v0, p0, LX/1aX;->e:LX/1aY;

    invoke-interface {v0}, LX/1aY;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 277806
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/1aX;->a(Z)V

    .line 277807
    invoke-direct {p0, p0}, LX/1aX;->a(LX/1aX;)V

    .line 277808
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    if-eqz v0, :cond_1

    .line 277809
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    invoke-interface {v0, p1}, LX/1aZ;->a(LX/1aY;)V

    .line 277810
    :cond_1
    return-void

    .line 277811
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/1aZ;)V
    .locals 3
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277788
    iget-boolean v0, p0, LX/1aX;->a:Z

    .line 277789
    if-eqz v0, :cond_0

    .line 277790
    invoke-direct {p0}, LX/1aX;->m()V

    .line 277791
    :cond_0
    iget-object v1, p0, LX/1aX;->f:LX/1aZ;

    if-eqz v1, :cond_1

    .line 277792
    iget-object v1, p0, LX/1aX;->g:LX/1ab;

    sget-object v2, LX/1at;->ON_CLEAR_OLD_CONTROLLER:LX/1at;

    invoke-virtual {v1, v2}, LX/1ab;->a(LX/1at;)V

    .line 277793
    iget-object v1, p0, LX/1aX;->f:LX/1aZ;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1aZ;->a(LX/1aY;)V

    .line 277794
    :cond_1
    iput-object p1, p0, LX/1aX;->f:LX/1aZ;

    .line 277795
    iget-object v1, p0, LX/1aX;->f:LX/1aZ;

    if-eqz v1, :cond_3

    .line 277796
    iget-object v1, p0, LX/1aX;->g:LX/1ab;

    sget-object v2, LX/1at;->ON_SET_CONTROLLER:LX/1at;

    invoke-virtual {v1, v2}, LX/1ab;->a(LX/1at;)V

    .line 277797
    iget-object v1, p0, LX/1aX;->f:LX/1aZ;

    iget-object v2, p0, LX/1aX;->e:LX/1aY;

    invoke-interface {v1, v2}, LX/1aZ;->a(LX/1aY;)V

    .line 277798
    :goto_0
    if-eqz v0, :cond_2

    .line 277799
    invoke-direct {p0}, LX/1aX;->l()V

    .line 277800
    :cond_2
    return-void

    .line 277801
    :cond_3
    iget-object v1, p0, LX/1aX;->g:LX/1ab;

    sget-object v2, LX/1at;->ON_CLEAR_CONTROLLER:LX/1at;

    invoke-virtual {v1, v2}, LX/1ab;->a(LX/1at;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 277782
    iget-boolean v0, p0, LX/1aX;->c:Z

    if-ne v0, p1, :cond_0

    .line 277783
    :goto_0
    return-void

    .line 277784
    :cond_0
    iget-object v1, p0, LX/1aX;->g:LX/1ab;

    if-eqz p1, :cond_1

    sget-object v0, LX/1at;->ON_DRAWABLE_SHOW:LX/1at;

    :goto_1
    invoke-virtual {v1, v0}, LX/1ab;->a(LX/1at;)V

    .line 277785
    iput-boolean p1, p0, LX/1aX;->c:Z

    .line 277786
    invoke-static {p0}, LX/1aX;->n(LX/1aX;)V

    goto :goto_0

    .line 277787
    :cond_1
    sget-object v0, LX/1at;->ON_DRAWABLE_HIDE:LX/1at;

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 277779
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    if-nez v0, :cond_0

    .line 277780
    const/4 v0, 0x0

    .line 277781
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1aX;->f:LX/1aZ;

    invoke-interface {v0, p1}, LX/1aZ;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 277769
    iget-boolean v0, p0, LX/1aX;->a:Z

    if-eqz v0, :cond_0

    .line 277770
    :goto_0
    return-void

    .line 277771
    :cond_0
    iget-boolean v0, p0, LX/1aX;->d:Z

    if-nez v0, :cond_1

    .line 277772
    const-class v0, LX/1ab;

    const-string v1, "%x: Draw requested for a non-attached controller %x. %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, LX/1aX;->f:LX/1aZ;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-virtual {p0}, LX/1aX;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 277773
    sget-object v3, LX/03J;->a:LX/03G;

    const/4 v4, 0x6

    invoke-interface {v3, v4}, LX/03G;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 277774
    sget-object v3, LX/03J;->a:LX/03G;

    invoke-static {v0}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v4, v7}, LX/03G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 277775
    :cond_1
    iput-boolean v6, p0, LX/1aX;->d:Z

    .line 277776
    iput-boolean v5, p0, LX/1aX;->b:Z

    .line 277777
    iput-boolean v5, p0, LX/1aX;->c:Z

    .line 277778
    invoke-static {p0}, LX/1aX;->n(LX/1aX;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 277765
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_HOLDER_ATTACH:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 277766
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1aX;->b:Z

    .line 277767
    invoke-static {p0}, LX/1aX;->n(LX/1aX;)V

    .line 277768
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 277761
    iget-object v0, p0, LX/1aX;->g:LX/1ab;

    sget-object v1, LX/1at;->ON_HOLDER_DETACH:LX/1at;

    invoke-virtual {v0, v1}, LX/1ab;->a(LX/1at;)V

    .line 277762
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1aX;->b:Z

    .line 277763
    invoke-static {p0}, LX/1aX;->n(LX/1aX;)V

    .line 277764
    return-void
.end method

.method public final h()LX/1aY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDH;"
        }
    .end annotation

    .prologue
    .line 277760
    iget-object v0, p0, LX/1aX;->e:LX/1aY;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aY;

    return-object v0
.end method

.method public final j()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 277759
    iget-object v0, p0, LX/1aX;->e:LX/1aY;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/1aX;->e:LX/1aY;

    invoke-interface {v0}, LX/1aY;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277758
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "controllerAttached"

    iget-boolean v2, p0, LX/1aX;->a:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "holderAttached"

    iget-boolean v2, p0, LX/1aX;->b:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "drawableVisible"

    iget-boolean v2, p0, LX/1aX;->c:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "trimmed"

    iget-boolean v2, p0, LX/1aX;->d:Z

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Z)LX/15g;

    move-result-object v0

    const-string v1, "events"

    iget-object v2, p0, LX/1aX;->g:LX/1ab;

    invoke-virtual {v2}, LX/1ab;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
