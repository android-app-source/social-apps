.class public LX/1Pz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pb;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Sh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244222
    iput-object p1, p0, LX/1Pz;->a:LX/0Ot;

    .line 244223
    iput-object p2, p0, LX/1Pz;->b:LX/0Sh;

    .line 244224
    iput-object p3, p0, LX/1Pz;->c:LX/0Ot;

    .line 244225
    return-void
.end method

.method public static a(LX/0QB;)LX/1Pz;
    .locals 1

    .prologue
    .line 244220
    invoke-static {p0}, LX/1Pz;->b(LX/0QB;)LX/1Pz;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244226
    iget-object v0, p0, LX/1Pz;->b:LX/0Sh;

    new-instance v1, LX/99M;

    invoke-direct {v1, p0}, LX/99M;-><init>(LX/1Pz;)V

    invoke-virtual {v0, p1, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 244227
    return-void
.end method

.method public static b(LX/0QB;)LX/1Pz;
    .locals 4

    .prologue
    .line 244218
    new-instance v1, LX/1Pz;

    const/16 v0, 0xa71

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    const/16 v3, 0xa7a

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/1Pz;-><init>(LX/0Ot;LX/0Sh;LX/0Ot;)V

    .line 244219
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244215
    iget-object v0, p0, LX/1Pz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-virtual {v0, p1, p2}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 244216
    invoke-direct {p0, v0}, LX/1Pz;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 244217
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 244212
    iget-object v0, p0, LX/1Pz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-virtual {v0, p1, p2}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 244213
    invoke-direct {p0, v0}, LX/1Pz;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 244214
    return-void
.end method
