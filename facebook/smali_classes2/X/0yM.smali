.class public LX/0yM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0yM;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 164670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164671
    iput-object p1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 164672
    return-void
.end method

.method public static a(LX/0QB;)LX/0yM;
    .locals 4

    .prologue
    .line 164657
    sget-object v0, LX/0yM;->b:LX/0yM;

    if-nez v0, :cond_1

    .line 164658
    const-class v1, LX/0yM;

    monitor-enter v1

    .line 164659
    :try_start_0
    sget-object v0, LX/0yM;->b:LX/0yM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164660
    if-eqz v2, :cond_0

    .line 164661
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 164662
    new-instance p0, LX/0yM;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/0yM;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 164663
    move-object v0, p0

    .line 164664
    sput-object v0, LX/0yM;->b:LX/0yM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164665
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164666
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164667
    :cond_1
    sget-object v0, LX/0yM;->b:LX/0yM;

    return-object v0

    .line 164668
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164654
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_0

    .line 164655
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 164656
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 164653
    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0Tm;->a:LX/0Tn;

    invoke-static {p1}, LX/0yM;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 4

    .prologue
    .line 164673
    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0Tm;->a:LX/0Tn;

    invoke-static {p1}, LX/0yM;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2, p3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/1p2;
    .locals 2

    .prologue
    .line 164652
    new-instance v0, LX/1p1;

    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/1p1;-><init>(LX/0yM;LX/0hN;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 164651
    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0Tm;->a:LX/0Tn;

    invoke-static {p1}, LX/0yM;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 164650
    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0Tm;->a:LX/0Tn;

    invoke-static {p1}, LX/0yM;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    .line 164649
    iget-object v1, p0, LX/0yM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/0Tm;->a:LX/0Tn;

    invoke-static {p1}, LX/0yM;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
