.class public final enum LX/0rR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0rR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0rR;

.field public static final enum ALLOW:LX/0rR;

.field public static final enum DENY:LX/0rR;


# instance fields
.field private final mRequestResult:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149580
    new-instance v0, LX/0rR;

    const-string v1, "ALLOW"

    const-string v2, "allow"

    invoke-direct {v0, v1, v3, v2}, LX/0rR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rR;->ALLOW:LX/0rR;

    .line 149581
    new-instance v0, LX/0rR;

    const-string v1, "DENY"

    const-string v2, "deny"

    invoke-direct {v0, v1, v4, v2}, LX/0rR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0rR;->DENY:LX/0rR;

    .line 149582
    const/4 v0, 0x2

    new-array v0, v0, [LX/0rR;

    sget-object v1, LX/0rR;->ALLOW:LX/0rR;

    aput-object v1, v0, v3

    sget-object v1, LX/0rR;->DENY:LX/0rR;

    aput-object v1, v0, v4

    sput-object v0, LX/0rR;->$VALUES:[LX/0rR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149583
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149584
    iput-object p3, p0, LX/0rR;->mRequestResult:Ljava/lang/String;

    .line 149585
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0rR;
    .locals 1

    .prologue
    .line 149586
    const-class v0, LX/0rR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0rR;

    return-object v0
.end method

.method public static values()[LX/0rR;
    .locals 1

    .prologue
    .line 149587
    sget-object v0, LX/0rR;->$VALUES:[LX/0rR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0rR;

    return-object v0
.end method


# virtual methods
.method public final getRequestResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149588
    iget-object v0, p0, LX/0rR;->mRequestResult:Ljava/lang/String;

    return-object v0
.end method
