.class public final LX/17b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17c;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile u:LX/17b;


# instance fields
.field private a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HmW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hn1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JaN;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6sK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FRX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/J1p;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EBM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fof;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FkO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G61;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G6P;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 199028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199029
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199030
    iput-object v0, p0, LX/17b;->a:LX/0Ot;

    .line 199031
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199032
    iput-object v0, p0, LX/17b;->b:LX/0Ot;

    .line 199033
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199034
    iput-object v0, p0, LX/17b;->c:LX/0Ot;

    .line 199035
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199036
    iput-object v0, p0, LX/17b;->d:LX/0Ot;

    .line 199037
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199038
    iput-object v0, p0, LX/17b;->e:LX/0Ot;

    .line 199039
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199040
    iput-object v0, p0, LX/17b;->f:LX/0Ot;

    .line 199041
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199042
    iput-object v0, p0, LX/17b;->g:LX/0Ot;

    .line 199043
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199044
    iput-object v0, p0, LX/17b;->h:LX/0Ot;

    .line 199045
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199046
    iput-object v0, p0, LX/17b;->i:LX/0Ot;

    .line 199047
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199048
    iput-object v0, p0, LX/17b;->j:LX/0Ot;

    .line 199049
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199050
    iput-object v0, p0, LX/17b;->k:LX/0Ot;

    .line 199051
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199052
    iput-object v0, p0, LX/17b;->l:LX/0Ot;

    .line 199053
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199054
    iput-object v0, p0, LX/17b;->m:LX/0Ot;

    .line 199055
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199056
    iput-object v0, p0, LX/17b;->n:LX/0Ot;

    .line 199057
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199058
    iput-object v0, p0, LX/17b;->o:LX/0Ot;

    .line 199059
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199060
    iput-object v0, p0, LX/17b;->p:LX/0Ot;

    .line 199061
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199062
    iput-object v0, p0, LX/17b;->q:LX/0Ot;

    .line 199063
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199064
    iput-object v0, p0, LX/17b;->r:LX/0Ot;

    .line 199065
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199066
    iput-object v0, p0, LX/17b;->s:LX/0Ot;

    .line 199067
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 199068
    iput-object v0, p0, LX/17b;->t:LX/0Ot;

    .line 199069
    return-void
.end method

.method public static a(LX/0QB;)LX/17b;
    .locals 3

    .prologue
    .line 199018
    sget-object v0, LX/17b;->u:LX/17b;

    if-nez v0, :cond_1

    .line 199019
    const-class v1, LX/17b;

    monitor-enter v1

    .line 199020
    :try_start_0
    sget-object v0, LX/17b;->u:LX/17b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 199021
    if-eqz v2, :cond_0

    .line 199022
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/17b;->b(LX/0QB;)LX/17b;

    move-result-object v0

    sput-object v0, LX/17b;->u:LX/17b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199023
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 199024
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 199025
    :cond_1
    sget-object v0, LX/17b;->u:LX/17b;

    return-object v0

    .line 199026
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 199027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/17b;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17b;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HmW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Hn1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JaN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6sK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FRX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/J1p;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EBM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fof;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FkO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8p6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/G61;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/G6P;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199017
    iput-object p1, p0, LX/17b;->a:LX/0Ot;

    iput-object p2, p0, LX/17b;->b:LX/0Ot;

    iput-object p3, p0, LX/17b;->c:LX/0Ot;

    iput-object p4, p0, LX/17b;->d:LX/0Ot;

    iput-object p5, p0, LX/17b;->e:LX/0Ot;

    iput-object p6, p0, LX/17b;->f:LX/0Ot;

    iput-object p7, p0, LX/17b;->g:LX/0Ot;

    iput-object p8, p0, LX/17b;->h:LX/0Ot;

    iput-object p9, p0, LX/17b;->i:LX/0Ot;

    iput-object p10, p0, LX/17b;->j:LX/0Ot;

    iput-object p11, p0, LX/17b;->k:LX/0Ot;

    iput-object p12, p0, LX/17b;->l:LX/0Ot;

    iput-object p13, p0, LX/17b;->m:LX/0Ot;

    iput-object p14, p0, LX/17b;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/17b;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/17b;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/17b;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/17b;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/17b;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/17b;->t:LX/0Ot;

    return-void
.end method

.method private static b(LX/0QB;)LX/17b;
    .locals 23

    .prologue
    .line 199014
    new-instance v2, LX/17b;

    invoke-direct {v2}, LX/17b;-><init>()V

    .line 199015
    const/16 v3, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x17f8

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x17ff

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x35fe

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2334

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x35fe

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x35fe

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2cc6

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2dc5

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2db3

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x35fe

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x31ce

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x355c

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x353d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x35fe

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x35fe

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x35fe

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x35fe

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x3890

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x3896

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v2 .. v22}, LX/17b;->a(LX/17b;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 199016
    return-object v2
.end method


# virtual methods
.method public final a(I)LX/48b;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 199003
    if-ltz p1, :cond_0

    invoke-static {}, LX/0cQ;->values()[LX/0cQ;

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 199004
    :cond_0
    iget-object v0, p0, LX/17b;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "ComponentHelperFactoryImpl"

    const-string v3, "No fragment type for fragmentId = %d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 199005
    :goto_0
    return-object v0

    .line 199006
    :cond_1
    sparse-switch p1, :sswitch_data_0

    move-object v0, v1

    .line 199007
    goto :goto_0

    .line 199008
    :sswitch_0
    iget-object v0, p0, LX/17b;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    .line 199009
    :sswitch_1
    iget-object v0, p0, LX/17b;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    .line 199010
    :sswitch_2
    iget-object v0, p0, LX/17b;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    .line 199011
    :sswitch_3
    iget-object v0, p0, LX/17b;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    .line 199012
    :sswitch_4
    iget-object v0, p0, LX/17b;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    .line 199013
    :sswitch_5
    iget-object v0, p0, LX/17b;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_4
        0x2a -> :sswitch_3
        0x4b -> :sswitch_1
        0x77 -> :sswitch_5
        0x117 -> :sswitch_2
        0x13f -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;)LX/48b;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 198987
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 198988
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 198989
    :sswitch_0
    const-string v1, "com.facebook.beam.receiver.BeamReceiverActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "com.facebook.beam.sender.activity.BeamSenderActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "com.facebook.composer.lifeevent.type.ComposerLifeEventTypeActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "com.facebook.heisman.ProfilePictureOverlayCameraActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "com.facebook.heisman.ProfilePictureOverlayPivotActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "com.facebook.payments.checkout.CheckoutActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "com.facebook.payments.picker.PickerScreenActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "com.facebook.payments.receipt.PaymentsReceiptActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "com.facebook.platform.PlatformCanonicalProfileIdActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "com.facebook.socialgood.triggers.FundraiserCuratedCharityPickerActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "com.facebook.timeline.refresher.ProfileRefresherProfileStepActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v1, "com.facebook.wem.sahayta.SahaytaChecklistActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    .line 198990
    :pswitch_0
    iget-object v0, p0, LX/17b;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198991
    :pswitch_1
    iget-object v0, p0, LX/17b;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198992
    :pswitch_2
    iget-object v0, p0, LX/17b;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198993
    :pswitch_3
    iget-object v0, p0, LX/17b;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198994
    :pswitch_4
    iget-object v0, p0, LX/17b;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198995
    :pswitch_5
    iget-object v0, p0, LX/17b;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198996
    :pswitch_6
    iget-object v0, p0, LX/17b;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198997
    :pswitch_7
    iget-object v0, p0, LX/17b;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198998
    :pswitch_8
    iget-object v0, p0, LX/17b;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 198999
    :pswitch_9
    iget-object v0, p0, LX/17b;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 199000
    :pswitch_a
    iget-object v0, p0, LX/17b;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 199001
    :pswitch_b
    iget-object v0, p0, LX/17b;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    .line 199002
    :pswitch_c
    iget-object v0, p0, LX/17b;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/48b;

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x78de5ca3 -> :sswitch_1
        -0x660ececa -> :sswitch_2
        -0x345a0c18 -> :sswitch_0
        -0x27fe7568 -> :sswitch_9
        -0xda7317d -> :sswitch_6
        0x12eaccde -> :sswitch_7
        0x1e75d12d -> :sswitch_a
        0x2adb98ff -> :sswitch_c
        0x38233d59 -> :sswitch_3
        0x442499ef -> :sswitch_b
        0x6f84a5f7 -> :sswitch_5
        0x7a9314cc -> :sswitch_4
        0x7e6e12b2 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
