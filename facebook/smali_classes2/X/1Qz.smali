.class public LX/1Qz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1R0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "LX/1R0",
        "<TP;TE;>;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;

.field private final b:LX/1Dc;

.field private final c:LX/0Sh;

.field private final d:LX/1Db;

.field private final e:LX/1Qx;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;"
        }
    .end annotation
.end field

.field private final g:Z

.field private final h:LX/23K;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/0Yi;


# direct methods
.method public constructor <init>(LX/03V;LX/1Db;LX/1Dc;LX/0Sh;LX/1Qx;LX/0Ot;ZLX/23K;LX/0Yi;)V
    .locals 0
    .param p8    # LX/23K;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Db;",
            "LX/1Dc;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1Qx;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;Z",
            "LX/23K;",
            "LX/0Yi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245382
    iput-object p1, p0, LX/1Qz;->a:LX/03V;

    .line 245383
    iput-object p3, p0, LX/1Qz;->b:LX/1Dc;

    .line 245384
    iput-object p4, p0, LX/1Qz;->c:LX/0Sh;

    .line 245385
    iput-object p2, p0, LX/1Qz;->d:LX/1Db;

    .line 245386
    iput-object p5, p0, LX/1Qz;->e:LX/1Qx;

    .line 245387
    iput-object p6, p0, LX/1Qz;->f:LX/0Ot;

    .line 245388
    iput-boolean p7, p0, LX/1Qz;->g:Z

    .line 245389
    iput-object p8, p0, LX/1Qz;->h:LX/23K;

    .line 245390
    iput-object p9, p0, LX/1Qz;->i:LX/0Yi;

    .line 245391
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1PW;)LX/1RA;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TE;)",
            "LX/1RA",
            "<TP;TE;>;"
        }
    .end annotation

    .prologue
    .line 245380
    new-instance v0, LX/1RA;

    iget-object v1, p0, LX/1Qz;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v4, p0, LX/1Qz;->a:LX/03V;

    iget-object v5, p0, LX/1Qz;->b:LX/1Dc;

    iget-object v6, p0, LX/1Qz;->c:LX/0Sh;

    iget-object v7, p0, LX/1Qz;->d:LX/1Db;

    iget-object v8, p0, LX/1Qz;->e:LX/1Qx;

    iget-boolean v9, p0, LX/1Qz;->g:Z

    iget-object v10, p0, LX/1Qz;->h:LX/23K;

    iget-object v11, p0, LX/1Qz;->i:LX/0Yi;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v11}, LX/1RA;-><init>(Ljava/lang/Object;LX/1PW;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/03V;LX/1Dc;LX/0Sh;LX/1Db;LX/1Qx;ZLX/23K;LX/0Yi;)V

    return-object v0
.end method
