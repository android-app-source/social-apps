.class public LX/1gu;
.super LX/0jZ;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/api/feedtype/FeedType;

.field public final c:LX/1gi;

.field private final d:LX/0jY;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1jV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 295022
    const-class v0, LX/1gu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1gu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/facebook/api/feedtype/FeedType;LX/1gi;LX/0jY;)V
    .locals 3
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1gi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 295023
    new-array v0, v2, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, LX/0jZ;-><init>(Landroid/os/Looper;[I)V

    .line 295024
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295025
    iput-object v0, p0, LX/1gu;->e:LX/0Ot;

    .line 295026
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295027
    iput-object v0, p0, LX/1gu;->f:LX/0Ot;

    .line 295028
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295029
    iput-object v0, p0, LX/1gu;->g:LX/0Ot;

    .line 295030
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 295031
    iput-object v0, p0, LX/1gu;->h:LX/0Ot;

    .line 295032
    iput-object p2, p0, LX/1gu;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 295033
    iput-object p3, p0, LX/1gu;->c:LX/1gi;

    .line 295034
    iput-object p4, p0, LX/1gu;->d:LX/0jY;

    .line 295035
    return-void
.end method

.method private b(I)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x5

    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 295036
    if-eq p1, v6, :cond_0

    if-eq p1, v5, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not a valid result type for dbFetch"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 295037
    iget-object v0, p0, LX/1gu;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/0fe;->ao:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 295038
    if-eqz v0, :cond_5

    .line 295039
    invoke-static {p0, v1}, LX/1gu;->c(LX/1gu;I)LX/0Px;

    move-result-object v1

    .line 295040
    iget-object v0, p0, LX/1gu;->d:LX/0jY;

    invoke-virtual {v0, v1, p1}, LX/0jY;->b(LX/0Px;I)V

    .line 295041
    invoke-static {p0, v2}, LX/1gu;->c(LX/1gu;I)LX/0Px;

    move-result-object v2

    .line 295042
    iget-object v0, p0, LX/1gu;->d:LX/0jY;

    invoke-virtual {v0, v2, p1}, LX/0jY;->b(LX/0Px;I)V

    .line 295043
    if-eq p1, v5, :cond_1

    if-ne p1, v6, :cond_2

    .line 295044
    :cond_1
    iget-object v0, p0, LX/1gu;->c:LX/1gi;

    invoke-virtual {v0}, LX/1gi;->b()V

    .line 295045
    :cond_2
    if-ne p1, v5, :cond_3

    .line 295046
    iget-object v0, p0, LX/1gu;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jV;

    .line 295047
    iget-object v3, v0, LX/1jV;->a:LX/0Zb;

    const-string v4, "android_feed_content_availability"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 295048
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 295049
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 295050
    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 295051
    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 295052
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/1jV;->a(LX/1jV;LX/0oG;LX/0Px;)V

    .line 295053
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v1

    .line 295054
    goto :goto_0

    .line 295055
    :cond_5
    iget-object v0, p0, LX/1gu;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0fe;->aG:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 295056
    if-eqz v0, :cond_9

    .line 295057
    invoke-static {p0, v1}, LX/1gu;->c(LX/1gu;I)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 295058
    :goto_2
    iget-object v0, p0, LX/1gu;->d:LX/0jY;

    invoke-virtual {v0, v1, p1}, LX/0jY;->b(LX/0Px;I)V

    .line 295059
    if-eq p1, v5, :cond_6

    if-ne p1, v6, :cond_7

    .line 295060
    :cond_6
    iget-object v0, p0, LX/1gu;->c:LX/1gi;

    invoke-virtual {v0}, LX/1gi;->b()V

    .line 295061
    :cond_7
    if-ne p1, v5, :cond_3

    .line 295062
    iget-object v0, p0, LX/1gu;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1jV;

    .line 295063
    iget-object v2, v0, LX/1jV;->a:LX/0Zb;

    const-string v3, "android_feed_content_availability"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 295064
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 295065
    invoke-static {v0, v2, v1}, LX/1jV;->a(LX/1jV;LX/0oG;LX/0Px;)V

    .line 295066
    :cond_8
    goto :goto_1

    .line 295067
    :cond_9
    iget-object v7, p0, LX/1gu;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0pn;

    iget-object v8, p0, LX/1gu;->b:Lcom/facebook/api/feedtype/FeedType;

    const-wide/32 v9, 0xf731400

    invoke-virtual {v7, v8, v9, v10}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v9

    .line 295068
    iget-object v7, p0, LX/1gu;->f:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0pV;

    sget-object v10, LX/1gu;->a:Ljava/lang/String;

    sget-object v11, LX/1gs;->STORIES_READ_FROM_DB:LX/1gs;

    const-string v12, "feedUnitEdges"

    if-nez v9, :cond_a

    const-string v8, "0"

    :goto_3
    invoke-virtual {v7, v10, v11, v12, v8}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 295069
    move-object v0, v9

    .line 295070
    move-object v1, v0

    goto :goto_2

    .line 295071
    :cond_a
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_3
.end method

.method private static c(LX/1gu;I)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295072
    iget-object v0, p0, LX/1gu;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pn;

    iget-object v1, p0, LX/1gu;->b:Lcom/facebook/api/feedtype/FeedType;

    const-wide/32 v2, 0xf731400

    .line 295073
    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 295074
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 295075
    sget-object v6, LX/0pp;->a:LX/0U1;

    .line 295076
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 295077
    invoke-virtual {v1}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 295078
    sget-object v6, LX/0pp;->i:LX/0U1;

    .line 295079
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 295080
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 295081
    sget-object v6, LX/0pp;->b:LX/0U1;

    .line 295082
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 295083
    iget-object v7, v0, LX/0pn;->i:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    sub-long/2addr v7, v2

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0uu;->f(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 295084
    sget-object v6, LX/0pp;->e:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v6

    .line 295085
    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v7, v5, v6}, LX/0pn;->a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    move-object v2, v5

    .line 295086
    iget-object v0, p0, LX/1gu;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0pV;

    sget-object v3, LX/1gu;->a:Ljava/lang/String;

    const-string v4, "readAllStoriesBySeenState, feedUnitEdges:"

    if-nez v2, :cond_0

    const-string v1, "0"

    :goto_1
    invoke-virtual {v0, v3, v4, v1}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295087
    return-object v2

    .line 295088
    :cond_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 295089
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 295090
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 295091
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295092
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, LX/1gu;->b(I)V

    .line 295093
    :goto_0
    return-void

    .line 295094
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1J0;

    .line 295095
    iget-object v1, p0, LX/1gu;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0pn;

    iget-object v2, v0, LX/1J0;->b:Lcom/facebook/api/feedtype/FeedType;

    iget-object v3, v0, LX/1J0;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0pn;->b(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 295096
    iget-object v1, p0, LX/1gu;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0pV;

    sget-object v4, LX/1gu;->a:Ljava/lang/String;

    sget-object v5, LX/1gs;->STORIES_READ_FROM_DB:LX/1gs;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "loading before: "

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/1J0;->g:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p1, ", feedUnitEdges"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    if-nez v3, :cond_0

    const-string v2, "0"

    :goto_1
    invoke-virtual {v1, v4, v5, p1, v2}, LX/0pV;->a(Ljava/lang/String;LX/1gt;Ljava/lang/String;Ljava/lang/String;)V

    .line 295097
    iget-object v1, p0, LX/1gu;->c:LX/1gi;

    const/16 v2, 0xe

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, LX/1gi;->a(LX/0Px;II)V

    .line 295098
    iget-object v1, p0, LX/1gu;->c:LX/1gi;

    invoke-virtual {v1}, LX/1gi;->b()V

    .line 295099
    goto :goto_0

    .line 295100
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
