.class public LX/1Ba;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 213335
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 213336
    return-void
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;)LX/31I;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/7zX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3E6;",
            ">;)",
            "LX/31I;"
        }
    .end annotation

    .prologue
    .line 213337
    sget-object v0, LX/0pP;->j:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 213338
    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31I;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/31I;

    goto :goto_0
.end method

.method public static b(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;)LX/1Bc;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/7zY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Bb;",
            ">;)",
            "LX/1Bc;"
        }
    .end annotation

    .prologue
    .line 213339
    sget-object v0, LX/0eJ;->i:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 213340
    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bc;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bc;

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 213341
    return-void
.end method
