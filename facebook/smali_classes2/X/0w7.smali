.class public LX/0w7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;
.end annotation


# static fields
.field public static final a:LX/0w7;


# instance fields
.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4a1;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public mParams:LX/0w8;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "params"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 159301
    const-class v0, Lcom/facebook/graphql/query/GraphQlQueryParamSetDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159300
    new-instance v0, LX/0w7;

    invoke-direct {v0}, LX/0w7;-><init>()V

    sput-object v0, LX/0w7;->a:LX/0w7;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 159285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159286
    new-instance v0, LX/0w8;

    invoke-direct {v0}, LX/0w8;-><init>()V

    iput-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159287
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/0w7;->b:Ljava/util/Map;

    .line 159288
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/0w7;->c:Ljava/util/Map;

    .line 159289
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159302
    invoke-direct {p0}, LX/0w7;-><init>()V

    .line 159303
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159304
    invoke-virtual {v0}, LX/0gS;->a()LX/0n9;

    move-result-object p0

    invoke-virtual {v0, p0, p1}, LX/0gS;->a(LX/0n9;Ljava/util/Map;)V

    .line 159305
    return-void
.end method


# virtual methods
.method public final a()LX/0Zh;
    .locals 1

    .prologue
    .line 159306
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159307
    iget-object p0, v0, LX/0gS;->a:LX/0Zh;

    move-object v0, p0

    .line 159308
    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/0w7;
    .locals 4

    .prologue
    .line 159309
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159310
    invoke-virtual {v0}, LX/0gS;->a()LX/0n9;

    move-result-object v1

    .line 159311
    const/4 v2, 0x0

    :goto_0
    iget v3, v1, LX/0n9;->c:I

    if-ge v2, v3, :cond_0

    .line 159312
    invoke-virtual {v1, v2}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 159313
    iget-object v3, v1, LX/0n9;->b:Ljava/util/ArrayList;

    mul-int/lit8 v0, v2, 0x2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 159314
    iget-object v3, v1, LX/0n9;->b:Ljava/util/ArrayList;

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 159315
    iget v2, v1, LX/0n9;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, LX/0n9;->c:I

    .line 159316
    :cond_0
    return-object p0

    .line 159317
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/0gS;)LX/0w7;
    .locals 1

    .prologue
    .line 159318
    if-eqz p2, :cond_0

    .line 159319
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159320
    invoke-virtual {v0, p1, p2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 159321
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/4a1;)LX/0w7;
    .locals 1

    .prologue
    .line 159322
    if-eqz p2, :cond_0

    .line 159323
    iget-object v0, p0, LX/0w7;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159324
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0w7;
    .locals 1

    .prologue
    .line 159325
    if-eqz p2, :cond_0

    .line 159326
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    invoke-virtual {v0, p1, p2}, LX/0w8;->b(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 159327
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Enum;)LX/0w7;
    .locals 3

    .prologue
    .line 159290
    if-eqz p2, :cond_0

    .line 159291
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159292
    invoke-virtual {v0}, LX/0gS;->a()LX/0n9;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    .line 159293
    invoke-static {v1, p1, v2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159294
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;
    .locals 2

    .prologue
    .line 159295
    if-eqz p2, :cond_0

    .line 159296
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159297
    invoke-virtual {v0}, LX/0gS;->a()LX/0n9;

    move-result-object v1

    .line 159298
    invoke-static {v1, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 159299
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 159257
    if-eqz p2, :cond_0

    .line 159258
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    invoke-virtual {v0, p1, p2}, LX/0w8;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159259
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/0w7;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 159260
    if-eqz p2, :cond_0

    .line 159261
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159262
    invoke-virtual {v0, p1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159263
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)LX/0w7;
    .locals 1
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<*>;)",
            "LX/0w7;"
        }
    .end annotation

    .prologue
    .line 159264
    if-eqz p2, :cond_0

    .line 159265
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159266
    invoke-virtual {v0, p1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 159267
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;
    .locals 1

    .prologue
    .line 159268
    if-eqz p2, :cond_0

    .line 159269
    iget-object v0, p0, LX/0w7;->c:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159270
    :cond_0
    return-object p0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4a1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159271
    iget-object v0, p0, LX/0w7;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159272
    iget-object v0, p0, LX/0w7;->c:Ljava/util/Map;

    return-object v0
.end method

.method public final d()LX/0n9;
    .locals 1

    .prologue
    .line 159273
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    .line 159274
    iget-object p0, v0, LX/0gS;->b:LX/0n9;

    move-object v0, p0

    .line 159275
    return-object v0
.end method

.method public final e()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159276
    iget-object v0, p0, LX/0w7;->mParams:LX/0w8;

    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 159277
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 159278
    const-string v0, "params"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 159279
    invoke-virtual {p0}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 159280
    const-string v0, "input_name"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 159281
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 159282
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 159283
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 1

    .prologue
    .line 159284
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
