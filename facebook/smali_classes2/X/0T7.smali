.class public final enum LX/0T7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0T7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0T7;

.field public static final enum APPLICATION_INITIALIZING:LX/0T7;

.field public static final enum APPLICATION_LOADED:LX/0T7;

.field public static final enum APPLICATION_LOADING:LX/0T7;

.field public static final enum START:LX/0T7;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62553
    new-instance v0, LX/0T7;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/0T7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0T7;->START:LX/0T7;

    .line 62554
    new-instance v0, LX/0T7;

    const-string v1, "APPLICATION_INITIALIZING"

    invoke-direct {v0, v1, v3}, LX/0T7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0T7;->APPLICATION_INITIALIZING:LX/0T7;

    .line 62555
    new-instance v0, LX/0T7;

    const-string v1, "APPLICATION_LOADING"

    invoke-direct {v0, v1, v4}, LX/0T7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0T7;->APPLICATION_LOADING:LX/0T7;

    .line 62556
    new-instance v0, LX/0T7;

    const-string v1, "APPLICATION_LOADED"

    invoke-direct {v0, v1, v5}, LX/0T7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    .line 62557
    const/4 v0, 0x4

    new-array v0, v0, [LX/0T7;

    sget-object v1, LX/0T7;->START:LX/0T7;

    aput-object v1, v0, v2

    sget-object v1, LX/0T7;->APPLICATION_INITIALIZING:LX/0T7;

    aput-object v1, v0, v3

    sget-object v1, LX/0T7;->APPLICATION_LOADING:LX/0T7;

    aput-object v1, v0, v4

    sget-object v1, LX/0T7;->APPLICATION_LOADED:LX/0T7;

    aput-object v1, v0, v5

    sput-object v0, LX/0T7;->$VALUES:[LX/0T7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0T7;
    .locals 1

    .prologue
    .line 62559
    const-class v0, LX/0T7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0T7;

    return-object v0
.end method

.method public static values()[LX/0T7;
    .locals 1

    .prologue
    .line 62560
    sget-object v0, LX/0T7;->$VALUES:[LX/0T7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0T7;

    return-object v0
.end method
