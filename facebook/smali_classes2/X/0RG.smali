.class public LX/0RG;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# instance fields
.field private final a:LX/0RF;

.field private final b:LX/0RB;

.field private final c:Landroid/content/Context;

.field public d:LX/0QA;


# direct methods
.method public constructor <init>(LX/0RF;LX/0RB;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 59748
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 59749
    iput-object p1, p0, LX/0RG;->a:LX/0RF;

    .line 59750
    iput-object p2, p0, LX/0RG;->b:LX/0RB;

    .line 59751
    iput-object p3, p0, LX/0RG;->c:Landroid/content/Context;

    .line 59752
    return-void
.end method


# virtual methods
.method public final a(LX/0QA;)V
    .locals 2

    .prologue
    .line 59753
    iput-object p1, p0, LX/0RG;->d:LX/0QA;

    .line 59754
    invoke-virtual {p1}, LX/0QA;->getInjectorThreadStack()LX/0S7;

    move-result-object v0

    iget-object v1, p0, LX/0RG;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/0S7;->a(Landroid/content/Context;)V

    .line 59755
    return-void
.end method

.method public final configure()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NontrivialConfigureMethod"
        }
    .end annotation

    .prologue
    .line 59756
    const-class v0, Ljavax/inject/Singleton;

    iget-object v1, p0, LX/0RG;->a:LX/0RF;

    invoke-virtual {p0, v0, v1}, LX/0Q3;->bindScope(Ljava/lang/Class;LX/0RC;)V

    .line 59757
    const-class v0, LX/0RF;

    invoke-virtual {p0, v0}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    iget-object v1, p0, LX/0RG;->a:LX/0RF;

    invoke-interface {v0, v1}, LX/0RS;->a(Ljava/lang/Object;)V

    .line 59758
    const-class v0, Lcom/facebook/inject/ContextScoped;

    iget-object v1, p0, LX/0RG;->b:LX/0RB;

    invoke-virtual {p0, v0, v1}, LX/0Q3;->bindScope(Ljava/lang/Class;LX/0RC;)V

    .line 59759
    const-class v0, LX/0RB;

    invoke-virtual {p0, v0}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    iget-object v1, p0, LX/0RG;->b:LX/0RB;

    invoke-interface {v0, v1}, LX/0RS;->a(Ljava/lang/Object;)V

    .line 59760
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    new-instance v1, LX/0RU;

    invoke-direct {v1, p0}, LX/0RU;-><init>(LX/0RG;)V

    invoke-interface {v0, v1}, LX/0RS;->a(LX/0Or;)LX/0RR;

    .line 59761
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-virtual {v0, v1}, LX/0RO;->a(Ljava/lang/Class;)LX/0RS;

    move-result-object v0

    new-instance v1, LX/0RX;

    invoke-direct {v1, p0}, LX/0RX;-><init>(LX/0RG;)V

    invoke-interface {v0, v1}, LX/0RS;->a(LX/0Or;)LX/0RR;

    .line 59762
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LX/0Q3;->bind(Ljava/lang/Class;)LX/0RO;

    move-result-object v0

    const-class v1, Lcom/facebook/inject/ForWindowedContext;

    invoke-virtual {v0, v1}, LX/0RO;->a(Ljava/lang/Class;)LX/0RS;

    move-result-object v0

    new-instance v1, LX/0RY;

    invoke-direct {v1, p0}, LX/0RY;-><init>(LX/0RG;)V

    invoke-interface {v0, v1}, LX/0RS;->a(LX/0Or;)LX/0RR;

    .line 59763
    return-void
.end method
