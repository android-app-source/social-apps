.class public LX/1Lg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/1Li;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/03V;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Lr;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "LX/1Li;",
            "LX/1M6;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "LX/1Li;",
            "LX/1M6;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/video/server/prefetcher/VideoPrefetchModel$Callback;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCallbacks"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234058
    const-class v0, LX/1Lg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1Lg;->a:Ljava/lang/String;

    .line 234059
    new-instance v0, LX/1Lh;

    invoke-direct {v0}, LX/1Lh;-><init>()V

    sput-object v0, LX/1Lg;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/1Lv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Lr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234111
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, LX/1Lg;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    .line 234112
    new-instance v0, Ljava/util/TreeMap;

    sget-object v1, LX/1Lg;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/1Lg;->g:Ljava/util/TreeMap;

    .line 234113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Lg;->h:Ljava/util/Set;

    .line 234114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1Lg;->i:Ljava/util/Set;

    .line 234115
    iput-object p1, p0, LX/1Lg;->c:LX/03V;

    .line 234116
    iput-object p2, p0, LX/1Lg;->d:LX/0Ot;

    .line 234117
    iput-object p3, p0, LX/1Lg;->e:LX/0Ot;

    .line 234118
    return-void
.end method

.method private a(Ljava/util/Map;LX/1Li;)LX/1M6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/1Li;",
            "LX/1M6;",
            ">;",
            "LX/1Li;",
            ")",
            "LX/1M6;"
        }
    .end annotation

    .prologue
    .line 234119
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234120
    if-nez v0, :cond_0

    .line 234121
    new-instance v0, LX/1M6;

    new-instance v1, LX/1M8;

    invoke-direct {v1, p0}, LX/1M8;-><init>(LX/1Lg;)V

    iget-object v2, p0, LX/1Lg;->c:LX/03V;

    invoke-direct {v0, v1, v2, p2}, LX/1M6;-><init>(LX/1M8;LX/03V;LX/1Li;)V

    .line 234122
    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234123
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/Map;Z)LX/374;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/1Li;",
            "LX/1M6;",
            ">;Z)",
            "LX/374;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 234129
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234130
    invoke-virtual {v0}, LX/1M6;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234131
    invoke-virtual {v0}, LX/1M6;->d()LX/36s;

    move-result-object v2

    .line 234132
    new-instance v1, LX/374;

    .line 234133
    iget-object p0, v0, LX/1M6;->e:LX/1Li;

    move-object v0, p0

    .line 234134
    invoke-direct {v1, v2, v0, p1}, LX/374;-><init>(LX/36s;LX/1Li;Z)V

    move-object v0, v1

    .line 234135
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1Lg;)V
    .locals 5

    .prologue
    .line 234124
    iget-object v1, p0, LX/1Lg;->i:Ljava/util/Set;

    monitor-enter v1

    .line 234125
    :try_start_0
    iget-object v0, p0, LX/1Lg;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lx;

    .line 234126
    iget-object v3, v0, LX/1Lx;->a:LX/1Lv;

    iget-object v3, v3, LX/1Lv;->m:Landroid/os/Handler;

    iget-object v4, v0, LX/1Lx;->a:LX/1Lv;

    iget-object v4, v4, LX/1Lv;->I:Ljava/lang/Runnable;

    const p0, -0x54ebfb54

    invoke-static {v3, v4, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 234127
    goto :goto_0

    .line 234128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1Li;)LX/1M7;
    .locals 3

    .prologue
    .line 234099
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 234100
    iget-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    invoke-direct {p0, v0, p1}, LX/1Lg;->a(Ljava/util/Map;LX/1Li;)LX/1M6;

    move-result-object v1

    .line 234101
    iget-object v0, p0, LX/1Lg;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lr;

    invoke-virtual {v0}, LX/1Lr;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 234102
    :goto_0
    monitor-exit p0

    return-object v0

    .line 234103
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1Lg;->g:Ljava/util/TreeMap;

    invoke-direct {p0, v0, p1}, LX/1Lg;->a(Ljava/util/Map;LX/1Li;)LX/1M6;

    move-result-object v2

    .line 234104
    new-instance v0, LX/7Q4;

    invoke-direct {v0, v1, v2}, LX/7Q4;-><init>(LX/1M6;LX/1M6;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 234105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/1Lx;)V
    .locals 2

    .prologue
    .line 234106
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/1Lg;->i:Ljava/util/Set;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 234107
    :try_start_1
    iget-object v0, p0, LX/1Lg;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 234108
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 234109
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/374;Z)V
    .locals 3

    .prologue
    .line 234085
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->h:Ljava/util/Set;

    .line 234086
    iget-object v1, p1, LX/374;->b:LX/36s;

    move-object v1, v1

    .line 234087
    iget-object v2, v1, LX/36s;->d:Landroid/net/Uri;

    move-object v1, v2

    .line 234088
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 234089
    if-nez p2, :cond_0

    if-eqz v0, :cond_0

    .line 234090
    iget-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    .line 234091
    iget-object v1, p1, LX/374;->c:LX/1Li;

    move-object v1, v1

    .line 234092
    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M7;

    .line 234093
    if-eqz v0, :cond_0

    .line 234094
    const/4 v1, 0x1

    new-array v1, v1, [LX/36s;

    const/4 v2, 0x0

    .line 234095
    iget-object p2, p1, LX/374;->b:LX/36s;

    move-object p2, p2

    .line 234096
    aput-object p2, v1, v2

    invoke-interface {v0, v1}, LX/1M7;->a([LX/36s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234097
    :cond_0
    monitor-exit p0

    return-void

    .line 234098
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 234079
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234080
    invoke-virtual {v0, p1}, LX/1M6;->a(Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234081
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234082
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1Lg;->g:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234083
    invoke-virtual {v0, p1}, LX/1M6;->a(Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 234084
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 234070
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234071
    invoke-virtual {v0}, LX/1M6;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 234072
    :goto_0
    monitor-exit p0

    return v0

    .line 234073
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1Lg;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lr;

    invoke-virtual {v0}, LX/1Lr;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234074
    iget-object v0, p0, LX/1Lg;->g:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1M6;

    .line 234075
    invoke-virtual {v0}, LX/1M6;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 234076
    goto :goto_0

    .line 234077
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 234078
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/374;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 234061
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->f:Ljava/util/TreeMap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1Lg;->a(Ljava/util/Map;Z)LX/374;

    move-result-object v1

    .line 234062
    if-nez v1, :cond_1

    iget-object v0, p0, LX/1Lg;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Lr;

    invoke-virtual {v0}, LX/1Lr;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234063
    iget-object v0, p0, LX/1Lg;->g:Ljava/util/TreeMap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/1Lg;->a(Ljava/util/Map;Z)LX/374;

    move-result-object v0

    .line 234064
    :goto_0
    if-eqz v0, :cond_0

    .line 234065
    iget-object v1, v0, LX/374;->b:LX/36s;

    move-object v1, v1

    .line 234066
    iget-object v2, v1, LX/36s;->d:Landroid/net/Uri;

    move-object v1, v2

    .line 234067
    iget-object v2, p0, LX/1Lg;->h:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234068
    :cond_0
    monitor-exit p0

    return-object v0

    .line 234069
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized b(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 234060
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1Lg;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
