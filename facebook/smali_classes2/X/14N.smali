.class public LX/14N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final A:Z

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0n9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Z

.field private final j:Z

.field public final k:LX/14S;

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/lang/Object;

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field public final q:Z

.field private final r:Z

.field private final s:Z

.field public t:Z

.field public final u:Z

.field public final v:LX/14R;

.field public final w:LX/14Q;

.field public final x:LX/14P;

.field private final y:LX/0zW;

.field public final z:LX/4cx;


# direct methods
.method public constructor <init>(LX/14O;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 178677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178678
    iget-object v0, p1, LX/14O;->b:Ljava/lang/String;

    move-object v0, v0

    .line 178679
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178680
    iget-object v0, p1, LX/14O;->c:Ljava/lang/String;

    move-object v0, v0

    .line 178681
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178682
    iget-object v0, p1, LX/14O;->d:Ljava/lang/String;

    move-object v0, v0

    .line 178683
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178684
    iget-object v0, p1, LX/14O;->g:Ljava/util/List;

    move-object v0, v0

    .line 178685
    if-nez v0, :cond_0

    .line 178686
    iget-object v0, p1, LX/14O;->h:LX/0n9;

    move-object v0, v0

    .line 178687
    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v4, "Either setParameters or setPoolableParameters must be used"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 178688
    iget-object v0, p1, LX/14O;->g:Ljava/util/List;

    move-object v0, v0

    .line 178689
    if-eqz v0, :cond_1

    .line 178690
    iget-object v0, p1, LX/14O;->h:LX/0n9;

    move-object v0, v0

    .line 178691
    if-nez v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Conflict detected: both setParameters and setPoolableParameters used"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 178692
    iget-object v0, p1, LX/14O;->b:Ljava/lang/String;

    move-object v0, v0

    .line 178693
    iput-object v0, p0, LX/14N;->a:Ljava/lang/String;

    .line 178694
    iget-object v0, p1, LX/14O;->c:Ljava/lang/String;

    move-object v0, v0

    .line 178695
    iput-object v0, p0, LX/14N;->b:Ljava/lang/String;

    .line 178696
    iget-object v0, p1, LX/14O;->d:Ljava/lang/String;

    move-object v0, v0

    .line 178697
    iput-object v0, p0, LX/14N;->c:Ljava/lang/String;

    .line 178698
    iget-object v0, p1, LX/14O;->e:Ljava/lang/String;

    move-object v0, v0

    .line 178699
    iput-object v0, p0, LX/14N;->d:Ljava/lang/String;

    .line 178700
    iget-object v0, p1, LX/14O;->f:LX/0Px;

    move-object v0, v0

    .line 178701
    iput-object v0, p0, LX/14N;->e:LX/0Px;

    .line 178702
    invoke-static {p1}, LX/14O;->D(LX/14O;)LX/0zW;

    move-result-object v0

    move-object v0, v0

    .line 178703
    iput-object v0, p0, LX/14N;->y:LX/0zW;

    .line 178704
    iget-object v0, p1, LX/14O;->g:Ljava/util/List;

    move-object v0, v0

    .line 178705
    if-eqz v0, :cond_4

    .line 178706
    iget-object v0, p1, LX/14O;->g:Ljava/util/List;

    move-object v0, v0

    .line 178707
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/14N;->g:LX/0Px;

    .line 178708
    iget-object v0, p1, LX/14O;->h:LX/0n9;

    move-object v0, v0

    .line 178709
    iput-object v0, p0, LX/14N;->h:LX/0n9;

    .line 178710
    iget-boolean v0, p1, LX/14O;->i:Z

    move v0, v0

    .line 178711
    iput-boolean v0, p0, LX/14N;->i:Z

    .line 178712
    iget-boolean v0, p1, LX/14O;->j:Z

    move v0, v0

    .line 178713
    iput-boolean v0, p0, LX/14N;->j:Z

    .line 178714
    iget-object v0, p1, LX/14O;->k:LX/14S;

    move-object v0, v0

    .line 178715
    iput-object v0, p0, LX/14N;->k:LX/14S;

    .line 178716
    iget-object v0, p1, LX/14O;->l:Ljava/util/List;

    move-object v0, v0

    .line 178717
    iput-object v0, p0, LX/14N;->l:Ljava/util/List;

    .line 178718
    iget-object v0, p1, LX/14O;->m:Ljava/lang/Object;

    move-object v0, v0

    .line 178719
    iput-object v0, p0, LX/14N;->m:Ljava/lang/Object;

    .line 178720
    iget-boolean v0, p1, LX/14O;->n:Z

    move v0, v0

    .line 178721
    iput-boolean v0, p0, LX/14N;->n:Z

    .line 178722
    iget-boolean v0, p1, LX/14O;->o:Z

    move v0, v0

    .line 178723
    iput-boolean v0, p0, LX/14N;->o:Z

    .line 178724
    iget-boolean v0, p1, LX/14O;->q:Z

    move v0, v0

    .line 178725
    iput-boolean v0, p0, LX/14N;->q:Z

    .line 178726
    iget-boolean v0, p1, LX/14O;->r:Z

    move v0, v0

    .line 178727
    iput-boolean v0, p0, LX/14N;->r:Z

    .line 178728
    iget-boolean v0, p1, LX/14O;->s:Z

    move v0, v0

    .line 178729
    iput-boolean v0, p0, LX/14N;->s:Z

    .line 178730
    iget-object v0, p1, LX/14O;->v:LX/14Q;

    move-object v0, v0

    .line 178731
    iput-object v0, p0, LX/14N;->w:LX/14Q;

    .line 178732
    iget-boolean v0, p1, LX/14O;->t:Z

    move v0, v0

    .line 178733
    iput-boolean v0, p0, LX/14N;->t:Z

    .line 178734
    iget-boolean v0, p1, LX/14O;->u:Z

    move v0, v0

    .line 178735
    iput-boolean v0, p0, LX/14N;->u:Z

    .line 178736
    iget-object v0, p1, LX/14O;->w:LX/14R;

    move-object v0, v0

    .line 178737
    iput-object v0, p0, LX/14N;->v:LX/14R;

    .line 178738
    iget-object v0, p1, LX/14O;->x:LX/14P;

    move-object v0, v0

    .line 178739
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14P;

    iput-object v0, p0, LX/14N;->x:LX/14P;

    .line 178740
    iget-object v0, p1, LX/14O;->C:LX/4cx;

    move-object v0, v0

    .line 178741
    iput-object v0, p0, LX/14N;->z:LX/4cx;

    .line 178742
    iget-boolean v0, p1, LX/14O;->p:Z

    move v0, v0

    .line 178743
    iput-boolean v0, p0, LX/14N;->p:Z

    .line 178744
    iget-boolean v0, p0, LX/14N;->p:Z

    if-eqz v0, :cond_5

    .line 178745
    iget-object v0, p1, LX/14O;->A:Ljava/lang/String;

    move-object v0, v0

    .line 178746
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/14N;->f:Ljava/lang/String;

    .line 178747
    :goto_2
    iget-boolean v0, p1, LX/14O;->B:Z

    move v0, v0

    .line 178748
    iput-boolean v0, p0, LX/14N;->A:Z

    .line 178749
    return-void

    :cond_3
    move v0, v1

    .line 178750
    goto/16 :goto_0

    :cond_4
    move-object v0, v3

    .line 178751
    goto :goto_1

    .line 178752
    :cond_5
    iput-object v3, p0, LX/14N;->f:Ljava/lang/String;

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V
    .locals 3
    .param p4    # Lcom/facebook/http/interfaces/RequestPriority;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "LX/14S;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 178753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178754
    iput-object p1, p0, LX/14N;->a:Ljava/lang/String;

    .line 178755
    iput-object p2, p0, LX/14N;->b:Ljava/lang/String;

    .line 178756
    iput-object p3, p0, LX/14N;->c:Ljava/lang/String;

    .line 178757
    iput-object v2, p0, LX/14N;->d:Ljava/lang/String;

    .line 178758
    iput-object v2, p0, LX/14N;->e:LX/0Px;

    .line 178759
    invoke-static {p1, p4}, LX/14T;->a(Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zW;

    move-result-object v0

    iput-object v0, p0, LX/14N;->y:LX/0zW;

    .line 178760
    invoke-static {p5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/14N;->g:LX/0Px;

    .line 178761
    iput-object v2, p0, LX/14N;->h:LX/0n9;

    .line 178762
    iput-boolean v1, p0, LX/14N;->i:Z

    .line 178763
    iput-boolean v1, p0, LX/14N;->j:Z

    .line 178764
    iput-object p6, p0, LX/14N;->k:LX/14S;

    .line 178765
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 178766
    iput-object v0, p0, LX/14N;->l:Ljava/util/List;

    .line 178767
    iput-object v2, p0, LX/14N;->m:Ljava/lang/Object;

    .line 178768
    iput-boolean v1, p0, LX/14N;->n:Z

    .line 178769
    iput-boolean v1, p0, LX/14N;->o:Z

    .line 178770
    iput-boolean v1, p0, LX/14N;->p:Z

    .line 178771
    iput-boolean v1, p0, LX/14N;->q:Z

    .line 178772
    iput-boolean v1, p0, LX/14N;->r:Z

    .line 178773
    iput-boolean v1, p0, LX/14N;->s:Z

    .line 178774
    sget-object v0, LX/14Q;->FALLBACK_NOT_REQUIRED:LX/14Q;

    iput-object v0, p0, LX/14N;->w:LX/14Q;

    .line 178775
    iput-boolean v1, p0, LX/14N;->t:Z

    .line 178776
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14N;->u:Z

    .line 178777
    sget-object v0, LX/14R;->AUTO:LX/14R;

    iput-object v0, p0, LX/14N;->v:LX/14R;

    .line 178778
    sget-object v0, LX/14O;->a:LX/14P;

    iput-object v0, p0, LX/14N;->x:LX/14P;

    .line 178779
    iput-object v2, p0, LX/14N;->z:LX/4cx;

    .line 178780
    iput-object v2, p0, LX/14N;->f:Ljava/lang/String;

    .line 178781
    iput-boolean v1, p0, LX/14N;->A:Z

    .line 178782
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "LX/14S;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 178783
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    .line 178784
    return-void
.end method

.method public static newBuilder()LX/14O;
    .locals 1

    .prologue
    .line 178785
    new-instance v0, LX/14O;

    invoke-direct {v0}, LX/14O;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 178786
    iget-boolean v0, p0, LX/14N;->A:Z

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178787
    iget-object v0, p0, LX/14N;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 178788
    iget-object v0, p0, LX/14N;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178672
    iget-object v0, p0, LX/14N;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178789
    iget-object v0, p0, LX/14N;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final g()LX/0zW;
    .locals 2

    .prologue
    .line 178790
    iget-object v0, p0, LX/14N;->y:LX/0zW;

    invoke-static {}, LX/15A;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zW;->b(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 178791
    iget-object v0, p0, LX/14N;->y:LX/0zW;

    return-object v0
.end method

.method public final h()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178792
    iget-object v0, p0, LX/14N;->g:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/14N;->g:LX/0Px;

    :goto_0
    return-object v0

    .line 178793
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 178794
    goto :goto_0
.end method

.method public final j()LX/0n9;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 178673
    iget-object v0, p0, LX/14N;->h:LX/0n9;

    if-nez v0, :cond_0

    .line 178674
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call hasPoolableParameters first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178675
    :cond_0
    iget-object v0, p0, LX/14N;->h:LX/0n9;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 178676
    iget-boolean v0, p0, LX/14N;->i:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 178664
    iget-boolean v0, p0, LX/14N;->j:Z

    return v0
.end method

.method public final m()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178658
    iget-object v0, p0, LX/14N;->l:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/14N;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178659
    :cond_0
    const/4 v0, 0x0

    .line 178660
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/14N;->l:Ljava/util/List;

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 178661
    iget-boolean v0, p0, LX/14N;->n:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 178662
    iget-boolean v0, p0, LX/14N;->o:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 178663
    iget-boolean v0, p0, LX/14N;->p:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 178665
    iget-boolean v0, p0, LX/14N;->q:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 178666
    iget-boolean v0, p0, LX/14N;->r:Z

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 178667
    iget-boolean v0, p0, LX/14N;->s:Z

    return v0
.end method

.method public final u()LX/14Q;
    .locals 1

    .prologue
    .line 178668
    iget-object v0, p0, LX/14N;->w:LX/14Q;

    return-object v0
.end method

.method public final v()LX/14R;
    .locals 1

    .prologue
    .line 178669
    iget-object v0, p0, LX/14N;->v:LX/14R;

    return-object v0
.end method

.method public final w()LX/14P;
    .locals 1

    .prologue
    .line 178670
    iget-object v0, p0, LX/14N;->x:LX/14P;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178671
    iget-object v0, p0, LX/14N;->f:Ljava/lang/String;

    return-object v0
.end method
