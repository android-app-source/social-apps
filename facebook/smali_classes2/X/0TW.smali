.class public LX/0TW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63258
    return-void
.end method

.method public static a(LX/0Or;LX/0TU;)LX/0TU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0TU;",
            ")",
            "LX/0TU;"
        }
    .end annotation

    .prologue
    .line 63256
    new-instance v0, LX/0TX;

    invoke-direct {v0, p1, p0}, LX/0TX;-><init>(LX/0TU;LX/0Or;)V

    return-object v0
.end method

.method public static a(LX/0Or;LX/0Tf;)LX/0Tf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0Tf;",
            ")",
            "LX/0Tf;"
        }
    .end annotation

    .prologue
    .line 63259
    new-instance v0, LX/0Tg;

    invoke-direct {v0, p1, p0}, LX/0Tg;-><init>(LX/0Tf;LX/0Or;)V

    return-object v0
.end method

.method public static a(LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")",
            "Ljava/util/concurrent/ScheduledExecutorService;"
        }
    .end annotation

    .prologue
    .line 63255
    new-instance v0, LX/0UC;

    invoke-direct {v0, p1, p0}, LX/0UC;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;)V

    return-object v0
.end method

.method public static b(Ljava/util/concurrent/Callable;LX/0Or;)Ljava/util/concurrent/Callable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)",
            "Ljava/util/concurrent/Callable",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63251
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    .line 63252
    invoke-interface {v0}, LX/0SI;->e()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 63253
    if-nez v2, :cond_0

    .line 63254
    :goto_0
    return-object p0

    :cond_0
    new-instance v1, LX/44V;

    invoke-direct {v1, p0, v0, v2}, LX/44V;-><init>(Ljava/util/concurrent/Callable;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    move-object p0, v1

    goto :goto_0
.end method
