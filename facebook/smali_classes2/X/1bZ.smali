.class public LX/1bZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final g:LX/1bZ;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Landroid/graphics/Bitmap$Config;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 280676
    invoke-static {}, LX/1bZ;->newBuilder()LX/1ba;

    move-result-object v0

    invoke-virtual {v0}, LX/1ba;->g()LX/1bZ;

    move-result-object v0

    sput-object v0, LX/1bZ;->g:LX/1bZ;

    return-void
.end method

.method public constructor <init>(LX/1ba;)V
    .locals 1

    .prologue
    .line 280677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280678
    iget v0, p1, LX/1ba;->a:I

    move v0, v0

    .line 280679
    iput v0, p0, LX/1bZ;->a:I

    .line 280680
    iget-boolean v0, p1, LX/1ba;->b:Z

    move v0, v0

    .line 280681
    iput-boolean v0, p0, LX/1bZ;->b:Z

    .line 280682
    iget-boolean v0, p1, LX/1ba;->c:Z

    move v0, v0

    .line 280683
    iput-boolean v0, p0, LX/1bZ;->c:Z

    .line 280684
    iget-boolean v0, p1, LX/1ba;->d:Z

    move v0, v0

    .line 280685
    iput-boolean v0, p0, LX/1bZ;->d:Z

    .line 280686
    iget-boolean v0, p1, LX/1ba;->e:Z

    move v0, v0

    .line 280687
    iput-boolean v0, p0, LX/1bZ;->e:Z

    .line 280688
    iget-object v0, p1, LX/1ba;->f:Landroid/graphics/Bitmap$Config;

    move-object v0, v0

    .line 280689
    iput-object v0, p0, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    .line 280690
    return-void
.end method

.method public static newBuilder()LX/1ba;
    .locals 1

    .prologue
    .line 280691
    new-instance v0, LX/1ba;

    invoke-direct {v0}, LX/1ba;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280692
    if-ne p0, p1, :cond_1

    .line 280693
    :cond_0
    :goto_0
    return v0

    .line 280694
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 280695
    :cond_3
    check-cast p1, LX/1bZ;

    .line 280696
    iget-boolean v2, p0, LX/1bZ;->b:Z

    iget-boolean v3, p1, LX/1bZ;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 280697
    :cond_4
    iget-boolean v2, p0, LX/1bZ;->c:Z

    iget-boolean v3, p1, LX/1bZ;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 280698
    :cond_5
    iget-boolean v2, p0, LX/1bZ;->d:Z

    iget-boolean v3, p1, LX/1bZ;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 280699
    :cond_6
    iget-boolean v2, p0, LX/1bZ;->e:Z

    iget-boolean v3, p1, LX/1bZ;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 280700
    :cond_7
    iget-object v2, p0, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    iget-object v3, p1, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280701
    iget v0, p0, LX/1bZ;->a:I

    .line 280702
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/1bZ;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 280703
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/1bZ;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 280704
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/1bZ;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 280705
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/1bZ;->e:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 280706
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    .line 280707
    return v0

    :cond_0
    move v0, v2

    .line 280708
    goto :goto_0

    :cond_1
    move v0, v2

    .line 280709
    goto :goto_1

    :cond_2
    move v0, v2

    .line 280710
    goto :goto_2

    :cond_3
    move v1, v2

    .line 280711
    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 280712
    const/4 v0, 0x0

    const-string v1, "%d-%b-%b-%b-%b-%s"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/1bZ;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, LX/1bZ;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, LX/1bZ;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-boolean v4, p0, LX/1bZ;->d:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-boolean v4, p0, LX/1bZ;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, LX/1bZ;->f:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4}, Landroid/graphics/Bitmap$Config;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
