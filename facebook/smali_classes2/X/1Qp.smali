.class public LX/1Qp;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/1Cw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "Landroid/widget/BaseAdapter;",
        "LX/1Cw;"
    }
.end annotation


# instance fields
.field private final a:LX/1OD;

.field private final b:LX/1OO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1OO",
            "<TVH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1OO;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1OO",
            "<TVH;>;)V"
        }
    .end annotation

    .prologue
    .line 245215
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 245216
    new-instance v0, LX/1Rw;

    invoke-direct {v0, p0}, LX/1Rw;-><init>(LX/1Qp;)V

    iput-object v0, p0, LX/1Qp;->a:LX/1OD;

    .line 245217
    iput-object p1, p0, LX/1Qp;->b:LX/1OO;

    .line 245218
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    iget-object v1, p0, LX/1Qp;->a:LX/1OD;

    invoke-interface {v0, v1}, LX/1OQ;->a(LX/1OD;)V

    .line 245219
    return-void
.end method

.method public static synthetic a(LX/1Qp;)V
    .locals 0

    .prologue
    .line 245214
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public static synthetic b(LX/1Qp;)V
    .locals 0

    .prologue
    .line 245213
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public static synthetic c(LX/1Qp;)V
    .locals 0

    .prologue
    .line 245212
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public static synthetic d(LX/1Qp;)V
    .locals 0

    .prologue
    .line 245211
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public static synthetic e(LX/1Qp;)V
    .locals 0

    .prologue
    .line 245210
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 245206
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0, p2, p1}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 245207
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    .line 245208
    const v2, 0x7f0d0044

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 245209
    return-object v1
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 245203
    const v0, 0x7f0d0044

    invoke-virtual {p3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 245204
    iget-object v1, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v1, v0, p1}, LX/1OQ;->a(LX/1a1;I)V

    .line 245205
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 245189
    const/4 v0, 0x1

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 245202
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 245201
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 245200
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0, p1}, LX/1OQ;->C_(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 245199
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0, p1}, LX/1OP;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 245195
    if-eqz p2, :cond_0

    move-object v3, p2

    .line 245196
    :goto_0
    invoke-virtual {p0, p1}, LX/1Qp;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, p1}, LX/1Qp;->getItemViewType(I)I

    move-result v4

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/1Qp;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 245197
    return-object v3

    .line 245198
    :cond_0
    invoke-virtual {p0, p1}, LX/1Qp;->getItemViewType(I)I

    move-result v0

    invoke-virtual {p0, v0, p3}, LX/1Qp;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 245194
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0}, LX/1OO;->ii_()I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 245193
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0}, LX/1OQ;->eC_()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 245192
    const/4 v0, 0x1

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 245190
    iget-object v0, p0, LX/1Qp;->b:LX/1OO;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 245191
    return-void
.end method
