.class public final LX/0Rj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation build Ljavax/annotation/CheckReturnValue;
.end annotation


# static fields
.field public static final COMMA_JOINER:LX/0PO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60432
    const/16 v0, 0x2c

    invoke-static {v0}, LX/0PO;->on(C)LX/0PO;

    move-result-object v0

    sput-object v0, LX/0Rj;->COMMA_JOINER:LX/0PO;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static and(LX/0Rl;LX/0Rl;)LX/0Rl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rl",
            "<-TT;>;",
            "LX/0Rl",
            "<-TT;>;)",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60428
    new-instance v2, LX/2IB;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Rl;

    .line 60429
    const/4 p0, 0x2

    new-array p0, p0, [LX/0Rl;

    const/4 p1, 0x0

    aput-object v0, p0, p1

    const/4 p1, 0x1

    aput-object v1, p0, p1

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    .line 60430
    invoke-direct {v2, v0}, LX/2IB;-><init>(Ljava/util/List;)V

    return-object v2
.end method

.method public static compose(LX/0Rl;LX/0QK;)LX/0Rl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rl",
            "<TB;>;",
            "LX/0QK",
            "<TA;+TB;>;)",
            "LX/0Rl",
            "<TA;>;"
        }
    .end annotation

    .prologue
    .line 60427
    new-instance v0, LX/2zz;

    invoke-direct {v0, p0, p1}, LX/2zz;-><init>(LX/0Rl;LX/0QK;)V

    return-object v0
.end method

.method public static equalTo(Ljava/lang/Object;)LX/0Rl;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60433
    if-nez p0, :cond_0

    invoke-static {}, LX/0Rj;->isNull()LX/0Rl;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/2BR;

    invoke-direct {v0, p0}, LX/2BR;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static in(Ljava/util/Collection;)LX/0Rl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TT;>;)",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60426
    new-instance v0, LX/0Rk;

    invoke-direct {v0, p0}, LX/0Rk;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static instanceOf(Ljava/lang/Class;)LX/0Rl;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "Class.isInstance"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0Rl",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60425
    new-instance v0, LX/3d1;

    invoke-direct {v0, p0}, LX/3d1;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static isNull()LX/0Rl;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60424
    sget-object v0, LX/1i5;->IS_NULL:LX/1i5;

    invoke-virtual {v0}, LX/1i5;->withNarrowedType()LX/0Rl;

    move-result-object v0

    return-object v0
.end method

.method public static not(LX/0Rl;)LX/0Rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rl",
            "<TT;>;)",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60423
    new-instance v0, LX/0Rm;

    invoke-direct {v0, p0}, LX/0Rm;-><init>(LX/0Rl;)V

    return-object v0
.end method

.method public static notNull()LX/0Rl;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtCompatible;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0Rl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60422
    sget-object v0, LX/1i5;->NOT_NULL:LX/1i5;

    invoke-virtual {v0}, LX/1i5;->withNarrowedType()LX/0Rl;

    move-result-object v0

    return-object v0
.end method
