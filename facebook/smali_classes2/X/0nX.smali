.class public abstract LX/0nX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lE;
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# instance fields
.field public a:LX/0lZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/0lD;
.end method

.method public a(I)LX/0nX;
    .locals 0

    .prologue
    .line 136322
    return-object p0
.end method

.method public abstract a(LX/0lD;)LX/0nX;
.end method

.method public a(LX/0lZ;)LX/0nX;
    .locals 0

    .prologue
    .line 136323
    iput-object p1, p0, LX/0nX;->a:LX/0lZ;

    .line 136324
    return-object p0
.end method

.method public a(LX/0lc;)LX/0nX;
    .locals 1

    .prologue
    .line 136325
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LX/4pa;)LX/0nX;
    .locals 0

    .prologue
    .line 136326
    return-object p0
.end method

.method public abstract a(C)V
.end method

.method public abstract a(D)V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(LX/0lG;)V
.end method

.method public abstract a(LX/0ln;[BII)V
.end method

.method public final a(LX/4pX;)V
    .locals 3

    .prologue
    .line 136345
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Generator of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support schema of type \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, LX/4pX;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a(Ljava/lang/Object;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public final a(Ljava/lang/String;D)V
    .locals 0

    .prologue
    .line 136342
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136343
    invoke-virtual {p0, p2, p3}, LX/0nX;->a(D)V

    .line 136344
    return-void
.end method

.method public final a(Ljava/lang/String;F)V
    .locals 0

    .prologue
    .line 136310
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136311
    invoke-virtual {p0, p2}, LX/0nX;->a(F)V

    .line 136312
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 136339
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136340
    invoke-virtual {p0, p2}, LX/0nX;->b(I)V

    .line 136341
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 136336
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136337
    invoke-virtual {p0, p2, p3}, LX/0nX;->a(J)V

    .line 136338
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 136333
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136334
    invoke-virtual {p0, p2}, LX/0nX;->a(Ljava/lang/Object;)V

    .line 136335
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136330
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136331
    invoke-virtual {p0, p2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 136332
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 136327
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136328
    invoke-virtual {p0, p2}, LX/0nX;->a(Z)V

    .line 136329
    return-void
.end method

.method public abstract a(Ljava/math/BigDecimal;)V
.end method

.method public abstract a(Ljava/math/BigInteger;)V
.end method

.method public a(S)V
    .locals 0

    .prologue
    .line 136305
    invoke-virtual {p0, p1}, LX/0nX;->b(I)V

    .line 136306
    return-void
.end method

.method public abstract a(Z)V
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 136307
    sget-object v0, LX/0lm;->b:LX/0ln;

    move-object v0, v0

    .line 136308
    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {p0, v0, p1, v1, v2}, LX/0nX;->a(LX/0ln;[BII)V

    .line 136309
    return-void
.end method

.method public abstract a([CII)V
.end method

.method public abstract b(I)V
.end method

.method public abstract b(LX/0lc;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract b([CII)V
.end method

.method public abstract c()LX/0nX;
.end method

.method public abstract c(LX/0lc;)V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract close()V
.end method

.method public abstract d()V
.end method

.method public d(LX/0lc;)V
    .locals 1

    .prologue
    .line 136313
    invoke-interface {p1}, LX/0lc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 136314
    return-void
.end method

.method public abstract d(Ljava/lang/String;)V
.end method

.method public abstract e()V
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract f()V
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136315
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136316
    invoke-virtual {p0}, LX/0nX;->d()V

    .line 136317
    return-void
.end method

.method public abstract flush()V
.end method

.method public abstract g()V
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 136318
    invoke-virtual {p0, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 136319
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 136320
    return-void
.end method

.method public abstract h()V
.end method
