.class public LX/1d0;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Landroid/graphics/Paint;

.field public static b:Landroid/graphics/Paint;

.field private static c:Landroid/graphics/Rect;

.field private static d:Landroid/graphics/Paint;

.field private static e:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(F)I
    .locals 1

    .prologue
    .line 283997
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;I)I
    .locals 2

    .prologue
    .line 284045
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 284046
    int-to-float v1, p1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIII)V
    .locals 7

    .prologue
    .line 284047
    if-le p2, p4, :cond_1

    .line 284048
    :goto_0
    if-le p3, p5, :cond_0

    .line 284049
    :goto_1
    int-to-float v1, p4

    int-to-float v2, p5

    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 284050
    return-void

    :cond_0
    move v6, p5

    move p5, p3

    move p3, v6

    goto :goto_1

    :cond_1
    move v6, p4

    move p4, p2

    move p2, v6

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIII)V
    .locals 6

    .prologue
    .line 284042
    add-int v4, p2, p4

    int-to-float v0, p5

    invoke-static {v0}, LX/1d0;->a(F)I

    move-result v0

    mul-int/2addr v0, p6

    add-int v5, p3, v0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIII)V

    .line 284043
    int-to-float v0, p4

    invoke-static {v0}, LX/1d0;->a(F)I

    move-result v0

    mul-int/2addr v0, p6

    add-int v4, p2, v0

    add-int v5, p3, p5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIII)V

    .line 284044
    return-void
.end method

.method public static a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 284041
    invoke-static {p0}, LX/1dF;->a(Landroid/view/View;)LX/1dM;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1dF;->b(Landroid/view/View;)LX/1dR;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/1dF;->c(Landroid/view/View;)LX/1dd;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1cn;Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 283998
    invoke-virtual {p0}, LX/1cn;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 283999
    sget-object v0, LX/1d0;->c:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 284000
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1d0;->c:Landroid/graphics/Rect;

    .line 284001
    :cond_0
    sget-object v0, LX/1d0;->d:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 284002
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 284003
    sput-object v0, LX/1d0;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 284004
    sget-object v0, LX/1d0;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-static {v2, v1}, LX/1d0;->a(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 284005
    :cond_1
    sget-object v0, LX/1d0;->e:Landroid/graphics/Paint;

    if-nez v0, :cond_2

    .line 284006
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 284007
    sput-object v0, LX/1d0;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 284008
    sget-object v0, LX/1d0;->e:Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-static {v2, v1}, LX/1d0;->a(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 284009
    :cond_2
    invoke-virtual {p0}, LX/1cn;->getMountItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_6

    .line 284010
    invoke-virtual {p0, v1}, LX/1cn;->a(I)LX/1cv;

    move-result-object v0

    .line 284011
    iget-object v3, v0, LX/1cv;->a:LX/1X1;

    move-object v3, v3

    .line 284012
    iget-object v4, v0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v4

    .line 284013
    iget-object v4, v3, LX/1X1;->e:LX/1S3;

    move-object v4, v4

    .line 284014
    instance-of v4, v4, LX/1dT;

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 284015
    if-eqz v4, :cond_4

    .line 284016
    instance-of v4, v0, Landroid/view/View;

    if-eqz v4, :cond_5

    .line 284017
    check-cast v0, Landroid/view/View;

    .line 284018
    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 284019
    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 284020
    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 284021
    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    .line 284022
    :cond_3
    :goto_2
    sget-object v0, LX/1d0;->d:Landroid/graphics/Paint;

    .line 284023
    invoke-static {v3}, LX/1X1;->b(LX/1X1;)Z

    move-result v4

    if-eqz v4, :cond_8

    const v4, -0x6600ff01

    :goto_3
    move v4, v4

    .line 284024
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 284025
    sget-object v0, LX/1d0;->d:Landroid/graphics/Paint;

    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    .line 284026
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v7

    float-to-int v7, v7

    div-int/lit8 v7, v7, 0x2

    .line 284027
    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v7

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v7

    int-to-float v9, v9

    iget v10, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v7

    int-to-float v10, v10

    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v11, v7

    int-to-float v11, v7

    move-object v7, p1

    move-object v12, v0

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 284028
    sget-object v0, LX/1d0;->e:Landroid/graphics/Paint;

    .line 284029
    invoke-static {v3}, LX/1X1;->b(LX/1X1;)Z

    move-result v4

    if-eqz v4, :cond_9

    const v4, -0xff0001

    :goto_4
    move v3, v4

    .line 284030
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 284031
    sget-object v0, LX/1d0;->e:Landroid/graphics/Paint;

    sget-object v3, LX/1d0;->c:Landroid/graphics/Rect;

    sget-object v4, LX/1d0;->e:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    float-to-int v4, v4

    sget-object v5, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sget-object v6, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    const/16 v6, 0xc

    invoke-static {v2, v6}, LX/1d0;->a(Landroid/content/res/Resources;I)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 284032
    iget v9, v3, Landroid/graphics/Rect;->left:I

    iget v10, v3, Landroid/graphics/Rect;->top:I

    move-object v7, p1

    move-object v8, v0

    move v11, v4

    move v12, v4

    move v13, v5

    invoke-static/range {v7 .. v13}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIII)V

    .line 284033
    iget v9, v3, Landroid/graphics/Rect;->left:I

    iget v10, v3, Landroid/graphics/Rect;->bottom:I

    neg-int v12, v4

    move-object v7, p1

    move-object v8, v0

    move v11, v4

    move v13, v5

    invoke-static/range {v7 .. v13}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIII)V

    .line 284034
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->top:I

    neg-int v11, v4

    move-object v7, p1

    move-object v8, v0

    move v12, v4

    move v13, v5

    invoke-static/range {v7 .. v13}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIII)V

    .line 284035
    iget v9, v3, Landroid/graphics/Rect;->right:I

    iget v10, v3, Landroid/graphics/Rect;->bottom:I

    neg-int v11, v4

    neg-int v12, v4

    move-object v7, p1

    move-object v8, v0

    move v13, v5

    invoke-static/range {v7 .. v13}, LX/1d0;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIII)V

    .line 284036
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto/16 :goto_0

    .line 284037
    :cond_5
    instance-of v4, v0, Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_3

    .line 284038
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 284039
    sget-object v4, LX/1d0;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    .line 284040
    :cond_6
    return-void

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_8
    const/high16 v4, -0x66010000

    goto/16 :goto_3

    :cond_9
    const v4, -0xffff01

    goto :goto_4
.end method
