.class public LX/1Lk;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Ln;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Ln;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 234167
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Ln;
    .locals 13

    .prologue
    .line 234168
    sget-object v0, LX/1Lk;->a:LX/1Ln;

    if-nez v0, :cond_1

    .line 234169
    const-class v1, LX/1Lk;

    monitor-enter v1

    .line 234170
    :try_start_0
    sget-object v0, LX/1Lk;->a:LX/1Ln;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 234171
    if-eqz v2, :cond_0

    .line 234172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 234173
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v5

    check-cast v5, LX/1AA;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v6

    check-cast v6, LX/0en;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v7

    check-cast v7, LX/0V8;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v8

    check-cast v8, LX/0pq;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v11

    check-cast v11, LX/0wp;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static/range {v3 .. v12}, LX/19Y;->a(Landroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0pq;LX/0lC;LX/0ad;LX/0wp;LX/0Sh;)LX/1Ln;

    move-result-object v3

    move-object v0, v3

    .line 234174
    sput-object v0, LX/1Lk;->a:LX/1Ln;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 234176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 234177
    :cond_1
    sget-object v0, LX/1Lk;->a:LX/1Ln;

    return-object v0

    .line 234178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 234179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 234180
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v2

    check-cast v2, LX/1AA;

    invoke-static {p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v3

    check-cast v3, LX/0en;

    invoke-static {p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v4

    check-cast v4, LX/0V8;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v5

    check-cast v5, LX/0pq;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v8

    check-cast v8, LX/0wp;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v9

    check-cast v9, LX/0Sh;

    invoke-static/range {v0 .. v9}, LX/19Y;->a(Landroid/content/Context;LX/0SG;LX/1AA;LX/0en;LX/0V8;LX/0pq;LX/0lC;LX/0ad;LX/0wp;LX/0Sh;)LX/1Ln;

    move-result-object v0

    return-object v0
.end method
