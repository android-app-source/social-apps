.class public final LX/1IQ;
.super LX/1IC;
.source ""


# static fields
.field public static final INSTANCE:LX/1IQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228661
    new-instance v0, LX/1IQ;

    invoke-direct {v0}, LX/1IQ;-><init>()V

    sput-object v0, LX/1IQ;->INSTANCE:LX/1IQ;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 228659
    const-string v0, "CharMatcher.any()"

    invoke-direct {p0, v0}, LX/1IC;-><init>(Ljava/lang/String;)V

    .line 228660
    return-void
.end method


# virtual methods
.method public final indexIn(Ljava/lang/CharSequence;)I
    .locals 1

    .prologue
    .line 228658
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final indexIn(Ljava/lang/CharSequence;I)I
    .locals 1

    .prologue
    .line 228655
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 228656
    invoke-static {p2, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 228657
    if-ne p2, v0, :cond_0

    const/4 p2, -0x1

    :cond_0
    return p2
.end method

.method public final matches(C)Z
    .locals 1

    .prologue
    .line 228662
    const/4 v0, 0x1

    return v0
.end method

.method public final matchesAllOf(Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 228653
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228654
    const/4 v0, 0x1

    return v0
.end method

.method public final or(LX/1IA;)LX/1IA;
    .locals 0

    .prologue
    .line 228651
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228652
    return-object p0
.end method

.method public final removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228647
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228648
    const-string v0, ""

    return-object v0
.end method

.method public final trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 228649
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228650
    const-string v0, ""

    return-object v0
.end method
