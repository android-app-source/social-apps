.class public LX/1Kw;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;
.implements LX/0fs;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Kx;

.field private final c:LX/0bH;

.field public d:LX/1Pq;

.field public e:LX/1Iu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Iu",
            "<",
            "Lcom/facebook/api/feed/data/LegacyFeedUnitUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233380
    const-class v0, LX/1Kw;

    sput-object v0, LX/1Kw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0bH;LX/1Kx;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 233381
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 233382
    iput-object p2, p0, LX/1Kw;->b:LX/1Kx;

    .line 233383
    iput-object p1, p0, LX/1Kw;->c:LX/0bH;

    .line 233384
    return-void
.end method


# virtual methods
.method public final a(LX/1Qr;LX/0g8;LX/1Pf;)V
    .locals 0

    .prologue
    .line 233385
    iput-object p3, p0, LX/1Kw;->d:LX/1Pq;

    .line 233386
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 233387
    iget-object v0, p0, LX/1Kw;->f:LX/0TF;

    if-nez v0, :cond_0

    .line 233388
    new-instance v0, LX/1ZU;

    invoke-direct {v0, p0}, LX/1ZU;-><init>(LX/1Kw;)V

    move-object v0, v0

    .line 233389
    iput-object v0, p0, LX/1Kw;->f:LX/0TF;

    .line 233390
    :cond_0
    iget-object v0, p0, LX/1Kw;->b:LX/1Kx;

    iget-object v1, p0, LX/1Kw;->f:LX/0TF;

    .line 233391
    iput-object v1, v0, LX/1Kx;->c:LX/0TF;

    .line 233392
    iget-object v0, p0, LX/1Kw;->c:LX/0bH;

    iget-object v1, p0, LX/1Kw;->b:LX/1Kx;

    .line 233393
    iget-object p0, v1, LX/1Kx;->b:LX/1Ky;

    move-object v1, p0

    .line 233394
    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 233395
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 233396
    iget-object v0, p0, LX/1Kw;->c:LX/0bH;

    iget-object v1, p0, LX/1Kw;->b:LX/1Kx;

    .line 233397
    iget-object v2, v1, LX/1Kx;->b:LX/1Ky;

    move-object v1, v2

    .line 233398
    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 233399
    iget-object v0, p0, LX/1Kw;->b:LX/1Kx;

    invoke-virtual {v0}, LX/1Kx;->b()V

    .line 233400
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 233401
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Kw;->d:LX/1Pq;

    .line 233402
    return-void
.end method
