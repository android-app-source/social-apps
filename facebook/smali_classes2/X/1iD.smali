.class public LX/1iD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1iD;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297569
    iput-object p1, p0, LX/1iD;->a:LX/0Zb;

    .line 297570
    return-void
.end method

.method public static a(LX/0QB;)LX/1iD;
    .locals 4

    .prologue
    .line 297588
    sget-object v0, LX/1iD;->b:LX/1iD;

    if-nez v0, :cond_1

    .line 297589
    const-class v1, LX/1iD;

    monitor-enter v1

    .line 297590
    :try_start_0
    sget-object v0, LX/1iD;->b:LX/1iD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297591
    if-eqz v2, :cond_0

    .line 297592
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297593
    new-instance p0, LX/1iD;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/1iD;-><init>(LX/0Zb;)V

    .line 297594
    move-object v0, p0

    .line 297595
    sput-object v0, LX/1iD;->b:LX/1iD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297596
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297597
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297598
    :cond_1
    sget-object v0, LX/1iD;->b:LX/1iD;

    return-object v0

    .line 297599
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297600
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/15D;JJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;JJ)V"
        }
    .end annotation

    .prologue
    .line 297571
    iget-object v0, p0, LX/1iD;->a:LX/0Zb;

    const-string v1, "android_network_thread_time"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 297572
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 297573
    :goto_0
    return-void

    .line 297574
    :cond_0
    const-string v1, "friendly_name"

    .line 297575
    iget-object v2, p1, LX/15D;->c:Ljava/lang/String;

    move-object v2, v2

    .line 297576
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 297577
    iget-object v1, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 297578
    if-eqz v1, :cond_1

    .line 297579
    const-string v1, "caller_class"

    .line 297580
    iget-object v2, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 297581
    iget-object v3, v2, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v2, v3

    .line 297582
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 297583
    :cond_1
    const-string v1, "total_time"

    invoke-virtual {v0, v1, p2, p3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 297584
    const-wide/16 v2, 0x0

    cmp-long v1, p4, v2

    if-ltz v1, :cond_2

    .line 297585
    const-string v1, "time_reading"

    invoke-virtual {v0, v1, p4, p5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 297586
    const-string v1, "time_processing"

    sub-long v2, p2, p4

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 297587
    :cond_2
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
