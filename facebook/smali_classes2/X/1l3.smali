.class public final LX/1l3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kQ;


# direct methods
.method public constructor <init>(LX/1kQ;)V
    .locals 0

    .prologue
    .line 310885
    iput-object p1, p0, LX/1l3;->a:LX/1kQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 310886
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 310887
    invoke-static {p1}, LX/1kR;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v0

    .line 310888
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 310889
    iget-object v2, p0, LX/1l3;->a:LX/1kQ;

    .line 310890
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 310891
    if-nez v0, :cond_2

    .line 310892
    :cond_0
    iget-object v0, p0, LX/1l3;->a:LX/1kQ;

    iget-object v0, v0, LX/1kQ;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x930001

    const/16 v3, 0xd

    const-string v4, "fetcher"

    const-class v5, LX/1kQ;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISLjava/lang/String;Ljava/lang/String;)V

    .line 310893
    if-eqz p1, :cond_6

    .line 310894
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 310895
    if-eqz v2, :cond_6

    .line 310896
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 310897
    check-cast v2, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v2

    .line 310898
    :goto_0
    move-object v2, v2

    .line 310899
    move-object v0, v2

    .line 310900
    if-nez v0, :cond_1

    .line 310901
    iget-object v2, p0, LX/1l3;->a:LX/1kQ;

    iget-object v2, v2, LX/1kQ;->i:LX/03V;

    sget-object v3, LX/1kQ;->b:Ljava/lang/String;

    .line 310902
    const/4 v6, 0x0

    .line 310903
    if-nez p1, :cond_7

    .line 310904
    const-string v5, "null GraphQLResult"

    .line 310905
    :goto_1
    move-object v5, v5

    .line 310906
    move-object v4, v5

    .line 310907
    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 310908
    :cond_1
    iget-object v2, p0, LX/1l3;->a:LX/1kQ;

    iget-object v2, v2, LX/1kQ;->h:LX/1kS;

    invoke-virtual {v2, v0}, LX/1kS;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;)V

    .line 310909
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 310910
    :cond_2
    iget-object v3, v2, LX/1kQ;->k:LX/0fO;

    invoke-virtual {v3}, LX/0fO;->a()Z

    move-result v6

    .line 310911
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v7, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;

    .line 310912
    invoke-static {v3, v6}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Z)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v8

    .line 310913
    invoke-static {v3, v5, v6}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a(Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$ProductionPromptsInfoModel;Ljava/util/Calendar;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v2, LX/1kQ;->d:LX/1kR;

    invoke-virtual {v3, v8}, LX/1kR;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 310914
    invoke-virtual {v8}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v8}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 310915
    iget-object v10, v2, LX/1kQ;->j:LX/0ad;

    sget-short v11, LX/1Nu;->m:S

    invoke-interface {v10, v11, v9}, LX/0ad;->a(SZ)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, v2, LX/1kQ;->j:LX/0ad;

    sget-short v11, LX/AzO;->d:S

    invoke-interface {v10, v11, v9}, LX/0ad;->a(SZ)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 310916
    :cond_3
    :goto_3
    move v3, v3

    .line 310917
    if-eqz v3, :cond_4

    .line 310918
    new-instance v3, LX/1kW;

    invoke-direct {v3, v8}, LX/1kW;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 310919
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    :cond_5
    move v3, v9

    .line 310920
    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    goto :goto_0

    .line 310921
    :cond_7
    iget-object v5, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 310922
    check-cast v5, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v5

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    move v4, v5

    .line 310923
    :goto_4
    if-eqz v4, :cond_8

    .line 310924
    iget-object v5, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 310925
    check-cast v5, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->j()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ProductionPromptsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    move v6, v5

    .line 310926
    :cond_8
    iget-object v5, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 310927
    check-cast v5, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v5

    if-nez v5, :cond_a

    .line 310928
    const-string v5, "no client production prompts in graphql result. Has Production prompts in result? %s with %d prompt models"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_9
    move v4, v6

    .line 310929
    goto :goto_4

    .line 310930
    :cond_a
    const-string v6, "Client production prompt models exist: number of client prompts: %d"

    .line 310931
    iget-object v5, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 310932
    check-cast v5, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;->a()Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel$ClientProductionPromptsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
.end method
