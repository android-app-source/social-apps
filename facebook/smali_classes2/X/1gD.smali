.class public final LX/1gD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 293877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1bh;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bh;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293878
    :try_start_0
    instance-of v0, p0, LX/1gE;

    if-eqz v0, :cond_1

    .line 293879
    check-cast p0, LX/1gE;

    .line 293880
    iget-object v0, p0, LX/1gE;->a:Ljava/util/List;

    move-object v3, v0

    .line 293881
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 293882
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 293883
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bh;

    invoke-static {v0}, LX/1gD;->c(LX/1bh;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293884
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 293885
    :goto_1
    return-object v0

    .line 293886
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 293887
    invoke-static {p0}, LX/1gD;->c(LX/1bh;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 293888
    :catch_0
    move-exception v0

    .line 293889
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static b(LX/1bh;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 293890
    :try_start_0
    instance-of v0, p0, LX/1gE;

    if-eqz v0, :cond_0

    .line 293891
    check-cast p0, LX/1gE;

    .line 293892
    iget-object v0, p0, LX/1gE;->a:Ljava/util/List;

    move-object v0, v0

    .line 293893
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bh;

    invoke-static {v0}, LX/1gD;->c(LX/1bh;)Ljava/lang/String;

    move-result-object v0

    .line 293894
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1gD;->c(LX/1bh;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 293895
    :catch_0
    move-exception v0

    .line 293896
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c(LX/1bh;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 293897
    invoke-interface {p0}, LX/1bh;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, LX/03l;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
