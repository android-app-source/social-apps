.class public final LX/1LZ;
.super LX/1La;
.source ""


# instance fields
.field public final synthetic a:LX/1KV;


# direct methods
.method public constructor <init>(LX/1KV;)V
    .locals 0

    .prologue
    .line 233995
    iput-object p1, p0, LX/1LZ;->a:LX/1KV;

    invoke-direct {p0}, LX/1La;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 233974
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/1K5;

    invoke-interface {v0, p1}, LX/1K5;->a(Landroid/content/res/Configuration;)V

    .line 233975
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 233993
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    invoke-interface {v0, p2, p3, p4}, LX/0hj;->a(IILandroid/content/Intent;)V

    .line 233994
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 233991
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0fm;

    invoke-interface {v0, p2}, LX/0fm;->a(Landroid/view/View;)V

    .line 233992
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 233989
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/1KT;

    invoke-interface {v0, p1}, LX/1KT;->a(Z)V

    .line 233990
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 233987
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0hl;

    invoke-interface {v0}, LX/0hl;->f()V

    .line 233988
    return-void
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 233985
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0fm;

    invoke-interface {v0}, LX/0fm;->g()V

    .line 233986
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 233983
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0hl;

    invoke-interface {v0}, LX/0hl;->e()V

    .line 233984
    return-void
.end method

.method public final c(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 233981
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0hk;

    invoke-interface {v0}, LX/0hk;->d()V

    .line 233982
    return-void
.end method

.method public final d(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 233979
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0hk;

    invoke-interface {v0}, LX/0hk;->c()V

    .line 233980
    return-void
.end method

.method public final e(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 233976
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/1DD;

    invoke-interface {v0}, LX/1DD;->b()V

    .line 233977
    iget-object v0, p0, LX/1LZ;->a:LX/1KV;

    iget-object v0, v0, LX/1KV;->a:LX/0hj;

    check-cast v0, LX/0hi;

    invoke-virtual {v0}, LX/0hi;->a()V

    .line 233978
    return-void
.end method
