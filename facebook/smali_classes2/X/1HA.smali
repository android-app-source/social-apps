.class public LX/1HA;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 226523
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 226524
    sget-object v0, LX/1HA;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 226525
    const-class v1, LX/1HA;

    monitor-enter v1

    .line 226526
    :try_start_0
    sget-object v0, LX/1HA;->a:Ljava/lang/Boolean;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 226527
    if-eqz v2, :cond_0

    .line 226528
    :try_start_1
    invoke-static {}, LX/1Aq;->b()Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/1HA;->a:Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226529
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 226530
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226531
    :cond_1
    sget-object v0, LX/1HA;->a:Ljava/lang/Boolean;

    return-object v0

    .line 226532
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 226533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226534
    invoke-static {}, LX/1Aq;->b()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
