.class public final synthetic LX/1lp;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 312398
    invoke-static {}, LX/0uO;->values()[LX/0uO;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/1lp;->c:[I

    :try_start_0
    sget-object v0, LX/1lp;->c:[I

    sget-object v1, LX/0uO;->SUCCESS:LX/0uO;

    invoke-virtual {v1}, LX/0uO;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_0
    :try_start_1
    sget-object v0, LX/1lp;->c:[I

    sget-object v1, LX/0uO;->END_OF_FEED:LX/0uO;

    invoke-virtual {v1}, LX/0uO;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_1
    :try_start_2
    sget-object v0, LX/1lp;->c:[I

    sget-object v1, LX/0uO;->END_OF_CACHED_FEED:LX/0uO;

    invoke-virtual {v1}, LX/0uO;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_2
    :try_start_3
    sget-object v0, LX/1lp;->c:[I

    sget-object v1, LX/0uO;->ALREADY_SCHEDULED:LX/0uO;

    invoke-virtual {v1}, LX/0uO;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    .line 312399
    :goto_3
    invoke-static {}, LX/18G;->values()[LX/18G;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/1lp;->b:[I

    :try_start_4
    sget-object v0, LX/1lp;->b:[I

    sget-object v1, LX/18G;->EMPTY:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_4
    :try_start_5
    sget-object v0, LX/1lp;->b:[I

    sget-object v1, LX/18G;->SUCCESS:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_5
    :try_start_6
    sget-object v0, LX/1lp;->b:[I

    sget-object v1, LX/18G;->SERVICE_EXCEPTION:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_6
    :try_start_7
    sget-object v0, LX/1lp;->b:[I

    sget-object v1, LX/18G;->CANCELLATION:LX/18G;

    invoke-virtual {v1}, LX/18G;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    .line 312400
    :goto_7
    invoke-static {}, LX/1lq;->values()[LX/1lq;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/1lp;->a:[I

    :try_start_8
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->HideLoadingIndicator:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_8
    :try_start_9
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->ShowNSBPLoadingIndicator:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_9
    :try_start_a
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->ShowNSBPFullyLoadedText:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_a
    :try_start_b
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->HideNSBP:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_b
    :try_start_c
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->HideNSBPIfNotFullyLoaded:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_c
    :try_start_d
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->AvoidNewStoryPill:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_d
    :try_start_e
    sget-object v0, LX/1lp;->a:[I

    sget-object v1, LX/1lq;->ShowNewStoryPill:LX/1lq;

    invoke-virtual {v1}, LX/1lq;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_e
    return-void

    :catch_0
    goto :goto_e

    :catch_1
    goto :goto_d

    :catch_2
    goto :goto_c

    :catch_3
    goto :goto_b

    :catch_4
    goto :goto_a

    :catch_5
    goto :goto_9

    :catch_6
    goto :goto_8

    :catch_7
    goto :goto_7

    :catch_8
    goto :goto_6

    :catch_9
    goto :goto_5

    :catch_a
    goto/16 :goto_4

    :catch_b
    goto/16 :goto_3

    :catch_c
    goto/16 :goto_2

    :catch_d
    goto/16 :goto_1

    :catch_e
    goto/16 :goto_0
.end method
