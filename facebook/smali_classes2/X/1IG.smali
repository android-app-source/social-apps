.class public final LX/1IG;
.super LX/1IH;
.source ""


# static fields
.field public static final INSTANCE:LX/1IG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228580
    new-instance v0, LX/1IG;

    invoke-direct {v0}, LX/1IG;-><init>()V

    sput-object v0, LX/1IG;->INSTANCE:LX/1IG;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 228581
    const-string v0, "CharMatcher.digit()"

    .line 228582
    const-string v1, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    move-object v1, v1

    .line 228583
    const/16 v5, 0x1f

    .line 228584
    new-array v3, v5, [C

    .line 228585
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    .line 228586
    const-string v4, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v4, v4, 0x9

    int-to-char v4, v4

    aput-char v4, v3, v2

    .line 228587
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 228588
    :cond_0
    move-object v2, v3

    .line 228589
    invoke-direct {p0, v0, v1, v2}, LX/1IH;-><init>(Ljava/lang/String;[C[C)V

    .line 228590
    return-void
.end method
