.class public LX/0mh;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final j:LX/0mi;

.field private static final k:LX/0mi;

.field private static final l:LX/0mi;

.field private static final m:LX/0mi;

.field private static final n:LX/0mj;


# instance fields
.field public final a:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "LX/0mj;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ma;

.field public final c:LX/0mc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0mP;

.field public final e:LX/0mO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0mk;

.field public final g:LX/0Zh;

.field public final h:LX/0mM;

.field public final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/flexiblesampling/SamplingPolicyConfig;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 132773
    new-instance v1, LX/0mi;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2d

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1e

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    sput-object v1, LX/0mh;->j:LX/0mi;

    .line 132774
    new-instance v1, LX/0mi;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    sput-object v1, LX/0mh;->k:LX/0mi;

    .line 132775
    new-instance v1, LX/0mi;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2d

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1e

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    sput-object v1, LX/0mh;->l:LX/0mi;

    .line 132776
    new-instance v1, LX/0mi;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v1 .. v9}, LX/0mi;-><init>(JJJJ)V

    sput-object v1, LX/0mh;->m:LX/0mi;

    .line 132777
    new-instance v0, LX/0mj;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/0mj;-><init>(Z)V

    sput-object v0, LX/0mh;->n:LX/0mj;

    return-void
.end method

.method public constructor <init>(LX/0mZ;)V
    .locals 17

    .prologue
    .line 132778
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 132779
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->b:LX/0Zk;

    invoke-static {v1}, LX/0mh;->a(LX/0Zk;)LX/0Zk;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->a:LX/0Zk;

    .line 132780
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->c:LX/0ma;

    invoke-static {v1}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ma;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->b:LX/0ma;

    .line 132781
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->k:LX/0mP;

    invoke-static {v1}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0mP;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->d:LX/0mP;

    .line 132782
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->l:LX/0mO;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->e:LX/0mO;

    .line 132783
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->g:LX/0Zh;

    .line 132784
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->f:LX/0mc;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->c:LX/0mc;

    .line 132785
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->j:LX/0mM;

    invoke-static {v1}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0mM;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->h:LX/0mM;

    .line 132786
    move-object/from16 v0, p1

    iget-object v1, v0, LX/0mZ;->m:Ljava/lang/Class;

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->i:Ljava/lang/Class;

    .line 132787
    new-instance v1, LX/0mk;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/0mZ;->a:Landroid/content/Context;

    invoke-static {v2}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/0mZ;->i:Ljava/lang/Class;

    invoke-static {v3}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/0mZ;->g:LX/0mI;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/0mZ;->h:LX/0mI;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/0mZ;->m:Ljava/lang/Class;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/0mZ;->n:Ljava/lang/Class;

    invoke-static {v7}, LX/0mh;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v7

    new-instance v8, LX/0mm;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/0mh;->g:LX/0Zh;

    move-object/from16 v0, p1

    iget-object v9, v0, LX/0mZ;->d:LX/0mR;

    invoke-static {v9}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0mR;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/0mZ;->e:LX/0mb;

    invoke-static {v10}, LX/0mh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0mb;

    invoke-direct {v8, v11, v9, v10}, LX/0mm;-><init>(LX/0Zh;LX/0mR;LX/0mb;)V

    move-object/from16 v0, p0

    iget-object v9, v0, LX/0mh;->h:LX/0mM;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/0mh;->g:LX/0Zh;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/0mZ;->c:LX/0ma;

    move-object/from16 v0, p1

    iget-object v12, v0, LX/0mZ;->o:LX/0mg;

    if-eqz v12, :cond_0

    move-object/from16 v0, p1

    iget-object v12, v0, LX/0mZ;->o:LX/0mg;

    :goto_0
    move-object/from16 v0, p1

    iget-object v13, v0, LX/0mZ;->p:LX/0mg;

    if-eqz v13, :cond_1

    move-object/from16 v0, p1

    iget-object v13, v0, LX/0mZ;->p:LX/0mg;

    :goto_1
    move-object/from16 v0, p1

    iget-object v14, v0, LX/0mZ;->q:LX/0me;

    if-eqz v14, :cond_2

    move-object/from16 v0, p1

    iget-object v14, v0, LX/0mZ;->q:LX/0me;

    :goto_2
    move-object/from16 v0, p1

    iget-object v15, v0, LX/0mZ;->r:LX/0mU;

    move-object/from16 v0, p1

    iget-object v0, v0, LX/0mZ;->s:LX/40D;

    move-object/from16 v16, v0

    invoke-direct/range {v1 .. v16}, LX/0mk;-><init>(Landroid/content/Context;Ljava/lang/Class;LX/0mI;LX/0mI;Ljava/lang/Class;Ljava/lang/Class;LX/0mm;LX/0mM;LX/0Zh;LX/0ma;LX/0mg;LX/0mg;LX/0me;LX/0mU;LX/40D;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/0mh;->f:LX/0mk;

    .line 132788
    return-void

    .line 132789
    :cond_0
    new-instance v12, LX/0mn;

    sget-object v13, LX/0mh;->j:LX/0mi;

    sget-object v14, LX/0mh;->l:LX/0mi;

    invoke-direct {v12, v13, v14}, LX/0mn;-><init>(LX/0mi;LX/0mi;)V

    goto :goto_0

    :cond_1
    new-instance v13, LX/0mn;

    sget-object v14, LX/0mh;->k:LX/0mi;

    sget-object v15, LX/0mh;->m:LX/0mi;

    invoke-direct {v13, v14, v15}, LX/0mn;-><init>(LX/0mi;LX/0mi;)V

    goto :goto_1

    :cond_2
    new-instance v14, LX/0ml;

    const/16 v15, 0x32

    invoke-direct {v14, v15}, LX/0ml;-><init>(I)V

    goto :goto_2
.end method

.method private static a(LX/0Zk;)LX/0Zk;
    .locals 1
    .param p0    # LX/0Zk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zk",
            "<",
            "LX/0mj;",
            ">;)",
            "LX/0Zk",
            "<",
            "LX/0mj;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 132790
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance p0, LX/0Zi;

    const/4 v0, 0x6

    invoke-direct {p0, v0}, LX/0Zi;-><init>(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 0
    .param p0    # Ljava/lang/Class;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;)",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/analytics2/logger/HandlerThreadFactory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132791
    if-nez p0, :cond_0

    const-class p0, Lcom/facebook/analytics2/logger/DefaultHandlerThreadFactory;

    :cond_0
    return-object p0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 132792
    if-nez p0, :cond_0

    .line 132793
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 132794
    :cond_0
    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132795
    iget-object v0, p0, LX/0mh;->a:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mj;

    .line 132796
    if-nez v0, :cond_0

    .line 132797
    new-instance v0, LX/0mj;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/0mj;-><init>(Z)V

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    .line 132798
    iput-object v1, v0, LX/0mj;->b:LX/0mh;

    .line 132799
    iput-object v2, v0, LX/0mj;->c:Ljava/lang/String;

    .line 132800
    iput-object v3, v0, LX/0mj;->d:Ljava/lang/String;

    .line 132801
    iput-object v4, v0, LX/0mj;->e:LX/0mq;

    .line 132802
    iput-boolean v5, v0, LX/0mj;->f:Z

    .line 132803
    iget-object p0, v1, LX/0mh;->g:LX/0Zh;

    invoke-virtual {p0}, LX/0Zh;->b()LX/0n9;

    move-result-object p0

    iput-object p0, v0, LX/0mj;->r:LX/0n9;

    .line 132804
    iget-object p0, v0, LX/0mj;->r:LX/0n9;

    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0nA;->a(LX/0nD;)V

    .line 132805
    invoke-static {v0}, LX/0mj;->j(LX/0mj;)V

    .line 132806
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/0mj;->t:Z

    .line 132807
    return-object v0
.end method


# virtual methods
.method public final a(LX/1tk;)LX/0mj;
    .locals 6

    .prologue
    .line 132808
    iget-object v1, p1, LX/1tk;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1tk;->b:Ljava/lang/String;

    iget-boolean v3, p1, LX/1tk;->c:Z

    iget-object v4, p1, LX/1tk;->d:LX/0mq;

    iget-boolean v5, p1, LX/1tk;->e:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/0mj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132809
    invoke-direct {p0, p1, p2, p3, p4}, LX/0mh;->b(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;

    move-result-object v0

    .line 132810
    invoke-virtual {v0}, LX/0mj;->a()Z

    .line 132811
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/0mj;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 132812
    iget-object v0, p0, LX/0mh;->d:LX/0mP;

    .line 132813
    iget-object v1, v0, LX/0mP;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zm;

    invoke-virtual {v1, p2, p3}, LX/0Zm;->a(Ljava/lang/String;Z)Z

    move-result v1

    move v0, v1

    .line 132814
    if-eqz v0, :cond_0

    .line 132815
    invoke-direct {p0, p1, p2, p4, p5}, LX/0mh;->b(Ljava/lang/String;Ljava/lang/String;LX/0mq;Z)LX/0mj;

    move-result-object v0

    .line 132816
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0mh;->n:LX/0mj;

    goto :goto_0
.end method
