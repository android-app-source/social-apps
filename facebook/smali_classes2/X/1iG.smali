.class public LX/1iG;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 297637
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 297638
    return-void
.end method

.method public static a(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/zero/sdk/annotations/UseSessionlessBackupRewriteRules;
    .end annotation

    .prologue
    .line 297639
    const/16 v0, 0x4a

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/zero/sdk/annotations/UseBackupRewriteRulesGatekeeper;
    .end annotation

    .prologue
    .line 297640
    const/16 v0, 0x67f

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0Uh;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/zero/annotations/DisableZeroTokenBootstrapGatekeeper;
    .end annotation

    .prologue
    .line 297641
    const/16 v0, 0x332

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 297642
    return-void
.end method
