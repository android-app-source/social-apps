.class public LX/1ft;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fy;


# instance fields
.field private a:Lcom/facebook/common/perftest/PerfTestConfig;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lt;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/common/perftest/PerfTestConfig;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0Ot",
            "<",
            "LX/3lt;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 292401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292402
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ft;->d:Z

    .line 292403
    iput-object p1, p0, LX/1ft;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 292404
    iput-object p2, p0, LX/1ft;->b:LX/0Ot;

    .line 292405
    iput-object p3, p0, LX/1ft;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 292406
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 3

    .prologue
    const v2, 0xa0096

    .line 292407
    iget-object v0, p0, LX/1ft;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3lt;

    invoke-virtual {v0, p2}, LX/3lt;->a(I)V

    .line 292408
    packed-switch p2, :pswitch_data_0

    .line 292409
    :cond_0
    :goto_0
    return-void

    .line 292410
    :pswitch_0
    iget-boolean v0, p0, LX/1ft;->d:Z

    if-eqz v0, :cond_0

    .line 292411
    iget-object v0, p0, LX/1ft;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 292412
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1ft;->d:Z

    goto :goto_0

    .line 292413
    :pswitch_1
    iget-boolean v0, p0, LX/1ft;->d:Z

    if-nez v0, :cond_0

    .line 292414
    iget-object v0, p0, LX/1ft;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 292415
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1ft;->d:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/0g8;III)V
    .locals 0

    .prologue
    .line 292416
    return-void
.end method
