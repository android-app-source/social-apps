.class public LX/1lv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 312643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSuggestedContentConnection;->a()LX/0Px;

    move-result-object v0

    .line 312644
    :goto_0
    return-object v0

    .line 312645
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312646
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCelebrationsFeedUnit;->n()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;

    move-result-object v0

    .line 312638
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCommerceSaleStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 312639
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312640
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 312632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;->a()LX/0Px;

    move-result-object v0

    .line 312633
    :goto_0
    return-object v0

    .line 312634
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312635
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;->n()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    .line 312627
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 312628
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312629
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->s()LX/0Px;

    move-result-object v0

    .line 312624
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312625
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->n()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->n()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)LX/0Px;
    .locals 6

    .prologue
    .line 312614
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 312615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v0

    .line 312616
    if-eqz v0, :cond_1

    .line 312617
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;

    .line 312618
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v5

    invoke-static {v5}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 312619
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312620
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 312621
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312599
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 312600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->k()LX/0Px;

    move-result-object v3

    .line 312601
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    .line 312602
    const/4 v6, 0x0

    .line 312603
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_0
    move v5, v6

    .line 312604
    :goto_1
    move v5, v5

    .line 312605
    if-eqz v5, :cond_1

    .line 312606
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312607
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 312608
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 312609
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v7, v6

    :goto_2
    if-ge v7, v9, :cond_4

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    .line 312610
    sget-object p0, LX/4ZY;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v5

    aget v5, p0, v5

    packed-switch v5, :pswitch_data_0

    move v5, v6

    .line 312611
    goto :goto_1

    .line 312612
    :pswitch_0
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_2

    .line 312613
    :cond_4
    const/4 v5, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;

    move-result-object v0

    .line 312596
    if-nez v0, :cond_0

    .line 312597
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312598
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v0

    .line 312584
    if-nez v0, :cond_0

    .line 312585
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312586
    :goto_0
    return-object v0

    .line 312587
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v2

    .line 312588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->x()LX/0Px;

    move-result-object v3

    .line 312589
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 312590
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 312591
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    if-eqz v6, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 312592
    :cond_1
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312593
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 312594
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    .line 312648
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 312649
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312650
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    .line 312509
    if-nez v0, :cond_0

    .line 312510
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312511
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v0

    .line 312513
    if-nez v0, :cond_0

    .line 312514
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312515
    :goto_0
    return-object v0

    .line 312516
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v2

    .line 312517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->A()LX/0Px;

    move-result-object v3

    .line 312518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 312519
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 312520
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    if-eqz v6, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 312521
    :cond_1
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312522
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 312523
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    .line 312525
    if-nez v0, :cond_0

    .line 312526
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312527
    :goto_0
    return-object v0

    .line 312528
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v2

    .line 312529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->R()LX/0Px;

    move-result-object v3

    .line 312530
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 312531
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;

    .line 312532
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 312533
    :cond_1
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312534
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 312535
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouShouldFollowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLPeopleToFollowConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPeopleToFollowConnection;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->r()LX/0Px;

    move-result-object v2

    .line 312538
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312539
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v0

    .line 312540
    :goto_0
    return-object v0

    .line 312541
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 312542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 312543
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 312544
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312545
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 312546
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312547
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312548
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312549
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;

    move-result-object v0

    .line 312551
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 312552
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312553
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLResearchPollMultipleChoiceQuestion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;

    move-result-object v0

    .line 312555
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312556
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312557
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLResearchPollSurvey;->m()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    .line 312559
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 312560
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312561
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnitItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSocialWifiFeedUnit;->m()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 312564
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312565
    :goto_0
    return-object v0

    .line 312566
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopicCustomizationStory;->k()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsConnection;->a()LX/0Px;

    move-result-object v2

    .line 312567
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 312568
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsEdge;

    .line 312569
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsEdge;->a()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 312570
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOptionsEdge;->a()Lcom/facebook/graphql/model/GraphQLTrueTopicFeedOption;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 312571
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 312572
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 312574
    if-nez v0, :cond_0

    .line 312575
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312576
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 312578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoChainingFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSuggestedVideoConnection;->a()LX/0Px;

    move-result-object v0

    .line 312579
    :goto_0
    return-object v0

    .line 312580
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 312581
    goto :goto_0
.end method
