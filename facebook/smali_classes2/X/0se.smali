.class public LX/0se;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/0se;


# instance fields
.field private final a:LX/0sa;

.field private final b:LX/0rq;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 152379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152380
    iput-object p1, p0, LX/0se;->a:LX/0sa;

    .line 152381
    iput-object p2, p0, LX/0se;->b:LX/0rq;

    .line 152382
    return-void
.end method

.method public static a(LX/0QB;)LX/0se;
    .locals 5

    .prologue
    .line 152383
    sget-object v0, LX/0se;->c:LX/0se;

    if-nez v0, :cond_1

    .line 152384
    const-class v1, LX/0se;

    monitor-enter v1

    .line 152385
    :try_start_0
    sget-object v0, LX/0se;->c:LX/0se;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 152386
    if-eqz v2, :cond_0

    .line 152387
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 152388
    new-instance p0, LX/0se;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-direct {p0, v3, v4}, LX/0se;-><init>(LX/0sa;LX/0rq;)V

    .line 152389
    move-object v0, p0

    .line 152390
    sput-object v0, LX/0se;->c:LX/0se;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 152392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152393
    :cond_1
    sget-object v0, LX/0se;->c:LX/0se;

    return-object v0

    .line 152394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 152395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/0se;LX/0gW;LX/0wF;)LX/0gW;
    .locals 2
    .param p1    # LX/0gW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 152396
    const-string v0, "media_type"

    if-nez p2, :cond_0

    iget-object v1, p0, LX/0se;->b:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->a()LX/0wF;

    move-result-object p2

    :cond_0
    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 152397
    const-string v0, "profile_pic_media_type"

    iget-object v1, p0, LX/0se;->b:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->b()LX/0wF;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 152398
    const-string v0, "size_style"

    invoke-static {}, LX/0rq;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 152399
    const-string v0, "image_high_width"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152400
    const-string v0, "image_high_height"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    .line 152401
    iget-object p2, v1, LX/0sa;->a:Ljava/lang/Integer;

    move-object v1, p2

    .line 152402
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152403
    const-string v0, "image_medium_width"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->x()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152404
    const-string v0, "image_medium_height"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    .line 152405
    iget-object p2, v1, LX/0sa;->a:Ljava/lang/Integer;

    move-object v1, p2

    .line 152406
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152407
    const-string v0, "image_low_width"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->w()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152408
    const-string v0, "image_low_height"

    iget-object v1, p0, LX/0se;->a:LX/0sa;

    .line 152409
    iget-object p0, v1, LX/0sa;->a:Ljava/lang/Integer;

    move-object v1, p0

    .line 152410
    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 152411
    return-object p1
.end method


# virtual methods
.method public final a(LX/0gW;)LX/0gW;
    .locals 1

    .prologue
    .line 152412
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/0se;->b(LX/0se;LX/0gW;LX/0wF;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0gW;LX/0wF;)LX/0gW;
    .locals 1
    .param p2    # LX/0wF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 152413
    invoke-static {p0, p1, p2}, LX/0se;->b(LX/0se;LX/0gW;LX/0wF;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
