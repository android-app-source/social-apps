.class public LX/1MU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/1MU;


# instance fields
.field private final b:LX/0W3;

.field private c:Lcom/facebook/proxygen/PersistentSSLCacheSettings;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235631
    const-class v0, LX/1MU;

    sput-object v0, LX/1MU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 235650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235651
    iput-object p1, p0, LX/1MU;->b:LX/0W3;

    .line 235652
    return-void
.end method

.method public static a(LX/0QB;)LX/1MU;
    .locals 4

    .prologue
    .line 235637
    sget-object v0, LX/1MU;->d:LX/1MU;

    if-nez v0, :cond_1

    .line 235638
    const-class v1, LX/1MU;

    monitor-enter v1

    .line 235639
    :try_start_0
    sget-object v0, LX/1MU;->d:LX/1MU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 235640
    if-eqz v2, :cond_0

    .line 235641
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 235642
    new-instance p0, LX/1MU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/1MU;-><init>(LX/0W3;)V

    .line 235643
    move-object v0, p0

    .line 235644
    sput-object v0, LX/1MU;->d:LX/1MU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235645
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 235646
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 235647
    :cond_1
    sget-object v0, LX/1MU;->d:LX/1MU;

    return-object v0

    .line 235648
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 235649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/proxygen/PersistentSSLCacheSettings;)V
    .locals 0

    .prologue
    .line 235635
    iput-object p1, p0, LX/1MU;->c:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 235636
    return-void
.end method

.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235653
    iget-object v0, p0, LX/1MU;->c:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1MU;->c:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    iget-object v0, v0, Lcom/facebook/proxygen/PersistentSSLCacheSettings;->filename:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 235654
    :cond_0
    const/4 v0, 0x0

    .line 235655
    :goto_0
    return-object v0

    .line 235656
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 235657
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/1MU;->c:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    iget-object v2, v2, Lcom/facebook/proxygen/PersistentSSLCacheSettings;->filename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235658
    new-instance v2, Ljava/io/File;

    const-string p0, "fb_liger_dns_cache_json.txt"

    invoke-direct {v2, p1, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235659
    invoke-static {v1, v2}, LX/78s;->a(Ljava/io/File;Ljava/io/File;)V

    .line 235660
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    move-object v1, v2

    .line 235661
    const-string v2, "fb_liger_dns_cache_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235662
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 235663
    :catch_0
    move-exception v0

    .line 235664
    sget-object v1, LX/1MU;->a:Ljava/lang/Class;

    const-string v2, "Exception saving liger trace"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235665
    throw v0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235634
    const/4 v0, 0x0

    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 235633
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 235632
    iget-object v0, p0, LX/1MU;->b:LX/0W3;

    sget-wide v2, LX/0X5;->aQ:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
