.class public final LX/1SN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1SB;

.field public final synthetic b:LX/1SC;


# direct methods
.method public constructor <init>(LX/1SC;LX/1SB;)V
    .locals 0

    .prologue
    .line 247857
    iput-object p1, p0, LX/1SN;->b:LX/1SC;

    iput-object p2, p0, LX/1SN;->a:LX/1SB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 247867
    new-instance v0, Ljava/lang/Error;

    invoke-direct {v0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 247858
    check-cast p1, Ljava/lang/Boolean;

    .line 247859
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1SN;->b:LX/1SC;

    iget-boolean v0, v0, LX/1SC;->d:Z

    if-nez v0, :cond_0

    .line 247860
    iget-object v1, p0, LX/1SN;->b:LX/1SC;

    iget-object v0, p0, LX/1SN;->a:LX/1SB;

    .line 247861
    iget-object p1, v0, LX/1SB;->a:LX/1D8;

    iget-object p1, p1, LX/1D8;->a:LX/0Or;

    move-object v0, p1

    .line 247862
    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    .line 247863
    iput-object v0, v1, LX/1SC;->c:LX/1Cw;

    .line 247864
    iget-object v0, p0, LX/1SN;->b:LX/1SC;

    iget-object v0, v0, LX/1SC;->c:LX/1Cw;

    iget-object v1, p0, LX/1SN;->b:LX/1SC;

    iget-object v1, v1, LX/1SC;->a:LX/1SD;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 247865
    iget-object v0, p0, LX/1SN;->b:LX/1SC;

    invoke-static {v0}, LX/1SC;->a(LX/1SC;)V

    .line 247866
    :cond_0
    return-void
.end method
