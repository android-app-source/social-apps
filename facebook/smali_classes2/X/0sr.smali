.class public interface abstract LX/0sr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 153276
    new-instance v0, LX/0U1;

    const-string v1, "rowid"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->a:LX/0U1;

    .line 153277
    new-instance v0, LX/0U1;

    const-string v1, "query_id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->b:LX/0U1;

    .line 153278
    new-instance v0, LX/0U1;

    const-string v1, "persisted_fb_id"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->c:LX/0U1;

    .line 153279
    new-instance v0, LX/0U1;

    const-string v1, "user_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->d:LX/0U1;

    .line 153280
    new-instance v0, LX/0U1;

    const-string v1, "timestamp"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->e:LX/0U1;

    .line 153281
    new-instance v0, LX/0U1;

    const-string v1, "class"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->f:LX/0U1;

    .line 153282
    new-instance v0, LX/0U1;

    const-string v1, "model_type"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->g:LX/0U1;

    .line 153283
    new-instance v0, LX/0U1;

    const-string v1, "flags"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->h:LX/0U1;

    .line 153284
    new-instance v0, LX/0U1;

    const-string v1, "exports"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->i:LX/0U1;

    .line 153285
    new-instance v0, LX/0U1;

    const-string v1, "max_age_ms"

    const-string v2, "INTEGER NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->j:LX/0U1;

    .line 153286
    new-instance v0, LX/0U1;

    const-string v1, "confirmed_model"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->k:LX/0U1;

    .line 153287
    new-instance v0, LX/0U1;

    const-string v1, "optimistic_model"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->l:LX/0U1;

    .line 153288
    new-instance v0, LX/0U1;

    const-string v1, "is_consistent"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/0sr;->m:LX/0U1;

    return-void
.end method
