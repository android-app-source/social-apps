.class public LX/1QS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pk;


# instance fields
.field private a:LX/1SX;

.field private final b:LX/1Pf;

.field private final c:LX/1QT;


# direct methods
.method public constructor <init>(LX/1Pf;LX/1QT;)V
    .locals 1
    .param p1    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 244613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244614
    const/4 v0, 0x0

    iput-object v0, p0, LX/1QS;->a:LX/1SX;

    .line 244615
    iput-object p1, p0, LX/1QS;->b:LX/1Pf;

    .line 244616
    iput-object p2, p0, LX/1QS;->c:LX/1QT;

    .line 244617
    return-void
.end method


# virtual methods
.method public final declared-synchronized e()LX/1SX;
    .locals 2

    .prologue
    .line 244609
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1QS;->a:LX/1SX;

    if-nez v0, :cond_0

    .line 244610
    iget-object v0, p0, LX/1QS;->c:LX/1QT;

    iget-object v1, p0, LX/1QS;->b:LX/1Pf;

    invoke-virtual {v0, v1}, LX/1QT;->a(LX/1Pf;)LX/1SX;

    move-result-object v0

    iput-object v0, p0, LX/1QS;->a:LX/1SX;

    .line 244611
    :cond_0
    iget-object v0, p0, LX/1QS;->a:LX/1SX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 244612
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
