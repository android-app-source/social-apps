.class public LX/10v;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 169506
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 169507
    return-void
.end method

.method public static a(LX/10u;)Ljava/lang/Boolean;
    .locals 3
    .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 169501
    const/4 v2, 0x0

    .line 169502
    iget-object v0, p0, LX/10u;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 169503
    iget-object v0, p0, LX/10u;->c:LX/0Uh;

    const/16 v1, 0x43

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 169504
    :goto_0
    move v0, v0

    .line 169505
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/10u;->b:LX/0Uh;

    const/16 v1, 0x518

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2Dg;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/aldrin/status/annotations/ShouldBeAldrinUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 169494
    invoke-virtual {p0}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    .line 169495
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 169496
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/2Dg;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinUser;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 169498
    invoke-virtual {p0}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    .line 169499
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 169500
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 169497
    return-void
.end method
