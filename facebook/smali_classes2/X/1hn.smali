.class public LX/1hn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1hn;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/appstate/handler/IsAppInBackground;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297070
    iput-object p1, p0, LX/1hn;->a:LX/0Or;

    .line 297071
    return-void
.end method

.method public static a(LX/0QB;)LX/1hn;
    .locals 4

    .prologue
    .line 297072
    sget-object v0, LX/1hn;->b:LX/1hn;

    if-nez v0, :cond_1

    .line 297073
    const-class v1, LX/1hn;

    monitor-enter v1

    .line 297074
    :try_start_0
    sget-object v0, LX/1hn;->b:LX/1hn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 297075
    if-eqz v2, :cond_0

    .line 297076
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 297077
    new-instance v3, LX/1hn;

    const/16 p0, 0x1463

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1hn;-><init>(LX/0Or;)V

    .line 297078
    move-object v0, v3

    .line 297079
    sput-object v0, LX/1hn;->b:LX/1hn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297080
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 297081
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297082
    :cond_1
    sget-object v0, LX/1hn;->b:LX/1hn;

    return-object v0

    .line 297083
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 297084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
