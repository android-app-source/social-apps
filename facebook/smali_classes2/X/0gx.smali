.class public LX/0gx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:I

.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserTrustedTester;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation runtime Lcom/facebook/video/videohome/annotations/IsVideoHomeDebugOverlayEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/192;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Yl;",
            ">;"
        }
    .end annotation
.end field

.field public volatile i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Yi;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hB;",
            ">;"
        }
    .end annotation
.end field

.field public volatile k:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gy;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public m:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0go;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/0hL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/18q;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iS;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0hO;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/0fd;

.field public final w:Lcom/facebook/widget/CustomLinearLayout;

.field private x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation
.end field

.field public y:Z

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0fd;Landroid/view/View;)V
    .locals 2
    .param p1    # LX/0fd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 114351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114352
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114353
    iput-object v0, p0, LX/0gx;->q:LX/0Ot;

    .line 114354
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114355
    iput-object v0, p0, LX/0gx;->r:LX/0Ot;

    .line 114356
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114357
    iput-object v0, p0, LX/0gx;->s:LX/0Ot;

    .line 114358
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 114359
    iput-object v0, p0, LX/0gx;->t:LX/0Ot;

    .line 114360
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/0gx;->u:Ljava/util/Map;

    .line 114361
    iput-object p1, p0, LX/0gx;->v:LX/0fd;

    .line 114362
    check-cast p2, Lcom/facebook/widget/CustomLinearLayout;

    iput-object p2, p0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    .line 114363
    return-void
.end method

.method public static a(LX/0gx;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;Landroid/content/Context;LX/0go;LX/0hL;LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gx;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/view/LayoutInflater;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0y2;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "LX/192;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Yl;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Yi;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hB;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gy;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "Landroid/content/Context;",
            "LX/0go;",
            "LX/0hL;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/18q;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114350
    iput-object p1, p0, LX/0gx;->a:LX/0Or;

    iput-object p2, p0, LX/0gx;->b:LX/0Or;

    iput-object p3, p0, LX/0gx;->c:LX/0Or;

    iput-object p4, p0, LX/0gx;->d:LX/0Or;

    iput-object p5, p0, LX/0gx;->e:LX/0Or;

    iput-object p6, p0, LX/0gx;->f:LX/0Or;

    iput-object p7, p0, LX/0gx;->g:LX/0Or;

    iput-object p8, p0, LX/0gx;->h:LX/0Or;

    iput-object p9, p0, LX/0gx;->i:LX/0Or;

    iput-object p10, p0, LX/0gx;->j:LX/0Or;

    iput-object p11, p0, LX/0gx;->k:LX/0Or;

    iput-object p12, p0, LX/0gx;->l:LX/0Or;

    iput-object p13, p0, LX/0gx;->m:Landroid/content/Context;

    iput-object p14, p0, LX/0gx;->n:LX/0go;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/0gx;->o:LX/0hL;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/0gx;->p:LX/0Uh;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/0gx;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/0gx;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/0gx;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/0gx;->t:LX/0Ot;

    return-void
.end method

.method public static b(LX/0gx;Lcom/facebook/apptab/state/TabTag;)V
    .locals 6

    .prologue
    .line 114315
    const-string v0, "addTab(%s)"

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    const v2, -0x4d3caf43

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 114316
    :try_start_0
    iget-object v0, p0, LX/0gx;->p:LX/0Uh;

    const/16 v1, 0x3bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 114317
    :cond_0
    new-instance v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    iget-object v1, p0, LX/0gx;->m:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/apptab/ui/DownloadableImageTabView;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    .line 114318
    :goto_0
    iget v0, p1, Lcom/facebook/apptab/state/TabTag;->c:I

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setTabIconImageResource(I)V

    .line 114319
    iget-boolean v0, p1, Lcom/facebook/apptab/state/TabTag;->d:Z

    iput-boolean v0, v1, LX/0hO;->d:Z

    .line 114320
    invoke-virtual {v1}, LX/0hO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p1, Lcom/facebook/apptab/state/TabTag;->j:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/0hO;->i:Ljava/lang/CharSequence;

    .line 114321
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/0gx;->k(LX/0gx;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114322
    new-instance v0, LX/HeY;

    invoke-direct {v0, p0}, LX/HeY;-><init>(LX/0gx;)V

    invoke-virtual {v1, v0}, LX/0hO;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114323
    :cond_1
    instance-of v0, p1, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/0gx;->k(LX/0gx;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114324
    new-instance v0, LX/HeZ;

    invoke-direct {v0, p0}, LX/HeZ;-><init>(LX/0gx;)V

    invoke-virtual {v1, v0}, LX/0hO;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114325
    :cond_2
    sget-object v0, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, LX/0gx;->k(LX/0gx;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114326
    new-instance v0, LX/Hea;

    invoke-direct {v0, p0}, LX/Hea;-><init>(LX/0gx;)V

    invoke-virtual {v1, v0}, LX/0hO;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114327
    :cond_3
    sget-object v0, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, LX/0gx;->k(LX/0gx;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114328
    iget-object v0, p0, LX/0gx;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 114329
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114330
    new-instance v0, LX/2r1;

    invoke-direct {v0, p0}, LX/2r1;-><init>(LX/0gx;)V

    invoke-virtual {v1, v0}, LX/0hO;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114331
    :cond_4
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114332
    iget-object v0, p0, LX/0gx;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y2;

    .line 114333
    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 114334
    if-eqz v0, :cond_5

    .line 114335
    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    .line 114336
    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_7

    .line 114337
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsJapanTab;->l:Lcom/facebook/notifications/tab/NotificationsJapanTab;

    iget v0, v0, Lcom/facebook/apptab/state/TabTag;->c:I

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setTabIconImageResource(I)V

    .line 114338
    :cond_5
    :goto_1
    iget v0, p1, Lcom/facebook/apptab/state/TabTag;->k:I

    invoke-virtual {v1, v0}, LX/0hO;->setId(I)V

    .line 114339
    new-instance v0, LX/0y0;

    invoke-direct {v0, p0, p1}, LX/0y0;-><init>(LX/0gx;Lcom/facebook/apptab/state/TabTag;)V

    invoke-virtual {v1, v0}, LX/0hO;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114340
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 114341
    invoke-virtual {p0, p1, v1}, LX/0gx;->a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V

    .line 114342
    iget-object v2, p0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114343
    iget-object v0, p0, LX/0gx;->u:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114344
    const v0, 0x3a3b412

    invoke-static {v0}, LX/02m;->a(I)V

    .line 114345
    return-void

    .line 114346
    :cond_6
    :try_start_1
    new-instance v0, LX/0hO;

    iget-object v1, p0, LX/0gx;->m:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0hO;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    goto/16 :goto_0

    .line 114347
    :cond_7
    const-wide/high16 v4, -0x3fc2000000000000L    # -30.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_5

    .line 114348
    sget-object v0, Lcom/facebook/notifications/tab/NotificationsEastTab;->l:Lcom/facebook/notifications/tab/NotificationsEastTab;

    iget v0, v0, Lcom/facebook/apptab/state/TabTag;->c:I

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setTabIconImageResource(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 114349
    :catchall_0
    move-exception v0

    const v1, 0x4dc37390    # 4.09891328E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static k(LX/0gx;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 114310
    iget-object v0, p0, LX/0gx;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 114311
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 114312
    :goto_0
    return v0

    .line 114313
    :cond_0
    iget-object v0, p0, LX/0gx;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 114314
    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v0, v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0hO;
    .locals 1

    .prologue
    .line 114309
    iget-object v0, p0, LX/0gx;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hO;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 114236
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0gx;->a(IF)V

    .line 114237
    return-void
.end method

.method public final a(IF)V
    .locals 5

    .prologue
    .line 114277
    iget-object v0, p0, LX/0gx;->v:LX/0fd;

    .line 114278
    iget-object v1, v0, LX/0fd;->c:LX/0h2;

    if-eqz v1, :cond_1

    .line 114279
    iget-object v1, v0, LX/0fd;->c:LX/0h2;

    .line 114280
    const/4 v3, -0x1

    .line 114281
    iget v2, v1, LX/0h2;->t:I

    if-ne v2, v3, :cond_0

    iget v2, v1, LX/0h2;->u:I

    if-eq v2, v3, :cond_f

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 114282
    if-nez v2, :cond_a

    .line 114283
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 114284
    iget-object v0, p0, LX/0gx;->o:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sub-int v0, v3, p1

    add-int/lit8 p1, v0, -0x1

    .line 114285
    :cond_2
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_9

    .line 114286
    iget-object v0, p0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0hO;

    move-object v1, v0

    .line 114287
    instance-of v0, v1, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;

    .line 114288
    iget-boolean v4, v0, Lcom/facebook/apptab/ui/DownloadableImageTabView;->k:Z

    move v0, v4

    .line 114289
    if-eqz v0, :cond_4

    .line 114290
    :cond_3
    if-ne v2, p1, :cond_5

    .line 114291
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setSelectionPercentage(F)V

    .line 114292
    :cond_4
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 114293
    :cond_5
    add-int/lit8 v0, p1, 0x1

    if-ne v2, v0, :cond_6

    add-int/lit8 v0, p1, 0x1

    if-ge v0, v3, :cond_6

    iget-object v0, p0, LX/0gx;->o:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    add-int/lit8 v0, p1, -0x1

    if-ne v2, v0, :cond_8

    add-int/lit8 v0, p1, -0x1

    if-ltz v0, :cond_8

    iget-object v0, p0, LX/0gx;->o:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 114294
    :cond_7
    invoke-virtual {v1, p2}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setSelectionPercentage(F)V

    goto :goto_3

    .line 114295
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/apptab/glyph/BadgableGlyphView;->setSelectionPercentage(F)V

    goto :goto_3

    .line 114296
    :cond_9
    return-void

    .line 114297
    :cond_a
    iget-object v2, v1, LX/0h2;->r:LX/0fd;

    invoke-virtual {v2}, LX/0fd;->i()I

    move-result v3

    .line 114298
    iget-object v2, v1, LX/0h2;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1vx;

    .line 114299
    invoke-virtual {v2}, LX/1vx;->a()Z

    move-result v4

    .line 114300
    if-nez v4, :cond_b

    .line 114301
    iget-object v2, v1, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iget-object v0, v1, LX/0h2;->r:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->b()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    iget v0, v0, Lcom/facebook/apptab/state/TabTag;->j:I

    invoke-virtual {v2, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 114302
    iget-object v0, v1, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iget v2, v1, LX/0h2;->t:I

    if-ne v3, v2, :cond_c

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->c(Z)V

    .line 114303
    :cond_b
    if-eqz v4, :cond_d

    iget v2, v1, LX/0h2;->t:I

    if-ne v3, v2, :cond_d

    .line 114304
    iget-object v2, v1, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->g()V

    goto/16 :goto_1

    .line 114305
    :cond_c
    const/4 v2, 0x0

    goto :goto_4

    .line 114306
    :cond_d
    iget v2, v1, LX/0h2;->u:I

    if-ne v3, v2, :cond_e

    .line 114307
    iget-object v2, v1, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->h()V

    goto/16 :goto_1

    .line 114308
    :cond_e
    iget-object v2, v1, LX/0h2;->s:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v2}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->f()V

    goto/16 :goto_1

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/apptab/state/TabTag;LX/0hO;)V
    .locals 3

    .prologue
    .line 114364
    iget-object v0, p0, LX/0gx;->m:Landroid/content/Context;

    iget v1, p1, Lcom/facebook/apptab/state/TabTag;->j:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v0}, LX/0fd;->i()I

    move-result v0

    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p2, v1, v0}, LX/0hO;->a(Ljava/lang/CharSequence;Z)V

    .line 114365
    return-void

    .line 114366
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 114266
    if-nez p1, :cond_1

    move v1, v3

    .line 114267
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 114268
    :goto_1
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 114269
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114270
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 114271
    :cond_2
    iget-object v0, p0, LX/0gx;->n:LX/0go;

    invoke-virtual {v0, p1}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v0, :cond_4

    .line 114272
    :goto_2
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 114273
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v0, :cond_3

    move v1, v2

    .line 114274
    goto :goto_0

    .line 114275
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v1, v3

    .line 114276
    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/apptab/state/TabTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114263
    iget-object v0, p0, LX/0gx;->x:LX/0Px;

    if-nez v0, :cond_0

    .line 114264
    iget-object v0, p0, LX/0gx;->n:LX/0go;

    invoke-virtual {v0}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    iput-object v0, p0, LX/0gx;->x:LX/0Px;

    .line 114265
    :cond_0
    iget-object v0, p0, LX/0gx;->x:LX/0Px;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 114255
    :try_start_0
    iget-object v0, p0, LX/0gx;->n:LX/0go;

    invoke-virtual {v0, p1}, LX/0go;->a(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    .line 114256
    invoke-virtual {p0, p1}, LX/0gx;->a(Ljava/lang/String;)LX/0hO;

    move-result-object v1

    .line 114257
    iget-object v2, p0, LX/0gx;->m:Landroid/content/Context;

    iget v3, v0, Lcom/facebook/apptab/state/TabTag;->j:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v3}, LX/0fd;->i()I

    move-result v3

    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne v3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0hO;->a(Ljava/lang/CharSequence;Z)V
    :try_end_0
    .catch LX/0iF; {:try_start_0 .. :try_end_0} :catch_0

    .line 114258
    :goto_1
    return-void

    .line 114259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 114260
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 114261
    iget-object v0, p0, LX/0gx;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 114262
    const-string v2, "tab manager"

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final e()Lcom/facebook/apptab/state/TabTag;
    .locals 2

    .prologue
    .line 114254
    invoke-virtual {p0}, LX/0gx;->d()LX/0Px;

    move-result-object v0

    iget-object v1, p0, LX/0gx;->v:LX/0fd;

    invoke-virtual {v1}, LX/0fd;->i()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/apptab/state/TabTag;

    return-object v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 114249
    iget-boolean v0, p0, LX/0gx;->y:Z

    if-eqz v0, :cond_0

    .line 114250
    invoke-virtual {p0}, LX/0gx;->h()V

    .line 114251
    :goto_0
    return-void

    .line 114252
    :cond_0
    iget-object v0, p0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v1, LX/10T;

    invoke-direct {v1, p0}, LX/10T;-><init>(LX/0gx;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 114253
    iget-object v0, p0, LX/0gx;->w:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->invalidate()V

    goto :goto_0
.end method

.method public final h()V
    .locals 7

    .prologue
    const v2, 0xa007d

    .line 114238
    const/4 v0, 0x1

    .line 114239
    iput-boolean v0, p0, LX/0gx;->y:Z

    .line 114240
    iget-object v0, p0, LX/0gx;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yl;

    .line 114241
    const-string v1, "MainTabActivityChromeDrawn"

    invoke-virtual {v0, v2, v1}, LX/0Yl;->k(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114242
    const-string v1, "MainTabActivityChromeDrawn"

    invoke-virtual {v0, v2, v1}, LX/0Yl;->a(ILjava/lang/String;)LX/0Yl;

    .line 114243
    :cond_0
    iget-object v0, p0, LX/0gx;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/192;

    .line 114244
    iget-wide v3, v0, LX/192;->g:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    .line 114245
    iget-object v3, v0, LX/192;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iput-wide v3, v0, LX/192;->g:J

    .line 114246
    :cond_1
    iget-object v0, p0, LX/0gx;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18q;

    .line 114247
    iget-object v1, v0, LX/18q;->a:LX/0Xl;

    const-string v2, "com.facebook.apptab.ui.MAINTAB_CHROME_DRAWN"

    invoke-interface {v1, v2}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 114248
    return-void
.end method
