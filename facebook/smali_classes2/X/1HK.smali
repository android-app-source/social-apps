.class public LX/1HK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/ContentResolver;

.field public b:Landroid/content/res/Resources;

.field public c:Landroid/content/res/AssetManager;

.field public final d:LX/1FQ;

.field public final e:LX/1GL;

.field public final f:LX/1Gv;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:LX/1Ft;

.field public final l:LX/1Fj;

.field public final m:LX/1HY;

.field public final n:LX/1HY;

.field public final o:LX/1Ib;

.field public final p:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/1Ao;

.field private final s:Landroid/content/Context;

.field public t:LX/1IZ;

.field private final u:I

.field public final v:LX/1FZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1FQ;LX/1GL;LX/1Gv;ZZZLX/1Ft;LX/1Fj;LX/1Fh;LX/1Fh;LX/1HY;LX/1HY;LX/1IZ;LX/1Ao;LX/1FZ;ZI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/imagepipeline/memory/ByteArrayPool;",
            "LX/1GL;",
            "Lcom/facebook/imagepipeline/decoder/ProgressiveJpegConfig;",
            "ZZZ",
            "LX/1Ft;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBufferFactory;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;",
            "LX/1HY;",
            "LX/1HY;",
            "LX/1IZ;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1FZ;",
            "ZI)V"
        }
    .end annotation

    .prologue
    .line 226998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226999
    iput-object p1, p0, LX/1HK;->s:Landroid/content/Context;

    .line 227000
    move/from16 v0, p18

    iput v0, p0, LX/1HK;->u:I

    .line 227001
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, LX/1HK;->a:Landroid/content/ContentResolver;

    .line 227002
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, LX/1HK;->b:Landroid/content/res/Resources;

    .line 227003
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    iput-object v4, p0, LX/1HK;->c:Landroid/content/res/AssetManager;

    .line 227004
    iput-object p2, p0, LX/1HK;->d:LX/1FQ;

    .line 227005
    iput-object p3, p0, LX/1HK;->e:LX/1GL;

    .line 227006
    iput-object p4, p0, LX/1HK;->f:LX/1Gv;

    .line 227007
    iput-boolean p5, p0, LX/1HK;->g:Z

    .line 227008
    iput-boolean p6, p0, LX/1HK;->h:Z

    .line 227009
    iput-boolean p7, p0, LX/1HK;->j:Z

    .line 227010
    iput-object p8, p0, LX/1HK;->k:LX/1Ft;

    .line 227011
    iput-object p9, p0, LX/1HK;->l:LX/1Fj;

    .line 227012
    iput-object p10, p0, LX/1HK;->q:LX/1Fh;

    .line 227013
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1HK;->p:LX/1Fh;

    .line 227014
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1HK;->m:LX/1HY;

    .line 227015
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1HK;->n:LX/1HY;

    .line 227016
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1HK;->t:LX/1IZ;

    .line 227017
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1HK;->r:LX/1Ao;

    .line 227018
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1HK;->v:LX/1FZ;

    .line 227019
    move/from16 v0, p17

    iput-boolean v0, p0, LX/1HK;->i:Z

    .line 227020
    if-lez p18, :cond_0

    .line 227021
    new-instance v4, LX/31G;

    move-object/from16 v0, p12

    move-object/from16 v1, p13

    move-object/from16 v2, p15

    move/from16 v3, p18

    invoke-direct {v4, v0, v1, v2, v3}, LX/31G;-><init>(LX/1HY;LX/1HY;LX/1Ao;I)V

    iput-object v4, p0, LX/1HK;->o:LX/1Ib;

    .line 227022
    :goto_0
    return-void

    .line 227023
    :cond_0
    new-instance v4, LX/1Ia;

    move-object/from16 v0, p12

    move-object/from16 v1, p13

    move-object/from16 v2, p15

    invoke-direct {v4, v0, v1, v2}, LX/1Ia;-><init>(LX/1HY;LX/1HY;LX/1Ao;)V

    iput-object v4, p0, LX/1HK;->o:LX/1Ib;

    goto :goto_0
.end method

.method public static a(LX/1cF;)LX/1cO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cO;"
        }
    .end annotation

    .prologue
    .line 227024
    new-instance v0, LX/1cO;

    invoke-direct {v0, p0}, LX/1cO;-><init>(LX/1cF;)V

    return-object v0
.end method

.method public static a(LX/1cF;LX/1HH;)LX/1cT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1cF",
            "<TT;>;",
            "LX/1HH;",
            ")",
            "LX/1cT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 226996
    new-instance v0, LX/1cT;

    invoke-direct {v0, p0, p1}, LX/1cT;-><init>(LX/1cF;LX/1HH;)V

    return-object v0
.end method

.method public static m(LX/1cF;)LX/24o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1cF",
            "<TT;>;)",
            "LX/24o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 226997
    new-instance v0, LX/24o;

    invoke-direct {v0, p0}, LX/24o;-><init>(LX/1cF;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/1cF;ZZ)LX/1cP;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;ZZ)",
            "LX/1cP;"
        }
    .end annotation

    .prologue
    .line 226995
    new-instance v0, LX/1cP;

    iget-object v1, p0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v1}, LX/1Ft;->d()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, LX/1HK;->l:LX/1Fj;

    if-eqz p2, :cond_0

    iget-boolean v3, p0, LX/1HK;->g:Z

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    move-object v4, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/1cP;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;ZLX/1cF;Z)V

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final e()LX/4f1;
    .locals 4

    .prologue
    .line 226994
    new-instance v0, LX/4f1;

    iget-object v1, p0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v1}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, LX/1HK;->l:LX/1Fj;

    iget-object v3, p0, LX/1HK;->a:Landroid/content/ContentResolver;

    invoke-direct {v0, v1, v2, v3}, LX/4f1;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;)V

    return-object v0
.end method

.method public final f()LX/4f4;
    .locals 4

    .prologue
    .line 226993
    new-instance v0, LX/4f4;

    iget-object v1, p0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v1}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, LX/1HK;->l:LX/1Fj;

    iget-boolean v3, p0, LX/1HK;->i:Z

    invoke-direct {v0, v1, v2, v3}, LX/4f4;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    return-object v0
.end method

.method public final o(LX/1cF;)LX/1cH;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;)",
            "LX/1cH;"
        }
    .end annotation

    .prologue
    .line 226992
    new-instance v0, LX/1cH;

    iget-object v1, p0, LX/1HK;->k:LX/1Ft;

    invoke-interface {v1}, LX/1Ft;->d()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, LX/1HK;->l:LX/1Fj;

    invoke-direct {v0, v1, v2, p1}, LX/1cH;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;LX/1cF;)V

    return-object v0
.end method
