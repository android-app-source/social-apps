.class public LX/0zX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0v9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0v9",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0v9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v9",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 167373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167374
    iput-object p1, p0, LX/0zX;->a:LX/0v9;

    .line 167375
    return-void
.end method

.method public static b()LX/0zX;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167370
    new-instance v0, LX/0zX;

    .line 167371
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0v9;->b(Ljava/lang/Iterable;)LX/0v9;

    move-result-object v1

    move-object v1, v1

    .line 167372
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/0QK;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TOut:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0QK",
            "<TT;TTOut;>;)",
            "LX/0zX",
            "<TTOut;>;"
        }
    .end annotation

    .prologue
    .line 167366
    new-instance v0, LX/4V8;

    invoke-direct {v0, p0, p1}, LX/4V8;-><init>(LX/0zX;LX/0QK;)V

    .line 167367
    new-instance v1, LX/0zX;

    iget-object v2, p0, LX/0zX;->a:LX/0v9;

    .line 167368
    new-instance p0, LX/53F;

    invoke-direct {p0, v0}, LX/53F;-><init>(LX/4V7;)V

    invoke-static {v2, p0}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p0

    move-object v0, p0

    .line 167369
    invoke-direct {v1, v0}, LX/0zX;-><init>(LX/0v9;)V

    return-object v1
.end method

.method public final a(LX/0zX;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<TT;>;)",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167340
    new-instance v0, LX/0zX;

    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    iget-object v2, p1, LX/0zX;->a:LX/0v9;

    .line 167341
    invoke-static {v1, v2}, LX/0v9;->a(LX/0v9;LX/0v9;)LX/0v9;

    move-result-object p0

    move-object v1, p0

    .line 167342
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Executor;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167363
    new-instance v0, LX/0zX;

    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    invoke-static {p1}, LX/54b;->a(Ljava/util/concurrent/Executor;)LX/52s;

    move-result-object v2

    .line 167364
    new-instance p0, LX/53Q;

    invoke-direct {p0, v2}, LX/53Q;-><init>(LX/52s;)V

    invoke-static {v1, p0}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p0

    move-object v1, p0

    .line 167365
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method

.method public final a(LX/0rl;)LX/0zi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rl",
            "<TT;>;)",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 167360
    new-instance v0, LX/0zY;

    invoke-direct {v0, p0, p1}, LX/0zY;-><init>(LX/0zX;LX/0rl;)V

    .line 167361
    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    invoke-virtual {v1, v0}, LX/0v9;->b(LX/0zZ;)LX/0za;

    move-result-object v0

    .line 167362
    new-instance v1, LX/0zi;

    invoke-direct {v1, v0}, LX/0zi;-><init>(LX/0za;)V

    return-object v1
.end method

.method public final b(LX/0QK;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167356
    new-instance v0, LX/4V9;

    invoke-direct {v0, p0, p1}, LX/4V9;-><init>(LX/0zX;LX/0QK;)V

    .line 167357
    new-instance v1, LX/0zX;

    iget-object v2, p0, LX/0zX;->a:LX/0v9;

    .line 167358
    new-instance p0, LX/53D;

    invoke-direct {p0, v0}, LX/53D;-><init>(LX/4V7;)V

    invoke-static {v2, p0}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p0

    move-object v0, p0

    .line 167359
    invoke-direct {v1, v0}, LX/0zX;-><init>(LX/0v9;)V

    return-object v1
.end method

.method public final b(LX/0zX;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<TT;>;)",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167350
    new-instance v0, LX/0zX;

    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    iget-object v2, p1, LX/0zX;->a:LX/0v9;

    .line 167351
    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v1, p0, p1

    const/4 p1, 0x1

    aput-object v2, p0, p1

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, LX/0v9;->b(Ljava/lang/Iterable;)LX/0v9;

    move-result-object p0

    move-object p0, p0

    .line 167352
    new-instance p1, LX/53B;

    invoke-direct {p1}, LX/53B;-><init>()V

    invoke-static {p0, p1}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p1

    move-object p0, p1

    .line 167353
    move-object p0, p0

    .line 167354
    move-object v1, p0

    .line 167355
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method

.method public final b(Ljava/util/concurrent/Executor;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167346
    new-instance v0, LX/0zX;

    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    invoke-static {p1}, LX/54b;->a(Ljava/util/concurrent/Executor;)LX/52s;

    move-result-object v2

    .line 167347
    new-instance p0, LX/53q;

    invoke-direct {p0, v1}, LX/53q;-><init>(Ljava/lang/Object;)V

    move-object p0, p0

    .line 167348
    new-instance p1, LX/53Z;

    invoke-direct {p1, v2}, LX/53Z;-><init>(LX/52s;)V

    invoke-static {p0, p1}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p0

    move-object v1, p0

    .line 167349
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method

.method public final c(LX/0zX;)LX/0zX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<TT;>;)",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167343
    new-instance v0, LX/0zX;

    iget-object v1, p0, LX/0zX;->a:LX/0v9;

    iget-object v2, p1, LX/0zX;->a:LX/0v9;

    .line 167344
    new-instance p0, LX/53T;

    invoke-direct {p0, v2}, LX/53T;-><init>(LX/0v9;)V

    invoke-static {v1, p0}, LX/0v9;->a(LX/0v9;LX/52o;)LX/0v9;

    move-result-object p0

    move-object v1, p0

    .line 167345
    invoke-direct {v0, v1}, LX/0zX;-><init>(LX/0v9;)V

    return-object v0
.end method
