.class public LX/1a3;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public a:LX/1a1;

.field public final b:Landroid/graphics/Rect;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 275884
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 275885
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 275886
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1a3;->c:Z

    .line 275887
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1a3;->d:Z

    .line 275888
    return-void
.end method

.method public constructor <init>(LX/1a3;)V
    .locals 1

    .prologue
    .line 275879
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 275880
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 275881
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1a3;->c:Z

    .line 275882
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1a3;->d:Z

    .line 275883
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 275874
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 275875
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 275876
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1a3;->c:Z

    .line 275877
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1a3;->d:Z

    .line 275878
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 275869
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 275870
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 275871
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1a3;->c:Z

    .line 275872
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1a3;->d:Z

    .line 275873
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 275889
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 275890
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1a3;->b:Landroid/graphics/Rect;

    .line 275891
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1a3;->c:Z

    .line 275892
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1a3;->d:Z

    .line 275893
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 275868
    iget-object v0, p0, LX/1a3;->a:LX/1a1;

    invoke-virtual {v0}, LX/1a1;->q()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 275867
    iget-object v0, p0, LX/1a3;->a:LX/1a1;

    invoke-virtual {v0}, LX/1a1;->o()Z

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 275866
    iget-object v0, p0, LX/1a3;->a:LX/1a1;

    invoke-virtual {v0}, LX/1a1;->d()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 275865
    iget-object v0, p0, LX/1a3;->a:LX/1a1;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    return v0
.end method
