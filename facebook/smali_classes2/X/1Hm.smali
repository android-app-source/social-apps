.class public LX/1Hm;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Hz;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1Hz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 227681
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Hz;
    .locals 4

    .prologue
    .line 227682
    sget-object v0, LX/1Hm;->a:LX/1Hz;

    if-nez v0, :cond_1

    .line 227683
    const-class v1, LX/1Hm;

    monitor-enter v1

    .line 227684
    :try_start_0
    sget-object v0, LX/1Hm;->a:LX/1Hz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 227685
    if-eqz v2, :cond_0

    .line 227686
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 227687
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object p0

    check-cast p0, LX/1Hn;

    invoke-static {v3, p0}, LX/1Hw;->a(Landroid/content/Context;LX/1Hn;)LX/1Hz;

    move-result-object v3

    move-object v0, v3

    .line 227688
    sput-object v0, LX/1Hm;->a:LX/1Hz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227689
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 227690
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 227691
    :cond_1
    sget-object v0, LX/1Hm;->a:LX/1Hz;

    return-object v0

    .line 227692
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 227693
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 227694
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v1

    check-cast v1, LX/1Hn;

    invoke-static {v0, v1}, LX/1Hw;->a(Landroid/content/Context;LX/1Hn;)LX/1Hz;

    move-result-object v0

    return-object v0
.end method
