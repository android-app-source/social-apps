.class public LX/1JC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1JC;


# instance fields
.field private final a:LX/18A;


# direct methods
.method public constructor <init>(LX/18A;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 229879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229880
    iput-object p1, p0, LX/1JC;->a:LX/18A;

    .line 229881
    return-void
.end method

.method public static a(LX/0QB;)LX/1JC;
    .locals 4

    .prologue
    .line 229866
    sget-object v0, LX/1JC;->b:LX/1JC;

    if-nez v0, :cond_1

    .line 229867
    const-class v1, LX/1JC;

    monitor-enter v1

    .line 229868
    :try_start_0
    sget-object v0, LX/1JC;->b:LX/1JC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 229869
    if-eqz v2, :cond_0

    .line 229870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 229871
    new-instance p0, LX/1JC;

    invoke-static {v0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v3

    check-cast v3, LX/18A;

    invoke-direct {p0, v3}, LX/1JC;-><init>(LX/18A;)V

    .line 229872
    move-object v0, p0

    .line 229873
    sput-object v0, LX/1JC;->b:LX/1JC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 229875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 229876
    :cond_1
    sget-object v0, LX/1JC;->b:LX/1JC;

    return-object v0

    .line 229877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 229878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/0fz;Z)V
    .locals 2

    .prologue
    .line 229859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_1

    .line 229860
    :cond_0
    :goto_0
    return-void

    .line 229861
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 229862
    invoke-static {v0}, LX/0x1;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eq v1, p2, :cond_0

    .line 229863
    invoke-static {v0, p2}, LX/0x1;->c(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 229864
    iget-object v1, p1, LX/0fz;->a:LX/0qq;

    move-object v1, v1

    .line 229865
    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    .line 229831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/0x1;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 229832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aE()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 229833
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v5, :cond_2

    .line 229834
    add-int/lit8 v0, v1, 0x1

    .line 229835
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 229836
    :cond_0
    move v0, v1

    .line 229837
    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(ZLX/0qw;ILcom/facebook/api/feed/FetchFeedResult;LX/0fz;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 229838
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 229839
    :cond_0
    return-void

    .line 229840
    :cond_1
    if-eqz p1, :cond_0

    if-lez p3, :cond_0

    .line 229841
    iget-object v0, p4, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 229842
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v2, :cond_0

    sget-object v0, LX/0qw;->CHUNKED_INITIAL:LX/0qw;

    if-ne p2, v0, :cond_0

    .line 229843
    invoke-virtual {p5}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 229844
    invoke-static {v0, p5, v1}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/0fz;Z)V

    goto :goto_0

    .line 229845
    :cond_2
    iget-object v0, p4, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 229846
    if-eqz v0, :cond_3

    .line 229847
    iget-object v0, p0, LX/1JC;->a:LX/18A;

    .line 229848
    iget-object v2, p4, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v2, v2

    .line 229849
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v2, v3

    .line 229850
    iget-object v3, v0, LX/18A;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1fI;

    .line 229851
    new-instance p0, LX/30s;

    iget-object v5, v3, LX/1fI;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1fM;

    new-instance v0, LX/30t;

    invoke-direct {v0, v2}, LX/30t;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v5, v0}, LX/30s;-><init>(LX/1fM;LX/1fd;)V

    .line 229852
    iget-object v5, v3, LX/1fI;->b:LX/1fJ;

    invoke-virtual {v5, p0}, LX/1fJ;->a(LX/1fL;)V

    .line 229853
    goto :goto_1

    .line 229854
    :cond_3
    invoke-virtual {p4}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 229855
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 229856
    instance-of v5, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v5, :cond_4

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ak()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 229857
    const/4 v1, 0x1

    invoke-static {v0, p5, v1}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/0fz;Z)V

    .line 229858
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method
