.class public LX/10x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/10x;


# instance fields
.field public final b:LX/0SG;

.field public final c:I

.field private final d:Z

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0fU;

.field public final g:LX/0Zb;

.field private final h:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169576
    const-class v0, LX/10x;

    sput-object v0, LX/10x;->a:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/0SG;LX/0V6;ILcom/facebook/prefs/shared/FbSharedPreferences;LX/0fU;LX/0rb;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 169656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169657
    iput-object p1, p0, LX/10x;->b:LX/0SG;

    .line 169658
    invoke-virtual {p2}, LX/0V6;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/10x;->d:Z

    .line 169659
    iget-boolean v0, p0, LX/10x;->d:Z

    if-eqz v0, :cond_0

    const/4 p3, 0x6

    :cond_0
    iput p3, p0, LX/10x;->c:I

    .line 169660
    iput-object p4, p0, LX/10x;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 169661
    iput-object p5, p0, LX/10x;->f:LX/0fU;

    .line 169662
    invoke-interface {p6, p0}, LX/0rb;->a(LX/0rf;)V

    .line 169663
    iput-object p7, p0, LX/10x;->g:LX/0Zb;

    .line 169664
    iget-object v0, p0, LX/10x;->f:LX/0fU;

    new-instance v1, LX/10y;

    invoke-direct {v1, p0}, LX/10y;-><init>(LX/10x;)V

    .line 169665
    iput-object v1, v0, LX/0fU;->m:LX/10y;

    .line 169666
    iput-object p8, p0, LX/10x;->h:Ljava/util/concurrent/ExecutorService;

    .line 169667
    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0V6;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0fU;LX/0rb;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V
    .locals 9
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169654
    const/16 v3, 0x8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/10x;-><init>(LX/0SG;LX/0V6;ILcom/facebook/prefs/shared/FbSharedPreferences;LX/0fU;LX/0rb;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V

    .line 169655
    return-void
.end method

.method public static a(LX/0QB;)LX/10x;
    .locals 11

    .prologue
    .line 169641
    sget-object v0, LX/10x;->i:LX/10x;

    if-nez v0, :cond_1

    .line 169642
    const-class v1, LX/10x;

    monitor-enter v1

    .line 169643
    :try_start_0
    sget-object v0, LX/10x;->i:LX/10x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169644
    if-eqz v2, :cond_0

    .line 169645
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169646
    new-instance v3, LX/10x;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v5

    check-cast v5, LX/0V6;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0fU;->a(LX/0QB;)LX/0fU;

    move-result-object v7

    check-cast v7, LX/0fU;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v8

    check-cast v8, LX/0rb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v10}, LX/10x;-><init>(LX/0SG;LX/0V6;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0fU;LX/0rb;LX/0Zb;Ljava/util/concurrent/ExecutorService;)V

    .line 169647
    move-object v0, v3

    .line 169648
    sput-object v0, LX/10x;->i:LX/10x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169649
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169650
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169651
    :cond_1
    sget-object v0, LX/10x;->i:LX/10x;

    return-object v0

    .line 169652
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169653
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169632
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 169633
    iget-object v2, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v2, v0}, LX/0fU;->d(Landroid/app/Activity;)V

    .line 169634
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 169635
    iget-object v2, p0, LX/10x;->g:LX/0Zb;

    const-string v3, "activity_cleaned_by_cleaner"

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 169636
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 169637
    const-string v3, "cleaned_activity"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 169638
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 169639
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    goto :goto_0

    .line 169640
    :cond_1
    return-void
.end method

.method public static a$redex0(LX/10x;ILandroid/app/Activity;)V
    .locals 8
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 169605
    iget-object v0, p0, LX/10x;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169606
    :goto_0
    return-void

    .line 169607
    :cond_0
    iget-object v0, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->f()I

    move-result v0

    sub-int v2, v0, p1

    .line 169608
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 169609
    iget-object v0, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    .line 169610
    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 169611
    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/10x;->h(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169612
    const/4 v0, 0x1

    .line 169613
    :goto_1
    move v0, v0

    .line 169614
    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 169615
    :goto_2
    iget-object v3, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v3}, LX/0fU;->e()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    move v2, v0

    :cond_2
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    .line 169616
    if-lez v3, :cond_5

    .line 169617
    if-eqz v2, :cond_4

    move v2, v1

    .line 169618
    goto :goto_3

    :cond_3
    move v0, v1

    .line 169619
    goto :goto_2

    .line 169620
    :cond_4
    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v0

    .line 169621
    invoke-static {v0}, LX/10x;->h(Landroid/app/Activity;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 169622
    if-eq v0, p2, :cond_2

    .line 169623
    invoke-static {v0}, LX/0fU;->b(Landroid/app/Activity;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 169624
    iget-object v6, p0, LX/10x;->f:LX/0fU;

    .line 169625
    if-eqz v0, :cond_8

    iget-object v7, v6, LX/0fU;->e:Ljava/util/Set;

    iget-object p1, v6, LX/0fU;->d:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v7, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v7, 0x1

    :goto_4
    move v6, v7

    .line 169626
    if-nez v6, :cond_2

    .line 169627
    if-eqz v0, :cond_6

    .line 169628
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169629
    add-int/lit8 v0, v3, -0x1

    :goto_5
    move v3, v0

    .line 169630
    goto :goto_3

    .line 169631
    :cond_5
    invoke-direct {p0, v4}, LX/10x;->a(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    :cond_8
    const/4 v7, 0x0

    goto :goto_4
.end method

.method public static f(LX/10x;Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 169597
    iget-object v0, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->e()Ljava/util/List;

    move-result-object v2

    .line 169598
    instance-of v0, p1, LX/0fI;

    if-eqz v0, :cond_0

    .line 169599
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 169600
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v0

    .line 169601
    if-eq v0, p1, :cond_1

    instance-of v3, v0, LX/0fI;

    if-eqz v3, :cond_1

    .line 169602
    check-cast v0, LX/0fI;

    invoke-interface {v0}, LX/0fI;->v()V

    .line 169603
    :cond_0
    return-void

    .line 169604
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public static h(Landroid/app/Activity;)Z
    .locals 1
    .param p0    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 169594
    if-nez p0, :cond_0

    .line 169595
    const/4 v0, 0x0

    .line 169596
    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, LX/0f7;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 3

    .prologue
    .line 169590
    sget-object v0, LX/32G;->OnAppBackgrounded:LX/32G;

    if-ne p1, v0, :cond_1

    .line 169591
    :cond_0
    :goto_0
    return-void

    .line 169592
    :cond_1
    iget-boolean v0, p0, LX/10x;->d:Z

    if-nez v0, :cond_2

    sget-object v0, LX/32G;->OnSystemLowMemoryWhileAppInForeground:LX/32G;

    if-eq p1, v0, :cond_0

    .line 169593
    :cond_2
    iget-object v0, p0, LX/10x;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/common/activitycleaner/ActivityCleaner$2;

    invoke-direct {v1, p0}, Lcom/facebook/common/activitycleaner/ActivityCleaner$2;-><init>(LX/10x;)V

    const v2, -0x57daed08

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 169585
    iget-boolean v0, p0, LX/10x;->d:Z

    if-eqz v0, :cond_0

    .line 169586
    invoke-static {p0, p1}, LX/10x;->f(LX/10x;Landroid/app/Activity;)V

    .line 169587
    instance-of v0, p1, LX/0fI;

    if-eqz v0, :cond_0

    .line 169588
    check-cast p1, LX/0fI;

    invoke-interface {p1}, LX/0fI;->v()V

    .line 169589
    :cond_0
    return-void
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 169577
    iget-object v0, p0, LX/10x;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169578
    :goto_0
    return-void

    .line 169579
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 169580
    iget-object v0, p0, LX/10x;->f:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fc;

    .line 169581
    invoke-virtual {v0}, LX/0fc;->b()Landroid/app/Activity;

    move-result-object v0

    .line 169582
    invoke-static {v0}, LX/10x;->h(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eq v0, p1, :cond_1

    .line 169583
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 169584
    :cond_2
    invoke-direct {p0, v1}, LX/10x;->a(Ljava/util/List;)V

    goto :goto_0
.end method
