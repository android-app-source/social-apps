.class public LX/14z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179841
    const-string v0, "(\\d*)(\\D*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/14z;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 179801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 179839
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 179840
    :goto_0
    return p1

    :catch_0
    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 179803
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 179804
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 179805
    array-length v0, v6

    array-length v1, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    move v5, v3

    move v2, v3

    .line 179806
    :goto_0
    if-nez v2, :cond_b

    if-ge v5, v8, :cond_b

    .line 179807
    array-length v0, v6

    if-ge v5, v0, :cond_4

    aget-object v0, v6, v5

    .line 179808
    :goto_1
    array-length v1, v7

    if-ge v5, v1, :cond_5

    aget-object v1, v7, v5

    .line 179809
    :goto_2
    sget-object v9, LX/14z;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 179810
    sget-object v0, LX/14z;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    move v0, v2

    .line 179811
    :cond_0
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    .line 179812
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v11

    .line 179813
    if-nez v2, :cond_1

    if-eqz v11, :cond_3

    .line 179814
    :cond_1
    if-eqz v2, :cond_6

    invoke-virtual {v9, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, LX/14z;->a(Ljava/lang/String;I)I

    move-result v0

    move v1, v0

    .line 179815
    :goto_3
    if-eqz v11, :cond_7

    invoke-virtual {v10, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, LX/14z;->a(Ljava/lang/String;I)I

    move-result v0

    .line 179816
    :goto_4
    if-ge v1, v0, :cond_d

    .line 179817
    const/4 p0, -0x1

    .line 179818
    :goto_5
    move v1, p0

    .line 179819
    if-nez v1, :cond_c

    .line 179820
    if-eqz v2, :cond_8

    invoke-virtual {v9, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 179821
    :goto_6
    if-eqz v11, :cond_9

    invoke-virtual {v10, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 179822
    :goto_7
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_a

    move v1, v4

    .line 179823
    :cond_2
    :goto_8
    if-nez v1, :cond_c

    .line 179824
    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 179825
    :goto_9
    if-eqz v0, :cond_0

    .line 179826
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v2, v0

    goto :goto_0

    .line 179827
    :cond_4
    const-string v0, ""

    goto :goto_1

    .line 179828
    :cond_5
    const-string v1, ""

    goto :goto_2

    :cond_6
    move v1, v3

    .line 179829
    goto :goto_3

    :cond_7
    move v0, v3

    .line 179830
    goto :goto_4

    .line 179831
    :cond_8
    const-string v0, ""

    move-object v2, v0

    goto :goto_6

    .line 179832
    :cond_9
    const-string v0, ""

    goto :goto_7

    .line 179833
    :cond_a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_2

    .line 179834
    const/4 v1, -0x1

    goto :goto_8

    .line 179835
    :cond_b
    return v2

    :cond_c
    move v0, v1

    goto :goto_9

    .line 179836
    :cond_d
    if-le v1, v0, :cond_e

    .line 179837
    const/4 p0, 0x1

    goto :goto_5

    .line 179838
    :cond_e
    const/4 p0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 179802
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, LX/14z;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
