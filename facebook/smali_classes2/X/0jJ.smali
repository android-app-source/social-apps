.class public LX/0jJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
        "<",
        "Lcom/facebook/composer/system/api/ComposerMutation;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/HvF;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/HvE;

.field private final c:LX/HvB;

.field private final d:LX/HvG;

.field public final e:LX/0Sh;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0iK",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:LX/HvD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/composer/system/model/ComposerModelImpl;


# direct methods
.method public constructor <init>(LX/HvE;LX/2zF;LX/HvH;LX/0Sh;Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0in;)V
    .locals 2
    .param p5    # Lcom/facebook/composer/system/model/ComposerModelImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0in;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HvE;",
            "LX/2zF;",
            "LX/HvH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/composer/system/model/ComposerModelImpl;",
            "LX/0in",
            "<",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 122620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122621
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/0jJ;->f:Ljava/util/Set;

    .line 122622
    new-instance v0, LX/HvF;

    invoke-direct {v0, p0}, LX/HvF;-><init>(LX/0jJ;)V

    iput-object v0, p0, LX/0jJ;->a:LX/HvF;

    .line 122623
    iput-object p1, p0, LX/0jJ;->b:LX/HvE;

    .line 122624
    invoke-virtual {p2, p6}, LX/2zF;->a(LX/0in;)LX/HvB;

    move-result-object v0

    iput-object v0, p0, LX/0jJ;->c:LX/HvB;

    .line 122625
    new-instance v1, LX/HvG;

    const-class v0, LX/2zF;

    invoke-interface {p3, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/2zF;

    const/16 p1, 0x15e7

    invoke-static {p3, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    const/16 p2, 0x22ae

    invoke-static {p3, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v1, p6, v0, p1, p2}, LX/HvG;-><init>(LX/0in;LX/2zF;LX/0Or;LX/0Ot;)V

    .line 122626
    move-object v0, v1

    .line 122627
    iput-object v0, p0, LX/0jJ;->d:LX/HvG;

    .line 122628
    iput-object p4, p0, LX/0jJ;->e:LX/0Sh;

    .line 122629
    iput-object p5, p0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122630
    return-void
.end method

.method public static a$redex0(LX/0jJ;LX/HvD;)V
    .locals 6

    .prologue
    .line 122631
    iget-object v0, p0, LX/0jJ;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122632
    iget-object v0, p0, LX/0jJ;->g:LX/HvD;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122633
    iget-object v0, p0, LX/0jJ;->g:LX/HvD;

    if-ne v0, p1, :cond_7

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 122634
    const/4 v0, 0x0

    iput-object v0, p0, LX/0jJ;->g:LX/HvD;

    .line 122635
    iget-object v1, p0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122636
    const/4 v0, 0x0

    .line 122637
    :cond_0
    iget-object v2, p1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-eqz v2, :cond_8

    iget-object v2, p1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 122638
    iget-object v3, p0, LX/0jJ;->d:LX/HvG;

    invoke-virtual {v3, p1, v1, v2}, LX/HvG;->a(LX/HvD;Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v3

    .line 122639
    if-eqz v3, :cond_2

    .line 122640
    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x32

    if-lt v0, v1, :cond_1

    .line 122641
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Iteration limit reached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v1, v2

    .line 122642
    :cond_2
    if-nez v3, :cond_0

    .line 122643
    iget-object v0, p1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 122644
    if-nez v0, :cond_4

    .line 122645
    :cond_3
    return-void

    .line 122646
    :cond_4
    iget-object v3, p0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122647
    iput-object v2, p0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 122648
    iget-object v0, p1, LX/0jL;->a:LX/0cA;

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 122649
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5L2;

    .line 122650
    iget-object v1, p0, LX/0jJ;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iK;

    .line 122651
    sget-object v5, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    if-ne v0, v5, :cond_6

    .line 122652
    iget-object v5, p0, LX/0jJ;->c:LX/HvB;

    invoke-virtual {v5, v3}, LX/HvB;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)LX/2zG;

    move-result-object v5

    invoke-interface {v1, v3, v5}, LX/0iK;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 122653
    :cond_6
    invoke-interface {v1, v0}, LX/0iK;->a(LX/5L2;)V

    goto :goto_3

    .line 122654
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    :cond_8
    iget-object v2, p1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final synthetic a(LX/0jK;)LX/0jL;
    .locals 1

    .prologue
    .line 122619
    invoke-virtual {p0, p1}, LX/0jJ;->b(LX/0jK;)LX/HvD;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0jK;)LX/HvD;
    .locals 5

    .prologue
    .line 122605
    iget-object v0, p0, LX/0jJ;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 122606
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122607
    iget-object v0, p0, LX/0jJ;->g:LX/HvD;

    if-eqz v0, :cond_0

    .line 122608
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tried to mutate with originator "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 122609
    iget-object v2, p1, LX/0jK;->a:Ljava/lang/String;

    move-object v2, v2

    .line 122610
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but another mutation already started by originator "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/0jJ;->g:LX/HvD;

    .line 122611
    iget-object p0, v2, LX/HvD;->a:LX/0jK;

    move-object v2, p0

    .line 122612
    iget-object p0, v2, LX/0jK;->a:Ljava/lang/String;

    move-object v2, p0

    .line 122613
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is still in progress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122614
    :cond_0
    iget-object v0, p0, LX/0jJ;->b:LX/HvE;

    iget-object v1, p0, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    iget-object v2, p0, LX/0jJ;->a:LX/HvF;

    .line 122615
    new-instance v4, LX/HvD;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-direct {v4, v1, p1, v2, v3}, LX/HvD;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0jK;LX/HvF;LX/0Sh;)V

    .line 122616
    move-object v0, v4

    .line 122617
    iput-object v0, p0, LX/0jJ;->g:LX/HvD;

    .line 122618
    iget-object v0, p0, LX/0jJ;->g:LX/HvD;

    return-object v0
.end method
