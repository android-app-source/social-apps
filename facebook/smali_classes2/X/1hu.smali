.class public LX/1hu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hr;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 297291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297292
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 297290
    const v0, 0x7fffffff

    return v0
.end method

.method public final a(Lorg/apache/http/impl/client/RequestWrapper;)V
    .locals 3

    .prologue
    .line 297279
    invoke-virtual {p1}, Lorg/apache/http/impl/client/RequestWrapper;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297280
    invoke-static {v0}, LX/1iH;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 297281
    if-nez v1, :cond_0

    .line 297282
    :goto_0
    return-void

    .line 297283
    :cond_0
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-static {v0}, LX/1iH;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297284
    const-string v0, "ak"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297285
    const-string v0, "Pragma"

    const-string v1, "akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-get-client-ip"

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/impl/client/RequestWrapper;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 297286
    :catch_0
    move-exception v0

    .line 297287
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 297288
    :cond_1
    const-string v0, "X-FB-Debug"

    const-string v1, "True"

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/impl/client/RequestWrapper;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 297289
    const-string v0, "X-FB-Origin-Debug"

    const-string v1, "True"

    invoke-virtual {p1, v0, v1}, Lorg/apache/http/impl/client/RequestWrapper;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
