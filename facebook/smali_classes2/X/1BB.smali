.class public LX/1BB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:LX/0lC;


# instance fields
.field private a:[J

.field public b:I

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 212358
    new-instance v0, LX/0lC;

    invoke-direct {v0}, LX/0lC;-><init>()V

    sput-object v0, LX/1BB;->e:LX/0lC;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 212359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212360
    const/16 v0, 0xc0

    new-array v0, v0, [J

    iput-object v0, p0, LX/1BB;->a:[J

    .line 212361
    const/4 v0, 0x0

    iput v0, p0, LX/1BB;->b:I

    .line 212362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1BB;->c:Ljava/util/Map;

    .line 212363
    const-string v0, "StatMap.class"

    iput-object v0, p0, LX/1BB;->d:Ljava/lang/String;

    return-void
.end method

.method private static a(IS)J
    .locals 6

    .prologue
    .line 212364
    int-to-long v0, p0

    .line 212365
    int-to-long v2, p1

    .line 212366
    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(LX/0P1;LX/0Rf;)LX/0lF;
    .locals 13
    .param p1    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Rf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x20

    const/4 v2, 0x0

    .line 212367
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    move v1, v2

    .line 212368
    :goto_0
    iget v0, p0, LX/1BB;->b:I

    if-ge v1, v0, :cond_9

    .line 212369
    iget-object v0, p0, LX/1BB;->a:[J

    add-int/lit8 v4, v1, 0x0

    aget-wide v4, v0, v4

    .line 212370
    shr-long v6, v4, v12

    long-to-int v6, v6

    .line 212371
    const/4 v0, -0x1

    .line 212372
    if-eqz p1, :cond_8

    .line 212373
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 212374
    if-eqz v0, :cond_7

    .line 212375
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 212376
    :cond_0
    :goto_1
    if-eqz v0, :cond_6

    .line 212377
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 212378
    and-int/lit8 v8, v0, 0x1

    if-eqz v8, :cond_1

    .line 212379
    const-string v8, "count"

    iget-object v9, p0, LX/1BB;->a:[J

    add-int/lit8 v10, v1, 0x1

    aget-wide v10, v9, v10

    invoke-virtual {v7, v8, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 212380
    :cond_1
    and-int/lit8 v8, v0, 0x2

    if-eqz v8, :cond_2

    .line 212381
    const-string v8, "sum"

    iget-object v9, p0, LX/1BB;->a:[J

    add-int/lit8 v10, v1, 0x2

    aget-wide v10, v9, v10

    invoke-virtual {v7, v8, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 212382
    :cond_2
    and-int/lit8 v8, v0, 0x4

    if-eqz v8, :cond_3

    .line 212383
    const-string v8, "s_sum"

    iget-object v9, p0, LX/1BB;->a:[J

    add-int/lit8 v10, v1, 0x3

    aget-wide v10, v9, v10

    invoke-virtual {v7, v8, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 212384
    :cond_3
    and-int/lit8 v8, v0, 0x8

    if-eqz v8, :cond_4

    .line 212385
    const-string v8, "max"

    iget-object v9, p0, LX/1BB;->a:[J

    add-int/lit8 v10, v1, 0x4

    aget-wide v10, v9, v10

    invoke-virtual {v7, v8, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 212386
    :cond_4
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 212387
    const-string v0, "last"

    iget-object v8, p0, LX/1BB;->a:[J

    add-int/lit8 v9, v1, 0x5

    aget-wide v8, v8, v9

    invoke-virtual {v7, v0, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 212388
    :cond_5
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 212389
    long-to-int v4, v4

    int-to-short v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 212390
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 212391
    :cond_6
    add-int/lit8 v0, v1, 0x6

    move v1, v0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 212392
    goto :goto_1

    .line 212393
    :cond_8
    if-eqz p2, :cond_0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p2, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    goto/16 :goto_1

    .line 212394
    :cond_9
    iget-object v0, p0, LX/1BB;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 212395
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 212396
    shr-long v6, v4, v12

    long-to-int v1, v6

    .line 212397
    if-eqz p1, :cond_c

    .line 212398
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 212399
    :cond_b
    :goto_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 212400
    long-to-int v1, v4

    int-to-short v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 212401
    :try_start_0
    sget-object v5, LX/1BB;->e:LX/0lC;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 212402
    if-nez v1, :cond_d

    .line 212403
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "_"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v5, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 212404
    :catch_0
    move-exception v1

    .line 212405
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    invoke-virtual {v1}, LX/2aQ;->getMessage()Ljava/lang/String;

    .line 212406
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "_"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_2

    .line 212407
    :cond_c
    if-eqz p2, :cond_b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p2, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    goto :goto_3

    .line 212408
    :cond_d
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_1
    .catch LX/2aQ; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 212409
    :catch_1
    move-exception v0

    .line 212410
    const-string v1, "StatMap.class"

    const-string v4, "IO Exception in readTree(). %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 212411
    :cond_e
    return-object v3
.end method

.method public final declared-synchronized a(ISJ)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 212412
    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2}, LX/1BB;->a(IS)J

    move-result-wide v4

    .line 212413
    iget v0, p0, LX/1BB;->b:I

    div-int/lit8 v0, v0, 0x6

    move v2, v1

    .line 212414
    :goto_0
    if-ge v2, v0, :cond_1

    .line 212415
    sub-int v1, v0, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    .line 212416
    iget-object v3, p0, LX/1BB;->a:[J

    mul-int/lit8 v6, v1, 0x6

    aget-wide v6, v3, v6

    cmp-long v3, v6, v4

    if-gez v3, :cond_0

    .line 212417
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 212418
    goto :goto_0

    .line 212419
    :cond_1
    mul-int/lit8 v0, v0, 0x6

    .line 212420
    iget v1, p0, LX/1BB;->b:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, LX/1BB;->a:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 212421
    :cond_2
    iget v1, p0, LX/1BB;->b:I

    iget-object v2, p0, LX/1BB;->a:[J

    array-length v2, v2

    if-lt v1, v2, :cond_4

    .line 212422
    iget-object v1, p0, LX/1BB;->a:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [J

    .line 212423
    iget-object v2, p0, LX/1BB;->a:[J

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v3, v1, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212424
    iget-object v2, p0, LX/1BB;->a:[J

    add-int/lit8 v3, v0, 0x6

    iget v6, p0, LX/1BB;->b:I

    sub-int/2addr v6, v0

    invoke-static {v2, v0, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212425
    iput-object v1, p0, LX/1BB;->a:[J

    .line 212426
    :goto_1
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v2, v0, 0x0

    aput-wide v4, v1, v2

    .line 212427
    iget v1, p0, LX/1BB;->b:I

    add-int/lit8 v1, v1, 0x6

    iput v1, p0, LX/1BB;->b:I

    .line 212428
    :cond_3
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v2, v0, 0x1

    aget-wide v4, v1, v2

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 212429
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v2, v0, 0x2

    aget-wide v4, v1, v2

    add-long/2addr v4, p3

    aput-wide v4, v1, v2

    .line 212430
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v2, v0, 0x3

    aget-wide v4, v1, v2

    mul-long v6, p3, p3

    add-long/2addr v4, v6

    aput-wide v4, v1, v2

    .line 212431
    add-int/lit8 v1, v0, 0x4

    .line 212432
    iget-object v2, p0, LX/1BB;->a:[J

    iget-object v3, p0, LX/1BB;->a:[J

    aget-wide v4, v3, v1

    invoke-static {v4, v5, p3, p4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 212433
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v0, v0, 0x5

    aput-wide p3, v1, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212434
    monitor-exit p0

    return-void

    .line 212435
    :cond_4
    :try_start_1
    iget-object v1, p0, LX/1BB;->a:[J

    iget-object v2, p0, LX/1BB;->a:[J

    add-int/lit8 v3, v0, 0x6

    iget v6, p0, LX/1BB;->b:I

    sub-int/2addr v6, v0

    invoke-static {v1, v0, v2, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212436
    iget-object v1, p0, LX/1BB;->a:[J

    add-int/lit8 v2, v0, 0x6

    const-wide/16 v6, 0x0

    invoke-static {v1, v0, v2, v6, v7}, Ljava/util/Arrays;->fill([JIIJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 212437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ISLjava/lang/String;)V
    .locals 4

    .prologue
    .line 212438
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1BB;->c:Ljava/util/Map;

    invoke-static {p1, p2}, LX/1BB;->a(IS)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212439
    monitor-exit p0

    return-void

    .line 212440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/1BB;
    .locals 3

    .prologue
    .line 212441
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/1BB;

    invoke-direct {v0}, LX/1BB;-><init>()V

    .line 212442
    iget-object v1, p0, LX/1BB;->a:[J

    .line 212443
    iget-object v2, v0, LX/1BB;->a:[J

    iput-object v2, p0, LX/1BB;->a:[J

    .line 212444
    iput-object v1, v0, LX/1BB;->a:[J

    .line 212445
    iget-object v1, p0, LX/1BB;->c:Ljava/util/Map;

    .line 212446
    iget-object v2, v0, LX/1BB;->c:Ljava/util/Map;

    iput-object v2, p0, LX/1BB;->c:Ljava/util/Map;

    .line 212447
    iput-object v1, v0, LX/1BB;->c:Ljava/util/Map;

    .line 212448
    iget v1, p0, LX/1BB;->b:I

    .line 212449
    iget v2, v0, LX/1BB;->b:I

    iput v2, p0, LX/1BB;->b:I

    .line 212450
    iput v1, v0, LX/1BB;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212451
    monitor-exit p0

    return-object v0

    .line 212452
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
