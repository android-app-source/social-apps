.class public LX/0zc;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Z

.field private final b:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0zZ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 167523
    invoke-direct {p0, p1}, LX/0zZ;-><init>(LX/0zZ;)V

    .line 167524
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/0zc;->a:Z

    .line 167525
    iput-object p1, p0, LX/0zc;->b:LX/0zZ;

    .line 167526
    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 167488
    :try_start_0
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 167489
    invoke-virtual {v0}, LX/0vB;->b()LX/0vD;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 167490
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/0zc;->b:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 167491
    :try_start_2
    invoke-virtual {p0}, LX/0zZ;->b()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_7

    .line 167492
    return-void

    .line 167493
    :catch_0
    move-exception v0

    .line 167494
    invoke-static {v0}, LX/0zc;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 167495
    :catch_1
    move-exception v0

    .line 167496
    instance-of v1, v0, LX/531;

    if-eqz v1, :cond_0

    .line 167497
    :try_start_3
    invoke-virtual {p0}, LX/0zZ;->b()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 167498
    check-cast v0, LX/531;

    throw v0

    .line 167499
    :catch_2
    move-exception v1

    .line 167500
    :try_start_4
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 167501
    invoke-virtual {v0}, LX/0vB;->b()LX/0vD;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 167502
    :goto_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Observer.onError not implemented and error while unsubscribing."

    new-instance v3, LX/52x;

    new-array v4, v8, [Ljava/lang/Throwable;

    aput-object p1, v4, v6

    aput-object v1, v4, v7

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, v1, v6}, LX/52x;-><init>(Ljava/util/Collection;B)V

    invoke-direct {v0, v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 167503
    :catch_3
    move-exception v0

    .line 167504
    invoke-static {v0}, LX/0zc;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 167505
    :cond_0
    :try_start_5
    sget-object v1, LX/0vB;->b:LX/0vB;

    move-object v1, v1

    .line 167506
    invoke-virtual {v1}, LX/0vB;->b()LX/0vD;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    .line 167507
    :goto_2
    :try_start_6
    invoke-virtual {p0}, LX/0zZ;->b()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_5

    .line 167508
    new-instance v1, LX/530;

    const-string v2, "Error occurred when trying to propagate error to Observer.onError"

    new-instance v3, LX/52x;

    new-array v4, v8, [Ljava/lang/Throwable;

    aput-object p1, v4, v6

    aput-object v0, v4, v7

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0, v6}, LX/52x;-><init>(Ljava/util/Collection;B)V

    invoke-direct {v1, v2, v3}, LX/530;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 167509
    :catch_4
    move-exception v1

    .line 167510
    invoke-static {v1}, LX/0zc;->c(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 167511
    :catch_5
    move-exception v2

    .line 167512
    :try_start_7
    sget-object v1, LX/0vB;->b:LX/0vB;

    move-object v1, v1

    .line 167513
    invoke-virtual {v1}, LX/0vB;->b()LX/0vD;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_6

    .line 167514
    :goto_3
    new-instance v1, LX/530;

    const-string v3, "Error occurred when trying to propagate error to Observer.onError and during unsubscription."

    new-instance v4, LX/52x;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Throwable;

    aput-object p1, v5, v6

    aput-object v0, v5, v7

    aput-object v2, v5, v8

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0, v6}, LX/52x;-><init>(Ljava/util/Collection;B)V

    invoke-direct {v1, v3, v4}, LX/530;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 167515
    :catch_6
    move-exception v1

    .line 167516
    invoke-static {v1}, LX/0zc;->c(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 167517
    :catch_7
    move-exception v1

    .line 167518
    :try_start_8
    sget-object v0, LX/0vB;->b:LX/0vB;

    move-object v0, v0

    .line 167519
    invoke-virtual {v0}, LX/0vB;->b()LX/0vD;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_8

    .line 167520
    :goto_4
    new-instance v0, LX/530;

    invoke-direct {v0, v1}, LX/530;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 167521
    :catch_8
    move-exception v0

    .line 167522
    invoke-static {v0}, LX/0zc;->c(Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method private static c(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 167465
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RxJavaErrorHandler threw an Exception. It shouldn\'t. => "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 167466
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 167467
    return-void
.end method


# virtual methods
.method public final R_()V
    .locals 1

    .prologue
    .line 167479
    iget-boolean v0, p0, LX/0zc;->a:Z

    if-nez v0, :cond_0

    .line 167480
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0zc;->a:Z

    .line 167481
    :try_start_0
    iget-object v0, p0, LX/0zc;->b:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167482
    invoke-virtual {p0}, LX/0zZ;->b()V

    .line 167483
    :cond_0
    :goto_0
    return-void

    .line 167484
    :catch_0
    move-exception v0

    .line 167485
    :try_start_1
    invoke-static {v0}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 167486
    invoke-direct {p0, v0}, LX/0zc;->b(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167487
    invoke-virtual {p0}, LX/0zZ;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/0zZ;->b()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 167473
    :try_start_0
    iget-boolean v0, p0, LX/0zc;->a:Z

    if-nez v0, :cond_0

    .line 167474
    iget-object v0, p0, LX/0zc;->b:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 167475
    :cond_0
    :goto_0
    return-void

    .line 167476
    :catch_0
    move-exception v0

    .line 167477
    invoke-static {v0}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 167478
    invoke-virtual {p0, v0}, LX/0zc;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 167468
    invoke-static {p1}, LX/52y;->b(Ljava/lang/Throwable;)V

    .line 167469
    iget-boolean v0, p0, LX/0zc;->a:Z

    if-nez v0, :cond_0

    .line 167470
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/0zc;->a:Z

    .line 167471
    invoke-direct {p0, p1}, LX/0zc;->b(Ljava/lang/Throwable;)V

    .line 167472
    :cond_0
    return-void
.end method
