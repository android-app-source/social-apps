.class public LX/0mC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/0mC;

.field private static final b:LX/0mC;

.field private static final c:LX/0mC;

.field private static final serialVersionUID:J = -0x2d6844aba38e871aL


# instance fields
.field private final _cfgBigDecimalExact:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 132088
    new-instance v0, LX/0mC;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/0mC;-><init>(Z)V

    sput-object v0, LX/0mC;->b:LX/0mC;

    .line 132089
    new-instance v0, LX/0mC;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/0mC;-><init>(Z)V

    sput-object v0, LX/0mC;->c:LX/0mC;

    .line 132090
    sget-object v0, LX/0mC;->b:LX/0mC;

    sput-object v0, LX/0mC;->a:LX/0mC;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132086
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/0mC;-><init>(Z)V

    .line 132087
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 132083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132084
    iput-boolean p1, p0, LX/0mC;->_cfgBigDecimalExact:Z

    .line 132085
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0mD;
    .locals 1

    .prologue
    .line 132082
    invoke-static {p0}, LX/0mD;->g(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)LX/0mE;
    .locals 1

    .prologue
    .line 132081
    new-instance v0, LX/4rJ;

    invoke-direct {v0, p0}, LX/4rJ;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(D)LX/0rP;
    .locals 2

    .prologue
    .line 132080
    invoke-static {p0, p1}, LX/0rO;->b(D)LX/0rO;

    move-result-object v0

    return-object v0
.end method

.method public static a(F)LX/0rP;
    .locals 1

    .prologue
    .line 132079
    invoke-static {p0}, LX/2Cd;->a(F)LX/2Cd;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)LX/0rP;
    .locals 1

    .prologue
    .line 132091
    invoke-static {p0}, LX/0rQ;->c(I)LX/0rQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)LX/0rP;
    .locals 2

    .prologue
    .line 132078
    invoke-static {p0, p1}, LX/10w;->b(J)LX/10w;

    move-result-object v0

    return-object v0
.end method

.method public static a(S)LX/0rP;
    .locals 1

    .prologue
    .line 132077
    invoke-static {p0}, LX/4rK;->a(S)LX/4rK;

    move-result-object v0

    return-object v0
.end method

.method public static a(Z)LX/1Xb;
    .locals 1

    .prologue
    .line 132072
    if-eqz p0, :cond_0

    .line 132073
    sget-object v0, LX/1Xb;->a:LX/1Xb;

    move-object v0, v0

    .line 132074
    :goto_0
    return-object v0

    .line 132075
    :cond_0
    sget-object v0, LX/1Xb;->b:LX/1Xb;

    move-object v0, v0

    .line 132076
    goto :goto_0
.end method

.method public static a()LX/2FN;
    .locals 1

    .prologue
    .line 132070
    sget-object v0, LX/2FN;->a:LX/2FN;

    move-object v0, v0

    .line 132071
    return-object v0
.end method

.method public static a([B)LX/4rH;
    .locals 1

    .prologue
    .line 132064
    invoke-static {p0}, LX/4rH;->a([B)LX/4rH;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/math/BigDecimal;)LX/0rP;
    .locals 1

    .prologue
    .line 132067
    iget-boolean v0, p0, LX/0mC;->_cfgBigDecimalExact:Z

    if-eqz v0, :cond_0

    .line 132068
    invoke-static {p1}, LX/2zM;->a(Ljava/math/BigDecimal;)LX/2zM;

    move-result-object v0

    .line 132069
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/2zM;->a:LX/2zM;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0}, LX/2zM;->a(Ljava/math/BigDecimal;)LX/2zM;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/162;
    .locals 1

    .prologue
    .line 132066
    new-instance v0, LX/162;

    invoke-direct {v0, p0}, LX/162;-><init>(LX/0mC;)V

    return-object v0
.end method

.method public final c()LX/0m9;
    .locals 1

    .prologue
    .line 132065
    new-instance v0, LX/0m9;

    invoke-direct {v0, p0}, LX/0m9;-><init>(LX/0mC;)V

    return-object v0
.end method
