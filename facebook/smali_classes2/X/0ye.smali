.class public LX/0ye;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field public static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Ljava/lang/Integer;

.field public static final o:Ljava/lang/Integer;

.field private static volatile p:LX/0ye;


# instance fields
.field public b:J
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/49z;",
            "LX/0Rf",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0lC;

.field public g:LX/0W3;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 165785
    const-class v0, LX/0ye;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/0ye;->a:Ljava/lang/String;

    .line 165786
    const/16 v0, 0x2a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "thumbnail"

    aput-object v1, v0, v3

    const-string v1, "map"

    aput-object v1, v0, v4

    const-string v1, "about"

    aput-object v1, v0, v5

    const-string v1, "privacy"

    aput-object v1, v0, v6

    const-string v1, "small_photo"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "search"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "search_typeahead"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "bookmarks"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "event_dashboard"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "event_suggestions"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "event_subscriptions"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "page_events_list"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "event_profile_pic"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "composer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "inline_composer"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "notifications_view"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "dbl"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "reaction_dialog"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "attachment_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "iorg_common"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "qr_code"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "feed_awesomizer"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "bookmark_item_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "app_icon_image"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "notification_image"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "life_event_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "preview"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "iorg_image_view"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "attachment_profile_image"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, ".*megaphone_.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "fbui_content_view_thumbnail"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "map_url_image"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "new_place_creation"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "group_admin_cog_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "landing"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "add_member"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "messenger_image"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "qp_image"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "fresco_impl"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "zero_optin_interstitial"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "dialtone_optin_interstitial"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "lightswitch_optin_interstitial"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ye;->i:Ljava/util/List;

    .line 165787
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "video_cover_image"

    aput-object v1, v0, v3

    const-string v1, "inline_video_cover_image"

    aput-object v1, v0, v4

    const-string v1, "page_identity_video"

    aput-object v1, v0, v5

    const-string v1, "reaction_dialog_videos"

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ye;->j:Ljava/util/List;

    .line 165788
    const/16 v0, 0x1f

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "^/follow/feedsources.*"

    aput-object v1, v0, v3

    const-string v1, "^/ads/preference.*"

    aput-object v1, v0, v4

    const-string v1, "^/settings.*"

    aput-object v1, v0, v5

    const-string v1, "^/help/android-app.*"

    aput-object v1, v0, v6

    const-string v1, "^/\\d.*/allactivity.*"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "^/privacy.*"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "^/about/privacy.*"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "^/policies.*"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "^/about/basics/.*"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "https://m.facebook.com/help/contact/.*"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "^/report.*"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "^/terms.*"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "^/trust.*"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "^/policy.*"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "^/communitystandards.*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "^/ad_guidelines.*"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "^/page_guidelines.*"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "^/payments_terms.*"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "^/help.*"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "^/pages/create.*"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "https://m.facebook.com/groups/create/.*"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "^/invite/history.*"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "https://(www|m).facebook.com/safetycheck.*"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "^(https://m.facebook.com)?/zero/toggle/settings($|\\?.*$|/.*$)"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "https://(www|m).facebook.com/events/birthdays.*"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "https://m.facebook.com/.*/about.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "https://m.facebook.com/timeline/app_section/.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "^/allactivity/options\\?id=.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "^https://m.facebook.com/a/approval_queue/.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "^/survey.*"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "^/legal/thirdpartynotices"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ye;->k:Ljava/util/List;

    .line 165789
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "^(https?)://(m-|z-m-|z-1-|z-n-|)static\\.(ak|xx)\\.fbcdn\\.net/rsrc\\.php($|\\?.*$|/.*$)"

    aput-object v1, v0, v3

    const-string v1, "^file://.*"

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ye;->l:Ljava/util/List;

    .line 165790
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "^.*xx.fbcdn.net\\/safe_image.php?.*w=(\\d+)&h=(\\d+).*"

    aput-object v1, v0, v3

    const-string v1, "$1"

    aput-object v1, v0, v4

    const-string v1, "$2"

    aput-object v1, v0, v5

    const-string v1, "^.*xx.fbcdn.net(\\/.+)*((\\/s|\\/p)(\\d+)x(\\d+))(\\/.+)*?.*"

    aput-object v1, v0, v6

    const-string v1, "$4"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "$5"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/0ye;->m:Ljava/util/List;

    .line 165791
    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/0ye;->n:Ljava/lang/Integer;

    .line 165792
    const/16 v0, 0xc8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/0ye;->o:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0lC;LX/0W3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0lC;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 165776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165777
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/0ye;->c:Ljava/util/Map;

    .line 165778
    iput-object v1, p0, LX/0ye;->d:Ljava/util/List;

    .line 165779
    iput-object v1, p0, LX/0ye;->e:Landroid/util/Pair;

    .line 165780
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/0ye;->b:J

    .line 165781
    iput-object p1, p0, LX/0ye;->h:LX/0Ot;

    .line 165782
    iput-object p2, p0, LX/0ye;->f:LX/0lC;

    .line 165783
    iput-object p3, p0, LX/0ye;->g:LX/0W3;

    .line 165784
    return-void
.end method

.method public static a(LX/0QB;)LX/0ye;
    .locals 6

    .prologue
    .line 165763
    sget-object v0, LX/0ye;->p:LX/0ye;

    if-nez v0, :cond_1

    .line 165764
    const-class v1, LX/0ye;

    monitor-enter v1

    .line 165765
    :try_start_0
    sget-object v0, LX/0ye;->p:LX/0ye;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 165766
    if-eqz v2, :cond_0

    .line 165767
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 165768
    new-instance v5, LX/0ye;

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-direct {v5, p0, v3, v4}, LX/0ye;-><init>(LX/0Ot;LX/0lC;LX/0W3;)V

    .line 165769
    move-object v0, v5

    .line 165770
    sput-object v0, LX/0ye;->p:LX/0ye;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165771
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 165772
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 165773
    :cond_1
    sget-object v0, LX/0ye;->p:LX/0ye;

    return-object v0

    .line 165774
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 165775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165719
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 165720
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 165721
    const/4 v3, 0x2

    invoke-static {v0, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165722
    :cond_0
    return-object v1
.end method

.method private static b(LX/49z;)LX/0Rf;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/49z;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165793
    invoke-virtual {p0}, LX/49z;->getWhitePatternList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0ye;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 9

    .prologue
    .line 165745
    invoke-static {}, LX/49z;->values()[LX/49z;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 165746
    iget-object v0, p0, LX/0ye;->g:LX/0W3;

    invoke-virtual {v4}, LX/49z;->getMobileConfigSpecifier()J

    move-result-wide v6

    const-string v5, "XCONFIG_FETCH_DID_NOT_RETURN_RESULTS"

    invoke-interface {v0, v6, v7, v5}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165747
    const-string v5, "XCONFIG_FETCH_DID_NOT_RETURN_RESULTS"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 165748
    :try_start_0
    iget-object v5, p0, LX/0ye;->f:LX/0lC;

    new-instance v6, LX/49x;

    invoke-direct {v6, p0}, LX/49x;-><init>(LX/0ye;)V

    invoke-virtual {v5, v0, v6}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 165749
    invoke-static {v5}, LX/0ye;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 165750
    :goto_1
    move-object v0, v5

    .line 165751
    if-nez v0, :cond_1

    .line 165752
    iget-object v0, p0, LX/0ye;->c:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    .line 165753
    if-nez v0, :cond_0

    .line 165754
    invoke-static {v4}, LX/0ye;->b(LX/49z;)LX/0Rf;

    move-result-object v0

    .line 165755
    iget-object v5, p0, LX/0ye;->c:Ljava/util/Map;

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165756
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 165757
    :cond_1
    iget-object v5, p0, LX/0ye;->c:Ljava/util/Map;

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 165758
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/0ye;->b:J

    .line 165759
    return-void

    .line 165760
    :catch_0
    move-exception v5

    move-object v6, v5

    .line 165761
    iget-object v5, p0, LX/0ye;->h:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v7, "getWhitelistedTagRegexes"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165762
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/49z;)LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/49z;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165741
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 165742
    iget-wide v2, p0, LX/0ye;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/0ye;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x6ddd00

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 165743
    :cond_0
    invoke-direct {p0}, LX/0ye;->f()V

    .line 165744
    :cond_1
    iget-object v0, p0, LX/0ye;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165723
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 165724
    iget-object v9, p0, LX/0ye;->d:Ljava/util/List;

    if-eqz v9, :cond_0

    iget-wide v9, p0, LX/0ye;->b:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_0

    iget-wide v9, p0, LX/0ye;->b:J

    sub-long/2addr v7, v9

    const-wide/32 v9, 0x6ddd00

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    .line 165725
    :cond_0
    iget-object v7, p0, LX/0ye;->g:LX/0W3;

    sget-wide v9, LX/0X5;->bW:J

    const-string v8, "XCONFIG_FETCH_DID_NOT_RETURN_RESULTS"

    invoke-interface {v7, v9, v10, v8}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 165726
    const-string v8, "XCONFIG_FETCH_DID_NOT_RETURN_RESULTS"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 165727
    :try_start_0
    iget-object v8, p0, LX/0ye;->f:LX/0lC;

    new-instance v9, LX/49y;

    invoke-direct {v9, p0}, LX/49y;-><init>(LX/0ye;)V

    invoke-virtual {v8, v7, v9}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    iput-object v7, p0, LX/0ye;->d:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 165728
    :cond_1
    :goto_0
    iget-object v7, p0, LX/0ye;->d:Ljava/util/List;

    if-nez v7, :cond_2

    .line 165729
    sget-object v7, LX/0ye;->m:Ljava/util/List;

    iput-object v7, p0, LX/0ye;->d:Ljava/util/List;

    .line 165730
    :cond_2
    const/4 v2, 0x0

    .line 165731
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    iget-object v0, p0, LX/0ye;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 165732
    iget-object v0, p0, LX/0ye;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 165733
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 165734
    :try_start_1
    new-instance v1, Landroid/util/Pair;

    iget-object v0, p0, LX/0ye;->d:Ljava/util/List;

    add-int/lit8 v5, v3, 0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v0, p0, LX/0ye;->d:Ljava/util/List;

    add-int/lit8 v6, v3, 0x2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    .line 165735
    :goto_2
    add-int/lit8 v1, v3, 0x3

    move v3, v1

    move-object v2, v0

    goto :goto_1

    .line 165736
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 165737
    iget-object v0, p0, LX/0ye;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "getWhitelistedSizeDimens"

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move-object v0, v2

    goto :goto_2

    .line 165738
    :cond_4
    return-object v2

    .line 165739
    :catch_1
    move-exception v7

    move-object v8, v7

    .line 165740
    iget-object v7, p0, LX/0ye;->h:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/03V;

    const-string v9, "getWhitelistedTagRegexes"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v9, v10, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method
