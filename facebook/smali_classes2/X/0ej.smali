.class public LX/0ej;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:I

.field public final c:Z


# direct methods
.method public constructor <init>(LX/26w;)V
    .locals 2

    .prologue
    .line 103807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103808
    invoke-virtual {p1}, LX/26w;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    .line 103809
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, LX/0ej;->b:I

    .line 103810
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0ej;->c:Z

    .line 103811
    return-void

    .line 103812
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 103840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103841
    iput-object p1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    .line 103842
    invoke-direct {p0}, LX/0ej;->b()V

    .line 103843
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    iput v0, p0, LX/0ej;->b:I

    .line 103844
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/0ej;->c:Z

    .line 103845
    return-void

    .line 103846
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b()V
    .locals 5

    .prologue
    .line 103824
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103825
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 103826
    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 103827
    new-instance v1, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "data.bin is too small to verify: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes less than expected: 20"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103828
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 103829
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 103830
    const v2, -0x5312ff3

    if-eq v1, v2, :cond_1

    .line 103831
    new-instance v0, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected magic: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Expected: -87109619"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103832
    :cond_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 103833
    const v2, 0x20151014

    if-eq v1, v2, :cond_2

    .line 103834
    new-instance v0, LX/5ol;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Expected: 538251284"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103835
    :cond_2
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 103836
    iget-object v2, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103837
    if-eq v0, v1, :cond_3

    .line 103838
    new-instance v2, LX/5ol;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected data.bin size: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Expected: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5ol;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103839
    :cond_3
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized g(LX/0ej;LX/0oc;I)I
    .locals 3

    .prologue
    .line 103817
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/0od;->a:[I

    invoke-virtual {p1}, LX/0oc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 103818
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid authority: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103819
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 103820
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    mul-int/lit8 v1, p2, 0x4

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 103821
    :goto_0
    monitor-exit p0

    return v0

    .line 103822
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ej;->b:I

    add-int/2addr v1, p2

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 103823
    :pswitch_2
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    iget v1, p0, LX/0ej;->b:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a(LX/0oc;IF)F
    .locals 2

    .prologue
    .line 103813
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103814
    if-gez v0, :cond_0

    .line 103815
    :goto_0
    monitor-exit p0

    return p3

    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result p3

    goto :goto_0

    .line 103816
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;II)I
    .locals 2

    .prologue
    .line 103755
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103756
    if-gez v0, :cond_0

    .line 103757
    :goto_0
    monitor-exit p0

    return p3

    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result p3

    goto :goto_0

    .line 103758
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;IJ)J
    .locals 3

    .prologue
    .line 103803
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103804
    if-gez v0, :cond_0

    .line 103805
    :goto_0
    monitor-exit p0

    return-wide p3

    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide p3

    goto :goto_0

    .line 103806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ">(",
            "LX/0oc;",
            "I",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 103788
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103789
    if-gez v0, :cond_1

    .line 103790
    :cond_0
    :goto_0
    monitor-exit p0

    return-object p4

    .line 103791
    :cond_1
    :try_start_1
    iget-object v2, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    .line 103792
    new-array v3, v2, [B

    .line 103793
    iget-object v4, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103794
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 103795
    new-instance v4, Ljava/lang/String;

    sget-object v0, LX/0oe;->a:Ljava/nio/charset/Charset;

    invoke-direct {v4, v3, v0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 103796
    invoke-virtual {p3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    .line 103797
    array-length v3, v0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    .line 103798
    aget-object v1, v0, v2

    .line 103799
    invoke-virtual {v1}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    move-object p4, v1

    .line 103800
    goto :goto_0

    .line 103801
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 103802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 103847
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I

    move-result v0

    .line 103848
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 103849
    new-array v2, v1, [B

    .line 103850
    iget-object v3, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103851
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 103852
    new-instance v0, Ljava/lang/String;

    sget-object v1, LX/0oe;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 103853
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 103779
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103780
    if-gez v0, :cond_0

    .line 103781
    :goto_0
    monitor-exit p0

    return-object p3

    .line 103782
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    .line 103783
    new-array v2, v1, [B

    .line 103784
    iget-object v3, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103785
    iget-object v0, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 103786
    new-instance p3, Ljava/lang/String;

    sget-object v0, LX/0oe;->a:Ljava/nio/charset/Charset;

    invoke-direct {p3, v2, v0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 103787
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Z
    .locals 2

    .prologue
    .line 103778
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-static {p0, v0, p1}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0oc;IZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 103774
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 103775
    if-gez v1, :cond_0

    .line 103776
    :goto_0
    monitor-exit p0

    return p3

    :cond_0
    :try_start_1
    iget-object v2, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-ne v1, v0, :cond_1

    move p3, v0

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    goto :goto_0

    .line 103777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/0oc;I)I
    .locals 2

    .prologue
    .line 103771
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I

    move-result v0

    .line 103772
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 103773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/0oc;I)J
    .locals 2

    .prologue
    .line 103768
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I

    move-result v0

    .line 103769
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    .line 103770
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(LX/0oc;I)F
    .locals 2

    .prologue
    .line 103765
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I

    move-result v0

    .line 103766
    iget-object v1, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 103767
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(LX/0oc;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 103762
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I

    move-result v1

    .line 103763
    iget-object v2, p0, LX/0ej;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 103764
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(LX/0oc;I)Z
    .locals 1

    .prologue
    .line 103759
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/0ej;->g(LX/0ej;LX/0oc;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 103760
    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 103761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
