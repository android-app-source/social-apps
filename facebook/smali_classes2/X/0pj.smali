.class public LX/0pj;
.super LX/0Vx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0pj;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 145130
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 145131
    return-void
.end method

.method public static a(LX/0QB;)LX/0pj;
    .locals 4

    .prologue
    .line 145132
    sget-object v0, LX/0pj;->b:LX/0pj;

    if-nez v0, :cond_1

    .line 145133
    const-class v1, LX/0pj;

    monitor-enter v1

    .line 145134
    :try_start_0
    sget-object v0, LX/0pj;->b:LX/0pj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 145135
    if-eqz v2, :cond_0

    .line 145136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 145137
    new-instance p0, LX/0pj;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-direct {p0, v3}, LX/0pj;-><init>(LX/2WA;)V

    .line 145138
    move-object v0, p0

    .line 145139
    sput-object v0, LX/0pj;->b:LX/0pj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 145141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145142
    :cond_1
    sget-object v0, LX/0pj;->b:LX/0pj;

    return-object v0

    .line 145143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 145144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final W_()V
    .locals 8

    .prologue
    .line 145145
    iget-object v0, p0, LX/0Vx;->b:Ljava/util/concurrent/ConcurrentMap;

    move-object v0, v0

    .line 145146
    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 145147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v3, LX/17N;->BYTES_COUNT:LX/17N;

    invoke-virtual {v3}, LX/17N;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 145148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/0Vx;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 145149
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 145150
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/0Vx;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 145151
    :cond_2
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145152
    const-string v0, "cache_counters"

    return-object v0
.end method
