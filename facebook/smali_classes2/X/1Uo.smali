.class public LX/1Uo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Up;

.field public static final b:LX/1Up;


# instance fields
.field public c:Landroid/content/res/Resources;

.field public d:I

.field public e:F

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:LX/1Up;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:LX/1Up;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:LX/1Up;

.field public l:Landroid/graphics/drawable/Drawable;

.field public m:LX/1Up;

.field public n:LX/1Up;

.field public o:Landroid/graphics/Matrix;

.field public p:Landroid/graphics/PointF;

.field public q:Landroid/graphics/ColorFilter;

.field public r:Landroid/graphics/drawable/Drawable;

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public t:Landroid/graphics/drawable/Drawable;

.field public u:LX/4Ab;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256778
    sget-object v0, LX/1Up;->f:LX/1Up;

    sput-object v0, LX/1Uo;->a:LX/1Up;

    .line 256779
    sget-object v0, LX/1Up;->g:LX/1Up;

    sput-object v0, LX/1Uo;->b:LX/1Up;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 256780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256781
    iput-object p1, p0, LX/1Uo;->c:Landroid/content/res/Resources;

    .line 256782
    invoke-static {p0}, LX/1Uo;->v(LX/1Uo;)V

    .line 256783
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)LX/1Uo;
    .locals 1

    .prologue
    .line 256784
    new-instance v0, LX/1Uo;

    invoke-direct {v0, p0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    return-object v0
.end method

.method public static v(LX/1Uo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256785
    const/16 v0, 0x12c

    iput v0, p0, LX/1Uo;->d:I

    .line 256786
    const/4 v0, 0x0

    iput v0, p0, LX/1Uo;->e:F

    .line 256787
    iput-object v1, p0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 256788
    sget-object v0, LX/1Uo;->a:LX/1Up;

    iput-object v0, p0, LX/1Uo;->g:LX/1Up;

    .line 256789
    iput-object v1, p0, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 256790
    sget-object v0, LX/1Uo;->a:LX/1Up;

    iput-object v0, p0, LX/1Uo;->i:LX/1Up;

    .line 256791
    iput-object v1, p0, LX/1Uo;->j:Landroid/graphics/drawable/Drawable;

    .line 256792
    sget-object v0, LX/1Uo;->a:LX/1Up;

    iput-object v0, p0, LX/1Uo;->k:LX/1Up;

    .line 256793
    iput-object v1, p0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 256794
    sget-object v0, LX/1Uo;->a:LX/1Up;

    iput-object v0, p0, LX/1Uo;->m:LX/1Up;

    .line 256795
    sget-object v0, LX/1Uo;->b:LX/1Up;

    iput-object v0, p0, LX/1Uo;->n:LX/1Up;

    .line 256796
    iput-object v1, p0, LX/1Uo;->o:Landroid/graphics/Matrix;

    .line 256797
    iput-object v1, p0, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 256798
    iput-object v1, p0, LX/1Uo;->q:Landroid/graphics/ColorFilter;

    .line 256799
    iput-object v1, p0, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    .line 256800
    iput-object v1, p0, LX/1Uo;->s:Ljava/util/List;

    .line 256801
    iput-object v1, p0, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    .line 256802
    iput-object v1, p0, LX/1Uo;->u:LX/4Ab;

    .line 256803
    return-void
.end method


# virtual methods
.method public final a(LX/4Ab;)LX/1Uo;
    .locals 0
    .param p1    # LX/4Ab;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256804
    iput-object p1, p0, LX/1Uo;->u:LX/4Ab;

    .line 256805
    return-object p0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;
    .locals 0
    .param p2    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256806
    iput-object p1, p0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 256807
    iput-object p2, p0, LX/1Uo;->g:LX/1Up;

    .line 256808
    return-object p0
.end method

.method public final b(I)LX/1Uo;
    .locals 1

    .prologue
    .line 256809
    iget-object v0, p0, LX/1Uo;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 256810
    return-object p0
.end method

.method public final e(LX/1Up;)LX/1Uo;
    .locals 1
    .param p1    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256811
    iput-object p1, p0, LX/1Uo;->n:LX/1Up;

    .line 256812
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Uo;->o:Landroid/graphics/Matrix;

    .line 256813
    return-object p0
.end method

.method public final f(Landroid/graphics/drawable/Drawable;)LX/1Uo;
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256814
    if-nez p1, :cond_0

    .line 256815
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Uo;->s:Ljava/util/List;

    .line 256816
    :goto_0
    return-object p0

    .line 256817
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/1Uo;->s:Ljava/util/List;

    goto :goto_0
.end method

.method public final g(Landroid/graphics/drawable/Drawable;)LX/1Uo;
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 256818
    if-nez p1, :cond_0

    .line 256819
    const/4 v0, 0x0

    iput-object v0, p0, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    .line 256820
    :goto_0
    return-object p0

    .line 256821
    :cond_0
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 256822
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x10100a7

    aput v3, v1, v2

    invoke-virtual {v0, v1, p1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 256823
    iput-object v0, p0, LX/1Uo;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final u()LX/1af;
    .locals 2

    .prologue
    .line 256824
    iget-object v0, p0, LX/1Uo;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 256825
    iget-object v0, p0, LX/1Uo;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 256826
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 256827
    :cond_0
    new-instance v0, LX/1af;

    invoke-direct {v0, p0}, LX/1af;-><init>(LX/1Uo;)V

    return-object v0
.end method
