.class public LX/0af;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicIntegerArray;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 85315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85316
    new-instance v0, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicIntegerArray;-><init>(I)V

    iput-object v0, p0, LX/0af;->a:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    .line 85317
    invoke-virtual {p0}, LX/0af;->a()V

    .line 85318
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85319
    iget-object v0, p0, LX/0af;->a:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->length()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 85320
    iget-object v3, p0, LX/0af;->a:Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerArray;->set(II)V

    .line 85321
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85322
    :cond_0
    return-void
.end method
