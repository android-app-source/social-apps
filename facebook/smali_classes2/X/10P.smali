.class public LX/10P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hy;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/10P;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0lC;

.field public c:LX/0fJ;

.field private d:LX/03V;

.field private e:LX/0tF;


# direct methods
.method public constructor <init>(LX/0aU;LX/0lC;LX/0fJ;LX/03V;LX/0tF;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 169128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169129
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    iput-object v0, p0, LX/10P;->b:LX/0lC;

    .line 169130
    iput-object p3, p0, LX/10P;->c:LX/0fJ;

    .line 169131
    iput-object p4, p0, LX/10P;->d:LX/03V;

    .line 169132
    iput-object p5, p0, LX/10P;->e:LX/0tF;

    .line 169133
    const-string v0, "VIEW_PERMALINK"

    invoke-virtual {p1, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/10P;->a:Ljava/lang/String;

    .line 169134
    return-void
.end method

.method public static a(LX/0QB;)LX/10P;
    .locals 9

    .prologue
    .line 169115
    sget-object v0, LX/10P;->f:LX/10P;

    if-nez v0, :cond_1

    .line 169116
    const-class v1, LX/10P;

    monitor-enter v1

    .line 169117
    :try_start_0
    sget-object v0, LX/10P;->f:LX/10P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 169118
    if-eqz v2, :cond_0

    .line 169119
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 169120
    new-instance v3, LX/10P;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v4

    check-cast v4, LX/0aU;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v0}, LX/0ns;->a(LX/0QB;)LX/0ns;

    move-result-object v6

    check-cast v6, LX/0fJ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v8

    check-cast v8, LX/0tF;

    invoke-direct/range {v3 .. v8}, LX/10P;-><init>(LX/0aU;LX/0lC;LX/0fJ;LX/03V;LX/0tF;)V

    .line 169121
    move-object v0, v3

    .line 169122
    sput-object v0, LX/10P;->f:LX/10P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 169124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169125
    :cond_1
    sget-object v0, LX/10P;->f:LX/10P;

    return-object v0

    .line 169126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 169127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 168937
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 168938
    const-string v0, "extra_permalink_param_type"

    sget-object v2, LX/89m;->NOTIF_STORY_ID_KEY:LX/89m;

    invoke-virtual {v2}, LX/89m;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168939
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    move-object v0, v0

    .line 168940
    if-eqz v0, :cond_0

    .line 168941
    const-string v0, "permalink_cache_type"

    .line 168942
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    move-object v2, v2

    .line 168943
    invoke-virtual {v2}, LX/89g;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168944
    :cond_0
    const-string v0, "story_cache_id"

    .line 168945
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 168946
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168947
    const-string v0, "story_id"

    .line 168948
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 168949
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168950
    const-string v0, "include_comments_disabled_fields"

    invoke-virtual {p1}, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168951
    const-string v0, "show_keyboard_on_first_load"

    invoke-virtual {p1}, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168952
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    move-object v0, v0

    .line 168953
    if-eqz v0, :cond_2

    .line 168954
    const-string v0, "default_comment_ordering"

    .line 168955
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    move-object v2, v2

    .line 168956
    invoke-virtual {v2}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168957
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->h:Ljava/lang/String;

    move-object v0, v0

    .line 168958
    if-eqz v0, :cond_1

    .line 168959
    const-string v0, "story_fbid"

    .line 168960
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 168961
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168962
    :cond_1
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 168963
    if-eqz v0, :cond_2

    .line 168964
    const-string v0, "relevant_comment_id"

    .line 168965
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 168966
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168967
    const-string v0, "relevant_comment"

    .line 168968
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v2, v2

    .line 168969
    invoke-static {v1, v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 168970
    :cond_2
    sget-object v0, LX/0cQ;->NATIVE_PERMALINK_PAGE_FRAGMENT:LX/0cQ;

    .line 168971
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 168972
    if-eqz v2, :cond_3

    .line 168973
    const-string v2, "feedback_id"

    .line 168974
    iget-object v3, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->f:Ljava/lang/String;

    move-object v3, v3

    .line 168975
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168976
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 168977
    if-eqz v2, :cond_3

    .line 168978
    const-string v2, "comment"

    .line 168979
    iget-object v3, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v3

    .line 168980
    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 168981
    const-string v2, "comment_id"

    .line 168982
    iget-object v3, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 168983
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168984
    iget-object v2, p0, LX/10P;->e:LX/0tF;

    const/4 v3, 0x1

    .line 168985
    invoke-virtual {v2}, LX/0tF;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, v2, LX/0tF;->a:LX/0ad;

    sget-short p0, LX/0wn;->aa:S

    invoke-interface {v4, p0, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_7

    :goto_0
    move v2, v3

    .line 168986
    if-nez v2, :cond_3

    .line 168987
    sget-object v0, LX/0cQ;->COMMENT_PERMALINK_FRAGMENT:LX/0cQ;

    .line 168988
    :cond_3
    const-string v2, "target_fragment"

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168989
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->i:Ljava/lang/String;

    move-object v0, v0

    .line 168990
    if-eqz v0, :cond_4

    .line 168991
    const-string v0, "group_id"

    .line 168992
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 168993
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168994
    :cond_4
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    move-object v0, v0

    .line 168995
    if-eqz v0, :cond_5

    .line 168996
    const-string v0, "notification_source"

    .line 168997
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    move-object v2, v2

    .line 168998
    invoke-virtual {v2}, LX/21C;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168999
    :cond_5
    const-string v0, "relevant_reaction_key"

    .line 169000
    iget v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->p:I

    move v2, v2

    .line 169001
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169002
    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->q:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v0

    .line 169003
    if-eqz v0, :cond_6

    .line 169004
    const-string v0, "feedback_logging_params"

    .line 169005
    iget-object v2, p1, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->q:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v2, v2

    .line 169006
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 169007
    :cond_6
    return-object v1

    :cond_7
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/ipc/feed/ViewPermalinkParams;)Landroid/os/Bundle;
    .locals 7

    .prologue
    .line 169057
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 169058
    iget-object v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->b:Lcom/facebook/graphql/model/FeedUnit;

    move-object v2, v0

    .line 169059
    iget-boolean v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->c:Z

    move v1, v0

    .line 169060
    if-eqz v1, :cond_5

    .line 169061
    :try_start_0
    instance-of v1, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_3

    .line 169062
    move-object v0, v2

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v0

    .line 169063
    sget-object v4, LX/89m;->AD_PREVIEW_STORY_JSON:LX/89m;

    invoke-virtual {v4}, LX/89m;->name()Ljava/lang/String;

    move-result-object v4

    .line 169064
    invoke-static {v1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    .line 169065
    if-eqz v5, :cond_0

    .line 169066
    const-string v6, "default_comment_ordering"

    invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169067
    const-string v5, "story_fbid"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v1, v4

    .line 169068
    :goto_0
    const-string v4, "extra_permalink_param_type"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169069
    const-string v1, "permalink_story"

    iget-object v4, p0, LX/10P;->b:LX/0lC;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169070
    :cond_1
    :goto_1
    const-string v1, "use_photo_mode"

    .line 169071
    iget-boolean v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->d:Z

    move v2, v0

    .line 169072
    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169073
    iget-boolean v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->d:Z

    move v1, v0

    .line 169074
    if-eqz v1, :cond_2

    .line 169075
    const-string v1, "title_bar_background_color_id"

    const v2, 0x7f0a00cb

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169076
    const-string v1, "fragment_background_color_id"

    const v2, 0x7f0a00cb

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169077
    :cond_2
    const-string v1, "is_from_deferred feedback"

    .line 169078
    iget-boolean v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->e:Z

    move v2, v0

    .line 169079
    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169080
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->NATIVE_PERMALINK_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v1, v3

    .line 169081
    :goto_2
    return-object v1

    .line 169082
    :cond_3
    :try_start_1
    instance-of v1, v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v1, :cond_4

    .line 169083
    sget-object v1, LX/89m;->AD_PREVIEW_PYML_JSON:LX/89m;

    invoke-virtual {v1}, LX/89m;->name()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    :cond_4
    move-object v1, v3

    .line 169084
    goto :goto_2

    .line 169085
    :catch_0
    move-exception v1

    .line 169086
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 169087
    :cond_5
    instance-of v1, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_6

    .line 169088
    iget-object v1, p0, LX/10P;->d:LX/03V;

    const-class v2, LX/0hy;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Permalink only supports GraphQLStory outside of an ad preview"

    invoke-virtual {v1, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 169089
    goto :goto_2

    .line 169090
    :cond_6
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 169091
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 169092
    const-string v1, "content_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169093
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 169094
    iget-boolean v0, p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->e:Z

    move v1, v0

    .line 169095
    if-nez v1, :cond_9

    .line 169096
    const-string v1, "extra_permalink_param_type"

    sget-object v4, LX/89m;->FEED_STORY_ID_KEY:LX/89m;

    invoke-virtual {v4}, LX/89m;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169097
    const-string v1, "story_cache_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169098
    const-string v1, "story_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169099
    :goto_3
    invoke-static {v2}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 169100
    if-eqz v1, :cond_8

    .line 169101
    const-string v4, "default_comment_ordering"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169102
    const-string v1, "story_fbid"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169103
    :cond_8
    const-string v1, "include_comments_disabled_fields"

    invoke-static {v2}, LX/16z;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169104
    invoke-static {v2}, LX/0x1;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v1

    .line 169105
    if-eqz v1, :cond_1

    .line 169106
    const-string v2, "search_results_decoration"

    invoke-static {v3, v2, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 169107
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 169108
    const-string v1, "extra_permalink_param_type"

    sget-object v4, LX/89m;->PLATFORM_KEY:LX/89m;

    invoke-virtual {v4}, LX/89m;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169109
    const-string v1, "extra_platform_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169110
    const-string v1, "extra_fallback_url"

    const-string v4, ""

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 169111
    :cond_a
    :try_start_2
    const-string v1, "extra_permalink_param_type"

    sget-object v4, LX/89m;->FEED_STORY_JSON:LX/89m;

    invoke-virtual {v4}, LX/89m;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169112
    const-string v1, "permalink_story"

    iget-object v4, p0, LX/10P;->b:LX/0lC;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 169113
    :catch_1
    move-exception v1

    .line 169114
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/content/ComponentName;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 169049
    if-nez p1, :cond_0

    .line 169050
    new-instance v0, Landroid/content/Intent;

    .line 169051
    iget-object v1, p0, LX/10P;->a:Ljava/lang/String;

    move-object v1, v1

    .line 169052
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169053
    :goto_0
    invoke-direct {p0, p2}, LX/10P;->b(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 169054
    iget-object v1, p0, LX/10P;->c:LX/0fJ;

    invoke-interface {v1, v0}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 169055
    return-object v0

    .line 169056
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/ViewPermalinkParams;)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/content/ComponentName;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 169041
    if-nez p1, :cond_0

    .line 169042
    new-instance v0, Landroid/content/Intent;

    .line 169043
    iget-object v1, p0, LX/10P;->a:Ljava/lang/String;

    move-object v1, v1

    .line 169044
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169045
    :goto_0
    invoke-direct {p0, p2}, LX/10P;->b(Lcom/facebook/ipc/feed/ViewPermalinkParams;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 169046
    iget-object v1, p0, LX/10P;->c:LX/0fJ;

    invoke-interface {v1, v0}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 169047
    return-object v0

    .line 169048
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 169026
    const/4 v0, 0x0

    .line 169027
    if-nez v0, :cond_0

    .line 169028
    new-instance v1, Landroid/content/Intent;

    .line 169029
    iget-object v2, p0, LX/10P;->a:Ljava/lang/String;

    move-object v2, v2

    .line 169030
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169031
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 169032
    const-string v3, "extra_permalink_param_type"

    sget-object v0, LX/89m;->STORY_FBID_KEY:LX/89m;

    invoke-virtual {v0}, LX/89m;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169033
    const-string v3, "story_fbid"

    iget-object v0, p1, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169034
    const-string v3, "target_fragment"

    sget-object v0, LX/0cQ;->NATIVE_PERMALINK_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169035
    move-object v2, v2

    .line 169036
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 169037
    iget-object v2, p0, LX/10P;->c:LX/0fJ;

    invoke-interface {v2, v1}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 169038
    move-object v0, v1

    .line 169039
    return-object v0

    .line 169040
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 169025
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, LX/10P;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 169023
    check-cast p1, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 169024
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, LX/10P;->a(Landroid/content/ComponentName;Lcom/facebook/ipc/feed/ViewPermalinkParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 169011
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 169012
    const-string v1, "extra_permalink_param_type"

    sget-object v2, LX/89m;->FEED_STORY_ID_KEY:LX/89m;

    invoke-virtual {v2}, LX/89m;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169013
    const-string v1, "story_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169014
    const-string v1, "include_comments_disabled_fields"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169015
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->NATIVE_PERMALINK_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169016
    new-instance v1, Landroid/content/Intent;

    .line 169017
    iget-object v2, p0, LX/10P;->a:Ljava/lang/String;

    move-object v2, v2

    .line 169018
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169019
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 169020
    iget-object v2, p0, LX/10P;->c:LX/0fJ;

    invoke-interface {v2, v1}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 169021
    move-object v0, v1

    .line 169022
    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 169008
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 169009
    iget-object v1, p0, LX/10P;->a:Ljava/lang/String;

    move-object v1, v1

    .line 169010
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
