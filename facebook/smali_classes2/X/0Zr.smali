.class public LX/0Zr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-android.os.HandlerThread._Constructor"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/regex/Pattern;

.field public static final d:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/0Zr;


# instance fields
.field public final c:LX/0Sj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83951
    const-class v0, LX/0Zr;

    sput-object v0, LX/0Zr;->a:Ljava/lang/Class;

    .line 83952
    const-string v0, "^>>>>> Dispatching to (\\w+)(?:\\{\\w+\\})?\\s*(?:\\(([\\w\\$\\.]+)\\))?\\s*(?:\\{[0-9a-f]+\\})? ([^@]+)(?:@\\w+)?: (\\d+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/0Zr;->b:Ljava/util/regex/Pattern;

    .line 83953
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LX/0Zr;->d:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(LX/0Sj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 83926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83927
    iput-object p1, p0, LX/0Zr;->c:LX/0Sj;

    .line 83928
    return-void
.end method

.method public static a(LX/0QB;)LX/0Zr;
    .locals 4

    .prologue
    .line 83929
    sget-object v0, LX/0Zr;->e:LX/0Zr;

    if-nez v0, :cond_1

    .line 83930
    const-class v1, LX/0Zr;

    monitor-enter v1

    .line 83931
    :try_start_0
    sget-object v0, LX/0Zr;->e:LX/0Zr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 83932
    if-eqz v2, :cond_0

    .line 83933
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 83934
    new-instance p0, LX/0Zr;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v3

    check-cast v3, LX/0Sj;

    invoke-direct {p0, v3}, LX/0Zr;-><init>(LX/0Sj;)V

    .line 83935
    move-object v0, p0

    .line 83936
    sput-object v0, LX/0Zr;->e:LX/0Zr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83937
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 83938
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 83939
    :cond_1
    sget-object v0, LX/0Zr;->e:LX/0Zr;

    return-object v0

    .line 83940
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 83941
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 83942
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Looper;->setMessageLogging(Landroid/util/Printer;)V

    .line 83943
    return-void
.end method

.method public static a$redex0(LX/0Zr;Ljava/lang/String;Landroid/os/Looper;Z)V
    .locals 1

    .prologue
    .line 83944
    if-nez p3, :cond_0

    .line 83945
    invoke-static {p2}, LX/0Zr;->a(Landroid/os/Looper;)V

    .line 83946
    :goto_0
    return-void

    .line 83947
    :cond_0
    new-instance v0, LX/22h;

    invoke-direct {v0, p0, p2, p1}, LX/22h;-><init>(LX/0Zr;Landroid/os/Looper;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/os/Looper;->setMessageLogging(Landroid/util/Printer;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/HandlerThread;
    .locals 3

    .prologue
    .line 83948
    new-instance v0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;

    sget-object v1, LX/0TP;->NORMAL:LX/0TP;

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;-><init>(LX/0Zr;Ljava/lang/String;LX/0TP;Z)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;
    .locals 2

    .prologue
    .line 83949
    new-instance v0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;-><init>(LX/0Zr;Ljava/lang/String;LX/0TP;Z)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0TP;Z)Landroid/os/HandlerThread;
    .locals 1

    .prologue
    .line 83950
    new-instance v0, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/common/executors/FbHandlerThreadFactory$FbHandlerThread;-><init>(LX/0Zr;Ljava/lang/String;LX/0TP;Z)V

    return-object v0
.end method
