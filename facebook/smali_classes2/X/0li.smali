.class public final LX/0li;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:LX/0li;

.field public static final b:LX/0lH;

.field public static final c:LX/0lH;

.field public static final d:LX/0lH;

.field public static final e:LX/0lH;

.field private static final h:[LX/0lJ;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _modifiers:[LX/0nn;

.field public final _parser:LX/0lk;

.field public final _typeCache:LX/0lj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0lj",
            "<",
            "LX/1Xc;",
            "LX/0lJ;",
            ">;"
        }
    .end annotation
.end field

.field public transient f:LX/1Y4;

.field public transient g:LX/1Y4;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 130134
    const/4 v0, 0x0

    new-array v0, v0, [LX/0lJ;

    sput-object v0, LX/0li;->h:[LX/0lJ;

    .line 130135
    new-instance v0, LX/0li;

    invoke-direct {v0}, LX/0li;-><init>()V

    sput-object v0, LX/0li;->a:LX/0li;

    .line 130136
    new-instance v0, LX/0lH;

    const-class v1, Ljava/lang/String;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/0li;->b:LX/0lH;

    .line 130137
    new-instance v0, LX/0lH;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/0li;->c:LX/0lH;

    .line 130138
    new-instance v0, LX/0lH;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/0li;->d:LX/0lH;

    .line 130139
    new-instance v0, LX/0lH;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/0li;->e:LX/0lH;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 130258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130259
    new-instance v0, LX/0lj;

    const/16 v1, 0x10

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/0lj;-><init>(II)V

    iput-object v0, p0, LX/0li;->_typeCache:LX/0lj;

    .line 130260
    new-instance v0, LX/0lk;

    invoke-direct {v0, p0}, LX/0lk;-><init>(LX/0li;)V

    iput-object v0, p0, LX/0li;->_parser:LX/0lk;

    .line 130261
    const/4 v0, 0x0

    iput-object v0, p0, LX/0li;->_modifiers:[LX/0nn;

    .line 130262
    return-void
.end method

.method private constructor <init>(LX/0lk;[LX/0nn;)V
    .locals 3

    .prologue
    .line 130263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130264
    new-instance v0, LX/0lj;

    const/16 v1, 0x10

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/0lj;-><init>(II)V

    iput-object v0, p0, LX/0li;->_typeCache:LX/0lj;

    .line 130265
    iput-object p1, p0, LX/0li;->_parser:LX/0lk;

    .line 130266
    iput-object p2, p0, LX/0li;->_modifiers:[LX/0nn;

    .line 130267
    return-void
.end method

.method public static a(Ljava/lang/Class;)LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130268
    new-instance v0, LX/0lH;

    invoke-direct {v0, p0}, LX/0lH;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Class;[LX/0lJ;)LX/0lJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;[",
            "LX/0lJ;",
            ")",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 130269
    invoke-virtual {p0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v1

    .line 130270
    array-length v0, v1

    array-length v2, p1

    if-eq v0, v2, :cond_0

    .line 130271
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Parameter type mismatch for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v1, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " parameters, was given "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130272
    :cond_0
    array-length v0, v1

    new-array v2, v0, [Ljava/lang/String;

    .line 130273
    array-length v3, v1

    move v0, v6

    :goto_0
    if-ge v0, v3, :cond_1

    .line 130274
    aget-object v5, v1, v0

    invoke-interface {v5}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    .line 130275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130276
    :cond_1
    new-instance v0, LX/0lH;

    move-object v1, p0

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/0lH;-><init>(Ljava/lang/Class;[Ljava/lang/String;[LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 130277
    return-object v0
.end method

.method private a(Ljava/lang/reflect/GenericArrayType;LX/1Y3;)LX/0lJ;
    .locals 1

    .prologue
    .line 130278
    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    .line 130279
    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/reflect/ParameterizedType;LX/1Y3;)LX/0lJ;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 130280
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 130281
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    .line 130282
    if-nez v5, :cond_1

    move v4, v3

    .line 130283
    :goto_0
    if-nez v4, :cond_2

    .line 130284
    sget-object v1, LX/0li;->h:[LX/0lJ;

    .line 130285
    :cond_0
    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 130286
    invoke-static {v0, v1}, LX/0li;->a(Ljava/lang/Class;[LX/0lJ;)LX/0lJ;

    move-result-object v1

    .line 130287
    const-class v2, Ljava/util/Map;

    invoke-virtual {p0, v1, v2}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v1

    .line 130288
    array-length v2, v1

    const/4 v4, 0x2

    if-eq v2, v4, :cond_3

    .line 130289
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find 2 type parameters for Map class "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (found "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130290
    :cond_1
    array-length v1, v5

    move v4, v1

    goto :goto_0

    .line 130291
    :cond_2
    new-array v1, v4, [LX/0lJ;

    move v2, v3

    .line 130292
    :goto_1
    if-ge v2, v4, :cond_0

    .line 130293
    aget-object v6, v5, v2

    invoke-virtual {p0, v6, p2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v6

    aput-object v6, v1, v2

    .line 130294
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 130295
    :cond_3
    aget-object v2, v1, v3

    aget-object v1, v1, v7

    invoke-static {v0, v2, v1}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v0

    .line 130296
    :goto_2
    return-object v0

    .line 130297
    :cond_4
    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 130298
    invoke-static {v0, v1}, LX/0li;->a(Ljava/lang/Class;[LX/0lJ;)LX/0lJ;

    move-result-object v1

    .line 130299
    const-class v2, Ljava/util/Collection;

    invoke-virtual {p0, v1, v2}, LX/0li;->b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v1

    .line 130300
    array-length v2, v1

    if-eq v2, v7, :cond_5

    .line 130301
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find 1 type parameter for Collection class "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (found "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130302
    :cond_5
    aget-object v1, v1, v3

    invoke-static {v0, v1}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v0

    goto :goto_2

    .line 130303
    :cond_6
    if-nez v4, :cond_7

    .line 130304
    new-instance v1, LX/0lH;

    invoke-direct {v1, v0}, LX/0lH;-><init>(Ljava/lang/Class;)V

    move-object v0, v1

    goto :goto_2

    .line 130305
    :cond_7
    invoke-static {v0, v1}, LX/0li;->a(Ljava/lang/Class;[LX/0lJ;)LX/0lJ;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Ljava/lang/reflect/TypeVariable;LX/1Y3;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/TypeVariable",
            "<*>;",
            "LX/1Y3;",
            ")",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130248
    if-nez p2, :cond_1

    .line 130249
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    .line 130250
    :cond_0
    :goto_0
    return-object v0

    .line 130251
    :cond_1
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v1

    .line 130252
    invoke-virtual {p2, v1}, LX/1Y3;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    .line 130253
    if-nez v0, :cond_0

    .line 130254
    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 130255
    invoke-virtual {p2, v1}, LX/1Y3;->b(Ljava/lang/String;)V

    .line 130256
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0, p2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/WildcardType;LX/1Y3;)LX/0lJ;
    .locals 2

    .prologue
    .line 130306
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0, p2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(LX/1Y4;)LX/1Y4;
    .locals 2

    .prologue
    .line 130307
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0li;->f:LX/1Y4;

    if-nez v0, :cond_0

    .line 130308
    invoke-virtual {p1}, LX/1Y4;->a()LX/1Y4;

    move-result-object v0

    .line 130309
    const-class v1, Ljava/util/Map;

    invoke-direct {p0, v0, v1}, LX/0li;->a(LX/1Y4;Ljava/lang/Class;)LX/1Y4;

    .line 130310
    iget-object v1, v0, LX/1Y4;->d:LX/1Y4;

    move-object v0, v1

    .line 130311
    iput-object v0, p0, LX/0li;->f:LX/1Y4;

    .line 130312
    :cond_0
    iget-object v0, p0, LX/0li;->f:LX/1Y4;

    invoke-virtual {v0}, LX/1Y4;->a()LX/1Y4;

    move-result-object v0

    .line 130313
    iput-object v0, p1, LX/1Y4;->d:LX/1Y4;

    .line 130314
    iput-object p1, v0, LX/1Y4;->e:LX/1Y4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130315
    monitor-exit p0

    return-object p1

    .line 130316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LX/1Y4;Ljava/lang/Class;)LX/1Y4;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Y4;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Y4;"
        }
    .end annotation

    .prologue
    .line 130317
    iget-object v0, p1, LX/1Y4;->b:Ljava/lang/Class;

    move-object v1, v0

    .line 130318
    invoke-virtual {v1}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 130319
    if-eqz v2, :cond_1

    .line 130320
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 130321
    invoke-direct {p0, v4, p2}, LX/0li;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;

    move-result-object v4

    .line 130322
    if-eqz v4, :cond_0

    .line 130323
    iput-object p1, v4, LX/1Y4;->e:LX/1Y4;

    .line 130324
    iput-object v4, p1, LX/1Y4;->d:LX/1Y4;

    .line 130325
    :goto_1
    return-object p1

    .line 130326
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130327
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 130328
    if-eqz v0, :cond_2

    .line 130329
    invoke-direct {p0, v0, p2}, LX/0li;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;

    move-result-object v0

    .line 130330
    if-eqz v0, :cond_2

    .line 130331
    iput-object p1, v0, LX/1Y4;->e:LX/1Y4;

    .line 130332
    iput-object v0, p1, LX/1Y4;->d:LX/1Y4;

    .line 130333
    goto :goto_1

    .line 130334
    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Y4;"
        }
    .end annotation

    .prologue
    .line 130335
    new-instance v0, LX/1Y4;

    invoke-direct {v0, p1}, LX/1Y4;-><init>(Ljava/lang/reflect/Type;)V

    .line 130336
    iget-object v1, v0, LX/1Y4;->b:Ljava/lang/Class;

    move-object v1, v1

    .line 130337
    if-ne v1, p2, :cond_0

    .line 130338
    :goto_0
    return-object v0

    .line 130339
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 130340
    if-eqz v1, :cond_1

    .line 130341
    invoke-direct {p0, v1, p2}, LX/0li;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;

    move-result-object v1

    .line 130342
    if-eqz v1, :cond_1

    .line 130343
    iput-object v0, v1, LX/1Y4;->e:LX/1Y4;

    .line 130344
    iput-object v1, v0, LX/1Y4;->d:LX/1Y4;

    .line 130345
    goto :goto_0

    .line 130346
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Class;LX/1Y3;)[LX/0lJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/1Y3;",
            ")[",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130347
    invoke-direct {p0, p1, p2}, LX/0li;->c(Ljava/lang/Class;Ljava/lang/Class;)LX/1Y4;

    move-result-object v0

    .line 130348
    if-nez v0, :cond_1

    .line 130349
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a subtype of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object p3, v1

    .line 130350
    :cond_1
    iget-object v1, v0, LX/1Y4;->d:LX/1Y4;

    move-object v1, v1

    .line 130351
    if-eqz v1, :cond_2

    .line 130352
    iget-object v1, v0, LX/1Y4;->d:LX/1Y4;

    move-object v0, v1

    .line 130353
    iget-object v1, v0, LX/1Y4;->b:Ljava/lang/Class;

    move-object v2, v1

    .line 130354
    new-instance v1, LX/1Y3;

    invoke-direct {v1, p0, v2}, LX/1Y3;-><init>(LX/0li;Ljava/lang/Class;)V

    .line 130355
    invoke-virtual {v0}, LX/1Y4;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 130356
    iget-object v3, v0, LX/1Y4;->c:Ljava/lang/reflect/ParameterizedType;

    move-object v3, v3

    .line 130357
    invoke-interface {v3}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    .line 130358
    invoke-virtual {v2}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v4

    .line 130359
    array-length v5, v3

    .line 130360
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    .line 130361
    aget-object v6, v4, v2

    invoke-interface {v6}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v6

    .line 130362
    aget-object v7, v3, v2

    invoke-virtual {p0, v7, p3}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v7

    .line 130363
    invoke-virtual {v1, v6, v7}, LX/1Y3;->a(Ljava/lang/String;LX/0lJ;)V

    .line 130364
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 130365
    :cond_2
    invoke-virtual {v0}, LX/1Y4;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 130366
    const/4 v0, 0x0

    .line 130367
    :goto_1
    return-object v0

    :cond_3
    invoke-virtual {p3}, LX/1Y3;->b()[LX/0lJ;

    move-result-object v0

    goto :goto_1
.end method

.method private declared-synchronized b(LX/1Y4;)LX/1Y4;
    .locals 2

    .prologue
    .line 130368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0li;->g:LX/1Y4;

    if-nez v0, :cond_0

    .line 130369
    invoke-virtual {p1}, LX/1Y4;->a()LX/1Y4;

    move-result-object v0

    .line 130370
    const-class v1, Ljava/util/List;

    invoke-direct {p0, v0, v1}, LX/0li;->a(LX/1Y4;Ljava/lang/Class;)LX/1Y4;

    .line 130371
    iget-object v1, v0, LX/1Y4;->d:LX/1Y4;

    move-object v0, v1

    .line 130372
    iput-object v0, p0, LX/0li;->g:LX/1Y4;

    .line 130373
    :cond_0
    iget-object v0, p0, LX/0li;->g:LX/1Y4;

    invoke-virtual {v0}, LX/1Y4;->a()LX/1Y4;

    move-result-object v0

    .line 130374
    iput-object v0, p1, LX/1Y4;->d:LX/1Y4;

    .line 130375
    iput-object p1, v0, LX/1Y4;->e:LX/1Y4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130376
    monitor-exit p0

    return-object p1

    .line 130377
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Y4;"
        }
    .end annotation

    .prologue
    .line 130378
    new-instance v0, LX/1Y4;

    invoke-direct {v0, p1}, LX/1Y4;-><init>(Ljava/lang/reflect/Type;)V

    .line 130379
    iget-object v1, v0, LX/1Y4;->b:Ljava/lang/Class;

    move-object v1, v1

    .line 130380
    if-ne v1, p2, :cond_0

    .line 130381
    new-instance v0, LX/1Y4;

    invoke-direct {v0, p1}, LX/1Y4;-><init>(Ljava/lang/reflect/Type;)V

    .line 130382
    :goto_0
    return-object v0

    .line 130383
    :cond_0
    const-class v2, Ljava/util/HashMap;

    if-ne v1, v2, :cond_1

    .line 130384
    const-class v2, Ljava/util/Map;

    if-ne p2, v2, :cond_1

    .line 130385
    invoke-direct {p0, v0}, LX/0li;->a(LX/1Y4;)LX/1Y4;

    move-result-object v0

    goto :goto_0

    .line 130386
    :cond_1
    const-class v2, Ljava/util/ArrayList;

    if-ne v1, v2, :cond_2

    .line 130387
    const-class v1, Ljava/util/List;

    if-ne p2, v1, :cond_2

    .line 130388
    invoke-direct {p0, v0}, LX/0li;->b(LX/1Y4;)LX/1Y4;

    move-result-object v0

    goto :goto_0

    .line 130389
    :cond_2
    invoke-direct {p0, v0, p2}, LX/0li;->a(LX/1Y4;Ljava/lang/Class;)LX/1Y4;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/Class;Ljava/lang/Class;)[LX/0lJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)[",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130390
    new-instance v0, LX/1Y3;

    invoke-direct {v0, p0, p1}, LX/1Y3;-><init>(LX/0li;Ljava/lang/Class;)V

    invoke-direct {p0, p1, p2, v0}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;LX/1Y3;)[LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public static c()LX/0lJ;
    .locals 2

    .prologue
    .line 130257
    new-instance v0, LX/0lH;

    const-class v1, Ljava/lang/Object;

    invoke-direct {v0, v1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private c(Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130127
    const-class v0, Ljava/util/Map;

    invoke-direct {p0, p1, v0}, LX/0li;->b(Ljava/lang/Class;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v0

    .line 130128
    if-nez v0, :cond_0

    .line 130129
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v0

    .line 130130
    :goto_0
    return-object v0

    .line 130131
    :cond_0
    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 130132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Strange Map type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": can not determine type parameters"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130133
    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {p1, v1, v0}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/Class;Ljava/lang/Class;)LX/1Y4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Y4;"
        }
    .end annotation

    .prologue
    .line 130140
    invoke-virtual {p2}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130141
    invoke-direct {p0, p1, p2}, LX/0li;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;

    move-result-object v0

    .line 130142
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, LX/0li;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)LX/1Y4;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130143
    const-class v0, Ljava/util/Collection;

    invoke-direct {p0, p1, v0}, LX/0li;->b(Ljava/lang/Class;Ljava/lang/Class;)[LX/0lJ;

    move-result-object v0

    .line 130144
    if-nez v0, :cond_0

    .line 130145
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v0

    invoke-static {p1, v0}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v0

    .line 130146
    :goto_0
    return-object v0

    .line 130147
    :cond_0
    array-length v1, v0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 130148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Strange Collection type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": can not determine type parameters"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130149
    :cond_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p1, v0}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130150
    instance-of v0, p1, LX/0lH;

    if-eqz v0, :cond_4

    .line 130151
    invoke-virtual {p2}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130152
    :cond_0
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 130153
    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 130154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not subtype of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130155
    :cond_1
    new-instance v0, LX/1Y3;

    .line 130156
    iget-object v1, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v1, v1

    .line 130157
    invoke-direct {v0, p0, v1}, LX/1Y3;-><init>(LX/0li;Ljava/lang/Class;)V

    invoke-virtual {p0, p2}, LX/0li;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 130158
    invoke-virtual {p1}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v1

    .line 130159
    if-eqz v1, :cond_2

    .line 130160
    invoke-virtual {v0, v1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    .line 130161
    :cond_2
    invoke-virtual {p1}, LX/0lJ;->u()Ljava/lang/Object;

    move-result-object v1

    .line 130162
    if-eqz v1, :cond_3

    .line 130163
    invoke-virtual {v0, v1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v0

    .line 130164
    :cond_3
    :goto_0
    return-object v0

    :cond_4
    invoke-virtual {p1, p2}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/266;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/266",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130165
    iget-object v0, p1, LX/266;->a:Ljava/lang/reflect/Type;

    move-object v0, v0

    .line 130166
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/util/List;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/List",
            "<",
            "LX/0lJ;",
            ">;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 130167
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130168
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object v0

    .line 130169
    :goto_0
    return-object v0

    .line 130170
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130171
    new-instance v0, LX/0lH;

    invoke-direct {v0, p1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    goto :goto_0

    .line 130172
    :cond_1
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130173
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 130174
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 130175
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lJ;

    .line 130176
    :goto_1
    invoke-static {p1, v0, v1}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v0

    goto :goto_0

    .line 130177
    :cond_2
    invoke-static {}, LX/0li;->c()LX/0lJ;

    move-result-object v1

    goto :goto_1

    .line 130178
    :cond_3
    invoke-direct {p0, p1}, LX/0li;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130179
    :cond_4
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 130180
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 130181
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    invoke-static {p1, v0}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v0

    goto :goto_0

    .line 130182
    :cond_5
    invoke-direct {p0, p1}, LX/0li;->d(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130183
    :cond_6
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 130184
    new-instance v0, LX/0lH;

    invoke-direct {v0, p1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    goto :goto_0

    .line 130185
    :cond_7
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LX/0lJ;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0lJ;

    .line 130186
    invoke-static {p1, v0}, LX/0li;->a(Ljava/lang/Class;[LX/0lJ;)LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/0lJ;
    .locals 1

    .prologue
    .line 130187
    iget-object v0, p0, LX/0li;->_parser:LX/0lk;

    invoke-virtual {v0, p1}, LX/0lk;->a(Ljava/lang/String;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Type;)LX/0lJ;
    .locals 1

    .prologue
    .line 130188
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;
    .locals 1

    .prologue
    .line 130189
    invoke-virtual {p0, p1, p2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0nn;)LX/0li;
    .locals 4

    .prologue
    .line 130190
    iget-object v0, p0, LX/0li;->_modifiers:[LX/0nn;

    if-nez v0, :cond_0

    .line 130191
    new-instance v0, LX/0li;

    iget-object v1, p0, LX/0li;->_parser:LX/0lk;

    const/4 v2, 0x1

    new-array v2, v2, [LX/0nn;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v0, v1, v2}, LX/0li;-><init>(LX/0lk;[LX/0nn;)V

    .line 130192
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/0li;

    iget-object v2, p0, LX/0li;->_parser:LX/0lk;

    iget-object v0, p0, LX/0li;->_modifiers:[LX/0nn;

    invoke-static {v0, p1}, LX/0nj;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0nn;

    invoke-direct {v1, v2, v0}, LX/0li;-><init>(LX/0lk;[LX/0nn;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)LX/1Xn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/util/Map;",
            ">;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/1Xn;"
        }
    .end annotation

    .prologue
    .line 130193
    invoke-virtual {p0, p2}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    invoke-virtual {p0, p3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/util/Collection;",
            ">;",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/267;"
        }
    .end annotation

    .prologue
    .line 130194
    invoke-virtual {p0, p2}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    invoke-static {p1, v0}, LX/267;->a(Ljava/lang/Class;LX/0lJ;)LX/267;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Class;)LX/0lJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130195
    const-class v0, Ljava/lang/String;

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0li;->b:LX/0lH;

    .line 130196
    :cond_0
    :goto_0
    return-object v0

    .line 130197
    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_2

    sget-object v0, LX/0li;->c:LX/0lH;

    goto :goto_0

    .line 130198
    :cond_2
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_3

    sget-object v0, LX/0li;->d:LX/0lH;

    goto :goto_0

    .line 130199
    :cond_3
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne p1, v0, :cond_4

    sget-object v0, LX/0li;->e:LX/0lH;

    goto :goto_0

    .line 130200
    :cond_4
    new-instance v1, LX/1Xc;

    invoke-direct {v1, p1}, LX/1Xc;-><init>(Ljava/lang/Class;)V

    .line 130201
    iget-object v2, p0, LX/0li;->_typeCache:LX/0lj;

    monitor-enter v2

    .line 130202
    :try_start_0
    iget-object v0, p0, LX/0li;->_typeCache:LX/0lj;

    invoke-virtual {v0, v1}, LX/0lj;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lJ;

    .line 130203
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 130204
    if-nez v0, :cond_0

    .line 130205
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 130206
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/0li;->b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;

    move-result-object v0

    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object v0

    .line 130207
    :goto_1
    iget-object v2, p0, LX/0li;->_typeCache:LX/0lj;

    monitor-enter v2

    .line 130208
    :try_start_1
    iget-object v3, p0, LX/0li;->_typeCache:LX/0lj;

    invoke-virtual {v3, v1, v0}, LX/0lj;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130209
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 130210
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 130211
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 130212
    new-instance v0, LX/0lH;

    invoke-direct {v0, p1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    goto :goto_1

    .line 130213
    :cond_6
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 130214
    invoke-direct {p0, p1}, LX/0li;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_1

    .line 130215
    :cond_7
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 130216
    invoke-direct {p0, p1}, LX/0li;->d(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    goto :goto_1

    .line 130217
    :cond_8
    new-instance v0, LX/0lH;

    invoke-direct {v0, p1}, LX/0lH;-><init>(Ljava/lang/Class;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/reflect/Type;LX/1Y3;)LX/0lJ;
    .locals 5

    .prologue
    .line 130218
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 130219
    check-cast p1, Ljava/lang/Class;

    .line 130220
    invoke-virtual {p0, p1}, LX/0li;->b(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 130221
    :goto_0
    iget-object v1, p0, LX/0li;->_modifiers:[LX/0nn;

    if-eqz v1, :cond_7

    invoke-virtual {v0}, LX/0lJ;->l()Z

    move-result v1

    if-nez v1, :cond_7

    .line 130222
    iget-object v3, p0, LX/0li;->_modifiers:[LX/0nn;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_7

    aget-object v2, v3, v1

    .line 130223
    invoke-virtual {v2, v0, p0}, LX/0nn;->a(LX/0lJ;LX/0li;)LX/0lJ;

    move-result-object v2

    .line 130224
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_1

    .line 130225
    :cond_0
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_1

    .line 130226
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    invoke-direct {p0, p1, p2}, LX/0li;->a(Ljava/lang/reflect/ParameterizedType;LX/1Y3;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130227
    :cond_1
    instance-of v0, p1, LX/0lJ;

    if-eqz v0, :cond_2

    .line 130228
    check-cast p1, LX/0lJ;

    .line 130229
    :goto_2
    return-object p1

    .line 130230
    :cond_2
    instance-of v0, p1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_3

    .line 130231
    check-cast p1, Ljava/lang/reflect/GenericArrayType;

    invoke-direct {p0, p1, p2}, LX/0li;->a(Ljava/lang/reflect/GenericArrayType;LX/1Y3;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130232
    :cond_3
    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_4

    .line 130233
    check-cast p1, Ljava/lang/reflect/TypeVariable;

    invoke-direct {p0, p1, p2}, LX/0li;->a(Ljava/lang/reflect/TypeVariable;LX/1Y3;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130234
    :cond_4
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_5

    .line 130235
    check-cast p1, Ljava/lang/reflect/WildcardType;

    invoke-direct {p0, p1, p2}, LX/0li;->a(Ljava/lang/reflect/WildcardType;LX/1Y3;)LX/0lJ;

    move-result-object v0

    goto :goto_0

    .line 130236
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Unrecognized Type: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_6

    const-string v0, "[null]"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    move-object p1, v0

    .line 130237
    goto :goto_2
.end method

.method public final b(LX/0lJ;Ljava/lang/Class;)[LX/0lJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "Ljava/lang/Class",
            "<*>;)[",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 130238
    iget-object v0, p1, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 130239
    if-ne v0, p2, :cond_2

    .line 130240
    invoke-virtual {p1}, LX/0lJ;->s()I

    move-result v2

    .line 130241
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 130242
    :cond_0
    :goto_0
    return-object v0

    .line 130243
    :cond_1
    new-array v0, v2, [LX/0lJ;

    .line 130244
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    .line 130245
    invoke-virtual {p1, v1}, LX/0lJ;->a(I)LX/0lJ;

    move-result-object v3

    aput-object v3, v0, v1

    .line 130246
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 130247
    :cond_2
    new-instance v1, LX/1Y3;

    invoke-direct {v1, p0, p1}, LX/1Y3;-><init>(LX/0li;LX/0lJ;)V

    invoke-direct {p0, v0, p2, v1}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;LX/1Y3;)[LX/0lJ;

    move-result-object v0

    goto :goto_0
.end method
