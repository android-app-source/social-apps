.class public LX/1nC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field public final b:LX/0SI;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/03V;LX/0SI;LX/0Ot;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SI;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 315252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315253
    iput-object p1, p0, LX/1nC;->a:LX/03V;

    .line 315254
    iput-object p2, p0, LX/1nC;->b:LX/0SI;

    .line 315255
    iput-object p3, p0, LX/1nC;->c:LX/0Ot;

    .line 315256
    iput-object p4, p0, LX/1nC;->d:Ljava/util/concurrent/Executor;

    .line 315257
    return-void
.end method

.method public static a(LX/0QB;)LX/1nC;
    .locals 1

    .prologue
    .line 314949
    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315244
    sget-object v0, LX/21D;->INVALID:LX/21D;

    if-eq p0, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 315245
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 315246
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->STATUS:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 315247
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->GROUP_FEED:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->TIMELINE:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->REACTION:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->COMPOST:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->EVENT:LX/21D;

    if-eq p0, v1, :cond_0

    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    if-ne p0, v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 315248
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 315249
    goto :goto_0

    :cond_2
    move v1, v2

    .line 315250
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public static a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 315243
    invoke-static {p0, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ComposerCallToAction;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 1

    .prologue
    .line 315242
    invoke-static {p0, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialComposerCallToAction(Lcom/facebook/ipc/composer/model/ComposerCallToAction;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/21D;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/21D;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 315238
    invoke-static {p0, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->SELL:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setHideKeyboardIfReachedMinimumHeight(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCommerceInfo(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 315239
    if-eqz p4, :cond_0

    .line 315240
    invoke-virtual {v0, p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setGroupCommerceCategories(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315241
    :cond_0
    return-object v0
.end method

.method public static a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 315233
    new-instance v0, LX/89I;

    sget-object v1, LX/2rw;->PAGE_RECOMMENDATION:LX/2rw;

    invoke-direct {v0, p3, p4, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 315234
    iput-object p5, v0, LX/89I;->c:Ljava/lang/String;

    .line 315235
    move-object v0, v0

    .line 315236
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 315237
    invoke-static {p0, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEdit(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {p6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setOgMechanism(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {p7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setOgSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->RECOMMENDATION:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/ipc/composer/model/ComposerTaggedUser;
    .locals 2

    .prologue
    .line 315226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 315227
    iput-object v1, v0, LX/5Rc;->b:Ljava/lang/String;

    .line 315228
    move-object v0, v0

    .line 315229
    invoke-static {p0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    .line 315230
    iput-object v1, v0, LX/5Rc;->c:Ljava/lang/String;

    .line 315231
    move-object v0, v0

    .line 315232
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/ipc/media/MediaItem;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 315186
    if-nez p0, :cond_1

    move-object v1, v0

    .line 315187
    :goto_0
    if-eqz v1, :cond_0

    .line 315188
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 315189
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 315190
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    new-instance v1, LX/4gN;

    invoke-direct {v1}, LX/4gN;-><init>()V

    .line 315191
    iput-wide v2, v1, LX/4gN;->d:J

    .line 315192
    move-object v1, v1

    .line 315193
    invoke-static {p0}, LX/1nC;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/4gP;

    move-result-object v2

    invoke-virtual {v2}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v1

    invoke-virtual {v1}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 315194
    iput-object v1, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 315195
    move-object v0, v0

    .line 315196
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    .line 315197
    :cond_0
    :goto_1
    return-object v0

    .line 315198
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    goto :goto_0

    .line 315199
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 315200
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v0, v4

    .line 315201
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 315202
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    new-instance v5, LX/4gN;

    invoke-direct {v5}, LX/4gN;-><init>()V

    .line 315203
    iput-wide v2, v5, LX/4gN;->d:J

    .line 315204
    move-object v2, v5

    .line 315205
    invoke-static {p0}, LX/1nC;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/4gP;

    move-result-object v3

    .line 315206
    iput v4, v3, LX/4gP;->h:F

    .line 315207
    move-object v3, v3

    .line 315208
    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v2

    invoke-virtual {v2}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 315209
    iput-object v2, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 315210
    move-object v0, v0

    .line 315211
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 315212
    iput-object v1, v0, LX/74m;->c:Ljava/lang/String;

    .line 315213
    move-object v0, v0

    .line 315214
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto :goto_1

    .line 315215
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 315216
    new-instance v5, LX/74k;

    invoke-direct {v5}, LX/74k;-><init>()V

    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    .line 315217
    iput-wide v2, v0, LX/4gN;->d:J

    .line 315218
    move-object v6, v0

    .line 315219
    new-instance v7, LX/4gP;

    invoke-direct {v7}, LX/4gP;-><init>()V

    new-instance v8, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-static {v1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v8, v0, v2, v3}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    .line 315220
    iput v4, v0, LX/4gP;->h:F

    .line 315221
    move-object v0, v0

    .line 315222
    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    .line 315223
    iput-object v0, v5, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 315224
    move-object v0, v5

    .line 315225
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 315251
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/1nC;
    .locals 5

    .prologue
    .line 314947
    new-instance v3, LX/1nC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v1

    check-cast v1, LX/0SI;

    const/16 v2, 0xafd

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v4, v2}, LX/1nC;-><init>(LX/03V;LX/0SI;LX/0Ot;Ljava/util/concurrent/Executor;)V

    .line 314948
    return-object v3
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/4gP;
    .locals 3

    .prologue
    .line 314950
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_2

    .line 314951
    :cond_0
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    .line 314952
    :cond_1
    :goto_0
    return-object v0

    .line 314953
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 314954
    new-instance v1, LX/4gP;

    invoke-direct {v1}, LX/4gP;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v1

    sget-object v2, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v1, v2}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v2

    .line 314955
    iput v2, v1, LX/4gP;->g:I

    .line 314956
    move-object v1, v1

    .line 314957
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v0

    .line 314958
    iput v0, v1, LX/4gP;->f:I

    .line 314959
    move-object v0, v1

    .line 314960
    invoke-static {p0}, LX/1VO;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 314961
    sget-object v1, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    goto :goto_0
.end method

.method public static b(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3

    .prologue
    .line 314962
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, LX/2rt;->LIFE_EVENT:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    sget-object v2, LX/5RI;->LIFE_EVENT_TYPE_PICKER:LX/5RI;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPicker(LX/5RI;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    .line 314963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    .line 314964
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 314965
    :cond_0
    :goto_0
    return-object v0

    .line 314966
    :cond_1
    invoke-static {p0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 314967
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    .line 314968
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v4

    .line 314969
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    .line 314970
    :goto_1
    :try_start_0
    new-instance v6, LX/5m9;

    invoke-direct {v6}, LX/5m9;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v7

    .line 314971
    iput-object v7, v6, LX/5m9;->f:Ljava/lang/String;

    .line 314972
    move-object v6, v6

    .line 314973
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v1

    .line 314974
    iput-object v1, v6, LX/5m9;->h:Ljava/lang/String;

    .line 314975
    move-object v1, v6

    .line 314976
    new-instance v6, LX/5mE;

    invoke-direct {v6}, LX/5mE;-><init>()V

    .line 314977
    iput-wide v4, v6, LX/5mE;->a:D

    .line 314978
    move-object v4, v6

    .line 314979
    iput-wide v2, v4, LX/5mE;->b:D

    .line 314980
    move-object v2, v4

    .line 314981
    invoke-virtual {v2}, LX/5mE;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    .line 314982
    iput-object v2, v1, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 314983
    move-object v1, v1

    .line 314984
    invoke-virtual {v1}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 314985
    :catch_0
    goto :goto_0

    :cond_2
    move-wide v4, v2

    goto :goto_1
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 314986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 314987
    if-nez v0, :cond_0

    .line 314988
    const/4 v0, 0x0

    .line 314989
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->a(Ljava/lang/String;J)LX/5S2;

    move-result-object v0

    invoke-virtual {v0}, LX/5S2;->a()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 314990
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x25d6af

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 5

    .prologue
    .line 314991
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 314992
    new-instance v0, LX/89I;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 314993
    sparse-switch v1, :sswitch_data_0

    .line 314994
    sget-object v4, LX/2rw;->OTHER:LX/2rw;

    :goto_0
    move-object v1, v4

    .line 314995
    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v1

    .line 314996
    iput-object v1, v0, LX/89I;->c:Ljava/lang/String;

    .line 314997
    move-object v0, v0

    .line 314998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 314999
    invoke-static {v1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 315000
    iput-object v1, v0, LX/89I;->d:Ljava/lang/String;

    .line 315001
    move-object v0, v0

    .line 315002
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 315003
    :goto_2
    return-object v0

    .line 315004
    :cond_0
    invoke-static {p0}, LX/1nC;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315005
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 315006
    new-instance v1, LX/89I;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    invoke-static {v0}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    .line 315007
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 315008
    move-object v1, v1

    .line 315009
    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 315010
    iput-object v0, v1, LX/89I;->d:Ljava/lang/String;

    .line 315011
    move-object v0, v1

    .line 315012
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    goto :goto_2

    .line 315013
    :cond_1
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    goto :goto_2

    .line 315014
    :sswitch_0
    sget-object v4, LX/2rw;->USER:LX/2rw;

    goto :goto_0

    .line 315015
    :sswitch_1
    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    goto :goto_0

    .line 315016
    :sswitch_2
    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    goto :goto_0

    .line 315017
    :sswitch_3
    sget-object v4, LX/2rw;->EVENT:LX/2rw;

    goto :goto_0

    .line 315018
    :sswitch_4
    sget-object v4, LX/2rw;->FUNDRAISER:LX/2rw;

    goto :goto_0

    :cond_2
    const-string v2, ""

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4e6785e3 -> :sswitch_4
        0x25d6af -> :sswitch_2
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/21D;Ljava/lang/String;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/21D;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315019
    new-instance v0, LX/5R4;

    invoke-direct {v0}, LX/5R4;-><init>()V

    move-object v0, v0

    .line 315020
    const-string v1, "page_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 315021
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 315022
    iget-object v0, p0, LX/1nC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    new-instance v0, LX/892;

    move-object v1, p0

    move-wide v2, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/892;-><init>(LX/1nC;JLX/21D;Ljava/lang/String;)V

    iget-object v1, p0, LX/1nC;->d:Ljava/util/concurrent/Executor;

    invoke-static {v6, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 315023
    if-eqz p3, :cond_0

    .line 315024
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->R()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditPrivacyEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315025
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bc()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setEditPostFeatureCapabilities(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315026
    :cond_0
    invoke-static {p1}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315027
    sget-object v0, LX/2rt;->SELL:LX/2rt;

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315028
    invoke-static {p1}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 315029
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->gv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->gv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 315030
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 315031
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x64

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 315032
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->k()Ljava/lang/String;

    move-result-object v2

    .line 315033
    new-instance v4, LX/5Rj;

    invoke-direct {v4}, LX/5Rj;-><init>()V

    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    .line 315034
    iput-object v5, v4, LX/5Rj;->a:Ljava/lang/String;

    .line 315035
    move-object v4, v4

    .line 315036
    iput-object v0, v4, LX/5Rj;->b:Ljava/lang/String;

    .line 315037
    move-object v0, v4

    .line 315038
    iput-object v1, v0, LX/5Rj;->d:Ljava/lang/String;

    .line 315039
    move-object v0, v0

    .line 315040
    iput-object v3, v0, LX/5Rj;->e:Ljava/lang/Long;

    .line 315041
    move-object v0, v0

    .line 315042
    iput-object v2, v0, LX/5Rj;->f:Ljava/lang/String;

    .line 315043
    move-object v0, v0

    .line 315044
    invoke-virtual {v0}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    .line 315045
    invoke-virtual {p2, p3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEdit(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCommerceInfo(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315046
    :cond_2
    invoke-static {p1}, LX/1nC;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 315047
    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315048
    invoke-static {p1}, LX/1nC;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315049
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 315050
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-static {v0}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/1nC;->b:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315051
    :cond_3
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 315052
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315053
    :cond_4
    invoke-static {p1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 315054
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 315055
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 315056
    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315057
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 315058
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 315059
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->be()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLWithTagsConnection;->a()LX/0Px;

    move-result-object v0

    .line 315060
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 315061
    :try_start_0
    invoke-static {v0}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 315062
    :catch_0
    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 315063
    goto/16 :goto_0

    .line 315064
    :cond_7
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315065
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    .line 315066
    if-eqz v0, :cond_12

    .line 315067
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    .line 315068
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 315069
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315070
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 315071
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315072
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 315073
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/5RS;->getValue(Ljava/lang/String;)LX/5RS;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setTextAlign(LX/5RS;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315074
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 315075
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/5RY;->getValue(Ljava/lang/String;)LX/5RY;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setFontWeight(LX/5RY;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315076
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 315077
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundImageUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315078
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 315079
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setBackgroundImageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315080
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->p()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 315081
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientEndColor(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315082
    :cond_f
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->q()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 315083
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setGradientDirection(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315084
    :cond_10
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->r()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 315085
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->setPresetId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    .line 315086
    :cond_11
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315087
    invoke-virtual {p2, v9}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowDynamicTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315088
    invoke-virtual {p2, v9}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowRichTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315089
    :cond_12
    invoke-static {p1}, LX/1nC;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 315090
    if-eqz v0, :cond_19

    .line 315091
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315092
    :cond_13
    :goto_2
    invoke-static {p1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 315093
    if-eqz v0, :cond_14

    .line 315094
    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315095
    :cond_14
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->h()Z

    move-result v0

    if-nez v0, :cond_17

    .line 315096
    :cond_15
    if-eqz p3, :cond_16

    .line 315097
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->Q()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCanViewerEditPostMedia(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315098
    :cond_16
    const/4 v1, 0x0

    .line 315099
    :try_start_1
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 315100
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MINUTIAE_UNIT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v2

    move v2, v2

    .line 315101
    if-eqz v2, :cond_1a
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 315102
    :cond_17
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aC()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    .line 315103
    if-eqz v0, :cond_18

    .line 315104
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSticker;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setStickerId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSticker;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setStaticWebUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315105
    :cond_18
    return-void

    .line 315106
    :cond_19
    invoke-static {p1}, LX/1nC;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v0

    .line 315107
    if-eqz v0, :cond_13

    .line 315108
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v1

    .line 315109
    iput-object v0, v1, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 315110
    move-object v0, v1

    .line 315111
    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    goto :goto_2

    .line 315112
    :cond_1a
    :try_start_2
    const/4 v3, 0x0

    .line 315113
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v5

    .line 315114
    if-nez v5, :cond_25

    move-object v2, v3

    .line 315115
    :cond_1b
    :goto_4
    move-object v2, v2

    .line 315116
    invoke-static {p1}, LX/17E;->t(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 315117
    if-eqz v2, :cond_21

    .line 315118
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    .line 315119
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 315120
    invoke-static {v2}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sget-object v1, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 315121
    :catch_1
    move-exception v0

    .line 315122
    iget-object v1, p0, LX/1nC;->a:LX/03V;

    const-string v2, "composer_edit_intent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Edit post attachment preview error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    .line 315123
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v5, "Story id: "

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_28

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    :goto_5
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", shareable id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v4, :cond_29

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v5

    :goto_6
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 315124
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 315125
    :cond_1c
    :try_start_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 315126
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 315127
    const/4 v0, 0x0

    move v2, v0

    :goto_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1f

    .line 315128
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 315129
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 315130
    invoke-static {v0}, LX/1nC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 315131
    if-eqz v0, :cond_1d

    .line 315132
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315133
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_1e

    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    :goto_8
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 315134
    :cond_1d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 315135
    :cond_1e
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_8

    .line 315136
    :cond_1f
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 315137
    :cond_20
    :goto_9
    invoke-virtual {p2, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    goto/16 :goto_3

    .line 315138
    :cond_21
    if-eqz v3, :cond_22

    .line 315139
    invoke-static {v3}, LX/1VO;->x(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 315140
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 315141
    invoke-static {v3}, LX/8Y8;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v4

    .line 315142
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 315143
    if-eqz v0, :cond_24

    if-eqz v2, :cond_24

    .line 315144
    invoke-static {v3}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 315145
    iput-object v0, v1, LX/39x;->r:Ljava/lang/String;

    .line 315146
    move-object v0, v1

    .line 315147
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 315148
    invoke-static {v5}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v1

    .line 315149
    iput-object v4, v1, LX/89G;->e:Ljava/lang/String;

    .line 315150
    move-object v1, v1

    .line 315151
    iput-object v0, v1, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 315152
    move-object v0, v1

    .line 315153
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    :goto_a
    move-object v1, v0

    .line 315154
    goto :goto_9

    :cond_22
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 315155
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 315156
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/890;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 315157
    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    .line 315158
    iput-object v1, v0, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 315159
    move-object v0, v0

    .line 315160
    invoke-static {p1}, LX/89F;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    move-result-object v1

    .line 315161
    iput-object v1, v0, LX/89G;->f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 315162
    move-object v0, v0

    .line 315163
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    goto :goto_9

    .line 315164
    :cond_23
    invoke-static {v0}, LX/1VO;->q(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 315165
    new-instance v1, LX/170;

    invoke-direct {v1}, LX/170;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 315166
    iput-object v2, v1, LX/170;->o:Ljava/lang/String;

    .line 315167
    move-object v1, v1

    .line 315168
    invoke-virtual {v1}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 315169
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 315170
    iput-object v0, v2, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 315171
    move-object v0, v2

    .line 315172
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 315173
    invoke-static {v1}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v1

    .line 315174
    iput-object v0, v1, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 315175
    move-object v0, v1

    .line 315176
    const/4 v1, 0x1

    .line 315177
    iput-boolean v1, v0, LX/89G;->g:Z

    .line 315178
    move-object v0, v0

    .line 315179
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 315180
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_9

    :cond_24
    move-object v0, v1

    goto :goto_a

    .line 315181
    :cond_25
    :try_start_4
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_b
    if-ge v4, v6, :cond_27

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 315182
    if-eqz v2, :cond_26

    .line 315183
    invoke-static {v2}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p3

    if-nez p3, :cond_1b

    invoke-static {v2}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p3

    if-nez p3, :cond_1b

    .line 315184
    :cond_26
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_b

    :cond_27
    move-object v2, v3

    .line 315185
    goto/16 :goto_4
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_28
    const-string v5, ""

    goto/16 :goto_5

    :cond_29
    const-string v5, ""

    goto/16 :goto_6
.end method
