.class public LX/1Sj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1Sj;


# instance fields
.field public final a:LX/03V;

.field public final b:LX/1Sk;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0tX;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Uh;


# direct methods
.method public constructor <init>(LX/03V;LX/1Sk;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Sk;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 249180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249181
    iput-object p1, p0, LX/1Sj;->a:LX/03V;

    .line 249182
    iput-object p2, p0, LX/1Sj;->b:LX/1Sk;

    .line 249183
    iput-object p3, p0, LX/1Sj;->c:Ljava/util/concurrent/ExecutorService;

    .line 249184
    iput-object p4, p0, LX/1Sj;->d:LX/0tX;

    .line 249185
    iput-object p5, p0, LX/1Sj;->e:LX/0Or;

    .line 249186
    iput-object p6, p0, LX/1Sj;->f:LX/0Uh;

    .line 249187
    return-void
.end method

.method private static a(Ljava/lang/String;Z)LX/0jT;
    .locals 12

    .prologue
    .line 248855
    if-eqz p1, :cond_0

    .line 248856
    new-instance v0, LX/7AZ;

    invoke-direct {v0}, LX/7AZ;-><init>()V

    new-instance v1, LX/7Aa;

    invoke-direct {v1}, LX/7Aa;-><init>()V

    new-instance v2, LX/7Ab;

    invoke-direct {v2}, LX/7Ab;-><init>()V

    .line 248857
    iput-object p0, v2, LX/7Ab;->b:Ljava/lang/String;

    .line 248858
    move-object v2, v2

    .line 248859
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 248860
    iput-object v3, v2, LX/7Ab;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 248861
    move-object v2, v2

    .line 248862
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 248863
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248864
    iget-object v5, v2, LX/7Ab;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248865
    iget-object v7, v2, LX/7Ab;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 248866
    iget-object v9, v2, LX/7Ab;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v4, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 248867
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 248868
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 248869
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 248870
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 248871
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248872
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248873
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248874
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248875
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248876
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;-><init>(LX/15i;)V

    .line 248877
    move-object v2, v5

    .line 248878
    iput-object v2, v1, LX/7Aa;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    .line 248879
    move-object v1, v1

    .line 248880
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 248881
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248882
    iget-object v5, v1, LX/7Aa;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248883
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 248884
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 248885
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248886
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248887
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248888
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248889
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248890
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;-><init>(LX/15i;)V

    .line 248891
    move-object v1, v5

    .line 248892
    iput-object v1, v0, LX/7AZ;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    .line 248893
    move-object v0, v0

    .line 248894
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 248895
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248896
    iget-object v5, v0, LX/7AZ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248897
    iget-object v7, v0, LX/7AZ;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 248898
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 248899
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 248900
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 248901
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248902
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248903
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248904
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248905
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248906
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel;-><init>(LX/15i;)V

    .line 248907
    move-object v0, v5

    .line 248908
    new-instance v1, LX/7AY;

    invoke-direct {v1}, LX/7AY;-><init>()V

    .line 248909
    iput-object v0, v1, LX/7AY;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel;

    .line 248910
    move-object v0, v1

    .line 248911
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 248912
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248913
    iget-object v5, v0, LX/7AY;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel$SavableFeedUnitModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248914
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 248915
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 248916
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248917
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248918
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248919
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248920
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248921
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitSaveMutationFieldsModel;-><init>(LX/15i;)V

    .line 248922
    move-object v0, v5

    .line 248923
    :goto_0
    return-object v0

    .line 248924
    :cond_0
    new-instance v0, LX/7Ae;

    invoke-direct {v0}, LX/7Ae;-><init>()V

    new-instance v1, LX/7Af;

    invoke-direct {v1}, LX/7Af;-><init>()V

    new-instance v2, LX/7Ag;

    invoke-direct {v2}, LX/7Ag;-><init>()V

    .line 248925
    iput-object p0, v2, LX/7Ag;->b:Ljava/lang/String;

    .line 248926
    move-object v2, v2

    .line 248927
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 248928
    iput-object v3, v2, LX/7Ag;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 248929
    move-object v2, v2

    .line 248930
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 248931
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248932
    iget-object v5, v2, LX/7Ag;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248933
    iget-object v7, v2, LX/7Ag;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 248934
    iget-object v9, v2, LX/7Ag;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v4, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 248935
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 248936
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 248937
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 248938
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 248939
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248940
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248941
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248942
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248943
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248944
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;-><init>(LX/15i;)V

    .line 248945
    move-object v2, v5

    .line 248946
    iput-object v2, v1, LX/7Af;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    .line 248947
    move-object v1, v1

    .line 248948
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 248949
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248950
    iget-object v5, v1, LX/7Af;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel$SavableModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248951
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 248952
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 248953
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248954
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248955
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248956
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248957
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248958
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;-><init>(LX/15i;)V

    .line 248959
    move-object v1, v5

    .line 248960
    iput-object v1, v0, LX/7Ae;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    .line 248961
    move-object v0, v0

    .line 248962
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 248963
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248964
    iget-object v5, v0, LX/7Ae;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248965
    iget-object v7, v0, LX/7Ae;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel$SaveInfoModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 248966
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 248967
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 248968
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 248969
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248970
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248971
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248972
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248973
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248974
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;-><init>(LX/15i;)V

    .line 248975
    move-object v0, v5

    .line 248976
    new-instance v1, LX/7Ad;

    invoke-direct {v1}, LX/7Ad;-><init>()V

    .line 248977
    iput-object v0, v1, LX/7Ad;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    .line 248978
    move-object v0, v1

    .line 248979
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 248980
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 248981
    iget-object v5, v0, LX/7Ad;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel$SavableFeedUnitModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 248982
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 248983
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 248984
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 248985
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 248986
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 248987
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 248988
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 248989
    new-instance v5, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$SavableFeedUnitUnsaveMutationFieldsModel;-><init>(LX/15i;)V

    .line 248990
    move-object v0, v5

    .line 248991
    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/1Sj;
    .locals 10

    .prologue
    .line 248992
    sget-object v0, LX/1Sj;->g:LX/1Sj;

    if-nez v0, :cond_1

    .line 248993
    const-class v1, LX/1Sj;

    monitor-enter v1

    .line 248994
    :try_start_0
    sget-object v0, LX/1Sj;->g:LX/1Sj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 248995
    if-eqz v2, :cond_0

    .line 248996
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 248997
    new-instance v3, LX/1Sj;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1Sk;->a(LX/0QB;)LX/1Sk;

    move-result-object v5

    check-cast v5, LX/1Sk;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    const/16 v8, 0x122d

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, LX/1Sj;-><init>(LX/03V;LX/1Sk;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0Or;LX/0Uh;)V

    .line 248998
    move-object v0, v3

    .line 248999
    sput-object v0, LX/1Sj;->g:LX/1Sj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249000
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 249001
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 249002
    :cond_1
    sget-object v0, LX/1Sj;->g:LX/1Sj;

    return-object v0

    .line 249003
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 249004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Sj;Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/79i;)V
    .locals 4
    .param p2    # Z
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/79i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249005
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 249006
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 249007
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 249008
    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249009
    iget-object v0, p0, LX/1Sj;->a:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Trying to (un)save all attachments of a story while not having enough data for save."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249010
    :goto_0
    return-void

    .line 249011
    :cond_0
    invoke-static {p1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 249012
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 249013
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 249014
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249015
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 249016
    if-eqz p2, :cond_1

    .line 249017
    new-instance v0, LX/7AL;

    invoke-direct {v0}, LX/7AL;-><init>()V

    move-object v1, v0

    .line 249018
    new-instance v0, LX/4JZ;

    invoke-direct {v0}, LX/4JZ;-><init>()V

    .line 249019
    const-string p1, "story_id"

    invoke-virtual {v0, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249020
    move-object v0, v0

    .line 249021
    const-string p1, "surface"

    invoke-virtual {v0, p1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249022
    move-object v0, v0

    .line 249023
    const-string p1, "mechanism"

    invoke-virtual {v0, p1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249024
    move-object v0, v0

    .line 249025
    const-string p1, "tracking"

    invoke-virtual {v0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 249026
    move-object v0, v0

    .line 249027
    :goto_1
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 249028
    new-instance v0, LX/3G1;

    invoke-direct {v0}, LX/3G1;-><init>()V

    new-instance v2, LX/399;

    invoke-direct {v2, v1}, LX/399;-><init>(LX/0zP;)V

    invoke-virtual {v0, v2}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    invoke-static {v3, p2}, LX/1Sj;->b(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3G1;->a(LX/0jT;)LX/3G1;

    move-result-object v0

    .line 249029
    invoke-virtual {v0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    invoke-direct {p0, v0, p5}, LX/1Sj;->a(LX/3G4;LX/79i;)V

    goto :goto_0

    .line 249030
    :cond_1
    new-instance v0, LX/7AM;

    invoke-direct {v0}, LX/7AM;-><init>()V

    move-object v1, v0

    .line 249031
    new-instance v0, LX/4Ja;

    invoke-direct {v0}, LX/4Ja;-><init>()V

    .line 249032
    const-string p1, "story_id"

    invoke-virtual {v0, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249033
    move-object v0, v0

    .line 249034
    const-string p1, "surface"

    invoke-virtual {v0, p1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249035
    move-object v0, v0

    .line 249036
    const-string p1, "mechanism"

    invoke-virtual {v0, p1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249037
    move-object v0, v0

    .line 249038
    const-string p1, "tracking"

    invoke-virtual {v0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 249039
    move-object v0, v0

    .line 249040
    goto :goto_1
.end method

.method public static a(LX/1Sj;Lcom/facebook/graphql/model/GraphQLSavable;Ljava/lang/String;Ljava/lang/String;LX/162;LX/79i;)V
    .locals 7
    .param p1    # Lcom/facebook/graphql/model/GraphQLSavable;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249041
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/1Sj;->a(LX/1Sj;Lcom/facebook/graphql/model/GraphQLSavable;ZLjava/lang/String;Ljava/lang/String;LX/162;LX/79i;)V

    .line 249042
    return-void
.end method

.method private static a(LX/1Sj;Lcom/facebook/graphql/model/GraphQLSavable;ZLjava/lang/String;Ljava/lang/String;LX/162;LX/79i;)V
    .locals 5
    .param p2    # Z
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249043
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavable;->j()Ljava/lang/String;

    move-result-object v2

    .line 249044
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 249045
    if-eqz p5, :cond_0

    .line 249046
    invoke-virtual {p5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249047
    :cond_0
    if-eqz p2, :cond_1

    .line 249048
    new-instance v0, LX/7AN;

    invoke-direct {v0}, LX/7AN;-><init>()V

    move-object v0, v0

    .line 249049
    :goto_0
    new-instance v1, LX/4Ei;

    invoke-direct {v1}, LX/4Ei;-><init>()V

    .line 249050
    const-string v4, "savable_id"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249051
    move-object v4, v1

    .line 249052
    if-eqz p2, :cond_2

    const-string v1, "SAVE"

    .line 249053
    :goto_1
    const-string p1, "save_action"

    invoke-virtual {v4, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249054
    move-object v1, v4

    .line 249055
    const-string v4, "surface"

    invoke-virtual {v1, v4, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249056
    move-object v1, v1

    .line 249057
    const-string v4, "mechanism"

    invoke-virtual {v1, v4, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 249058
    move-object v1, v1

    .line 249059
    const-string v4, "tracking"

    invoke-virtual {v1, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 249060
    move-object v1, v1

    .line 249061
    const-string v3, "input"

    invoke-virtual {v0, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 249062
    new-instance v1, LX/3G1;

    invoke-direct {v1}, LX/3G1;-><init>()V

    new-instance v3, LX/399;

    invoke-direct {v3, v0}, LX/399;-><init>(LX/0zP;)V

    invoke-virtual {v1, v3}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    invoke-static {v2, p2}, LX/1Sj;->a(Ljava/lang/String;Z)LX/0jT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3G1;->a(LX/0jT;)LX/3G1;

    move-result-object v0

    .line 249063
    invoke-virtual {v0}, LX/3G1;->b()LX/3G4;

    move-result-object v0

    invoke-direct {p0, v0, p6}, LX/1Sj;->a(LX/3G4;LX/79i;)V

    .line 249064
    return-void

    .line 249065
    :cond_1
    new-instance v0, LX/7AO;

    invoke-direct {v0}, LX/7AO;-><init>()V

    move-object v0, v0

    .line 249066
    goto :goto_0

    .line 249067
    :cond_2
    const-string v1, "UNSAVE"

    goto :goto_1
.end method

.method private a(LX/3G4;LX/79i;)V
    .locals 3
    .param p2    # LX/79i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249068
    iget-object v0, p0, LX/1Sj;->d:LX/0tX;

    sget-object v1, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v0, p1, v1}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 249069
    new-instance v1, LX/79h;

    invoke-direct {v1, p0, p2}, LX/79h;-><init>(LX/1Sj;LX/79i;)V

    iget-object v2, p0, LX/1Sj;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 249070
    return-void
.end method

.method private static b(Ljava/lang/String;Z)LX/0jT;
    .locals 10

    .prologue
    .line 249071
    if-eqz p1, :cond_0

    .line 249072
    new-instance v0, LX/7AR;

    invoke-direct {v0}, LX/7AR;-><init>()V

    .line 249073
    iput-object p0, v0, LX/7AR;->a:Ljava/lang/String;

    .line 249074
    move-object v0, v0

    .line 249075
    new-instance v1, LX/7AT;

    invoke-direct {v1}, LX/7AT;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 249076
    iput-object v2, v1, LX/7AT;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 249077
    move-object v1, v1

    .line 249078
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 249079
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249080
    iget-object v4, v1, LX/7AT;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 249081
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 249082
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 249083
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249084
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249085
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249086
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249087
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249088
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;-><init>(LX/15i;)V

    .line 249089
    move-object v1, v4

    .line 249090
    iput-object v1, v0, LX/7AR;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 249091
    move-object v0, v0

    .line 249092
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 249093
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249094
    iget-object v4, v0, LX/7AR;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 249095
    iget-object v6, v0, LX/7AR;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 249096
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, LX/186;->c(I)V

    .line 249097
    invoke-virtual {v3, v9, v4}, LX/186;->b(II)V

    .line 249098
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 249099
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249100
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249101
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249102
    invoke-virtual {v4, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249103
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249104
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;-><init>(LX/15i;)V

    .line 249105
    move-object v0, v4

    .line 249106
    new-instance v1, LX/7AQ;

    invoke-direct {v1}, LX/7AQ;-><init>()V

    .line 249107
    iput-object v0, v1, LX/7AQ;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;

    .line 249108
    move-object v0, v1

    .line 249109
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 249110
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249111
    iget-object v4, v0, LX/7AQ;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel$StoryModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 249112
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 249113
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 249114
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249115
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249116
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249117
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249118
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249119
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStorySaveMutationFieldsModel;-><init>(LX/15i;)V

    .line 249120
    move-object v0, v4

    .line 249121
    :goto_0
    return-object v0

    .line 249122
    :cond_0
    new-instance v0, LX/7AV;

    invoke-direct {v0}, LX/7AV;-><init>()V

    .line 249123
    iput-object p0, v0, LX/7AV;->a:Ljava/lang/String;

    .line 249124
    move-object v0, v0

    .line 249125
    new-instance v1, LX/7AX;

    invoke-direct {v1}, LX/7AX;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 249126
    iput-object v2, v1, LX/7AX;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 249127
    move-object v1, v1

    .line 249128
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 249129
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249130
    iget-object v4, v1, LX/7AX;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v3, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 249131
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 249132
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 249133
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249134
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249135
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249136
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249137
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249138
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;-><init>(LX/15i;)V

    .line 249139
    move-object v1, v4

    .line 249140
    iput-object v1, v0, LX/7AV;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    .line 249141
    move-object v0, v0

    .line 249142
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 249143
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249144
    iget-object v4, v0, LX/7AV;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 249145
    iget-object v6, v0, LX/7AV;->b:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel$SaveInfoModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 249146
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, LX/186;->c(I)V

    .line 249147
    invoke-virtual {v3, v9, v4}, LX/186;->b(II)V

    .line 249148
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 249149
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249150
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249151
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249152
    invoke-virtual {v4, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249153
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249154
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;-><init>(LX/15i;)V

    .line 249155
    move-object v0, v4

    .line 249156
    new-instance v1, LX/7AU;

    invoke-direct {v1}, LX/7AU;-><init>()V

    .line 249157
    iput-object v0, v1, LX/7AU;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    .line 249158
    move-object v0, v1

    .line 249159
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 249160
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 249161
    iget-object v4, v0, LX/7AU;->a:Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel$StoryModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 249162
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 249163
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 249164
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 249165
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 249166
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 249167
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249168
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 249169
    new-instance v4, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/saved/protocol/graphql/SaveMutationsModels$FeedStoryUnsaveMutationFieldsModel;-><init>(LX/15i;)V

    .line 249170
    move-object v0, v4

    .line 249171
    goto/16 :goto_0
.end method

.method public static b(LX/1Sj;Lcom/facebook/graphql/model/GraphQLSavable;Ljava/lang/String;Ljava/lang/String;LX/162;LX/79i;)V
    .locals 7
    .param p1    # Lcom/facebook/graphql/model/GraphQLSavable;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249172
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/1Sj;->a(LX/1Sj;Lcom/facebook/graphql/model/GraphQLSavable;ZLjava/lang/String;Ljava/lang/String;LX/162;LX/79i;)V

    .line 249173
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249174
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 249175
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/79i;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p4    # LX/79i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/79i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249176
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/1Sj;->a(LX/1Sj;Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 249177
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249178
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/1Sj;->a(LX/1Sj;Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/79i;)V

    .line 249179
    return-void
.end method
