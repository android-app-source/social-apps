.class public LX/14U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/4ck;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/14V;

.field public c:LX/4d1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1iW;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/http/interfaces/RequestPriority;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/fbtrace/FbTraceNode;

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178895
    sget-object v0, LX/14V;->DEFAULT:LX/14V;

    iput-object v0, p0, LX/14U;->b:LX/14V;

    .line 178896
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    iput-object v0, p0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 178897
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/14U;->h:Z

    return-void
.end method

.method public static a(LX/14U;)LX/14U;
    .locals 2

    .prologue
    .line 178898
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 178899
    iget-object v1, p0, LX/14U;->a:LX/4ck;

    iput-object v1, v0, LX/14U;->a:LX/4ck;

    .line 178900
    iget-object v1, p0, LX/14U;->b:LX/14V;

    iput-object v1, v0, LX/14U;->b:LX/14V;

    .line 178901
    iget-object v1, p0, LX/14U;->c:LX/4d1;

    iput-object v1, v0, LX/14U;->c:LX/4d1;

    .line 178902
    iget-object v1, p0, LX/14U;->d:Ljava/util/List;

    iput-object v1, v0, LX/14U;->d:Ljava/util/List;

    .line 178903
    iget-object v1, p0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v1, v0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 178904
    iget-object v1, p0, LX/14U;->f:LX/0Px;

    iput-object v1, v0, LX/14U;->f:LX/0Px;

    .line 178905
    iget-object v1, p0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    iput-object v1, v0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 178906
    iget-boolean v1, p0, LX/14U;->h:Z

    iput-boolean v1, v0, LX/14U;->h:Z

    .line 178907
    return-object v0
.end method


# virtual methods
.method public final a()LX/4ck;
    .locals 1

    .prologue
    .line 178893
    iget-object v0, p0, LX/14U;->a:LX/4ck;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 178887
    if-eqz p1, :cond_0

    .line 178888
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 178889
    const-string v4, "X-"

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x2

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 178890
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 178891
    :cond_0
    iput-object p1, p0, LX/14U;->f:LX/0Px;

    .line 178892
    return-void
.end method

.method public final a(LX/14V;)V
    .locals 1

    .prologue
    .line 178885
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14V;

    iput-object v0, p0, LX/14U;->b:LX/14V;

    .line 178886
    return-void
.end method

.method public final a(LX/4ck;)V
    .locals 0

    .prologue
    .line 178908
    iput-object p1, p0, LX/14U;->a:LX/4ck;

    .line 178909
    return-void
.end method

.method public final a(LX/4d1;)V
    .locals 0

    .prologue
    .line 178883
    iput-object p1, p0, LX/14U;->c:LX/4d1;

    .line 178884
    return-void
.end method

.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 0
    .param p1    # Lcom/facebook/http/interfaces/RequestPriority;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 178881
    iput-object p1, p0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 178882
    return-void
.end method

.method public final b()LX/14V;
    .locals 1

    .prologue
    .line 178880
    iget-object v0, p0, LX/14U;->b:LX/14V;

    return-object v0
.end method

.method public final c()LX/4d1;
    .locals 1

    .prologue
    .line 178878
    iget-object v0, p0, LX/14U;->c:LX/4d1;

    return-object v0
.end method

.method public final e()Lcom/facebook/fbtrace/FbTraceNode;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 178879
    iget-object v0, p0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    return-object v0
.end method
