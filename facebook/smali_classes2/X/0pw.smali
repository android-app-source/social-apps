.class public abstract LX/0pw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0py;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0py",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/IntentFilter;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2FM;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0py;Landroid/content/IntentFilter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0py",
            "<TT;>;",
            "Landroid/content/IntentFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146868
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/0pw;-><init>(LX/0py;Landroid/content/IntentFilter;Ljava/lang/String;)V

    .line 146869
    return-void
.end method

.method private constructor <init>(LX/0py;Landroid/content/IntentFilter;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0py",
            "<TT;>;",
            "Landroid/content/IntentFilter;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146863
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0py;

    iput-object v0, p0, LX/0pw;->a:LX/0py;

    .line 146864
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    iput-object v0, p0, LX/0pw;->b:Landroid/content/IntentFilter;

    .line 146865
    iput-object p3, p0, LX/0pw;->c:Ljava/lang/String;

    .line 146866
    const/4 v0, 0x3

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0pw;->d:Ljava/util/List;

    .line 146867
    return-void
.end method

.method private static declared-synchronized b(LX/0pw;Landroid/os/Looper;)LX/2FM;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 146858
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2FM;

    .line 146859
    iget-object v2, v0, LX/2FM;->b:Landroid/os/Looper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, p1, :cond_0

    .line 146860
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 146861
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/os/Looper;)Ljava/util/Collection;
    .locals 2
    .param p1    # Landroid/os/Looper;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Looper;",
            ")",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 146827
    monitor-enter p0

    .line 146828
    :try_start_0
    if-nez p1, :cond_0

    .line 146829
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    .line 146830
    :cond_0
    move-object v0, p1

    .line 146831
    invoke-static {p0, v0}, LX/0pw;->b(LX/0pw;Landroid/os/Looper;)LX/2FM;

    move-result-object v1

    .line 146832
    if-nez v1, :cond_1

    .line 146833
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146834
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, v1, LX/2FM;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146835
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract a(Landroid/content/BroadcastReceiver;)V
.end method

.method public abstract a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)V
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 146849
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 146850
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146851
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2FM;

    .line 146852
    iget-object v2, v0, LX/2FM;->c:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146853
    iget-object v2, v0, LX/2FM;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146854
    iget-object v0, v0, LX/2FM;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, LX/0pw;->a(Landroid/content/BroadcastReceiver;)V

    .line 146855
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146856
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146857
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p2    # Landroid/os/Handler;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146837
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146838
    if-nez p2, :cond_1

    .line 146839
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 146840
    :goto_0
    move-object v0, v0

    .line 146841
    invoke-static {p0, v0}, LX/0pw;->b(LX/0pw;Landroid/os/Looper;)LX/2FM;

    move-result-object v1

    .line 146842
    if-eqz v1, :cond_0

    .line 146843
    iget-object v0, v1, LX/2FM;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146844
    :goto_1
    monitor-exit p0

    return-void

    .line 146845
    :cond_0
    :try_start_1
    new-instance v1, LX/2FL;

    iget-object v2, p0, LX/0pw;->a:LX/0py;

    invoke-direct {v1, v2, p0, v0}, LX/2FL;-><init>(LX/0py;LX/0pw;Landroid/os/Looper;)V

    .line 146846
    iget-object v2, p0, LX/0pw;->d:Ljava/util/List;

    new-instance v3, LX/2FM;

    invoke-direct {v3, v1, v0, p1}, LX/2FM;-><init>(Landroid/content/BroadcastReceiver;Landroid/os/Looper;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146847
    iget-object v0, p0, LX/0pw;->b:Landroid/content/IntentFilter;

    iget-object v2, p0, LX/0pw;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, p2}, LX/0pw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 146848
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 146836
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0pw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
