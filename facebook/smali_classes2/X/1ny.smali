.class public final LX/1ny;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>(LX/1xm;)V
    .locals 1

    .prologue
    .line 317843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317844
    iget-object v0, p1, LX/1xm;->a:Ljava/lang/String;

    iput-object v0, p0, LX/1ny;->a:Ljava/lang/String;

    .line 317845
    iget-object v0, p1, LX/1xm;->b:Ljava/util/List;

    iput-object v0, p0, LX/1ny;->b:Ljava/util/List;

    .line 317846
    iget-boolean v0, p1, LX/1xm;->c:Z

    iput-boolean v0, p0, LX/1ny;->c:Z

    .line 317847
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/1ny;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 317837
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317838
    :cond_0
    const/4 v0, 0x0

    .line 317839
    :goto_0
    return-object v0

    .line 317840
    :cond_1
    new-instance v0, LX/1xm;

    invoke-direct {v0, p0}, LX/1xm;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 317841
    new-instance v1, LX/1ny;

    invoke-direct {v1, v0}, LX/1ny;-><init>(LX/1xm;)V

    move-object v0, v1

    .line 317842
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317836
    iget-object v0, p0, LX/1ny;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/3BC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 317835
    iget-object v0, p0, LX/1ny;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 317834
    iget-boolean v0, p0, LX/1ny;->c:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 317828
    instance-of v1, p1, LX/1ny;

    if-nez v1, :cond_1

    .line 317829
    :cond_0
    :goto_0
    return v0

    .line 317830
    :cond_1
    check-cast p1, LX/1ny;

    .line 317831
    iget-object v1, p0, LX/1ny;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1ny;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/1ny;->c:Z

    iget-boolean v2, p1, LX/1ny;->c:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/1ny;->b:Ljava/util/List;

    iget-object v2, p1, LX/1ny;->b:Ljava/util/List;

    invoke-static {v1, v2}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 317833
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1ny;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/1ny;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/1ny;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/15f;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 317832
    const/4 v0, 0x0

    const-string v1, "%s-%b-%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/1ny;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, LX/1ny;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/1ny;->b:Ljava/util/List;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
