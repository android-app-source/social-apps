.class public LX/15H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/15I;


# instance fields
.field private final a:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final b:LX/15D;


# direct methods
.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/15D;)V
    .locals 1

    .prologue
    .line 180388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180389
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    iput-object v0, p0, LX/15H;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 180390
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    iput-object v0, p0, LX/15H;->b:LX/15D;

    .line 180391
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2

    .prologue
    .line 180392
    iget-object v0, p0, LX/15H;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iget-object v1, p0, LX/15H;->b:LX/15D;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 180393
    return-void
.end method
