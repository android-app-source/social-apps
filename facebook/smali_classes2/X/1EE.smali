.class public final LX/1EE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jW;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:LX/1EF;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final k:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final l:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final m:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final n:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final o:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final p:I
    .annotation build Landroid/support/annotation/Dimension;
    .end annotation
.end field

.field public final q:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

.field public final t:Z

.field public final u:Z


# direct methods
.method public constructor <init>(LX/0Px;ZZLcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;ZLX/1EF;IIIZIIIILjava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1EF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Landroid/support/annotation/Dimension;
        .end annotation
    .end param
    .param p15    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1RN;",
            ">;ZZ",
            "Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;",
            "Z",
            "LX/1EF;",
            "IIIZIIII",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219547
    iput-object p1, p0, LX/1EE;->a:LX/0Px;

    .line 219548
    iput-boolean p2, p0, LX/1EE;->b:Z

    .line 219549
    iput-boolean p3, p0, LX/1EE;->c:Z

    .line 219550
    iput-object p4, p0, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219551
    iput-boolean p5, p0, LX/1EE;->d:Z

    .line 219552
    move/from16 v0, p20

    iput-boolean v0, p0, LX/1EE;->t:Z

    .line 219553
    if-nez p6, :cond_0

    sget-object p6, LX/1EF;->a:LX/1EF;

    :cond_0
    iput-object p6, p0, LX/1EE;->e:LX/1EF;

    .line 219554
    iput p7, p0, LX/1EE;->j:I

    .line 219555
    iput p8, p0, LX/1EE;->k:I

    .line 219556
    iput p9, p0, LX/1EE;->l:I

    .line 219557
    iput-boolean p10, p0, LX/1EE;->u:Z

    .line 219558
    iget-boolean v1, p0, LX/1EE;->u:Z

    if-eqz v1, :cond_1

    :goto_0
    iput p11, p0, LX/1EE;->m:I

    .line 219559
    iget-boolean v1, p0, LX/1EE;->u:Z

    if-eqz v1, :cond_2

    :goto_1
    iput p12, p0, LX/1EE;->n:I

    .line 219560
    iget-boolean v1, p0, LX/1EE;->u:Z

    if-eqz v1, :cond_3

    :goto_2
    iput p13, p0, LX/1EE;->o:I

    .line 219561
    move/from16 v0, p14

    iput v0, p0, LX/1EE;->p:I

    .line 219562
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1EE;->r:Ljava/lang/Integer;

    .line 219563
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1EE;->f:Ljava/lang/String;

    .line 219564
    move-object/from16 v0, p17

    iput-object v0, p0, LX/1EE;->g:Ljava/lang/String;

    .line 219565
    move-object/from16 v0, p18

    iput-object v0, p0, LX/1EE;->h:Ljava/lang/String;

    .line 219566
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1EE;->i:Ljava/lang/String;

    .line 219567
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1EE;->q:Ljava/lang/Integer;

    .line 219568
    return-void

    .line 219569
    :cond_1
    const/4 p11, 0x0

    goto :goto_0

    .line 219570
    :cond_2
    const/4 p12, 0x0

    goto :goto_1

    .line 219571
    :cond_3
    const/4 p13, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/1RN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 219542
    iget-object v0, p0, LX/1EE;->a:LX/0Px;

    move-object v0, v0

    .line 219543
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 219544
    :cond_0
    iget-object v0, p0, LX/1EE;->a:LX/0Px;

    move-object v0, v0

    .line 219545
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RN;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 219539
    iget-object v0, p0, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219540
    iget-boolean p0, v0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    move v0, p0

    .line 219541
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219533
    const-string v0, "NO_PROMPT_INLINE_COMPOSER_MODEL_CACHE_KEY"

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 219536
    iget-object v0, p0, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1EE;->s:Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 219537
    iget-boolean p0, v0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    move v0, p0

    .line 219538
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 219535
    iget-boolean v0, p0, LX/1EE;->d:Z

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219534
    iget-object v0, p0, LX/1EE;->i:Ljava/lang/String;

    return-object v0
.end method
