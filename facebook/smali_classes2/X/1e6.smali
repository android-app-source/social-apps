.class public LX/1e6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 287325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287326
    iput-object p1, p0, LX/1e6;->a:LX/0tX;

    .line 287327
    iput-object p2, p0, LX/1e6;->b:LX/0Or;

    .line 287328
    return-void
.end method

.method public static a(LX/0QB;)LX/1e6;
    .locals 1

    .prologue
    .line 287329
    invoke-static {p0}, LX/1e6;->b(LX/0QB;)LX/1e6;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1e6;
    .locals 3

    .prologue
    .line 287330
    new-instance v1, LX/1e6;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/1e6;-><init>(LX/0tX;LX/0Or;)V

    .line 287331
    return-object v1
.end method
