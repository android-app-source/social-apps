.class public LX/1Vw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Integer;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 266916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266917
    return-void
.end method

.method public static a(LX/0QB;)LX/1Vw;
    .locals 4

    .prologue
    .line 266918
    const-class v1, LX/1Vw;

    monitor-enter v1

    .line 266919
    :try_start_0
    sget-object v0, LX/1Vw;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 266920
    sput-object v2, LX/1Vw;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 266921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 266923
    new-instance p0, LX/1Vw;

    invoke-direct {p0}, LX/1Vw;-><init>()V

    .line 266924
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    .line 266925
    iput-object v3, p0, LX/1Vw;->a:LX/0ad;

    .line 266926
    move-object v0, p0

    .line 266927
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 266928
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Vw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266929
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 266930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static f(LX/1Vw;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 266931
    iget-object v0, p0, LX/1Vw;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 266932
    iget-object v0, p0, LX/1Vw;->a:LX/0ad;

    sget-char v1, LX/CCm;->a:C

    const-string v2, "DEFAULT"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1Vw;->e:Ljava/lang/String;

    .line 266933
    :cond_0
    iget-object v0, p0, LX/1Vw;->e:Ljava/lang/String;

    return-object v0
.end method
