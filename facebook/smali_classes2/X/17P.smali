.class public LX/17P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/17Q;

.field public final b:LX/0Zb;

.field private c:LX/17R;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/17S;

.field private final f:LX/17v;

.field private final g:LX/0W3;

.field private final h:LX/0SG;


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;LX/17R;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17S;LX/17v;LX/0W3;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 197650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197651
    iput-object p1, p0, LX/17P;->a:LX/17Q;

    .line 197652
    iput-object p2, p0, LX/17P;->b:LX/0Zb;

    .line 197653
    iput-object p3, p0, LX/17P;->c:LX/17R;

    .line 197654
    iput-object p4, p0, LX/17P;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 197655
    iput-object p5, p0, LX/17P;->e:LX/17S;

    .line 197656
    iput-object p6, p0, LX/17P;->f:LX/17v;

    .line 197657
    iput-object p7, p0, LX/17P;->g:LX/0W3;

    .line 197658
    iput-object p8, p0, LX/17P;->h:LX/0SG;

    .line 197659
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;LX/0am;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
            "LX/0am",
            "<",
            "Lcom/facebook/api/feedtype/FeedType;",
            ">;)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .prologue
    .line 197633
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 197634
    iget-object v2, p0, LX/17P;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 197635
    iget-object v4, p0, LX/17P;->g:LX/0W3;

    sget-wide v6, LX/0X5;->hX:J

    const/4 v5, 0x1

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    .line 197636
    sub-long v0, v2, v0

    const-wide/32 v2, 0x36ee80

    int-to-long v4, v4

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 197637
    :goto_0
    return-object p1

    .line 197638
    :cond_0
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    .line 197639
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "feed_unit_dropped"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "tracking"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "unit_type"

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "reason"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "native_newsfeed"

    .line 197640
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 197641
    move-object v4, v3

    .line 197642
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 197643
    const-string v5, "feed_type"

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v3}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "feed_name"

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/feedtype/FeedType;

    .line 197644
    iget-object v0, v3, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v3, v0

    .line 197645
    iget-object v0, v3, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v3, v0

    .line 197646
    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 197647
    :cond_1
    move-object v3, v4

    .line 197648
    iget-object v4, p0, LX/17P;->b:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 197649
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public static a(LX/17P;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStory;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 197418
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 197419
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 197420
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 197421
    iget-object v4, p0, LX/17P;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0pP;->p:LX/0Tn;

    invoke-interface {v4, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v1, v1

    .line 197422
    if-eqz v1, :cond_2

    .line 197423
    :cond_1
    :goto_0
    return-object v0

    .line 197424
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 197425
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 197426
    invoke-direct {p0, v1, p1}, LX/17P;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v2

    .line 197427
    goto :goto_0

    .line 197428
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 197429
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 197430
    invoke-direct {p0, v1, p1}, LX/17P;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v0, v2

    .line 197431
    goto :goto_0

    .line 197432
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 197630
    invoke-static {p1, p2, p3}, LX/17Q;->a(LX/0lF;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 197631
    iget-object v1, p0, LX/17P;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 197632
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 197593
    const/4 v1, 0x0

    .line 197594
    if-nez p1, :cond_2

    move-object v0, v1

    .line 197595
    :goto_0
    move-object v0, v0

    .line 197596
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 197597
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 197598
    :goto_1
    iget-object v2, p0, LX/17P;->f:LX/17v;

    invoke-static {p2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 197599
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 197600
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    const/4 v4, 0x0

    .line 197601
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v1}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 197602
    :cond_0
    :goto_2
    move v0, v4

    .line 197603
    return v0

    :cond_1
    move-object v1, v0

    goto :goto_1

    .line 197604
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 197605
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, -0x1e53800c

    if-ne v5, v6, :cond_3

    .line 197606
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 197607
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 197608
    goto :goto_0

    .line 197609
    :cond_5
    invoke-static {v1}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 197610
    invoke-static {v2, v5}, LX/17v;->a(LX/17v;Landroid/net/Uri;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 197611
    invoke-static {v5}, LX/17d;->c(Landroid/net/Uri;)LX/32n;

    move-result-object v4

    .line 197612
    sget-object v5, LX/32n;->INSTALLED:LX/32n;

    if-ne v4, v5, :cond_6

    const-string v4, "already_present"

    .line 197613
    :goto_4
    invoke-static {v3, v0, v4}, LX/17Q;->a(LX/0lF;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 197614
    iget-object v5, v2, LX/17v;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 197615
    const/4 v4, 0x1

    goto :goto_2

    .line 197616
    :cond_6
    const-string v4, "applink_no_fallback"

    goto :goto_4

    .line 197617
    :cond_7
    invoke-static {v5}, LX/17d;->c(Landroid/net/Uri;)LX/32n;

    move-result-object v6

    sget-object p0, LX/32n;->NONE:LX/32n;

    if-ne v6, p0, :cond_0

    .line 197618
    iget-object v6, v2, LX/17v;->d:LX/17d;

    iget-object p0, v2, LX/17v;->c:Landroid/content/Context;

    invoke-virtual {v6, p0, v5}, LX/17d;->d(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v6

    .line 197619
    const/4 v5, 0x0

    .line 197620
    if-nez v6, :cond_9

    .line 197621
    const-string v5, "reengagement_fallback"

    .line 197622
    :cond_8
    :goto_5
    if-eqz v5, :cond_0

    .line 197623
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ad_treatment"

    invoke-direct {v6, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v6, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "unit_type"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "reason"

    invoke-virtual {v6, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "native_newsfeed"

    .line 197624
    iput-object p0, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 197625
    move-object v6, v6

    .line 197626
    move-object v5, v6

    .line 197627
    iget-object v6, v2, LX/17v;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_2

    .line 197628
    :cond_9
    const-string p0, "android.intent.action.DIAL"

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    const-string p0, "android.intent.action.CALL"

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 197629
    const-string v5, "reengagement"

    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 197592
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/17P;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0am;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;LX/0am;)Lcom/facebook/graphql/model/FeedUnit;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/0am",
            "<",
            "Lcom/facebook/api/feedtype/FeedType;",
            ">;)",
            "Lcom/facebook/graphql/model/FeedUnit;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 197433
    if-nez p1, :cond_1

    move-object p1, v1

    .line 197434
    :cond_0
    :goto_0
    return-object p1

    .line 197435
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 197436
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {p0, v0}, LX/17P;->a(LX/17P;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0

    .line 197437
    :cond_2
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_5

    .line 197438
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 v1, 0x0

    .line 197439
    if-eqz p1, :cond_3

    .line 197440
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_20

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_20

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 197441
    if-nez v0, :cond_1c

    :cond_3
    move-object p1, v1

    .line 197442
    :cond_4
    :goto_2
    move-object p1, p1

    .line 197443
    goto :goto_0

    .line 197444
    :cond_5
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    if-eqz v0, :cond_6

    .line 197445
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-direct {p0, p1, p2}, LX/17P;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;LX/0am;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p1

    goto :goto_0

    .line 197446
    :cond_6
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_7

    .line 197447
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 197448
    invoke-static {p1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 197449
    :goto_3
    move-object p1, p1

    .line 197450
    goto :goto_0

    .line 197451
    :cond_7
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    if-eqz v0, :cond_9

    .line 197452
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;

    const/4 v0, 0x0

    .line 197453
    const/4 v2, 0x0

    .line 197454
    invoke-static {p1}, LX/2ch;->c(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {p1}, LX/2ch;->c(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {p1}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {p1}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->o()LX/2ci;

    move-result-object v1

    if-nez v1, :cond_23

    :cond_8
    move v1, v2

    .line 197455
    :goto_4
    move v1, v1

    .line 197456
    if-eqz v1, :cond_22

    .line 197457
    :goto_5
    move-object p1, p1

    .line 197458
    goto/16 :goto_0

    .line 197459
    :cond_9
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_b

    .line 197460
    check-cast p1, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    .line 197461
    const/4 v1, 0x0

    .line 197462
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;->w()LX/0Px;

    move-result-object v3

    .line 197463
    if-eqz v3, :cond_a

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    :cond_a
    move v0, v1

    .line 197464
    :goto_6
    move v0, v0

    .line 197465
    if-eqz v0, :cond_26

    .line 197466
    :goto_7
    move-object p1, p1

    .line 197467
    goto/16 :goto_0

    .line 197468
    :cond_b
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v0, :cond_c

    .line 197469
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 197470
    invoke-static {p1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 197471
    :goto_8
    move-object p1, p1

    .line 197472
    goto/16 :goto_0

    .line 197473
    :cond_c
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;

    if-eqz v0, :cond_d

    .line 197474
    check-cast p1, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;

    const/4 v0, 0x0

    .line 197475
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    if-eqz v1, :cond_2c

    const/4 v1, 0x1

    :goto_9
    move v1, v1

    .line 197476
    if-eqz v1, :cond_2b

    .line 197477
    :goto_a
    move-object p1, p1

    .line 197478
    goto/16 :goto_0

    .line 197479
    :cond_d
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    if-eqz v0, :cond_f

    .line 197480
    check-cast p1, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;

    const/4 v0, 0x0

    .line 197481
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->u()Z

    move-result v1

    if-eqz v1, :cond_2d

    move-object p1, v0

    .line 197482
    :cond_e
    :goto_b
    move-object p1, p1

    .line 197483
    goto/16 :goto_0

    .line 197484
    :cond_f
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    if-eqz v0, :cond_11

    .line 197485
    check-cast p1, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;

    const/4 v0, 0x0

    .line 197486
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->K()Z

    move-result v1

    if-eqz v1, :cond_2e

    move-object p1, v0

    .line 197487
    :cond_10
    :goto_c
    move-object p1, p1

    .line 197488
    goto/16 :goto_0

    .line 197489
    :cond_11
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    if-eqz v0, :cond_13

    .line 197490
    iget-object v0, p0, LX/17P;->e:LX/17S;

    invoke-virtual {v0}, LX/17S;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 197491
    iget-object v0, p0, LX/17P;->e:LX/17S;

    invoke-virtual {v0}, LX/17S;->l()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 197492
    :cond_12
    :goto_d
    move-object p1, p1

    .line 197493
    goto/16 :goto_0

    .line 197494
    :cond_13
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_15

    move-object v0, p1

    .line 197495
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    const/4 v3, 0x0

    .line 197496
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v5

    .line 197497
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_14

    if-eqz v5, :cond_14

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    if-nez v2, :cond_30

    :cond_14
    move v2, v3

    .line 197498
    :goto_e
    move v0, v2

    .line 197499
    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197500
    :cond_15
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_17

    move-object v0, p1

    .line 197501
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    const/4 v3, 0x0

    .line 197502
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_33

    :cond_16
    move v2, v3

    .line 197503
    :goto_f
    move v0, v2

    .line 197504
    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197505
    :cond_17
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v0, :cond_18

    move-object v0, p1

    .line 197506
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    const/4 v3, 0x0

    .line 197507
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v2

    .line 197508
    if-nez v2, :cond_36

    move v2, v3

    .line 197509
    :goto_10
    move v0, v2

    .line 197510
    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197511
    :cond_18
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    if-eqz v0, :cond_19

    move-object v0, p1

    .line 197512
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197513
    :cond_19
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    if-eqz v0, :cond_1a

    move-object v0, p1

    .line 197514
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v0}, LX/23B;->b(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197515
    :cond_1a
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    if-eqz v0, :cond_1b

    move-object v0, p1

    .line 197516
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 197517
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3b

    const/4 v2, 0x1

    :goto_11
    move v0, v2

    .line 197518
    if-nez v0, :cond_0

    move-object p1, v1

    goto/16 :goto_0

    .line 197519
    :cond_1b
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLUnknownFeedUnit;

    if-eqz v0, :cond_0

    .line 197520
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 197521
    if-nez v0, :cond_0

    move-object p1, v1

    .line 197522
    goto/16 :goto_0

    .line 197523
    :cond_1c
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 197524
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 197525
    invoke-static {p1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v2, v0

    :goto_12
    if-ge v2, v6, :cond_1e

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 197526
    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p2

    .line 197527
    invoke-static {p0, p2}, LX/17P;->a(LX/17P;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    if-eqz p2, :cond_1d

    .line 197528
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 197529
    :cond_1d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_12

    .line 197530
    :cond_1e
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 197531
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1f

    move-object p1, v1

    .line 197532
    goto/16 :goto_2

    .line 197533
    :cond_1f
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-static {p1}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-eq v1, v2, :cond_4

    .line 197534
    invoke-static {p1}, LX/4Yw;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/4Yw;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->x()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v2

    invoke-static {v2}, LX/4Yy;->a(Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;)LX/4Yy;

    move-result-object v2

    invoke-static {v0}, LX/16z;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 197535
    iput-object v0, v2, LX/4Yy;->c:LX/0Px;

    .line 197536
    move-object v0, v2

    .line 197537
    invoke-virtual {v0}, LX/4Yy;->a()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v0

    .line 197538
    iput-object v0, v1, LX/4Yw;->c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 197539
    move-object v0, v1

    .line 197540
    invoke-virtual {v0}, LX/4Yw;->a()Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object p1

    goto/16 :goto_2

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 197541
    :cond_21
    invoke-static {p1}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    invoke-static {p0, v0, v1, v2}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    .line 197542
    const/4 p1, 0x0

    goto/16 :goto_3

    .line 197543
    :cond_22
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    invoke-static {p0, v0, v1, v2}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    move-object p1, v0

    .line 197544
    goto/16 :goto_5

    .line 197545
    :cond_23
    invoke-static {p1}, LX/2ch;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnit;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    .line 197546
    :goto_13
    if-ge v3, v5, :cond_25

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;

    .line 197547
    invoke-static {v1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSavedCollectionFeedUnitItem;)Z

    move-result v1

    if-nez v1, :cond_24

    move v1, v2

    .line 197548
    goto/16 :goto_4

    .line 197549
    :cond_24
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_13

    .line 197550
    :cond_25
    const/4 v1, 0x1

    goto/16 :goto_4

    .line 197551
    :cond_26
    invoke-static {p1}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    invoke-static {p0, v0, v1, v2}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    .line 197552
    const/4 p1, 0x0

    goto/16 :goto_7

    .line 197553
    :cond_27
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_14
    if-ge v2, v4, :cond_29

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 197554
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;)Z

    move-result v0

    if-nez v0, :cond_28

    move v0, v1

    .line 197555
    goto/16 :goto_6

    .line 197556
    :cond_28
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 197557
    :cond_29
    const/4 v0, 0x1

    goto/16 :goto_6

    .line 197558
    :cond_2a
    invoke-static {p1}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    invoke-static {p0, v0, v1, v2}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    .line 197559
    const/4 p1, 0x0

    goto/16 :goto_8

    .line 197560
    :cond_2b
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLHoldoutAdFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    const-string v2, "invalid_data"

    invoke-static {p0, v0, v1, v2}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    move-object p1, v0

    .line 197561
    goto/16 :goto_a

    :cond_2c
    const/4 v1, 0x0

    goto/16 :goto_9

    .line 197562
    :cond_2d
    invoke-static {p1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 197563
    invoke-static {p1}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSurveyFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    const-string v3, "invalid_data"

    invoke-static {p0, v1, v2, v3}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    move-object p1, v0

    .line 197564
    goto/16 :goto_b

    .line 197565
    :cond_2e
    invoke-static {p1}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 197566
    invoke-static {p1}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLResearchPollFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    const-string v3, "invalid_data"

    invoke-static {p0, v1, v2, v3}, LX/17P;->a(LX/17P;LX/162;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)V

    move-object p1, v0

    .line 197567
    goto/16 :goto_c

    .line 197568
    :cond_2f
    iget-object v0, p0, LX/17P;->b:LX/0Zb;

    .line 197569
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ig_pff_invalidation"

    invoke-direct {v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "native_newsfeed"

    .line 197570
    iput-object p0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 197571
    move-object v1, v1

    .line 197572
    move-object v1, v1

    .line 197573
    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 197574
    const/4 p1, 0x0

    goto/16 :goto_d

    .line 197575
    :cond_30
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_15
    if-ge v4, v6, :cond_32

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;

    .line 197576
    invoke-static {v2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnitItem;)Z

    move-result v2

    if-nez v2, :cond_31

    move v2, v3

    .line 197577
    goto/16 :goto_e

    .line 197578
    :cond_31
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_15

    .line 197579
    :cond_32
    const/4 v2, 0x1

    goto/16 :goto_e

    .line 197580
    :cond_33
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_16
    if-ge v4, v6, :cond_35

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    .line 197581
    invoke-static {v2}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;)Z

    move-result v2

    if-nez v2, :cond_34

    move v2, v3

    .line 197582
    goto/16 :goto_f

    .line 197583
    :cond_34
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_16

    .line 197584
    :cond_35
    const/4 v2, 0x1

    goto/16 :goto_f

    .line 197585
    :cond_36
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v2

    .line 197586
    if-eqz v2, :cond_37

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_38

    :cond_37
    move v2, v3

    .line 197587
    goto/16 :goto_10

    .line 197588
    :cond_38
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_39
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 197589
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->a()Z

    move-result v2

    if-nez v2, :cond_39

    move v2, v3

    .line 197590
    goto/16 :goto_10

    .line 197591
    :cond_3a
    const/4 v2, 0x1

    goto/16 :goto_10

    :cond_3b
    const/4 v2, 0x0

    goto/16 :goto_11
.end method
