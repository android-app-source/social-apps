.class public LX/0RL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<-TT;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/reflect/Type;

.field public final c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59900
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/0RL;->b(Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    .line 59901
    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, LX/0RM;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/0RL;->a:Ljava/lang/Class;

    .line 59902
    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, LX/0RL;->c:I

    .line 59903
    invoke-direct {p0}, LX/0RL;->c()V

    .line 59904
    return-void
.end method

.method public constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 2

    .prologue
    .line 59883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59884
    if-nez p1, :cond_0

    .line 59885
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "type is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59886
    :cond_0
    invoke-static {p1}, LX/0RM;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    .line 59887
    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, LX/0RM;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/0RL;->a:Ljava/lang/Class;

    .line 59888
    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, LX/0RL;->c:I

    .line 59889
    invoke-direct {p0}, LX/0RL;->c()V

    .line 59890
    return-void
.end method

.method public static a(Ljava/lang/reflect/Type;)LX/0RL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "LX/0RL",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 59898
    new-instance v0, LX/0RL;

    invoke-direct {v0, p0}, LX/0RL;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;)Ljava/lang/reflect/Type;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Type;"
        }
    .end annotation

    .prologue
    .line 59893
    invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 59894
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_0

    .line 59895
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Missing type parameter."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59896
    :cond_0
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 59897
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, LX/0RM;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 59905
    iget-object v0, p0, LX/0RL;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59906
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Primitive types are not allowed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/0RL;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59907
    :cond_0
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 59892
    instance-of v0, p1, LX/0RL;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    check-cast p1, LX/0RL;

    iget-object v1, p1, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-static {v0, v1}, LX/0RM;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 59891
    iget v0, p0, LX/0RL;->c:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59882
    iget-object v0, p0, LX/0RL;->b:Ljava/lang/reflect/Type;

    invoke-static {v0}, LX/0RM;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
