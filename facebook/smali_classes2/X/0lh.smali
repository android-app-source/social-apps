.class public final LX/0lh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x448d3f549d79fc0eL


# instance fields
.field public final _annotationIntrospector:LX/0lU;

.field public final _classIntrospector:LX/0lM;

.field public final _dateFormat:Ljava/text/DateFormat;

.field public final _defaultBase64:LX/0ln;

.field public final _handlerInstantiator:LX/4py;

.field public final _locale:Ljava/util/Locale;

.field public final _propertyNamingStrategy:LX/4pt;

.field public final _timeZone:Ljava/util/TimeZone;

.field public final _typeFactory:LX/0li;

.field public final _typeResolverBuilder:LX/4qy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4qy",
            "<*>;"
        }
    .end annotation
.end field

.field public final _visibilityChecker:LX/0lW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0lW",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lM;LX/0lU;LX/0lW;LX/4pt;LX/0li;LX/4qy;Ljava/text/DateFormat;LX/4py;Ljava/util/Locale;Ljava/util/TimeZone;LX/0ln;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lM;",
            "LX/0lU;",
            "LX/0lW",
            "<*>;",
            "LX/4pt;",
            "LX/0li;",
            "LX/4qy",
            "<*>;",
            "Ljava/text/DateFormat;",
            "LX/4py;",
            "Ljava/util/Locale;",
            "Ljava/util/TimeZone;",
            "LX/0ln;",
            ")V"
        }
    .end annotation

    .prologue
    .line 130111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130112
    iput-object p1, p0, LX/0lh;->_classIntrospector:LX/0lM;

    .line 130113
    iput-object p2, p0, LX/0lh;->_annotationIntrospector:LX/0lU;

    .line 130114
    iput-object p3, p0, LX/0lh;->_visibilityChecker:LX/0lW;

    .line 130115
    iput-object p4, p0, LX/0lh;->_propertyNamingStrategy:LX/4pt;

    .line 130116
    iput-object p5, p0, LX/0lh;->_typeFactory:LX/0li;

    .line 130117
    iput-object p6, p0, LX/0lh;->_typeResolverBuilder:LX/4qy;

    .line 130118
    iput-object p7, p0, LX/0lh;->_dateFormat:Ljava/text/DateFormat;

    .line 130119
    iput-object p8, p0, LX/0lh;->_handlerInstantiator:LX/4py;

    .line 130120
    iput-object p9, p0, LX/0lh;->_locale:Ljava/util/Locale;

    .line 130121
    iput-object p10, p0, LX/0lh;->_timeZone:Ljava/util/TimeZone;

    .line 130122
    iput-object p11, p0, LX/0lh;->_defaultBase64:LX/0ln;

    .line 130123
    return-void
.end method


# virtual methods
.method public final a(LX/0li;)LX/0lh;
    .locals 12

    .prologue
    .line 130125
    iget-object v0, p0, LX/0lh;->_typeFactory:LX/0li;

    if-ne v0, p1, :cond_0

    .line 130126
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/0lh;

    iget-object v1, p0, LX/0lh;->_classIntrospector:LX/0lM;

    iget-object v2, p0, LX/0lh;->_annotationIntrospector:LX/0lU;

    iget-object v3, p0, LX/0lh;->_visibilityChecker:LX/0lW;

    iget-object v4, p0, LX/0lh;->_propertyNamingStrategy:LX/4pt;

    iget-object v6, p0, LX/0lh;->_typeResolverBuilder:LX/4qy;

    iget-object v7, p0, LX/0lh;->_dateFormat:Ljava/text/DateFormat;

    iget-object v8, p0, LX/0lh;->_handlerInstantiator:LX/4py;

    iget-object v9, p0, LX/0lh;->_locale:Ljava/util/Locale;

    iget-object v10, p0, LX/0lh;->_timeZone:Ljava/util/TimeZone;

    iget-object v11, p0, LX/0lh;->_defaultBase64:LX/0ln;

    move-object v5, p1

    invoke-direct/range {v0 .. v11}, LX/0lh;-><init>(LX/0lM;LX/0lU;LX/0lW;LX/4pt;LX/0li;LX/4qy;Ljava/text/DateFormat;LX/4py;Ljava/util/Locale;Ljava/util/TimeZone;LX/0ln;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/0np;LX/0lX;)LX/0lh;
    .locals 12

    .prologue
    .line 130124
    new-instance v0, LX/0lh;

    iget-object v1, p0, LX/0lh;->_classIntrospector:LX/0lM;

    iget-object v2, p0, LX/0lh;->_annotationIntrospector:LX/0lU;

    iget-object v3, p0, LX/0lh;->_visibilityChecker:LX/0lW;

    invoke-interface {v3, p1, p2}, LX/0lW;->a(LX/0np;LX/0lX;)LX/0lW;

    move-result-object v3

    iget-object v4, p0, LX/0lh;->_propertyNamingStrategy:LX/4pt;

    iget-object v5, p0, LX/0lh;->_typeFactory:LX/0li;

    iget-object v6, p0, LX/0lh;->_typeResolverBuilder:LX/4qy;

    iget-object v7, p0, LX/0lh;->_dateFormat:Ljava/text/DateFormat;

    iget-object v8, p0, LX/0lh;->_handlerInstantiator:LX/4py;

    iget-object v9, p0, LX/0lh;->_locale:Ljava/util/Locale;

    iget-object v10, p0, LX/0lh;->_timeZone:Ljava/util/TimeZone;

    iget-object v11, p0, LX/0lh;->_defaultBase64:LX/0ln;

    invoke-direct/range {v0 .. v11}, LX/0lh;-><init>(LX/0lM;LX/0lU;LX/0lW;LX/4pt;LX/0li;LX/4qy;Ljava/text/DateFormat;LX/4py;Ljava/util/Locale;Ljava/util/TimeZone;LX/0ln;)V

    return-object v0
.end method
