.class public final LX/1Ui;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/1y5;",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 256678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 256679
    check-cast p1, LX/1y5;

    const/4 v0, 0x0

    .line 256680
    if-nez p1, :cond_0

    .line 256681
    :goto_0
    return-object v0

    .line 256682
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 256683
    invoke-interface {p1}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    invoke-interface {p1}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/1y5;->m()LX/3Bf;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, LX/1y5;->m()LX/3Bf;

    move-result-object v0

    invoke-interface {v0}, LX/3Bf;->b()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-interface {p1}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v0, v4}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 256684
    goto :goto_0
.end method
