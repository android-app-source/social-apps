.class public LX/1E9;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219281
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 219282
    return-void
.end method

.method public static a(LX/0ad;LX/1E5;LX/1E6;LX/1E7;LX/1E8;Landroid/content/Context;)LX/1EE;
    .locals 5
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 219283
    new-instance v0, LX/1EA;

    invoke-direct {v0}, LX/1EA;-><init>()V

    const v1, 0x7f0814d7

    invoke-virtual {p5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 219284
    iput-object v1, v0, LX/1EA;->l:Ljava/lang/String;

    .line 219285
    move-object v0, v0

    .line 219286
    const v1, 0x7f0814d8

    invoke-virtual {p5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 219287
    iput-object v1, v0, LX/1EA;->m:Ljava/lang/String;

    .line 219288
    move-object v0, v0

    .line 219289
    const v1, 0x7f0814d9

    invoke-virtual {p5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 219290
    iput-object v1, v0, LX/1EA;->n:Ljava/lang/String;

    .line 219291
    move-object v0, v0

    .line 219292
    sget-char v1, LX/1EB;->x:C

    const v2, 0x7f081405

    invoke-virtual {p5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 219293
    iput-object v1, v0, LX/1EA;->o:Ljava/lang/String;

    .line 219294
    move-object v1, v0

    .line 219295
    invoke-virtual {p4}, LX/1E8;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f020964

    .line 219296
    :goto_0
    iput v0, v1, LX/1EA;->e:I

    .line 219297
    move-object v1, v1

    .line 219298
    invoke-virtual {p1}, LX/1E5;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 219299
    invoke-virtual {p2}, LX/1E6;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p2}, LX/1E6;->a()Ljava/lang/String;

    move-result-object v0

    .line 219300
    :goto_1
    iput-object v0, v1, LX/1EA;->l:Ljava/lang/String;

    .line 219301
    move-object v0, v1

    .line 219302
    const v2, 0x7f02090f

    .line 219303
    iput v2, v0, LX/1EA;->c:I

    .line 219304
    move-object v0, v0

    .line 219305
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1196

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 219306
    iput v2, v0, LX/1EA;->i:I

    .line 219307
    move-object v0, v0

    .line 219308
    iput-boolean v4, v0, LX/1EA;->t:Z

    .line 219309
    :cond_0
    :goto_2
    iget-object v0, p4, LX/1E8;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 219310
    iget-object v0, p4, LX/1E8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EC;->j:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p4, LX/1E8;->c:Ljava/lang/Boolean;

    .line 219311
    :cond_1
    iget-object v0, p4, LX/1E8;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 219312
    if-eqz v0, :cond_4

    .line 219313
    iput-boolean v4, v1, LX/1EA;->u:Z

    .line 219314
    move-object v2, v1

    .line 219315
    invoke-virtual {p1}, LX/1E5;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 219316
    iget-object v0, p4, LX/1E8;->e:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 219317
    iget-object v0, p4, LX/1E8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v4, LX/1EC;->n:I

    iget-object v3, p4, LX/1E8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const p0, 0x7f0a0094

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v0, v4, v3}, LX/0ad;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p4, LX/1E8;->e:Ljava/lang/Integer;

    .line 219318
    :cond_2
    iget-object v0, p4, LX/1E8;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v0, v0

    .line 219319
    :goto_3
    iput v0, v2, LX/1EA;->f:I

    .line 219320
    move-object v0, v2

    .line 219321
    invoke-virtual {p4}, LX/1E8;->e()I

    move-result v2

    .line 219322
    iput v2, v0, LX/1EA;->g:I

    .line 219323
    move-object v0, v0

    .line 219324
    iget-object v2, p4, LX/1E8;->h:Ljava/lang/Integer;

    if-nez v2, :cond_3

    .line 219325
    iget-object v2, p4, LX/1E8;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget v3, LX/1EC;->i:I

    const v4, -0xaea91

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p4, LX/1E8;->h:Ljava/lang/Integer;

    .line 219326
    :cond_3
    iget-object v2, p4, LX/1E8;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v2, v2

    .line 219327
    iput v2, v0, LX/1EA;->h:I

    .line 219328
    :cond_4
    iget-object v0, p4, LX/1E8;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    .line 219329
    iget-object v0, p4, LX/1E8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EC;->r:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p4, LX/1E8;->n:Ljava/lang/Boolean;

    .line 219330
    :cond_5
    iget-object v0, p4, LX/1E8;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 219331
    if-eqz v0, :cond_6

    .line 219332
    invoke-virtual {p4}, LX/1E8;->n()I

    move-result v0

    .line 219333
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1EA;->k:Ljava/lang/Integer;

    .line 219334
    :cond_6
    invoke-virtual {p4}, LX/1E8;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 219335
    const v0, 0x7f0207b3

    .line 219336
    iput v0, v1, LX/1EA;->d:I

    .line 219337
    :cond_7
    const-string v0, "CONTROL"

    invoke-static {p3}, LX/1E7;->c(LX/1E7;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 219338
    if-eqz v0, :cond_9

    .line 219339
    const/4 v0, -0x1

    .line 219340
    invoke-static {p3}, LX/1E7;->c(LX/1E7;)Ljava/lang/String;

    move-result-object v2

    .line 219341
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_8
    move v2, v0

    :goto_5
    packed-switch v2, :pswitch_data_0

    .line 219342
    :goto_6
    move v0, v0

    .line 219343
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/1EA;->j:Ljava/lang/Integer;

    .line 219344
    :cond_9
    invoke-virtual {v1}, LX/1EA;->a()LX/1EE;

    move-result-object v0

    return-object v0

    .line 219345
    :cond_a
    const v0, 0x7f020966

    goto/16 :goto_0

    .line 219346
    :cond_b
    const v0, 0x7f0813f9

    invoke-virtual {p5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 219347
    :cond_c
    invoke-virtual {p4}, LX/1E8;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219348
    const v0, 0x7f02080f

    .line 219349
    iput v0, v1, LX/1EA;->c:I

    .line 219350
    goto/16 :goto_2

    .line 219351
    :cond_d
    invoke-virtual {p4}, LX/1E8;->d()I

    move-result v0

    goto/16 :goto_3

    :cond_e
    const/4 v0, 0x0

    goto :goto_4

    .line 219352
    :sswitch_0
    const-string v3, "FIG_50"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    goto :goto_5

    :sswitch_1
    const-string v3, "FIG_80"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    goto :goto_5

    .line 219353
    :pswitch_0
    iget-object v0, p3, LX/1E7;->b:Landroid/content/Context;

    const v2, 0x7f0a00ba

    invoke-static {v0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_6

    .line 219354
    :pswitch_1
    iget-object v0, p3, LX/1E7;->b:Landroid/content/Context;

    const v2, 0x7f0a00c0

    invoke-static {v0, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x7b99a0d6 -> :sswitch_0
        0x7b99a133 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 219355
    return-void
.end method
