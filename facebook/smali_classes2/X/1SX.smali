.class public abstract LX/1SX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/1Pf;

.field private final D:LX/0qn;

.field public final E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation
.end field

.field public final F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field public final G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;"
        }
    .end annotation
.end field

.field public H:Ljava/lang/String;

.field private I:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public J:LX/1Sp;

.field public K:LX/0wL;

.field private L:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public final M:LX/1Sl;

.field public final N:LX/0tX;

.field public final O:LX/1Sm;

.field private P:LX/1Sn;

.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Ck;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0lC;

.field public final f:LX/1Sa;

.field public final g:LX/1Sj;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/16H;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Sh;

.field public final l:LX/0bH;

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0SG;

.field private final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper$IFeedUnitMenuOptions;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Zb;

.field private final u:LX/17Q;

.field public final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/14w;

.field public final x:LX/0ad;

.field public final y:LX/0W3;

.field private z:Z


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V
    .locals 11
    .param p16    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsGroupCommerceNewDeleteInterceptEnabled;
        .end annotation
    .end param
    .param p23    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/1Pf;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248034
    const/4 v1, 0x0

    iput-object v1, p0, LX/1SX;->H:Ljava/lang/String;

    .line 248035
    const/4 v1, 0x0

    iput-object v1, p0, LX/1SX;->J:LX/1Sp;

    .line 248036
    iput-object p1, p0, LX/1SX;->a:LX/0Or;

    .line 248037
    iput-object p2, p0, LX/1SX;->b:LX/0Or;

    .line 248038
    iput-object p4, p0, LX/1SX;->c:LX/1Ck;

    .line 248039
    move-object/from16 v0, p5

    iput-object v0, p0, LX/1SX;->d:LX/0Ot;

    .line 248040
    move-object/from16 v0, p6

    iput-object v0, p0, LX/1SX;->e:LX/0lC;

    .line 248041
    move-object/from16 v0, p7

    iput-object v0, p0, LX/1SX;->f:LX/1Sa;

    .line 248042
    move-object/from16 v0, p8

    iput-object v0, p0, LX/1SX;->g:LX/1Sj;

    .line 248043
    move-object/from16 v0, p9

    iput-object v0, p0, LX/1SX;->h:LX/0Ot;

    .line 248044
    move-object/from16 v0, p10

    iput-object v0, p0, LX/1SX;->i:LX/16H;

    .line 248045
    move-object/from16 v0, p11

    iput-object v0, p0, LX/1SX;->j:LX/0Or;

    .line 248046
    move-object/from16 v0, p12

    iput-object v0, p0, LX/1SX;->k:LX/0Sh;

    .line 248047
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1SX;->l:LX/0bH;

    .line 248048
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1SX;->p:LX/0Or;

    .line 248049
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1SX;->q:LX/0Or;

    .line 248050
    move-object/from16 v0, p21

    iput-object v0, p0, LX/1SX;->n:LX/0SG;

    .line 248051
    move-object/from16 v0, p24

    iput-object v0, p0, LX/1SX;->s:LX/0Or;

    .line 248052
    invoke-static/range {p18 .. p18}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17Q;

    iput-object v1, p0, LX/1SX;->u:LX/17Q;

    .line 248053
    invoke-static/range {p17 .. p17}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    iput-object v1, p0, LX/1SX;->t:LX/0Zb;

    .line 248054
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/1SX;->o:Ljava/util/Map;

    .line 248055
    move-object/from16 v0, p19

    iput-object v0, p0, LX/1SX;->v:LX/0Or;

    .line 248056
    move-object/from16 v0, p20

    iput-object v0, p0, LX/1SX;->m:LX/0Or;

    .line 248057
    move-object/from16 v0, p23

    iput-object v0, p0, LX/1SX;->r:LX/0Or;

    .line 248058
    move-object/from16 v0, p25

    iput-object v0, p0, LX/1SX;->w:LX/14w;

    .line 248059
    move-object/from16 v0, p27

    iput-object v0, p0, LX/1SX;->L:LX/0Or;

    .line 248060
    move-object/from16 v0, p28

    iput-object v0, p0, LX/1SX;->C:LX/1Pf;

    .line 248061
    move-object/from16 v0, p26

    iput-object v0, p0, LX/1SX;->x:LX/0ad;

    .line 248062
    move-object/from16 v0, p29

    iput-object v0, p0, LX/1SX;->D:LX/0qn;

    .line 248063
    move-object/from16 v0, p30

    iput-object v0, p0, LX/1SX;->y:LX/0W3;

    .line 248064
    move-object/from16 v0, p31

    iput-object v0, p0, LX/1SX;->A:LX/0Ot;

    .line 248065
    move-object/from16 v0, p32

    iput-object v0, p0, LX/1SX;->B:LX/0Or;

    .line 248066
    move-object/from16 v0, p33

    iput-object v0, p0, LX/1SX;->M:LX/1Sl;

    .line 248067
    move-object/from16 v0, p34

    iput-object v0, p0, LX/1SX;->N:LX/0tX;

    .line 248068
    move-object/from16 v0, p35

    iput-object v0, p0, LX/1SX;->O:LX/1Sm;

    .line 248069
    move-object/from16 v0, p36

    iput-object v0, p0, LX/1SX;->E:LX/0Ot;

    .line 248070
    move-object/from16 v0, p37

    iput-object v0, p0, LX/1SX;->G:LX/0Ot;

    .line 248071
    move-object/from16 v0, p38

    iput-object v0, p0, LX/1SX;->F:LX/0Ot;

    .line 248072
    new-instance v1, LX/1Sn;

    iget-object v10, p0, LX/1SX;->C:LX/1Pf;

    move-object/from16 v2, p25

    move-object/from16 v3, p29

    move-object/from16 v4, p26

    move-object v5, p3

    move-object v6, p2

    move-object/from16 v7, p15

    move-object/from16 v8, p22

    move-object v9, p1

    invoke-direct/range {v1 .. v10}, LX/1Sn;-><init>(LX/14w;LX/0qn;LX/0ad;LX/1Kf;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/1Pf;)V

    iput-object v1, p0, LX/1SX;->P:LX/1Sn;

    .line 248073
    move-object/from16 v0, p39

    iput-object v0, p0, LX/1SX;->K:LX/0wL;

    .line 248074
    return-void
.end method

.method public static synthetic a(LX/1SX;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 12

    .prologue
    .line 248075
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248076
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 248077
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    .line 248078
    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 248079
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 248080
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 248081
    :cond_0
    invoke-virtual {p0, v2, p3}, LX/1SX;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 248082
    :goto_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 248083
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    .line 248084
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v4

    .line 248085
    check-cast v6, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 248086
    invoke-interface {v6}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v4

    .line 248087
    iget-object v5, p0, LX/1SX;->H:Ljava/lang/String;

    move-object v5, v5

    .line 248088
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "story_hidden"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "hideable_token"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 248089
    iput-object v5, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 248090
    move-object v7, v7

    .line 248091
    move-object v8, v7

    .line 248092
    invoke-interface {v6}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    .line 248093
    iget-object v4, p0, LX/1SX;->t:LX/0Zb;

    invoke-interface {v4, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 248094
    :cond_1
    :goto_1
    return-void

    .line 248095
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v3, :cond_3

    .line 248096
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 248097
    invoke-virtual {p0, v0, p3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    goto :goto_0

    .line 248098
    :cond_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v3, :cond_4

    .line 248099
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {p0, v0, p3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    goto :goto_0

    .line 248100
    :cond_4
    invoke-virtual {p0, v0, p3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    goto :goto_0

    .line 248101
    :cond_5
    iget-object v4, p0, LX/1SX;->j:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1dv;

    iget-object v5, p0, LX/1SX;->J:LX/1Sp;

    invoke-interface {v5}, LX/1Sp;->a()LX/0wD;

    move-result-object v5

    invoke-virtual {v5}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v5

    const-string v7, ""

    invoke-virtual {v4, v2, v0, v5, v7}, LX/1dv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 248102
    iget-object v11, p0, LX/1SX;->k:LX/0Sh;

    new-instance v4, LX/Akd;

    move-object v5, p0

    move-object v7, p3

    move-object v9, v0

    invoke-direct/range {v4 .. v9}, LX/Akd;-><init>(LX/1SX;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    invoke-virtual {v11, v10, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method

.method public static c(LX/1SX;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 248103
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248104
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 248105
    :goto_0
    return v0

    .line 248106
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 248107
    invoke-virtual {p0, v0}, LX/1SX;->b(Lcom/facebook/graphql/model/FeedUnit;)LX/BoM;

    move-result-object v0

    .line 248108
    if-nez v0, :cond_2

    move v0, v1

    .line 248109
    goto :goto_0

    .line 248110
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    .line 248111
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 248112
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->e()LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    .line 248113
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_3

    .line 248114
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248115
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 248116
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 248117
    const p0, 0x7f081042

    invoke-interface {p1, p0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object p0

    .line 248118
    const p3, 0x7f0208cf

    invoke-interface {p0, p3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 248119
    new-instance p3, LX/BoK;

    invoke-direct {p3, v0, v1, v2, p2}, LX/BoK;-><init>(LX/BoM;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p0, p3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 248120
    const p0, 0x7f081043

    invoke-interface {p1, p0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object p0

    .line 248121
    const p3, 0x7f0209ae

    invoke-interface {p0, p3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 248122
    new-instance p3, LX/BoL;

    invoke-direct {p3, v0, v1, v2, p2}, LX/BoL;-><init>(LX/BoM;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p0, p3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public static g(LX/1SX;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation

    .prologue
    .line 248123
    iget-object v0, p0, LX/1SX;->I:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 248124
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Make sure you call setCurationSurface"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248125
    :cond_0
    iget-object v0, p0, LX/1SX;->I:Ljava/lang/String;

    return-object v0
.end method

.method public static final i(LX/1SX;)Lcom/facebook/content/SecureContextHelper;
    .locals 1

    .prologue
    .line 248126
    iget-object v0, p0, LX/1SX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    return-object v0
.end method

.method public static synthetic l(LX/1SX;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 248127
    iget-object v0, p0, LX/1SX;->H:Ljava/lang/String;

    move-object v0, v0

    .line 248128
    return-object v0
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 3

    .prologue
    .line 248129
    invoke-virtual {p0, p1}, LX/1SX;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    .line 248130
    iget-object v0, p0, LX/1SX;->o:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wH;

    .line 248131
    if-eqz v0, :cond_1

    .line 248132
    :cond_0
    :goto_0
    return-object v0

    .line 248133
    :cond_1
    invoke-virtual {p0, p1}, LX/1SX;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    .line 248134
    if-eqz v0, :cond_0

    .line 248135
    iget-object v2, p0, LX/1SX;->o:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 1

    .prologue
    .line 248136
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    invoke-virtual {v0, p1}, LX/1Sn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 248137
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 248138
    :goto_0
    sget-object v1, LX/AkY;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 248139
    :goto_1
    return-object v0

    .line 248140
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 248141
    :pswitch_0
    sget-object v1, LX/AkY;->b:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->k()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackTargetType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 248142
    :goto_2
    move-object v0, v0

    .line 248143
    goto :goto_1

    .line 248144
    :pswitch_1
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 248145
    if-eqz v1, :cond_1

    const v1, 0x7f0810b3

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_1
    const v1, 0x7f0810b2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 248146
    :pswitch_2
    const v1, 0x7f0810b5

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 248147
    :pswitch_3
    const v1, 0x7f0810b4

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 248148
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Pf;)V
    .locals 2

    .prologue
    .line 248149
    iput-object p1, p0, LX/1SX;->C:LX/1Pf;

    .line 248150
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    iget-object v1, p0, LX/1SX;->C:LX/1Pf;

    .line 248151
    iput-object v1, v0, LX/1Sn;->i:LX/1Pf;

    .line 248152
    return-void
.end method

.method public final a(LX/1Sp;)V
    .locals 0

    .prologue
    .line 248245
    iput-object p1, p0, LX/1SX;->J:LX/1Sp;

    .line 248246
    return-void
.end method

.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248153
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248154
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    .line 248155
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 248156
    :cond_0
    :goto_0
    return-void

    .line 248157
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 248158
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v4

    .line 248159
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, LX/1SX;->J:LX/1Sp;

    invoke-interface {v4}, LX/1Sp;->a()LX/0wD;

    move-result-object v4

    sget-object v5, LX/0wD;->SEARCH_RESULTS:LX/0wD;

    if-eq v4, v5, :cond_3

    .line 248160
    :cond_2
    invoke-virtual {p0, p1, p2, v0, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V

    .line 248161
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 248162
    :cond_4
    iget-object v0, p0, LX/1SX;->B:LX/0Or;

    if-eqz v0, :cond_0

    .line 248163
    iget-object v0, p0, LX/1SX;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "573681459506672"

    .line 248164
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 248165
    move-object v0, v0

    .line 248166
    invoke-virtual {v0}, LX/0gt;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 248167
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248168
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 248169
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1SX;->J:LX/1Sp;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    invoke-interface {v0, v6, v1}, LX/1Sp;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 248170
    :cond_0
    :goto_0
    return-void

    .line 248171
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p3}, LX/1SX;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    .line 248172
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1SX;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Ljava/lang/String;

    move-result-object v1

    .line 248173
    if-eqz v1, :cond_2

    instance-of v0, v7, LX/3Ai;

    if-eqz v0, :cond_2

    move-object v0, v7

    .line 248174
    check-cast v0, LX/3Ai;

    invoke-virtual {v0, v1}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 248175
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v3

    .line 248176
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v8

    .line 248177
    invoke-interface {v7}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, p2, v0, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 248178
    new-instance v0, LX/Aka;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Aka;-><init>(LX/1SX;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;Landroid/view/View;)V

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 248179
    invoke-static {v8}, LX/Al4;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result v0

    .line 248180
    invoke-virtual {p0, v7, v0, v6}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    .line 248181
    invoke-virtual {p0}, LX/1SX;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248182
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 248183
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 248184
    iget-boolean v0, p0, LX/1SX;->z:Z

    if-nez v0, :cond_1

    .line 248185
    :cond_0
    :goto_0
    return-void

    .line 248186
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248187
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 248188
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 248189
    invoke-static {p1}, LX/14w;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    .line 248190
    iget-object v1, p0, LX/1SX;->w:LX/14w;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/14w;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    .line 248191
    :goto_1
    iget-object v7, p0, LX/1SX;->t:LX/0Zb;

    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    move-object v0, p3

    move v1, p2

    move v2, p4

    .line 248192
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "feed_chevron"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "option_name"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "option_pos"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "is_secondary"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "self"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-virtual {p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "native_newsfeed"

    .line 248193
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 248194
    move-object p0, p0

    .line 248195
    const-string p1, "tracking"

    invoke-virtual {p0, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 248196
    if-eqz v2, :cond_3

    const-string p0, "clk"

    .line 248197
    :goto_2
    const-string p2, "event_type"

    invoke-virtual {p1, p2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 248198
    move-object v0, p1

    .line 248199
    invoke-interface {v7, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 248200
    :cond_2
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v1, :cond_0

    .line 248201
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->v()Z

    move-result v4

    move v5, v3

    .line 248202
    goto :goto_1

    .line 248203
    :cond_3
    const-string p0, "imp"

    goto :goto_2
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248204
    iget-object v0, p0, LX/1SX;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 248205
    if-eqz v3, :cond_1

    const v0, 0x7f082510

    move v2, v0

    .line 248206
    :goto_0
    if-eqz v3, :cond_2

    const v0, 0x7f08250d

    move v1, v0

    .line 248207
    :goto_1
    if-eqz v3, :cond_3

    const v0, 0x7f08250e

    .line 248208
    :goto_2
    new-instance v4, LX/31Y;

    invoke-direct {v4, p2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    new-instance v4, LX/Akf;

    invoke-direct {v4, p0, v3, p1, p2}, LX/Akf;-><init>(LX/1SX;ZLcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v2, v1, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/Ake;

    invoke-direct {v2, p0, p1, p2}, LX/Ake;-><init>(LX/1SX;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0810d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 248209
    if-eqz v3, :cond_0

    .line 248210
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08250f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 248211
    :cond_0
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 248212
    return-void

    .line 248213
    :cond_1
    const v0, 0x7f081051

    move v2, v0

    goto :goto_0

    .line 248214
    :cond_2
    const v0, 0x7f082503

    move v1, v0

    goto :goto_1

    .line 248215
    :cond_3
    const v0, 0x7f08105c

    goto :goto_2
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248216
    iget-object v0, p0, LX/1SX;->L:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 248217
    invoke-virtual {v0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 248218
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248219
    :cond_0
    :goto_0
    return-void

    .line 248220
    :cond_1
    new-instance v2, LX/7TY;

    invoke-direct {v2, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 248221
    const/4 v4, 0x0

    .line 248222
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 248223
    instance-of v3, v3, Lcom/facebook/graphql/model/FeedUnit;

    if-nez v3, :cond_3

    move v3, v4

    .line 248224
    :goto_1
    move v3, v3

    .line 248225
    if-nez v3, :cond_2

    invoke-static {p0, v2, p1, p2}, LX/1SX;->c(LX/1SX;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 248226
    :cond_2
    new-instance v3, LX/Aki;

    invoke-direct {v3, p0, v1, v0}, LX/Aki;-><init>(LX/1SX;Landroid/app/Activity;LX/3Af;)V

    .line 248227
    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 248228
    invoke-virtual {v0, v2}, LX/3Af;->a(LX/1OM;)V

    .line 248229
    invoke-static {v0}, LX/4ml;->a(Landroid/app/Dialog;)V

    .line 248230
    new-instance v1, LX/AkZ;

    invoke-direct {v1, p0, v3, p2}, LX/AkZ;-><init>(LX/1SX;LX/Aki;Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 248231
    :cond_3
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 248232
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v3}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v3

    .line 248233
    if-nez v3, :cond_4

    move v3, v4

    .line 248234
    goto :goto_1

    .line 248235
    :cond_4
    invoke-virtual {v3, v2, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 248236
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/enums/StoryVisibility;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/HideableUnit;",
            ">;",
            "Landroid/view/View;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Lcom/facebook/graphql/enums/StoryVisibility;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 248237
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 248238
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 248239
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v1, v0, LX/17w;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, LX/17w;

    invoke-static {v1}, LX/0x1;->a(LX/17w;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248240
    :cond_0
    iget-object v0, p0, LX/1SX;->t:LX/0Zb;

    invoke-interface {v0, p3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 248241
    :goto_0
    return-void

    .line 248242
    :cond_1
    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    .line 248243
    iget-object v1, p0, LX/1SX;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dv;

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p4, p5}, LX/1dv;->a(Lcom/facebook/graphql/model/HideableUnit;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 248244
    iget-object v2, p0, LX/1SX;->k:LX/0Sh;

    new-instance v3, LX/Akh;

    invoke-direct {v3, p0, p3, v0, p2}, LX/Akh;-><init>(LX/1SX;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public a(Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 248247
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    .line 248248
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 248249
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248250
    iget-object v1, v0, LX/1Sn;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Q3;

    invoke-virtual {v1, p1}, LX/8Q3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v3

    .line 248251
    iget-object v1, v0, LX/1Sn;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x740

    const-class v2, Landroid/app/Activity;

    invoke-static {p2, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-interface {v1, v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 248252
    :cond_0
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 248020
    instance-of v0, p1, LX/17w;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/17w;

    invoke-static {v0}, LX/0x1;->a(LX/17w;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248021
    check-cast p1, Lcom/facebook/graphql/model/HideableUnit;

    .line 248022
    iget-object v0, p0, LX/1SX;->n:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 248023
    invoke-static {p1, v0, v1}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 248024
    invoke-static {p1, v2}, LX/6X8;->a(Lcom/facebook/graphql/model/HideableUnit;Lcom/facebook/graphql/enums/StoryVisibility;)V

    .line 248025
    invoke-static {p1, v3}, LX/6X8;->a(Lcom/facebook/graphql/model/HideableUnit;I)V

    .line 248026
    :goto_0
    iget-object v0, p0, LX/1SX;->l:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 248027
    return-void

    .line 248028
    :cond_0
    iget-object v6, p0, LX/1SX;->l:LX/0bH;

    new-instance v0, LX/1Nd;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/1SX;->J:LX/1Sp;

    invoke-interface {v3}, LX/1Sp;->a()LX/0wD;

    move-result-object v3

    .line 248029
    sget-object v4, LX/AkY;->c:[I

    invoke-virtual {v3}, LX/0wD;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 248030
    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->CONTRACTING:Lcom/facebook/graphql/enums/StoryVisibility;

    :goto_1
    move-object v4, v4

    .line 248031
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 248032
    :pswitch_0
    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 248253
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    .line 248254
    invoke-static {p1}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248255
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248256
    iget-object v1, v0, LX/1Sn;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, p1, p0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 248257
    iget-object v1, v0, LX/1Sn;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, p0, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 248258
    :cond_0
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 247953
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    .line 247954
    iget-object v2, v0, LX/1Sn;->d:LX/1Kf;

    const/4 v3, 0x0

    invoke-virtual {v0, p1}, LX/1Sn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 p0, 0x6de

    const-class v1, Landroid/app/Activity;

    invoke-static {p2, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, p0, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 247955
    return-void
.end method

.method public a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 0

    .prologue
    .line 247958
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 247956
    iput-object p1, p0, LX/1SX;->I:Ljava/lang/String;

    .line 247957
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 247959
    iput-boolean p1, p0, LX/1SX;->z:Z

    .line 247960
    return-void
.end method

.method public b()LX/1Pf;
    .locals 1

    .prologue
    .line 247961
    iget-object v0, p0, LX/1SX;->C:LX/1Pf;

    return-object v0
.end method

.method public b(Lcom/facebook/graphql/model/FeedUnit;)LX/BoM;
    .locals 1

    .prologue
    .line 247962
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 247963
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 247964
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 247965
    iget-object v1, p0, LX/1SX;->l:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, p1}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 247966
    iget-object v1, p0, LX/1SX;->l:LX/0bH;

    new-instance v2, LX/1YM;

    invoke-direct {v2}, LX/1YM;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 247967
    iget-object v1, p0, LX/1SX;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dv;

    invoke-virtual {v1, v0}, LX/1dv;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 247968
    iget-object v2, p0, LX/1SX;->k:LX/0Sh;

    new-instance v3, LX/AkX;

    invoke-direct {v3, p0, v0}, LX/AkX;-><init>(LX/1SX;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 247969
    return-void
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 247970
    const-string v0, "573681459506672"

    invoke-static {v0}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247971
    iget-object v0, p0, LX/1SX;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "573681459506672"

    .line 247972
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 247973
    move-object v0, v0

    .line 247974
    sget-object v1, LX/1x2;->MESSENGER:LX/1x2;

    invoke-virtual {v0, v1}, LX/0gt;->a(LX/1x2;)LX/0gt;

    move-result-object v0

    const/4 v1, 0x1

    .line 247975
    iget-object v2, v0, LX/0gt;->b:LX/1ww;

    .line 247976
    iput-boolean v1, v2, LX/1ww;->h:Z

    .line 247977
    move-object v6, v0

    .line 247978
    new-instance v0, LX/Akw;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, LX/1SX;->J:LX/1Sp;

    invoke-interface {v1}, LX/1Sp;->a()LX/0wD;

    move-result-object v1

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, p2}, LX/1SX;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)LX/9lZ;

    move-result-object v5

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/Akw;-><init>(LX/1SX;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/9lZ;)V

    .line 247979
    iget-object v1, v6, LX/0gt;->b:LX/1ww;

    .line 247980
    iput-object v0, v1, LX/1ww;->i:LX/1bH;

    .line 247981
    move-object v0, v6

    .line 247982
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->b(Landroid/content/Context;)V

    .line 247983
    :goto_0
    return-void

    .line 247984
    :cond_0
    iget-object v0, p0, LX/1SX;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9yI;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/1SX;->J:LX/1Sp;

    invoke-interface {v2}, LX/1Sp;->a()LX/0wD;

    move-result-object v2

    invoke-virtual {v2}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2}, LX/1SX;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)LX/9lZ;

    move-result-object v3

    invoke-virtual {v0, v1, p1, v2, v3}, LX/9yI;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/9lZ;)V

    goto :goto_0
.end method

.method public b(Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 247985
    const-string v0, "native_newsfeed"

    invoke-virtual {p0, p1, v0, p2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Landroid/content/Context;)V

    .line 247986
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 247987
    iput-object p1, p0, LX/1SX;->H:Ljava/lang/String;

    .line 247988
    return-void
.end method

.method public c()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247989
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)LX/9lZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;",
            "Landroid/view/View;",
            ")",
            "LX/9lZ;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 247990
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247991
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 247992
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 247993
    new-instance v0, LX/1wH;

    invoke-direct {v0, p0}, LX/1wH;-><init>(LX/1SX;)V

    .line 247994
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 247995
    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 247996
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 247997
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    const/4 p0, 0x0

    .line 247998
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_0

    move v1, p0

    .line 247999
    :goto_0
    move v0, v1

    .line 248000
    return v0

    .line 248001
    :cond_0
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248002
    iget-object v1, v0, LX/1Sn;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, LX/0sa;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, LX/14w;->t(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v1, p0

    goto :goto_0
.end method

.method public f()LX/6Vy;
    .locals 1

    .prologue
    .line 248003
    sget-object v0, LX/6Vy;->FEED_POST_CHEVRON:LX/6Vy;

    return-object v0
.end method

.method public final f(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 248004
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    const/4 v1, 0x0

    .line 248005
    instance-of p0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez p0, :cond_1

    .line 248006
    :cond_0
    :goto_0
    move v0, v1

    .line 248007
    return v0

    .line 248008
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248009
    invoke-static {v0, p1}, LX/1Sn;->b(LX/1Sn;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, LX/14w;->t(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final g(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 248010
    iget-object v0, p0, LX/1SX;->P:LX/1Sn;

    const/4 v1, 0x0

    .line 248011
    instance-of p0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez p0, :cond_1

    .line 248012
    :cond_0
    :goto_0
    move v0, v1

    .line 248013
    return v0

    .line 248014
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248015
    invoke-static {v0, p1}, LX/1Sn;->b(LX/1Sn;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, LX/14w;->t(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final i(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 248016
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    .line 248017
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 248018
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1SX;->D:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 248019
    :cond_1
    return v0
.end method
