.class public final enum LX/0wG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0wG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0wG;

.field public static final enum CONTAIN_FIT:LX/0wG;

.field public static final enum COVER_FILL:LX/0wG;

.field public static final enum COVER_FILL_CROPPED:LX/0wG;


# instance fields
.field public sizeStyle:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 159562
    new-instance v0, LX/0wG;

    const-string v1, "CONTAIN_FIT"

    const-string v2, "contain-fit"

    invoke-direct {v0, v1, v3, v2}, LX/0wG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wG;->CONTAIN_FIT:LX/0wG;

    .line 159563
    new-instance v0, LX/0wG;

    const-string v1, "COVER_FILL"

    const-string v2, "cover-fill"

    invoke-direct {v0, v1, v4, v2}, LX/0wG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wG;->COVER_FILL:LX/0wG;

    .line 159564
    new-instance v0, LX/0wG;

    const-string v1, "COVER_FILL_CROPPED"

    const-string v2, "cover-fill-cropped"

    invoke-direct {v0, v1, v5, v2}, LX/0wG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/0wG;->COVER_FILL_CROPPED:LX/0wG;

    .line 159565
    const/4 v0, 0x3

    new-array v0, v0, [LX/0wG;

    sget-object v1, LX/0wG;->CONTAIN_FIT:LX/0wG;

    aput-object v1, v0, v3

    sget-object v1, LX/0wG;->COVER_FILL:LX/0wG;

    aput-object v1, v0, v4

    sget-object v1, LX/0wG;->COVER_FILL_CROPPED:LX/0wG;

    aput-object v1, v0, v5

    sput-object v0, LX/0wG;->$VALUES:[LX/0wG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 159560
    iput-object p3, p0, LX/0wG;->sizeStyle:Ljava/lang/String;

    .line 159561
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0wG;
    .locals 1

    .prologue
    .line 159566
    const-class v0, LX/0wG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0wG;

    return-object v0
.end method

.method public static values()[LX/0wG;
    .locals 1

    .prologue
    .line 159558
    sget-object v0, LX/0wG;->$VALUES:[LX/0wG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0wG;

    return-object v0
.end method


# virtual methods
.method public final styleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159557
    iget-object v0, p0, LX/0wG;->sizeStyle:Ljava/lang/String;

    return-object v0
.end method
