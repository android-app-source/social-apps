.class public final enum LX/1sR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1sR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1sR;

.field public static final enum HIGH_POWER:LX/1sR;

.field public static final enum LOW_POWER:LX/1sR;

.field public static final enum MEDIUM_POWER:LX/1sR;

.field public static final enum NEVER:LX/1sR;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 334739
    new-instance v0, LX/1sR;

    const-string v1, "LOW_POWER"

    invoke-direct {v0, v1, v2}, LX/1sR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1sR;->LOW_POWER:LX/1sR;

    .line 334740
    new-instance v0, LX/1sR;

    const-string v1, "MEDIUM_POWER"

    invoke-direct {v0, v1, v3}, LX/1sR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1sR;->MEDIUM_POWER:LX/1sR;

    .line 334741
    new-instance v0, LX/1sR;

    const-string v1, "HIGH_POWER"

    invoke-direct {v0, v1, v4}, LX/1sR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1sR;->HIGH_POWER:LX/1sR;

    .line 334742
    new-instance v0, LX/1sR;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v5}, LX/1sR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1sR;->NEVER:LX/1sR;

    .line 334743
    const/4 v0, 0x4

    new-array v0, v0, [LX/1sR;

    sget-object v1, LX/1sR;->LOW_POWER:LX/1sR;

    aput-object v1, v0, v2

    sget-object v1, LX/1sR;->MEDIUM_POWER:LX/1sR;

    aput-object v1, v0, v3

    sget-object v1, LX/1sR;->HIGH_POWER:LX/1sR;

    aput-object v1, v0, v4

    sget-object v1, LX/1sR;->NEVER:LX/1sR;

    aput-object v1, v0, v5

    sput-object v0, LX/1sR;->$VALUES:[LX/1sR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 334744
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1sR;
    .locals 1

    .prologue
    .line 334738
    const-class v0, LX/1sR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1sR;

    return-object v0
.end method

.method public static values()[LX/1sR;
    .locals 1

    .prologue
    .line 334737
    sget-object v0, LX/1sR;->$VALUES:[LX/1sR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1sR;

    return-object v0
.end method
