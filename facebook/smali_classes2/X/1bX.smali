.class public LX/1bX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:LX/1bY;

.field public c:LX/1o9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1bd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/1bZ;

.field public f:LX/1bb;

.field public g:Z

.field public h:Z

.field public i:LX/1bc;

.field public j:LX/33B;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:LX/1BU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1ny;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 280597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280598
    iput-object v1, p0, LX/1bX;->a:Landroid/net/Uri;

    .line 280599
    sget-object v0, LX/1bY;->FULL_FETCH:LX/1bY;

    iput-object v0, p0, LX/1bX;->b:LX/1bY;

    .line 280600
    iput-object v1, p0, LX/1bX;->c:LX/1o9;

    .line 280601
    iput-object v1, p0, LX/1bX;->d:LX/1bd;

    .line 280602
    sget-object v0, LX/1bZ;->g:LX/1bZ;

    move-object v0, v0

    .line 280603
    iput-object v0, p0, LX/1bX;->e:LX/1bZ;

    .line 280604
    sget-object v0, LX/1bb;->DEFAULT:LX/1bb;

    iput-object v0, p0, LX/1bX;->f:LX/1bb;

    .line 280605
    sget-object v0, LX/1H6;->x:LX/1H7;

    move-object v0, v0

    .line 280606
    iget-boolean v2, v0, LX/1H7;->a:Z

    move v0, v2

    .line 280607
    iput-boolean v0, p0, LX/1bX;->g:Z

    .line 280608
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1bX;->h:Z

    .line 280609
    sget-object v0, LX/1bc;->HIGH:LX/1bc;

    iput-object v0, p0, LX/1bX;->i:LX/1bc;

    .line 280610
    iput-object v1, p0, LX/1bX;->j:LX/33B;

    .line 280611
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1bX;->k:Z

    .line 280612
    iput-object v1, p0, LX/1bX;->m:LX/1ny;

    .line 280613
    return-void
.end method

.method public static a(I)LX/1bX;
    .locals 1

    .prologue
    .line 280614
    invoke-static {p0}, LX/1be;->a(I)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1bf;)LX/1bX;
    .locals 2

    .prologue
    .line 280615
    iget-object v0, p0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 280616
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 280617
    iget-object v1, p0, LX/1bf;->g:LX/1bZ;

    move-object v1, v1

    .line 280618
    iput-object v1, v0, LX/1bX;->e:LX/1bZ;

    .line 280619
    move-object v0, v0

    .line 280620
    iget-object v1, p0, LX/1bf;->a:LX/1bb;

    move-object v1, v1

    .line 280621
    iput-object v1, v0, LX/1bX;->f:LX/1bb;

    .line 280622
    move-object v0, v0

    .line 280623
    iget-boolean v1, p0, LX/1bf;->f:Z

    move v1, v1

    .line 280624
    iput-boolean v1, v0, LX/1bX;->h:Z

    .line 280625
    move-object v0, v0

    .line 280626
    iget-object v1, p0, LX/1bf;->k:LX/1bY;

    move-object v1, v1

    .line 280627
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 280628
    move-object v0, v0

    .line 280629
    iget-object v1, p0, LX/1bf;->c:LX/1ny;

    move-object v1, v1

    .line 280630
    iput-object v1, v0, LX/1bX;->m:LX/1ny;

    .line 280631
    move-object v0, v0

    .line 280632
    iget-object v1, p0, LX/1bf;->m:LX/33B;

    move-object v1, v1

    .line 280633
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 280634
    move-object v0, v0

    .line 280635
    iget-boolean v1, p0, LX/1bf;->e:Z

    move v1, v1

    .line 280636
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 280637
    move-object v0, v0

    .line 280638
    iget-object v1, p0, LX/1bf;->j:LX/1bc;

    move-object v1, v1

    .line 280639
    iput-object v1, v0, LX/1bX;->i:LX/1bc;

    .line 280640
    move-object v0, v0

    .line 280641
    iget-object v1, p0, LX/1bf;->h:LX/1o9;

    move-object v1, v1

    .line 280642
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 280643
    move-object v0, v0

    .line 280644
    iget-object v1, p0, LX/1bf;->n:LX/1BU;

    move-object v1, v1

    .line 280645
    iput-object v1, v0, LX/1bX;->l:LX/1BU;

    .line 280646
    move-object v0, v0

    .line 280647
    iget-object v1, p0, LX/1bf;->i:LX/1bd;

    move-object v1, v1

    .line 280648
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 280649
    move-object v0, v0

    .line 280650
    return-object v0
.end method

.method public static a(Landroid/net/Uri;)LX/1bX;
    .locals 1

    .prologue
    .line 280651
    new-instance v0, LX/1bX;

    invoke-direct {v0}, LX/1bX;-><init>()V

    invoke-virtual {v0, p0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 280652
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 280653
    new-instance v0, LX/4fP;

    const-string v1, "Source must be set!"

    invoke-direct {v0, v1}, LX/4fP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280654
    :cond_0
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1be;->g(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280655
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_1

    .line 280656
    new-instance v0, LX/4fP;

    const-string v1, "Resource URI path must be absolute."

    invoke-direct {v0, v1}, LX/4fP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280657
    :cond_1
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280658
    new-instance v0, LX/4fP;

    const-string v1, "Resource URI must not be empty"

    invoke-direct {v0, v1}, LX/4fP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280659
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280660
    :cond_3
    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1be;->f(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/1bX;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_4

    .line 280661
    new-instance v0, LX/4fP;

    const-string v1, "Asset URI path must be absolute."

    invoke-direct {v0, v1}, LX/4fP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280662
    :catch_0
    new-instance v0, LX/4fP;

    const-string v1, "Resource URI path must be a resource id."

    invoke-direct {v0, v1}, LX/4fP;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280663
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(LX/1ny;)LX/1bX;
    .locals 0

    .prologue
    .line 280595
    iput-object p1, p0, LX/1bX;->m:LX/1ny;

    .line 280596
    return-object p0
.end method

.method public final a(LX/1o9;)LX/1bX;
    .locals 0
    .param p1    # LX/1o9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 280577
    iput-object p1, p0, LX/1bX;->c:LX/1o9;

    .line 280578
    return-object p0
.end method

.method public final a(LX/33B;)LX/1bX;
    .locals 0

    .prologue
    .line 280593
    iput-object p1, p0, LX/1bX;->j:LX/33B;

    .line 280594
    return-object p0
.end method

.method public final a(Z)LX/1bX;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 280584
    if-eqz p1, :cond_0

    .line 280585
    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v0

    .line 280586
    iput-object v0, p0, LX/1bX;->d:LX/1bd;

    .line 280587
    move-object v0, p0

    .line 280588
    :goto_0
    return-object v0

    .line 280589
    :cond_0
    new-instance v0, LX/1bd;

    const/4 v1, -0x2

    const/4 p1, 0x0

    invoke-direct {v0, v1, p1}, LX/1bd;-><init>(IZ)V

    move-object v0, v0

    .line 280590
    iput-object v0, p0, LX/1bX;->d:LX/1bd;

    .line 280591
    move-object v0, p0

    .line 280592
    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)LX/1bX;
    .locals 0

    .prologue
    .line 280581
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280582
    iput-object p1, p0, LX/1bX;->a:Landroid/net/Uri;

    .line 280583
    return-object p0
.end method

.method public final n()LX/1bf;
    .locals 1

    .prologue
    .line 280579
    invoke-direct {p0}, LX/1bX;->o()V

    .line 280580
    new-instance v0, LX/1bf;

    invoke-direct {v0, p0}, LX/1bf;-><init>(LX/1bX;)V

    return-object v0
.end method
