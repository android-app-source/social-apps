.class public final LX/1r8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:Ljava/util/Iterator;

.field public final synthetic c:LX/1r6;


# direct methods
.method public constructor <init>(LX/1r6;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 331740
    iput-object p1, p0, LX/1r8;->c:LX/1r6;

    iput-object p2, p0, LX/1r8;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 331741
    iget-object v0, p0, LX/1r8;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 331742
    iget-object v0, p0, LX/1r8;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/1r8;->a:Ljava/util/Map$Entry;

    .line 331743
    iget-object v0, p0, LX/1r8;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 331744
    iget-object v0, p0, LX/1r8;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 331745
    iget-object v0, p0, LX/1r8;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 331746
    iget-object v1, p0, LX/1r8;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 331747
    iget-object v1, p0, LX/1r8;->c:LX/1r6;

    iget-object v1, v1, LX/1r6;->a:LX/0Xs;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-static {v1, v2}, LX/0Xs;->b(LX/0Xs;I)I

    .line 331748
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 331749
    return-void

    .line 331750
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
