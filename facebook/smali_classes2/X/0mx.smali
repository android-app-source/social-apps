.class public abstract LX/0mx;
.super LX/0my;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public transient d:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Object;",
            "LX/4rX;",
            ">;"
        }
    .end annotation
.end field

.field public transient e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4pS",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133320
    invoke-direct {p0}, LX/0my;-><init>()V

    return-void
.end method

.method public constructor <init>(LX/0my;LX/0m2;LX/0nS;)V
    .locals 0

    .prologue
    .line 133318
    invoke-direct {p0, p1, p2, p3}, LX/0my;-><init>(LX/0my;LX/0m2;LX/0nS;)V

    .line 133319
    return-void
.end method


# virtual methods
.method public abstract a(LX/0m2;LX/0nS;)LX/0mx;
.end method

.method public final a(Ljava/lang/Object;LX/4pS;)LX/4rX;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/4pS",
            "<*>;)",
            "LX/4rX;"
        }
    .end annotation

    .prologue
    .line 133301
    iget-object v0, p0, LX/0mx;->d:Ljava/util/IdentityHashMap;

    if-nez v0, :cond_3

    .line 133302
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, LX/0mx;->d:Ljava/util/IdentityHashMap;

    .line 133303
    :cond_0
    const/4 v1, 0x0

    .line 133304
    iget-object v0, p0, LX/0mx;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 133305
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0x8

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/0mx;->e:Ljava/util/ArrayList;

    move-object v0, v1

    .line 133306
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 133307
    invoke-virtual {p2}, LX/4pS;->b()LX/4pS;

    move-result-object v0

    .line 133308
    iget-object v1, p0, LX/0mx;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133309
    :cond_2
    new-instance v1, LX/4rX;

    invoke-direct {v1, v0}, LX/4rX;-><init>(LX/4pS;)V

    .line 133310
    iget-object v0, p0, LX/0mx;->d:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 133311
    :goto_1
    return-object v0

    .line 133312
    :cond_3
    iget-object v0, p0, LX/0mx;->d:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4rX;

    .line 133313
    if-eqz v0, :cond_0

    goto :goto_1

    .line 133314
    :cond_4
    const/4 v0, 0x0

    iget-object v2, p0, LX/0mx;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_2
    if-ge v2, v3, :cond_5

    .line 133315
    iget-object v0, p0, LX/0mx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4pS;

    .line 133316
    invoke-virtual {v0, p2}, LX/4pS;->a(LX/4pS;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 133317
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/0nX;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 133235
    if-nez p2, :cond_1

    .line 133236
    iget-object v1, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object v1, v1

    .line 133237
    :goto_0
    :try_start_0
    invoke-virtual {v1, p2, p1, p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 133238
    if-eqz v0, :cond_0

    .line 133239
    invoke-virtual {p1}, LX/0nX;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 133240
    :cond_0
    return-void

    .line 133241
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 133242
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v1, v3}, LX/0my;->a(Ljava/lang/Class;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v2

    .line 133243
    iget-object v3, p0, LX/0my;->_config:LX/0m2;

    .line 133244
    iget-object v4, v3, LX/0m3;->_rootName:Ljava/lang/String;

    move-object v3, v4

    .line 133245
    if-nez v3, :cond_2

    .line 133246
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    sget-object v1, LX/0mt;->WRAP_ROOT_VALUE:LX/0mt;

    invoke-virtual {v0, v1}, LX/0m2;->c(LX/0mt;)Z

    move-result v0

    .line 133247
    if-eqz v0, :cond_5

    .line 133248
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 133249
    iget-object v1, p0, LX/0my;->_rootNames:LX/0m1;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v1, v3, v4}, LX/0m1;->a(Ljava/lang/Class;LX/0m4;)LX/0lb;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/0nX;->b(LX/0lc;)V

    move-object v1, v2

    goto :goto_0

    .line 133250
    :cond_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    move-object v1, v2

    .line 133251
    goto :goto_0

    .line 133252
    :cond_3
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 133253
    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    move v0, v1

    move-object v1, v2

    goto :goto_0

    .line 133254
    :catch_0
    move-exception v0

    .line 133255
    throw v0

    .line 133256
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 133257
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 133258
    if-nez v0, :cond_4

    .line 133259
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[no message for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133260
    :cond_4
    new-instance v2, LX/28E;

    invoke-direct {v2, v0, v1}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(LX/0nX;Ljava/lang/Object;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0nX;",
            "Ljava/lang/Object;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133277
    if-nez p2, :cond_2

    .line 133278
    iget-object v0, p0, LX/0my;->_nullValueSerializer:Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object p4, v0

    .line 133279
    const/4 v0, 0x0

    .line 133280
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p4, p2, p1, p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 133281
    if-eqz v0, :cond_1

    .line 133282
    invoke-virtual {p1}, LX/0nX;->g()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 133283
    :cond_1
    return-void

    .line 133284
    :cond_2
    if-eqz p3, :cond_3

    .line 133285
    iget-object v0, p3, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 133286
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 133287
    invoke-static {p2, p3}, LX/0my;->a(Ljava/lang/Object;LX/0lJ;)V

    .line 133288
    :cond_3
    if-nez p4, :cond_4

    .line 133289
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p3, v0, v1}, LX/0my;->a(LX/0lJ;ZLX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object p4

    .line 133290
    :cond_4
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    sget-object v1, LX/0mt;->WRAP_ROOT_VALUE:LX/0mt;

    invoke-virtual {v0, v1}, LX/0m2;->c(LX/0mt;)Z

    move-result v0

    .line 133291
    if-eqz v0, :cond_0

    .line 133292
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 133293
    iget-object v1, p0, LX/0my;->_rootNames:LX/0m1;

    iget-object v2, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v1, p3, v2}, LX/0m1;->a(LX/0lJ;LX/0m4;)LX/0lb;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/0nX;->b(LX/0lc;)V

    goto :goto_0

    .line 133294
    :catch_0
    move-exception v0

    .line 133295
    throw v0

    .line 133296
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 133297
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 133298
    if-nez v0, :cond_5

    .line 133299
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[no message for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133300
    :cond_5
    new-instance v2, LX/28E;

    invoke-direct {v2, v0, v1}, LX/28E;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public final b(LX/0lO;Ljava/lang/Object;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lO;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 133261
    if-nez p2, :cond_1

    .line 133262
    :cond_0
    :goto_0
    return-object v0

    .line 133263
    :cond_1
    instance-of v1, p2, Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v1, :cond_2

    .line 133264
    check-cast p2, Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 133265
    :goto_1
    invoke-virtual {p0, p2}, LX/0my;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_0

    .line 133266
    :cond_2
    instance-of v1, p2, Ljava/lang/Class;

    if-nez v1, :cond_3

    .line 133267
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned serializer definition of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected type JsonSerializer or Class<JsonSerializer> instead"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133268
    :cond_3
    check-cast p2, Ljava/lang/Class;

    .line 133269
    const-class v1, Lcom/fasterxml/jackson/databind/JsonSerializer$None;

    if-eq p2, v1, :cond_0

    const-class v1, LX/1Xp;

    if-eq p2, v1, :cond_0

    .line 133270
    const-class v1, Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 133271
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AnnotationIntrospector returned Class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; expected Class<JsonSerializer>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133272
    :cond_4
    iget-object v1, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v1}, LX/0m4;->l()LX/4py;

    move-result-object v1

    .line 133273
    if-nez v1, :cond_5

    .line 133274
    :goto_2
    if-nez v0, :cond_6

    .line 133275
    iget-object v0, p0, LX/0my;->_config:LX/0m2;

    invoke-virtual {v0}, LX/0m4;->h()Z

    move-result v0

    invoke-static {p2, v0}, LX/1Xw;->b(Ljava/lang/Class;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-object p2, v0

    goto :goto_1

    .line 133276
    :cond_5
    invoke-virtual {v1}, LX/4py;->c()Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    goto :goto_2

    :cond_6
    move-object p2, v0

    goto :goto_1
.end method
