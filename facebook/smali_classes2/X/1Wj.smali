.class public LX/1Wj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:LX/1Wh;

.field public final d:I

.field public final e:I

.field public final f:LX/1Wk;

.field public final g:LX/1Wl;

.field public final h:LX/1Wl;


# direct methods
.method public constructor <init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V
    .locals 0

    .prologue
    .line 269381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269382
    iput p1, p0, LX/1Wj;->a:I

    .line 269383
    iput p2, p0, LX/1Wj;->b:I

    .line 269384
    iput-object p3, p0, LX/1Wj;->c:LX/1Wh;

    .line 269385
    iput p4, p0, LX/1Wj;->d:I

    .line 269386
    iput p5, p0, LX/1Wj;->e:I

    .line 269387
    iput-object p6, p0, LX/1Wj;->f:LX/1Wk;

    .line 269388
    iput-object p7, p0, LX/1Wj;->g:LX/1Wl;

    .line 269389
    iput-object p8, p0, LX/1Wj;->h:LX/1Wl;

    .line 269390
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269391
    iget v0, p0, LX/1Wj;->a:I

    if-gtz v0, :cond_0

    .line 269392
    const/4 v0, 0x0

    .line 269393
    :goto_0
    return-object v0

    .line 269394
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne p2, v0, :cond_1

    iget v0, p0, LX/1Wj;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 269395
    const/4 v0, 0x2

    new-array v1, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iget v2, p0, LX/1Wj;->a:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget v2, p0, LX/1Wj;->b:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v0

    .line 269396
    new-instance v0, LX/82e;

    invoke-direct {v0, v1}, LX/82e;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 269397
    :cond_1
    iget v0, p0, LX/1Wj;->a:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269398
    iget v0, p0, LX/1Wj;->d:I

    if-gtz v0, :cond_0

    .line 269399
    const/4 v0, 0x0

    .line 269400
    :goto_0
    return-object v0

    .line 269401
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne p2, v0, :cond_1

    iget v0, p0, LX/1Wj;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 269402
    const/4 v0, 0x2

    new-array v1, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iget v2, p0, LX/1Wj;->d:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget v2, p0, LX/1Wj;->e:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v0

    .line 269403
    new-instance v0, LX/82e;

    invoke-direct {v0, v1}, LX/82e;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 269404
    :cond_1
    iget v0, p0, LX/1Wj;->d:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
