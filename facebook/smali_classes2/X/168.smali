.class public LX/168;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jl;
.implements LX/0x7;
.implements LX/0yL;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/168;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0SG;

.field public final c:LX/16A;

.field private final d:LX/169;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0SG;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13x;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 184524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184525
    iput-object p1, p0, LX/168;->a:LX/0Ot;

    .line 184526
    iput-object p4, p0, LX/168;->b:LX/0SG;

    .line 184527
    new-instance v0, LX/169;

    invoke-direct {v0}, LX/169;-><init>()V

    iput-object v0, p0, LX/168;->d:LX/169;

    .line 184528
    new-instance v0, LX/16A;

    invoke-direct {v0, p0}, LX/16A;-><init>(LX/168;)V

    iput-object v0, p0, LX/168;->c:LX/16A;

    .line 184529
    iput-object p3, p0, LX/168;->e:LX/0Or;

    .line 184530
    iput-object p2, p0, LX/168;->f:LX/0Ot;

    .line 184531
    return-void
.end method

.method public static a(LX/0QB;)LX/168;
    .locals 7

    .prologue
    .line 184559
    sget-object v0, LX/168;->g:LX/168;

    if-nez v0, :cond_1

    .line 184560
    const-class v1, LX/168;

    monitor-enter v1

    .line 184561
    :try_start_0
    sget-object v0, LX/168;->g:LX/168;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 184562
    if-eqz v2, :cond_0

    .line 184563
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 184564
    new-instance v4, LX/168;

    const/16 v3, 0xbc

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0xc0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v4, v5, v6, p0, v3}, LX/168;-><init>(LX/0Ot;LX/0Ot;LX/0Or;LX/0SG;)V

    .line 184565
    move-object v0, v4

    .line 184566
    sput-object v0, LX/168;->g:LX/168;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184567
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 184568
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184569
    :cond_1
    sget-object v0, LX/168;->g:LX/168;

    return-object v0

    .line 184570
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 184571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/168;LX/1rF;J)V
    .locals 11

    .prologue
    .line 184542
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/168;->d:LX/169;

    const-wide/16 v8, 0x3e8

    .line 184543
    const/4 v4, 0x0

    .line 184544
    sget-object v5, LX/1rG;->a:[I

    invoke-virtual {p1}, LX/1rF;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 184545
    :cond_0
    :goto_0
    move-object v1, v4

    .line 184546
    if-eqz v1, :cond_3

    .line 184547
    iget-object v0, p0, LX/168;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, LX/1rF;->LOGIN:LX/1rF;

    if-ne p1, v0, :cond_2

    .line 184548
    :cond_1
    const-string v0, "pre_login"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 184549
    :cond_2
    iget-object v0, p0, LX/168;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13x;

    invoke-virtual {v0, v1}, LX/13x;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 184550
    iget-object v0, p0, LX/168;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184551
    :cond_3
    monitor-exit p0

    return-void

    .line 184552
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 184553
    :pswitch_0
    div-long v6, p2, v8

    .line 184554
    iget-wide v8, v0, LX/169;->c:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 184555
    invoke-static {v0, v6, v7}, LX/169;->b(LX/169;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    goto :goto_0

    .line 184556
    :pswitch_1
    iget-object v5, v0, LX/169;->e:[I

    if-eqz v5, :cond_0

    .line 184557
    div-long v4, p2, v8

    .line 184558
    invoke-static {v0, v4, v5, p1}, LX/169;->c(LX/169;JLX/1rF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 184540
    sget-object v0, LX/1rF;->USER_ACTION:LX/1rF;

    invoke-static {p0, v0, p1, p2}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 184541
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 184538
    sget-object v0, LX/1rF;->DATASAVINGS:LX/1rF;

    iget-object v1, p0, LX/168;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, v0, v2, v3}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 184539
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 184536
    sget-object v0, LX/1rF;->USER_ACTION:LX/1rF;

    invoke-static {p0, v0, p1, p2}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 184537
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 184535
    return-void
.end method

.method public final e_(Z)V
    .locals 0

    .prologue
    .line 184534
    return-void
.end method

.method public final ig_()V
    .locals 4

    .prologue
    .line 184532
    sget-object v0, LX/1rF;->DIALTONE:LX/1rF;

    iget-object v1, p0, LX/168;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0, v0, v2, v3}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 184533
    return-void
.end method
