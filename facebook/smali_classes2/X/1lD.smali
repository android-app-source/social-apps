.class public final enum LX/1lD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1lD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1lD;

.field public static final enum ERROR:LX/1lD;

.field public static final enum LOADING:LX/1lD;

.field public static final enum LOAD_FINISHED:LX/1lD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 311386
    new-instance v0, LX/1lD;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/1lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lD;->LOADING:LX/1lD;

    .line 311387
    new-instance v0, LX/1lD;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, LX/1lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lD;->ERROR:LX/1lD;

    .line 311388
    new-instance v0, LX/1lD;

    const-string v1, "LOAD_FINISHED"

    invoke-direct {v0, v1, v4}, LX/1lD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    .line 311389
    const/4 v0, 0x3

    new-array v0, v0, [LX/1lD;

    sget-object v1, LX/1lD;->LOADING:LX/1lD;

    aput-object v1, v0, v2

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    aput-object v1, v0, v3

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    aput-object v1, v0, v4

    sput-object v0, LX/1lD;->$VALUES:[LX/1lD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 311390
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1lD;
    .locals 1

    .prologue
    .line 311391
    const-class v0, LX/1lD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1lD;

    return-object v0
.end method

.method public static values()[LX/1lD;
    .locals 1

    .prologue
    .line 311392
    sget-object v0, LX/1lD;->$VALUES:[LX/1lD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1lD;

    return-object v0
.end method
