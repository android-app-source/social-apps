.class public final LX/1WP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 268297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 268392
    if-nez p1, :cond_0

    .line 268393
    :goto_0
    return v0

    .line 268394
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 268395
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 268396
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 268397
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 268398
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 268399
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 268400
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)I
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 268355
    if-nez p1, :cond_0

    .line 268356
    :goto_0
    return v2

    .line 268357
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    .line 268358
    if-eqz v3, :cond_2

    .line 268359
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 268360
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 268361
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 268362
    if-nez v0, :cond_3

    .line 268363
    :goto_2
    move v0, v8

    .line 268364
    aput v0, v4, v1

    .line 268365
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 268366
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 268367
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 268368
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 268369
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 268370
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 268371
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v9

    .line 268372
    if-eqz v9, :cond_5

    .line 268373
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v6

    new-array v10, v6, [I

    move v7, v8

    .line 268374
    :goto_4
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_4

    .line 268375
    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v12, 0x0

    .line 268376
    if-nez v6, :cond_6

    .line 268377
    :goto_5
    move v6, v12

    .line 268378
    aput v6, v10, v7

    .line 268379
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_4

    .line 268380
    :cond_4
    invoke-virtual {p0, v10, v11}, LX/186;->a([IZ)I

    move-result v6

    .line 268381
    :goto_6
    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 268382
    invoke-virtual {p0, v8, v6}, LX/186;->b(II)V

    .line 268383
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 268384
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_2

    :cond_5
    move v6, v8

    goto :goto_6

    .line 268385
    :cond_6
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v13

    invoke-virtual {p0, v13}, LX/186;->c(Ljava/util/List;)I

    move-result v13

    .line 268386
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    invoke-static {p0, p1}, LX/1WP;->a(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result p1

    .line 268387
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 268388
    invoke-virtual {p0, v12, v13}, LX/186;->b(II)V

    .line 268389
    const/4 v12, 0x1

    invoke-virtual {p0, v12, p1}, LX/186;->b(II)V

    .line 268390
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 268391
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 268317
    if-nez p0, :cond_1

    .line 268318
    :cond_0
    :goto_0
    return-object v2

    .line 268319
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 268320
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 268321
    if-nez p0, :cond_3

    .line 268322
    :goto_1
    move v1, v4

    .line 268323
    if-eqz v1, :cond_0

    .line 268324
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 268325
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 268326
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 268327
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 268328
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 268329
    const-string v1, "StoryUtilModelConverter.getShouldDisplaySubStoryGalleryGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 268330
    :cond_2
    new-instance v2, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$ShouldDisplaySubStoryGalleryGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 268331
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    .line 268332
    if-eqz v5, :cond_5

    .line 268333
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 268334
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 268335
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v8, 0x0

    .line 268336
    if-nez v1, :cond_6

    .line 268337
    :goto_3
    move v1, v8

    .line 268338
    aput v1, v6, v3

    .line 268339
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 268340
    :cond_4
    invoke-virtual {v0, v6, v7}, LX/186;->a([IZ)I

    move-result v1

    .line 268341
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v3

    invoke-static {v0, v3}, LX/1WP;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)I

    move-result v3

    .line 268342
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 268343
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 268344
    invoke-virtual {v0, v7, v3}, LX/186;->b(II)V

    .line 268345
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ad()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 268346
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 268347
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto :goto_1

    :cond_5
    move v1, v4

    goto :goto_4

    .line 268348
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 268349
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 268350
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 268351
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 268352
    const/4 v8, 0x1

    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 268353
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 268354
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto :goto_3
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/6XF;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 268298
    if-nez p0, :cond_1

    .line 268299
    :cond_0
    :goto_0
    return-object v2

    .line 268300
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 268301
    const/4 v1, 0x0

    .line 268302
    if-nez p0, :cond_3

    .line 268303
    :goto_1
    move v1, v1

    .line 268304
    if-eqz v1, :cond_0

    .line 268305
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 268306
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 268307
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 268308
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 268309
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 268310
    const-string v1, "StoryUtilModelConverter.getHasFeedbackTargetOfTypeGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 268311
    :cond_2
    new-instance v2, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/graphql/story/util/GraphQLStoryUtilGraphQLModels$HasFeedbackTargetOfTypeGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 268312
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-static {v0, v3}, LX/1WP;->a(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v3

    .line 268313
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 268314
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 268315
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 268316
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method
