.class public final LX/1RL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1RJ;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1RJ;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 246029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246030
    iput-object p1, p0, LX/1RL;->a:LX/0QB;

    .line 246031
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 246033
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/1RL;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 246034
    packed-switch p2, :pswitch_data_0

    .line 246035
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246036
    :pswitch_0
    new-instance v1, LX/1RM;

    const/16 v0, 0x6a4

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, p0, v0}, LX/1RM;-><init>(LX/0Ot;Landroid/content/Context;)V

    .line 246037
    move-object v0, v1

    .line 246038
    :goto_0
    return-object v0

    .line 246039
    :pswitch_1
    new-instance v0, LX/1RO;

    invoke-direct {v0}, LX/1RO;-><init>()V

    .line 246040
    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const/16 v2, 0x22cc

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x22d1

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class v4, Landroid/content/Context;

    invoke-interface {p1, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x66b

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x22c1

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2ff5

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2588

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    .line 246041
    iput-object v1, v0, LX/1RO;->a:Landroid/content/res/Resources;

    iput-object v2, v0, LX/1RO;->b:LX/0Ot;

    iput-object v3, v0, LX/1RO;->c:LX/0Ot;

    iput-object v4, v0, LX/1RO;->d:Landroid/content/Context;

    iput-object v5, v0, LX/1RO;->e:LX/0Ot;

    iput-object v6, v0, LX/1RO;->f:LX/0Ot;

    iput-object p0, v0, LX/1RO;->g:LX/0Ot;

    iput-object p2, v0, LX/1RO;->h:LX/0Ot;

    .line 246042
    move-object v0, v0

    .line 246043
    goto :goto_0

    .line 246044
    :pswitch_2
    new-instance v0, LX/1RQ;

    .line 246045
    new-instance v1, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/1RR;

    invoke-direct {v3, p1}, LX/1RR;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 246046
    const/16 v2, 0x2fed

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1RQ;-><init>(Ljava/util/Set;LX/0Ot;)V

    .line 246047
    move-object v0, v0

    .line 246048
    goto :goto_0

    .line 246049
    :pswitch_3
    new-instance v1, LX/1RS;

    invoke-direct {v1}, LX/1RS;-><init>()V

    .line 246050
    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 246051
    iput-object v0, v1, LX/1RS;->a:Landroid/content/res/Resources;

    .line 246052
    move-object v0, v1

    .line 246053
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 246032
    const/4 v0, 0x4

    return v0
.end method
