.class public final enum LX/0zI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0zI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0zI;

.field public static final enum DEFAULT:LX/0zI;

.field public static final enum IMMERSIVE:LX/0zI;

.field public static final enum SCOPED:LX/0zI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166790
    new-instance v0, LX/0zI;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/0zI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0zI;->DEFAULT:LX/0zI;

    .line 166791
    new-instance v0, LX/0zI;

    const-string v1, "IMMERSIVE"

    invoke-direct {v0, v1, v3}, LX/0zI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0zI;->IMMERSIVE:LX/0zI;

    .line 166792
    new-instance v0, LX/0zI;

    const-string v1, "SCOPED"

    invoke-direct {v0, v1, v4}, LX/0zI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0zI;->SCOPED:LX/0zI;

    .line 166793
    const/4 v0, 0x3

    new-array v0, v0, [LX/0zI;

    sget-object v1, LX/0zI;->DEFAULT:LX/0zI;

    aput-object v1, v0, v2

    sget-object v1, LX/0zI;->IMMERSIVE:LX/0zI;

    aput-object v1, v0, v3

    sget-object v1, LX/0zI;->SCOPED:LX/0zI;

    aput-object v1, v0, v4

    sput-object v0, LX/0zI;->$VALUES:[LX/0zI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 166794
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0zI;
    .locals 1

    .prologue
    .line 166795
    const-class v0, LX/0zI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0zI;

    return-object v0
.end method

.method public static values()[LX/0zI;
    .locals 1

    .prologue
    .line 166796
    sget-object v0, LX/0zI;->$VALUES:[LX/0zI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0zI;

    return-object v0
.end method
