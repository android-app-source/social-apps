.class public LX/1cP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/1Fj;

.field public final c:Z

.field private final d:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;ZLX/1cF;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/imagepipeline/memory/PooledByteBufferFactory;",
            "Z",
            "LX/1cF",
            "<",
            "LX/1FL;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 281973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281974
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/1cP;->a:Ljava/util/concurrent/Executor;

    .line 281975
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fj;

    iput-object v0, p0, LX/1cP;->b:LX/1Fj;

    .line 281976
    iput-boolean p3, p0, LX/1cP;->c:Z

    .line 281977
    invoke-static {p4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/1cP;->d:LX/1cF;

    .line 281978
    iput-boolean p5, p0, LX/1cP;->e:Z

    .line 281979
    return-void
.end method

.method public static b(LX/1bd;LX/1FL;)I
    .locals 2

    .prologue
    .line 281980
    iget v0, p0, LX/1bd;->a:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 281981
    if-nez v0, :cond_1

    .line 281982
    const/4 v0, 0x0

    .line 281983
    :cond_0
    :goto_1
    return v0

    .line 281984
    :cond_1
    iget v0, p1, LX/1FL;->d:I

    move v0, v0

    .line 281985
    sparse-switch v0, :sswitch_data_0

    .line 281986
    const/4 v0, 0x0

    :goto_2
    move v0, v0

    .line 281987
    invoke-virtual {p0}, LX/1bd;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281988
    invoke-virtual {p0}, LX/1bd;->f()I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 281989
    :sswitch_0
    iget v0, p1, LX/1FL;->d:I

    move v0, v0

    .line 281990
    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method public static d(LX/1bf;LX/1FL;Z)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/16 v1, 0x8

    .line 281991
    if-nez p2, :cond_1

    move v0, v1

    .line 281992
    :cond_0
    :goto_0
    return v0

    .line 281993
    :cond_1
    iget-object v0, p0, LX/1bf;->h:LX/1o9;

    move-object v5, v0

    .line 281994
    if-nez v5, :cond_2

    move v0, v1

    .line 281995
    goto :goto_0

    .line 281996
    :cond_2
    iget-object v0, p0, LX/1bf;->i:LX/1bd;

    move-object v0, v0

    .line 281997
    invoke-static {v0, p1}, LX/1cP;->b(LX/1bd;LX/1FL;)I

    move-result v0

    .line 281998
    const/16 v3, 0x5a

    if-eq v0, v3, :cond_3

    const/16 v3, 0x10e

    if-ne v0, v3, :cond_5

    :cond_3
    move v4, v2

    .line 281999
    :goto_1
    if-eqz v4, :cond_6

    .line 282000
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 282001
    move v3, v0

    .line 282002
    :goto_2
    if-eqz v4, :cond_7

    .line 282003
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 282004
    :goto_3
    if-nez v5, :cond_9

    .line 282005
    const/high16 v4, 0x3f800000    # 1.0f

    .line 282006
    :cond_4
    :goto_4
    move v0, v4

    .line 282007
    iget v3, v5, LX/1o9;->d:F

    .line 282008
    const/high16 v4, 0x41000000    # 8.0f

    mul-float/2addr v4, v0

    add-float/2addr v4, v3

    float-to-int v4, v4

    move v0, v4

    .line 282009
    if-le v0, v1, :cond_8

    move v0, v1

    .line 282010
    goto :goto_0

    .line 282011
    :cond_5
    const/4 v0, 0x0

    move v4, v0

    goto :goto_1

    .line 282012
    :cond_6
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 282013
    move v3, v0

    goto :goto_2

    .line 282014
    :cond_7
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 282015
    goto :goto_3

    .line 282016
    :cond_8
    if-gtz v0, :cond_0

    move v0, v2

    goto :goto_0

    .line 282017
    :cond_9
    iget v4, v5, LX/1o9;->a:I

    int-to-float v4, v4

    int-to-float p0, v3

    div-float/2addr v4, p0

    .line 282018
    iget p0, v5, LX/1o9;->b:I

    int-to-float p0, p0

    int-to-float p1, v0

    div-float/2addr p0, p1

    .line 282019
    invoke-static {v4, p0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 282020
    int-to-float p0, v3

    mul-float/2addr p0, v4

    iget p1, v5, LX/1o9;->c:F

    cmpl-float p0, p0, p1

    if-lez p0, :cond_a

    .line 282021
    iget v4, v5, LX/1o9;->c:F

    int-to-float p0, v3

    div-float/2addr v4, p0

    .line 282022
    :cond_a
    int-to-float p0, v0

    mul-float/2addr p0, v4

    iget p1, v5, LX/1o9;->c:F

    cmpl-float p0, p0, p1

    if-lez p0, :cond_4

    .line 282023
    iget v4, v5, LX/1o9;->c:F

    int-to-float p0, v0

    div-float/2addr v4, p0

    goto :goto_4
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 282024
    iget-object v0, p0, LX/1cP;->d:LX/1cF;

    new-instance v1, LX/1eY;

    invoke-direct {v1, p0, p1, p2}, LX/1eY;-><init>(LX/1cP;LX/1cd;LX/1cW;)V

    invoke-interface {v0, v1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 282025
    return-void
.end method
