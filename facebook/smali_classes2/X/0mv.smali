.class public final enum LX/0mv;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0m7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0mv;",
        ">;",
        "LX/0m7;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0mv;

.field public static final enum ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

.field public static final enum ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

.field public static final enum ADJUST_DATES_TO_CONTEXT_TIME_ZONE:LX/0mv;

.field public static final enum EAGER_DESERIALIZER_FETCH:LX/0mv;

.field public static final enum FAIL_ON_INVALID_SUBTYPE:LX/0mv;

.field public static final enum FAIL_ON_NULL_FOR_PRIMITIVES:LX/0mv;

.field public static final enum FAIL_ON_NUMBERS_FOR_ENUMS:LX/0mv;

.field public static final enum FAIL_ON_UNKNOWN_PROPERTIES:LX/0mv;

.field public static final enum READ_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mv;

.field public static final enum READ_ENUMS_USING_TO_STRING:LX/0mv;

.field public static final enum READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

.field public static final enum UNWRAP_ROOT_VALUE:LX/0mv;

.field public static final enum USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

.field public static final enum USE_BIG_INTEGER_FOR_INTS:LX/0mv;

.field public static final enum USE_JAVA_ARRAY_FOR_JSON_ARRAY:LX/0mv;

.field public static final enum WRAP_EXCEPTIONS:LX/0mv;


# instance fields
.field private final _defaultState:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133213
    new-instance v0, LX/0mv;

    const-string v1, "USE_BIG_DECIMAL_FOR_FLOATS"

    invoke-direct {v0, v1, v3, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    .line 133214
    new-instance v0, LX/0mv;

    const-string v1, "USE_BIG_INTEGER_FOR_INTS"

    invoke-direct {v0, v1, v4, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    .line 133215
    new-instance v0, LX/0mv;

    const-string v1, "USE_JAVA_ARRAY_FOR_JSON_ARRAY"

    invoke-direct {v0, v1, v5, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->USE_JAVA_ARRAY_FOR_JSON_ARRAY:LX/0mv;

    .line 133216
    new-instance v0, LX/0mv;

    const-string v1, "READ_ENUMS_USING_TO_STRING"

    invoke-direct {v0, v1, v6, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->READ_ENUMS_USING_TO_STRING:LX/0mv;

    .line 133217
    new-instance v0, LX/0mv;

    const-string v1, "FAIL_ON_UNKNOWN_PROPERTIES"

    invoke-direct {v0, v1, v7, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->FAIL_ON_UNKNOWN_PROPERTIES:LX/0mv;

    .line 133218
    new-instance v0, LX/0mv;

    const-string v1, "FAIL_ON_NULL_FOR_PRIMITIVES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->FAIL_ON_NULL_FOR_PRIMITIVES:LX/0mv;

    .line 133219
    new-instance v0, LX/0mv;

    const-string v1, "FAIL_ON_NUMBERS_FOR_ENUMS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->FAIL_ON_NUMBERS_FOR_ENUMS:LX/0mv;

    .line 133220
    new-instance v0, LX/0mv;

    const-string v1, "FAIL_ON_INVALID_SUBTYPE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->FAIL_ON_INVALID_SUBTYPE:LX/0mv;

    .line 133221
    new-instance v0, LX/0mv;

    const-string v1, "WRAP_EXCEPTIONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->WRAP_EXCEPTIONS:LX/0mv;

    .line 133222
    new-instance v0, LX/0mv;

    const-string v1, "ACCEPT_SINGLE_VALUE_AS_ARRAY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    .line 133223
    new-instance v0, LX/0mv;

    const-string v1, "UNWRAP_ROOT_VALUE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->UNWRAP_ROOT_VALUE:LX/0mv;

    .line 133224
    new-instance v0, LX/0mv;

    const-string v1, "ACCEPT_EMPTY_STRING_AS_NULL_OBJECT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    .line 133225
    new-instance v0, LX/0mv;

    const-string v1, "READ_UNKNOWN_ENUM_VALUES_AS_NULL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    .line 133226
    new-instance v0, LX/0mv;

    const-string v1, "READ_DATE_TIMESTAMPS_AS_NANOSECONDS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->READ_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mv;

    .line 133227
    new-instance v0, LX/0mv;

    const-string v1, "ADJUST_DATES_TO_CONTEXT_TIME_ZONE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->ADJUST_DATES_TO_CONTEXT_TIME_ZONE:LX/0mv;

    .line 133228
    new-instance v0, LX/0mv;

    const-string v1, "EAGER_DESERIALIZER_FETCH"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/0mv;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/0mv;->EAGER_DESERIALIZER_FETCH:LX/0mv;

    .line 133229
    const/16 v0, 0x10

    new-array v0, v0, [LX/0mv;

    sget-object v1, LX/0mv;->USE_BIG_DECIMAL_FOR_FLOATS:LX/0mv;

    aput-object v1, v0, v3

    sget-object v1, LX/0mv;->USE_BIG_INTEGER_FOR_INTS:LX/0mv;

    aput-object v1, v0, v4

    sget-object v1, LX/0mv;->USE_JAVA_ARRAY_FOR_JSON_ARRAY:LX/0mv;

    aput-object v1, v0, v5

    sget-object v1, LX/0mv;->READ_ENUMS_USING_TO_STRING:LX/0mv;

    aput-object v1, v0, v6

    sget-object v1, LX/0mv;->FAIL_ON_UNKNOWN_PROPERTIES:LX/0mv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0mv;->FAIL_ON_NULL_FOR_PRIMITIVES:LX/0mv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/0mv;->FAIL_ON_NUMBERS_FOR_ENUMS:LX/0mv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/0mv;->FAIL_ON_INVALID_SUBTYPE:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/0mv;->WRAP_EXCEPTIONS:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/0mv;->ACCEPT_SINGLE_VALUE_AS_ARRAY:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/0mv;->UNWRAP_ROOT_VALUE:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/0mv;->ACCEPT_EMPTY_STRING_AS_NULL_OBJECT:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/0mv;->READ_DATE_TIMESTAMPS_AS_NANOSECONDS:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/0mv;->ADJUST_DATES_TO_CONTEXT_TIME_ZONE:LX/0mv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/0mv;->EAGER_DESERIALIZER_FETCH:LX/0mv;

    aput-object v2, v0, v1

    sput-object v0, LX/0mv;->$VALUES:[LX/0mv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 133210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133211
    iput-boolean p3, p0, LX/0mv;->_defaultState:Z

    .line 133212
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0mv;
    .locals 1

    .prologue
    .line 133209
    const-class v0, LX/0mv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0mv;

    return-object v0
.end method

.method public static values()[LX/0mv;
    .locals 1

    .prologue
    .line 133206
    sget-object v0, LX/0mv;->$VALUES:[LX/0mv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0mv;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 133208
    iget-boolean v0, p0, LX/0mv;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 2

    .prologue
    .line 133207
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/0mv;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    return v0
.end method
