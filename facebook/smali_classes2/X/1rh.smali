.class public LX/1rh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1rh;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1rj;

.field public final c:LX/1wV;


# direct methods
.method public constructor <init>(LX/0Or;LX/1ri;LX/1wV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/1ri;",
            "LX/1wV;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 332879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332880
    iput-object p1, p0, LX/1rh;->a:LX/0Or;

    .line 332881
    iget-object v0, p2, LX/1ri;->a:LX/1rj;

    move-object v0, v0

    .line 332882
    iput-object v0, p0, LX/1rh;->b:LX/1rj;

    .line 332883
    iput-object p3, p0, LX/1rh;->c:LX/1wV;

    .line 332884
    return-void
.end method

.method public static a(LX/0QB;)LX/1rh;
    .locals 6

    .prologue
    .line 332885
    sget-object v0, LX/1rh;->d:LX/1rh;

    if-nez v0, :cond_1

    .line 332886
    const-class v1, LX/1rh;

    monitor-enter v1

    .line 332887
    :try_start_0
    sget-object v0, LX/1rh;->d:LX/1rh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 332888
    if-eqz v2, :cond_0

    .line 332889
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 332890
    new-instance v5, LX/1rh;

    const/16 v3, 0x19e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1ri;->b(LX/0QB;)LX/1ri;

    move-result-object v3

    check-cast v3, LX/1ri;

    invoke-static {v0}, LX/1wV;->b(LX/0QB;)LX/1wV;

    move-result-object v4

    check-cast v4, LX/1wV;

    invoke-direct {v5, p0, v3, v4}, LX/1rh;-><init>(LX/0Or;LX/1ri;LX/1wV;)V

    .line 332891
    move-object v0, v5

    .line 332892
    sput-object v0, LX/1rh;->d:LX/1rh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332893
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 332894
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 332895
    :cond_1
    sget-object v0, LX/1rh;->d:LX/1rh;

    return-object v0

    .line 332896
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 332897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
