.class public LX/0yE;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/common/time/RealtimeSinceBootClock;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Lcom/facebook/common/time/RealtimeSinceBootClock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 164211
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;
    .locals 3

    .prologue
    .line 164212
    sget-object v0, LX/0yE;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    if-nez v0, :cond_1

    .line 164213
    const-class v1, LX/0yE;

    monitor-enter v1

    .line 164214
    :try_start_0
    sget-object v0, LX/0yE;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 164215
    if-eqz v2, :cond_0

    .line 164216
    :try_start_1
    invoke-static {}, LX/0Sn;->c()Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v0

    sput-object v0, LX/0yE;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164217
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 164218
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164219
    :cond_1
    sget-object v0, LX/0yE;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    return-object v0

    .line 164220
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 164221
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164222
    invoke-static {}, LX/0Sn;->c()Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v0

    return-object v0
.end method
