.class public abstract LX/0Q0;
.super LX/0P7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P7",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 57666
    invoke-direct {p0}, LX/0P7;-><init>()V

    .line 57667
    const-string v0, "initialCapacity"

    invoke-static {p1, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 57668
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    .line 57669
    const/4 v0, 0x0

    iput v0, p0, LX/0Q0;->b:I

    .line 57670
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 57671
    iget-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 57672
    iget-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    iget-object v1, p0, LX/0Q0;->a:[Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1, p1}, LX/0P7;->a(II)I

    move-result v1

    invoke-static {v0, v1}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    .line 57673
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Iterable;)LX/0P7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57674
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 57675
    check-cast v0, Ljava/util/Collection;

    .line 57676
    iget v1, p0, LX/0Q0;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/0Q0;->a(I)V

    .line 57677
    :cond_0
    invoke-super {p0, p1}, LX/0P7;->a(Ljava/lang/Iterable;)LX/0P7;

    .line 57678
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)LX/0P7;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LX/0P7",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57679
    invoke-static {p1}, LX/0P8;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 57680
    iget v0, p0, LX/0Q0;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/0Q0;->a(I)V

    .line 57681
    const/4 v0, 0x0

    iget-object v1, p0, LX/0Q0;->a:[Ljava/lang/Object;

    iget v2, p0, LX/0Q0;->b:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57682
    iget v0, p0, LX/0Q0;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, LX/0Q0;->b:I

    .line 57683
    return-object p0
.end method

.method public a(Ljava/lang/Object;)LX/0Q0;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/0Q0",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 57684
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57685
    iget v0, p0, LX/0Q0;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/0Q0;->a(I)V

    .line 57686
    iget-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    iget v1, p0, LX/0Q0;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/0Q0;->b:I

    aput-object p1, v0, v1

    .line 57687
    return-object p0
.end method

.method public synthetic b(Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 57688
    invoke-virtual {p0, p1}, LX/0Q0;->a(Ljava/lang/Object;)LX/0Q0;

    move-result-object v0

    return-object v0
.end method
