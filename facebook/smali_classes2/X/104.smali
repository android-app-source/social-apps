.class public final LX/104;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Landroid/view/View$OnClickListener;

.field public final synthetic b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

.field private c:Landroid/graphics/Rect;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 168339
    iput-object p1, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iput-object p2, p0, LX/104;->a:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 168340
    iget-object v0, p0, LX/104;->c:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/104;->c:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    const v6, 0x3e99999a    # 0.3f

    const/4 v5, 0x1

    .line 168341
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 168342
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/104;->c:Landroid/graphics/Rect;

    .line 168343
    iget-object v0, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, v6}, Lcom/facebook/ui/search/SearchEditText;->setAlpha(F)V

    .line 168344
    iput-boolean v5, p0, LX/104;->d:Z

    .line 168345
    :cond_0
    :goto_0
    return v5

    .line 168346
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_2

    invoke-direct {p0, p1, p2}, LX/104;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168347
    iget-object v0, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-static {v0, v2}, LX/0wS;->a(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V

    .line 168348
    iget-object v0, p0, LX/104;->a:Landroid/view/View$OnClickListener;

    iget-object v1, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 168349
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    goto :goto_0

    .line 168350
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, LX/104;->d:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2}, LX/104;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 168351
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/104;->d:Z

    .line 168352
    iget-object v0, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-static {v0, v2}, LX/0wS;->a(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 168353
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/104;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, LX/104;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168354
    iput-boolean v5, p0, LX/104;->d:Z

    .line 168355
    iget-object v0, p0, LX/104;->b:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    iget-object v0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, v6}, Lcom/facebook/ui/search/SearchEditText;->setAlpha(F)V

    goto :goto_0
.end method
