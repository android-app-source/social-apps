.class public final LX/1Ol;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Om;


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 242045
    iput-object p1, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/v7/widget/RecyclerView;B)V
    .locals 0

    .prologue
    .line 242046
    invoke-direct {p0, p1}, LX/1Ol;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1a1;)V
    .locals 3

    .prologue
    .line 242047
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 242048
    iget-object v0, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/1a1;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242049
    iget-object v0, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 242050
    :cond_0
    return-void
.end method

.method public final b(LX/1a1;)V
    .locals 2

    .prologue
    .line 242051
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 242052
    invoke-static {p1}, LX/1a1;->A(LX/1a1;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242053
    iget-object v0, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 242054
    :cond_0
    return-void
.end method

.method public final c(LX/1a1;)V
    .locals 2

    .prologue
    .line 242055
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 242056
    invoke-static {p1}, LX/1a1;->A(LX/1a1;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242057
    iget-object v0, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 242058
    :cond_0
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 242059
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 242060
    iget-object v0, p1, LX/1a1;->g:LX/1a1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1a1;->h:LX/1a1;

    if-nez v0, :cond_0

    .line 242061
    iput-object v2, p1, LX/1a1;->g:LX/1a1;

    .line 242062
    const/16 v0, -0x41

    iget v1, p1, LX/1a1;->l:I

    invoke-virtual {p1, v0, v1}, LX/1a1;->a(II)V

    .line 242063
    :cond_0
    iput-object v2, p1, LX/1a1;->h:LX/1a1;

    .line 242064
    invoke-static {p1}, LX/1a1;->A(LX/1a1;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 242065
    iget-object v0, p0, LX/1Ol;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Z

    .line 242066
    :cond_1
    return-void
.end method
