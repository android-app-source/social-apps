.class public final enum LX/0VZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/0VZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/0VZ;

.field public static final enum APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

.field public static final enum APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

.field public static final enum APPLICATION_LOADED_UI_IDLE:LX/0VZ;

.field public static final enum APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

.field public static final enum APPLICATION_LOADING:LX/0VZ;

.field public static final enum STARTUP_INITIALIZATION:LX/0VZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67911
    new-instance v0, LX/0VZ;

    const-string v1, "STARTUP_INITIALIZATION"

    invoke-direct {v0, v1, v3}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->STARTUP_INITIALIZATION:LX/0VZ;

    .line 67912
    new-instance v0, LX/0VZ;

    const-string v1, "APPLICATION_LOADING"

    invoke-direct {v0, v1, v4}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->APPLICATION_LOADING:LX/0VZ;

    .line 67913
    new-instance v0, LX/0VZ;

    const-string v1, "APPLICATION_LOADED_HIGH_PRIORITY"

    invoke-direct {v0, v1, v5}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    .line 67914
    new-instance v0, LX/0VZ;

    const-string v1, "APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY"

    invoke-direct {v0, v1, v6}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    .line 67915
    new-instance v0, LX/0VZ;

    const-string v1, "APPLICATION_LOADED_UI_IDLE"

    invoke-direct {v0, v1, v7}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    .line 67916
    new-instance v0, LX/0VZ;

    const-string v1, "APPLICATION_LOADED_LOW_PRIORITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/0VZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    .line 67917
    const/4 v0, 0x6

    new-array v0, v0, [LX/0VZ;

    sget-object v1, LX/0VZ;->STARTUP_INITIALIZATION:LX/0VZ;

    aput-object v1, v0, v3

    sget-object v1, LX/0VZ;->APPLICATION_LOADING:LX/0VZ;

    aput-object v1, v0, v4

    sget-object v1, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    aput-object v1, v0, v5

    sget-object v1, LX/0VZ;->APPLICATION_LOADED_UI_IDLE_HIGH_PRIORITY:LX/0VZ;

    aput-object v1, v0, v6

    sget-object v1, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/0VZ;->APPLICATION_LOADED_LOW_PRIORITY:LX/0VZ;

    aput-object v2, v0, v1

    sput-object v0, LX/0VZ;->$VALUES:[LX/0VZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 67918
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/0VZ;
    .locals 1

    .prologue
    .line 67919
    const-class v0, LX/0VZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/0VZ;

    return-object v0
.end method

.method public static values()[LX/0VZ;
    .locals 1

    .prologue
    .line 67920
    sget-object v0, LX/0VZ;->$VALUES:[LX/0VZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0VZ;

    return-object v0
.end method
