.class public LX/1q1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1q2;


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/1q2;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1E1;


# direct methods
.method public constructor <init>(LX/0Uh;LX/1q3;LX/1q4;LX/1q5;LX/1E1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 329942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329943
    iput-object p1, p0, LX/1q1;->a:LX/0Uh;

    .line 329944
    invoke-static {p2, p3, p4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1q1;->b:LX/0Rf;

    .line 329945
    iput-object p5, p0, LX/1q1;->c:LX/1E1;

    .line 329946
    return-void
.end method


# virtual methods
.method public final a(JZ)V
    .locals 3

    .prologue
    .line 329947
    iget-object v0, p0, LX/1q1;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q2;

    .line 329948
    invoke-interface {v0, p1, p2, p3}, LX/1q2;->a(JZ)V

    goto :goto_0

    .line 329949
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 329950
    const/4 v0, 0x1

    return v0
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 329951
    iget-object v0, p0, LX/1q1;->c:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 329952
    :goto_0
    return v0

    .line 329953
    :cond_0
    iget-object v0, p0, LX/1q1;->a:LX/0Uh;

    sget v3, LX/1S0;->a:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 329954
    goto :goto_0

    .line 329955
    :cond_1
    iget-object v0, p0, LX/1q1;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q2;

    .line 329956
    invoke-interface {v0}, LX/1q2;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0, p1, p2}, LX/1q2;->a(J)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 329957
    goto :goto_0

    :cond_3
    move v0, v1

    .line 329958
    goto :goto_0
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 329959
    iget-object v0, p0, LX/1q1;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1q2;

    .line 329960
    invoke-interface {v0, p1, p2}, LX/1q2;->b(J)V

    goto :goto_0

    .line 329961
    :cond_0
    return-void
.end method
