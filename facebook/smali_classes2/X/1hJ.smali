.class public final LX/1hJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public cacheCapacity:I

.field public enableCrossDomainTickets:Z

.field public filename:Ljava/lang/String;

.field public syncInterval:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 295749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295750
    const/16 v0, 0x32

    iput v0, p0, LX/1hJ;->cacheCapacity:I

    .line 295751
    const/16 v0, 0x96

    iput v0, p0, LX/1hJ;->syncInterval:I

    .line 295752
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1hJ;->enableCrossDomainTickets:Z

    .line 295753
    iput-object p1, p0, LX/1hJ;->filename:Ljava/lang/String;

    .line 295754
    return-void
.end method


# virtual methods
.method public build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;
    .locals 5

    .prologue
    .line 295755
    new-instance v0, Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    iget-object v1, p0, LX/1hJ;->filename:Ljava/lang/String;

    iget v2, p0, LX/1hJ;->cacheCapacity:I

    iget v3, p0, LX/1hJ;->syncInterval:I

    iget-boolean v4, p0, LX/1hJ;->enableCrossDomainTickets:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/proxygen/PersistentSSLCacheSettings;-><init>(Ljava/lang/String;IIZ)V

    return-object v0
.end method
