.class public LX/1et;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private final b:F

.field private final c:LX/1V8;

.field private final d:LX/1ev;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1eu;LX/1V8;LX/1ev;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 289233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289234
    const v0, 0x7f0b0034

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/1et;->b:F

    .line 289235
    const v0, 0x7f020aa7

    .line 289236
    iget-object p1, p2, LX/1eu;->a:Landroid/content/res/Resources;

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v0, 0x0

    .line 289237
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2, v0, v0, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 289238
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289239
    :goto_0
    move-object p1, p2

    .line 289240
    move-object v0, p1

    .line 289241
    iput-object v0, p0, LX/1et;->a:Landroid/graphics/Rect;

    .line 289242
    iput-object p3, p0, LX/1et;->c:LX/1V8;

    .line 289243
    iput-object p4, p0, LX/1et;->d:LX/1ev;

    .line 289244
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1Ua;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289263
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 289264
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MEME_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289265
    sget-object v0, LX/1Ua;->o:LX/1Ua;

    .line 289266
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Ua;",
            ")",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 289267
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 289268
    new-instance v1, LX/1X6;

    invoke-direct {v1, v0, p1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    return-object v1
.end method

.method public static a(LX/0QB;)LX/1et;
    .locals 7

    .prologue
    .line 289252
    const-class v1, LX/1et;

    monitor-enter v1

    .line 289253
    :try_start_0
    sget-object v0, LX/1et;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 289254
    sput-object v2, LX/1et;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 289255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 289257
    new-instance p0, LX/1et;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1eu;->a(LX/0QB;)LX/1eu;

    move-result-object v4

    check-cast v4, LX/1eu;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V8;

    invoke-static {v0}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v6

    check-cast v6, LX/1ev;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1et;-><init>(Landroid/content/res/Resources;LX/1eu;LX/1V8;LX/1ev;)V

    .line 289258
    move-object v0, p0

    .line 289259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 289260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1et;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 289261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 289262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1f6;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1f6;"
        }
    .end annotation

    .prologue
    .line 289245
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 289246
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 289247
    invoke-static {p1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Ua;

    move-result-object v1

    .line 289248
    :goto_0
    iget-object v2, p0, LX/1et;->c:LX/1V8;

    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    iget v4, p0, LX/1et;->b:F

    invoke-virtual {v2, v1, v3, v4}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v1

    .line 289249
    iget-object v2, p0, LX/1et;->d:LX/1ev;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    iget-object v3, p0, LX/1et;->a:Landroid/graphics/Rect;

    iget-object v4, p0, LX/1et;->d:LX/1ev;

    invoke-virtual {v4, v1}, LX/1ev;->a(I)I

    move-result v1

    invoke-virtual {v2, v0, v3, v1}, LX/1ev;->a(Lcom/facebook/graphql/model/GraphQLMedia;Landroid/graphics/Rect;I)LX/1f6;

    move-result-object v0

    return-object v0

    .line 289250
    :cond_0
    sget-object v1, LX/1Ua;->e:LX/1Ua;

    move-object v1, v1

    .line 289251
    goto :goto_0
.end method
