.class public LX/1dF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final g:Landroid/graphics/Rect;

.field private static final h:Landroid/graphics/Rect;


# instance fields
.field public final a:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "LX/1cv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "LX/1dX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1dO;",
            ">;"
        }
    .end annotation
.end field

.field public d:[J

.field public e:Z

.field public final f:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "LX/1cn;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1De;

.field public final j:Lcom/facebook/components/ComponentView;

.field public final k:Landroid/graphics/Rect;

.field private final l:LX/1dG;

.field public final m:LX/1dH;

.field public n:LX/3Ep;

.field public o:I

.field public p:I

.field public q:I

.field private final r:LX/1cv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 284748
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    .line 284749
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/components/ComponentView;)V
    .locals 3

    .prologue
    .line 284710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284711
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/1dF;->f:LX/0tf;

    .line 284712
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/1dF;->k:Landroid/graphics/Rect;

    .line 284713
    new-instance v0, LX/1dG;

    invoke-direct {v0}, LX/1dG;-><init>()V

    iput-object v0, p0, LX/1dF;->l:LX/1dG;

    .line 284714
    new-instance v0, LX/1dH;

    invoke-direct {v0}, LX/1dH;-><init>()V

    iput-object v0, p0, LX/1dF;->m:LX/1dH;

    .line 284715
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/1dF;->a:LX/0tf;

    .line 284716
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/1dF;->b:LX/0tf;

    .line 284717
    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/1De;

    iput-object v0, p0, LX/1dF;->i:LX/1De;

    .line 284718
    iput-object p1, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    .line 284719
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1dF;->e:Z

    .line 284720
    sget-boolean v0, LX/1V5;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :goto_0
    iput-object v0, p0, LX/1dF;->c:Ljava/util/Map;

    .line 284721
    invoke-static {}, LX/1dI;->q()LX/1X1;

    move-result-object v0

    iget-object v1, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    invoke-static {v0, v1, v2}, LX/1cy;->a(LX/1X1;LX/1cn;Ljava/lang/Object;)LX/1dJ;

    move-result-object v0

    iput-object v0, p0, LX/1dF;->r:LX/1cv;

    .line 284722
    return-void

    .line 284723
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Rect;)I
    .locals 2

    .prologue
    .line 284724
    invoke-virtual {p0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 284725
    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    check-cast p0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/1dK;LX/0tf;)LX/1cn;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dK;",
            "LX/0tf",
            "<",
            "LX/1cn;",
            ">;)",
            "LX/1cn;"
        }
    .end annotation

    .prologue
    .line 284726
    iget-wide v2, p0, LX/1dK;->g:J

    move-wide v0, v2

    .line 284727
    invoke-virtual {p1, v0, v1}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cn;

    return-object v0
.end method

.method private a(ILX/1X1;Ljava/lang/Object;LX/1cn;LX/1dK;)LX/1cv;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1X1",
            "<*>;",
            "Ljava/lang/Object;",
            "LX/1cn;",
            "LX/1dK;",
            ")",
            "LX/1cv;"
        }
    .end annotation

    .prologue
    .line 284728
    instance-of v0, p5, LX/1dL;

    if-eqz v0, :cond_1

    .line 284729
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {p5, v0}, LX/1dK;->b(Landroid/graphics/Rect;)V

    move-object v0, p5

    .line 284730
    check-cast v0, LX/1dL;

    sget-object v1, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-static {p2, p4, p3, v0, v1}, LX/1cy;->a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dL;Landroid/graphics/Rect;)LX/1dJ;

    move-result-object v0

    move-object v6, v0

    .line 284731
    :goto_0
    iget-object v0, p0, LX/1dF;->a:LX/0tf;

    iget-object v1, p0, LX/1dF;->d:[J

    aget-wide v2, v1, p1

    invoke-virtual {v0, v2, v3, v6}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 284732
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {p5, v0}, LX/1dK;->a(Landroid/graphics/Rect;)V

    .line 284733
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {p4, p1, v6, v0}, LX/1cn;->a(ILX/1cv;Landroid/graphics/Rect;)V

    .line 284734
    invoke-static {v6}, LX/1dF;->b(LX/1cv;)V

    .line 284735
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    const/4 v5, 0x1

    move-object v0, p3

    invoke-static/range {v0 .. v5}, LX/1dF;->a(Ljava/lang/Object;IIIIZ)V

    .line 284736
    iget-object v0, v6, LX/1cv;->t:LX/1cz;

    move-object v0, v0

    .line 284737
    if-eqz v0, :cond_0

    .line 284738
    iget-object v0, v6, LX/1cv;->t:LX/1cz;

    move-object v0, v0

    .line 284739
    const/4 v1, 0x0

    .line 284740
    iput-boolean v1, v0, LX/1cz;->c:Z

    .line 284741
    :cond_0
    return-object v6

    .line 284742
    :cond_1
    sget-object v0, LX/1cy;->g:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    .line 284743
    if-nez v0, :cond_2

    .line 284744
    new-instance v0, LX/1cv;

    invoke-direct {v0}, LX/1cv;-><init>()V

    .line 284745
    :cond_2
    invoke-virtual {v0, p2, p4, p3, p5}, LX/1cv;->a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dK;)V

    .line 284746
    move-object v0, v0

    .line 284747
    move-object v6, v0

    goto :goto_0
.end method

.method public static a(LX/1dF;I)LX/1cv;
    .locals 4

    .prologue
    .line 284750
    iget-object v0, p0, LX/1dF;->a:LX/0tf;

    iget-object v1, p0, LX/1dF;->d:[J

    aget-wide v2, v1, p1

    invoke-virtual {v0, v2, v3}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cv;

    return-object v0
.end method

.method public static a(Landroid/view/View;)LX/1dM;
    .locals 1

    .prologue
    .line 284751
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284752
    check-cast p0, LX/1cn;

    .line 284753
    iget-object v0, p0, LX/1cn;->x:LX/1dM;

    move-object v0, v0

    .line 284754
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d0009

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dM;

    goto :goto_0
.end method

.method private a(LX/1X1;LX/1cn;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;",
            "LX/1cn;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 284755
    iget-object v0, p1, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 284756
    invoke-static {p1}, LX/1X1;->b(LX/1X1;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 284757
    iget-object v0, p2, LX/1cn;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 284758
    iget-object v0, p2, LX/1cn;->p:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cn;

    .line 284759
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x11

    if-ge v1, p0, :cond_0

    .line 284760
    invoke-virtual {p2, v0}, LX/1cn;->bringChildToFront(Landroid/view/View;)V

    .line 284761
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/1cn;->s:Z

    .line 284762
    :goto_0
    move-object v0, v0

    .line 284763
    :goto_1
    return-object v0

    :cond_1
    iget-object v1, p0, LX/1dF;->i:LX/1De;

    .line 284764
    iget p0, v0, LX/1S3;->d:I

    move v0, p0

    .line 284765
    invoke-static {v1, v0}, LX/1cy;->a(Landroid/content/Context;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/1cv;LX/1dK;)V
    .locals 6

    .prologue
    .line 284766
    instance-of v0, p1, LX/1dL;

    if-eqz v0, :cond_0

    .line 284767
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, LX/1dK;->b(Landroid/graphics/Rect;)V

    move-object v0, p0

    .line 284768
    check-cast v0, LX/1dJ;

    .line 284769
    iget-object v1, p1, LX/1dK;->b:LX/1X1;

    move-object v1, v1

    .line 284770
    iget-object v2, p0, LX/1cv;->c:LX/1cn;

    move-object v2, v2

    .line 284771
    iget-object v3, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 284772
    move-object v4, p1

    check-cast v4, LX/1dL;

    sget-object v5, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual/range {v0 .. v5}, LX/1dJ;->a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dL;Landroid/graphics/Rect;)V

    .line 284773
    :goto_0
    return-void

    .line 284774
    :cond_0
    iget-object v0, p1, LX/1dK;->b:LX/1X1;

    move-object v0, v0

    .line 284775
    iget-object v1, p0, LX/1cv;->c:LX/1cn;

    move-object v1, v1

    .line 284776
    iget-object v2, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 284777
    invoke-virtual {p0, v0, v1, v2, p1}, LX/1cv;->a(LX/1X1;LX/1cn;Ljava/lang/Object;LX/1dK;)V

    .line 284778
    goto :goto_0
.end method

.method public static a(LX/1dF;ILX/1dK;LX/1dN;)V
    .locals 12

    .prologue
    .line 284779
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p2, v0}, LX/1dF;->a(LX/1dK;LX/0tf;)LX/1cn;

    move-result-object v4

    .line 284780
    if-nez v4, :cond_1

    .line 284781
    const/4 v0, 0x0

    iget-object v1, p0, LX/1dF;->d:[J

    array-length v1, v1

    .line 284782
    :goto_0
    if-ge v0, v1, :cond_0

    .line 284783
    iget-object v2, p0, LX/1dF;->d:[J

    aget-wide v2, v2, v0

    .line 284784
    iget-wide v10, p2, LX/1dK;->g:J

    move-wide v4, v10

    .line 284785
    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 284786
    invoke-virtual {p3, v0}, LX/1dN;->b(I)LX/1dK;

    move-result-object v1

    .line 284787
    invoke-static {p0, v0, v1, p3}, LX/1dF;->a(LX/1dF;ILX/1dK;LX/1dN;)V

    .line 284788
    :cond_0
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p2, v0}, LX/1dF;->a(LX/1dK;LX/0tf;)LX/1cn;

    move-result-object v4

    .line 284789
    :cond_1
    iget-object v0, p2, LX/1dK;->b:LX/1X1;

    move-object v2, v0

    .line 284790
    iget-object v0, v2, LX/1X1;->e:LX/1S3;

    move-object v6, v0

    .line 284791
    invoke-direct {p0, v2, v4}, LX/1dF;->a(LX/1X1;LX/1cn;)Ljava/lang/Object;

    move-result-object v3

    .line 284792
    if-nez v3, :cond_2

    .line 284793
    iget-object v0, p0, LX/1dF;->i:LX/1De;

    invoke-virtual {v6, v0}, LX/1S3;->a(LX/1De;)Ljava/lang/Object;

    move-result-object v3

    .line 284794
    :cond_2
    iget-object v0, v2, LX/1X1;->f:LX/1De;

    move-object v0, v0

    .line 284795
    invoke-virtual {v6, v0, v3, v2}, LX/1S3;->e(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284796
    invoke-static {v2}, LX/1X1;->b(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v3

    .line 284797
    check-cast v0, LX/1cn;

    .line 284798
    iget-wide v10, p2, LX/1dK;->g:J

    move-wide v8, v10

    .line 284799
    iput-wide v8, v0, LX/1cn;->t:J

    .line 284800
    iget-wide v10, p2, LX/1dK;->a:J

    move-wide v8, v10

    .line 284801
    invoke-static {p0, v8, v9, v0}, LX/1dF;->a(LX/1dF;JLX/1cn;)V

    :cond_3
    move-object v0, p0

    move v1, p1

    move-object v5, p2

    .line 284802
    invoke-direct/range {v0 .. v5}, LX/1dF;->a(ILX/1X1;Ljava/lang/Object;LX/1cn;LX/1dK;)LX/1cv;

    move-result-object v0

    .line 284803
    iget-object v1, v2, LX/1X1;->f:LX/1De;

    move-object v1, v1

    .line 284804
    invoke-virtual {v6, v1, v3, v2}, LX/1S3;->b(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284805
    const/4 v1, 0x1

    .line 284806
    iput-boolean v1, v0, LX/1cv;->r:Z

    .line 284807
    iget-object v0, p0, LX/1dF;->m:LX/1dH;

    .line 284808
    iget v1, v0, LX/1dH;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/1dH;->a:I

    .line 284809
    return-void

    .line 284810
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(LX/1dF;JLX/1cn;)V
    .locals 1

    .prologue
    .line 284811
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/1cn;->a(Z)V

    .line 284812
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v0, p1, p2, p3}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 284813
    return-void
.end method

.method public static a(LX/1dF;LX/1De;ILX/0tf;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "LX/0tf",
            "<",
            "LX/1cn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284814
    invoke-static {p0, p2}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v4

    .line 284815
    if-eqz v4, :cond_0

    iget-object v0, p0, LX/1dF;->d:[J

    aget-wide v0, v0, p2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 284816
    :cond_0
    :goto_0
    return-void

    .line 284817
    :cond_1
    iget-object v0, v4, LX/1cv;->b:Ljava/lang/Object;

    move-object v1, v0

    .line 284818
    instance-of v0, v1, LX/1cn;

    if-eqz v0, :cond_5

    instance-of v0, v1, Lcom/facebook/components/ComponentView;

    if-nez v0, :cond_5

    move-object v0, v1

    .line 284819
    check-cast v0, LX/1cn;

    .line 284820
    invoke-virtual {v0}, LX/1cn;->getMountItemCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_1
    if-ltz v3, :cond_4

    .line 284821
    invoke-virtual {v0, v3}, LX/1cn;->a(I)LX/1cv;

    move-result-object v2

    .line 284822
    iget-object v5, p0, LX/1dF;->a:LX/0tf;

    iget-object v6, p0, LX/1dF;->a:LX/0tf;

    invoke-virtual {v6, v2}, LX/0tf;->a(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v5, v2}, LX/0tf;->b(I)J

    move-result-wide v6

    .line 284823
    iget-object v2, p0, LX/1dF;->d:[J

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    :goto_2
    if-ltz v2, :cond_2

    .line 284824
    iget-object v5, p0, LX/1dF;->d:[J

    aget-wide v8, v5, v2

    cmp-long v5, v8, v6

    if-nez v5, :cond_3

    .line 284825
    invoke-static {p0, p1, v2, p3}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    .line 284826
    :cond_2
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_1

    .line 284827
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 284828
    :cond_4
    invoke-virtual {v0}, LX/1cn;->getMountItemCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 284829
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursively unmounting items from a ComponentHost, left some items behind maybe because not tracked by its MountState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284830
    :cond_5
    iget-object v0, v4, LX/1cv;->c:LX/1cn;

    move-object v0, v0

    .line 284831
    iget-object v2, v4, LX/1cv;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 284832
    instance-of v3, v2, Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_a

    .line 284833
    iget-object v3, v4, LX/1cv;->t:LX/1cz;

    move-object v3, v3

    .line 284834
    if-eqz v3, :cond_9

    .line 284835
    iget-object v2, v4, LX/1cv;->t:LX/1cz;

    move-object v2, v2

    .line 284836
    invoke-static {v0, p2, v2}, LX/1cn;->a(LX/1cn;ILandroid/graphics/drawable/Drawable;)V

    .line 284837
    :cond_6
    :goto_3
    iget-object v2, v0, LX/1cn;->b:LX/0YU;

    invoke-static {p2, v2}, LX/1ct;->a(ILX/0YU;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 284838
    iget-object v2, v0, LX/1cn;->b:LX/0YU;

    .line 284839
    invoke-virtual {v2, p2}, LX/0YU;->b(I)V

    .line 284840
    :goto_4
    invoke-static {v0}, LX/1cn;->h(LX/1cn;)V

    .line 284841
    invoke-static {v4}, LX/1ct;->a(LX/1cv;)V

    .line 284842
    invoke-static {v4}, LX/1dF;->c(LX/1cv;)V

    .line 284843
    iget-object v0, v4, LX/1cv;->a:LX/1X1;

    move-object v2, v0

    .line 284844
    invoke-static {v2}, LX/1X1;->b(LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 284845
    check-cast v0, LX/1cn;

    invoke-virtual {p3, v0}, LX/0tf;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p3, v0}, LX/0tf;->a(I)V

    .line 284846
    :cond_7
    iget-object v0, v2, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 284847
    iget-boolean v3, v4, LX/1cv;->r:Z

    move v3, v3

    .line 284848
    if-eqz v3, :cond_8

    .line 284849
    invoke-virtual {v0, p1, v1, v2}, LX/1S3;->h(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284850
    const/4 v3, 0x0

    .line 284851
    iput-boolean v3, v4, LX/1cv;->r:Z

    .line 284852
    :cond_8
    invoke-virtual {v0, p1, v1, v2}, LX/1S3;->f(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284853
    iget-object v0, p0, LX/1dF;->a:LX/0tf;

    iget-object v1, p0, LX/1dF;->d:[J

    aget-wide v2, v1, p2

    .line 284854
    invoke-virtual {v0, v2, v3}, LX/0tf;->b(J)V

    .line 284855
    invoke-virtual {v4, p1}, LX/1cv;->a(Landroid/content/Context;)V

    .line 284856
    instance-of v0, v4, LX/1dJ;

    if-eqz v0, :cond_f

    .line 284857
    sget-object v0, LX/1cy;->h:LX/0Zi;

    check-cast v4, LX/1dJ;

    invoke-virtual {v0, v4}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 284858
    :goto_5
    iget-object v0, p0, LX/1dF;->m:LX/1dH;

    .line 284859
    iget v1, v0, LX/1dH;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/1dH;->b:I

    .line 284860
    goto/16 :goto_0

    .line 284861
    :cond_9
    check-cast v2, Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p2, v2}, LX/1cn;->a(LX/1cn;ILandroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 284862
    :cond_a
    instance-of v3, v2, Landroid/view/View;

    if-eqz v3, :cond_6

    .line 284863
    check-cast v2, Landroid/view/View;

    .line 284864
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/1cn;->s:Z

    .line 284865
    instance-of v3, v2, LX/1cn;

    if-eqz v3, :cond_d

    move-object v3, v2

    .line 284866
    check-cast v3, LX/1cn;

    .line 284867
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 284868
    invoke-virtual {v0}, LX/1cn;->invalidate()V

    .line 284869
    invoke-static {v3}, LX/1cn;->b(Landroid/view/View;)V

    .line 284870
    iget-object v5, v0, LX/1cn;->p:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284871
    :goto_6
    iget-object v2, v0, LX/1cn;->d:LX/0YU;

    invoke-static {p2, v2}, LX/1ct;->a(ILX/0YU;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 284872
    iget-object v2, v0, LX/1cn;->d:LX/0YU;

    .line 284873
    invoke-virtual {v2, p2}, LX/0YU;->b(I)V

    .line 284874
    :goto_7
    invoke-static {v0, p2, v4}, LX/1cn;->c(LX/1cn;ILX/1cv;)V

    goto/16 :goto_3

    .line 284875
    :cond_b
    iget-object v2, v0, LX/1cn;->c:LX/0YU;

    .line 284876
    invoke-virtual {v2, p2}, LX/0YU;->b(I)V

    .line 284877
    goto :goto_7

    .line 284878
    :cond_c
    iget-object v2, v0, LX/1cn;->a:LX/0YU;

    .line 284879
    invoke-virtual {v2, p2}, LX/0YU;->b(I)V

    .line 284880
    goto/16 :goto_4

    .line 284881
    :cond_d
    iget-boolean v3, v0, LX/1cn;->u:Z

    if-eqz v3, :cond_e

    .line 284882
    invoke-virtual {v0, v2}, LX/1cn;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_6

    .line 284883
    :cond_e
    invoke-virtual {v0, v2}, LX/1cn;->removeView(Landroid/view/View;)V

    goto :goto_6

    .line 284884
    :cond_f
    sget-object v0, LX/1cy;->g:LX/0Zi;

    invoke-virtual {v0, v4}, LX/0Zj;->a(Ljava/lang/Object;)Z

    goto :goto_5
.end method

.method public static a(LX/1dF;LX/1dN;)V
    .locals 12

    .prologue
    .line 284885
    iget-object v0, p0, LX/1dF;->c:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 284886
    :cond_0
    return-void

    .line 284887
    :cond_1
    iget-object v0, p0, LX/1dF;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dO;

    .line 284888
    const/4 v3, 0x0

    .line 284889
    iput-object v3, v0, LX/1dO;->a:Ljava/lang/String;

    .line 284890
    iget-object v2, v0, LX/1dO;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    .line 284891
    iput-object v3, v0, LX/1dO;->c:LX/1cn;

    .line 284892
    sget-object v2, LX/1cy;->m:LX/0Zi;

    invoke-virtual {v2, v0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 284893
    goto :goto_0

    .line 284894
    :cond_2
    iget-object v0, p0, LX/1dF;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 284895
    const/4 v0, 0x0

    .line 284896
    iget-object v1, p1, LX/1dN;->n:Ljava/util/List;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_1
    move v2, v1

    .line 284897
    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_0

    .line 284898
    iget-object v0, p1, LX/1dN;->n:Ljava/util/List;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    :goto_3
    move-object v3, v0

    .line 284899
    iget-wide v10, v3, LX/1dP;->b:J

    move-wide v4, v10

    .line 284900
    sget-object v0, LX/1cy;->m:LX/0Zi;

    if-nez v0, :cond_3

    .line 284901
    new-instance v0, LX/0Zi;

    const/16 v6, 0x40

    invoke-direct {v0, v6}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1cy;->m:LX/0Zi;

    .line 284902
    :cond_3
    sget-object v0, LX/1cy;->m:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dO;

    .line 284903
    if-nez v0, :cond_4

    .line 284904
    new-instance v0, LX/1dO;

    invoke-direct {v0}, LX/1dO;-><init>()V

    .line 284905
    :cond_4
    move-object v6, v0

    .line 284906
    const-wide/16 v8, -0x1

    cmp-long v0, v4, v8

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 284907
    :goto_4
    iput-object v0, v6, LX/1dO;->c:LX/1cn;

    .line 284908
    iget-object v0, v3, LX/1dP;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 284909
    iget-object v4, v6, LX/1dO;->b:Landroid/graphics/Rect;

    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 284910
    iget-object v0, v3, LX/1dP;->a:Ljava/lang/String;

    move-object v0, v0

    .line 284911
    iput-object v0, v6, LX/1dO;->a:Ljava/lang/String;

    .line 284912
    iget-object v0, p0, LX/1dF;->c:Ljava/util/Map;

    .line 284913
    iget-object v4, v3, LX/1dP;->a:Ljava/lang/String;

    move-object v3, v4

    .line 284914
    invoke-interface {v0, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284915
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 284916
    :cond_5
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v0, v4, v5}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cn;

    goto :goto_4

    :cond_6
    iget-object v1, p1, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_1

    :cond_7
    iget-object v0, p1, LX/1dN;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dP;

    goto :goto_3
.end method

.method public static a(LX/1dF;Z)V
    .locals 2

    .prologue
    .line 284917
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 284918
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v0, v1}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cn;

    invoke-virtual {v0, p1}, LX/1cn;->a(Z)V

    .line 284919
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 284920
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;LX/1cv;)V
    .locals 1

    .prologue
    .line 284921
    instance-of v0, p0, LX/1cn;

    if-nez v0, :cond_0

    .line 284922
    :goto_0
    return-void

    .line 284923
    :cond_0
    check-cast p0, LX/1cn;

    .line 284924
    iget-object v0, p1, LX/1cv;->j:LX/1dQ;

    move-object v0, v0

    .line 284925
    iput-object v0, p0, LX/1cn;->B:LX/1dQ;

    .line 284926
    iget-object v0, p1, LX/1cv;->k:LX/1dQ;

    move-object v0, v0

    .line 284927
    iput-object v0, p0, LX/1cn;->C:LX/1dQ;

    .line 284928
    iget-object v0, p1, LX/1cv;->l:LX/1dQ;

    move-object v0, v0

    .line 284929
    iput-object v0, p0, LX/1cn;->D:LX/1dQ;

    .line 284930
    iget-object v0, p1, LX/1cv;->m:LX/1dQ;

    move-object v0, v0

    .line 284931
    iput-object v0, p0, LX/1cn;->E:LX/1dQ;

    .line 284932
    iget-object v0, p1, LX/1cv;->o:LX/1dQ;

    move-object v0, v0

    .line 284933
    iput-object v0, p0, LX/1cn;->G:LX/1dQ;

    .line 284934
    iget-object v0, p1, LX/1cv;->n:LX/1dQ;

    move-object v0, v0

    .line 284935
    iput-object v0, p0, LX/1cn;->F:LX/1dQ;

    .line 284936
    iget-object v0, p1, LX/1cv;->p:LX/1dQ;

    move-object v0, v0

    .line 284937
    iput-object v0, p0, LX/1cn;->H:LX/1dQ;

    .line 284938
    iget-object v0, p1, LX/1cv;->q:LX/1dQ;

    move-object v0, v0

    .line 284939
    iput-object v0, p0, LX/1cn;->I:LX/1dQ;

    .line 284940
    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/1dM;)V
    .locals 1

    .prologue
    .line 284941
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284942
    check-cast p0, LX/1cn;

    .line 284943
    iput-object p1, p0, LX/1cn;->x:LX/1dM;

    .line 284944
    invoke-virtual {p0, p1}, LX/1cn;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284945
    :goto_0
    return-void

    .line 284946
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284947
    const v0, 0x7f0d0009

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/1dR;)V
    .locals 1

    .prologue
    .line 284948
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284949
    check-cast p0, LX/1cn;

    .line 284950
    iput-object p1, p0, LX/1cn;->y:LX/1dR;

    .line 284951
    invoke-virtual {p0, p1}, LX/1cn;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 284952
    :goto_0
    return-void

    .line 284953
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 284954
    const v0, 0x7f0d000a

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 284955
    invoke-static {}, LX/1dS;->b()V

    .line 284956
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 284957
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->intersect(IIII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 284958
    :cond_0
    :goto_0
    return-void

    .line 284959
    :cond_1
    instance-of v0, p0, Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_2

    .line 284960
    check-cast p0, Lcom/facebook/components/ComponentView;

    .line 284961
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/components/ComponentView;->a(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 284962
    :cond_2
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 284963
    check-cast p0, Landroid/view/ViewGroup;

    .line 284964
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 284965
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 284966
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->right:I

    .line 284967
    sget-object v0, LX/1dF;->h:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 284968
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 284969
    sget-object v5, LX/1dF;->h:Landroid/graphics/Rect;

    invoke-virtual {v5, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 284970
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    sget-object v6, LX/1dF;->h:Landroid/graphics/Rect;

    invoke-static {v5, v6}, LX/1dF;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 284971
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 284972
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 284973
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 284974
    :goto_0
    return-void

    .line 284975
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;IIIIZ)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 284687
    invoke-static {}, LX/1dS;->b()V

    .line 284688
    instance-of v0, p0, Landroid/view/View;

    if-eqz v0, :cond_4

    .line 284689
    check-cast p0, Landroid/view/View;

    .line 284690
    sub-int v0, p3, p1

    .line 284691
    sub-int v1, p4, p2

    .line 284692
    if-nez p5, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 284693
    :cond_0
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 284694
    :cond_1
    if-nez p5, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-ne v0, p1, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    if-ne v0, p2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    if-ne v0, p3, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-eq v0, p4, :cond_3

    .line 284695
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    .line 284696
    :cond_3
    :goto_0
    return-void

    .line 284697
    :cond_4
    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    .line 284698
    check-cast p0, Landroid/graphics/drawable/Drawable;

    .line 284699
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 284700
    iget v1, v0, Landroid/graphics/Rect;->left:I

    if-ne v1, p1, :cond_5

    iget v1, v0, Landroid/graphics/Rect;->top:I

    if-ne v1, p2, :cond_5

    iget v1, v0, Landroid/graphics/Rect;->right:I

    if-ne v1, p3, :cond_5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-eq v0, p4, :cond_3

    .line 284701
    :cond_5
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 284702
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported mounted content "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/1cv;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 284703
    if-nez p0, :cond_0

    move v0, v1

    .line 284704
    :goto_0
    return v0

    .line 284705
    :cond_0
    iget-object v0, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284706
    instance-of v2, v0, LX/1cn;

    if-nez v2, :cond_1

    move v0, v1

    .line 284707
    goto :goto_0

    .line 284708
    :cond_1
    check-cast v0, LX/1cn;

    .line 284709
    invoke-virtual {v0}, LX/1cn;->getMountItemCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/1dF;LX/1dK;LX/1cv;ZLX/1cp;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 284131
    iget-object v0, p1, LX/1dK;->b:LX/1X1;

    move-object v6, v0

    .line 284132
    iget-object v0, p2, LX/1cv;->a:LX/1X1;

    move-object v7, v0

    .line 284133
    iget-object v3, p0, LX/1dF;->a:LX/0tf;

    iget-object v4, p0, LX/1dF;->d:[J

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/1dF;->a(LX/1dK;LX/1cv;ZLX/0tf;[JLX/1cp;)Z

    move-result v1

    .line 284134
    if-eqz v1, :cond_0

    .line 284135
    invoke-static {p2}, LX/1dF;->c(LX/1cv;)V

    .line 284136
    :cond_0
    iget-boolean v0, p2, LX/1cv;->r:Z

    move v0, v0

    .line 284137
    if-eqz v0, :cond_1

    .line 284138
    iget-object v0, v7, LX/1X1;->e:LX/1S3;

    move-object v0, v0

    .line 284139
    iget-object v2, v7, LX/1X1;->f:LX/1De;

    move-object v2, v2

    .line 284140
    iget-object v3, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 284141
    invoke-virtual {v0, v2, v3, v7}, LX/1S3;->h(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284142
    iput-boolean v8, p2, LX/1cv;->r:Z

    .line 284143
    :cond_1
    invoke-static {p2, p1}, LX/1dF;->a(LX/1cv;LX/1dK;)V

    .line 284144
    if-eqz v1, :cond_2

    .line 284145
    iget-object v0, p1, LX/1dK;->b:LX/1X1;

    move-object v0, v0

    .line 284146
    invoke-static {v0}, LX/1X1;->b(LX/1X1;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 284147
    :goto_0
    invoke-static {p2}, LX/1dF;->b(LX/1cv;)V

    .line 284148
    :cond_2
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284149
    invoke-static {p1, p2}, LX/1dF;->b(LX/1dK;LX/1cv;)V

    .line 284150
    iget-object v2, v6, LX/1X1;->e:LX/1S3;

    move-object v2, v2

    .line 284151
    iget-object v3, v6, LX/1X1;->f:LX/1De;

    move-object v3, v3

    .line 284152
    invoke-virtual {v2, v3, v0, v6}, LX/1S3;->b(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284153
    const/4 v0, 0x1

    .line 284154
    iput-boolean v0, p2, LX/1cv;->r:Z

    .line 284155
    invoke-static {p2}, LX/1ct;->a(LX/1cv;)V

    .line 284156
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284157
    instance-of v0, v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 284158
    iget-object v0, p2, LX/1cv;->c:LX/1cn;

    move-object v2, v0

    .line 284159
    iget-object v0, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284160
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 284161
    iget v3, p2, LX/1cv;->u:I

    move v3, v3

    .line 284162
    invoke-static {v2, v0, v3}, LX/1ct;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    .line 284163
    :cond_3
    iget-object v0, p2, LX/1cv;->t:LX/1cz;

    move-object v0, v0

    .line 284164
    if-eqz v0, :cond_4

    .line 284165
    iget-object v0, p2, LX/1cv;->t:LX/1cz;

    move-object v0, v0

    .line 284166
    iput-boolean v8, v0, LX/1cz;->c:Z

    .line 284167
    :cond_4
    return v1

    .line 284168
    :cond_5
    iget-object v2, p2, LX/1cv;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 284169
    iget-object v3, v0, LX/1X1;->e:LX/1S3;

    move-object v3, v3

    .line 284170
    iget-object v4, v7, LX/1X1;->f:LX/1De;

    move-object v4, v4

    .line 284171
    invoke-virtual {v3, v4, v2, v7}, LX/1S3;->f(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284172
    iget-object v4, v0, LX/1X1;->f:LX/1De;

    move-object v4, v4

    .line 284173
    invoke-virtual {v3, v4, v2, v0}, LX/1S3;->e(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284174
    goto :goto_0
.end method

.method private static a(LX/1dK;LX/1cv;ZLX/0tf;[JLX/1cp;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dK;",
            "LX/1cv;",
            "Z",
            "LX/0tf",
            "<",
            "LX/1cv;",
            ">;[J",
            "Lcom/facebook/components/ComponentsLogger;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 284432
    iget v0, p0, LX/1dK;->w:I

    move v0, v0

    .line 284433
    iget-object v1, p1, LX/1cv;->a:LX/1X1;

    move-object v1, v1

    .line 284434
    iget-object v2, v1, LX/1X1;->e:LX/1S3;

    move-object v2, v2

    .line 284435
    iget-object v3, p0, LX/1dK;->b:LX/1X1;

    move-object v3, v3

    .line 284436
    iget-object v4, v3, LX/1X1;->e:LX/1S3;

    move-object v4, v4

    .line 284437
    iget-object v5, p0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v5, v5

    .line 284438
    iget-object v6, p1, LX/1cv;->b:Ljava/lang/Object;

    move-object v6, v6

    .line 284439
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-static {v6}, LX/1dF;->a(Ljava/lang/Object;)I

    move-result v8

    if-ne v7, v8, :cond_d

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    .line 284440
    instance-of v7, v6, Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_e

    check-cast v6, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    :goto_0
    move v6, v7

    .line 284441
    if-ne v5, v6, :cond_d

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 284442
    if-nez v5, :cond_0

    invoke-virtual {v4}, LX/1S3;->m()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 284443
    const/4 v0, 0x1

    .line 284444
    :goto_2
    return v0

    .line 284445
    :cond_0
    if-eqz p2, :cond_b

    .line 284446
    const/4 v5, 0x1

    if-ne v0, v5, :cond_a

    .line 284447
    instance-of v0, v2, LX/1dT;

    if-eqz v0, :cond_9

    instance-of v0, v4, LX/1dT;

    if-eqz v0, :cond_9

    invoke-virtual {v2, v1, v3}, LX/1S3;->b(LX/1X1;LX/1X1;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 284448
    if-eqz p5, :cond_5

    .line 284449
    new-instance v1, LX/1dU;

    invoke-direct {v1}, LX/1dU;-><init>()V

    .line 284450
    invoke-virtual {p3, p1}, LX/0tf;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p3, v0}, LX/0tf;->b(I)J

    move-result-wide v6

    iput-wide v6, v1, LX/1dU;->a:J

    .line 284451
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1dU;->b:Ljava/lang/String;

    .line 284452
    iget-wide v8, p0, LX/1dK;->a:J

    move-wide v2, v8

    .line 284453
    iput-wide v2, v1, LX/1dU;->e:J

    .line 284454
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/1dU;->f:Ljava/lang/String;

    .line 284455
    const/4 v0, 0x0

    :goto_3
    array-length v2, p4

    if-ge v0, v2, :cond_3

    .line 284456
    aget-wide v2, p4, v0

    iget-wide v4, v1, LX/1dU;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 284457
    iget v2, v1, LX/1dU;->c:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 284458
    iput v0, v1, LX/1dU;->c:I

    .line 284459
    :cond_1
    iput v0, v1, LX/1dU;->d:I

    .line 284460
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 284461
    :cond_3
    iget-wide v2, v1, LX/1dU;->e:J

    iget-wide v4, v1, LX/1dU;->a:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_6

    .line 284462
    iget v0, v1, LX/1dU;->c:I

    iput v0, v1, LX/1dU;->g:I

    .line 284463
    iget v0, v1, LX/1dU;->d:I

    iput v0, v1, LX/1dU;->h:I

    .line 284464
    :cond_4
    if-eqz p5, :cond_5

    .line 284465
    const/16 v0, 0x8

    invoke-virtual {p5, v0, v1}, LX/1cp;->a(ILjava/lang/Object;)V

    .line 284466
    const/16 v0, 0x8

    const/16 v2, 0x10

    invoke-virtual {p5, v0, v1, v2}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 284467
    :cond_5
    const/4 v0, 0x1

    goto :goto_2

    .line 284468
    :cond_6
    const/4 v0, 0x0

    :goto_4
    array-length v2, p4

    if-ge v0, v2, :cond_4

    .line 284469
    aget-wide v2, p4, v0

    iget-wide v4, v1, LX/1dU;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_8

    .line 284470
    iget v2, v1, LX/1dU;->g:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_7

    .line 284471
    iput v0, v1, LX/1dU;->g:I

    .line 284472
    :cond_7
    iput v0, v1, LX/1dU;->h:I

    .line 284473
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 284474
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 284475
    :cond_a
    const/4 v4, 0x2

    if-ne v0, v4, :cond_b

    .line 284476
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 284477
    :cond_b
    invoke-virtual {v2}, LX/1S3;->l()Z

    move-result v0

    if-nez v0, :cond_c

    .line 284478
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 284479
    :cond_c
    invoke-virtual {v2, v1, v3}, LX/1S3;->b(LX/1X1;LX/1X1;)Z

    move-result v0

    goto/16 :goto_2

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_e
    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v7

    goto/16 :goto_0
.end method

.method public static b(Landroid/view/View;)LX/1dR;
    .locals 1

    .prologue
    .line 284428
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284429
    check-cast p0, LX/1cn;

    .line 284430
    iget-object v0, p0, LX/1cn;->y:LX/1dR;

    move-object v0, v0

    .line 284431
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d000a

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dR;

    goto :goto_0
.end method

.method private static b(LX/1cv;)V
    .locals 5

    .prologue
    .line 284364
    iget-object v0, p0, LX/1cv;->a:LX/1X1;

    move-object v1, v0

    .line 284365
    invoke-static {v1}, LX/1X1;->d(LX/1X1;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 284366
    :cond_0
    :goto_0
    return-void

    .line 284367
    :cond_1
    iget-object v0, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284368
    check-cast v0, Landroid/view/View;

    .line 284369
    iget-object v2, p0, LX/1cv;->g:LX/1dQ;

    move-object v2, v2

    .line 284370
    if-eqz v2, :cond_3

    .line 284371
    invoke-static {v0}, LX/1dF;->a(Landroid/view/View;)LX/1dM;

    move-result-object v3

    .line 284372
    if-nez v3, :cond_2

    .line 284373
    new-instance v3, LX/1dM;

    invoke-direct {v3}, LX/1dM;-><init>()V

    .line 284374
    invoke-static {v0, v3}, LX/1dF;->a(Landroid/view/View;LX/1dM;)V

    .line 284375
    :cond_2
    iput-object v2, v3, LX/1dM;->a:LX/1dQ;

    .line 284376
    if-eqz v2, :cond_3

    .line 284377
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 284378
    :cond_3
    iget-object v2, p0, LX/1cv;->h:LX/1dQ;

    move-object v2, v2

    .line 284379
    if-eqz v2, :cond_5

    .line 284380
    invoke-static {v0}, LX/1dF;->b(Landroid/view/View;)LX/1dR;

    move-result-object v3

    .line 284381
    if-nez v3, :cond_4

    .line 284382
    new-instance v3, LX/1dR;

    invoke-direct {v3}, LX/1dR;-><init>()V

    .line 284383
    invoke-static {v0, v3}, LX/1dF;->a(Landroid/view/View;LX/1dR;)V

    .line 284384
    :cond_4
    iput-object v2, v3, LX/1dR;->a:LX/1dQ;

    .line 284385
    if-eqz v2, :cond_5

    .line 284386
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/View;->setLongClickable(Z)V

    .line 284387
    :cond_5
    iget-object v2, p0, LX/1cv;->i:LX/1dQ;

    move-object v2, v2

    .line 284388
    invoke-static {v2, v0}, LX/1dF;->c(LX/1dQ;Landroid/view/View;)V

    .line 284389
    invoke-static {v0, p0}, LX/1dF;->b(Landroid/view/View;LX/1cv;)V

    .line 284390
    invoke-static {v0, p0}, LX/1dF;->c(Landroid/view/View;LX/1cv;)V

    .line 284391
    iget-object v2, p0, LX/1cv;->d:Ljava/lang/CharSequence;

    move-object v2, v2

    .line 284392
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 284393
    :goto_1
    iget v2, p0, LX/1cv;->s:I

    move v2, v2

    .line 284394
    if-nez v2, :cond_9

    .line 284395
    :goto_2
    invoke-static {v0, p0}, LX/1dF;->a(Landroid/view/View;LX/1cv;)V

    .line 284396
    iget v2, p0, LX/1cv;->u:I

    move v2, v2

    .line 284397
    and-int/lit16 v3, v2, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_b

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 284398
    if-eqz v2, :cond_a

    .line 284399
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 284400
    :cond_6
    :goto_4
    instance-of v2, p0, LX/1dJ;

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/1X1;->b(LX/1X1;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 284401
    check-cast p0, LX/1dJ;

    .line 284402
    invoke-virtual {p0}, LX/1dJ;->D()Z

    move-result v1

    if-nez v1, :cond_d

    .line 284403
    :goto_5
    iget-object v1, p0, LX/1dJ;->b:LX/1dc;

    move-object v2, v1

    .line 284404
    if-eqz v2, :cond_7

    .line 284405
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, LX/1De;

    invoke-static {v1, v2}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, LX/1dF;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 284406
    :cond_7
    invoke-static {v0, p0}, LX/1dF;->e(Landroid/view/View;LX/1dJ;)V

    .line 284407
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v1, v2, :cond_e

    .line 284408
    :goto_6
    goto/16 :goto_0

    .line 284409
    :cond_8
    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 284410
    :cond_9
    invoke-static {v0, v2}, LX/0vv;->d(Landroid/view/View;I)V

    goto :goto_2

    .line 284411
    :cond_a
    iget v2, p0, LX/1cv;->u:I

    move v2, v2

    .line 284412
    and-int/lit16 v3, v2, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_c

    const/4 v3, 0x1

    :goto_7
    move v2, v3

    .line 284413
    if-eqz v2, :cond_6

    .line 284414
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_4

    :cond_b
    const/4 v3, 0x0

    goto :goto_3

    :cond_c
    const/4 v3, 0x0

    goto :goto_7

    .line 284415
    :cond_d
    iget-object v1, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    move v1, v1

    .line 284416
    iget-object v2, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    move v2, v2

    .line 284417
    iget-object v3, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    move v3, v3

    .line 284418
    iget-object v4, p0, LX/1dJ;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    move v4, v4

    .line 284419
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    .line 284420
    :cond_e
    iget v1, p0, LX/1dJ;->d:I

    move v1, v1

    .line 284421
    packed-switch v1, :pswitch_data_0

    .line 284422
    const/4 v1, 0x2

    .line 284423
    :goto_8
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    goto :goto_6

    .line 284424
    :pswitch_0
    const/4 v1, 0x0

    .line 284425
    goto :goto_8

    .line 284426
    :pswitch_1
    const/4 v1, 0x1

    .line 284427
    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/1dF;LX/1dN;)V
    .locals 11

    .prologue
    const/4 v8, 0x5

    const-wide/16 v4, 0x0

    .line 284335
    iget-object v0, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    .line 284336
    iget-object v1, v0, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    move-object v1, v1

    .line 284337
    iget-object v0, v1, LX/1dV;->k:LX/1De;

    move-object v0, v0

    .line 284338
    iget-object v2, v0, LX/1De;->d:LX/1cp;

    move-object v2, v2

    .line 284339
    iget-object v0, v1, LX/1dV;->k:LX/1De;

    move-object v0, v0

    .line 284340
    iget-object v3, v0, LX/1De;->c:Ljava/lang/String;

    move-object v0, v3

    .line 284341
    if-eqz v2, :cond_0

    .line 284342
    invoke-virtual {v2, v8, v1}, LX/1cp;->a(ILjava/lang/Object;)V

    .line 284343
    :cond_0
    invoke-static {p0, p1}, LX/1dF;->c(LX/1dF;LX/1dN;)LX/1dG;

    move-result-object v3

    .line 284344
    if-eqz v2, :cond_1

    .line 284345
    const/4 v9, 0x5

    .line 284346
    const-string v6, "log_tag"

    invoke-virtual {v2, v9, v1, v6, v0}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 284347
    const-string v6, "unmounted_count"

    iget v7, v3, LX/1dG;->a:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v9, v1, v6, v7}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 284348
    const-string v6, "moved_count"

    iget v7, v3, LX/1dG;->b:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v9, v1, v6, v7}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 284349
    const-string v6, "unchanged_count"

    iget v7, v3, LX/1dG;->c:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v9, v1, v6, v7}, LX/1cp;->b(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 284350
    :cond_1
    iget-object v0, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v0, v4, v5}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 284351
    iget-object v0, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    invoke-static {p0, v4, v5, v0}, LX/1dF;->a(LX/1dF;JLX/1cn;)V

    .line 284352
    iget-object v0, p0, LX/1dF;->a:LX/0tf;

    iget-object v3, p0, LX/1dF;->r:LX/1cv;

    invoke-virtual {v0, v4, v5, v3}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 284353
    :cond_2
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v3

    .line 284354
    iget-object v0, p0, LX/1dF;->d:[J

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1dF;->d:[J

    array-length v0, v0

    if-eq v3, v0, :cond_4

    .line 284355
    :cond_3
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, LX/1dF;->d:[J

    .line 284356
    :cond_4
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_5

    .line 284357
    iget-object v4, p0, LX/1dF;->d:[J

    invoke-virtual {p1, v0}, LX/1dN;->b(I)LX/1dK;

    move-result-object v5

    .line 284358
    iget-wide v9, v5, LX/1dK;->a:J

    move-wide v6, v9

    .line 284359
    aput-wide v6, v4, v0

    .line 284360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 284361
    :cond_5
    if-eqz v2, :cond_6

    .line 284362
    const/16 v0, 0x10

    invoke-virtual {v2, v8, v1, v0}, LX/1cp;->a(ILjava/lang/Object;I)V

    .line 284363
    :cond_6
    return-void
.end method

.method public static b(LX/1dF;LX/1dN;Landroid/graphics/Rect;)V
    .locals 13

    .prologue
    .line 284263
    if-nez p2, :cond_1

    .line 284264
    :cond_0
    return-void

    .line 284265
    :cond_1
    const/4 v0, 0x0

    .line 284266
    iget-object v1, p1, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move v3, v1

    .line 284267
    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 284268
    iget-object v0, p1, LX/1dN;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dW;

    move-object v0, v0

    .line 284269
    iget-object v1, v0, LX/1dW;->d:LX/1dQ;

    move-object v1, v1

    .line 284270
    iget-object v4, v0, LX/1dW;->e:LX/1dQ;

    move-object v4, v4

    .line 284271
    iget-object v5, v0, LX/1dW;->f:LX/1dQ;

    move-object v5, v5

    .line 284272
    iget-object v6, v0, LX/1dW;->g:LX/1dQ;

    move-object v6, v6

    .line 284273
    iget-wide v11, v0, LX/1dW;->a:J

    move-wide v8, v11

    .line 284274
    iget-object v7, v0, LX/1dW;->c:Landroid/graphics/Rect;

    move-object v7, v7

    .line 284275
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 284276
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v10

    .line 284277
    iget-object v0, p0, LX/1dF;->b:LX/0tf;

    invoke-virtual {v0, v8, v9}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dX;

    .line 284278
    if-eqz v10, :cond_d

    .line 284279
    if-nez v0, :cond_4

    .line 284280
    sget-object v0, LX/1cy;->n:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dX;

    .line 284281
    if-nez v0, :cond_2

    .line 284282
    new-instance v0, LX/1dX;

    invoke-direct {v0}, LX/1dX;-><init>()V

    .line 284283
    :cond_2
    iput-object v6, v0, LX/1dX;->b:LX/1dQ;

    .line 284284
    move-object v0, v0

    .line 284285
    iget-object v6, p0, LX/1dF;->b:LX/0tf;

    invoke-virtual {v6, v8, v9, v0}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 284286
    if-eqz v1, :cond_4

    .line 284287
    invoke-static {}, LX/1dS;->b()V

    .line 284288
    sget-object v6, LX/1dY;->d:LX/1dZ;

    if-nez v6, :cond_3

    .line 284289
    new-instance v6, LX/1dZ;

    invoke-direct {v6}, LX/1dZ;-><init>()V

    sput-object v6, LX/1dY;->d:LX/1dZ;

    .line 284290
    :cond_3
    iget-object v6, v1, LX/1dQ;->a:LX/1X1;

    .line 284291
    iget-object v8, v6, LX/1X1;->e:LX/1S3;

    move-object v6, v8

    .line 284292
    sget-object v8, LX/1dY;->d:LX/1dZ;

    invoke-virtual {v6, v1, v8}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284293
    :cond_4
    move-object v1, v0

    .line 284294
    if-eqz v4, :cond_6

    .line 284295
    iget v0, v1, LX/1dX;->a:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 284296
    if-nez v0, :cond_6

    .line 284297
    iget-object v0, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 284298
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sget-object v8, LX/1dF;->g:Landroid/graphics/Rect;

    .line 284299
    mul-int v9, v6, v0

    div-int/lit8 v9, v9, 0x2

    .line 284300
    invoke-static {v7}, LX/1dF;->a(Landroid/graphics/Rect;)I

    move-result v10

    .line 284301
    invoke-static {v8}, LX/1dF;->a(Landroid/graphics/Rect;)I

    move-result v11

    .line 284302
    if-lt v10, v9, :cond_11

    if-lt v11, v9, :cond_10

    const/4 v9, 0x1

    :goto_2
    move v0, v9

    .line 284303
    if-eqz v0, :cond_6

    .line 284304
    iget v0, v1, LX/1dX;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v1, LX/1dX;->a:I

    .line 284305
    invoke-static {}, LX/1dS;->b()V

    .line 284306
    sget-object v0, LX/1dY;->f:LX/1da;

    if-nez v0, :cond_5

    .line 284307
    new-instance v0, LX/1da;

    invoke-direct {v0}, LX/1da;-><init>()V

    sput-object v0, LX/1dY;->f:LX/1da;

    .line 284308
    :cond_5
    iget-object v0, v4, LX/1dQ;->a:LX/1X1;

    .line 284309
    iget-object v6, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v6

    .line 284310
    sget-object v6, LX/1dY;->f:LX/1da;

    invoke-virtual {v0, v4, v6}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284311
    :cond_6
    if-eqz v5, :cond_c

    invoke-virtual {v1}, LX/1dX;->d()Z

    move-result v0

    if-nez v0, :cond_c

    .line 284312
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    .line 284313
    iget v4, v7, Landroid/graphics/Rect;->top:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    if-ne v4, v6, :cond_7

    .line 284314
    iget v4, v1, LX/1dX;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v1, LX/1dX;->a:I

    .line 284315
    :cond_7
    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    if-ne v4, v6, :cond_8

    .line 284316
    iget v4, v1, LX/1dX;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, v1, LX/1dX;->a:I

    .line 284317
    :cond_8
    iget v4, v7, Landroid/graphics/Rect;->left:I

    iget v6, v0, Landroid/graphics/Rect;->left:I

    if-ne v4, v6, :cond_9

    .line 284318
    iget v4, v1, LX/1dX;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, LX/1dX;->a:I

    .line 284319
    :cond_9
    iget v4, v7, Landroid/graphics/Rect;->right:I

    iget v6, v0, Landroid/graphics/Rect;->right:I

    if-ne v4, v6, :cond_a

    .line 284320
    iget v4, v1, LX/1dX;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, v1, LX/1dX;->a:I

    .line 284321
    :cond_a
    invoke-virtual {v1}, LX/1dX;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 284322
    invoke-static {}, LX/1dS;->b()V

    .line 284323
    sget-object v0, LX/1dY;->g:LX/1db;

    if-nez v0, :cond_b

    .line 284324
    new-instance v0, LX/1db;

    invoke-direct {v0}, LX/1db;-><init>()V

    sput-object v0, LX/1dY;->g:LX/1db;

    .line 284325
    :cond_b
    iget-object v0, v5, LX/1dQ;->a:LX/1X1;

    .line 284326
    iget-object v1, v0, LX/1X1;->e:LX/1S3;

    move-object v0, v1

    .line 284327
    sget-object v1, LX/1dY;->g:LX/1db;

    invoke-virtual {v0, v5, v1}, LX/1S3;->a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284328
    :cond_c
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 284329
    :cond_d
    if-eqz v0, :cond_c

    .line 284330
    if-eqz v6, :cond_e

    .line 284331
    invoke-static {v6}, LX/1dY;->d(LX/1dQ;)V

    .line 284332
    :cond_e
    iget-object v1, p0, LX/1dF;->b:LX/0tf;

    .line 284333
    invoke-virtual {v1, v8, v9}, LX/0tf;->b(J)V

    .line 284334
    invoke-static {v0}, LX/1cy;->a(LX/1dX;)V

    goto :goto_3

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 v9, 0x0

    goto/16 :goto_2

    :cond_11
    invoke-virtual {v7, v8}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v9

    goto/16 :goto_2
.end method

.method private static b(LX/1dK;LX/1cv;)V
    .locals 8

    .prologue
    .line 284253
    iget-wide v6, p0, LX/1dK;->a:J

    move-wide v0, v6

    .line 284254
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 284255
    :goto_0
    return-void

    .line 284256
    :cond_0
    sget-object v0, LX/1dF;->g:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, LX/1dK;->a(Landroid/graphics/Rect;)V

    .line 284257
    instance-of v0, p1, LX/1dJ;

    if-eqz v0, :cond_1

    .line 284258
    iget-object v0, p1, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284259
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    .line 284260
    :goto_1
    iget-object v0, p1, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284261
    sget-object v1, LX/1dF;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget-object v2, LX/1dF;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget-object v3, LX/1dF;->g:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sget-object v4, LX/1dF;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-static/range {v0 .. v5}, LX/1dF;->a(Ljava/lang/Object;IIIIZ)V

    goto :goto_0

    .line 284262
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private static b(Landroid/view/View;LX/1cv;)V
    .locals 2

    .prologue
    .line 284247
    iget-object v0, p1, LX/1cv;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 284248
    instance-of v1, p0, LX/1cn;

    if-eqz v1, :cond_0

    .line 284249
    check-cast p0, LX/1cn;

    .line 284250
    iput-object v0, p0, LX/1cn;->j:Ljava/lang/Object;

    .line 284251
    :goto_0
    return-void

    .line 284252
    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static c(LX/1dF;LX/1dN;)LX/1dG;
    .locals 9

    .prologue
    .line 284175
    iget-object v0, p0, LX/1dF;->l:LX/1dG;

    const/4 v1, 0x0

    .line 284176
    iput v1, v0, LX/1dG;->c:I

    .line 284177
    iput v1, v0, LX/1dG;->b:I

    .line 284178
    iput v1, v0, LX/1dG;->a:I

    .line 284179
    iget-object v0, p0, LX/1dF;->d:[J

    if-nez v0, :cond_0

    .line 284180
    iget-object v0, p0, LX/1dF;->l:LX/1dG;

    .line 284181
    :goto_0
    return-object v0

    .line 284182
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, LX/1dF;->d:[J

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 284183
    iget-object v1, p0, LX/1dF;->d:[J

    aget-wide v2, v1, v0

    invoke-virtual {p1, v2, v3}, LX/1dN;->a(J)I

    move-result v1

    .line 284184
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 284185
    iget-object v1, p0, LX/1dF;->i:LX/1De;

    iget-object v2, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p0, v1, v0, v2}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    .line 284186
    iget-object v1, p0, LX/1dF;->l:LX/1dG;

    invoke-static {v1}, LX/1dG;->b(LX/1dG;)I

    .line 284187
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 284188
    :cond_1
    invoke-virtual {p1, v1}, LX/1dN;->b(I)LX/1dK;

    move-result-object v2

    .line 284189
    iget-wide v7, v2, LX/1dK;->g:J

    move-wide v2, v7

    .line 284190
    invoke-static {p0, v0}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v4

    .line 284191
    if-nez v4, :cond_2

    .line 284192
    iget-object v1, p0, LX/1dF;->l:LX/1dG;

    invoke-static {v1}, LX/1dG;->b(LX/1dG;)I

    goto :goto_2

    .line 284193
    :cond_2
    iget-object v5, v4, LX/1cv;->c:LX/1cn;

    move-object v5, v5

    .line 284194
    iget-object v6, p0, LX/1dF;->f:LX/0tf;

    invoke-virtual {v6, v2, v3}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v2

    if-eq v5, v2, :cond_3

    .line 284195
    iget-object v1, p0, LX/1dF;->i:LX/1De;

    iget-object v2, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p0, v1, v0, v2}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    .line 284196
    iget-object v1, p0, LX/1dF;->l:LX/1dG;

    invoke-static {v1}, LX/1dG;->b(LX/1dG;)I

    goto :goto_2

    .line 284197
    :cond_3
    if-eq v1, v0, :cond_6

    .line 284198
    iget-object v2, v4, LX/1cv;->c:LX/1cn;

    move-object v2, v2

    .line 284199
    if-nez v4, :cond_4

    iget-object v3, v2, LX/1cn;->b:LX/0YU;

    if-eqz v3, :cond_4

    .line 284200
    iget-object v3, v2, LX/1cn;->b:LX/0YU;

    invoke-virtual {v3, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1cv;

    move-object v4, v3

    .line 284201
    :cond_4
    if-nez v4, :cond_8

    .line 284202
    :cond_5
    :goto_3
    iget-object v1, p0, LX/1dF;->l:LX/1dG;

    .line 284203
    iget v2, v1, LX/1dG;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v1, LX/1dG;->b:I

    .line 284204
    goto :goto_2

    .line 284205
    :cond_6
    iget-object v1, p0, LX/1dF;->l:LX/1dG;

    .line 284206
    iget v2, v1, LX/1dG;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v1, LX/1dG;->c:I

    .line 284207
    goto :goto_2

    .line 284208
    :cond_7
    iget-object v0, p0, LX/1dF;->l:LX/1dG;

    goto :goto_0

    .line 284209
    :cond_8
    invoke-static {v2, v4, v0, v1}, LX/1cn;->b(LX/1cn;LX/1cv;II)V

    .line 284210
    iget-object v3, v4, LX/1cv;->b:Ljava/lang/Object;

    move-object v5, v3

    .line 284211
    instance-of v3, v5, Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_12

    .line 284212
    iget-object v3, v2, LX/1cn;->e:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 284213
    iget-object v3, v2, LX/1cn;->f:LX/0YU;

    if-nez v3, :cond_9

    .line 284214
    invoke-static {}, LX/1cy;->k()LX/0YU;

    move-result-object v3

    iput-object v3, v2, LX/1cn;->f:LX/0YU;

    .line 284215
    :cond_9
    iget-object v3, v2, LX/1cn;->e:LX/0YU;

    iget-object v6, v2, LX/1cn;->f:LX/0YU;

    invoke-static {v1, v3, v6}, LX/1ct;->a(ILX/0YU;LX/0YU;)V

    .line 284216
    :cond_a
    iget-object v3, v4, LX/1cv;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 284217
    instance-of v3, v3, LX/1cu;

    .line 284218
    if-eqz v3, :cond_d

    iget-object v6, v2, LX/1cn;->g:LX/0YU;

    invoke-virtual {v6, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 284219
    iget-object v6, v2, LX/1cn;->h:LX/0YU;

    if-nez v6, :cond_c

    .line 284220
    sget-object v6, LX/1cy;->v:LX/0Zj;

    invoke-virtual {v6}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0YU;

    .line 284221
    if-nez v6, :cond_b

    .line 284222
    new-instance v6, LX/0YU;

    const/4 v7, 0x4

    invoke-direct {v6, v7}, LX/0YU;-><init>(I)V

    .line 284223
    :cond_b
    move-object v6, v6

    .line 284224
    iput-object v6, v2, LX/1cn;->h:LX/0YU;

    .line 284225
    :cond_c
    iget-object v6, v2, LX/1cn;->g:LX/0YU;

    iget-object v7, v2, LX/1cn;->h:LX/0YU;

    invoke-static {v1, v6, v7}, LX/1ct;->a(ILX/0YU;LX/0YU;)V

    .line 284226
    :cond_d
    iget-object v6, v2, LX/1cn;->e:LX/0YU;

    iget-object v7, v2, LX/1cn;->f:LX/0YU;

    invoke-static {v0, v1, v6, v7}, LX/1ct;->a(IILX/0YU;LX/0YU;)V

    .line 284227
    if-eqz v3, :cond_e

    .line 284228
    iget-object v3, v2, LX/1cn;->g:LX/0YU;

    iget-object v6, v2, LX/1cn;->h:LX/0YU;

    invoke-static {v0, v1, v3, v6}, LX/1ct;->a(IILX/0YU;LX/0YU;)V

    .line 284229
    :cond_e
    invoke-virtual {v2}, LX/1cn;->invalidate()V

    .line 284230
    invoke-static {v2}, LX/1cn;->h(LX/1cn;)V

    .line 284231
    :cond_f
    :goto_4
    iget-object v3, v2, LX/1cn;->a:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_11

    .line 284232
    iget-object v3, v2, LX/1cn;->b:LX/0YU;

    if-nez v3, :cond_10

    .line 284233
    invoke-static {}, LX/1cy;->k()LX/0YU;

    move-result-object v3

    iput-object v3, v2, LX/1cn;->b:LX/0YU;

    .line 284234
    :cond_10
    iget-object v3, v2, LX/1cn;->a:LX/0YU;

    iget-object v6, v2, LX/1cn;->b:LX/0YU;

    invoke-static {v1, v3, v6}, LX/1ct;->a(ILX/0YU;LX/0YU;)V

    .line 284235
    :cond_11
    iget-object v3, v2, LX/1cn;->a:LX/0YU;

    iget-object v6, v2, LX/1cn;->b:LX/0YU;

    invoke-static {v0, v1, v3, v6}, LX/1ct;->a(IILX/0YU;LX/0YU;)V

    .line 284236
    invoke-static {v2}, LX/1cn;->h(LX/1cn;)V

    .line 284237
    instance-of v3, v5, Landroid/view/View;

    if-eqz v3, :cond_5

    .line 284238
    check-cast v5, Landroid/view/View;

    invoke-static {v5}, LX/0vv;->q(Landroid/view/View;)V

    goto/16 :goto_3

    .line 284239
    :cond_12
    instance-of v3, v5, Landroid/view/View;

    if-eqz v3, :cond_f

    .line 284240
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/1cn;->s:Z

    move-object v3, v5

    .line 284241
    check-cast v3, Landroid/view/View;

    invoke-static {v3}, LX/1cn;->b(Landroid/view/View;)V

    .line 284242
    iget-object v3, v2, LX/1cn;->c:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 284243
    iget-object v3, v2, LX/1cn;->d:LX/0YU;

    if-nez v3, :cond_13

    .line 284244
    invoke-static {}, LX/1cy;->k()LX/0YU;

    move-result-object v3

    iput-object v3, v2, LX/1cn;->d:LX/0YU;

    .line 284245
    :cond_13
    iget-object v3, v2, LX/1cn;->c:LX/0YU;

    iget-object v6, v2, LX/1cn;->d:LX/0YU;

    invoke-static {v1, v3, v6}, LX/1ct;->a(ILX/0YU;LX/0YU;)V

    .line 284246
    :cond_14
    iget-object v3, v2, LX/1cn;->c:LX/0YU;

    iget-object v6, v2, LX/1cn;->d:LX/0YU;

    invoke-static {v0, v1, v3, v6}, LX/1ct;->a(IILX/0YU;LX/0YU;)V

    goto :goto_4
.end method

.method public static c(Landroid/view/View;)LX/1dd;
    .locals 1

    .prologue
    .line 284127
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284128
    check-cast p0, LX/1cn;

    .line 284129
    iget-object v0, p0, LX/1cn;->z:LX/1dd;

    move-object v0, v0

    .line 284130
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d000b

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dd;

    goto :goto_0
.end method

.method private static c(LX/1cv;)V
    .locals 5

    .prologue
    .line 284486
    iget-object v0, p0, LX/1cv;->a:LX/1X1;

    move-object v1, v0

    .line 284487
    invoke-static {v1}, LX/1X1;->d(LX/1X1;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 284488
    :cond_0
    :goto_0
    return-void

    .line 284489
    :cond_1
    iget-object v0, p0, LX/1cv;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 284490
    check-cast v0, Landroid/view/View;

    .line 284491
    iget-object v2, p0, LX/1cv;->g:LX/1dQ;

    move-object v2, v2

    .line 284492
    if-eqz v2, :cond_2

    .line 284493
    invoke-static {v0}, LX/1dF;->a(Landroid/view/View;)LX/1dM;

    move-result-object v2

    .line 284494
    if-eqz v2, :cond_2

    .line 284495
    const/4 v3, 0x0

    .line 284496
    iput-object v3, v2, LX/1dM;->a:LX/1dQ;

    .line 284497
    :cond_2
    iget-object v2, p0, LX/1cv;->h:LX/1dQ;

    move-object v2, v2

    .line 284498
    if-eqz v2, :cond_3

    .line 284499
    invoke-static {v0}, LX/1dF;->b(Landroid/view/View;)LX/1dR;

    move-result-object v2

    .line 284500
    if-eqz v2, :cond_3

    .line 284501
    const/4 v3, 0x0

    .line 284502
    iput-object v3, v2, LX/1dR;->a:LX/1dQ;

    .line 284503
    :cond_3
    iget v2, p0, LX/1cv;->u:I

    move v2, v2

    .line 284504
    and-int/lit8 v3, v2, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_8

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 284505
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 284506
    iget v2, p0, LX/1cv;->u:I

    move v2, v2

    .line 284507
    and-int/lit8 v3, v2, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_9

    const/4 v3, 0x1

    :goto_2
    move v2, v3

    .line 284508
    invoke-virtual {v0, v2}, Landroid/view/View;->setLongClickable(Z)V

    .line 284509
    invoke-static {v0, p0}, LX/1dF;->f(Landroid/view/View;LX/1cv;)V

    .line 284510
    iget-object v2, p0, LX/1cv;->i:LX/1dQ;

    move-object v2, v2

    .line 284511
    if-eqz v2, :cond_4

    .line 284512
    invoke-static {v0}, LX/1dF;->c(Landroid/view/View;)LX/1dd;

    move-result-object v2

    .line 284513
    if-eqz v2, :cond_4

    .line 284514
    const/4 v3, 0x0

    .line 284515
    iput-object v3, v2, LX/1dd;->a:LX/1dQ;

    .line 284516
    :cond_4
    invoke-static {v0}, LX/1dF;->h(Landroid/view/View;)V

    .line 284517
    invoke-static {v0, p0}, LX/1dF;->d(Landroid/view/View;LX/1cv;)V

    .line 284518
    iget-object v2, p0, LX/1cv;->d:Ljava/lang/CharSequence;

    move-object v2, v2

    .line 284519
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 284520
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 284521
    :cond_5
    iget v2, p0, LX/1cv;->s:I

    move v2, v2

    .line 284522
    if-eqz v2, :cond_6

    .line 284523
    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/0vv;->d(Landroid/view/View;I)V

    .line 284524
    :cond_6
    invoke-static {v0}, LX/1dF;->d(Landroid/view/View;)V

    .line 284525
    instance-of v2, p0, LX/1dJ;

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/1X1;->b(LX/1X1;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 284526
    check-cast p0, LX/1dJ;

    .line 284527
    const/4 v2, 0x0

    .line 284528
    invoke-virtual {p0}, LX/1dJ;->D()Z

    move-result v1

    if-nez v1, :cond_a

    .line 284529
    :goto_3
    iget-object v1, p0, LX/1dJ;->b:LX/1dc;

    move-object v2, v1

    .line 284530
    if-eqz v2, :cond_7

    .line 284531
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, LX/1De;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v1, v3, v2}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 284532
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1dF;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 284533
    :cond_7
    invoke-static {v0, p0}, LX/1dF;->f(Landroid/view/View;LX/1dJ;)V

    .line 284534
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-ge v1, v2, :cond_b

    .line 284535
    :goto_4
    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_1

    :cond_9
    const/4 v3, 0x0

    goto :goto_2

    .line 284536
    :cond_a
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_3

    .line 284537
    :cond_b
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    goto :goto_4
.end method

.method private static c(LX/1dQ;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 284538
    if-eqz p0, :cond_1

    .line 284539
    invoke-static {p1}, LX/1dF;->c(Landroid/view/View;)LX/1dd;

    move-result-object v0

    .line 284540
    if-nez v0, :cond_0

    .line 284541
    new-instance v0, LX/1dd;

    invoke-direct {v0}, LX/1dd;-><init>()V

    .line 284542
    instance-of v1, p1, LX/1cn;

    if-eqz v1, :cond_2

    .line 284543
    check-cast p1, LX/1cn;

    .line 284544
    iput-object v0, p1, LX/1cn;->z:LX/1dd;

    .line 284545
    invoke-virtual {p1, v0}, LX/1cn;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 284546
    :cond_0
    :goto_0
    iput-object p0, v0, LX/1dd;->a:LX/1dQ;

    .line 284547
    :cond_1
    return-void

    .line 284548
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 284549
    const v1, 0x7f0d000b

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private static c(Landroid/view/View;LX/1cv;)V
    .locals 5

    .prologue
    .line 284550
    iget-object v0, p1, LX/1cv;->f:Landroid/util/SparseArray;

    move-object v1, v0

    .line 284551
    if-nez v1, :cond_1

    .line 284552
    :cond_0
    :goto_0
    return-void

    .line 284553
    :cond_1
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_2

    .line 284554
    check-cast p0, LX/1cn;

    .line 284555
    iput-object v1, p0, LX/1cn;->k:Landroid/util/SparseArray;

    .line 284556
    goto :goto_0

    .line 284557
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_0

    .line 284558
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 284559
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static d(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284560
    instance-of v0, p0, LX/1cn;

    if-nez v0, :cond_0

    .line 284561
    :goto_0
    return-void

    .line 284562
    :cond_0
    check-cast p0, LX/1cn;

    .line 284563
    iput-object v1, p0, LX/1cn;->E:LX/1dQ;

    .line 284564
    iput-object v1, p0, LX/1cn;->C:LX/1dQ;

    .line 284565
    iput-object v1, p0, LX/1cn;->D:LX/1dQ;

    .line 284566
    iput-object v1, p0, LX/1cn;->G:LX/1dQ;

    .line 284567
    iput-object v1, p0, LX/1cn;->B:LX/1dQ;

    .line 284568
    iput-object v1, p0, LX/1cn;->F:LX/1dQ;

    .line 284569
    iput-object v1, p0, LX/1cn;->H:LX/1dQ;

    .line 284570
    iput-object v1, p0, LX/1cn;->I:LX/1dQ;

    .line 284571
    goto :goto_0
.end method

.method private static d(Landroid/view/View;LX/1cv;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 284572
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_1

    .line 284573
    check-cast p0, LX/1cn;

    .line 284574
    iput-object v4, p0, LX/1cn;->k:Landroid/util/SparseArray;

    .line 284575
    :cond_0
    return-void

    .line 284576
    :cond_1
    iget-object v0, p1, LX/1cv;->f:Landroid/util/SparseArray;

    move-object v1, v0

    .line 284577
    if-eqz v1, :cond_0

    .line 284578
    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 284579
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 284580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static d(LX/1dF;LX/1dN;Landroid/graphics/Rect;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 284581
    iget-object v0, p0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 284582
    :goto_0
    return v0

    .line 284583
    :cond_0
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ne v0, v2, :cond_1

    iget v0, p2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-eq v0, v2, :cond_2

    :cond_1
    move v0, v1

    .line 284584
    goto :goto_0

    .line 284585
    :cond_2
    iget-object v0, p1, LX/1dN;->l:Ljava/util/ArrayList;

    move-object v2, v0

    .line 284586
    iget-object v0, p1, LX/1dN;->m:Ljava/util/ArrayList;

    move-object v3, v0

    .line 284587
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v0

    .line 284588
    iget v4, p2, Landroid/graphics/Rect;->top:I

    if-gtz v4, :cond_3

    iget-object v4, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-lez v4, :cond_6

    .line 284589
    :cond_3
    iget v4, p0, LX/1dF;->p:I

    if-ge v4, v0, :cond_4

    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v0, p0, LX/1dF;->p:I

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284590
    iget-object p1, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, p1

    .line 284591
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v0, :cond_5

    :cond_4
    iget v0, p0, LX/1dF;->p:I

    if-lez v0, :cond_6

    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v0, p0, LX/1dF;->p:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284592
    iget-object v3, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v3

    .line 284593
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v0, :cond_6

    :cond_5
    move v0, v1

    .line 284594
    goto :goto_0

    .line 284595
    :cond_6
    iget-object v0, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getHeight()I

    move-result v0

    .line 284596
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    if-lt v3, v0, :cond_7

    iget-object v3, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v0, :cond_a

    .line 284597
    :cond_7
    iget v0, p0, LX/1dF;->o:I

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_8

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v0, p0, LX/1dF;->o:I

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284598
    iget-object v4, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v4

    .line 284599
    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gt v3, v0, :cond_9

    :cond_8
    iget v0, p0, LX/1dF;->o:I

    if-lez v0, :cond_a

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v0, p0, LX/1dF;->o:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284600
    iget-object v2, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v2

    .line 284601
    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gt v3, v0, :cond_a

    :cond_9
    move v0, v1

    .line 284602
    goto/16 :goto_0

    .line 284603
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static e(Landroid/view/View;LX/1dJ;)V
    .locals 3

    .prologue
    .line 284480
    iget-object v0, p1, LX/1dJ;->c:LX/1dc;

    move-object v1, v0

    .line 284481
    if-eqz v1, :cond_1

    .line 284482
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v0, v2, :cond_0

    .line 284483
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MountState has a ViewMountItem with foreground however the current Android version doesn\'t support foreground on Views"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284484
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/1De;

    invoke-static {v0, v1}, LX/1dc;->a(LX/1De;LX/1dc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 284485
    :cond_1
    return-void
.end method

.method public static e(LX/1dF;LX/1dN;Landroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 284604
    iget-object v1, p0, LX/1dF;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 284605
    :cond_0
    :goto_0
    return v0

    .line 284606
    :cond_1
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ne v1, v2, :cond_0

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-ne v1, v2, :cond_0

    .line 284607
    iget-object v0, p1, LX/1dN;->l:Ljava/util/ArrayList;

    move-object v1, v0

    .line 284608
    iget-object v0, p1, LX/1dN;->m:Ljava/util/ArrayList;

    move-object v2, v0

    .line 284609
    invoke-virtual {p1}, LX/1dN;->c()I

    move-result v3

    .line 284610
    iget v0, p2, Landroid/graphics/Rect;->top:I

    if-gtz v0, :cond_2

    iget-object v0, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_4

    .line 284611
    :cond_2
    :goto_1
    iget v0, p0, LX/1dF;->p:I

    if-ge v0, v3, :cond_3

    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v0, p0, LX/1dF;->p:I

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284612
    iget-object v5, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v5

    .line 284613
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-lt v4, v0, :cond_3

    .line 284614
    iget v0, p0, LX/1dF;->p:I

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284615
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v4, v6

    .line 284616
    iget-object v0, p0, LX/1dF;->i:LX/1De;

    invoke-virtual {p1, v4, v5}, LX/1dN;->a(J)I

    move-result v4

    iget-object v5, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p0, v0, v4, v5}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    .line 284617
    iget v0, p0, LX/1dF;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1dF;->p:I

    goto :goto_1

    .line 284618
    :cond_3
    :goto_2
    iget v0, p0, LX/1dF;->p:I

    if-lez v0, :cond_4

    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v0, p0, LX/1dF;->p:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284619
    iget-object v5, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v5

    .line 284620
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v0, :cond_4

    .line 284621
    iget v0, p0, LX/1dF;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1dF;->p:I

    .line 284622
    iget v0, p0, LX/1dF;->p:I

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284623
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v4, v6

    .line 284624
    invoke-virtual {p1, v4, v5}, LX/1dN;->a(J)I

    move-result v4

    .line 284625
    invoke-static {p0, v4}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v4

    if-nez v4, :cond_3

    .line 284626
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v4, v6

    .line 284627
    invoke-virtual {p1, v4, v5}, LX/1dN;->a(J)I

    move-result v4

    invoke-static {p0, v4, v0, p1}, LX/1dF;->a(LX/1dF;ILX/1dK;LX/1dN;)V

    goto :goto_2

    .line 284628
    :cond_4
    iget-object v0, p0, LX/1dF;->j:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getHeight()I

    move-result v0

    .line 284629
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    if-lt v2, v0, :cond_5

    iget-object v2, p0, LX/1dF;->k:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-ge v2, v0, :cond_8

    .line 284630
    :cond_5
    :goto_3
    iget v0, p0, LX/1dF;->o:I

    if-ge v0, v3, :cond_7

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v0, p0, LX/1dF;->o:I

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284631
    iget-object v4, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v4

    .line 284632
    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-le v2, v0, :cond_7

    .line 284633
    iget v0, p0, LX/1dF;->o:I

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284634
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v4, v6

    .line 284635
    invoke-virtual {p1, v4, v5}, LX/1dN;->a(J)I

    move-result v2

    .line 284636
    invoke-static {p0, v2}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v2

    if-nez v2, :cond_6

    .line 284637
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v4, v6

    .line 284638
    invoke-virtual {p1, v4, v5}, LX/1dN;->a(J)I

    move-result v2

    invoke-static {p0, v2, v0, p1}, LX/1dF;->a(LX/1dF;ILX/1dK;LX/1dN;)V

    .line 284639
    :cond_6
    iget v0, p0, LX/1dF;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/1dF;->o:I

    goto :goto_3

    .line 284640
    :cond_7
    :goto_4
    iget v0, p0, LX/1dF;->o:I

    if-lez v0, :cond_8

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v0, p0, LX/1dF;->o:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284641
    iget-object v3, v0, LX/1dK;->c:Landroid/graphics/Rect;

    move-object v0, v3

    .line 284642
    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gt v2, v0, :cond_8

    .line 284643
    iget v0, p0, LX/1dF;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/1dF;->o:I

    .line 284644
    iget v0, p0, LX/1dF;->o:I

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dK;

    .line 284645
    iget-wide v6, v0, LX/1dK;->a:J

    move-wide v2, v6

    .line 284646
    iget-object v0, p0, LX/1dF;->i:LX/1De;

    invoke-virtual {p1, v2, v3}, LX/1dN;->a(J)I

    move-result v2

    iget-object v3, p0, LX/1dF;->f:LX/0tf;

    invoke-static {p0, v0, v2, v3}, LX/1dF;->a(LX/1dF;LX/1De;ILX/0tf;)V

    goto :goto_4

    .line 284647
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private static f(Landroid/view/View;LX/1cv;)V
    .locals 2

    .prologue
    .line 284648
    iget v0, p1, LX/1cv;->u:I

    move v0, v0

    .line 284649
    and-int/lit16 v1, v0, 0x400

    const/16 p1, 0x400

    if-ne v1, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 284650
    invoke-virtual {p0, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 284651
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static f(Landroid/view/View;LX/1dJ;)V
    .locals 3

    .prologue
    .line 284652
    iget-object v0, p1, LX/1dJ;->c:LX/1dc;

    move-object v1, v0

    .line 284653
    if-eqz v1, :cond_1

    .line 284654
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v0, v2, :cond_0

    .line 284655
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MountState has a ViewMountItem with foreground however the current Android version doesn\'t support foreground on Views"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284656
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/1De;

    invoke-virtual {p0}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2, v1}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 284657
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 284658
    :cond_1
    return-void
.end method

.method private static h(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 284659
    instance-of v0, p0, LX/1cn;

    if-eqz v0, :cond_0

    .line 284660
    check-cast p0, LX/1cn;

    .line 284661
    iput-object v1, p0, LX/1cn;->j:Ljava/lang/Object;

    .line 284662
    :goto_0
    return-void

    .line 284663
    :cond_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 284664
    iget-object v0, p0, LX/1dF;->d:[J

    if-nez v0, :cond_0

    .line 284665
    :goto_0
    return-void

    .line 284666
    :cond_0
    iget-object v0, p0, LX/1dF;->d:[J

    array-length v2, v0

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    .line 284667
    invoke-static {p0, v0}, LX/1dF;->a(LX/1dF;I)LX/1cv;

    move-result-object v3

    .line 284668
    if-eqz v3, :cond_1

    .line 284669
    iget-boolean v4, v3, LX/1cv;->r:Z

    move v4, v4

    .line 284670
    if-eqz v4, :cond_1

    .line 284671
    iget-object v4, v3, LX/1cv;->a:LX/1X1;

    move-object v4, v4

    .line 284672
    iget-object v5, v4, LX/1X1;->e:LX/1S3;

    move-object v5, v5

    .line 284673
    iget-object v6, p0, LX/1dF;->i:LX/1De;

    .line 284674
    iget-object v7, v3, LX/1cv;->b:Ljava/lang/Object;

    move-object v7, v7

    .line 284675
    invoke-virtual {v5, v6, v7, v4}, LX/1S3;->c(LX/1De;Ljava/lang/Object;LX/1X1;)V

    .line 284676
    iput-boolean v1, v3, LX/1cv;->r:Z

    .line 284677
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 284678
    :cond_2
    iget-object v0, p0, LX/1dF;->b:LX/0tf;

    invoke-virtual {v0}, LX/0tf;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 284679
    iget-object v0, p0, LX/1dF;->b:LX/0tf;

    invoke-virtual {v0, v1}, LX/0tf;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dX;

    .line 284680
    iget-object v2, v0, LX/1dX;->b:LX/1dQ;

    move-object v2, v2

    .line 284681
    if-eqz v2, :cond_3

    .line 284682
    invoke-static {v2}, LX/1dY;->d(LX/1dQ;)V

    .line 284683
    :cond_3
    iget-object v2, p0, LX/1dF;->b:LX/0tf;

    invoke-virtual {v2, v1}, LX/0tf;->a(I)V

    .line 284684
    invoke-static {v0}, LX/1cy;->a(LX/1dX;)V

    .line 284685
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 284686
    :cond_4
    goto :goto_0
.end method
