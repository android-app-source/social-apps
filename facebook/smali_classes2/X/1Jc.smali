.class public LX/1Jc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/1JZ;

.field public final c:Ljava/lang/Object;

.field private final d:Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mUiLock"
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mUiLock"
    .end annotation
.end field

.field public volatile g:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/1JZ;)V
    .locals 3
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1JZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0x28

    const/4 v1, 0x0

    .line 230175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230176
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/1Jc;->c:Ljava/lang/Object;

    .line 230177
    new-instance v0, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;-><init>(LX/1Jc;)V

    iput-object v0, p0, LX/1Jc;->d:Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;

    .line 230178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1Jc;->e:Ljava/util/List;

    .line 230179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/1Jc;->f:Ljava/util/List;

    .line 230180
    iput-boolean v1, p0, LX/1Jc;->g:Z

    .line 230181
    iput-object p1, p0, LX/1Jc;->a:Ljava/util/concurrent/ExecutorService;

    .line 230182
    iput-object p2, p0, LX/1Jc;->b:LX/1JZ;

    .line 230183
    return-void
.end method

.method public static c(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 230184
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 230185
    invoke-interface {p1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230186
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/prefetch/PrefetchParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230187
    iget-object v1, p0, LX/1Jc;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 230188
    :try_start_0
    iget-object v0, p0, LX/1Jc;->e:Ljava/util/List;

    invoke-static {p1, v0}, LX/1Jc;->c(Ljava/util/List;Ljava/util/List;)V

    .line 230189
    iget-object v0, p0, LX/1Jc;->f:Ljava/util/List;

    invoke-static {p2, v0}, LX/1Jc;->c(Ljava/util/List;Ljava/util/List;)V

    .line 230190
    iget-boolean v0, p0, LX/1Jc;->g:Z

    if-eqz v0, :cond_0

    .line 230191
    monitor-exit v1

    .line 230192
    :goto_0
    return-void

    .line 230193
    :cond_0
    iget-object v0, p0, LX/1Jc;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, LX/1Jc;->d:Lcom/facebook/photos/prefetch/BackgroundHandoffHelper$BackgroundRunnable;

    const v3, -0x2d90d00

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 230194
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1Jc;->g:Z

    .line 230195
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
