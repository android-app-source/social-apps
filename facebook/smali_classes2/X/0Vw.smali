.class public LX/0Vw;
.super LX/0Vx;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/0Vw;


# direct methods
.method public constructor <init>(LX/2WA;)V
    .locals 0
    .param p1    # LX/2WA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 75175
    invoke-direct {p0, p1}, LX/0Vx;-><init>(LX/2WA;)V

    .line 75176
    return-void
.end method

.method public static a(LX/0QB;)LX/0Vw;
    .locals 4

    .prologue
    .line 75161
    sget-object v0, LX/0Vw;->b:LX/0Vw;

    if-nez v0, :cond_1

    .line 75162
    const-class v1, LX/0Vw;

    monitor-enter v1

    .line 75163
    :try_start_0
    sget-object v0, LX/0Vw;->b:LX/0Vw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 75164
    if-eqz v2, :cond_0

    .line 75165
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 75166
    new-instance p0, LX/0Vw;

    invoke-static {v0}, LX/0Ws;->a(LX/0QB;)LX/2WA;

    move-result-object v3

    check-cast v3, LX/2WA;

    invoke-direct {p0, v3}, LX/0Vw;-><init>(LX/2WA;)V

    .line 75167
    move-object v0, p0

    .line 75168
    sput-object v0, LX/0Vw;->b:LX/0Vw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75169
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 75170
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75171
    :cond_1
    sget-object v0, LX/0Vw;->b:LX/0Vw;

    return-object v0

    .line 75172
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 75173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75174
    const-string v0, "analytic_counters"

    return-object v0
.end method
