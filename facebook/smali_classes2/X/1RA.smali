.class public LX/1RA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "LX/0fn;",
        "Lcom/facebook/feed/rows/core/HasSize;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<-TP;*-TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/03V;

.field private final g:LX/1Dc;

.field private final h:LX/0Sh;

.field private final i:LX/1Db;

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Rk",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1RZ",
            "<****>;>;"
        }
    .end annotation
.end field

.field private final l:LX/1Qx;

.field private final m:Z

.field private final n:LX/23K;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0Yi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245567
    const-class v0, LX/1RA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1RA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;LX/1PW;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/03V;LX/1Dc;LX/0Sh;LX/1Db;LX/1Qx;ZLX/23K;LX/0Yi;)V
    .locals 1
    .param p10    # LX/23K;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TE;",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<-TP;-TE;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Dc;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1Db;",
            "LX/1Qx;",
            "Z",
            "LX/23K;",
            "LX/0Yi;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1RA;->j:Ljava/util/List;

    .line 245693
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    .line 245694
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    :goto_1
    iput-object p1, p0, LX/1RA;->c:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 245695
    iput-object p2, p0, LX/1RA;->d:LX/1PW;

    .line 245696
    instance-of v0, p3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v0, :cond_2

    .line 245697
    new-instance v0, Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;

    check-cast p3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-direct {v0, p3}, Lcom/facebook/feed/rows/core/parts/SingleChildMultiRowGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;)V

    move-object p3, v0

    .line 245698
    :goto_2
    move-object v0, p3

    .line 245699
    iput-object v0, p0, LX/1RA;->e:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    .line 245700
    iput-object p4, p0, LX/1RA;->f:LX/03V;

    .line 245701
    iput-object p5, p0, LX/1RA;->g:LX/1Dc;

    .line 245702
    iput-object p6, p0, LX/1RA;->h:LX/0Sh;

    .line 245703
    iput-object p7, p0, LX/1RA;->i:LX/1Db;

    .line 245704
    iput-boolean p9, p0, LX/1RA;->m:Z

    .line 245705
    iput-object p10, p0, LX/1RA;->n:LX/23K;

    .line 245706
    iput-object p8, p0, LX/1RA;->l:LX/1Qx;

    .line 245707
    iput-object p11, p0, LX/1RA;->o:LX/0Yi;

    .line 245708
    return-void

    :cond_0
    move-object v0, p1

    .line 245709
    goto :goto_0

    .line 245710
    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    check-cast p3, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    goto :goto_2
.end method

.method private a(Z)V
    .locals 9

    .prologue
    .line 245670
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0}, LX/1RA;->a()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 245671
    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    instance-of v0, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    .line 245672
    :goto_1
    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    .line 245673
    iget-object v1, p0, LX/1RA;->b:Ljava/lang/Object;

    instance-of v1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_0

    .line 245674
    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 245675
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 245676
    :cond_0
    instance-of v1, v0, LX/0jW;

    if-eqz v1, :cond_3

    .line 245677
    check-cast v0, LX/0jW;

    .line 245678
    :goto_2
    move-object v7, v0

    .line 245679
    iget-object v0, p0, LX/1RA;->j:Ljava/util/List;

    new-instance v1, LX/1Rk;

    iget-object v6, p0, LX/1RA;->c:Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v2, p0

    move v8, p1

    invoke-direct/range {v1 .. v8}, LX/1Rk;-><init>(LX/1RA;IJLcom/facebook/graphql/model/GraphQLFeedUnitEdge;LX/0jW;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245680
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 245681
    :cond_1
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 245682
    :cond_2
    return-void

    .line 245683
    :cond_3
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 245684
    iget-object v1, v0, LX/1RZ;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 245685
    instance-of v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_4

    .line 245686
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 245687
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 245688
    :cond_4
    instance-of v1, v0, LX/0jW;

    if-eqz v1, :cond_5

    .line 245689
    check-cast v0, LX/0jW;

    goto :goto_2

    .line 245690
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(IZ)Z
    .locals 4

    .prologue
    .line 245660
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245661
    iget-object v0, p0, LX/1RA;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 245662
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 245663
    iget-boolean v1, v0, LX/1RZ;->g:Z

    move v1, v1

    .line 245664
    if-eqz v1, :cond_0

    .line 245665
    const/4 v0, 0x0

    .line 245666
    :goto_0
    return v0

    .line 245667
    :cond_0
    iget-object v2, p0, LX/1RA;->g:LX/1Dc;

    if-eqz p2, :cond_1

    sget-object v1, LX/1aA;->PREPARE_ASYNC:LX/1aA;

    :goto_1
    new-instance v3, LX/1aB;

    invoke-direct {v3, p0, p1, p2, v0}, LX/1aB;-><init>(LX/1RA;IZLX/1RZ;)V

    invoke-virtual {v2, v1, v3}, LX/1Dc;->a(LX/1aA;Ljava/util/concurrent/Callable;)V

    .line 245668
    const/4 v0, 0x1

    goto :goto_0

    .line 245669
    :cond_1
    sget-object v1, LX/1aA;->PREPARE:LX/1aA;

    goto :goto_1
.end method

.method public static e(LX/1RA;)V
    .locals 15

    .prologue
    .line 245612
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    if-eqz v0, :cond_0

    .line 245613
    :goto_0
    return-void

    .line 245614
    :cond_0
    const-string v0, "FeedUnitAdapter.init"

    const v1, 0x22345d92

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 245615
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 245616
    :try_start_0
    iget-object v0, p0, LX/1RA;->n:LX/23K;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/1RA;->n:LX/23K;

    iget-object v1, p0, LX/1RA;->e:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v4, p0, LX/1RA;->b:Ljava/lang/Object;

    iget-object v5, p0, LX/1RA;->d:LX/1PW;

    iget-object v6, p0, LX/1RA;->l:LX/1Qx;

    const/4 v14, 0x0

    const/4 v11, 0x0

    .line 245617
    iput-object v5, v0, LX/23K;->h:LX/1PW;

    .line 245618
    iput-object v6, v0, LX/23K;->i:LX/1Qx;

    .line 245619
    invoke-static {v1, v4, v5, v6}, LX/1RD;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/0Px;

    move-result-object v7

    iput-object v7, v0, LX/23K;->e:LX/0Px;

    .line 245620
    iget-object v7, v0, LX/23K;->e:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v12

    move v10, v11

    :goto_1
    if-ge v10, v12, :cond_5

    iget-object v7, v0, LX/23K;->e:LX/0Px;

    invoke-virtual {v7, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1RZ;

    .line 245621
    iget-object v8, v7, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v9, v8

    .line 245622
    instance-of v8, v9, LX/1Rj;

    if-eqz v8, :cond_1

    move-object v8, v9

    check-cast v8, LX/1Rj;

    iget-object v13, v0, LX/23K;->h:LX/1PW;

    invoke-interface {v8, v13}, LX/1Rj;->a(LX/1PW;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 245623
    :cond_1
    invoke-static {v0}, LX/23K;->a(LX/23K;)V

    .line 245624
    iget-object v8, v0, LX/23K;->d:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245625
    :goto_2
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    goto :goto_1

    .line 245626
    :cond_2
    check-cast v9, LX/1Rj;

    .line 245627
    iget-object v8, v7, LX/1RZ;->d:Ljava/lang/Object;

    move-object v8, v8

    .line 245628
    const/4 v13, 0x0

    .line 245629
    iget-object v1, v0, LX/23K;->f:LX/0jW;

    if-nez v1, :cond_7

    .line 245630
    :cond_3
    :goto_3
    move v13, v13

    .line 245631
    if-nez v13, :cond_4

    .line 245632
    invoke-static {v0}, LX/23K;->a(LX/23K;)V

    .line 245633
    iget-object v13, v0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v13, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245634
    invoke-interface {v9, v8}, LX/1Rj;->b(Ljava/lang/Object;)LX/0jW;

    move-result-object v7

    iput-object v7, v0, LX/23K;->f:LX/0jW;

    goto :goto_2

    .line 245635
    :cond_4
    iget-object v8, v0, LX/23K;->c:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 245636
    :cond_5
    invoke-static {v0}, LX/23K;->a(LX/23K;)V

    .line 245637
    iput-object v14, v0, LX/23K;->h:LX/1PW;

    .line 245638
    iput-object v14, v0, LX/23K;->i:LX/1Qx;

    .line 245639
    iput v11, v0, LX/23K;->g:I

    .line 245640
    iget-object v7, v0, LX/23K;->d:Ljava/util/List;

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 245641
    iget-object v8, v0, LX/23K;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 245642
    move-object v0, v7

    .line 245643
    :goto_4
    iput-object v0, p0, LX/1RA;->k:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245644
    const v0, 0x4a2046b2    # 2625964.5f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 245645
    iget-boolean v0, p0, LX/1RA;->m:Z

    invoke-direct {p0, v0}, LX/1RA;->a(Z)V

    .line 245646
    iget-object v0, p0, LX/1RA;->o:LX/0Yi;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 245647
    iget-wide v7, v0, LX/0Yi;->G:J

    add-long/2addr v7, v2

    iput-wide v7, v0, LX/0Yi;->G:J

    .line 245648
    goto/16 :goto_0

    .line 245649
    :cond_6
    :try_start_1
    iget-object v0, p0, LX/1RA;->e:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v1, p0, LX/1RA;->b:Ljava/lang/Object;

    iget-object v4, p0, LX/1RA;->d:LX/1PW;

    iget-object v5, p0, LX/1RA;->l:LX/1Qx;

    invoke-static {v0, v1, v4, v5}, LX/1RD;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1PW;LX/1Qx;)LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_4

    .line 245650
    :catchall_0
    move-exception v0

    const v1, 0x4b422b0c    # 1.2725004E7f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 245651
    :cond_7
    iget-object v1, v0, LX/23K;->f:LX/0jW;

    invoke-interface {v1}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    .line 245652
    if-eqz v1, :cond_3

    .line 245653
    invoke-interface {v9, v8}, LX/1Rj;->b(Ljava/lang/Object;)LX/0jW;

    move-result-object v4

    .line 245654
    if-eqz v4, :cond_3

    .line 245655
    iget-object v5, v0, LX/23K;->f:LX/0jW;

    if-ne v4, v5, :cond_8

    .line 245656
    const/4 v13, 0x1

    goto :goto_3

    .line 245657
    :cond_8
    invoke-interface {v4}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v4

    .line 245658
    if-eqz v4, :cond_3

    .line 245659
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    goto :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 245610
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245611
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1Rb;
    .locals 1

    .prologue
    .line 245606
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245607
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 245608
    iget-object p0, v0, LX/1RZ;->c:LX/1Rb;

    move-object v0, p0

    .line 245609
    return-object v0
.end method

.method public final a(LX/5Mj;)V
    .locals 4

    .prologue
    .line 245597
    iget-object v0, p1, LX/5Mj;->e:Ljava/io/PrintWriter;

    move-object v1, v0

    .line 245598
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DUMPING SECTIONS for adapter with "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/1RA;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " parts"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245599
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "story: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/1RA;->b:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245600
    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    instance-of v0, v0, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    .line 245601
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "debug info: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/1RA;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 245602
    :cond_0
    iget-object v0, p0, LX/1RA;->e:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v2, p0, LX/1RA;->b:Ljava/lang/Object;

    iget-object v3, p0, LX/1RA;->d:LX/1PW;

    .line 245603
    new-instance p0, LX/6Vl;

    invoke-direct {p0, v1, v3}, LX/6Vl;-><init>(Ljava/io/PrintWriter;LX/1PW;)V

    .line 245604
    invoke-virtual {p0, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z

    .line 245605
    return-void
.end method

.method public final b(I)LX/1Cz;
    .locals 1

    .prologue
    .line 245595
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245596
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    invoke-virtual {v0}, LX/1RZ;->b()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)LX/1Ra;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1Ra",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245593
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245594
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ra;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 245587
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    if-nez v0, :cond_1

    .line 245588
    :cond_0
    return-void

    .line 245589
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 245590
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ra;

    .line 245591
    invoke-static {v0}, LX/1aL;->a(LX/1Ra;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 245592
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 245582
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245583
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 245584
    invoke-virtual {p0, v0}, LX/1RA;->i(I)Z

    .line 245585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245586
    :cond_0
    return-void
.end method

.method public final e(I)Z
    .locals 1

    .prologue
    .line 245581
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1RA;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 245580
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/1RA;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final g(I)LX/1Rk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1Rk",
            "<TP;>;"
        }
    .end annotation

    .prologue
    .line 245578
    invoke-static {p0}, LX/1RA;->e(LX/1RA;)V

    .line 245579
    iget-object v0, p0, LX/1RA;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    return-object v0
.end method

.method public final i(I)Z
    .locals 2

    .prologue
    .line 245568
    iget-object v0, p0, LX/1RA;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 245569
    iget-object v0, p0, LX/1RA;->k:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RZ;

    .line 245570
    iget-boolean v1, v0, LX/1RZ;->g:Z

    move v1, v1

    .line 245571
    if-eqz v1, :cond_0

    .line 245572
    invoke-static {v0}, LX/1aL;->a(LX/1Ra;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 245573
    if-eqz v1, :cond_1

    .line 245574
    :cond_0
    const/4 v0, 0x0

    .line 245575
    :goto_1
    return v0

    .line 245576
    :cond_1
    iget-object v1, p0, LX/1RA;->d:LX/1PW;

    invoke-virtual {v0, v1}, LX/1Ra;->b(LX/1PW;)V

    .line 245577
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
