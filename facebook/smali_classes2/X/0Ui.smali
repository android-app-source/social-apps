.class public LX/0Ui;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[LX/03R;

.field public final b:[LX/03R;

.field public final c:[LX/03R;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 66766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66767
    invoke-static {p1}, LX/0Ui;->g(I)[LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/0Ui;->a:[LX/03R;

    .line 66768
    invoke-static {p1}, LX/0Ui;->g(I)[LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/0Ui;->b:[LX/03R;

    .line 66769
    invoke-static {p1}, LX/0Ui;->g(I)[LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/0Ui;->c:[LX/03R;

    .line 66770
    return-void
.end method

.method private static g(I)[LX/03R;
    .locals 3

    .prologue
    .line 66761
    new-array v1, p0, [LX/03R;

    .line 66762
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    .line 66763
    sget-object v2, LX/03R;->UNSET:LX/03R;

    aput-object v2, v1, v0

    .line 66764
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66765
    :cond_0
    return-object v1
.end method

.method private h(I)V
    .locals 3

    .prologue
    .line 66756
    iget-object v0, p0, LX/0Ui;->c:[LX/03R;

    iget-object v1, p0, LX/0Ui;->a:[LX/03R;

    aget-object v1, v1, p1

    iget-object v2, p0, LX/0Ui;->b:[LX/03R;

    aget-object v2, v2, p1

    .line 66757
    sget-object p0, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p0, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 66758
    :goto_0
    move-object v1, v1

    .line 66759
    aput-object v1, v0, p1

    .line 66760
    return-void

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/03R;
    .locals 1

    .prologue
    .line 66755
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->c:[LX/03R;

    aget-object v0, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(IZ)V
    .locals 2

    .prologue
    .line 66750
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->a:[LX/03R;

    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    aput-object v1, v0, p1

    .line 66751
    invoke-direct {p0, p1}, LX/0Ui;->h(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66752
    monitor-exit p0

    return-void

    .line 66753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)LX/03R;
    .locals 1

    .prologue
    .line 66754
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->a:[LX/03R;

    aget-object v0, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(IZ)V
    .locals 2

    .prologue
    .line 66746
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->b:[LX/03R;

    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    aput-object v1, v0, p1

    .line 66747
    invoke-direct {p0, p1}, LX/0Ui;->h(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66748
    monitor-exit p0

    return-void

    .line 66749
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)LX/03R;
    .locals 1

    .prologue
    .line 66745
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->b:[LX/03R;

    aget-object v0, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 66731
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/0Ui;->a:[LX/03R;

    array-length v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 66732
    invoke-virtual {p0, v1}, LX/0Ui;->d(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 66733
    const/4 v0, 0x1

    .line 66734
    :cond_0
    monitor-exit p0

    return v0

    .line 66735
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(I)Z
    .locals 2

    .prologue
    .line 66744
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iget-object v1, p0, LX/0Ui;->c:[LX/03R;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e(I)V
    .locals 2

    .prologue
    .line 66740
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->a:[LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    aput-object v1, v0, p1

    .line 66741
    invoke-direct {p0, p1}, LX/0Ui;->h(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66742
    monitor-exit p0

    return-void

    .line 66743
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f(I)V
    .locals 2

    .prologue
    .line 66736
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/0Ui;->b:[LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    aput-object v1, v0, p1

    .line 66737
    invoke-direct {p0, p1}, LX/0Ui;->h(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66738
    monitor-exit p0

    return-void

    .line 66739
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
