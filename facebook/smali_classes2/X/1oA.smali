.class public final LX/1oA;
.super Landroid/graphics/Matrix;
.source ""


# instance fields
.field public a:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 318483
    invoke-direct {p0}, Landroid/graphics/Matrix;-><init>()V

    .line 318484
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView$ScaleType;II)LX/1oA;
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 318485
    if-nez p1, :cond_0

    .line 318486
    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    .line 318487
    :cond_0
    if-nez p0, :cond_1

    move-object v0, v1

    .line 318488
    :goto_0
    return-object v0

    .line 318489
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 318490
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 318491
    if-lez v3, :cond_2

    if-lez v5, :cond_2

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    if-eq v4, p1, :cond_2

    sget-object v4, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v4, p1, :cond_3

    :cond_2
    move-object v0, v1

    .line 318492
    goto :goto_0

    .line 318493
    :cond_3
    if-ne p2, v3, :cond_4

    if-ne p3, v5, :cond_4

    move-object v0, v1

    .line 318494
    goto :goto_0

    .line 318495
    :cond_4
    new-instance v4, LX/1oA;

    invoke-direct {v4}, LX/1oA;-><init>()V

    .line 318496
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne v1, p1, :cond_7

    .line 318497
    sub-int v1, p2, v3

    int-to-float v1, v1

    mul-float/2addr v1, v7

    invoke-static {v1}, LX/1n0;->a(F)I

    move-result v1

    int-to-float v1, v1

    sub-int v2, p3, v5

    int-to-float v2, v2

    mul-float/2addr v2, v7

    invoke-static {v2}, LX/1n0;->a(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v4, v1, v2}, LX/1oA;->setTranslate(FF)V

    .line 318498
    if-gt v3, p2, :cond_5

    if-le v5, p3, :cond_6

    :cond_5
    :goto_1
    iput-boolean v0, v4, LX/1oA;->a:Z

    :goto_2
    move-object v0, v4

    .line 318499
    goto :goto_0

    .line 318500
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 318501
    :cond_7
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    if-ne v1, p1, :cond_9

    .line 318502
    mul-int v1, v3, p3

    mul-int v6, p2, v5

    if-le v1, v6, :cond_8

    .line 318503
    int-to-float v1, p3

    int-to-float v5, v5

    div-float/2addr v1, v5

    .line 318504
    int-to-float v5, p2

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v3, v5, v3

    mul-float/2addr v3, v7

    .line 318505
    :goto_3
    invoke-virtual {v4, v1, v1}, LX/1oA;->setScale(FF)V

    .line 318506
    invoke-static {v3}, LX/1n0;->a(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v2}, LX/1n0;->a(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v4, v1, v2}, LX/1oA;->postTranslate(FF)Z

    .line 318507
    iput-boolean v0, v4, LX/1oA;->a:Z

    goto :goto_2

    .line 318508
    :cond_8
    int-to-float v1, p2

    int-to-float v3, v3

    div-float/2addr v1, v3

    .line 318509
    int-to-float v3, p3

    int-to-float v5, v5

    mul-float/2addr v5, v1

    sub-float/2addr v3, v5

    mul-float/2addr v3, v7

    move v8, v3

    move v3, v2

    move v2, v8

    goto :goto_3

    .line 318510
    :cond_9
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne v0, p1, :cond_b

    .line 318511
    if-gt v3, p2, :cond_a

    if-gt v5, p3, :cond_a

    .line 318512
    const/high16 v0, 0x3f800000    # 1.0f

    .line 318513
    :goto_4
    int-to-float v1, p2

    int-to-float v2, v3

    mul-float/2addr v2, v0

    sub-float/2addr v1, v2

    mul-float/2addr v1, v7

    invoke-static {v1}, LX/1n0;->a(F)I

    move-result v1

    int-to-float v1, v1

    .line 318514
    int-to-float v2, p3

    int-to-float v3, v5

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    mul-float/2addr v2, v7

    invoke-static {v2}, LX/1n0;->a(F)I

    move-result v2

    int-to-float v2, v2

    .line 318515
    invoke-virtual {v4, v0, v0}, LX/1oA;->setScale(FF)V

    .line 318516
    invoke-virtual {v4, v1, v2}, LX/1oA;->postTranslate(FF)Z

    goto :goto_2

    .line 318517
    :cond_a
    int-to-float v0, p2

    int-to-float v1, v3

    div-float/2addr v0, v1

    int-to-float v1, p3

    int-to-float v2, v5

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_4

    .line 318518
    :cond_b
    invoke-static {}, LX/1cy;->m()Landroid/graphics/RectF;

    move-result-object v1

    .line 318519
    invoke-static {}, LX/1cy;->m()Landroid/graphics/RectF;

    move-result-object v2

    .line 318520
    const/4 v0, 0x0

    const/4 v6, 0x0

    int-to-float v3, v3

    int-to-float v5, v5

    :try_start_0
    invoke-virtual {v1, v0, v6, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 318521
    const/4 v0, 0x0

    const/4 v3, 0x0

    int-to-float v5, p2

    int-to-float v6, p3

    invoke-virtual {v2, v0, v3, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 318522
    invoke-static {p1}, LX/1oA;->a(Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Matrix$ScaleToFit;

    move-result-object v0

    invoke-virtual {v4, v1, v2, v0}, LX/1oA;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318523
    invoke-static {v1}, LX/1cy;->a(Landroid/graphics/RectF;)V

    .line 318524
    invoke-static {v2}, LX/1cy;->a(Landroid/graphics/RectF;)V

    goto/16 :goto_2

    .line 318525
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1cy;->a(Landroid/graphics/RectF;)V

    .line 318526
    invoke-static {v2}, LX/1cy;->a(Landroid/graphics/RectF;)V

    throw v0
.end method

.method private static a(Landroid/widget/ImageView$ScaleType;)Landroid/graphics/Matrix$ScaleToFit;
    .locals 2

    .prologue
    .line 318527
    sget-object v0, LX/1vh;->a:[I

    invoke-virtual {p0}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 318528
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only FIT_... values allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318529
    :pswitch_0
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    .line 318530
    :goto_0
    return-object v0

    .line 318531
    :pswitch_1
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->START:Landroid/graphics/Matrix$ScaleToFit;

    goto :goto_0

    .line 318532
    :pswitch_2
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    goto :goto_0

    .line 318533
    :pswitch_3
    sget-object v0, Landroid/graphics/Matrix$ScaleToFit;->END:Landroid/graphics/Matrix$ScaleToFit;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
