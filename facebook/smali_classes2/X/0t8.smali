.class public LX/0t8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 153575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153576
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x43

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    .line 153577
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x1b7352de

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153578
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x3c2b9d5

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "can_viewer_message"

    aput-object v3, v2, v5

    const-string v3, "friendship_status"

    aput-object v3, v2, v6

    const-string v3, "live_video_subscription_status"

    aput-object v3, v2, v7

    const-string v3, "secondary_subscribe_status"

    aput-object v3, v2, v8

    const-string v3, "subscribe_status"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "video_channel_has_viewer_subscribed"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "video_channel_is_viewer_following"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153579
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x5a045f73

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153580
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x49792cfc

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153581
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x64104400

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "is_on_viewer_contact_list"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153582
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x78a7c446

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "has_viewer_claimed"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153583
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x1409faf4

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153584
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x2dc9932c

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153585
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x403827a

    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "can_viewer_change_guest_status"

    aput-object v3, v2, v5

    const-string v3, "event_declines.count"

    aput-object v3, v2, v6

    const-string v3, "event_invitees.count"

    aput-object v3, v2, v7

    const-string v3, "event_maybes.count"

    aput-object v3, v2, v8

    const-string v3, "event_members.count"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "event_watchers.count"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "is_canceled"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "is_event_draft"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "name"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "scheduled_publish_timestamp"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "time_range.end"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "time_range.start"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "time_range.timezone"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "total_purchased_tickets"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "viewer_guest_status"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "viewer_has_pending_invite"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "viewer_notification_subscription_level"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "viewer_watch_status"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153586
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x2763d928

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "assigned_ticket_count"

    aput-object v3, v2, v5

    const-string v3, "available_inventory"

    aput-object v3, v2, v6

    const-string v3, "tier_status"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153587
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x2022d481

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153588
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0xd0d7ab1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "is_favorited"

    aput-object v3, v2, v5

    const-string v3, "show_audience_header"

    aput-object v3, v2, v6

    const-string v3, "video_channel_is_viewer_pinned"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153589
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x78fb05b

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "can_viewer_comment"

    aput-object v3, v2, v5

    const-string v3, "can_viewer_like"

    aput-object v3, v2, v6

    const-string v3, "comments.count"

    aput-object v3, v2, v7

    const-string v3, "does_viewer_like"

    aput-object v3, v2, v8

    const-string v3, "have_comments_been_disabled"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "is_viewer_subscribed"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "likers.count"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "reactors.count"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "reshares.count"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "seen_by.count"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "top_level_comments.count"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "top_level_comments.total_count"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "viewer_feedback_reaction_key"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153590
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x711ea344

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "topic_does_viewer_follow"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153591
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0xe198c7c

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v5

    const-string v3, "members.count"

    aput-object v3, v2, v6

    const-string v3, "name"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153592
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x655ab173

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153593
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x5af615b9

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153594
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x193455fc

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153595
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x41e065f

    const/16 v2, 0x13

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "archived_time"

    aput-object v3, v2, v5

    const-string v3, "can_viewer_claim_adminship"

    aput-object v3, v2, v6

    const-string v3, "description"

    aput-object v3, v2, v7

    const-string v3, "group_owner_authored_stories.available_for_sale_count"

    aput-object v3, v2, v8

    const-string v3, "group_owner_authored_stories.total_for_sale_count"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "has_viewer_favorited"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "join_approval_setting"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "name"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "post_permission_setting"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "requires_admin_membership_approval"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "requires_post_approval"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "subscribe_status"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "unread_count"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "video_channel_has_viewer_subscribed"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "video_channel_is_viewer_following"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "viewer_join_state"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "viewer_push_subscription_level"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "viewer_subscription_level"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "visibility"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153596
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x4af7005f    # 8093743.5f

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153597
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0xe59b30c

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153598
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x51dd9867

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "status"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153599
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x32512483

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153600
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x2df91497

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "has_shared_info"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153601
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x372290f1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "has_shared_info"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153602
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x49e6ef21

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "viewer_has_chosen"

    aput-object v3, v2, v5

    const-string v3, "vote_count"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153603
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x2ca60061

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v5

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153604
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x58e74759

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "is_subscribed"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153605
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x5bdbec3a

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v5

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153606
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x25d1e3bc

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "show_highlight_badge"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153607
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x343dbafa    # -2.5463308E7f

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "messenger_inbox_item_clicks_remaining"

    aput-object v3, v2, v5

    const-string v3, "messenger_inbox_item_hides_remaining"

    aput-object v3, v2, v6

    const-string v3, "messenger_inbox_item_impressions_remaining"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153608
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x34385c89    # -2.6167022E7f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "messenger_inbox_unit_hides_remaining"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153609
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x41eb5d99

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "show_highlight_badge"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153610
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x252222

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "distinct_recommenders_count"

    aput-object v3, v2, v5

    const-string v3, "event_members.count"

    aput-object v3, v2, v6

    const-string v3, "friendship_status"

    aput-object v3, v2, v7

    const-string v3, "is_sold"

    aput-object v3, v2, v8

    const-string v3, "locally_updated_containing_collection_id"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "show_audience_header"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "subscribe_status"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "viewer_guest_status"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "viewer_has_pending_invite"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "viewer_join_state"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "viewer_saved_state"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "viewer_watch_status"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153611
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x2d45dd0b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "local_is_rich_notif_collapsed"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153612
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x2163595b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "local_is_rich_notif_collapsed"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153613
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x275591d0

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153614
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x25d6af

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "does_viewer_like"

    aput-object v3, v2, v5

    const-string v3, "events_calendar_subscriber_count"

    aput-object v3, v2, v6

    const-string v3, "events_calendar_subscription_status"

    aput-object v3, v2, v7

    const-string v3, "is_connect_with_facebook_blacklisted"

    aput-object v3, v2, v8

    const-string v3, "is_viewer_subscribed_to_messenger_content"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "secondary_subscribe_status"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "tarot_publisher_info.is_viewer_subscribed"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "video_channel_has_viewer_subscribed"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "video_channel_is_viewer_following"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153615
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x6c406786

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "border_color"

    aput-object v3, v2, v5

    const-string v3, "color"

    aput-object v3, v2, v6

    const-string v3, "color_spec.border_color"

    aput-object v3, v2, v7

    const-string v3, "color_spec.color"

    aput-object v3, v2, v8

    const-string v3, "color_spec.fill_color"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "fill_color"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153616
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0xb717b40

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "does_viewer_like"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153617
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x21900448

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153618
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x70a4a2e1

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153619
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x533f585d

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153620
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x10d5daa

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153621
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x78e4008f

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v5

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153622
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x681541e0

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153623
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x499e8e7

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "address.full_address"

    aput-object v3, v2, v5

    const-string v3, "name"

    aput-object v3, v2, v6

    const-string v3, "viewer_saved_state"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153624
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x75d3b463

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153625
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x7af1f23b

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153626
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x50c72189

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "does_viewer_like"

    aput-object v3, v2, v5

    const-string v3, "friendship_status"

    aput-object v3, v2, v6

    const-string v3, "secondary_subscribe_status"

    aput-object v3, v2, v7

    const-string v3, "subscribe_status"

    aput-object v3, v2, v8

    const-string v3, "viewer_guest_status"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "viewer_has_pending_invite"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "viewer_join_state"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "viewer_watch_status"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153627
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x197b5b5b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v5

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153628
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x38f9781b

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "viewer_has_voted"

    aput-object v3, v2, v5

    const-string v3, "voters.count"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153629
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x61d8ffb8

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153630
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x180c5609

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153631
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x44774584

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "local_is_completed"

    aput-object v3, v2, v5

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v6

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v7

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153632
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x2d284ade

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "viewer_saved_state"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153633
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x72442839

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153634
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x31509926

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "away_team_fan_count"

    aput-object v3, v2, v5

    const-string v3, "away_team_score"

    aput-object v3, v2, v6

    const-string v3, "home_team_fan_count"

    aput-object v3, v2, v7

    const-string v3, "home_team_score"

    aput-object v3, v2, v8

    const-string v3, "status_text"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "viewer_can_vote_fan_favorite"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153635
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x4c808d5

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "local_edit_pending"

    aput-object v3, v2, v5

    const-string v3, "local_group_did_approve"

    aput-object v3, v2, v6

    const-string v3, "local_group_did_ignore_report"

    aput-object v3, v2, v7

    const-string v3, "local_group_did_pin"

    aput-object v3, v2, v8

    const-string v3, "local_group_did_unpin"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "local_last_negative_feedback_action_type"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "local_story_visibility"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "local_story_visible_height"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "privacy_scope.type"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "save_info.viewer_save_state"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "seen_state"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153636
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x30c82027

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153637
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x6a3d0f4d

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v5

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v6

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153638
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x46f2ee24

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "local_is_completed"

    aput-object v3, v2, v5

    const-string v3, "local_last_negative_feedback_action_type"

    aput-object v3, v2, v6

    const-string v3, "local_story_visibility"

    aput-object v3, v2, v7

    const-string v3, "local_story_visible_height"

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153639
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x939b30f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "locally_updated_containing_collection_id"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153640
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, -0x201e1d4c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "is_checked"

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153641
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x285feb

    const/16 v2, 0xf

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "can_viewer_message"

    aput-object v3, v2, v5

    const-string v3, "can_viewer_poke"

    aput-object v3, v2, v6

    const-string v3, "can_viewer_post"

    aput-object v3, v2, v7

    const-string v3, "friendship_status"

    aput-object v3, v2, v8

    const-string v3, "is_connect_with_facebook_blacklisted"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "is_messenger_cymk_hidden"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "is_pymm_hidden"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "is_pysf_blacklisted"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "local_is_pymk_blacklisted"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "secondary_subscribe_status"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "subscribe_status"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "username"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "video_channel_has_viewer_subscribed"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "video_channel_is_viewer_following"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "viewer_saved_state"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153642
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x56118b5d    # 4.0006937E13f

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "formatted_start_time"

    aput-object v3, v2, v5

    const-string v3, "is_rescheduled"

    aput-object v3, v2, v6

    const-string v3, "is_viewer_subscribed"

    aput-object v3, v2, v7

    const-string v3, "rescheduled_endscreen_body"

    aput-object v3, v2, v8

    const-string v3, "rescheduled_endscreen_title"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "rescheduled_heading"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "start_time"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153643
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    const v1, 0x2d116428

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "video_channel_has_viewer_subscribed"

    aput-object v3, v2, v5

    const-string v3, "video_channel_is_viewer_following"

    aput-object v3, v2, v6

    const-string v3, "video_channel_is_viewer_pinned"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 153644
    return-void
.end method


# virtual methods
.method public final a(I)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 153645
    iget-object v0, p0, LX/0t8;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153646
    packed-switch p1, :pswitch_data_0

    .line 153647
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 153648
    :pswitch_0
    const-string v0, "local_last_negative_feedback_action_type"

    .line 153649
    :goto_0
    return-object v0

    .line 153650
    :pswitch_1
    const-string v0, "local_story_visibility"

    goto :goto_0

    .line 153651
    :pswitch_2
    const-string v0, "local_story_visible_height"

    goto :goto_0

    .line 153652
    :pswitch_3
    const-string v0, "can_viewer_message"

    goto :goto_0

    .line 153653
    :pswitch_4
    const-string v0, "friendship_status"

    goto :goto_0

    .line 153654
    :pswitch_5
    const-string v0, "live_video_subscription_status"

    goto :goto_0

    .line 153655
    :pswitch_6
    const-string v0, "secondary_subscribe_status"

    goto :goto_0

    .line 153656
    :pswitch_7
    const-string v0, "subscribe_status"

    goto :goto_0

    .line 153657
    :pswitch_8
    const-string v0, "video_channel_has_viewer_subscribed"

    goto :goto_0

    .line 153658
    :pswitch_9
    const-string v0, "video_channel_is_viewer_following"

    goto :goto_0

    .line 153659
    :pswitch_a
    const-string v0, "is_on_viewer_contact_list"

    goto :goto_0

    .line 153660
    :pswitch_b
    const-string v0, "has_viewer_claimed"

    goto :goto_0

    .line 153661
    :pswitch_c
    const-string v0, "can_viewer_change_guest_status"

    goto :goto_0

    .line 153662
    :pswitch_d
    const-string v0, "event_declines.count"

    goto :goto_0

    .line 153663
    :pswitch_e
    const-string v0, "event_invitees.count"

    goto :goto_0

    .line 153664
    :pswitch_f
    const-string v0, "event_maybes.count"

    goto :goto_0

    .line 153665
    :pswitch_10
    const-string v0, "event_members.count"

    goto :goto_0

    .line 153666
    :pswitch_11
    const-string v0, "event_watchers.count"

    goto :goto_0

    .line 153667
    :pswitch_12
    const-string v0, "is_canceled"

    goto :goto_0

    .line 153668
    :pswitch_13
    const-string v0, "is_event_draft"

    goto :goto_0

    .line 153669
    :pswitch_14
    const-string v0, "name"

    goto :goto_0

    .line 153670
    :pswitch_15
    const-string v0, "scheduled_publish_timestamp"

    goto :goto_0

    .line 153671
    :pswitch_16
    const-string v0, "time_range.end"

    goto :goto_0

    .line 153672
    :pswitch_17
    const-string v0, "time_range.start"

    goto :goto_0

    .line 153673
    :pswitch_18
    const-string v0, "time_range.timezone"

    goto :goto_0

    .line 153674
    :pswitch_19
    const-string v0, "total_purchased_tickets"

    goto :goto_0

    .line 153675
    :pswitch_1a
    const-string v0, "viewer_guest_status"

    goto :goto_0

    .line 153676
    :pswitch_1b
    const-string v0, "viewer_has_pending_invite"

    goto :goto_0

    .line 153677
    :pswitch_1c
    const-string v0, "viewer_notification_subscription_level"

    goto :goto_0

    .line 153678
    :pswitch_1d
    const-string v0, "viewer_watch_status"

    goto :goto_0

    .line 153679
    :pswitch_1e
    const-string v0, "assigned_ticket_count"

    goto :goto_0

    .line 153680
    :pswitch_1f
    const-string v0, "available_inventory"

    goto :goto_0

    .line 153681
    :pswitch_20
    const-string v0, "tier_status"

    goto :goto_0

    .line 153682
    :pswitch_21
    const-string v0, "is_favorited"

    goto :goto_0

    .line 153683
    :pswitch_22
    const-string v0, "show_audience_header"

    goto :goto_0

    .line 153684
    :pswitch_23
    const-string v0, "video_channel_is_viewer_pinned"

    goto :goto_0

    .line 153685
    :pswitch_24
    const-string v0, "can_viewer_comment"

    goto :goto_0

    .line 153686
    :pswitch_25
    const-string v0, "can_viewer_like"

    goto :goto_0

    .line 153687
    :pswitch_26
    const-string v0, "comments.count"

    goto :goto_0

    .line 153688
    :pswitch_27
    const-string v0, "does_viewer_like"

    goto :goto_0

    .line 153689
    :pswitch_28
    const-string v0, "have_comments_been_disabled"

    goto :goto_0

    .line 153690
    :pswitch_29
    const-string v0, "is_viewer_subscribed"

    goto :goto_0

    .line 153691
    :pswitch_2a
    const-string v0, "likers.count"

    goto :goto_0

    .line 153692
    :pswitch_2b
    const-string v0, "reactors.count"

    goto/16 :goto_0

    .line 153693
    :pswitch_2c
    const-string v0, "reshares.count"

    goto/16 :goto_0

    .line 153694
    :pswitch_2d
    const-string v0, "seen_by.count"

    goto/16 :goto_0

    .line 153695
    :pswitch_2e
    const-string v0, "top_level_comments.count"

    goto/16 :goto_0

    .line 153696
    :pswitch_2f
    const-string v0, "top_level_comments.total_count"

    goto/16 :goto_0

    .line 153697
    :pswitch_30
    const-string v0, "viewer_feedback_reaction_key"

    goto/16 :goto_0

    .line 153698
    :pswitch_31
    const-string v0, "topic_does_viewer_follow"

    goto/16 :goto_0

    .line 153699
    :pswitch_32
    const-string v0, "id"

    goto/16 :goto_0

    .line 153700
    :pswitch_33
    const-string v0, "members.count"

    goto/16 :goto_0

    .line 153701
    :pswitch_34
    const-string v0, "archived_time"

    goto/16 :goto_0

    .line 153702
    :pswitch_35
    const-string v0, "can_viewer_claim_adminship"

    goto/16 :goto_0

    .line 153703
    :pswitch_36
    const-string v0, "description"

    goto/16 :goto_0

    .line 153704
    :pswitch_37
    const-string v0, "group_owner_authored_stories.available_for_sale_count"

    goto/16 :goto_0

    .line 153705
    :pswitch_38
    const-string v0, "group_owner_authored_stories.total_for_sale_count"

    goto/16 :goto_0

    .line 153706
    :pswitch_39
    const-string v0, "has_viewer_favorited"

    goto/16 :goto_0

    .line 153707
    :pswitch_3a
    const-string v0, "join_approval_setting"

    goto/16 :goto_0

    .line 153708
    :pswitch_3b
    const-string v0, "post_permission_setting"

    goto/16 :goto_0

    .line 153709
    :pswitch_3c
    const-string v0, "requires_admin_membership_approval"

    goto/16 :goto_0

    .line 153710
    :pswitch_3d
    const-string v0, "requires_post_approval"

    goto/16 :goto_0

    .line 153711
    :pswitch_3e
    const-string v0, "unread_count"

    goto/16 :goto_0

    .line 153712
    :pswitch_3f
    const-string v0, "viewer_join_state"

    goto/16 :goto_0

    .line 153713
    :pswitch_40
    const-string v0, "viewer_push_subscription_level"

    goto/16 :goto_0

    .line 153714
    :pswitch_41
    const-string v0, "viewer_subscription_level"

    goto/16 :goto_0

    .line 153715
    :pswitch_42
    const-string v0, "visibility"

    goto/16 :goto_0

    .line 153716
    :pswitch_43
    const-string v0, "status"

    goto/16 :goto_0

    .line 153717
    :pswitch_44
    const-string v0, "has_shared_info"

    goto/16 :goto_0

    .line 153718
    :pswitch_45
    const-string v0, "viewer_has_chosen"

    goto/16 :goto_0

    .line 153719
    :pswitch_46
    const-string v0, "vote_count"

    goto/16 :goto_0

    .line 153720
    :pswitch_47
    const-string v0, "is_subscribed"

    goto/16 :goto_0

    .line 153721
    :pswitch_48
    const-string v0, "show_highlight_badge"

    goto/16 :goto_0

    .line 153722
    :pswitch_49
    const-string v0, "messenger_inbox_item_clicks_remaining"

    goto/16 :goto_0

    .line 153723
    :pswitch_4a
    const-string v0, "messenger_inbox_item_hides_remaining"

    goto/16 :goto_0

    .line 153724
    :pswitch_4b
    const-string v0, "messenger_inbox_item_impressions_remaining"

    goto/16 :goto_0

    .line 153725
    :pswitch_4c
    const-string v0, "messenger_inbox_unit_hides_remaining"

    goto/16 :goto_0

    .line 153726
    :pswitch_4d
    const-string v0, "distinct_recommenders_count"

    goto/16 :goto_0

    .line 153727
    :pswitch_4e
    const-string v0, "is_sold"

    goto/16 :goto_0

    .line 153728
    :pswitch_4f
    const-string v0, "locally_updated_containing_collection_id"

    goto/16 :goto_0

    .line 153729
    :pswitch_50
    const-string v0, "viewer_saved_state"

    goto/16 :goto_0

    .line 153730
    :pswitch_51
    const-string v0, "local_is_rich_notif_collapsed"

    goto/16 :goto_0

    .line 153731
    :pswitch_52
    const-string v0, "events_calendar_subscriber_count"

    goto/16 :goto_0

    .line 153732
    :pswitch_53
    const-string v0, "events_calendar_subscription_status"

    goto/16 :goto_0

    .line 153733
    :pswitch_54
    const-string v0, "is_connect_with_facebook_blacklisted"

    goto/16 :goto_0

    .line 153734
    :pswitch_55
    const-string v0, "is_viewer_subscribed_to_messenger_content"

    goto/16 :goto_0

    .line 153735
    :pswitch_56
    const-string v0, "tarot_publisher_info.is_viewer_subscribed"

    goto/16 :goto_0

    .line 153736
    :pswitch_57
    const-string v0, "border_color"

    goto/16 :goto_0

    .line 153737
    :pswitch_58
    const-string v0, "color"

    goto/16 :goto_0

    .line 153738
    :pswitch_59
    const-string v0, "color_spec.border_color"

    goto/16 :goto_0

    .line 153739
    :pswitch_5a
    const-string v0, "color_spec.color"

    goto/16 :goto_0

    .line 153740
    :pswitch_5b
    const-string v0, "color_spec.fill_color"

    goto/16 :goto_0

    .line 153741
    :pswitch_5c
    const-string v0, "fill_color"

    goto/16 :goto_0

    .line 153742
    :pswitch_5d
    const-string v0, "address.full_address"

    goto/16 :goto_0

    .line 153743
    :pswitch_5e
    const-string v0, "viewer_has_voted"

    goto/16 :goto_0

    .line 153744
    :pswitch_5f
    const-string v0, "voters.count"

    goto/16 :goto_0

    .line 153745
    :pswitch_60
    const-string v0, "local_is_completed"

    goto/16 :goto_0

    .line 153746
    :pswitch_61
    const-string v0, "away_team_fan_count"

    goto/16 :goto_0

    .line 153747
    :pswitch_62
    const-string v0, "away_team_score"

    goto/16 :goto_0

    .line 153748
    :pswitch_63
    const-string v0, "home_team_fan_count"

    goto/16 :goto_0

    .line 153749
    :pswitch_64
    const-string v0, "home_team_score"

    goto/16 :goto_0

    .line 153750
    :pswitch_65
    const-string v0, "status_text"

    goto/16 :goto_0

    .line 153751
    :pswitch_66
    const-string v0, "viewer_can_vote_fan_favorite"

    goto/16 :goto_0

    .line 153752
    :pswitch_67
    const-string v0, "local_edit_pending"

    goto/16 :goto_0

    .line 153753
    :pswitch_68
    const-string v0, "local_group_did_approve"

    goto/16 :goto_0

    .line 153754
    :pswitch_69
    const-string v0, "local_group_did_ignore_report"

    goto/16 :goto_0

    .line 153755
    :pswitch_6a
    const-string v0, "local_group_did_pin"

    goto/16 :goto_0

    .line 153756
    :pswitch_6b
    const-string v0, "local_group_did_unpin"

    goto/16 :goto_0

    .line 153757
    :pswitch_6c
    const-string v0, "privacy_scope.type"

    goto/16 :goto_0

    .line 153758
    :pswitch_6d
    const-string v0, "save_info.viewer_save_state"

    goto/16 :goto_0

    .line 153759
    :pswitch_6e
    const-string v0, "seen_state"

    goto/16 :goto_0

    .line 153760
    :pswitch_6f
    const-string v0, "is_checked"

    goto/16 :goto_0

    .line 153761
    :pswitch_70
    const-string v0, "can_viewer_poke"

    goto/16 :goto_0

    .line 153762
    :pswitch_71
    const-string v0, "can_viewer_post"

    goto/16 :goto_0

    .line 153763
    :pswitch_72
    const-string v0, "is_messenger_cymk_hidden"

    goto/16 :goto_0

    .line 153764
    :pswitch_73
    const-string v0, "is_pymm_hidden"

    goto/16 :goto_0

    .line 153765
    :pswitch_74
    const-string v0, "is_pysf_blacklisted"

    goto/16 :goto_0

    .line 153766
    :pswitch_75
    const-string v0, "local_is_pymk_blacklisted"

    goto/16 :goto_0

    .line 153767
    :pswitch_76
    const-string v0, "username"

    goto/16 :goto_0

    .line 153768
    :pswitch_77
    const-string v0, "formatted_start_time"

    goto/16 :goto_0

    .line 153769
    :pswitch_78
    const-string v0, "is_rescheduled"

    goto/16 :goto_0

    .line 153770
    :pswitch_79
    const-string v0, "rescheduled_endscreen_body"

    goto/16 :goto_0

    .line 153771
    :pswitch_7a
    const-string v0, "rescheduled_endscreen_title"

    goto/16 :goto_0

    .line 153772
    :pswitch_7b
    const-string v0, "rescheduled_heading"

    goto/16 :goto_0

    .line 153773
    :pswitch_7c
    const-string v0, "start_time"

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_14
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_14
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_7
        :pswitch_3e
        :pswitch_8
        :pswitch_9
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_43
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_44
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_1
        :pswitch_2
        :pswitch_47
        :pswitch_1
        :pswitch_2
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_48
        :pswitch_4d
        :pswitch_10
        :pswitch_4
        :pswitch_4e
        :pswitch_4f
        :pswitch_22
        :pswitch_7
        :pswitch_1a
        :pswitch_1b
        :pswitch_3f
        :pswitch_50
        :pswitch_1d
        :pswitch_51
        :pswitch_51
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_27
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_6
        :pswitch_56
        :pswitch_8
        :pswitch_9
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_27
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5d
        :pswitch_14
        :pswitch_50
        :pswitch_32
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_27
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_1a
        :pswitch_1b
        :pswitch_3f
        :pswitch_1d
        :pswitch_1
        :pswitch_2
        :pswitch_5e
        :pswitch_5f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_60
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_50
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_60
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4f
        :pswitch_6f
        :pswitch_3
        :pswitch_70
        :pswitch_71
        :pswitch_4
        :pswitch_54
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_6
        :pswitch_7
        :pswitch_76
        :pswitch_8
        :pswitch_9
        :pswitch_50
        :pswitch_77
        :pswitch_78
        :pswitch_29
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_8
        :pswitch_9
        :pswitch_23
    .end packed-switch
.end method

.method public final c(I)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 153774
    packed-switch p1, :pswitch_data_0

    .line 153775
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 153776
    :pswitch_0
    const-class v0, Ljava/lang/String;

    .line 153777
    :goto_0
    return-object v0

    .line 153778
    :pswitch_1
    const-class v0, Ljava/lang/Integer;

    goto :goto_0

    .line 153779
    :pswitch_2
    const-class v0, Ljava/lang/Boolean;

    goto :goto_0

    .line 153780
    :pswitch_3
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 153781
    :pswitch_4
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    goto :goto_0

    .line 153782
    :pswitch_5
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_0

    .line 153783
    :pswitch_6
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 153784
    :pswitch_7
    const-class v0, Ljava/lang/Long;

    goto :goto_0

    .line 153785
    :pswitch_8
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    .line 153786
    :pswitch_9
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    goto :goto_0

    .line 153787
    :pswitch_a
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_0

    .line 153788
    :pswitch_b
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventTicketTierStatusEnum;

    goto :goto_0

    .line 153789
    :pswitch_c
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    goto :goto_0

    .line 153790
    :pswitch_d
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    goto :goto_0

    .line 153791
    :pswitch_e
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_0

    .line 153792
    :pswitch_f
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 153793
    :pswitch_10
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    goto :goto_0

    .line 153794
    :pswitch_11
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    goto :goto_0

    .line 153795
    :pswitch_12
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    goto :goto_0

    .line 153796
    :pswitch_13
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 153797
    :pswitch_14
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    goto :goto_0

    .line 153798
    :pswitch_15
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_2
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_1
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_8
        :pswitch_2
        :pswitch_e
        :pswitch_13
        :pswitch_a
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_14
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_2
        :pswitch_e
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_13
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_13
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
