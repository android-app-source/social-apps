.class public LX/0Sy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/0Sy;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:LX/0Sz;

.field public final f:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mInteractingStateLock"
    .end annotation
.end field

.field private final g:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "LX/0Vq;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mListenerLock"
    .end annotation
.end field

.field private h:Landroid/os/Handler;

.field private i:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mInteractingStateLock"
    .end annotation
.end field

.field public j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Vq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62464
    const-class v0, LX/0Sy;

    sput-object v0, LX/0Sy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 62465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62466
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 62467
    iput-object v0, p0, LX/0Sy;->b:LX/0Ot;

    .line 62468
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 62469
    iput-object v0, p0, LX/0Sy;->c:LX/0Ot;

    .line 62470
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 62471
    iput-object v0, p0, LX/0Sy;->d:LX/0Ot;

    .line 62472
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0Sy;->j:Ljava/lang/Object;

    .line 62473
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/0Sy;->k:Ljava/lang/Object;

    .line 62474
    new-instance v0, LX/0Sz;

    invoke-direct {v0, p0}, LX/0Sz;-><init>(LX/0Sy;)V

    iput-object v0, p0, LX/0Sy;->e:LX/0Sz;

    .line 62475
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    .line 62476
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/0Sy;->g:Ljava/util/WeakHashMap;

    .line 62477
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LX/0T4;

    invoke-direct {v2, p0}, LX/0T4;-><init>(LX/0Sy;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LX/0Sy;->h:Landroid/os/Handler;

    .line 62478
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/0Sy;->l:Ljava/util/List;

    .line 62479
    return-void
.end method

.method public static a(LX/0QB;)LX/0Sy;
    .locals 6

    .prologue
    .line 62480
    sget-object v0, LX/0Sy;->m:LX/0Sy;

    if-nez v0, :cond_1

    .line 62481
    const-class v1, LX/0Sy;

    monitor-enter v1

    .line 62482
    :try_start_0
    sget-object v0, LX/0Sy;->m:LX/0Sy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 62483
    if-eqz v2, :cond_0

    .line 62484
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 62485
    new-instance v3, LX/0Sy;

    invoke-direct {v3}, LX/0Sy;-><init>()V

    .line 62486
    const/16 v4, 0x271

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2db

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 62487
    iput-object v4, v3, LX/0Sy;->b:LX/0Ot;

    iput-object v5, v3, LX/0Sy;->c:LX/0Ot;

    iput-object p0, v3, LX/0Sy;->d:LX/0Ot;

    .line 62488
    move-object v0, v3

    .line 62489
    sput-object v0, LX/0Sy;->m:LX/0Sy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62490
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 62491
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62492
    :cond_1
    sget-object v0, LX/0Sy;->m:LX/0Sy;

    return-object v0

    .line 62493
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 62494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()V
    .locals 10

    .prologue
    .line 62391
    iget-object v0, p0, LX/0Sy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 62392
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 62393
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62394
    :try_start_0
    iget-object v2, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v2}, Ljava/util/WeakHashMap;->putAll(Ljava/util/Map;)V

    .line 62395
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62396
    const/4 v2, 0x0

    .line 62397
    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 62398
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/32 v8, 0xea60

    cmp-long v0, v6, v8

    if-ltz v0, :cond_0

    .line 62399
    if-nez v2, :cond_1

    .line 62400
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    move-object v2, v0

    .line 62401
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62402
    iget-object v0, p0, LX/0Sy;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v6, "DefaultUserInteractionController"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "View "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " may not have ended it\'s user interaction event"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62403
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 62404
    :cond_2
    if-nez v2, :cond_3

    .line 62405
    :goto_1
    return-void

    .line 62406
    :cond_3
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62407
    :try_start_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 62408
    iget-object v3, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v3, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 62409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 62410
    :cond_4
    :try_start_3
    iget-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    .line 62411
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 62412
    invoke-direct {p0}, LX/0Sy;->e()V

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    .line 62495
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62496
    :try_start_0
    invoke-virtual {p0}, LX/0Sy;->b()Z

    move-result v0

    .line 62497
    iget-boolean v2, p0, LX/0Sy;->i:Z

    if-eq v0, v2, :cond_0

    .line 62498
    iget-object v0, p0, LX/0Sy;->h:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 62499
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static f(LX/0Sy;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 62430
    iget-object v0, p0, LX/0Sy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 62431
    iget-object v2, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 62432
    :try_start_0
    invoke-virtual {p0}, LX/0Sy;->b()Z

    move-result v0

    .line 62433
    iget-boolean v3, p0, LX/0Sy;->i:Z

    if-eq v0, v3, :cond_3

    .line 62434
    const/4 v1, 0x1

    .line 62435
    iput-boolean v0, p0, LX/0Sy;->i:Z

    move v5, v0

    move v0, v1

    move v1, v5

    .line 62436
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62437
    if-eqz v0, :cond_2

    .line 62438
    iget-object v0, p0, LX/0Sy;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 62439
    iget-object v2, p0, LX/0Sy;->k:Ljava/lang/Object;

    monitor-enter v2

    .line 62440
    :try_start_1
    iget-object v0, p0, LX/0Sy;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vq;

    .line 62441
    iget-object v4, p0, LX/0Sy;->l:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62442
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 62443
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 62444
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62445
    iget-object v0, p0, LX/0Sy;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Vq;

    .line 62446
    invoke-interface {v0, v1}, LX/0Vq;->a(Z)V

    goto :goto_2

    .line 62447
    :cond_1
    iget-object v0, p0, LX/0Sy;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 62448
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 62449
    iget-object v0, p0, LX/0Sy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 62450
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 62451
    new-instance v1, LX/2tJ;

    invoke-direct {v1, p0, v0}, LX/2tJ;-><init>(LX/0Sy;Ljava/util/concurrent/Semaphore;)V

    .line 62452
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 62453
    invoke-virtual {p0, v1}, LX/0Sy;->a(LX/0Vq;)V

    .line 62454
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v4}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 62455
    if-nez v0, :cond_0

    .line 62456
    invoke-direct {p0}, LX/0Sy;->d()V

    .line 62457
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 62458
    const-wide/16 v4, 0xa

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 62459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "User interaction blocked data processing for "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ms."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62460
    :cond_1
    invoke-virtual {p0, v1}, LX/0Sy;->b(LX/0Vq;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62461
    :goto_0
    return-void

    .line 62462
    :catch_0
    move-exception v0

    .line 62463
    sget-object v1, LX/0Sy;->a:Ljava/lang/Class;

    const-string v2, "Exception when the user interaction to be finished."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(LX/0Vq;)V
    .locals 3

    .prologue
    .line 62424
    iget-object v1, p0, LX/0Sy;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 62425
    :try_start_0
    iget-object v0, p0, LX/0Sy;->g:Ljava/util/WeakHashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62426
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62427
    invoke-virtual {p0}, LX/0Sy;->b()Z

    move-result v0

    invoke-interface {p1, v0}, LX/0Vq;->a(Z)V

    .line 62428
    return-void

    .line 62429
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 62416
    iget-object v0, p0, LX/0Sy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 62417
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62418
    :try_start_0
    iget-object v2, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    iget-object v0, p0, LX/0Sy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62419
    iget-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    .line 62420
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62421
    invoke-static {p0}, LX/0Sy;->f(LX/0Sy;)V

    .line 62422
    return-void

    .line 62423
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/0Vq;)V
    .locals 2

    .prologue
    .line 62413
    iget-object v1, p0, LX/0Sy;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 62414
    :try_start_0
    iget-object v0, p0, LX/0Sy;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62415
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 62383
    iget-object v0, p0, LX/0Sy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 62384
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62385
    :try_start_0
    iget-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62386
    iget-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    .line 62387
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62388
    invoke-direct {p0}, LX/0Sy;->e()V

    .line 62389
    return-void

    .line 62390
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 62380
    iget-object v1, p0, LX/0Sy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 62381
    :try_start_0
    iget-object v0, p0, LX/0Sy;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62382
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 62378
    const-wide/16 v0, 0x2710

    invoke-virtual {p0, v0, v1}, LX/0Sy;->a(J)V

    .line 62379
    return-void
.end method
